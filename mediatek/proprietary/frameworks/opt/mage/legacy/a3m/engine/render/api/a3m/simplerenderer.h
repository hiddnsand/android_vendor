/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Simple Renderer
 *
 */
#pragma once
#ifndef A3M_SIMPLERENDERER_H
#define A3M_SIMPLERENDERER_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/assetcachepool.h>    /* for AssetCachePool                     */
#include <a3m/renderer.h>          /* Renderer interface                     */
#include <a3m/scenenodevisitor.h>  /* for SceneNodeVisitor                   */
#include <vector>                  /* for std::vector                        */
#include <a3m/rendertarget.h>      /* for reflection render target           */

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  class RenderTarget;

  /** \todo Integrate into documentation (this is not currently in any groups) */


  /** SimpleRenderer class.
   * An implementation of the Renderer interface. This class was originally
   * written to do the minimum necessary work to display a scene graph. It has
   * grown now to set lighting uniforms and render to texture for reflection
   * effects.
   * \todo design an alternative (a selection of different renderers or a
   *       generic means to assemble a number of render stages).
   */
  class SimpleRenderer : public Renderer, public SceneNodeVisitor
  {
  public:
    /** Default constructor.
     */
    SimpleRenderer( /** Context to use when rendering the scene */
      RenderContext::Ptr const& context,
      /** Cache pool for loading renderer assets */
      AssetCachePool::Ptr const& pool );

    // Override
    void render( Camera* camera, SceneNode& node, FlagMask const& renderFlags,
                 FlagMask const& recursiveFlags );

    // Override
    void update( A3M_FLOAT time ) { m_time = time; }

    // Override
    RenderContext::Ptr getRenderContext() const { return m_context; }

    // Override
    void visit( Solid* solid );

    // Override
    void visit( Light* light );

    /** \cond */
    // These types are public so that free functions in the implementation
    // can access them. They are not part of the interface.
    typedef std::vector< Solid* > RenderList;
    typedef std::vector< Light* > LightList;
    /** \endcond */

  private:
    RenderList m_opaqueSolidsToRender;  /* List of solids to be rendered
                                           (generated each frame). */
    RenderList m_blendedSolidsToRender; /* List of blended solids to be
                                           rendered (generated each frame). */
    RenderList m_reflectionSolidsToRender; /* List of solids using reflection.
                                              (generated each frame). */
    LightList m_lights;                 // List of lights in the scene.

    RenderContext::Ptr m_context; // Tracks global render state
    AssetCachePool::Ptr m_pool; // Pool for asset creation

    // A render target used for reflection effect. Only created when it is
    // requested.
    RenderTarget::Ptr m_reflectionTarget;

    A3M_FLOAT m_time; // Time to set in uniform "u_time"

    A3M_INT32 m_lightZOrder; // Platform related
    A3M_INT32 m_lightCounter; // Platform related
  };
  /** @} */

} /* namespace a3m */

#endif /* A3M_SIMPLERENDERER_H */
