/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.ContentResolver;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.ListView;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.touchscreen.TouchScreenList;
import com.mediatek.engineermode.touchscreen.TouchScreenSettings;
import com.mediatek.engineermode.touchscreen.TsHandWriting;
import com.mediatek.engineermode.touchscreen.TsMultiTouch;
import com.mediatek.engineermode.touchscreen.TsRateReport;
import com.mediatek.engineermode.touchscreen.TsVerifyList;
import com.mediatek.engineermode.touchscreen.TsVerifyLine;
import com.mediatek.engineermode.touchscreen.TsVerifyShakingPoint;

public class TouchScreenTest extends ActivityInstrumentationTestCase2<TouchScreenList> {

    private static final String TAG = "TouchScreenList";
    private static final String EM_BAKC_FAIL_MSG = "Back to TouchScreenList fail";
    private Solo mSolo = null;
    private Activity mActivity = null;
    private Context mContext = null;
    private Instrumentation mInst = null;
    private ContentResolver mCr = null;

    public TouchScreenTest() {
        super(TouchScreenList.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        mInst = getInstrumentation();
        mContext = mInst.getTargetContext();
        mActivity = getActivity();
        mSolo = new Solo(mInst, mActivity);
    }

    @UiAnnotation
    public void test01_Precondition() {
        assertNotNull(mInst);
        assertNotNull(mContext);
        assertNotNull(mActivity);
        assertNotNull(mSolo);
    }

    @UiAnnotation
    public void test02_HandWriting() {
        mSolo.sleep(EmOperate.TIME_MID);
        if (!EmOperate.enterItem(mSolo, "HandWriting", "TsHandWriting"))
            return;
        mSolo.sleep(EmOperate.TIME_MID);
        mSolo.goBack();
    }

    @UiAnnotation
    public void test03_VerificationList() {
        mSolo.sleep(EmOperate.TIME_MID);
        if (!EmOperate.enterItem(mSolo, "Verification", "TsVerifyList"))
            return;
        mSolo.sleep(EmOperate.TIME_MID);
        if (!EmOperate.enterItem(mSolo, "PointVerification", "TsVerifyShakingPoint"))
            return;
        mSolo.sleep(EmOperate.TIME_MID);
        EmOperate.backKey(mInst);

        EmOperate.enterItem(mSolo, "LineVerification", "TsVerifyLine");
        mSolo.sleep(EmOperate.TIME_MID);
        EmOperate.backKey(mInst);

        EmOperate.enterItem(mSolo, "ShakingVerification", "TsVerifyShakingPoint");
        mSolo.sleep(EmOperate.TIME_MID);
        EmOperate.backKey(mInst);

        EmOperate.backKey(mInst);
    }

    @UiAnnotation
    public void test04_Settings() {

        if (!EmOperate.enterItem(mSolo, "Settings", "TouchScreenSettings"))
            return;

        TouchTest_settings();
        EmOperate.backKey(mInst);
    }

    @UiAnnotation
    public void TouchTest_settings() {

        // open uart log
        mSolo.pressSpinnerItem(0, 0);
        mSolo.clearEditText(0);
        mSolo.enterText(0, "1");
        mSolo.clickOnButton(0);

        assertTrue("TouchTest_settings have issues", mSolo.waitForText("success"));
        mSolo.sleep(EmOperate.TIME_MID);

        // close uart log
        mSolo.clearEditText(0);
        mSolo.enterText(0, "0");
        mSolo.clickOnButton(0);

        assertTrue("TouchTest_settings have issues", mSolo.waitForText("success"));
        mSolo.sleep(EmOperate.TIME_MID);
        // sd record
        mSolo.pressSpinnerItem(0, 1);

        mSolo.clearEditText(0);
        mSolo.enterText(0, "1");
        mSolo.clickOnButton(0);

        assertTrue("TouchTest_settings have issues", mSolo.waitForText("success"));
        mSolo.sleep(EmOperate.TIME_LONG);

        mSolo.clearEditText(0);
        mSolo.enterText(0, "0");
        mSolo.clickOnButton(0);

        assertTrue("TouchTest_settings have issues", mSolo.waitForText("success"));
        mSolo.sleep(EmOperate.TIME_MID);
        // para write
        mSolo.pressSpinnerItem(0, 10);
        mSolo.clickOnButton(0);

        assertTrue("TouchTest_settings have issues", mSolo.waitForText("success"));
    }

    @UiAnnotation
    public void test05_RateReport() {

        if (!EmOperate.enterItem(mSolo, "RateReport", "TsRateReport"))
            return;
        EmOperate.waitSomeTime(EmOperate.TIME_MID);
        EmOperate.backKey(mInst);
    }

    @UiAnnotation
    public void test06_MutilTouch() {

        if(!EmOperate.enterItem(mSolo, "MultiTouch", "TsMultiTouch"))
            return;

        EmOperate.backKey(mInst);
    }

}
