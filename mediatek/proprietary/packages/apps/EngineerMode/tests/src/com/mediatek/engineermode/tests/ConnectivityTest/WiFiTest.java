/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.SystemProperties;
import android.test.SingleLaunchActivityTestCase;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.content.Intent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.test.ActivityUnitTestCase;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.wifi.WiFi;

import java.util.ArrayList;

public class WiFiTest extends SingleLaunchActivityTestCase<WiFi> {

    private static final String TAG = "EMTest/wifi";

    private Solo sSolo = null;
    private Activity sActivity = null;
    private Context sContext = null;
    private Instrumentation sInst = null;
    private ListView sListView = null;
    private static boolean sFinished = false;

    private static boolean s11acSupported = false;
    private WifiManager mWifiManager = null;
    private static Boolean[] checkBoxStatus = new Boolean[10];

    public WiFiTest() {
        super("com.mediatek.engineermode", WiFi.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        sInst = getInstrumentation();
        sContext = sInst.getTargetContext();
        sActivity = getActivity();
        sSolo = new Solo(sInst, sActivity);
    }

    @Override
    protected void tearDown() throws Exception {
        if (sFinished) {
            sSolo.finishOpenedActivities();
        }
        super.tearDown();
    }

    private void disableWiFi() {
        mWifiManager = (WifiManager) sContext.getSystemService(Context.WIFI_SERVICE);
        assertNotNull(mWifiManager);
        EmOperate.waitSomeTime(EmOperate.TIME_MID);
        if (WifiManager.WIFI_STATE_DISABLED != mWifiManager.getWifiState()) {
            mWifiManager.setWifiEnabled(false);
            int jumpCount = 0;
            while (WifiManager.WIFI_STATE_DISABLED != mWifiManager.getWifiState()
                    && (jumpCount++ < 15)) {
                EmOperate.waitSomeTime(EmOperate.TIME_MID);
            }
            assertTrue("disableWiFi have issues",
                    WifiManager.WIFI_STATE_DISABLED == mWifiManager.getWifiState());
        }
    }

    @BasicFuncAnnotation
    public void test01_Precondition() {
        assertNotNull(sInst);
        assertNotNull(sContext);
        assertNotNull(sActivity);
        assertNotNull(sSolo);

        if (!sSolo.waitForActivity("WiFi", EmOperate.TIME_MID)) {
            Bundle mBundle = new Bundle();
            mBundle.putString("Data", "data from TestBundle");
            launchActivity("com.mediatek.engineermode", WiFi.class, mBundle);
        }

        if (sSolo.searchText("Error")) {
            sSolo.clickOnText("OK");
            disableWiFi();
            Intent mStartIntent = new Intent();
            mStartIntent.setClass(sContext, WiFi.class);
            sContext.startActivity(mStartIntent);
        }

        // while (sSolo.searchText("Please wait for device to initialize ..."))
        // {
        // sSolo.sleep(EmOperate.TIME_MID);
        // }
    }

    @UiAnnotation
    public void test02_WiFiMainUITest() {
        sSolo.assertCurrentActivity("Not test02_WiFiMainUITest Class", WiFi.class);
        sSolo.sleep(EmOperate.TIME_LONG);

        ArrayList<CheckBox> checkBoxs = sSolo.getCurrentViews(CheckBox.class);

        for (int i = 0; i < checkBoxs.size(); i++) {
            CheckBox check = checkBoxs.get(i);
            if (check.isShown()) {
                sSolo.clickOnView(check);
                sSolo.sleep(EmOperate.TIME_SHORT);
                checkBoxStatus[i] = check.isChecked();
            }
        }

    }

    @UiAnnotation
    public void test03_WiFiMainUITestBack() {

        ArrayList<CheckBox> checkBoxs = sSolo.getCurrentViews(CheckBox.class);

        for (int i = 0; i < checkBoxs.size(); i++) {
            CheckBox check = checkBoxs.get(i);
            if (check.isShown()) {
                assertTrue("test03_WiFiMainUITestBack have issues",
                        check.isChecked() == checkBoxStatus[i]);
                sSolo.sleep(EmOperate.TIME_SHORT);
                sSolo.clickOnView(check);
            }
        }
    }

    public void test04_WiFiTx() {

        if (!EmOperate.enterItem(sSolo, "Tx", "WiFiTx6620"))
            return;

        sSolo.pressSpinnerItem(0, 1); // channel //
        sSolo.sleep(EmOperate.TIME_MID);
        sSolo.pressSpinnerItem(1, 1); // rate
        sSolo.sleep(EmOperate.TIME_MID);

        sSolo.clearEditText(0);
        sSolo.clearEditText(1);

        sSolo.enterText(0, "2048"); // pkt length
        sSolo.sleep(EmOperate.TIME_MID);
        sSolo.enterText(1, "5000"); // Rate
        sSolo.sleep(EmOperate.TIME_MID);

        sSolo.clickOnButton("Go"); // start test
        assertFalse(sSolo.getButton("Go").isEnabled());
        assertTrue(sSolo.getButton("Stop").isEnabled());

        sSolo.sleep(EmOperate.TIME_LONG);

        sSolo.clickOnButton("Stop"); // end test
        assertFalse(sSolo.getButton("Stop").isEnabled());
        assertTrue(sSolo.getButton("Go").isEnabled());

        sSolo.sleep(EmOperate.TIME_LONG);

        sSolo.goBack();
    }

    public void test05_WiFiRx() {

        if (!EmOperate.enterItem(sSolo, "Rx", "WiFiRx6620"))
            return;

        sSolo.pressSpinnerItem(0, 1); // channel //
        sSolo.sleep(EmOperate.TIME_MID);
        sSolo.pressSpinnerItem(1, 1); //
        sSolo.sleep(EmOperate.TIME_MID);

        sSolo.clickOnButton("Go"); // start test
        sSolo.sleep(EmOperate.TIME_SHORT);
        assertFalse(sSolo.getButton("Go").isEnabled());
        assertTrue(sSolo.getButton("Stop").isEnabled());

        sSolo.sleep(EmOperate.TIME_LONG);

        sSolo.clickOnButton("Stop"); // end test
        sSolo.sleep(EmOperate.TIME_SHORT);
        assertFalse(sSolo.getButton("Stop").isEnabled());
        assertTrue(sSolo.getButton("Go").isEnabled());

        sSolo.sleep(EmOperate.TIME_LONG);

        sSolo.goBack();
    }

    public void test06_McrTest() {

        if (!EmOperate.enterItem(sSolo, "MCR", "WiFiMcr"))
            return;

        sSolo.clearEditText(0);
        sSolo.enterText(0, "0");
        sSolo.sleep(EmOperate.TIME_MID);
        sSolo.clickOnButton(0); // read
        sSolo.sleep(EmOperate.TIME_MID);
        assertEquals(8, sSolo.getEditText(1).length());
        sSolo.clickOnButton(1); // write
        sSolo.sleep(EmOperate.TIME_MID);
        sSolo.goBack();
    }

    public void test07_NvramAccessTest() {

        sFinished = true;

        if (!EmOperate.enterItem(sSolo, "NVRAM", "WiFiEeprom"))
            return;

        sSolo.clearEditText(0);
        sSolo.enterText(0, "0");
        sSolo.sleep(EmOperate.TIME_SHORT);
        sSolo.clickOnButton(0); // read
        sSolo.sleep(EmOperate.TIME_SHORT);
        assertTrue(sSolo.getEditText(1).length() > 0);
        sSolo.clickOnButton(1); // write
        sSolo.sleep(EmOperate.TIME_SHORT);

        sSolo.clearEditText(2);
        sSolo.clearEditText(3);

        sSolo.enterText(2, "0");
        sSolo.enterText(3, "1");

        sSolo.sleep(EmOperate.TIME_SHORT);
        sSolo.clickOnButton(2); // read
        sSolo.sleep(EmOperate.TIME_SHORT);
        assertTrue(sSolo.getEditText(4).length() > 0);
        sSolo.clickOnButton(3); // write
        sSolo.sleep(EmOperate.TIME_SHORT);

        sSolo.goBack();

    }
}
