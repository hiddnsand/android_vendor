package com.mediatek.op01.phone.plugin;

import android.content.Context;
import android.util.Log;

import com.mediatek.phone.ext.ICallFeaturesSettingExt;
import com.mediatek.phone.ext.ICallForwardExt;
import com.mediatek.phone.ext.IIncomingCallExt;
import com.mediatek.phone.ext.IMobileNetworkSettingsExt;
import com.mediatek.phone.ext.INetworkSettingExt;
import com.mediatek.phone.ext.ISsRoamingServiceExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;

public class Op01PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {

    public Context mContext;
    public Op01PhoneCustomizationFactory (Context context){
        mContext = context;
    }
    @Override
    public ICallFeaturesSettingExt makeCallFeaturesSettingExt() {
        Log.i("Op01PhoneCustomizationFactory", "makeCallFeaturesSettingExt");
        return new Op01CallFeaturesSettingExt();
    }

    public ICallForwardExt makeCallForwardExt() {
        Log.i("Op01PhoneCustomizationFactory", "makeCallForwardExt");
        return new Op01CallForwardExt(mContext);
    }

    public ISsRoamingServiceExt makeSsRoamingServiceExt() {
        Log.i("Op01PhoneCustomizationFactory", "makeSsRoamingServiceExt");
        return new Op01SsRoamingServiceExt();
    }

    public IMobileNetworkSettingsExt makeMobileNetworkSettingsExt() {
        Log.i("Op01PhoneCustomizationFactory", "makeMobileNetworkSettingsExt");
        return new Op01MobileNetworkSettingsExt(mContext);
    }

    public INetworkSettingExt makeNetworkSettingExt() {
        Log.i("Op01PhoneCustomizationFactory", "makeNetworkSettingExt");
        return new OP01NetworkSettingExt(mContext);
    }

    public IIncomingCallExt makeIncomingCallExt() {
        Log.i("Op01PhoneCustomizationFactory", "makeIncomingCallExt");
        return new Op01IncomingCallExt(mContext);
    }
}
