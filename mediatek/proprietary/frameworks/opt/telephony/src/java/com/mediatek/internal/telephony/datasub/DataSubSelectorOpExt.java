package com.mediatek.internal.telephony.datasub;

import android.content.Context;
import android.content.Intent;

import android.os.Build;
import android.os.SystemProperties;

import android.telephony.Rlog;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.SubscriptionController;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;

import com.mediatek.internal.telephony.datasub.DataSubConstants;

import java.util.ArrayList;
import java.util.Arrays;

public class DataSubSelectorOpExt implements IDataSubSelectorOPExt {
    private static final boolean USER_BUILD = TextUtils.equals(Build.TYPE, "user");
    private static boolean DBG = true;

    private static String LOG_TAG = "DSSExt";

    private static Context mContext = null;

    private static DataSubSelectorOpExt mInstance = null;

    private static DataSubSelector mDataSubSelector = null;

    private static ISimSwitchForDSSExt mSimSwitchForDSS = null;

    private static CapabilitySwitch mCapabilitySwitch = null;

    private Intent mIntent = null;


    public DataSubSelectorOpExt(Context context) {
        mContext = context;
    }

    public void init(DataSubSelector dataSubSelector, ISimSwitchForDSSExt simSwitchForDSS) {
        mDataSubSelector = dataSubSelector;
        mCapabilitySwitch = CapabilitySwitch.getInstance(mContext, dataSubSelector);
        mSimSwitchForDSS = simSwitchForDSS;
    }

    @Override
    public void handleSimStateChanged(Intent intent) {
        String simStatus = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
        int slotId = intent.getIntExtra(PhoneConstants.SLOT_KEY, PhoneConstants.SIM_ID_1);
        if (simStatus.equals(IccCardConstants.INTENT_VALUE_ICC_IMSI)) {
            mCapabilitySwitch.handleSimImsiStatus(intent);

            handleNeedWaitImsi(intent);
            handleNeedWaitUnlock(intent);
        } else if (simStatus.equals(IccCardConstants.INTENT_VALUE_ICC_ABSENT)) {
            mCapabilitySwitch.handleSimImsiStatus(intent);
        } else if (simStatus.equals(IccCardConstants.INTENT_VALUE_ICC_NOT_READY)) {
            mCapabilitySwitch.handleSimImsiStatus(intent);
        }
    }

    @Override
    public void handleSubinfoRecordUpdated(Intent intent) {
        int detectedType = intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                    MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE);
        if (DataSubSelectorUtil.isC2kProject()) {
            if (mCapabilitySwitch.isSimUnLocked()){
                subSelector(intent);
            }
        }
        subSelector(intent);
    }

    private void handleNeedWaitImsi(Intent intent) {
        if (CapabilitySwitch.isNeedWaitImsi()) {
            CapabilitySwitch.setNeedWaitImsi(Boolean.toString(false));
        }
        if (CapabilitySwitch.isNeedWaitImsiRoaming() == true) {
            CapabilitySwitch.setNeedWaitImsiRoaming(Boolean.toString(false));
        }
    }

    private void handleNeedWaitUnlock(Intent intent) {
        if (CapabilitySwitch.isNeedWaitUnlock()) {
            CapabilitySwitch.setNeedWaitUnlock("false");
            subSelector(intent);
        }
        if (CapabilitySwitch.isNeedWaitUnlockRoaming()) {
            CapabilitySwitch.setNeedWaitUnlockRoaming("false");
        }
    }

    private int getHighCapabilityPhoneIdBySimType() {
        int phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        int[] simOpInfo = new int[mDataSubSelector.getPhoneNum()];
        int[] simType = new int[mDataSubSelector.getPhoneNum()];
        int insertedState = 0;
        int insertedSimCount = 0;
        int tSimCount = 0;
        int wSimCount = 0;
        int cSimCount = 0;
        String[] currIccId = new String[mDataSubSelector.getPhoneNum()];

        if (RadioCapabilitySwitchUtil.isPS2SupportLTE() && mDataSubSelector.getPhoneNum() == 2) {
            // check sim cards number
            for (int i = 0; i < mDataSubSelector.getPhoneNum(); i++) {
                currIccId[i] = DataSubSelectorUtil.getIccidFromProp(i);
                if (currIccId[i] == null || "".equals(currIccId[i])) {
                    log("iccid not found, can not get high capability phone id");
                    return SubscriptionManager.INVALID_PHONE_INDEX;
                }
                if (!DataSubConstants.NO_SIM_VALUE.equals(currIccId[i])) {
                    ++insertedSimCount;
                    insertedState = insertedState | (1 << i);
                }
            }

            // no sim card
            if (insertedSimCount == 0) {
                log("no sim card, don't switch");
                return SubscriptionManager.INVALID_PHONE_INDEX;
            }

            // check sim info
            if (RadioCapabilitySwitchUtil.getSimInfo(simOpInfo, simType, insertedState) == false) {
                log("cannot get sim operator info, don't switch");
                return SubscriptionManager.INVALID_PHONE_INDEX;
            }

            for (int i = 0; i < mDataSubSelector.getPhoneNum(); i++) {
                if (RadioCapabilitySwitchUtil.SIM_OP_INFO_OP01 == simOpInfo[i]) {
                    tSimCount++;
                } else if (RadioCapabilitySwitchUtil.isCdmaCard(i)) {
                    cSimCount++;
                    simOpInfo[i] = RadioCapabilitySwitchUtil.SIM_OP_INFO_OP09;
                } else if (RadioCapabilitySwitchUtil.SIM_OP_INFO_UNKNOWN!= simOpInfo[i]){
                    wSimCount++;
                    simOpInfo[i] = RadioCapabilitySwitchUtil.SIM_OP_INFO_OP02;
                }
            }
            log("getHighCapabilityPhoneIdBySimType : Inserted SIM count: " + insertedSimCount
                + ", insertedStatus: " + insertedState + ", tSimCount: " + tSimCount
                + ", wSimCount: " + wSimCount + ", cSimCount: " + cSimCount
                + Arrays.toString(simOpInfo));

            // t + w --> if support real T+W, always on t card
            if (RadioCapabilitySwitchUtil.isTPlusWSupport()) {
                if (simOpInfo[0] == RadioCapabilitySwitchUtil.SIM_OP_INFO_OP01 &&
                    simOpInfo[1] == RadioCapabilitySwitchUtil.SIM_OP_INFO_OP02) {
                    phoneId = 0;
                } else if (simOpInfo[0] == RadioCapabilitySwitchUtil.SIM_OP_INFO_OP02 &&
                    simOpInfo[1] == RadioCapabilitySwitchUtil.SIM_OP_INFO_OP01) {
                    phoneId = 1;
                }
            }

            // t + c / w + c--> always on c card
            if (RadioCapabilitySwitchUtil.isSupportSimSwitchEnhancementForCdmaCard()) {
                if (simOpInfo[0] == RadioCapabilitySwitchUtil.SIM_OP_INFO_OP01 &&
                    RadioCapabilitySwitchUtil.isCdmaCard(1)) {
                    phoneId = 1;
                } else if (RadioCapabilitySwitchUtil.isCdmaCard(0) &&
                    simOpInfo[1] == RadioCapabilitySwitchUtil.SIM_OP_INFO_OP01) {
                    phoneId = 0;
                } else if (RadioCapabilitySwitchUtil.isCdmaCard(0) &&
                    simOpInfo[1] == RadioCapabilitySwitchUtil.SIM_OP_INFO_OP02) {
                    phoneId = 0;
                } else if (simOpInfo[0] == RadioCapabilitySwitchUtil.SIM_OP_INFO_OP02 &&
                    RadioCapabilitySwitchUtil.isCdmaCard(1)) {
                    phoneId = 1;
                }
            }
        }
        log("getHighCapabilityPhoneIdBySimType : " + phoneId);
        return phoneId;
    }

    @Override
    public void subSelector(Intent intent) {
        // only handle 3/4G switching
        int phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        String[] currIccId = new String[mDataSubSelector.getPhoneNum()];

        phoneId = getHighCapabilityPhoneIdBySimType();
        if (phoneId == SubscriptionManager.INVALID_PHONE_INDEX) {
            //Get previous default data
            //Modify the way for get defaultIccid,
            //because the SystemProperties may not update on time
            String defaultIccid = "";
            int defDataSubId = SubscriptionController.getInstance().getDefaultDataSubId();
            int defDataPhoneId = SubscriptionManager.getPhoneId(defDataSubId);
            if (defDataPhoneId >= 0) {
                if (defDataPhoneId >= DataSubSelectorUtil.getIccidNum()) {
                   log("phoneId out of boundary :" + defDataPhoneId);
                } else {
                   defaultIccid = DataSubSelectorUtil.getIccidFromProp(defDataPhoneId);
                }
            }
            if (!USER_BUILD) {
                log("Default data Iccid = " + SubscriptionInfo.givePrintableIccid(defaultIccid));
            }
            if (("N/A".equals(defaultIccid)) || ("".equals(defaultIccid))) {
                return;
            }
            for (int i = 0; i < mDataSubSelector.getPhoneNum(); i++) {
                currIccId[i] = DataSubSelectorUtil.getIccidFromProp(i);
                if (currIccId[i] == null || "".equals(currIccId[i])) {
                    log("error: iccid not found, wait for next sub ready");
                    return;
                }
                if (defaultIccid.equals(currIccId[i])) {
                    phoneId = i;
                    break;
                }

                if (DataSubConstants.NO_SIM_VALUE.equals(currIccId[i])) {
                    log("clear mcc.mnc:" + i);
                    String propStr;
                    if (i == 0) {
                        propStr = "gsm.sim.ril.mcc.mnc";
                    } else {
                        propStr = "gsm.sim.ril.mcc.mnc." + (i + 1);
                    }
                    SystemProperties.set(propStr, "");
                }
            }
        }

        // check pin lock
        if (mCapabilitySwitch.isSimUnLocked() == false) {
            log("DataSubSelector for OM: do not switch because of sim locking");
            CapabilitySwitch.setNeedWaitUnlock("true");
            mIntent = intent;
            CapabilitySwitch.setSimStatus(intent);
            CapabilitySwitch.setNewSimSlot(intent);
            return;
        } else {
            log("DataSubSelector for OM: no pin lock");
            CapabilitySwitch.setNeedWaitUnlock("false");
        }

        log("Default data phoneid = " + phoneId);
        if (phoneId != SubscriptionManager.INVALID_PHONE_INDEX) {
            // always set capability to this phone
            mCapabilitySwitch.setCapabilityIfNeeded(phoneId);
        }

        // clean system property
        CapabilitySwitch.resetSimStatus();
        CapabilitySwitch.resetNewSimSlot();
    }

    @Override
    public void handleAirPlaneModeOff(Intent intent) {
        subSelector(intent);
    }

    public void handlePlmnChanged(Intent intent) {}

    public void handleDataEnable(int status) {}

    public void handleDefaultDataChanged(Intent intent) {}

    private void log(String txt) {
        if (DBG) {
            Rlog.d(LOG_TAG, txt);
        }
    }

    private void loge(String txt) {
        if (DBG) {
            Rlog.e(LOG_TAG, txt);
        }
    }
}
