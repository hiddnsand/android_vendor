/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.selfregister;

import android.app.AlarmManager.OnAlarmListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

import com.mediatek.selfregister.utils.AgentProxy;
import com.mediatek.selfregister.utils.PlatformManager;
import com.mediatek.selfregister.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Service which process the main logic of registration.
 */
public class Op09SelfRegister {

    public static final String TAG = Const.TAG_PREFIX + "RegisterService";

    private static final int MSG_BOOT_COMPLETED = 1;
    private static final int MSG_PRE_BOOT_COMPLETED = 2;
    private static final int MSG_SIM_STATE_CHANGED = 3;
    private static final int MSG_SIM_INSERVICE = 4;
    private static final int MSG_DEFAULT_DATA_SUBSCRIPTION_CHANGED = 5;
    private static final int MSG_WATCH_DOG = 6;
    private static final int MSG_RETRY = 8;
    private static final int MSG_RETRY_DATASUB = 9;

    private static final int TIMES_MAX_WAIT = 12;
    private static final int TIMES_MAX_RETRY = 10;

    private static final int ONE_MINUTE = 60;
    private static final int ONE_HOUR = 60 * 60;

    private static final int TYPE_COMPARE_ICCID = 1;
    private static final int TYPE_NO_COMPARE = 2;

    private int[] mSlotList;
    private String[] mIccIdList;
    private int[] mSubIdSnapShot;
    private PhoneStateListener[] mPhoneStateListener;

    private Context mContext;
    private PlatformManager mPlatformManager;

    private int mMasterSlot = -1;
    private int mLocationSlot = -1;
    private int mDataSub = -1;

    private int mWaitTimes = 0;
    private int mRetryTimes = 0;

    private int mRegisterType = -1;
    private boolean mHasInServiceAlarm = false;

    private ArrayList<Integer> mMessageList = new ArrayList<Integer>();

    private OnAlarmListener mWatchDogAlarmListener;
    private StartupReceiver mStartupReceiver;
    private VolteObserver[] mVolteObservers;

    private static Op09SelfRegister sInstance = null;
    public static void newSelfRegister(Context context) {
        if (sInstance == null) {
            sInstance = new Op09SelfRegister(context);
        }
    }

    public Op09SelfRegister(Context context) {
        mContext = context;
        if (PlatformManager.isFeatureSupported()) {
            init();
        }
    }

    private void init() {
        Log.i(TAG, "init");

        mPlatformManager = new PlatformManager(mContext);

        if (mPlatformManager.isSingleLoad()) {
            mSlotList = Const.SINGLE_SIM_SLOT;
        } else {
            mSlotList = Const.DUAL_SIM_SLOTS;
        }

        mPhoneStateListener = new PhoneStateListener[mSlotList.length];
        mIccIdList = new String[mSlotList.length];
        mSubIdSnapShot = new int[mSlotList.length];
        mVolteObservers = new VolteObserver[Const.VOLTE_PATH.length];

        for (int i = 0; i < mSlotList.length; ++i) {
            mPhoneStateListener[i] = null;
            mIccIdList = null;
            mSubIdSnapShot[i] = -1;
            mVolteObservers[i] = null;
        }
        registerStartupReceiver();
    }

    private void resetParameters() {
        Log.i(TAG, "resetParameters");

        // not reset mDataSub & mRetryTimes
        mWaitTimes = 0;
        mHasInServiceAlarm = false;
        unRegisterPhoneListeners();
        mMessageList.clear();
    }

    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
            case MSG_PRE_BOOT_COMPLETED:
                // system update or factory reset, reset register flag
                AgentProxy.getInstance().resetRegisterFlag();
                break;

            case MSG_BOOT_COMPLETED:
                doAfterBoot();
                break;

            case MSG_SIM_STATE_CHANGED:
            case MSG_DEFAULT_DATA_SUBSCRIPTION_CHANGED:
                addMessageToList(msg.what);
                registerPhoneListeners();
                break;

            case MSG_SIM_INSERVICE:
                processActionList();
                doAfterSimInservice();
                break;

            case MSG_RETRY:
            case MSG_RETRY_DATASUB:
                addMessageToList(msg.what);
                doAfterRetry();
                break;

            case MSG_WATCH_DOG:
                doForWatchDog();
                break;

            default:
                break;
            }
        }
    };

    private void doAfterBoot() {

        // boot complete may come later than sim IN_SERVICE
        if (!mHasInServiceAlarm) {
            setWatchDogAlarm();

            if (!mMessageList.contains(MSG_SIM_STATE_CHANGED)) {
                startServiceIfSimLoaded();
            }
        }
    }

    private void startServiceIfSimLoaded() {
        Intent intent = mContext.registerReceiver(null,
                new IntentFilter(Const.ACTION_SIM_STATE_CHANGED));

        // If SIM_STATE_CHANGED all received when device locked, not register
        // PhoneStateListener and register will fail. Start service here if has previous
        // SIM_STATE_CHANGED (absent/loaded) intent.
        if (intent != null) {
            String state = intent.getStringExtra(Const.EXTRA_ICC_STATE);
            int slot = intent.getIntExtra(Const.EXTRA_ICC_SLOT, Const.SLOT_ID_INVALID);

            if (state.equals(Const.VALUE_ICC_LOADED) || state.equals(Const.VALUE_ICC_ABSENT)) {
                Log.i(TAG, "Slot " + slot + ", state is " + state + ", startService");
                mHandler.sendEmptyMessage(MSG_SIM_STATE_CHANGED);
            }
        }
    }

    private void doAfterRetry() {
        Log.i(TAG, "doAfterRetry");
        setInServiceDelayAlarm(ONE_MINUTE);
    }

    private void addMessageToList(int type) {
        mMessageList.add((Integer) type);
    }

    private void processActionList() {
        if (mWaitTimes > 0) {
            return ;
        }

        // Remove header and update register type
        if (mMessageList.size() > 0) {
            mRegisterType = getActionType(mMessageList.get(0));
            mMessageList.remove(0);
        }
        dumpActionList("After process");
    }

    private int getActionType(int type) {
        if (type == MSG_SIM_STATE_CHANGED || type == MSG_RETRY) {
            return TYPE_COMPARE_ICCID;
        } else {
            return TYPE_NO_COMPARE;
        }
    }

    private void dumpActionList(String prefix) {
        Log.i(TAG, prefix + ", list size " + mMessageList.size());
        for (int message: mMessageList) {
            String action = "";
            switch (message) {
            case MSG_SIM_STATE_CHANGED:
                action = "SIM_STATE_CHANGED";
                break;

            case MSG_DEFAULT_DATA_SUBSCRIPTION_CHANGED:
                action = "DEFAULT_DATA_SUBSCRIPTION_CHANGED";
                break;

            case MSG_RETRY:
                action = "RETRY";
                break;

            case MSG_RETRY_DATASUB:
                action = "RETRY_DATA_SUB";
                break;

            default:
                break;
            }
            Log.i(TAG, "Action is " + action);
        }
    }

    private void doAfterSimInservice() {
        Log.i(TAG, "doAfterSimInservice, type is " + mRegisterType + ", wait time " + mWaitTimes);
        if (!mPlatformManager.areSlotsInfoReady(mSlotList) && mWaitTimes < TIMES_MAX_WAIT) {
            mWaitTimes++;
            setInServiceDelayAlarm(30);
            return;
        }

        initIdentityInfo();

        if (!needRegister()) {
            checkNextAction();
            return;
        }

        if ((getCdmaBaseId() > 0) && isNetworkTypeValid()) {

            final String message = new RegisterMessage(this, mMasterSlot).prepareContent();
            new Thread(new Runnable() {

                @Override
                public void run() {
                    JSONObject response = Utils.httpSend(message);
                    handleResponse(response);
                }

            }).start();
        } else {
            if (mWaitTimes < TIMES_MAX_WAIT) {
                mWaitTimes++;
                int second = (mWaitTimes % 3 + 1) * 10;
                setInServiceDelayAlarm(second);

            } else {
                setRetryAlarm();

                if (mRegisterType == TYPE_COMPARE_ICCID) {
                    registerVolteObserver();
                }
                checkNextAction();
            }
        }
    }

    private void doForWatchDog() {
        setRetryAlarm();
        resetParameters();
    }

    /*
     * If not register to network in 10 minutes, set a retry alarm.
     */
    private void setWatchDogAlarm() {
        Log.i(TAG, "setWatchDogAlarm for 10 minutes");
        mWatchDogAlarmListener = new CustomizedAlarmListener(MSG_WATCH_DOG);
        PlatformManager.setElapsedAlarm(mContext, mWatchDogAlarmListener, 10 * ONE_MINUTE);
    }

    private void cancelWatchDogAlarm() {
        if (mWatchDogAlarmListener != null) {
            Log.i(TAG, "cancelWatchDogAlarm");
            PlatformManager.cancelAlarm(mContext, mWatchDogAlarmListener);
            mWatchDogAlarmListener = null;
        }
    }

    /*
     * Trigger when SIM IN_SERVICE, unregister listen to avoid further state
     * change
     */
    private void setInServiceDelayAlarm() {
        setInServiceDelayAlarm(ONE_MINUTE);
        cancelWatchDogAlarm();
    }

    private void setInServiceDelayAlarm(int second) {
        Log.i(TAG, "setInServiceDelayAlarm " + second + "s");
        OnAlarmListener listener = new CustomizedAlarmListener(MSG_SIM_INSERVICE);
        PlatformManager.setElapsedAlarm(mContext, listener, second);
        mHasInServiceAlarm = true;
    }

    private void setRetryAlarm() {
        int retryType = MSG_RETRY;
        if (mRegisterType != TYPE_COMPARE_ICCID) {
            retryType = MSG_RETRY_DATASUB;
        }

        if (mRetryTimes < TIMES_MAX_RETRY) {
            mRetryTimes++;
            Log.i(TAG, "Retry after one hour, times " + mRetryTimes);

            OnAlarmListener listener = new CustomizedAlarmListener(retryType);
            PlatformManager.setRtcAlarm(mContext, listener, ONE_HOUR);
        } else {
            Log.i(TAG, "Already retried " + mRetryTimes + " times");
        }
    }

    private void handleResponse(JSONObject response) {
        boolean result = Utils.checkRegisterResult(response);

        if (result) {
            Log.i(TAG, "analyseResponse(), resultCode:0 resultDesc:Success");

            if (mRegisterType == TYPE_COMPARE_ICCID) {
                AgentProxy.getInstance().setSavedIccId(mIccIdList);
                AgentProxy.getInstance().setRegisterFlag(true);
                unRegisterVolteObserver();
            }

            resetParameters();
        } else {
            Log.e(TAG, "Register fail!");

            if (mRegisterType == TYPE_COMPARE_ICCID) {
                registerVolteObserver();
            }
            setRetryAlarm();
            checkNextAction();
        }
    }

    /*
     * If not register in last round (no need or condition not OK), check the action list
     * to find a different type and trigger register flow with new type. If All action have same
     * register type to last round, just ignore and stop.
     *
     * Example 1:
     *   Last action is SIM_STATE_CHANGED (TYPE_COMPARE_ICCID), and no need to register.
     *   Action list is {
     *      1. SIM_STATE_CHANGED,
     *      2. SIM_STATE_CHANGED,
     *      3. DEFAULT_DATA_SUBSCRIPTION_CHANGED,
     *      4. SIM_STATE_CHANGED,
     *      5. DEFAULT_DATA_SUBSCRIPTION_CHANGED,}
     *   Find action 3 has different type (TYPE_NO_COMPARE), and trigger a new register flow.
     *
     * Example 2:
     *   Last action is SIM_STATE_CHANGED (TYPE_COMPARE_ICCID), and no need to register.
     *   Action list is {
     *      1. SIM_STATE_CHANGED,
     *      2. SIM_STATE_CHANGED,
     *      3. RETRY}
     *   All action is list have same type to last round, just quit.
     */
    private void checkNextAction() {
        dumpActionList("Last register type: " + mRegisterType);

        for (int item: mMessageList) {
            int nextType = getActionType(item);
            if (mRegisterType != nextType) {

                mRegisterType = nextType;
                Log.i(TAG, "Find different: " + mRegisterType + ", break");
                mWaitTimes = 0;
                mMessageList.clear();
                doAfterSimInservice();
                return ;
            }
        }
        mMessageList.clear();
        resetParameters();
    }

    private boolean needRegister() {

        if (mRegisterType == TYPE_COMPARE_ICCID) {
            // 1. A lab single load, no need if no CT
            if (!mPlatformManager.supportCTForAllSlots()) {
                if (mPlatformManager.isSingleLoad()
                        && !mPlatformManager.isValidUim(Const.SLOT_ID_0)) {
                    Log.i(TAG, "[needRegister] lab A's single load - no CT, no need");
                    return false;
                }
            }
        } else {
            // master/slave switch only occur when there're two cards
            if (mPlatformManager.isSingleLoad()) {
                Log.i(TAG, "[needRegister] single load can't master/slave switch");
                return false;
            }

            for (int slotId: mSlotList) {
                if (!mPlatformManager.hasIccCard(slotId)) {
                    Log.i(TAG, "[needRegister] slot " + slotId + " no sim");
                    return false;
                }
            }
        }

        // 2. No need if roaming
        for (int i = 0; i < mSlotList.length; ++i) {
            if (mPlatformManager.isNetworkRoaming(mSlotList[i])) {
                Log.i(TAG, "[needRegister] Sim " + i + " roaming, no need");
                return false;
            }
        }

        if (mRegisterType == TYPE_COMPARE_ICCID) {
            // 3. If already registered and iccIds not change, no need
            if (AgentProxy.getInstance().isRegistered()) {

                if (isIccIdSame()) {
                    Log.i(TAG, "[needRegister] same iccid, no need");
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isIccIdSame() {
        String[] iccidSaved = AgentProxy.getInstance().getSavedIccId(mSlotList.length);

        for (int i = 0; i < iccidSaved.length; ++i) {
            iccidSaved[i] = mPlatformManager.getProcessedIccid(iccidSaved[i]);
        }

        return Utils.compareUnsortArray(mIccIdList, iccidSaved);
    }

    private void initIdentityInfo() {
        mMasterSlot = getMasterSlot();

        if (mPlatformManager.supportCTForAllSlots()) {
            mLocationSlot = mMasterSlot;

            if (!mPlatformManager.isValidUim(mMasterSlot)) {
                for (int slotId: mSlotList) {
                    if (slotId != mMasterSlot && mPlatformManager.isValidUim(slotId)) {
                        mLocationSlot = slotId;
                        break;
                    }
                }
            }

        } else {

            // Location slot: first slot not empty
            for (int slotId: mSlotList) {
                if (mPlatformManager.hasIccCard(slotId)) {
                    mLocationSlot = slotId;
                    break;
                }
            }
        }
        Log.i(TAG, "master/location slot are " + mMasterSlot + "/" + mLocationSlot);

        mIccIdList = new String[mSlotList.length];
        for (int i = 0; i < mIccIdList.length; ++i) {
            mIccIdList[i] = mPlatformManager.getIccId(mSlotList[i]);
        }
    }

    private int getMasterSlot() {

        // CT could in both slot 0 and 1
        if (mPlatformManager.supportCTForAllSlots()) {
            // 1. try mobile data slot
            int slotId = PlatformManager.getDefaultSim();
            if (slotId != Const.SLOT_ID_INVALID && mPlatformManager.hasIccCard(slotId)) {
                return slotId;
            }

            // 2. If not valid, try 3/4G capability slot
            slotId = mPlatformManager.getSimSwitchSlot();
            if (slotId != Const.SLOT_ID_INVALID && mPlatformManager.hasIccCard(slotId)) {
                return slotId;
            }

            // 3. Use first slot not empty
            for (int i : mSlotList) {
                if (mPlatformManager.hasIccCard(i)) {
                    slotId = i;
                    break;
                }
            }
            return slotId;
        } else {
            // CT could only in slot 0
            return Const.SLOT_ID_0;
        }
    }

    /*
     * Whether network is value: should be Wi-Fi on data link on a CT card
     */
    private boolean isNetworkTypeValid() {
        if (hasWiFiConnection()) {
            Log.i(TAG, "[isNetworkTypeValid] find Wi-Fi, network valid");
            return true;
        }

        if (hasMobileLinkConnection()) {
            int slotId = PlatformManager.getDefaultSim();

            if (mPlatformManager.isValidUim(slotId)) {
                return true;
            }
        }
        Log.i(TAG, "[isNetworkTypeValid] no Wi-Fi or invalid data link");
        return false;
    }

    private boolean hasWiFiConnection() {
        return mPlatformManager.hasNetworkConnection(ConnectivityManager.TYPE_WIFI);

    }

    private boolean hasMobileLinkConnection() {
        return mPlatformManager.hasNetworkConnection(ConnectivityManager.TYPE_MOBILE);
    }

    // ------------------------------------------------------
    // Wrapper of PlatformManager
    // ------------------------------------------------------

    public PlatformManager getPlatformManager() {
        return mPlatformManager;
    }

    public String getIccIdFromCard(int slotId) {
        return mIccIdList[slotId];
    }

    public String[] getComplexImsi(int slotId) {
        String imsiArray[] = mPlatformManager.getComplexImsi(slotId);
        return imsiArray;
    }

    // M: Get card type of 1 for ICC type, 2 for UICC type.
    public int getSimType(int slotId) {
        int value = mPlatformManager.getSimType(slotId);
        return value;
    }

    public String getMeid() {
        String meid = mPlatformManager.getDeviceMeid(mSlotList);
        Log.i(TAG, "meid is " + PlatformManager.encryptMessage(meid));
        return meid;
    }

    public String getImei(int slotId) {
        String imei = mPlatformManager.getImei(slotId);
        Log.i(TAG, "imei " + slotId + " is " + PlatformManager.encryptMessage(imei));
        return imei;
    }

    // ------------------------------------------------------
    // Location info
    // ------------------------------------------------------

    private CellLocation getLocationInfo() {
        return mPlatformManager.getCellLocation(mLocationSlot);
    }

    public int getCdmaBaseId() {
        int baseId = Const.VALUE_INVALID_INT;

        CellLocation location = getLocationInfo();
        if (location instanceof CdmaCellLocation) {
            baseId = ((CdmaCellLocation) location).getBaseStationId();
        } else if (location instanceof GsmCellLocation) {
            baseId = ((GsmCellLocation) location).getCid();
        }
        Log.i(TAG, "[getCdmaBaseId] " + PlatformManager.encryptMessage(baseId));
        return baseId;
    }

    public int getCdmaNetworkId() {
        int networkId = Const.VALUE_INVALID_INT;

        CellLocation location = getLocationInfo();
        if (location instanceof CdmaCellLocation) {
            networkId = ((CdmaCellLocation) location).getNetworkId();
        }

        Log.i(TAG, "[getCdmaNetworkId] " + PlatformManager.encryptMessage(networkId));
        return networkId;
    }

    public int getCdmaSystemId() {
        int systemId = Const.VALUE_INVALID_INT;

        CellLocation location = getLocationInfo();
        if (location instanceof CdmaCellLocation) {
            systemId = ((CdmaCellLocation) location).getSystemId();
        }
        Log.i(TAG, "[getCdmaSystemId] " + PlatformManager.encryptMessage(systemId));
        return systemId;
    }

    private void registerVolteObserver() {
        for (int i = 0; i < Const.VOLTE_PATH.length; ++i) {
            if (mVolteObservers[i] == null) {
                Log.i(TAG, "registerVolteObserver " + i);
                mVolteObservers[i] = new VolteObserver(mHandler);
                Uri uri = Settings.Global.getUriFor(Const.VOLTE_PATH[i]);
                mContext.getContentResolver()
                        .registerContentObserver(uri, true, mVolteObservers[i]);
                PlatformManager.showVolteState(mContext);
            } else {
                Log.i(TAG, "Already register for " + i);
            }
        }
    }

    private void unRegisterVolteObserver() {
        Log.i(TAG, "unRegisterVolteObserver");
        for (int i = 0; i < Const.VOLTE_PATH.length; ++i) {
            if (mVolteObservers[i] != null) {
                mContext.getContentResolver().unregisterContentObserver(mVolteObservers[i]);
                mVolteObservers[i] = null;
            }
        }
    }

    private void registerStartupReceiver() {
        if (mStartupReceiver == null) {
            mStartupReceiver = new StartupReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(Const.ACTION_PRE_BOOT_COMPLETED);
            filter.addAction(Const.ACTION_BOOT_COMPLETED);
            filter.addAction(Const.ACTION_SIM_STATE_CHANGED);
            filter.addAction(Const.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED);
            mContext.registerReceiver(mStartupReceiver, filter);
        }
    }

    private void registerPhoneListeners() {

        for (int i = 0; i < mSlotList.length; ++i) {

            int[] subId = PlatformManager.getSubId(mSlotList[i]);

            if (PlatformManager.isSubIdsValid(subId)) {

                if (subId[0] == mSubIdSnapShot[i]) {
                    Log.i(TAG, "[registerPhoneListeners] Slot " + i + " already registered.");
                } else {
                    Log.i(TAG, "[registerPhoneListeners] Slot " + i + " subId changed, refresh");
                    mPlatformManager.unRegisterPhoneListener(mPhoneStateListener[i]);

                    mSubIdSnapShot[i] = subId[0];
                    mPhoneStateListener[i] = new CustomizedPhoneStateListener(i, subId[0]);
                    mPlatformManager.registerPhoneListener(mPhoneStateListener[i]);
                }

            } else {
                if (mPhoneStateListener[i] != null) {
                    Log.i(TAG, "[registerPhoneListeners] Slot " + i + " unplug, unregister");
                    mPlatformManager.unRegisterPhoneListener(mPhoneStateListener[i]);
                    mSubIdSnapShot[i] = -1;
                    mPhoneStateListener[i] = null;
                } else {
                    Log.i(TAG, "[registerPhoneListeners] No need for slot " + i);
                }

            }
        }
    }

    private void unRegisterPhoneListeners() {
        Log.i(TAG, "unRegisterPhoneListeners");

        for (int i = 0; i < mPhoneStateListener.length; ++i) {
            if (mPhoneStateListener[i] != null) {
                mPlatformManager.unRegisterPhoneListener(mPhoneStateListener[i]);
                mSubIdSnapShot[i] = -1;
                mPhoneStateListener[i] = null;
            }
        }
    }

    private class CustomizedAlarmListener implements OnAlarmListener {

        private int mMsgType;

        public CustomizedAlarmListener(int type) {
            mMsgType = type;
        }

        @Override
        public void onAlarm() {
            mHandler.sendEmptyMessage(mMsgType);
        }
    }

    private class VolteObserver extends ContentObserver {

        public VolteObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            Log.i(TAG, "selfChange " + selfChange);
            onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            Log.i(TAG, "selfChange " + selfChange + ", uri " + uri);
            PlatformManager.showVolteState(mContext);
            mHandler.sendEmptyMessage(MSG_SIM_STATE_CHANGED);
        }
    }

    private class CustomizedPhoneStateListener extends PhoneStateListener {

        private int mSlotId;

        public CustomizedPhoneStateListener(int slotId, int subId) {
            super(subId);
            mSlotId = slotId;
        }

        @Override
        public void onServiceStateChanged(ServiceState serviceState) {
            super.onServiceStateChanged(serviceState);

            if (serviceState.getState() != ServiceState.STATE_IN_SERVICE) {
                Log.i(TAG, "[onService " + mSlotId + "] not in service " + serviceState.getState());

            } else {

                if (!mHasInServiceAlarm) {
                    setInServiceDelayAlarm();
                }
            }
        }
    }

    private class StartupReceiver extends BroadcastReceiver {

        private static final String TAG = Const.TAG_PREFIX + "StartupReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isSystem = android.os.Process.myUserHandle().isSystem();
            if (!isSystem) {
                Log.d(TAG, "This is not system user, return.");
                return;
            }

            if(!isSwitchOpen(context)) {
                Log.i(TAG, "Feature is not enabled, do nothing");
                return;
            }

            String action = intent.getAction();

            if (action.equalsIgnoreCase(Const.ACTION_SIM_STATE_CHANGED)) {
                String state = intent.getStringExtra(Const.EXTRA_ICC_STATE);
                int slot = intent.getIntExtra(Const.EXTRA_ICC_SLOT, Const.SLOT_ID_INVALID);

                if (state.equals(Const.VALUE_ICC_LOADED) || state.equals(Const.VALUE_ICC_ABSENT)) {
                    Log.i(TAG, "Slot " + slot + ", state is " + state + ", go to service");
                    mHandler.sendEmptyMessage(MSG_SIM_STATE_CHANGED);

                } else {
                    Log.i(TAG, "Slot " + slot + ", state is " + state + ", ignore");
                }

            } else if (action.equals(Const.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED)) {
                int subId = intent.getIntExtra(Const.SUBSCRIPTION_KEY, -1);

                Log.i(TAG, "Old/now data sub are " + mDataSub + "/" + subId);

                if (subId >= 0 && subId != mDataSub) {
                    if (mDataSub >= 0 ) {
                        mHandler.sendEmptyMessage(MSG_DEFAULT_DATA_SUBSCRIPTION_CHANGED);
                    }else {
                        Log.i(TAG, "Old sub invalid, do nothing");
                    }
                    mDataSub = subId;
                }

            } else if (action.equals(Const.ACTION_BOOT_COMPLETED)) {
                Log.i(TAG, "onReceive " + action);
                mHandler.sendEmptyMessage(MSG_BOOT_COMPLETED);

            } else if (action.equals(Const.ACTION_PRE_BOOT_COMPLETED)) {
                Log.i(TAG, "onReceive " + action);
                mHandler.sendEmptyMessage(MSG_PRE_BOOT_COMPLETED);
            }
        }

        private boolean isSwitchOpen(Context context) {
            if (AgentProxy.getInstance() == null) {
                Log.e(TAG, "DmAgent is null, need to check this error");
                return false;
            }
            return AgentProxy.isFeatureEnabled(context.getContentResolver());
        }
    }
}
