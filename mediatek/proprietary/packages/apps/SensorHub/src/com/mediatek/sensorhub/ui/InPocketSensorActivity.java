package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class InPocketSensorActivity extends BaseTriggerSensorActivity {

    public InPocketSensorActivity() {
        super(Utils.KEY_INPOCKET_STATUS, Utils.KEY_INPOCKET_NOTIFY_STATUS,
                Utils.KEY_INPOCKET_AUTO_STATUS, Sensor.TYPE_IN_POCKET);
    }
}
