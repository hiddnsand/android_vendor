package com.mediatek.phone.op09.plugin;

import android.content.Context;

import com.mediatek.phone.ext.ICallFeaturesSettingExt;
import com.mediatek.phone.ext.IEmergencyDialerExt;
import com.mediatek.phone.ext.IMobileNetworkSettingsExt;
import com.mediatek.phone.ext.IPhoneMiscExt;
import com.mediatek.phone.ext.ITelephonyConnectionServiceExt;

import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;


public class Op09PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {

    public Context mContext;
    public Op09PhoneCustomizationFactory (Context context){
        mContext = context;
    }
    @Override
    public IMobileNetworkSettingsExt makeMobileNetworkSettingsExt() {
        return new OP09MobileNetworkSettingsExt(mContext);
    }

    @Override
    public IEmergencyDialerExt makeEmergencyDialerExt() {
        return new OP09EmergencyDialerExt(mContext);
    }

    @Override
    public IPhoneMiscExt makePhoneMiscExt() {
        return new OP09PhoneMiscExt(mContext);
    }

    @Override
    public ICallFeaturesSettingExt makeCallFeaturesSettingExt() {
        return new OP09CallSettingsExt(mContext);
    }

    @Override
    public ITelephonyConnectionServiceExt makeTelephonyConnectionServiceExt() {
        return new OP09TelephonyConnectionServiceExt();
    }
}
