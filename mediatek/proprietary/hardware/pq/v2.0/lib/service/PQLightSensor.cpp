#define LOG_TAG "PQLightSensor"

#define MTK_LOG_ENABLE 1
#include <cutils/log.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <time.h>
#include <linux/sensors_io.h>
#include "PQLightSensor.h"

#define MILLI_TO_NANO(v) (static_cast<nsecs_t>(v) * static_cast<nsecs_t>(1000L * 1000L))

PQLightSensor::PQLightSensor() :
    mIsEnabled(false),
    mIsSensorInited(false),
    mLastObservedLuxTime(0),
    mShortTermAverageLux(-1),
    mLongTermAverageLux(-1),
    mDebounceLuxTime(0),
    mDebouncedLux(-1),
    mNotifiedLux(-1),
    mContRunning(false),
    mListenerCallback(NULL),
    mListenerUser(NULL),
    mCustPQSensorInitFun(NULL),
    mCustPQSensorDeInitFun(NULL),
    mCustPQSensorLoopFun(NULL),
    mCustPQSensorEnableFun(NULL),
    mPQSensorCallback(NULL)

{
    mLib = dlopen("librgbwlightsensor.so", RTLD_GLOBAL);

    if (isRGBWLibGood()) {
        mCustPQSensorInitFun = getFunction<PQSensorInit>("pqLightSensorInit");
        mCustPQSensorDeInitFun = getFunction<PQSensorDeInit>("pqLightSensorDeInit");
        mCustPQSensorLoopFun = getFunction<PQSensorLoop>("pqLightSensorLoop");
        mCustPQSensorEnableFun = getFunction<PQSensorEnable>("pqLightSensorEnable");
        mPQSensorCallback = getFunction<PQSensorCallback>("pqSetListener");

        mPQSensorCallback(sensorListener,this); // set "sensorListener" as sensor listener callback

        mContRunning = true;
    }
}


PQLightSensor::~PQLightSensor()
{
    mContRunning = false;
    setEnabled(false);

    if (mLib != NULL) {
        if (mIsSensorInited == true) {
            mCustPQSensorDeInitFun();
        }
        dlclose(mLib);
    }

    join();
}


millisec_t PQLightSensor::getTime()
{
    struct timeval time;
    gettimeofday(&time, NULL);

    return static_cast<millisec_t>(time.tv_sec) * 1000L +
        static_cast<millisec_t>(time.tv_usec / 1000L);
}


void PQLightSensor::setEnabled(bool enabled)
{
    Mutex::Autolock _l(mLock);
    int enableVal = (enabled ? 1 : 0);

    if (mContRunning != true) {
        ALOGD("setEnabled fail: rgbw lib status=(%d,%d)", isRGBWLibGood(), mContRunning);
        return;
    }

    ALOGD("setEnabled %d-->%d", mIsEnabled, enabled);
    if (mIsEnabled != enabled) {
        mIsEnabled = enabled;

        if (getTid() == -1 && enableVal == 1)
            run("PQLightSensor");
        else {
            if (mIsSensorInited == true)
                mCustPQSensorEnableFun(enableVal);
        }
    }
}


void PQLightSensor::updateLuxValue(int alsR, int alsG, int alsB, int alsW)
{
    if (mListenerCallback != NULL)
        mListenerCallback(mListenerUser, alsR, alsG, alsB, alsW);
}


void PQLightSensor::sensorListener(void* obj, int alsR, int alsG, int alsB, int alsW)
{
    PQLightSensor *service = static_cast<PQLightSensor*>(obj);
    service->updateLuxValue(alsR, alsG, alsB, alsW);
}


bool PQLightSensor::threadLoop()
{
    int ret = 0;
    int rgbwSensorInited = 0;

    if (mContRunning) {
        rgbwSensorInited = mCustPQSensorInitFun();
        if(rgbwSensorInited == 1) {
            mIsSensorInited = true;
            mCustPQSensorLoopFun();
        } else {
            mContRunning = false;
        }
    }

    return mContRunning;
}

