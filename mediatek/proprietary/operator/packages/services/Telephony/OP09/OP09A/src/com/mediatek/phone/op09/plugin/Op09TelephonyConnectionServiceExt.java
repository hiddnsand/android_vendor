package com.mediatek.phone.op09.plugin;

import android.util.Log;
import java.util.ArrayList;

import com.mediatek.telephony.MtkTelephonyManagerEx;
import com.mediatek.phone.ext.DefaultTelephonyConnectionServiceExt;

/**
 * CT OP09 Telephony connection service feature.
 */
public class Op09TelephonyConnectionServiceExt extends DefaultTelephonyConnectionServiceExt {
    private static final String TAG = "Op09TelephonyConnectionServiceExt";
    /**
     * Customize strings which contains 'SIM', replace 'SIM' by 'UIM' etc.
     * @param stringList string list
     * @param slotId slot id
     * @return new string list
     */
    public ArrayList<String> customizeSimDisplayString(ArrayList<String> stringList, int slotId) {
        log("customizeSimDisplayString slotId:" + slotId);
        if (isCdmaCard(slotId)) {
            ArrayList<String> cellStringModified = new ArrayList<String>();
            for(String simString : stringList) {
                if (simString.contains("SIM")) {
                    simString = simString.replaceAll("SIM", "UIM");
                }
                log("customizeSimDisplayString simString:" + simString);
                cellStringModified.add(simString);
            }
            stringList = cellStringModified;
        }

        return stringList;
    }

    private boolean isSupportCdma(int slotId) {
        boolean isSupportCdma = false;
        String[] type = MtkTelephonyManagerEx.getDefault().getSupportCardType(slotId);
        if (type != null) {
            for (int i = 0; i < type.length; i++) {
                if ("RUIM".equals(type[i]) || "CSIM".equals(type[i])) {
                    isSupportCdma = true;
                }
            }
        }
        log("slotId = " + slotId + " isSupportCdma = " + isSupportCdma);
        return isSupportCdma;
    }

    private boolean isCdmaCard(int slotId) {
        boolean isCdmaCard = false;
        if (isSupportCdma(slotId) || MtkTelephonyManagerEx.getDefault().isCt3gDualMode(slotId)) {
            isCdmaCard = true;
        }
        log("slotId = " + slotId + " isCdmaCard = " + isCdmaCard);
        return isCdmaCard;
    }


    /**
     * simple log info.
     *
     * @param msg need print out string.
     * @return void.
     */
    private static void log(String msg) {
        Log.d(TAG, msg);
    }
}
