/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**
 * @file NodeBufferSizeMgr_BM.cpp
 * @brief Buffer size provider for stereo features on Bayer+Mono platform
*/

// Standard C header file

// Android system/core header file

// mtkcam custom header file

// mtkcam global header file
#include <mtkcam/feature/stereo/hal/stereo_size_provider.h>
// Module header file
// Local header file
#include "NodeBufferSizeMgr_BM.h"
#include "../../DepthMapPipe_Common.h"
// Logging header file
#define PIPE_CLASS_TAG "NodeBufferSizeMgr"
#include <featurePipe/core/include/PipeLog.h>

/*******************************************************************************
* Namespace start.
********************************************************************************/
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe_DepthMap {

using namespace NSCam::NSIoPipe;
using namespace StereoHAL;
/*******************************************************************************
* External Function
********************************************************************************/

/*******************************************************************************
* Enum Define
********************************************************************************/


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  NodeBufferSizeMgr_BM::BMNodeP2ABufferSize class
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

NodeBufferSizeMgr_BM::BMNodeP2ABufferSize::
BMNodeP2ABufferSize(ENUM_STEREO_SCENARIO scenario)
: P2ABufferSize(scenario)
{
    MY_LOGD("+");
    StereoSizeProvider* pSizePrvder = StereoSizeProvider::getInstance();

    Pass2SizeInfo pass2SizeInfo;
    pSizePrvder->getPass2SizeInfo(PASS2A_P, scenario, pass2SizeInfo);
    mFEB_INPUT_SIZE_MAIN2 = pass2SizeInfo.areaWROT;
    mFEAO_AREA_MAIN2 = pass2SizeInfo.areaFEO;
    // frame 1
    pSizePrvder->getPass2SizeInfo(PASS2A, scenario, pass2SizeInfo);
    mFEB_INPUT_SIZE_MAIN1 = pass2SizeInfo.areaWROT;
    // crop info
    pSizePrvder->getPass2SizeInfo(PASS2A_CROP, scenario, pass2SizeInfo);
    mFEB_INPUT_CROP_MAIN1 = pass2SizeInfo.areaWROT;
    // frame 1-Bayer
    pSizePrvder->getPass2SizeInfo(PASS2A_B, scenario, pass2SizeInfo);
    mFD_IMG_SIZE = pass2SizeInfo.areaIMG2O.size;
    mMAIN_IMAGE_SIZE = pass2SizeInfo.areaWDMA.size;
    mBAYER_OUT_SIZE = pass2SizeInfo.areaWROT.size;
    // frame 1-Bayer - crop info
    pSizePrvder->getPass2SizeInfo(PASS2A_B_CROP, scenario, pass2SizeInfo);
    mFD_IMG_CROP = pass2SizeInfo.areaIMG2O;
    mMAIN_IMAGE_CROP = pass2SizeInfo.areaWDMA;
    mBAYER_OUT_CROP = pass2SizeInfo.areaWROT;

    // frame 2
    pSizePrvder->getPass2SizeInfo(PASS2A_P_2, scenario, pass2SizeInfo);
    mFEC_INPUT_SIZE_MAIN2 = pass2SizeInfo.areaIMG2O;
    mRECT_IN_SIZE_MAIN2 = pass2SizeInfo.areaWDMA.size;
    mFEBO_AREA_MAIN2 = pass2SizeInfo.areaFEO;

    // frame 3
    pSizePrvder->getPass2SizeInfo(PASS2A_2, scenario, pass2SizeInfo);
    mFEC_INPUT_SIZE_MAIN1 = pass2SizeInfo.areaIMG2O;
    mRECT_IN_SIZE_MAIN1 = pass2SizeInfo.areaWDMA.size;
    mRECT_IN_CONTENT_SIZE = pass2SizeInfo.areaWDMA.size - pass2SizeInfo.areaWDMA.padding;

    // frame 4
    pSizePrvder->getPass2SizeInfo(PASS2A_P_3, scenario, pass2SizeInfo);
    mFECO_AREA_MAIN2 = pass2SizeInfo.areaFEO;
    // cc_in
    mCCIN_SIZE = pass2SizeInfo.areaIMG2O;

    // MY_S/SL
    StereoSizeProvider * pSizeProvder = StereoSizeProvider::getInstance();
    mMYS_SIZE = pSizeProvder->getBufferSize(E_MY_S, scenario);
    mMYSL_SIZE = pSizeProvder->getBufferSize(E_GF_IN_IMG_2X, scenario);
    mMYSLL_SIZE = pSizeProvder->getBufferSize(E_GF_IN_IMG_4X, scenario);

    debug();
    MY_LOGD("-");
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  NodeBufferSizeMgr_BM::BMNodeN3DBufferSize class
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

NodeBufferSizeMgr_BM::BMNodeN3DBufferSize::
BMNodeN3DBufferSize(ENUM_STEREO_SCENARIO scenario)
: NodeN3DBufferSize(scenario)
{
    // same as NodeBufferSizeMgr::BMNodeN3DBufferSize
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  NodeBufferSizeMgr_BM::BMNodeDPEBufferSize class
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

NodeBufferSizeMgr_BM::BMNodeDPEBufferSize::
BMNodeDPEBufferSize(ENUM_STEREO_SCENARIO scenario)
: NodeDPEBufferSize(scenario)
{
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  NodeBufferSizeMgr_BM::BMNodeOCCBufferSize class
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

NodeBufferSizeMgr_BM::BMNodeOCCBufferSize::
BMNodeOCCBufferSize(ENUM_STEREO_SCENARIO scenario)
: NodeOCCBufferSize(scenario)
{
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  NodeBufferSizeMgr_BM::BMNodeWMFBufferSize class
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

NodeBufferSizeMgr_BM::BMNodeWMFBufferSize::
BMNodeWMFBufferSize(ENUM_STEREO_SCENARIO scenario)
: NodeWMFBufferSize(scenario)
{
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  NodeBufferSizeMgr_BM::BMNodeGFBufferSize class
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

NodeBufferSizeMgr_BM::BMNodeGFBufferSize::
BMNodeGFBufferSize(ENUM_STEREO_SCENARIO scenario)
: NodeGFBufferSize(scenario)
{
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  NodeBufferSizeMgr_BM class
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

NodeBufferSizeMgr_BM::
NodeBufferSizeMgr_BM()
{
    ENUM_STEREO_SCENARIO scen[] = {eSTEREO_SCENARIO_RECORD, eSTEREO_SCENARIO_CAPTURE, eSTEREO_SCENARIO_PREVIEW};

    for(int index=0;index<3;++index)
    {
        mP2ASize.add(scen[index], BMNodeP2ABufferSize(scen[index]));
        mN3DSize.add(scen[index], BMNodeN3DBufferSize(scen[index]));
        mDPESize.add(scen[index], BMNodeDPEBufferSize(scen[index]));
        mOCCSize.add(scen[index], BMNodeOCCBufferSize(scen[index]));
        mWMFSize.add(scen[index], BMNodeWMFBufferSize(scen[index]));
        mGFSize.add(scen[index], BMNodeGFBufferSize(scen[index]));
    }
}

NodeBufferSizeMgr_BM::
~NodeBufferSizeMgr_BM()
{
}

}; // NSFeaturePipe_DepthMap
}; // NSCamFeature
}; // NSCam



