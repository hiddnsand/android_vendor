package com.mediatek.cta;

import com.android.okhttp.MediaType;
import com.android.okhttp.Protocol;
import com.android.okhttp.Request;
import com.android.okhttp.Response;
import com.android.okhttp.ResponseBody;
import com.android.okhttp.okio.Buffer;
import com.android.okhttp.okio.BufferedSource;


import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpRequest;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.RequestLine;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EncodingUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 * CTA implementation
 */
final class CtaHttp {
    //OkHTTP
    public static boolean isMmsAndEmailSendingPermitted(Request request) {
      if (isMoMMS(request)) {
          if (!CtaUtils.enforceCheckPermission("com.mediatek.permission.CTA_SEND_MMS","Send MMS")) {
              System.out.println("Fail to send due to user permission");
              return false;
          }
      } else if (isEmailSend(request)) {
          if (!CtaUtils.enforceCheckPermission("com.mediatek.permission.CTA_SEND_EMAIL","Send emails")) {
              System.out.println("Fail to send due to user permission");
              return false;
          }
      }

      return true;
    }

    public boolean isMmsSendPdu(byte[] pdu) {
        System.out.println("Check isMmsSendPdu");

        int len = pdu.length;
        System.out.println("PDU read len:" + len);
        if (len >= 2) {
            //Convert to unsigned byte
            System.out.println("MMS PDU Type:"
                + (pdu[0] & 0xFF) + ":" + (pdu[1] & 0xFF));
            //X-Mms-Message-Type: m-send-req (0x80)
            if ((pdu[0] & 0xFF) == 0x8C && (pdu[1] & 0xFF) == 0x80) {
                return true;
            }
        }
        return false;
    }

    private static boolean isMoMMS(Request request) {
      final String mimetype = "application/vnd.wap.mms-message";

      if ("POST".equals(request.method())) {
          String userAgent = request.header("User-Agent");
          if (userAgent != null && userAgent.indexOf("MMS") != -1) {
              return true;
          } else {
              String contentType = request.header("Content-Type");
              if (contentType != null) {
                  if (contentType.indexOf(mimetype) != -1) {
                      System.out.println("content type done");
                      return true;
                  }
              }

              String acceptType = request.header("Accept");
              if (acceptType != null) {
                  if (acceptType.indexOf(mimetype) != -1) {
                      System.out.println("accept type done");
                      return true;
                  }
              }

              List<String> contentTypes = request.headers().values("Content-Type");
              for (String value : contentTypes) {
                  if (value.indexOf(mimetype) != -1) {
                      System.out.println("content types done");
                      return true;
                  }
              }
          }
      }

      return false;
    }

    private static boolean isEmailSend(Request request) {
      final String mimetype = "application/vnd.ms-sync.wbxml";
      System.out.println("isEmailSend:" + request.method());

       if ("POST".equals(request.method())
            || "PUT".equals(request.method())) {
          String contentType = request.header("Content-Type");
          if (contentType != null) {
              if (contentType.startsWith("message/rfc822")) {
                  System.out.println("content type done");
                  return true;
              }
          }

          List<String> contentTypes = request.headers().values("Content-Type");
          for (String value : contentTypes) {
              if (value.startsWith("message/rfc822")) {
                  System.out.println("content types done");
                  return true;
              }
          }
      }
      return false;
    }

    public static Response getBadResponse() {
      ResponseBody emptyResponseBody = new ResponseBody() {
          @Override public MediaType contentType() {
            return null;
          }
          @Override public long contentLength() {
            return 0;
          }
          @Override public BufferedSource source() {
            return new Buffer();
          }
      };

      Response badResponse = new Response.Builder()
          .protocol(Protocol.HTTP_1_1)
          .code(400)
          .message("User Permission is denied")
          .body(emptyResponseBody)
          .build();

      return badResponse;
    }

    //Apache HTTP
    public static boolean isMmsAndEmailSendingPermitted(HttpRequest request,
            HttpParams defaultParams) {
        if (isMoMMS(request, defaultParams)) {
            if (!CtaUtils.enforceCheckPermission("com.mediatek.permission.CTA_SEND_MMS", "Send MMS")) {
                System.out.println("Fail to send due to user permission");
                return false;
            }
        } else if (isEmailSend(request)) {
            if (!CtaUtils.enforceCheckPermission("com.mediatek.permission.CTA_SEND_EMAIL", "Send emails")) {
                System.out.println("Fail to send due to user permission");
                return false;
            }
        }

        return true;
    }

    private static boolean isMoMMS(HttpRequest request, HttpParams defaultParams) {
        final String mimetype = "application/vnd.wap.mms-message";
        RequestLine reqLine = request.getRequestLine();

        if (reqLine.getMethod().equals(HttpPost.METHOD_NAME)) {
            String userAgent = HttpProtocolParams.getUserAgent(defaultParams);
            if (userAgent != null && userAgent.indexOf("MMS") != -1) {
                return isMmsSendPdu(request);
            } else {
                if (request instanceof HttpEntityEnclosingRequest) {

                    HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();
                    if (entity != null) {
                        Header httpHeader = entity.getContentType();
                        if (httpHeader != null && httpHeader.getValue() != null) {
                            if (httpHeader.getValue().startsWith(mimetype)) {
                                System.out.println("header done");
                                return isMmsSendPdu(request);
                            }
                        }
                    }

                    Header[] headers = request.getHeaders(HTTP.CONTENT_TYPE);
                    if (headers != null) {
                        for (Header header : headers) {
                            if (header.getValue().indexOf(mimetype) != -1) {
                                System.out.println("header done");
                                return isMmsSendPdu(request);
                            }
                        }
                    }

                    headers = request.getHeaders("ACCEPT");
                    if (headers != null) {
                        for (Header header : headers) {
                            if (header.getValue().indexOf(mimetype) != -1) {
                                System.out.println("header done");
                                return isMmsSendPdu(request);
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    private static boolean isMmsSendPdu(HttpRequest request) {
        if (request instanceof HttpEntityEnclosingRequest) {
            System.out.println("Check isMmsSendPdu");
            HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();
            if (entity != null) {
                InputStream nis = null;
                byte[] buf = new byte[2];
                int len = 0;

                try {
                    InputStream is = entity.getContent();
                    Header contentEncoding = entity.getContentEncoding();
                    if (contentEncoding != null
                            && contentEncoding.getValue().equals("gzip")) {
                        nis = new GZIPInputStream(is);
                    } else {
                        nis = is;
                    }

                    len = nis.read(buf);
                    System.out.println("PDU read len:" + len);
                    if (len == 2) {
                        //Convert to unsigned byte
                        System.out.println("MMS PDU Type:"
                            + (buf[0] & 0xFF) + ":" + (buf[1] & 0xFF));
                        //X-Mms-Message-Type: m-send-req (0x80)
                        if ((buf[0] & 0xFF) == 0x8C && (buf[1] & 0xFF) == 0x80) {
                            return true;
                        }
                    }
                } catch (IOException e) {
                    System.out.println("[CDS]:" + e);
                } catch (IndexOutOfBoundsException ee) {
                    System.out.println("[CDS]:" + ee);
                }
            }
        }
        return false;
    }

    private static boolean isEmailSend(HttpRequest request) {
        RequestLine reqLine = request.getRequestLine();
        final String mimetype = "application/vnd.ms-sync.wbxml";
        System.out.println("isEmailSend:" + reqLine.getMethod());

         if (reqLine.getMethod().equals(HttpPost.METHOD_NAME)
                    || reqLine.getMethod().equals(HttpPut.METHOD_NAME)) {

            // For debugging purpose
            Header[] hs = request.getAllHeaders();

            System.out.println("getAllHeaders:" + reqLine.getMethod());
            for (Header h : hs) {
                System.out.println("test:" + h.getName() + ":" + h.getValue());
            }

            if (request instanceof HttpEntityEnclosingRequest) {

                HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();
                if (entity != null) {
                    Header httpHeader = entity.getContentType();
                    System.out.println("httpHeader:" + httpHeader);
                    if (httpHeader != null && httpHeader.getValue() != null) {
                        if (httpHeader.getValue().startsWith("message/rfc822")
                            || httpHeader.getValue().startsWith(mimetype)) {
                            System.out.println("header done");
                            return true;
                        }
                    }
                }

                Header[] headers = request.getHeaders(HTTP.CONTENT_TYPE);
                if (headers != null) {
                    for (Header header : headers) {
                        if (header.getValue().startsWith("message/rfc822")
                            || header.getValue().startsWith(mimetype)) {
                            System.out.println("header done");
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static HttpResponse returnBadHttpResponse() {
        StatusLine statusLine = new BasicStatusLine(HttpVersion.HTTP_1_1,
                                HttpStatus.SC_BAD_REQUEST, "Bad Request");
        HttpResponse response = new BasicHttpResponse(statusLine);

        byte[] msg = EncodingUtils.getAsciiBytes("User Permission is denied");
        ByteArrayEntity entity = new ByteArrayEntity(msg);
        entity.setContentType("text/plain; charset=US-ASCII");
        response.setEntity(entity);

        return response;
    }

    //Socket
    public static boolean isSendingPermitted(int port) {
        System.out.println("port:" + port);
        if (port == 25 || port == 465 || port == 587) {
            System.out.println("port:" + port);
            if (!CtaUtils.enforceCheckPermission("com.mediatek.permission.CTA_SEND_EMAIL", "Send emails")) {
                System.out.println("Fail to send due to user permission");
                return false;
            }
        }

        return true;
    }
}
