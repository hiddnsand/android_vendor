
/*
 * customized hint config table
 */

struct tCusConfig cusHintConfig[] = {
   /* example
    * {HINT, COMMAND , 0, 0, 0, 0},
    */
    // MTK_CUS_AUDIO_LATENCY_DL
    {(int)MtkCusPowerHint::MTK_CUS_AUDIO_LATENCY_DL, (int)MtkPowerCmd::CMD_SET_CLUSTER_CPU_CORE_MIN, 0, 2, 0 ,0},

    // MTK_CUS_AUDIO_LATENCY_UL
    {(int)MtkCusPowerHint::MTK_CUS_AUDIO_LATENCY_UL, (int)MtkPowerCmd::CMD_SET_CLUSTER_CPU_CORE_MIN, 0, 2, 0 ,0},

    // MTK_CUS_AUDIO_Power_DL
    {(int)MtkCusPowerHint::MTK_CUS_AUDIO_Power_DL, (int)MtkPowerCmd::CMD_SET_CLUSTER_CPU_FREQ_MIN, 0, 800000, 0 ,0},

    // dummy
    {-1, 0, 0, 0, 0, 0},
};

