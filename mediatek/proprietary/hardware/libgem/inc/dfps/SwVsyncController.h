#ifndef __SWVSYNCCONTROLLER_H__
#define __SWVSYNCCONTROLLER_H__

#include <binder/Parcel.h>
#include <binder/ProcessState.h>
#include <binder/IServiceManager.h>
#include <binder/TextOutput.h>

#include <utils/RefBase.h>

namespace android
{
class String16;

class SwVsyncController : public RefBase
{
public:
    SwVsyncController();
    ~SwVsyncController();

    // adjustVSyncMode is used to change the fps and mode of SW VSync
    status_t adjustVSyncMode(int32_t fps, int32_t mode);

    // updateState() is used to update the service state. Binder exception will
    // update this info, so does not call it directly.
    void updateState(int32_t flag);

    // this definition need to be the same with DispSync.h
    enum {
        VSYNC_MODE_DEFAULT_SW = 0,
        VSYNC_MODE_DIRECT_PASS_HW,
        VSYNC_MODE_INTERNAL_SW,
    };

    enum {
        STATE_SERVER_DIED = 0x01,
    };

private:
    status_t reconnectLocked();
    status_t changeVSyncModeWithFps(int32_t fps, int32_t mode);

    sp<IBinder> mSf;
    String16 mIfName;
    status_t mState;
    mutable Mutex mStateLock;
};

};

#endif
