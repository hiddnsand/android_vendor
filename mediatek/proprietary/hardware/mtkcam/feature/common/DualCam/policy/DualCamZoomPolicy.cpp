/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifdef LOG_TAG
    #undef LOG_TAG
#endif

#define LOG_TAG "MtkCam/DualCamZoomPolicy"

#include "DualCamZoomPolicy.h"

#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/utils/metastore/IMetadataProvider.h>

#include <cutils/properties.h>
#include <mtkcam/feature/featurePipe/IStreamingFeaturePipe.h>
#include <mtkcam/feature/featurePipe/IStreamingFeaturePipe_var.h>
#include <mtkcam/feature/3dnr/util_3dnr.h>
#include <mtkcam/feature/3dnr/lmv_state.h>

#include <mtkcam/feature/DualCam/DualCam.Common.h>

#include <mtkcam/drv/mem/cam_cal_drv.h>
// for policy
#include <mtkcam/utils/hw/CamManager.h>
#include <mtkcam/aaa/ISync3A.h>

#if (MTKCAM_HAVE_DUAL_ZOOM_SUPPORT == 1)
#include <camera_custom_dualzoom.h>
#include <camera_custom_dualzoom_func.h>
#include <mtkcam/feature/stereo/hal/stereo_setting_provider.h>
#endif

using NSCam::NSCamFeature::NSFeaturePipe::IStreamingFeaturePipe;
using NSCam::NSCamFeature::NSFeaturePipe::FeaturePipeParam;
using NSCam::NSCamFeature::VarMap;

#undef MY_LOGV
#undef MY_LOGD
#undef MY_LOGI
#undef MY_LOGW
#undef MY_LOGE
#undef MY_LOGA
#undef MY_LOGF

#define MY_LOGV(id, fmt, arg...)        CAM_LOGV("[%d]id:%d[%s] " fmt, mRefCount, id, __FUNCTION__, ##arg)
#define MY_LOGD(id, fmt, arg...)        CAM_LOGD("[%d]id:%d[%s] " fmt, mRefCount, id, __FUNCTION__, ##arg)
#define MY_LOGI(id, fmt, arg...)        CAM_LOGI("[%d]id:%d[%s] " fmt, mRefCount, id, __FUNCTION__, ##arg)
#define MY_LOGW(id, fmt, arg...)        CAM_LOGW("[%d]id:%d[%s] " fmt, mRefCount, id, __FUNCTION__, ##arg)
#define MY_LOGE(id, fmt, arg...)        CAM_LOGE("[%d]id:%d[%s] " fmt, mRefCount, id, __FUNCTION__, ##arg)
#define MY_LOGA(id, fmt, arg...)        CAM_LOGA("[%d]id:%d[%s] " fmt, mRefCount, id, __FUNCTION__, ##arg)
#define MY_LOGF(id, fmt, arg...)        CAM_LOGF("[%d]id:%d[%s] " fmt, mRefCount, id, __FUNCTION__, ##arg)

//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

#define FUNCTION_IN()             CAM_LOGI("[%d][%s]+", mRefCount, __FUNCTION__)
#define FUNCTION_OUT()            CAM_LOGI("[%d][%s]-", mRefCount, __FUNCTION__)

#define THERMAL_DUALZOOM_POLICY_NAME "thermal_policy_07"

namespace NSCam
{

/******************************************************************************
 * Initialize
 ******************************************************************************/
MINT32 DualCamZoomPolicy::totalRefCount = 0;
std::future<void> DualCamZoomPolicy::tPolicySetting;

/******************************************************************************
 *
 ******************************************************************************/
DualCamZoomPolicy::DualCamZoomPolicy():
    mRefCount(totalRefCount)
{
    DualCamZoomPolicy::totalRefCount++;
    MY_LOGD(-1, "new DualCamZoomPolicy(%p)", this);

    MY_LOGD(-1, "set dualzoom thermal policy");
    if(DualCamZoomPolicy::tPolicySetting.valid())
    {
        MY_LOGD(-1, "have share state");
        DualCamZoomPolicy::tPolicySetting.get();
    }
    // set thermal policy first.
    DualCamZoomPolicy::tPolicySetting = std::async(
                std::launch::async,
                []()
                {
                    Utils::CamManager::getInstance()->setThermalPolicy((char *)THERMAL_DUALZOOM_POLICY_NAME, 1);
                });

    mIFrame3AControl = NULL;
    mOpenIds.clear();

    mPreviousZoomRatio = 0;
    mWaitStable = DUALZOOM_WAIT_STABLE_COUNT;
    mWaitEnterLP = DUALZOOM_WAIT_LOW_POWER_COUNT;
    mWaitEnterAct = DUALZOOM_WAIT_CAM_STANDBY_TO_ACT;
    mWideState = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_INVALID;
    mTeleState = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_INVALID;

    mPrevCamID = -1;
    mLockStatus = false;
    mSet3ASyncMode = MTK_DUAL_CAM_AAA_SYNC_STATE::NOT_SET;
    mAFLowCount = 0;
    mAFHighCount = 0;
    mAELowCount = 0;
    mAEHighCount = 0;
    mTeleAEHighCount = 0;
    mAAAForceCamId = -1;
    mAFPrevDAC = 0;
    mPauseAFCamId = -1;
    mResumeAFCount = 0;
    mDoCapCamId = -1;
    mAETeleLv = -1;
    mSkipAAACheck = property_get_int32("debug.camera.dualzoom.skip3a", 0);

    mSetSensorStatus = MTK_P1_SENSOR_STATUS_NONE;

    // for 3DNR/LMV
    LmvStateMachine::createInstance(LmvStateMachine::STATE_ON_WIDE);

    // online fov calibration
    mFovOnlineStatus = 0;

    mTGNum = MTK_P1_TWIN_SWITCH_TWO_TG; // while open camera, default is two TG mode
    mSlaveInfo.Id = -1;
    mSlaveInfo.IsStandby = false;

    // debug
    mLogLevel = property_get_int32("debug.dualzoom.policy", 0);
#if (MTKCAM_HAVE_DUAL_ZOOM_FUSION_SUPPORT == 1)
    mForceState = property_get_int32("debug.dualzoom.forcestate", 1);
#else
    mForceState = property_get_int32("debug.dualzoom.forcestate", 8);
#endif

    // onlinr fov tune
    mthresholdZoomRatio = property_get_int32("debug.fovonline.zoomratio", 1000);
    mthresholdThermal = property_get_int32("debug.fovonline.thermal", DUALZOOM_FOV_ONLINE_TEMP_MAX);
    mthresholdDACWide = property_get_int32("debug.fovonline.widedac", DUALZOOM_FOV_ONLINE_DAC_WIDE_MAX);
    mthresholdDACTele = property_get_int32("debug.fovonline.teledac", DUALZOOM_FOV_ONLINE_DAC_TELE_MAX);
    mthresholdISO = property_get_int32("debug.fovonline.iso", DUALZOOM_FOV_ONLINE_ISO_MAX);
    mthresholdExpTime = property_get_int32("debug.fovonline.exptime", DUALZOOM_FOV_ONLINE_EXPTIME_MAX);
    // 3A policy tune
    mAFStrategy = property_get_int32("debug.dualzoom.afstrategy", 0);
    mAFTeleDac  = property_get_int32("debug.dualzoom.afteledac", 0);
    mTeleLowISO = property_get_int32("debug.dualzoom.tele.lowiso", DUALZOOM_AE_ISO_LOW_THRESHOLD);
    mTeleHighISO = property_get_int32("debug.dualzoom.tele.highiso", 1000);
}

DualCamZoomPolicy::~DualCamZoomPolicy()
{
    FUNCTION_IN();
    if(DualCamZoomPolicy::tPolicySetting.valid())
    {
        MY_LOGD(-1, "have share state");
        DualCamZoomPolicy::tPolicySetting.get();
    }
    MY_LOGD(-1, "set default thermal policy");
    DualCamZoomPolicy::tPolicySetting = std::async(
                std::launch::async,
                [this]()
                {
                    Utils::CamManager::getInstance()->setThermalPolicy((char *)THERMAL_DUALZOOM_POLICY_NAME, 0);
                });
    mIFrame3AControl = NULL;
    mMgrCb = NULL;
    mOpenIds.clear();

    // for 3DNR/LMV
    LmvStateMachine::destroyInstance();
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID DualCamZoomPolicy::setCameraOpenIds(vector<MINT32> ids)
{
    mOpenIds.clear();

    mWideState = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_INVALID;
    mTeleState = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_INVALID;

    for (MINT i = 0; i < static_cast<MINT>(ids.size()); i++)
    {
        MINT32 id = ids[i];

        MY_LOGD(-1, "id[%d]: %d", i, ids[i]);
        mOpenIds.push_back(ids[i]);

        if (id == DUALZOOM_WIDE_CAM_ID)
        {
            mWideState = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE;
            sp<IMetadataProvider> pMetadataProvider = NSMetadataProviderManager::valueFor(id);
            if (! pMetadataProvider.get())
            {
                MY_LOGE(id, " ! pMetadataProvider.get() ");
                return ;
            }

            IMetadata static_meta = pMetadataProvider->getMtkStaticCharacteristics();
            {
                IMetadata::IEntry active_array_entry = static_meta.entryFor(MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION);
                if (!active_array_entry.isEmpty())
                {
                    mActiveArrayWide = active_array_entry.itemAt(0, Type2Type<MRect>());
                    MY_LOGD(id, "wide camera: active array(%d, %d, %dx%d)",
                            mActiveArrayWide.p.x, mActiveArrayWide.p.y, mActiveArrayWide.s.w, mActiveArrayWide.s.h);
                }
                else
                {
                    MY_LOGE(id, "no static info: MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION");
                }
            }
            #if (MTKCAM_HAVE_DUAL_ZOOM_SUPPORT == 1)
            // set AF DAC data
            {
                int32_t devIdx[2];
                int MacroPos;
                int InfPos;
                int MidPos;
                StereoSettingProvider::getStereoSensorDevIndex(devIdx[0], devIdx[1]);
                //Get calibration data
                CAM_CAL_DATA_STRUCT calibrationData;
                CamCalDrvBase *pCamCalDrvObj = CamCalDrvBase::createInstance();
                MUINT32 queryResult;

                queryResult = pCamCalDrvObj->GetCamCalCalData(devIdx[0], CAMERA_CAM_CAL_DATA_3A_GAIN, (void *)&calibrationData);

                MidPos = calibrationData.Single2A.S2aAF_t.AF_Middle_calibration;
                MacroPos = calibrationData.Single2A.S2aAf[1];
                InfPos = calibrationData.Single2A.S2aAf[0];

                get_Dualzoom_AF_Thresholds(InfPos, MacroPos, mAFDACHighTh, mAFDACLowTh);
                mAFPrevDAC = 0;

                MY_LOGD(id, "AF DAC high/low : %d/%d", mAFDACHighTh, mAFDACLowTh);
                MY_LOGD(id, "AF DAC macro/inf/mid : %d/%d/%d", MacroPos, InfPos, MidPos);

                pCamCalDrvObj->destroyInstance();
            }
            #endif
            mPrevCamID = id;
        }
        if (id == DUALZOOM_TELE_CAM_ID)
        {
            mTeleState = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE;
            sp<IMetadataProvider> pMetadataProvider = NSMetadataProviderManager::valueFor(id);
            if (! pMetadataProvider.get())
            {
                MY_LOGE(id, " ! pMetadataProvider.get() ");
                return ;
            }

            IMetadata static_meta = pMetadataProvider->getMtkStaticCharacteristics();
            {
                IMetadata::IEntry active_array_entry = static_meta.entryFor(MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION);
                if (!active_array_entry.isEmpty())
                {
                    mActiveArrayTele = active_array_entry.itemAt(0, Type2Type<MRect>());
                    MY_LOGD(id, "tele camera: active array(%d, %d, %dx%d)",
                            mActiveArrayTele.p.x, mActiveArrayTele.p.y, mActiveArrayTele.s.w, mActiveArrayTele.s.h);
                }
                else
                {
                    MY_LOGE(id, "no static info: MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION");
                }
            }
            mAFTeleState = 0;
            #if (MTKCAM_HAVE_DUAL_ZOOM_SUPPORT == 1)
            {
                if (mAFTeleDac == 0)
                {
                    get_Dualzoom_AF_TeleDACTh(mAFTeleDac);
                }
            }
            #endif
        }
    }
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID DualCamZoomPolicy::setObject(void* obj, MINT32 arg1)
{
    if (obj != NULL)
    {
        mIFrame3AControl = (IFrame3AControl*)obj;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID DualCamZoomPolicy::setParameter(std::string str)
{
    Mutex::Autolock _l(mLock);
    if (str.compare(VAR_DUALCAM_FOV_ONLINE) == 0)
    {
        MY_LOGD(-1, "reset waitStable to %d", DUALZOOM_WAIT_STABLE_COUNT);
        mWaitStable = DUALZOOM_WAIT_STABLE_COUNT;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID DualCamZoomPolicy::addCallBack(sp<ISyncManagerCallback> Cb)
{
    mMgrCb = Cb;
}



/******************************************************************************
 *
 ******************************************************************************/
MVOID DualCamZoomPolicy::updateSetting(
    MINT32 const i4OpenId,
    IMetadata* appMetadata, IMetadata* halMetadata,
    SyncManagerParams &syncParams)
{
    Mutex::Autolock _l(mLock);
    IMetadata::IEntry const& entry = appMetadata->entryFor(MTK_SCALER_CROP_REGION);
    MRect crop;
    MINT32 dropReq = false;
    AAAInfo info;
    MINT32 zoomRatio;
    if (!entry.isEmpty())
    {
        MY_LOGD(i4OpenId, "size : %d", entry.count());
        crop = entry.itemAt(0, Type2Type<MRect>());
        MY_LOGD(i4OpenId, "Crop info : X(%d), Y(%d), W(%d), H(%d)", crop.p.x, crop.p.y, crop.s.w,
                crop.s.h);
        /*MY_LOGD(i4OpenId, "WillDBG Crop info : X(%d), Y(%d), W(%d), H(%d)", crop.p.x, crop.p.y, crop.s.w,
                crop.s.h);
        MY_LOGD(i4OpenId, "WillDBG wide AR info : X(%d), Y(%d), W(%d), H(%d)",
                mActiveArrayWide.p.x, mActiveArrayWide.p.y, mActiveArrayWide.s.w, mActiveArrayWide.s.h);
        MY_LOGD(i4OpenId, "WillDBG tele AR info : X(%d), Y(%d), W(%d), H(%d)",
                mActiveArrayTele.p.x, mActiveArrayTele.p.y, mActiveArrayTele.s.w, mActiveArrayTele.s.h);*/
    }
    if (syncParams.mPartialUpdate)
    {
        MY_LOGD(i4OpenId, "goto fov partialupdate");
        goto UPDATE_FOVMARGIN;
    }
    if (mPreviousZoomRatio != syncParams.miZoomRatio)
    {
        mPreviousZoomRatio = syncParams.miZoomRatio;
        mWaitStable = DUALZOOM_WAIT_STABLE_COUNT;
    }

    if (mWideState != MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE && mTeleState != MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE)
    {
        MY_LOGE(i4OpenId, "Should not be here, camera state machine error, please check!!!!!!!");
    }

    if (mLockStatus)
    {
        if(i4OpenId != mPrevCamID)
        {
            dropReq = true;
        }
        else
        {
            MTK_SYNC_CAEMRA_STATE state = (i4OpenId == mOpenIds[0]) ? mWideState : mTeleState;
            if (state != MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE)
            {
                MY_LOGE(i4OpenId, "Error!!!, lock preview camera is not active state : camID(%d), state(%d)", mPrevCamID, (int)state);
            }
            dropReq = false;
            MY_LOGD(i4OpenId, "info : Preview CamID(%d), wide state(%d), tele state(%d)", mPrevCamID, (int)mWideState, (int)mTeleState);
        }
    }
    else
    {
        // Wide or Tele
        selectPrevCamBy3A(i4OpenId, syncParams.miZoomRatio >= DUALZOOM_SWICH_CAM_ZOOM_RATIO, mWideState, mTeleState);
        if (i4OpenId == DUALZOOM_WIDE_CAM_ID)
        {
            //selectPrevCamBy3A(i4OpenId);
            info.ZoomDrop = syncParams.miZoomRatio >= DUALZOOM_SWICH_CAM_ZOOM_RATIO;
            info.AAAJudgeId = mAAAForceCamId;
            info.is4KVideo = syncParams.mb4KVideo;
            checkStallPipe(i4OpenId, info, dropReq);
            changeCameraState(i4OpenId, info, dropReq,
                    appMetadata, halMetadata, mWideState);
        }
        else if (i4OpenId == DUALZOOM_TELE_CAM_ID)
        {
            info.ZoomDrop = syncParams.miZoomRatio < DUALZOOM_SWICH_CAM_ZOOM_RATIO;
            info.AAAJudgeId = mAAAForceCamId;
            info.is4KVideo = syncParams.mb4KVideo;
            checkStallPipe(i4OpenId, info, dropReq);
            changeCameraState(i4OpenId, info, dropReq,
                    appMetadata, halMetadata, mTeleState);
        }
    }

    // do dropFrame if set
    setDropReqAndStandby(i4OpenId, dropReq, mSetSensorStatus, halMetadata);

    if (mWideState == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE &&
        mTeleState == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE)
    {
        do {
            if (mForceState & DUALCAM_FORCE_NO_FOV_ONLINE) {
                MY_LOGD(i4OpenId, "force not do fov online frame sync");
                break;
            }
            if (!checkFOVOnline(i4OpenId, syncParams.miZoomRatio, syncParams.mIsVideoRec)) {
                // environment not qulified, can't do online fov
                break;
            }
            if (mForceState & DUALCAM_FORCE_NO_SYNCMGR_SYNC) {
                MY_LOGD(i4OpenId, "force not add frame sync hint to sync manager");
                break;
            }
            MY_LOGD(i4OpenId, "start frame sync to do fov online calibration");
            IMetadata::IEntry tag(MTK_DUALZOOM_DO_FRAME_SYNC);
            tag.push_back(1, Type2Type<MINT32>());
            halMetadata->update(MTK_DUALZOOM_DO_FRAME_SYNC, tag);
            if (dropReq && mIFrame3AControl.get()) {
                Frame3ASetting_t setting =
                {
                    .openId   = i4OpenId,
                    .dropMode = MTK_DUALZOOM_DROP_NEED_SYNCMGR_NEED_STREAM_F_PIPE,
                };
                mIFrame3AControl->set(&setting, F3A_TYPE_DROP_MODE, NULL, halMetadata);
            }
        } while (0);
        changeCameraMode(i4OpenId, F3A_TYPE_FRAME_SYNC, false, true, 0, 0, appMetadata, halMetadata);
    }
    else
    {
        //MY_LOGD(i4OpenId, "pause frame sync");
        changeCameraMode(i4OpenId, F3A_TYPE_FRAME_SYNC, false, false, 0, 0, appMetadata, halMetadata);
        mFovOnlineStatus = 0;
    }

    // fix strobe issue
    if (mWideState != MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_INVALID && mTeleState != MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_INVALID)
    {
        IMetadata::IEntry const& entry1 = appMetadata->entryFor(MTK_CONTROL_AE_PRECAPTURE_TRIGGER);

        if (mResumeAFCount > 0 && mDoCapCamId == i4OpenId)
        {
            mResumeAFCount--;
        }
        if (!entry1.isEmpty())
        {
            MUINT8 AE_PreCap = entry1.itemAt(0, Type2Type<MUINT8>());
            if (AE_PreCap == MTK_CONTROL_AE_PRECAPTURE_TRIGGER_START)
            {
                mPauseAFCamId = (i4OpenId == mOpenIds[0]) ? mOpenIds[1] : mOpenIds[0];
                mDoCapCamId = i4OpenId;
                mResumeAFCount = 30;
                MY_LOGD(i4OpenId, "strobe trace1");
            }
        }

        if (mPauseAFCamId == i4OpenId && mResumeAFCount == 30)
        {
            IMetadata::IEntry tag(MTK_FOCUS_PAUSE);
            tag.push_back(1, Type2Type<MUINT8>());
            halMetadata->update(MTK_FOCUS_PAUSE, tag);
            MY_LOGD(i4OpenId, "strobe trace2");
        }
        if (mPauseAFCamId == i4OpenId && mResumeAFCount == 0)
        {
            IMetadata::IEntry tag(MTK_FOCUS_PAUSE);
            tag.push_back(0, Type2Type<MUINT8>());
            halMetadata->update(MTK_FOCUS_PAUSE, tag);
            mPauseAFCamId = -1;
            mDoCapCamId = -1;
            mResumeAFCount = 0;
            MY_LOGD(i4OpenId, "strobe trace3");
        }
    }
    // for strobe
    if (dropReq)
    {
        /*IMetadata::IEntry entry = appMetadata->entryFor(MTK_CONTROL_AE_MODE);
        entry.push_back(MTK_CONTROL_AE_MODE_ON, Type2Type<MINT32>());
        appMetadata->update(MTK_CONTROL_AE_MODE, entry);*/
    }
    else
    {
        if (mWideState != MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_INVALID && mTeleState != MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_INVALID)
        {
            setMasterCamera(i4OpenId, (i4OpenId == mOpenIds[0]) ? mOpenIds[1] : mOpenIds[0], halMetadata);
            if (i4OpenId == mPauseAFCamId)
            {
                IMetadata::IEntry tag(MTK_FOCUS_PAUSE);
                tag.push_back(0, Type2Type<MUINT8>());
                halMetadata->update(MTK_FOCUS_PAUSE, tag);
                mPauseAFCamId = -1;
                mDoCapCamId = -1;
                mResumeAFCount = 0;
                MY_LOGD(i4OpenId, "strobe trace4");
            }
        }
    }

    {
    // ************************** 3DNR LMV, per frame setting Begin ********************************
        // ask LmvStateMachine to check
        LmvStateMachine* lmv = LmvStateMachine::getInstance();

        if (lmv != NULL && mOpenIds.size() > 1)
        {
            MBOOL doSend = false;

            if ((i4OpenId == DUALZOOM_WIDE_CAM_ID && lmv->getCurrentState() == LmvStateMachine::STATE_ON_WIDE) ||
                (i4OpenId == DUALZOOM_TELE_CAM_ID && lmv->getCurrentState() == LmvStateMachine::STATE_ON_TELE))
            {
                LmvStateMachine::SensorId id =
                    (mPrevCamID == DUALZOOM_WIDE_CAM_ID) ? LmvStateMachine::WIDE : LmvStateMachine::TELE;

                LmvStateMachine::Action act;
                lmv->switchTo(id, &act);

                if (LmvStateMachine::DO_NOTHING != act.cmd)
                {
                    MY_LOGD(i4OpenId, "id = %d, act.cmd = %d, i4OpenId = %d", id, act.cmd, i4OpenId);
                    doSend = true;

                    MY_LOGD(i4OpenId, "add MTK_NR_FEATURE_LMV_SEND_SWITCH_OUT on CamID %d", i4OpenId);
                    IMetadata::IEntry tag(MTK_LMV_SEND_SWITCH_OUT);
                    tag.push_back(1, Type2Type<MINT32>());
                    halMetadata->update(MTK_LMV_SEND_SWITCH_OUT, tag);
                }
            }

            // double check per-frame for retry (to trigger driver)
            if (!doSend)
            {
                LmvStateMachine::SensorId id =
                    (i4OpenId == DUALZOOM_WIDE_CAM_ID) ? LmvStateMachine::WIDE : LmvStateMachine::TELE;
                doSend = lmv->needEnque(id, true);
            }

            if (doSend)
            {
                // avoid drop frame for 3DNR LMV state machine
                // dummy frame
                if (dropReq)
                {
                    IMetadata::IEntry const& entry = halMetadata->entryFor(MTK_DUALZOOM_DROP_REQ);
                    int DropValue = MTK_DUALZOOM_DROP_NONE;
                    if (!entry.isEmpty()) {
                        DropValue = entry.itemAt(0, Type2Type<MINT32>());
                    }

                    if (DropValue > MTK_DUALZOOM_DROP_DIRECTLY)
                    {
                        MY_LOGD(i4OpenId, "add MTK_DUALZOOM_DROP_NEED_SYNCMGR on CamID %d", i4OpenId);
                        if (mIFrame3AControl.get() != NULL)
                        {
                            Frame3ASetting_t setting =
                            {
                                .openId = i4OpenId,
                                .dropMode = MTK_DUALZOOM_DROP_NEED_SYNCMGR,
                            };
                            mIFrame3AControl->set(&setting, F3A_TYPE_DROP_MODE, NULL, halMetadata);
                        }
                        else
                        {
                            IMetadata::IEntry tag(MTK_DUALZOOM_DROP_REQ);
                            tag.push_back(MTK_DUALZOOM_DROP_NEED_SYNCMGR, Type2Type<MINT32>());
                            halMetadata->update(MTK_DUALZOOM_DROP_REQ, tag);
                        }
                    }
                }
            }
        }


        IMetadata::IEntry tag_LMV(MTK_DUALZOOM_3DNR_MODE);
        if (mPrevCamID == i4OpenId)
        {
            tag_LMV.push_back(MTK_NR_FEATURE_3DNR_MODE_ON, Type2Type<MINT32>());
        }
        else
        {
            tag_LMV.push_back(MTK_NR_FEATURE_3DNR_MODE_OFF, Type2Type<MINT32>());
        }
        halMetadata->update(MTK_DUALZOOM_3DNR_MODE, tag_LMV);
    // ************************** 3DNR LMV, per frame setting End ********************************
    }

    // Wide or Tele
    if (i4OpenId == DUALZOOM_WIDE_CAM_ID)
    {
        zoomRatio = ((mActiveArrayWide.s.w * 100)) / crop.s.w;
        {
            IMetadata::IEntry tag(MTK_DUALZOOM_ZOOM_FACTOR);
            tag.push_back(zoomRatio, Type2Type<MINT32>());
            halMetadata->update(MTK_DUALZOOM_ZOOM_FACTOR, tag);
        }

    }
    else if (i4OpenId == DUALZOOM_TELE_CAM_ID)
    {
        zoomRatio = ((mActiveArrayTele.s.w * 100)) / crop.s.w;
        {
            IMetadata::IEntry tag(MTK_DUALZOOM_ZOOM_FACTOR);
            tag.push_back(zoomRatio, Type2Type<MINT32>());
            halMetadata->update(MTK_DUALZOOM_ZOOM_FACTOR, tag);
        }
    }
    if (syncParams.miZoomRatio != 0)
    {
        IMetadata::IEntry tag(MTK_DUALZOOM_ZOOMRATIO);
        tag.push_back(syncParams.miZoomRatio, Type2Type<MINT32>());
        halMetadata->update(MTK_DUALZOOM_ZOOMRATIO, tag);
    }
UPDATE_FOVMARGIN:
    {
        MRect FOVCrop;
        MSize MarginSize = MSize(0, 0);
        IMetadata::IEntry tag(MTK_DUALZOOM_FOV_RECT_INFO);
        {
            IMetadata::IEntry const& entry = halMetadata->entryFor(MTK_DUALZOOM_FOV_MARGIN_PIXEL);
            if (!entry.isEmpty())
            {
                MarginSize = entry.itemAt(0, Type2Type<MSize>());
                MarginSize.h = MarginSize.w * crop.s.h / crop.s.w;
            }
        }
        if (MarginSize.w != 0 && MarginSize.h != 0)
        {

            if (crop.s.w > MarginSize.w)
            {
                FOVCrop.s.w = crop.s.w - MarginSize.w;
                FOVCrop.s.h = crop.s.h - MarginSize.h;
                FOVCrop.s.w = FOVCrop.s.w + (FOVCrop.s.w & 0x01);
                FOVCrop.s.h = FOVCrop.s.h + (FOVCrop.s.h & 0x01);
            }
            {
                IMetadata::IEntry tag(MTK_DUALZOOM_FOV_MARGIN_PIXEL);
                tag.push_back(MarginSize, Type2Type<MSize>());
                halMetadata->update(MTK_DUALZOOM_FOV_MARGIN_PIXEL, tag);
            }
            tag.push_back(mActiveArrayWide.s.w, Type2Type<MINT32>());
            tag.push_back(mActiveArrayWide.s.h, Type2Type<MINT32>());
            tag.push_back(mActiveArrayTele.s.w, Type2Type<MINT32>());
            tag.push_back(mActiveArrayTele.s.h, Type2Type<MINT32>());
            tag.push_back(crop.s.w, Type2Type<MINT32>());
            tag.push_back(crop.s.h, Type2Type<MINT32>());
            tag.push_back(FOVCrop.s.w, Type2Type<MINT32>());
            tag.push_back(FOVCrop.s.h, Type2Type<MINT32>());
            tag.push_back(crop.p.x + (crop.s.w >> 1), Type2Type<MINT32>());
            tag.push_back(crop.p.y + (crop.s.h >> 1), Type2Type<MINT32>());
            halMetadata->update(MTK_DUALZOOM_FOV_RECT_INFO, tag);
            MY_LOGD(i4OpenId, "FOVCrop : %dx%d", FOVCrop.s.w, FOVCrop.s.h);
            MY_LOGD(i4OpenId, "Crop : %dx%d", crop.s.w, crop.s.h);
        }
        {
            IMetadata::IEntry tag(MTK_DUALZOOM_FOV_CALB_INFO);
            if (i4OpenId == DUALZOOM_WIDE_CAM_ID)
            {
                tag.push_back(mActiveArrayWide.s.w, Type2Type<MINT32>());
                tag.push_back(mActiveArrayWide.s.h, Type2Type<MINT32>());
            }
            else
            {
                tag.push_back(mActiveArrayTele.s.w, Type2Type<MINT32>());
                tag.push_back(mActiveArrayTele.s.h, Type2Type<MINT32>());
            }
            tag.push_back(crop.s.w, Type2Type<MINT32>());
            tag.push_back(crop.s.h, Type2Type<MINT32>());
            tag.push_back(zoomRatio, Type2Type<MINT32>());
            halMetadata->update(MTK_DUALZOOM_FOV_CALB_INFO, tag);
        }
    }
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID DualCamZoomPolicy::updateAfterP1(MINT32 const i4OpenId, MUINT32 const frameNo,
                                       void* obj, MINT32 arg1)
{
    if (obj == NULL)
    {
        return;
    }

    FeaturePipeParam* pipeParam = (FeaturePipeParam*)obj;


    // update for 3DNR/LMV
    LmvStateMachine* lmv = LmvStateMachine::getInstance();

    if (lmv != NULL && mOpenIds.size() > 0)
    {
        LmvStateMachine::SensorId id =
            (i4OpenId == mOpenIds[0]) ? LmvStateMachine::WIDE : LmvStateMachine::TELE;

        MINT32 lmv_result = -1;
        lmv_result = pipeParam->mVarMap.get<MINT32>(VAR_LMV_SWITCH_OUT_RST, -1);
        if (lmv_result >= 0)
        {
            lmv->notifySwitchResult(id, (LmvStateMachine::SwitchResult)lmv_result);
        }

        MINT32 lmv_validity = -1;
        lmv_validity = pipeParam->mVarMap.get<MINT32>(VAR_LMV_VALIDITY, -1);
        if (lmv_validity >= 0)
        {
            lmv->notifyLmvValidity(id, lmv_validity);
        }
    }

    {
        MINT32 twin_num = -1;
        twin_num = pipeParam->mVarMap.get<MINT32>(VAR_P1RAW_TWIN_STATUS, -1);
        if (twin_num >= 0)
        {
            Mutex::Autolock _l(mLock);
            mTGNum = twin_num;
        }
    }
}
/******************************************************************************
 *
 ******************************************************************************/
MVOID
DualCamZoomPolicy::updateFeatureMask(MUINT32 &featureMask)
{
#if (MTKCAM_HAVE_DUAL_ZOOM_VENDOR_FOV_SUPPORT==1)
    NSCamFeature::NSFeaturePipe::ENABLE_VENDOR_FOV(featureMask);
#else
    NSCamFeature::NSFeaturePipe::ENABLE_FOV(featureMask);
#endif // MTKCAM_HAVE_DUAL_ZOOM_VENDOR_FOV_SUPPORT
}

/******************************************************************************
 *
 ******************************************************************************/
MINT32
DualCamZoomPolicy::getPreviewCameraId(MINT32 *dropOthers)
{
    if (dropOthers)
    {
        if (!(mForceState & DUALCAM_FORCE_NO_DROP))
        {
            *dropOthers = 1;
        }
        else
        {
            *dropOthers = 0;
        }
    }
    else
    {
        MY_LOGD(-1, "skip drop other");
    }
    return mPrevCamID;
}

/******************************************************************************
 *
 ******************************************************************************/
void DualCamZoomPolicy::checkStallPipe(
    MINT32  const               openId,
    AAAInfo                     info,
    MINT32&                     dropReq)
{
    if (openId == DUALZOOM_WIDE_CAM_ID)
    {
        if (mWideState != MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE)
        {
            dropReq = true;
        }
        else if (mTeleState != MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE
                 )
        {
            if (mTeleState == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_INVALID && info.ZoomDrop)
            {
                dropReq = true;
            }
            else {
                dropReq = false;
            }
        }
        else if (info.is4KVideo)
        {
            dropReq = false;
        }
        else if (info.AAAJudgeId != -1)
        {
            if (info.AAAJudgeId == openId)
            {
                dropReq = false;
            }
            else
            {
                dropReq = true;
            }
        }
        else if (info.ZoomDrop)
        {
            dropReq = true;
        }
    }
    else if (openId == DUALZOOM_TELE_CAM_ID)
    {
        if (mWideState != MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE)
        {
            dropReq = false;
        }
        else if (mTeleState != MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE)
        {
            dropReq = true;
        }
        else if (info.is4KVideo)
        {
            dropReq = true;
        }
        else if (info.AAAJudgeId != -1)
        {
            if (info.AAAJudgeId == openId)
            {
                dropReq = false;
            }
            else
            {
                dropReq = true;
            }
        }
        if (info.ZoomDrop)
        {
            dropReq = true;
        }
    }
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL DualCamZoomPolicy::checkFOVOnline(MINT32 const openId, MUINT32 zoomRatio, MINT32 IsRec)
{
    MINT32 ret = MTRUE;
    Frame3ASetting_t setting =
    {
        .openId   = openId
    };

    if (IsRec || mLockStatus)
        return MFALSE;

    if (mForceState & DUALCAM_FORCE_NO_FOV_RULE_CHECK)
    {
        return ret;
    }
    if (openId == DUALZOOM_TELE_CAM_ID)
    {
        goto lbEXIT;
    }
    {
        // 0. check zoom ratio
        MINT32 thresholdZoomRatio = mthresholdZoomRatio;
        if (zoomRatio > (MUINT32)thresholdZoomRatio) {
            MY_LOGD(openId, "zoomRatio(%d) too large (>%d)", zoomRatio, 1000);
            ret = MFALSE;
            goto lbEXIT;
        }
    }
    if (mIFrame3AControl != NULL)
    {
        mIFrame3AControl->get(&setting, F3A_TYPE_3A_INFO | F3A_TYPE_FOV_ONLINE_INFO);
    }
    {
        // 1. check thermal
        MINT32 thresholdThermal = mthresholdThermal;
        if (setting.temperature > thresholdThermal) {
            MY_LOGD(openId, "temperature(%d) too high (>%d)", setting.temperature, thresholdThermal);
            ret = MFALSE;
            goto lbEXIT;
        }
    }
    //
    {
        // 2. check DAC
        MINT32 thresholdDAC;
        MINT32 normalized_value;
        if (openId == DUALZOOM_WIDE_CAM_ID) {
            thresholdDAC = mthresholdDACWide;
        } else {
            thresholdDAC = mthresholdDACTele;
        }
        if (setting.maxDac - setting.minDac == 0) {
            MY_LOGE(openId, "maxDac == minDac (%d), something wrong", setting.maxDac);
        } else {
            normalized_value = (setting.AFDAC - setting.minDac) * 1000 / (setting.maxDac - setting.minDac);
            if (normalized_value >= thresholdDAC) {
                MY_LOGD(openId, "dac(%d) too high (>%d)", normalized_value, thresholdDAC);
                ret = MFALSE;
                goto lbEXIT;
            }
        }
    }
    //
    {
        // 3. check ISO
        MINT32 thresholdISO = mthresholdISO;
        if (setting.isoValue >= thresholdISO) {
            MY_LOGD(openId, "iso(%d) too high (>%d)", setting.isoValue, thresholdISO);
            ret = MFALSE;
            goto lbEXIT;
        }
    }
    //
    {
        // 4. check exposure time
        MINT32 thresholdExpTime = mthresholdExpTime;
        if (setting.expTime > thresholdExpTime) {
            MY_LOGD(openId, "exposure time(%d) too long (%d)", setting.expTime, thresholdExpTime);
            ret = MFALSE;
        }
    }
    //
lbEXIT:
    if (openId == DUALZOOM_WIDE_CAM_ID) {
        mFovOnlineStatus = ret ? (mFovOnlineStatus |DUALCAM_FOV_ONLINE_WIDE) : (mFovOnlineStatus & ~DUALCAM_FOV_ONLINE_WIDE);
    }
    MY_LOGD(openId, "mFovOnlineStatus(%d)", mFovOnlineStatus);
    if (mFovOnlineStatus & DUALCAM_FOV_ONLINE_WIDE) {
        return MTRUE;
    } else {
        return MFALSE;
    }
}

/******************************************************************************
 *
 ******************************************************************************/
status_t DualCamZoomPolicy::changeCameraMode(
    MINT32  const               openId,
    MINT32                      type,
    MBOOL                       enableAAASync,
    MBOOL                       enableHWSync,
    MINT32                      minFps,
    MINT32                      maxFps,
    IMetadata*                  appMetadata,
    IMetadata*                  halMetadata)
{
    Frame3ASetting_t setting =
    {
        .openId   = openId,
        .is3ASyncEnabled = enableAAASync,
        .hwSyncMode = (MUINT32)enableHWSync,
        .minFps          = minFps,
        .maxFps          = maxFps,
    };
    if (mIFrame3AControl != NULL)
    {
        mIFrame3AControl->set(&setting, type, appMetadata, halMetadata);
    }

    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
DualCamZoomPolicy::
lock(
    void* arg1
)
{
    Mutex::Autolock _l(mLock);
    mLockStatus = true;
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
DualCamZoomPolicy::
unlock(
    void* arg1
)
{
    Mutex::Autolock _l(mLock);
    mLockStatus = false;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t DualCamZoomPolicy::setMasterCamera(
    MINT32  const               openId,
    MINT32  const               slaveId,
    IMetadata*                  halMetadata)
{
    Frame3ASetting_t setting =
    {
        .openId   = openId,
        .masterId = openId,
        .slaveId  = slaveId,
    };
    MINT32 type = F3A_TYPE_MASTER_SLAVE_SET;

    if (mIFrame3AControl != NULL)
    {
        mIFrame3AControl->set(&setting, type, NULL, halMetadata);
    }

    if (openId != mPrevCamID)
    {
        MY_LOGD(openId, "switch main cam : %d -> %d", mPrevCamID, openId);
        mPrevCamID = openId;
        if (mMgrCb != NULL)
            mMgrCb->onEvent(mPrevCamID, MTK_SYNCMGR_MSG_NOTIFY_MASTER_ID, 0, NULL);
    }
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MINT32 DualCamZoomPolicy::selectPrevCamBy3A(
    MINT32  const                              openId,
    MINT32  const                              swtichTele,
    MTK_SYNC_CAEMRA_STATE  const               wideState,
    MTK_SYNC_CAEMRA_STATE  const               teleState)
{
    Frame3ASetting_t setting =
    {
        .openId   = openId
    };

    if (mSkipAAACheck)
        return 0;

    if (openId == DUALZOOM_WIDE_CAM_ID)
    {
        if (mIFrame3AControl != NULL)
        {
            mIFrame3AControl->get(&setting, F3A_TYPE_3A_INFO);
        }

        if(setting.AFDAC != mAFPrevDAC)
        {
            setting.isAFDone = 0;
        }

        if (setting.isoValue > DUALZOOM_AE_ISO_HIGH_THRESHOLD)
        {
            mAEHighCount++;
        }
        else if (setting.isoValue < DUALZOOM_AE_ISO_LOW_THRESHOLD)
        {
            mAEHighCount = 0;
        }
        if (setting.isAFDone)
        {
            if (setting.AFDAC > mAFDACHighTh)
            {
                mAFLowCount++;
            }
            else if (setting.AFDAC < mAFDACLowTh)
            {
                mAFLowCount = 0;
            }
        }
        if (mAEHighCount > 3 || mAFLowCount > 3)
        {
            mAAAForceCamId = openId;
        }
        else
        {
            mAAAForceCamId = -1;
        }
        if (mAETeleLv != -1 && (setting.AELv_x10 - mAETeleLv) > DUALZOOM_AE_LV_DIFFERENCE)
        {
            mAAAForceCamId = openId;
            mWaitStable = DUALZOOM_WAIT_STABLE_COUNT;
        }
        mAFPrevDAC = setting.AFDAC;
        MY_LOGD(openId, "AF DAC TH : %d/%d", mAFDACLowTh, mAFDACHighTh);
        MY_LOGD(openId, "iso : %d, AFDAC : %d, AF Done : %d", setting.isoValue, setting.AFDAC, setting.isAFDone);
        MY_LOGD(openId, "mAEHighCount/mAFLowCount  : %d/%d, mAAAForceCamId : %d", mAEHighCount, mAFLowCount, mAAAForceCamId);
        MY_LOGD(openId, "wide AE LV : %d", setting.AELv_x10);
    }
    else
    {
        #if (DUALZOOM_WIDE_STANDY_EN == 1)
        MINT32 WakeupWide = 0;
        if (mIFrame3AControl != NULL)
        {
            mIFrame3AControl->get(&setting, F3A_TYPE_3A_INFO);
        }
        // check tele iso if wide is in standby mode
        if (mWideState == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_STANDBY
            || mWideState == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_STANDBY)
        {
            if (setting.isoValue > mTeleHighISO)
            {
                mTeleAEHighCount++;
            }
            else if (setting.isoValue < DUALZOOM_AE_ISO_LOW_THRESHOLD)
            {
                mTeleAEHighCount = 0;
            }
            if (mTeleAEHighCount > 3)
            {
                WakeupWide = 1;
            }
        }
        else
        {
            mTeleAEHighCount = 0;
        }
        MY_LOGD(openId, "setting.iso:%d, mTeleAEHighCount: %d", setting.isoValue, mTeleAEHighCount);

        if (mAFStrategy == 0)
        {
            if (mAFTeleState == true && setting.isAFDone == false)
            {
                WakeupWide = 1;
            }
        }
        if (mAFStrategy == 1)
        {
            if (mAFTeleState == false && setting.isAFDone == true &&
                setting.AFDAC > mAFTeleDac)
            {
                WakeupWide = 1;
            }
        }
        MY_LOGD(openId, "wakeup wide debug info: mAFStrategy(%d), mAFTeleDac(%d), mAFTeleState(%d), current AF(%d), current DAC(%d)",
            mAFStrategy, mAFTeleDac, mAFTeleState, setting.isAFDone, setting.AFDAC);
        mAFTeleState = setting.isAFDone;
        mAETeleLv = setting.AELv_x10;

        MY_LOGD(openId, "tele AE LV : %d", setting.AELv_x10);

        if (WakeupWide && wideState == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_STANDBY)
        {
            mWaitStable = DUALZOOM_WAIT_STABLE_COUNT;
        }
        #endif
        if (mAAAForceCamId == -1 && swtichTele &&
            (teleState == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_LOWFPS || teleState == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_STANDBY))
        {
            if (mWaitStable == 0)
            {
                mWaitStable = DUALZOOM_WAIT_STABLE_COUNT;
            }
        }
    }

    return 0;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t DualCamZoomPolicy::setDropReqAndStandby(
    MINT32  const               openId,
    MINT32  const               drop,
    MINT32                      &setStatus,
    IMetadata*                  halMetadata)
{
    MINT32 dropReq = drop;
    if (mForceState & DUALCAM_FORCE_NO_DROP) {
        MY_LOGD_IF(mLogLevel > 2, openId, "force no dropReq");
        dropReq = 0;
    }
    setDropReq(openId, dropReq, setStatus, halMetadata);
    if (!(mForceState & DUALCAM_FORCE_ACTIVE)) {
        updateStandbyMode(openId, setStatus, halMetadata);
        // for dynamic twin
        updateTwinMode(openId, halMetadata);
        //
    }
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t DualCamZoomPolicy::setDropReq(
    MINT32  const               openId,
    MINT32  const               drop,
    MINT32  const               setStatus,
    IMetadata*                  halMetadata)

{
    Frame3ASetting_t setting =
    {
        .openId   = openId,
        .dropMode = MTK_DUALZOOM_DROP_NONE,
    };
    MTK_SYNC_CAEMRA_STATE state = (openId == DUALZOOM_WIDE_CAM_ID)? mWideState : mTeleState;
    if (drop)
    {
        if (setStatus == MTK_P1_SENSOR_STATUS_HW_STANDBY)
        {
            setting.dropMode = MTK_DUALZOOM_DROP_NEED_P1;
            //setting.dropMode = MTK_DUALZOOM_DROP_DIRECTLY;
            MY_LOGD(openId, "set standby, drop to p1");
        }
        else if (setStatus == MTK_P1_SENSOR_STATUS_STREAMING)
        {
            setting.dropMode = MTK_DUALZOOM_DROP_NEED_P1;
            //setting.dropMode = MTK_DUALZOOM_DROP_DIRECTLY;
            MY_LOGD(openId, "set STREAMING, drop to p1");
        }
        else if (openId == mSlaveInfo.Id && mSlaveInfo.IsStandby)
        {
            setting.dropMode = MTK_DUALZOOM_DROP_DIRECTLY;
            //setting.dropMode = MTK_DUALZOOM_DROP_DIRECTLY;
            MY_LOGD(openId, "standby mode, set drop directly");
        }
        else
        {
            setting.dropMode = MTK_DUALZOOM_DROP_NEED_P1;
            MY_LOGD(openId, "not in standby mode, drop to p1");
        }
        if (mIFrame3AControl == NULL)
        {
            IMetadata::IEntry tag_drop(MTK_DUALZOOM_DROP_REQ);
            tag_drop.push_back(MTK_DUALZOOM_DROP_DIRECTLY, Type2Type<MINT32>());
            halMetadata->update(MTK_DUALZOOM_DROP_REQ, tag_drop);
            MY_LOGD(openId, "no frame3Acontroller, drop directly");
        }
        else
        {
            mIFrame3AControl->set(&setting, F3A_TYPE_DROP_MODE, NULL, halMetadata);
        }
    }
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t DualCamZoomPolicy::updateStandbyMode(
    MINT32  const               openId,
    MINT32                      &setStatus,
    IMetadata*                  halMetadata)
{
    if (setStatus == MTK_P1_SENSOR_STATUS_HW_STANDBY)
    {
        {
            IMetadata::IEntry tag(MTK_P1NODE_SENSOR_STATUS);
            tag.push_back(MTK_P1_SENSOR_STATUS_HW_STANDBY, Type2Type<MINT32>());
            halMetadata->update(MTK_P1NODE_SENSOR_STATUS, tag);
        }

        setStatus = MTK_P1_SENSOR_STATUS_NONE;
        mSlaveInfo.Id = openId;
        mSlaveInfo.IsStandby = true;
        MY_LOGD(openId, "set streaming off");
    }
    else if (setStatus == MTK_P1_SENSOR_STATUS_STREAMING)
    {
        {
            IMetadata::IEntry tag(MTK_P1NODE_SENSOR_STATUS);
            tag.push_back(MTK_P1_SENSOR_STATUS_STREAMING, Type2Type<MINT32>());
            halMetadata->update(MTK_P1NODE_SENSOR_STATUS, tag);
        }

        setStatus = MTK_P1_SENSOR_STATUS_NONE;
        mSlaveInfo.Id = openId;
        mSlaveInfo.IsStandby = false;
        MY_LOGD(openId, "set streaming on");
    }
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t DualCamZoomPolicy::updateTwinMode(
    MINT32  const               openId,
    IMetadata*                  halMetadata)
{
    MTK_SYNC_CAEMRA_STATE MainState = (openId == DUALZOOM_WIDE_CAM_ID) ? mWideState : mTeleState;
    MTK_SYNC_CAEMRA_STATE SlaveState = (openId == DUALZOOM_WIDE_CAM_ID) ? mTeleState : mWideState;
    if (MainState == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE &&
        SlaveState == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_STANDBY &&
        mWaitEnterLP <= 3 &&
        mTGNum == MTK_P1_TWIN_STATUS_TG_MODE_2)
    {
        if (openId == DUALZOOM_WIDE_CAM_ID)
        {
            {
                IMetadata::IEntry tag(MTK_P1NODE_TWIN_SWITCH);
                tag.push_back(MTK_P1_TWIN_SWITCH_ONE_TG, Type2Type<MINT32>());
                halMetadata->update(MTK_P1NODE_TWIN_SWITCH, tag);
            }

            MY_LOGD(openId, "set camera to TG mode = %d since camera sleep", mTGNum);
        }
    }
    else if (MainState == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE &&
        SlaveState == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_ACTIVE &&
        mWaitEnterAct != 0 &&
        mTGNum == MTK_P1_TWIN_STATUS_TG_MODE_1)
    {
        {
            IMetadata::IEntry tag(MTK_P1NODE_TWIN_SWITCH);
            tag.push_back(MTK_P1_TWIN_SWITCH_TWO_TG, Type2Type<MINT32>());
            halMetadata->update(MTK_P1NODE_TWIN_SWITCH, tag);
        }

        MY_LOGD(openId, "set camera to TG mode = %d since camera avtive", mTGNum);
    }
    return OK;
}



/******************************************************************************
 *
 ******************************************************************************/
void DualCamZoomPolicy::changeCameraState(
    MINT32  const               openId,
    AAAInfo                     info,
    MINT32&                     dropReq,
    IMetadata*                  appMetadata,
    IMetadata*                  halMetadata,
    MTK_SYNC_CAEMRA_STATE&      state)
{
    if (mForceState & DUALCAM_FORCE_ACTIVE)
    {
        MY_LOGD(openId, "force keep in active state");
        return;
    }
    else if (state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_LOWFPS || state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_STANDBY)
    {
        MY_LOGE_IF(!dropReq, openId, "This state need drop req!!! please check 1");
        if (mWaitStable > 0 && state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_LOWFPS)
        {
            MY_LOGD(openId, "go to low power to active done");
            // wide cam should be able to switch back to active no matter what state it is.
            changeCameraMode(openId, F3A_TYPE_FRAMERATE_CHANGE, false, false,
                    0, 0, appMetadata, halMetadata);
            state = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE;
            mWaitStable = DUALZOOM_WAIT_STABLE_COUNT;
            mWaitEnterLP = 0;
            mWaitEnterAct = 0;
            dropReq = false;
        }
        else if (mWaitEnterLP <= 0)
        {
            MY_LOGD(openId, "low power done");
            if (state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_LOWFPS)
            {
                changeCameraMode(openId, F3A_TYPE_FRAMERATE_CHANGE, false, false,
                        15, 15, appMetadata, halMetadata);
                state = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_LOWFPS;
            }
            else if (state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_STANDBY)
            {
                if (openId == DUALZOOM_TELE_CAM_ID && mTGNum == MTK_P1_TWIN_STATUS_TG_MODE_2)
                {
                    MY_LOGD(openId, "TG mode is still two TG mode, keep state in MTK_SYNC_CAEMRA_GOTO_STANDBY");
                }
                else
                {
                    state = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_STANDBY;
                }
            }
        }
        else
        {
            mWaitEnterLP--;
            if (mWaitEnterLP == (DUALZOOM_WAIT_LOW_POWER_COUNT - 2) && state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_STANDBY)
            {
                // tele delay into low power mode for disable 3A sync
                //changeCameraMode(openId, F3A_TYPE_FRAMERATE_CHANGE, false, false, 15, 15, appMetadata, halMetadata);
                mSetSensorStatus = MTK_P1_SENSOR_STATUS_HW_STANDBY;
                mSet3ASyncMode = MTK_DUAL_CAM_AAA_SYNC_STATE::DISABLE;
            }
        }
    }
    else if (state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_LOWFPS ||
             state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_STANDBY)
    {
        MY_LOGE_IF(!dropReq, openId, "This state need drop req!!! please check 2");
        if (mWaitStable > 0 || (info.AAAJudgeId != -1 && info.AAAJudgeId == openId)
                            || (info.is4KVideo && openId == DUALZOOM_WIDE_CAM_ID))
        {
            if (state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_LOWFPS)
            {
                changeCameraMode(openId, F3A_TYPE_FRAMERATE_CHANGE, false, false,
                        0, 0, appMetadata, halMetadata);
            }
            else
            {
                #if 0
                //changeCameraMode(openId, F3A_TYPE_FRAMERATE_CHANGE, false, false, 15, 30, appMetadata, halMetadata);
                mSetSensorStatus = MTK_P1_SENSOR_STATUS_STREAMING;
                // tele leave low power mode, enable wide 3A sync
                mSet3ASyncMode = MTK_DUAL_CAM_AAA_SYNC_STATE::ENABLE;

                {
                    NS3Av3::ISync3AMgr::getInstance()->getSync3A()->syncAEInit((openId == mOpenIds[0]) ? mOpenIds[1] : mOpenIds[0], openId);
                }

                setMasterCamera((openId == mOpenIds[0]) ? mOpenIds[1] : mOpenIds[0], openId, halMetadata);
                #endif

            }
            if (state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_LOWFPS)
            {
                mWaitEnterAct = DUALZOOM_WAIT_CAM_LOWFPS_TO_ACT;
            }
            else
            {
                mWaitEnterAct = DUALZOOM_WAIT_CAM_STANDBY_TO_ACT;
            }
            if (state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_LOWFPS)
            {
                // set wide to active
                MY_LOGD(openId, "low power to active done");
                state = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE;
                dropReq = false;
            }
            else
            {
                MY_LOGD(openId, "go to active");
                state = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_ACTIVE;
            }
            if (mWaitStable == 0)
            {
                mWaitStable = DUALZOOM_WAIT_STABLE_COUNT;
            }
        }
    }
    else if (state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_ACTIVE)
    {
        MY_LOGE_IF(!dropReq, openId, "This state need drop req!!! please check 3");
        if (mWaitEnterAct <= 0)
        {
            MY_LOGD(openId, "active done");
            state = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE;
            //dropReq = false;
        }
        else
        {
            mWaitEnterAct--;
            if (mWaitEnterAct == (DUALZOOM_WAIT_CAM_STANDBY_TO_ACT - 4))
            {
                if (mTGNum == MTK_P1_TWIN_SWITCH_TWO_TG)
                {
                    //changeCameraMode(openId, F3A_TYPE_FRAMERATE_CHANGE, false, false, 15, 30, appMetadata, halMetadata);
                    MY_LOGD(openId, "wakeup sensor and set AE sync");
                    mSetSensorStatus = MTK_P1_SENSOR_STATUS_STREAMING;
                    // tele leave low power mode, enable wide 3A sync
                    mSet3ASyncMode = MTK_DUAL_CAM_AAA_SYNC_STATE::ENABLE;

                    {
                        NS3Av3::ISync3AMgr::getInstance()->getSync3A()->syncAEInit((openId == mOpenIds[0]) ? mOpenIds[1] : mOpenIds[0], openId);
                    }

                    setMasterCamera((openId == mOpenIds[0]) ? mOpenIds[1] : mOpenIds[0], openId, halMetadata);
                }
                else
                {
                    MY_LOGW(openId, "Want to wakeup camera but twin is not switch yet, wait twin switch");
                    mWaitEnterAct++;
                }
            }
        }
    }

    if ((info.ZoomDrop || (info.AAAJudgeId != -1 && info.AAAJudgeId != openId))
            && dropReq) // need modify magic number
    {
        //usleep(30 * 1000);
        if (state == MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_ACTIVE)
        {
            if (mWaitStable > 0)
            {
                mWaitStable--;
            }
            if (mWaitStable == 0)
            {
                MY_LOGD(openId, "go to low power");
                if (openId == DUALZOOM_WIDE_CAM_ID)
                {
                    #if (DUALZOOM_WIDE_STANDY_EN == 1)
                    state = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_STANDBY;
                    #else
                    state = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_LOWFPS;
                    #endif
                }
                else
                {
                    // tele delay into low power mode, only notify wide disable 3A sync
                    //mSet3ASyncMode = MTK_DUAL_CAM_AAA_SYNC_STATE::DISABLE;
                    state = MTK_SYNC_CAEMRA_STATE::MTK_SYNC_CAEMRA_GOTO_STANDBY;
                }
                mWaitEnterLP = DUALZOOM_WAIT_LOW_POWER_COUNT;
            }
        }
    }

    if (mSet3ASyncMode != MTK_DUAL_CAM_AAA_SYNC_STATE::NOT_SET)
    {
        changeCameraMode(openId, F3A_TYPE_3A_SYNC, mSet3ASyncMode == MTK_DUAL_CAM_AAA_SYNC_STATE::ENABLE, false, 0, 0,
                         appMetadata, halMetadata);
        mSet3ASyncMode = MTK_DUAL_CAM_AAA_SYNC_STATE::NOT_SET;
    }
}



} // NSCam


