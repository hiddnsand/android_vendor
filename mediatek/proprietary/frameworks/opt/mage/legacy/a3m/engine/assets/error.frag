/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Shader error fragment shader.
 *
 */

precision mediump float;

uniform sampler2D u_m_errorTexture;

void main()
{
  vec2 uv = gl_FragCoord.xy / 16.0;
  uv.y *= -1.0;
  gl_FragColor = texture2D(u_m_errorTexture, uv);
}
