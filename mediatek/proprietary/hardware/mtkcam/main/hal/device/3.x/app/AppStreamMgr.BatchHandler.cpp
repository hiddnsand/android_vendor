/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "AppStreamMgr.h"
#include "MyUtils.h"

using namespace android;
using namespace NSCam;
using namespace NSCam::v3;

#define ThisNamespace   AppStreamMgr::BatchHandler

/******************************************************************************
 *
 ******************************************************************************/
#define MY_DEBUG(level, fmt, arg...) \
    do { \
        CAM_LOG##level("[%s::%s] " fmt, mInstanceName.c_str(), __FUNCTION__, ##arg); \
        mCommonInfo->mDebugPrinter->printFormatLine(#level" [%s::%s] " fmt, mInstanceName.c_str(), __FUNCTION__, ##arg); \
    } while(0)

#define MY_WARN(level, fmt, arg...) \
    do { \
        CAM_LOG##level("[%s::%s] " fmt, mInstanceName.c_str(), __FUNCTION__, ##arg); \
        mCommonInfo->mWarningPrinter->printFormatLine(#level" [%s::%s] " fmt, mInstanceName.c_str(), __FUNCTION__, ##arg); \
    } while(0)

#define MY_ERROR(level, fmt, arg...) \
    do { \
        CAM_LOG##level("[%s::%s] " fmt, mInstanceName.c_str(), __FUNCTION__, ##arg); \
        mCommonInfo->mErrorPrinter->printFormatLine(#level" [%s::%s] " fmt, mInstanceName.c_str(), __FUNCTION__, ##arg); \
    } while(0)

#define MY_LOGV(...)                MY_DEBUG(V, __VA_ARGS__)
#define MY_LOGD(...)                MY_DEBUG(D, __VA_ARGS__)
#define MY_LOGI(...)                MY_DEBUG(I, __VA_ARGS__)
#define MY_LOGW(...)                MY_WARN (W, __VA_ARGS__)
#define MY_LOGE(...)                MY_ERROR(E, __VA_ARGS__)
#define MY_LOGA(...)                MY_ERROR(A, __VA_ARGS__)
#define MY_LOGF(...)                MY_ERROR(F, __VA_ARGS__)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)


/******************************************************************************
 *
 ******************************************************************************/
ThisNamespace::
BatchHandler(
    std::shared_ptr<CommonInfo> pCommonInfo,
    android::sp<CallbackHandler> pCallbackHandler
)
    : mInstanceName{std::to_string(pCommonInfo->mInstanceId) + ":BatchHandler"}
    , mCommonInfo(pCommonInfo)
    , mCallbackHandler(pCallbackHandler)
    , mBatchNumber(-1)
{
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
resetBatchStreamId() -> void
{
    Mutex::Autolock _l(mLock);
    mBatchedStreams.clear();
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
checkStreamUsageforBatchMode(const android::sp<AppImageStreamInfo> pStreamInfo) -> bool
{
    //check if the stream usage include video_ENCODER
    if ( (pStreamInfo->getStreamType() == static_cast<MUINT32>(StreamType::OUTPUT)) &&
        (pStreamInfo->getUsageForConsumer() & GRALLOC_USAGE_HW_VIDEO_ENCODER) ){
        mBatchedStreams.push_back(pStreamInfo->getStreamId());
        MY_LOGD("Stream %u is registered to Batch Stream",pStreamInfo->getStreamId());
        return true;
    }else return false;

}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
registerBatch(const android::Vector<Request> requests) -> int
{
    mBatchNumber++;
    auto batch = std::make_shared<InflightBatch>();
    batch->mFirstFrame = requests[0].frameNo;
    batch->mBatchSize = requests.size();
    batch->mLastFrame = batch->mFirstFrame + batch->mBatchSize - 1;
    batch->FrameRemoved.assign(batch->mBatchSize,false);
    batch->FrameHasLastPartial.clear();
    batch->ShutterReturned.clear();
    batch->mRemoved=false;
    Mutex::Autolock _l(mLock);
    mInflightBatches.push_back(batch);
    MY_LOGD("Batch %d is registered. FrameNo: %d - %d",mBatchNumber,
        batch->mFirstFrame,batch->mLastFrame);
    return mBatchNumber;
}
auto
ThisNamespace::
removeBatchlock(uint32_t batchNum) -> void{
    Mutex::Autolock _l(mLock);
    MY_LOGD("batch %d is removed",batchNum);
    mInflightBatches.erase(mInflightBatches.begin()+batchNum);
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
getBatch(uint32_t frameNumber) ->
std::pair<int, std::shared_ptr<AppStreamMgr::BatchHandler::InflightBatch>>
{
    Mutex::Autolock _l(mLock);
    int numBatches = mInflightBatches.size();
    if (numBatches == 0) {
        return std::make_pair(NOT_BATCHED, nullptr);
    }
    uint32_t frameMin = mInflightBatches[0]->mFirstFrame;
    uint32_t frameMax = mInflightBatches[numBatches - 1]->mLastFrame;
    if (frameNumber < frameMin || frameNumber > frameMax) {
        return std::make_pair(NOT_BATCHED, nullptr);
    }
    for (int i = 0; i < numBatches; i++) {
        if (frameNumber >= mInflightBatches[i]->mFirstFrame &&
                frameNumber <= mInflightBatches[i]->mLastFrame) {
            return std::make_pair(i, mInflightBatches[i]);
        }
    }
    return std::make_pair(NOT_BATCHED, nullptr);
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
destroy() -> void
{
    mCallbackHandler = nullptr;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
dumpState(android::Printer& printer, const std::vector<std::string>& /*options*/) -> void
{
    Mutex::Autolock _lcb(mcbWaitBatch);
    dumpStateLocked(printer);
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
dumpStateLocked(android::Printer& printer) const -> void
{
    for(CallbackParcel cb : cbWaitBatch){
        android::String8 str;
        str += android::String8::format("CallbackParcel: frameNo(%u) valid(%d) shutter(%lld)",cb.frameNo, cb.valid, cb.timestampShutter);
        str += "vInputImage: ";
        for(ssize_t i=0; i<cb.vInputImageItem.size(); i++ ) {
            str += android::String8::format("(%zu:%p:%p) ", i, cb.vInputImageItem[i].buffer.get(), cb.vInputImageItem[i].stream.get());
        }

        str += "; vOutputImage: ";
        for(ssize_t i=0; i<cb.vOutputImageItem.size(); i++ ) {
        str += android::String8::format("(%zu:%p:%p) ", i, cb.vOutputImageItem[i].buffer.get(), cb.vOutputImageItem[i].stream.get());
        }
        str += "; vOutputMeta: ";
        for(ssize_t i=0; i<cb.vOutputMetaItem.size(); i++ ) {
        str += android::String8::format("(%zu:%p:%u) ", i, cb.vOutputMetaItem[i].buffer.get(), cb.vOutputMetaItem[i].bufferNo);
        }
        str += "; vError: ";
        for(ssize_t i=0; i<cb.vError.size(); i++ ) {
        str += android::String8::format("(%zu:%p:%d) ", i, cb.vError[i].stream.get(), cb.vError[i].errorCode);
        }
        str += "; Shutter: ";
        if(cb.shutter.get())str += android::String8::format("(%p:%lld) ", cb.shutter.get(), cb.shutter->timestamp);
        printer.printLine(str.string());

    }
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
waitUntilDrained(nsecs_t const timeout __unused) -> int
{
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
push(std::list<CallbackParcel>& item) -> void
{
    std::list<CallbackParcel> updatedcbList;
    updateCallback(item,updatedcbList);
    mCallbackHandler->push(updatedcbList);

}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
updateBatchStatus(uint32_t frameNo) -> void
{
    auto batchPair = getBatch(frameNo);
    if( batchPair.first != NOT_BATCHED ){
        if(batchPair.second->FrameRemoved.size()>0){
            batchPair.second->
                FrameRemoved[frameNo - batchPair.second->mFirstFrame] = true;
            batchPair.second->mRemoved = true;
            for(bool flag : batchPair.second->FrameRemoved){
                if(!flag) batchPair.second->mRemoved = false;
            }
        }
    }
}

/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
copyCbMetaItem(CallbackParcel SrcParcel,CallbackParcel &DstParcel) -> void
{
    if(SrcParcel.vOutputMetaItem.size()>0){
    Vector<CallbackParcel::MetaItem>* pvMetaCbItem = &DstParcel.vOutputMetaItem;
        for(CallbackParcel::MetaItem MI : SrcParcel.vOutputMetaItem){
            CallbackParcel::MetaItem& rCbItem = pvMetaCbItem->editItemAt(pvMetaCbItem->add());
            rCbItem.buffer = MI.buffer;
            rCbItem.bufferNo = MI.bufferNo;
        }
    }
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
copyCbImageItem(CallbackParcel SrcParcel,CallbackParcel &DstParcel,bool input) -> void
{
    if(SrcParcel.vOutputImageItem.size()>0){
        Vector<CallbackParcel::ImageItem>* pvImageCbItem = &DstParcel.vOutputImageItem;
        for(CallbackParcel::ImageItem II : SrcParcel.vOutputImageItem){
            CallbackParcel::ImageItem& rCbItem = pvImageCbItem->editItemAt(pvImageCbItem->add());
            rCbItem.buffer = II.buffer;
            rCbItem.stream = II.stream;
        }
    }
    if(input && SrcParcel.vInputImageItem.size()>0){
        Vector<CallbackParcel::ImageItem>* pvImageCbItem = &DstParcel.vInputImageItem;
        for(CallbackParcel::ImageItem II : SrcParcel.vInputImageItem){
            CallbackParcel::ImageItem& rCbItem = pvImageCbItem->editItemAt(pvImageCbItem->add());
            rCbItem.buffer = II.buffer;
            rCbItem.stream = II.stream;
        }
    }
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
copyCbErrorItem(CallbackParcel SrcParcel,CallbackParcel &DstParcel) -> void
{
    if(SrcParcel.vError.size() >0){
        Vector<CallbackParcel::Error>* pvError = &DstParcel.vError;
        for(CallbackParcel::Error EI : SrcParcel.vError){
            CallbackParcel::Error& rCbItem = pvError->editItemAt(pvError->add());
            rCbItem.stream = EI.stream;
            rCbItem.errorCode =  EI.errorCode;
        }
    }
}

/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
updateCallback(std::list<CallbackParcel> item,std::list<CallbackParcel>& updatedcbList) -> void
{
    Mutex::Autolock _lcb(mcbWaitBatch);
    for(CallbackParcel cbParcel  : item){
        auto batchPair = getBatch(cbParcel.frameNo);
        //bypass result for non-batch frame
        if(batchPair.first == NOT_BATCHED){
            CallbackParcel cb;
            cb.valid = cbParcel.valid;
            cb.frameNo = cbParcel.frameNo;
            cb.timestampShutter = cbParcel.timestampShutter;
            cb.shutter = cbParcel.shutter;

            if(cbParcel.vError.size() > 0){
                copyCbErrorItem(cbParcel,cb);
                copyCbImageItem(cbParcel,cb,false);
            }
            else{
                copyCbMetaItem(cbParcel,cb);
                copyCbImageItem(cbParcel,cb,true);
            }
            updatedcbList.push_back(cb);
            continue;
        }
        bool needAdd = true;
        for(CallbackParcel &cbPending: cbWaitBatch){
            if( cbParcel.frameNo!=cbPending.frameNo ) continue;
            needAdd = false;
            //copy new result to cbWaitBatch: cbParcel ----> cbPending
            if(cbParcel.vError.size() > 0){
                copyCbErrorItem(cbParcel,cbPending);
                copyCbImageItem(cbParcel,cbPending,false);
            }
            else{
                copyCbMetaItem(cbParcel,cbPending);
                copyCbImageItem(cbParcel,cbPending,true);
            }
            break;
        }

        //result from new request comes
        if(needAdd){
            CallbackParcel cb;
            cb.valid = cbParcel.valid;
            cb.frameNo = cbParcel.frameNo;
            cb.timestampShutter = cbParcel.timestampShutter;
            cb.shutter = cbParcel.shutter;

            if(cbParcel.vError.size() > 0){
                copyCbErrorItem(cbParcel,cb);
                copyCbImageItem(cbParcel,cb,false);
            }
            else{
                copyCbMetaItem(cbParcel,cb);
                copyCbImageItem(cbParcel,cb,true);
            }
            cbWaitBatch.push_back(cb);
        }
    }

    bool anyCb = false;
    int currentBatch = -1;
    std::list<CallbackParcel>::iterator itCb = cbWaitBatch.begin();
    //Check status of pending results for each frame.
    KeyedVector<int,BatchCbInfo> PendingCbInfoMap;
    while(itCb !=  cbWaitBatch.end()){
        CallbackParcel& pCb = *itCb;
        auto pair = getBatch(pCb.frameNo);
        if(pair.first != currentBatch){
            BatchCbInfo cb_info;
            cb_info.batchNumber = pair.first;
            cb_info.outMetaResultCount=0;
            cb_info.inImageResultCount=0;
            cb_info.outImageResultCount=0;
            PendingCbInfoMap.add(pair.first,cb_info);
            currentBatch = pair.first;
        }
        BatchCbInfo& pcb_Info = PendingCbInfoMap.editValueFor(currentBatch);

        //Metadata
        if( pCb.vOutputMetaItem.size() > 0 ){
            pcb_Info.outMetaResultCount++;
            for(CallbackParcel::MetaItem MI : pCb.vOutputMetaItem ){
                if( (MI.bufferNo == mCommonInfo->mAtMostMetaStreamCount) &&
                    (std::find(pair.second->FrameHasLastPartial.begin(),
                        pair.second->FrameHasLastPartial.end(),pCb.frameNo)
                            == pair.second->FrameHasLastPartial.end())
                  ){
                    pair.second->FrameHasLastPartial.push_back(pCb.frameNo);
                }
            }
        }
        //InputImage
        for(CallbackParcel::ImageItem II : pCb.vInputImageItem){
            if( std::find(mBatchedStreams.begin(),mBatchedStreams.end(),II.stream->getStreamId()) != mBatchedStreams.end() )
                pcb_Info.inImageResultCount++;
        }
        //OutputImage
        for(CallbackParcel::ImageItem II : pCb.vOutputImageItem){
            if( std::find(mBatchedStreams.begin(),mBatchedStreams.end(),II.stream->getStreamId()) != mBatchedStreams.end() )
                pcb_Info.outImageResultCount++;
        }
        itCb++;
    }


    itCb = cbWaitBatch.begin();
    while(itCb !=  cbWaitBatch.end()){
        anyCb = false;//Check there is any callback for this frame
        CallbackParcel& pCb = *itCb;
        auto pair = getBatch(pCb.frameNo);
        BatchCbInfo& pcb_Info = PendingCbInfoMap.editValueFor(pair.first);
        //OutMeta
        if(pCb.vOutputMetaItem.size()> 0){
            //All the last partial has arrived, send all the remaining meta
            if(pair.second->FrameHasLastPartial.size() == pair.second->mBatchSize){
                android::Vector<CallbackParcel::MetaItem>::iterator itOutputMeta = pCb.vOutputMetaItem.begin();
                CallbackParcel cbtemp;
                cbtemp.valid = pCb.valid;
                cbtemp.frameNo = pCb.frameNo;
                if((std::find(pair.second->ShutterReturned.begin(),
                        pair.second->ShutterReturned.end(),pCb.frameNo)
                            == pair.second->ShutterReturned.end())
                   ){
                    cbtemp.timestampShutter = pCb.timestampShutter;
                    cbtemp.shutter = pCb.shutter;
                }
                while( itOutputMeta != pCb.vOutputMetaItem.end() ){
                    CallbackParcel::MetaItem MI = *itOutputMeta;
                    Vector<CallbackParcel::MetaItem>* pvMetaCbItem = &cbtemp.vOutputMetaItem;
                    CallbackParcel::MetaItem& rCbItem = pvMetaCbItem->editItemAt(pvMetaCbItem->add());
                    rCbItem.buffer = MI.buffer;
                    rCbItem.bufferNo = MI.bufferNo;
                    itOutputMeta = pCb.vOutputMetaItem.erase(itOutputMeta);
                }
                updatedcbList.push_back(cbtemp);
                anyCb = true;
            } //metas from the same batch all arrived
            else if(pcb_Info.outMetaResultCount == pair.second->mBatchSize){
                //only copy one
                CallbackParcel cbtemp;
                cbtemp.valid = pCb.valid;
                cbtemp.frameNo = pCb.frameNo;
                cbtemp.timestampShutter = pCb.timestampShutter;
                cbtemp.shutter = pCb.shutter;

                if( (cbtemp.shutter != 0 ) &&
                    (std::find(pair.second->ShutterReturned.begin(),
                        pair.second->ShutterReturned.end(),pCb.frameNo)
                            == pair.second->ShutterReturned.end())
                  ){
                    pair.second->ShutterReturned.push_back(pCb.frameNo);
                }

                Vector<CallbackParcel::MetaItem>* pvMetaCbItem = &cbtemp.vOutputMetaItem;
                CallbackParcel::MetaItem& rCbItem = pvMetaCbItem->editItemAt(pvMetaCbItem->add());
                rCbItem.buffer = pCb.vOutputMetaItem.begin()->buffer;
                rCbItem.bufferNo = pCb.vOutputMetaItem.begin()->bufferNo;
                updatedcbList.push_back(cbtemp);
                pCb.vOutputMetaItem.erase(pCb.vOutputMetaItem.begin());
                anyCb = true;
            }
        }
        //InImage
        android::Vector<CallbackParcel::ImageItem>::iterator itInputImage = pCb.vInputImageItem.begin();
        while( itInputImage != pCb.vInputImageItem.end() ){
             bool inputImageCb=false;
            CallbackParcel::ImageItem II = *itInputImage;
            //the streamId isn't regitered
            if( std::find(mBatchedStreams.begin(),mBatchedStreams.end(),II.stream->getStreamId()) == mBatchedStreams.end()){
                if(anyCb){
                    Vector<CallbackParcel::ImageItem>* pvImageCbItem = &updatedcbList.back().vInputImageItem;
                    CallbackParcel::ImageItem& rCbItem = pvImageCbItem->editItemAt(pvImageCbItem->add());
                    rCbItem.buffer = II.buffer;
                    rCbItem.stream = II.stream;
                    inputImageCb = true;
                }else{
                    CallbackParcel cbtemp;
                    cbtemp.valid = pCb.valid;
                    cbtemp.frameNo = pCb.frameNo;
                    //cbtemp.timestampShutter = pCb.timestampShutter;
                    //cbtemp.shutter = pCb.shutter;

                    Vector<CallbackParcel::ImageItem>* pvImageCbItem = &cbtemp.vInputImageItem;
                    CallbackParcel::ImageItem& rCbItem = pvImageCbItem->editItemAt(pvImageCbItem->add());
                    rCbItem.buffer = II.buffer;
                    rCbItem.stream = II.stream;
                    updatedcbList.push_back(cbtemp);
                    inputImageCb = true;
                    anyCb = true;
                }

            }else if(pcb_Info.inImageResultCount >= pair.second->mBatchSize){
                if(anyCb){
                    Vector<CallbackParcel::ImageItem>* pvImageCbItem = &updatedcbList.back().vInputImageItem;
                    CallbackParcel::ImageItem& rCbItem = pvImageCbItem->editItemAt(pvImageCbItem->add());
                    rCbItem.buffer = II.buffer;
                    rCbItem.stream = II.stream;
                    inputImageCb = true;
                }else{
                    CallbackParcel cbtemp;
                    cbtemp.valid = pCb.valid;
                    cbtemp.frameNo = pCb.frameNo;
                    //cbtemp.timestampShutter = pCb.timestampShutter;
                    //cbtemp.shutter = pCb.shutter;

                    Vector<CallbackParcel::ImageItem>* pvImageCbItem = &cbtemp.vInputImageItem;
                    CallbackParcel::ImageItem& rCbItem = pvImageCbItem->editItemAt(pvImageCbItem->add());
                    rCbItem.buffer = II.buffer;
                    rCbItem.stream = II.stream;
                    updatedcbList.push_back(cbtemp);
                    inputImageCb = true;
                    anyCb = true;
                }
            }
            if( inputImageCb )itInputImage = pCb.vInputImageItem.erase(itInputImage);
            else itInputImage++;
        }

        //OutImage
        android::Vector<CallbackParcel::ImageItem>::iterator itOutputImage = pCb.vOutputImageItem.begin();
        while( itOutputImage != pCb.vOutputImageItem.end() ){
            CallbackParcel::ImageItem II = *itOutputImage;
            bool outputImageCb=false;
            //the streamId isn't regitered
            if( std::find(mBatchedStreams.begin(),mBatchedStreams.end(),II.stream->getStreamId()) == mBatchedStreams.end()){
                if(anyCb){
                    Vector<CallbackParcel::ImageItem>* pvImageCbItem = &updatedcbList.back().vOutputImageItem;
                    CallbackParcel::ImageItem& rCbItem = pvImageCbItem->editItemAt(pvImageCbItem->add());
                    rCbItem.buffer = II.buffer;
                    rCbItem.stream = II.stream;
                    if(pCb.vError.size() >0 ){
                        copyCbErrorItem(pCb,updatedcbList.back());
                        pCb.vError.clear();
                    }
                    outputImageCb = true;
                }else{
                    CallbackParcel cbtemp;
                    cbtemp.valid = pCb.valid;
                    cbtemp.frameNo = pCb.frameNo;
                    //cbtemp.timestampShutter = pCb.timestampShutter;
                    //cbtemp.shutter = pCb.shutter;

                    Vector<CallbackParcel::ImageItem>* pvImageCbItem = &cbtemp.vOutputImageItem;
                    CallbackParcel::ImageItem& rCbItem = pvImageCbItem->editItemAt(pvImageCbItem->add());
                    rCbItem.buffer = II.buffer;
                    rCbItem.stream = II.stream;
                    if(pCb.vError.size() >0 ){
                        copyCbErrorItem(pCb,cbtemp);
                        pCb.vError.clear();
                    }
                    updatedcbList.push_back(cbtemp);
                    outputImageCb = true;
                    anyCb = true;
                }

            }else if(pcb_Info.outImageResultCount >= pair.second->mBatchSize){
                if(anyCb){
                    Vector<CallbackParcel::ImageItem>* pvImageCbItem = &updatedcbList.back().vOutputImageItem;
                    CallbackParcel::ImageItem& rCbItem = pvImageCbItem->editItemAt(pvImageCbItem->add());
                    rCbItem.buffer = II.buffer;
                    rCbItem.stream = II.stream;
                    if(pCb.vError.size() >0 ){
                        copyCbErrorItem(pCb,updatedcbList.back());
                        pCb.vError.clear();
                    }
                    outputImageCb = true;
                }else{
                    CallbackParcel cbtemp;
                    cbtemp.valid = pCb.valid;
                    cbtemp.frameNo = pCb.frameNo;
                    //cbtemp.timestampShutter = pCb.timestampShutter;
                    //cbtemp.shutter = pCb.shutter;

                    Vector<CallbackParcel::ImageItem>* pvImageCbItem = &cbtemp.vOutputImageItem;
                    CallbackParcel::ImageItem& rCbItem = pvImageCbItem->editItemAt(pvImageCbItem->add());
                    rCbItem.buffer = II.buffer;
                    rCbItem.stream = II.stream;
                    if(pCb.vError.size() >0 ){
                        copyCbErrorItem(pCb,cbtemp);
                        pCb.vError.clear();
                    }
                    updatedcbList.push_back(cbtemp);
                    outputImageCb = true;
                    anyCb = true;
                }
            }
            if( outputImageCb )itOutputImage = pCb.vOutputImageItem.erase(itOutputImage);
            else itOutputImage++;
        }
        if( pCb.vInputImageItem.isEmpty() && pCb.vOutputImageItem.isEmpty() &&
                        pCb.vOutputMetaItem.isEmpty()&& pCb.vError.isEmpty()){
            itCb = cbWaitBatch.erase(itCb);
            if(pair.second->mRemoved)removeBatchlock(pair.first);
        }
        else itCb++;
    }
}