ifeq ($(strip $(MTK_TEE_GP_SUPPORT)), yes)

# =============================================================================
#
# Makefile responsible for:
# - building a test binary - tlctplay
# - building the TLC library - libtlctplayLib.so
#
# =============================================================================

# Do not remove this - Android build needs the definition
LOCAL_PATH	:= $(call my-dir)

# =============================================================================
# library Crypto

include $(CLEAR_VARS)
LOCAL_MODULE := cacrypto_core
#LOCAL_MULTILIB := 32 ### moved obj_arm
LOCAL_MODULE_CLASS = STATIC_LIBRARIES
LOCAL_MODULE_SUFFIX = .a
LOCAL_SRC_FILES := lib/cacrypto_core.a
include $(BUILD_PREBUILT)


# =============================================================================
# binary tlctplay

include $(CLEAR_VARS)

# Module name
LOCAL_MODULE	:= tlctplay

# Add your folders with header files here
LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../../../../../../../trustonic/source/external/mobicore/common/302c/MobiCoreDriverLib/ClientLib/public/GP \
    $(LOCAL_PATH)/inc \
    $(LOCAL_PATH)/../ta/public \
    $(MOBICORE_LIB_PATH)

# Add your source files here
LOCAL_SRC_FILES	+= main.cpp

LOCAL_STATIC_LIBRARIES := cacrypto_core
LOCAL_SHARED_LIBRARIES := libMcClient
LOCAL_SHARED_LIBRARIES += liblog

include $(BUILD_EXECUTABLE)
# =============================================================================

endif # ifeq ($(strip $(MTK_TEE_GP_SUPPORT)), yes)