/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */


package com.mediatek.op07.settings;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.entitlement.ISesService;
import com.mediatek.entitlement.ISesServiceListener;
//import com.mediatek.entitlement.ux.E911WebsheetFragment;
import com.mediatek.ims.internal.MtkImsManagerEx;
import com.mediatek.internal.telephony.MtkPhoneConstants;

/**
 * A placeholder fragment containing a simple view.
 */
public class WfcSettingsActivity extends Activity
    /*implements E911WebsheetFragment.E911WebSheetListener */ {
    private static final boolean DBG = true;
    private static final String TAG = "OP07WfcSettingsActivity";

    private static final String VOWIFI_SERVIVE = "vowifi";
    private static final String SERVICE_PKG_NAME = "com.mediatek.entitlement";
    private static final String SERVICE_NAME = "com.mediatek.entitlement.EntitlementService";

    public static final int EVT_ID_SERVICE_CONNECTED = 0;
    public static final int EVT_ID_SERVICE_DISCONNECTED = 1;
    public static final int EVT_ID_SES_STATE_CHANGE = 2;

    public static final int WEBSHEET_CODE_COMPLETED = 0;
    public static final int WEBSHEET_CODE_ABANDONED = 1;

    public static final int STATE_NOT_ENTITLED = 0;
    public static final int STATE_PENDING = 1;
    public static final int STATE_ENTITlED = 2;
    public static final int STATE_ENTITLEMENT_FAILED = 3;

    private boolean mIsActivityVisible = false;
    private int mEntitlementState = STATE_NOT_ENTITLED;
    //private E911WebsheetFragment mE911WebsheetFragment = null;
    private WiFiCallingFragment mWifiCallingFragment = null;
    private ProgressDialog mLoadingDialog;
    private ISesService mSesService = null;
    private boolean mShouldShowWebShet = false;
    private String mUrl = null;
    private String mPostData = null;
    private static final String UX_APP_PATH =
             "com.mediatek.entitlement/com.mediatek.entitlement.ux.EntitlementActivity";
    private static final int REQUEST_UPDATE_WFC_EMERGENCY_ADDRESS = 1;
    public static final int LAUNCH_UPDATE_ONLY = 2;
    public final String EXTRA_LAUNCH_CARRIER_APP = "EXTRA_LAUNCH_CARRIER_APP";
    public final String EXTRA_LAUNCH_E911_URL = "EXTRA_LAUNCH_E911_URL";
    public final String EXTRA_LAUNCH_E911_POSTDATA = "EXTRA_LAUNCH_E911_POSTDATA";

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "action:" + action);
            if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(action)
                    && (IccCardConstants.INTENT_VALUE_ICC_LOADED.equals(intent
                    .getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE))
                    || IccCardConstants.INTENT_VALUE_ICC_ABSENT.equals(intent
                    .getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE)))) {
                /*boolean isWfcEnabled = mWificallingSwitch.isChecked() ?
                        WfcUtils.isWfcEntitled() : mWificallingSwitch.isChecked();
                updateWifiCalling(isWfcEnabled);*/
                //TODO:: Check what handling we require to do here
            } else if (action.equals(CarrierConfigManager.ACTION_CARRIER_CONFIG_CHANGED)) {
                if (!ImsManager.isWfcEnabledByPlatform(context)) {
                    Log.d(TAG, "carrier config changed, finish WFC activity");
                    finish();
                }
            }
        }
    };

    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            Log.d(TAG, "SES service Connected");
            mSesService = ISesService.Stub.asInterface(binder);
            Intent intent = new Intent();
            intent.setClassName(SERVICE_PKG_NAME, SERVICE_NAME);
            startService(intent);
            dismissProgressDialog();
            try {
                mSesService.registerListener(mSesStateListener);
            } catch (RemoteException e) {
                Log.e(TAG, "Exception happened! " + e.getMessage());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "SES service DisConnected");
            try {
                mSesService.unregisterListener(mSesStateListener);
            } catch (RemoteException e) {
                Log.e(TAG, "Exception happened! " + e.getMessage());
            }
            mSesService = null;
        }
    };

    private ISesServiceListener.Stub mSesStateListener = new ISesServiceListener.Stub() {
        @Override
        public void onEntitlementEvent(String service, String event, Bundle extras) {
             runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "onEntitlementEvent: " + service + ", " + event + ", " + extras);
                    mEntitlementState = translateToInternalState(event);
                    dismissProgressDialog();
                    switch (event) {
                        case "entitled":
                            updateWifiCalling(true);
                            mWifiCallingFragment.updateUI(true);
                            Toast.makeText(WfcSettingsActivity.this, "Entitlement succeeded",
                                 Toast.LENGTH_LONG).show();
                            break;
                        case "failed":
                            updateWifiCalling(false);
                            mWifiCallingFragment.updateUI(false);
                            Toast.makeText(WfcSettingsActivity.this, "Entitlement failed",
                                Toast.LENGTH_LONG).show();
                            break;
                        case "not-entitled":
                            updateWifiCalling(false);
                            mWifiCallingFragment.updateUI(false);
                            Toast.makeText(WfcSettingsActivity.this, "Not Entitled",
                                Toast.LENGTH_LONG).show();
                            break;
                        case "pending":
                            Toast.makeText(WfcSettingsActivity.this, "Entitlement pending",
                                Toast.LENGTH_LONG).show();
                            break;
                        case "remove_sim":
                        case "new_sim":
                            updateWifiCalling(false);
                            mWifiCallingFragment.updateUI(false);
                            Toast.makeText(WfcSettingsActivity.this, "New/Remove SIM",
                                Toast.LENGTH_LONG).show();
                            break;
                    }
                }
            });
        }

        @Override
        public void onWebsheetPost(String url, String postData) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "onWebsheetPost: url: " + url + ", postData: " + postData);
                    mUrl = url;
                    mPostData = postData;
                    if (mIsActivityVisible) {
                        dismissProgressDialog();
                        showEmergencyFragment();
                    } else {
                        mShouldShowWebShet = true;
                    }
                }
            });
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wfc_settings_activity_layout);

        //WfcUtils.registerListener(this, mListener);
        bindWithService();

        ImsManager.setWfcMode(this,
                ImsConfig.WfcModeFeatureValueConstants.CELLULAR_PREFERRED);

        mWifiCallingFragment = new WiFiCallingFragment(this);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.fragment_container, mWifiCallingFragment);
        ft.commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unbindWithService();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Fragment f = getFragmentManager().findFragmentById(R.id.fragment_container);
        Log.d(TAG, "onBackPressed:" + f);
        if (f instanceof WiFiCallingFragment) {
            dismissProgressDialog();
        }
        super.onBackPressed();
    }

    @Override
    public void onResume() {
        mIsActivityVisible = true;
        if (!isServiceConnected()) {
            showProgressDialog(R.string.loading_message);
        } else if (mShouldShowWebShet) {
            showEmergencyFragment();
            mShouldShowWebShet = false;
        }

        mWifiCallingFragment.updateUI(ImsManager.isWfcEnabledByUser(this));

        //Register for Sim Change and Config Change

        IntentFilter filter = new IntentFilter(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        filter.addAction(CarrierConfigManager.ACTION_CARRIER_CONFIG_CHANGED);
        registerReceiver(mReceiver, filter);

        super.onResume();
    }

    public void showEmergencyFragment() {
        /*Fragment f = getFragmentManager().findFragmentById(R.id.fragment_container);
        Log.d(TAG, "showEmergencyFragment:" + f);

        if (!(f instanceof E911WebsheetFragment)) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            if (mE911WebsheetFragment == null) {
                mE911WebsheetFragment = new E911WebsheetFragment();
            }
            ft.replace(R.id.fragment_container, mE911WebsheetFragment);
            ft.addToBackStack(null);
            ft.commit();
        }*/
        ComponentName componentName = ComponentName.unflattenFromString(UX_APP_PATH);
        // Build and return intent
        Intent intent = new Intent();
        intent.setComponent(componentName);
        intent.putExtra(EXTRA_LAUNCH_CARRIER_APP, LAUNCH_UPDATE_ONLY);
        intent.putExtra(EXTRA_LAUNCH_E911_URL, mUrl);
        intent.putExtra(EXTRA_LAUNCH_E911_POSTDATA, mPostData);
        startActivityForResult(intent, REQUEST_UPDATE_WFC_EMERGENCY_ADDRESS);
    }

    @Override
    public void onPause() {
        mIsActivityVisible = false;

        unregisterReceiver(mReceiver);
        super.onPause();
    }
    /**
     * Method of E911WebsheetFragment.E911WebsheetFragmentListener interface.
     * @return
     */
    public void finishFragment() {
        getFragmentManager().popBackStackImmediate();
    }

    /**
    * Method of E911WebsheetFragment.E911WebsheetFragmentListener interface.
    * @param webSheetState Abandoned/Completed
    * @param finishWebSheet true if to be finished else false
    * @return
    */
    public void webSheetStateChange(boolean isComplete) {
        getFragmentManager().popBackStackImmediate();
       /* if (!isComplete) {
            cancelEntitlementCheck();
        }*/
    }

    public boolean turnWfcOn() {
        // Show toast for time being & wait for service callback to enable switch
        startEntitlement(false);

        //mWificallingSwitch.setChecked(false);
        updateWifiCalling(false);
        // Wifi not connected means Alert dialog is being displayed.
        // Display progress dialog once user takes action on it
        showProgressDialog(R.string.SES_ongoing_string);

        return false;
    }

    public boolean updateWifiCalling(boolean enabled) {
        Log.d(TAG, "enabled:" + enabled);

        if (enabled) {
            if (isInSwitchProcess()) {
                Log.d(TAG, "Switching process ongoing");
                Toast.makeText(this, R.string.Switch_not_in_use_string, Toast.LENGTH_SHORT)
                        .show();

                return false;
            }
        }
        ImsManager.setWfcSetting(this, enabled);
        return enabled;
    }

    private boolean isInSwitchProcess() {
        int imsState = MtkPhoneConstants.IMS_STATE_DISABLED;
        try {
         imsState = MtkImsManagerEx.getInstance().getImsState(SubscriptionManager
                .getDefaultVoiceSubscriptionId());
        } catch (ImsException e) {
           return false;
        }
        Log.d("@M_" + TAG, "isInSwitchProcess , imsState = " + imsState);
        return imsState == MtkPhoneConstants.IMS_STATE_DISABLING
                || imsState == MtkPhoneConstants.IMS_STATE_ENABLING;
    }

    private void showProgressDialog(int message) {
        if (mLoadingDialog == null) {
            mLoadingDialog = ProgressDialog.show(this, null,
                    getResources().getString(message), true,
                    true,
                    new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                        // TODO:: Should do the cancel dialog handling here
                            Log.d(TAG, "OnCancel");
                            mLoadingDialog = null;
                            cancelEntitlementCheck();
                            if (!isServiceConnected()) {
                                finish();
                            }
                        }
                    });
            mLoadingDialog.setCanceledOnTouchOutside(false);
            mLoadingDialog.setInverseBackgroundForced(false);
        }
    }

    private void dismissProgressDialog() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            Log.d(TAG, "Dismiss dialog");
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }
    }

        /**
         * Gets wfc summary on basis of entitlement status.
         * @param service service
         * @return int
         */
    public int getSummary() {
        int resId = R.string.empty_message;

        switch (mEntitlementState) {

            case STATE_NOT_ENTITLED:
               break;

            case STATE_PENDING:
                resId = R.string.prov_pending_message;
                break;

            case STATE_ENTITlED:
                resId = R.string.prov_complete_message;
                break;

            case STATE_ENTITLEMENT_FAILED:
                resId = R.string.service_not_allowed;
                break;

            default:
                Log.d(TAG, "invalid state:" + mEntitlementState);
                break;
        }
        return resId;
    }

        /**
     * Determines whether Wifi is ON or not.
     * @param context context
     * @return boolean
     */
    public boolean isWifiConnected(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
            Log.d(TAG, "Wifi ON");
            return true;
        }
        Log.d(TAG, "Wifi off");
        return false;
    }

        /**
     * Binds with Ses Service.
     * @param context context
     * @param serviceConnection serviceConnection
     * @return
     */
    private void bindWithService() {
        if (isPackageExist(SERVICE_PKG_NAME)) {
            Intent intent = new Intent();
            intent.setClassName(SERVICE_PKG_NAME, SERVICE_NAME);
            //startService(intent);
            boolean b = bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
            Log.d(TAG, "bindWithService:" + b);
        }
    }

    /**
     * Unbinds with Ses Service.
     * @param context context
     * @param serviceConnection serviceConnection
     * @return
     */
    private void unbindWithService() {
        if (mSesService != null) {
            Log.d(TAG, "unbindWithService");
            try {
                mSesService.unregisterListener(mSesStateListener);
            } catch (RemoteException e) {
                Log.e(TAG, "Exception happened! " + e.getMessage());
            }
            unbindService(mServiceConnection);
        }
    }

    private boolean isPackageExist(String packageName) {
        try {
            getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SERVICES);
        } catch (PackageManager.NameNotFoundException  e) {
            Log.d(TAG, "package exist: false");
            return false;
        }
        Log.d(TAG, "package exist: true");
        return true;
    }

        /**
         * Checks whether wfc is entitled or not.
         * @param service service
         * @return boolean
         */
    public boolean isWfcEntitled() {
        boolean isWfcEntitled = false;
        try {
            if (mSesService != null) {
                String state = mSesService.getCurrentEntitlementState(VOWIFI_SERVIVE);
                isWfcEntitled = (translateToInternalState(state) == STATE_ENTITlED) ? true : false;
            }
        } catch (RemoteException e) {
            Log.e(TAG, "isWfcEntitled:Exception happened! " + e.getMessage());
        }
        return isWfcEntitled;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getServerData() {
        return mPostData;
    }

    public void startEntitlement(boolean retry) {

         if (mSesService != null) {
            try {
                if (retry) {
                    mSesService.startEntitlementCheck(VOWIFI_SERVIVE, 30, 2);
                } else {
                    mSesService.startEntitlementCheck(VOWIFI_SERVIVE, 0, 0);
                }
            } catch (RemoteException e) {
                Log.e(TAG, "startEntitlementCheck :Exception happened! " + e.getMessage());
            }
        }
    }

    public void updateLocationAndTc() {
        //showEmergencyFragment();
        showProgressDialog(R.string.loading_message);
        try {
            mSesService.updateLocationAndTc(VOWIFI_SERVIVE);
        } catch (RemoteException e) {
            Log.e(TAG, "updateLocationAndTc :Exception happened! " + e.getMessage());
        }
    }

    public void cancelEntitlementCheck() {
        if (mSesService != null) {
            try {
                mSesService.stopEntitlementCheck(VOWIFI_SERVIVE);
            } catch (RemoteException e) {
                Log.e(TAG, "cancelEntitlementCheck :Exception happened! " + e.getMessage());
            }
        }
    }


    public boolean isServiceConnected() {
        return mSesService != null ? true : false;
    }

    /**
        Used to Deactivate the Entitled service.
    **/
    public void deactivateService() {
        if (mSesService != null) {
            try {
                mSesService.deactivateService(VOWIFI_SERVIVE);
            } catch (RemoteException e) {
                Log.e(TAG, "deactivateService :Exception happened! " + e.getMessage());
            }
        } else {
            Log.d(TAG, "deactivateService mSesService is null");
        }
    }

    private int translateToInternalState(String internalState) {
        if (internalState == null) {
            return STATE_NOT_ENTITLED;
        }
        switch (internalState) {
            case "not-entitled":
                return STATE_NOT_ENTITLED;
            case "failed":
                return STATE_ENTITLEMENT_FAILED;
            case "pending":
                return STATE_PENDING;
            case "entitled":
                return STATE_ENTITlED;
            default:
                Log.e(TAG, "Unhandled state: " + internalState);
                return STATE_NOT_ENTITLED;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_UPDATE_WFC_EMERGENCY_ADDRESS) {
            Log.d(TAG, "WFC emergency address activity result = " + resultCode);

            if (resultCode == Activity.RESULT_OK) {
                boolean updateResult = data.getBooleanExtra("isComplete", false);
                if (updateResult) {
                    turnWfcOn();
                } else {
                    //startEntitlement(true);
                    //finish();
                }
            } else {
                if (mEntitlementState != STATE_ENTITlED) {
                    cancelEntitlementCheck();
                }
            }
        }
    }

    public static class ErrorCodes {
        public static final int INVALID_RESPONSE = -1;

        public static final int REQUEST_SUCCESSFUL = 1000;
        public static final int AKA_CHALLENGE = 1003;
        public static final int INVALID_REQUEST = 1004;
        public static final int INVALID_IP_AUTH = 1005;
        public static final int AKA_AUTH_FAILED = 1006;
        public static final int FORBIDDEN_REQUEST = 1007;
        public static final int INVALID_CLIENT_ID = 1008;
        public static final int MAX_DEVICES_REACHED = 1010;
        public static final int UNKNOWN_DEVICE = 1020;
        public static final int EPDG_NOT_FOUND = 1021;
        public static final int CERT_FAILURE = 1022;
        public static final int REMOVE_FAILURE = 1023;
        public static final int INVALID_OWNER_ID = 1024;
        public static final int INVALID_CSR = 1025;
        public static final int DEVICE_LOCKED = 1028;
        public static final int INVALID_DEVICE_STATUS = 1029;
        public static final int MAX_SERVICES_REACHED = 1040;
        public static final int INVALID_SERVICE_FINGERPRINT = 1041;
        public static final int INVALID_TARGET_DEVICE = 1042;
        public static final int INVALID_TARGET_USER = 1043;
        public static final int MAX_SERVICE_INSTANCE_REACHED = 1044;
        public static final int FORBIDDEN_COPY = 1045;
        public static final int INVALID_SERVICE_NAME = 1046;
        public static final int SERVICE_INVALID_IMSI = 1047;
        public static final int SERVICE_NOT_ENTITLED = 1048;
        public static final int SERVICE_NO_PERMISSION = 1049;
        public static final int SERVICE_PARAMS_FAILURE = 1050;
        public static final int REMOVE_SERVICE_FAILURE = 1052;
        public static final int INVALID_SERVICE_INSTANCE_ID = 1053;
        public static final int INVALID_DEVICE_GROUP = 1054;
        public static final int NO_MSISDN = 1060;
        public static final int ERROR_IN_MSISDN_CREATION = 1061;
        public static final int MAX_MSISDN_EXCEEDED = 1062;
        public static final int INVALID_MSISDN = 1070;
        public static final int INVALID_PUSH_TOKEN = 1080;
        public static final int SERVER_ERROR = 1111;
        public static final int THREEGPP_AUTH_ONGOING = 1112;
        public static final int ONGOING_REQUEST = 1500;
        public static final int UNSUPPORTED_OPERATION = 9999;

        /**
         * Is error code valid.
         * @param errorCode errorCode
         * @return boolean
         */
        public static boolean isValidErrorCode(int errorCode) {
            switch (errorCode) {
                case REQUEST_SUCCESSFUL:
                case AKA_CHALLENGE:
                case INVALID_REQUEST:
                case INVALID_IP_AUTH:
                case AKA_AUTH_FAILED:
                case FORBIDDEN_REQUEST:
                case INVALID_CLIENT_ID:
                case MAX_DEVICES_REACHED:
                case UNKNOWN_DEVICE:
                case EPDG_NOT_FOUND:
                case CERT_FAILURE:
                case REMOVE_FAILURE:
                case INVALID_OWNER_ID:
                case INVALID_CSR:
                case DEVICE_LOCKED:
                case INVALID_DEVICE_STATUS:
                case MAX_SERVICES_REACHED:
                case INVALID_SERVICE_FINGERPRINT:
                case INVALID_TARGET_DEVICE:
                case INVALID_TARGET_USER:
                case MAX_SERVICE_INSTANCE_REACHED:
                case FORBIDDEN_COPY:
                case INVALID_SERVICE_NAME:
                case SERVICE_INVALID_IMSI:
                case SERVICE_NOT_ENTITLED:
                case SERVICE_NO_PERMISSION:
                case SERVICE_PARAMS_FAILURE:
                case REMOVE_SERVICE_FAILURE:
                case INVALID_SERVICE_INSTANCE_ID:
                case INVALID_DEVICE_GROUP:
                case NO_MSISDN:
                case ERROR_IN_MSISDN_CREATION:
                case MAX_MSISDN_EXCEEDED:
                case INVALID_MSISDN:
                case INVALID_PUSH_TOKEN:
                case SERVER_ERROR:
                case THREEGPP_AUTH_ONGOING:
                case ONGOING_REQUEST:
                case UNSUPPORTED_OPERATION:
                    return true;
                default:
                    return false;
            }
        }
    }
}
