package com.mediatek.internal.telephony.selfactivation;

import android.os.Bundle;
import android.telephony.Rlog;
import android.telephony.SubscriptionManager;

class SelfActivationDefault implements ISelfActivation {
    private static final String TAG = "SelfActivationDefault";
    private static ISelfActivation mInstance = null;

    static void init() {
        mInstance = new SelfActivationDefault();
    }

    static ISelfActivation getInstance() {
        if (mInstance == null) {
            mInstance = new SelfActivationDefault();
        }
        return mInstance;
    }

    SelfActivationDefault() {
        Rlog.d("SelfActivationDefault", "init");
    }

    public int selfActivationAction(int action, Bundle param) {
        return -1;
    }

    public int getSelfActivateState() {
        return SaUtils.STATE_NONE;
    }
}
