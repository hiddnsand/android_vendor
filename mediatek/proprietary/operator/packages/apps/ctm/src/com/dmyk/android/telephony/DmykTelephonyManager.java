package com.dmyk.android.telephony;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.INetworkStatsSession;
import android.net.NetworkTemplate;
import android.net.NetworkInfo;
import android.net.INetworkStatsService;
import android.net.Uri;
import android.net.TrafficStats;
import android.os.SystemProperties;
import android.os.Build;
import android.os.ServiceManager;
import android.telephony.CellLocation;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.database.Cursor;
import android.content.ContentResolver;
import java.lang.reflect.Method;

import com.android.ims.ImsManager;

import com.mediatek.custom.CustomProperties;
import com.mediatek.telephony.MtkTelephonyManagerEx;
import com.android.internal.telephony.SubscriptionController;

public final class DmykTelephonyManager extends DmykAbsTelephonyManager {
    private static final int LENGTH_MEID = 14;
    private static final int LENGTH_ICCID = 20;
    private static final String PROPERTY_ICCID_SIM_PREFIX = "ril.iccid.sim";
    private static final String PROPERTY_MULTIPLE_IMS_SUPPORT = "ro.mtk_multiple_ims_support";

    public static final class MLog {
        private static final String TAG = "CTM";
        private static final boolean sEnable = true;
        public static void v(String msg) {
            if (sEnable) {
                android.util.Log.v(TAG, msg);
            }
        }
        public static void d(String msg) {
            if (sEnable) {
                android.util.Log.d(TAG, msg);
            }
        }
        public static void i(String msg) {
            if (sEnable) {
                android.util.Log.i(TAG, msg);
            }
        }
        public static void w(String msg) {
            if (sEnable) {
                android.util.Log.w(TAG, msg);
            }
        }
        public static void e(String msg) {
            if (sEnable) {
                android.util.Log.e(TAG, msg);
            }
        }
        public static void wtf(String msg) {
            if (sEnable) {
                android.util.Log.wtf(TAG, msg);
            }
        }
    }

    private Context mContext;
    private TelephonyManager mTelephonyManager = null;
    //private ConnectivityManager mConnectivityManager = null;

    public DmykTelephonyManager(Context context) {
        MLog.d("DmykTelephonyManager instantiated");
        if (context == null) {
            MLog.e("DmykTelephonyManager: context is null!");
        }
        mContext = context;
    }

    private TelephonyManager getTelephonyManager() {
        if (mTelephonyManager == null) {
            if (mContext != null) {
                mTelephonyManager =
                    (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            }
        }
        return mTelephonyManager;
    }

    @Override
    public int getPhoneCount() {
        // MTK proprietary API
        int result = getTelephonyManager().getSimCount();
        MLog.d("getPhoneCount(): " + result);
        return result;
    }

    @Override
    public String getGsmDeviceId(int phoneId) {
        String result = null;
        if (phoneId >= 0 && phoneId < 2) {
            // the sole parameter of getImei is slotId
            result = getTelephonyManager().getImei(phoneId);
        }
        MLog.d("getGsmDeviceId(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public String getCdmaDeviceId() {
        // MTK proprietary API
        // iterate over all phones to get a valid MEID
        for (int i = 0; i < getPhoneCount(); i++) {
            String result = getTelephonyManager().getMeid(i);
            // sanity check
            if (result != null && result.length() == LENGTH_MEID) {
                MLog.d("getCdmaDeviceId(): " + result);
                return result;
            }
        }
        MLog.d("getCdmaDeviceId(): null");
        return null;
    }

    @Override
    public String getSubscriberId(int phoneId) {
        String result = null;
        if (phoneId >= 0 && phoneId < 2) {
            int subId = getSubIdForSlotId(phoneId);
            if (subId >= 0) {
                result = getTelephonyManager().getSubscriberId(subId);
            }
        }
        MLog.d("getSubscriberId(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public String getIccId(int phoneId) {
        String result = null;
        MtkTelephonyManagerEx tme = MtkTelephonyManagerEx.getDefault();
        if (phoneId >= 0 && phoneId < 2) {
            result = tme.getSimSerialNumber(phoneId);
        }
        MLog.d("getIccId(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public int getDataState(int phoneId) {
        int result = DATA_UNKNOWN;
        if (phoneId >= 0 && phoneId < 2) {
            int subId = getSubIdForSlotId(phoneId);
            int defaultSubId = SubscriptionManager.getDefaultDataSubscriptionId();
            if((subId >= 0) && (subId == defaultSubId)){
                result = getTelephonyManager().getDataState();
            }
        }
        MLog.d("getDataState(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public int getSimState(int phoneId) {
        int result = DATA_UNKNOWN;
        if (phoneId >= 0 && phoneId < 2) {
            // MTK proprietary API
            // getSimState() requires a SIM slot ID
            result = getTelephonyManager().getSimState(phoneId);
        }
        MLog.d("getSimState(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public int getNetworkType(int phoneId) {
        int result = TelephonyManager.NETWORK_TYPE_UNKNOWN;
        if (phoneId >= 0 && phoneId < 2) {
            int subId = getSubIdForSlotId(phoneId);
            if (subId >= 0) {
                // MTK proprietary API
                // getNetworkType() requires a sub ID
                result = getTelephonyManager().getNetworkType(subId);
                MLog.d("TelephonyManager.getNetworkType(" + phoneId + "): " + result);
                if (result == 0) {
                    result =
                        getTelephonyManager().getVoiceNetworkType(subId);
                        MLog.d("getVoiceNetworkType(" + phoneId + "): " + result);
                }
            }
        }
        MLog.d("getNetworkType(" + phoneId + "): " + result);
        switch(result){
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return NETWORK_TYPE_GPRS;
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return NETWORK_TYPE_EDGE;
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return NETWORK_TYPE_UMTS;
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return NETWORK_TYPE_CDMA;
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return NETWORK_TYPE_EVDO_0;
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return NETWORK_TYPE_EVDO_A;
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return NETWORK_TYPE_1xRTT;
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return NETWORK_TYPE_HSDPA;
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return NETWORK_TYPE_HSUPA;
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return NETWORK_TYPE_HSPA;
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return NETWORK_TYPE_IDEN;
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return NETWORK_TYPE_EVDO_B;
            case TelephonyManager.NETWORK_TYPE_LTE:
                return NETWORK_TYPE_LTE;
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return NETWORK_TYPE_EHRPD;
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return NETWORK_TYPE_HSPAP;
            case TelephonyManager.NETWORK_TYPE_GSM:
                return NETWORK_TYPE_GSM;
            case TelephonyManager.NETWORK_TYPE_TD_SCDMA:
                return NETWORK_TYPE_TD_SCDMA;
            case TelephonyManager.NETWORK_TYPE_IWLAN:
                return NETWORK_TYPE_IWLAN;
            case TelephonyManager.NETWORK_TYPE_LTE_CA:
                return NETWORK_TYPE_LTE_CA;
            default:
                return NETWORK_TYPE_UNKNOWN;
            }
    }

    @Override
    public String getDeviceSoftwareVersion() {
        String result = CustomProperties.getString(
                            CustomProperties.MODULE_DM,
                            "SoftwareVersion",
                            "MTK");
        MLog.d("getDeviceSoftwareVersion(): " + result);
        return result;
    }

    /*
     * This method should be left to vendor to customize as there is no promising method to detect
     * device type automatically.
     *
     * @return DEVICE_TYPE_CELLPHONE as default
     */
    @Override
    public int getDeviceType() {
        int result = DEVICE_TYPE_CELLPHONE;
        MLog.d("getDeviceType(): " + result);
        return result;
    }

    @Override
    public int getMasterPhoneId() {
        int result = SystemProperties.getInt("persist.radio.simswitch", 1) - 1;
        MLog.d("getMasterPhoneId(), simswitch -1 : " + result);
        if(result >= 0 && result < 2){
            int subId = getSubIdForSlotId(result);
            if (subId >= 0){
                MLog.d("getMasterPhoneId(): " + result);
                return result;
            }
        }

        MLog.d("getMasterPhoneId(): -1 ");
        return -1;
    }

    @Override
    public boolean isInternationalNetworkRoaming(int phoneId) {
        boolean result = false;
        if (phoneId >= 0 && phoneId < 2) {
            int subId = getSubIdForSlotId(phoneId);
            if (subId >= 0) {
                // MTK proprietary API
                // getNetworkType() requires a sub ID
                result = getTelephonyManager().isNetworkRoaming(subId);
            }
        }
        MLog.d("isInternationalNetworkRoaming(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public int getVoLTEState(int phoneId) {
        int result = VOLTE_STATE_UNKNOWN;
        if (phoneId >= 0 && phoneId < 2) {
            if (!supportsMultiIms()) {
                if (phoneId == 0) {
                    boolean enabled =
                            ImsManager.isEnhanced4gLteModeSettingEnabledByUser(mContext);
                    result = enabled ? VOLTE_STATE_ON : VOLTE_STATE_OFF;
                }
            } else {
                // phoneId specified in parameter list is actually slot ID
                phoneId = SubscriptionManager.getPhoneId(getSubIdForSlotId(phoneId));
                try {
                    Method method = ImsManager.class.getDeclaredMethod(
                        "isEnhanced4gLteModeSettingEnabledByUser",
                        Context.class,
                        Integer.class);
                    boolean enabled =
                        (Boolean) method.invoke(null, new Object[]{mContext, phoneId});
                    result = enabled ? VOLTE_STATE_ON : VOLTE_STATE_OFF;
                } catch (NoSuchMethodException e1) {
                    MLog.d("No isEnhanced4gLteModeSettingEnabledByUser(Context, int)");
                    if (phoneId == 0) {
                        boolean enabled =
                            ImsManager.isEnhanced4gLteModeSettingEnabledByUser(mContext);
                        result = enabled ? VOLTE_STATE_ON : VOLTE_STATE_OFF;
                    } else {
                        result = VOLTE_STATE_UNKNOWN;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    result = VOLTE_STATE_UNKNOWN;
                }
            }
        }
        MLog.d("getVoLTEState(" + phoneId + "): " + result);
        return result;
    }

    public void setVoLTEState(int phoneId, int status) {
        MLog.d("setVoLTEState(" + phoneId + ", " + status + ")");
        if (phoneId >= 0 && phoneId < 2) {
            if (supportsMultiIms()) {
                // phoneId specified in parameter list is actually slot ID
                phoneId = SubscriptionManager.getPhoneId(getSubIdForSlotId(phoneId));
                try {
                    Method method = ImsManager.class.getDeclaredMethod(
                        "setEnhanced4gLteModeSetting",
                        Context.class,
                        Boolean.class,
                        Integer.class);
                    method.invoke(
                        null,
                        new Object[]{mContext, (status == VOLTE_STATE_ON), phoneId});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if (phoneId == 0) {
                    ImsManager.setEnhanced4gLteModeSetting(
                        mContext,
                        (status == VOLTE_STATE_ON));
                }
            }
        } else {
            MLog.e("Invalid phoneId, do nothing");
        }
    }


    /**
     * Get APN content uri for Slot ID. Internally APN content uris are defined
     * by Sub ID, so we have to map Slot ID to Sub ID first.
     * @param phoneId slot ID
     * @return APN uri of the slot specified as phoneId, null if no SIM in slot
     */
    @Override
    public Uri getAPNContentUri(int phoneId) {
        String result = null;
        Uri uri = null;
        Cursor cr = null;
        if (phoneId >= 0 && phoneId < 2) {
            int subId = getSubIdForSlotId(phoneId);
            if (subId >= 0) {
                result = "content://telephony/carriers/preferapn/subId/" + subId;
                uri = Uri.parse(result);
                cr = mContext.getContentResolver().query(uri,null,null,null,null);
                if((cr == null) || (cr.getCount() == 0))
                {
                     if(cr != null){
                         cr.close();
                     }
                     Uri temp = Uri.parse("content://telephony/carriers/subId/" + subId);
                     cr = mContext.getContentResolver().query(temp,null,null,null,null);
                     if((cr != null) && (cr.moveToNext())){
                         String id = cr.getString(cr.getColumnIndex("_id"));
                         result = "content://telephony/carriers/" + id;
                         uri = Uri.parse(result);
                     }
                }
                if(cr != null){
                    cr.close();
                }
            }
        }
        MLog.d("getContentUri(" + phoneId + "): " + result);
        return uri;
    }

    /**
     * @param phoneId the phoneId here is actually subId
     * @return slot ID
     */
    @Override
    public int getSlotId(int phoneId) {
        return SubscriptionManager.getSlotIndex(phoneId);
    }

    @Override
    public int getCellId(int phoneId) {
        int result = -1;
        if (getSubIdForSlotId(phoneId) == -1){
            MLog.d("getCellId(" + phoneId + "): " + result);
            return  result;
        }
        if (phoneId >= 0 && phoneId < 2) {
            MtkTelephonyManagerEx tme = MtkTelephonyManagerEx.getDefault();
            if (tme == null) {
                MLog.w("MtkTelephonyManagerEx may not be ready");
            } else {
                // TelephonyManagerEx.getCellLocation(int) accepts slot ID
                CellLocation cl = tme.getCellLocation(phoneId);
                if (cl == null) {
                    MLog.e("CellLocation is null");
                } else if (cl instanceof GsmCellLocation) {
                    result = ((GsmCellLocation)cl).getCid();
                } else if (cl instanceof CdmaCellLocation) {
                    result = ((CdmaCellLocation)cl).getSystemId();
                } else {
                    MLog.e("CellLocation type " + cl.getClass() + " is not supported");
                }
            }
        }
        MLog.d("getCellId(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public int getLac(int phoneId) {
        int result = -1;
        if (getSubIdForSlotId(phoneId) == -1){
            MLog.d("getLac(" + phoneId + "): " + result);
            return  result;
        }
        if (phoneId >= 0 && phoneId < 2) {
            MtkTelephonyManagerEx tme = MtkTelephonyManagerEx.getDefault();
            if (tme == null) {
                MLog.w("TelephonyManagerEx may not be ready");
            } else {
                // TelephonyManagerEx.getCellLocation(int) accepts slot ID
                CellLocation cl = tme.getCellLocation(phoneId);
                if (cl == null) {
                    MLog.e("CellLocation is null");
                } else if (cl instanceof GsmCellLocation) {
                    result = ((GsmCellLocation)cl).getLac();
                } else if (cl instanceof CdmaCellLocation) {
                    result = ((CdmaCellLocation)cl).getNetworkId();
                } else {
                    MLog.e("CellLocation type " + cl.getClass() + " is not supported");
                }
            }
        }
        MLog.d("getLac(" + phoneId + "): " + result);
        return result;
    }

    @Override
    public String getCTAModel(){
        return "invalid CTA Model";
    }

    @Override
    public String getRomStorageSize(){
        return "16G";
    }

    @Override
    public String getRamStorageSize(){
        return "4G";
    }

    @Override
    public String getMacAddress(){
        WifiManager wifi = (WifiManager) getContext().
                            getSystemService(Context.WIFI_SERVICE);
        if(!wifi.isWifiEnabled()){
            return null;
        }
        WifiInfo wifiInfo = wifi.getConnectionInfo();
        String macAddress =
             wifiInfo == null ? null : wifiInfo.getMacAddress();
        return macAddress;
    }

    @Override
    public String getCPUModel(){
        return "invalid CPU Model";
    }

    @Override
    public String getOSVersion(){
        String OPERATING_SYSTEM = "android";
        return OPERATING_SYSTEM + Build.VERSION.RELEASE;
    }

    @Override
    public long getWiFiTotalBytes(long startTime, long endTime){
        long totalBytes = 0;
        if((endTime - startTime) < 0){
            MLog.d("getWiFiTotalBytes startTime=" + startTime + " endTime" + endTime
                + " invalid parameters" );
            return -1;
        }
        NetworkTemplate templete;
        INetworkStatsService statsService = INetworkStatsService.Stub.asInterface(
                ServiceManager.getService(Context.NETWORK_STATS_SERVICE));
        try {
            templete = NetworkTemplate.buildTemplateWifiWildcard();
            INetworkStatsSession statsSession = statsService.openSession();
            if (statsSession != null) {
                 totalBytes = statsSession.getSummaryForNetwork(
                                    templete, startTime, endTime)
                                    .getTotalBytes();
                 TrafficStats.closeQuietly(statsSession);
             }
        }catch (Exception e) {
                 MLog.e("getMobileTotalBytes Exception" + e);
         }
         MLog.d("getWiFiTotalBytes startTime=" + startTime + " endTime" + endTime
                + " totalBytes=" + totalBytes);
         return totalBytes;
    }

    @Override
    public long getMobileTotalBytes(
        int phoneId, long startTime, long endTime) {
        if((phoneId>2) || (phoneId <0) ||(endTime - startTime) < 0){
            MLog.d("getMobileTotalBytes phoneId=" + phoneId
                + " startTime=" + startTime + " endTime" + endTime
                + " invalid parameters");
            return -1;
        }
        String subscriberId = null;
        long totalBytes = 0;
        NetworkTemplate templete;
        INetworkStatsService statsService = INetworkStatsService.Stub.asInterface(
                ServiceManager.getService(Context.NETWORK_STATS_SERVICE));
        final TelephonyManager tele = TelephonyManager.from(mContext);
        if (tele != null) {
            try {
                if (tele.getSimState(phoneId) == SIM_STATE_READY) {
                    int subId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
                    int[] subIds = SubscriptionManager.getSubId(phoneId);
                    if ((subIds != null) && (subIds.length != 0)) {
                        subId = subIds[0];
                    }
                    if (!SubscriptionManager.isValidSubscriptionId(subId)){
                         //Dual sim fetch fail, always fetch sim1
                         MLog.e("Dual sim phoneId:" + phoneId
                             + " fetch subID fail, always fetch default");
                             subId = SubscriptionManager.getDefaultDataSubscriptionId();
                    }
                    if (SubscriptionManager.isValidSubscriptionId(subId)) {
                        subscriberId = tele.getSubscriberId(subId);
                        INetworkStatsSession statsSession = statsService.openSession();
                        if (statsSession != null) {
                            templete = NetworkTemplate.buildTemplateMobileAll(subscriberId);
                            totalBytes = statsSession.getSummaryForNetwork(
                                    templete, startTime, endTime)
                                        .getTotalBytes();
                           TrafficStats.closeQuietly(statsSession);
                        } else {
                            totalBytes = 0;
                        }
                    } else {
                            MLog.e("getMobileTotalBytes subId not valid");
                     }
                 } else {
                            MLog.e("getMobileTotalBytes SimState != SIM_STATE_READY");
                }
            } catch (Exception e) {
                MLog.e("getMobileTotalBytes Exception" + e);
            }
        } else {
            MLog.e("getMobileTotalBytes TelephonyManager is null");
        }
        MLog.d("getMobileTotalBytes phoneId=" + phoneId + " subscriberId" + subscriberId
                + " startTime=" + startTime + " endTime" + endTime
                + " totalBytes=" + totalBytes);
        return totalBytes;
    }

    @Override
    public int getPriorNetworkType() {
        int result = 0;
        if ("OP01".equals(SystemProperties.get("persist.operator.optr", "")) ||
            "OP01".equals(SystemProperties.get("ro.operator.optr", ""))) {
            result = 1;
        }
        MLog.v("getPriorNetworkType(): " + result);
        return result;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    public boolean supportsMultiIms() {
        if (SystemProperties.getInt(PROPERTY_MULTIPLE_IMS_SUPPORT, 1) == 1) {
            return false;
        } else {
            return true;
        }
    }

    private int getSubIdForSlotId(int slotId) {
        // MTK proprietary API
        if (!getTelephonyManager().hasIccCard(slotId)) {
            MLog.d("getSubIdForSlotId(" + slotId + "): -1 (no sim found)");
            return -1;
        }
        // MTK proprietary API
        int[] subIds = SubscriptionManager.getSubId(slotId);
        if (subIds == null || subIds.length < 1 || subIds[0] < 0) {
            MLog.e("getSubIdForSlotId(" + slotId + "): -1 (error)");
            return -1;
        }
        MLog.d("getSubIdForSlotId(" + slotId + "): " + subIds[0]);
        return subIds[0];
    }
}
