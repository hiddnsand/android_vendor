/*
 * Copyright (C) 2016 MediaTek Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See http://www.gnu.org/licenses/gpl-2.0.html for more details.
 */

#ifndef _GATEKEEPER_MODULE_H_
#define _GATEKEEPER_MODULE_H_

#define GATEKEEPER_SOFTWARE_MODULE_NAME "Soft Gatekeeper HAL"

#ifdef __cplusplus
extern "C" {
#endif

/* -------------------------------------------------------------------------
   That's the only function used by module.c. Name of this function must
   not be mangled when compiled by C++ compiler.
   -------------------------------------------------------------------------*/
int soft_gatekeeper_open(const struct hw_module_t* module, const char* id,
            struct hw_device_t** device);

int soft_gatekeeper_close(hw_device_t *hw);

#ifdef __cplusplus
}
#endif

#endif /* _GATEKEEPER_MODULE_H_ */
