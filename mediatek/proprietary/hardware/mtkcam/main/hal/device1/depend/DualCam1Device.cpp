/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/DualCam1Device"
//
#include "MyUtils.h"
#include "DualCam1Device.h"

#include <mtkcam/utils/fwk/MtkCamera.h>
#include <cutils/properties.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>


//
using namespace android;
//
#include <mtkcam/utils/hw/CamManager.h>
using namespace NSCam::Utils;

#include <mtkcam/middleware/v1/camutils/FrameworkCBThread.h>

#include <mtkcam/feature/stereo/hal/stereo_setting_provider.h>
//
#include <sys/prctl.h>
#if (MTKCAM_HAVE_AEE_FEATURE == 1)
#include <aee.h>
#endif

// dual cam denoise related
#if (MTK_CAM_STEREO_DENOISE_SUPPORT == 1)
#define DUALCAM_DENOISE (1)
#include <mtkcam/middleware/v1/LegacyPipeline/IResourceContainer.h>
#include <mtkcam/middleware/v1/LegacyPipeline/StreamId.h>
#include <mtkcam/feature/DualCam/IDualCamPolicy.h>
#include <mtkcam/feature/DualCam/DualCam.Common.h>
#else
#define DUALCAM_DENOISE (0)
#endif
#include <future>
#include <functional>

#if (MTKCAM_HAVE_DUAL_ZOOM_FUSION_SUPPORT == 1)
#include <future>
#include <functional>
#endif

using namespace MtkCamUtils;
using namespace NSCam;
using namespace NSCam::v1::Stereo;

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

bool
ImgBufProviderClientBridge::
onImgBufProviderCreated(sp<IImgBufProvider>const& rpProvider)
{
    Mutex::Autolock _l(mLock);
    for (size_t i = 0; i < mvClient.size(); i++)
    {
        CAM_LOGD("WillDBG3 onImgBufProviderCreated");
        mvClient.editItemAt(i)->onImgBufProviderCreated(rpProvider);
    }
    return  true;
}

void
ImgBufProviderClientBridge::
onImgBufProviderDestroyed(int32_t const i4ProviderId)
{
    Mutex::Autolock _l(mLock);
    for (size_t i = 0; i < mvClient.size(); i++)
    {
        mvClient.editItemAt(i)->onImgBufProviderDestroyed(i4ProviderId);
    }
}

void
ImgBufProviderClientBridge::
addImgBufProviderClient(sp<IImgBufProviderClient> rpClient)
{
    Mutex::Autolock _l(mLock);
    mvClient.push_back(rpClient);
}

void
ImgBufProviderClientBridge::
clearImgBufProviderClient()
{
    Mutex::Autolock _l(mLock);
    mvClient.clear();
}

/******************************************************************************
 *
 ******************************************************************************/
#define IMG_1080P_W         (1920)
#define IMG_1080P_H         (1080)
#define IMG_1080P_H_ALIGN   (1088)
#define IMG_1080P_SIZE      (IMG_1080P_W*IMG_1080P_H_ALIGN)

extern "C"
NSCam::Cam1Device*
createCam1Device_Dual(
    String8 const&          rDevName,
    int32_t const           i4OpenId
)
{
    return new DualCam1Device(rDevName, i4OpenId);
}


/******************************************************************************
 *
 ******************************************************************************/
DualCam1Device::
DualCam1Device(
    String8 const&          rDevName,
    int32_t const           i4OpenId
)
    : Cam1DeviceBase(rDevName, i4OpenId)
      //
    , mpHalSensor(NULL)
      //
#if '1'==MTKCAM_HAVE_3A_HAL
    , mpHal3a(NULL)
#endif
    , mbThreadRunning(MFALSE)
    , mpCamAdapter_W(NULL)
    , mpCamAdapter_T(NULL)
      //
{
}


/******************************************************************************
 *
 ******************************************************************************/
DualCam1Device::
~DualCam1Device()
{
    if (!waitThreadInitDone())
    {
        MY_LOGE("init in thread failed");
    }
}


/******************************************************************************
 *
 ******************************************************************************/
bool
DualCam1Device::
onInit()
{
    MY_LOGD("+");
    {
        CAM_TRACE_NAME("init(profile)");
    }
    CamProfile  profile(__FUNCTION__, "DefaultCam1Device");
    //
    bool    ret = false;
    int     err = 0, i4DeviceNum = 0;
    int     Supported = 0;
    //
    //--------------------------------------------------------------------------
    if(updateSensorId() != OK)
    {
        MY_LOGE("Cannot get valid sensor id, init camera fail.");
        goto lbExit;
    }
    //--------------------------------------------------------------------------
    {
        CAM_TRACE_NAME("init(sensor)");
        //  (0) power on sensor
        if (pthread_create(&mThreadHandle, NULL, doThreadInit, this) != 0)
        {
            MY_LOGE("pthread create failed");
            goto lbExit;
        }
        mbThreadRunning = MTRUE;
        //
        // dual cam zoom not support YUV sensor yet
#if 0
        // workaround: yuv sensor, 3A depends on sensor power-on
        if (NSCam::IHalSensorList::get()->queryType(getOpenId()) == NSCam::NSSensorType::eYUV)
        {
            if (!waitThreadInitDone())
            {
                MY_LOGE("init in thread failed");
                goto lbExit;
            }
        }
#endif
    }
    //--------------------------------------------------------------------------
    //  (1) Open 3A
#if '1'==MTKCAM_HAVE_3A_HAL
    {
        CAM_TRACE_NAME("init(3A)");
        mpHal3a = MAKE_Hal3A(
                getOpenId(),
                LOG_TAG);
        if (! mpHal3a)
        {
            MY_LOGE("IHal3A::createInstance() fail");
            goto lbExit;
        }
        if (mpHal3a != NULL)
        {
            mpHal3a->notifyPwrOn();
        }
        profile.print("3A Hal -");
    }
#endif  //MTKCAM_HAVE_3A_HAL
    //--------------------------------------------------------------------------
#if 0
    {
        CAM_TRACE_NAME("init(base)");
        //  (2) Init Base.
        if (! Cam1DeviceBase::onInit())
        {
            goto lbExit;
        }
    }
    mpParamsMgrV3_Sup = IParamsManagerV3::createInstance(mDevName, 1, mpParamsMgr);
    mpParamsMgrV3_Sup->setStaticData();
#endif
    {
        CAM_TRACE_NAME("init(base)");
        //  (2) Init Base.
        if (! Cam1DeviceBase::onInit())
        {
            goto lbExit;
        }
    }
    mpParamsMgrV3_Sup = IParamsManagerV3::createInstance(mDevName, mSensorId_Main2, mpParamsMgr);
    mpParamsMgrV3_Sup->setStaticData();

    mpBridge = new ImgBufProviderClientBridge();

    //
    //--------------------------------------------------------------------------
    //
    ret = true;
lbExit:
    profile.print("");
    mb4K2KVideoRecord = false;
    MY_LOGD("- ret(%d)", ret);
    updateDeviceState(MtkCameraParameters::KEY_DUALCAM_DEVICE_STATE_IDLE);
    mbIsUninit = false;
    return  ret;
}


/******************************************************************************
 *
 ******************************************************************************/
bool
DualCam1Device::
onUninit()
{
    MY_LOGD("+");
    if(mbIsUninit)
    {
        MY_LOGE("already uninit. skip");
        MY_LOGD("-");
        return true;
    }
    CamProfile  profile(__FUNCTION__, "DefaultCam1Device");
    //
    if (!waitThreadInitDone())
    {
        MY_LOGE("init in thread failed");
    }
    //--------------------------------------------------------------------------
    // reset stereo feature mode
    StereoSettingProvider::setStereoFeatureMode(0);
    StereoSettingProvider::setStereoShotMode(0);
    StereoSettingProvider::setStereoProfile((ENUM_STEREO_SENSOR_PROFILE)0);
    std::future<void> tUninitClient;
    //  (1) Uninit Base
    {
        CAM_TRACE_NAME("uninit(base)");
        //Cam1DeviceBase::onUninit();
        // restore paramV3
        if(mpParamsMgrV3 != NULL)
        {
            mpParamsMgrV3->restoreStaticData();
        }
        else
        {
            MY_LOGW("mpParamsMgrV3 is NULL, no need to restoreStaticData");
        }

        if(mpParamsMgrV3_Sup != NULL)
        {
            mpParamsMgrV3_Sup->restoreStaticData();
        }
        else
        {
            MY_LOGW("mpParamsMgrV3_Sup is NULL, no need to restoreStaticData");
        }
        //
        if (mpDisplayClient != 0)
        {
            mpDisplayClient->uninit();
            mpDisplayClient.clear();
        }
        //
        // use other thread to uninit camera client
        tUninitClient = std::async(
                        std::launch::async,
                        [this]()
                        {
                            if (mpCamClient != 0)
                            {
                                mpCamClient->uninit();
                                mpCamClient.clear();
                            }
                        });
        // forward to registered clients
        Vector<sp<ICamClient> >::const_iterator it;
        for (it = vmpCamClient.begin(); it != vmpCamClient.end(); ++it)
        {
            (*it)->uninit();
        }
        vmpCamClient.clear();
        //
        MY_LOGD("WillDBG uninit Wide");
        if (mpCamAdapter_W != 0)
        {
            mpCamAdapter_W->uninit();
            mpCamAdapter_W = NULL;
        }
        MY_LOGD("WillDBG uninit Tele");
        if (mpCamAdapter_T != 0)
        {
            mpCamAdapter_T->uninit();
            mpCamAdapter_T = NULL;
        }
        MY_LOGD("WillDBG uninit adapter done");
        mpCamAdapter = NULL;
        if (mpBridge != NULL)
        {
            mpBridge->clearImgBufProviderClient();
            mpBridge = NULL;
        }
        //
        mpParamsMgr->uninit();
        //
        //  Uninitialize Device Callback.
        mpCamMsgCbInfo->mCam1DeviceCb = NULL;
        //
        if (mpCpuCtrl != NULL)
        {
            mpCpuCtrl->disable();
        }
        profile.print("Cam1DeviceBase::onUninit() -");
    }
    {
        mSyncMgr = NULL;
    }
    //--------------------------------------------------------------------------
    //  (2) Uninit3A and power off sensor.
    std::vector<std::future<MBOOL> > vThread;
#define UNINIT_PIPELINE(JOB_NAME, EXECTUE_FUNC_NAME, PARAM1, PARAM2)\
    do{\
        struct JOB_NAME\
        {\
            static\
                MBOOL      execute(DualCam1Device* pSelf) {\
                                return pSelf->EXECTUE_FUNC_NAME(pSelf->PARAM1, pSelf->PARAM2);\
                            }\
        };\
        vThread.push_back(std::async(std::launch::async, &JOB_NAME::execute, this));\
    }while(0);
#if '1'==MTKCAM_HAVE_3A_HAL
    UNINIT_PIPELINE(job_uninit3a_main, uninit3A, getOpenId(), mpHal3a);
    UNINIT_PIPELINE(job_uninit3a_main2, uninit3A, getOpenId_Main2(), mpHal3a_2);
#endif  //MTKCAM_HAVE_3A_HAL
#if '1'==MTKCAM_HAVE_SENSOR_HAL
    UNINIT_PIPELINE(job_powerOff_main, powerOffSensor, getOpenId(), mpHalSensor);
    UNINIT_PIPELINE(job_powerOff_main2, powerOffSensor, getOpenId_Main2(), mpHalSensor_2);
#endif  //MTKCAM_HAVE_SENSOR_HAL
#undef UNINIT_PIPELINE
    //--------------------------------------------------------------------------
    //
    // check if tUninitClient is finished.
    if(tUninitClient.valid())
    {
        tUninitClient.get();
    }
    mpScenarioCtrl_main1 = NULL;
    mpScenarioCtrl_main2 = NULL;
    mbIsUninit = true;
    profile.print("");
    MY_LOGD("-");
    return  true;
}


/******************************************************************************
 * [Template method] Called by startPreview().
 ******************************************************************************/
bool
DualCam1Device::
onStartPreview()
{
    bool ret = false;
    CamManager* pCamMgr = CamManager::getInstance();
    //
    //  (0) wait for thread
    if (!mDisableWaitSensorThread)
    {
        if (!waitThreadInitDone())
        {
            MY_LOGE("init in thread failed");
            goto lbExit;
        }
    }
#if 0
    //
    //  (1) Check Permission.
    if (! pCamMgr->getPermission())
    {
        MY_LOGE("Cannot start preview ... Permission denied");
        goto lbExit;
    }
#endif
    //
    //  (2) Update Hal App Mode.
    if (! mpParamsMgr->updateHalAppMode())
    {
        MY_LOGE("mpParamsMgr->updateHalAppMode() fail");
        goto lbExit;
    }
    // set feature mode
    if(setFeatureMode() != OK)
    {
        MY_LOGE("unsupport dual cam mode, please check it");
        goto lbExit;
    }

    //  (3) Initialize Camera Adapter.
    if (! initDualCameraAdapter())
    {
        MY_LOGE("NULL Camera Adapter");
        goto lbExit;
    }
    //
    ret = true;
lbExit:
    return ret;
}


/******************************************************************************
 *  [Template method] Called by stopPreview().
 ******************************************************************************/
void
DualCam1Device::
onStopPreview()
{
    MY_LOGD("+");
    std::future<void> tUninitTask_W = std::async(
                    std::launch::async,
                    [this]()
                    {
                        if (mpCamAdapter_W != 0)
                        {
                            mpCamAdapter_W->cancelPicture();
                            mpCamAdapter_W->uninit();
                            mpCamAdapter_W = NULL;
                        }
                    });
    if (mpCamAdapter_T != 0)
    {
        mpCamAdapter_T->cancelPicture();
        mpCamAdapter_T->uninit();
        mpCamAdapter_T = NULL;
    }
    if(tUninitTask_W.valid())
    {
        tUninitTask_W.get();
    }
    mpCamAdapter = NULL;
    mpBridge->clearImgBufProviderClient();
    MY_LOGD("-");
}


/******************************************************************************
 * Create a thread to hide some initial steps to speed up launch time
 ******************************************************************************/
bool
DualCam1Device::
powerOnSensor()
{
    MY_LOGD("+");
    bool    ret = false;
    //  (1) Open Sensor
#if '1'==MTKCAM_HAVE_SENSOR_HAL
    //
    NSCam::Utils::CamManager::getInstance()->incSensorCount(LOG_TAG);
    NSCam::Utils::CamManager::getInstance()->incSensorCount(LOG_TAG);
    //
    NSCam::IHalSensorList* pHalSensorList = MAKE_HalSensorList();
    MUINT pIndex_Main1[1] = { (MUINT)getOpenId()};
    MUINT pIndex_Main2[1] = { (MUINT)getOpenId_Main2()};
    MUINT const main1Index = getOpenId();
    MUINT const main2Index = getOpenId_Main2();

    mpScenarioCtrl_main1 = NSCam::IScenarioControl::create(getOpenId());
    mpScenarioCtrl_main2 = NSCam::IScenarioControl::create(getOpenId_Main2());
    if(!pHalSensorList)
    {
        MY_LOGE("pHalSensorList == NULL");
        goto lbExit;
    }
    //
    mpHalSensor = pHalSensorList->createSensor(
                                        USER_NAME,
                                        1,
                                        pIndex_Main1);
    if(mpHalSensor == NULL)
    {
       MY_LOGE("mpHalSensor is NULL");
       goto lbExit;
    }

    //
    mpHalSensor_2 = pHalSensorList->createSensor(
                                        USER_NAME,
                                        1,
                                        pIndex_Main2);
    if(mpHalSensor_2 == NULL)
    {
       MY_LOGE("mpHalSensor_2 is NULL");
       goto lbExit;
    }

    MY_LOGD("powerOn dual sensors 1-by-1");
    if( !mpHalSensor->powerOn(USER_NAME, 1, &main1Index) )
    {
        MY_LOGE("sensor power on failed: %d", pIndex_Main1[0]);
        NSCam::Utils::CamManager::getInstance()->decSensorCount(LOG_TAG);
        goto lbExit;
    }
    if( !mpHalSensor_2->powerOn(USER_NAME, 1, &main2Index) )
    {
        MY_LOGE("sensor power on failed: %d", pIndex_Main2[0]);
        NSCam::Utils::CamManager::getInstance()->decSensorCount(LOG_TAG);
        goto lbExit;
    }

    mpHal3a_2 = MAKE_Hal3A(
                getOpenId_Main2(),
                LOG_TAG);
    if (! mpHal3a_2)
    {
        MY_LOGE("IHal3A::createInstance() fail: main2");
        goto lbExit;
    }

    MY_LOGD("notifyPwrOn");
    if (mpHal3a_2 != NULL)
    {
        mpHal3a_2->notifyPwrOn();
    }
    //
#endif  //MTKCAM_HAVE_SENSOR_HAL
    ret = true;
lbExit:
    MY_LOGD("-");
    return ret;
}


/******************************************************************************
 * the init function to be called in the thread
 ******************************************************************************/
void*
DualCam1Device::
doThreadInit(void* arg)
{
    ::prctl(PR_SET_NAME, "initCamdevice", 0, 0, 0);
    DualCam1Device* pSelf = reinterpret_cast<DualCam1Device*>(arg);
    pSelf->mRet = pSelf->powerOnSensor();
    pthread_exit(NULL);
    return NULL;
}


/******************************************************************************
 * Wait for initializations by thread are done.
 ******************************************************************************/
bool
DualCam1Device::
waitThreadInitDone()
{
    bool ret = false;
    if (mbThreadRunning)
    {
        MY_LOGD("wait init done +");
        int s = pthread_join(mThreadHandle, NULL);
        MY_LOGD("wait init done -");
        mbThreadRunning = MFALSE;
        if (s != 0)
        {
            MY_LOGE("pthread join error: %d", s);
            goto lbExit;
        }

        if (!mRet)
        {
            MY_LOGE("init in thread failed");
            goto lbExit;
        }
    }

    ret = true;
lbExit:
    return ret;
}


/******************************************************************************
 *  Set the camera parameters. This returns BAD_VALUE if any parameter is
 *  invalid or not supported.
 ******************************************************************************/
status_t
DualCam1Device::
onSetParameters(const char* params    __attribute__((unused)))
{
    MY_LOGD("");
    status_t status = OK;
    //  Here (1) succeeded.
    //  (2) If CamAdapter exists, apply mpParamsMgr to CamAdapter;
    //      otherwise it will be applied when CamAdapter is created.
    {
#if 0 //TBD
#if '1'==MTKCAM_HAVE_3A_HAL
        else if (mpHal3a)
        {
            //  Flashlight may turn on/off in case that CamAdapter doesn't exist (i.e. never call startPreview)

            using namespace NS3Av3;
            Param_T param;
            //
            if (! mpHal3a->getParams(param))
            {
                MY_LOGW("3A Hal::getParams() fail - err(%x)", mpHal3a->getErrorCode());
            }
            //
            String8 const s8FlashMode = mpParamsMgr->getStr(CameraParameters::KEY_FLASH_MODE);
            if (! s8FlashMode.isEmpty())
            {
                param.u4StrobeMode = PARAMSMANAGER_MAP_INST(eMapFlashMode)->valueFor(s8FlashMode);
            }
            //
            if (! mpHal3a->setParams(param))
            {
                MY_LOGW("3A Hal::setParams() fail - err(%x)", mpHal3a->getErrorCode());
            }
        }
        else
        {
            MY_LOGW("mpHal3a==NULL");
        }
#endif
#endif
    }
#if '1' == MTKCAM_HR_MONITOR_SUPPORT
    {
        sp<ICamClient> pCamClient = mpCamClient;
        //if  ( pCamClient != 0 && INVALID_OPERATION != (status = pCamClient->sendCommand(CAMERA_CMD_CHECKPARA_HR_PREVIEW, 0, 0)) )
        if (pCamClient != 0)
        {
            //  we just return since this cammand has been handled.
            //return  status;
            pCamClient->sendCommand(CAMERA_CMD_CHECKPARA_HR_PREVIEW, 0, 0);
        }
    }
#endif

lbExit:
    return  status;
}


/******************************************************************************
 *
 *
 ******************************************************************************/
bool
DualCam1Device::
disableWaitSensorThread(bool diable)
{
    MY_LOGD("diable(%d)", diable);
    mDisableWaitSensorThread = diable;
    return true;
}

// Overwrite Cam1DeviceBase
/******************************************************************************
 *  Start preview mode.
 ******************************************************************************/
status_t
DualCam1Device::
startPreview()
{
    CAM_TRACE_CALL();
    MY_LOGI("+");
    //
    status_t status = OK;
    bool usePreviewThread = false;
    CamManager* pCamMgr = CamManager::getInstance();
    //
    /*if( mpParamsMgr->getIfFirstPreviewFrameAsBlack() &&
        mbWindowReady == false)
    {
        usePreviewThread = true;
        disableWaitSensorThread(true);
    }*/
    //
    {
        CAM_TRACE_NAME("deviceStartPreview");
        if (mpCamAdapter != 0 && mpCamAdapter->isTakingPicture())
        {
            MY_LOGE("Capture is not done");
            status = INVALID_OPERATION;
            return  status;
        }
        //
        if (previewEnabled())
        {
            MY_LOGD("Preview already running");
            status = ALREADY_EXISTS;
            return  status;
        }
        //  Check Permission.
        if (! pCamMgr->getPermission())
        {
            MY_LOGE("Cannot start preview ... Permission denied");
            sp<IFrameworkCBThread> spFrameworkCBThread = IFrameworkCBThread::createInstance(getOpenId(),
                    mpCamMsgCbInfo);
            spFrameworkCBThread->init();
            IFrameworkCBThread::callback_data cbData;
            cbData.callbackType = IFrameworkCBThread::CALLBACK_TYPE_NOTIFY;
            cbData.type         = CAMERA_MSG_ERROR;
            cbData.ext1         = CAMERA_ERROR_SERVER_DIED;
            cbData.ext2         = 0;
            spFrameworkCBThread->postCB(cbData);
            spFrameworkCBThread->uninit();
            spFrameworkCBThread = NULL;
            status = OK;
            goto lbExit;
        }
        //
        if (! onStartPreview())
        {
            MY_LOGE("onStartPreviewLocked() fail");
            status = INVALID_OPERATION;
            goto lbExit;
        }
    }
    //
    {
        CAM_TRACE_NAME("clientStartPreview");
        if (mpDisplayClient == 0)
        {
            MY_LOGD("DisplayClient is not ready.");
        }
        else if (OK != (status = enableDisplayClient()))
        {
            goto lbExit;
        }
        //
        if (mpCamClient != 0)
        {
            if (! mpCamClient->startPreview())
            {
                status = INVALID_OPERATION;
                goto lbExit;
            }
        }
        // forward to registered clients
        Vector<sp<ICamClient> >::const_iterator it;
        for (it = vmpCamClient.begin(); it != vmpCamClient.end(); ++it)
        {
            (*it)->startPreview();
        }
    }

    //
    //  startPreview in Camera Adapter.
    {
        if (usePreviewThread)
        {
            if (pthread_create(&mStartPreviewTThreadHandle, NULL, startPreviewThread, this) != 0)
            {
                ALOGE("startPreview pthread create failed");
            }
        }
        else
        {
            CAM_TRACE_NAME("adapterStartPreview");
            status = mpCamAdapter_W->startPreview();
            if (OK != status)
            {
                MY_LOGE("startPreview() in CameraAdapter returns: [%s(%d)]", ::strerror(-status), -status);
                sp<IFrameworkCBThread> spFrameworkCBThread = IFrameworkCBThread::createInstance(getOpenId(),mpCamMsgCbInfo);
                spFrameworkCBThread->init();
                IFrameworkCBThread::callback_data cbData;
                cbData.callbackType = IFrameworkCBThread::CALLBACK_TYPE_NOTIFY;
                cbData.type         = CAMERA_MSG_ERROR;
                cbData.ext1         = CAMERA_ERROR_SERVER_DIED;
                cbData.ext2         = 0;
                spFrameworkCBThread->postCB(cbData);
                spFrameworkCBThread->uninit();
                spFrameworkCBThread = NULL;
                status = OK;
                MY_LOGE("startPreview fail, return OK, callback CAMERA_ERROR_SERVER_DIED");
                goto lbExit;
            }
            // register callback to syncmanager
            {
                mSyncMgr = NSCam::ISyncManager::getInstance(getOpenId());
                if (mSyncMgr != NULL)
                {
                    mSyncMgr->registerMgrCb(this);
                    mSyncMgr = NULL;
                }
                else
                {
                    MY_LOGW("Cannot get syncmanager in start preview");
                }
            }
            if (mpCamAdapter_T != 0)
            {
                status = mpCamAdapter_T->startPreview();
                if (OK != status)
                {
                    MY_LOGE("startPreview() in CameraAdapter2 returns: [%s(%d)]", ::strerror(-status), -status);
                    goto lbExit;
                }
            }
        }
    }
    //
    //
    enableMsgType(CAMERA_MSG_PREVIEW_METADATA);
    //
    mIsPreviewEnabled = true;

    if (mpParamsMgr->getRecordingHint())
    {
        MSize paramSize;
        mpParamsMgr->getVideoSize(&paramSize.w, &paramSize.h);
        mb4K2KVideoRecord = (paramSize.w*paramSize.h > IMG_1080P_SIZE) ? MTRUE : MFALSE;
    }
    //
    status = OK;
lbExit:
    if (OK != status)
    {
        MY_LOGD("Cleanup after error");
        //
        if (mpCamClient != 0)
        {
            mpCamClient->stopPreview();
        }
        // forward to registered clients
        Vector<sp<ICamClient> >::const_iterator it;
        for (it = vmpCamClient.begin(); it != vmpCamClient.end(); ++it)
        {
            (*it)->stopPreview();
        }
        //
        disableDisplayClient();
    }
    else
    {
        updateDeviceState(MtkCameraParameters::KEY_DUALCAM_DEVICE_STATE_PREVIEWING);
    }
    //
    MY_LOGI("- status(%d)", status);
    return  status;
}

/******************************************************************************
 *  Stop a previously started preview.
 ******************************************************************************/
void
DualCam1Device::
stopPreview()
{
    CAM_TRACE_CALL();
    MY_LOGI("+");

    //
    disableMsgType(CAMERA_MSG_PREVIEW_METADATA);
    //
    waitStartPreviewDone();
    //
    if (! previewEnabled())
    {
        MY_LOGD("Preview already stopped, perhaps!");
        MY_LOGD("We still force to clean up again.");
    }
    // flush hwsync
    mSyncMgr = NSCam::ISyncManager::getInstance(getOpenId());
    if (mSyncMgr != NULL)
    {
        mSyncMgr->setEvent(0, NULL);
        mSyncMgr = NULL;
    }
    else
    {
        MY_LOGW("Cannot get syncmanager in stop preview. Can't flush hwsync");
    }
    // stop wide adapter simultaneously
    std::function<void(void)> stopAction_W =
                    [this]()mutable{
                        CAM_TRACE_NAME("adapterStopPreview");
                        if (mpCamAdapter_W != 0)
                        {
                            if (mpCamAdapter_W->recordingEnabled())
                            {
                                stopRecording();
                            }
                            mpCamAdapter_W->stopPreview();
                        }
                    };
    std::future<void> stopPreviewCmd_W =
                        std::async(
                                    std::launch::async,
                                    stopAction_W);
    //
    {
        CAM_TRACE_NAME("adapterStopPreview");
        if (mpCamAdapter_T != 0)
        {
            if (mpCamAdapter_T->recordingEnabled())
            {
                stopRecording();
            }
            mpCamAdapter_T->stopPreview();
        }
    }
    // join stop thread
    if(stopPreviewCmd_W.valid())
    {
        MY_LOGD("have share state");
        stopPreviewCmd_W.get();
    }
    //
    {
        CAM_TRACE_NAME("clientStopPreview");
        if (mpCamClient != 0)
        {
            mpCamClient->stopPreview();
        }
        //
        Vector<sp<ICamClient> >::const_iterator it;
        for (it = vmpCamClient.begin(); it != vmpCamClient.end(); ++it)
        {
            (*it)->stopPreview();
        }
        //
        disableDisplayClient();
    }
    //
    //
    {
        CAM_TRACE_NAME("deviceStopPreview");
        onStopPreview();
    }
    //
    //
#if 1
    if (mpDisplayClient != 0)
    {
        mpDisplayClient->waitUntilDrained();
    }
#endif
    //
    //
lbExit:
    //  Always set it to false.
    mIsPreviewEnabled = false;
    mb4K2KVideoRecord = false;
    updateDeviceState(MtkCameraParameters::KEY_DUALCAM_DEVICE_STATE_IDLE);
    MY_LOGI("-");
}


/******************************************************************************
 * Set the camera parameters. This returns BAD_VALUE if any parameter is
 * invalid or not supported.
 ******************************************************************************/
status_t
DualCam1Device::
setParameters(const char* params)
{
    CAM_TRACE_CALL();
    MY_LOGD("");
    waitStartPreviewDone();
    status_t status = OK;
    //char* zoom;
    //char* zoom_ratio;
    //
    //  (1) Update params to mpParamsMgr.
    status = mpParamsMgr->setParameters(String8(params));
    if (OK != status)
    {
        goto lbExit;
    }

    //  Here (1) succeeded.
    //  (2) If CamAdapter exists, apply mpParamsMgr to CamAdapter;
    //      otherwise it will be applied when CamAdapter is created.
    /*{
        String8 zoom = mpParamsMgr->getStr(MtkCameraParameters::KEY_ZOOM);
        String8 zoom_ratio = mpParamsMgr->getStr(MtkCameraParameters::KEY_ZOOM_RATIOS);
        MY_LOGD("Willdbg Key zoom : %s, key zoom table : %s", zoom.string(), zoom_ratio.string());
        unsigned int zoomdata = mpParamsMgr->getZoomRatio(mi4OpenId);
        MY_LOGD("Willdbg zoom data : %d", zoomdata);
    }*/
    {
        /*unsigned int zoomIndex = mpParamsMgr->getInt(MtkCameraParameters::KEY_ZOOM);
        MY_LOGD("Willdbg zoom index : %d", zoomIndex);
        if (zoomIndex >= mpParamsMgr->getInt(MtkCameraParameters::KEY_MAX_ZOOM) / 2)
        {
            if (mpCamAdapter == mpCamAdapter_W && mpCamAdapter_W != 0 && mpCamAdapter_T != 0)
            {
                mpCamAdapter = mpCamAdapter_T;
            }
        }
        else
        {
            if (mpCamAdapter == mpCamAdapter_T && mpCamAdapter_W != 0 && mpCamAdapter_T != 0)
            {
                mpCamAdapter = mpCamAdapter_W;
            }
        }*/
        if (mpCamAdapter_W != 0)
        {
            status = mpCamAdapter_W->setParameters();

        }
        if (mpCamAdapter_T != 0)
        {
            status = mpCamAdapter_T->setParameters();
        }
    }

    status = onSetParameters(params);

lbExit:
    return  status;
}

/******************************************************************************
 *
 ******************************************************************************/
bool
DualCam1Device::
initDualCameraAdapter()
{
    bool ret = false;
    bool WideIsValid = false;
    bool TeleIsValid = false;
    //
    //  (0) setup imgRatio for StereoSettingProvider
    {
        int iPreviewWidth = 0, iPreviewHeight = 0;
        mpParamsMgr->getPreviewSize(&iPreviewWidth, &iPreviewHeight);

        MY_LOGD("width(%d) height(%d)", iPreviewWidth, iPreviewHeight);
        double ratio_4_3 = 4.0/3.0;
        double preview_ratio = ((double)iPreviewWidth) / ((double)iPreviewHeight);
        if(preview_ratio == ratio_4_3)
        {
            MY_LOGD("set to 4:3");
            StereoSettingProvider::setImageRatio(eRatio_4_3);
        }
        else
        {
            MY_LOGD("set to 16:9");
            StereoSettingProvider::setImageRatio(eRatio_16_9);
        }
    }
    //
    //  (1) Check to see if CamAdapter has existed or not.
    //mpCamAdapter = NULL;
    mpBridge->clearImgBufProviderClient();
    if (mpCamAdapter_W != 0)
    {
        if (ICamAdapter::isValidInstance(mpCamAdapter_W))
        {
            // do nothing & just return true if the same app.
            MY_LOGD("valid camera adapter: %s", mpCamAdapter_W->getName());
            WideIsValid = true;
        }
        else
        {
            // cleanup the original one if different app.
            MY_LOGW("invalid camera adapter: %s", mpCamAdapter_W->getName());
            mpCamAdapter_W->uninit();
            mpCamAdapter_W.clear();
        }
    }
    if (mpCamAdapter_T != 0 && StereoSettingProvider::isDualCamMode())
    {
        if (ICamAdapter::isValidInstance(mpCamAdapter_T))
        {
            // do nothing & just return true if the same app.
            MY_LOGD("valid camera adapter: %s", mpCamAdapter_T->getName());
            TeleIsValid = true;
        }
        else
        {
            // cleanup the original one if different app.
            MY_LOGW("invalid camera adapter: %s", mpCamAdapter_T->getName());
            mpCamAdapter_T->uninit();
            mpCamAdapter_T.clear();
        }
    }
    //
    //  (2) Create & init a new CamAdapter.
    if(!WideIsValid) {
        mpCamAdapter_W = ICamAdapter::createInstance(mDevName, mi4OpenId, mpParamsMgrV3);
        if (mpCamAdapter_W != 0 && mpCamAdapter_W->init())
        {
            //  (.1) init.
            mpCamAdapter_W->setCallbacks(mpCamMsgCbInfo);
            mpCamAdapter_W->enableMsgType(mpCamMsgCbInfo->mMsgEnabled);

            //  (.2) Invoke its setParameters
            if (OK != mpCamAdapter_W->setParameters())
            {
                //  If fail, it should destroy instance before return.
                MY_LOGE("mpCamAdapter_W->setParameters() fail");
                goto lbExit;
            }

            //  (.3) Send to-do commands.
            {
                Mutex::Autolock _lock(mTodoCmdMapLock);
                for (size_t i = 0; i < mTodoCmdMap.size(); i++)
                {
                    CommandInfo const& rCmdInfo = mTodoCmdMap.valueAt(i);
                    MY_LOGD("send queued cmd(%#x),args(%d,%d)", rCmdInfo.cmd, rCmdInfo.arg1, rCmdInfo.arg2);
                    mpCamAdapter_W->sendCommand(rCmdInfo.cmd, rCmdInfo.arg1, rCmdInfo.arg2);
                }
                mTodoCmdMap.clear();
            }

        }
        else
        {
            MY_LOGE("mpCamAdapter_W(%p)->init() fail", mpCamAdapter_W.get());
            goto lbExit;
        }
        mpCamAdapter = mpCamAdapter_W;
    }
    if (!TeleIsValid && StereoSettingProvider::isDualCamMode())
    {
        mpCamAdapter_T = ICamAdapter::createInstance(mDevName, mSensorId_Main2, mpParamsMgrV3_Sup);
        if (mpCamAdapter_T != 0 && mpCamAdapter_T->init())
        {
            //  (.1) init.
            mpCamAdapter_T->setCallbacks(mpCamMsgCbInfo);
            mpCamAdapter_T->enableMsgType(mpCamMsgCbInfo->mMsgEnabled);

            //  (.2) Invoke its setParameters
            if (OK != mpCamAdapter_T->setParameters())
            {
                //  If fail, it should destroy instance before return.
                MY_LOGE("mpCamAdapter->setParameters() fail");
                goto lbExit;
            }

            //  (.3) Send to-do commands.
            {
                Mutex::Autolock _lock(mTodoCmdMapLock);
                for (size_t i = 0; i < mTodoCmdMap.size(); i++)
                {
                    CommandInfo const& rCmdInfo = mTodoCmdMap.valueAt(i);
                    MY_LOGD("send queued cmd(%#x),args(%d,%d)", rCmdInfo.cmd, rCmdInfo.arg1, rCmdInfo.arg2);
                    mpCamAdapter_T->sendCommand(rCmdInfo.cmd, rCmdInfo.arg1, rCmdInfo.arg2);
                }
                mTodoCmdMap.clear();
            }

        }
        else
        {
            MY_LOGE("mpCamAdapter_T(%p)->init() fail", mpCamAdapter_T.get());
            goto lbExit;
        }
    }
    //
    mpBridge->addImgBufProviderClient(mpCamAdapter_W);
    if (mpCamAdapter_T != 0)
        mpBridge->addImgBufProviderClient(mpCamAdapter_T);

    if (mpDisplayClient != 0 && ! mpDisplayClient->setImgBufProviderClient(mpBridge))
    {
        MY_LOGE("mpDisplayClient->setImgBufProviderClient() fail");
        goto lbExit;
    }
    //  (.5) [CamClient] set Image Buffer Provider Client if needed.
    if (mpCamClient != 0 && ! mpCamClient->setImgBufProviderClient(mpBridge))
    {
        MY_LOGE("mpCamClient->setImgBufProviderClient() fail");
        goto lbExit;
    }
    //
    Vector<sp<ICamClient> >::const_iterator it;
    for (it = vmpCamClient.begin(); it != vmpCamClient.end(); ++it)
    {
        (*it)->setImgBufProviderClient(mpBridge) ;
    }
    ret = true;
lbExit:

    return ret;
}


/******************************************************************************
 *
 ******************************************************************************/
status_t
DualCam1Device::
initDisplayClient(preview_stream_ops* window)
{
    CAM_TRACE_CALL();
#if 0//'1'!=MTKCAM_HAVE_DISPLAY_CLIENT
#warning "Not Build Display Client"
    MY_LOGD("Not Build Display Client");
    return  OK;
#else
    status_t status = OK;
    Size previewSize;
    //
    MY_LOGD("+ window(%p)", window);
    //
    //
    //  [1] Check to see whether the passed window is NULL or not.
    if (! window)
    {
        MY_LOGW("NULL window is passed into...");
        mbWindowReady = false;
        //
        if (mpDisplayClient != 0)
        {
            MY_LOGW("destroy the current display client(%p)...", mpDisplayClient.get());
            mpDisplayClient->uninit();
            mpDisplayClient.clear();
        }
        status = OK;
        goto lbExit;
    }
    mbWindowReady = true;
    //
    //
    //  [2] Get preview size.
    if (! queryPreviewSize(previewSize.width, previewSize.height))
    {
        MY_LOGE("queryPreviewSize");
        status = DEAD_OBJECT;
        goto lbExit;
    }
    //
    //
    //  [3] Initialize Display Client.
    if (mpDisplayClient != 0)
    {
        if (previewEnabled())
        {
            MY_LOGW("Do nothing since Display Client(%p) is already created after startPreview()",
                    mpDisplayClient.get());
            //          This method must be called before startPreview(). The one exception is that
            //          if the preview surface texture is not set (or set to null) before startPreview() is called,
            //          then this method may be called once with a non-null parameter to set the preview surface.
            status = OK;
            goto lbExit;
        }
        else
        {
            MY_LOGW("New window is set after stopPreview or takePicture. Destroy the current display client(%p)...",
                    mpDisplayClient.get());
            mpDisplayClient->uninit();
            mpDisplayClient.clear();
        }
    }
    //  [3.1] create a Display Client.
    mpDisplayClient = IDisplayClient::createInstance();
    if (mpDisplayClient == 0)
    {
        MY_LOGE("Cannot create mpDisplayClient");
        status = NO_MEMORY;
        goto lbExit;
    }
    //  Display Rotation
    if (mpParamsMgr->getDisplayRotationSupported())
    {
        MY_LOGD("orientation = %d", mOrientation);
        mpDisplayClient->SetOrientationForDisplay(mOrientation);
    }
    //  [3.2] initialize the newly-created Display Client.
    if (! mpDisplayClient->init())
    {
        MY_LOGE("mpDisplayClient init() failed");
        mpDisplayClient->uninit();
        mpDisplayClient.clear();
        status = NO_MEMORY;
        goto lbExit;
    }
    //  [3.3] set preview_stream_ops & related window info.
    if (! mpDisplayClient->setWindow(window, previewSize.width, previewSize.height,
                                     queryDisplayBufCount()))
    {
        status = INVALID_OPERATION;
        goto lbExit;
    }

    //  [3.4] set Image Buffer Provider Client if it exist.
    if (mpBridge != 0 && ! mpDisplayClient->setImgBufProviderClient(mpBridge))
    {
        status = INVALID_OPERATION;
        goto lbExit;
    }
    //
    //
    status = OK;
    //
lbExit:
    if (OK != status)
    {
        MY_LOGD("Cleanup...");
        mpDisplayClient->uninit();
        mpDisplayClient.clear();
    }
    //
    MY_LOGD("- status(%d)", status);
    return  status;
#endif//MTKCAM_HAVE_DISPLAY_CLIENT
}


/******************************************************************************
 *
 ******************************************************************************/
status_t
DualCam1Device::
enableDisplayClient()
{
    status_t status = OK;
    Size previewSize;
    //
    MY_LOGD("+");
    //
    //  [1] Get preview size.
    if (! queryPreviewSize(previewSize.width, previewSize.height))
    {
        MY_LOGE("queryPreviewSize");
        status = DEAD_OBJECT;
        goto lbExit;
    }
    //
    if (mpParamsMgr->getIfFirstPreviewFrameAsBlack())
    {
        mpDisplayClient->setFirstFrameBlack();
        mpParamsMgr->set(MtkCameraParameters::KEY_FIRST_PREVIEW_FRAME_BLACK, 0);
    }
    //  [2] Enable
    if (! mpDisplayClient->enableDisplay(previewSize.width, previewSize.height, queryDisplayBufCount(),
                                         mpBridge))
    {
        MY_LOGE("mpDisplayClient(%p)->enableDisplay()", mpDisplayClient.get());
        status = INVALID_OPERATION;
        goto lbExit;
    }
    //
    status = OK;
lbExit:
    MY_LOGD("- status(%d)", status);
    return  status;
}

/******************************************************************************
 * logic to this function should be the same as
 * FeatureFlowControl::needReconstructRecordingPipe()
 ******************************************************************************/
MBOOL
DualCam1Device::
needReconstructRecordingPipe(sp<IParamsManagerV3> param, MBOOL mIsRecordMode)
{
    MSize paramSize;
    param->getParamsMgr()->getVideoSize(&paramSize.w, &paramSize.h);
    MBOOL b4K2KVideoRecord = (paramSize.w*paramSize.h > IMG_1080P_SIZE) ? MTRUE : MFALSE;

    MBOOL ret = MFALSE;

    // mb4K2KVideoRecord in FeatureFlowControl is assigned in constructRecordingPipeline()
    // which is called after FeatureFlowControl::needReconstructRecordingPipe()
    // the value is previous setting (ex: video mode and only preview), not current operation
    // as the timing here in DualCam1Device, it means current operation.
    //
    // DualCam1Device::mb4K2KVideoRecord     (it means current operation)
    // FeatureFlowControl::mb4K2KVideoRecord (it means previous setting)
    //
    // so 4k2k force to reconstruct when recording
    if( mb4K2KVideoRecord != b4K2KVideoRecord)
    {
        ret = MTRUE;
    }
    MY_LOGD("(0x%p) param(%dx%d), b4K2K(%d), ret(%d)", param.get(),
             paramSize.w, paramSize.h, mb4K2KVideoRecord, ret);
    return ret;
}


/******************************************************************************
 *
 ******************************************************************************/
void
DualCam1Device::
updateDeviceState(MINT32 state)
{
    mpParamsMgr->set(MtkCameraParameters::KEY_DUALCAM_DEVICE_STATE, state);
}



/******************************************************************************
 * Start record mode. When a record image is available a CAMERA_MSG_VIDEO_FRAME
 * message is sent with the corresponding frame. Every record frame must be released
 * by a cameral hal client via releaseRecordingFrame() before the client calls
 * disableMsgType(CAMERA_MSG_VIDEO_FRAME). After the client calls
 * disableMsgType(CAMERA_MSG_VIDEO_FRAME), it is camera hal's responsibility
 * to manage the life-cycle of the video recording frames, and the client must
 * not modify/access any video recording frames.
 ******************************************************************************/
status_t
DualCam1Device::
startRecording()
{
    CAM_TRACE_CALL();
    MY_LOGI("+");
    //
    status_t status = OK;
    //
    if (mpCamAdapter == 0)
    {
        MY_LOGE("NULL Camera Adapter");
        status = DEAD_OBJECT;
        goto lbExit;
    }
    //  startRecording in Camera Adapter.
    {
        // *****************************************************************************************
        // stopPreview() to make sure both wide/tele DualCamStreamFeaturePipe are destroyed
        // (destroy Cam0, Cam2 (SyncManager ref count:2->1->0(end)) and then create Cam0 and Cam2)
        //
        // without stop tele, FeatureFlowControl::needReconstructRecordingPipe() would re-create.
        // the SyncManager ref count:(2->1->2)->1->2 would not be zero
        // *****************************************************************************************
        MBOOL needReconstruct = needReconstructRecordingPipe(mpParamsMgrV3, true);
        needReconstruct = needReconstruct || needReconstructRecordingPipe(mpParamsMgrV3_Sup, true);

        if (mpCamAdapter_T != 0 && needReconstruct)
        {
            MY_LOGD("Tele stopPreview()");
            // flush hwsync
            mSyncMgr = NSCam::ISyncManager::getInstance(getOpenId());
            if (mSyncMgr != NULL)
            {
                mSyncMgr->setEvent(0, NULL);
                mSyncMgr = NULL;
            }
            else
            {
                MY_LOGW("Cannot get syncmanager in stop preview. Can't flush hwsync");
            }
            //
            mpCamAdapter_T->stopPreview();
        }

        CAM_TRACE_NAME("adapterStartRecording");
        MY_LOGD("Wide startRecording()");
        status = mpCamAdapter_W->startRecording();
        if (OK != status)
        {
            MY_LOGE("Wide startRecording() in CameraAdapter returns: [%s(%d)]", ::strerror(-status), -status);
            goto lbExit;
        }

        if (mpCamAdapter_T != 0)
        {
            if (needReconstruct)
            {
                MY_LOGD("Tele startPreview()");
                mpParamsMgr->setRecordLikely(true);
                mpCamAdapter_T->startPreview();
            }
            MY_LOGD("Tele startRecording()");
            status = mpCamAdapter_T->startRecording();
            if (needReconstruct)
            {
                mpParamsMgr->setRecordLikely(false);
                // register callback to syncmanager
                mSyncMgr = NSCam::ISyncManager::getInstance(getOpenId());
                if (mSyncMgr != NULL)
                {
                    mSyncMgr->registerMgrCb(this);
                    mSyncMgr = NULL;
                }
                else
                {
                    MY_LOGW("Cannot get syncmanager in start record");
                }
            }
        }

        if (OK != status)
        {
            MY_LOGE("Tele startRecording() in CameraAdapter returns: [%s(%d)]", ::strerror(-status), -status);
            goto lbExit;
        }
    }
    //
    {
        CAM_TRACE_NAME("clientStartRecording");
        if (mpCamClient != 0)
        {
            //  Get recording format & size.
            //  Set.
            if (! mpCamClient->startRecording())
            {
                status = INVALID_OPERATION;
                goto lbExit;
            }
        }
        // forward to registered clients
        Vector<sp<ICamClient> >::const_iterator it;
        for (it = vmpCamClient.begin(); it != vmpCamClient.end(); ++it)
        {
            (*it)->startRecording();
        }
    }
lbExit:
    if (OK == status)
    {
        updateDeviceState(MtkCameraParameters::KEY_DUALCAM_DEVICE_STATE_RECORDING);
    }
    return  status;
}


/******************************************************************************
 *  Stop a previously started recording.
 ******************************************************************************/
void
DualCam1Device::
stopRecording()
{
    CAM_TRACE_CALL();
    MY_LOGI("+");
    //
    {
        CAM_TRACE_NAME("adapterStopRecording");
        if (mpCamAdapter_W != 0)
        {
            mpCamAdapter_W->stopRecording();
        }
        if (mpCamAdapter_T != 0)
        {
            mpCamAdapter_T->stopRecording();
        }
    }
    //
    {
        CAM_TRACE_NAME("clientStopRecording");
        if (mpCamClient != 0)
        {
            mpCamClient->stopRecording();
        }
        // forward to registered clients
        Vector<sp<ICamClient> >::const_iterator it;
        for (it = vmpCamClient.begin(); it != vmpCamClient.end(); ++it)
        {
            (*it)->stopRecording();
        }
    }



    // ************************************************************************
    //
    // needReconstruct shall has the same logic as startRecording()
    //
    // ************************************************************************
    MBOOL needReconstruct = needReconstructRecordingPipe(mpParamsMgrV3, true);
    needReconstruct = needReconstruct || needReconstructRecordingPipe(mpParamsMgrV3_Sup, true);

    if (needReconstruct)
    {
        // flush hwsync
        mSyncMgr = NSCam::ISyncManager::getInstance(getOpenId());
        if (mSyncMgr != NULL)
        {
            mSyncMgr->setEvent(0, NULL);
            mSyncMgr = NULL;
        }
        else
        {
            MY_LOGW("Cannot get syncmanager in stop preview. Can't flush hwsync");
        }
        // reset low layer flag and reduce the resource (e.g. 4K to FHD)
        if (mpCamAdapter_W != 0)
        {
            MY_LOGD("Wide stopPreview()");
            mpCamAdapter_W->stopPreview();
        }
        if (mpCamAdapter_T != 0)
        {
            MY_LOGD("Tele stopPreview()");
            mpCamAdapter_T->stopPreview();
        }

        // re-start
        if (mpCamAdapter_W != 0)
        {
            MY_LOGD("Wide startPreview()");
            mpCamAdapter_W->startPreview();
        }
        if (mpCamAdapter_T != 0)
        {
            MY_LOGD("Tele startPreview()");
            mpCamAdapter_T->startPreview();
        }
    }

    updateDeviceState(MtkCameraParameters::KEY_DUALCAM_DEVICE_STATE_PREVIEWING);
}

/******************************************************************************
 *  Take a picture.
 ******************************************************************************/
status_t
DualCam1Device::
takePicture()
{
    CAM_TRACE_CALL();
    MY_LOGI("+");
    //
    status_t status = OK;
    //
    if  ( mpCamAdapter == 0 )
    {
        MY_LOGE("NULL Camera Adapter");
        status = DEAD_OBJECT;
        goto lbExit;
    }
    //
    if(!(mLastEnableMsg & CAMERA_MSG_SHUTTER))
    {
        MY_LOGD("Disable shutter sound");
        disableMsgType(CAMERA_MSG_SHUTTER);
    }
    //
    {
        CAM_TRACE_NAME("clientTakePicture");
        if(!recordingEnabled())
        {
            disableMsgType(CAMERA_MSG_PREVIEW_METADATA);
        }
        //
        if  ( mpCamClient != 0 )
        {
            mpCamClient->takePicture();
        }
        // forward to registered clients
        Vector<sp<ICamClient> >::const_iterator it;
        for (it = vmpCamClient.begin(); it != vmpCamClient.end(); ++it)
        {
            (*it)->takePicture();
        }
    }
    //
    {
        int featureMode = StereoSettingProvider::getStereoFeatureMode();
        if(featureMode == E_DUALCAM_FEATURE_ZOOM)
        {
            sp<ISyncManager> pSyncMgr = ISyncManager::getInstance(getOpenId());
            if(pSyncMgr!=nullptr){
                pSyncMgr->lock(nullptr);
            }
        }
        Mutex::Autolock _l(mSyncLock);
        CAM_TRACE_NAME("adapterTakePicture");
        //  takePicture in Camera Adapter.
        if(mIsRaw16CBEnabled)
        {
            MY_LOGD("CAMERA_CMD_ENABLE_RAW16_CALLBACK:1");
            mpCamAdapter->sendCommand(CAMERA_CMD_ENABLE_RAW16_CALLBACK, 1, 0);
        }
        //
        if ( strcmp(mpCamAdapter->getName(), MtkCameraParameters::APP_MODE_NAME_MTK_ZSD) &&
             !recordingEnabled() )
        {
            // flush hwsync
            mSyncMgr = NSCam::ISyncManager::getInstance(getOpenId());
            if (mSyncMgr != NULL)
            {
                mSyncMgr->setEvent(0, NULL);
                mSyncMgr = NULL;
            }
            else
            {
                MY_LOGW("Cannot get syncmanager in stop preview. Can't flush hwsync");
            }
            if (mpCamAdapter != mpCamAdapter_W) {
                mpCamAdapter_W->stopPreview();
            } else if (mpCamAdapter != mpCamAdapter_T) {
                mpCamAdapter_T->stopPreview();
            }
        }

        if(featureMode == E_DUALCAM_FEATURE_ZOOM)
        {

#if (MTKCAM_HAVE_DUAL_ZOOM_FUSION_SUPPORT == 1)
            // Fusion shot entry point
            StereoSettingProvider::setStereoShotMode(eShotMode_FusionShot);
            if (mpCamAdapter_W.get() == NULL || mpCamAdapter_T.get() == NULL)
            {
                MY_LOGE("Error: Wide CamAdatper(%p), Tele CamAdapter(%p)",
                                (void*)mpCamAdapter_W.get(),
                                (void*)mpCamAdapter_T.get());
                goto lbExit;
            }

            // make sure it is ZSD mode
            String8 pszZsdMode = mpParamsMgr->getStr(MtkCameraParameters::KEY_ZSD_MODE);
            if  (pszZsdMode != MtkCameraParameters::ON)
            {
                MY_LOGE("%s=%s, fusion shall work in ZSD mode !",
                    MtkCameraParameters::KEY_ZSD_MODE, pszZsdMode.string());
                status = UNKNOWN_ERROR;
                goto lbExit;
            }

            MY_LOGD("wait wide capture cmd done+");
            status = mpCamAdapter_W->takePicture();
            MY_LOGD("wait wide capture cmd done- (%d)", status);

            status_t tele_status = OK;

            // do takepicture simultaneously
            std::function<status_t(void)> main2TakeAction =
                    [this, &tele_status] () mutable {
                        MY_LOGD("wait tele capture cmd done+");
                        tele_status = mpCamAdapter_T->takePicture();
                        return tele_status;
                    };

            std::future<status_t> main2CaptureCmd =
                    std::async(std::launch::async, main2TakeAction);

            main2CaptureCmd.wait();
            MY_LOGD("wait tele capture cmd done- (%d)", tele_status);

            if (status != OK || tele_status != OK)
            {
                MY_LOGE("capture fail");
                status = UNKNOWN_ERROR;
            }
#else
            // for zoom, just takepicture to specific adapter
            status = mpCamAdapter->takePicture();
#endif // MTKCAM_HAVE_DUAL_ZOOM_FUSION_SUPPORT
        }

#if DUALCAM_DENOISE
        else if(featureMode == E_STEREO_FEATURE_DENOISE)
        {
            String8 s8ShotMode;
            uint32_t u4ShotMode;
            // check denoise decision#if DUALCAM_DENOISE
            auto getDenoiseDetectionResult = [this](
                String8& shotModeStr, uint32_t& shotMode)
            {
                bool bUsingDenoiseShot = MFALSE;
                // get sync mgr and lock it
                sp<ISyncManager> pSyncMgr = ISyncManager::getInstance(getOpenId());
                if(pSyncMgr!=nullptr)
                {
                    PolicyLockParams policyLockParams;
                    pSyncMgr->lock(&policyLockParams);

                    shotMode = mpParamsMgr->getShotMode();
                    if(shotMode == eShotMode_ContinuousShot){
                        MY_LOGD("dualcam does not support CShot!");
                        return bUsingDenoiseShot;
                    }

                    MY_LOGD("bUseDualCamShot:%d, bIsMain2On:%d",
                        policyLockParams.bUseDualCamShot,
                        policyLockParams.bIsMain2On
                    );

                    // check property
                    MINT32 value = -1;
                    value = property_get_int32("debug.camera.forceDenoiseShot", -1);

                    if(value == 1 && policyLockParams.bIsMain2On)
                    {
                        MY_LOGD("force using BMDNShot");
                        shotModeStr = "BMDN";
                        shotMode = NSCam::eShotMode_BMDNShot;
                        bUsingDenoiseShot = MTRUE;
                    }
                    else if(value == 2 && policyLockParams.bIsMain2On)
                    {
                        MY_LOGD("force using MFHRShot");
                        shotModeStr = "MFHR";
                        shotMode = NSCam::eShotMode_MFHRShot;
                        bUsingDenoiseShot = MTRUE;
                    }
                    else if(policyLockParams.bUseDualCamShot)
                    {
                        MUINT8 sceneResult = (MUINT8)NSCam::DENOISE_SCENE_RESULT::DENOISE_SCENE_NOT_SUPPORT;
                        // check which shot should be used.
                        sp<IFrameInfo> pFrameInfo =
                            IResourceContainer::getInstance(getOpenId())->queryLatestFrameInfo();
                        MY_LOGW_IF((pFrameInfo == nullptr), "Can't query Latest FrameInfo!");
                        IMetadata metadata;
                        if (pFrameInfo.get())
                            pFrameInfo->getFrameMetadata(
                                    eSTREAMID_META_APP_DYNAMIC_P2, metadata);
                        // get shot mode from p2 app matadata
                        {
                            IMetadata::IEntry entry = metadata.entryFor(MTK_STEREO_FEATURE_SHOT_MODE);
                            if(!entry.isEmpty()) {
                                sceneResult = entry.itemAt(0, Type2Type<MUINT8>());
                            }
                            else
                            {
                                MY_LOGW("Cannot get denoise scene result");
                            }
                            // check shot mode
                            if(sceneResult == (MUINT8)NSCam::DENOISE_SCENE_RESULT::DENOISE_SCENE_DENOISE)
                            {
                                MY_LOGD("using BMDNShot");
                                shotModeStr = "BMDN";
                                shotMode = NSCam::eShotMode_BMDNShot;
                                bUsingDenoiseShot = MTRUE;
                            }
                            else if(sceneResult == (MUINT8)NSCam::DENOISE_SCENE_RESULT::DENOISE_SCENE_HIGH_RES)
                            {
                                MY_LOGD("using MFHRShot");
                                shotModeStr = "MFHR";
                                shotMode = NSCam::eShotMode_MFHRShot;
                                bUsingDenoiseShot = MTRUE;
                            }
                            else
                            {
                                MY_LOGD("scene result does not use denoise");
                                shotModeStr = "ZSD";
                                shotMode = NSCam::eShotMode_ZsdShot;
                            }
                        }
                    }
                    else
                    {
                        MY_LOGD("dual cam is not ready, using default shotmode");
                        shotModeStr = "ZSD";
                        shotMode = NSCam::eShotMode_ZsdShot;
                    }
                }
                return bUsingDenoiseShot;
            };
            bool checkResult = getDenoiseDetectionResult(s8ShotMode, u4ShotMode);
            MY_LOGD("decision shot: (%s:%d)", s8ShotMode.string(), u4ShotMode);
            StereoSettingProvider::setStereoShotMode(u4ShotMode);
            if(checkResult)
            {
                if(mpCamAdapter_W.get() == nullptr ||
                   mpCamAdapter_T.get() == nullptr)
                {
                    MY_LOGE("Error: Cam1Adatper(%p) Cam2Adapter(%p)",
                                    (void*)mpCamAdapter_W.get(),
                                    (void*)mpCamAdapter_T.get());
                    goto lbExit;
                }
                // do takepicture simultaneously
                std::function<status_t(void)> main2TakeAction =
                                [this]()mutable{
                                    status_t status =
                                            mpCamAdapter_T->takePicture();
                                    return status;
                                };
                std::future<status_t> main2CaptureCmd =
                                    std::async(
                                                std::launch::async,
                                                main2TakeAction);
                mpCamAdapter_W->takePicture();
                MY_LOGD("wait capture cmd done+");
                main2CaptureCmd.wait();
                MY_LOGD("wait capture cmd done-");
                if(status != OK || main2CaptureCmd.get() != OK)
                {
                    MY_LOGE("capture fail");
                    status = UNKNOWN_ERROR;
                }
            }
            else
            {
                // single cam capture
                status = mpCamAdapter->takePicture();
                sp<ISyncManager> pSyncMgr = ISyncManager::getInstance(getOpenId());
                if(pSyncMgr!=nullptr){
                    pSyncMgr->unlock(nullptr);
                }
            }
        }
#endif // DUALCAM_DENOISE
        else
        {
            MY_LOGE("Not support");
            goto lbExit;
        }
        if  ( OK != status )
        {
            MY_LOGE("CamAdapter->takePicture() returns: [%s(%d)]", ::strerror(-status), -status);
            goto lbExit;
        }
    }

lbExit:

    MY_LOGI("-");
    return  status;
}


/******************************************************************************
 *
 ******************************************************************************/
MINT32
DualCam1Device::
onEvent(
    MINT32 const i4OpenId           __attribute__((unused)),
    MINT32 arg1                     __attribute__((unused)),
    MINT32 arg2                     __attribute__((unused)),
    void* arg3                      __attribute__((unused)))
{
    Mutex::Autolock _l(mSyncLock);
    MY_LOGD("DualCam1Device::onEvent id(%d), 0x%08X, 0x%08X",i4OpenId, arg1, arg2);
    switch(arg1)
    {
        case MTK_SYNCMGR_MSG_NOTIFY_MASTER_ID:
        {
            if (i4OpenId == getOpenId()) {
                mpCamAdapter = mpCamAdapter_W;
            } else {
                mpCamAdapter = mpCamAdapter_T;
            }
            mpCamClient->sendCommand(CAMERA_CMD_SET_FD_MAINCAM_ID, i4OpenId, 0);
            break;
        }
        default:
        {
            break;
        }
    }
    return 0;
}


/******************************************************************************
 *
 ******************************************************************************/
status_t
DualCam1Device::
updateSensorId(
)
{
    int i4DeviceNum = 0;
    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    if(!pHalSensorList)
    {
        MY_LOGE("pHalSensorList == NULL");
        return 0;
    }
    //
    if(NSCam::Utils::CamManager::getInstance()->getDeviceCount() > 0)
    {
        MY_LOGE("CamManager.getDeviceCount() > 0, close them before open stereo mode.");
        return UNKNOWN_ERROR;
    }
    //
    i4DeviceNum = pHalSensorList->queryNumberOfSensors();
    if(i4DeviceNum < 2)
    {
        MY_LOGE("i4DeviceNum(%d) < 2", i4DeviceNum);
        return UNKNOWN_ERROR;
    }
    else if(i4DeviceNum > 4)
    {
        MY_LOGW("i4DeviceNum(%d) > 4, it is weired to turn on stereo cam in such situation!", i4DeviceNum);
    }
    else
    {
        MY_LOGD("i4DeviceNum(%d)", i4DeviceNum);
    }

    uint32_t curDevIdx = pHalSensorList->querySensorDevIdx(mi4OpenId);
    switch(curDevIdx)
    {
        case SENSOR_DEV_MAIN:
            MY_LOGD("setup rear+rear sensor pair to StereoSettingProvider");
            StereoSettingProvider::setStereoProfile(STEREO_SENSOR_PROFILE_REAR_REAR);
            break;
        case SENSOR_DEV_SUB:
            MY_LOGD("setup front+front sensor pair to StereoSettingProvider");
            StereoSettingProvider::setStereoProfile(STEREO_SENSOR_PROFILE_FRONT_FRONT);
            break;
#warning workaround
        case SENSOR_DEV_MAIN_2:
            MY_LOGE("should not happend, please check AP flow");
            StereoSettingProvider::setStereoProfile(STEREO_SENSOR_PROFILE_REAR_REAR);
            break;
        default:
            MY_LOGE("Unrecognized opendId(%d)", mi4OpenId);
            return UNKNOWN_ERROR;
    }

    if(i4DeviceNum == 2)
    {
        MY_LOGW("There is only 2 sensors, use rear+front setting!");
        StereoSettingProvider::setStereoProfile(STEREO_SENSOR_PROFILE_REAR_FRONT);
    }

    int dummy_main1Id = -1;
    if(!StereoSettingProvider::getStereoSensorIndex(dummy_main1Id, mSensorId_Main2)){
        MY_LOGE("Cannot get sensor ids from StereoSettingProvider!");
        return UNKNOWN_ERROR;
    }
    else
    {
        MY_LOGD("main1 openId:%d, main2 openId:%d", mi4OpenId, mSensorId_Main2);
    }
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
DualCam1Device::
setFeatureMode(
)
{
    status_t ret = UNKNOWN_ERROR;
    if(mDevName == MtkCameraParameters::APP_MODE_NAME_MTK_STEREO)
    {
        MY_LOGD("vsdof mode");
        StereoSettingProvider::setStereoFeatureMode(E_STEREO_FEATURE_VSDOF);
        ret = OK;
    }
    else
    {
        // denoise and dual cam zoom use same device.
        // using main2 id to decide current feature mode.
        MY_LOGD("Dual cam main2 id: %d", mSensorId_Main2);
        MY_LOGD("Dual cam main2 fmt: %d", StereoSettingProvider::getSensorRawFormat(mSensorId_Main2));
#if DUALCAM_DENOISE
        if(SENSOR_RAW_MONO == StereoSettingProvider::getSensorRawFormat(mSensorId_Main2))
        {
            MY_LOGD("denosie mode");
            StereoSettingProvider::setStereoFeatureMode(E_STEREO_FEATURE_DENOISE);
            ret = OK;
        }
        else
#endif
        {
            // *****************************************************************
            // MTKCAM_HAVE_DUAL_ZOOM_SUPPORT
            // MTKCAM_HAVE_DUAL_ZOOM_FUSION_SUPPORT
            // Both use the same Stereo Feature Mode - E_DUALCAM_FEATURE_ZOOM
            // *****************************************************************
            MY_LOGD("dualcam zoom mode");
            StereoSettingProvider::setStereoFeatureMode(E_DUALCAM_FEATURE_ZOOM);
            ret = OK;
        }
    }
    return ret;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
DualCam1Device::
powerOffSensor(
    int32_t openId,
    NSCam::IHalSensor*& halSensor
)
{
    MY_LOGD("(%d) +", openId);
    if(halSensor == nullptr)
    {
        MY_LOGW("(%d) halSensor is nullptr", openId);
        return OK;
    }
    if(openId < 0)
    {
        MY_LOGW("openId(%d) < 0, skip.", openId);
        return OK;
    }
    MUINT pIndex_main[1] = { (MUINT)openId};
    halSensor->powerOff(USER_NAME, 1, pIndex_main);
    NSCam::Utils::CamManager::getInstance()->decSensorCount(LOG_TAG);

    halSensor->destroyInstance(USER_NAME);
    halSensor = NULL;
    MY_LOGD("(%d) -", openId);
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
DualCam1Device::
uninit3A(
    int32_t openId,
    NS3Av3::IHal3A*& hal3A
)
{
    MY_LOGD("(%d) +", openId);
    if(hal3A == nullptr)
    {
        MY_LOGW("(%d) hal3a is nullptr", openId);
        return OK;
    }

    hal3A->notifyPwrOff();
    hal3A->destroyInstance(LOG_TAG);
    hal3A = NULL;
    MY_LOGD("(%d) -", openId);
    return OK;
}

/******************************************************************************
 *  Start auto focus, the notification callback routine is called
 *  with CAMERA_MSG_FOCUS once when focusing is complete. autoFocus()
 *  will be called again if another auto focus is needed.
 ******************************************************************************/
status_t
DualCam1Device::
autoFocus()
{
    MY_LOGD("+");
    status_t status = OK;
    //
    waitStartPreviewDone();
    //
    if  ( ! previewEnabled() )
    {
        MY_LOGW("preview is not enabled");
        return OK;
    }
    //
    //disableMsgType(CAMERA_MSG_PREVIEW_METADATA);
    //
    if ( mpCamAdapter_W == 0 && mpCamAdapter_T == 0)
    {
        return (status_t)DEAD_OBJECT;
    }
    if (mpCamAdapter_W != 0)
    {
        status = mpCamAdapter_W->autoFocus();
    }
    if (status != OK)
    {
        return status;
    }
    if (mpCamAdapter_T != 0)
    {
        status = mpCamAdapter_T->autoFocus();
    }
    return status;
}


/******************************************************************************
 * Cancels auto-focus function. If the auto-focus is still in progress,
 * this function will cancel it. Whether the auto-focus is in progress
 * or not, this function will return the focus position to the default.
 * If the camera does not support auto-focus, this is a no-op.
 ******************************************************************************/
status_t
DualCam1Device::
cancelAutoFocus()
{
    status_t status = OK;
    waitStartPreviewDone();
    //
    if ( mpCamAdapter_W == 0 && mpCamAdapter_T == 0)
    {
        return (status_t)OK;
    }
    if (mpCamAdapter_W != 0)
    {
        status = mpCamAdapter_W->cancelAutoFocus();
    }
    if (status != OK)
    {
        return status;
    }
    if (mpCamAdapter_T != 0)
    {
        status = mpCamAdapter_T->cancelAutoFocus();
    }
    return  status;
}

