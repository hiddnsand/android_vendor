/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncResult;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.Settings;

import android.telephony.Rlog;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.android.ims.ImsCallProfile;
import com.android.ims.ImsConfig;
import com.android.ims.ImsManager;
import com.android.ims.ImsReasonInfo;
import com.android.ims.ImsServiceBase;
import com.android.ims.ImsServiceClass;
import com.android.ims.internal.IImsRegistrationListener;
import com.android.ims.internal.IImsCallSession;
import com.android.ims.internal.IImsCallSessionListener;
import com.android.ims.internal.IImsEcbm;
import com.android.ims.internal.IImsMultiEndpoint;
import com.android.ims.internal.IImsUt;
import com.android.ims.internal.IImsConfig;
import com.android.ims.internal.IImsService;

import com.android.internal.telephony.CommandsInterface;

import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.gba.NafSessionKey;
import com.mediatek.ims.config.internal.ImsConfigUtils;
import com.mediatek.ims.ext.IImsServiceExt;
import com.mediatek.ims.ext.OpImsServiceCustomizationUtils;
import com.mediatek.ims.internal.ImsMultiEndpointImpl;
import com.mediatek.ims.internal.ImsDataTracker;
import com.mediatek.ims.internal.IMtkImsCallSession;
import com.mediatek.ims.internal.IMtkImsUt;
import com.mediatek.ims.internal.ImsXuiManager;
import com.mediatek.ims.internal.MtkImsManager;
import com.mediatek.ims.ImsAdapter;
import com.mediatek.ims.ril.ImsCommandsInterface;
import com.mediatek.ims.ril.ImsCommandsInterface.RadioState;
import com.mediatek.ims.ril.ImsRILAdapter;
import com.mediatek.ims.ImsEventPackageAdapter;
import com.mediatek.ims.WfcReasonInfo;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.wfo.DisconnectCause;
import com.mediatek.wfo.IWifiOffloadService;
import com.mediatek.wfo.WifiOffloadManager;
import com.mediatek.wfo.IMwiService;
import com.mediatek.wfo.MwisConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ImsService extends ImsServiceBase {
    private static final String LOG_TAG = "ImsService";
    private static final boolean DBG = true;
    private static final boolean VDBG = false; // STOPSHIP if true
    private static final boolean ENGLOAD = "eng".equals(Build.TYPE);

    // ImsService Name
    private static final String IMS_SERVICE = "ims";

    private ImsAdapter mImsAdapter = null;
    private ImsCommandsInterface [] mImsRILAdapters = null;
    private IImsCallSession mPendingMT = null;
    private IMtkImsCallSession mMtkPendingMT = null;

    // For synchronization of private variables
    private Object mLockObj = new Object();
    private Context mContext;

    private static IWifiOffloadService sWifiOffloadService = null;
    private IWifiOffloadServiceDeathRecipient mDeathRecipient =
            new IWifiOffloadServiceDeathRecipient();
    private IWifiOffloadListenerProxy mProxy = null;

    private static HashMap<Integer, ImsUtStub> sImsUtStub =
                new HashMap<Integer, ImsUtStub>();
    private static HashMap<Integer, MtkImsUtStub> sMtkImsUtStub =
                new HashMap<Integer, MtkImsUtStub>();
    private static HashMap<Integer, ImsUtImpl> sImsUtImpl =
                new HashMap<Integer, ImsUtImpl>();
    private static HashMap<Integer, MtkImsUtImpl> sMtkImsUtImpl =
                new HashMap<Integer, MtkImsUtImpl>();

    private int mNumOfPhones = 0;
    private final Handler [] mHandler;

    // Set via onAddRegistrationListener, source should be ImsPhoneCallTracker
    private IImsRegistrationListener[] mListener;
    private int[] mImsRegInfo;
    private int[] mImsExtInfo;
    private int[] mServiceId;
    private int[] mImsState;
    private int[] mExpectedImsState;
    private int[] mRegErrorCode;
    private int[] mRAN;

    /// M: ECBM @{
    private ImsEcbmProxy[] mImsEcbm;
    /// @}

    /// N: IMS Configuration Manager
    private ImsConfigManager mImsConfigManager = null;

    //***** Event Constants
    private static final int EVENT_IMS_REGISTRATION_INFO = 1;
    protected static final int EVENT_RADIO_NOT_AVAILABLE    = 2;
    protected static final int EVENT_SET_IMS_ENABLED_DONE   = 3;
    protected static final int EVENT_SET_IMS_DISABLE_DONE   = 4;
    protected static final int EVENT_IMS_DISABLED_URC   = 5;
    private static final int EVENT_VIRTUAL_SIM_ON = 6;
    protected static final int EVENT_INCOMING_CALL_INDICATION = 7;
    protected static final int EVENT_CALL_INFO_INDICATION = 8;
    // protected static final int EVENT_CALL_RING = 9;
    protected static final int EVENT_IMS_ENABLING_URC   = 10;
    protected static final int EVENT_IMS_ENABLED_URC   = 11;
    protected static final int EVENT_IMS_DISABLING_URC   = 12;
    ///M : WFC @{
    protected static final int EVENT_SIP_CODE_INDICATION = 13;
    /// @}
    /// M: Event for network initiated USSI @{
    protected static final int EVENT_ON_NETWORK_INIT_USSI = 14;
    /// @}
    /// M: Event for IMS deregistration @{
    protected static final int EVENT_IMS_DEREG_DONE = 15;
    protected static final int EVENT_IMS_DEREG_URC = 16;
    /// @}
    /// M: Event for radio off @{
    protected static final int EVENT_RADIO_OFF = 17;
    /// @}
    /// M: Event for radio on @{
    protected static final int EVENT_RADIO_ON = 18;
    /// @}
    /// M: Event for IMS RTP Report @{
    protected static final int EVENT_IMS_RTP_INFO_URC = 19;
    /// @}

    /// M: Event for IMS registration report @{
    protected static final int EVENT_SET_IMS_REGISTRATION_REPORT_DONE = 20;
    /// @}

    /// M: Sync volte setting value. @{
    protected static final int EVENT_IMS_VOLTE_SETTING_URC = 21;
    /// @}

    /// M: Event fori 93 IMS SS @{
    protected static final int EVENT_RUN_GBA = 22;
    /// @}

    // Event for MD multi-ims support count URC
    protected static final int EVENT_MULTI_IMS_COUNT_URC = 23;

    /// M: 93 IMS SS. @{
    private static final int IMS_SS_TIMEOUT_ERROR = 1;
    private static final int IMS_SS_INTERRUPT_ERROR = 2;
    private static final int IMS_SS_CMD_ERROR = 3;
    private static final int IMS_SS_CMD_SUCCESS = 4;

    private class NafSessionKeyResult {
        NafSessionKey nafSessionKey = null;
        int cmdResult = IMS_SS_TIMEOUT_ERROR;
        Object lockObj = new Object();
    }
    /// @}

    private static final int IMS_ALLOW_INCOMING_CALL_INDICATION = 0;
    private static final int IMS_DISALLOW_INCOMING_CALL_INDICATION = 1;

    //***** IMS Feature Support
    private static final int IMS_VOICE_OVER_LTE = 1;
    private static final int IMS_RCS_OVER_LTE = 2;
    private static final int IMS_SMS_OVER_LTE = 4;
    private static final int IMS_VIDEO_OVER_LTE = 8;
    private static final int IMS_VOICE_OVER_WIFI = 16;

    //Refer to ImsConfig FeatureConstants
    private static final int IMS_MAX_FEATURE_SUPPORT_SIZE = 6;

    /// M: Sync volte setting value. @{
    private static final String PROPERTY_IMSCONFIG_FORCE_NOTIFY = "ril.imsconfig.force.notify";
    /// @}

    private ImsDataTracker mImsDataTracker;

    private ImsEventPackageAdapter[] mImsEvtPkgAdapter;

    private Map<Integer, IImsMultiEndpoint> mMultiEndpointMap =
            new HashMap<Integer, IImsMultiEndpoint>();

    /// M: for query MD multi-ims support count @{
    private Object mMultiImsSyncLock = new Object();
    private int mModemMultiImsCount = -1;
    /// @}

    /// M: ImsService plugin instance @{
    private IImsServiceExt mExt;
    /// @ }

    /// M: Simulate IMS Registration @{
    private boolean mImsRegistry = false;
    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            log("[onReceive] action=" + intent.getAction());
            ///M : WFC @{
            if ("ACTION_IMS_SIMULATE".equals(intent.getAction())){
            /// @}
                mImsRegistry = intent.getBooleanExtra("registry", false);
                logw("Simulate IMS Registration: " + mImsRegistry);
                int phoneId = ImsCommonUtil.getMainCapabilityPhoneId();
                int[] result = new int[] {
                    (mImsRegistry ? 1 : 0),
                    15,
                    phoneId};
                AsyncResult ar = new AsyncResult(null, result, null);
                mHandler[phoneId].sendMessage(
                  mHandler[phoneId].obtainMessage(EVENT_IMS_REGISTRATION_INFO, ar));
            } else if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
                if (ImsCommonUtil.supportMdAutoSetupIms() == false) {
                    bindAndRegisterWifiOffloadService();
                } else {
                    bindAndRegisterMWIService();
                }
                // send IMS_SERVICE_UP intent again in case previous is before BOOT_COMPLETE
                for(int i = 0; i < mNumOfPhones; i++) {
                    if (mImsState[i] == MtkPhoneConstants.IMS_STATE_ENABLE) {
                        Intent newIntent = new Intent(ImsManager.ACTION_IMS_SERVICE_UP);
                        newIntent.putExtra(ImsManager.EXTRA_PHONE_ID, i);
                        mContext.sendBroadcast(newIntent);
                        log("broadcast IMS_SERVICE_UP for phone=" + i);
                    }
                }
            }
            log("[onReceive] finished action=" + intent.getAction());
        }
    };
    /// @}

    public ImsService(Context context) {
        log("init");
        mContext = context;
        /// Get Number of Phones
        mNumOfPhones = TelephonyManager.getDefault().getPhoneCount();
        /// M: keep old logic for 92gen and before
        if (ImsCommonUtil.supportMdAutoSetupIms() == false) {
            mImsAdapter = new ImsAdapter(context);
        }

        mHandler = new MyHandler[mNumOfPhones];
        mImsRILAdapters = new ImsCommandsInterface[mNumOfPhones];
        for(int i = 0; i < mNumOfPhones; i++) {
            mHandler[i] = new MyHandler(i);
            ImsRILAdapter ril = new ImsRILAdapter(context, i);

            /// register for radio state changed
            ril.registerForNotAvailable(mHandler[i], EVENT_RADIO_NOT_AVAILABLE, null);
            ril.registerForOff(mHandler[i], EVENT_RADIO_OFF, null);
            ril.registerForOn(mHandler[i], EVENT_RADIO_ON, null);

            ril.registerForImsRegistrationInfo(mHandler[i], EVENT_IMS_REGISTRATION_INFO, null);
            ril.registerForImsEnableStart(mHandler[i], EVENT_IMS_ENABLING_URC, null);
            ril.registerForImsEnableComplete(mHandler[i], EVENT_IMS_ENABLED_URC, null);
            ril.registerForImsDisableStart(mHandler[i], EVENT_IMS_DISABLING_URC, null);
            ril.registerForImsDisableComplete(mHandler[i], EVENT_IMS_DISABLED_URC, null);
            ril.setOnIncomingCallIndication(mHandler[i], EVENT_INCOMING_CALL_INDICATION, null);
            ril.registerForCallProgressIndicator(mHandler[i], EVENT_SIP_CODE_INDICATION, null);
            ril.registerForImsDeregisterComplete(mHandler[i], EVENT_IMS_DEREG_URC, null);
            ril.registerForMultiImsCount(mHandler[i], EVENT_MULTI_IMS_COUNT_URC, null);

            /// M: Listen for network initiated USSI @{
            ril.setOnNetworkInitUSSI(mHandler[i], EVENT_ON_NETWORK_INIT_USSI, null);
            /// @}
            /// M: register for IMS RTP report event @{
            ril.registerForImsRTPInfo(mHandler[i], EVENT_IMS_RTP_INFO_URC, null);
            /// @}
            /// M: Sync volte setting value. @{
            ril.registerForVolteSettingChanged(mHandler[i], EVENT_IMS_VOLTE_SETTING_URC, null);
            /// @}
            mImsRILAdapters[i] = ril;
        }

        /// M: create for 93MD events handle.
        if (ImsCommonUtil.supportMdAutoSetupIms()) {
            log("Initializing");
            mImsDataTracker = new ImsDataTracker(context, mImsRILAdapters);
        }

        /// M: Simulate IMS Registration @{
        final IntentFilter filter = new IntentFilter();
        filter.addAction("ACTION_IMS_SIMULATE");
        /// @}
        filter.addAction(Intent.ACTION_BOOT_COMPLETED);

        context.registerReceiver(mBroadcastReceiver, filter);

        mListener = new IImsRegistrationListener[mNumOfPhones];
        mImsRegInfo = new int[mNumOfPhones];
        mImsExtInfo = new int[mNumOfPhones];
        mServiceId = new int[mNumOfPhones];
        mImsState = new int[mNumOfPhones];
        mExpectedImsState = new int[mNumOfPhones];
        mRegErrorCode = new int[mNumOfPhones];
        mRAN = new int[mNumOfPhones];
        mImsEcbm = new ImsEcbmProxy[mNumOfPhones];
        mImsEvtPkgAdapter = new ImsEventPackageAdapter[mNumOfPhones];
        mImsConfigManager = new ImsConfigManager(context, mImsRILAdapters);

        for (int i = 0; i < mNumOfPhones; i++) {
            mListener[i] = null;
            mImsRegInfo[i] = ServiceState.STATE_POWER_OFF;
            mImsExtInfo[i] = 0;
            mServiceId[i] = i+1;
            mImsState[i] = MtkPhoneConstants.IMS_STATE_DISABLED;
            mExpectedImsState[i] = MtkPhoneConstants.IMS_STATE_DISABLED;
            mRegErrorCode[i] = ImsReasonInfo.CODE_UNSPECIFIED;
            mRAN[i] = WifiOffloadManager.RAN_TYPE_MOBILE_3GPP;
            mImsEcbm[i] = null;
            mImsConfigManager.init(i);
            if (ImsCommonUtil.supportMdAutoSetupIms()) {
                sImsUtImpl.put(i, new ImsUtImpl(mContext, this, mImsRILAdapters[i], i));
                sMtkImsUtImpl.put(i, new MtkImsUtImpl(mContext, this, i, mImsRILAdapters[i], sImsUtImpl.get(i)));
            } else {
                sImsUtStub.put(i, new ImsUtStub(mContext, this, i));
                sMtkImsUtStub.put(i, new MtkImsUtStub(mContext, this, i, sImsUtStub.get(i)));
            }
            mImsEvtPkgAdapter[i] =
                new ImsEventPackageAdapter(mContext, mHandler[i], mImsRILAdapters[i], i);
        }

        // init flow, initialze ims capability at constructor. for multiple ims
        // do this for all phones
        if (ImsCommonUtil.supportMims() == false) {
            final int mainPhoneId = ImsCommonUtil.getMainCapabilityPhoneId();
            log("getMainCapabilityPhoneId: mainPhoneId = " + mainPhoneId);

            // Error handling in case ImsService process died
            if (ImsCommonUtil.supportMdAutoSetupIms()) {
                mImsRILAdapters[mainPhoneId].setImsRegistrationReport(
                    mHandler[mainPhoneId].obtainMessage(EVENT_SET_IMS_REGISTRATION_REPORT_DONE));
            } else {
                if (SubscriptionManager.isValidPhoneId(mainPhoneId)) {
                    initImsAvailability(mainPhoneId, 0, EVENT_SET_IMS_ENABLED_DONE,
                            EVENT_SET_IMS_DISABLE_DONE);
                }
            }
        } else {
            for (int i = 0; i < TelephonyManager.getDefault().getPhoneCount(); i++) {

                // Error handling in case ImsService process died
                if (ImsCommonUtil.supportMdAutoSetupIms()) {
                    mImsRILAdapters[i].setImsRegistrationReport(
                            mHandler[i].obtainMessage(EVENT_SET_IMS_REGISTRATION_REPORT_DONE));
                } else {
                    initImsAvailability(i, i, EVENT_SET_IMS_ENABLED_DONE,
                            EVENT_SET_IMS_DISABLE_DONE);
                }
            }
        }
        /// @}
        /// M: Plugin @{
        mExt = OpImsServiceCustomizationUtils.getOpFactory(context).makeImsServiceExt(context);
    }

    private void enableImsAdapter(int phoneId) {
        mImsAdapter.enableImsAdapter(phoneId);
    }

    private void disableImsAdapter(int phoneId, boolean isNormalDisable) {
        mImsAdapter.disableImsAdapter(phoneId, isNormalDisable);
    }

    @Override
    protected int onOpen(int phoneId, int serviceClass, PendingIntent incomingCallIntent,
            IImsRegistrationListener listener) {
        log("onOpen: phoneId=" + phoneId + " serviceClass=" + serviceClass +
                " listener=" + listener);
        int serviceId = mapPhoneIdToServiceId(phoneId);
        synchronized(mLockObj) {
            if (mListener[phoneId] != null) {
                loge("onOpen: did not invoke close() IMS service before open() !!");
            }
            englog("onOpen: serviceId=" + serviceId);
            return serviceId;
        }
    }

    @Override
    protected void onClose(int serviceId) {

        synchronized(mLockObj) {
            int phoneId = mapServiceIdToPhoneId(serviceId);
            // remove registration listener
            mListener[phoneId] = null;
            /// M: clear ImsEcbmProxy listener @{
            if (mImsEcbm[phoneId] != null) {
                mImsEcbm[phoneId].setListener(null);
            }
            /// @}
        }
    }

    /**
     * Checks if the IMS service has successfully registered to the IMS network
     * with the specified service & call type.
     *
     * TODO check each serviceType and callType
     */
    @Override
    protected boolean onIsConnected(int serviceId, int serviceType, int callType) {
        log("onIsConnected: serviceId=" + serviceId + ", serviceType=" + serviceType
                + ", callType=" + callType);
        int phoneId = mapServiceIdToPhoneId(serviceId);
        return (mImsRegInfo[phoneId] == ServiceState.STATE_IN_SERVICE);
    }

    /**
     * Checks if the specified IMS service is opend.
     *
     * @return true if the specified service id is opened; false otherwise
     */
    @Override
    protected boolean onIsOpened(int serviceId) {
        log("onIsOpened: serviceId=" + serviceId);
        int phoneId = mapServiceIdToPhoneId(serviceId);
        /* onOpen will set RegistrationListener */
        return (mListener[phoneId] != null);
    }

    /**
     * We assume only ImsPhoneCallTracker will use this API but actually
     * anyone who could access ImsService could invoke this and replace listener
     */
    @Override
    protected void onSetRegistrationListener(int serviceId, IImsRegistrationListener listener) {
        log("onSetRegistrationListener: serviceId=" + serviceId + ", listener=" + listener);
        int phoneId = mapServiceIdToPhoneId(serviceId);
        mListener[phoneId] = listener;

        if (mImsRegInfo[phoneId] != ServiceState.STATE_POWER_OFF) {
            notifyRegistrationStateChange(phoneId, mImsRegInfo[phoneId]);
        }

        if ((mImsRegInfo[phoneId] == ServiceState.STATE_IN_SERVICE)) {
            notifyRegistrationCapabilityChange(phoneId, mImsExtInfo[phoneId]);
        }
    }

    @Override
    protected void onAddRegistrationListener(
            int phoneId, int serviceType, IImsRegistrationListener listener) {
        log("onAddRegistrationListener: phoneId=" + phoneId + " serviceType=" + serviceType +
                " listener=" + listener);

        synchronized (mLockObj) {
            if (mListener[phoneId] != null) {
                logw("onAddRegistrationListener: replacing RegistrationListener");
            }
            mListener[phoneId] = listener;
        }
        if (mImsRegInfo[phoneId] != ServiceState.STATE_POWER_OFF) {
            notifyRegistrationStateChange(phoneId, mImsRegInfo[phoneId]);
        }

        if ((mImsRegInfo[phoneId] == ServiceState.STATE_IN_SERVICE)) {
            notifyRegistrationCapabilityChange(phoneId, mImsExtInfo[phoneId]);
        }
    }

    @Override
    protected ImsCallProfile onCreateCallProfile(int serviceId, int serviceType, int callType) {
        return new ImsCallProfile(serviceType, callType);
    }

    @Override
    protected IImsCallSession onCreateCallSession(int serviceId, ImsCallProfile profile,
            IImsCallSessionListener listener) {
        // This API is for outgoing call to create IImsCallSession
        int phoneId = mapServiceIdToPhoneId(serviceId);
        return new ImsCallSessionProxy(mContext, profile, listener, this, mHandler[phoneId],
                mImsRILAdapters[phoneId], phoneId);
    }

    // Relay from MtkImsService
    protected IMtkImsCallSession onCreateMtkCallSession(int serviceId, ImsCallProfile profile,
                                                  IImsCallSessionListener listener) {
        // This API is for outgoing call to create IImsCallSession
        int phoneId = mapServiceIdToPhoneId(serviceId);
        return new MtkImsCallSessionProxy(mContext, profile, listener, this, mHandler[phoneId],
                mImsRILAdapters[phoneId], phoneId);
    }

    // Relay from MtkImsService
    /**
     * Used to trigger GBA in native SS solution.
     */
    protected NafSessionKey onRunGbaAuthentication(String nafFqdn,
            byte[] nafSecureProtocolId, boolean forceRun, int netId, int phoneId) {
        NafSessionKeyResult result = new NafSessionKeyResult();
        Message msg = mHandler[phoneId].obtainMessage(EVENT_RUN_GBA, result);

        synchronized(result.lockObj) {
            mImsRILAdapters[phoneId].runGbaAuthentication(nafFqdn,
                   ImsCommonUtil.bytesToHex(nafSecureProtocolId), forceRun, netId, msg);
            try {
                result.lockObj.wait(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                result.cmdResult = IMS_SS_INTERRUPT_ERROR;
            }
        }
        log("onRunGbaAuthentication complete, nafSessionKey:" + result.nafSessionKey +
                ", cmdResult:" + result.cmdResult);

        return result.nafSessionKey;
    }

    @Override
    protected IImsCallSession onGetPendingCallSession(int serviceId, String callId) {

        int phoneId = mapServiceIdToPhoneId(serviceId);

        // This API is for incoming call to create IImsCallSession
        if (mPendingMT == null) {
            return null;
        }

        IImsCallSession pendingMT = mPendingMT;

        try {
            if (pendingMT.getCallId().equals(callId)) {
                mPendingMT = null;
                return pendingMT;
            }
        } catch (RemoteException e) {
            // error handling. Currently no-op
        }

        return null;
    }

    /**
     * Ut interface for the supplementary service configuration.
     */
    @Override
    protected IImsUt onGetUtInterface(int serviceId) {

        int phoneId = mapServiceIdToPhoneId(serviceId);

        if (ImsCommonUtil.supportMdAutoSetupIms()) {
            synchronized (sImsUtImpl) {
                if (sImsUtImpl.containsKey(phoneId)) {
                    return sImsUtImpl.get(phoneId);
                }
                ImsUtImpl imsUtImpl = new ImsUtImpl(mContext, this, mImsRILAdapters[phoneId], phoneId);
                sImsUtImpl.put(phoneId, imsUtImpl);
                return imsUtImpl;
            }
        } else {
            synchronized (sImsUtStub) {
                if (sImsUtStub.containsKey(phoneId)) {
                    return sImsUtStub.get(phoneId);
                }
                ImsUtStub imsUtStub = new ImsUtStub(mContext, this, phoneId);
                sImsUtStub.put(phoneId, imsUtStub);
                return imsUtStub;
            }
        }
    }

    // Relay from MtkImsService
    protected IMtkImsUt onGetMtkUtInterface(int serviceId) {
        int phoneId = mapServiceIdToPhoneId(serviceId);

        if (ImsCommonUtil.supportMdAutoSetupIms()) {
            synchronized (sMtkImsUtImpl) {
                if (sMtkImsUtImpl.containsKey(phoneId)) {
                    return sMtkImsUtImpl.get(phoneId);
                }
                MtkImsUtImpl mtkImsUtImpl = new MtkImsUtImpl(mContext, this, phoneId,
                                                             mImsRILAdapters[phoneId],
                                                             sImsUtImpl.get(phoneId));
                sMtkImsUtImpl.put(phoneId, mtkImsUtImpl);
                return mtkImsUtImpl;
            }
        } else {
            synchronized (sMtkImsUtStub) {
                if (sMtkImsUtStub.containsKey(phoneId)) {
                    return sMtkImsUtStub.get(phoneId);
                }
                MtkImsUtStub mtkImsUtStub = new MtkImsUtStub(mContext, this, phoneId, sImsUtStub.get(phoneId));
                sMtkImsUtStub.put(phoneId, mtkImsUtStub);
                return mtkImsUtStub;
            }
        }
    }

    /**
     * Config interface to get/set IMS service/capability parameters.
     */
    @Override
    protected IImsConfig onGetConfigInterface(int phoneId) {
        if (ImsCommonUtil.supportMdAutoSetupIms() == false) {
            bindAndRegisterWifiOffloadService();
        } else {
            bindAndRegisterMWIService();
        }
        /*
        if (!SubscriptionManager.isValidPhoneId(phoneId)) {
            logw("Invalid phoneId " + phoneId + " to get config interface");
            phoneId = getMainCapabilityPhoneId();
            logw("Get config interface on main capability phone " + phoneId);
        }
        */
        if (ImsCommonUtil.supportMims() == false) {
            // Always get main capability phone Id for ImsConfig
            phoneId = ImsCommonUtil.getMainCapabilityPhoneId();
            log("Get config interface on main capability phone " + phoneId);
        } else {
            log("Multi ims support, use phoneId: " + phoneId);
        }

        return mImsConfigManager.get(phoneId);
    }


    /**
     * Used for turning on IMS when its in OFF state.
     */

    @Override
    protected void onTurnOnIms(int phoneId) {
        log("turnOnIms phoneId = " + phoneId);
    }

    /**
     * Used for turning off IMS when its in ON state.
     * When IMS is OFF, device will behave as CSFB'ed.
     */
    @Override
    protected void onTurnOffIms(int phoneId) {
        log("turnOffIms, phoneId = " + phoneId);
    }

    /**
     * ECBM interface for Emergency Callback mode mechanism.
     */
    @Override
    protected IImsEcbm onGetEcbmInterface(int serviceId) {
        int phoneId = mapServiceIdToPhoneId(serviceId);

        if (mImsEcbm[phoneId] == null) {
            mImsEcbm[phoneId] = new ImsEcbmProxy(mImsRILAdapters[phoneId]);
        }
        return mImsEcbm[phoneId];
    }

    /**
      * Used to set current TTY Mode.
      */
    @Override
    protected void onSetUiTTYMode(int serviceId, int uiTtyMode, Message onComplete) {
        log("onSetUiTTYMode: " + uiTtyMode);
        int phoneId = mapServiceIdToPhoneId(serviceId);

        return;
    }

    /**
      * MultiEndpoint interface for DEP.
      */
    @Override
    protected IImsMultiEndpoint onGetMultiEndpointInterface(int serviceId) {
        int phoneId = mapServiceIdToPhoneId(serviceId);

        Rlog.d(LOG_TAG, "onGetMultiEndpointInterface serviceId is " + serviceId);
        if (mMultiEndpointMap.containsKey(serviceId)) {
            return mMultiEndpointMap.get(serviceId);
        }
        IImsMultiEndpoint instance = new ImsMultiEndpointImpl(mContext);
        Rlog.d(LOG_TAG, "onGetMultiEndpointInterface instance is " + instance);
        mMultiEndpointMap.put(serviceId, instance);
        return instance;
    }

    /**
     * Query ims enable/disable status.
     *@return ims status
     *@hide
     */
    protected int getImsState(int phoneId) {
        return mImsState[phoneId];
    }

    /**
     * Use to hang up all calls .
     *@hide
     */
    protected void onHangupAllCall(int serviceId) {
        int phoneId = mapServiceIdToPhoneId(serviceId);
        mImsRILAdapters[phoneId].hangupAllCall(null);
    }

    /**
     * Used to deregister IMS.
     */
    protected void deregisterIms(int phoneId) {
        log("deregisterIms, phoneId = " + phoneId);
        if (ImsCommonUtil.supportMims() == false) {
            phoneId = ImsCommonUtil.getMainCapabilityPhoneId();
            log("deregisterIms, MainCapabilityPhoneId = " + phoneId);
        }
        mImsRILAdapters[phoneId].deregisterIms(
                mHandler[phoneId].obtainMessage(EVENT_IMS_DEREG_DONE));
    }

    /**
     * Used to deregister IMS for Volte SS.
     */
    public void deregisterImsWithCause(int phoneId, int cause) {
        log("deregisterImsWithCause, phoneId = " + phoneId + " cause = " + cause);

        if (ImsCommonUtil.supportMims() == false) {
            phoneId = ImsCommonUtil.getMainCapabilityPhoneId();
            log("deregisterImsWithCause, MainCapabilityPhoneId = " + phoneId);
        }
        mImsRILAdapters[phoneId].deregisterImsWithCause(cause,
                mHandler[phoneId].obtainMessage(EVENT_IMS_DEREG_DONE));
    }

    /**
     * Used to update radio state.
     *
     * @param radioState int value cast from RadioState
     * @param phoneId  to indicate which phone.
     */
    public void updateRadioState(int radioState, int phoneId) {
        log("updateRadioState, phoneId = " + phoneId + " radioState = " + radioState);
        if (ImsCommonUtil.supportMdAutoSetupIms()) {
            if (RadioState.RADIO_ON.ordinal() == radioState) {
                ImsConfigUtils.triggerSendCfg(mContext, mImsRILAdapters[phoneId], phoneId);
            }
            //bindAndRegisterMWIService();
        } else {
            bindAndRegisterWifiOffloadService();

            if (sWifiOffloadService != null) {
                try {
                    sWifiOffloadService.updateRadioState(phoneId, radioState);
                } catch (RemoteException e) {
                    loge("can't update radio state");
                }
            } else {
                loge("can't get WifiOffloadService");
            }
        }
        englog("updateRadioState done");
    }

    /**
     * Use to map serviceId to phoneId.
     * @param serviceId Service ID
     * @return phoneId
     *@hide
     */
    public int mapServiceIdToPhoneId(int serviceId) {
        if (serviceId < 1) {
            loge("mapServiceIdToPhoneId: serviceId should be larger than 0, not "
                    + serviceId);
            return 0;
        }
        return (serviceId - 1);
    }

    /**
     * Use to map phoneId to serviceId.
     * @param phoneId Phone ID
     * @return serviceId
     *@hide
     */
    public int mapPhoneIdToServiceId(int phoneId) {
        return (phoneId + 1);
    }

    /**
     * Use to query ims service state .
     *@return mImsRegInfo for service state information.
     *@hide
     */
    public int getImsServiceState(int phoneId) {
        if (ImsCommonUtil.supportMims() == false) {
            phoneId = ImsCommonUtil.getMainCapabilityPhoneId();
        }

        return mImsRegInfo[phoneId];
    }

    /**
     * Use to query multiple ims support count.
     *@return modem multi ims count
     */
    protected int getModemMultiImsCount() {
        log("getModemMultiImsCount");
        synchronized (mMultiImsSyncLock) {
            if (mModemMultiImsCount != -1) {
                return mModemMultiImsCount;
            } else {
                log("mModemMultiImsCount not initialized, wait...");
                return mModemMultiImsCount;
            }
        }
    }

    /**
     *create wifiOffloadListnerProxy.
     *@return return wifiOffloadLisetnerProxy
     *@hide
     */
    private IWifiOffloadListenerProxy createWifiOffloadListenerProxy() {
        if (mProxy == null) {
            log("create WifiOffloadListenerProxy");
            mProxy = new IWifiOffloadListenerProxy();
        }
        return mProxy;
    }

    /**
     *transfer sip error cause to wfc specified error cause
     *@param sipErrorCode sip error code.
     *@param sipMethod sip operration. (0:REG, 9:SUBSCRIBE)
     *@param sipReasonText sip reason text. ("BLOCK_WIFI_REG09")
     *@return return wfcRegErrorCode which is used in AP side.
     *@hide
     */
    private int mapToWfcRegErrorCause(int sipErrorCode, int sipMethod, String sipReasonText) {

        int wfcRegErrorCode = WfcReasonInfo.CODE_UNSPECIFIED;

        switch (sipErrorCode) {
            case 403:
                if (sipMethod == 9 && sipReasonText.equals("BLOCK_WIFI_REG09")) {
                    /* REG09: Missing 911 Address */
                    wfcRegErrorCode = WfcReasonInfo.CODE_WFC_911_MISSING;
                }
                else if (sipMethod == 0) {
                    /* REG90: Unable to Connect */
                    wfcRegErrorCode = WfcReasonInfo.CODE_WFC_403_FORBIDDEN;
                } else {
                    /* Error403: Mismatch identities */
                    wfcRegErrorCode = WfcReasonInfo.CODE_WFC_403_MISMATCH_IDENTITIES;
                }
                break;
            case 40301:
                wfcRegErrorCode = WfcReasonInfo.CODE_WFC_403_UNKNOWN_USER;
                break;
            case 40302:
                wfcRegErrorCode = WfcReasonInfo.CODE_WFC_403_ROAMING_NOT_ALLOWED;
                break;
            case 40303:
                /* Error403: Mismatch identities */
                wfcRegErrorCode = WfcReasonInfo.CODE_WFC_403_MISMATCH_IDENTITIES;
                break;
            case 40304:
                wfcRegErrorCode = WfcReasonInfo.CODE_WFC_403_AUTH_SCHEME_UNSUPPORTED;
                break;
            case 40305:
                wfcRegErrorCode = WfcReasonInfo.CODE_WFC_403_HANDSET_BLACKLISTED;
                break;
            case 500:
                wfcRegErrorCode = WfcReasonInfo.CODE_WFC_INTERNAL_SERVER_ERROR;
                break;
            default:
                break;
        }
        log("mapToWfcRegErrorCause(), sipErrorCode:" + sipErrorCode + " sipMethod:" + sipMethod +
            " sipReasonText: " + sipReasonText + " wfcRegErrorCode:" + wfcRegErrorCode);
        return wfcRegErrorCode;
    }

    /**
     * Adapter class for {@link IWifiOffloadListener}.
     */
    private class IWifiOffloadListenerProxy extends WifiOffloadManager.Listener {

        @Override
        public void onHandover(int simIdx, int stage, int ratType) {
            log("onHandover simIdx=" + simIdx + ", stage=" + stage + ", ratType=" + ratType);

            if ((stage == WifiOffloadManager.HANDOVER_END &&
                    mImsRegInfo[simIdx] == ServiceState.STATE_IN_SERVICE)) {
                // only update rat type after a successful handover
                mRAN[simIdx] = ratType;
                notifyRegistrationCapabilityChange(simIdx, mImsExtInfo[simIdx]);
            }
        }

        @Override
        public void onRequestImsSwitch(int simIdx, boolean isImsOn) {
            if (ImsCommonUtil.supportMdAutoSetupIms()) {
                return;
            }
            int mainCapabilityPhoneId = ImsCommonUtil.getMainCapabilityPhoneId();
            log("onRequestImsSwitch simIdx=" + simIdx +
                    " isImsOn=" + isImsOn + " mainCapabilityPhoneId=" + mainCapabilityPhoneId);

            if (simIdx >= mNumOfPhones) {
                loge("onRequestImsSwitch can't enable/disable ims due to wrong sim id");
            }

            if (ImsCommonUtil.supportMims() == false) {
                if (simIdx != mainCapabilityPhoneId) {
                    logw("onRequestImsSwitch, ignore not MainCapabilityPhoneId request");
                    return;
                }
            }

            if (isImsOn) {
                if (mImsState[simIdx] != MtkPhoneConstants.IMS_STATE_ENABLE
                    || mExpectedImsState[simIdx] == MtkPhoneConstants.IMS_STATE_DISABLED) {
                    mImsRILAdapters[simIdx].turnOnIms(
                            mHandler[simIdx].obtainMessage(EVENT_SET_IMS_ENABLED_DONE));
                    mExpectedImsState[simIdx] = MtkPhoneConstants.IMS_STATE_ENABLE;
                    mImsState[simIdx] = MtkPhoneConstants.IMS_STATE_ENABLING;
                } else {
                    log("Ims already enable and ignore to send AT command.");
                }
            } else {
                if (mImsState[simIdx] != MtkPhoneConstants.IMS_STATE_DISABLED
                    || mExpectedImsState[simIdx] == MtkPhoneConstants.IMS_STATE_ENABLE) {
                    mImsRILAdapters[simIdx].turnOffIms(
                            mHandler[simIdx].obtainMessage(EVENT_SET_IMS_DISABLE_DONE));
                    mExpectedImsState[simIdx] = MtkPhoneConstants.IMS_STATE_DISABLED;
                    mImsState[simIdx] = MtkPhoneConstants.IMS_STATE_DISABLING;
                } else {
                    log("Ims already disabled and ignore to send AT command.");
                }
            }
        }
    }

    public ImsCommandsInterface getImsRILAdapter(int phoneId) {
        if (mImsRILAdapters[phoneId] == null) {
            logw("getImsRILAdapter phoneId=" + phoneId + ", mImsRILAdapter is null ");
        }
        return mImsRILAdapters[phoneId];
    }

    public ImsConfigManager getImsConfigManager() {
        return mImsConfigManager;
    }

    /**
     * Binds the WifiOffload service.
     */
    private void checkAndBindWifiOffloadService() {
        IBinder b = ServiceManager.getService(WifiOffloadManager.WFO_SERVICE);

        if (b != null) {
            try {
                b.linkToDeath(mDeathRecipient, 0);
            } catch (RemoteException e) {
            }
            sWifiOffloadService = IWifiOffloadService.Stub.asInterface(b);
        } else {
            loge("can't get WifiOffloadService");
            b = ServiceManager.getService(MwisConstants.MWI_SERVICE);
            try {
                if (b != null) {
                    b.linkToDeath(mDeathRecipient, 0);
                    sWifiOffloadService = IMwiService.Stub.asInterface(b).getWfcHandlerInterface();
                } else {
                    log("No MwiService exist");
                }
            } catch (RemoteException e) {
                Rlog.e(LOG_TAG, "can't get MwiService", e);
            }
        }

        log("checkAndBindWifiOffloadService: sWifiOffloadService = " +
                sWifiOffloadService);
    }

    /**
     * Death recipient class for monitoring WifiOffload service.
     */
    private class IWifiOffloadServiceDeathRecipient implements IBinder.DeathRecipient {
        @Override
        public void binderDied() {
            sWifiOffloadService = null;
        }
    }

    /**
     * Try to bind WifiOffload service and register for handover event.
     */
    void bindAndRegisterWifiOffloadService() {
        if (sWifiOffloadService == null) {
            //first use wifioffloadservice and new the object.
            checkAndBindWifiOffloadService();
            if (sWifiOffloadService != null) {
                try {
                    sWifiOffloadService.registerForHandoverEvent(
                            createWifiOffloadListenerProxy());
                } catch (RemoteException e) {
                    loge("can't register handover event");
                }
            } else {
                loge("can't get WifiOffloadService");
            }
        }
    }

    /**
     * Try to bind MWI service and register for handover event.
     * MWI service and WifiOffload service use the same service name.
     * This is just a wrapper to reduce confusion.
     */
    private void bindAndRegisterMWIService() {
        bindAndRegisterWifiOffloadService();
    }

    private int getRadioTech(int phoneId) throws RemoteException {
        int radioTech = ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN;

        if (ImsCommonUtil.supportMdAutoSetupIms() == false) {
            bindAndRegisterWifiOffloadService();

            if (sWifiOffloadService != null) {
                mRAN[phoneId] = sWifiOffloadService.getRatType(phoneId);
            }
        } else {
            // MWI service does not support getRatType()
            // rat type will be updated via +CIREGU
            bindAndRegisterMWIService();
        }

        switch (mRAN[phoneId]) {
            case WifiOffloadManager.RAN_TYPE_WIFI:
                radioTech = ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN;
                break;
            case WifiOffloadManager.RAN_TYPE_MOBILE_3GPP:
            case WifiOffloadManager.RAN_TYPE_MOBILE_3GPP2:
            default:
                radioTech = ServiceState.RIL_RADIO_TECHNOLOGY_LTE;
                break;
        }
        log("getRadioTech mRAN=" + mRAN[phoneId] + ", radioTech=" + radioTech);
        return radioTech;
    }

    private ImsReasonInfo createImsReasonInfo(int phoneId) {
        ImsReasonInfo imsReasonInfo = null;
        imsReasonInfo = new ImsReasonInfo(ImsReasonInfo.CODE_REGISTRATION_ERROR,
                mRegErrorCode[phoneId], Integer.toString(mRegErrorCode[phoneId]));
        return imsReasonInfo;
    }
    /**
     * Notifies the application when the device is connected/disconnected to the IMS network.
     *
     * @param phoneId   the specific phone index
     * @param imsRegInfo   the registration inforamtion
     *@hide
     */
    private void notifyRegistrationStateChange(int phoneId, int imsRegInfo) {
        synchronized(mLockObj) {
            log("notifyRegistrationStateChange imsRegInfo= " + imsRegInfo
                    + ", phoneId=" + phoneId);

            if (imsRegInfo == ServiceState.STATE_IN_SERVICE) {
                try {
                    int radioTech = getRadioTech(phoneId);

                    if (mListener[phoneId] != null) {
                        mListener[phoneId].registrationConnectedWithRadioTech(radioTech);
                    }
                    mExt.notifyRegistrationStateChange(mRAN[phoneId], mHandler[phoneId],
                            (Object) mImsRILAdapters[phoneId]);
                    mRegErrorCode[phoneId] = ImsReasonInfo.CODE_UNSPECIFIED;
                } catch (RemoteException e) {
                    // error handling. Currently no-op
                    loge("IMS: notifyStateChange fail on access WifiOffloadService");
                }
            } else {
                try {
                    ImsReasonInfo imsReasonInfo = createImsReasonInfo(phoneId);

                    if (mListener[phoneId] != null) {
                        mListener[phoneId].registrationDisconnected(imsReasonInfo);
                    }
                } catch (RemoteException e) {
                    // error handling. Currently no-op
                }
            }
        }
    }

    private void updateCapabilityChange(int phoneId, int imsExtInfo,
            int[] enabledFeatures, int[] disabledFeatures) {
        for (int i = 0; i < IMS_MAX_FEATURE_SUPPORT_SIZE; i++) {
            enabledFeatures[i] = -1;
            disabledFeatures[i] = -1;
        }

        if (mRAN[phoneId] != WifiOffloadManager.RAN_TYPE_WIFI &&
                (imsExtInfo & IMS_VOICE_OVER_LTE) == IMS_VOICE_OVER_LTE) {
            enabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE] =
                    ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE;
            enabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_UT_OVER_LTE] =
                    ImsConfig.FeatureConstants.FEATURE_TYPE_UT_OVER_LTE;
        } else {
            disabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE] =
                    ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE;
            disabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_UT_OVER_LTE] =
                    ImsConfig.FeatureConstants.FEATURE_TYPE_UT_OVER_LTE;
        }

        if (mRAN[phoneId] != WifiOffloadManager.RAN_TYPE_WIFI &&
                (imsExtInfo & IMS_VIDEO_OVER_LTE) == IMS_VIDEO_OVER_LTE) {
            enabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE] =
                    ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE;
        } else {
            disabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE] =
                    ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE;
        }
        /// WFC @{
        if (mRAN[phoneId] == WifiOffloadManager.RAN_TYPE_WIFI &&
                (imsExtInfo & IMS_VOICE_OVER_LTE) == IMS_VOICE_OVER_LTE) {
            enabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI] =
                    ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI;
            enabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_UT_OVER_WIFI] =
                    ImsConfig.FeatureConstants.FEATURE_TYPE_UT_OVER_WIFI;
            log("[WFC]IMS_VOICE_OVER_WIFI");
        } else {
            disabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI] =
                    ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI;
            disabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_UT_OVER_WIFI] =
                    ImsConfig.FeatureConstants.FEATURE_TYPE_UT_OVER_WIFI;
        }

        if (mRAN[phoneId] == WifiOffloadManager.RAN_TYPE_WIFI &&
                (imsExtInfo & IMS_VIDEO_OVER_LTE) == IMS_VIDEO_OVER_LTE) {
            enabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_WIFI] =
                    ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_WIFI;
            log("[WFC]IMS_VIDEO_OVER_WIFI");
        } else {
            disabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_WIFI] =
                    ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_WIFI;
        }
        /// @}
    }

    /**
     * Notifies the application when features on a particular service enabled or
     * disabled successfully based on user preferences.
     *
     * @param phoneId   the specific phone index
     * @param imsExtInfo   the ims feature capability inforamtion.
     *@hide
     */
    private void notifyRegistrationCapabilityChange(int phoneId, int imsExtInfo) {
        log("notifyRegistrationCapabilityChange imsExtInfo= " + imsExtInfo
                + ", phoneId=" + phoneId);

        int[] enabledFeatures = new int[IMS_MAX_FEATURE_SUPPORT_SIZE];
        int[] disabledFeatures = new int[IMS_MAX_FEATURE_SUPPORT_SIZE];
        updateCapabilityChange(phoneId, imsExtInfo, enabledFeatures, disabledFeatures);

        synchronized (mLockObj) {
            try {
                if (mListener[phoneId] != null){
                    mListener[phoneId].registrationFeatureCapabilityChanged(ImsServiceClass.MMTEL,
                            enabledFeatures, disabledFeatures);
                }
            } catch (RemoteException e) {
                // error handling. Currently no-op
            }
        }
    }

    private String eventToString(int eventId) {
        switch (eventId) {
            case EVENT_IMS_REGISTRATION_INFO:
                return "EVENT_IMS_REGISTRATION_INFO";
            case EVENT_RADIO_NOT_AVAILABLE:
                return "EVENT_RADIO_NOT_AVAILABLE";
            case EVENT_SET_IMS_ENABLED_DONE:
                return "EVENT_SET_IMS_ENABLED_DONE";
            case EVENT_SET_IMS_DISABLE_DONE:
                return "EVENT_SET_IMS_DISABLE_DONE";
            case EVENT_IMS_DISABLED_URC:
                return "EVENT_IMS_DISABLED_URC";
            case EVENT_VIRTUAL_SIM_ON:
                return "EVENT_VIRTUAL_SIM_ON";
            case EVENT_INCOMING_CALL_INDICATION:
                return "EVENT_INCOMING_CALL_INDICATION";
            case EVENT_CALL_INFO_INDICATION:
                return "EVENT_CALL_INFO_INDICATION";
            case EVENT_IMS_ENABLING_URC:
                return "EVENT_IMS_ENABLING_URC";
            case EVENT_IMS_ENABLED_URC:
                return "EVENT_IMS_ENABLED_URC";
            case EVENT_IMS_DISABLING_URC:
                return "EVENT_IMS_DISABLING_URC";
            case EVENT_SIP_CODE_INDICATION:
                return "EVENT_SIP_CODE_INDICATION";
            case EVENT_ON_NETWORK_INIT_USSI:
                return "EVENT_ON_NETWORK_INIT_USSI";
            case EVENT_IMS_DEREG_DONE:
                return "EVENT_IMS_DEREG_DONE";
            case EVENT_IMS_DEREG_URC:
                return "EVENT_IMS_DEREG_URC";
            case EVENT_RADIO_OFF:
                return "EVENT_RADIO_OFF";
            case EVENT_RADIO_ON:
                return "EVENT_RADIO_ON";
            case EVENT_IMS_RTP_INFO_URC:
                return "EVENT_IMS_RTP_INFO_URC";
            case EVENT_SET_IMS_REGISTRATION_REPORT_DONE:
                return "EVENT_SET_IMS_REGISTRATION_REPORT_DONE";
            case EVENT_IMS_VOLTE_SETTING_URC:
                return "EVENT_IMS_VOLTE_SETTING_URC";
            case EVENT_MULTI_IMS_COUNT_URC:
                return "EVENT_MULTI_IMS_COUNT_URC";
            default:
                return "UNKNOWN EVENT";
        }
    }

    /**
     *Ims service Message hanadler.
     *@hide
     */
    private class MyHandler extends Handler {

        int mSocketId;

        /**
         * Constructor associates this handler with the socket Id
         * @param socketId The socket id, must not be less than 0.
         */
        public MyHandler(int socketId) {
            super(null, false);
            mSocketId = socketId;
        }

        @Override
        public void handleMessage(Message msg) {
            AsyncResult ar;
            Intent intent;
            log("receive " + eventToString(msg.what) + ", mSocketId=" + mSocketId);
            switch (msg.what) {
                case EVENT_IMS_REGISTRATION_INFO:

                    /**
                     * According to 3GPP TS 27.007 +CIREGU format
                     *
                     * AsyncResult.result is an Object[]
                     * ((Object[])AsyncResult.result)[0] is integer type to indicate the IMS regiration status.
                     *                                    0: not registered
                     *                                    1: registered
                     * ((Object[])AsyncResult.result)[1] is numeric value in hexadecimal format to indicate the IMS capability.
                     *                                    1: RTP-based transfer of voice according to MMTEL (see 3GPP TS 24.173 [87])
                     *                                    2: RTP-based transfer of text according to MMTEL (see 3GPP TS 24.173 [87])
                     *                                    4: SMS using IMS functionality (see 3GPP TS 24.341[101])
                     *                                    8: RTP-based transfer of video according to MMTEL (see 3GPP TS 24.183 [87])
                     *
                     */
                    ar = (AsyncResult) msg.obj;

                    int newImsRegInfo = ServiceState.STATE_POWER_OFF;
                    if (((int[]) ar.result)[0] == 1) {
                        newImsRegInfo = ServiceState.STATE_IN_SERVICE;
                    } else {
                        newImsRegInfo = ServiceState.STATE_OUT_OF_SERVICE;
                    }
                    /// M: Simulate IMS Registration @{
                    if (SystemProperties.getInt("persist.ims.simulate", 0) == 1) {
                        newImsRegInfo = (mImsRegistry ?
                                ServiceState.STATE_IN_SERVICE : ServiceState.STATE_OUT_OF_SERVICE);
                        log("Override EVENT_IMS_REGISTRATION_INFO: newImsRegInfo=" +
                                newImsRegInfo);
                    }
                    /// @}
                    int newImsExtInfo = ((int[]) ar.result)[1];
                    // 93MD IMS framework can NOT get ims RAN type, transfer EWFC state
                    // by ext_info.
                    if (ImsCommonUtil.supportMdAutoSetupIms()) {
                        if ((newImsExtInfo & IMS_VOICE_OVER_WIFI) == IMS_VOICE_OVER_WIFI) {
                            mRAN[mSocketId] = WifiOffloadManager.RAN_TYPE_WIFI;
                        } else {
                            // switch to default value
                            mRAN[mSocketId] = WifiOffloadManager.RAN_TYPE_MOBILE_3GPP;
                        }
                    }

                    /* notify upper application the IMS registration status is chagned */
                    log("newReg:" + newImsRegInfo + " oldReg:" + mImsRegInfo[mSocketId]);

                    mImsRegInfo[mSocketId] = newImsRegInfo;
                    notifyRegistrationStateChange(mSocketId, mImsRegInfo[mSocketId]);

                    /* notify upper application the IMS capability is chagned when IMS is registered */
                    log("newRegExt:" + newImsExtInfo + "oldRegExt:" +  mImsExtInfo[mSocketId]);

                    if ((mImsRegInfo[mSocketId] == ServiceState.STATE_IN_SERVICE)) {
                        mImsExtInfo[mSocketId] = newImsExtInfo;
                    } else {
                        mImsExtInfo[mSocketId] = 0;
                    }
                    // reset xui to null string when ims disconnected.
                    if (mImsRegInfo[mSocketId] == ServiceState.STATE_OUT_OF_SERVICE) {
                        ImsXuiManager.getInstance().setXui(mSocketId,null);
                    }
                    notifyRegistrationCapabilityChange(mSocketId, mImsExtInfo[mSocketId]);
                    break;
                case EVENT_IMS_ENABLING_URC:
                    //+EIMS: 1

                    // notify AP Ims Service is up only when state changed to ENABLE
                    if (mImsState[mSocketId] != MtkPhoneConstants.IMS_STATE_ENABLE) {
                        intent = new Intent(ImsManager.ACTION_IMS_SERVICE_UP);
                        intent.putExtra(ImsManager.EXTRA_PHONE_ID, mSocketId);
                        mContext.sendBroadcast(intent);
                        log("broadcast IMS_SERVICE_UP");
                    }
                    if (ImsCommonUtil.supportMdAutoSetupIms() == false) {
                        // enable ImsAdapter
                        enableImsAdapter(mSocketId);
                    }
                    mImsState[mSocketId] = MtkPhoneConstants.IMS_STATE_ENABLE;
                    break;
                case EVENT_IMS_ENABLED_URC:
                    //+EIMCFLAG: 1
                    break;
                case EVENT_IMS_DISABLING_URC:
                    //+EIMS: 0
                    break;
                case EVENT_IMS_DISABLED_URC:
                    //+EIMCFLAG: 0
                    disableIms(mSocketId, true);
                    break;
                case EVENT_SET_IMS_ENABLED_DONE:
                    ar = (AsyncResult) msg.obj;
                    if (ar.exception != null) {
                        logw("turnOnIms failed, return to disabled state!");
                        disableIms(mSocketId, false);
                    }
                    break;
                case EVENT_INCOMING_CALL_INDICATION:
                    ar = (AsyncResult) msg.obj;
                    sendIncomingCallIndication(mSocketId, ar);
                    break;
                case EVENT_SIP_CODE_INDICATION:
                    ar = (AsyncResult) msg.obj;
                    String[] sipMessage = (String[]) ar.result;
                    /* ESIPCPI:
                     * <call_id>,<dir>,<SIP_msg_type>,<method>,<response_code>,
                     * <reason_text>,<socket Id>
                    */
                    if (sipMessage != null) {
                        log("Method =" + sipMessage[3] + " response_code =" + sipMessage[4] +
                            " reason_text =" + sipMessage[5]);

                        int sipMethod = Integer.parseInt(sipMessage[3]);
                        int sipResponseCode = Integer.parseInt(sipMessage[4]);
                        String sipReasonText = sipMessage[5];
                        if (sipMethod == 0 || sipMethod == 9) {
                            if (mRAN[mSocketId] == WifiOffloadManager.RAN_TYPE_WIFI) {
                                mRegErrorCode[mSocketId] = mapToWfcRegErrorCause(sipResponseCode,
                                        sipMethod, sipReasonText);
                            }else {
                                mRegErrorCode[mSocketId] = sipResponseCode;
                            }
                        }
                    }
                    break;
                /// M: Event for network initiated USSI @{
                case EVENT_ON_NETWORK_INIT_USSI:
                    ar = (AsyncResult) msg.obj;
                    // +EIUSD: <m>,<n>,<str>,<lang>
                    String[] eiusd = (String[]) ar.result;

                    ImsCallProfile imsCallProfile = onCreateCallProfile(1,
                            ImsCallProfile.SERVICE_TYPE_NORMAL, ImsCallProfile.CALL_TYPE_VOICE);
                    imsCallProfile.setCallExtraInt(ImsCallProfile.EXTRA_DIALSTRING,
                            ImsCallProfile.DIALSTRING_USSD);
                    imsCallProfile.setCallExtra("m", eiusd[0]);
                    imsCallProfile.setCallExtra("n", eiusd[1]);
                    imsCallProfile.setCallExtra("str", eiusd[2]);

                    mPendingMT = new ImsCallSessionProxy(mContext, imsCallProfile,
                            null, ImsService.this, mHandler[mSocketId], mImsRILAdapters[mSocketId],
                            "1", mSocketId);

                    intent = new Intent(ImsManager.ACTION_IMS_INCOMING_CALL);
                    intent.putExtra(ImsManager.EXTRA_USSD, true);
                    intent.putExtra(ImsManager.EXTRA_CALL_ID, "1");
                    intent.putExtra(ImsManager.EXTRA_SERVICE_ID, mapPhoneIdToServiceId(mSocketId));
                    mContext.sendBroadcast(intent);
                    break;
                /// @}
                case EVENT_IMS_DEREG_DONE:
                    // Only log for tracking ims deregister command response
                    break;
                case EVENT_IMS_DEREG_URC:
                    //+EIMSDEREG
                    intent = new Intent(MtkImsManager.ACTION_IMS_SERVICE_DEREGISTERED);
                    intent.putExtra(ImsManager.EXTRA_PHONE_ID, mSocketId);
                    mContext.sendBroadcast(intent);
                    break;
                case EVENT_SET_IMS_REGISTRATION_REPORT_DONE:
                    // Only log for tracking
                    break;
                case EVENT_RADIO_NOT_AVAILABLE:
                    disableIms(mSocketId, false);
                    break;
                case EVENT_RADIO_OFF:
                    updateRadioState(RadioState.RADIO_OFF.ordinal(), mSocketId);
                    break;
                case EVENT_RADIO_ON:
                    updateRadioState(RadioState.RADIO_ON.ordinal(), mSocketId);
                    break;
                /// M: Sync volte setting value. @{
                case EVENT_IMS_VOLTE_SETTING_URC:
                    ar = (AsyncResult) msg.obj;
                    boolean enable = ((int[]) ar.result)[0] == 1;
                    log("Volte_Setting_Enable=" + enable);
                    SystemProperties.set(PROPERTY_IMSCONFIG_FORCE_NOTIFY, "1");
                    MtkImsManager.setEnhanced4gLteModeSetting(mContext, enable, mSocketId);
                    SystemProperties.set(PROPERTY_IMSCONFIG_FORCE_NOTIFY, "0");
                    break;
                /// @}
                case EVENT_RUN_GBA:
                    log("receive EVENT_RUN_GBA: Enter messege");

                    NafSessionKeyResult result;
                    String[] nafInfoTemp;

                    ar = (AsyncResult) msg.obj;
                    nafInfoTemp = (String[]) ar.result;
                    result = (NafSessionKeyResult) ar.userObj;

                    synchronized(result.lockObj) {
                        if (ar.exception != null) {
                            result.cmdResult = IMS_SS_CMD_ERROR;
                            log("receive EVENT_RUN_GBA: IMS_SS_CMD_ERROR");
                        } else {
                            log("receive EVENT_RUN_GBA: hexkey:" + nafInfoTemp[0] +
                                    ", btid:" + nafInfoTemp[2] + ", keylifetime:" + nafInfoTemp[3]);
                            NafSessionKey nafKey = new NafSessionKey(
                                    nafInfoTemp[2], ImsCommonUtil.hexToBytes(nafInfoTemp[0]), nafInfoTemp[3]);

                            result.nafSessionKey = nafKey;
                            result.cmdResult = IMS_SS_CMD_SUCCESS;
                            log("receive EVENT_RUN_GBA: IMS_SS_CMD_SUCCESS");
                        }
                        result.lockObj.notify();
                        log("receive EVENT_RUN_GBA: notify result");
                    }
                    break;
                case EVENT_MULTI_IMS_COUNT_URC:
                    ar = (AsyncResult) msg.obj;
                    synchronized (mMultiImsSyncLock) {
                        mModemMultiImsCount = ((int[]) ar.result)[0];
                    }
                    log("mModemMultiImsCount=" + mModemMultiImsCount);
                    break;
                default:
                    break;
            }
            mExt.notifyImsServiceEvent(mSocketId, mContext, msg);
        }
    }

    /**
     * Notify AP IMS Service is disabled and disable ImsAdapter.
     *@param isNormalDisable is IMS service disabled normally or abnormally
     *@hide
     */
    private void disableIms(int phoneId, boolean isNormalDisable) {
        if (ImsCommonUtil.supportMdAutoSetupIms() == false) {
            disableImsAdapter(phoneId,isNormalDisable);
        }
        mImsState[phoneId] = MtkPhoneConstants.IMS_STATE_DISABLED;
        mExpectedImsState[phoneId] = MtkPhoneConstants.IMS_STATE_DISABLED;
    }

    /**
     * initialize ims avalability, turn on/off ims for specified phone id
     * @param phoneId
     * @param capabilityOffset
     */
    private void initImsAvailability(int phoneId, int capabilityOffset,
            int enableMessageId, int disableMessageId) {
        int volteCapability = SystemProperties.getInt("persist.mtk.volte.enable", 0);
        int wfcCapability = SystemProperties.getInt("persist.mtk.wfc.enable", 0);
        if ((volteCapability & (1 << capabilityOffset)) > 0
                || (wfcCapability & (1 << capabilityOffset)) > 0) {
            log("initImsAvailability turnOnIms : " + phoneId);
            mImsRILAdapters[phoneId].turnOnIms(mHandler[phoneId].obtainMessage(enableMessageId));
            mImsState[phoneId] = MtkPhoneConstants.IMS_STATE_ENABLING;
        } else {
            log("initImsAvailability turnOffIms : " + phoneId);
            mImsRILAdapters[phoneId].turnOffIms(mHandler[phoneId].obtainMessage(disableMessageId));
            mImsState[phoneId] = MtkPhoneConstants.IMS_STATE_DISABLING;
        }

        // IMS Ctrl flow error handling
        // report RADIO_UNAVAILABL here in case of ImsService died
        updateRadioState(RadioState.RADIO_UNAVAILABLE.ordinal(), phoneId);
    }

    public int getRatType(int phoneId) {
        return mRAN[phoneId];
    }

    private void log(String s) {
        if (DBG) {
            Rlog.d(LOG_TAG, s);
        }
    }

    private void englog(String s) {
        if (ENGLOAD) {
            log(s);
        }
    }

    private void logw(String s) {
        Rlog.w(LOG_TAG, s);
    }

    private void loge(String s) {
        Rlog.e(LOG_TAG, s);
    }

    /**
     *call interface for allowing/refusing the incoming call indication send to App.
     *@hide
     */
    protected void onSetCallIndication(int serviceId, String callId, String callNum, int seqNum,
                                       boolean isAllow) {
        /* leave blank */
        int phoneId = mapServiceIdToPhoneId(serviceId);
        if (isAllow) {
            ImsCallProfile imsCallProfile = new ImsCallProfile();
            if ((callNum != null) && (!callNum.equals(""))) {
                loge("setCallIndication new call profile: " + callNum);
                imsCallProfile.setCallExtra(ImsCallProfile.EXTRA_OI, callNum);
                imsCallProfile.setCallExtraInt(ImsCallProfile.EXTRA_OIR,
                        ImsCallProfile.OIR_PRESENTATION_NOT_RESTRICTED);
            }

            if (mPendingMT != null) {
                try {
                    mPendingMT.close();
                } catch (RemoteException e) {
                    // error handling. Currently no-op
                    loge("setCallIndication: can't close pending MT");
                }
            }
            mMtkPendingMT = new MtkImsCallSessionProxy(mContext, imsCallProfile,
                    null, ImsService.this, mHandler[phoneId], mImsRILAdapters[phoneId], callId,
                    phoneId);
            mImsRILAdapters[phoneId].setCallIndication(IMS_ALLOW_INCOMING_CALL_INDICATION,
                    Integer.parseInt(callId), seqNum);
        } else {
            mImsRILAdapters[phoneId].setCallIndication(IMS_DISALLOW_INCOMING_CALL_INDICATION,
                    Integer.parseInt(callId), seqNum);
        }
    }

    IMtkImsCallSession onGetPendingMtkCallSession(int serviceId, String callId) {
        int phoneId = mapServiceIdToPhoneId(serviceId);

        // This API is for incoming call to create IImsCallSession
        if (mMtkPendingMT == null) {
            return null;
        }

        IMtkImsCallSession pendingMT = mMtkPendingMT;

        try {
            if (pendingMT.getCallId().equals(callId)) {
                mMtkPendingMT = null;
                return pendingMT;
            }
        } catch (RemoteException e) {
            // error handling. Currently no-op
        }

        return null;
    }

    private void sendIncomingCallIndication(int phoneId, AsyncResult ar) {
        // +EAIC:<call_id>,<number>,<type>,<call_mode>,<seq_no>
        String callId = ((String[]) ar.result)[0];
        String dialString = ((String[]) ar.result)[1];
        String callMode = ((String[]) ar.result)[3];
        String seqNum = ((String[]) ar.result)[4];
        int serviceId = mapPhoneIdToServiceId(phoneId);

        log("IMS: sendIncomingCallIndication() call_id = " + callId +
                " dialString = " +  dialString + " seqNum = " + seqNum +
                " serviceId = " + serviceId);

        Intent intent = new Intent(MtkImsManager.ACTION_IMS_INCOMING_CALL_INDICATION);
        intent.putExtra(ImsManager.EXTRA_CALL_ID, callId);
        intent.putExtra(MtkImsManager.EXTRA_DIAL_STRING, dialString);
        intent.putExtra(MtkImsManager.EXTRA_CALL_MODE, Integer.parseInt(callMode));
        intent.putExtra(MtkImsManager.EXTRA_SEQ_NUM, Integer.parseInt(seqNum));
        intent.putExtra(ImsManager.EXTRA_SERVICE_ID, serviceId);
        mContext.sendBroadcast(intent);
    }
}
