package com.mediatek.op16.settings;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuff;
import android.util.Log;
import android.widget.Toast;

import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.internal.telephony.RILConstants;
import com.mediatek.ims.internal.MtkImsManagerEx;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;
/**
 * Class to support operator customizations for WFC settings.
 */
public class WfcSwitchController {

    private static final String TAG = "Op16WfcSwitchController";
    private static final String AOSP_SETTING_WFC_PREFERENCE = "wifi_calling_settings";
    private static final String OP16_WFC_PREFERENCE_KEY = "op16_wfc_pref_switch";
    private static WfcSwitchController sController = null;

    private Context mPluginContext;
    private Context mAppContext;
    private android.support.v14.preference.SwitchPreference mWfcSwitchSettings = null;
    private android.support.v7.preference.PreferenceScreen mPreferenceScreenSettings = null;
    private CallStateListener mCallListener;

    private WfcSwitchController(Context context) {
       mPluginContext = context;
       mCallListener = new CallStateListener();
    }

    /** Returns instance of OP18WfcSettings.
         * @param context context
         * @return OP18WfcSettings
         */
    public static WfcSwitchController getInstance(Context context) {

        if (sController == null) {
            sController = new WfcSwitchController(context);
        }
        return sController;
    }

    /**.
     * Customize wfc preference
     * @param context Context
     * @param preferenceScreen preferenceScreen
     * @return
     */
    public void customizedWfcPreference(Context context, Object preferenceScreen) {
        mAppContext = context;
        android.preference.Preference wfcPreferenceCallSettings = null;
        android.support.v7.preference.Preference wfcPreferenceSettings = null;
        if (!init(preferenceScreen)) {
            return;
        }
        if (mPreferenceScreenSettings != null) {
            wfcPreferenceSettings =
                    (android.support.v7.preference.Preference) mPreferenceScreenSettings
                    .findPreference(AOSP_SETTING_WFC_PREFERENCE);
            Log.d(TAG, "wfcSettingsPreference: " + wfcPreferenceSettings);
        }

        CharSequence title = null;
        int order = 0;
        if (wfcPreferenceSettings != null) {
            mPreferenceScreenSettings.removePreference(wfcPreferenceSettings);
            title = wfcPreferenceSettings.getTitle();
            order = wfcPreferenceSettings.getOrder();
        }

        checkAndAddWfcSwitch(title, order);

        updateWfcSwitchState();
    }

    private boolean init(Object preferenceScreen) {
        if (preferenceScreen instanceof android.support.v7.preference.PreferenceScreen) {
            mPreferenceScreenSettings =
                    (android.support.v7.preference.PreferenceScreen) preferenceScreen;
            mWfcSwitchSettings =
                    (android.support.v14.preference.SwitchPreference) mPreferenceScreenSettings
                    .findPreference(OP16_WFC_PREFERENCE_KEY);
            Log.d(TAG, "mPreferenceScreenSettings: " + mPreferenceScreenSettings);
        } else {
            Log.d(TAG, "invalid class of preferenceScreen: " + preferenceScreen);
            return false;
        }
        return true;
    }

    private void checkAndAddWfcSwitch(CharSequence title, int order) {
        Log.d(TAG, "mWfcSwitchSettings: " + mWfcSwitchSettings);
        if (mWfcSwitchSettings == null) {
            if (mPreferenceScreenSettings != null) {
                mWfcSwitchSettings =
                        new android.support.v14.preference.SwitchPreference(mAppContext);
                mWfcSwitchSettings
                        .setOnPreferenceChangeListener(new WfcSwitchListenerForSettings());
                mWfcSwitchSettings.setKey(OP16_WFC_PREFERENCE_KEY);
                mWfcSwitchSettings.setTitle(title);
                mWfcSwitchSettings.setOrder(order);
                Drawable wfcIcon = mPluginContext.getResources()
                                              .getDrawable(R.drawable.ic_wifi_calling);
                wfcIcon.setColorFilter(mPluginContext.getResources()
                     .getColor(R.color.dialtacts_secondary_text_color), PorterDuff.Mode.MULTIPLY);
                mWfcSwitchSettings.setIcon(wfcIcon);
                mPreferenceScreenSettings.addPreference(mWfcSwitchSettings);
            }
            ImsManager.setWfcMode(mAppContext,
                    ImsConfig.WfcModeFeatureValueConstants.WIFI_PREFERRED);
        }
    }

    /**.
     * Updates wfc preference
     * @return
     */

    public void updateWfcSwitchState() {
        // Disable switch if PS call ongoing
        if (mWfcSwitchSettings != null) {
            mWfcSwitchSettings.setChecked(ImsManager.isWfcEnabledByUser(mPluginContext));
        }
    }

    private boolean isInSwitchProcess() {
        int imsState = MtkPhoneConstants.IMS_STATE_DISABLED;
        try {
         imsState = MtkImsManagerEx.getInstance().getImsState(RadioCapabilitySwitchUtil
                         .getMainCapabilityPhoneId());
        } catch (ImsException e) {
           return false;
        }
        Log.d(TAG, "isInSwitchProcess , imsState = " + imsState);
        return imsState == MtkPhoneConstants.IMS_STATE_DISABLING
                || imsState == MtkPhoneConstants.IMS_STATE_ENABLING;
    }

    /** Remove preference.
     * @return
     */
    public void removeWfcPreference() {
        if (mPreferenceScreenSettings != null && mWfcSwitchSettings != null) {
            mPreferenceScreenSettings.removePreference(mWfcSwitchSettings);
        }
    }

    /** Registers listener/receiver.
     * @return
     */
    public void register() {
        ((TelephonyManager) mPluginContext.getSystemService(Context.TELEPHONY_SERVICE))
                .listen(mCallListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    /** Unregisters listener/receiver.
     * @return
     */
    public void unRegister() {
        ((TelephonyManager) mPluginContext.getSystemService(Context.TELEPHONY_SERVICE))
                .listen(mCallListener, PhoneStateListener.LISTEN_NONE);
    }

    /**.
     * Listener Class for support.v7 type preference
     */
    private class WfcSwitchListenerForSettings implements
            android.support.v7.preference.Preference.OnPreferenceChangeListener {

        public WfcSwitchListenerForSettings() {
        }

        @Override
        public boolean onPreferenceChange(android.support.v7.preference.Preference preference,
                Object newValue) {
            // TODO: check if newValue is right
            //final boolean isChecked = !mWfcSwitchSettings.isChecked();
            boolean isChecked = ((Boolean) newValue).booleanValue();
            if (isInSwitchProcess()) {
                Toast.makeText(mAppContext, "Operation not allowed", Toast.LENGTH_SHORT)
                    .show();
                return false;
            }
            ImsManager.setWfcSetting(mAppContext, isChecked);
            return true;
        }
    }

    /**.
     *Listener Class for Call state change
     */
    private class CallStateListener extends PhoneStateListener {
    @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            if (!ImsManager.isWfcEnabledByPlatform(mPluginContext)) {
                Log.d(TAG, "isWfcEnabledByPlatform: false");
                return;
            }
            // TODO Auto-generated method stub
            int phoneType = getPhoneType();
            Log.d(TAG, "in onCallStateChanged state:" + state + ", phoneType:" + phoneType);
            switch (state) {
                case TelephonyManager.CALL_STATE_OFFHOOK:
                case TelephonyManager.CALL_STATE_RINGING:
                    if (phoneType == RILConstants.IMS_PHONE) {
                        if (mWfcSwitchSettings != null) {
                            mWfcSwitchSettings.setEnabled(false);
                        }
                    }
                break;

                case TelephonyManager.CALL_STATE_IDLE:
                default:
                    if (mWfcSwitchSettings != null) {
                        mWfcSwitchSettings.setEnabled(true);
                    }
                break;
            }
        }
    }

    private int getPhoneType() {
        int phoneType = TelephonyManager.PHONE_TYPE_NONE;
        for (int i = 0; i < TelephonyManager.getDefault().getSimCount(); i++) {
            Log.d(TAG, "simCount: " + i);
            int[] subIds = SubscriptionManager.getSubId(i);
            if (subIds == null || subIds.length == 0) {
                continue;
            }
            Log.d(TAG, "subIds: " + subIds);
            if (TelephonyManager.getDefault().getCallState(subIds[0])
                        != TelephonyManager.CALL_STATE_IDLE) {
                phoneType = TelephonyManager.getDefault().getCurrentPhoneType(subIds[0]);
                break;
            }
        }
        return phoneType;
    }
}
