package com.mediatek.op01.dialer.speeddial;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.SystemProperties;
import android.telecom.PhoneAccount;
import android.telecom.TelecomManager;
import android.telecom.VideoProfile;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupMenu;

import com.android.contacts.common.list.ContactListItemView;

import com.mediatek.dialer.ext.DefaultDialPadExtension;
import com.mediatek.op01.dialer.PhoneStateUtils;
import com.mediatek.op01.dialer.R;
import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.lang.ref.WeakReference;

import java.util.ArrayList;
import java.util.List;

public class Op01DialPadExtension extends DefaultDialPadExtension
    implements View.OnLongClickListener, PhoneStateUtils.OnChangedListener {

    private static final String TAG = "OP01DialPadExtension";

    private static final boolean DEBUG = ("eng".equals(Build.TYPE) ||
                                        "userdebug".equals(Build.TYPE));

    private Activity mHostActivity;
    private String mHostPackage;
    private Resources mHostResources;
    private EditText mEditText;
    private Context mContext;
    private int mMenuId = 0x1000;

    private PhoneStateUtils mPhoneStateUtils;
    WeakReference<ContactListItemView> mContactListItemView;
    WeakReference<Menu> mMenu;

    public final static int SHORTCUT_DIRECT_CALL = 0;
    public final static int SHORTCUT_CREATE_NEW_CONTACT = 1;
    public final static int SHORTCUT_ADD_TO_EXISTING_CONTACT = 2;
    public final static int SHORTCUT_SEND_SMS_MESSAGE = 3;
    public final static int SHORTCUT_MAKE_VIDEO_CALL = 4;
    public final static int SHORTCUT_BLOCK_NUMBER = 5;

    /**
     * for op01
     * @param durationView the duration text
     */

    public Op01DialPadExtension (Context context) {
        super();
        mContext = context;
        mPhoneStateUtils = PhoneStateUtils.getInstance(context);
        mPhoneStateUtils.addPhoneStateListener(this);
    }

    @Override
    public void buildOptionsMenu(final Activity activity, Menu menu){
        mPhoneStateUtils.setCurrentActiviy(activity);
        int index = menu.size();
        MenuItem speedDialMenu = menu.add(Menu.NONE,
                index, 0, mContext.getText(R.string.call_speed_dial));
        speedDialMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                Log.d(TAG, "SpeedDial onMenuItemClick");
                SpeedDialController.getInstance().enterSpeedDial(activity);
                return true;
            }
        });
    }

    @Override
    public void onViewCreated(Activity activity, View view) {
        Log.d(TAG, "onViewCreated.");
        mHostActivity = activity;

        mHostPackage = activity.getPackageName();
        mHostResources = activity.getResources();

        View two = (View) view.findViewById(mHostResources.getIdentifier("two",
                                "id", mHostPackage));
        two.setOnLongClickListener(this);

        View three = (View) view.findViewById(mHostResources.getIdentifier("three",
                                "id", mHostPackage));
        three.setOnLongClickListener(this);

        View four = (View) view.findViewById(mHostResources.getIdentifier("four",
                                "id", mHostPackage));
        four.setOnLongClickListener(this);

        View five = (View) view.findViewById(mHostResources.getIdentifier("five",
                                "id", mHostPackage));
        five.setOnLongClickListener(this);

        View six = (View) view.findViewById(mHostResources.getIdentifier("six",
                                "id", mHostPackage));
        six.setOnLongClickListener(this);

        View seven = (View) view.findViewById(mHostResources.getIdentifier("seven",
                                "id", mHostPackage));
        seven.setOnLongClickListener(this);

        View eight = (View) view.findViewById(mHostResources.getIdentifier("eight",
                                "id", mHostPackage));
        eight.setOnLongClickListener(this);

        View nine = (View) view.findViewById(mHostResources.getIdentifier("nine",
                                "id", mHostPackage));
        nine.setOnLongClickListener(this);

        mEditText = (EditText) view.findViewById(mHostResources.getIdentifier("digits",
                                "id", mHostPackage));
    }

    @Override
    public boolean onLongClick(View view) {
        int id = view.getId();

        int key = 0;
        if (id == mHostResources.getIdentifier("two","id", mHostPackage)) {
            key = 2;
        }
        else if (id == mHostResources.getIdentifier("three","id", mHostPackage)) {
            key = 3;
        }
        else if (id == mHostResources.getIdentifier("four","id", mHostPackage)) {
            key = 4;
        }
        else if (id == mHostResources.getIdentifier("five","id", mHostPackage)) {
            key = 5;
        }
        else if (id == mHostResources.getIdentifier("six","id", mHostPackage)) {
            key = 6;
        }
        else if (id == mHostResources.getIdentifier("seven","id", mHostPackage)) {
            key = 7;
        }
        else if (id == mHostResources.getIdentifier("eight","id", mHostPackage)) {
            key = 8;
        }
        else if (id == mHostResources.getIdentifier("nine","id", mHostPackage)) {
            key = 9;
        }

        if (key > 0 && key < 10 && mEditText.getText().length() <= 1) {
            SpeedDialController.getInstance().handleKeyLongProcess(mHostActivity, mContext, key);
            mEditText.getText().clear();
            return true;
        }
        return false;
    }

    @Override
    public List<String> getSingleIMEI(List<String> list) {
        Log.d(TAG, "getSingleIMEI");
        if (list == null) {
            return null;
        }

        if (isC2KSupport()) {
            list.clear();
            String meid = "";
            int count = TelephonyManager.getDefault().getPhoneCount();
            for (int i = 0; i < count; i++) {
                String imei = TelephonyManager.getDefault().getImei(i);
                if (DEBUG) {
                    Log.d(TAG, "getSingleIMEI, imei = " + imei);
                }
                if (!TextUtils.isEmpty(imei)) {
                    list.add("IMEI: " + imei);
                }

                if (TextUtils.isEmpty(meid)) {
                    meid = TelephonyManager.getDefault().getMeid(i);
                    if (DEBUG) {
                        Log.d(TAG, "getSingleIMEI, meid = " + meid);
                    }
                }
            }
            meid = "MEID:" + meid;
            list.add(meid);
        } else if (isSigleImeiEnabled()){
            if (list.size() > 1) {
                for (int i = list.size() - 1; i < list.size(); i++) {
                    list.remove(i);
                }
            }
        }
        return list;
    }

    private boolean isSigleImeiEnabled() {
        return SystemProperties.get("ro.mtk_single_imei").equals("1");
    }

    private boolean isC2KSupport() {
         return "1".equals(SystemProperties.get("ro.boot.opt_c2k_support"));
    }

    @Override
    public void constructPopupMenu(PopupMenu popupMenu, View anchorView, Menu menu) {
        mMenu = new WeakReference<Menu> (menu);
        MenuItem item = menu.findItem(mMenuId);
        boolean canStart = mPhoneStateUtils.canStartVideoCall();
        String number = mEditText.getText().toString();
        if (DEBUG) {
            Log.d(TAG, "constructPopupMenu, canStartVideoCall = "
                                + canStart + ", number = " + number);
        }

        if ((!canStart) || TextUtils.isEmpty(number)) {
            //Can not start video call, remove video call item
            menu.removeItem(mMenuId);
            return;
        } else {
            //Meaning already have video call item, just return
            if (item != null) {
                return;
            }
        }

        MenuItem videoMenu = menu.add(Menu.NONE,
                mMenuId, 0, mContext.getText(R.string.start_video_dial));
        videoMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                Log.d(TAG, "video onMenuItemClick");
                placeOutgoingVideoCall();
                return true;
            }
        });
    }

    private Uri getCallUri(String number) {
        if (DEBUG) {
            Log.d(TAG, "getCallUri, number = " + number);
        }

        if (PhoneNumberUtils.isUriNumber(number)) {
             return Uri.fromParts(PhoneAccount.SCHEME_SIP, number, null);
        }
        return Uri.fromParts(PhoneAccount.SCHEME_TEL, number, null);
     }

    private void placeOutgoingVideoCall() {
        TelecomManager telecommMgr = (TelecomManager)
                mContext.getSystemService(Context.TELECOM_SERVICE);
        if (telecommMgr == null) {
            return;
        }

        if (mHostActivity == null || mEditText == null) {
            return;
        }

        final Intent intent = new Intent(Intent.ACTION_CALL,
                        getCallUri(mEditText.getText().toString()));
        intent.putExtra(TelecomManager.EXTRA_START_CALL_WITH_VIDEO_STATE,
                VideoProfile.STATE_BIDIRECTIONAL);

        telecommMgr.placeCall(intent.getData(), intent.getExtras());
        mEditText.getText().clear();
    }

    @Override
    public void onCallStatusChange(int state) {
        Log.d(TAG, "onCallStatusChange");
        updateShortcutView(state);
        updateVideoMenuItem(state);
    }

    @Override
    public void customizeDialerOptions(View view, int type, String number) {
        if (SHORTCUT_MAKE_VIDEO_CALL != type) {
            return;
        }
        if (view instanceof ContactListItemView) {
            ContactListItemView viewItem = (ContactListItemView) view;
            mContactListItemView = new WeakReference<ContactListItemView> (viewItem);
        }
        updateShortcutView(mPhoneStateUtils.getPhoneState());
    }

    private void updateVideoMenuItem(int state) {
        if (mMenu == null) {
            return;
        }

        Menu menu = mMenu.get();
        if (menu == null) {
            return;
        }

        if (mEditText == null) {
            return;
        }

        String number = mEditText.getText().toString();
        if (TextUtils.isEmpty(number)) {
            return;
        }

        if (state != TelephonyManager.CALL_STATE_IDLE) {
            MenuItem item = menu.findItem(mMenuId);
            if (item != null) {
                menu.removeItem(mMenuId);
            }
        } else {
            MenuItem item = menu.findItem(mMenuId);
            if (item != null) {
                return;
            }

            if (mPhoneStateUtils.hasVideoCapability()) {
                MenuItem videoMenu = menu.add(Menu.NONE,
                        mMenuId, 0, mContext.getText(R.string.start_video_dial));
                videoMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Log.d(TAG, "video onMenuItemClick");
                        placeOutgoingVideoCall();
                        return true;
                    }
                });
            }
        }
    }

    private void updateShortcutView(int state) {
        if (mContactListItemView == null) {
            return;
        }

        ContactListItemView view = mContactListItemView.get();
        if (view == null) {
            return;
        }

        if (state != TelephonyManager.CALL_STATE_IDLE) {
            Log.d(TAG, "updateShortcutView set view as GONE.");
            view.setVisibility(View.GONE);
        } else {
            if (mPhoneStateUtils.hasVideoCapability()) {
                Log.d(TAG, "updateShortcutView set view as VISIBLE.");
                view.setVisibility(View.VISIBLE);
                view.invalidate();
            }
        }
    }

    @Override
    public boolean isSupportVideoCallIcon(boolean support) {
        return false;
    }
}

