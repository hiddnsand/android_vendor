package com.mediatek.mms.ext;

import android.content.ContentResolver;
import android.database.sqlite.SQLiteException;
import android.provider.Settings;
import android.provider.Telephony.Mms;
import android.provider.Telephony.MmsSms;
import android.util.Log;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import com.mediatek.mms.callback.ITransactionServiceCallback;

public class DefaultOpTransactionServiceExt extends ContextWrapper implements
        IOpTransactionServiceExt {
    private static final String TAG = "DefaultOpTransactionServiceExt";
    private Context mContext = null;

    public DefaultOpTransactionServiceExt(Context base) {
        super(base);
        mContext = base;
    }

    @Override
    public boolean handleTransactionProcessed(
            ITransactionServiceCallback callback, int result,
            Object transaction, Intent intent) {
        return false;
    }

    @Override
    public void init(ITransactionServiceCallback transactionCallback){

    }

    @Override
    public void setTransactionFail(Context context, int failType, Cursor cursor) {

    }

    @Override
    public boolean onNewIntent(Uri pduUri, int failureType) {
        return !isPendingMmsNeedRestart(pduUri, failureType);
    }

    @Override
    public void onOpNewIntent(Intent intent) {
    }

    @Override
    public boolean processTransaction(boolean isCancelling) {
        return false;
    }

    @Override
    public void handleOpTransactionProcessed(Intent intent, boolean isReadRecTransaction,
            boolean subDisabled) {
    }

    @Override
    public void cancelTransaction(Context context, Uri uri) {
    }

    @Override
    public boolean cancelandRemoveTransaction() {
        return false;
    }

    @Override
    public void markStateComplete(Uri uri) {

    }

    /// M: ALPS00545779, for FT, restart pending receiving mms @ {
    /**
     * isPendingMmsNeedRestart.
     * @param pduUri Uri
     * @param failureType int
     * @return true if restart.
     */
    private boolean isPendingMmsNeedRestart(Uri pduUri, int failureType) {
        Log.d(TAG, "isPendingMmsNeedRestart, uri=" + pduUri);

        final int pduColumnStatus = 2;
        final String[] pduProjection = new String[] {
            Mms.MESSAGE_BOX,
            Mms.MESSAGE_ID,
            Mms.STATUS,
        };
        Cursor c = null;
        ContentResolver contentResolver = mContext.getContentResolver();

        try {
            c = contentResolver.query(pduUri, pduProjection, null, null, null);

            if ((c == null) || (c.getCount() != 1) || !c.moveToFirst()) {
                Log.d(TAG, "Bad uri");
                return true;
            }

            int status = c.getInt(pduColumnStatus);
            Log.v(TAG, "status" + status);

            /* This notification is not processed yet, so need restart*/
            if (status == 0) {
                return true;
            }
            /* DEFERRED_MASK is not set, it is auto download*/
            if ((status & 0x04) == 0) {
                return isTransientFailure(failureType);
            }
            /* Reach here means it is manully download*/
            return false;
        } catch (SQLiteException e) {
            Log.e(TAG, "Catch a SQLiteException when query: ", e);
            return isTransientFailure(failureType);
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    private static boolean isTransientFailure(int type) {
        Log.d(TAG, "isTransientFailure, type=" + type);
        return (type > MmsSms.NO_ERROR && type < MmsSms.ERR_TYPE_GENERIC_PERMANENT);
    }
    /// @}
}

