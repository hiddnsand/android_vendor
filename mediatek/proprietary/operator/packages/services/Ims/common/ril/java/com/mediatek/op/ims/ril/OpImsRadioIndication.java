package com.mediatek.op.ims.ril;

import static com.mediatek.opcommon.telephony.MtkRILConstantsOp.RIL_UNSOL_RTT_MODIFY_RESPONSE;
import static com.mediatek.opcommon.telephony.MtkRILConstantsOp.RIL_UNSOL_RTT_TEXT_RECEIVE;
import static com.mediatek.opcommon.telephony.MtkRILConstantsOp.RIL_UNSOL_GTT_CAPABILITY_INDICATION;
import static com.mediatek.opcommon.telephony.MtkRILConstantsOp.RIL_UNSOL_RTT_MODIFY_REQUEST_RECEIVE;

import vendor.mediatek.hardware.radio_op.V1_1.IImsRadioIndicationOp;
import android.os.AsyncResult;
import android.os.RemoteException;

public class OpImsRadioIndication extends IImsRadioIndicationOp.Stub {

    OpImsRadioIndication(OpImsRIL ril, int phoneId) {
        mRil= ril;
        mPhoneId = phoneId;
        mRil.riljLogv("OpImsRadioIndication, phone = " + mPhoneId);
    }

    // IMS RIL Instance
    private OpImsRIL mRil;
    // Phone Id
    private int mPhoneId;

    /**
     * RTT Modify Response
     * URC: RIL_UNSOL_RTT_MODIFY_RESPONSE
     * @param type Indication Type
     * @param callId Call Id
     * @param length Length of text
     * @param text Text
     */
    @Override
    public void rttModifyResponse(int indicationType, int callid, int result) {

        mRil.processIndication(indicationType);

        int ret[] = {callid, result};

        if (OpImsRIL.IMS_RILA_LOGD) {
            mRil.unsljLogRet(RIL_UNSOL_RTT_MODIFY_RESPONSE, ret);
        }

        if (mRil.mRttModifyResponseRegistrants != null) {
            mRil.mRttModifyResponseRegistrants.notifyRegistrants(new AsyncResult(null, ret,
                                                                                 null));
        }
    }

    /**
     * RTT Text Receive
     * URC: RIL_UNSOL_RTT_TEXT_RECEIVE
     * @param type Indication Type
     * @param callId Call Id
     * @param length Length of text
     * @param text Text
     */
    @Override
    public void rttTextReceive(int indicationType, int callid, int length, String text) {

        mRil.processIndication(indicationType);

        String strCallId = Integer.toString(callid);
        String strLength = Integer.toString(length);

        String ret[] = {strCallId, strLength, text};

        if (OpImsRIL.IMS_RILA_LOGD) {
            mRil.unsljLogRet(RIL_UNSOL_RTT_TEXT_RECEIVE, ret);
        }

        if (mRil.mRttTextReceiveRegistrants != null) {
            mRil.mRttTextReceiveRegistrants.notifyRegistrants(new AsyncResult(null, ret,
                                                                              null));
        }
    }

    /**
     * RTT Modify Request Receive
     * URC: RIL_UNSOL_RTT_MODIFY_REQUEST_RECEIVE
     * @param type Indication Type
     * @param callid Call Id
     * @param rttType Type
     */
    @Override
    public void rttModifyRequestReceive(int indicationType, int callid, int rttType) {

        mRil.processIndication(indicationType);

        int ret[] = {callid, rttType};

        if (OpImsRIL.IMS_RILA_LOGD) {
            mRil.unsljLogRet(RIL_UNSOL_RTT_MODIFY_REQUEST_RECEIVE, ret);
        }

        if (mRil.mRttModifyRequestReceiveRegistrants != null) {
            mRil.mRttModifyRequestReceiveRegistrants.notifyRegistrants(new AsyncResult(null, ret,
                                                                                       null));
        }
    }

    /**
     * GTT Capability Indication
     * URC: RIL_UNSOL_GTT_CAPABILITY_INDICATION
     * @param type Indication Type
     * @param inCallNotify Call Notification object
     */
    @Override
    public void gttCapabilityIndication(int indicationType, int callid,
                                        int localCapability, int remoteCapability,
                                        int localStatus, int remoteStatus) {

        mRil.processIndication(indicationType);

        int ret[] = {callid, localCapability, remoteCapability, localStatus, remoteStatus};

        if (OpImsRIL.IMS_RILA_LOGD) {
            mRil.unsljLogRet(RIL_UNSOL_GTT_CAPABILITY_INDICATION, ret);
        }

        if (mRil.mGttCapabilityIndicatorRegistrants != null) {
            mRil.mGttCapabilityIndicatorRegistrants.notifyRegistrants(new AsyncResult(null, ret,
                                                                                      null));
        }
    }
}
