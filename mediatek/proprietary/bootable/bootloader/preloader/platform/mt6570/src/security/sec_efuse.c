#include "typedefs.h"
#include "pmic.h"
#include "msdc.h"
#include "pll.h"
#include "sec_efuse.h"

/**************************************************************
 * WDT
 **************************************************************/
void efuse_wdt_restart(void)
{
    mtk_wdt_restart();
}

/****************************************************
 * Fsource
 * return 0 : success
 ****************************************************/
U32 efuse_fsource_set(void)
{
    U32 ret_val = 0;
    U32 get_val = 0;

    ret_val |= pmic_config_interface((kal_uint32)(MT6350_PMIC_RG_VGP2_CAL_ADDR),
            (kal_uint32)(0xB),
            (kal_uint32)(MT6350_PMIC_RG_VGP2_CAL_MASK),
            (kal_uint32)(MT6350_PMIC_RG_VGP2_CAL_SHIFT)
            );

    ret_val |= pmic_config_interface((kal_uint32)(MT6350_PMIC_RG_VGP2_VOSEL_ADDR),
            (kal_uint32)(0x3),
            (kal_uint32)(MT6350_PMIC_RG_VGP2_VOSEL_MASK),
            (kal_uint32)(MT6350_PMIC_RG_VGP2_VOSEL_SHIFT)
            );

    ret_val |= pmic_config_interface((kal_uint32)(MT6350_PMIC_RG_VGP2_EN_ADDR),
            (kal_uint32)(0x1),
            (kal_uint32)(MT6350_PMIC_RG_VGP2_EN_MASK),
            (kal_uint32)(MT6350_PMIC_RG_VGP2_EN_SHIFT)
            );

    mdelay(10);

    return ret_val;
}

U32 efuse_fsource_close(void)
{
    U32 ret_val = 0;

    ret_val |= pmic_config_interface((kal_uint32)(MT6350_PMIC_RG_VGP2_EN_ADDR),
            (kal_uint32)(0x0),
            (kal_uint32)(MT6350_PMIC_RG_VGP2_EN_MASK),
            (kal_uint32)(MT6350_PMIC_RG_VGP2_EN_SHIFT)
            );

    mdelay(10);

    return ret_val;
}
