package com.mediatek.presence.core.ims.service.presence.extension;

import com.android.ims.ImsConnectionStateListener;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.ims.ImsReasonInfo;
import com.android.ims.ImsServiceClass;

import com.mediatek.internal.telephony.MtkPhoneConstants;

import com.mediatek.presence.platform.AndroidFactory;
import com.mediatek.presence.provider.settings.RcsSettings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.telephony.ServiceState;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemProperties;


public class ViLTEExtension extends PresenceExtension {

    public final String TAG = "ViLTEExtension";  
    private Context mContext = AndroidFactory.getApplicationContext();
    private volatile boolean mIsActive;
    private volatile boolean mIsDuplex;
    private int mCurrentNetworkType;
    private int mCurrentMobileNetworkClass;

    private BroadcastReceiver mNetworkChangedReceiver;
    private PhoneStateListener mMobileNetworkClassChangedListener;
    private HandlerThread mThread;

    private ImsConnectionStateListener mImsRegListener;
    private ImsManager mImsManager;

    public ViLTEExtension() {
        super(PresenceExtension.EXTENSION_VILTE);
        extensionName = TAG;
        mIsActive = false;
        mIsDuplex = false;
        mImsManager = ImsManager.getInstance(mContext, getMainCapabilityPhoneId());
    }


    @Override
    public void attachExtension(PresenceExtensionListener listener) {
        // TODO Auto-generated method stub
        logger.debug("attachExtension : " +extensionName);

       //intialize the value
        super.attachExtension(listener);

        mCurrentNetworkType = getNetworkType();
        logger.debug("currentNetworkType : " + mCurrentNetworkType);
        if (mCurrentNetworkType == ConnectivityManager.TYPE_MOBILE) {
            mCurrentMobileNetworkClass = getDataNetworkClass();
            logger.debug("CurrentMobileNetworkClass : " + mCurrentMobileNetworkClass);
        }

        mNetworkChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        handleNetworkChanged(intent);
                    }
                };
                thread.start();
            }
        };
        mContext.registerReceiver(mNetworkChangedReceiver, new IntentFilter(
                ConnectivityManager.CONNECTIVITY_ACTION));

        mThread = new HandlerThread(TAG);
        mThread.start();
        mMobileNetworkClassChangedListener = new PhoneStateListener(
                SubscriptionManager.getDefaultDataSubscriptionId(), mThread.getLooper()) {
            @Override
            public void onServiceStateChanged(ServiceState state) {
                handleServiceStateChanged(state);
            }
        };
        TelephonyManager.getDefault().listen(mMobileNetworkClassChangedListener,
                PhoneStateListener.LISTEN_SERVICE_STATE);

        //initialize values
        if (RcsSettings.getInstance().isIR94VideoCallSupported() &&
                isViLTENetworkSupported())
        {
            mIsActive = true;
            mIsDuplex = true;
        }

        mImsRegListener = new ImsConnectionStateListener() {
            @Override
            public void onFeatureCapabilityChanged(int serviceClass,
                    int[] enabledFeatures, int[] disabledFeatures) {
                logger.debug("onFeatureCapabilityChanged");
            }
            @Override
            public void onImsConnected(int imsRadioTech) {
                logger.debug("onImsConnected imsRadioTech=" + imsRadioTech);
                //TODO: check what action should be taken
                handleImsStateChanged(null);
            }
            @Override
            public void onImsDisconnected(ImsReasonInfo imsReasonInfo) {
                logger.debug("onImsDisconnected imsReasonInfo=" + imsReasonInfo);
            }
        };
        //register ims listener
        try {
            if (mImsManager != null) {
                mImsManager.addRegistrationListener(ImsServiceClass.MMTEL, mImsRegListener);
            } else {
                logger.debug("mImsManager is null");
            }
        } catch (ImsException e) {
            logger.debug("addRegistrationListener: " + e);
        }
    }

    @Override
    public void detachExtension() {

        logger.debug("detachExtension : " +extensionName);
        // TODO Auto-generated method stub
        super.detachExtension();

        mContext.unregisterReceiver(mNetworkChangedReceiver);
        TelephonyManager.getDefault().listen(mMobileNetworkClassChangedListener,
                PhoneStateListener.LISTEN_NONE);
        if (mImsRegListener != null && mImsManager != null) {
            try {
                mImsManager.removeRegistrationListener(mImsRegListener);
                mImsRegListener = null;
            } catch (ImsException e) {
                logger.debug("removeRegistrationListener: " + e);
            }
        }
    }

    public void handleNetworkChanged(Intent intent) {
        if (logger.isActivated()) {
            logger.debug("handleNetworkChanged:" + intent.getAction() + "("
                + mCurrentNetworkType + "/" + mCurrentMobileNetworkClass + ")");          
        }
        NetworkInfo info = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
        int type = info.getType();
        if (mCurrentNetworkType == type)
            return;
        if (type == ConnectivityManager.TYPE_MOBILE) {
            int networkClass = getDataNetworkClass();
            if (networkClass == TelephonyManager.NETWORK_CLASS_4_G) {
                // wifi to 4G
                return;
            } else {
                // wifi to 2/3G
                mIsActive = false;
                mIsDuplex = false;
                notifyListener();
            }
            mCurrentMobileNetworkClass = networkClass;
        } else if (type == ConnectivityManager.TYPE_WIFI) {
            if (mCurrentMobileNetworkClass == TelephonyManager.NETWORK_CLASS_2_G ||
                    mCurrentMobileNetworkClass == TelephonyManager.NETWORK_CLASS_3_G) {
                // 2/3G to Wifi
                mIsActive = true;
                mIsDuplex = true;
                notifyListener();
            } else {
                return;
            }
        }
        mCurrentNetworkType = type;
    }

    public void handleServiceStateChanged(ServiceState state) {
        if (mCurrentNetworkType == ConnectivityManager.TYPE_WIFI)
            return;
        int networkClass = TelephonyManager.getNetworkClass(state.getDataNetworkType());
        if (networkClass == mCurrentMobileNetworkClass)
            return;
        if (networkClass == TelephonyManager.NETWORK_CLASS_4_G && (
                mCurrentMobileNetworkClass == TelephonyManager.NETWORK_CLASS_2_G ||
                mCurrentMobileNetworkClass == TelephonyManager.NETWORK_CLASS_3_G)) {
            mIsActive = true;
            mIsDuplex = true;
            notifyListener();
        } else if ((networkClass == TelephonyManager.NETWORK_CLASS_2_G ||
                networkClass == TelephonyManager.NETWORK_CLASS_3_G) &&
                mCurrentMobileNetworkClass == TelephonyManager.NETWORK_CLASS_4_G) {
            mIsActive = false;
            mIsDuplex = false;
            notifyListener();
        }
        mCurrentMobileNetworkClass = networkClass;
    }

    private boolean isViLTENetworkSupported() {
        if (mCurrentNetworkType == ConnectivityManager.TYPE_WIFI)
            return true;
        if (mCurrentNetworkType == ConnectivityManager.TYPE_MOBILE) {
            if (mCurrentMobileNetworkClass == TelephonyManager.NETWORK_CLASS_4_G)
                return true;
        }
        return false;
    }

    public boolean isActive()
    {
        return mIsActive;
    }

    public boolean isDuplex()
    {
        return mIsDuplex;
    }


    @Override
    public void notifyListener() {
        // TODO Auto-generated method stub
        super.notifyListener();
    }

    private int getNetworkType() {
        ConnectivityManager connectivityMgr =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityMgr.getActiveNetworkInfo();

        if((networkInfo !=null) && (networkInfo.isConnected())){
            return networkInfo.getType();
        }else{
            return ConnectivityManager.TYPE_NONE;
        }
    }

    private int getDataNetworkClass() {
        ServiceState state = TelephonyManager.getDefault().
                getServiceStateForSubscriber(SubscriptionManager.getDefaultDataSubscriptionId());
        return TelephonyManager.getNetworkClass(state.getDataNetworkType());
    }

    private static int getMainCapabilityPhoneId() {
    int phoneId = SystemProperties.getInt(MtkPhoneConstants.PROPERTY_CAPABILITY_SWITCH, 1) - 1;
        if (phoneId < 0 || phoneId >= TelephonyManager.getDefault().getPhoneCount()) {
            phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        }
    return phoneId;
    }
}
