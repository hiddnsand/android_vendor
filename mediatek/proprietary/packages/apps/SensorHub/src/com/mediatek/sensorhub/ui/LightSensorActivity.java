package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class LightSensorActivity extends CustomerSensorBaseActivity {

    public LightSensorActivity() {
        super(Utils.KEY_LIGHT_STATUS, Sensor.TYPE_LIGHT);
    }
}
