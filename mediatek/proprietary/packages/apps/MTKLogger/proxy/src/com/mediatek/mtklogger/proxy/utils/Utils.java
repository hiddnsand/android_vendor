package com.mediatek.mtklogger.proxy.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author MTK81255
 *
 */
public class Utils {
    public static final String TAG = "MTKLoggerProxy";

    public static final String CONFIG_FILE_NAME = "mtklogger_proxy_config";
    public static final String CONFIG_KEY_MONITOR_FILE = "monitor_file";
    public static final String CONFIG_KEY_COPY_TO_FILE = "copy_to_file";

    /**
     * Communicate with other application broadcast commands.
     */
    public static final String PROXY_ACTION_RECEIVER = "com.mediatek.mtklogger.proxy.COMMAND";
    public static final String PROXY_ACTION_RECEIVER_RETURN =
            "com.mediatek.mtklogger.proxy.COMMAND_RESULT";
    public static final String PROXY_EXTRA_CMD_OPERATE = "CMD_OPERATE";
    public static final String PROXY_EXTRA_CMD_TARGET = "CMD_TARGET";
    public static final String PROXY_EXTRA_CMD_VALUE = "CMD_VALUE";
    public static final String PROXY_EXTRA_CMD_RESULT = "CMD_RESULT";
    public static final String PROXY_CMD_OPERATE_MONITOR_FILE = "monitorFile";
    public static final String PROXY_CMD_OPERATE_COPY_FILE = "copyFile";
    public static final String PROXY_CMD_OPERATE_CONNECT_SOCKET = "connectSocket";
    // Send command to socket by SocketName.
    public static final String PROXY_CMD_OPERATE_SEND_COMMAND = "sendCommand";
    /**
     * All command operate should add to CMD_OPERATE_SET to
     *  easy filter out the not support commands.
     */
    public static final Set<String> CMD_OPERATE_SET = new HashSet<String>();
    static {
        CMD_OPERATE_SET.add(PROXY_CMD_OPERATE_MONITOR_FILE);
        CMD_OPERATE_SET.add(PROXY_CMD_OPERATE_COPY_FILE);
        CMD_OPERATE_SET.add(PROXY_CMD_OPERATE_CONNECT_SOCKET);
        CMD_OPERATE_SET.add(PROXY_CMD_OPERATE_SEND_COMMAND);
    }

    public static final String PROXY_RESULT_OK = "1";
    public static final String PROXY_RESULT_FAILED = "0";

    /**
     * Current supported log type.
     */
    public static final int LOG_TYPE_MOBILE = 0x1;
    public static final int LOG_TYPE_MODEM = 0x2;
    public static final int LOG_TYPE_NETWORK = 0x4;
    public static final int LOG_TYPE_MET = 0x8; // new feature
    public static final int LOG_TYPE_GPS = 0x10;
    public static final Set<Integer> LOG_TYPE_SET = new HashSet<Integer>();
    static {
        LOG_TYPE_SET.add(LOG_TYPE_MOBILE);
        LOG_TYPE_SET.add(LOG_TYPE_MODEM);
        LOG_TYPE_SET.add(LOG_TYPE_NETWORK);
        LOG_TYPE_SET.add(LOG_TYPE_GPS);
    }

    public static Context sApplicationContext;
    public static final String BUILD_TYPE = Build.TYPE;

    /**
     * @param sourceFilePath
     *            String
     * @param targetFilePath
     *            String
     * @return boolean
     */
    public static boolean doCopy(String sourceFilePath, String targetFilePath) {
        Utils.logi(TAG, "-->doCopy() from " + sourceFilePath + " to " + targetFilePath);
        File sourceFile = new File(sourceFilePath);
        if (null == sourceFile || !sourceFile.exists()) {
            Utils.logw(TAG, "The sourceFilePath = " + sourceFilePath
                    + " is not existes, do copy failed!");
            return false;
        }
        // Get all files and sub directories under the current directory
        File[] files = sourceFile.listFiles();
        if (null == files) {
            // Current file is not a directory
            String tagLogPath = sourceFile.getAbsolutePath();
            return copyFile(tagLogPath, targetFilePath);
        } else {
            // Current file is a directory
            File targetFile = new File(targetFilePath);
            if (!targetFile.exists()) {
                targetFile.mkdirs();
            }
            for (File subFile : files) {
                doCopy(subFile.getAbsolutePath(),
                        targetFilePath + File.separator + subFile.getName());
            }
        }
        return true;
    }

    private static final int COPY_BUFFER_SIZE = 1024;
    private static boolean copyFile(String sourceFilePath, String targetFilePath) {
        Utils.logi(TAG, "-->copyFile() from " + sourceFilePath + " to " + targetFilePath);
        File sourceFile = new File(sourceFilePath);
        if (!sourceFile.exists()) {
            Utils.logw(TAG, "The sourceFilePath = " + sourceFilePath
                    + " is not existes, do copy failed!");
            return false;
        }

        File targetFile = new File(targetFilePath);
        if (targetFile.exists()) {
            targetFile.delete();
        }

        try {
            File parentFile = targetFile.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();
            }
            targetFile.createNewFile();
            FileInputStream fis = new FileInputStream(sourceFile);
            FileOutputStream fos = new FileOutputStream(targetFile);
            byte[] temp = new byte[COPY_BUFFER_SIZE];
            int len;
            while ((len = fis.read(temp)) != -1) {
                fos.write(temp, 0, len);
            }
            fos.flush();
            fos.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * @param intent Intent
     */
    public static void sendBroadCast(Intent intent) {
        if (sApplicationContext != null) {
            sApplicationContext.sendBroadcast(intent);
        }
    }

    /**
     * @param tag String
     * @param msg String
     */
    public static void logv(String tag, String msg) {
        if (Utils.BUILD_TYPE.equals("eng")) {
            Log.v(tag, msg);
        }
    }

    /**
     * @param tag String
     * @param msg String
     */
    public static void logd(String tag, String msg) {
        if (Utils.BUILD_TYPE.equals("eng")) {
            Log.d(tag, msg);
        }
    }

    /**
     * @param tag String
     * @param msg String
     */
    public static void logi(String tag, String msg) {
        Log.i(tag, msg);
    }

    /**
     * @param tag String
     * @param msg String
     */
    public static void logw(String tag, String msg) {
        Log.w(tag, msg);
    }

    /**
     * @param tag String
     * @param msg String
     */
    public static void loge(String tag, String msg) {
        Log.e(tag, msg);
    }

    /**
     * @param tag String
     * @param msg String
     * @param tr Throwable
     */
    public static void loge(String tag, String msg, Throwable tr) {
        Log.e(tag, msg, tr);
    }
}
