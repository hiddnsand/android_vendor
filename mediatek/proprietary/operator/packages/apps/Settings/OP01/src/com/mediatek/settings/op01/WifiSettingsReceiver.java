/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op01;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.provider.Settings;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.telephony.MtkTelephonyManagerEx;
import com.mediatek.provider.MtkSettingsExt;

import java.util.List;

/**
 * For CMCC AP afer change SIM.
 */
public class WifiSettingsReceiver extends BroadcastReceiver {

    private final static String TAG = "WifiSettingsReceiver";
    static final String CMCC_SSID = "CMCC";
    static final String CMCC_AUTO_SSID = "CMCC-AUTO";
    private static final String PREF_REMIND = "pref_remind";
    private static final String PREF_REMIND_HOTSPOT = "pref_remind_hotspot";
    private static final String PREF_REMIND_CONNECT = "pref_remind_connect";
    private static final String CRYPT_KEEPER = "com.android.settings.CryptKeeper";

    private static final int SIM_CARD_1 = PhoneConstants.SIM_ID_1;
    private static final int SIM_CARD_2 = PhoneConstants.SIM_ID_2;
    private static final int SIM_CARD_UNDEFINED = -1;
    // constant for current sim mode
    private static final int ALL_RADIO_ON = 3;
    private static final int INVALID_NETWORK_ID = -1;


    private static final int BUFFER_LENGTH = 40;
    private static final int MNC_SUB_BEG = 3;
    private static final int MNC_SUB_END = 5;
    private static final int MCC_SUB_BEG = 0;
    //remind user if connect to access point
    private static final int WIFI_CONNECT_REMINDER_ALWAYS = 0;

    /*to mark if the tcard is insert, set to true only the time SD inserted*/
    private WifiManager mWifiManager;
    private TelephonyManager mTm;
    private MtkTelephonyManagerEx mTelephonyManagerEx;
    private static boolean sHasDisconnect = true;

    private int mSimId;
    private int mAutoConnect;
    private String mNumeric;

    @Override
    public void onReceive(Context context, Intent intent) {

        String stateExtra;
        boolean mIsSIMExist = false;
        boolean isCmccCard = false;
        boolean isGemini = isGeminiSupport();

        String action = intent.getAction();

        if (Intent.ACTION_AIRPLANE_MODE_CHANGED.equals(action)) {
            SharedPreferences sh =
                context.getSharedPreferences("flight_mode_notify", context.MODE_PRIVATE);
            boolean isEnable = sh.getBoolean(PREF_REMIND, true);
            Log.i(TAG, "flgiht isEnable = " + isEnable);
            if (isEnable) {
                boolean state = Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
                if (state) {
                    ActivityManager amgr =
                        (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                    ComponentName cpname = null;
                    String classname = null;
                    List<RunningTaskInfo> runningTasks = amgr.getRunningTasks(1);
                    if (runningTasks != null) {
                        if (runningTasks.size() == 0) {
                            Log.i(TAG, "task size:0");
                            return;
                        } else if (runningTasks.get(0) != null) {
                            cpname = runningTasks.get(0).topActivity;
                        }
                    }
                    if (cpname != null) {
                        classname = cpname.getClassName();
                        Log.i(TAG, "ClassName:" + classname);
                        if (classname != null && classname.equals(CRYPT_KEEPER)) {
                            Log.i(TAG, "CryptKeeper screen");
                            return;
                        }
                    }

                    int myUser = UserHandle.myUserId();
                    int currentUser = ActivityManager.getCurrentUser();
                    Log.d(TAG, "Current user: " + currentUser + " My user: " + myUser);
                    if (myUser != currentUser) {
                        return;
                    }
                    Intent start = new Intent("com.mediatek.OP01.WIFI_FLIGHT_MODE_NOTIFY");
                    start.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(start);
                }
            }
            return;
        }

        if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(action)) {
            NetworkInfo netInfo =
                (NetworkInfo) intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            NetworkInfo.DetailedState dState = netInfo.getDetailedState();

            if (dState == NetworkInfo.DetailedState.DISCONNECTED) {
                sHasDisconnect = true;
                Log.i(TAG, "wifi disconnected state");
            } else if (sHasDisconnect
                        && dState == NetworkInfo.DetailedState.CONNECTED) {
                sHasDisconnect = false;
                WifiInfo wifiInfo =
                    (WifiInfo) intent.getParcelableExtra(WifiManager.EXTRA_WIFI_INFO);
                Log.i(TAG, "wifi connected state");
                if (wifiInfo != null) {
                    Log.i(TAG, "wifi info");
                    int networkId = wifiInfo.getNetworkId();
                    if (networkId == INVALID_NETWORK_ID) {
                        Log.i(TAG, "can't get networkId");
                        return;
                    }

                    String ssid = removeDoubleQuotes(wifiInfo.getSSID());
                    if (CMCC_AUTO_SSID.equals(ssid) || CMCC_SSID.equals(ssid)) {
                        WifiManager wifiManager =
                            (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                        final List<WifiConfiguration> configs
                            = wifiManager.getConfiguredNetworks();
                        boolean isCMCC = false;
                        if (configs != null) {
                            for (WifiConfiguration config : configs) {
                                if (config != null
                                    && networkId == config.networkId
                                    && (config.allowedKeyManagement.get(KeyMgmt.WPA_EAP)
                                       || config.allowedKeyManagement.get(KeyMgmt.IEEE8021X))) {
                                    isCMCC = true;
                                    break;
                                }
                            }
                        }

                        if (!isCMCC) {
                            return;
                        }

                        SharedPreferences sh = context.getSharedPreferences(
                                "wifi_connect_notify",
                                context.MODE_PRIVATE);
                        boolean isEnable = sh.getBoolean(PREF_REMIND_CONNECT, true);
                        int value = Settings.System.getInt(context.getContentResolver(),
                                MtkSettingsExt.System.WIFI_CONNECT_REMINDER,
                                WIFI_CONNECT_REMINDER_ALWAYS);
                        Log.i(TAG, "wifi connect remind = " + isEnable + value);
                        if (isEnable && value == WIFI_CONNECT_REMINDER_ALWAYS) {
                            Intent start = new Intent(context, WifiConnectNotifyDialog.class);
                            start.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(start);
                        }
                    }
                }
            }
            return;
        }

        mTm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        mTelephonyManagerEx = MtkTelephonyManagerEx.getDefault();

        if (WifiManager.WIFI_AP_STATE_CHANGED_ACTION.equals(action)) {
            int state = intent.getIntExtra(
                    WifiManager.EXTRA_WIFI_AP_STATE, WifiManager.WIFI_AP_STATE_FAILED);
            Log.i(TAG, "state = " + state);
            if (state == WifiManager.WIFI_AP_STATE_ENABLED) {
                boolean isDataOn = mTm.getDataEnabled();
                Log.i(TAG, "isDataOn = " + isDataOn);

                if (!isDataOn) {
                    SharedPreferences sh = context.getSharedPreferences(
                        "wifi_hotspot_enabled_notify", context.MODE_PRIVATE);
                    boolean isEnable = sh.getBoolean(PREF_REMIND_HOTSPOT, true);
                    Log.i(TAG, "wifi hotspot remind = " + isEnable);
                    if (isEnable) {
                        Intent start = new Intent(context, WifiHotspotNotifyDialog.class);
                        start.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(start);
                    }
                }
            }
            return;
        }

        mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        Log.i(TAG, "onReceive() action = " + action);

        mAutoConnect = Settings.System.getInt(context.getContentResolver(),
                MtkSettingsExt.System.WIFI_CONNECT_TYPE,
                MtkSettingsExt.System.WIFI_CONNECT_TYPE_AUTO);
        final int wifiState = mWifiManager.getWifiState();
        Log.i(TAG, "onReceive() wifiState = " + wifiState);
    }

    /**
     * get gemini support status.
     */
    private boolean isGeminiSupport() {
        TelephonyManager.MultiSimVariants config
            = TelephonyManager.getDefault().getMultiSimConfiguration();
        if (config == TelephonyManager.MultiSimVariants.DSDS
            || config == TelephonyManager.MultiSimVariants.DSDA) {
            return true;
        }
        return false;
    }

    /**
      * M: make NAI.
      * @param imsi eapMethod
      * @return the string of NAI
      */
    private static String makeNAI(String imsi, String eapMethod) {
        // airplane mode & select wrong sim slot
        if (imsi == null) {
              return addQuote("error");
        }

        StringBuffer stringNAI = new StringBuffer(BUFFER_LENGTH);
        // s = sb.append("a = ").append(a).append("!").toString();
        System.out.println("".length());

        if (eapMethod.equals("SIM")) {
              stringNAI.append("1");
        } else if (eapMethod.equals("AKA")) {
              stringNAI.append("0");
        }

        // add imsi
        stringNAI.append(imsi);
        stringNAI.append("@wlan.mnc");
        // add mnc
        stringNAI.append("0");
        stringNAI.append(imsi.substring(MNC_SUB_BEG, MNC_SUB_END));
        stringNAI.append(".mcc");
        // add mcc
        stringNAI.append(imsi.substring(MCC_SUB_BEG, MNC_SUB_BEG));

        // stringNAI.append(imsi.substring(5));
        stringNAI.append(".3gppnetwork.org");
        Log.i(TAG, stringNAI.toString());
        Log.i(TAG, "\"" + stringNAI.toString() + "\"");
        return addQuote(stringNAI.toString());
    }

    /**
      * M: add quote for strings.
      * @param string
      * @return add quote to the string
      */
    private static String addQuote(String s) {
        return "\"" + s + "\"";
    }

    private String removeDoubleQuotes(String string) {
        if (string != null) {
            int length = string.length();
            if ((length > 1) && (string.charAt(0) == '"')
                && (string.charAt(length - 1) == '"')) {
                return string.substring(1, length - 1);
            }
        }
        return string;
    }

    private boolean checkCmccCard(int simId) {
        boolean isCmccCard = false;
        int subId = getSubIdBySlot(simId);
        mNumeric = mTm.getSimOperator(subId);
        isCmccCard = mNumeric.equals("46000") || mNumeric.equals("46002")
            || mNumeric.equals("46004") || mNumeric.equals("46007")
            || mNumeric.equals("46008");
        return isCmccCard;
    }

    private void updateCmccConfig(int simslot, WifiConfiguration config,
            WifiManager mWifiManager) {
        String simtype = null;
        simtype = getIccCardType(simslot);
        Log.i(TAG, " updateCmccConfig() simslot = " + simslot);

        if (simtype != null) {
            if (simslot == SIM_CARD_1) {
                config.simSlot = addQuote("0");
            } else if (simslot == SIM_CARD_2) {
                config.simSlot = addQuote("1");
            }
            updateConfig(config);
        }
    }

    private void reConnectCmccAuto() {
        Log.i(TAG, "reConnectCmccAuto()");
        if (mAutoConnect != MtkSettingsExt.System.WIFI_CONNECT_TYPE_AUTO) {
            Log.i(TAG, "reConnectCmccAuto() auto connnect is off");
            return;
        }

        List<WifiConfiguration> configs = mWifiManager.getConfiguredNetworks();
        if (configs == null) {
            return;
        }
        int totalSize = configs.size();
        WifiConfiguration highPriorutyAp = null;
        if (totalSize > 0) {
            highPriorutyAp = configs.get(0);
        }

        for (int i = 1; i < totalSize; i++) {
            WifiConfiguration curAp = configs.get(i);
            if (highPriorutyAp == null) {
                 highPriorutyAp = curAp;
            } else if (curAp == null) {
                 continue;
            } else if (curAp.priority > highPriorutyAp.priority) {
                highPriorutyAp = curAp;
            }
        }

        if (highPriorutyAp != null) {
            Log.i(TAG, "config.ssid=" + highPriorutyAp.SSID
                        + ",priority=" + highPriorutyAp.priority);
            String highSSID = (highPriorutyAp.SSID == null ? ""
                    : removeDoubleQuotes(highPriorutyAp.SSID));
            if (CMCC_SSID.equals(highSSID) && highPriorutyAp.networkId != -1) {
                mWifiManager.connect(highPriorutyAp.networkId, null);
                Log.i(TAG, "reConnectCmccAuto() done");
            }
        }
    }

    private int getSubIdBySlot(int slot) {
        int [] subId = SubscriptionManager.getSubId(slot);
        Log.i(TAG, "getSubIdBySlot, simId " + slot +
                "subId " + ((subId != null) ? subId[0] : "invalid!"));
        return (subId != null) ? subId[0] : SubscriptionManager.getDefaultSubscriptionId();
    }

    private String getIccCardType(int slot) {
        String simtype = null;
        int subId = getSubIdBySlot(slot);
        simtype = mTelephonyManagerEx.getIccCardType(subId);
        return simtype;
    }

    private String getSubscriberId(int slot) {
        String subscriber = null;
        int subId = getSubIdBySlot(slot);
        subscriber = mTm.getSubscriberId(subId);
        return subscriber;
    }

    private void updateConfig(WifiConfiguration config) {
        Log.i(TAG, "updateConfig()");
        if (config == null) {
            return;
        }
        config.priority = -1;
        config.enterpriseConfig.setAnonymousIdentity("");
        mWifiManager.updateNetwork(config);

        reConnectCmccAuto();
    }

}
