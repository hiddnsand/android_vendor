MTK_PTHREAD_DEBUG := yes

LOCAL_PATH:= $(call my-dir)

# ========================================================
# libudf.so
# ========================================================
include $(CLEAR_VARS)

LOCAL_CFLAGS := \
    -DCORKSCREW_HAVE_ARCH \
    -DSELF_UNWIND_NO_GCC \
    -Werror -fvisibility=hidden

ifneq ($(strip $(TARGET_PLATFORM_VERSION)),OPM1)
LOCAL_CFLAGS += \
    -DO0
endif

ifneq ($(TARGET_IS_64_BIT),true)
LOCAL_SRC_FILES := \
    unwind/backtrace.c \
    unwind/backtrace-helper.c \
    unwind/map_info.c \
    unwind/ptrace.c \
    unwind/arch-arm/backtrace-arm.c
else
LOCAL_SRC_FILES_arm := \
    unwind/backtrace.c \
    unwind/backtrace-helper.c \
    unwind/map_info.c \
    unwind/ptrace.c \
    unwind/arch-arm/backtrace-arm.c
endif

ifneq ($(wildcard vendor/mediatek/internal/libudf_enable),)
ifeq ($(TARGET_BUILD_VARIANT),eng)
LOCAL_CFLAGS += \
    -DMALLOC_DEBUG_MTK \
    -D_MTK_ENG_
endif

ifeq ($(TARGET_BUILD_VARIANT),eng)
ifeq ($(MTK_USE_RESERVED_EXT_MEM),yes)
LOCAL_CFLAGS += \
    -DMTK_USE_RESERVED_EXT_MEM
endif
endif

ifeq ($(TARGET_BUILD_VARIANT),eng)
ifeq ($(MTK_GMO_RAM_OPTIMIZE),yes)
LOCAL_CFLAGS += \
    -DDEBUG15_GMO_RAM_OPTIMIZE
endif
endif
endif

LOCAL_CFLAGS += -DDEBUG15_GUARD_CHECK

# disable malloc.c warning
LOCAL_CFLAGS += -Wno-strict-aliasing

ifneq ($(TARGET_IS_64_BIT),true)
LOCAL_CFLAGS += \
    -fno-omit-frame-pointer
else
LOCAL_CFLAGS_arm += \
    -fno-omit-frame-pointer
endif

LOCAL_CFLAGS += -fvisibility=hidden

ifdef ENABLE_DEBUG15
LOCAL_SRC_FILES += \
        malloc_debug/malloc_debug_mtk.c \
        malloc_debug/recorder.c \
        malloc_debug/back_trace.c \
        malloc_debug/sig_handler.c \
        malloc_debug/config.c
else
LOCAL_SRC_FILES += \
        malloc_debug/recorder.c
endif

ifeq ($(TARGET_BUILD_VARIANT),eng)
ifneq ($(wildcard vendor/mediatek/internal/libudf_enable),)
LOCAL_CFLAGS += \
    -D_MTK_ENG_
endif
endif

ifneq ($(TARGET_IS_64_BIT),true)
LOCAL_CFLAGS += \
    -fno-omit-frame-pointer
else
LOCAL_CFLAGS_arm += \
    -fno-omit-frame-pointer
endif

ifneq ($(wildcard vendor/mediatek/internal/libudf_enable),)
ifeq ($(TARGET_BUILD_VARIANT),eng)
ifeq ($(MTK_USE_RESERVED_EXT_MEM),yes)
LOCAL_CFLAGS += \
    -DMTK_USE_RESERVED_EXT_MEM
endif
endif
endif

LOCAL_CFLAGS += -fvisibility=hidden

LOCAL_C_INCLUDES := vendor/mediatek/proprietary/external/udf

#
# Native backtrace record common code, mainly for backtrace data base create
# and backtrace get
#
LOCAL_SRC_FILES += \
	ubrd_core/recorder.c \
	ubrd_core/backtrace.cpp \
	ubrd_core/sighandler.c \
	ubrd_core/dump.c

# mmap/munmap debug, you can reference it for hook system call
LOCAL_SRC_FILES += \
    mmap_debug/mmap_debug.c

# fdleak debug
LOCAL_SRC_FILES += \
    fdleak_debug/fdleak_debug.cpp

# debug15 with libc_malloc_debug_mtk

# pthread_debug
ifeq ($(TARGET_BUILD_VARIANT),eng)
ifeq ($(MTK_PTHREAD_DEBUG),yes)

LOCAL_CFLAGS += -D_MTK_PTHREAD_DEBUG_

LOCAL_SRC_FILES += pthread_debug/pthread_debug.cpp

endif
endif

LOCAL_MODULE := libudf
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_SHARED_LIBRARIES := liblog libdl libc libbacktrace

LOCAL_C_INCLUDES += $(MTK_ROOT)/external/aee/binary/inc

LOCAL_C_INCLUDES += bionic/libc \
                    bionic/libc/bionic
ifneq ($(strip $(TARGET_PLATFORM_VERSION)),OPM1)
LOCAL_STATIC_LIBRARIES += libc_logging
else
LOCAL_STATIC_LIBRARIES += libasync_safe
endif

# Don't prelink
LOCAL_MODULE_TAGS := optional

LOCAL_ARM_MODE := arm
include $(MTK_SHARED_LIBRARY)

# ========================================================
# native test tool:
# 1. UT tool for libudf.so
# 2. only in eng load
# 3. needn't set "LOCAL_PATH := $(my-dir)" again
# ========================================================
include $(CLEAR_VARS)

ifneq ($(TARGET_IS_64_BIT),true)
LOCAL_CFLAGS += \
    -fno-omit-frame-pointer
else
LOCAL_CFLAGS_arm += \
    -fno-omit-frame-pointer
endif

ifdef BOARD_VNDK_VERSION
LOCAL_CFLAGS += -DWORKAROUND_MALLOC_ITERATE
endif

LOCAL_SRC_FILES := \
    ubrd_test/ubrd_test.cpp

LOCAL_C_INCLUDES := bionic/libc \
                    vendor/mediatek/proprietary/external/udf

LOCAL_SHARED_LIBRARIES := libc libdl liblog

LOCAL_MODULE := ubrd_utest
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
ifeq ($(TARGET_IS_64_BIT),true)
LOCAL_MODULE_STEM_32 := ubrd_utest
LOCAL_MODULE_STEM_64 := ubrd_utest64
LOCAL_MULTILIB := both
endif

LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := eng
LOCAL_ARM_MODE := arm
include $(MTK_EXECUTABLE)

# ========================================================
# debug15.conf
# ========================================================
ifneq ($(wildcard vendor/mediatek/internal/libudf_enable),)
ifeq ($(TARGET_BUILD_VARIANT),eng)
ifeq ($(MTK_USE_RESERVED_EXT_MEM),yes)
include $(CLEAR_VARS)
LOCAL_MODULE := debug15.conf
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
ifneq ($(TARGET_IS_64_BIT),true)
  LOCAL_SRC_FILES := malloc_debug/debug15.conf
else
  LOCAL_SRC_FILES := malloc_debug/debug15_64bit.conf
endif
LOCAL_MODULE_TAGS := eng
LOCAL_MODULE_CLASS := ETC
include $(BUILD_PREBUILT)
endif
endif
endif

# ========================================================
# wrapper for specific java process debug
# ========================================================
include $(CLEAR_VARS)
LOCAL_MODULE := udfwrapper
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
ifeq ($(TARGET_IS_64_BIT),true)
LOCAL_MODULE_STEM_32 := udfwrapper
LOCAL_MODULE_STEM_64 := udfwrapper64
LOCAL_MULTILIB := both
LOCAL_CFLAGS_arm += \
    -DUDF_64BIT_PLATFORM_32BIT_PROC
endif
LOCAL_MODULE_TAGS := eng
LOCAL_C_INCLUDES := bionic
LOCAL_SRC_FILES := udf_wrapper/udfwrapper.cc
LOCAL_CPP_EXTENSION := .cc
LOCAL_SHARED_LIBRARIES += libc liblog
#include external/libcxx/libcxx.mk
include $(MTK_EXECUTABLE)

# ========================================================
# mtm test tool:
# ========================================================
include $(CLEAR_VARS)

LOCAL_SRC_FILES := mtm_test/multi_threads_malloc_test.c
LOCAL_STATIC_LIBRARIES :=
LOCAL_SHARED_LIBRARIES := liblog
LOCAL_C_INCLUDES :=

LOCAL_MODULE := mtmtest
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
ifeq ($(TARGET_IS_64_BIT),true)
LOCAL_MODULE_STEM_32 := mtm_utest
LOCAL_MODULE_STEM_64 := mtm_utest64
LOCAL_MULTILIB := both
endif

LOCAL_MODULE_TAGS := eng
include $(MTK_EXECUTABLE)
