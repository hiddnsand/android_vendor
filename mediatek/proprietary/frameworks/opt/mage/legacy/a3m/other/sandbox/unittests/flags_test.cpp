/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <cstdio> /* for printf */
#include <set> /* for std::set */

#include <a3m/flags.h> /* for FlagMask and FlagSet */
#include <a3m/scenenode.h> /* for SceneNode */
#include <a3m/scenenodevisitor.h> /* for SceneNodeVisitor */
#include <a3m/sceneutility.h> /* for visitScene() */

#define test_assert(expr) { if (!(expr)) printf("Test failed at line %d: " \
                          #expr"\n", __LINE__); }

struct SceneNodeCollector : public a3m::SceneNodeVisitor
{
  void visit(a3m::SceneNode *node)
  {
    nodes.insert(node);
  }

  std::set<a3m::SceneNode*> nodes;
};

int main(void)
{
  a3m::FlagMask const FLAG0_TRUE(0, A3M_TRUE);
  a3m::FlagMask const FLAG1_FALSE(1, A3M_FALSE);
  a3m::FlagMask const FLAG2_FALSE(2, A3M_FALSE);
  a3m::FlagMask const FLAG3_TRUE(3, A3M_TRUE);

  // Test initial state
  {
    a3m::FlagSet f;
    test_assert(f.get(FLAG0_TRUE));
    test_assert(!f.get(FLAG1_FALSE));

    test_assert(!f.get(FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!f.get(FLAG1_FALSE | FLAG0_TRUE));
    test_assert(!f.get(~FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!f.get(FLAG1_FALSE | ~FLAG0_TRUE));
    test_assert(f.get(FLAG0_TRUE | ~FLAG1_FALSE));
    test_assert(f.get(~FLAG1_FALSE | FLAG0_TRUE));
    test_assert(!f.get(~FLAG0_TRUE | ~FLAG1_FALSE));
    test_assert(!f.get(~FLAG1_FALSE | ~FLAG0_TRUE));
  }

  // Set TRUE flag to TRUE
  {
    a3m::FlagSet f;
    f.set(FLAG0_TRUE, A3M_TRUE);
    test_assert(f.get(FLAG0_TRUE));
    test_assert(!f.get(FLAG1_FALSE));

    test_assert(!f.get(FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!f.get(FLAG1_FALSE | FLAG0_TRUE));
    test_assert(!f.get(~FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!f.get(FLAG1_FALSE | ~FLAG0_TRUE));
    test_assert(f.get(FLAG0_TRUE | ~FLAG1_FALSE));
    test_assert(f.get(~FLAG1_FALSE | FLAG0_TRUE));
    test_assert(!f.get(~FLAG0_TRUE | ~FLAG1_FALSE));
    test_assert(!f.get(~FLAG1_FALSE | ~FLAG0_TRUE));
  }

  // Set FALSE flag to FALSE
  {
    a3m::FlagSet f;
    f.set(FLAG1_FALSE, A3M_FALSE);
    test_assert(f.get(FLAG0_TRUE));
    test_assert(!f.get(FLAG1_FALSE));

    test_assert(!f.get(FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!f.get(FLAG1_FALSE | FLAG0_TRUE));
    test_assert(!f.get(~FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!f.get(FLAG1_FALSE | ~FLAG0_TRUE));
    test_assert(f.get(FLAG0_TRUE | ~FLAG1_FALSE));
    test_assert(f.get(~FLAG1_FALSE | FLAG0_TRUE));
    test_assert(!f.get(~FLAG0_TRUE | ~FLAG1_FALSE));
    test_assert(!f.get(~FLAG1_FALSE | ~FLAG0_TRUE));
  }

  // Set TRUE flag to FALSE
  {
    a3m::FlagSet f;
    f.set(FLAG0_TRUE, A3M_FALSE);
    test_assert(!f.get(FLAG0_TRUE));
    test_assert(!f.get(FLAG1_FALSE));

    test_assert(!f.get(FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!f.get(FLAG1_FALSE | FLAG0_TRUE));
    test_assert(!f.get(~FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!f.get(FLAG1_FALSE | ~FLAG0_TRUE));
    test_assert(!f.get(FLAG0_TRUE | ~FLAG1_FALSE));
    test_assert(!f.get(~FLAG1_FALSE | FLAG0_TRUE));
    test_assert(f.get(~FLAG0_TRUE | ~FLAG1_FALSE));
    test_assert(f.get(~FLAG1_FALSE | ~FLAG0_TRUE));
  }

  // Set FALSE flag to TRUE
  {
    a3m::FlagSet f;
    f.set(FLAG1_FALSE, A3M_TRUE);
    test_assert(f.get(FLAG0_TRUE));
    test_assert(f.get(FLAG1_FALSE));

    test_assert(f.get(FLAG0_TRUE | FLAG1_FALSE));
    test_assert(f.get(FLAG1_FALSE | FLAG0_TRUE));
    test_assert(!f.get(~FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!f.get(FLAG1_FALSE | ~FLAG0_TRUE));
    test_assert(!f.get(FLAG0_TRUE | ~FLAG1_FALSE));
    test_assert(!f.get(~FLAG1_FALSE | FLAG0_TRUE));
    test_assert(!f.get(~FLAG0_TRUE | ~FLAG1_FALSE));
    test_assert(!f.get(~FLAG1_FALSE | ~FLAG0_TRUE));
  }

  // Set TRUE flag to FALSE and FALSE flag to TRUE
  {
    a3m::FlagSet f;
    f.set(FLAG0_TRUE, A3M_FALSE);
    f.set(FLAG1_FALSE, A3M_TRUE);
    test_assert(!f.get(FLAG0_TRUE));
    test_assert(f.get(FLAG1_FALSE));

    test_assert(!f.get(FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!f.get(FLAG1_FALSE | FLAG0_TRUE));
    test_assert(f.get(~FLAG0_TRUE | FLAG1_FALSE));
    test_assert(f.get(FLAG1_FALSE | ~FLAG0_TRUE));
    test_assert(!f.get(FLAG0_TRUE | ~FLAG1_FALSE));
    test_assert(!f.get(~FLAG1_FALSE | FLAG0_TRUE));
    test_assert(!f.get(~FLAG0_TRUE | ~FLAG1_FALSE));
    test_assert(!f.get(~FLAG1_FALSE | ~FLAG0_TRUE));
  }

  // Basic flag derivation
  {
    a3m::SceneNode::Ptr a(new a3m::SceneNode());
    a3m::SceneNode::Ptr b(new a3m::SceneNode());
    a3m::SceneNode::Ptr c(new a3m::SceneNode());
    c->setParent(b);
    b->setParent(a);

    test_assert(a->getDerivedFlag(FLAG0_TRUE));
    test_assert(!a->getDerivedFlag(FLAG1_FALSE));
    test_assert(b->getDerivedFlag(FLAG0_TRUE));
    test_assert(!b->getDerivedFlag(FLAG1_FALSE));
    test_assert(c->getDerivedFlag(FLAG0_TRUE));
    test_assert(!c->getDerivedFlag(FLAG1_FALSE));

    c->setFlag(FLAG0_TRUE, A3M_FALSE);

    test_assert(a->getDerivedFlag(FLAG0_TRUE));
    test_assert(!a->getDerivedFlag(FLAG1_FALSE));
    test_assert(b->getDerivedFlag(FLAG0_TRUE));
    test_assert(!b->getDerivedFlag(FLAG1_FALSE));
    test_assert(!c->getDerivedFlag(FLAG0_TRUE));
    test_assert(!c->getDerivedFlag(FLAG1_FALSE));

    b->setFlag(FLAG0_TRUE, A3M_FALSE);

    test_assert(a->getDerivedFlag(FLAG0_TRUE));
    test_assert(!a->getDerivedFlag(FLAG1_FALSE));
    test_assert(!b->getDerivedFlag(FLAG0_TRUE));
    test_assert(!b->getDerivedFlag(FLAG1_FALSE));
    test_assert(!c->getDerivedFlag(FLAG0_TRUE));
    test_assert(!c->getDerivedFlag(FLAG1_FALSE));

    c->setFlag(FLAG0_TRUE, A3M_TRUE);

    test_assert(a->getDerivedFlag(FLAG0_TRUE));
    test_assert(!a->getDerivedFlag(FLAG1_FALSE));
    test_assert(!b->getDerivedFlag(FLAG0_TRUE));
    test_assert(!b->getDerivedFlag(FLAG1_FALSE));
    test_assert(!c->getDerivedFlag(FLAG0_TRUE));
    test_assert(!c->getDerivedFlag(FLAG1_FALSE));

    a->setFlag(FLAG0_TRUE, A3M_FALSE);

    test_assert(!a->getDerivedFlag(FLAG0_TRUE));
    test_assert(!a->getDerivedFlag(FLAG1_FALSE));
    test_assert(!b->getDerivedFlag(FLAG0_TRUE));
    test_assert(!b->getDerivedFlag(FLAG1_FALSE));
    test_assert(!c->getDerivedFlag(FLAG0_TRUE));
    test_assert(!c->getDerivedFlag(FLAG1_FALSE));

    b->setFlag(FLAG0_TRUE, A3M_TRUE);

    test_assert(!a->getDerivedFlag(FLAG0_TRUE));
    test_assert(!a->getDerivedFlag(FLAG1_FALSE));
    test_assert(!b->getDerivedFlag(FLAG0_TRUE));
    test_assert(!b->getDerivedFlag(FLAG1_FALSE));
    test_assert(!c->getDerivedFlag(FLAG0_TRUE));
    test_assert(!c->getDerivedFlag(FLAG1_FALSE));

    a->setFlag(FLAG0_TRUE, A3M_TRUE);

    test_assert(a->getDerivedFlag(FLAG0_TRUE));
    test_assert(!a->getDerivedFlag(FLAG1_FALSE));
    test_assert(b->getDerivedFlag(FLAG0_TRUE));
    test_assert(!b->getDerivedFlag(FLAG1_FALSE));
    test_assert(c->getDerivedFlag(FLAG0_TRUE));
    test_assert(!c->getDerivedFlag(FLAG1_FALSE));
  }

  // Basic flag derivation
  {
    a3m::SceneNode::Ptr a(new a3m::SceneNode());
    a3m::SceneNode::Ptr b(new a3m::SceneNode());
    a3m::SceneNode::Ptr c(new a3m::SceneNode());
    a3m::SceneNode::Ptr d(new a3m::SceneNode());
    a3m::SceneNode::Ptr e(new a3m::SceneNode());
    a3m::SceneNode::Ptr f(new a3m::SceneNode());
    f->setParent(e);
    e->setParent(d);
    d->setParent(c);
    c->setParent(b);
    b->setParent(a);

    c->setFlag(FLAG1_FALSE, A3M_TRUE);
    e->setFlag(FLAG0_TRUE, A3M_FALSE);

    a->setFlag(FLAG2_FALSE, A3M_FALSE);
    b->setFlag(FLAG2_FALSE, A3M_TRUE);
    c->setFlag(FLAG2_FALSE, A3M_TRUE);
    d->setFlag(FLAG2_FALSE, A3M_TRUE);
    e->setFlag(FLAG2_FALSE, A3M_FALSE);
    f->setFlag(FLAG2_FALSE, A3M_FALSE);

    a->setFlag(FLAG3_TRUE, A3M_TRUE);
    b->setFlag(FLAG3_TRUE, A3M_TRUE);
    c->setFlag(FLAG3_TRUE, A3M_FALSE);
    d->setFlag(FLAG3_TRUE, A3M_TRUE);
    e->setFlag(FLAG3_TRUE, A3M_TRUE);
    f->setFlag(FLAG3_TRUE, A3M_FALSE);

    test_assert(!a->getDerivedFlag(FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!b->getDerivedFlag(FLAG0_TRUE | FLAG1_FALSE));
    test_assert(c->getDerivedFlag(FLAG0_TRUE | FLAG1_FALSE));
    test_assert(d->getDerivedFlag(FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!e->getDerivedFlag(FLAG0_TRUE | FLAG1_FALSE));
    test_assert(!f->getDerivedFlag(FLAG0_TRUE | FLAG1_FALSE));

    {
      SceneNodeCollector collector;
      visitScene(collector, *a, FLAG0_TRUE | FLAG1_FALSE, FLAG0_TRUE | FLAG1_FALSE);
      test_assert(collector.nodes.size() == 2);
      test_assert(collector.nodes.find(c.get()) != collector.nodes.end());
      test_assert(collector.nodes.find(d.get()) != collector.nodes.end());
    }

    {
      SceneNodeCollector collector;
      visitScene(collector, *a, FLAG0_TRUE, FLAG0_TRUE);
      test_assert(collector.nodes.size() == 4);
      test_assert(collector.nodes.find(a.get()) != collector.nodes.end());
      test_assert(collector.nodes.find(b.get()) != collector.nodes.end());
      test_assert(collector.nodes.find(c.get()) != collector.nodes.end());
      test_assert(collector.nodes.find(d.get()) != collector.nodes.end());
    }
  }

  return 0;
}

