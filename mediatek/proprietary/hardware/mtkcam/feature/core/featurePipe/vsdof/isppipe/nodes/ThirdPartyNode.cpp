/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

// Standard C header file

// Android system/core header file

// mtkcam custom header file
#include <camera_custom_stereo.h>
// mtkcam global header file
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
// Module header file
// Local header file
#include "ThirdPartyNode.h"
// Logging
#undef PIPE_CLASS_TAG
#undef PIPE_MODULE_TAG
#define PIPE_MODULE_TAG "IspPipe"
#define PIPE_CLASS_TAG "TPNode"
#include <featurePipe/core/include/PipeLog.h>

/*******************************************************************************
* Namespace start.
********************************************************************************/
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace DualCamThirdParty {

using namespace VSDOF::util;
/*******************************************************************************
* Const Definition
********************************************************************************/

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  P2ANode Instantiation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ThirdPartyNode::
ThirdPartyNode(
    const char *name,
    IspPipeNodeID nodeID,
    const PipeNodeConfigs& config
)
: IspPipeNode(name, nodeID, config)
{
    MY_LOGD("ctor +");
    this->addWaitQueue(&mRequestQue);
    MY_LOGD("ctor -");
}

ThirdPartyNode::
~ThirdPartyNode()
{
    MY_LOGD("+");
    MY_LOGD("-");
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  P2ANode Functions
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

MBOOL
ThirdPartyNode::
onInit()
{
    MY_LOGD("+");
    MY_LOGD("-");
    return MTRUE;
}

MBOOL
ThirdPartyNode::
onUninit()
{
    MY_LOGD("+");
    MY_LOGD("-");
    return MTRUE;
}

MBOOL
ThirdPartyNode::
onThreadStart()
{
    // used for tmp flow
    mpDpStream = new DpBlitStream();
    // Put Third-party SW initialization
    return MTRUE;
}

MBOOL
ThirdPartyNode::
onThreadStop()
{
    // used for tmp flow
    delete mpDpStream;
    // Put third-party SW un-initilization
    return MTRUE;
}

MBOOL
ThirdPartyNode::
onData(DataID dataID, const IspPipeRequestPtr& pRequest)
{
    MBOOL ret = MTRUE;
    PIPE_LOGD("+ : reqID=%d", pRequest->getRequestNo());
    switch(dataID)
    {
        case P2A_TO_TP_YUV_DATA:
            mRequestQue.enque(pRequest);
            break;
        default:
            MY_LOGW("Un-recognized dataID ID, id=%s reqID=%d", ID2Name(dataID), pRequest->getRequestNo());
            ret = MFALSE;
            break;
    }

  PIPE_LOGD("-");
  return ret;
}

MVOID
ThirdPartyNode::
onFlush()
{
    MY_LOGD("+");
    IspPipeRequestPtr pRequest;
    while( mRequestQue.deque(pRequest) )
    {
        sp<PipeBufferHandler> pBufferHandler = pRequest->getBufferHandler();
        pBufferHandler->onProcessDone(getNodeId());
    }
    IspPipeNode::onFlush();
    MY_LOGD("-");
}

MBOOL
ThirdPartyNode::
onThreadLoop()
{
    IspPipeRequestPtr pRequest;

    if( !waitAllQueue() )
    {
        return MFALSE;
    }
    if( !mRequestQue.deque(pRequest) )
    {
        MY_LOGE("mRequestQue.deque() failed");
        return MFALSE;
    }
    // mark on-going-request start
    this->incExtThreadDependency();
    AutoProfileLogging profile("TPNode::threadLoop", pRequest->getRequestNo());
    // timer
    pRequest->mTimer.startTP();
    MBOOL bRet = MFALSE;
    if(bRet = executeThirdParty(pRequest))
    {
        pRequest->mTimer.stopTP();
        profile.logging("third-party exec-time=%d ms", pRequest->mTimer.getElapsedTP());
    }
    else
    {
        MY_LOGE("reqID=%d Third-party ALGO execute failed!", pRequest->getRequestNo());
        this->handleData(ERROR_OCCUR_NOTIFY, pRequest);
    }
    // launch onProcessDone
    pRequest->getBufferHandler()->onProcessDone(getNodeId());
    // mark on-going-request end
    this->decExtThreadDependency();
    return bRet;
}

MBOOL
ThirdPartyNode::
executeThirdParty(
    const IspPipeRequestPtr& pRequest
)
{
    AutoProfileLogging profile("TPNode::executeThirdParty", pRequest->getRequestNo());
    if(pRequest->getRequestAttr().reqType == PIPE_REQ_PREVIEW)
    {
        if(onHandlePreviewRequest(pRequest))
        {
            this->handleDataAndDump(TP_TO_MDP_PVYUV, pRequest);
            return MTRUE;
        }
    }
    else
    {
        // not handle PIPE_REQ_CAPTURE/PIPE_REQ_META type currently
        pRequest->setOutputBufferReady(BID_META_OUT_APP);
        pRequest->setOutputBufferReady(BID_META_OUT_HAL);
        this->handleDataAndDump(TP_OUT_DEPTH_BOKEH, pRequest);
        return MTRUE;
    }
    return MFALSE;
}

MBOOL
ThirdPartyNode::
onHandlePreviewRequest(
    const IspPipeRequestPtr& pRequest
)
{
    AutoProfileLogging profile("TPNode::onHandlePreviewRequest", pRequest->getRequestNo());
    sp<PipeBufferHandler> pBufferHandler = pRequest->getBufferHandler();

    // input Main1 PV YUV (preview size)
    IImageBuffer *pImgBuf_PVYUVMain1 = nullptr;
    // input synced Main1 YUV (preview size)
    IImageBuffer *pImgBuf_SyncedMain1 = nullptr;
    // input synced Main2 YUV (preview size)
    IImageBuffer *pImgBuf_SyncedMain2 = nullptr;
    // preview output YUV0
    IImageBuffer *pImgBuf_OutYUV = nullptr;

    MBOOL bRet = MTRUE;
    // get input buffer
    bRet &= pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_YUV_MAIN1, pImgBuf_SyncedMain1);
    bRet &= pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_YUV_MAIN2, pImgBuf_SyncedMain2);
    if(!bRet)
    {
        MY_LOGE("reqID=%d Failed to get input buffers", pRequest->getRequestNo());
        return MFALSE;
    }
    // get output buffer of preview, could be anyone of BID_PIPE_OUT_PV_YUV0/BID_PIPE_OUT_PV_YUV1/BID_PIPE_OUT_PV_FD
    IspPipeBufferID pvOutBufferID = (pRequest->isRequestBuffer(BID_PIPE_OUT_PV_YUV0)) ? BID_PIPE_OUT_PV_YUV0 :
                                    (pRequest->isRequestBuffer(BID_PIPE_OUT_PV_YUV1)) ? BID_PIPE_OUT_PV_YUV1 : BID_PIPE_OUT_PV_FD;
    PIPE_LOGD("Target output preview buffer id =%d", pvOutBufferID);
    pImgBuf_OutYUV = pBufferHandler->requestBuffer(getNodeId(), pvOutBufferID);

    // TODO: execute the third-party SW for preview frame

    // Mark buffer ready
    pRequest->setOutputBufferReady(pvOutBufferID);
    // config out meta ready
    pRequest->setOutputBufferReady(BID_META_OUT_APP);
    pRequest->setOutputBufferReady(BID_META_OUT_HAL);
    // Notes: the left preview buffers will be filled in MDP nodes
    return MTRUE;
}


}; // DualCamThirdParty
}; // NSFeaturePipe
}; // NSCamFeature
}; // NSCam

