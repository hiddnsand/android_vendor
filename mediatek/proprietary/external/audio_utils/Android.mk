LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    pulse.c \
    AudioDetectPulse.cpp

LOCAL_C_INCLUDES := \
    $(call include-path-for, audio-utils) \
    $(MTK_PATH_CUSTOM)/cgen/inc \
    $(MTK_PATH_CUSTOM)/cgen/cfgfileinc \
    $(MTK_PATH_CUSTOM)/cgen/cfgdefault



LOCAL_SHARED_LIBRARIES := \
    libcutils \
    liblog \
    libutils \
    libaudioutils


ifneq ($(filter $(TARGET_BUILD_VARIANT),eng userdebug),)
    LOCAL_CFLAGS += -DMTK_LATENCY_DETECT_PULSE
endif

LOCAL_MODULE := libmtkaudio_utils_vendor
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_MODULE_TAGS := optional

LOCAL_MULTILIB := both

include $(MTK_SHARED_LIBRARY)



###################################################################


include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    pulse.c \
    AudioDetectPulse.cpp

LOCAL_C_INCLUDES := \
    $(call include-path-for, audio-utils) \
    $(MTK_PATH_CUSTOM)/cgen/inc \
    $(MTK_PATH_CUSTOM)/cgen/cfgfileinc \
    $(MTK_PATH_CUSTOM)/cgen/cfgdefault



LOCAL_SHARED_LIBRARIES := \
    libcutils \
    liblog \
    libutils \
    libaudioutils


ifneq ($(filter $(TARGET_BUILD_VARIANT),eng userdebug),)
    LOCAL_CFLAGS += -DMTK_LATENCY_DETECT_PULSE
endif

LOCAL_MODULE := libmtkaudio_utils
LOCAL_MODULE_OWNER := mtk

LOCAL_MODULE_TAGS := optional

LOCAL_MULTILIB := both

include $(MTK_SHARED_LIBRARY)


