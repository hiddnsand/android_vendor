package com.mediatek.engineermode.iotconfig;

import java.util.ArrayList;
import java.util.List;

import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.epdgconfig.epdgConfigGeneralFragment;
import com.mediatek.engineermode.epdgconfig.epdgConfigTimerFragment;
import com.mediatek.engineermode.iotconfig.ApnConfigFragment;

import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.Fragment;

public class IotConfigurationActivity extends FragmentActivity {
    private static String TAG = "EM/IotConfigurationActivity";
    private static int[] TAB_TITLE_IOT_WFC = { R.string.iot_apn, R.string.iot_xcap,
            R.string.iot_wfc, R.string.iot_vilte };
    private static int[] TAB_TITLE_IOT_NO_WFC = { R.string.iot_apn,
            R.string.iot_xcap, R.string.iot_vilte };
    private static int[] TAB_TITLE_IOT;
    private ViewPager mViewPager;
    private PagerTitleStrip pagerTitleStrip;
    private List<Fragment> mFragments;
    private List<String> titleList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.iot_config);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        pagerTitleStrip = (PagerTitleStrip) findViewById(R.id.pagertitle);
        pagerTitleStrip.setTextSpacing(100);
        titleList = new ArrayList<String>();
        TAB_TITLE_IOT = FeatureSupport.isSupportWfc() ? TAB_TITLE_IOT_WFC : TAB_TITLE_IOT_NO_WFC;
        for (int i = 0; i < TAB_TITLE_IOT.length; i++) {
            titleList.add(getString(TAB_TITLE_IOT[i]).toString());
        }
        mFragments = new ArrayList<Fragment>();
        mFragments.add(new ApnConfigFragment());
        mFragments.add(new XCAPConfigFragment());
        if (FeatureSupport.isSupportWfc()) {
            mFragments.add(new WfcConfigFragment());
        }
        mFragments.add(new VilteConfigFragment());
        FragPagerAdapter adapter = new FragPagerAdapter(
                getSupportFragmentManager(), mFragments, titleList);
        mViewPager.setAdapter(adapter);
    }

    public class FragPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> mFragments;
        private List<String> mTitle;

        public FragPagerAdapter(FragmentManager fm, List<Fragment> mFragments,
                List<String> title) {
            super(fm);
            // TODO Auto-generated constructor stub
            this.mFragments = mFragments;
            this.mTitle = title;
        }

        @Override
        public Fragment getItem(int arg0) {
            // TODO Auto-generated method stub
            return mFragments.get(arg0);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // TODO Auto-generated method stub
            return getString(TAB_TITLE_IOT[position]).toString();
        }
    }

}
