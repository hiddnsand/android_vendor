package com.mediatek.flp.em;

import android.app.TabActivity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.Manifest;
import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;
import android.widget.Toast;
import java.util.Arrays;

@SuppressWarnings("deprecation")
public class FlpTabActivity extends TabActivity {
    final static int PERMISSION_REQUEST_BITS_ALL = 0xF;
    final static String [] sNeededPermissions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
//            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    TabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab);
    }

    private void addAllTab() {
        if (mTabHost != null) {
            log("Tabs are already added");
            return;
        }

        mTabHost = getTabHost();

        mTabHost.addTab(mTabHost.newTabSpec("Geofence")
            .setIndicator("Geofence")
            .setContent(new Intent().setClass(this, FlpGeofence.class)));

        mTabHost.addTab(mTabHost.newTabSpec("Map").setIndicator("Map")
            .setContent(new Intent().setClass(this, FlpMap.class)));

        mTabHost.addTab(mTabHost
            .newTabSpec("Heading")
            .setIndicator("Heading")
            .setContent(new Intent().setClass(this, FlpHeading.class)));

        for (int i = 0; i < 4; i++) {
            mTabHost.setCurrentTab(i);
        }

        mTabHost.setCurrentTab(0);
    }

     @Override
     protected void onResume() {
         super.onResume();
         if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                 && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED
//                 && checkSelfPermission(Manifest.permission.READ_PHONE_STATE)== PackageManager.PERMISSION_GRANTED
                 && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {
             if (mTabHost == null) {
                 addAllTab();
             }
         } else {
             int grantResultBits = checkAndRequestPermission(
                      PERMISSION_REQUEST_BITS_ALL);
             log("onResume: checkAndRequestPermission");
         }
     }



 @Override
 protected void onDestroy() {
  super.onDestroy();
 }


    @SuppressWarnings(value = { "unused" })
    private void log(String msg) {
        Log.d("FlpEM", msg);
    }

    private int checkAndRequestPermission(int reqBitmap) {
        int grantResultBits = 0;
        reqBitmap &= PERMISSION_REQUEST_BITS_ALL;
        if (reqBitmap == 0) return grantResultBits;

        int total = Integer.bitCount(reqBitmap);
        String [] perms = new String [total];

        int i, j;
        for (i = j = 0; i < sNeededPermissions.length; i++) {
            if (((1 << i) & reqBitmap) != 0) {
                if (checkSelfPermission(sNeededPermissions[i])
                        != PackageManager.PERMISSION_GRANTED) {
                    perms[j++] = sNeededPermissions[i];
                   // Should we show an explanation?
                   if (shouldShowRequestPermissionRationale(
                           sNeededPermissions[i])) {
                       // Explain to the user why we need to read the contacts
                       log("We need " + sNeededPermissions[i] + " to work");
                   }
                } else {
                    reqBitmap &= ~(1 << i);
                    grantResultBits |= (1 << i);
                }
            }
        }

        if (j < total) {
            total = j;
            if (total == 0) return grantResultBits;
            perms = Arrays.copyOf(perms, total);
        }

        requestPermissions(perms, reqBitmap);

        return grantResultBits;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
            String permissions[], int[] grantResults) {
                log("onRequestPermissionsResult: reqCode=" + requestCode);
                for (int i = 0; i < permissions.length; i++) {
                    log("onRequestPermissionsResult: perm[" + i + "]=" + permissions[i] + ", result=" + grantResults[i]);
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED
                              //  && checkSelfPermission(Manifest.permission.READ_PHONE_STATE)== PackageManager.PERMISSION_GRANTED
                                && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {
                            addAllTab();
                        // permission was granted, yay! do the
                        // calendar task you need to do.
                        }
                    } else if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) == false
                                && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) == false
                               // && shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE) == false
                                && shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) == false) {
                            Toast.makeText(this, "You've disabled the permission, please allow permission from Settings -> Apps -> Menu -> Show system -> LocationEM2 -> Permissions to enable all permissions",
                                Toast.LENGTH_SHORT).show();
                        }
                        finish();
                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
                    }
                }
                return ;
    }


}
