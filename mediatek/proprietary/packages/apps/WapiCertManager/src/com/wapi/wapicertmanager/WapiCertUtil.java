/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 */

package com.wapi.wapicertmanager;

import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

class WapiCertUtil {
    private static final String TAG = "WapiCertUtil";

    // shared preference name
    public static final String CERT_INFO_PREFS_NAME = "CertInfoCfg";

    private static final String CERT_BEGIN = "-----BEGIN CERTIFICATE-----";
    private static final String CERT_END = "-----END CERTIFICATE-----";
    private static final String PRIKEY_BEGIN = "-----BEGIN EC PRIVATE KEY-----";
    private static final String PRIKEY_END = "-----END EC PRIVATE KEY-----";

    static byte[] getCertElement(String strCertData) {
        if (strCertData == null) {
            return null;
        }
        int nBegin = strCertData.indexOf(CERT_BEGIN);
        int nEnd = strCertData.indexOf(CERT_END) + CERT_END.length();
        String strElement = strCertData.substring(nBegin, nEnd);
        return strElement.getBytes();
    }

    static byte[] getPriKeyElement(String strCertData) {
        if (strCertData == null) {
            return null;
        }
        int nBegin = strCertData.indexOf(PRIKEY_BEGIN) + PRIKEY_BEGIN.length()
                + 1;
        int nEnd = strCertData.indexOf(PRIKEY_END) - 1;
        String strElement = strCertData.substring(nBegin, nEnd);
        return strElement.getBytes();
    }

    static byte[] addCertHeader(String strCertElement) {
        String strCertData = CERT_BEGIN + "\n" + strCertElement + "\n"
                + CERT_END;
        return strCertData.getBytes();
    }

    static byte[] readFile(File file) {
        try {
            byte[] data = new byte[(int) file.length()];
            FileInputStream inputStream = new FileInputStream(file);
            inputStream.read(data);
            inputStream.close();
            return data;
        } catch (Exception e) {
            Log.w(TAG, "Read file error: " + e);
            return null;
        }
    }

    static int getCertificateType(File cert) {

        if (cert == null) {
            return 0;
        }

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(cert);
            byte[] data = new byte[inputStream.available()];
            inputStream.read(data);
            String certContent = new String(data);

            int indexCertBegin = 0;
            int indexCertEnd = 0;
            int indexPriKeyBegin = 0;
            int indexPriKeyEnd = 0;

            indexCertBegin = certContent.indexOf(CERT_BEGIN);
            indexCertEnd = certContent.indexOf(CERT_END);
            indexPriKeyBegin = certContent.indexOf(PRIKEY_BEGIN);
            indexPriKeyEnd = certContent.indexOf(PRIKEY_END);

            if (indexCertBegin >= 0 && indexCertEnd > 0) {
                if (indexPriKeyBegin > 0 && indexPriKeyEnd > 0) {
                    Log.d(TAG, "user cert file");
                    return 1;
                } else if (indexPriKeyBegin <= 0 && indexPriKeyEnd <= 0) {
                    Log.d(TAG, "ca cert file");
                    return 2;
                } else {
                    Log.d(TAG, "other cert file 1");
                    return 0;
                }
            } else {
                Log.d(TAG, "other cert file 2");
                return 0;
            }
        } catch (Exception e) {
            Log.e(TAG, "read file exception: " + e.toString());
            return -1;
        } finally {
            try {
                inputStream.close();
                Log.d(TAG, "local file stream close success.");
            } catch (Exception e) {
                Log.e(TAG, "local file stream close faild.");
                e.printStackTrace();
                return -1;
            }
        }
    }

    static boolean isTheSuffix(String file, String suffix) {
        if (file == null || file.length() < suffix.length()) {
            return false;
        }
        for (int i = 0; i < suffix.length(); i++) {
            if (file.charAt(file.length() - suffix.length() + i) != suffix
                    .charAt(i)) {
                return false;
            }
        }
        return true;
    }
}
