/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/*
 * PSS Timer API implementation for the Win32 platform
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <windows.h>    /* QueryPerformanceCounter */
#include <a3m/timer.h>  /* PSS Timer API */
#include <a3m/assert.h> /* A3M_ASSERT */

/*****************************************************************************
 * Constant
 *****************************************************************************/
#define PSS_MILLISECONDS_PER_SECOND   1000

/*****************************************************************************
 * Local Variables
 *****************************************************************************/
static A3M_INT64 s_startTicks = 0;
static A3M_FLOAT s_tickPeriod = 0.0;

/*****************************************************************************
 * Global Functions
 *****************************************************************************/
/**
 * PSS Timer initialisation routine
 */
A3M_BOOL pssTimerInit(void)
{
  LARGE_INTEGER queryResult;

  /* Only initialise the timer if it hasn't been initialised yet */
  if ((s_startTicks == 0) && (s_tickPeriod == 0.0))
  {
    /* Save the clock period for later use */
    if (QueryPerformanceFrequency(&queryResult))
    {
      s_tickPeriod = 1.0f / (static_cast<A3M_FLOAT> (queryResult.QuadPart));
    }
    else
    {
      /* The platform doesn't have a hi-res timer */
      return A3M_FALSE;
    }

    /* Save the start tick count to use as a reference */
    if (QueryPerformanceCounter(&queryResult))
    {
      s_startTicks = queryResult.QuadPart;
    }
    else
    {
      return A3M_FALSE;
    }
  }
  return A3M_TRUE;
}

/**
 * PSS Timer get elapsed tick count
 */
A3M_INT64 pssTimerGetTicks(void)
{
  LARGE_INTEGER currentTicks;

  A3M_ASSERT(s_startTicks);
  if (QueryPerformanceCounter(&currentTicks))
  {
    return (currentTicks.QuadPart - s_startTicks);
  }
  return 0;
}

/**
 * PSS Timer get elapsed time in milliseconds
 */
A3M_UINT32 pssTimerGetTimeMs(void)
{
  A3M_INT64 currentTicks;

  A3M_ASSERT(s_tickPeriod);
  currentTicks = pssTimerGetTicks();
  return (static_cast<A3M_UINT32>(currentTicks * s_tickPeriod *
                                  PSS_MILLISECONDS_PER_SECOND));
}

/**
 * PSS Timer get clock frequency
 */
A3M_UINT32 pssTimerGetFrequency(void)
{
  LARGE_INTEGER freq;

  if (QueryPerformanceFrequency(&freq))
  {
    return freq.LowPart;
  }
  return 0;
}
