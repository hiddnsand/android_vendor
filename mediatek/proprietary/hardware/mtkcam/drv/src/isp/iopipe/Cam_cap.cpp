/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "cam_capi"

#include <Cam/ICam_capibility.h>
#include <cam/cam_capibility.h>
#include <cutils/log.h>

#ifndef NULL
#define NULL 0
#endif

CAM_CAPIBILITY::CAM_CAPIBILITY()
{}
CAM_CAPIBILITY::~CAM_CAPIBILITY()
{}


static CAM_CAPIBILITY QueryObj;
CAM_CAPIBILITY* CAM_CAPIBILITY::CreateInsatnce(char const* szCallerName)
{
    if(szCallerName == NULL){
        ALOGE(LOG_TAG"[%s]ERR(%5d):no user name\n" , __FUNCTION__, __LINE__);
        return NULL;
    }


    return (CAM_CAPIBILITY*)&QueryObj;

}

void CAM_CAPIBILITY::DestroyInstance(char const* szCallerName)
{
    if(szCallerName == NULL){
        ALOGE(LOG_TAG"[%s]ERR(%5d):no user name\n" , __FUNCTION__, __LINE__);
        return;
    }
}


MBOOL CAM_CAPIBILITY::GetCapibility(MUINT32 portId,E_CAM_Query e_Op,CAM_Queryrst &QueryRst)
{
    capibility capObj;

    return capObj.GetCapibility(portId, e_Op, QueryRst);
}

MBOOL CAM_CAPIBILITY::GetCapibility(MUINT32 portId,NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd e_Op,
                                            NSCam::NSIoPipe::NSCamIOPipe::NormalPipe_InputInfo inputInfo,
                                            NSCam::NSIoPipe::NSCamIOPipe::NormalPipe_QueryInfo &QueryRst)
{
    MBOOL rst = MTRUE;
    tCAM_rst camrst;
    capibility* ptr = new capibility;

    rst = ptr->GetCapibility(portId, e_Op, inputInfo, camrst, E_CAM_UNKNOWNN);

    if(rst == MTRUE){
        if (e_Op & NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_BS_RATIO) {
            QueryRst.bs_ratio = camrst.ratio;
        }
        if (e_Op & NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_QUERY_FMT) {
            QueryRst.query_fmt = camrst.Queue_fmt;
        }
        if (e_Op & NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_PIPELINE_BITDEPTH) {
            QueryRst.pipelinebitdepth = camrst.pipelinebitdepth;
        }

        if (e_Op & (NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_X_PIX|NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_CROP_X_PIX)) {
            QueryRst.x_pix = camrst.x_pix;
        }
        if (e_Op & (NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_X_BYTE|NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_CROP_X_BYTE)) {
            QueryRst.xsize_byte = camrst.xsize_byte;
        }
        if (e_Op & NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_CROP_START_X) {
            QueryRst.crop_x = camrst.crop_x;
        }
        if (e_Op & NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_STRIDE_PIX) {
            QueryRst.stride_pix = camrst.stride_pix;
        }
        if (e_Op & NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_STRIDE_BYTE) {
            QueryRst.stride_byte = camrst.stride_byte;
        }
        if (e_Op & NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_D_Twin) {
            QueryRst.D_TWIN = camrst.D_TWIN;
        }
        if (e_Op & NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_UNI_NUM) {
            QueryRst.uni_num = camrst.uni_num;
        }

    }

    delete ptr;

    return rst;
}

