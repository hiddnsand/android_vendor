package com.mediatek.connectivity;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemProperties;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mediatek.connectivity.CdsEmbmsService.EmbmsBinder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
  * main class to handle eMBMS test activity.
  *
  */
public class CdsEmbmsInfoActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "CdsEmbms";

    private static final String  RP_EMBMS_PROPERTY_ENABLE = "persist.sys.embms.enable";

    private static final int SESSION_BUTTON_ID    = 0x1000;
    private static final int SESSION_EDITTEXT_ID  = 0x2000;
    private static final int SESSION_EDITTEXT_ID2 = 0x3000;
    private static final int SESSION_EDITTEXT_ID3 = 0x4000;
    private static final int SESSION_EDITTEXT_ID4 = 0x5000;

    private Context mContext;
    private CdsEmbmsService mService;
    private LinearLayout mSessionLayout;
    private SharedPreferences mDataStore;
    private SharedPreferences.Editor mDataEditor;

    private CdsEmbmsListAdapter mListAdapter;
    private ExpandableListView mExpListView;

    private List<String> mListDataHeader;
    HashMap<String, List<String>> mListDataChild;

    private boolean mBound;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cds_embms_info);

        mContext = this.getBaseContext();

        CheckBox status = (CheckBox) findViewById(R.id.embmsSrvStatus);
        status.setOnClickListener(this);

        status = (CheckBox) findViewById(R.id.embmsSrvAuto);
        status.setOnClickListener(this);

        status = (CheckBox) findViewById(R.id.embmsPriority);
        status.setOnClickListener(this);

        Button btn = (Button) findViewById(R.id.embmsSessionI);
        btn.setOnClickListener(this);

        btn = (Button) findViewById(R.id.embmsSessionIm);
        btn.setOnClickListener(this);

        btn = (Button) findViewById(R.id.embmsFunction);
        btn.setOnClickListener(this);

        btn = (Button) findViewById(R.id.embmsSessionF);
        btn.setOnClickListener(this);

        btn = (Button) findViewById(R.id.embmsSessionEm);
        btn.setOnClickListener(this);

        mDataStore = this.getSharedPreferences(CdsEmbmsConsts.EMBMS_FILE, 0);
        mDataEditor = mDataStore.edit();


        // Keep eMBMS service running
        Intent i = new Intent(mContext, CdsEmbmsService.class);
        i.putExtra(CdsEmbmsConsts.BOOT, false);
        mContext.startService(i);

        Log.i(TAG, "onCreate in CdsEmbmsInfoActivity");
    }

    @Override
    protected void onResume() {
        super.onResume();
        provisionDefaultValue();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, CdsEmbmsService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        mListDataHeader = new ArrayList<String>();
        mListDataChild = new HashMap<String, List<String>>();

        for (int i = 0; i < CdsEmbmsConsts.MAX_SESSION_NUM; i ++) {
            // Adding header data.
            String header = getString(R.string.embms) + "-" + (i + 1);
            mListDataHeader.add(header);

            // Adding child data.
            List<String> child = new ArrayList<String>();
            child.add(getString(R.string.tmgi) + ":");
            child.add(getString(R.string.sessionid) + ":");
            child.add(getString(R.string.sailist) + ":");
            child.add(getString(R.string.freqlist) + ":");
            mListDataChild.put(header, child);
        }
    }

    private void setCheckBoxByValue(int id, boolean v) {
        Log.i(TAG, "set checkbox:" + v);
        CheckBox box = (CheckBox) findViewById(id);
        box.setChecked(v);
    }

    private void provisionDefaultValue() {
        setCheckBoxByValue(R.id.embmsSrvAuto,
            mDataStore.getBoolean(CdsEmbmsConsts.EMBMS_AUTO_ENABLED, false));
        fillInfoBySrv();
    }

    private TextView getInfoText(String info) {
        TextView t = new TextView(this);
        t.setText(info);
        return t;
    }

    private void setEmbmsStatus(boolean isEnabled) {
        if (mService != null) {
            mService.enableEmbmsSrv(isEnabled);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        int id = v.getId();

        switch (id) {
            case R.id.embmsSrvStatus:
                boolean value = ((CheckBox) v).isChecked();
                Log.i(TAG, "status:" + value);
                setEmbmsStatus(value);
                break;
            case R.id.embmsSrvAuto:
                boolean autoValue = ((CheckBox) v).isChecked();
                Log.i(TAG, "auto:" + autoValue);
                mDataEditor.putBoolean(CdsEmbmsConsts.EMBMS_AUTO_ENABLED,
                                        autoValue);
                mDataEditor.commit();
                SystemProperties.set(RP_EMBMS_PROPERTY_ENABLE, (autoValue) ? "1" : "0");
                break;
            case R.id.embmsSessionI:
                intent = new Intent();
                intent.setClassName(this,
                            "com.mediatek.connectivity.CdsEmbmsSessionListActivity");
                this.startActivity(intent);
                break;
            case R.id.embmsSessionIm:
                sendInterestedSessions();
                break;
            case R.id.embmsFunction:
                enableEmbmsModemFunction();
                break;
            case R.id.embmsSessionF:
                enableEmbmsFt();
                break;
            case R.id.embmsSessionEm:
                intent = new Intent();
                intent.setClassName(this,
                            "com.mediatek.connectivity.CdsEmbmsEmActivity");
                this.startActivity(intent);
                break;
            case R.id.embmsPriority:
                value = ((CheckBox) v).isChecked();
                Log.i(TAG, "priority:" + value);
                mDataEditor.putBoolean(CdsEmbmsConsts.EMBMS_SESSION_PRIORITY, value);
                mDataEditor.commit();
                enablePriority(value);
                break;
            default:
                break;
        }
    }

    private void enablePriority(boolean isEnabled) {
        if (mService != null) {
            mService.enableEmbmsPriority(isEnabled);
        }
    }

    private void enableEmbmsFt() {
        if (mService != null) {
            String sessionList = mService.readCurrentSessionList();
            if (sessionList == null || sessionList.length() == 0) {
                String text = mContext.getString(R.string.error3);
                Toast toast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);
                toast.show();
                return;
            }
            Intent intent = new Intent();
            intent.setClassName(this,
                    "com.mediatek.connectivity.CdsEmbmsNwSessionListActivity");
            intent.putExtra("data", sessionList);
            Log.i(TAG, "enableEmbmsFt");
            this.startActivity(intent);
        }
    }

    private void sendInterestedSessions() {
        if (mService != null) {
            mService.sendInterestedEmbmsSession();
        }
    }

    private void enableEmbmsModemFunction() {
        if (mService != null) {
            mService.enableEmbmsFeature();
        }
    }

    private void fillInfoBySrv() {
        if (mService != null) {
            setCheckBoxByValue(R.id.embmsSrvStatus, mService.getEmbmsSrvStatus());
            setCheckBoxByValue(R.id.embmsPriority, mService.getEmbmsPriority());
        }
    }

    /** Defines callbacks for service binding, passed to bindService(). */
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            Log.i(TAG, "onServiceConnected");
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            EmbmsBinder binder = (EmbmsBinder) service;
            mService = binder.getService();
            fillInfoBySrv();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}