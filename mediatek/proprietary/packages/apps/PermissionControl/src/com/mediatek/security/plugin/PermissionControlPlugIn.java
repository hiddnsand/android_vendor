package com.mediatek.security.plugin;


import android.provider.SearchIndexableData;

import android.content.Context;
import android.content.Intent;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceGroup;
import android.util.Log;
import android.content.ContextWrapper;

import com.mediatek.security.R;
import com.mediatek.security.service.PermControlUtils;
import com.mediatek.cta.CtaManager;
import com.mediatek.cta.CtaManagerFactory;


import java.util.ArrayList;
import java.util.List;

public class PermissionControlPlugIn extends ContextWrapper {
    private static final String TAG = "PermControlPlugIn";

    private Preference mAutoBootPrf;
    private Preference mPermissionRequest;

    private Preference mCellularData;
    private CtaManager mCtaManager;
    private Context mContext;

    public PermissionControlPlugIn(Context context) {
        super(context);
        Log.d(TAG, "PermissionControlPlugIn");
        mContext = context;
        mContext.setTheme(com.android.internal.R.style.Theme_Material_Settings);
        mCtaManager = CtaManagerFactory.getInstance().makeCtaManager();
        mCellularData = new Preference(mContext);
        mCellularData.setTitle(R.string.cellular_data_control_title);
        mCellularData.setSummary(R.string.cellular_data_control_title);
        Intent intent4 = new Intent();
        intent4.setAction("com.mediatek.security.CELLULAR_DATA");
        intent4.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mCellularData.setIntent(intent4);
    }

    public void addPermSwitchPrf(PreferenceGroup prefGroup) {
        Log.d(TAG, "addPermSwitchPrf");
        if (mCtaManager.isCtaSupported() && prefGroup instanceof PreferenceGroup) {
            /// new permission request preference instance
            mPermissionRequest = new Preference(prefGroup.getPreferenceManager().getContext());
            mPermissionRequest.setTitle(mContext.getResources()
                                    .getString(R.string.trigger_history_pref_title));
            mPermissionRequest.setSummary(mContext.getResources()
                                    .getString(R.string.trigger_history_pref_summary));
            Intent intent3 = new Intent();
            intent3.setAction("com.mediatek.security.TRIGGER_HISTORY");
            intent3.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mPermissionRequest.setIntent(intent3);
            prefGroup.addPreference(mPermissionRequest);
        }
    }

    public void addAutoBootPrf(PreferenceGroup prefGroup) {
        Log.d(TAG, "addAutoBootPrf");
        if (mCtaManager.isCtaSupported() && prefGroup instanceof PreferenceGroup) {
            /// new auto boot preference instance
            mAutoBootPrf = new Preference(prefGroup.getPreferenceManager().getContext());
            mAutoBootPrf.setTitle(mContext.getResources().getString(R.string.auto_boot_pref_title));
            mAutoBootPrf.setSummary(mContext.getResources()
                                .getString(R.string.auto_boot_pref_summary));
            Intent intent2 = new Intent();
            intent2.setAction("com.mediatek.security.AUTO_BOOT");
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mAutoBootPrf.setIntent(intent2);
            prefGroup.addPreference(mAutoBootPrf);
        }
    }

    public void enablerResume() {

    }

    public void enablerPause() {

    }

    public List<SearchIndexableData> getRawDataToIndex(boolean enabled) {
        List<SearchIndexableData> indexables = new ArrayList<SearchIndexableData>();

        if (mCtaManager.isCtaSupported()) {
            Log.d(TAG, "loading search info");
            SearchIndexableRaw autoboot = new SearchIndexableRaw(mContext);
            autoboot.title = mContext.getString(R.string.auto_boot_pref_title);
            autoboot.intentAction = "com.mediatek.security.AUTO_BOOT";
            indexables.add(autoboot);

            SearchIndexableRaw permissionRequest = new SearchIndexableRaw(
                    mContext);
            permissionRequest.title = mContext
                    .getString(R.string.trigger_history_pref_title);
            permissionRequest.intentAction = "com.mediatek.security.TRIGGER_HISTORY";
            indexables.add(permissionRequest);

            SearchIndexableRaw cellularData = new SearchIndexableRaw(mContext);
            cellularData.title = mContext
                    .getString(R.string.cellular_data_control_title);
            cellularData.intentAction = "com.mediatek.security.CELLULAR_DATA";
            indexables.add(cellularData);
        }
        return indexables;
    }
}
