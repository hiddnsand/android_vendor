/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/** \file
 * Raycast results class
 */
#pragma once
#ifndef A3M_RAYCASTRESULT_H
#define A3M_RAYCASTRESULT_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/vector3.h>           /* for Vector3f                           */

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  /**
   * \ingroup  a3mCollision
   *
   * The result of a raycast detection calculation.
   *
   * @{
   */

  /**
   * Raycast result class.
   * Performing a raycast collision test may result in an intersection with the
   * shape under test.  If an intersection is detected, the intersection is
   * described by a distance from the ray position along the ray direction,
   * such that (intersectionpoint = rayposition + distance * raydirection),
   * where raydirection is normalized and distance is in world units, as well
   * as the normal of the surface of the shape at the point of intersection.
   */
  class RaycastResult
  {
  public:
    /**
     * Constructs a ray result for a failed intersection test.
     */
    RaycastResult() :
      m_intersected(A3M_FALSE),
      m_distance(0.0f)
    {
    }

    /**
     * Constructs a ray result for a successful intersection test.
     */
    RaycastResult(
      A3M_FLOAT distance,
      /**< Distance of the intersection point along the ray. */
      Vector3f const& normal
      /**< Normal of the surface of the shape. */) :
      m_intersected(A3M_TRUE),
      m_distance(distance),
      m_normal(normal)
    {
    }

    /**
     * Returns whether the intersection occurred or not.
     * \return Intersection success flag
     */
    A3M_BOOL getIntersected() const
    {
      return m_intersected;
    }

    /**
     * Returns the distance of the intersection along the ray.
     * \return Intersection distance
     */
    A3M_FLOAT getDistance() const
    {
      return m_distance;
    }

    /**
     * Returns the normal of the surface at the point of intersection.
     * \return Surface normal vector
     */
    Vector3f const& getNormal() const
    {
      return m_normal;
    }

  private:
    A3M_BOOL m_intersected; /**< Whether an intersection happened */
    A3M_FLOAT m_distance; /**< Distance of intersection along ray */
    Vector3f m_normal; /**< Normal of intersection */
  };

  /** @} */

} // namespace a3m

#endif // A3M_RAYCASTRESULT_H
