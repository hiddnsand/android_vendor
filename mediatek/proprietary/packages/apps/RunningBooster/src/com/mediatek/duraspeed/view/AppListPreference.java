/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
package com.mediatek.duraspeed.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import com.mediatek.duraspeed.R;
import com.mediatek.duraspeed.presenter.AppRecord;
import com.mediatek.duraspeed.presenter.BoosterContract;

/**
 * AppListPreference is the preference type. used to display white list manage
 * UI.
 */
public final class AppListPreference extends Preference implements
        View.OnClickListener {
    private static final String TAG = "AppListPreference";
    private Context mContext;
    private final String mEndStr = "/";
    private boolean mSwitchState;
    private Switch mSwitch;
    private AppRecord mAppRecord;
    private final SharedPreferences mSharedPrf;
    private static final String SHARED_PREFERENCES_NAME = "AppListPreference";

    private BoosterContract.IViewPresenter mIViewPresenter;

    public AppListPreference(Context context, AppRecord appRecord,
                             BoosterContract.IViewPresenter viewPresenter) {
        super(context);
        mContext = context;
        mIViewPresenter = viewPresenter;
        mAppRecord = appRecord;
        mSharedPrf = context.getSharedPreferences(SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE);
        setLayoutResource(R.layout.preference_trust_list);
        // add switch
        setWidgetLayoutResource(R.layout.preference_switch);
        onDeviceAttributesChanged();
        mSwitchState = mAppRecord.getStatus() == AppRecord.STATUS_ENABLED;
    }

    protected View onCreateView(ViewGroup parent) {
        View view = super.onCreateView(parent);
        mSwitch = (Switch) view.findViewById(R.id.status);
        return view;
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        mSwitch.setChecked(mSwitchState);
        view.setOnClickListener(this);
    }

    public boolean isChecked() {
        return mAppRecord.getStatus() == AppRecord.STATUS_ENABLED;
    }

    @Override
    public void onClick(View v) {
        mSwitchState = !mSwitch.isChecked();
        mSwitch.setChecked(mSwitchState);
        mAppRecord.setStatus(mSwitchState ? AppRecord.STATUS_ENABLED : AppRecord.STATUS_DISABLED);
        Log.d(TAG, "onClick()," + mAppRecord.getPkgName() + ", join in whilteList ? "
                + mSwitchState);
        mIViewPresenter.setAppInWhiteList(mAppRecord, mSwitchState ? AppRecord.STATUS_ENABLED :
                AppRecord.STATUS_DISABLED);
    }

    /**
     * update the attributes.
     */
    public void onDeviceAttributesChanged() {
        setTitle(mAppRecord.getLabel());
        if (mAppRecord.getIcon() == null) {
            mAppRecord.setIcon(ViewUtils.getAppDrawable(mContext, mAppRecord.getPkgName()));
        }
        setIcon(mAppRecord.getIcon());
    }
}
