/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.SystemProperties;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.internal.telephony.MtkIccCardConstants;
import com.mediatek.internal.telephony.MtkIccCardConstants.CardType;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.telephony.MtkTelephonyManagerEx;

/**
* Show sim alert dialog.
*/
public class SimDialogService extends Service {

    private static final String TAG = "SimDialogService";
    public static final String ACTION_START_SELF =
        "com.mediatek.intent.action.STARTSELF_SIM_DIALOG_SERVICE";

    public static final String EXTRA_NAME = "extra_name";
    public static final String TEXT = "text";

    public static final String ACTION_NAME = "com.mediatek.OP09.SIM_DIALOG_SERVICE";
    private static final String UIM_CHANGE_ALERT_ACTIVITY_NAME
        = "com.mediatek.OP09.UIM_CHANGE_ALERT";
    private boolean mSimtype = false;
    private boolean mUserpresent = false;
    private String mText = "";
    private String mSharePreferencesName = "SimDialogServiceSharePreferences";
    private SharedPreferences mSharedPreferences;
    private String mSimtypeKey = "SIMTYPEKEY";
    private String mUserpresentKey = "USERPRESENTKEY";
    private String mTextKey = "TEXTKEY";
    private Context mContext;
    private boolean mIsKeyguardOnScreen;
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        mContext = this.createDeviceProtectedStorageContext();
        mSharedPreferences = mContext.getSharedPreferences(mSharePreferencesName, Context.MODE_PRIVATE);
        getAllJudge();
        if (intent != null) {
            Intent receiverIntent = (Intent) intent.getParcelableExtra(EXTRA_NAME);
            String action = receiverIntent.getAction();
            Log.d(TAG, "receiverIntent action = " + action);
            if (Intent.ACTION_USER_PRESENT.equals(action)) {
                mSharedPreferences.edit().putBoolean(mUserpresentKey, true).commit();
                Log.d(TAG, "PRESENT mSimtype = " + mSimtype + "mUserpresent =" + mUserpresent);
                showAlert();
            } else if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(action)) {
                String cardState =
                    receiverIntent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
                int phoneId = receiverIntent.getIntExtra(PhoneConstants.PHONE_KEY, -1);
                Log.d(TAG, "card state is: " + cardState + ", PhoneId: " + phoneId);
                SubscriptionManager subManager = SubscriptionManager.from(this);
                int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(phoneId);
                int slotId = subManager.getSlotIndex(subId);
                Log.d(TAG, "slot id: " + slotId);
                if (phoneId != 0) {
                    return;
                }
                MtkTelephonyManagerEx telephonyManagerEx = MtkTelephonyManagerEx.getDefault();
                CardType cardType = telephonyManagerEx.getCdmaCardType(PhoneConstants.SIM_ID_1);
                Log.i(TAG, "cardType = " + cardType.toString());
                Log.d(TAG, "CARDTYPE mSimtype = " + mSimtype + "mUserpresent =" + mUserpresent);
                if (CardType.CARD_NOT_INSERTED.equals(cardType)
                    || CardType.UNKNOW_CARD.equals(cardType)) {
                    if (!cardState.equals(MtkIccCardConstants.INTENT_VALUE_ICC_ABSENT)) {
                        return;
                    }
                    String sim2Info = null;
                    sim2Info = SystemProperties.get("gsm.ril.fulluicctype.2");
                    Log.i(TAG, "sim2Info = +" + sim2Info + "+");
                    if ((sim2Info == null) || (sim2Info.equals(""))) {
                        Log.i(TAG, "not insert sim2");
                        mText = getString(R.string.no_sim_dialog_message);
                        mSharedPreferences.edit().putString(mTextKey, mText).commit();
                        mSharedPreferences.edit().putBoolean(mSimtypeKey, true).commit();
                    }

                } else if (!(CardType.PIN_LOCK_CARD).equals(cardType)
                        && !(CardType.CT_4G_UICC_CARD).equals(cardType)) {
                    if (!cardState.equals(MtkIccCardConstants.INTENT_VALUE_ICC_READY) &&
                            !cardState.equals(MtkIccCardConstants.INTENT_VALUE_ICC_ABSENT)) {
                        return;
                    }
                    mText = getString(R.string.lte_sim_dialog_message);
                    mSharedPreferences.edit().putString(mTextKey, mText).commit();
                    mSharedPreferences.edit().putBoolean(mSimtypeKey, true).commit();

                } else if ((CardType.CT_4G_UICC_CARD).equals(cardType)) {
                    mText = null;
                    mSharedPreferences.edit().putString(mTextKey, mText).commit();
                    mSharedPreferences.edit().putBoolean(mSimtypeKey, false).commit();
                }
                Log.d(TAG, "mText = " + mText);
                showAlert();
            } else if (action.equals(SimDialogService.ACTION_START_SELF)) {
                Log.d(TAG, "recv  ACTION_START_SELF");
                mSharedPreferences.edit().putBoolean(mSimtypeKey, false).commit();
            }
        }
    }

    private void getAllJudge() {
        mSimtype = mSharedPreferences.getBoolean(mSimtypeKey, false);
        mUserpresent = mSharedPreferences.getBoolean(mUserpresentKey, false);
        mText = mSharedPreferences.getString(mTextKey, "");
        KeyguardManager keyguardManager
            = (KeyguardManager) this.getSystemService(Context.KEYGUARD_SERVICE);
        mIsKeyguardOnScreen = keyguardManager.inKeyguardRestrictedInputMode();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void showAlert() {
        getAllJudge();
        Log.d(TAG, "showAlert mSimtype = " + mSimtype
            + " mUserpresent = " + mUserpresent
            + " mText = " + mText
            + " mIsKeyguardOnScreen = " + mIsKeyguardOnScreen);
        if (mText != null && mSimtype && !mIsKeyguardOnScreen) {
            Intent launchIntent = new Intent(UIM_CHANGE_ALERT_ACTIVITY_NAME);
            launchIntent.setPackage(getPackageName());
            launchIntent.putExtra(TEXT, mText);
            launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(launchIntent);
        }
    }

}
