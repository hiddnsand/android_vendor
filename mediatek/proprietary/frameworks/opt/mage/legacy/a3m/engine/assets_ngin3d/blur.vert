/**************************************************************************
 *
 * Copyright (c) 2013 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * A vertex shader for blur effect.
 *
 */

uniform vec2 u_m_sampleMult;

attribute vec4 a_position;    // Vertex position (model space)
attribute vec2 a_uv0;         // Texture coordinate


// Adjusted texture coordinates
varying vec2 v_texcoord0;
varying vec2 v_texcoord1;
varying vec2 v_texcoord2;
varying vec2 v_texcoord3;
varying vec2 v_texcoord4;

void main()
{
  gl_Position = vec4( 2.0, 2.0, 2.0, 1.0 ) * a_position;
  v_texcoord0 = a_uv0 - 2.0 * u_m_sampleMult;
  v_texcoord1 = a_uv0 - u_m_sampleMult;
  v_texcoord2 = a_uv0;
  v_texcoord3 = a_uv0 + u_m_sampleMult;
  v_texcoord4 = a_uv0 + 2.0 * u_m_sampleMult;
}
