LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

#This is the target being built.(Name of Jar)
LOCAL_MODULE := OP09CStk
LOCAL_SRC_FILES := $(call all-java-files-under, src)

# Put operator customization apk together to specific folder
# Specify install path for MTK CIP solution
ifeq ($(strip $(MTK_CIP_SUPPORT)),yes)
LOCAL_MODULE_PATH := $(TARGET_CUSTOM_OUT)/operator/libs
else
LOCAL_MODULE_PATH := $(TARGET_OUT)/operator/libs
endif

LOCAL_JAVA_LIBRARIES += com.mediatek.stk.ext \
                        telephony-common \
                        mediatek-telephony-common \

#Tell it to build a Jar
include $(BUILD_JAVA_LIBRARY)
