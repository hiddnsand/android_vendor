/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

 // Standard C header file

// Android system/core header file

// mtkcam custom header file

// mtkcam global header file
#include <mtkcam/drv/iopipe/PostProc/DpeUtility.h>
#include <stereo_tuning_provider.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
// Module header file
// Local header file
#include "WMFNode.h"
#include "../DepthMapPipe_Common.h"
#include "../DepthMapPipeUtils.h"
#include "../bufferPoolMgr/BaseBufferHandler.h"
// Logging header file
#define PIPE_CLASS_TAG "WMFNode"
#include <featurePipe/core/include/PipeLog.h>

using namespace NSCam::NSIoPipe::NSDpe;
using namespace StereoHAL;

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe_DepthMap {

WMFNode::
WMFNode(
    const char *name,
    DepthMapPipeNodeID nodeID,
    PipeNodeConfigs config
)
: DepthMapPipeNode(name, nodeID, config)
{
    this->addWaitQueue(&mJobQueue);
}

WMFNode::
~WMFNode()
{
    MY_LOGD("[Destructor]");
}

MVOID
WMFNode::
cleanUp()
{
    VSDOF_LOGD("+");
    if(mpDPEStream != NULL)
    {
        mpDPEStream->uninit();
        mpDPEStream->destroyInstance("VSDOF_WMF");
        mpDPEStream = NULL;
    }
    // JOB queue clear
    mJobQueue.clear();
    // release Tbli buffer pool
    mpTbliImgBuf = NULL;
    ImageBufferPool::destroy(mpTbliImgBufPool);

    VSDOF_LOGD("-");
}

MBOOL
WMFNode::
onInit()
{
    VSDOF_INIT_LOG("+");
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
WMFNode::
onUninit()
{
    VSDOF_INIT_LOG("+");
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
WMFNode::
onThreadStart()
{
    CAM_TRACE_NAME("WMFNode::onThreadStart");
    VSDOF_INIT_LOG("+");
    // init DPEStream
    mpDPEStream = NSCam::NSIoPipe::NSDpe::IDpeStream::createInstance("VSDOF_DPE");
    if(mpDPEStream == NULL)
    {
        MY_LOGE("DPE Stream create instance failed!");
        return MFALSE;
    }
    MBOOL bRet = mpDPEStream->init();
    if(!bRet)
    {
        MY_LOGE("DPEStream init failed!!");
        return MFALSE;
    }
    // Tbli buffer pool
    StereoSizeProvider * pSizeProvder = StereoSizeProvider::getInstance();
    MUINT32 iWMFInputWidth = pSizeProvder->getBufferSize(E_MY_S, eSTEREO_SCENARIO_RECORD).size.w;
    MUINT32 iTbliStride = DPEQueryInDMAStride(DMA_WMFE_TBLI, WMFE_DPI_D_FMT, iWMFInputWidth);
    mpTbliImgBufPool = ImageBufferPool::create("TbliBufPool", iTbliStride, 1,
                                                eImgFmt_Y8, ImageBufferPool::USAGE_HW, MTRUE);
    // allocate one buffer
    mpTbliImgBufPool->allocate(1);
    // get Tbli tuning buffer
    mpTbliImgBuf = mpTbliImgBufPool->request();
    // query tuning mgr to get Tbli + filter size
    StereoTuningProvider::getWMFTuningInfo(eWMFFilterSize, reinterpret_cast<void*>(mpTbliImgBuf->mImageBuffer->getBufVA(0)));
    mpTbliImgBuf->mImageBuffer->syncCache(eCACHECTRL_FLUSH);
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
WMFNode::
onThreadStop()
{
    CAM_TRACE_NAME("WMFNode::onThreadStop");
    VSDOF_INIT_LOG("+");
    cleanUp();
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
WMFNode::
onData(DataID data, DepthMapRequestPtr& pRequest)
{
    MBOOL ret=MTRUE;
    VSDOF_LOGD("+, dataID=%d reqId=%d", data, pRequest->getRequestNo());

    switch(data)
    {
        case OCC_TO_WMF_DMH_MY_S:
            VSDOF_PRFLOG("+ : reqID=%d size=%d", pRequest->getRequestNo(), mJobQueue.size());
            mJobQueue.enque(pRequest);
            break;
        default:
            ret = MFALSE;
            break;
    }

    VSDOF_LOGD("-");
    return ret;
}

MBOOL
WMFNode::
onThreadLoop()
{
    DepthMapRequestPtr pRequest;

    if( !waitAnyQueue() )
    {
        return MFALSE;
    }
    CAM_TRACE_NAME("WMFNode::onThreadLoop");

    if( !mJobQueue.deque(pRequest) )
    {
        MY_LOGD("mJobQueue.deque() failed");
        return MFALSE;
    }
    return threadLoop_WMFFirstPass(pRequest);
}

MVOID
WMFNode::
debugWMFParam(NSIoPipe::WMFEConfig &config)
{
    if(!DepthPipeLoggingSetup::mbDebugLog)
        return;

    #define LOG_BUFFER_INFO(buff_info)\
        MY_LOGD(#buff_info ".dmaport=%d", buff_info.dmaport);\
        MY_LOGD(#buff_info ".memID=%d", buff_info.memID);\
        MY_LOGD(#buff_info ".u4BufVA=%x", buff_info.u4BufVA);\
        MY_LOGD(#buff_info ".u4BufPA=%x", buff_info.u4BufPA);\
        MY_LOGD(#buff_info ".u4BufSize=%d", buff_info.u4BufSize);\
        MY_LOGD(#buff_info ".u4Stride=%d", buff_info.u4Stride);

    for (unsigned int j=0; j<config.mWMFECtrlVec.size(); j++)
    {
        WMFECtrl ctrl = config.mWMFECtrlVec.at(j);

        MY_LOGD("========== Wmfe_Ctrl_0 section==============");
        MY_LOGD("Wmfe_Enable=%d", ctrl.Wmfe_Enable);
        MY_LOGD("WmfeFilterSize=%d", ctrl.WmfeFilterSize);
        MY_LOGD("Wmfe_Width=%d", ctrl.Wmfe_Width);
        MY_LOGD("Wmfe_Height=%d", ctrl.Wmfe_Height);
        MY_LOGD("WmfeImgiFmt=%d", ctrl.WmfeImgiFmt);
        MY_LOGD("WmfeDpiFmt=%d", ctrl.WmfeDpiFmt);

        LOG_BUFFER_INFO(ctrl.Wmfe_Imgi);
        LOG_BUFFER_INFO(ctrl.Wmfe_Dpi);
        LOG_BUFFER_INFO(ctrl.Wmfe_Tbli);
        LOG_BUFFER_INFO(ctrl.Wmfe_Dpo);


        MY_LOGD("Tbli: ");
        short *addr = (short*) ctrl.Wmfe_Tbli.u4BufVA;
        int index = 0;
        while(index < 128)
        {
          MY_LOGD("%d \t%d \t%d \t%d \t%d \t%d \t%d \t%d \n",
                     addr[index], addr[index+1], addr[index+2], addr[index+3],
                     addr[index+4], addr[index+5], addr[index+6], addr[index+7]);
          index +=8;
        }
    }





}

MBOOL
WMFNode::
threadLoop_WMFFirstPass(DepthMapRequestPtr& pRequest)
{
    CAM_TRACE_NAME("WMFNode::threadLoop_WMFFirstPass");
    // mark on-going-request start
    this->incExtThreadDependency();

    VSDOF_LOGD("threadLoop start: WMF First-Pass, reqID=%d", pRequest->getRequestNo());
    sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();
    EnqueCookieContainer *pEnqueCookie = new EnqueCookieContainer(pRequest, this);

    // enque param
    WMFEParams enqueParams;
    NSIoPipe::WMFEConfig wmfConfig;
    EnqueBIDConfig config = {BID_OCC_OUT_MY_S ,BID_OCC_OUT_DMH, BID_WMF_DMW_INTERNAL, BID_WMF_OUT_DMW};
    // prepare the enque params
    prepareWMFEnqueConfig(pBufferHandler, config, wmfConfig);
    enqueParams.mpfnCallback = onWMFEnqueDone_FirstPass;
    enqueParams.mWMFEConfigVec.push_back(wmfConfig);
    enqueParams.mpCookie = (void*) pEnqueCookie;
    // debug params
    //debugWMFParam(wmfConfig);

    // timer
    pRequest->mTimer.startWMF();
    pRequest->mTimer.startWMFEnque();
    CAM_TRACE_BEGIN("WMFNode::WMFEEnque");
    MBOOL bRet = mpDPEStream->WMFEenque(enqueParams);
    //
    pRequest->mTimer.stopWMFEnque();
    VSDOF_PRFLOG("WMFE enque start - first pass, reqID=%d, config-time=%d ms",
                    pRequest->getRequestNo(), pRequest->mTimer.getElapsedWMFEnque());
    CAM_TRACE_END();
    if(!bRet)
    {
        MY_LOGE("WMF enque failed!!");
        goto lbExit;
    }
    return MTRUE;

lbExit:
    delete pEnqueCookie;
    // mark on-going-request end
    this->decExtThreadDependency();
    return MFALSE;
}

MVOID
WMFNode::
onWMFEnqueDone_FirstPass(WMFEParams& rParams)
{
    EnqueCookieContainer* pEnqueCookie = reinterpret_cast<EnqueCookieContainer*>(rParams.mpCookie);
    WMFNode* pWMFNode = reinterpret_cast<WMFNode*>(pEnqueCookie->mpNode);
    pWMFNode->handleWMFEnqueDone_FirstPass(rParams, pEnqueCookie);
}

MVOID
WMFNode::
handleWMFEnqueDone_FirstPass(
    WMFEParams& rParams,
    EnqueCookieContainer* pEnqueCookie
)
{
    CAM_TRACE_NAME("WMFNode::handleWMFEnqueDone_FirstPass");
    DepthMapRequestPtr pRequest = pEnqueCookie->mRequest;
    sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();
    MUINT32 iReqNo =  pRequest->getRequestNo();
    // check flush status
    if(mpNodeSignal->getStatus(NodeSignal::STATUS_IN_FLUSH))
        goto lbExit;
    // timer
    pRequest->mTimer.stopWMF();
    VSDOF_PRFLOG("+, reqID=%d exec-time=%d msec", iReqNo, pRequest->mTimer.getElapsedWMF());

    if(pRequest->isQueuedDepthRequest(mpPipeOption))
    {
        VSDOF_LOGD("reqID=%d, Store depth info to data storage!.", iReqNo);
        DepthBufferInfo depthInfo;
        // get depth map
        pBufferHandler->getEnquedSmartBuffer(getNodeId(), BID_WMF_OUT_DMW, depthInfo.mpDepthBuffer);
        depthInfo.mpDepthBuffer->mImageBuffer->syncCache(eCACHECTRL_INVALID);
        // get convergence offset
        IMetadata* pOutHalMeta = pBufferHandler->requestMetadata(getNodeId(), BID_META_OUT_HAL_QUEUED);
        if(!tryGetMetadata<MFLOAT>(pOutHalMeta, MTK_CONVERGENCE_DEPTH_OFFSET, depthInfo.mfConvOffset))
        {
            MY_LOGE("Failed to get MTK_CONVERGENCE_DEPTH_OFFSET!!");
            return;
        }
        // push into storage
        mpDepthStorage->push_back(depthInfo);
        // dump buffer
        DumpConfig config("BID_WMF_OUT_DMW_INTO_QUEUE", NULL, MFALSE);
        handleDump(QUEUED_FLOW_DONE, pRequest, &config);
        // notify queued flow type finished
        handleData(QUEUED_FLOW_DONE, pRequest);
    }
    else
    {
        // config MY_S/DMW to GF Node
        pBufferHandler->configOutBuffer(getNodeId(), BID_OCC_OUT_MY_S, eDPETHMAP_PIPE_NODEID_GF);
        pBufferHandler->configOutBuffer(getNodeId(), BID_WMF_OUT_DMW, eDPETHMAP_PIPE_NODEID_GF);
        // get buffer and invalidate
        IImageBuffer* pImgBuf_MY_S, *pImgBuf_DMW;
        pBufferHandler->getEnqueBuffer(getNodeId(), BID_OCC_OUT_MY_S, pImgBuf_MY_S);
        pBufferHandler->getEnqueBuffer(getNodeId(), BID_WMF_OUT_DMW, pImgBuf_DMW);
        pImgBuf_MY_S->syncCache(eCACHECTRL_INVALID);
        pImgBuf_DMW->syncCache(eCACHECTRL_INVALID);
        // handle data and dump
        handleDataAndDump(WMF_TO_GF_DMW_MY_S, pRequest);
    }

lbExit:
    // launch onProcessDone
    pBufferHandler->onProcessDone(getNodeId());
    delete pEnqueCookie;
    // mark on-going-request end
    this->decExtThreadDependency();
}


MVOID
WMFNode::
prepareWMFEnqueConfig(
    sp<BaseBufferHandler> pBufferHandler,
    const EnqueBIDConfig& enqueConfig,
    NSIoPipe::WMFEConfig &rWMFConfig
)
{
    MBOOL bRet = MTRUE;
    IImageBuffer *pImgBuf_SrcImg, *pImgBuf_SrcDepth, *pImgBuf_OutputDepth;
    WMFECtrl ctrl_0, ctrl_1, ctrl_2;
    // input buffers
    bRet &= pBufferHandler->getEnqueBuffer(getNodeId(), enqueConfig.srcImgBID, pImgBuf_SrcImg);
    bRet &= pBufferHandler->getEnqueBuffer(getNodeId(), enqueConfig.srcDepthBID, pImgBuf_SrcDepth);
    pImgBuf_SrcImg->syncCache(eCACHECTRL_FLUSH);
    pImgBuf_SrcDepth->syncCache(eCACHECTRL_FLUSH);
    // output buffers
    pImgBuf_OutputDepth = pBufferHandler->requestBuffer(getNodeId(), enqueConfig.outDepthBID);
    // request internal working buffer
    IImageBuffer* pInterBuf = pBufferHandler->requestBuffer(getNodeId(), enqueConfig.interalImgBID);

    // WMF TBLI tuning buffer
    setupDPEBufInfo(DMA_WMFE_TBLI, mpTbliImgBuf->mImageBuffer.get(), ctrl_0.Wmfe_Tbli);
    setupDPEBufInfo(DMA_WMFE_TBLI, mpTbliImgBuf->mImageBuffer.get(), ctrl_1.Wmfe_Tbli);
    // filter size
    ctrl_0.WmfeFilterSize = eWMFFilterSize;
    ctrl_1.WmfeFilterSize = eWMFFilterSize;

    // config NSIoPipe::WMFEConfig set 0
    ctrl_0.Wmfe_Enable = true;
    // size are all the same, use srcImg(MY_S or MY_SL) size
    ctrl_0.Wmfe_Width = pImgBuf_SrcImg->getImgSize().w;
    ctrl_0.Wmfe_Height = pImgBuf_SrcImg->getImgSize().h;

    // config NSIoPipe::WMFEConfig set 1
    ctrl_1.Wmfe_Enable = true;
    // interal working buffer size
    ctrl_1.Wmfe_Width = pInterBuf->getImgSize().w;
    ctrl_1.Wmfe_Height = pInterBuf->getImgSize().h;

    // IMGI FMT setting
    if(pImgBuf_SrcImg->getImgFormat() == eImgFmt_YV12 || pImgBuf_SrcImg->getImgFormat() == eImgFmt_Y8)
    {
        ctrl_0.WmfeImgiFmt = DPE_IMGI_Y_FMT;
        ctrl_1.WmfeImgiFmt = DPE_IMGI_Y_FMT;
    }
    else if(pImgBuf_SrcImg->getImgFormat() == eImgFmt_YUY2)
    {
        ctrl_0.WmfeImgiFmt = DPE_IMGI_YC_FMT;
        ctrl_1.WmfeImgiFmt = DPE_IMGI_YC_FMT;
    }
    else
    {
        MY_LOGE("WMF CTRL0:IMGI Format not-suporrted!! ImageFormat=%x", pImgBuf_SrcImg->getImgFormat());
        return;
    }

    ctrl_0.WmfeDpiFmt = WMFE_DPI_D_FMT;
    ctrl_1.WmfeDpiFmt = WMFE_DPI_D_FMT;
    // CTRL0: input buffer IMGI pImgBuf_SrcImg: MY_S or MY_SL
    setupDPEBufInfo(DMA_WMFE_IMGI, pImgBuf_SrcImg, ctrl_0.Wmfe_Imgi);
    // CTRL0: input buffer DPI pImgBuf_SrcDepth:  DMH or DMW
    setupDPEBufInfo(DMA_WMFE_DPI, pImgBuf_SrcDepth, ctrl_0.Wmfe_Dpi);
    // CTRL0: output buffer DPO: WorkingBuf_Internal
    setupDPEBufInfo(DMA_WMFE_DPO, pInterBuf, ctrl_0.Wmfe_Dpo);

    // CTRL1: input buffer IMGI: MY_S or MY_SL
    setupDPEBufInfo(DMA_WMFE_IMGI, pImgBuf_SrcImg, ctrl_1.Wmfe_Imgi);
    // CTRL1: input buffer DPI: WorkingBuf_Internal
    setupDPEBufInfo(DMA_WMFE_DPI, pInterBuf, ctrl_1.Wmfe_Dpi);
    // CTRL1: output buffer DPO: DWM or DepthMap
    setupDPEBufInfo(DMA_WMFE_DPO, pImgBuf_OutputDepth, ctrl_1.Wmfe_Dpo);

    // disable the set 2
    ctrl_2.Wmfe_Enable = false;

    rWMFConfig.mWMFECtrlVec.push_back(ctrl_0);
    rWMFConfig.mWMFECtrlVec.push_back(ctrl_1);
    rWMFConfig.mWMFECtrlVec.push_back(ctrl_2);

    #define DEBUG_BUFFER_SETUP(buf) \
        MY_LOGD("DPE buf:" # buf);\
        MY_LOGD("Image buffer size=%dx%d:", buf->getImgSize().w, buf->getImgSize().h);\
        MY_LOGD("Image buffer format=%x", buf->getImgFormat());

    // debug section
    if(DepthPipeLoggingSetup::mbDebugLog)
    {
        DEBUG_BUFFER_SETUP(mpTbliImgBuf->mImageBuffer);
        DEBUG_BUFFER_SETUP(pImgBuf_SrcImg);
        DEBUG_BUFFER_SETUP(pImgBuf_SrcDepth);
        DEBUG_BUFFER_SETUP(pInterBuf);
        DEBUG_BUFFER_SETUP(pImgBuf_OutputDepth);
    }

    #undef DEBUG_BUFFER_SETUP
}

MBOOL
WMFNode::
setupDPEBufInfo(NSIoPipe::DPEDMAPort dmaPort, IImageBuffer* pImgBuf, NSIoPipe::DPEBufInfo& rBufInfo)
{
    // plane 0 address
    rBufInfo.memID = pImgBuf->getFD(0);
    rBufInfo.dmaport = dmaPort;
    rBufInfo.u4BufVA = pImgBuf->getBufVA(0);
    rBufInfo.u4BufPA = pImgBuf->getBufPA(0);
    rBufInfo.u4BufSize = pImgBuf->getBufSizeInBytes(0);
    rBufInfo.u4Stride = pImgBuf->getBufStridesInBytes(0);

    return MTRUE;
}


MVOID
WMFNode::
onFlush()
{
    VSDOF_LOGD("+");
    DepthMapPipeNode::onFlush();
    VSDOF_LOGD("-");
}

}; //NSFeaturePipe_DepthMap
}; //NSCamFeature
}; //NSCam
