/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.rcs.blacklist;

import android.os.Build;
import android.os.SystemProperties;

/**
 * Log.
 */
public class Log {
    private static final String TAG = "Blacklist";

    private static final String PROP_FORCE_DEBUG_KEY = "persist.log.tag.blacklist";

    private static final boolean FORCE_DEBUG = // key or eng load
            (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1) ||
            "eng".equals(Build.TYPE);

    private static final boolean DEBUG = FORCE_DEBUG ||
            android.util.Log.isLoggable(TAG, android.util.Log.DEBUG);

    private static final boolean VERBOSE = FORCE_DEBUG ||
            android.util.Log.isLoggable(TAG, android.util.Log.VERBOSE);

    protected static void d(String tag, Object obj, String msg) {
        if (DEBUG) {
            android.util.Log.d(tag, getPrefix(obj) + msg);
        }
    }

    protected static void d(Object obj, String msg) {
        if (DEBUG) {
            android.util.Log.d(TAG, getPrefix(obj) + msg);
        }
    }

    protected static void d(String tag, String msg) {
        if (DEBUG) {
            android.util.Log.d(tag, getPrefix(tag) + msg);
        }
    }

    protected static void v(String tag, Object obj, String msg) {
        if (VERBOSE) {
            android.util.Log.v(tag, getPrefix(obj) + msg);
        }
    }

    protected static void v(Object obj, String msg) {
        if (VERBOSE) {
            android.util.Log.v(TAG, getPrefix(obj) + msg);
        }
    }

    protected static void e(String tag, String msg) {
        android.util.Log.e(tag, msg);
    }

    protected static void e(Object obj, String msg) {
        android.util.Log.e(TAG, getPrefix(obj) + msg);
    }

    protected static void i(String tag, Object obj, String msg) {
        android.util.Log.i(tag, getPrefix(obj) + msg);
    }

    protected static void i(Object obj, String msg) {
        android.util.Log.i(TAG, getPrefix(obj) + msg);
    }

    protected static void i(String tag, String msg) {
        android.util.Log.i(TAG, getPrefix(tag) + msg);
    }

    protected static void w(String tag, Object obj, String msg) {
        android.util.Log.w(tag, getPrefix(obj) + msg);
    }

    protected static void w(Object obj, String msg) {
        android.util.Log.w(TAG, getPrefix(obj) + msg);
    }

    protected static void wtf(String tag, String msg) {
        android.util.Log.wtf(tag, msg);
    }

    private static String getPrefix(Object obj) {
        String ret = "";
        if (obj != null) {
            ret = obj instanceof String ? (String.valueOf(obj)) : obj.getClass().getSimpleName();
            ret = "[" + ret + "]";
        }

        return ret;
    }

    private static final int LENGTH_NUMBER_PRINT = 2;
    protected static String formatNumber(String number) {
        String formatNum = number;
        if (!DEBUG) {
            int i = number.length() - LENGTH_NUMBER_PRINT;
            i = i > 0 ? i : 0;
            formatNum = String.format("XXXXX%s", number.substring(i));
        }

        return formatNum;
    }
}
