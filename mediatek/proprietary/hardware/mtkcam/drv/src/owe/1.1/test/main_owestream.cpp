/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

///////////////////////////////////////////////////////////////////////////////
// No Warranty
// Except as may be otherwise agreed to in writing, no warranties of any
// kind, whether express or implied, are given by MTK with respect to any MTK
// Deliverables or any use thereof, and MTK Deliverables are provided on an
// "AS IS" basis.  MTK hereby expressly disclaims all such warranties,
// including any implied warranties of merchantability, non-infringement and
// fitness for a particular purpose and any warranties arising out of course
// of performance, course of dealing or usage of trade.  Parties further
// acknowledge that Company may, either presently and/or in the future,
// instruct MTK to assist it in the development and the implementation, in
// accordance with Company's designs, of certain softwares relating to
// Company's product(s) (the "Services").  Except as may be otherwise agreed
// to in writing, no warranties of any kind, whether express or implied, are
// given by MTK with respect to the Services provided, and the Services are
// provided on an "AS IS" basis.  Company further acknowledges that the
// Services may contain errors, that testing is important and Company is
// solely responsible for fully testing the Services and/or derivatives
// thereof before they are used, sublicensed or distributed.  Should there be
// any third party action brought against MTK, arising out of or relating to
// the Services, Company agree to fully indemnify and hold MTK harmless.
// If the parties mutually agree to enter into or continue a business
// relationship or other arrangement, the terms and conditions set forth
// hereunder shall remain effective and, unless explicitly stated otherwise,
// shall prevail in the event of a conflict in the terms in any agreements
// entered into between the parties.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008, MediaTek Inc.
// All rights reserved.
//
// Unauthorized use, practice, perform, copy, distribution, reproduction,
// or disclosure of this information in whole or in part is prohibited.

 
#define LOG_TAG "owestream_test"

#include <vector>

#include <sys/time.h>
#include <sys/stat.h>
#include <sys/prctl.h>

#include <stdio.h>
#include <stdlib.h>
//
#include <errno.h>
#include <fcntl.h>

#include <mtkcam/def/common.h>

//
#include <semaphore.h>
#include <pthread.h>
#include <utils/threads.h>
#include <mtkcam/drv/iopipe/PostProc/IEgnStream.h>
#include <oweunittest.h>

#include <drv/imem_drv.h>

#include <mtkcam/utils/imgbuf/IImageBuffer.h>
#include <utils/StrongPointer.h>
#include <mtkcam/utils/std/common.h>
#include <mtkcam/utils/imgbuf/ImageBufferHeap.h>
//
#include "owe/bittrue.h"
#include "owe/owe_default/frame_0/owe_tc00.h"
#if 0
#include "owe/owe_test_case_00/frame_0/frame_0_owe_setting_00.h"
#include "owe/owe_test_case_00/frame_1/frame_1_owe_setting_00.h"
#include "owe/owe_test_case_00/frame_2/frame_2_owe_setting_00.h"
#include "owe/owe_test_case_00/frame_3/frame_3_owe_setting_00.h"
#include "owe/owe_test_case_00/frame_4/frame_4_owe_setting_00.h"
#include "owe/owe_test_case_00/MultiEnque_owe_test_case_00_frame_0_owe_setting_00.h"
#include "owe/owe_test_case_00/MultiEnque_owe_test_case_00_frame_2_owe_setting_00.h"
#endif
using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSEgn;

pthread_t       OweUserThread;
sem_t           OweSem;
volatile bool            g_bOweThreadTerminated = 0;

static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size);

/*******************************************************************************
*  Main Function 
*  
********************************************************************************/
#if 0
void* OweThreadLoop(void *arg)
{
    (void)arg;
    int ret = 0; 

    ::pthread_detach(::pthread_self());
    ::sem_wait(&OweSem);

    while (g_bOweThreadTerminated) {

        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("Sub Thread do owe_test_case_00_frame_3_OWE_Config\n");
        ret=owe_test_case_00_frame_3_OWE_Config();
        printf("Sub Thread do owe_test_case_00_frame_4_OWE_Config\n");
        ret=owe_test_case_00_frame_4_OWE_Config();
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
    }
    ::sem_post(&OweSem);
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    return NULL;
}
void* OweThreadLoop_OneReqHaveOneBuffer(void *arg)
{
    (void)arg;
    int ret = 0; 

    ::pthread_detach(::pthread_self());
    ::sem_wait(&OweSem);

    while (g_bOweThreadTerminated) {

        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        ret = multi_enque_owe_test_case_00_frame_2_OWE_Config();
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
    }
    ::sem_post(&OweSem);
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    return NULL;
}
void* OweThreadLoop_OneReqHaveTwoBuffer(void *arg)
{
    (void)arg;
    int ret = 0; 

    ::pthread_detach(::pthread_self());
    ::sem_wait(&OweSem);

    while (g_bOweThreadTerminated) {

        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        ret = multi_enque_owe_test_case_00_frame_2_OWE_Config();
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
        printf("###################Sub  Thread###################\n");
    }
    ::sem_post(&OweSem);
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    printf("###################Sub  Wait###################\n");
    return NULL;
}
#endif

int test_oweStream(int argc, char** argv)
{
    int ret = 0; 
    int testCase = -1;
    int i;
    if (argc>=1)
    {
        testCase = atoi(argv[0]);
    }

    switch(testCase)
    {
        case 0:  //Unit Test Case, Only One OCC Request, and One WMFE Request
            g_OWE_UnitTest_Num = OWE_DEFAULT_UT; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            ret=owe_default();
            break;
#if 0
        case 1: //Main and OweUserThread will do the one occ request and one wmfe request at the same time.
            ::sem_init(&OweSem,0, 0);
            g_OWE_UnitTest_Num = OWE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            pthread_create(&OweUserThread, NULL, OweThreadLoop, (void*)NULL);
            g_bOweThreadTerminated = 1;
            g_bTestBlockingDeque = 0;
            ::sem_post(&OweSem);
            for (i=0;i<10;i++)
            {
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");

                printf("Main Thread do owe_test_case_00_frame_0_OWE_Config\n");
                ret=owe_test_case_00_frame_0_OWE_Config();
                printf("Main Thread do owe_test_case_00_frame_1_OWE_Config\n");
                ret=owe_test_case_00_frame_1_OWE_Config();
                printf("Main Thread do owe_test_case_00_frame_2_OWE_Config\n");
                ret=owe_test_case_00_frame_2_OWE_Config();
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
            }

            g_bOweThreadTerminated = 0;
            
            ::sem_wait(&OweSem);

            break;
        case 2: //Two occ request and Two wmfe request at the same time.
            g_OWE_UnitTest_Num = OWE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            g_bOneRequestHaveManyBuffer = 0;
            g_frame2_bOneRequestHaveManyBuffer = 0;
            ret = multi_enque_owe_test_case_00_frame_0_OWE_Config();
            ret = multi_enque_owe_test_case_00_frame_2_OWE_Config();
            break;
        case 3: //Main and Sub thread do Two occ request and Two wmfe request at the same time.
            g_OWE_UnitTest_Num = OWE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            g_bOneRequestHaveManyBuffer = 0;
            g_frame2_bOneRequestHaveManyBuffer = 0;

            ::sem_init(&OweSem,0, 0);
            pthread_create(&OweUserThread, NULL, OweThreadLoop_OneReqHaveOneBuffer, (void*)NULL);
            g_bOweThreadTerminated = 1;
            ::sem_post(&OweSem);
            for (i=0;i<10;i++)
            {
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                ret = multi_enque_owe_test_case_00_frame_0_OWE_Config();
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
            }
            g_bOweThreadTerminated = 0;
            ::sem_wait(&OweSem);

            break;

        case 4: //One occ request (two buffer), One wmfe request (two buffer).
            g_OWE_UnitTest_Num = OWE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            g_bOneRequestHaveManyBuffer = 1;
            g_frame2_bOneRequestHaveManyBuffer = 1;
            ret = multi_enque_owe_test_case_00_frame_0_OWE_Config();
            ret = multi_enque_owe_test_case_00_frame_2_OWE_Config();
            break;
        case 5: //Main and Sub thread do One occ request (two buffer), One wmfe request (two buffer).
            g_OWE_UnitTest_Num = OWE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            g_bOneRequestHaveManyBuffer = 1;
            g_frame2_bOneRequestHaveManyBuffer = 1;

            ::sem_init(&OweSem,0, 0);
            pthread_create(&OweUserThread, NULL, OweThreadLoop_OneReqHaveTwoBuffer, (void*)NULL);
            g_bOweThreadTerminated = 1;
            ::sem_post(&OweSem);
            for (i=0;i<10;i++)
            {
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                ret = multi_enque_owe_test_case_00_frame_0_OWE_Config();
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
                printf("===================Main  Thread===================\n");
            }
            g_bOweThreadTerminated = 0;
            ::sem_wait(&OweSem);
            break;
        case 6: 
            g_OWE_UnitTest_Num = OWE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            g_bTestBlockingDeque = 1;
            ret=owe_test_case_00_frame_0_OWE_Config();
            break;
        case 7: 
            g_OWE_UnitTest_Num = OWE_TESTCASE_UT_0; // some parameter can't be controled by user, but the bit-ture test case use the parameter
            printf("===================Main  Thread===================\n");
            g_bTestBlockingDeque = 0;
            pid_t child_pid;
            child_pid = fork ();

            if (child_pid < 0)
            {
                printf("error in fork!");
            }
            else if (child_pid == 0) {  
                printf("i am the child process, my process id is %d/n",getpid());  
                do{
                    ret=owe_test_case_00_frame_1_OWE_Config();
                }while(1);
            }  
            else {  
                printf("i am the parent process, my process id is %d/n",getpid());   
                do{

                    ret=owe_test_case_00_frame_0_OWE_Config();
                }while(1);
            }  
            break;
#endif
        default:
            break;
    }
    
    ret = 1;
    return ret; 
}

/******************************************************************************
* save the buffer to the file
*******************************************************************************/
static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)
{
    int nw, cnt = 0;
    uint32_t written = 0;

    //LOG_INF("(name, buf, size) = (%s, %x, %d)", fname, buf, size);
    //LOG_INF("opening file [%s]\n", fname);
    int fd = ::open(fname, O_RDWR | O_CREAT, S_IRWXU);
    if (fd < 0) {
        printf(": failed to create file [%s]: %s \n", fname, ::strerror(errno));
        return false;
    }

    //LOG_INF("writing %d bytes to file [%s]\n", size, fname);
    while (written < size) {
        nw = ::write(fd,
                     buf + written,
                     size - written);
        if (nw < 0) {
            printf(": failed to write to file [%s]: %s\n", fname, ::strerror(errno));
            break;
        }
        written += nw;
        cnt++;
    }
    //LOG_INF("done writing %d bytes to file [%s] in %d passes\n", size, fname, cnt);
    ::close(fd);
    return true;
}



