/*
 * Copyright (C) 2011-2014 MediaTek Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mediatek.calllog.calllogfilter;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.android.internal.telephony.TelephonyIntents;

/** M: [Call Log Account Filter] support CALL_LOG_ACCOUNT_FILTER @{ */
public class PhoneAccountInfoHelper {
    public static final String TAG = "PhoneAccountInfoHelper";
    /**
     * Key for get and save the Call Log filter value.
     */
    private static final String CALL_LOG_FILTER_KEY = "call_log_filter";

    /**
     * For Call Log Filter, means display call log from all accounts
     */
    public static final String FILTER_ALL_ACCOUNT_ID = "all_account";

    public static final PhoneAccountInfoHelper INSTANCE = new PhoneAccountInfoHelper();
    private Context mContext;
    private List<AccountInfoListener> mListeners = new ArrayList<AccountInfoListener>();

    private WeakReference<Activity> mActivity;
    private static final String PREF_NAME = "account_select";
    private SharedPreferences  mPref;
    private static final String PRE_TAG = "account_id";

    private static boolean mRegistered = false;

    /**
     * register a listener for phoneAccount info change.
     * @param listener
     */
    public void registerForAccountChange(AccountInfoListener listener) {
        registerAccountReceiverIfNeeded(mContext);
        if (!mListeners.contains(listener)) {
            mListeners.add(listener);
        }
    }

    /**
     * unregister a listener for phoneAccount info change.
     * @param listener
     */
    public void unRegisterForAccountChange(AccountInfoListener listener) {
        mListeners.remove(listener);
    }

    private void registerAccountReceiverIfNeeded(Context context) {
        Log.d(TAG, "registerAccountReceiverIfNeeded(), mRegistered = " + mRegistered);
        if (context == null) {
            Log.d(TAG, "registerAccountReceiverIfNeeded(), context is null");
            return;
        }

        if (!mRegistered) {
            mRegistered = true;
            try {
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(TelephonyIntents.ACTION_DEFAULT_VOICE_SUBSCRIPTION_CHANGED);
                intentFilter.addAction(TelephonyIntents.ACTION_SUBINFO_CONTENT_CHANGE);
                intentFilter.addAction(TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED);

                context.registerReceiver(mReceiver, intentFilter);
            } catch (RuntimeException e) {
                Log.d(TAG, "registerAccountReceiverIfNeeded()-->e = " + e);
                mRegistered = false;
            }
        }
    }

    /**
     * initialize the info helper to receive PhoneAccount info update.
     * @param context  the application context
     */
    public void onInit(Context context) {
        mContext = context;
        registerAccountReceiverIfNeeded(context);
    }

    /**
     * unregister when the receiver is no need
     */
    public void onDestroy() {
        if (mContext != null) {
            mContext.unregisterReceiver(mReceiver);
            mContext = null;
        }
    }

    /**
     * receiver to listen the account info changed.
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (TelephonyIntents.ACTION_DEFAULT_VOICE_SUBSCRIPTION_CHANGED.equals(action) ||
                TelephonyIntents.ACTION_SUBINFO_CONTENT_CHANGE.equals(action) ||
                TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED.equals(action)) {
                Log.d(TAG, "BroadcastReceiver()-->action = " + action);
                if (ensurePerferAccountAvailable(mContext)) {
                    notifyAccountInfoUpdate();
                }
            }
        }
    };

    /**
     * Make sure whether the prefer account has been removed.
     * @param context context
     */
    public boolean ensurePerferAccountAvailable(Context context) {
        String preferAccountId = getPreferAccountId(context);
        if (FILTER_ALL_ACCOUNT_ID.equals(preferAccountId)) {
            return false;
        }
        boolean preferAccountRemoved = true;

        TelecomManager telecomManager = (TelecomManager) context
                .getSystemService(Context.TELECOM_SERVICE);
        List<PhoneAccountHandle> handles = telecomManager.getCallCapablePhoneAccounts();
        for (PhoneAccountHandle handle : handles) {
            if (handle.getId().equals(preferAccountId)) {
                preferAccountRemoved = false;
                break;
            }
        }

        // if the selected account is removed or only one account left, show all accounts
        if (preferAccountRemoved
                || telecomManager.getCallCapablePhoneAccounts().size() <= 1) {
            setPreferAccountId(context, FILTER_ALL_ACCOUNT_ID);
            return true;
        }
        return false;
    }

    /**
     * Get the filter value for Call Logs filter.
     *
     * @param context context
     * @return {@link FILTER_ALL_ACCOUNT_ID}, PhoneAccount Id
     */
    public String getPreferAccountId(Context context) {
        if (context != null) {
            boolean preferAccountRemoved = true;
            TelecomManager telecomManager = (TelecomManager) context
                    .getSystemService(Context.TELECOM_SERVICE);
            String accountPrefered = "all_account";
            if (mPref == null) {
                return accountPrefered;
            }

            accountPrefered = mPref.getString(PRE_TAG, "all_account");
            Log.d(TAG, "getPreferAccountId(), " + accountPrefered);

            if (accountPrefered.equals(FILTER_ALL_ACCOUNT_ID)) {
                return accountPrefered;
            }

            List<PhoneAccountHandle> handles =
                    telecomManager.getCallCapablePhoneAccounts();
            for (PhoneAccountHandle handle : handles) {
                if (handle.getId().equals(accountPrefered)) {
                    preferAccountRemoved = false;
                    break;
                }
            }

            if (preferAccountRemoved
                    || telecomManager.getCallCapablePhoneAccounts().size() <= 1) {
                setPreferAccountId(context, FILTER_ALL_ACCOUNT_ID);
                accountPrefered = "all_account";
            }

            return accountPrefered;
        }
        return FILTER_ALL_ACCOUNT_ID;
    }

    /**
     * Save the user selected value for Call Logs filter.
     *
     * @param context context
     * @param id {@link FILTER_ALL_ACCOUNT_ID}, PhoneAccount Id
     */
    public boolean setPreferAccountId(Context context, String id) {
        Log.d(TAG, "setPreferAccountId()-->context = " + context + " id = " + id);
        TelecomManager telecomManager = (TelecomManager) context
                .getSystemService(Context.TELECOM_SERVICE);
        List<PhoneAccountHandle> handles =
                telecomManager.getCallCapablePhoneAccounts();
        if (telecomManager.getCallCapablePhoneAccounts().size() <= 1) {
            return false;
        }

        if (!"all_account".equals(id)) {
            boolean preferAccountRemoved = true;
            for (PhoneAccountHandle handle : handles) {
                if (handle.getId().equals(id)) {
                    preferAccountRemoved = false;
                    break;
                }
            }

            if (preferAccountRemoved) {
                return false;
            }
        }

        if (context != null) {
            SharedPreferences.Editor editor = mPref.edit();
            editor.putString(PRE_TAG, id);
            editor.apply();
            Log.d(TAG, "setPreferAccountId(), " + id);
            return true;
        }
        return false;
    }

    public void setActivity(Activity activity) {
        mActivity = new WeakReference<Activity> (activity);
        setSharedPref();
    }

    private void setSharedPref() {
        if (mActivity == null) {
            return;
        }

        Activity activity = mActivity.get();
        if (activity != null) {
            mPref = activity.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        }

        ensurePerferAccountAvailable(mContext);
    }

    /**
     * Notify listeners when the account info changed.
     */
    public void notifyAccountInfoUpdate() {
        for (AccountInfoListener listener : mListeners) {
            listener.onAccountInfoUpdate();
        }
    }

    /**
     * Notify listeners when the prefer account changed.
     * @param id {@link FILTER_ALL_ACCOUNT_ID}, PhoneAccount Id
     */
    public void notifyPreferAccountChange(String id) {
        Log.d(TAG, "notifyPreferAccountChange()-->mListeners = " + mListeners.size()
                + " id = " + id);
        for (AccountInfoListener listener : mListeners) {
            listener.onPreferAccountChanged(id);
        }
    }

    public interface AccountInfoListener {
        public void onPreferAccountChanged(String index);
        public void onAccountInfoUpdate();
    }

}
/** @} */
