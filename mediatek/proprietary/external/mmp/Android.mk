LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)
LOCAL_SRC_FILES = \
    mmp.c  \

LOCAL_C_INCLUDES += \
	$(MTK_PATH_SOURCE)/kernel/include \

LOCAL_MODULE_TAGS := debug
LOCAL_MODULE := mmp
LOCAL_MODULE_OWNER := mtk
LOCAL_MULTILIB := 32

ifeq ($(strip $(TARGET_BUILD_VARIANT)),eng)
    LOCAL_CFLAGS += -DDEFAULT_ENABLE_MMPROFILE
endif

LOCAL_SHARED_LIBRARIES := libcutils liblog
include $(MTK_EXECUTABLE)
