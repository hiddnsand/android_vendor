package com.mediatek.mms.plugin;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony.Mms;
import android.provider.Telephony.Sms;
import android.util.Log;

import com.google.android.mms.util.SqliteWrapper;

import com.mediatek.mms.ext.DefaultOpMessagingNotificationExt;

/**
 *  Op18MessagingNotificationExt.
 *
 */
public class Op18MessagingNotificationExt extends DefaultOpMessagingNotificationExt {

    private  Op18MmsPreference mMmsPreferenceExt = null;
    private static final String TAG = "Mms/Op18MessagingNotificationExt";
    private static final String[] SMS_SUB_ID_PROJECTION = new String[] { Sms.SUBSCRIPTION_ID };
    private static final Uri UNDELIVERED_URI = Uri.parse("content://mms-sms/undelivered");

    /**
     * Construction.
     * @param context Context
     */
    public  Op18MessagingNotificationExt(Context context) {
        super(context);
        mMmsPreferenceExt = Op18MmsPreference.getInstance(context);
    }

    public String notifyFailed(Context context, long threadId, String defaultRingtone) {

        Log.d(TAG, "notifyFailed = " + threadId);
        String ringtoneStr = Op18MmsPreference.DEFAULT_RINGTONE;
        String where = "read=0";
        String NEW_NOTIFY_FAILED_CONSTRAINT =
                                 "(" + "read=0" + " AND " + Sms.THREAD_ID + " = " + threadId + ")";

        String[] MMS_THREAD_ID_PROJECTION = new String[] {Sms.SUBSCRIPTION_ID};

        Cursor undeliveredCursor = SqliteWrapper.query(context, context.getContentResolver(),
                            UNDELIVERED_URI, MMS_THREAD_ID_PROJECTION,
                            NEW_NOTIFY_FAILED_CONSTRAINT, null, null);

        if (undeliveredCursor == null) {
            Log.d(TAG, "notifyFailed = undeliveredCursor = null");
        } else {
            Log.d(TAG, "notifyFailed count : " + undeliveredCursor.getCount());
        }
        int index = undeliveredCursor.getColumnIndex(Sms.SUBSCRIPTION_ID);
        Log.d(TAG, "notifyFailed index = " + index);
        if (undeliveredCursor.moveToFirst()) {
            int subId = undeliveredCursor.getInt(index);
            ringtoneStr = mMmsPreferenceExt.getMMSRingtone(context, subId);
        }
        undeliveredCursor.close();
        Log.d(TAG, "notifyFailed: ringtoneStr = " + ringtoneStr);
        return ringtoneStr;
    }


    public String updateNotification(Context context, Uri mostRecentUri, String defaultRingtone) {
        Log.d(TAG, "updateNotification=" + mostRecentUri);
        String ringtoneStr = Op18MmsPreference.DEFAULT_RINGTONE;
        String[] SMS_SUB_ID_PROJECTION = new String[] { Sms.SUBSCRIPTION_ID };
        Cursor cursor = SqliteWrapper.query(context, context.getContentResolver(),
                              mostRecentUri, SMS_SUB_ID_PROJECTION, null, null, null);
        int index = cursor.getColumnIndex(Sms.SUBSCRIPTION_ID);
        if (cursor.moveToFirst()) {
            int subId =  cursor.getInt(index);
            ringtoneStr = mMmsPreferenceExt.getMMSRingtone(context, subId);
            Log.d(TAG, "updateNotification: ringtoneStr = " + ringtoneStr);
        }
        cursor.close();
        return ringtoneStr;
    }


    public String notifyClassZeroMessage(Context context, int subId, String defaultRingtone) {
        return mMmsPreferenceExt.getMMSRingtone(context, subId);
    }
    public String blockingUpdateNewMessageIndicator(Context context, long newMsgThreadId,
                            String defaultTone, boolean appNotificationEnabled) {
        Log.d(TAG, "blockingUpdateNewMessageIndicator =" + newMsgThreadId);
        String ringtoneStr = Op18MmsPreference.DEFAULT_RINGTONE;

        if (!appNotificationEnabled) {
            return defaultTone;
        }
        ContentResolver resolver = context.getContentResolver();

        Log.d(TAG, "blockingUpdateNewMessageIndicator sms empty");
        String NEW_INCOMING_SMT_CONSTRAINT =
                                 "(" + Sms.TYPE + " = " + Sms.MESSAGE_TYPE_INBOX
                                  + " AND " + Sms.THREAD_ID + " = " + newMsgThreadId + ")";

        Cursor cursor = SqliteWrapper.query(context, resolver, Sms.CONTENT_URI,
                                                SMS_SUB_ID_PROJECTION, NEW_INCOMING_SMT_CONSTRAINT,
                                                null, Sms.DATE + " desc");

        if (cursor == null || cursor.getCount() == 0) {
            Log.d(TAG, "sms count = 0");
            cursor.close();
            resolver = context.getContentResolver();

            String NEW_INCOMING_MMT_CONSTRAINT =
                                "(" + Mms.MESSAGE_BOX + "=" + Mms.MESSAGE_BOX_INBOX
                                + " AND " + Mms.THREAD_ID + " = " + newMsgThreadId + ")";

            cursor = SqliteWrapper.query(context, resolver, Mms.CONTENT_URI,
                                                SMS_SUB_ID_PROJECTION, NEW_INCOMING_MMT_CONSTRAINT,
                                                null, Mms.DATE + " desc");
        }
        String[] columnNames = cursor.getColumnNames();
        int index = cursor.getColumnIndex(Sms.SUBSCRIPTION_ID);
        Log.d(TAG, "cursor.getCount()" + cursor.getCount());
        if (cursor.moveToFirst()) {
            Log.d(TAG, "fetch: ringtoneStr");
            int subId =  cursor.getInt(index);
            ringtoneStr = mMmsPreferenceExt.getMMSRingtone(context, subId);
        }
        Log.d(TAG, "blockingUpdateNewMessageIndicator: ringtoneStr = " + ringtoneStr);
        cursor.close();
        return ringtoneStr;
    }
}
