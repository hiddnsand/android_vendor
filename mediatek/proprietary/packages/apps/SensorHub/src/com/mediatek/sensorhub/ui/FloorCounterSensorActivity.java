package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.SwitchPreference;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.mediatek.sensorhub.settings.Utils;

public class FloorCounterSensorActivity extends BaseActivity implements OnClickListener,
        OnPreferenceChangeListener {

    private static final String TAG = "FloorCounterSensorActivity";
    public static final String KEY_TOTAL_STEPS_PREFER = "total_steps";
    private SwitchPreference mFloorCounterPref;
    private Preference mTotalFloorCountPref;
    private Button mClearButton;
    private float mPreToatalFloorCount;

    public FloorCounterSensorActivity() {
        super(Utils.KEY_FLOOR_COUNTER_STATUS);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.sensor_step_counter_pref);
        initializeAllPreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePreferenceStatus();
    }

    @Override
    protected void onPause() {
        float totalFloorCount = 0;
        try {
            totalFloorCount = Float.valueOf(findPreference("0").getSummary().toString());
        } catch (NullPointerException e) {
            Log.d(TAG, "onPause totalFloorCount : " + totalFloorCount);
        }
        Utils.setSensorValues(Utils.KEY_TOTAL_FLOOR_COUNT, totalFloorCount);
        super.onPause();
    }

    private void initializeAllPreferences() {
        mFloorCounterPref = (SwitchPreference) Utils.createPreference(Utils.TYPE_SWITCH,
                mSensorKeyMap.get(Sensor.TYPE_FLOOR_COUNT).getName(), String
                        .valueOf(Sensor.TYPE_FLOOR_COUNT), getPreferenceScreen(), this);
        mFloorCounterPref.setOrder(-1);
        mTotalFloorCountPref = findPreference(KEY_TOTAL_STEPS_PREFER);
        mTotalFloorCountPref.setTitle(R.string.total_floor_count_string);
        findPreference("0").setTitle(R.string.accumulated_floor_count_string);
        PedometerLayoutPreference pedometerLayout =
            (PedometerLayoutPreference) findPreference("pedometer_set_view");
        pedometerLayout.findViewById(R.id.step_length_value).setVisibility(View.GONE);
        pedometerLayout.findViewById(R.id.step_length_title).setVisibility(View.GONE);
        pedometerLayout.findViewById(R.id.set_step).setVisibility(View.GONE);
        mClearButton = (Button) pedometerLayout.findViewById(R.id.clear_step);
        mClearButton.setText(R.string.clear_floor_count);
        mClearButton.setOnClickListener(this);
    }

    private void updatePreferenceStatus() {
        mFloorCounterPref.setChecked(Utils.getSensorStatus(Utils.KEY_FLOOR_COUNTER_STATUS));
        mPreToatalFloorCount = Utils.getSensorValues(Utils.KEY_PRE_TOTAL_FLOOR_COUNT);
        float totalFloorCount = Utils.getSensorValues(Utils.KEY_TOTAL_FLOOR_COUNT);
        findPreference("0").setSummary(String.valueOf(totalFloorCount));
        mTotalFloorCountPref.setSummary(String.valueOf(totalFloorCount - mPreToatalFloorCount));
        if (mTotalFloorCountPref.getSummary() != null
                && !TextUtils.isEmpty(mTotalFloorCountPref.getSummary())) {
            mClearButton.setEnabled(true);
        } else {
            mClearButton.setEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {
        Button buttonViewButton = (Button) v;
        Log.d(TAG, "onclick: " + buttonViewButton.getText());
        if (v == mClearButton) {
            clear();
        }
    }

    private void clear() {
        if (mTotalFloorCountPref.getSummary() != null
                && !TextUtils.isEmpty(mTotalFloorCountPref.getSummary())) {
            mTotalFloorCountPref.setSummary(R.string.init_number_str);
        }
        try {
            mPreToatalFloorCount = Float.valueOf(findPreference("0").getSummary().toString());
        } catch (NullPointerException e) {
            Log.d(TAG, "clear mPreToatalFloorCount : " + mPreToatalFloorCount);
        }
        Utils.setSensorValues(Utils.KEY_PRE_TOTAL_FLOOR_COUNT, mPreToatalFloorCount);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean bNewValue = (Boolean) newValue;
        if (preference == mFloorCounterPref) {
            Utils.setSensorStatus(Utils.KEY_FLOOR_COUNTER_STATUS, bNewValue);
            if (mBound) {
                mSensorService.registerSensor(preference.getKey(), bNewValue);
            }
            if (!bNewValue) {
                clear();
            }
        }
        return true;
    }

    @Override
    public void onSensorChanged(float[] value) {
        // Log.d(TAG, "onSensorChanged value is: " + value[0]);
        for (int i = 0; i < value.length; i++) {
            Preference preference = findPreference(String.valueOf(i));
            if (preference != null) {
                preference.setSummary(String.valueOf(value[i]));
            }
        }
        mTotalFloorCountPref.setSummary(String.valueOf(value[0] - mPreToatalFloorCount));
        mClearButton.setEnabled(true);
    }
}
