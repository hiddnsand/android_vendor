package com.mediatek.op12.settings.wfc.service;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.mediatek.op12.settings.wfc.E911Errors;
import com.mediatek.op12.settings.wfc.parser.EmergencyAddressParser;
import com.mediatek.op12.settings.wfc.parser.RequestResponse;
import com.mediatek.op12.settings.wfc.parser.XmlConstructor;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

/** Class interacting with network.
 */
public class E911NwInterface {

    static final String TAG = "OP12E911NwInterface";
    // TODO: get actual URL
    private static final String SERVER_URL = "https://sentitlement2.mobile.att.net";
    private static final int CONNECTION_TIMER = 15000;

    /** Interface for network reponse callback.
     */
    public interface E911ResponseCallback {
        /** Query response callback method.
         * @param rsp response
         * @return
         */
        public void onQueryAddressResponse(RequestResponse.QueryAddressResponse rsp);
        /** Add response callback method.
         * @param rsp response
         * @return
         */
        public void onAddUpdateAddressResponse(RequestResponse.AddUpdateAddressResponse rsp);
        // For future development
        //public void onDeleteAddressResponse();

        /** Network error callback method.
         * @param cause error cause
         * @return
         */
        public void onNetworkError(int cause);
    }

    private static Random sRandomGenerator = new Random();
    private Context mContext;
    private static E911NwInterface sE911NwInterface;

    private HandlerThread mThread;
    private Handler mThreadHandler;

    private int mCurrentTransactionId = -1;

    /** Method to get E911NwInterface instance.
     * @param context context
     * @return E911NwInterface
     */
    public static E911NwInterface getInstance(Context context) {
        if (sE911NwInterface == null) {
            sE911NwInterface = new E911NwInterface(context);
        }
        return sE911NwInterface;
    }

    private E911NwInterface(Context context) {
        mContext = context;

        mThread = new HandlerThread("EmergencyAddressNwIntreface-worker");
        mThread.start();
        mThreadHandler = new Handler(mThread.getLooper());
    }

    /** Method to query E911 Address from server.
     * @param rspCallback rspCallback
     * @return
     */
    public void queryE911Address(E911ResponseCallback rspCallback) {
        Log.d(TAG, "Query request");
        sendRequest(RequestResponse.QUERY_REQUEST, null, rspCallback);
    }

    /** Method to add E911 address at server.
     * @param userDetails userDetails
     * @param rspCallback rspCallback
     * @return
     */
    public void addAddress(RequestResponse.Address userDetails,
            E911ResponseCallback rspCallback) {
        Log.d(TAG, "Add, request: " + userDetails);
        sendRequest(RequestResponse.ADD_REQUEST, userDetails, rspCallback);
    }

    /** Method to perform partial validation of E911 address.
     * @param userDetails userDetails
     * @param rspCallback rspCallback
     * @return
     */
    public void performPartialValidationAddress(RequestResponse.Address userDetails,
        E911ResponseCallback rspCallback) {
        Log.d(TAG, "PartialValidation, request: " + userDetails);
        sendRequest(RequestResponse.PARTIAL_VALIDATION_REQUEST, userDetails, rspCallback);
    }

    /** Method to delete E911 address at server.
     * @param userDetails userDetails
     * @param rspCallback rspCallback
     * @return
     */
    public void deleteAddress(RequestResponse.Address userDetails,
            E911ResponseCallback rspCallback) {
        Log.d(TAG, "Delete, request: " + userDetails);
        sendRequest(RequestResponse.DELETE_REQUEST, userDetails, rspCallback);
    }

    /** Method to cancel sent request.
     * @return
     */
    public synchronized void cancelRequest() {
        // Cannot close the ongoing connection as the thread on which its processing is blocked.
        // So, set Transaction id as negative, as genuine id will be >0.
        Log.d(TAG, "Cancel request");
        setCurrentTransactionId(-1);
    }

    private void sendRequest(int reqType, RequestResponse.Address userDetails,
            E911ResponseCallback rspCallback) {
        /* Discard next request if one is underprocessing. Entertain one request at a time.
         * To queue new request, first cancel ongoing request.*/
        if (getCurrentTransactionId() > 0) {
            Log.d(TAG, "Transaction:" + getCurrentTransactionId() + " ongoing.Drop new request");
            return;
        }
        int transactionId = getNewTransactionId();
        setCurrentTransactionId(transactionId);

        HttpsURLConnection connection = null;
        try {
            URL obj = new URL(getServerUrl());
            connection = (HttpsURLConnection) obj.openConnection();
            connection.setConnectTimeout(CONNECTION_TIMER);
            // TODO: add reuqest header
            // Deepti con.setRequestMethod("POST");
            connection.setRequestMethod("GET");
            //con.setRequestProperty("User-Agent", USER_AGENT);
            connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            String xmlFile = XmlConstructor.makeXml(mContext, reqType, userDetails);
            Log.d(TAG, "xmlFile: " + xmlFile);
            // Send post request
            connection.setDoOutput(true);
            //con.setDoInput(true);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(xmlFile);
            wr.flush();
            wr.close();
            int responseCode = connection.getResponseCode();
            Log.d(TAG, "TransactionId:" + transactionId + ",Response Code:" + responseCode);
            if (isResponseCodeValid(responseCode)) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                Log.d(TAG, "Response: " + response.toString());
                sendResponse(transactionId, reqType, response.toString(), rspCallback);
            } else {
                sendErrorResponse(transactionId, responseCode, rspCallback);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e);
            sendErrorResponse(transactionId, E911Errors.E911_ERR_MALFORMED_URL, rspCallback);
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e);
            sendErrorResponse(transactionId, E911Errors.E911_ERR_PROTOCOL_EXP, rspCallback);
        } catch (SocketTimeoutException e) {
            Log.e(TAG, "SocketTimeoutException: " + e);
            sendErrorResponse(transactionId, E911Errors.E911_ERR_SOCKET_IO_EXP, rspCallback);
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e);
            sendErrorResponse(transactionId, E911Errors.E911_ERR_IO_EXP, rspCallback);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            setCurrentTransactionId(-1);
        }
    }

    private void sendResponse(int transactionId, int reqType, String response,
            E911ResponseCallback rspCallback) {
        int currentTransId = getCurrentTransactionId();
        if (transactionId != currentTransId) {
            Log.d(TAG, "sendResponse, transactionId:" + transactionId + ", ongoingTrans:"
                    + currentTransId + ". Drop response");
            return;
        }
        EmergencyAddressParser.ParserData parsedData = parseResponse(response);
        switch (reqType) {
            case RequestResponse.QUERY_REQUEST:
                sendQueryResponse(parsedData, rspCallback);
                break;
            case RequestResponse.PARTIAL_VALIDATION_REQUEST:
            case RequestResponse.ADD_REQUEST:
                sendAddResponse(parsedData, rspCallback);
                break;
            default:
                 Log.d(TAG, "invalid reqType: " + reqType);
                 break;
        }
    }

    private synchronized void sendQueryResponse(EmergencyAddressParser.ParserData parsedData,
           E911ResponseCallback rspCallback) {
        RequestResponse.ErrorCode err = EmergencyAddressParser.getErrDetails(parsedData);
        RequestResponse.UserDetail userDetails = EmergencyAddressParser.getUserDetails(parsedData);
        RequestResponse.QueryAddressResponse rsp
                = new RequestResponse.QueryAddressResponse(err, userDetails);
        Log.d(TAG, "errStatus: " + rsp.errStatus);
        Log.d(TAG, "userDetails: " + rsp.userDetail);
        if (rspCallback != null) {
            rspCallback.onQueryAddressResponse(rsp);
        }
    }

    private synchronized void sendAddResponse(EmergencyAddressParser.ParserData parsedData,
            E911ResponseCallback rspCallback) {
        RequestResponse.ErrorCode err = EmergencyAddressParser.getErrDetails(parsedData);
        RequestResponse.UserDetail userDetails = EmergencyAddressParser.getUserDetails(parsedData);
        ArrayList<RequestResponse.Address> altAddress = EmergencyAddressParser
                .getAltAddressList(parsedData);
        RequestResponse.AddUpdateAddressResponse rsp
                = new RequestResponse.AddUpdateAddressResponse(err, userDetails, altAddress);
        Log.d(TAG, "errStatus: " + rsp.errStatus);
        Log.d(TAG, "userDetails: " + rsp.userDetail);
        Log.d(TAG, "altAddress: " + rsp.altAddress);
        if (rspCallback != null) {
            rspCallback.onAddUpdateAddressResponse(rsp);
        }
    }

    private synchronized void sendErrorResponse(int transactionId, int error,
            E911ResponseCallback rspCallback) {
        int currentTransId = getCurrentTransactionId();
        if (transactionId != currentTransId) {
            Log.d(TAG, "sendErrorResponse, transactionId:" + transactionId + "ongoingTrans:"
                    + currentTransId);
            return;
        }
        Log.d(TAG, "sendErrorResponse, error: " + error + ", callback:" + rspCallback);
        if (rspCallback != null) {
            rspCallback.onNetworkError(error);
        }
    }

    private EmergencyAddressParser.ParserData parseResponse(String xml) {
        EmergencyAddressParser parser = EmergencyAddressParser.getInstance();
        EmergencyAddressParser.ParserData parsedData = parser.parse(xml);
        return parsedData;
    }

    private boolean isResponseCodeValid(int responseCode) {
        boolean d = (responseCode == HttpURLConnection.HTTP_OK) ? true : false;
        Log.d(TAG, "isResponseCodeValid: " + d);
        return (responseCode == HttpURLConnection.HTTP_OK) ? true : false;
    }

    private String getServerUrl() {
        return SERVER_URL;
    }

    private synchronized void setCurrentTransactionId(int id) {
        Log.d(TAG, "setCurrentTransactionId: " + id);
        mCurrentTransactionId = id;
    }

    private synchronized int getCurrentTransactionId() {
        return mCurrentTransactionId;
    }

    private synchronized static int getNewTransactionId() {
        return sRandomGenerator.nextInt(10000);
    }
}
