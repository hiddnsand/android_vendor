/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op02.calendar;

import android.content.Context;
import android.util.Log;

import com.mediatek.calendar.ext.OpCalendarCustomizationFactoryBase;
import com.mediatek.calendar.ext.ILunarExt;
import com.mediatek.calendar.ext.IEditEventViewExt;

public class Op02CalendarCustomizationFactory extends OpCalendarCustomizationFactoryBase {
    private static LunarPlugin op02LunarPlugin = null;
    private static EditEventViewExt op02EditEventViewExt = null;
    private Context mContext;

    public Op02CalendarCustomizationFactory(Context context) {
        super();
        Log.d("Op02CalendarCustomizationFactory", "[context]= "+ context);
        mContext = context;
    }

    @Override
    public ILunarExt makeLunarCalendar(Context context) {
        Log.d("Op02CalendarCustomizationFactory", "[makeLunarCalendar]");
        if(op02LunarPlugin == null) {
            op02LunarPlugin = new LunarPlugin(mContext);
        }
        Log.d("Op02CalendarCustomizationFactory", "[obj1] :"+  op02LunarPlugin);
        return op02LunarPlugin;
    }

    @Override
    public IEditEventViewExt makeEditEventCalendar(Context context) {
        Log.d("Op02CalendarCustomizationFactory", "[makeEditEventCalendar]");
        if(op02EditEventViewExt == null) {
            op02EditEventViewExt = new EditEventViewExt(mContext);
        }
        Log.d("Op02CalendarCustomizationFactory", "[obj2] :"+  op02EditEventViewExt);
        return op02EditEventViewExt;
    }
}