/*
 * jpeglib.h
 *
 * Copyright (C) 1991-1998, Thomas G. Lane.
 * Modified 2002-2011 by Guido Vollbeding.
 * This file is part of the Independent JPEG Group's software.
 * For conditions of distribution and use, see the accompanying README file.
 *
 * This file defines the application interface for the JPEG library.
 * Most applications using the library need only include this file,
 * and perhaps jerror.h if they want to know the exact error codes.
 */

#ifndef JPEGLIB_MTK_H
#define JPEGLIB_MTK_H

/*
 * First we include the configuration files that record how this
 * installation of the JPEG library is set up.  jconfig.h can be
 * generated automatically for many systems.  jmorecfg.h contains
 * manual configuration options that most people need not worry about.
 */

#ifndef JCONFIG_MTK_INCLUDED	/* in case jinclude.h already did */
#include "jconfig.h"		/* widely used configuration options */
#endif
#include "jmorecfg.h"		/* seldom changed options */

#if 1				/* def JPEG_THREAD_SUPPORT */
#include "jthread.h"
#define MAX_CORE_NUM_MTK			4

/* Encode speed options for Multi core algorithm */
typedef enum {
	SLOW_MTK,
	FAST_MTK,
	ULTRA_FAST_MTK
} J_ENCODE_SPEED_MTK;

struct jpeg_output_table_MTK {
	int size;
	JOCTET_MTK *address;
};
#endif

/* Version ID for the JPEG library.
 * Might be useful for tests like "#if JPEG_LIB_VERSION_MTK >= 60".
 */

#define JPEG_LIB_VERSION_MTK  62	/* Version 6b */


/* Various constants determining the sizes of things.
 * All of these are specified by the JPEG standard, so don't change them
 * if you want to be compatible.
 */

#define DCTSIZE_MTK		    8	/* The basic DCT block is 8x8 samples */
#define DCTSIZE2_MTK	    64	/* DCTSIZE squared; # of elements in a block */
#define NUM_QUANT_TBLS_MTK      4	/* Quantization tables are numbered 0..3 */
#define NUM_HUFF_TBLS_MTK       4	/* Huffman tables are numbered 0..3 */
#define NUM_ARITH_TBLS_MTK      16	/* Arith-coding tables are numbered 0..15 */
#define MAX_COMPS_IN_SCAN_MTK   4	/* JPEG limit on # of components in one scan */
#define MAX_SAMP_FACTOR_MTK     4	/* JPEG limit on sampling factors */
/* Unfortunately, some bozo at Adobe saw no reason to be bound by the standard;
 * the PostScript DCT filter can emit files with many more than 10 blocks/MCU.
 * If you happen to run across such a file, you can up D_MAX_BLOCKS_IN_MCU
 * to handle it.  We even let you do this from the jconfig.h file.  However,
 * we strongly discourage changing C_MAX_BLOCKS_IN_MCU; just because Adobe
 * sometimes emits noncompliant files doesn't mean you should too.
 */
#define C_MAX_BLOCKS_IN_MCU_MTK   10	/* compressor's limit on blocks per MCU */
#ifndef D_MAX_BLOCKS_IN_MCU_MTK
#define D_MAX_BLOCKS_IN_MCU_MTK   10	/* decompressor's limit on blocks per MCU */
#endif


/* Data structures for images (arrays of samples and of DCT coefficients).
 * On 80x86 machines, the image arrays are too big for near pointers,
 * but the pointer arrays can fit in near memory.
 */

typedef JSAMPLE_MTK FAR_MTK * JSAMPROW_MTK;	/* ptr to one image row of pixel samples. */
typedef JSAMPROW_MTK * JSAMPARRAY_MTK;	/* ptr to some rows (a 2-D sample array) */
typedef JSAMPARRAY_MTK * JSAMPIMAGE_MTK;	/* a 3-D sample array: top index is color */

typedef JCOEF_MTK JBLOCK_MTK[DCTSIZE2_MTK];	/* one block of coefficients */
typedef JBLOCK_MTK FAR_MTK * JBLOCKROW_MTK;	/* pointer to one row of coefficient blocks */
typedef JBLOCKROW_MTK * JBLOCKARRAY_MTK;	/* a 2-D array of coefficient blocks */
typedef JBLOCKARRAY_MTK * JBLOCKIMAGE_MTK;	/* a 3-D array of coefficient blocks */

typedef JCOEF_MTK FAR_MTK * JCOEFPTR_MTK;	/* useful in a couple of places */


/* Types for JPEG compression parameters and working tables. */


/* DCT coefficient quantization tables. */

typedef struct {
	/* This array gives the coefficient quantizers in natural array order
	 * (not the zigzag order in which they are stored in a JPEG DQT marker).
	 * CAUTION: IJG versions prior to v6a kept this array in zigzag order.
	 */
	UINT16_MTK quantval[DCTSIZE2_MTK];	/* quantization step for each coefficient */
	/* This field is used only during compression.  It's initialized FALSE when
	 * the table is created, and set TRUE when it's been output to the file.
	 * You could suppress output of a table by setting this to TRUE.
	 * (See jpeg_suppress_tables for an example.)
	 */
	boolean_MTK sent_table;	/* TRUE when table has been output */
} JQUANT_TBL_MTK;


/* Huffman coding tables. */

typedef struct {
	/* These two fields directly represent the contents of a JPEG DHT marker */
	UINT8_MTK bits[17];		/* bits[k] = # of symbols with codes of */
	/* length k bits; bits[0] is unused */
	UINT8_MTK huffval[256];	/* The symbols, in order of incr code length */
	/* This field is used only during compression.  It's initialized FALSE when
	 * the table is created, and set TRUE when it's been output to the file.
	 * You could suppress output of a table by setting this to TRUE.
	 * (See jpeg_suppress_tables for an example.)
	 */
	boolean_MTK sent_table;	/* TRUE when table has been output */
} JHUFF_TBL_MTK;


/* Basic info about one component (color channel). */

typedef struct {
	/* These values are fixed over the whole image. */
	/* For compression, they must be supplied by parameter setup; */
	/* for decompression, they are read from the SOF marker. */
	int component_id;	/* identifier for this component (0..255) */
	int component_index;	/* its index in SOF or cinfo->comp_info[] */
	int h_samp_factor;	/* horizontal sampling factor (1..4) */
	int v_samp_factor;	/* vertical sampling factor (1..4) */
	int quant_tbl_no;	/* quantization table selector (0..3) */
	/* These values may vary between scans. */
	/* For compression, they must be supplied by parameter setup; */
	/* for decompression, they are read from the SOS marker. */
	/* The decompressor output side may not use these variables. */
	int dc_tbl_no;		/* DC entropy table selector (0..3) */
	int ac_tbl_no;		/* AC entropy table selector (0..3) */

	/* Remaining fields should be treated as private by applications. */

	/* These values are computed during compression or decompression startup: */
	/* Component's size in DCT blocks.
	 * Any dummy blocks added to complete an MCU are not counted; therefore
	 * these values do not depend on whether a scan is interleaved or not.
	 */
	JDIMENSION_MTK width_in_blocks;
	JDIMENSION_MTK height_in_blocks;
	/* Size of a DCT block in samples.  Always DCTSIZE for compression.
	 * For decompression this is the size of the output from one DCT block,
	 * reflecting any scaling we choose to apply during the IDCT step.
	 * Values of 1,2,4,8 are likely to be supported.  Note that different
	 * components may receive different IDCT scalings.
	 */
	int DCT_scaled_size;
	/* The downsampled dimensions are the component's actual, unpadded number
	 * of samples at the main buffer (preprocessing/compression interface), thus
	 * downsampled_width = ceil(image_width * Hi/Hmax)
	 * and similarly for height.  For decompression, IDCT scaling is included, so
	 * downsampled_width = ceil(image_width * Hi/Hmax * DCT_scaled_size/DCTSIZE)
	 */
	JDIMENSION_MTK downsampled_width;	/* actual width in samples */
	JDIMENSION_MTK downsampled_height;	/* actual height in samples */
	/* This flag is used only for decompression.  In cases where some of the
	 * components will be ignored (eg grayscale output from YCbCr image),
	 * we can skip most computations for the unused components.
	 */
	boolean_MTK component_needed;	/* do we need the value of this component? */

	/* These values are computed before starting a scan of the component. */
	/* The decompressor output side may not use these variables. */
	int MCU_width;		/* number of blocks per MCU, horizontally */
	int MCU_height;		/* number of blocks per MCU, vertically */
	int MCU_blocks;		/* MCU_width * MCU_height */
	int MCU_sample_width;	/* MCU width in samples, MCU_width*DCT_scaled_size */
	int last_col_width;	/* # of non-dummy blocks across in last MCU */
	int last_row_height;	/* # of non-dummy blocks down in last MCU */

	/* Saved quantization table for component; NULL if none yet saved.
	 * See jdinput.c comments about the need for this information.
	 * This field is currently used only for decompression.
	 */
	JQUANT_TBL_MTK *quant_table;

	/* Private per-component storage for DCT or IDCT subsystem. */
	void *dct_table;
} jpeg_component_info_MTK;


/* The script for encoding a multiple-scan file is an array of these: */

typedef struct {
	int comps_in_scan;	/* number of components encoded in this scan */
	int component_index[MAX_COMPS_IN_SCAN_MTK];	/* their SOF/comp_info[] indexes */
	int Ss, Se;		/* progressive JPEG spectral selection parms */
	int Ah, Al;		/* progressive JPEG successive approx. parms */
} jpeg_scan_info_MTK;

/* The decompressor can save APPn and COM markers in a list of these: */

typedef struct jpeg_marker_struct_MTK FAR_MTK * jpeg_saved_marker_ptr_MTK;

struct jpeg_marker_struct_MTK {
	jpeg_saved_marker_ptr_MTK next;	/* next in list, or NULL */
	UINT8_MTK marker;		/* marker code: JPEG_COM, or JPEG_APP0+n */
	unsigned int original_length;	/* # bytes of data in the file */
	unsigned int data_length;	/* # bytes of data saved at data[] */
	JOCTET_MTK FAR_MTK *data;	/* the data contained in the marker */
	/* the marker length word is not counted in data_length or original_length */
};

/* Known color spaces. */

typedef enum {
	JCS_UNKNOWN_MTK,		/* error/unspecified */
	JCS_GRAYSCALE_MTK,		/* monochrome */
	JCS_RGB_MTK,		/* red/green/blue */
	JCS_YCbCr_MTK,		/* Y/Cb/Cr (also known as YUV) */
	JCS_CMYK_MTK,		/* C/M/Y/K */
	JCS_YCCK_MTK,		/* Y/Cb/Cr/K */
#ifdef ANDROID_RGB_MTK
	JCS_RGBA_8888_MTK,		/* red/green/blue/alpha */
	JCS_RGB_565_MTK		/* red/green/blue in 565 format */
#endif
} J_COLOR_SPACE_MTK;

/* DCT/IDCT algorithm options. */

typedef enum {
	JDCT_ISLOW_MTK,		/* slow but accurate integer algorithm */
	JDCT_IFAST_MTK,		/* faster, less accurate integer method */
	JDCT_FLOAT_MTK		/* floating-point: accurate, fast on fast HW */
} J_DCT_METHOD_MTK;

#ifndef JDCT_DEFAULT_MTK		/* may be overridden in jconfig.h */
#define JDCT_DEFAULT_MTK  JDCT_ISLOW_MTK
#endif
#ifndef JDCT_FASTEST_MTK		/* may be overridden in jconfig.h */
#define JDCT_FASTEST_MTK  JDCT_IFAST_MTK
#endif

/* Dithering options for decompression. */

typedef enum {
	JDITHER_NONE_MTK,		/* no dithering */
	JDITHER_ORDERED_MTK,	/* simple ordered dither */
	JDITHER_FS_MTK		/* Floyd-Steinberg error diffusion dither */
} J_DITHER_MODE_MTK;

/* Common fields between JPEG compression and decompression master structs. */

#define jpeg_common_fields_MTK \
  struct jpeg_error_mgr_MTK *err;	/* Error handler module */\
  struct jpeg_memory_mgr_MTK *mem;	/* Memory manager module */\
  struct jpeg_progress_mgr_MTK *progress; /* Progress monitor, or NULL if none */\
  void *client_data;		/* Available for use by application */\
  boolean_MTK is_decompressor;	/* So common code can tell which is which */\
  int global_state		/* For checking call sequence validity */

/* Routines that are to be used by both halves of the library are declared
 * to receive a pointer to this structure.  There are no actual instances of
 * jpeg_common_struct, only of jpeg_compress_struct and jpeg_decompress_struct.
 */
struct jpeg_common_struct_MTK {
	jpeg_common_fields_MTK;	/* Fields common to both master struct types */
	/* Additional fields follow in an actual jpeg_compress_struct or
	 * jpeg_decompress_struct.  All three structs must agree on these
	 * initial fields!  (This would be a lot cleaner in C++.)
	 */
};

typedef struct jpeg_common_struct_MTK *j_common_ptr_MTK;
typedef struct jpeg_compress_struct_MTK *j_compress_ptr_MTK;
typedef struct jpeg_decompress_struct_MTK *j_decompress_ptr_MTK;

/* Master record for a compression instance */

struct jpeg_compress_struct_MTK {
	jpeg_common_fields_MTK;	/* Fields shared with jpeg_decompress_struct */

	/* Destination for compressed data */
	struct jpeg_destination_mgr_MTK *dest;

	/* Description of source image --- these fields must be filled in by
	 * outer application before starting compression.  in_color_space must
	 * be correct before you can even call jpeg_set_defaults().
	 */

	JDIMENSION_MTK image_width;	/* input image width */
	JDIMENSION_MTK image_height;	/* input image height */
	int input_components;	/* # of color components in input image */
	J_COLOR_SPACE_MTK in_color_space;	/* colorspace of input image */

	double input_gamma;	/* image gamma of input image */

	/* Compression parameters --- these fields must be set before calling
	 * jpeg_start_compress().  We recommend calling jpeg_set_defaults() to
	 * initialize everything to reasonable defaults, then changing anything
	 * the application specifically wants to change.  That way you won't get
	 * burnt when new parameters are added.  Also note that there are several
	 * helper routines to simplify changing parameters.
	 */

	int data_precision;	/* bits of precision in image data */

	int num_components;	/* # of color components in JPEG image */
	J_COLOR_SPACE_MTK jpeg_color_space;	/* colorspace of JPEG image */

	jpeg_component_info_MTK *comp_info;
	/* comp_info[i] describes component that appears i'th in SOF */

	JQUANT_TBL_MTK *quant_tbl_ptrs[NUM_QUANT_TBLS_MTK];
	/* ptrs to coefficient quantization tables, or NULL if not defined */

	JHUFF_TBL_MTK * dc_huff_tbl_ptrs[NUM_HUFF_TBLS_MTK];
	JHUFF_TBL_MTK * ac_huff_tbl_ptrs[NUM_HUFF_TBLS_MTK];
	/* ptrs to Huffman coding tables, or NULL if not defined */

	UINT8_MTK arith_dc_L[NUM_ARITH_TBLS_MTK];	/* L values for DC arith-coding tables */
	UINT8_MTK arith_dc_U[NUM_ARITH_TBLS_MTK];	/* U values for DC arith-coding tables */
	UINT8_MTK arith_ac_K[NUM_ARITH_TBLS_MTK];	/* Kx values for AC arith-coding tables */

	int num_scans;		/* # of entries in scan_info array */
	const jpeg_scan_info_MTK *scan_info;	/* script for multi-scan file, or NULL */
	/* The default value of scan_info is NULL, which causes a single-scan
	 * sequential JPEG file to be emitted.  To create a multi-scan file,
	 * set num_scans and scan_info to point to an array of scan definitions.
	 */

	boolean_MTK raw_data_in;	/* TRUE=caller supplies downsampled data */
	boolean_MTK arith_code;	/* TRUE=arithmetic coding, FALSE=Huffman */
	boolean_MTK optimize_coding;	/* TRUE=optimize entropy encoding parms */
	boolean_MTK CCIR601_sampling;	/* TRUE=first samples are cosited */
	int smoothing_factor;	/* 1..100, or 0 for no input smoothing */
	J_DCT_METHOD_MTK dct_method;	/* DCT algorithm selector */

	/* The restart interval can be specified in absolute MCUs by setting
	 * restart_interval, or in MCU rows by setting restart_in_rows
	 * (in which case the correct restart_interval will be figured
	 * for each scan).
	 */
	unsigned int restart_interval;	/* MCUs per restart, or 0 for no restart */
	int restart_in_rows;	/* if > 0, MCU rows per restart interval */

	/* Parameters controlling emission of special markers. */

	boolean_MTK write_JFIF_header;	/* should a JFIF marker be written? */
	UINT8_MTK JFIF_major_version;	/* What to write for the JFIF version number */
	UINT8_MTK JFIF_minor_version;
	/* These three values are not used by the JPEG code, merely copied */
	/* into the JFIF APP0 marker.  density_unit can be 0 for unknown, */
	/* 1 for dots/inch, or 2 for dots/cm.  Note that the pixel aspect */
	/* ratio is defined by X_density/Y_density even when density_unit=0. */
	UINT8_MTK density_unit;	/* JFIF code for pixel size units */
	UINT16_MTK X_density;	/* Horizontal pixel density */
	UINT16_MTK Y_density;	/* Vertical pixel density */
	boolean_MTK write_Adobe_marker;	/* should an Adobe marker be written? */

	/* State variable: index of next scanline to be written to
	 * jpeg_write_scanlines().  Application may use this to control its
	 * processing loop, e.g., "while (next_scanline < image_height)".
	 */

	JDIMENSION_MTK next_scanline;	/* 0 .. image_height-1  */

	/* Remaining fields are known throughout compressor, but generally
	 * should not be touched by a surrounding application.
	 */

	/*
	 * These fields are computed during compression startup
	 */
	boolean_MTK progressive_mode;	/* TRUE if scan script uses progressive mode */
	int max_h_samp_factor;	/* largest h_samp_factor */
	int max_v_samp_factor;	/* largest v_samp_factor */

	JDIMENSION_MTK total_iMCU_rows;	/* # of iMCU rows to be input to coef ctlr */
	/* The coefficient controller receives data in units of MCU rows as defined
	 * for fully interleaved scans (whether the JPEG file is interleaved or not).
	 * There are v_samp_factor * DCTSIZE sample rows of each component in an
	 * "iMCU" (interleaved MCU) row.
	 */

	/*
	 * These fields are valid during any one scan.
	 * They describe the components and MCUs actually appearing in the scan.
	 */
	int comps_in_scan;	/* # of JPEG components in this scan */
	jpeg_component_info_MTK *cur_comp_info[MAX_COMPS_IN_SCAN_MTK];
	/* *cur_comp_info[i] describes component that appears i'th in SOS */

	JDIMENSION_MTK MCUs_per_row;	/* # of MCUs across the image */
	JDIMENSION_MTK MCU_rows_in_scan;	/* # of MCU rows in the image */

	int blocks_in_MCU;	/* # of DCT blocks per MCU */
	int MCU_membership[C_MAX_BLOCKS_IN_MCU_MTK];
	/* MCU_membership[i] is index in cur_comp_info of component owning */
	/* i'th block in an MCU */

	int Ss, Se, Ah, Al;	/* progressive JPEG parameters for scan */

	/*
	 * Links to compression subobjects (methods and private variables of modules)
	 */
	struct jpeg_comp_master_MTK *master;
	struct jpeg_c_main_controller_MTK *main;
	struct jpeg_c_prep_controller_MTK *prep;
	struct jpeg_c_coef_controller_MTK *coef;
	struct jpeg_marker_writer_MTK *marker;
	struct jpeg_color_converter_MTK *cconvert;
	struct jpeg_downsampler_MTK *downsample;
	struct jpeg_forward_dct_MTK *fdct;
	struct jpeg_entropy_encoder_MTK *entropy;
	jpeg_scan_info_MTK *script_space;	/* workspace for jpeg_simple_progression */
	int script_space_size;

	int for_huff_opt[MAX_COMPS_IN_SCAN_MTK];
	int last_coffs_zero[C_MAX_BLOCKS_IN_MCU_MTK];
#if 1				/* def JPEG_THREAD_SUPPORT */
	J_ENCODE_SPEED_MTK encode_speed;
	int core_num;
	int thread_num;
	int cur_iMCU_row;

	JSAMPIMAGE_MTK data_in;
	int last_coffs_zero_thread[MAX_CORE_NUM_MTK][C_MAX_BLOCKS_IN_MCU_MTK];
	struct jpeg_destination_mgr_MTK *dest_thread[MAX_CORE_NUM_MTK];
	struct jpeg_output_table_MTK *output_table;

	ThreadContext_t_MTK thrcnxt[MAX_CORE_NUM_MTK];
	Thread_t_MTK pthread[MAX_CORE_NUM_MTK];
	Thread_mutex_t_MTK mutex_input;
	Thread_mutex_t_MTK mutex_state[MAX_CORE_NUM_MTK];
	Thread_cond_t_MTK cond_state[2 * MAX_CORE_NUM_MTK];
	int en_soi;
#endif
};


/* Master record for a decompression instance */

struct jpeg_decompress_struct_MTK {
	jpeg_common_fields_MTK;	/* Fields shared with jpeg_compress_struct */

	/* Source of compressed data */
	struct jpeg_source_mgr_MTK *src;

	/* Basic description of image --- filled in by jpeg_read_header(). */
	/* Application may inspect these values to decide how to process image. */

	JDIMENSION_MTK original_image_width;	/* nominal image width (from SOF marker) */

	JDIMENSION_MTK image_width;	/* nominal image width (from SOF marker)
				   may be changed by tile decode */
	JDIMENSION_MTK image_height;	/* nominal image height */
	int num_components;	/* # of color components in JPEG image */
	J_COLOR_SPACE_MTK jpeg_color_space;	/* colorspace of JPEG image */

	/* Decompression processing parameters --- these fields must be set before
	 * calling jpeg_start_decompress().  Note that jpeg_read_header() initializes
	 * them to default values.
	 */

	J_COLOR_SPACE_MTK out_color_space;	/* colorspace for output */

	unsigned int scale_num, scale_denom;	/* fraction by which to scale image */

	double output_gamma;	/* image gamma wanted in output */

	boolean_MTK buffered_image;	/* TRUE=multiple output passes */
	boolean_MTK raw_data_out;	/* TRUE=downsampled data wanted */

	J_DCT_METHOD_MTK dct_method;	/* IDCT algorithm selector */
	boolean_MTK do_fancy_upsampling;	/* TRUE=apply fancy upsampling */
	boolean_MTK do_block_smoothing;	/* TRUE=apply interblock smoothing */

	boolean_MTK quantize_colors;	/* TRUE=colormapped output wanted */
	/* the following are ignored if not quantize_colors: */
	J_DITHER_MODE_MTK dither_mode;	/* type of color dithering to use */
	boolean_MTK two_pass_quantize;	/* TRUE=use two-pass color quantization */
	int desired_number_of_colors;	/* max # colors to use in created colormap */
	/* these are significant only in buffered-image mode: */
	boolean_MTK enable_1pass_quant;	/* enable future use of 1-pass quantizer */
	boolean_MTK enable_external_quant;	/* enable future use of external colormap */
	boolean_MTK enable_2pass_quant;	/* enable future use of 2-pass quantizer */

	/* Description of actual output image that will be returned to application.
	 * These fields are computed by jpeg_start_decompress().
	 * You can also use jpeg_calc_output_dimensions() to determine these values
	 * in advance of calling jpeg_start_decompress().
	 */

	JDIMENSION_MTK output_width;	/* scaled image width */
	JDIMENSION_MTK output_height;	/* scaled image height */
	int out_color_components;	/* # of color components in out_color_space */
	int output_components;	/* # of color components returned */
	/* output_components is 1 (a colormap index) when quantizing colors;
	 * otherwise it equals out_color_components.
	 */
	int rec_outbuf_height;	/* min recommended height of scanline buffer */
	/* If the buffer passed to jpeg_read_scanlines() is less than this many rows
	 * high, space and time will be wasted due to unnecessary data copying.
	 * Usually rec_outbuf_height will be 1 or 2, at most 4.
	 */

	/* When quantizing colors, the output colormap is described by these fields.
	 * The application can supply a colormap by setting colormap non-NULL before
	 * calling jpeg_start_decompress; otherwise a colormap is created during
	 * jpeg_start_decompress or jpeg_start_output.
	 * The map has out_color_components rows and actual_number_of_colors columns.
	 */
	int actual_number_of_colors;	/* number of entries in use */
	JSAMPARRAY_MTK colormap;	/* The color map as a 2-D pixel array */

	/* State variables: these variables indicate the progress of decompression.
	 * The application may examine these but must not modify them.
	 */

	/* Row index of next scanline to be read from jpeg_read_scanlines().
	 * Application may use this to control its processing loop, e.g.,
	 * "while (output_scanline < output_height)".
	 */
	JDIMENSION_MTK output_scanline;	/* 0 .. output_height-1  */

	/* Current input scan number and number of iMCU rows completed in scan.
	 * These indicate the progress of the decompressor input side.
	 */
	int input_scan_number;	/* Number of SOS markers seen so far */
	JDIMENSION_MTK input_iMCU_row;	/* Number of iMCU rows completed */

	/* The "output scan number" is the notional scan being displayed by the
	 * output side.  The decompressor will not allow output scan/row number
	 * to get ahead of input scan/row, but it can fall arbitrarily far behind.
	 */
	int output_scan_number;	/* Nominal scan number being displayed */
	JDIMENSION_MTK output_iMCU_row;	/* Number of iMCU rows read */

	/* Current progression status.  coef_bits[c][i] indicates the precision
	 * with which component c's DCT coefficient i (in zigzag order) is known.
	 * It is -1 when no data has yet been received, otherwise it is the point
	 * transform (shift) value for the most recent scan of the coefficient
	 * (thus, 0 at completion of the progression).
	 * This pointer is NULL when reading a non-progressive file.
	 */
	int (*coef_bits)[DCTSIZE2_MTK];	/* -1 or current Al value for each coef */

	/* Internal JPEG parameters --- the application usually need not look at
	 * these fields.  Note that the decompressor output side may not use
	 * any parameters that can change between scans.
	 */

	/* Quantization and Huffman tables are carried forward across input
	 * datastreams when processing abbreviated JPEG datastreams.
	 */

	JQUANT_TBL_MTK *quant_tbl_ptrs[NUM_QUANT_TBLS_MTK];
	/* ptrs to coefficient quantization tables, or NULL if not defined */

	JHUFF_TBL_MTK * dc_huff_tbl_ptrs[NUM_HUFF_TBLS_MTK];
	JHUFF_TBL_MTK * ac_huff_tbl_ptrs[NUM_HUFF_TBLS_MTK];
	/* ptrs to Huffman coding tables, or NULL if not defined */

	/* These parameters are never carried across datastreams, since they
	 * are given in SOF/SOS markers or defined to be reset by SOI.
	 */

	int data_precision;	/* bits of precision in image data */

	jpeg_component_info_MTK *comp_info;
	/* comp_info[i] describes component that appears i'th in SOF */

	boolean_MTK tile_decode;	/* TRUE if using tile based decoding */
	boolean_MTK progressive_mode;	/* TRUE if SOFn specifies progressive mode */
	boolean_MTK arith_code;	/* TRUE=arithmetic coding, FALSE=Huffman */

	UINT8_MTK arith_dc_L[NUM_ARITH_TBLS_MTK];	/* L values for DC arith-coding tables */
	UINT8_MTK arith_dc_U[NUM_ARITH_TBLS_MTK];	/* U values for DC arith-coding tables */
	UINT8_MTK arith_ac_K[NUM_ARITH_TBLS_MTK];	/* Kx values for AC arith-coding tables */

	unsigned int restart_interval;	/* MCUs per restart interval, or 0 for no restart */

	/* These fields record data obtained from optional markers recognized by
	 * the JPEG library.
	 */
	boolean_MTK saw_JFIF_marker;	/* TRUE iff a JFIF APP0 marker was found */
	/* Data copied from JFIF marker; only valid if saw_JFIF_marker is TRUE: */
	UINT8_MTK JFIF_major_version;	/* JFIF version number */
	UINT8_MTK JFIF_minor_version;
	UINT8_MTK density_unit;	/* JFIF code for pixel size units */
	UINT16_MTK X_density;	/* Horizontal pixel density */
	UINT16_MTK Y_density;	/* Vertical pixel density */
	boolean_MTK saw_Adobe_marker;	/* TRUE iff an Adobe APP14 marker was found */
	UINT8_MTK Adobe_transform;	/* Color transform code from Adobe marker */

	boolean_MTK CCIR601_sampling;	/* TRUE=first samples are cosited */

	/* Aside from the specific data retained from APPn markers known to the
	 * library, the uninterpreted contents of any or all APPn and COM markers
	 * can be saved in a list for examination by the application.
	 */
	jpeg_saved_marker_ptr_MTK marker_list;	/* Head of list of saved markers */

	/* Remaining fields are known throughout decompressor, but generally
	 * should not be touched by a surrounding application.
	 */

	/*
	 * These fields are computed during decompression startup
	 */
	int max_h_samp_factor;	/* largest h_samp_factor */
	int max_v_samp_factor;	/* largest v_samp_factor */

	int min_DCT_scaled_size;	/* smallest DCT_scaled_size of any component */

	JDIMENSION_MTK total_iMCU_rows;	/* # of iMCU rows in image */
	/* The coefficient controller's input and output progress is measured in
	 * units of "iMCU" (interleaved MCU) rows.  These are the same as MCU rows
	 * in fully interleaved JPEG scans, but are used whether the scan is
	 * interleaved or not.  We define an iMCU row as v_samp_factor DCT block
	 * rows of each component.  Therefore, the IDCT output contains
	 * v_samp_factor*DCT_scaled_size sample rows of a component per iMCU row.
	 */

	JSAMPLE_MTK *sample_range_limit;	/* table for fast range-limiting */

	/*
	 * These fields are valid during any one scan.
	 * They describe the components and MCUs actually appearing in the scan.
	 * Note that the decompressor output side must not use these fields.
	 */
	int comps_in_scan;	/* # of JPEG components in this scan */
	jpeg_component_info_MTK *cur_comp_info[MAX_COMPS_IN_SCAN_MTK];
	/* *cur_comp_info[i] describes component that appears i'th in SOS */

	JDIMENSION_MTK MCUs_per_row;	/* # of MCUs across the image */
	JDIMENSION_MTK MCU_rows_in_scan;	/* # of MCU rows in the image */

	int blocks_in_MCU;	/* # of DCT blocks per MCU */
	int MCU_membership[D_MAX_BLOCKS_IN_MCU_MTK];
	/* MCU_membership[i] is index in cur_comp_info of component owning */
	/* i'th block in an MCU */

	int Ss, Se, Ah, Al;	/* progressive JPEG parameters for scan */

	/* This field is shared between entropy decoder and marker parser.
	 * It is either zero or the code of a JPEG marker that has been
	 * read from the data source, but has not yet been processed.
	 */
	int unread_marker;
#if 1				/* def USE_SW_JPEG_3PLANE */
	int isUseMtk3plane;
	int isUseIdctBuffer;
	int isUseUpsampleBuffer;
	int comp_id;
	int yindex;
	unsigned char *dstBufAddr[3];
	unsigned int dstBufStride[3];
	unsigned int dstBufHeight[3];
	unsigned int dstBufSize[3];

	unsigned char *upSampleBufAddr[3];
	unsigned char *idctBufAddr[3];
	unsigned int idctBufStride[3];
	unsigned int idctBufSize[3];
	unsigned int idctBufHeight[3];
	unsigned int idctBufBlkCnt[3];
	unsigned int idctBufRowNum;
	unsigned int idctBufRowSize[3];
	unsigned char *idctBufRowAddrY0[3][16];
	unsigned char *idctBufRowAddrY1[3][16];
#endif
	/*
	 * Links to decompression subobjects (methods, private variables of modules)
	 */
	struct jpeg_decomp_master_MTK *master;
	struct jpeg_d_main_controller_MTK *main;
	struct jpeg_d_coef_controller_MTK *coef;
	struct jpeg_d_post_controller_MTK *post;
	struct jpeg_input_controller_MTK *inputctl;
	struct jpeg_marker_reader_MTK *marker;
	struct jpeg_entropy_decoder_MTK *entropy;
	struct jpeg_inverse_dct_MTK *idct;
	struct jpeg_upsampler_MTK *upsample;
	struct jpeg_color_deconverter_MTK *cconvert;
	struct jpeg_color_quantizer_MTK *cquantize;
};

typedef struct {

	/* |--- byte_offset ---|- bit_left -| */
	/* \------ 27 -------/ \---- 5 ----/ */
	unsigned int bitstream_offset;
	short prev_dc[3];

	/* remaining EOBs in EOBRUN */
	unsigned short EOBRUN;

	/* save the decoder current bit buffer, entropy->bitstate.get_buffer. */
	INT32_MTK get_buffer;

	/* save the restart info. */
	unsigned short restarts_to_go;
	unsigned char next_restart_num;
	boolean_MTK insufficient_data;	/* set TRUE after emitting warning */
	int unread_marker;
} huffman_offset_data_MTK;

typedef struct {

	/* The header starting position of this scan */
	unsigned int bitstream_offset;

	/* Number of components in this scan */
	int comps_in_scan;

	/* Number of MCUs in each row */
	int MCUs_per_row;
	int MCU_rows_per_iMCU_row;

	/* The last MCU position and its dc value in this scan */
	huffman_offset_data_MTK prev_MCU_offset;

	huffman_offset_data_MTK **offset;
} huffman_scan_header_MTK;

#define DEFAULT_MCU_SAMPLE_SIZE_MTK 16

typedef struct {

	/* The number of MCUs that we sample each time as an index point */
	int MCU_sample_size;

	/* Number of scan in this image */
	int scan_count;

	/* Number of iMCUs rows in this image */
	int total_iMCU_rows;

	/* Memory used by scan struct */
	size_t mem_used;
	huffman_scan_header_MTK *scan;
} huffman_index_MTK;

/* "Object" declarations for JPEG modules that may be supplied or called
 * directly by the surrounding application.
 * As with all objects in the JPEG library, these structs only define the
 * publicly visible methods and state variables of a module.  Additional
 * private fields may exist after the public ones.
 */


/* Error handler object */

struct jpeg_error_mgr_MTK {
	/* Error exit handler: does not return to caller */
	JMETHOD_MTK(void, error_exit, (j_common_ptr_MTK cinfo));
	/* Conditionally emit a trace or warning message */
	 JMETHOD_MTK(void, emit_message, (j_common_ptr_MTK cinfo, int msg_level));
	/* Routine that actually outputs a trace or error message */
	 JMETHOD_MTK(void, output_message, (j_common_ptr_MTK cinfo));
	/* Format a message string for the most recent JPEG error or message */
	 JMETHOD_MTK(void, format_message, (j_common_ptr_MTK cinfo, char *buffer));
#define JMSG_LENGTH_MAX_MTK  200	/* recommended size of format_message buffer */
	/* Reset error state variables at start of a new image */
	 JMETHOD_MTK(void, reset_error_mgr, (j_common_ptr_MTK cinfo));

	/* The message ID code and any parameters are saved here.
	 * A message can have one string parameter or up to 8 int parameters.
	 */
	int msg_code;
#define JMSG_STR_PARM_MAX_MTK  80
	union {
		int i[8];
		char s[JMSG_STR_PARM_MAX_MTK];
	} msg_parm;

	/* Standard state variables for error facility */

	int trace_level;	/* max msg_level that will be displayed */

	/* For recoverable corrupt-data errors, we emit a warning message,
	 * but keep going unless emit_message chooses to abort.  emit_message
	 * should count warnings in num_warnings.  The surrounding application
	 * can check for bad data by seeing if num_warnings is nonzero at the
	 * end of processing.
	 */
	long num_warnings;	/* number of corrupt-data warnings */

	/* These fields point to the table(s) of error message strings.
	 * An application can change the table pointer to switch to a different
	 * message list (typically, to change the language in which errors are
	 * reported).  Some applications may wish to add additional error codes
	 * that will be handled by the JPEG library error mechanism; the second
	 * table pointer is used for this purpose.
	 *
	 * First table includes all errors generated by JPEG library itself.
	 * Error code 0 is reserved for a "no such error string" message.
	 */
	const char *const *jpeg_message_table;	/* Library errors */
	int last_jpeg_message;	/* Table contains strings 0..last_jpeg_message */
	/* Second table can be added by application (see cjpeg/djpeg for example).
	 * It contains strings numbered first_addon_message..last_addon_message.
	 */
	const char *const *addon_message_table;	/* Non-library errors */
	int first_addon_message;	/* code for first string in addon table */
	int last_addon_message;	/* code for last string in addon table */
};


/* Progress monitor object */

struct jpeg_progress_mgr_MTK {
	JMETHOD_MTK(void, progress_monitor, (j_common_ptr_MTK cinfo));

	long pass_counter;	/* work units completed in this pass */
	long pass_limit;	/* total number of work units in this pass */
	int completed_passes;	/* passes completed so far */
	int total_passes;	/* total number of passes expected */
};


/* Data destination object for compression */

struct jpeg_destination_mgr_MTK {
	JOCTET_MTK *next_output_byte;	/* => next byte to write in buffer */
	size_t free_in_buffer;	/* # of byte spaces remaining in buffer */

	 JMETHOD_MTK(void, init_destination, (j_compress_ptr_MTK cinfo));
	 JMETHOD_MTK(boolean_MTK, empty_output_buffer, (j_compress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, term_destination, (j_compress_ptr_MTK cinfo));
#if 1				/* def JPEG_THREAD_SUPPORT */
	 JMETHOD_MTK(boolean_MTK, output_buffer_thread,
		 (j_compress_ptr_MTK cinfo, JOCTET_MTK *output_addr, int output_size));
	 JMETHOD_MTK(JOCTET_MTK *, memcpy_buffer_thread,
		 (j_compress_ptr_MTK cinfo, JOCTET_MTK *last_addr, int index));
#endif
};


/* Data source object for decompression */

struct jpeg_source_mgr_MTK {
	const JOCTET_MTK *next_input_byte;	/* => next byte to read from buffer */
	const JOCTET_MTK *start_input_byte;	/* => first byte to read from input */
	size_t bytes_in_buffer;	/* # of bytes remaining in buffer */
	size_t current_offset;	/* current readed input offset */

	 JMETHOD_MTK(void, init_source, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(boolean_MTK, fill_input_buffer, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, skip_input_data, (j_decompress_ptr_MTK cinfo, long num_bytes));
	 JMETHOD_MTK(boolean_MTK, resync_to_restart, (j_decompress_ptr_MTK cinfo, int desired));
	 JMETHOD_MTK(void, term_source, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(boolean_MTK, seek_input_data, (j_decompress_ptr_MTK cinfo, long byte_offset));
};


/* Memory manager object.
 * Allocates "small" objects (a few K total), "large" objects (tens of K),
 * and "really big" objects (virtual arrays with backing store if needed).
 * The memory manager does not allow individual objects to be freed; rather,
 * each created object is assigned to a pool, and whole pools can be freed
 * at once.  This is faster and more convenient than remembering exactly what
 * to free, especially where malloc()/free() are not too speedy.
 * NB: alloc routines never return NULL.  They exit to error_exit if not
 * successful.
 */

#define JPOOL_PERMANENT_MTK	0	/* lasts until master record is destroyed */
#define JPOOL_IMAGE_MTK	1	/* lasts until done with image/datastream */
#define JPOOL_NUMPOOLS_MTK	2

typedef struct jvirt_sarray_control_MTK *jvirt_sarray_ptr_MTK;
typedef struct jvirt_barray_control_MTK *jvirt_barray_ptr_MTK;


struct jpeg_memory_mgr_MTK {
	/* Method pointers */
	JMETHOD_MTK(void *, alloc_small, (j_common_ptr_MTK cinfo, int pool_id, size_t sizeofobject));
	 JMETHOD_MTK(void FAR_MTK *, alloc_large, (j_common_ptr_MTK cinfo, int pool_id, size_t sizeofobject));
	 JMETHOD_MTK(JSAMPARRAY_MTK, alloc_sarray, (j_common_ptr_MTK cinfo, int pool_id,
					    JDIMENSION_MTK samplesperrow, JDIMENSION_MTK numrows));
	 JMETHOD_MTK(JBLOCKARRAY_MTK, alloc_barray, (j_common_ptr_MTK cinfo, int pool_id,
					     JDIMENSION_MTK blocksperrow, JDIMENSION_MTK numrows));
	 JMETHOD_MTK(jvirt_sarray_ptr_MTK, request_virt_sarray, (j_common_ptr_MTK cinfo,
							 int pool_id,
							 boolean_MTK pre_zero,
							 JDIMENSION_MTK samplesperrow,
							 JDIMENSION_MTK numrows, JDIMENSION_MTK maxaccess));
	 JMETHOD_MTK(jvirt_barray_ptr_MTK, request_virt_barray, (j_common_ptr_MTK cinfo,
							 int pool_id,
							 boolean_MTK pre_zero,
							 JDIMENSION_MTK blocksperrow,
							 JDIMENSION_MTK numrows, JDIMENSION_MTK maxaccess));
	 JMETHOD_MTK(void, realize_virt_arrays, (j_common_ptr_MTK cinfo));
	 JMETHOD_MTK(JSAMPARRAY_MTK, access_virt_sarray, (j_common_ptr_MTK cinfo,
						  jvirt_sarray_ptr_MTK ptr,
						  JDIMENSION_MTK start_row,
						  JDIMENSION_MTK num_rows, boolean_MTK writable));
	 JMETHOD_MTK(JBLOCKARRAY_MTK, access_virt_barray, (j_common_ptr_MTK cinfo,
						   jvirt_barray_ptr_MTK ptr,
						   JDIMENSION_MTK start_row,
						   JDIMENSION_MTK num_rows, boolean_MTK writable));
	 JMETHOD_MTK(void, free_pool, (j_common_ptr_MTK cinfo, int pool_id));
	 JMETHOD_MTK(void, self_destruct, (j_common_ptr_MTK cinfo));

	/* Limit on memory allocation for this JPEG object.  (Note that this is
	 * merely advisory, not a guaranteed maximum; it only affects the space
	 * used for virtual-array buffers.)  May be changed by outer application
	 * after creating the JPEG object.
	 */
	long max_memory_to_use;

	/* Maximum allocation request accepted by alloc_large. */
	long max_alloc_chunk;
};


/* Routine signature for application-supplied marker processing methods.
 * Need not pass marker code since it is stored in cinfo->unread_marker.
 */
typedef JMETHOD_MTK(boolean_MTK, jpeg_marker_parser_method_MTK, (j_decompress_ptr_MTK cinfo));


/* Declarations for routines called by application.
 * The JPP macro hides prototype parameters from compilers that can't cope.
 * Note JPP requires double parentheses.
 */

#ifdef HAVE_PROTOTYPES_MTK
#define JPP_MTK(arglist)	arglist
#else
#define JPP_MTK(arglist)	()
#endif


/* Short forms of external names for systems with brain-damaged linkers.
 * We shorten external names to be unique in the first six letters, which
 * is good enough for all known systems.
 * (If your compiler itself needs names to be unique in less than 15
 * characters, you are out of luck.  Get a better compiler.)
 */

#ifdef NEED_SHORT_EXTERNAL_NAMES_MTK
#define jpeg_std_error_MTK		jStdError
#define jpeg_CreateCompress_MTK	jCreaCompress
#define jpeg_CreateDecompress_MTK	jCreaDecompress
#define jpeg_destroy_compress_MTK	jDestCompress
#define jpeg_destroy_decompress_MTK	jDestDecompress
#define jpeg_stdio_dest_MTK		jStdDest
#define jpeg_stdio_src_MTK		jStdSrc
#define jpeg_mem_dest_MTK		jMemDest
#define jpeg_mem_src_MTK		jMemSrc
#define jpeg_set_defaults_MTK	jSetDefaults
#define jpeg_set_colorspace_MTK	jSetColorspace
#define jpeg_default_colorspace_MTK	jDefColorspace
#define jpeg_set_quality_MTK	jSetQuality
#define jpeg_set_linear_quality_MTK	jSetLQuality
#define jpeg_add_quant_table_MTK	jAddQuantTable
#define jpeg_quality_scaling_MTK	jQualityScaling
#define jpeg_simple_progression_MTK	jSimProgress
#define jpeg_suppress_tables_MTK	jSuppressTables
#define jpeg_alloc_quant_table_MTK	jAlcQTable
#define jpeg_alloc_huff_table_MTK	jAlcHTable
#define jpeg_start_compress_MTK	jStrtCompress
#define jpeg_write_scanlines_MTK	jWrtScanlines
#define jpeg_finish_compress_MTK	jFinCompress
#define jpeg_write_raw_data_MTK	jWrtRawData
#define jpeg_write_marker_MTK	jWrtMarker
#define jpeg_write_m_header_MTK	jWrtMHeader
#define jpeg_write_m_byte_MTK	jWrtMByte
#define jpeg_write_tables_MTK	jWrtTables
#define jpeg_read_header_MTK	jReadHeader
#define jpeg_start_decompress_MTK	jStrtDecompress
#define jpeg_read_scanlines_MTK	jReadScanlines
#define jpeg_finish_decompress_MTK	jFinDecompress
#define jpeg_read_raw_data_MTK	jReadRawData
#define jpeg_has_multiple_scans_MTK	jHasMultScn
#define jpeg_start_output_MTK	jStrtOutput
#define jpeg_finish_output_MTK	jFinOutput
#define jpeg_input_complete_MTK	jInComplete
#define jpeg_new_colormap_MTK	jNewCMap
#define jpeg_consume_input_MTK	jConsumeInput
#define jpeg_calc_output_dimensions_MTK	jCalcDimensions
#define jpeg_save_markers_MTK	jSaveMarkers
#define jpeg_set_marker_processor_MTK	jSetMarker
#define jpeg_read_coefficients_MTK	jReadCoefs
#define jpeg_write_coefficients_MTK	jWrtCoefs
#define jpeg_copy_critical_parameters_MTK	jCopyCrit
#define jpeg_abort_compress_MTK	jAbrtCompress
#define jpeg_abort_decompress_MTK	jAbrtDecompress
#define jpeg_abort_MTK		jAbort
#define jpeg_destroy_MTK		jDestroy
#define jpeg_resync_to_restart_MTK	jResyncRestart
#endif				/* NEED_SHORT_EXTERNAL_NAMES */

#define jpeg_std_error_MTK(err) \
    jpeg_std_error_MTK(err)

/* Default error-management setup */
EXTERN_MTK(struct jpeg_error_mgr_MTK *) jpeg_std_error_MTK JPP_MTK((struct jpeg_error_mgr_MTK *err));

/* Initialization of JPEG compression objects.
 * jpeg_create_compress() and jpeg_create_decompress() are the exported
 * names that applications should call.  These expand to calls on
 * jpeg_CreateCompress and jpeg_CreateDecompress with additional information
 * passed for version mismatch checking.
 * NB: you must set up the error-manager BEFORE calling jpeg_create_xxx.
 */
#define jpeg_create_compress_MTK(cinfo) \
    jpeg_CreateCompress_MTK((cinfo), JPEG_LIB_VERSION_MTK, \
			(size_t) sizeof(struct jpeg_compress_struct_MTK))
#define jpeg_create_decompress_MTK(cinfo) \
    jpeg_CreateDecompress_MTK((cinfo), JPEG_LIB_VERSION_MTK, \
			  (size_t) sizeof(struct jpeg_decompress_struct_MTK))
#define jpeg_destroy_compress_MTK(cinfo) \
    jpeg_destroy_compress_MTK(cinfo)
#define jpeg_destroy_decompress_MTK(cinfo) \
    jpeg_destroy_decompress_MTK(cinfo)
#define jpeg_mem_dest_MTK(cinfo, outbuffer, outsize) \
    jpeg_mem_dest_MTK(cinfo, outbuffer, outsize)
#define jpeg_set_defaults_MTK(cinfo) \
    jpeg_set_defaults_MTK(cinfo)
#define jpeg_default_colorspace_MTK(cinfo) \
    jpeg_default_colorspace_MTK(cinfo)
#define jpeg_set_quality_MTK(cinfo, quality, force_baseline) \
    jpeg_set_quality_MTK(cinfo, quality, force_baseline)
#define jpeg_alloc_huff_table_MTK(cinfo) \
    jpeg_alloc_huff_table_MTK(cinfo)
#define jpeg_start_compress_MTK(cinfo, write_all_tables) \
    jpeg_start_compress_MTK(cinfo, write_all_tables)
#define jpeg_finish_compress_MTK(cinfo) \
    jpeg_finish_compress_MTK(cinfo)
#define jpeg_write_raw_data_MTK(cinfo, data, num_lines) \
    jpeg_write_raw_data_MTK(cinfo, data, num_lines)
#define jpeg_write_tables_MTK(cinfo) \
    jpeg_write_tables_MTK(cinfo)
#define jpeg_read_header_MTK(cinfo, require_image) \
    jpeg_read_header_MTK(cinfo, require_image)
#define jpeg_start_decompress_MTK(cinfo) \
    jpeg_start_decompress_MTK(cinfo)
#define jpeg_start_tile_decompress_MTK(cinfo) \
    jpeg_start_tile_decompress_MTK(cinfo)
#define jpeg_read_scanlines_MTK(cinfo, scanlines, max_lines) \
    jpeg_read_scanlines_MTK(cinfo, scanlines, max_lines)
#define jpeg_read_coefficients_MTK(cinfo) \
    jpeg_read_coefficients_MTK(cinfo)
#define jpeg_build_huffman_index_MTK(cinfo, index) \
    jpeg_build_huffman_index_MTK(cinfo, index)
#define jpeg_init_read_tile_scanline_MTK(cinfo, index, start_x, start_y, width, height) \
    jpeg_init_read_tile_scanline_MTK(cinfo, index, start_x, start_y, width, height)
#define jpeg_finish_decompress_MTK(cinfo) \
    jpeg_finish_decompress_MTK(cinfo)
#define jpeg_consume_input_MTK(cinfo) \
    jpeg_consume_input_MTK(cinfo)
#define jpeg_calc_output_dimensions_MTK(cinfo) \
    jpeg_calc_output_dimensions_MTK(cinfo)
#define jpeg_read_header_MTK(cinfo, require_image) \
    jpeg_read_header_MTK(cinfo, require_image)
#define jpeg_configure_huffman_decoder_MTK(cinfo, offset) \
    jpeg_configure_huffman_decoder_MTK(cinfo, offset)
#define jpeg_get_huffman_decoder_configuration_MTK(cinfo, offset) \
    jpeg_get_huffman_decoder_configuration_MTK(cinfo, offset)
#define jpeg_create_huffman_index_MTK(cinfo, index) \
    jpeg_create_huffman_index_MTK(cinfo, index)
#define jpeg_configure_huffman_index_scan_MTK(cinfo, index, scan_no, offset) \
    jpeg_configure_huffman_index_scan_MTK(cinfo, index, scan_no, offset)
#define jpeg_destroy_huffman_index_MTK(index) \
    jpeg_destroy_huffman_index_MTK(index)

EXTERN_MTK(void)
jpeg_CreateCompress_MTK JPP_MTK((j_compress_ptr_MTK cinfo, int version, size_t structsize));
EXTERN_MTK(void)
jpeg_CreateDecompress_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, int version, size_t structsize));
/* Destruction of JPEG compression objects */
EXTERN_MTK(void)
jpeg_destroy_compress_MTK JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(void)
jpeg_destroy_decompress_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));

/* Standard data source and destination managers: stdio streams. */
/* Caller is responsible for opening the file before and closing after. */
EXTERN_MTK(void)
jpeg_stdio_dest_MTK JPP_MTK((j_compress_ptr_MTK cinfo, FILE * outfile));
EXTERN_MTK(void)
jpeg_stdio_src_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, FILE * infile));

/* Data source and destination managers: memory buffers. */
EXTERN_MTK(void)
jpeg_mem_dest_MTK JPP_MTK((j_compress_ptr_MTK cinfo, unsigned char **outbuffer, unsigned long *outsize));
EXTERN_MTK(void)
jpeg_mem_src_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, unsigned char *inbuffer, unsigned long insize));

/* Default parameter setup for compression */
EXTERN_MTK(void)
jpeg_set_defaults_MTK JPP_MTK((j_compress_ptr_MTK cinfo));
/* Compression parameter setup aids */
EXTERN_MTK(void)
jpeg_set_colorspace_MTK JPP_MTK((j_compress_ptr_MTK cinfo, J_COLOR_SPACE_MTK colorspace));
EXTERN_MTK(void)
jpeg_default_colorspace_MTK JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(void)
jpeg_set_quality_MTK JPP_MTK((j_compress_ptr_MTK cinfo, int quality, boolean_MTK force_baseline));
EXTERN_MTK(void)
jpeg_set_linear_quality_MTK JPP_MTK((j_compress_ptr_MTK cinfo, int scale_factor, boolean_MTK force_baseline));
EXTERN_MTK(void)
jpeg_add_quant_table_MTK JPP_MTK((j_compress_ptr_MTK cinfo, int which_tbl,
			  const unsigned int *basic_table,
			  int scale_factor, boolean_MTK force_baseline));
EXTERN_MTK(int)
jpeg_quality_scaling_MTK JPP_MTK((int quality));
EXTERN_MTK(void)
jpeg_simple_progression_MTK JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(void)
jpeg_suppress_tables_MTK JPP_MTK((j_compress_ptr_MTK cinfo, boolean_MTK suppress));
EXTERN_MTK(JQUANT_TBL_MTK *)
jpeg_alloc_quant_table_MTK JPP_MTK((j_common_ptr_MTK cinfo));
EXTERN_MTK(JHUFF_TBL_MTK *)
jpeg_alloc_huff_table_MTK JPP_MTK((j_common_ptr_MTK cinfo));

/* Main entry points for compression */
EXTERN_MTK(void)
jpeg_start_compress_MTK JPP_MTK((j_compress_ptr_MTK cinfo, boolean_MTK write_all_tables));
EXTERN_MTK(JDIMENSION_MTK)
jpeg_write_scanlines_MTK JPP_MTK((j_compress_ptr_MTK cinfo, JSAMPARRAY_MTK scanlines, JDIMENSION_MTK num_lines));
EXTERN_MTK(void)
jpeg_finish_compress_MTK JPP_MTK((j_compress_ptr_MTK cinfo));

/* Replaces jpeg_write_scanlines when writing raw downsampled data. */
EXTERN_MTK(JDIMENSION_MTK)
jpeg_write_raw_data_MTK JPP_MTK((j_compress_ptr_MTK cinfo, JSAMPIMAGE_MTK data, JDIMENSION_MTK num_lines));

/* Write a special marker.  See libjpeg.doc concerning safe usage. */
EXTERN_MTK(void) jpeg_write_marker_MTK
JPP_MTK((j_compress_ptr_MTK cinfo, int marker, const JOCTET_MTK *dataptr, unsigned int datalen));
/* Same, but piecemeal. */
EXTERN_MTK(void) jpeg_write_m_header_MTK JPP_MTK((j_compress_ptr_MTK cinfo, int marker, unsigned int datalen));
EXTERN_MTK(void) jpeg_write_m_byte_MTK JPP_MTK((j_compress_ptr_MTK cinfo, int val));

/* Alternate compression function: just write an abbreviated table file */
EXTERN_MTK(void)
jpeg_write_tables_MTK JPP_MTK((j_compress_ptr_MTK cinfo));

/* Decompression startup: read start of JPEG datastream to see what's there */
EXTERN_MTK(int)
jpeg_read_header_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, boolean_MTK require_image));
/* Return value is one of: */
#define JPEG_SUSPENDED_MTK		0	/* Suspended due to lack of input data */
#define JPEG_HEADER_OK_MTK		1	/* Found valid image datastream */
#define JPEG_HEADER_TABLES_ONLY_MTK	2	/* Found valid table-specs-only datastream */
/* If you pass require_image = TRUE (normal case), you need not check for
 * a TABLES_ONLY return code; an abbreviated file will cause an error exit.
 * JPEG_SUSPENDED is only possible if you use a data source module that can
 * give a suspension return (the stdio source module doesn't).
 */

/* Main entry points for decompression */
EXTERN_MTK(boolean_MTK)
jpeg_start_decompress_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(boolean_MTK)
jpeg_start_tile_decompress_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(JDIMENSION_MTK)
jpeg_read_scanlines_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, JSAMPARRAY_MTK scanlines, JDIMENSION_MTK max_lines));
EXTERN_MTK(JDIMENSION_MTK)
jpeg_read_scanlines_from_MTK JPP_MTK((j_decompress_ptr_MTK cinfo,
			      JSAMPARRAY_MTK scanlines, int line_offset, JDIMENSION_MTK max_lines));
EXTERN_MTK(JDIMENSION_MTK)
jpeg_read_tile_scanline_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, huffman_index_MTK * index, JSAMPARRAY_MTK scanlines));
EXTERN_MTK(void)
jpeg_init_read_tile_scanline_MTK JPP_MTK((j_decompress_ptr_MTK cinfo,
				  huffman_index_MTK * index,
				  int *start_x, int *start_y, int *width, int *height));
EXTERN_MTK(boolean_MTK)
jpeg_finish_decompress_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));

/* Replaces jpeg_read_scanlines when reading raw downsampled data. */
EXTERN_MTK(JDIMENSION_MTK)
jpeg_read_raw_data_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, JSAMPIMAGE_MTK data, JDIMENSION_MTK max_lines));

/* Additional entry points for buffered-image mode. */
EXTERN_MTK(boolean_MTK)
jpeg_has_multiple_scans_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(boolean_MTK)
jpeg_start_output_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, int scan_number));
EXTERN_MTK(boolean_MTK)
jpeg_finish_output_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(boolean_MTK)
jpeg_input_complete_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void)
jpeg_new_colormap_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(int)
jpeg_consume_input_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
/* Return value is one of: */
/* #define JPEG_SUSPENDED	0    Suspended due to lack of input data */
#define JPEG_REACHED_SOS_MTK	1	/* Reached start of new scan */
#define JPEG_REACHED_EOI_MTK	2	/* Reached end of image */
#define JPEG_ROW_COMPLETED_MTK	3	/* Completed one iMCU row */
#define JPEG_SCAN_COMPLETED_MTK	4	/* Completed last iMCU row of a scan */

/* Precalculate output dimensions for current decompression parameters. */
EXTERN_MTK(void)
jpeg_calc_output_dimensions_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));

/* Control saving of COM and APPn markers into marker_list. */
EXTERN_MTK(void) jpeg_save_markers_MTK
JPP_MTK((j_decompress_ptr_MTK cinfo, int marker_code, unsigned int length_limit));

/* Install a special processing method for COM or APPn markers. */
EXTERN_MTK(void) jpeg_set_marker_processor_MTK
JPP_MTK((j_decompress_ptr_MTK cinfo, int marker_code, jpeg_marker_parser_method_MTK routine));

/* Read or write raw DCT coefficients --- useful for lossless transcoding. */
EXTERN_MTK(jvirt_barray_ptr_MTK *)
jpeg_read_coefficients_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(boolean_MTK) jpeg_build_huffman_index_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, huffman_index_MTK * index));
EXTERN_MTK(void)
jpeg_write_coefficients_MTK JPP_MTK((j_compress_ptr_MTK cinfo, jvirt_barray_ptr_MTK *coef_arrays));
EXTERN_MTK(void)
jpeg_copy_critical_parameters_MTK JPP_MTK((j_decompress_ptr_MTK srcinfo, j_compress_ptr_MTK dstinfo));

/* If you choose to abort compression or decompression before completing
 * jpeg_finish_(de)compress, then you need to clean up to release memory,
 * temporary files, etc.  You can just call jpeg_destroy_(de)compress
 * if you're done with the JPEG object, but if you want to clean it up and
 * reuse it, call this:
 */
EXTERN_MTK(void)
jpeg_abort_compress_MTK JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(void)
jpeg_abort_decompress_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));

/* Generic versions of jpeg_abort and jpeg_destroy that work on either
 * flavor of JPEG object.  These may be more convenient in some places.
 */
EXTERN_MTK(void)
jpeg_abort_MTK JPP_MTK((j_common_ptr_MTK cinfo));
EXTERN_MTK(void)
jpeg_destroy_MTK JPP_MTK((j_common_ptr_MTK cinfo));

/* Default restart-marker-resync procedure for use by data source modules */
EXTERN_MTK(boolean_MTK)
jpeg_resync_to_restart_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, int desired));

EXTERN_MTK(void) jpeg_configure_huffman_decoder_MTK(j_decompress_ptr_MTK cinfo, huffman_offset_data_MTK offset);
EXTERN_MTK(void) jpeg_get_huffman_decoder_configuration_MTK(j_decompress_ptr_MTK cinfo,
						    huffman_offset_data_MTK *offset);
EXTERN_MTK(void) jpeg_create_huffman_index_MTK(j_decompress_ptr_MTK cinfo, huffman_index_MTK * index);
EXTERN_MTK(void) jpeg_configure_huffman_index_scan_MTK(j_decompress_ptr_MTK cinfo,
					       huffman_index_MTK * index, int scan_no, int offset);
EXTERN_MTK(void) jpeg_destroy_huffman_index_MTK(huffman_index_MTK * index);


/* These marker codes are exported since applications and data source modules
 * are likely to want to use them.
 */

#define JPEG_RST0_MTK	0xD0	/* RST0 marker code */
#define JPEG_EOI_MTK	0xD9	/* EOI marker code */
#define JPEG_APP0_MTK	0xE0	/* APP0 marker code */
#define JPEG_COM_MTK	0xFE	/* COM marker code */


/* If we have a brain-damaged compiler that emits warnings (or worse, errors)
 * for structure definitions that are never filled in, keep it quiet by
 * supplying dummy definitions for the various substructures.
 */

#ifdef INCOMPLETE_TYPES_BROKEN_MTK
#ifndef JPEG_INTERNALS_MTK		/* will be defined in jpegint.h */
struct jvirt_sarray_control_MTK {
	long dummy;
};
struct jvirt_barray_control_MTK {
	long dummy;
};
struct jpeg_comp_master_MTK {
	long dummy;
};
struct jpeg_c_main_controller_MTK {
	long dummy;
};
struct jpeg_c_prep_controller_MTK {
	long dummy;
};
struct jpeg_c_coef_controller_MTK {
	long dummy;
};
struct jpeg_marker_writer_MTK {
	long dummy;
};
struct jpeg_color_converter_MTK {
	long dummy;
};
struct jpeg_downsampler_MTK {
	long dummy;
};
struct jpeg_forward_dct_MTK {
	long dummy;
};
struct jpeg_entropy_encoder_MTK {
	long dummy;
};
struct jpeg_decomp_master_MTK {
	long dummy;
};
struct jpeg_d_main_controller_MTK {
	long dummy;
};
struct jpeg_d_coef_controller_MTK {
	long dummy;
};
struct jpeg_d_post_controller_MTK {
	long dummy;
};
struct jpeg_input_controller_MTK {
	long dummy;
};
struct jpeg_marker_reader_MTK {
	long dummy;
};
struct jpeg_entropy_decoder_MTK {
	long dummy;
};
struct jpeg_inverse_dct_MTK {
	long dummy;
};
struct jpeg_upsampler_MTK {
	long dummy;
};
struct jpeg_color_deconverter_MTK {
	long dummy;
};
struct jpeg_color_quantizer_MTK {
	long dummy;
};
#endif				/* JPEG_INTERNALS */
#endif				/* INCOMPLETE_TYPES_BROKEN */


/*
 * The JPEG library modules define JPEG_INTERNALS before including this file.
 * The internal structure declarations are read only when that is true.
 * Applications using the library should not include jpegint.h, but may wish
 * to include jerror.h.
 */

#ifdef JPEG_INTERNALS_MTK
#include "jpegint.h"		/* fetch private declarations */
#include "jerror.h"		/* fetch error codes too */
#endif

#endif				/* JPEGLIB_H */
