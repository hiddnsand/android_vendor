/*
 * Copyright (c) 2016, ARM Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <arch_helpers.h>
#include <arm_gic.h>
#include <bl_common.h>
#include <cci.h>
#include <console.h>
#include <debug.h>
#include <mmio.h>
#include <mtk_aee_debug.h>
#include <mtk_plat_common.h>
#include <mtk_sip_svc.h>
#include <mtspmc.h>
#include <platform.h>
#include <plat_private.h>
#include <xlat_tables.h>

unsigned long __RW_START__;
unsigned long __RW_END__;


#define BL31_RO_BASE (unsigned long)(&__RO_START__)
#define BL31_RO_LIMIT (unsigned long)(&__RO_END__)
#define BL31_COHERENT_RAM_BASE (unsigned long)(&__COHERENT_RAM_START__)
#define BL31_COHERENT_RAM_LIMIT (unsigned long)(&__COHERENT_RAM_END__)

#define BL31_RW_BASE (unsigned long)(&__RW_START__)
#define BL31_RW_LIMIT (unsigned long)(&__RW_END__)

#pragma weak check_wdt_sgi_routing

#ifdef DRAM_EXTENSION_SUPPORT
extern unsigned long __DRAM_RO_START__;
extern unsigned long __DRAM_RO_END__;
extern unsigned long __DRAM_RW_START__;
extern unsigned long __DRAM_RW_END__;

#define DRAM_RO_BASE (unsigned long)(&__DRAM_RO_START__)
#define DRAM_RO_END (unsigned long)(&__DRAM_RO_END__)
#define DRAM_RW_BASE (unsigned long)(&__DRAM_RW_START__)
#define DRAM_RW_END (unsigned long)(&__DRAM_RW_END__)
#endif

struct atf_arg_t gteearg;
uint8_t percpu_plat_suspend_state[PLATFORM_CORE_COUNT]
#if USE_COHERENT_MEM
__section("tzfw_coherent_mem")
#endif
;
void clean_top_32b_of_param(uint32_t smc_fid,
				uint64_t *px1,
				uint64_t *px2,
				uint64_t *px3,
				uint64_t *px4)
{
	/* if parameters from SMC32. Clean top 32 bits */
	if (0 == (smc_fid & SMC_AARCH64_BIT)) {
		*px1 = *px1 & SMC32_PARAM_MASK;
		*px2 = *px2 & SMC32_PARAM_MASK;
		*px3 = *px3 & SMC32_PARAM_MASK;
		*px4 = *px4 & SMC32_PARAM_MASK;
	}
}

#if MTK_SIP_KERNEL_BOOT_ENABLE
static struct kernel_info k_info;

static void save_kernel_info(uint64_t pc,
			uint64_t r0,
			uint64_t r1,
			uint64_t k32_64)
{
	k_info.k32_64 = k32_64;
	k_info.pc = pc;

	if (k32_64 == LINUX_KERNEL_32) {
		/* for 32 bits kernel */
		k_info.r0 = 0;
		/* machtype */
		k_info.r1 = r0;
		/* tags */
		k_info.r2 = r1;
	} else {
		/* for 64 bits kernel */
		k_info.r0 = r0;
		k_info.r1 = r1;
	}
}

uint64_t get_kernel_info_pc(void)
{
	return k_info.pc;
}

uint64_t get_kernel_info_r0(void)
{
	return k_info.r0;
}

uint64_t get_kernel_info_r1(void)
{
	return k_info.r1;
}

uint64_t get_kernel_info_r2(void)
{
	return k_info.r2;
}

void set_kernel_k32_64(uint64_t k32_64)
{
    k_info.k32_64 = k32_64;
}

uint64_t get_kernel_k32_64(void)
{
    return k_info.k32_64;
}

void boot_to_kernel(uint64_t x1, uint64_t x2, uint64_t x3, uint64_t x4)
{
	static uint8_t kernel_boot_once_flag;
	/* only support in booting flow */
	if (kernel_boot_once_flag == 0) {
		kernel_boot_once_flag = 1;

		console_init(gteearg.atf_log_port,
			UART_CLOCK, UART_BAUDRATE);
		INFO("save kernel info\n");
		save_kernel_info(x1, x2, x3, x4);
		bl31_prepare_kernel_entry(x4);
#if CONFIG_SPMC_MODE != 0
#if !defined(ATF_BYPASS_DRAM)
		spmc_init();
#endif
#endif
		INFO("el3_exit\n");
		console_uninit();
	}
}
#endif

uint32_t plat_get_spsr_for_bl33_entry(void)
{
	unsigned int mode;
	uint32_t spsr;
	unsigned int ee;
	unsigned long daif;

	INFO("Secondary bootloader is AArch32\n");
	mode = MODE32_svc;
	ee = 0;
	/*
	 * TODO: Choose async. exception bits if HYP mode is not
	 * implemented according to the values of SCR.{AW, FW} bits
	 */
	daif = DAIF_ABT_BIT | DAIF_IRQ_BIT | DAIF_FIQ_BIT;

	spsr = SPSR_MODE32(mode, 0, ee, daif);
	return spsr;
}
/*******************************************************************************
 * common function setting up the pagetables of the platform memory map & initialize the mmu, for the given exception level.
 * Use plat_mmp_tbl as platform customized page table mapping.
 ******************************************************************************/
void configure_mmu_el3(const mmap_region_t *plat_mmap_tbl) 
{
	unsigned long total_base = TZRAM_BASE;
#ifdef DRAM_EXTENSION_SUPPORT
	unsigned long total_size = (TZRAM_SIZE) & ~(PAGE_SIZE_MASK);
#else
	unsigned long total_size = (TZRAM_SIZE + TZRAM2_SIZE) & ~(PAGE_SIZE_MASK);
#endif
	unsigned long ro_start = BL31_RO_BASE & ~(PAGE_SIZE_MASK);
	unsigned long ro_size = BL31_RO_LIMIT - ro_start;
#if USE_COHERENT_MEM
	unsigned long coh_start, coh_size;
#endif

	/* add memory regions */
	mmap_add_region(total_base, total_base,
			total_size,
			MT_MEMORY | MT_RW | MT_SECURE);
	mmap_add_region(ro_start, ro_start,
			ro_size,
			MT_MEMORY | MT_RO | MT_SECURE);

#if USE_COHERENT_MEM
	coh_start = BL31_COHERENT_RAM_BASE;
	coh_size = BL31_COHERENT_RAM_LIMIT - BL31_COHERENT_RAM_BASE;

	mmap_add_region(coh_start, coh_start,
			coh_size,
			MT_DEVICE | MT_RW | MT_SECURE);
#endif
#ifdef DRAM_EXTENSION_SUPPORT
	mmap_add_region(DRAM_RO_BASE, DRAM_RO_BASE,
			DRAM_RO_END - DRAM_RO_BASE,
			MT_MEMORY | MT_RO | MT_SECURE);
	mmap_add_region(DRAM_RW_BASE, DRAM_RW_BASE,
			DRAM_RW_END - DRAM_RW_BASE,
			MT_MEMORY | MT_RW | MT_SECURE);
#endif
	/* add mmap table */
	if (plat_mmap_tbl)
		mmap_add(plat_mmap_tbl);
	else
		WARN("platform mmap table is not available\n");

	/* set up translation tables */
	init_xlat_tables();

	/* enable the MMU */
	enable_mmu_el3(0);
}

void check_wdt_sgi_routing(void)
{
}

void bl31_plat_runtime_setup(void)
{
	/* check WDT/SGI routing after S-OS init */
	check_wdt_sgi_routing();

	/*
	 * Finish the use of console driver in BL31 so that any runtime logs
	 * from BL31 will be suppressed.
	 */
	console_uninit();
}
