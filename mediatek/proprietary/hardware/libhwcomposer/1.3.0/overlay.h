#ifndef HWC_OVERLAY_H_
#define HWC_OVERLAY_H_

#include <ui/Rect.h>
#include <utils/Singleton.h>
#include <utils/Vector.h>
#include "hwc_priv.h"

#define HWLAYER_ID_NONE -1
#define HWLAYER_ID_DBQ  -20

using namespace android;

class DisplayBufferQueue;
struct dump_buff;

// ---------------------------------------------------------------------------

struct OverlayPrepareParam
{
    OverlayPrepareParam()
        : id(-1)
        , ion_fd(-1)
        , is_need_flush(0)
        , fence_index(0)
        , fence_fd(-1)
    { }

    int id;
    int ion_fd;
    unsigned int is_need_flush;

    unsigned int fence_index;
    int fence_fd;
};

enum INPUT_PARAM_STATE
{
    OVL_IN_PARAM_IGNORE  = -1,
    OVL_IN_PARAM_DISABLE = 0,
    OVL_IN_PARAM_ENABLE  = 1,
};

struct OverlayPortParam
{
    OverlayPortParam()
        : state(OVL_IN_PARAM_DISABLE)
        , va(NULL)
        , mva(NULL)
        , pitch(0)
        , format(0)
        , is_sharpen(0)
        , fence_index(0)
        , identity(HWLAYER_ID_NONE)
        , connected_type(0)
        , protect(false)
        , secure(false)
        , alpha_enable(0)
        , alpha(0xFF)
        , need_shift(false)
        , sequence(0)
        , ion_fd(-1)
    { }
    int state;
    void* va;
    void* mva;
    unsigned int pitch;
    unsigned int format;
    Rect src_crop;
    Rect dst_crop;
    unsigned int is_sharpen;
    unsigned int fence_index;
    int identity;
    int connected_type;
    bool protect;
    bool secure;
    unsigned int alpha_enable;
    unsigned char alpha;
    bool need_shift;
    unsigned int sequence;
#ifdef MTK_HWC_PROFILING
    int fbt_input_layers;
    int fbt_input_bytes;
#endif

    int ion_fd;
};

// OverlayEngine is used for UILayerComposer and MMLayerComposer
// to config overlay input layers
class OverlayEngine : public LightRefBase<OverlayEngine>
{
public:
    // prepareInput() is used to preconfig a buffer with specific input port
    status_t prepareInput(OverlayPrepareParam& param);

    // setInputQueue() is used to config a buffer queue with a specific input port
    status_t setInputQueue(int id, sp<DisplayBufferQueue> queue);

    // setInputDirect() is used to set a input port as direct type
    status_t setInputDirect(int id, OverlayPortParam* param = NULL);

    // setInputs() is used to update all inputs configs to driver
    status_t setInputs();

    // disableInput() is used to disable specific input port
    status_t disableInput(int id);

    // ignoreInput() is used to bypass config for specific input layer
    status_t ignoreInput(int id);

    // prepareOutput() is used to preconfig a buffer with output port
    status_t prepareOutput(OverlayPrepareParam& param);

    // setOutput() is used to set output port
    status_t setOutput(OverlayPortParam* param);

    // trigger() is used to nofity engine to start doing composition
    status_t trigger();

    // getInputParams() is used for client to get input params for configuration
    OverlayPortParam* const* getInputParams();

    void blank(int blank);

    // getInputQueue() returns the buffer queue of a specific input port
    sp<DisplayBufferQueue> getInputQueue(int id) const;

    // getMaxInputNum() is used for getting max amount of overlay inputs
    int getMaxInputNum() { return m_max_inputs; }

    // getAvailableInputNum() is used for getting current avaliable overlay inputs
    int getAvailableInputNum();

    // flip() is used to notify OverlayEngine to do its job after notify framework
    void flip();

    // dump() is used to dump each input data to OverlayEngine
    void dump(struct dump_buff* log, int dump_level);

    bool isEnable()
    {
        AutoMutex l(m_lock);
        return (m_state == OVL_ENGINE_ENABLED);
    }

    OverlayEngine(int dpy);
    ~OverlayEngine();

private:
    enum INPUT_TYPE
    {
        OVL_INPUT_NONE    = 0,
        OVL_INPUT_UNKNOWN = 1,
        OVL_INPUT_DIRECT  = 2,
        OVL_INPUT_QUEUE   = 3,
    };

    enum PORT_STATE
    {
        OVL_PORT_DISABLE = 0,
        OVL_PORT_ENABLE  = 1,
    };

    struct OverlayInput
    {
        OverlayInput();

        // connected_state points the input port usage state
        int connected_state;

        // connected_type is connecting type of input port
        int connected_type;

        // param is used for configure input parameters
        OverlayPortParam param;

        // queue is used to acquire and release buffer from client
        sp<DisplayBufferQueue> queue;

        // outdated_queues is used to keep outdated queues
        // until they are no longer needed by overlay engine
        //
        // protected by outdated_queues_lock
        mutable Mutex outdated_queues_lock;
        Vector< sp<DisplayBufferQueue> > outdated_queues[2];
        Vector< sp<DisplayBufferQueue> >* front_queues;
        Vector< sp<DisplayBufferQueue> >* back_queues;
    };

    // keepOutdatedQueue keeps outdated queue
    void keepOutdatedQueue(int id);

    // clearOutdatedQueue releases outdated queue when needed
    void clearOutdatedQueue(int id);

    struct OverlayOutput
    {
        OverlayOutput();

        // connected_state points the output port usage state
        int connected_state;

        // param is used for configure output parameters
        OverlayPortParam param;
    };

    // updateInput() would notify OverlayEngine to consume new queued buffer
    void updateInput(int id);

    // disableInputLocked is used to clear status of m_inputs
    void disableInputLocked(int id);

    mutable Mutex m_lock;

    // m_disp_id is display id
    int m_disp_id;

    enum
    {
        OVL_ENGINE_DISABLED = 0,
        OVL_ENGINE_ENABLED  = 1,
        OVL_ENGINE_PAUSED   = 2,
    };
    // m_state used to verify if OverlayEngine could be use
    int m_state;

    // m_max_inputs is overlay engine max amount of input layer
    // it is platform dependent
    int m_max_inputs;

    // m_inputs is input information array
    // it needs to be initialized with runtime information
    Vector<OverlayInput*> m_inputs;

    // m_input_params is an array which
    // points to all input configurations
    // which would be set to display driver
    Vector<OverlayPortParam*> m_input_params;

    OverlayOutput m_output;
};

// --------------------------------------------------------------------------

class ExternalStream : public Singleton<ExternalStream>
{
public:
    // prepareInput() s used to preconfig a buffer
    status_t prepareInput(OverlayPrepareParam& param);

    // setInputDirect() is used to config a buffer
    status_t setInputDirect(OverlayPortParam& param);

    ExternalStream() { }
    ~ExternalStream() { }

private:
    mutable Mutex m_lock;
};

#endif // HWC_OVERLAY_H_
