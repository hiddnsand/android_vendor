package com.mediatek.providers.contacts;

import android.os.SystemProperties;
import android.telephony.TelephonyManager;

/**
 * M: add for Contacts Provider.
 */
public class ContactsProviderUtils {

    /**
     * [Gemini] whether Gemini feature enabled on this device.
     * @return ture if allowed to enable
     */
    public static boolean isGeminiSupport() {
        return TelephonyManager.getDefault().getSimCount() > 1;
    }

    /**
    * [VOLTE/IMS] whether VOLTE feature enabled on this device.
    * @return ture if allowed to enable
    */
   public static boolean isVolteEnabled() {
       return ("1").equals(SystemProperties.get("persist.mtk_volte_support"));
   }

    /**
    * [VOLTE/IMS] whether ImsCall feature enabled on this device.
    * @return ture if allowed to enable
    */
   public static boolean isImsCallEnabled() {
       return ("1").equals(SystemProperties.get("persist.mtk_ims_support"));
   }
}