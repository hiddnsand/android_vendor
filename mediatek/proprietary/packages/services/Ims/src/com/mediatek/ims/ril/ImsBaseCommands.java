/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims.ril;


import android.content.Context;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Registrant;
import android.os.RegistrantList;
import android.telephony.Rlog;

/**
 * {@hide}
 */
public abstract class ImsBaseCommands implements ImsCommandsInterface {

    //***** Instance Variables

    // Context
    protected Context mContext;

    // Radio State of Current Phone
    protected RadioState mState = RadioState.RADIO_UNAVAILABLE;

    // Current Phone Id
    protected int mPhoneId;

    // Lock Token
    protected Object mStateMonitor = new Object();

    protected RegistrantList mRadioStateChangedRegistrants = new RegistrantList();
    protected RegistrantList mOnRegistrants = new RegistrantList();
    protected RegistrantList mOffRegistrants = new RegistrantList();
    protected RegistrantList mAvailRegistrants = new RegistrantList();
    protected RegistrantList mOffOrNotAvailRegistrants = new RegistrantList();
    protected RegistrantList mNotAvailRegistrants = new RegistrantList();

    // Call Control
    protected Registrant mIncomingCallIndicationRegistrant;

    // VoLTE
    protected RegistrantList mImsEnableStartRegistrants = new RegistrantList();
    protected RegistrantList mImsDisableStartRegistrants = new RegistrantList();
    protected RegistrantList mImsEnableDoneRegistrants = new RegistrantList();
    protected RegistrantList mImsDisableDoneRegistrants = new RegistrantList();

    protected RegistrantList mImsRegistrationInfoRegistrants = new RegistrantList();

    /// IMS Stack Relative

    /* Register for updating conference call merged/added result. */
    protected RegistrantList mEconfResultRegistrants = new RegistrantList();
    /* Register for updating call mode and pau. */
    protected RegistrantList mCallInfoRegistrants = new RegistrantList();
    /* Register for updating explicit call transfer result. */
    protected RegistrantList mEctResultRegistrants = new RegistrantList();

    protected RegistrantList mCallProgressIndicatorRegistrants = new RegistrantList();

    // ViLTE feature, call mode changed event.
    protected RegistrantList mCallModeChangeIndicatorRegistrants = new RegistrantList();
    protected RegistrantList mVideoCapabilityIndicatorRegistrants = new RegistrantList();

    // For incoming USSI event
    protected RegistrantList mUSSIRegistrants = new RegistrantList();
    protected RegistrantList mNetworkInitUSSIRegistrants = new RegistrantList();

    // ECBM
    protected RegistrantList mEnterECBMRegistrants = new RegistrantList();
    protected RegistrantList mExitECBMRegistrants = new RegistrantList();

    // IMS Provisioning
    protected RegistrantList mImsGetProvisionDoneRegistrants = new RegistrantList();
    // Register for updating RTP packets info
    protected RegistrantList mRTPInfoRegistrants = new RegistrantList();

    // Sync VoLTE setting value
    protected RegistrantList mVolteSettingRegistrants = new RegistrantList();
    protected Object mVolteSettingValue = null;

    // Register for IMS Bearer update
    protected RegistrantList mActivateBearerRegistrants = new RegistrantList();
    protected RegistrantList mDeactivateBearerRegistrants = new RegistrantList();
    protected RegistrantList mBearerInitRegistrants = new RegistrantList();

    // Register for IMS Xui information update
    protected RegistrantList mXuiRegistrants = new RegistrantList();

    // Register for IMS Event Package
    protected RegistrantList mImsEvtPkgRegistrants = new RegistrantList();

    // GTT
    protected RegistrantList mGttCapabilityIndicatorRegistrants = new RegistrantList();

    // IMS Deregister Done
    protected RegistrantList mImsDeregistrationDoneRegistrants = new RegistrantList();

    // MD support multiple IMS count
    protected RegistrantList mMultiImsCountRegistrants = new RegistrantList();

    /**
     * Constructor
     * @param context Android Context
     * @param phoneid PhoneId
     */
    public ImsBaseCommands(Context context, int phoneid) {
        mContext = context;
        mPhoneId = phoneid;
    }

    @Override
    public void setOnIncomingCallIndication(Handler h, int what, Object obj) {
        mIncomingCallIndicationRegistrant = new Registrant(h, what, obj);
    }

    @Override
    public void unsetOnIncomingCallIndication(Handler h) {
        mIncomingCallIndicationRegistrant.clear();
    }

    @Override
    public void registerForEconfResult(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mEconfResultRegistrants.add(r);
    }

    @Override
    public void unregisterForEconfResult(Handler h) {
        mEconfResultRegistrants.remove(h);
    }

    @Override
    public void registerForCallInfo(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mCallInfoRegistrants.add(r);
    }

    @Override
    public void unregisterForCallInfo(Handler h) {
        mCallInfoRegistrants.remove(h);
    }

    @Override
    public void registerForImsEnableStart(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mImsEnableStartRegistrants.add(r);
    }

    @Override
    public void unregisterForImsEnableStart(Handler h) {
        mImsEnableStartRegistrants.remove(h);
    }

    @Override
    public void registerForImsDisableStart(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mImsDisableStartRegistrants.add(r);
    }

    @Override
    public void unregisterForImsDisableStart(Handler h) {
        mImsDisableStartRegistrants.remove(h);
    }

    @Override
    public void registerForImsEnableComplete(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mImsEnableDoneRegistrants.add(r);
    }

    @Override
    public void unregisterForImsEnableComplete(Handler h) {
        mImsEnableDoneRegistrants.remove(h);
    }

    @Override
    public void registerForImsDisableComplete(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mImsDisableDoneRegistrants.add(r);
    }

    @Override
    public void unregisterForImsDisableComplete(Handler h) {
        mImsDisableDoneRegistrants.remove(h);
    }

    @Override
    public void registerForImsRegistrationInfo(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mImsRegistrationInfoRegistrants.add(r);
    }

    @Override
    public void unregisterForImsRegistrationInfo(Handler h) {
        mImsRegistrationInfoRegistrants.remove(h);
    }

    @Override
    public void registerForCallProgressIndicator(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);

        mCallProgressIndicatorRegistrants.add(r);
    }

    @Override
    public void unregisterForCallProgressIndicator(Handler h) {
        mCallProgressIndicatorRegistrants.remove(h);
    }

    @Override
    public void registerForOnEnterECBM(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mEnterECBMRegistrants.add(r);
    }

    @Override
    public void unregisterForOnEnterECBM(Handler h) {
        mEnterECBMRegistrants.remove(h);
    }

    @Override
    public void registerForOnExitECBM(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mExitECBMRegistrants.add(r);
    }

    @Override
    public void unregisterForOnExitECBM(Handler h) {
        mExitECBMRegistrants.remove(h);
    }

    @Override
    public void registerForGetProvisionComplete(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mImsGetProvisionDoneRegistrants.add(r);
    }

    @Override
    public void unregisterForGetProvisionComplete(Handler h) {
        mImsGetProvisionDoneRegistrants.remove(h);
    }

    @Override
    public void registerForEctResult(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mEctResultRegistrants.add(r);
    }

    @Override
    public void unregisterForEctResult(Handler h) {
        mEctResultRegistrants.remove(h);
    }

    @Override
    public void registerForCallModeChangeIndicator(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);

        mCallModeChangeIndicatorRegistrants.add(r);
    }

    @Override
    public void unregisterForCallModeChangeIndicator(Handler h) {
        mCallModeChangeIndicatorRegistrants.remove(h);
    }

    @Override
    public void registerForVideoCapabilityIndicator(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mVideoCapabilityIndicatorRegistrants.add(r);
    }

    @Override
    public void unregisterForVideoCapabilityIndicator(Handler h) {
        mVideoCapabilityIndicatorRegistrants.remove(h);
    }

    @Override
    public void registerForImsRTPInfo(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mRTPInfoRegistrants.add(r);
    }

    @Override
    public void unregisterForImsRTPInfo(Handler h) {
        mRTPInfoRegistrants.remove(h);
    }

    @Override
    public void registerForVolteSettingChanged(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mVolteSettingRegistrants.add(r);
        if (mVolteSettingValue != null) {
            r.notifyRegistrant(new AsyncResult(null, mVolteSettingValue, null));
        }
    }

    @Override
    public void unregisterForVolteSettingChanged(Handler h) {
        mVolteSettingRegistrants.remove(h);
    }

    @Override
    public void registerForBearerActivation(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mActivateBearerRegistrants.add(r);
    }

    @Override
    public void unregisterForBearerActivation(Handler h) {
        mActivateBearerRegistrants.remove(h);
    }

    @Override
    public void registerForBearerDeactivation(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mDeactivateBearerRegistrants.add(r);
    }

    @Override
    public void unregisterForBearerDeactivation(Handler h) {
        mDeactivateBearerRegistrants.remove(h);
    }

    @Override
    public void registerForXuiInfo(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mXuiRegistrants.add(r);
    }

    @Override
    public void unregisterForXuiInfo(Handler h) {
        mXuiRegistrants.remove(h);
    }

    @Override
    public void registerForImsEventPackage(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mImsEvtPkgRegistrants.add(r);
    }

    @Override
    public void setOnUSSI(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mUSSIRegistrants.add(r);
    }

    @Override
    public void unSetOnUSSI(Handler h) {
        mUSSIRegistrants.remove(h);
    }

    @Override
    public void setOnNetworkInitUSSI(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mNetworkInitUSSIRegistrants.add(r);
    }

    @Override
    public void unSetOnNetworkInitUSSI(Handler h) {
        mNetworkInitUSSIRegistrants.remove(h);
    }

    /**
     * Un-register for ims event package.
     *
     * @param h handler
     * @hide
     */
    @Override
    public void unregisterForImsEventPackage(Handler h) {
        mImsEvtPkgRegistrants.remove(h);
    }

    /**
     * Registers the handler for GTT capability changed event.
     * @param h Handler for notification message.
     * @param what User-defined message code.
     * @param obj User object.
     *
     */
    @Override
    public void registerForGttCapabilityIndicator(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mGttCapabilityIndicatorRegistrants.add(r);
    }

    /**
     * Unregisters the handler for GTT capability changed event.
     *
     * @param h Handler for notification message.
     *
     */
    @Override
    public void unregisterForGttCapabilityIndicator(Handler h) {
        mGttCapabilityIndicatorRegistrants.remove(h);
    }


    /** Register for IMS bearer initialize event.
     *
     * @param h handler
     * @param what message
     * @param obj object
     * @hide
     */
    @Override
    public void registerForBearerInit(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mBearerInitRegistrants.add(r);
    }

    /**
     * Un-register for IMS bearer initialize event.
     *
     * @param h handler
     * @hide
     */
    @Override
    public void unregisterForBearerInit(Handler h) {
        mBearerInitRegistrants.remove(h);
    }


    /**
     * Register for Not available
     * @param h
     * @param what
     * @param obj
     */
    @Override
    public void registerForNotAvailable(Handler h, int what, Object obj) {
        Registrant r = new Registrant (h, what, obj);

        synchronized (mStateMonitor) {
            mNotAvailRegistrants.add(r);

            if (!mState.isAvailable()) {
                r.notifyRegistrant(new AsyncResult(null, null, null));
            }
        }
    }

    /**
     * Unregister for not available
     * @param h
     */
    @Override
    public void unregisterForNotAvailable(Handler h) {
        synchronized (mStateMonitor) {
            mNotAvailRegistrants.remove(h);
        }
    }

    /**
     * Register for OFF
     * @param h
     * @param what
     * @param obj
     */
    @Override
    public void registerForOff(Handler h, int what, Object obj) {
        Registrant r = new Registrant (h, what, obj);

        synchronized (mStateMonitor) {
            mOffRegistrants.add(r);

            if (mState == RadioState.RADIO_OFF) {
                r.notifyRegistrant(new AsyncResult(null, null, null));
            }
        }
    }

    /**
     * Unregister for OFF
     * @param h
     * @param what
     * @param obj
     */
    @Override
    public void unregisterForOff(Handler h) {
        synchronized(mStateMonitor) {
            mOffRegistrants.remove(h);
        }
    }

    /**
     * Register for ON
     * @param h
     * @param what
     * @param obj
     */
    @Override
    public void registerForOn(Handler h, int what, Object obj) {
        Registrant r = new Registrant (h, what, obj);

        synchronized (mStateMonitor) {
            mOnRegistrants.add(r);

            if (mState.isOn()) {
                r.notifyRegistrant(new AsyncResult(null, null, null));
            }
        }
    }

    /**
     * Unregister for ON
     * @param h
     * @param what
     * @param obj
     */
    @Override
    public void unregisterForOn(Handler h) {
        synchronized (mStateMonitor) {
            mOnRegistrants.remove(h);
        }
    }

    /**
     * Register for IMS Deregister Complete
     * @param h
     * @param what
     * @param obj
     */
    @Override
    public void registerForImsDeregisterComplete(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mImsDeregistrationDoneRegistrants.add(r);
    }

    /**
     * Unregister for IMS Deregister Complete
     * @param h
     */
    @Override
    public void unregisterForImsDeregisterComplete(Handler h) {
        mImsDeregistrationDoneRegistrants.remove(h);
    }

    /**
     * Register for MD support IMS count indication
     * @param h
     * @param what
     * @param obj
     */
    @Override
    public void registerForMultiImsCount(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mMultiImsCountRegistrants.add(r);
    }

    /**
     * Unregister for MD support IMS count indication
     * @param h
     */
    @Override
    public void unregisterForMultiImsCount(Handler h) {
        mMultiImsCountRegistrants.remove(h);
    }

    // Protected Method Below

    protected void onRadioAvailable() {
    }

    /**
     * Store a new RadioState and send notification base on the <br/>
     * the changes when the RIL_UNSOL_RADIO_STATE_CHANGED comes.</br>
     * <br/>
     * This function is only called by the inheritance class while <br/>
     * receiving unsolicited event
     * @param newState New Radio State
     */
    protected void setRadioState(RadioState newState) {
        RadioState oldState;

        synchronized (mStateMonitor) {
            oldState = mState;
            mState = newState;

            if (oldState == mState) {
                // no state transition
                Rlog.d("ImsBaseCommands", "no state transition: " + mState);
                return;
            }

            mRadioStateChangedRegistrants.notifyRegistrants();

            if (mState.isAvailable() && !oldState.isAvailable()) {
                mAvailRegistrants.notifyRegistrants();
                onRadioAvailable();
            }

            if (!mState.isAvailable() && oldState.isAvailable()) {
                mNotAvailRegistrants.notifyRegistrants();
            }

            if (mState.isOn() && !oldState.isOn()) {
                mOnRegistrants.notifyRegistrants();
            }

            if ((!mState.isOn() || !mState.isAvailable())
                && !((!oldState.isOn() || !oldState.isAvailable()))) {
                mOffOrNotAvailRegistrants.notifyRegistrants();
            }

            if (mState.isAvailable() && !mState.isOn()) {
                mOffRegistrants.notifyRegistrants();
            }
        }
    }

    /**
     * Broadcast after the change of radio state
     * MTK Proprietary Logical
     * @param newState New Change
     */
    protected void notifyRadioStateChanged(RadioState newState) {
        // Do Notify Here
    }
}
