/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M J3M "Java" implementation.
 */
#pragma once
#ifndef JNI_A3M_J3M_H
#define JNI_A3M_J3M_H

#include <a3m_appearance.h>
#include <a3m_assetpool.h>
#include <a3m_camera.h>
#include <a3m_flagmask.h>
#include <a3m/info.h>
#include <a3m_light.h>
#include <a3m_plane.h>
#include <a3m_renderblock.h>
#include <a3m_renderer.h>
#include <a3m_rendertarget.h>
#include <a3m_scenenode.h>
#include <a3m_sphere.h>
#include <a3m_solid.h>
#include <a3m_square.h>
#include <a3m_version.h>
#include <a3m/simplerenderer.h>
#include <a3m/motionblurrenderer.h>
#include <a3mgetlightzorder.h>

class A3mJ3m
{
public:
  A3mJ3m()
  {
  }

  A3mVersion getVersion() const
  {
    return A3mVersion::toWrapper(a3m::getVersion());
  }

  char const* getBuildInfo() const
  {
    return a3m::getBuildInfo();
  }

  A3mRenderContext* createRenderContext()
  {
    return A3mRenderContext::toWrapper(
        a3m::RenderContext::Ptr(new a3m::RenderContext()));
  }

  A3mAssetPool* createAssetPool()
  {
    return A3mAssetPool::toWrapper(
        a3m::AssetCachePool::Ptr(new a3m::AssetCachePool()));
  }

  A3mAppearance* createAppearance()
  {
    return A3mAppearance::toWrapper(
        a3m::Appearance::Ptr(new a3m::Appearance()));
  }

  A3mFlagMask createFlagMask()
  {
    return A3mFlagMask::toWrapper(a3m::FlagMask());
  }

  A3mFlagMask createFlagMask(int index, bool state)
  {
    return A3mFlagMask::toWrapper(a3m::FlagMask(index, state));
  }

  A3mSceneNode* createSceneNode()
  {
    return A3mSceneNode::toWrapper(
        a3m::SceneNode::Ptr(new a3m::SceneNode()));
  }

  A3mSolid* createSolid()
  {
    return A3mSolid::toWrapper(
        a3m::Solid::Ptr(new a3m::Solid()));
  }

  A3mCamera* createCamera()
  {
    return A3mCamera::toWrapper(
        a3m::Camera::Ptr(new a3m::Camera()));
  }

  A3mLight* createLight()
  {
    return A3mLight::toWrapper(
        a3m::Light::Ptr(new a3m::Light()));
  }

  A3mRenderer* createRenderer(A3mRenderContext& context, A3mAssetPool& pool)
  {
    return A3mRenderer::toWrapper(
        a3m::Renderer::Ptr(new a3m::SimpleRenderer(
            context.getNative(), pool.getNative())));
  }

  A3mRenderer* createMotionBlurRenderer(
      A3mRenderContext& context, A3mAssetPool& pool)
  {
    return A3mRenderer::toWrapper(
        a3m::Renderer::Ptr(new a3m::MotionBlurRenderer(
            context.getNative(), pool.getNative())));
  }

  A3mRenderBlock* createRenderBlock(
      A3mRenderer renderer,
      A3mSceneNode sceneNode,
      A3mCamera *camera)
  {
    return A3mRenderBlock::toWrapper(
        a3m::RenderBlock::Ptr(new a3m::RenderBlock(
            renderer.getNative(),
            sceneNode.getNative(),
            A3mCamera::toNative(camera))));
  }

  A3mRenderBlockGroup* createRenderBlockGroup()
  {
    return A3mRenderBlockGroup::toWrapper(
        a3m::RenderBlockGroup::Ptr(new a3m::RenderBlockGroup()));
  }

  A3mRenderTarget *createRenderTarget(
      A3mTexture2D *colourTexture,
      A3mTexture2D *depthTexture,
      bool depthBuffer,
      bool stencilBuffer)
  {
    return A3mRenderTarget::toWrapper(
      a3m::RenderTarget::Ptr(new a3m::RenderTarget(
        A3mTexture2D::toNative(colourTexture),
        A3mTexture2D::toNative(depthTexture),
        depthBuffer,
        stencilBuffer)));
  }

  A3mRay createRay()
  {
    return A3mRay::toWrapper(a3m::Ray());
  }

  A3mPlane* createPlane()
  {
    return A3mPlane::toWrapper(a3m::Plane::Ptr(new a3m::Plane()));
  }

  A3mSquare* createSquare()
  {
    return A3mSquare::toWrapper(a3m::Square::Ptr(new a3m::Square()));
  }

  A3mSphere* createSphere()
  {
    return A3mSphere::toWrapper(a3m::Sphere::Ptr(new a3m::Sphere()));
  }

  A3mVersion createVersion(
      int major = 0, int minor = 0, int patch = 0, char const* extra = "")
  {
    return A3mVersion::toWrapper(a3m::Version(major, minor, patch, extra));
  }

  // BYTE tells SWIG to interpret this as a byte array rather than a string.
  void getPixels(char* BYTE, int left, int bottom, int width, int height)
  {
    a3m::RenderDevice::getPixels(
        left, bottom, width, height,
        reinterpret_cast<A3M_UINT8*>(BYTE));
  }

  int getLightZOrder()
  {
    return a3mGetLightZOrder();
  }
};

#endif // JNI_A3M_J3M_H
