package com.mediatek.browser.plugin;

import android.content.Context;

import com.mediatek.browser.ext.IBrowserBookmarkExt;
import com.mediatek.browser.ext.IBrowserDownloadExt;
import com.mediatek.browser.ext.IBrowserHistoryExt;
import com.mediatek.browser.ext.IBrowserMiscExt;
import com.mediatek.browser.ext.IBrowserSettingExt;
import com.mediatek.browser.ext.IBrowserSiteNavigationExt;
import com.mediatek.browser.ext.IBrowserUrlExt;
import com.mediatek.browser.ext.OpBrowserCustomizationFactoryBase;

public class Op09BrowserCustomizationFactory extends OpBrowserCustomizationFactoryBase {
    private Context mContext;

    public Op09BrowserCustomizationFactory(Context context) {
        mContext = context;
	}
	
    @Override
    public IBrowserBookmarkExt makeBrowserBookmarkExt() {
        android.util.Log.d("YifanTest", "new plugin framework op09!");
        return new Op09BrowserBookmarkExt(mContext);
    }
    @Override
    public IBrowserDownloadExt makeBrowserDownloadExt() {
        return new Op09BrowserDownloadExt(mContext);
    }
    @Override
    public IBrowserHistoryExt makeBrowserHistoryExt() {
        return new Op09BrowserHistoryExt(mContext);
    }
    @Override
    public IBrowserMiscExt makeBrowserMiscExt() {
        return new Op09BrowserMiscExt();
    }
    @Override
    public IBrowserSettingExt makeBrowserSettingExt() {
        return new Op09BrowserSettingExt(mContext);
    }
    @Override
    public IBrowserSiteNavigationExt makeBrowserSiteNavigationExt() {
        return new Op09BrowserSiteNavigationExt(mContext);
    }
    @Override
    public IBrowserUrlExt makeBrowserUrlExt() {
        return new Op09BrowserUrlExt(mContext);
    }   
}