package com.mediatek.security.datamanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.mediatek.security.R;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String TAG = "DatabaseHelper";
    private static final String DB_NAME = "permission_control.db";
    private static final  int DB_VERSION = 3;
    //
    private static final String FIELD_ID = "_id";
    private static final String FIELD_NAME_PACKAGE = "packages_name";

    private static final String TABLE_PACKAGE_PERMISSIONS = "package_permission";
    private static final String FIELD_NAME_CELLULAR_DATA = "cellular_data";
    private HashMap<String, CheckedPermRecord> mPkgPermissionsCache =
      new HashMap<String, CheckedPermRecord>();
    private Context mContext;
    private SQLiteDatabase mDb;

    /**
     * Construct of DatabaseHelper, this construct to init database and access
     * data base
     * @param context
     */
    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        mContext = context;
        mDb = getWritableDatabase();
    }

    public void initCacheData() {
        initCellularDataPerm();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_PACKAGE_PERMISSIONS +
                " (" + FIELD_ID + " INTEGER primary key autoincrement," +
                " " + FIELD_NAME_PACKAGE + " text," +
                " " + FIELD_NAME_CELLULAR_DATA + " int)";
        db.execSQL(sql);
    }

    /**
     * Because current there is no new add into db so do nothing
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "onUpgrade oldVersion = " + oldVersion + ", newVersion = "
                + newVersion);
        if (oldVersion != DB_VERSION) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_PACKAGE_PERMISSIONS);
            onCreate(db);
        }
    }

    /**
     * Get the cursor of table
     * @param tableName name of table
     * @return cursor of the table
     */
    public Cursor getCursor(String tableName) {
        Cursor cursor = mDb.query(tableName, null, null, null, null, null, null);
        return cursor;
    }

    public HashSet<String> getCellularDataPkgNameFromDB() {
        Cursor cursor = null;
        HashSet<String> set = new HashSet<String>();
        try {
            String[] columns = new String[] { FIELD_NAME_PACKAGE };
            cursor = mDb.query(TABLE_PACKAGE_PERMISSIONS, columns, null, null,
                    null, null, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String packageName = cursor.getString(cursor
                            .getColumnIndex(FIELD_NAME_PACKAGE));
                    set.add(packageName);
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return set;
    }

    public List<CheckedPermRecord> getCellularDataPermFromDB() {
        Cursor cursor = null;
        List<CheckedPermRecord> set = new ArrayList<CheckedPermRecord>();
        try {
            cursor = getCursor(TABLE_PACKAGE_PERMISSIONS);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String packageName = cursor.getString(cursor
                            .getColumnIndex(FIELD_NAME_PACKAGE));
                    int cellularData = cursor.getInt(cursor
                            .getColumnIndex(FIELD_NAME_CELLULAR_DATA));
                    CheckedPermRecord record = new CheckedPermRecord(
                            packageName,
                            R.string.cellular_data_message,
                            cellularData);
                    set.add(record);
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return set;
    }

    public void addCellularDatePerm(CheckedPermRecord info) {
        ContentValues cv = new ContentValues();

        cv.put(FIELD_NAME_PACKAGE, info.mPackageName);
        cv.put(FIELD_NAME_CELLULAR_DATA, info.getStatus());
        mDb.insert(TABLE_PACKAGE_PERMISSIONS, null, cv);
        mPkgPermissionsCache.put(info.mPackageName, info);
    }

    public void deleteCellularDatePerm(String pagName) {
        String where = FIELD_NAME_PACKAGE + "= ?";
        String[] whereValue = new String[] { pagName };
        mDb.delete(TABLE_PACKAGE_PERMISSIONS, where, whereValue);
        mPkgPermissionsCache.remove(pagName);
    }

    public boolean modifyCellularDatePerm(CheckedPermRecord info) {
        boolean isChanged = false;
        synchronized(mPkgPermissionsCache) {
            CheckedPermRecord target = mPkgPermissionsCache
                    .get(info.mPackageName);
            if (target.isEnable() != info.isEnable()) {
                isChanged = true;
            }
            if (target.getStatus() != info.getStatus()) {
                ContentValues cv = new ContentValues();
                cv.put(FIELD_NAME_CELLULAR_DATA, info.getStatus());
                String where = FIELD_NAME_PACKAGE + "= ?";
                String[] whereValue = new String[] { info.mPackageName };
                mDb.update(TABLE_PACKAGE_PERMISSIONS, cv, where, whereValue);
                target.setStatus(info.getStatus());
            }
        }
        return isChanged;
    }

    public HashMap<String, CheckedPermRecord> getCellularDataPermCache() {
        return mPkgPermissionsCache;
    }

    private void initCellularDataPerm() {
        Cursor cursor = null;
        try {
            cursor = getCursor(TABLE_PACKAGE_PERMISSIONS);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String packageName = cursor.getString(cursor
                            .getColumnIndex(FIELD_NAME_PACKAGE));
                    int cellularData = cursor.getInt(cursor
                            .getColumnIndex(FIELD_NAME_CELLULAR_DATA));
                    CheckedPermRecord record = new CheckedPermRecord(
                            packageName,
                            R.string.cellular_data_message,
                            cellularData);
                    mPkgPermissionsCache.put(packageName, record);
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
