/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.rcs.blacklist;

import android.content.ContentResolver;
//import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.BaseColumns;
import android.provider.CallLog.Calls;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.PhoneLookup;

import com.mediatek.provider.MtkCallLog;
import com.mediatek.rcs.blacklist.BlacklistData.BlackListCallLogTable;
import com.mediatek.rcs.blacklist.BlacklistData.BlacklistTable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * BlacklistUtils.
 */
public class BlacklistUtils {

    private static final String TAG = "Blacklist";

    private static final Uri BLACKLIST_URI = BlacklistData.AUTHORITY_URI;
    private static final Uri CONTACTS_URI = Data.CONTENT_URI;
    private static final String[] BLACKLIST_PROJECTION = {BaseColumns._ID,
                                                            BlacklistTable.DISPLAY_NAME,
                                                            BlacklistTable.PHONE_NUMBER};

    // regular expression for removing '.' '-' and ' '
    private static final String SPLIT_CHARS_EXP = new String("[.-]");
    private static final String WHITESPACE_CHAR_EXP = new String("\\s");

    private static final String[] CONTACTS_PROJECTION = new String[] {
        Phone.NUMBER,
        Phone.DISPLAY_NAME};

    private final static ArrayList<SyncWithContactsCallback> mSyncCallbacks =
                                new ArrayList<SyncWithContactsCallback>();
    private static SyncWithContactsTask sSyncTask = null;

    //Blacklist call log
    private static final String[] EMPTY_ARRAY = new String[0];
    private static final Uri CALLLOG_URI = Uri.parse("content://call_log/callsjoindataview");
    // for query call log items
    private static final String[] CALLLOG_PROJECTION = new String[] {
        Calls._ID,
        Calls.NUMBER,
        Contacts.DISPLAY_NAME,
        Calls.DATE
    };

    // for making cursor table
    private static final String[] TABLE_PROJECTION = new String[] {BlackListCallLogTable.CALL_ID,
            BlackListCallLogTable.PHONE_NUMBER, BlackListCallLogTable.NAME,
            BlackListCallLogTable.DATE};

    // for mapping CALLLOG_PROJECTION and TABLE_PROJECTION
    private static HashMap<String, String> tableMap = new HashMap<String, String>();
    // for mapping TABLE_PROJECTION and CALLLOG_PROJECTION
    private static HashMap<String, String> projectionMap = new HashMap<String, String>();

    static {
        for (int i = 0; i < TABLE_PROJECTION.length; i++) {
            tableMap.put(CALLLOG_PROJECTION[i], TABLE_PROJECTION[i]);
            projectionMap.put(TABLE_PROJECTION[i], CALLLOG_PROJECTION[i]);
        }
    }

    protected static void insertNumber(ContentResolver resolver, String name, String number) {
        ContentValues values = new ContentValues();

        values.put(BlacklistTable.PHONE_NUMBER, number);

        if (name != null && !name.isEmpty()) {
            values.put(BlacklistTable.DISPLAY_NAME, name);
        }

        resolver.insert(BLACKLIST_URI, values);
        Log.i("[BlacklistUtils]", "insertNumber: " + name + ", " + Log.formatNumber(number));
    }

    protected static void importFromContacts(ContentResolver resolver, final long[] ids) {
        if (ids == null || ids.length <= 0) {
            return;
        }

        StringBuilder selection = new StringBuilder(Phone._ID + " in (");
        for (long id : ids) {
            selection.append(Long.toString(id));
            selection.append(',');
        }
        selection.deleteCharAt(selection.length() - 1);
        selection.append(')');

        Log.i("[BlacklistUtils]", selection.toString());
        Cursor cursorContact = resolver.query(CONTACTS_URI, CONTACTS_PROJECTION,
                                        selection.toString(), null, null);
        if (cursorContact == null) {
            return;
        }

        try {
            cursorContact.moveToFirst();
            while (!cursorContact.isAfterLast()) {
                String number = cursorContact
                        .getString(cursorContact.getColumnIndexOrThrow(CONTACTS_PROJECTION[0]));
                String name = cursorContact
                        .getString(cursorContact.getColumnIndexOrThrow(CONTACTS_PROJECTION[1]));
                if (number == null || number.isEmpty()) {
                    Log.i("[BlacklistUtils]", "cursor is null or empty !");
                } else {
                    ContentValues values = new ContentValues();
                    values.put(BlacklistTable.PHONE_NUMBER, number);
                    if (name != null && !name.isEmpty()) {
                        values.put(BlacklistTable.DISPLAY_NAME, name);
                    }

                    resolver.insert(BLACKLIST_URI, values);
                }
                cursorContact.moveToNext();
           }
        } finally {
            cursorContact.close();
        }
    }

    protected static void deleteMembers(ContentResolver resolver, String where) {
        resolver.delete(BLACKLIST_URI, where, null);
    }

    /**
     * SyncWithContactsCallback.
     */
    public interface SyncWithContactsCallback {
        /**
         * onUpdatedWithContacts.
         * @param result boolean
         */
        void onUpdatedWithContacts(boolean result);
    }

    protected static void startSyncWithContacts(ContentResolver resolver,
                                                SyncWithContactsCallback cb) {
        if (sSyncTask != null && sSyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            sSyncTask.cancel(true);
        }

        if (!mSyncCallbacks.contains(cb)) {
            Log.i("[BlacklistUtils]", "add sync callback");
            if (mSyncCallbacks.size() > 0) {
                Log.d("[BlacklistUtils]", "remove all sync callback");
                mSyncCallbacks.clear();
            }

            mSyncCallbacks.add(cb);
        }

        if (sSyncTask == null || sSyncTask.getStatus() == AsyncTask.Status.FINISHED) {
            sSyncTask = new SyncWithContactsTask();

            Log.i("[BlacklistUtils]", "Start to sync with Contacts");
            sSyncTask.execute(resolver);
        }
    }

    protected static void cancelSyncWithContacts(SyncWithContactsCallback cb) {
        if (mSyncCallbacks.contains(cb)) {
            Log.i("[BlacklistUtils]", "cancel to sync with Contacts, remove callback");
            mSyncCallbacks.remove(cb);
        }

        if (mSyncCallbacks.size() == 0) {
            if (sSyncTask != null && sSyncTask.getStatus() == AsyncTask.Status.RUNNING) {
                sSyncTask.cancel(true);
            }
        }
    }

    /**
     * SyncWithContactsTask.
     * used to sync with contacts
     */
    public static class SyncWithContactsTask extends AsyncTask<ContentResolver, Integer, Integer> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(ContentResolver... params) {
            Log.i(TAG, this, "SyncWithContactsTask doInBackground");
            Integer ret = 0;
            if (syncwithContacts(params[0])) {
                ret = 1;
            }

            return ret;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (!this.isCancelled()) {
                Log.i(TAG, this, "sync contacts done: " + result);

                for (SyncWithContactsCallback cb : mSyncCallbacks) {
                    cb.onUpdatedWithContacts(result == 1 ? true : false);
                }
            }
        }
    }

    private static boolean syncwithContacts(ContentResolver resolver) {
        boolean ret = false;
        ArrayList<String> contactsNames = new ArrayList<String>();

        Cursor blackListCusror = resolver.query(BLACKLIST_URI, BLACKLIST_PROJECTION,
                                            null, null, null);

        Log.i("[BlacklistUtils]", "syncwithContacts ++");

        if (blackListCusror == null || blackListCusror.getCount() == 0) {
            Log.d("[BlacklistUtils]", "blacklist is empty");
            blackListCusror.close();
            return ret;
        }

        try {
            //blackListCusror.moveToFirst();
            while (blackListCusror.moveToNext()) {
                String number = blackListCusror
                        .getString(blackListCusror.getColumnIndexOrThrow(BLACKLIST_PROJECTION[2]));
                String name = blackListCusror
                        .getString(blackListCusror.getColumnIndexOrThrow(BLACKLIST_PROJECTION[1]));
                String id = blackListCusror
                        .getString(blackListCusror.getColumnIndexOrThrow(BLACKLIST_PROJECTION[0]));

                Cursor contactsCursor = resolver.query(
                        Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number)),
                                new String[] {PhoneLookup.DISPLAY_NAME}, null, null, null);

                Log.i("[BlacklistUtils]", "To sync the number" + Log.formatNumber(number));
                try {
                    if (contactsCursor == null || contactsCursor.getCount() == 0) {
                        continue;
                    }

                    contactsCursor.moveToFirst();

                    contactsNames.clear();
                    while (!contactsCursor.isAfterLast()) {
                        String contactName = contactsCursor.getString(0);
                        contactsNames.add(contactName);
                        Log.d("[BlacklistUtils]", "contacts name: " + contactName);
                        contactsCursor.moveToNext();
                    }

                    if (!contactsNames.contains(name)) {
                        name = contactsNames.get(0);
                        ContentValues values = new ContentValues();
                        values.put(BLACKLIST_PROJECTION[1], name);
                        Log.d("[BlacklistUtils]", "contacts name: " + name + ", " + id);

                        resolver.update(Uri.withAppendedPath(BLACKLIST_URI, Uri.encode(id)),
                                        values, null, null);
                        ret = true;
                    }
                } finally {
                    contactsCursor.close();
                }

            }
        } finally {
            blackListCusror.close();
            Log.i("[BlacklistUtils]", "syncwithContacts --");
        }

        return ret;
    }

    /**
     * buildQueryNubmer.
     * @param number String
     * @return String
     */
    public static String buildQueryNubmer(String number) {
        StringBuilder sb = new StringBuilder();

        sb.append("\'");
        sb.append(number);
        sb.append("\'");

        Log.i("[BlacklistUtils]", "buildQueryNubmer:" + Log.formatNumber(number));

        return sb.toString();
    }

    /**
     * removeSpeicalChars.
     * @param number String
     * @return String
     */
    public static String removeSpeicalChars(String number) {
        Log.i("[BlacklistUtils]", "removeSpeicalChars, befor:" + Log.formatNumber(number));

        String ret = number.replaceAll(WHITESPACE_CHAR_EXP, "");
        ret = ret.replaceAll(SPLIT_CHARS_EXP, "");
        Log.d("[BlacklistUtils]", "removeSpeicalChars, after:" + Log.formatNumber(ret));

        return ret;
    }

    /**.
     * Query blacklist call logs
     * @param resolver resolver
     * @param uri uri
     * @param projection projection
     * @param selection selection
     * @param selectionArgs selectionArgs
     * @param sortOrder sortOrder
     * @return cursor the query result
     */
    public static Cursor queryBlackListCallLogs(ContentResolver resolver, Uri uri,
            String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {
        Cursor result = null;

        //transfer the selection to call log column
        selection = transferSelection(selection);

        StringBuilder whereSelection = new StringBuilder("Calls.TYPE=?");
        if (selection != null && !selection.isEmpty()) {
            whereSelection.append(" AND ");
            whereSelection.append(selection);
        }

        ArrayList<String> listSelection = new ArrayList<String>();
        listSelection.add(Integer.toString(MtkCallLog.Calls.AUTO_REJECT_TYPE));
        if (selectionArgs != null && selectionArgs.length != 0) {
            for (String arg: selectionArgs) {
                listSelection.add(arg);
            }
        }

        String numberSelection = "";
        if (uri.getPathSegments().size() >= 1) {
            numberSelection = Uri.decode(uri.getLastPathSegment());
            Log.i("[BlacklistUtils]", "query by number: " + Log.formatNumber(numberSelection));
        }

        if (!numberSelection.equals("")) {
            whereSelection.append(" AND ");
            whereSelection.append(Calls.NUMBER);
            whereSelection.append(" = ?");

            listSelection.add(numberSelection);
        }

        if (sortOrder == null) {
            sortOrder = BaseColumns._ID + " ASC";
        }

        int projectionLength = (projection != null && projection.length != 0) ?
            projection.length : CALLLOG_PROJECTION.length;

        // we must query the _id or je will happened
        if (projection != null && projection.length != 0 && !hasId(projection)) {
            projectionLength += 1;
        }

        //for query param
        String[] queryProjection = new String[projectionLength];
        //for matix cursor
        String[] cursorProjection = new String[projectionLength];

        if (projection == null || projection.length == 0) {
            queryProjection = CALLLOG_PROJECTION;
            for (int i = 0; i < projectionLength; i++) {
                cursorProjection[i] = tableMap.get(CALLLOG_PROJECTION[i]);
                Log.i("[BlacklistUtils]",
                        "queryBlackListCallLogs() cursorProjection[i]:" + cursorProjection[i]);
            }
        } else {
            if (projectionLength > projection.length) {
                cursorProjection[0] = TABLE_PROJECTION[0];
                queryProjection[0] = CALLLOG_PROJECTION[0];
                for (int i = 1; i < projectionLength; i++) {
                    queryProjection[i] = projectionMap.get(projection[i - 1]);
                    cursorProjection[i] = projection[i - 1];
                    Log.d("[BlacklistUtils]",
                            "queryBlackListCallLogs() queryProjection[" + i + "]:"
                            + queryProjection[i]);
                }
            } else {
                cursorProjection = projection;
                for (int i = 0; i < projectionLength; i++) {
                    queryProjection[i] = projectionMap.get(projection[i]);
                    Log.d("[BlacklistUtils]",
                        "queryBlackListCallLogs() queryProjection[" + i + "]:"
                        + queryProjection[i]);
                }
            }
        }

        Log.i("[BlacklistUtils]", "queryBlackListCallLogs() whereSelection:" +
                whereSelection.toString() + " listSelection:" + listSelection);

        Log.i("[BlacklistUtils]", "queryBlackListCallLogs() queryProjection:" +
                printArrays(queryProjection) + "cursorProjection:" +
                printArrays(cursorProjection));

        result = resolver.query(CALLLOG_URI, queryProjection, whereSelection.toString(),
            listSelection.toArray(EMPTY_ARRAY), sortOrder);

        if (result == null || result.getCount() == 0) {
            Log.i("[BlacklistUtils]", "There is no blacklist call log");
            return result;
        }

        MatrixCursor cursor = new MatrixCursor(cursorProjection);

        try {
            while (result.moveToNext()) {
                String[] itemArray = new String[queryProjection.length];
                for (int i = 0; i < queryProjection.length; i++) {
                    itemArray[i] = result.getString(result.getColumnIndexOrThrow(
                        queryProjection[i]));
                    Log.i("[BlacklistUtils]", "result: " + itemArray[i]);
                }
                cursor.addRow(itemArray);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            result.close();
        }

        return cursor;
    }

    private static boolean hasId(String[] projection) {
        for (String strItem : projection) {
            if (strItem.equals(BlackListCallLogTable.CALL_ID)) {
                return true;
            }
        }
        return false;
    }

    private static String transferSelection(String selection) {
        if (selection == null || selection.isEmpty()) {
            return selection;
        }

        String strTrasfered = selection;
        for (int i = 0; i < TABLE_PROJECTION.length; i++) {
            strTrasfered = strTrasfered.replaceAll(TABLE_PROJECTION[i],
                CALLLOG_PROJECTION[i]);
        }
        Log.i("[BlacklistUtils]", "transferSelection " + strTrasfered);
        return strTrasfered;
    }

    protected static String printArrays(String[] array) {
        StringBuilder builder = new StringBuilder();
        for (String strValue : array) {
            builder.append(strValue);
        }
        return builder.toString();
    }
}
