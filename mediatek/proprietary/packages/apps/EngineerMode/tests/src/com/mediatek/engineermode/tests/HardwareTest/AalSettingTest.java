package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.widget.Button;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.AalSetting;

public class AalSettingTest extends ActivityInstrumentationTestCase2<AalSetting> {
    private static final String TAG = "EMTest/AalSetting";
    private Solo mSolo;
    private Context mContext;
    private Instrumentation mIns;
    private Activity mActivity;

    public AalSettingTest() {
        super("com.mediatek.engineermode", AalSetting.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mIns = getInstrumentation();
        mContext = mIns.getTargetContext();
        mActivity = getActivity();
        mSolo = new Solo(mIns, mActivity);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    private void verifyPreconditions() {
        assertTrue(mIns != null);
        assertTrue(mActivity != null);
        assertTrue(mContext != null);
        assertTrue(mSolo != null);
    }

    @BasicFuncAnnotation
    public void testCase01_VerifyPreconditions() {
        verifyPreconditions();
    }

    @UiAnnotation
    public void testCase02_CheckBtn() throws Exception {
        boolean textFound;
        mSolo.assertCurrentActivity("Not AalSettingTest Class", AalSetting.class);

        mSolo.sleep(EmOperate.TIME_MID);
        mSolo.clickOnButton(0);
        assertTrue("open AAL failed",
                mSolo.waitForText("Please reboot the device to make it take effect"));
        mSolo.sleep(EmOperate.TIME_LONG);
        mSolo.clickOnButton(0);
        assertTrue("close AAL failed",
                mSolo.waitForText("Please reboot the device to make it take effect"));
        mSolo.sleep(EmOperate.TIME_LONG);
        mSolo.goBack();
    }
    /*
     * @UiAnnotation public void testCase03_CheckBtnBack() throws Exception {
     * boolean textFound;
     * mSolo.assertCurrentActivity("Not AalSettingTest Class",
     * AalSetting.class);
     *
     * Button button = mSolo.getButton(0);
     *
     * assertTrue("open AAL failed", button.getText().toString().equals("OFF"));
     *
     * mSolo.sleep(EmOperate.TIME_MID);
     *
     * mSolo.clickOnButton(0); assertTrue("close AAL failed",
     * mSolo.waitForText("Please reboot the device to make it take effect"));
     *
     * mSolo.goBack(); }
     */
}
