package com.mediatek.testprovisioning;

import java.util.ArrayList;
import java.util.Iterator;

import junit.framework.Assert;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import com.gsma.services.rcs.capability.Capabilities;
import com.jayway.android.robotium.solo.Solo;

import com.mediatek.rcse.activities.RcseSystemSettingsActivity;

public class ProvisioningTest extends ActivityInstrumentationTestCase2<RcseSystemSettingsActivity> {

    private Solo solo;

    private final static int RCS_CORE_LOADED = 0;
    private final static int RCS_CORE_FAILED = 1;
    private final static int RCS_CORE_STARTED = 2;
    private final static int RCS_CORE_STOPPED = 3;
    private final static int RCS_CORE_IMS_CONNECTED = 4;
    private final static int RCS_CORE_IMS_TRY_CONNECTION = 5;
    private final static int RCS_CORE_IMS_CONNECTION_FAILED = 6;
    private final static int RCS_CORE_IMS_TRY_DISCONNECT = 7;
    private final static int RCS_CORE_IMS_BATTERY_DISCONNECTED = 8;
    private final static int RCS_CORE_IMS_DISCONNECTED = 9;
    private final static int RCS_CORE_NOT_LOADED = 10;
    
    private final static int WAIT_TIME = 10000;
    private final static int MAX_TRY = 10;

    /**
     * Boolean value "true"
     */
    public static final String TRUE = Boolean.toString(true);

    /**
     * Boolean value "false"
     */
    public static final String FALSE = Boolean.toString(false);

    public static final String TAG = "ProvisioningTest";
    public static final int MAX_NUMBER_OF_SCROLLS = 100;

    public ProvisioningTest() {
        super(RcseSystemSettingsActivity.class);
    }

    public void setUp() throws Exception {
        solo = new Solo(getInstrumentation(), getActivity());
        Log.d(TAG, "setUp entry ");
        setServicePermissionState(true, getActivity().getApplicationContext());
    }

    @Override
    public void tearDown() throws Exception {
        Log.d(TAG, "tearDown entry ");
        solo.finishOpenedActivities();
    }
    
    /*
    * Auto test to test Configuration.
    */
    public void testConfiguration() {

        Log.d(TAG, "testServiceConfiguration entry ");
        solo.assertCurrentActivity("wrong activity", RcseSystemSettingsActivity.class);
        boolean state = false;
        int currentState = RCS_CORE_NOT_LOADED;
        boolean stateSwitch = false;
        // View switchView = solo.getView("com.mediatek.rcs.R.id.switchWidget");
        // Log.d(TAG, switchView.toString());
        android.view.View view = solo.getView("switchWidget");

        android.widget.Switch switch1 = (android.widget.Switch) view;

        stateSwitch = switch1.isChecked();

        /*
         * ArrayList<android.view.View> views = solo.getCurrentViews();
         * 
         * Iterator<android.view.View> iterator = views.iterator();
         * 
         * while (iterator.hasNext()) { Log.d(TAG, iterator.next().toString());
         * // System.out.println(iterator.next()); }
         */

        if (stateSwitch == false) {
            Log.d(TAG, "testServiceConfiguration ON not found ");

            Log.d(TAG, "testServiceConfiguration OFF found ");
            // switch1.toggle();
            // switch1.setChecked(true);
            solo.clickOnView(view);

            solo.sleep(3000);
            state = getServiceState(getActivity().getApplicationContext());
            Log.d(TAG, "Service State " + state);
            if (!state) {
                for(int i=0; i<MAX_TRY;i++) {
                    solo.sleep(WAIT_TIME);
                    state = getServiceState(getActivity().getApplicationContext());
                    if(state) {
                        break;
                    }
                }
                
            }
            state = getServiceState(getActivity().getApplicationContext());
            Log.d(TAG, "Service State2 " + state);
            Log.d(TAG, "testServiceConfiguration OFF also not found ");

        } else {
            Log.d(TAG, "testServiceConfiguration ON found check with setting ");
            state = getServiceState(getActivity().getApplicationContext());
            if (!state) {
                Log.d(TAG, "testServiceConfiguration ON found Waiting for status ");
                for(int i=0; i<MAX_TRY;i++) {
                    solo.sleep(WAIT_TIME);
                    state = getServiceState(getActivity().getApplicationContext());
                    if(state) {
                        break;
                    }
                }
            }
            state = getServiceState(getActivity().getApplicationContext());
        }

        assertTrue(state);
    }

    /*
    * Auto test to test Registration.
    */
    public void testRegistration() {

        Log.d(TAG, "testServiceRegistartion entry ");
        solo.assertCurrentActivity("wrong activity", RcseSystemSettingsActivity.class);
        
        boolean state = false;
        int currentState = RCS_CORE_NOT_LOADED;
        boolean stateSwitch = false;
        // View switchView = solo.getView("com.mediatek.rcs.R.id.switchWidget");
        // Log.d(TAG, switchView.toString());
        android.view.View view = solo.getView("switchWidget");

        android.widget.Switch switch1 = (android.widget.Switch) view;

        stateSwitch = switch1.isChecked();

        if (stateSwitch == false) {
            Log.d(TAG, "testServiceRegistration Switch is off  ");
            // switch1.toggle();
            // switch1.setChecked(true);
            solo.clickOnView(view);
            solo.sleep(3000);
            state = getServiceState(getActivity().getApplicationContext());
            Log.d(TAG, "Service State " + state);
            if (!state) {
                Log.d(TAG, "testServiceRegistration Waiting for configuration to complete");
                for(int i=0; i<MAX_TRY;i++) {
                    solo.sleep(WAIT_TIME);
                    state = getServiceState(getActivity().getApplicationContext());
                    if(state) {
                        break;
                    }
                }
            }
            state = getServiceState(getActivity().getApplicationContext());
            // Log.d(TAG, "Service State2 " + state);
            Log.d(TAG, "testServiceRegistration is configuration done " + state);
            if (state) {
                Log.d(TAG, "testServiceRegistration check registration status ");
                currentState = getServiceCurrentState(getActivity().getApplicationContext());
                if (currentState!=RCS_CORE_IMS_CONNECTED) {
                    Log.d(TAG, "testServiceRegistration  Current State  " + currentState);
                    for(int i=0; i<MAX_TRY;i++) {
                        solo.sleep(WAIT_TIME);
                        currentState = getServiceCurrentState(getActivity().getApplicationContext());
                        if(currentState == RCS_CORE_IMS_CONNECTED) {
                            break;
                        }
                    }
                    currentState = getServiceCurrentState(getActivity().getApplicationContext());
                    Log.d(TAG, "testServiceRegistration  Current State  " + currentState);
                    if (currentState == RCS_CORE_IMS_TRY_CONNECTION) {
                        for(int i=0; i<MAX_TRY;i++) {
                            solo.sleep(WAIT_TIME);
                            currentState = getServiceCurrentState(getActivity().getApplicationContext());
                            if(currentState == RCS_CORE_IMS_CONNECTED) {
                                break;
                            }
                        }
                        currentState = getServiceCurrentState(getActivity().getApplicationContext());
                        Log.d(TAG, "testServiceRegistration  Current State  " + currentState);
                    }
                }
            }
        } else {
            Log.d(TAG, "testServiceRegistration Switch is ON check for Current Service State ");
            state = getServiceState(getActivity().getApplicationContext());
            if (!state) {
                Log.d(TAG, "testServiceRegistration Wait for configuration to happen ");
                for(int i=0; i<MAX_TRY;i++) {
                    solo.sleep(WAIT_TIME);
                    state = getServiceState(getActivity().getApplicationContext());
                    if(state) {
                        break;
                    }
                }
            }
            state = getServiceState(getActivity().getApplicationContext());
            if (state) {
                Log.d(TAG, "testServiceRegistration check registration status ");
                currentState = getServiceCurrentState(getActivity().getApplicationContext());
                if (currentState!=RCS_CORE_IMS_CONNECTED) {
                    Log.d(TAG, "testServiceRegistration  Current State  " + currentState);
                    for(int i=0; i<MAX_TRY;i++) {
                        solo.sleep(WAIT_TIME);
                        Log.d(TAG, "testServiceRegistration  Current State  " + currentState);
                        currentState = getServiceCurrentState(getActivity().getApplicationContext());
                        if(currentState == RCS_CORE_IMS_CONNECTED) {
                            break;
                        }
                    }
                    currentState = getServiceCurrentState(getActivity().getApplicationContext());
                    if (currentState == RCS_CORE_IMS_TRY_CONNECTION) {
                        for(int i=0; i<MAX_TRY;i++) {
                            solo.sleep(WAIT_TIME);
                            currentState = getServiceCurrentState(getActivity().getApplicationContext());
                            if(currentState == RCS_CORE_IMS_CONNECTED) {
                                break;
                            }
                        }
                        currentState = getServiceCurrentState(getActivity().getApplicationContext());
                    }
                }
            }
        }
        boolean result = currentState == RCS_CORE_IMS_CONNECTED;
        Log.d(TAG, "testServiceRegistration check registration status "+ result);
        assertTrue(currentState == RCS_CORE_IMS_CONNECTED);

    }

    /**
     * Set service Permission
     * 
     * @param permission
     *            state true
     */
    public static void setServicePermissionState(boolean state, Context ctx) {
        String stringState = "";
        if (state) {
            stringState = TRUE;
        } else {
            stringState = FALSE;
        }
        Uri databaseUri = Uri.parse("content://com.orangelabs.rcs.settings/settings");
        if (ctx != null) {
            ContentResolver cr = ctx.getContentResolver();
            ContentValues values = new ContentValues();
            values.put("value", stringState);
            String where = "key" + "='" + "servicePermitted" + "'";
            // long startTime = System.currentTimeMillis();
            if (databaseUri != null) {
                cr.update(databaseUri, values, where, null);
            }
        }
    }

    /**
     * Service Activation State
     * 
     * @param ctx
     * @return String Service State
     */
    public static boolean getServiceState(Context ctx) {
        boolean result = false;
        Uri databaseUri = Uri.parse("content://com.orangelabs.rcs.settings/settings");
        ContentResolver cr = ctx.getContentResolver();
        Cursor c = cr.query(databaseUri, null, "key" + "='" + "ServiceActivated" + "'", null, null);
        if (c != null) {
            if ((c.getCount() > 0) && c.moveToFirst()) {
                String value = c.getString(2);
                result = Boolean.parseBoolean(value);
            }
            c.close();
        }
        return result;
    }

    /**
     * Service Activation State
     * 
     * @param ctx
     * @return String Service State
     */
    public static int getServiceCurrentState(Context ctx) {
        int result = RCS_CORE_NOT_LOADED;
        Uri databaseUri = Uri.parse("content://com.orangelabs.rcs.settings/settings");
        ContentResolver cr = ctx.getContentResolver();
        Cursor c = cr.query(databaseUri, null, "key" + "='" + "CurrentServiceState" + "'", null, null);
        if (c != null) {
            if ((c.getCount() > 0) && c.moveToFirst()) {
                String value = c.getString(2);
                try {
                    result = Integer.parseInt(value);
                } catch (Exception e) {
                }
            }
            c.close();
        }
        return result;
    }

}
