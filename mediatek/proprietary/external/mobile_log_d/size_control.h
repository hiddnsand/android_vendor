#ifndef SIZE_CONTROL_H
#define SIZE_CONTROL_H
#include "bootmode.h"

struct folder_util {
	char name[255];
	long long size; //KB
	struct folder_util *next;
};

unsigned long read_folder_index(sd_type_t log_sd_type);
int update_folder_list(char *new_folder, unsigned long idx, sd_type_t log_sd_type);

#endif
