package com.mediatek.op01.telephony;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemProperties;
import android.telephony.Rlog;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;

import com.android.internal.telephony.SubscriptionController;
import com.mediatek.internal.telephony.datasub.DataSubSelectorUtil;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;
import com.mediatek.internal.telephony.datasub.DataSubSelector;
import com.mediatek.internal.telephony.datasub.DataSubSelectorUtil;
import com.mediatek.internal.telephony.datasub.ISimSwitchForDSSExt;
import com.mediatek.internal.telephony.datasub.CapabilitySwitch;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;

import java.util.Arrays;

import static com.mediatek.internal.telephony.datasub.DataSubConstants.*;

public class Op01SimSwitchForDSSExt implements ISimSwitchForDSSExt {
    private static String LOG_TAG = "SimSwitchOP01";

    private static boolean DBG = true;

    private static DataSubSelector mDataSubSelector = null;

    private static int mPhoneNum = 0;
    protected static SharedPreferences mSimSwitchPreference;
    private static final String PREF_CATEGORY_RADIO_STATUS = "RADIO_STATUS";

    private Context mContext = null;

    public Op01SimSwitchForDSSExt(Context context) {
        mContext = context;
    }

    public void init(DataSubSelector dataSubSelector) {
        mDataSubSelector = dataSubSelector;
        mPhoneNum = mDataSubSelector.getPhoneNum();
        mSimSwitchPreference = mContext.getSharedPreferences(PREF_CATEGORY_RADIO_STATUS, 0);
    }

    public boolean checkCapSwitch(int policy) {
        return false;
    }

    public boolean checkCapSwitch() {
        return false;
    }

    public int isNeedSimSwitch() {
        int[] simOpInfo = new int[mPhoneNum];
        int[] simType = new int[mPhoneNum];
        int targetPhoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        int insertedSimCount = 0;
        int insertedStatus = 0;
        String[] currIccId = new String[mPhoneNum];
        int[] priority = new int[mPhoneNum];
        int enabledSimCount = 0;
        int enabledPhoneId = -1;
        boolean hasOp01Sim = false;
        String curr3GSim = SystemProperties.get(PROPERTY_3G_SIM, "");
        int curr3GPhoneId = -1;
        int defDataSubId = SubscriptionController.getInstance().getDefaultDataSubId();
        int defDataPhoneId = SubscriptionManager.getPhoneId(defDataSubId);

        if (curr3GSim != null && !curr3GSim.equals("")) {
            curr3GPhoneId = Integer.parseInt(curr3GSim) - 1;
        }

        for (int i = 0; i < mPhoneNum; i++) {
            currIccId[i] = DataSubSelectorUtil.getIccidFromProp(i);
            if (currIccId[i] == null || "".equals(currIccId[i])) {
                log("isNeedSimSwitchForOp01, iccid not found, wait for next sub ready");
                return SIM_SWITCH_UNKNOWN;
            }
            if (!NO_SIM_VALUE.equals(currIccId[i])) {
                ++insertedSimCount;
                insertedStatus = insertedStatus | (1 << i);
                if (mSimSwitchPreference.contains(currIccId[i]) == false) {
                    ++enabledSimCount;
                    enabledPhoneId = i;
                } else {
                    log("isNeedSimSwitchForOp01, phone" + i + " is in off state");
                }
            } else {
                log("isNeedSimSwitchForOp01, sim" + i + ": no SIM");
            }
        }
        log("isNeedSimSwitchForOp01, curr3GSim = SIM" + curr3GSim + ", 3GPhoneId = " + curr3GPhoneId
            + ", enabledSimCount = " + enabledSimCount + ", insertedSimCount = " + insertedSimCount
            + ", insertedStatus = " + insertedStatus + ", mPhoneNum = " + mPhoneNum);

        if (RadioCapabilitySwitchUtil.getSimInfo(simOpInfo, simType, insertedStatus) == false) {
            log("isNeedSimSwitchForOp01, fail to get sim info");
            return SIM_SWITCH_UNKNOWN;
        }

        // only one enabled sim
        if (enabledSimCount == 1) {
            if (enabledPhoneId != curr3GPhoneId) {
                log("isNeedSimSwitchForOp01, only one enabled sim:" + enabledPhoneId);
                return SIM_SWITCH_NEEDED;
            }
        } else if (insertedSimCount == 0){
            log("isNeedSimSwitchForOp01, no sim cards");
            return SIM_SWITCH_UNKNOWN;
        } else {
            // dump op info
            log("isNeedSimSwitchForOp01, simOpInfo:" + Arrays.toString(simOpInfo));

            for (int i = 0; i < mPhoneNum; i++) {
                if ((simOpInfo[i] == RadioCapabilitySwitchUtil.SIM_OP_INFO_OP01) &&
                    (mSimSwitchPreference.contains(currIccId[i]) == false)) {
                    hasOp01Sim = true;
                    break;
                }
            }

            // main capability is on other sim
            if ((hasOp01Sim) && (curr3GPhoneId >= 0) && (curr3GPhoneId < mPhoneNum) &&
                (simOpInfo[curr3GPhoneId] != RadioCapabilitySwitchUtil.SIM_OP_INFO_OP01)) {
                log("isNeedSimSwitchForOp01, main capability is on other sim");
                return SIM_SWITCH_NEEDED;
            }

            // sync with default data
            if (RadioCapabilitySwitchUtil.getMainCapabilityPhoneId() != defDataPhoneId) {
                return SIM_SWITCH_NEEDED;
            }
        }

        return SIM_SWITCH_NO_NEED;
    }

    static private void log(String txt) {
        if (DBG) {
            Rlog.d(LOG_TAG, txt);
        }
    }

    static private void loge(String txt) {
        if (DBG) {
            Rlog.e(LOG_TAG, txt);
        }
    }
}