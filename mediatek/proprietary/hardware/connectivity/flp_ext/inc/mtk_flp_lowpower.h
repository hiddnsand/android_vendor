#ifndef MTK_FLP_LOWPOWER_H
#define MTK_FLP_LOWPOWER_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef K2_PLATFORM
#include "mtk_flp_connsys_sys.h"
#else
#include "mtk_flp_sys.h"
#endif

#define FLP_VERSION_HEAD 'F','L','P','_','V','E','R','_'
#define FLP_MAJOR_VERSION '1','6','1','1','2','1','0','2'  // y,y,m,m,d,d,rev,rev
#define FLP_BRANCH_INFO '_','0','.','9','9','_'
#define FLP_MINER_VERSION '0','1'
#define FLP_VER_INFO FLP_VERSION_HEAD,FLP_MAJOR_VERSION,FLP_BRANCH_INFO,FLP_MINER_VERSION

#define MTK_FLP_ZPG_FAR_DISTANCE 1000
#define MTK_FLP_ZPG_NORMAL_SEC 60
#define MTK_FLP_ZPG_NORMAL_ACC 15
#define MTK_FLP_ZPG_MAX_SMART_SEC 300
#define MTK_FLP_ZPG_MAX_TIMEOUT_SEC 600
#define MTK_FLP_ZPG_NTF_PERIOD_THRESHOLD_SEC 8
#define MTK_FLP_ZPG_INSIDE_UNKNOWN_SEC 30

int mtk_flp_lowpower_main(MTK_FLP_MSG_T *prmsg);
int mtk_flp_lowpower_set_latest_loc(MTK_FLP_LOCATION_T *location);
INT32 mtk_flp_lowpower_query_mode ();
void mtk_flp_lowpower_get_location_status (INT32 *flp_mode, INT32 *read_num);
void mtk_flp_lowpower_set_mode (INT32 flp_mode, INT32 read_num);
int mtk_flp_lowpower_get_latest_loc(MTK_FLP_LOCATION_T *location);
int mtk_flp_lowpower_query_start_elapsed_time(void);
int mtk_flp_lowpower_set_latest_gnss_loc(MTK_FLP_LOCATION_T *location);
int mtk_flp_lowpower_get_latest_gnss_loc(MTK_FLP_LOCATION_T *location);
void mtk_flp_lowpower_clear_latest_gnss_info();
int mtk_flp_lowpower_set_latest_pdr_in(MTK_FLP_PDR_INPUT *pdr_in);
int mtk_flp_lowpower_get_latest_pdr_in(MTK_FLP_PDR_INPUT *pdr_out);
void mtk_flp_lowpower_clear_latest_pdr_info();
FLP_VOID mtk_flp_lowpower_check_loc_available_expired ();
UINT32 mtk_flp_lowpower_query_source ();
void mtk_flp_lowpower_source_decision ();
void mtk_flp_lowpower_multiple_on_off(int module_type, MTK_FLP_MSG_T *flp_msg);
void mtk_flp_lowpower_update_option(MTK_FLP_BATCH_OPTION_T *option);
void mtk_flp_lowpower_smart_geofence_entry(INT32 mode, UINT32 duration_sec);
void mtk_flp_lowpower_smart_geofence_exit();
UINT32 mtk_flp_lowpower_get_geofence_normal_sec();

#ifdef __cplusplus
   }  /* extern "C" */
#endif

#endif
