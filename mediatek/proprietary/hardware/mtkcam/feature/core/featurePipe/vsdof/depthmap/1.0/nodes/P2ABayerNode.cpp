/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

 // Standard C header file

// Android system/core header file
#include <sync/sync.h>
// mtkcam custom header file
#include <camera_custom_stereo.h>
// mtkcam global header file
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/aaa/aaa_hal_common.h>
#include <libion_mtk/include/ion.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
// Module header file

// Local header file
#include "P2ABayerNode.h"
#include "../DepthMapPipe_Common.h"
#include "../bufferPoolMgr/BaseBufferHandler.h"

// Logging
#undef PIPE_CLASS_TAG
#define PIPE_CLASS_TAG "P2ABayerNode"
#include <featurePipe/core/include/PipeLog.h>

/*******************************************************************************
* Namespace start.
********************************************************************************/
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe_DepthMap {

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Instantiation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
P2ABayerNode::
P2ABayerNode(
    const char *name,
    DepthMapPipeNodeID nodeID,
    PipeNodeConfigs config
)
: DepthMapPipeNode(name, nodeID, config)
, miSensorIdx_Main1(config.mpSetting->miSensorIdx_Main1)
, miSensorIdx_Main2(config.mpSetting->miSensorIdx_Main2)
{
    this->addWaitQueue(&mRequestQue);
}

P2ABayerNode::
~P2ABayerNode()
{
    MY_LOGD("[Destructor]");
}

MVOID
P2ABayerNode::
cleanUp()
{
    MY_LOGD("+");
    if(mpINormalStream != nullptr)
    {
        mpINormalStream->uninit(getName());
        mpINormalStream->destroyInstance();
        mpINormalStream = nullptr;
    }

    if(mp3AHal_Main1)
    {
        mp3AHal_Main1->destroyInstance(getName());
        mp3AHal_Main1 = nullptr;
    }
    MY_LOGD("-");
}

MBOOL
P2ABayerNode::
onInit()
{
    VSDOF_INIT_LOG("+");
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
P2ABayerNode::
onUninit()
{
    VSDOF_INIT_LOG("+");
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
P2ABayerNode::
onThreadStart()
{
    CAM_TRACE_NAME("P2ABayerNode::onThreadStart");
    VSDOF_INIT_LOG("+");
    // Create NormalStream
    VSDOF_LOGD("NormalStream create instance: idx=%d", miSensorIdx_Main1);
    CAM_TRACE_BEGIN("P2ABayerNode::NormalStream::createInstance+init");
    mpINormalStream = NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(miSensorIdx_Main1);

    if (mpINormalStream == nullptr)
    {
        MY_LOGE("mpINormalStream create instance for P2AFM Node failed!");
        cleanUp();
        return MFALSE;
    }
    mpINormalStream->init(getName());
    CAM_TRACE_END();
    // create MDP stream
    mpAsynBlitStream = new DpAsyncBlitStream();
    // 3A: create instance
    // UT does not test 3A
    CAM_TRACE_BEGIN("P2ABayerNode::create_3A_instance");
    #ifndef GTEST
    mp3AHal_Main1 = MAKE_Hal3A(miSensorIdx_Main1, getName());
    MY_LOGD("3A create instance, Main1: %x Main2: %x", mp3AHal_Main1);
    #endif
    CAM_TRACE_END();

    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
P2ABayerNode::
onThreadStop()
{
    CAM_TRACE_NAME("P2ABayerNode::onThreadStop");
    VSDOF_INIT_LOG("+");
    cleanUp();
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
P2ABayerNode::
onData(DataID data, DepthMapRequestPtr &request)
{
  MBOOL ret = MTRUE;
  VSDOF_LOGD("+ : reqID=%d", request->getRequestNo());
  CAM_TRACE_NAME("P2ABayerNode::onData");

  switch(data)
  {
  case BAYER_ENQUE:
    VSDOF_PRFLOG("+ : reqID=%d size=%d", request->getRequestNo(), mRequestQue.size());
    mRequestQue.enque(request);
    break;
  default:
    MY_LOGW("Un-recognized data ID, id=%d reqID=%d", data, request->getRequestNo());
    ret = MFALSE;
    break;
  }

  VSDOF_LOGD("-");
  return ret;
}

AAATuningResult
P2ABayerNode::
applyISPTuning(
    DepthMapRequestPtr& rpRequest
)
{
    CAM_TRACE_NAME("P2ABayerNode::applyISPTuning");
    VSDOF_LOGD("+, reqID=%d", rpRequest->getRequestNo());
    AAATuningResult result;
    sp<BaseBufferHandler> pBufferHandler = rpRequest->getBufferHandler();
    MVOID* pTuningBuf = pBufferHandler->requestTuningBuf(getNodeId(), BID_P2A_TUNING1);
    result.tuningResult = {nullptr, nullptr};
    result.tuningResult.pRegBuf = pTuningBuf;
    result.tuningBuffer = pTuningBuf;

    // get APP/HAL meta
    IMetadata* pMeta_InApp  = pBufferHandler->requestMetadata(getNodeId(), BID_META_IN_APP);
    IMetadata* pMeta_InHal  = pBufferHandler->requestMetadata(getNodeId(), BID_META_IN_HAL_MAIN1);
    // pack into MetaSet_T
    MetaSet_T inMetaSet(*pMeta_InApp, *pMeta_InHal);
    // get raw type
    INPUT_RAW_TYPE rawType = mpFlowOption->getInputRawType(rpRequest, eP2APATH_MAIN1);
    // USE resize raw-->set PGN 0
    if(rawType == eRESIZE_RAW)
        updateEntry<MUINT8>(&(inMetaSet.halMeta), MTK_3A_PGN_ENABLE, 0);
    else
        updateEntry<MUINT8>(&(inMetaSet.halMeta), MTK_3A_PGN_ENABLE, 1);

    // UT do not test setIsp
    #ifndef GTEST
    // 3A result
    MetaSet_T resultMeta;
    mp3AHal_Main1->setIsp(0, inMetaSet, &result.tuningResult, nullptr);
    #endif
    VSDOF_LOGD("-, reqID=%d", rpRequest->getRequestNo());
    return result;
}

MBOOL
P2ABayerNode::
perform3AIspTuning(
    DepthMapRequestPtr& rpRequest,
    Stereo3ATuningRes& rOutTuningRes
)
{
    // only main1
    rOutTuningRes.tuningRes_main1 = applyISPTuning(rpRequest);
    return MTRUE;
}

MBOOL
P2ABayerNode::
onThreadLoop()
{
    DepthMapRequestPtr request;

    if( !waitAllQueue() )
    {
        return MFALSE;
    }

    if( !mRequestQue.deque(request) )
    {
        MY_LOGE("mRequestQue.deque() failed");
        return MFALSE;
    }

    CAM_TRACE_NAME("P2ABayerNode::onThreadLoop");
    // mark on-going-request start
    this->incExtThreadDependency();

    VSDOF_PRFLOG("threadLoop start, reqID=%d eState=%d",
                    request->getRequestNo(), request->getRequestAttr().opState);

    // enque QParams
    QParams enqueParams;
    // enque cookie instance
    EnqueCookieContainer *pCookieIns = new EnqueCookieContainer(request, this);
    // apply 3A Isp tuning
    Stereo3ATuningRes tuningRes;
    MBOOL bRet = perform3AIspTuning(request, tuningRes);

    if(!bRet)
        goto lbExit;

    // call flow option to build QParams
    bRet = mpFlowOption->buildQParam_Bayer(request, tuningRes, enqueParams);
    //
    debugQParams(enqueParams);
    if(!bRet)
    {
        MY_LOGE("Failed to build P2 enque parametes.");
        goto lbExit;
    }

    // callback
    enqueParams.mpfnCallback = onP2Callback;
    enqueParams.mpfnEnQFailCallback = onP2FailedCallback;
    enqueParams.mpCookie = (MVOID*) pCookieIns;
    // start P2A timer
    request->mTimer.startP2ABayer();
    request->mTimer.startP2ABayerEnque();
    // enque
    CAM_TRACE_BEGIN("P2ABayerNode::NormalStream::enque");
    bRet = mpINormalStream->enque(enqueParams);
    CAM_TRACE_END();
    // stop P2A Enque timer
    request->mTimer.stopP2ABayerEnque();
    VSDOF_PRFTIME_LOG("mpINormalStream enque end! reqID=%d, exec-time(enque)=%d msec",
                    request->getRequestNo(), request->mTimer.getElapsedP2ABayerEnque());
    if(!bRet)
    {
        MY_LOGE("mpINormalStream enque failed! reqID=%d", request->getRequestNo());
        goto lbExit;
    }
    return MTRUE;

lbExit:
    delete pCookieIns;
    // mark on-going-request end
    this->decExtThreadDependency();
    return MFALSE;

}

MVOID
P2ABayerNode::
onP2Callback(QParams& rParams)
{
    EnqueCookieContainer* pEnqueData = (EnqueCookieContainer*) (rParams.mpCookie);
    P2ABayerNode* pP2ABayerNode = (P2ABayerNode*) (pEnqueData->mpNode);
    pP2ABayerNode->handleP2Done(rParams, pEnqueData);
}

MVOID
P2ABayerNode::
onP2FailedCallback(QParams& rParams)
{
    MY_LOGE("P2A operations failed!!Check the following log:");
    EnqueCookieContainer* pEnqueData = (EnqueCookieContainer*) (rParams.mpCookie);
    P2ABayerNode* pP2ABayerNode = (P2ABayerNode*) (pEnqueData->mpNode);
    debugQParams(rParams);
    pP2ABayerNode->handleData(ERROR_OCCUR_NOTIFY, pEnqueData->mRequest);
    // launch onProcessDone
    pEnqueData->mRequest->getBufferHandler()->onProcessDone(pP2ABayerNode->getNodeId());
    // mark on-going-request end
    pP2ABayerNode->decExtThreadDependency();
    delete pEnqueData;
}

MVOID
P2ABayerNode::
handleP2Done(QParams& rParams, EnqueCookieContainer* pEnqueCookie)
{
    CAM_TRACE_NAME("P2ABayerNode::handleP2Done");
    DepthMapRequestPtr pRequest = pEnqueCookie->mRequest;
    // check flush status
    if(mpNodeSignal->getStatus(NodeSignal::STATUS_IN_FLUSH))
        goto lbExit;
    // stop timer
    pRequest->mTimer.stopP2ABayer();
    VSDOF_PRFTIME_LOG("+ :reqID=%d , p2 exec-time=%d ms", pRequest->getRequestNo(), pRequest->mTimer.getElapsedP2ABayer());
    // handle flow type task
    if(!onHandleFlowTypeP2Done(pRequest))
    {
        MY_LOGE("onHandleFlowTypeP2Done failed!");
        return;
    }
    // perform resize when capture
    if(pRequest->getRequestAttr().opState == eSTATE_CAPTURE)
    {
        if(!generateMDPResizedMYSLs(pRequest))
            MY_LOGE("reqID=%d Failed to generateMDPResizedMYSLs!!", pRequest->getRequestNo());
    }
    // launch flow option p2 done
    mpFlowOption->onP2ProcessDone_Bayer(this, pRequest);
lbExit:
    // launch onProcessDone
    pRequest->getBufferHandler()->onProcessDone(getNodeId());
    delete pEnqueCookie;
    VSDOF_PRFLOG("- :reqID=%d", pRequest->getRequestNo());
    // mark on-going-request end
    this->decExtThreadDependency();
}

MBOOL
P2ABayerNode::
generateMDPResizedMYSLs(
    DepthMapRequestPtr& rEffectReqPtr)
{
    VSDOF_LOGD("+ reqID=%d", rEffectReqPtr->getRequestNo());
    DepthMapPipeNodeID nodeID = eDPETHMAP_PIPE_NODEID_P2ABAYER;
    sp<BaseBufferHandler> pBufferHandler = rEffectReqPtr->getBufferHandler();
    IImageBuffer *pImgBuf_MYSLL = nullptr;
    IImageBuffer *pImgBuf_MYSL = nullptr;
    IImageBuffer *pImgBuf_MYS = nullptr;
    // MDP src buffer: MYSLL
    MBOOL bRet = pBufferHandler->getEnqueBuffer(getNodeId(), BID_P2A_OUT_MY_SLL, pImgBuf_MYSLL);
    // MDP output buffer: MY_S
    pImgBuf_MYS = pBufferHandler->requestBuffer(nodeID, BID_P2A_OUT_MY_S);
    // MDP output buffer: MY_SL
    pImgBuf_MYSL = pBufferHandler->requestBuffer(nodeID, BID_P2A_OUT_MY_SL);
    if(!bRet || pImgBuf_MYS == nullptr || pImgBuf_MYSL == nullptr)
    {
        MY_LOGE("Failed to retrieve in/out buffers, bRet=%d, MYS=%x MYSL=%x",
                bRet, pImgBuf_MYS, pImgBuf_MYSL);
        return MFALSE;
    }
    // create MDP job
    uint32_t jobID;
    int32_t fenceID;

    mpAsynBlitStream->createJob(jobID, fenceID);
    mpAsynBlitStream->setConfigBegin(jobID);
    {
        // src
        MINTPTR pVAList[3] = {pImgBuf_MYSLL->getBufVA(0),
                            pImgBuf_MYSLL->getBufVA(1),
                            pImgBuf_MYSLL->getBufVA(2)};

        MINTPTR pMVAList[3] = {pImgBuf_MYSLL->getBufPA(0),
                            pImgBuf_MYSLL->getBufPA(1),
                            pImgBuf_MYSLL->getBufPA(2)};

        uint32_t pSizeList[3] = {(uint32_t)pImgBuf_MYSLL->getBufSizeInBytes(0),
                                (uint32_t)pImgBuf_MYSLL->getBufSizeInBytes(1),
                                (uint32_t)pImgBuf_MYSLL->getBufSizeInBytes(2)};

        mpAsynBlitStream->setSrcBuffer((void**)pVAList, (void**)pMVAList,
                                        pSizeList, pImgBuf_MYSLL->getPlaneCount());
        mpAsynBlitStream->setSrcConfig(pImgBuf_MYSLL->getImgSize().w,
                                        pImgBuf_MYSLL->getImgSize().h,
                                        mapToDpColorFmt(pImgBuf_MYSLL->getImgFormat()));
        // output: MYS
        MINTPTR pVAList_MYS[3] = {pImgBuf_MYS->getBufVA(0),
                            pImgBuf_MYS->getBufVA(1),
                            pImgBuf_MYS->getBufVA(2)};

        MINTPTR pMVAList_MYS[3] = {pImgBuf_MYS->getBufPA(0),
                            pImgBuf_MYS->getBufPA(1),
                            pImgBuf_MYS->getBufPA(2)};

        uint32_t pSizeList_MYS[3] = {(uint32_t)pImgBuf_MYS->getBufSizeInBytes(0),
                                (uint32_t)pImgBuf_MYS->getBufSizeInBytes(1),
                                (uint32_t)pImgBuf_MYS->getBufSizeInBytes(2)};

        mpAsynBlitStream->setDstBuffer(0, (void**)pVAList_MYS, (void**)pMVAList_MYS ,pSizeList_MYS,
                                        pImgBuf_MYS->getPlaneCount());
        mpAsynBlitStream->setDstConfig(0, pImgBuf_MYS->getImgSize().w,
                                        pImgBuf_MYS->getImgSize().h,
                                        mapToDpColorFmt(pImgBuf_MYS->getImgFormat()));
        // output: MYSL
        MINTPTR pVAList_MYSL[3] = {pImgBuf_MYSL->getBufVA(0),
                            pImgBuf_MYSL->getBufVA(1),
                            pImgBuf_MYSL->getBufVA(2)};

        uint32_t pMVAList_MYSL[3] = {(uint32_t)pImgBuf_MYSL->getBufPA(0),
                                (uint32_t)pImgBuf_MYSL->getBufPA(1),
                                (uint32_t)pImgBuf_MYSL->getBufPA(2)};

        uint32_t pSizeList_MYSL[3] = {(uint32_t)pImgBuf_MYSL->getBufSizeInBytes(0),
                                (uint32_t)pImgBuf_MYSL->getBufSizeInBytes(1),
                                (uint32_t)pImgBuf_MYSL->getBufSizeInBytes(2)};

        mpAsynBlitStream->setDstBuffer(1, (void**)pVAList_MYSL, (void**)pMVAList_MYSL,
                                        pSizeList_MYSL, pImgBuf_MYSL->getPlaneCount());
        mpAsynBlitStream->setDstConfig(1, pImgBuf_MYSL->getImgSize().w,
                                        pImgBuf_MYSL->getImgSize().h,
                                        mapToDpColorFmt(pImgBuf_MYSL->getImgFormat()));
    }
    mpAsynBlitStream->setConfigEnd();
    mpAsynBlitStream->invalidate();
    // wait for fence
    sync_wait(fenceID, -1);
    close(fenceID);

    VSDOF_LOGD("-");
    return MTRUE;
}

DpColorFormat
P2ABayerNode::
mapToDpColorFmt(MINT format)
{
    switch(format)
    {
        case eImgFmt_YV12:
            return DP_COLOR_YV12;
        default:
            MY_LOGE("No support format:%d", format);
    }
    return DP_COLOR_UNKNOWN;
}


MBOOL
P2ABayerNode::
onHandleFlowTypeP2Done(
    sp<DepthMapEffectRequest> pRequest
)
{
    // only for BAYER_AND_BAYER
    if(mpPipeOption->mSensorType == v1::Stereo::BAYER_AND_MONO &&
        pRequest->isQueuedDepthRequest(mpPipeOption))
    {
        sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();
        IMetadata* pOutHalMeta;
        // get queue depth info
        DepthBufferInfo depthInfo;
        if(mpDepthStorage->pop_front(depthInfo)) {
            mLastQueueDepthInfo = depthInfo;
            VSDOF_LOGD("reqID=%d use queued depth info!",
                        pRequest->getRequestNo());
        }else
        {
            depthInfo = mLastQueueDepthInfo;
            VSDOF_LOGD("reqID=%d, no queued depth, use last depth info!",
                        pRequest->getRequestNo());
        }

        // check depthmap is not ready, notify and return
        if(depthInfo.mpDepthBuffer.get() == nullptr &&
            mpPipeOption->mFeatureMode == eDEPTHNODE_MODE_VSDOF)
        {
            VSDOF_LOGD("reqID=%d, depth not ready!", pRequest->getRequestNo());
            handleData(REQUEST_DEPTH_NOT_READY, pRequest);
            goto lbExit;
        }

        // config queued Depth buffer as output buffer to GF Node
        pBufferHandler->configExternalOutBuffer(getNodeId(), BID_WMF_OUT_DMW,
                                                eDPETHMAP_PIPE_NODEID_GF, depthInfo.mpDepthBuffer);
        // config MY_S buffer
        pBufferHandler->configOutBuffer(getNodeId(), BID_P2A_OUT_MY_S, eDPETHMAP_PIPE_NODEID_GF);
        // write meta
        {
            pOutHalMeta = pRequest->getBufferHandler()->requestMetadata(getNodeId(), BID_META_OUT_HAL);
            trySetMetadata<MFLOAT>(pOutHalMeta, MTK_CONVERGENCE_DEPTH_OFFSET, depthInfo.mfConvOffset);
        }
        // set meta ready
        pRequest->setOutputBufferReady(BID_META_OUT_APP);
        pRequest->setOutputBufferReady(BID_META_OUT_HAL);
        pRequest->setOutputBufferReady(BID_META_OUT_APP_QUEUED);
        pRequest->setOutputBufferReady(BID_META_OUT_HAL_QUEUED);
        // pass to GF

        handleDataAndDump(P2A_TO_GF_DMW_MYS, pRequest);
    }

lbExit:
    return MTRUE;
}


/******************************************************************************
 *
 ******************************************************************************/

MVOID
P2ABayerNode::
onFlush()
{
    VSDOF_LOGD("+");
    DepthMapPipeNode::onFlush();
    VSDOF_LOGD("-");
}

}; //NSFeaturePipe_DepthMap
}; //NSCamFeature
}; //NSCam

