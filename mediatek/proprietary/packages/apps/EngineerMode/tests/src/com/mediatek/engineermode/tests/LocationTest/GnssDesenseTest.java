/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.app.Instrumentation.ActivityMonitor;
import android.view.View;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Button;
import android.widget.CheckBox;
import android.util.Log;

import java.util.ArrayList;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.AutoAnswer;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.cameranew.AutoCalibration;
import com.mediatek.engineermode.desenseat.*;

public class GnssDesenseTest extends ActivityInstrumentationTestCase2<DesenseAtActivity> {
    private static final String TAG = "EmTest/GnssDesense";
    private Solo mSolo = null;
    private Context mContext = null;
    private Activity mActivity = null;
    private Instrumentation mInst = null;
    private Button mBtnTestItems = null;

    public GnssDesenseTest() {
        super(DesenseAtActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        Log.d("@M_" + TAG, "setUp");
        super.setUp();
        mInst = getInstrumentation();
        mContext = mInst.getTargetContext();
        mActivity = getActivity();
        mSolo = new Solo(mInst, mActivity);
        mBtnTestItems = (Button) mActivity.findViewById(R.id.desense_at_test_items);
        Log.v("@M_" + TAG, "setUp, done");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    @BasicFuncAnnotation
    public void test01_precondition() {
        Log.d("@M_" + TAG, "testCase01Precondition");
        assertNotNull(mInst);
        assertNotNull(mActivity);
        assertNotNull(mSolo);
        mSolo.assertCurrentActivity("current should be AutoCalibration activity",
                DesenseAtActivity.class);
        Log.d("@M_" + TAG, "testCase01Precondition pass");
    }

    @BasicFuncAnnotation
    public void test02_mainUI() {
        Log.d("@M_" + TAG, "test02_mainUI");
        mInst.waitForIdleSync();
        Intent intent = new Intent();
        intent.setClassName(mInst.getTargetContext(), DesenseAtActivity.class.getName());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mInst.startActivitySync(intent);
        mSolo.goBack();
        Log.d("@M_" + TAG, "test02_mainUI pass");
    }

    @BasicFuncAnnotation
    public void test03_items() {
        Log.d("@M_" + TAG, "test03_testItems");
        mInst.waitForIdleSync();
        mSolo.clickOnView(mBtnTestItems);
        mInst.waitForIdleSync();
        mSolo.clickOnText("Select All");
        mSolo.sleep(50);
        mSolo.clickOnText("Select All");
        mSolo.sleep(50);
        mSolo.clickOnText("LCM on");
        mSolo.clickOnText("WIFI TX");
        mSolo.sleep(50);
        mSolo.clickOnButton(mContext.getResources().getString(android.R.string.ok));
        mInst.waitForIdleSync();
        mSolo.goBack();
        Log.d("@M_" + TAG, "test03_testItems pass");
    }

    @BasicFuncAnnotation
    public void test04_gnssBand() {
        Log.d("@M_" + TAG, "test04_testItems");
        mInst.waitForIdleSync();
        Button btnGnssBand = (Button) mActivity.findViewById(R.id.desense_at_sv_ids);
        mSolo.clickOnView(btnGnssBand);
        mInst.waitForIdleSync();
        Spinner spinnerGps = (Spinner) mSolo.getView(R.id.spinner_gps);
        Spinner spinnerGlonassL = (Spinner) mSolo.getView(R.id.spinner_glonass_l);
        Spinner spinnerGlonassM = (Spinner) mSolo.getView(R.id.spinner_glonass_m);
        Spinner spinnerGlonassH = (Spinner) mSolo.getView(R.id.spinner_glonass_h);
        Spinner spinnerBeidou = (Spinner) mSolo.getView(R.id.spinner_bd);
        assertNotNull(spinnerGps);
        assertEquals(spinnerGps.getAdapter().getCount(), 33);
        assertEquals(spinnerGlonassL.getAdapter().getCount(), 11);
        assertEquals(spinnerGlonassM.getAdapter().getCount(), 12);
        assertEquals(spinnerGlonassH.getAdapter().getCount(), 12);
        assertEquals(spinnerBeidou.getAdapter().getCount(), 33);
        mSolo.clickOnView(spinnerGps);
        while (mSolo.scrollUp())
            ;
        mSolo.clickOnText(spinnerGps.getAdapter().getItem(1).toString());
        mSolo.clickOnView(spinnerGlonassL);
        while (mSolo.scrollUp())
            ;
        mSolo.clickOnText(spinnerGlonassL.getAdapter().getItem(1).toString());
        mSolo.clickOnView(spinnerGlonassM);
        while (mSolo.scrollUp())
            ;
        mSolo.clickOnText(spinnerGlonassM.getAdapter().getItem(1).toString());
        mSolo.clickOnView(spinnerGlonassH);
        while (mSolo.scrollUp())
            ;
        mSolo.clickOnText(spinnerGlonassH.getAdapter().getItem(1).toString());
        mSolo.clickOnView(spinnerBeidou);
        while (mSolo.scrollUp())
            ;
        mSolo.clickOnText(spinnerBeidou.getAdapter().getItem(1).toString());
        mSolo.clickOnButton(mContext.getResources().getString(android.R.string.ok));
        mSolo.goBack();
        Log.d("@M_" + TAG, "test04_testItems pass");
    }

    @BasicFuncAnnotation
    public void test05_rfBandConfig() {
        Log.d("@M_" + TAG, "test05_testRfBandConfig");
        mInst.waitForIdleSync();
        mSolo.clickOnMenuItem(mContext.getResources().getString(
                R.string.desense_at_rf_band_config));
        mInst.waitForIdleSync();
        mSolo.assertCurrentActivity("current should be RfBandConfig activity",
                RfBandConfigActivity.class);
        assertTrue(mSolo.searchText(
                mContext.getResources().getString(R.string.rf_desense_tx_test_gsm), 1, true));
        assertTrue(mSolo.searchText(
                mContext.getResources().getString(R.string.desense_at_lte), 1, true));
        mSolo.goBack();
        Log.d("@M_" + TAG, "test05_testRfBandConfig pass");
    }

    @BasicFuncAnnotation
    public void test06_api() {
        Log.d("@M_" + TAG, "test06_testAPI");
        mInst.waitForIdleSync();
        mSolo.scrollDown(); // to run on lcm off scenario, or it will fail
        mInst.waitForIdleSync();
        mSolo.clickOnMenuItem(mContext.getResources().getString(R.string.desense_at_api_test));
        mInst.waitForIdleSync();
        mSolo.assertCurrentActivity("current should be RfBandConfig activity",
                ApiTestActivity.class);
        Button btnStart = (Button) mSolo.getView(R.id.api_test_start);
        mSolo.clickOnView(btnStart);
        int count = 0;
        while (!btnStart.isEnabled()) {
            mSolo.sleep(1000);
            if (count++ > 60) {
                fail("time of test API is too long!");
                break;
            }
        }
        mSolo.goBack();
        Log.d("@M_" + TAG, "test06_testAPI pass");
    }

    @BasicFuncAnnotation
    public void test07_history() {
        Log.d("@M_" + TAG, "test07_history");
        mInst.waitForIdleSync();
        mSolo.clickOnMenuItem(mContext.getResources().getString(
                R.string.desense_at_show_history));
        mInst.waitForIdleSync();
        mSolo.assertCurrentActivity("current should be HistoryDetailActivity activity",
                HistoryListActivity.class);
        mSolo.goBack();
        Log.d("@M_" + TAG, "test07_history pass");
    }
}
