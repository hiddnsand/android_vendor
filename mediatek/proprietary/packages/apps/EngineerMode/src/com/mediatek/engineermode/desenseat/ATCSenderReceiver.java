package com.mediatek.engineermode.desenseat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;
import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.ModemCategory;


/**
 * Receiver for sending AT command.
 *
 */
public class ATCSenderReceiver extends BroadcastReceiver {
    private static final String TAG = "EM_desenseAT/ATCSenderReceiver";
    public static final String ATC_SEND_ACTION = "com.mediatek.engineermode.desenseat.atc_send";
    public static final String ATC_DONE_ACTION = "com.mediatek.engineermode.desenseat.atc_done";
    public static final String ATC_EXTRA_CMD = "atc_send.cmd";
    public static final String ATC_EXTRA_MODEM_TYPE = "atc_send.modem";
    public static final String ATC_EXTRA_MSG_ID = "atc_send.msgId";

    public enum AtcMsg {
        ATCMSG_NONE(0, "xxxx"),
        SUPPORT_QUERY(100, "query lte"),
        SUPPORT_QUERY_CDMA(101, "query cdma"),
        FLIGHT_MODE(102, "flight mode"),
        FLIGHT_MODE_CDMA(103, "flight mode cdma"),
        START_CDMA(104, "start cdma"),
        START_FDDTDD(105, "start FddTdd"),
        START_GSM(106, "start gsm"),
        START_LTE(107, "start lte"),
        PAUSE_CDMA(108, "pause cdma"),
        PAUSE_FDDTDD(109, "pause FddTdd"),
        PAUSE_GSM(110, "pause gsm"),
        PAUSE_LTE(111, "pause lte"),
        REBOOT_LTE(112, "reboot lte"),
        REBOOT_CDMA(113, "reboot cdma");

        public final int value;
        private final String name;
        AtcMsg(int value, String name) {
            this.value = value;
            this.name = name;
        }

        public final int getValue() {
            return this.value;
        }

        public final String getName() {
            return this.name;
        }

        public static AtcMsg getAtcMsg(int id) {
             for (AtcMsg msg : AtcMsg.values()) {
                if (id == msg.getValue()) {
                    return msg;
                }
            }
            return ATCMSG_NONE;
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        if (ATC_SEND_ACTION.equals(intent.getAction())) {
            String param = intent.getStringExtra(ATC_EXTRA_CMD);
            boolean isCDMA = intent.getBooleanExtra(ATC_EXTRA_MODEM_TYPE, false);
            int msgId = intent.getIntExtra(ATC_EXTRA_MSG_ID, -1);
            Elog.d(TAG, "receive broadcast: ATC_SEND_ACTION and param is " + param +
                        " isCDMA: " + isCDMA);
            String[] cmd = new String[2];
            String[] cmdCdma = new String[3];
            cmdCdma[0] = param;
            cmdCdma[1] = "";
            cmdCdma[2] = "DESTRILD:C2K";
            String[] cmdCdma_s = ModemCategory.getCdmaCmdArr(cmdCdma);
            cmd[0] = param;
            cmd[1] = "";
            Phone phone = getPhoneHandler(isCDMA);
            if (phone != null) {
                phone.invokeOemRilRequestStrings(isCDMA ? cmdCdma_s : cmd, null);
                Elog.d(TAG, "invokeOemRilRequestStrings done");
            } else {
                Elog.d(TAG, "phone is null");
            }

            Intent inDone = new Intent(ATCSenderReceiver.ATC_DONE_ACTION);
            context.sendBroadcast(inDone);
        }
    }

    public Phone getPhoneHandler(boolean isCDMA) {
        Phone phone = null;
        if (isCDMA) {
            if (("1".equals(FeatureSupport.isSupportC2kLte()))
                || ("2".equals(FeatureSupport.isSupportC2kLte()))) {
                phone = ModemCategory.getCdmaPhone();
            }
        } else {
            int phoneId = ModemCategory.getCapabilitySim();
            phone = PhoneFactory.getPhone(phoneId);
        }

        return phone;
    }
}
