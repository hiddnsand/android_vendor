package com.mediatek.phone.op02.plugin;

import android.content.Context;

import com.mediatek.phone.ext.INetworkSettingExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;


public class Op02PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {

    public Context mContext;
    public Op02PhoneCustomizationFactory (Context context){
        mContext = context;
    }
    @Override
    public INetworkSettingExt makeNetworkSettingExt() {
        return new OP02NetworkSettingExt(mContext);
    }

}
