package com.mediatek.engineermode.nsiot;


import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.hardware.usb.UsbManager;

import android.os.SystemProperties;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.mediatek.engineermode.ehrpdbgdata.EhrpdBgData;


public class UsbStateReceiverNsiot extends BroadcastReceiver {
    private final static String TAG = "UsbStateReceiverNsiot";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(UsbManager.ACTION_USB_STATE)) {
            Log.i(TAG, "receive");
            String mOptr = SystemProperties.get("ro.operator.optr");
            if( !("OP01".equals(mOptr) )){
                Log.i(TAG, "this project not for op01,skip the operate!");
                return;
            }
            boolean isUsbConnected = intent.getBooleanExtra(UsbManager.USB_CONNECTED, false);

            if (!isUsbConnected) {
                Log.i(TAG, "disconnected, clearIotFirewall");
                EhrpdBgData.setDataDisable(false);
            } else if ((SystemProperties.get("gsm.sim.ril.testsim").equals("1")
                        || (isGeminiSupport()
                           && SystemProperties.get("gsm.sim.ril.testsim.2").equals("1")))
                        && (SystemProperties.get("sys.usb.config").contains("acm")
                        || SystemProperties.get("sys.usb.acm_cnt").equals("1"))) {
                Log.i(TAG, "nsiot block");
                EhrpdBgData.setDataDisable(true);
            }
        }
    }

    /**
    * get gemini support status.
    */
    private boolean isGeminiSupport() {
        TelephonyManager.MultiSimVariants config
            = TelephonyManager.getDefault().getMultiSimConfiguration();
        if (config == TelephonyManager.MultiSimVariants.DSDS
            || config == TelephonyManager.MultiSimVariants.DSDA) {
            return true;
        }
        return false;
    }
}

