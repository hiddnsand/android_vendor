/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCallbackClient_impl"
#include <mtkcam/utils/std/Log.h>
#include <android/hardware/camera/device/3.2/types.h>
#include <system/camera_metadata.h>

//#include <ui/GraphicBuffer.h>
#include <ui/Rect.h>
#include <ui/GraphicBufferMapper.h>
#include <gralloc_mtk_defs.h>
#include <system/graphics.h>

#include <camera_custom_stereo.h>
#include <mtkcam/feature/effectHalBase/ICallbackClientMgr.h>

#include "CallbackClient.h"

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

#define FUNCTION_LOG_START          MY_LOGD_IF(1<=1, " +");
#define FUNCTION_LOG_END            MY_LOGD_IF(1<=1, " -");

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace callbackclient {
namespace V1_0 {
namespace implementation {

using namespace NSCam;

HandleImporter CallbackClientWrap::sHandleImporter;

// Methods from ::vendor::mediatek::hardware::camera::callbackclient::V1_0::IMtkCallbackClient follow.
Return<Status> MtkCallbackClient::existCallbackClient() {
    return canSupportBGService() ? Status::OK : Status::INTERNAL_ERROR;
}

Return<Status> MtkCallbackClient::createCallbackClient(int64_t timestamp) {
    return CallbackClientWrap::getInstance()->createCallbackClient(timestamp);
}

Return<Status> MtkCallbackClient::setOutputSurfaces(int64_t timestamp, const sp<IMtkBufferCallback>& bufferCB, const hidl_vec<int32_t>& msgType) {
    return CallbackClientWrap::getInstance()->setOutputSurfaces(timestamp, bufferCB, msgType.size());
}

Return<Status> MtkCallbackClient::destroyCallbackClient(int64_t timestamp) {
    return CallbackClientWrap::getInstance()->destroyCallbackClient(timestamp);
}


// Methods from ::android::hidl::base::V1_0::IBase follow.

IMtkCallbackClient* HIDL_FETCH_IMtkCallbackClient(const char* /* name */) {
    return new MtkCallbackClient();
}


/* CallbackClientWrap */
CallbackClientWrap*
CallbackClientWrap::getInstance()
{
    static CallbackClientWrap inst;
    return &inst;
}

Return<Status> CallbackClientWrap::createCallbackClient(int64_t timestamp) {
    std::unique_lock <std::mutex> l(mMutex);
    if (mSizeMap.size() == 0)
    {
        ICallbackClientMgr::getInstance()->registerBufferCB(bufferCallback);
    }
    mBufferCallbackMap.add(timestamp, nullptr);   // The initial is null
    mSizeMap.add(timestamp, 0);
    mlTimestamp.push_front(timestamp);
    ICallbackClientMgr::getInstance()->createCallbackClient(timestamp);
    return ::android::hardware::camera::common::V1_0::Status {};
}

Return<Status> CallbackClientWrap::setOutputSurfaces(int64_t timestamp, const sp<IMtkBufferCallback>& bufferCB, int32_t size) {
    std::unique_lock <std::mutex> l(mMutex);
    mBufferCallbackMap.replaceValueFor(timestamp, bufferCB);
    mSizeMap.replaceValueFor(timestamp, size);
    return ::android::hardware::camera::common::V1_0::Status {};
}

Return<Status> CallbackClientWrap::destroyCallbackClient(int64_t timestamp) {
    std::unique_lock <std::mutex> l(mMutex);
    mBufferCallbackMap.removeItem(timestamp);
    mSizeMap.removeItem(timestamp);
    if (!mlTimestamp.empty())
    {
        std::list<int64_t>::iterator iter = mlTimestamp.begin();
        for(;iter != mlTimestamp.end();++iter)
        {
            if (*iter == timestamp)
            {
                iter = mlTimestamp.erase(iter);
                break;
            }
        }
    }

    ICallbackClientMgr::getInstance()->removeCallbackClient(timestamp);
    return ::android::hardware::camera::common::V1_0::Status {};
}

bool
CallbackClientWrap::
onBufferCallback( int64_t const   i8Timestamp,
                  uint32_t const  u4BitstreamSize,
                  uint8_t const*  puBitstreamBuf,
                  uint32_t const  u4CallbackIndex,
                  bool            fgIsFinalImage,
                  uint32_t const  msgType,
                  bool            bNormal)
{
    FUNCTION_LOG_START
    std::unique_lock <std::mutex> l(mMutex);
    sp<IMtkBufferCallback> pBufferCallback = NULL;
    // get IMtkBufferCallback (i.e. which callbacklclient)
    if (mlTimestamp.empty())
    {
        MY_LOGE("CallbackClient list is empty!!!");
    }
    else
    {
        int32_t size = 0;
        std::list<int64_t>::iterator iter = mlTimestamp.begin();
        for(; iter != mlTimestamp.end(); iter++)
        {
            if (i8Timestamp < *iter) continue;

            pBufferCallback = mBufferCallbackMap.valueFor(*iter);
            size = mSizeMap.valueFor(*iter);
            if (pBufferCallback == NULL)
            {
                MY_LOGE("CallbackClient is erased!!!");
            }
            /* size > 1 is special mode, e.g.: vsdof */
            else if ((size <= 1) ^ bNormal)
            {
                continue;
            }
            break;
        }
    }
    if (pBufferCallback == NULL)
    {
        MY_LOGE("Can't find CallbackClient to callback!!!");
        return false;
    }
    // deque buffer
    uint64_t bufferId;
    buffer_handle_t importedBuf = nullptr;
    uint32_t stride;
    {
        Return<void> ret = pBufferCallback->dequeueBuffer(msgType,
            [&](auto status, uint64_t bufId, const auto& buf, uint32_t strd) {
                if (status == Status::OK)
                {
                    importedBuf = buf.getNativeHandle();
                    stride      = strd;
                    bufferId    = bufId;
                    sHandleImporter.importBuffer(importedBuf);
                    if (importedBuf == nullptr)
                    {
                        MY_LOGE("callback buffer import failed!");
                        return;
                    }
                    else
                    {
                        MY_LOGD("dequeue_buffer with importedBuf=%p, bufferId(%llu), stride(%d)", importedBuf, bufferId, stride);
                    }
                }
            });
        if (!ret.isOk())
        {
            MY_LOGE("Transaction error in IMtkBufferCallback::dequeueBuffer: %s", ret.description().c_str());
            return false;
        }
        if (importedBuf == nullptr)
        {
            MY_LOGE("dequeueBuffer fail!!");
            return false;
        }
    }
    // get buffer address to write
    status_t err = 0;
    uint8_t* img = NULL;
    Rect const bounds(0, 0, stride, 1);  // Rect const bounds(stride, 1);
    err = GraphicBufferMapper::get().lock(importedBuf, GRALLOC_USAGE_SW_READ_OFTEN | GRALLOC_USAGE_SW_WRITE_OFTEN, bounds, (void**)(&img));
    if (err)
    {
        MY_LOGE("GraphicBufferMapper.lock failed: status[%s(%d)]", ::strerror(-err), -err);
        // cancel buffer
        Return<Status> r = pBufferCallback->cancelBuffer(msgType, bufferId);
        if (!r.isOk()) {
            MY_LOGE("Transaction error in IMtkBufferCallback::cancelBuffer: %s", r.description().c_str());
        }
        return false;
    }
    // write bitstream size to header of clean/bokeh image for AP to read
    if (msgType == MTK_CAMERA_MSG_EXT_DATA_STEREO_CLEAR_IMAGE || msgType == CAMERA_MSG_COMPRESSED_IMAGE)
    {
        uint32_t*const pCBData = reinterpret_cast<uint32_t*>(img);
        pCBData[0] = u4BitstreamSize;
        img = reinterpret_cast<uint8_t*>(&pCBData[1]);
    }
    //copy image buffer
    ::memcpy(img, puBitstreamBuf, u4BitstreamSize);
    GraphicBufferMapper::get().unlock(importedBuf);
    // free importedBuf
    sHandleImporter.freeBuffer(importedBuf);
    // set timestamp
    {
        Return<Status> r = pBufferCallback->setTimestamp(msgType, bufferId);
        if (!r.isOk()) {
            MY_LOGE("Transaction error in IMtkBufferCallback::setTimestamp: %s", r.description().c_str());
            return false;
        }
    }
    // enque buffer
    {
        Return<Status> r = pBufferCallback->enqueueBuffer(msgType, bufferId);
        if (!r.isOk()) {
            MY_LOGE("Transaction error in IMtkBufferCallback::enqueueBuffer: %s", r.description().c_str());
            return false;
        }
    }
    FUNCTION_LOG_END
    return true;
}

bool
CallbackClientWrap::
bufferCallback( int64_t const   i8Timestamp,
                uint32_t const  u4BitstreamSize,
                uint8_t const*  puBitstreamBuf,
                uint32_t const  u4CallbackIndex,
                bool            fgIsFinalImage,
                uint32_t const  msgType,
                bool            bNormal)
{
    return CallbackClientWrap::getInstance()->onBufferCallback(i8Timestamp,
                                                               u4BitstreamSize,
                                                               puBitstreamBuf,
                                                               u4CallbackIndex,
                                                               fgIsFinalImage,
                                                               msgType,
                                                               bNormal);
}

}  // namespace implementation
}  // namespace V1_0
}  // namespace callbackclient
}  // namespace camera
}  // namespace hardware
}  // namespace mediatek
}  // namespace vendor
