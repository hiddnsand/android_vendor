/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.bluetooth.avrcp;

import android.os.RemoteCallbackList;
import android.os.RemoteException;

import com.android.music.MediaPlaybackService;
import com.android.music.MusicLogUtils;

/**
 * Class to handle BT AVRCP.
 */
public class ServiceAvrcpStub extends IBTAvrcpMusic.Stub {

    private final static String TAG = "SERVICE_AVRCP_STUB";

    private MediaPlaybackService mService;

    private int mRepeatMode = 1;

    private int mShuffleMode = 1;

    /**
     * Constructor of ServiceAvrcpStub.
     *
     * @param mediaPlaybackService playback service instance
     */
    public ServiceAvrcpStub(MediaPlaybackService mediaPlaybackService) {
        this.mService = mediaPlaybackService;
    }

    final RemoteCallbackList<IBTAvrcpMusicCallback> mAvrcpCallbacksList =
     new RemoteCallbackList<IBTAvrcpMusicCallback>();

    /**
     * Register event.
     *
     * @param callback callback function
     */
    public void registerCallback(IBTAvrcpMusicCallback callback) {

        MusicLogUtils.d(TAG, "[AVRCP] ServiceAvrcpStub. registerCallback");
        if (callback != null) {
            mAvrcpCallbacksList.register(callback);
        }
        getRepeatMode();
        getShuffleMode();
    }

    /**
     * Unregister event.
     *
     * @param callback callback function
     */
    public void unregisterCallback(IBTAvrcpMusicCallback callback) {
        MusicLogUtils.d(TAG, "[AVRCP] ServiceAvrcpStub. unregisterCallback");
        if (callback != null) {
            mAvrcpCallbacksList.unregister(callback);
        }
    }

    /**
     * Register event.
     *
     * @param eventId event ID
     * @param interval interval
     *
     * @return register successfully or not
     */
    public boolean regNotificationEvent(byte eventId, int interval) {
        switch (eventId) {
            case 0x01: // playstatus
                mPlaybackFlag = true;
                MusicLogUtils.v(TAG, "[AVRCP] mPlaybackFlag flag is " + mPlaybackFlag);
                return true;
            case 0x02: // track change
                mTrackchangeFlag = true;
                MusicLogUtils.v(TAG, "[AVRCP] bTrackchange flag is " + mTrackchangeFlag);
                return mTrackchangeFlag;
            case 0x09: // playing content
                mTrackNowPlayingChangedFlag = true;
                return true;
            default:
                MusicLogUtils.e(TAG, "[AVRCP] MusicApp doesn't support eventId:" + eventId);
                break;
        }

        return false;
    }

    /**
     * Useless function.
     *
     * @param attrId no use
     * @param value no use
     * @return no use
     */
    public boolean setPlayerApplicationSettingValue(byte attrId, byte value) {
        return false;
    }

    /**
     * Useless function.
     *
     * @return no use
     */
    public byte[] getCapabilities() {
        return null;
    }

    /**
     * stop to play.
     */
    public void stop() {
        mService.stop();
    }

    /**
     * Pause the play.
     */
    public void pause() {
        mService.pause();
    }

    /**
     * Useless function.
     */
    public void resume() {
    }

    /**
     * Start to play.
     */
    public void play() {
        mService.play();
    }

    /**
     * Play the previous song.
     */
    public void prev() {
        mService.prev();
    }

    /**
     * Play the next song.
     */
    public void next() {
        mService.gotoNext(true);
    }

    /**
     * Play the next song.
     */
    public void nextGroup() {
        mService.gotoNext(true);
    }

    /**
     * Play the pevious song.
     */
    public void prevGroup() {
        mService.prev();
    }

    /**
     * Get the playback status.
     *
     * @return playback status
     */
    public byte getPlayStatus() {
        if (true == mService.isPlaying()) {
            return 1; // playing
        } else {
            if (!mService.isCursorNull()) {
                return 2; // pause
            }
            return 0; // stop
        }
    }

    /**
     * Get the audio ID.
     *
     * @return audio ID
     */
    public long getAudioId() {
        return mService.getAudioId();
    }

    /**
     * Get the track name.
     *
     * @return track name
     */
    public String getTrackName() {
        return mService.getTrackName();
    }

    /**
     * Get the album name.
     *
     * @return album name
     */
    public String getAlbumName() {
        return mService.getAlbumName();
    }

    /**
     * Get the current album ID.
     *
     * @return album ID
     */
    public long getAlbumId() {
        return mService.getAlbumId();
    }

    /**
     * Get the current artist name.
     *
     * @return artist name
     */
    public String getArtistName() {
        return mService.getArtistName();
    }

    /**
     * Get the current position.
     *
     * @return current position
     */
    public long position() {
        return mService.position();
    }

    /**
     * Useless function.
     * @return no use
     */
    public long duration() {
        return mService.duration();
    }

    /**
     * Useless function.
     * @param equalizeMode no use
     * @return no use
     */
    public boolean setEqualizeMode(int equalizeMode) {
        return false;
    }

    /**
     * Useless function.
     * @return no use
     */
    public int getEqualizeMode() {
        return 0;
    }

    /**
     * Set the shuffle mode.
     *
     * @param shufflemode the suffle mode to set
     * @return set succesfully or not
     */
    public boolean setShuffleMode(int shufflemode) {
        int mode = 0;
        switch (shufflemode) {
            case 1: // SHUFFLE_NONE
                mode = 0;
                break;
            case 2: // SHUFFLE_NORMAL
                mode = 1;
                break;
            default:
                return false;
        }
        MusicLogUtils.d(TAG, "[AVRCP] setShuffleMode music_mode:" + mode);
        mService.setShuffleMode(mode);
        // sendBTDelaySetting( 2, mode);
        return true;
    }

    /**
     * Get the suffle mode.
     *
     * @return the shuffle mode
     */
    public int getShuffleMode() {
        mShuffleMode = (mService.getShuffleMode() + 1);
        return mShuffleMode;
    }

    /**
     * Set the repeat mode.
     *
     * @param repeatmode the repeatmode to set
     * @return the result to set
     */
    public boolean setRepeatMode(int repeatmode) {
        // avrcp repeat mode to local mode TODO: delay this
        // REPEAT_NONE = 0;
        // REPEAT_CURRENT = 1;
        // REPEAT_ALL = 2;
        int mode = 0;
        switch (repeatmode) {
            case 1: // REPEAT_MODE_OFF
                mode = 0;
                break;
            case 2: // REPEAT_MODE_SINGLE_TRACK
                mode = 1;
                break;
            case 3: // REPEAT_MODE_ALL_TRACK
                mode = 2;
                break;
            default:
                return false;
        }
        MusicLogUtils.d(TAG, String.format("[AVRCP] setRepeatMode musid_mode:%d", mode));
        mService.setRepeatMode(mode);
        // sendBTDelaySetting( 3, mode);
        return true;
    }

    /**
     * Get the repeat mode.
     *
     * @return the repeat mode
     */
    public int getRepeatMode() {
        mRepeatMode = (mService.getRepeatMode() + 1);
        return mRepeatMode;
    }

    /**
     * Useless function.
     * @param scanMode no use
     * @return no use
     */
    public boolean setScanMode(int scanMode) {
        return false;
    }

    /**
     * Useless function.
     *
     * @return no use
     */
    public int getScanMode() {
        return 0;
    }

    /**
     * Useless function.
     * @param charset no use
     * @return no use
     */
    public boolean informDisplayableCharacterSet(int charset) {
        if (charset == 0x6a) {
            return true;
        }
        return false;
    }

    /**
     * Useless function.
     *
     * @return no use
     */
    public boolean informBatteryStatusOfCT() {
        return true;
    }

    /**
     * Put the list into the now playing list.
     *
     * @param list the list to be added.
     * @param action the position to add.
     */
    public void enqueue(long[] list, int action) {
        mService.enqueue(list, action);
    }

    /**
     * Get the now playing list.
     *
     * @return the now playing list
     */
    public long[] getNowPlaying() {
        return mService.getQueue();
    }

    /**
     * Get the name of current playing item.
     *
     * @param id the audio id
     * @return the name of song
     */
    public String getNowPlayingItemName(long id) {
        return null;
    }

    /**
     * Open the play list.
     *
     * @param list the playlist
     * @param position the start position
     */
    public void open(long[] list, int position) {
        mService.open(list, position);
    }

    /**
     * Get the position of the queue.
     *
     * @return the position of the queue
     */
    public int getQueuePosition() {
        return mService.getQueuePosition();
    }

    /**
     * Set the position of the queue.
     *
     * @param index the position in queue
     */
    public void setQueuePosition(int index) {
        mService.setQueuePosition(index);
    }

    protected boolean mPlaybackFlag = false;

    protected boolean mTrackchangeFlag = false;

    protected boolean mTrackReachStartFlag = false;

    protected boolean mTrackReachEndFlag = false;

    protected boolean mTrackPosChangedFlag = false;

    protected boolean mTrackAppSettingChangedFlag = false;

    protected boolean mTrackNowPlayingChangedFlag = false;

    /**
     * Notify the status of playback to BT AVRCP.
     *
     * @param s the status
     */
    public void notifyBTAvrcp(String s) {

        MusicLogUtils.v(TAG, "[AVRCP] notifyBTAvrcp " + s);
        if (MediaPlaybackService.PLAYSTATE_CHANGED.equals(s)) {
            notifyPlaybackStatus(getPlayStatus());
        }
        if (MediaPlaybackService.PLAYBACK_COMPLETE.equals(s)) {
            notifyTrackChanged();
            // notifyTrackReachEnd();
        }
        if (MediaPlaybackService.QUEUE_CHANGED.equals(s)) {
            notifyTrackChanged();
            notifyNowPlayingContentChanged();
        }
        if (MediaPlaybackService.META_CHANGED.equals(s)) {
            notifyTrackChanged();
        }
    }

    /* AVRCP callback interface */
    protected void notifyPlaybackStatus(byte status) {
        // check the register & callback it back
        // if( true != mPlaybackFlag ){
        // MusicLogUtils.v(TAG, "notifyPlaybackStatus ignore mPlaybackFlag:" +
        // mPlaybackFlag);
        // return;
        // }
        mPlaybackFlag = false;

        final int n = mAvrcpCallbacksList.beginBroadcast();
        MusicLogUtils.d(TAG, "[AVRCP] notifyPlaybackStatus " + status + " n= " + n);
        for (int i = 0; i < n; i++) {
            IBTAvrcpMusicCallback listener = mAvrcpCallbacksList.getBroadcastItem(i);
            try {
                listener.notifyPlaybackStatus(status);
            } catch (RemoteException ex) {
                // The RemoteCallbackList will take care of removing the
                // dead listeners.
                MusicLogUtils.e(TAG, "Error:" + ex);
            }
        }
        mAvrcpCallbacksList.finishBroadcast();

    }

    protected void notifyTrackChanged() {
        // check the register & callback it back
        // if( true != mTrackchangeFlag ){
        // return;
        // }
        mTrackchangeFlag = false;

        final int n = mAvrcpCallbacksList.beginBroadcast();
        MusicLogUtils.d(TAG, "[AVRCP] notifyTrackChanged " + " n= " + n);
        for (int i = 0; i < n; i++) {
            IBTAvrcpMusicCallback listener = mAvrcpCallbacksList.getBroadcastItem(i);
            try {
                listener.notifyTrackChanged(getAudioId());
            } catch (RemoteException ex) {
                // The RemoteCallbackList will take care of removing the
                // dead listeners.
                MusicLogUtils.e(TAG, "Error:" + ex);
            }
        }
        mAvrcpCallbacksList.finishBroadcast();
    }

    protected void notifyTrackReachStart() {
        // check the register & callback it back
        if (true != mTrackReachStartFlag) {
            return;
        }

        // Default Music Player dones't support this
    }

    protected void notifyTrackReachEnd() {
        // check the register & callback it back
        if (true != mTrackReachEndFlag) {
            return;
        }

        // Default Music Player dones't support this
    }

    protected void notifyPlaybackPosChanged() {
        if (true != mTrackPosChangedFlag || null == mAvrcpCallbacksList) {
            return;
        }

        // Default Music Player dones't support this
    }

    protected void notifyAppSettingChanged() {
        if (true != mTrackAppSettingChangedFlag || null == mAvrcpCallbacksList) {
            return;
        }
        mTrackAppSettingChangedFlag = false;

        // check the register & callback it back
        final int n = mAvrcpCallbacksList.beginBroadcast();
        for (int i = 0; i < n; i++) {
            IBTAvrcpMusicCallback listener = mAvrcpCallbacksList.getBroadcastItem(i);
            try {
                listener.notifyAppSettingChanged();
            } catch (RemoteException ex) {
                // The RemoteCallbackList will take care of removing the
                // dead listeners.
                MusicLogUtils.e(TAG, "Error:" + ex);
            }
        }
        mAvrcpCallbacksList.finishBroadcast();
    }

    protected void notifyNowPlayingContentChanged() {
        MusicLogUtils.v(TAG, "[AVRCP] notifyNowPlayingContentChanged ");
        if (null == mAvrcpCallbacksList) {
            return;
        }
        mTrackNowPlayingChangedFlag = false;

        // check the register & callback it back
        final int n = mAvrcpCallbacksList.beginBroadcast();

        MusicLogUtils.d(TAG, "[AVRCP] notifyNowPlayingContentChanged " + " n= " + n);

        for (int i = 0; i < n; i++) {
            IBTAvrcpMusicCallback listener = mAvrcpCallbacksList.getBroadcastItem(i);
            try {
                listener.notifyNowPlayingContentChanged();
            } catch (RemoteException ex) {
                // The RemoteCallbackList will take care of removing the
                // dead listeners.
                MusicLogUtils.e(TAG, "Error:" + ex);
            }
        }
        mAvrcpCallbacksList.finishBroadcast();
    }

    protected void notifyVolumehanged(byte volume) {
        MusicLogUtils.v(TAG, "[AVRCP] notifyVolumehanged " + volume);
    }
}