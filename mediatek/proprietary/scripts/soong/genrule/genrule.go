package mtkGenrule

import (
	"os"
	"path/filepath"

	"github.com/google/blueprint"

	"android/soong/android"
)

type mtkDumpSingleton struct{}

func init() {
	android.RegisterSingletonType("mtkdump", MtkDumpSingleton)
}

func MtkDumpSingleton() blueprint.Singleton {
	return &mtkDumpSingleton{}
}

func (c *mtkDumpSingleton) GenerateBuildActions(ctx blueprint.SingletonContext) {
	config := ctx.Config().(android.Config)
	if !config.EmbeddedInMake() {
		return
	}
	variablesFileName := filepath.Join(config.BuildDir(), "mtk_soong.config")
	if _, err := os.Stat(variablesFileName); err == nil {
		ctx.AddNinjaFileDeps(variablesFileName)
	}
}
