#
# RSC_Test
#
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

#
LOCAL_SRC_FILES := \
    main.cpp \
    main_rscstream.cpp \

-include $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk
-include $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/driver.mk

ifeq ($(MTKRSC_SUPPORT), 1)
    ifneq (,$(filter $(strip $(MTKCAM_DRV_PLATFORM)), mt6763))
        TEST_SRC_FOLDER := v1.0
    else
        TEST_SRC_FOLDER := v1.0
    endif
        LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/src/rsc/1.0/hw/$(MTKCAM_DRV_PLATFORM)
else
        TEST_SRC_FOLDER := v1.0
        LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/src/rsc/1.0/hw/dummy
endif


LOCAL_SRC_FILES += hw/$(TEST_SRC_FOLDER)/rsc_testcommon.cpp
LOCAL_SRC_FILES += hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/MultiEnque_rsc_test_case_00_frame_0_rsc_setting_00.cpp
#LOCAL_SRC_FILES += hw/$(TEST_SRC_FOLDER)/rsc/rsc_test_case_00/MultiEnque_rsc_test_case_00_frame_2_rsc_setting_00.cpp

LOCAL_SRC_FILES +=  \
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_apli_c_frame_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_apli_p_frame_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_01verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_02verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_03verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_04verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_05verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_06verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_07verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_08verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_09verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_10verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_11verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_12verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_13verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_bvo_frame_00_14verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_imgi_c_frame_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_imgi_p_frame_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvi_frame_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_01verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_02verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_03verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_04verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_05verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_06verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_07verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_08verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_09verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_10verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_11verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_12verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_13verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_default/frame_0/frame_0_rsc_mvo_frame_00_14verif.c\

LOCAL_SRC_FILES +=  \
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_apli_c_frame_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_apli_p_frame_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_01verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_02verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_03verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_04verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_05verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_06verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_07verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_08verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_09verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_10verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_11verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_12verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_13verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_bvo_frame_00_14verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_imgi_c_frame_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_imgi_p_frame_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvi_frame_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_01verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_02verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_03verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_04verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_05verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_06verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_07verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_08verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_09verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_10verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_11verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_12verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_13verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_0/rsc_test_case_00_frame_0_rsc_mvo_frame_00_14verif.c\

LOCAL_SRC_FILES +=  \
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_apli_c_frame_01verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_apli_p_frame_01verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_01verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_02verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_03verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_04verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_05verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_06verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_07verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_08verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_09verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_10verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_11verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_12verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_13verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_bvo_frame_01_14verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_imgi_c_frame_01verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_imgi_p_frame_01verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvi_frame_01verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_00verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_01verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_02verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_03verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_04verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_05verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_06verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_07verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_08verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_09verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_10verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_11verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_12verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_13verif.c\
  hw/$(TEST_SRC_FOLDER)/rsc_test_case_00/frame_1/rsc_test_case_00_frame_1_rsc_mvo_frame_01_14verif.c\

LOCAL_SRC_FILES +=  \
  hw/$(TEST_SRC_FOLDER)/eis_offline/window/apli_p.c\
  hw/$(TEST_SRC_FOLDER)/eis_offline/window/rsc_mv_p_336x256.c\
  hw/$(TEST_SRC_FOLDER)/eis_offline/window/rsso_p_288x512.c\
  hw/$(TEST_SRC_FOLDER)/eis_offline/window/apli_c.c\
  hw/$(TEST_SRC_FOLDER)/eis_offline/window/rsc_mv_c_336x256.c\
  hw/$(TEST_SRC_FOLDER)/eis_offline/window/rsc_bv_c_144x256.c\
  hw/$(TEST_SRC_FOLDER)/eis_offline/window/rsso_c_288x512.c\
# hw/$(TEST_SRC_FOLDER)/eis_offline/flat_surface/apli_p.c\
# hw/$(TEST_SRC_FOLDER)/eis_offline/flat_surface/rsc_mv_p_336x256.c\
# hw/$(TEST_SRC_FOLDER)/eis_offline/flat_surface/rsso_p_288x512.c\
# hw/$(TEST_SRC_FOLDER)/eis_offline/flat_surface/apli_c.c\
# hw/$(TEST_SRC_FOLDER)/eis_offline/flat_surface/rsc_mv_c_336x256.c\
# hw/$(TEST_SRC_FOLDER)/eis_offline/flat_surface/rsc_bv_c_144x256.c\
# hw/$(TEST_SRC_FOLDER)/eis_offline/flat_surface/rsso_c_288x512.c\

# vector
LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libutils

LOCAL_SHARED_LIBRARIES += libstdc++

LOCAL_SHARED_LIBRARIES +=  libmtkcam_rsc
LOCAL_SHARED_LIBRARIES += libcamdrv_imem

LOCAL_STATIC_LIBRARIES := \

LOCAL_WHOLE_STATIC_LIBRARIES := \

LOCAL_MODULE := Rsc_Test
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_TAGS := eng

LOCAL_PRELINK_MODULE := false

#-----------------------------------------------------------
LOCAL_C_INCLUDES += $(LOCAL_PATH)/hw/$(TEST_SRC_FOLDER)/
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/include
LOCAL_C_INCLUDES += $(MTKCAM_DRV_INCLUDE)
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_PLATFORM)/drv
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_PLATFORM)/imageio

# End of common part ---------------------------------------
#
include $(BUILD_EXECUTABLE)


#
#include $(call all-makefiles-under,$(LOCAL_PATH))
