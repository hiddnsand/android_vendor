package com.mediatek.engineermode.modemnotification;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncResult;
import android.os.Binder;
import android.os.IBinder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.media.AudioManager;
import android.media.SoundPool;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.ModemCategory;
import com.mediatek.engineermode.eminfo.Content;
import com.mediatek.engineermode.eminfo.UrcParser;
import com.mediatek.engineermode.eminfo.UrcParser.Parser;
import com.mediatek.internal.telephony.MtkGsmCdmaPhone;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

/*
 * AT command tool should be able to run commands at background.
 */
public class ModemNotificationService extends Service {
    private static final String TAG = "EM/ModemNotificationService";
    private static final String MODEM_NOTIFICATION_SHAREPRE =
           "telephony_modem_notification_settings";
    private static final String caDetectedFile = "/vendor/res/sound/CADetected.ogg";
    private static final String caToCaFile = "/vendor/res/sound/CA2CA.ogg";
    private static final int TOTAL_NOTIFICATION_ITEM = 2;
    private static final int MSG_NW_INFO = 1;
    private static final int MSG_NW_INFO_URC = 2;
    private static final int MSG_NW_INFO_OPEN = 3;
    private static final int MSG_NW_INFO_CLOSE = 4;
    private static final int MSG_UPDATE_UI_URC = 7;
    private static final int FLAG_OR_DATA = 0xFFFFFFF7;
    private static final int FLAG_OFFSET_BIT = 0x08;
    private static final int FLAG_DATA_BIT = 8;
    private static final int UPDATE_INTERVAL = 50;
    private SoundPool soundPool = null;
    private HashMap<Integer,Integer> spMap;
    private boolean flag = false;

    //ERRC_EM_MOB_EVENT_IND
    //em_errc_mob_type_enum
    private static final int EM_ERRC_MOB_TYPE_CR = 0;
    private static final int EM_ERRC_MOB_TYPE_REDT = 1;
    private static final int EM_ERRC_MOB_TYPE_CCO = 2;
    private static final int EM_ERRC_MOB_TYPE_HO = 3;
    private static final int EM_ERRC_MOB_TYPE_REEST = 4;
    private static final int EM_ERRC_MOB_TYPE_INTER_BAND_HO_CA2CA = 5;
    private static final int EM_ERRC_MOB_TYPE_INTER_FREQ_HO_CA2CA = 6;
    private static final int EM_ERRC_MOB_TYPE_INTRA_FREQ_HO_CA2CA = 7;
    //em_errc_mob_dir_enum
    private static final int EM_ERRC_MOB_DIR_INTRA_LTE = 0;
    private static final int EM_ERRC_MOB_DIR_TO_LTE = 1;
    private static final int EM_ERRC_MOB_DIR_FROM_LTE = 2;
    //

    //ERRC_EM_RECONF_INFO
    //em_errc_reconf_info_type_enum
    private static final int ERRC_NORMAL_RECONF = 0;
    private static final int ERRC_INTRA_CEL_HO = 1;
    private static final int ERRC_INTER_CEL_HO = 2;
    private static final int ERRC_INTER_RAT_HO = 3;
    //

    //RAC_EM_INFO
    //rac_em_active_rat_info_enum
    private static final int RAC_EM_NO_SERVICE = 0;
    private static final int RAC_EM_LIMITED_SERVICE = 1;
    private static final int RAC_EM_GSM = 2;
    private static final int RAC_EM_UMTS_FDD = 3;
    private static final int RAC_EM_UMTS_TDD = 4;
    private static final int RAC_EM_LTE_FDD = 5;
    private static final int RAC_EM_LTE_TDD = 6;

    private int[] EM_TYPES = new int[] {Content.ERRC_EM_CONN_INFO,
                        Content.ERRC_EM_MOB_EVENT_IND};


    private List<String> mCommands = new ArrayList<String>();
    private boolean mSending = false;
    private boolean mFirstCaDetected = true;
    private boolean mFirstCaToCa = true;
    private Phone mPhone = null;
    private int mFlag = 0;
    private int mFlagCount = 0;
    private int[] mCheckedItem;
    private boolean mCaDetectedLastStatus = false;
    private boolean mCaDetectedCurStatus = false;
    private boolean mCaToCaLastStatus = false;
    private boolean mCaToCaCurStatus = false;
    private Handler mUrcHandler;
    private final HandlerThread mHandlerThread = new HandlerThread("md_notification_urc_handler");
    private Handler mUpdateUiHandler;
    private ArrayList<Integer> mCheckedEmTypes;



    private void urcHandle(Message msg) {
        if (msg.what == MSG_NW_INFO_URC) {

                AsyncResult ar = (AsyncResult) msg.obj;
                String[] data = (String[]) ar.result;
                if ((data.length <= 0) || (data[0] == null) || (data[1] == null)) {
                        return;
                    }
                Elog.v(TAG, "Receive URC: " + data[0] + ", " + data[1]);

                int type = -1;
                try {
                    type  = Integer.parseInt(data[0]);
                } catch (NumberFormatException e) {
                    Elog.e(TAG, "Return type error");
                    return;
                }
                Parser decodedData = UrcParser.parse(type, data[1]);
                Message msgToUi = mUpdateUiHandler.obtainMessage();
                msgToUi.what = MSG_UPDATE_UI_URC;
                msgToUi.arg1 = type;
                msgToUi.obj = decodedData;
                mUpdateUiHandler.sendMessageDelayed(msgToUi, UPDATE_INTERVAL);
            }
        }


    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

        soundPool = new SoundPool(TOTAL_NOTIFICATION_ITEM, AudioManager.STREAM_MUSIC, 0);
        spMap = new HashMap<Integer, Integer>();
        spMap.put(1, soundPool.load(caDetectedFile, 1));
        spMap.put(2, soundPool.load(caToCaFile, 1));

        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                 mFlagCount++;
                 Elog.d(TAG, "onLoadComplete sampleId " + sampleId + " mFlagCount " + mFlagCount);
            }
        });

    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        Elog.v(TAG, "Enter onDestroy");
        soundPool.release();
        soundPool = null;
        unregisterURC();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Elog.d(TAG, "Enter onStartCommand");
        registerURC();
        initNotification();
        return START_NOT_STICKY;
    }


    public boolean isRunning() {
        return mSending;
    }

    private void checkType() {
        final SharedPreferences modemNotificationSh = getSharedPreferences(
            MODEM_NOTIFICATION_SHAREPRE, android.content.Context.MODE_PRIVATE);
        int checkedOn = modemNotificationSh.getInt(getString(R.string.md_checkedshare_value), 0);

        Elog.d(TAG, "checkType checkedOn " + checkedOn);
        mCheckedEmTypes.clear();

        for (int i = 0; i < TOTAL_NOTIFICATION_ITEM; i++) {
            mCheckedItem[i] = (checkedOn >> i) & 0x0001;
            Elog.d(TAG, "checkType i: " + i + " mCheckedItem[i]: " + mCheckedItem[i]);
            int type = 0;
            if (mCheckedItem[i] == 1) {
                type = EM_TYPES[i];
            if (!mCheckedEmTypes.contains(type)) {
                    Elog.d(TAG, "checkType type: " + type);
                mCheckedEmTypes.add(type);
            }
        }

    }


    }

    private void initNotification() {

        mCheckedEmTypes = new ArrayList<Integer>();
        mCheckedItem = new int[TOTAL_NOTIFICATION_ITEM];
        for (int i = 0; i < TOTAL_NOTIFICATION_ITEM; i++) {
            mCheckedItem[i] = 0;
        }
        mUpdateUiHandler = new Handler() {
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case MSG_UPDATE_UI_URC:
                            int type = msg.arg1;
                            Elog.d(TAG, "MSG_UPDATE_UI_URC type " + type);
                            Parser decodedData = (Parser)msg.obj;
                            if (decodedData != null) {
                                checkType();
                                if (mCheckedEmTypes.contains(type)) {
                                    int id = checkNotification(type, decodedData);
                                    if (id > 0 && id < (TOTAL_NOTIFICATION_ITEM + 1)){
                                        Elog.d(TAG, "Need to notify, please play the sound");
                                        playSounds(id);
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            };

    }

    private void registerURC() {
        Elog.d(TAG, "registerURC");
        if (TelephonyManager.getDefault().getPhoneCount() > 1) {
            mPhone = PhoneFactory.getPhone(PhoneConstants.SIM_ID_1);
        } else {
            mPhone = PhoneFactory.getDefaultPhone();
        }

        mHandlerThread.start();
        mUrcHandler = new Handler(mHandlerThread.getLooper()) {
            public void handleMessage(Message msg) {
                urcHandle(msg);
            }
        };

        if (mPhone != null) {
            ((MtkGsmCdmaPhone)mPhone).registerForNetworkInfo(mUrcHandler, MSG_NW_INFO_URC, null);
        }

    }

    private void unregisterURC() {
        Elog.d(TAG, "unregisterURC");
        if (mPhone != null) {
            ((MtkGsmCdmaPhone)mPhone).unregisterForNetworkInfo(mUrcHandler);
        }
    }

    int checkNotification(int type, Parser data) {
        int soundId = -1;
        boolean caDetectedPass = false;
        boolean caToCaPass = false;
        switch (type) {
           case Content.ERRC_EM_CONN_INFO:
                List<Boolean> configured = data.getBooleanArray("is_scell_configured");
                for (int i = 0; i < configured.size(); i++) {
                    Elog.d(TAG, "checkNotification i " + i + ", is_scell_configured "
                        + configured.get(i));
                    if (configured.get(i)) {
                        caDetectedPass = true;
                        break;
                    }
                }
                if (mFirstCaDetected) {
                    mCaDetectedLastStatus = false;
                    mCaDetectedCurStatus = caDetectedPass;
                    mFirstCaDetected = false;
                } else {
                    mCaDetectedLastStatus = mCaDetectedCurStatus;
                    mCaDetectedCurStatus = caDetectedPass;
                }
                if ((mCaDetectedLastStatus != mCaDetectedCurStatus) && mCaDetectedCurStatus) {
                    soundId = 1;
                }
                break;
           case Content.ERRC_EM_MOB_EVENT_IND:
                int mob_type = data.getInt("mob_type");
                int mob_dir = data.getInt("mob_dir");
                Elog.d(TAG, "checkNotification mob_type " + mob_type + ", mob_dir " + mob_dir);
                if ((mob_type == EM_ERRC_MOB_TYPE_INTER_BAND_HO_CA2CA ||
                    mob_type == EM_ERRC_MOB_TYPE_INTER_FREQ_HO_CA2CA ||
                    mob_type == EM_ERRC_MOB_TYPE_INTRA_FREQ_HO_CA2CA)
                    && mob_dir == EM_ERRC_MOB_DIR_INTRA_LTE) {
                    caToCaPass = true;
                }
                if (mFirstCaToCa) {
                    mCaToCaLastStatus = false;
                    mCaToCaCurStatus = caToCaPass;
                    mFirstCaToCa = false;
                } else {
                    mCaToCaLastStatus = mCaToCaCurStatus;
                    mCaToCaCurStatus = caToCaPass;
                }
                if ((mCaToCaLastStatus != mCaToCaCurStatus) && mCaToCaCurStatus) {
                    soundId = 2;
                }
                break;
           default:
                break;
      }
     Elog.d(TAG, "checkNotification soundId " + soundId);
     return soundId;
   }

   void playSounds(int sound) {
       AudioManager am = (AudioManager)this.getSystemService(this.AUDIO_SERVICE);
       float audioMaxVolumn = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
       float audioCurrentVolumn = am.getStreamVolume(AudioManager.STREAM_MUSIC);
       float volumnRatio = audioCurrentVolumn/audioMaxVolumn;
       if (mFlagCount == 2) {
           flag = true;
       } else {
           Elog.e(TAG, "playSounds loading file fail");
           flag = false;
       }
       if (flag) {
           soundPool.play(spMap.get(sound), volumnRatio, volumnRatio, 1, 0, 1.0f);
       } else {
          Elog.e(TAG, "no playSounds since flag is false");
       }

   }
}

