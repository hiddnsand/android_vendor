#include <inttypes.h>

#include <cutils/memory.h>

#include <utils/Log.h>

#include <binder/IPCThreadState.h>
#include <binder/ProcessState.h>
#include <binder/IServiceManager.h>

#include <dfps/FpsPolicy.h>
#include <dfps/FpsInfo.h>

#define __STDC_FORMAT_MACROS 1

using namespace android;

class testFpsInfoListener : public FpsInfo::FpsInfoListener {
public:
    testFpsInfoListener() {};
    void onFpsChanged(const int32_t fps) { ALOGE("get cb fps : %d", fps);};
};

class testFpsRequestListener : public FpsPolicy::FpsRequestListener {
public:
    testFpsRequestListener() {};
    virtual void onRequestChanged(const FpsRequest& quest) { ALOGE("get request1: %d : %d", quest.fps, quest.mode);};
};

class testClassFpsRequestListener : public FpsPolicy::FpsRequestListener {
public:
    testClassFpsRequestListener() {};
    void init();
    virtual void onRequestChanged(const FpsRequest& quest) { ALOGE("get request2: %d : %d pid[%d] api[%d] mode[%d] t_pid[%d] gl_id[%" PRIx64 "]",
        quest.fps, quest.mode, quest.info.pid, quest.info.api, quest.info.mode, quest.info.targetPid, quest.info.glContextId);};
private:
    sp<FpsPolicy> mPolicy;
};

void testClassFpsRequestListener::init()
{
    String8 string("");
    string.appendFormat("test-callback");
    mPolicy = new FpsPolicy(FpsPolicy::API_THERMAL, string);
    mPolicy->registerFpsRequestListener(static_cast<FpsPolicy::FpsRequestListener*>(this));
}

int main(int /*argc*/, char** /*argv*/)
{
    sp<ProcessState> ps(ProcessState::self());
    ps->startThreadPool();

    sp<testClassFpsRequestListener> testC = new testClassFpsRequestListener();
    testC->init();

    String8 string("");
    string.appendFormat("test-policy");
    sp<FpsPolicy> policy = new FpsPolicy(FpsPolicy::API_THERMAL, string);
    sp<FpsPolicy::FpsRequestListener> fqListener = new testFpsRequestListener();
    policy->registerFpsRequestListener(fqListener);
    int number;
    FpsRange range;
    policy->getPanelInfoNumber(&number);
    ALOGI("total fps: %d", number);
    for (int i = 0; i < number; i++) {
        policy->getFpsRange(i, &range);
        ALOGI("+ panel fps: [%d] %d~%d", i, range.min, range.max);
    }

    uint64_t glContextId;
    glContextId = 0x1234ffff;
    policy->setFps(40, FpsPolicy::MODE_INTERNAL_SW, 456, glContextId);

    sp<FpsInfo> info = new FpsInfo();
    //sp<FpsInfo::FpsInfoListener> fiListener = new testFpsInfoListener();
    //info->regFpsInfoListener(fiListener);

    int32_t p = 123;
    String8 pn("the_fg_is_123");
    info->setForegroundInfo(p, pn);
    info->setWindowFlag(0, FpsInfo::FLAG_MULTI_WINDOW);
    //info->forbidAdjustingVsync(false);

    int i = 0;
    printf("press any key to release client\n");
    scanf("%d", &i);

    policy->cancelFpsRequestListener();
    policy = NULL;
    info = NULL;

    printf("press any key to exit\n");
    scanf("%d", &i);

    return 0;
}
