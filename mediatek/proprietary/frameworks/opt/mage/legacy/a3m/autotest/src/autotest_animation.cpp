/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Animation and AnimationController unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/animation.h>           /* for Animation and AnimationController */
#include <autotest_common.h>         /* for AutotestTest                      */

using namespace a3m;

namespace
{

  A3M_FLOAT const EPSILON = 1e-3f;

  typedef AnimationKey<A3M_INT32> IntKey;

  A3M_INT32 const KEY_COUNT = 4;

  // The keys are unordered or purpose to test the sorting functionality.
  IntKey const KEYS[KEY_COUNT] =
  {
    IntKey(4.0f, 8),
    IntKey(2.0f, 6),
    IntKey(1.0f, 5),
    IntKey(3.0f, 7)
  };

  struct IntAnimator : public Animator<A3M_INT32>
  {
    typedef SharedPtr<IntAnimator> Ptr;

    A3M_INT32 v;

    IntAnimator() :
      v(0)
    {
    }

    void apply(A3M_INT32 const& value)
    {
      v = value;
    }
  };

  /*
   * Test fixture.
   */
  struct A3mAnimationTest : public AutotestTest
  {
    Animation::Ptr animation;
    AnimationController::Ptr controller;
    IntAnimator::Ptr animator;

    A3mAnimationTest()
    {
      animator.reset(new IntAnimator());
      animation = createAnimationChannel<A3M_INT32>(
                    createAnimationKeySequence(KEYS, KEY_COUNT), animator);
      controller.reset(new AnimationController(animation));
    }
  };

} // namespace

/*
 * Validate state after construction.
 */
TEST_F(A3mAnimationTest, InitialState)
{
  EXPECT_FLOAT_EQ(1.0f, animation->getStart());
  EXPECT_FLOAT_EQ(4.0f, animation->getEnd());

  EXPECT_FLOAT_EQ(0.0f, controller->getProgress());
  EXPECT_TRUE(controller->getEnabled());
  EXPECT_FALSE(controller->getPaused());
  EXPECT_TRUE(controller->getLooping());
  EXPECT_FLOAT_EQ(1.0f, controller->getSpeed());
  EXPECT_FLOAT_EQ(1.0f, controller->getStart());
  EXPECT_FLOAT_EQ(4.0f, controller->getEnd());
  EXPECT_FLOAT_EQ(0.0f, controller->getLoopStart());
  EXPECT_FLOAT_EQ(0.0f, controller->getLoopEnd());
  EXPECT_EQ(animation, controller->getAnimation());

  EXPECT_EQ(0, animator->v);
}

/*
 * Validate setters and getters.
 */
TEST_F(A3mAnimationTest, SetGet)
{
  controller->setProgress(3.0f);
  EXPECT_FLOAT_EQ(3.0f, controller->getProgress());

  controller->setEnabled(A3M_FALSE);
  EXPECT_FALSE(controller->getEnabled());

  controller->setPaused(A3M_TRUE);
  EXPECT_TRUE(controller->getPaused());

  controller->setLooping(A3M_FALSE);
  EXPECT_FALSE(controller->getLooping());

  controller->setSpeed(0.5f);
  EXPECT_FLOAT_EQ(0.5f, controller->getSpeed());

  controller->setStart(2.0f);
  EXPECT_FLOAT_EQ(2.0f, controller->getStart());

  controller->setEnd(5.0f);
  EXPECT_FLOAT_EQ(5.0f, controller->getEnd());

  controller->setStart(3.5f);
  EXPECT_FLOAT_EQ(3.5f, controller->getStart());

  controller->setEnd(4.5f);
  EXPECT_FLOAT_EQ(4.5f, controller->getEnd());
}

/*
 * Validate convenience functions.
 */
TEST_F(A3mAnimationTest, Convenience)
{
  setRange(*controller, 2.0f, 6.0f);
  EXPECT_FLOAT_EQ(2.0f, controller->getStart());
  EXPECT_FLOAT_EQ(6.0f, controller->getEnd());

  setLoopRange(*controller, 3.5f, 4.5f);
  EXPECT_FLOAT_EQ(3.5f, controller->getLoopStart());
  EXPECT_FLOAT_EQ(4.5f, controller->getLoopEnd());

  EXPECT_FLOAT_EQ(3.0f, getLength(*animation));
  EXPECT_FLOAT_EQ(4.0f, getLength(*controller));
  EXPECT_FLOAT_EQ(1.0f, getLoopLength(*controller));

  EXPECT_TRUE(hasLoop(*controller));
  EXPECT_FALSE(isInsideLoop(*controller));

  controller->setProgress(3.5f);
  EXPECT_TRUE(isInsideLoop(*controller));

  controller->setProgress(4.5f);
  EXPECT_TRUE(isInsideLoop(*controller));

  controller->setProgress(4.0f);
  EXPECT_TRUE(isInsideLoop(*controller));

  controller->setLooping(A3M_FALSE);
  EXPECT_TRUE(hasLoop(*controller));
  EXPECT_FALSE(isInsideLoop(*controller));

  controller->setLooping(A3M_TRUE);
  setLoopRange(*controller, 4.0f, 4.0f);
  EXPECT_FALSE(hasLoop(*controller));
  EXPECT_FALSE(isInsideLoop(*controller));

  EXPECT_FALSE(isFinished(*controller));

  controller->setProgress(6.0f);
  EXPECT_TRUE(isFinished(*controller));

  controller->setSpeed(-1.0f);
  EXPECT_FALSE(isFinished(*controller));

  controller->setProgress(2.0f);
  EXPECT_TRUE(isFinished(*controller));

  setLoopRange(*controller, 2.0f, 4.0f);
  EXPECT_FALSE(isFinished(*controller));

  controller->setSpeed(1.0f);
  controller->setProgress(6.0f);
  setLoopRange(*controller, 2.0f, 6.0f);
  EXPECT_FALSE(isFinished(*controller));

  controller->setProgress(6.0f);
  controller->setLooping(A3M_FALSE);
  EXPECT_TRUE(isFinished(*controller));

  controller->setSpeed(-1.0f);
  controller->setEnabled(A3M_FALSE);
  controller->setPaused(A3M_TRUE);
  play(*controller, A3M_FALSE);
  EXPECT_TRUE(controller->getEnabled());
  EXPECT_FALSE(controller->getPaused());
  EXPECT_FLOAT_EQ(6.0f, controller->getProgress());
  EXPECT_EQ(0, animator->v);

  controller->setProgress(2.0f);
  play(*controller);
  EXPECT_TRUE(controller->getEnabled());
  EXPECT_FALSE(controller->getPaused());
  EXPECT_FLOAT_EQ(6.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);

  controller->setProgress(3.0f);
  pause(*controller, A3M_FALSE);
  EXPECT_TRUE(controller->getEnabled());
  EXPECT_TRUE(controller->getPaused());
  EXPECT_FLOAT_EQ(3.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);

  controller->setProgress(2.0f);
  pause(*controller);
  EXPECT_TRUE(controller->getEnabled());
  EXPECT_TRUE(controller->getPaused());
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->setProgress(4.0f);
  stop(*controller);
  EXPECT_FALSE(controller->getEnabled());
  EXPECT_TRUE(controller->getPaused());
  EXPECT_FLOAT_EQ(6.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);

  controller->setSpeed(1.0f);
  play(*controller, A3M_FALSE);
  EXPECT_TRUE(controller->getEnabled());
  EXPECT_FALSE(controller->getPaused());
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);

  controller->setProgress(6.0f);
  play(*controller);
  EXPECT_TRUE(controller->getEnabled());
  EXPECT_FALSE(controller->getPaused());
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->setProgress(4.0f);
  play(*controller);
  EXPECT_TRUE(controller->getEnabled());
  EXPECT_FALSE(controller->getPaused());
  EXPECT_FLOAT_EQ(4.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);

  stop(*controller, A3M_FALSE);
  EXPECT_FALSE(controller->getEnabled());
  EXPECT_FALSE(controller->getPaused());
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);

  controller->setProgress(4.0f);
  stop(*controller);
  EXPECT_FALSE(controller->getEnabled());
  EXPECT_FALSE(controller->getPaused());
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->setSpeed(-1.0f);
  controller->setEnabled(A3M_TRUE);
  rewind(*controller);
  EXPECT_FLOAT_EQ(6.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);

  controller->setSpeed(1.0f);
  rewind(*controller, false);
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);

  controller->setProgress(6.0f);
  rewind(*controller);
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  seek(*controller, 3.0f);
  EXPECT_FLOAT_EQ(3.0f, controller->getProgress());
  EXPECT_EQ(7, animator->v);
}

/*
 * Test updating without loop points.
 */
TEST_F(A3mAnimationTest, UpdateNoLoop)
{
  EXPECT_FLOAT_EQ(0.0f, controller->getProgress());
  EXPECT_EQ(0, animator->v);

  controller->update(0.5f);
  EXPECT_FLOAT_EQ(1.5f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(2.5f, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(3.5f, controller->getProgress());
  EXPECT_EQ(7, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(4.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);
}

/*
 * Test updating with loop points.
 */
TEST_F(A3mAnimationTest, UpdateLoop)
{
  setLoopRange(*controller, 1.0f, 4.0f);

  controller->update(2.5f);
  EXPECT_FLOAT_EQ(3.5f, controller->getProgress());
  EXPECT_EQ(7, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(1.5f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  setLoopRange(*controller, 2.0f, 3.0f);

  controller->update(2.0f);
  EXPECT_FLOAT_EQ(2.5f, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->setLooping(A3M_FALSE);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(3.5f, controller->getProgress());
  EXPECT_EQ(7, animator->v);
}

/*
 * Test the limits of animation.
 */
TEST_F(A3mAnimationTest, Limits)
{
  setLoopRange(*controller, 1.0f, 4.0f);

  controller->update();
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  controller->update(EPSILON);
  EXPECT_FLOAT_EQ(1.0f + EPSILON, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  controller->update(1.0f - 2.0f * EPSILON);
  EXPECT_FLOAT_EQ(2.0f - EPSILON, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  controller->update(EPSILON);
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->update(EPSILON);
  EXPECT_FLOAT_EQ(2.0f + EPSILON, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->update(1.0f - 2.0f * EPSILON);
  EXPECT_FLOAT_EQ(3.0f - EPSILON, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->update(EPSILON);
  EXPECT_FLOAT_EQ(3.0f, controller->getProgress());
  EXPECT_EQ(7, animator->v);

  controller->update(EPSILON);
  EXPECT_FLOAT_EQ(3.0f + EPSILON, controller->getProgress());
  EXPECT_EQ(7, animator->v);

  controller->update(1.0f - 2.0f * EPSILON);
  EXPECT_FLOAT_EQ(4.0f - EPSILON, controller->getProgress());
  EXPECT_EQ(7, animator->v);

  controller->update(EPSILON);
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());
  EXPECT_EQ(5, animator->v);
}

/*
 * Test playback at half speed.
 */
TEST_F(A3mAnimationTest, HalfSpeed)
{
  controller->setSpeed(0.5f);

  controller->update();
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(1.5f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(2.5f, controller->getProgress());
  EXPECT_EQ(6, animator->v);
}

/*
 * Test playback at double speed.
 */
TEST_F(A3mAnimationTest, DoubleSpeed)
{
  controller->setSpeed(2.0f);

  controller->update();
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(3.0f, controller->getProgress());
  EXPECT_EQ(7, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(4.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);
}

/*
 * Test reverse playback without loop points.
 */
TEST_F(A3mAnimationTest, ReverseNoLoop)
{
  controller->setSpeed(-1.0f);

  controller->update();
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  rewind(*controller);
  EXPECT_FLOAT_EQ(4.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);

  controller->update(0.5f);
  EXPECT_FLOAT_EQ(3.5f, controller->getProgress());
  EXPECT_EQ(7, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(2.5f, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(1.5f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());
  EXPECT_EQ(5, animator->v);
}

/*
 * Test reverse playback with loop points.
 */
TEST_F(A3mAnimationTest, ReverseLoop)
{
  controller->setSpeed(-1.0f);
  rewind(*controller);

  setLoopRange(*controller, 1.0f, 4.0f);

  controller->update(2.5f);
  EXPECT_FLOAT_EQ(1.5f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(3.5f, controller->getProgress());
  EXPECT_EQ(7, animator->v);

  setLoopRange(*controller, 2.0f, 3.0f);

  controller->update(2.0f);
  EXPECT_FLOAT_EQ(2.5f, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->setLooping(A3M_FALSE);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(1.5f, controller->getProgress());
  EXPECT_EQ(5, animator->v);
}

/*
 * Test loop functionality at the limits of the animation.
 */
TEST_F(A3mAnimationTest, LoopLimits)
{
  setLoopRange(*controller, 1.0f, 4.0f);

  controller->update();
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());

  controller->update();
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());

  controller->update(3.0f);
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());

  controller->update(-1.0f);
  EXPECT_FLOAT_EQ(3.0f, controller->getProgress());

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());

  controller->update(-1.0f);
  EXPECT_FLOAT_EQ(4.0f, controller->getProgress());

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());

  setLoopRange(*controller, 2.0f, 3.0f);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());

  controller->update(-1.0f);
  EXPECT_FLOAT_EQ(3.0f, controller->getProgress());

  controller->update(-0.5f);
  EXPECT_FLOAT_EQ(2.5f, controller->getProgress());

  controller->update(-0.5f);
  EXPECT_FLOAT_EQ(3.0f, controller->getProgress());
}

/*
 * Test exiting loops.
 */
TEST_F(A3mAnimationTest, ExitLoop)
{
  setLoopRange(*controller, 2.0f, 3.0f);

  controller->update(1.5f);
  EXPECT_FLOAT_EQ(2.5f, controller->getProgress());
  EXPECT_EQ(6, animator->v);
  EXPECT_TRUE(isInsideLoop(*controller));

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(2.5f, controller->getProgress());
  EXPECT_EQ(6, animator->v);
  EXPECT_TRUE(isInsideLoop(*controller));

  seek(*controller, 3.0f + EPSILON);
  EXPECT_FLOAT_EQ(3.0f + EPSILON, controller->getProgress());
  EXPECT_EQ(7, animator->v);
  EXPECT_FALSE(isInsideLoop(*controller));

  controller->update(2.0f);
  EXPECT_FLOAT_EQ(4.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);
  EXPECT_FALSE(isInsideLoop(*controller));

  controller->update(-1.0f);
  EXPECT_FLOAT_EQ(3.0f, controller->getProgress());
  EXPECT_EQ(7, animator->v);
  EXPECT_TRUE(isInsideLoop(*controller));

  controller->update(0.5f);
  EXPECT_FLOAT_EQ(2.5f, controller->getProgress());
  EXPECT_EQ(6, animator->v);
  EXPECT_TRUE(isInsideLoop(*controller));

  seek(*controller, 2.0f - EPSILON);
  EXPECT_FLOAT_EQ(2.0f - EPSILON, controller->getProgress());
  EXPECT_EQ(5, animator->v);
  EXPECT_FALSE(isInsideLoop(*controller));

  controller->update(-2.0f);
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());
  EXPECT_EQ(5, animator->v);
  EXPECT_FALSE(isInsideLoop(*controller));

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(6, animator->v);
  EXPECT_TRUE(isInsideLoop(*controller));

  controller->update(-0.5f);
  EXPECT_FLOAT_EQ(2.5f, controller->getProgress());
  EXPECT_EQ(6, animator->v);
  EXPECT_TRUE(isInsideLoop(*controller));
}

/*
 * Test pausing of playback.
 */
TEST_F(A3mAnimationTest, Pause)
{
  setLoopRange(*controller, 1.0f, 4.0f);
  controller->setPaused(A3M_TRUE);

  controller->update();
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  seek(*controller, 2.0f);
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->setPaused(A3M_FALSE);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(3.0f, controller->getProgress());
  EXPECT_EQ(7, animator->v);

  controller->setProgress(4.0f);
  controller->setPaused(A3M_TRUE);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(4.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);

  controller->setProgress(5.0f);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(4.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);
}

/*
 * Test setting speed to zero.
 */
TEST_F(A3mAnimationTest, ZeroSpeed)
{
  setLoopRange(*controller, 1.0f, 4.0f);
  controller->setSpeed(0.0f);

  controller->update();
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(1.0f, controller->getProgress());
  EXPECT_EQ(5, animator->v);

  seek(*controller, 2.0f);
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(6, animator->v);

  controller->setSpeed(1.0f);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(3.0f, controller->getProgress());
  EXPECT_EQ(7, animator->v);

  controller->setProgress(4.0f);
  controller->setSpeed(0.0f);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(4.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);

  controller->setProgress(5.0f);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(4.0f, controller->getProgress());
  EXPECT_EQ(8, animator->v);
}

/*
 * Test enabling and disabling of playback.
 */
TEST_F(A3mAnimationTest, Enable)
{
  setLoopRange(*controller, 1.0f, 4.0f);
  controller->setEnabled(A3M_FALSE);

  controller->update();
  EXPECT_FLOAT_EQ(0.0f, controller->getProgress());
  EXPECT_EQ(0, animator->v);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(0.0f, controller->getProgress());
  EXPECT_EQ(0, animator->v);

  controller->setProgress(2.0f);
  EXPECT_FLOAT_EQ(2.0f, controller->getProgress());
  EXPECT_EQ(0, animator->v);

  controller->setEnabled(A3M_TRUE);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(3.0f, controller->getProgress());
  EXPECT_EQ(7, animator->v);

  controller->setProgress(4.0f);
  controller->setEnabled(A3M_FALSE);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(4.0f, controller->getProgress());
  EXPECT_EQ(7, animator->v);

  controller->setProgress(5.0f);

  controller->update(1.0f);
  EXPECT_FLOAT_EQ(5.0f, controller->getProgress());
  EXPECT_EQ(7, animator->v);
}
