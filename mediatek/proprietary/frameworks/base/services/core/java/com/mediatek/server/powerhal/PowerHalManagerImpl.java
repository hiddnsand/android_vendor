/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.server.powerhal;

//import android.content.Context;
//import android.os.IBinder;
//import android.os.RemoteException;
//import android.os.ServiceManager;
import android.util.Log;
/*
import com.mediatek.am.AMEventHookData;
import com.mediatek.am.AMEventHookData.AfterActivityDestroyed;
import com.mediatek.am.AMEventHookData.AfterActivityPaused;
import com.mediatek.am.AMEventHookData.AfterActivityResumed;
import com.mediatek.am.AMEventHookData.AfterActivityStopped;
import com.mediatek.am.AMEventHookData.BeforeActivitySwitch;
import com.mediatek.am.AMEventHookData.StartProcess;
*/
import com.mediatek.powerhalwrapper.PowerHalWrapper;
import java.lang.reflect.Method;

/// MTK power
import vendor.mediatek.hardware.power.V1_1.*;

public class PowerHalManagerImpl extends PowerHalManager {
    private static final String TAG = "PowerHalManagerImpl";

    public boolean mIsRotationBoostEnable = false;

    private static boolean sHasChecked = false;
    private static boolean sIsMtkPowerHalExist = false;

    private static Class<?> clazz = null;
    private static Method methodSetRotationBoost = null;
    private static Method methodAmsBoostResume = null;
    private static Method methodAmsBoostNotify = null;
    private static Method methodAmsBoostProcessCreate = null;
    private static Method methodAmsBoostStop = null;

    @Override
    public void setRotationBoost(boolean enable) {
        int boostTime = 0;
        // boostTime > 0 means enable boost
        if (enable && !mIsRotationBoostEnable) {
            boostTime = PowerHalManager.ROTATE_BOOST_TIME;
        } else if (!enable && mIsRotationBoostEnable) {
            boostTime = 0;
        }

        if (checkWetherSupport()) {
            if (clazz != null && methodSetRotationBoost != null) {
                //Log.e(TAG, "<setRotationBoost> do boost with " + enable);
                try {
                    Object powerHal = clazz.newInstance();
                    methodSetRotationBoost.invoke(powerHal, boostTime);
                    mIsRotationBoostEnable = enable;
                } catch (Exception e) {
                    Log.e(TAG, "<setRotationBoost> Exception", e);
                }
            }
        }
    }

    public void amsBoostResume(String lastResumedPackageName, String nextResumedPackageName) {
        if (checkWetherSupport()) {
            if (clazz != null && methodAmsBoostResume != null) {
                //Log.e(TAG, "<amsBoostResume> last:" + lastResumedPackageName +
                //           ", next:" + nextResumedPackageName);
                try {
                    Object powerHal = clazz.newInstance();
                    methodAmsBoostResume.invoke(powerHal,
                                                lastResumedPackageName,
                                                nextResumedPackageName);
                } catch (Exception e) {
                    Log.e(TAG, "<amsBoostResume> Exception", e);
                }
            }
        }
    }

    public void amsBoostNotify(int pid, String activityName, String packageName) {
        if (checkWetherSupport()) {
            if (clazz != null && methodAmsBoostNotify != null) {
                //Log.e(TAG, "amsBoostNotify pid:" + pid +
                //           ", activity:" + activityName + ", package:" + packageName);
                try {
                    Object powerHal = clazz.newInstance();
                    methodAmsBoostNotify.invoke(powerHal, pid, activityName, packageName);
                } catch (Exception e) {
                    Log.e(TAG, "<amsBoostNotify> Exception", e);
                }
            }
        }
    }

    public void amsBoostProcessCreate(String hostingType, String packageName) {
        if (checkWetherSupport()) {
            if (clazz != null && methodAmsBoostProcessCreate != null) {
                //Log.e(TAG, "amsBoostProcessCreate hostingType:" + hostingType +
                //           ", package:" + packageName);
                try {
                    Object powerHal = clazz.newInstance();
                    methodAmsBoostProcessCreate.invoke(powerHal, hostingType, packageName);
                } catch (Exception e) {
                    Log.e(TAG, "<amsBoostProcessCreate> Exception", e);
                }
            }
        }
    }

    public void amsBoostStop() {
        if (checkWetherSupport()) {
            if (clazz != null && methodAmsBoostStop != null) {
                //Log.e(TAG, "amsBoostStop");
                try {
                    Object powerHal = clazz.newInstance();
                    methodAmsBoostStop.invoke(powerHal);
                } catch (Exception e) {
                    Log.e(TAG, "<amsBoostStop> Exception", e);
                }
            }
        }
    }

    private static boolean checkWetherSupport() {
        if (!sHasChecked) {
            Log.e(TAG, "<checkWetherSupport> sHasChecked: " + sHasChecked);
            try {
                clazz = PowerHalManagerImpl.class.getClassLoader().loadClass(
                        "com.mediatek.powerhalwrapper.PowerHalWrapper");
                Log.i(TAG, "<checkWetherSupport> clazz: " + clazz);
                sIsMtkPowerHalExist = (clazz != null);

                methodSetRotationBoost = clazz.getDeclaredMethod(
                                 "setRotationBoost", int.class);
                Log.i(TAG, "<checkWetherSupport> methodSetRotationBoost: "
                                                  + methodSetRotationBoost);
                methodSetRotationBoost.setAccessible(true);

                methodAmsBoostResume = clazz.getDeclaredMethod(
                                 "amsBoostResume", String.class, String.class);
                Log.i(TAG, "<checkWetherSupport> methodAmsBoostResume: "
                                                  + methodAmsBoostResume);
                methodAmsBoostResume.setAccessible(true);

                methodAmsBoostNotify = clazz.getDeclaredMethod(
                                 "amsBoostNotify", int.class, String.class, String.class);
                Log.i(TAG, "<checkWetherSupport> methodAmsBoostNotify: "
                                                  + methodAmsBoostNotify);
                methodAmsBoostNotify.setAccessible(true);

                methodAmsBoostProcessCreate = clazz.getDeclaredMethod(
                                 "amsBoostProcessCreate", String.class, String.class);
                Log.i(TAG, "<checkWetherSupport> methodAmsBoostProcessCreate: "
                                                  + methodAmsBoostProcessCreate);
                methodAmsBoostProcessCreate.setAccessible(true);

                methodAmsBoostStop = clazz.getDeclaredMethod("amsBoostStop");
                Log.i(TAG, "<checkWetherSupport> methodAmsBoostStop: "
                                                  + methodAmsBoostStop);
                methodAmsBoostStop.setAccessible(true);

                sIsMtkPowerHalExist = true;
            } catch (ClassNotFoundException e) {
                sIsMtkPowerHalExist = false;
                Log.e(TAG, "<checkWetherSupport> ClassNotFoundException", e);
            } catch (NoSuchMethodException e) {
                sIsMtkPowerHalExist = false;
                Log.e(TAG, "<checkWetherSupport> NoSuchMethodException", e);
            }
            sHasChecked = true;
            Log.d(TAG, "<checkWetherSupport> sIsMtkPowerHalExist: " + sIsMtkPowerHalExist);
        }
        return sIsMtkPowerHalExist;
    }
}
