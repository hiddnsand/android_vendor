#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "SpeechParamParser"
#include "SpeechParamParser.h"
#include "utstring.h"
#include <utils/Log.h>
#include <inttypes.h>
#include <media/AudioParameter.h>

#include "AudioUtility.h"//Mutex/assert
#include "AudioALSAStreamManager.h"


namespace android {
#define XML_FOLDER "/system/etc/audio_param/"

#define MAX_BYTE_PARAM_SPEECH 3434

#define SPH_DUMP_STR_SIZE (500)

//--------------------------------------------------------------------------------
//audio type: Speech
#define MAX_NUM_CATEGORY_TYPE_SPEECH 4
#define MAX_NUM_PARAM_SPEECH 3
const String8 audioType_Speech_CategoryType[ ] = {String8("Band"), String8("Profile"), String8("VolIndex"), String8("Network")};
const String8 audioType_Speech_ParamName[ ] = {String8("speech_mode_para"), String8("sph_in_fir"), String8("sph_out_fir"), String8("sph_in_iir_mic1_dsp"), String8("sph_in_iir_mic2_dsp"), String8("sph_in_iir_enh_dsp"), String8("sph_out_iir_enh_dsp")};

#define NUM_VOLUME_SPEECH 7
const char audioType_Speech_CategoryName3[NUM_VOLUME_SPEECH][128] = {"0", "1", "2", "3", "4", "5", "6"};
const char audioType_Speech_CategoryName2[SPEECH_PROFILE_MAX_NUM][128] = {
    "Normal",
    "4_pole_Headset",
    "Handsfree",
    "BT_Earphone",
    "BT_NREC_Off",
    "MagiConference",
    "HAC",
    "Lpbk_Handset",
    "Lpbk_Headset",
    "Lpbk_Handsfree",
    "3_pole_Headset",
    "5_pole_Headset",
    "5_pole_Headset+ANC",
    "Usb_Headset",
    "Handset_SV",
    "Handsfree_SV",
    "Tty_HCO_Handset",
    "Tty_HCO_Handsfree",
    "Tty_VCO_Handset",
    "Tty_VCO_Handsfree"
};

//--------------------------------------------------------------------------------
//audio type: SpeechDMNR
#define MAX_NUM_CATEGORY_TYPE_SPEECH_DMNR 2
#define MAX_NUM_PARAM_SPEECH_DMNR 1
const String8 audioType_SpeechDMNR_CategoryType[ ] = {String8("Band"), String8("Profile")};
const char audioType_SpeechDMNR_CategoryName2[2][128] = {"Handset", "MagiConference"};
const String8 audioType_SpeechDMNR_ParamName[ ] = {String8("dmnr_para")};

//--------------------------------------------------------------------------------
//audio type: SpeechGeneral
#define MAX_NUM_CATEGORY_TYPE_SPEECH_GENERAL 1
#define MAX_NUM_PARAM_SPEECH_GENERAL 2
const String8 audioType_SpeechGeneral_CategoryType[ ] = {String8("CategoryLayer")};
const char audioType_SpeechGeneral_CategoryName1[1][128] = {"Common"};
const String8 audioType_SpeechGeneral_ParamName[ ] = {String8("speech_common_para"), String8("debug_info")};

//--------------------------------------------------------------------------------
//audio type: SpeechMagiClarity
#define MAX_NUM_CATEGORY_TYPE_SPEECH_MAGICLARITY 1
#define MAX_NUM_PARAM_SPEECH_MAGICLARITY 1
const String8 audioType_SpeechMagiClarity_CategoryType[ ] = {String8("CategoryLayer")};
const char audioType_SpeechMagiClarity_CategoryName1[1][128] = {"Common"};
const String8 audioType_SpeechMagiClarity_ParamName[ ] = {String8("shape_rx_fir_para")};

//--------------------------------------------------------------------------------
//audio type: SpeechNetwork
#define MAX_NUM_CATEGORY_TYPE_SPEECH_NETWORK 1
#define MAX_NUM_PARAM_SPEECH_NETWORK 1
const String8 audioType_SpeechNetwork_CategoryType[ ] = {String8("Network")};
const String8 audioType_SpeechNetwork_ParamName[ ] = {String8("speech_network_support")};


//--------------------------------------------------------------------------------
//audio type: SpeechEchoRef
#define MAX_NUM_CATEGORY_TYPE_SPEECH_ECHOREF 1
#define MAX_NUM_PARAM_SPEECH_ECHOREF 1
const String8 audioType_SpeechEchoRef_CategoryType[ ] = {String8("Device")};
const char audioType_SpeechEchoRef_CategoryName1[1][128] = {"USBAudio"};
const String8 audioType_SpeechEchoRef_ParamName[ ] = {String8("EchoRef_para")};


/*==============================================================================
 *                     Property keys
 *============================================================================*/
const char PROPERTY_KEY_SPEECHLOG_ON[PROPERTY_KEY_MAX] = "persist.af.speech_log_on";


/*==============================================================================
 *                     Singleton Pattern
 *============================================================================*/

SpeechParamParser *SpeechParamParser::UniqueSpeechParamParser = NULL;


SpeechParamParser *SpeechParamParser::getInstance() {
    static Mutex mGetInstanceLock;
    Mutex::Autolock _l(mGetInstanceLock);
    ALOGV("%s()", __FUNCTION__);

    if (UniqueSpeechParamParser == NULL) {
        UniqueSpeechParamParser = new SpeechParamParser();
    }
    ASSERT(UniqueSpeechParamParser != NULL);
    return UniqueSpeechParamParser;
}
/*==============================================================================
 *                     Constructor / Destructor / Init / Deinit
 *============================================================================*/

SpeechParamParser::SpeechParamParser() {
    ALOGD("%s()", __FUNCTION__);
    mSphParamInfo.SpeechMode = SPEECH_MODE_NORMAL;
    mSphParamInfo.u4VolumeIndex = 3;
    mSphParamInfo.bBtHeadsetNrecOn = false;
    mSphParamInfo.bLPBK = false;
    mSphParamInfo.uHeadsetPole = 4;
    mSphParamInfo.isSingleBandTransfer = false;
    mSphParamInfo.IndexVoiceBandStart = 0;
    mSphParamInfo.bSV = false;
    mSphParamInfo.idxTTY = 0;
    mSphParamSupport.IsNetworkSupport = false;
    mSphParamSupport.IsTTYSupport = false;
    mSphParamSupport.IsSuperVolumeSupport = false;
    mSpeechParamVerFirst = 0;
    mSpeechParamVerLast = 0;
    mAppHandle = NULL;
    numSpeechParam = 3;

    Init();
}

SpeechParamParser::~SpeechParamParser() {
    ALOGD("%s()", __FUNCTION__);

}

void SpeechParamParser::Init() {
    ALOGD("%s()", __FUNCTION__);
    InitAppParser();
    InitSpeechNetwork();

    AppOps *appOps = appOpsGetInstance();
    if (appOps == NULL) {
        ALOGE("Error %s %d", __FUNCTION__, __LINE__);
        ASSERT(0);
    } else {
        const char *strSphVersion = appOps->appHandleGetFeatureOptionValue(mAppHandle, "SPH_PARAM_VERSION");
        if (strSphVersion != NULL) {
            sscanf(strSphVersion, "%" SCNd8 ".%" SCNd8, &mSpeechParamVerFirst, &mSpeechParamVerLast);
            switch (mSpeechParamVerFirst) {
            case 2:
                mSphParamSupport.IsNetworkSupport = true;
                numSpeechParam = 7;
                break;
            case 1:
                mSphParamSupport.IsNetworkSupport = true;
                numSpeechParam = 3;
                break;
            default:
                mSphParamSupport.IsNetworkSupport = false;
                numSpeechParam = 3;
                break;
            }
        } else {
            mSpeechParamVerFirst = 0;
            mSpeechParamVerLast = 0;
            mSphParamSupport.IsNetworkSupport = false;
            numSpeechParam = 3;
        }
        const char *strSphTTY = appOps->appHandleGetFeatureOptionValue(mAppHandle, "SPH_PARAM_TTY");
        if (strSphTTY != NULL) {
            if (strcmp(strSphTTY, "yes") == 0) {
                mSphParamSupport.IsTTYSupport = true;
            } else {
                mSphParamSupport.IsTTYSupport = false;
            }
        } else {
            mSphParamSupport.IsTTYSupport = false;
        }

        const char *strSphSV = appOps->appHandleGetFeatureOptionValue(mAppHandle, "SPH_PARAM_SV");
        if (strSphSV != NULL) {
            if (strcmp(strSphSV, "yes") == 0) {
                mSphParamSupport.IsSuperVolumeSupport = true;
            } else {
                mSphParamSupport.IsSuperVolumeSupport = false;
            }
        } else {
            mSphParamSupport.IsSuperVolumeSupport = false;
        }
    }
}

void SpeechParamParser::Deinit() {
    ALOGD("%s()", __FUNCTION__);
}


/*==============================================================================
 *                     SpeechParamParser Imeplementation
 *============================================================================*/
bool SpeechParamParser::GetSpeechParamSupport(const char *ParamName) {
    bool mSupport = false;
    if (ParamName != NULL) {
        if (strcmp(ParamName, "SPH_PARAM_TTY") == 0) {
            mSupport = mSphParamSupport.IsTTYSupport;
        } else if (strcmp(ParamName, "SPH_PARAM_SV") == 0) {
            mSupport = mSphParamSupport.IsSuperVolumeSupport;
        } else {
            mSupport = false;
        }
        ALOGD("%s(), %s = %d", __FUNCTION__, ParamName, mSupport);
    } else {
        mSupport = false;
    }
    return mSupport;
}

void SpeechParamParser::InitAppParser() {
    ALOGD("+%s()", __FUNCTION__);
    /* Init AppHandle */
    ALOGD("%s() appHandleGetInstance", __FUNCTION__);
    AppOps *appOps = appOpsGetInstance();
    if (appOps == NULL) {
        ALOGE("Error %s %d", __FUNCTION__, __LINE__);
        ASSERT(0);
        return;
    }
    mAppHandle = appOps->appHandleGetInstance();
    ALOGD("%s() appHandleRegXmlChangedCb", __FUNCTION__);

}

status_t SpeechParamParser::SpeechDataDump(uint16_t uSpeechTypeIndex, const char *nameParam, const char *SpeechParamData) {
    if (nameParam == NULL) {
        return NO_ERROR;
    }
    // Speech Log system property
    char property_value[PROPERTY_VALUE_MAX];
    property_get(PROPERTY_KEY_SPEECHLOG_ON, property_value, "0"); //"0": default off
    if (property_value[0] == '0') {
#if !defined(CONFIG_MT_ENG_BUILD) // user or user debug load
        return NO_ERROR;
#endif
    }

    ALOGV("+%s(), uSpeechTypeIndex=%d", __FUNCTION__, uSpeechTypeIndex);
    char SphDumpStr[SPH_DUMP_STR_SIZE] = {0};
    int u4I = 0, idxDump = 0, DataTypePrint = 0;
    //speech parameter dump

    switch (uSpeechTypeIndex) {
    case AUDIO_TYPE_SPEECH: {
        if (strcmp(nameParam, "speech_mode_para") == 0) {
            idxDump = 16;
        } else if (strcmp(nameParam, "sph_in_fir") == 0) {
            idxDump = 5;
        } else if (strcmp(nameParam, "sph_out_fir") == 0) {
            idxDump = 5;
        } else if (strcmp(nameParam, "sph_in_iir_mic1_dsp") == 0) {
            idxDump = 5;
        } else if (strcmp(nameParam, "sph_in_iir_mic2_dsp") == 0) {
            idxDump = 5;
        } else if (strcmp(nameParam, "sph_in_iir_enh_dsp") == 0) {
            idxDump = 5;
        } else if (strcmp(nameParam, "sph_out_iir_enh_dsp") == 0) {
            idxDump = 5;
        }
        break;
    }
    case AUDIO_TYPE_SPEECH_GENERAL: {
        if (strcmp(nameParam, "speech_common_para") == 0) {
            idxDump = 12;
        } else if (strcmp(nameParam, "debug_info") == 0) {
            idxDump = 8;
        }
        break;
    }
    case AUDIO_TYPE_SPEECH_NETWORK: {
        if (strcmp(nameParam, "speech_network_support") == 0) {
            DataTypePrint = 1;
            idxDump = 1;
        }
        break;
    }
    case AUDIO_TYPE_SPEECH_ECHOREF: {
        if (strcmp(nameParam, "USBAudio") == 0) {
            idxDump = 3;
        }
        break;
    }

    }
    if (idxDump != 0) {
        ALOGV("%s(), idxDump=%d", __FUNCTION__, idxDump);
    }
    snprintf(SphDumpStr, SPH_DUMP_STR_SIZE, "%s[%d]= ", nameParam, idxDump);

    for (u4I = 0; u4I < idxDump; u4I++) {
        char SphDumpTemp[100] = {0};
        if (DataTypePrint == 1) {
            snprintf(SphDumpTemp, 100, "[%d]0x%x, ", u4I, *((uint16_t *)SpeechParamData + u4I));
        } else {
            snprintf(SphDumpTemp, 100, "[%d]%d, ", u4I, *((uint16_t *)SpeechParamData + u4I));
        }
        audio_strncat(SphDumpStr, SphDumpTemp, SPH_DUMP_STR_SIZE);


        // ALOGD("%s() SpeechParam[%d]=%d", __FUNCTION__, u4I, *((uint16_t *)SpeechParamData + u4I));
    }

    if (u4I != 0) {
        ALOGD("%s(), %s, %s", __FUNCTION__, audioTypeNameList[uSpeechTypeIndex], SphDumpStr);
    }
    return NO_ERROR;
}


status_t SpeechParamParser::GetSpeechParamFromAppParser(uint16_t uSpeechTypeIndex, AUDIO_TYPE_SPEECH_LAYERINFO_STRUCT *mParamLayerInfo, char *pPackedParamUnit, uint16_t *sizeByteTotal) {
    ALOGV("+%s(), mParamLayerInfo->numCategoryType=0x%x", __FUNCTION__, mParamLayerInfo->numCategoryType);

    if (mAppHandle == NULL) {
        ALOGE("%s() mAppHandle == NULL, Assert!!!", __FUNCTION__);
        ASSERT(0);
        return UNKNOWN_ERROR;
    }

    char *categoryPath = NULL;
    ParamUnit *paramUnit = NULL;
    uint16_t  sizeByteParam = 0, u4Index;
    Param  *SpeechParam;
    UT_string *uts_categoryPath = NULL;

    /* If user select a category path, just like "NarrowBand / Normal of Handset / Level0" */
    utstring_new(uts_categoryPath);

    ALOGV("%s(), categoryType.size=%zu, paramName.size=%zu", __FUNCTION__, mParamLayerInfo->categoryType.size(), mParamLayerInfo->paramName.size());
    for (u4Index = 0; u4Index < mParamLayerInfo->categoryType.size() ; u4Index++) {
        ALOGV("%s(), categoryType[%d]= %s", __FUNCTION__, u4Index, mParamLayerInfo->categoryType.at(u4Index).string());
    }
    for (u4Index = 0; u4Index < mParamLayerInfo->categoryName.size() ; u4Index++) {
        ALOGV("%s(), categoryName[%d]= %s", __FUNCTION__, u4Index, mParamLayerInfo->categoryName.at(u4Index).string());
    }


    for (u4Index = 0; u4Index < mParamLayerInfo->numCategoryType ; u4Index++) {
        if (u4Index == mParamLayerInfo->numCategoryType - 1) {
            //last time concat
            utstring_printf(uts_categoryPath, "%s,%s", (char *)(mParamLayerInfo->categoryType.at(u4Index).string()), (char *)(mParamLayerInfo->categoryName.at(u4Index).string()));
        } else {
            utstring_printf(uts_categoryPath, "%s,%s,", (char *)(mParamLayerInfo->categoryType.at(u4Index).string()), (char *)(mParamLayerInfo->categoryName.at(u4Index).string()));
        }
    }
    categoryPath = strdup(utstring_body(uts_categoryPath));
    utstring_free(uts_categoryPath);

    ALOGV("%s() audioTypeName=%s", __FUNCTION__, mParamLayerInfo->audioTypeName);
    /* Query AudioType */
    AppOps *appOps = appOpsGetInstance();
    AudioType *audioType = NULL;
    if (appOps == NULL) {
        free(categoryPath);
        ALOGE("Error %s %d", __FUNCTION__, __LINE__);
        ASSERT(0);
        return UNKNOWN_ERROR;
    } else {
        audioType = appOps->appHandleGetAudioTypeByName(mAppHandle, mParamLayerInfo->audioTypeName);
    }
    if (!audioType) {
        free(categoryPath);
        ALOGE("%s() can't find audioTypeName=%s, Assert!!!", __FUNCTION__, mParamLayerInfo->audioTypeName);
        ASSERT(0);
        return UNKNOWN_ERROR;
    }

    /* Query the ParamUnit */
    appOps->audioTypeReadLock(audioType, __FUNCTION__);
    paramUnit = appOps->audioTypeGetParamUnit(audioType, categoryPath);
    if (!paramUnit) {
        appOps->audioTypeUnlock(audioType);
        ALOGE("%s() can't find paramUnit, Assert!!! audioType=%s, categoryPath=%s", __FUNCTION__, audioType->name, categoryPath);
        free(categoryPath);
        ASSERT(0);
        return UNKNOWN_ERROR;
    }
    ALOGD("%s() audioType=%s, categoryPath=%s, paramId=%d", __FUNCTION__, audioType->name, categoryPath, paramUnit->paramId);

    for (u4Index = 0; u4Index < (*mParamLayerInfo).numParam ; u4Index++) {

        SpeechParam = appOps->paramUnitGetParamByName(paramUnit, (const char *)mParamLayerInfo->paramName.at(u4Index).string());
        if (SpeechParam) {
            sizeByteParam = sizeByteParaData((DATA_TYPE)SpeechParam->paramInfo->dataType, SpeechParam->arraySize);
            memcpy(pPackedParamUnit + *sizeByteTotal, SpeechParam->data, sizeByteParam);
            *sizeByteTotal += sizeByteParam;
            ALOGV("%s() paramName=%s, sizeByteParam=%d", __FUNCTION__, mParamLayerInfo->paramName.at(u4Index).string(), sizeByteParam);
            //speech parameter dump
            SpeechDataDump(uSpeechTypeIndex, (const char *)mParamLayerInfo->paramName.at(u4Index).string(), (const char *)SpeechParam->data);
        }
    }

    appOps->audioTypeUnlock(audioType);
    free(categoryPath);

    return NO_ERROR;
}

uint16_t SpeechParamParser::sizeByteParaData(DATA_TYPE dataType, uint16_t arraySize) {
    uint16_t sizeUnit = 4;
    switch (dataType) {
    case TYPE_INT:
        sizeUnit = 4;
        break;
    case TYPE_UINT:
        sizeUnit = 4;
        break;
    case TYPE_FLOAT:
        sizeUnit = 4;
        break;
    case TYPE_BYTE_ARRAY:
        sizeUnit = arraySize;
        break;
    case TYPE_UBYTE_ARRAY:
        sizeUnit = arraySize;
        break;
    case TYPE_SHORT_ARRAY:
        sizeUnit = arraySize << 1;
        break;
    case TYPE_USHORT_ARRAY:
        sizeUnit = arraySize << 1;
        break;
    case TYPE_INT_ARRAY:
        sizeUnit = arraySize << 2;
        break;
    case TYPE_UINT_ARRAY:
        sizeUnit = arraySize << 2;
        break;
    default:
        ALOGE("%s(), Not an available dataType(%d)", __FUNCTION__, dataType);

        break;

    }

    ALOGV("-%s(), arraySize=%d, sizeUnit=%d", __FUNCTION__, arraySize, sizeUnit);

    return sizeUnit;


}


int SpeechParamParser::GetDmnrParamUnit(char *pPackedParamUnit) {
    ALOGV("+%s()", __FUNCTION__);
    uint16_t size = 0, u4IndexBand = 0, u4IndexProfile = 0, sizeByteFromApp = 0;
    uint16_t DataHeader, IdxTmp = 0, NumOfBand = 0, NumOfProfile = 0;
    SPEECH_DYNAMIC_PARAM_UNIT_HDR_STRUCT eParamUnitHdr;
    memset(&eParamUnitHdr, 0, sizeof(eParamUnitHdr));

    AUDIO_TYPE_SPEECH_LAYERINFO_STRUCT pParamLayerInfo;

    pParamLayerInfo.audioTypeName = (char *) audioTypeNameList[AUDIO_TYPE_SPEECH_DMNR];
    pParamLayerInfo.numCategoryType = MAX_NUM_CATEGORY_TYPE_SPEECH_DMNR;//4
    pParamLayerInfo.numParam = MAX_NUM_PARAM_SPEECH_DMNR;//4
    pParamLayerInfo.categoryType.assign(audioType_SpeechDMNR_CategoryType, audioType_SpeechDMNR_CategoryType + pParamLayerInfo.numCategoryType);
    pParamLayerInfo.paramName.assign(audioType_SpeechDMNR_ParamName, audioType_SpeechDMNR_ParamName + pParamLayerInfo.numParam);

    ALOGV("%s(), categoryType.size=%zu, paramName.size=%zu", __FUNCTION__, pParamLayerInfo.categoryType.size(), pParamLayerInfo.paramName.size());
    /* Query AudioType */
    AppOps *appOps = appOpsGetInstance();
    AudioType *audioType = NULL;
    if (appOps == NULL) {
        ALOGE("Error %s %d", __FUNCTION__, __LINE__);
        ASSERT(0);
        return UNKNOWN_ERROR;
    } else {
        audioType = appOps->appHandleGetAudioTypeByName(mAppHandle, pParamLayerInfo.audioTypeName);
    }
    char *pPackedParamUnitFromApp = new char [MAX_BYTE_PARAM_SPEECH];
    memset(pPackedParamUnitFromApp, 0, MAX_BYTE_PARAM_SPEECH);

    CategoryType *categoryBand = appOps->audioTypeGetCategoryTypeByName(audioType, audioType_SpeechDMNR_CategoryType[0].string());
    NumOfBand = appOps->categoryTypeGetNumOfCategory(categoryBand);
    CategoryType *categoryProfile = appOps->audioTypeGetCategoryTypeByName(audioType, audioType_SpeechDMNR_CategoryType[1].string());
    NumOfProfile = appOps->categoryTypeGetNumOfCategory(categoryProfile);
    IdxTmp = (NumOfBand & 0xF) << 4;
    eParamUnitHdr.NumEachLayer = IdxTmp + (NumOfProfile & 0xF);
    SetMDParamUnitHdr(AUDIO_TYPE_SPEECH_DMNR, &eParamUnitHdr, NumOfBand);

#if defined(CONFIG_MT_ENG_BUILD) // eng load
    char SphDumpStr[SPH_DUMP_STR_SIZE] = "MDParamUnitHdr ";
    int IdxDump = 0;
    for (IdxDump = 0; IdxDump < (int)(sizeof(eParamUnitHdr) >> 1); IdxDump++) { //uint16_t
        char SphDumpTemp[100];
        snprintf(SphDumpTemp, 100, "[%d]0x%x, ", IdxDump, *((uint16_t *)&eParamUnitHdr + IdxDump));
        audio_strncat(SphDumpStr, SphDumpTemp, SPH_DUMP_STR_SIZE);
    }
    if (IdxDump != 0) {
        ALOGD("%s(), %s", __FUNCTION__,  SphDumpStr);
    }
#endif
    memcpy(pPackedParamUnit + size, &eParamUnitHdr, sizeof(eParamUnitHdr));
    size += sizeof(eParamUnitHdr);

    for (u4IndexBand = 0; u4IndexBand < NumOfBand; u4IndexBand++) { //NB, WB, SWB
        for (u4IndexProfile = 0; u4IndexProfile < NumOfProfile; u4IndexProfile++) {
            sizeByteFromApp = 0;
            DataHeader = ((u4IndexBand + 1) << 4) + (u4IndexProfile + 1);
            memcpy(pPackedParamUnit + size, &DataHeader, sizeof(DataHeader));
            size += sizeof(DataHeader);
            Category *CateBand =  appOps->categoryTypeGetCategoryByIndex(categoryBand, u4IndexBand);
            pParamLayerInfo.categoryName.push_back(String8(CateBand->name));//Band
            pParamLayerInfo.categoryName.push_back(String8(audioType_SpeechDMNR_CategoryName2[u4IndexProfile]));//Profile

            GetSpeechParamFromAppParser(AUDIO_TYPE_SPEECH_DMNR, &pParamLayerInfo, pPackedParamUnitFromApp, &sizeByteFromApp);

            memcpy(pPackedParamUnit + size, pPackedParamUnitFromApp, sizeByteFromApp);
            size += sizeByteFromApp;
            ALOGD("%s(), DataHeader=0x%x, categoryName[%d,%d]= %s,%s, sizeByteFromApp=%d", __FUNCTION__, DataHeader, u4IndexBand, u4IndexProfile, pParamLayerInfo.categoryName.at(0).string(), pParamLayerInfo.categoryName.at(1).string(), sizeByteFromApp);
            pParamLayerInfo.categoryName.pop_back();
            pParamLayerInfo.categoryName.pop_back();

        }
    }

    if (pPackedParamUnitFromApp != NULL) {
        delete[] pPackedParamUnitFromApp;
    }

    ALOGD("-%s(), eParamUnitHdr.SphUnitMagiNum= 0x%x, total size byte=%d", __FUNCTION__, eParamUnitHdr.SphUnitMagiNum, size);
    return size;
}

int SpeechParamParser::GetGeneralParamUnit(char *pPackedParamUnit) {
    ALOGV("+%s()", __FUNCTION__);
    uint16_t size = 0, u4Index = 0, u4Index2 = 0, sizeByteFromApp = 0;
    uint16_t DataHeader;
    SPEECH_DYNAMIC_PARAM_UNIT_HDR_STRUCT eParamUnitHdr;
    memset(&eParamUnitHdr, 0, sizeof(eParamUnitHdr));

    eParamUnitHdr.SphParserVer = 1;
    eParamUnitHdr.NumLayer = 0x1;
    eParamUnitHdr.NumEachLayer = 0x1;
    eParamUnitHdr.ParamHeader[0] = 0x1;//Common
    eParamUnitHdr.SphUnitMagiNum = 0xAA02;

    memcpy(pPackedParamUnit + size, &eParamUnitHdr, sizeof(eParamUnitHdr));
    size += sizeof(eParamUnitHdr);

    char *pPackedParamUnitFromApp = new char [MAX_BYTE_PARAM_SPEECH];
    memset(pPackedParamUnitFromApp, 0, MAX_BYTE_PARAM_SPEECH);

    AUDIO_TYPE_SPEECH_LAYERINFO_STRUCT pParamLayerInfo;

    pParamLayerInfo.audioTypeName = (char *) audioTypeNameList[AUDIO_TYPE_SPEECH_GENERAL];
    pParamLayerInfo.numCategoryType = MAX_NUM_CATEGORY_TYPE_SPEECH_GENERAL;//4
    pParamLayerInfo.numParam = MAX_NUM_PARAM_SPEECH_GENERAL;//4

    pParamLayerInfo.categoryType.assign(audioType_SpeechGeneral_CategoryType, audioType_SpeechGeneral_CategoryType + pParamLayerInfo.numCategoryType);
    pParamLayerInfo.paramName.assign(audioType_SpeechGeneral_ParamName, audioType_SpeechGeneral_ParamName + pParamLayerInfo.numParam);

    ALOGV("%s(), eParamUnitHdr.SphUnitMagiNum= 0x%x, categoryType.size=%zu, paramName.size=%zu", __FUNCTION__, eParamUnitHdr.SphUnitMagiNum, pParamLayerInfo.categoryType.size(), pParamLayerInfo.paramName.size());
    for (u4Index = 0; u4Index < pParamLayerInfo.paramName.size() ; u4Index++) {
        ALOGV("%s(), paramName[%d]= %s", __FUNCTION__, u4Index, pParamLayerInfo.paramName.at(u4Index).string());
    }

    DataHeader = 0x000F;
    memcpy(pPackedParamUnit + size, &DataHeader, sizeof(DataHeader));
    size += sizeof(DataHeader);

    pParamLayerInfo.categoryName.push_back(String8(audioType_SpeechGeneral_CategoryName1[0]));

    GetSpeechParamFromAppParser(AUDIO_TYPE_SPEECH_GENERAL, &pParamLayerInfo, pPackedParamUnitFromApp, &sizeByteFromApp);

    memcpy(pPackedParamUnit + size, pPackedParamUnitFromApp, sizeByteFromApp);
    size += sizeByteFromApp;

    if (pPackedParamUnitFromApp != NULL) {
        delete[] pPackedParamUnitFromApp;
    }

    ALOGD("-%s(), eParamUnitHdr.SphUnitMagiNum= 0x%x, total size byte=%d", __FUNCTION__, eParamUnitHdr.SphUnitMagiNum, size);
    return size;
}

int SpeechParamParser::GetEchoRefParamUnit(char *pPackedParamUnit) {
    ALOGV("+%s()", __FUNCTION__);
    uint16_t size = 0, u4Index = 0, u4Index2 = 0, sizeByteFromApp = 0;
    uint16_t DataHeader;
    SPEECH_DYNAMIC_PARAM_UNIT_HDR_STRUCT eParamUnitHdr;
    memset(&eParamUnitHdr, 0, sizeof(eParamUnitHdr));

    eParamUnitHdr.SphParserVer = 1;
    eParamUnitHdr.NumLayer = 0x1;
    eParamUnitHdr.NumEachLayer = 0x1;
    eParamUnitHdr.ParamHeader[0] = 0x1;
    eParamUnitHdr.SphUnitMagiNum = 0xAA06;

    memcpy(pPackedParamUnit + size, &eParamUnitHdr, sizeof(eParamUnitHdr));
    size += sizeof(eParamUnitHdr);

    char *pPackedParamUnitFromApp = new char [MAX_BYTE_PARAM_SPEECH];
    memset(pPackedParamUnitFromApp, 0, MAX_BYTE_PARAM_SPEECH);
    AUDIO_TYPE_SPEECH_LAYERINFO_STRUCT pParamLayerInfo;

    pParamLayerInfo.audioTypeName = (char *) audioTypeNameList[AUDIO_TYPE_SPEECH_ECHOREF];
    pParamLayerInfo.numCategoryType = MAX_NUM_CATEGORY_TYPE_SPEECH_ECHOREF;//4
    pParamLayerInfo.numParam = MAX_NUM_PARAM_SPEECH_ECHOREF;//4

    pParamLayerInfo.categoryType.assign(audioType_SpeechEchoRef_CategoryType, audioType_SpeechEchoRef_CategoryType + pParamLayerInfo.numCategoryType);
    pParamLayerInfo.paramName.assign(audioType_SpeechEchoRef_ParamName, audioType_SpeechEchoRef_ParamName + pParamLayerInfo.numParam);

    ALOGV("%s(), eParamUnitHdr.SphUnitMagiNum= 0x%x, categoryType.size=%zu, paramName.size=%zu", __FUNCTION__, eParamUnitHdr.SphUnitMagiNum, pParamLayerInfo.categoryType.size(), pParamLayerInfo.paramName.size());
    for (u4Index = 0; u4Index < pParamLayerInfo.paramName.size() ; u4Index++) {
        ALOGV("%s(), paramName[%d]= %s", __FUNCTION__, u4Index, pParamLayerInfo.paramName.at(u4Index).string());
    }

    DataHeader = 0x000F;
    memcpy(pPackedParamUnit + size, &DataHeader, sizeof(DataHeader));
    size += sizeof(DataHeader);

    pParamLayerInfo.categoryName.push_back(String8(audioType_SpeechEchoRef_CategoryName1[0]));

    GetSpeechParamFromAppParser(AUDIO_TYPE_SPEECH_ECHOREF, &pParamLayerInfo, pPackedParamUnitFromApp, &sizeByteFromApp);

    memcpy(pPackedParamUnit + size, pPackedParamUnitFromApp, sizeByteFromApp);
    size += sizeByteFromApp;

    if (pPackedParamUnitFromApp != NULL) {
        delete[] pPackedParamUnitFromApp;
    }
    ALOGD("-%s(), eParamUnitHdr.SphUnitMagiNum= 0x%x, total size byte=%d", __FUNCTION__, eParamUnitHdr.SphUnitMagiNum, size);

    return size;
}
speech_profile_t SpeechParamParser::GetSpeechProfile(const speech_mode_t IdxMode, bool bBtHeadsetNrecOn) {
    speech_profile_t index_speech_profile;

    if (mSphParamInfo.bLPBK) {
        switch (IdxMode) {
        case SPEECH_MODE_NORMAL:
            index_speech_profile = SPEECH_PROFILE_LPBK_HANDSET;
            break;
        case SPEECH_MODE_EARPHONE:
            index_speech_profile = SPEECH_PROFILE_LPBK_HEADSET;
            break;
        case SPEECH_MODE_LOUD_SPEAKER:
            index_speech_profile = SPEECH_PROFILE_LPBK_HANDSFREE;
            break;
        default:
            index_speech_profile = SPEECH_PROFILE_LPBK_HANDSET;

            break;
        }
    } else {
        switch (IdxMode) {
            case SPEECH_MODE_NORMAL:
                if (mSphParamInfo.idxTTY == TTY_PARAM_OFF) {
                    if (mSphParamInfo.bSV) {
                        index_speech_profile = SPEECH_PROFILE_HANDSET_SV;
                    } else {
                        index_speech_profile = SPEECH_PROFILE_HANDSET;
                    }
                } else {
                    switch (mSphParamInfo.idxTTY) {
                        case TTY_PARAM_HCO:
                            index_speech_profile = SPEECH_PROFILE_TTY_HCO_HANDSET;
                            break;
                        case TTY_PARAM_VCO:
                            index_speech_profile = SPEECH_PROFILE_TTY_VCO_HANDSET;
                            break;
                        default:
                            index_speech_profile = SPEECH_PROFILE_TTY_HCO_HANDSET;
                            break;
                    }
                }
                break;

            case SPEECH_MODE_EARPHONE:
                switch (mSphParamInfo.uHeadsetPole) {
                    case 3:
                        index_speech_profile = SPEECH_PROFILE_3_POLE_HEADSET;
                        break;
                    case 4:
                        index_speech_profile = SPEECH_PROFILE_4_POLE_HEADSET;
                        break;
                    case 5:
                        index_speech_profile = SPEECH_PROFILE_5_POLE_HEADSET;
                        break;
                    default:
                        index_speech_profile = SPEECH_PROFILE_4_POLE_HEADSET;
                        break;
                }
                break;
            case SPEECH_MODE_LOUD_SPEAKER:
                if (mSphParamInfo.idxTTY == TTY_PARAM_OFF) {
                    if (mSphParamInfo.bSV) {
                        index_speech_profile = SPEECH_PROFILE_HANDSFREE_SV;
                    } else {
                        index_speech_profile = SPEECH_PROFILE_HANDSFREE;
                    }
                } else {
                    switch (mSphParamInfo.idxTTY) {
                        case TTY_PARAM_HCO:
                            index_speech_profile = SPEECH_PROFILE_TTY_HCO_HANDSFREE;
                            break;
                        case TTY_PARAM_VCO:
                            index_speech_profile = SPEECH_PROFILE_TTY_VCO_HANDSFREE;
                            break;
                        default:
                            index_speech_profile = SPEECH_PROFILE_TTY_HCO_HANDSFREE;
                            break;
                    }
                }
                break;
        case SPEECH_MODE_BT_EARPHONE:
        case SPEECH_MODE_BT_CORDLESS:
        case SPEECH_MODE_BT_CARKIT:
            if (bBtHeadsetNrecOn == true) {
                index_speech_profile = SPEECH_PROFILE_BT_EARPHONE;
            } else {
                index_speech_profile = SPEECH_PROFILE_BT_NREC_OFF;
            }
            break;
        case SPEECH_MODE_MAGIC_CON_CALL:
            index_speech_profile = SPEECH_PROFILE_MAGICONFERENCE;
            break;
        case SPEECH_MODE_HAC:
            index_speech_profile = SPEECH_PROFILE_HAC;
            break;
        case SPEECH_MODE_USB_AUDIO:
            index_speech_profile = SPEECH_PROFILE_USB_HEADSET;
            break;
        default:
            index_speech_profile = SPEECH_PROFILE_HANDSET;

            break;
        }
    }
    ALOGV("%s(), IdxMode = %d, IdxPfofile = %d", __FUNCTION__, IdxMode, index_speech_profile);

    return index_speech_profile;
}

status_t SpeechParamParser::SetMDParamUnitHdr(speech_type_dynamic_param_t idxAudioType, SPEECH_DYNAMIC_PARAM_UNIT_HDR_STRUCT *sParamUnitHdr, uint16_t ConfigValue) {
    switch (idxAudioType) {
    case AUDIO_TYPE_SPEECH:
        sParamUnitHdr->SphUnitMagiNum = 0xAA01;
        sParamUnitHdr->SphParserVer = 1;
        sParamUnitHdr->NumLayer = 0x2;
        sParamUnitHdr->ParamHeader[0] = 0x1F;//all network use, while modem not check it
        //Network: bit0: GSM, bit1: WCDMA,.bit2: CDMA, bit3: VoLTE, bit4:C2K
        if (mSphParamInfo.isSingleBandTransfer) {
            switch (ConfigValue) {
            case 0:
                sParamUnitHdr->ParamHeader[1] = 0x1;//voice band:NB
                break;
            case 1:
                sParamUnitHdr->ParamHeader[1] = 0x2;//voice band:WB
                break;

            default:
                sParamUnitHdr->ParamHeader[1] = 0x1;//voice band:NB
                break;
            }
        } else {
            switch (ConfigValue) {
            case 1:
                sParamUnitHdr->ParamHeader[1] = 0x1;//voice band:NB
                break;
            case 2:
                sParamUnitHdr->ParamHeader[1] = 0x3;//voice band:NB,WB
                break;
            case 3:
                sParamUnitHdr->ParamHeader[1] = 0x7;//voice band:NB,WB,SWB
                break;
            case 4:
                sParamUnitHdr->ParamHeader[1] = 0xF;//voice band:NB,WB,SWB,FB
                break;
            default:
                sParamUnitHdr->ParamHeader[1] = 0x3;//voice band:NB,WB
                break;
            }
        }
        sParamUnitHdr->ParamHeader[2] = (mSpeechParamVerFirst << 4) + mSpeechParamVerLast;
        ALOGV("%s(), SphUnitMagiNum = 0x%x, SPH_PARAM_VERSION(0x%x)", __FUNCTION__, sParamUnitHdr->SphUnitMagiNum, sParamUnitHdr->ParamHeader[2]);
        break;
    case AUDIO_TYPE_SPEECH_DMNR:
        sParamUnitHdr->SphUnitMagiNum = 0xAA03;
        sParamUnitHdr->SphParserVer = 1;
        sParamUnitHdr->NumLayer = 0x2;
        sParamUnitHdr->ParamHeader[0] = 0x3;//OutputDeviceType
        switch (ConfigValue) {
        case 1:
            sParamUnitHdr->ParamHeader[1] = 0x1;//voice band:NB
            break;
        case 2:
            sParamUnitHdr->ParamHeader[1] = 0x3;//voice band:NB,WB
            break;
        case 3:
            sParamUnitHdr->ParamHeader[1] = 0x7;//voice band:NB,WB,SWB
            break;
        case 4:
            sParamUnitHdr->ParamHeader[1] = 0xF;//voice band:NB,WB,SWB,FB
            break;
        default:
            sParamUnitHdr->ParamHeader[1] = 0x3;//voice band:NB,WB
            break;
        }
        sParamUnitHdr->ParamHeader[2] = (mSpeechParamVerFirst << 4) + mSpeechParamVerLast;
        ALOGV("%s(), SphUnitMagiNum = 0x%x, Version = 0x%x", __FUNCTION__, sParamUnitHdr->SphUnitMagiNum, sParamUnitHdr->ParamHeader[2]);
        break;

    default:
        break;
    }
    return NO_ERROR;
}

uint16_t SpeechParamParser::SetMDParamDataHdr(SPEECH_DYNAMIC_PARAM_UNIT_HDR_STRUCT sParamUnitHdr, Category *cateBand, Category *cateNetwork) {
    uint16_t u4Index = 0;
    uint16_t DataHeader = 0, MaskNetwork = 0;
    bool bNetworkMatch = false;

    if (cateBand->name != NULL) {
        if (strcmp(cateBand->name, "NB") == 0) { //All netwrok use
            DataHeader = 0x1000;
        } else if (strcmp(cateBand->name, "WB") == 0) {
            DataHeader = 0x2000;
        } else if (strcmp(cateBand->name, "SWB") == 0) {
            DataHeader = 0x3000;
        } else if (strcmp(cateBand->name, "FB") == 0) {
            DataHeader = 0x4000;
        }
    } else {
        DataHeader = 0x1000;
    }
    //search matched network
    if (cateNetwork->name != NULL) {
        for (u4Index = 0; u4Index < numSpeechNetwork ; u4Index++) {
            ALOGV("%s(), cateNetwork= %s, ListSpeechNetwork[%d]=%s", __FUNCTION__, cateNetwork->name, u4Index, ListSpeechNetwork[u4Index].name);
            if (strcmp(cateNetwork->name, ListSpeechNetwork[u4Index].name) == 0) {
                MaskNetwork = ListSpeechNetwork[u4Index].supportBit;
                ALOGV("%s(), cateNetwork= %s, ListSpeechNetwork[%d]=%s, MaskNetwork=0x%x", __FUNCTION__, cateNetwork->name, u4Index, ListSpeechNetwork[u4Index].name, MaskNetwork);
                bNetworkMatch = true;
                break;
            }
        }
        if (!bNetworkMatch) {
            ALOGE("%s(), cateNetwork= %s, ListSpeechNetwork[%d]=%s, bNetworkMatch=%d, NO match!!!", __FUNCTION__, cateNetwork->name, u4Index, ListSpeechNetwork[u4Index].name, bNetworkMatch);
        }
    }
    if (!mSphParamSupport.IsNetworkSupport) {
        DataHeader = DataHeader >> 8;
        MaskNetwork = 0xF;
    }
    DataHeader |= MaskNetwork;
    ALOGV("-%s(), SphUnitMagiNum=0x%x, DataHeader=0x%x, MaskNetwork=0x%x, cateBand=%s", __FUNCTION__, sParamUnitHdr.SphUnitMagiNum, DataHeader, MaskNetwork, cateBand->name);

    return DataHeader;
}


int SpeechParamParser::InitSpeechNetwork(void) {
    uint16_t size = 0, u4Index, sizeByteFromApp = 0;
    char *pPackedParamUnitFromApp = new char [10];
    memset(pPackedParamUnitFromApp, 0, 10);

    AUDIO_TYPE_SPEECH_LAYERINFO_STRUCT pParamLayerInfo;
    //-------------
    pParamLayerInfo.audioTypeName = (char *) audioTypeNameList[AUDIO_TYPE_SPEECH_NETWORK];

    if (mAppHandle == NULL) {
        ALOGE("%s() mAppHandle == NULL, Assert!!!", __FUNCTION__);
        ASSERT(0);
    }

    /* Query AudioType */
    AppOps *appOps = appOpsGetInstance();
    AudioType *audioType = NULL;
    if (appOps != NULL) {
        audioType = appOps->appHandleGetAudioTypeByName(mAppHandle, pParamLayerInfo.audioTypeName);
        pParamLayerInfo.numCategoryType = appOps->audioTypeGetNumOfCategoryType(audioType);//1

        pParamLayerInfo.numParam = MAX_NUM_PARAM_SPEECH_NETWORK;//4

        pParamLayerInfo.categoryType.assign(audioType_SpeechNetwork_CategoryType, audioType_SpeechNetwork_CategoryType + pParamLayerInfo.numCategoryType);
        pParamLayerInfo.paramName.assign(audioType_SpeechNetwork_ParamName, audioType_SpeechNetwork_ParamName + pParamLayerInfo.numParam);

        ALOGV("%s(), categoryType.size=%zu, paramName.size=%zu", __FUNCTION__, pParamLayerInfo.categoryType.size(), pParamLayerInfo.paramName.size());
        for (u4Index = 0; u4Index < pParamLayerInfo.categoryType.size() ; u4Index++) {
            ALOGV("%s(), categoryType[%d]= %s", __FUNCTION__, u4Index, pParamLayerInfo.categoryType.at(u4Index).string());
        }
        for (u4Index = 0; u4Index < pParamLayerInfo.paramName.size() ; u4Index++) {
            ALOGV("%s(), paramName[%d]= %s", __FUNCTION__, u4Index, pParamLayerInfo.paramName.at(u4Index).string());
        }
        //-----------
        //parse layer
        CategoryType *categoryNetwork = appOps->audioTypeGetCategoryTypeByName(audioType, audioType_SpeechNetwork_CategoryType[0].string());
        numSpeechNetwork = appOps->categoryTypeGetNumOfCategory(categoryNetwork);

        //parse network
        for (int i = 0; i < numSpeechNetwork; i++) {
            Category *CateNetwork = appOps->categoryTypeGetCategoryByIndex(categoryNetwork, i);
            sizeByteFromApp = 0;
            //clear
            while (!pParamLayerInfo.categoryName.empty()) {
                pParamLayerInfo.categoryName.pop_back();
            }
            audio_strncpy(ListSpeechNetwork[i].name, CateNetwork->name, 128);

            pParamLayerInfo.categoryName.push_back(String8(CateNetwork->name));//Network

            GetSpeechParamFromAppParser(AUDIO_TYPE_SPEECH_NETWORK, &pParamLayerInfo, pPackedParamUnitFromApp, &sizeByteFromApp);
            ListSpeechNetwork[i].supportBit = *((uint16_t *)pPackedParamUnitFromApp);
            size += sizeByteFromApp;

            ALOGD("%s(), i=%d, sizeByteFromApp=%d, supportBit=0x%x", __FUNCTION__, i, sizeByteFromApp, ListSpeechNetwork[i].supportBit);
        }
        ALOGD("-%s(), total size byte=%d", __FUNCTION__, size);
    } else {
        ALOGE("Error %s %d", __FUNCTION__, __LINE__);
        ASSERT(0);
    }
    //init the Name mapping table  for each SpeechNetwork
    bool IsNetworkFound = false;
    for (int BitIndex = 0; BitIndex < 12; BitIndex++) {
        IsNetworkFound = false;
        for (int NetworkIndex = 0; NetworkIndex < numSpeechNetwork; NetworkIndex++) {
            if (((ListSpeechNetwork[NetworkIndex].supportBit >> BitIndex) & 1) == 1) {
                audio_strncpy(NameForEachSpeechNetwork[BitIndex].name, ListSpeechNetwork[NetworkIndex].name, 128);
                IsNetworkFound = true;
                break;
            }
        }
        if (!IsNetworkFound) {
            audio_strncpy(NameForEachSpeechNetwork[BitIndex].name, ListSpeechNetwork[0].name, 128);
        }
        ALOGD("%s(), NameForEachSpeechNetwork[%d].name = %s", __FUNCTION__, BitIndex, NameForEachSpeechNetwork[BitIndex].name);
    }
    if (pPackedParamUnitFromApp != NULL) {
        delete[] pPackedParamUnitFromApp;
    }
    return size;
}

char *SpeechParamParser::GetNameForEachSpeechNetwork(unsigned char BitIndex) {
    ALOGD("%s(), NameForEachSpeechNetwork[%d].name = %s", __FUNCTION__, BitIndex, NameForEachSpeechNetwork[BitIndex].name);
    return NameForEachSpeechNetwork[BitIndex].name;
}

int SpeechParamParser::GetSpeechParamUnit(char *pPackedParamUnit, int *p4ParamArg) {
    uint16_t size = 0, u4Index, sizeByteFromApp = 0;
    uint16_t DataHeader, IdxInfo = 0, IdxTmp = 0, NumOfBand = 0, NumOfNetwork = 0, NumOfVolume = 0;
    short IdmVolumeFIR = 0;
    SPEECH_DYNAMIC_PARAM_UNIT_HDR_STRUCT eParamUnitHdr;
    int IdxProfile = 0, ConfigParamParsing = 0;

    speech_mode_t IdxMode = (speech_mode_t) * ((int *)p4ParamArg);
    int IdxVolume = *((int *)p4ParamArg + 1);
    bool bBtHeadsetNrecOn = (bool) * ((int *)p4ParamArg + 2);
    mSphParamInfo.bBtHeadsetNrecOn = bBtHeadsetNrecOn;
    mSphParamInfo.u4VolumeIndex = IdxVolume;
    mSphParamInfo.SpeechMode = IdxMode;

    ConfigParamParsing = * ((int *)p4ParamArg + 3);
    //bit 0: dv profile, bit 1: single band, bit 4~7: band number
    mSphParamInfo.isSingleBandTransfer = (bool)(ConfigParamParsing & 0x2);
    mSphParamInfo.IndexVoiceBandStart = (unsigned char)((ConfigParamParsing & 0xf0) >> 4);

    IdxProfile = GetSpeechProfile(IdxMode, bBtHeadsetNrecOn);
    ALOGD("+%s(), IdxMode=0x%x, IdxVolume=0x%x, bBtHeadsetNrecOn=0x%x, IdxProfile=%d, ConfigParamParsing=0x%x, isSingleBandTransfer=%d, IndexVoiceBandStart=%d", __FUNCTION__, IdxMode, IdxVolume, bBtHeadsetNrecOn, IdxProfile, ConfigParamParsing, mSphParamInfo.isSingleBandTransfer, mSphParamInfo.IndexVoiceBandStart);

    memset(&eParamUnitHdr, 0, sizeof(eParamUnitHdr));

    AUDIO_TYPE_SPEECH_LAYERINFO_STRUCT pParamLayerInfo;
    //-------------
    pParamLayerInfo.audioTypeName = (char *) audioTypeNameList[AUDIO_TYPE_SPEECH];

    if (mAppHandle == NULL) {
        ALOGE("%s() mAppHandle == NULL, Assert!!!", __FUNCTION__);
        ASSERT(0);
        return UNKNOWN_ERROR;
    }

    /* Query AudioType */
    AppOps *appOps = appOpsGetInstance();
    AudioType *audioType = NULL;
    if (appOps == NULL) {
        ALOGE("Error %s %d", __FUNCTION__, __LINE__);
        ASSERT(0);
        return UNKNOWN_ERROR;
    } else {
        audioType = appOps->appHandleGetAudioTypeByName(mAppHandle, pParamLayerInfo.audioTypeName);
    }

    char *pPackedParamUnitFromApp = new char [MAX_BYTE_PARAM_SPEECH];
    memset(pPackedParamUnitFromApp, 0, MAX_BYTE_PARAM_SPEECH);

    pParamLayerInfo.numCategoryType = appOps->audioTypeGetNumOfCategoryType(audioType);
    pParamLayerInfo.numParam = numSpeechParam;//4

    pParamLayerInfo.categoryType.assign(audioType_Speech_CategoryType, audioType_Speech_CategoryType + pParamLayerInfo.numCategoryType);
    pParamLayerInfo.paramName.assign(audioType_Speech_ParamName, audioType_Speech_ParamName + pParamLayerInfo.numParam);

    ALOGV("%s(), categoryType.size=%zu, paramName.size=%zu", __FUNCTION__, pParamLayerInfo.categoryType.size(), pParamLayerInfo.paramName.size());
    for (u4Index = 0; u4Index < pParamLayerInfo.categoryType.size() ; u4Index++) {
        ALOGV("%s(), categoryType[%d]= %s", __FUNCTION__, u4Index, pParamLayerInfo.categoryType.at(u4Index).string());
    }
    for (u4Index = 0; u4Index < pParamLayerInfo.paramName.size() ; u4Index++) {
        ALOGV("%s(), paramName[%d]= %s", __FUNCTION__, u4Index, pParamLayerInfo.paramName.at(u4Index).string());
    }
    //-----------

    //parse layer
    CategoryType *categoryNetwork = appOps->audioTypeGetCategoryTypeByName(audioType, audioType_Speech_CategoryType[3].string());
    CategoryType *categoryBand = appOps->audioTypeGetCategoryTypeByName(audioType, audioType_Speech_CategoryType[0].string());
    NumOfNetwork = appOps->categoryTypeGetNumOfCategory(categoryNetwork);
    NumOfBand = appOps->categoryTypeGetNumOfCategory(categoryBand);

    CategoryType *categoryVolume = appOps->audioTypeGetCategoryTypeByName(audioType, audioType_Speech_CategoryType[2].string());
    CategoryGroup *categoryGroupVolume = appOps->categoryTypeGetCategoryGroupByIndex(categoryVolume, 0);
    NumOfVolume = appOps->categoryGroupGetNumOfCategory(categoryGroupVolume);
    IdxTmp = (NumOfBand & 0xF) << 4;
    eParamUnitHdr.NumEachLayer = IdxTmp + (NumOfNetwork & 0xF);
    ALOGV("%s(), SphUnitMagiNum= 0x%x, NumEachLayer=0x%x", __FUNCTION__, eParamUnitHdr.SphUnitMagiNum, eParamUnitHdr.NumEachLayer);
    if (mSphParamInfo.isSingleBandTransfer) {
        SetMDParamUnitHdr(AUDIO_TYPE_SPEECH, &eParamUnitHdr, mSphParamInfo.IndexVoiceBandStart);
    } else {
        SetMDParamUnitHdr(AUDIO_TYPE_SPEECH, &eParamUnitHdr, NumOfBand);
    }
    ALOGV("%s(), SphUnitMagiNum= 0x%x, NumEachLayer=0x%x", __FUNCTION__, eParamUnitHdr.SphUnitMagiNum, eParamUnitHdr.NumEachLayer);
    ALOGV("%s(), categoryNetwork= %s, categoryBand = %s, categoryVolume = %s", __FUNCTION__, categoryNetwork->name, categoryBand->name, categoryVolume->name);
    ALOGV("%s(), NumOfNetwork= %d, NumOfBand = %d, NumOfVolume = %d", __FUNCTION__, NumOfNetwork, NumOfBand, NumOfVolume);

#if defined(CONFIG_MT_ENG_BUILD) // eng load
    char SphDumpStr[SPH_DUMP_STR_SIZE] = "MDParamUnitHdr ";
    int IdxDump = 0;
    for (IdxDump = 0; IdxDump < (int)(sizeof(eParamUnitHdr) >> 1); IdxDump++) { //uint16_t
        char SphDumpTemp[100];
        snprintf(SphDumpTemp, 100, "[%d]0x%x, ", IdxDump, *((uint16_t *)&eParamUnitHdr + IdxDump));
        audio_strncat(SphDumpStr, SphDumpTemp, SPH_DUMP_STR_SIZE);
    }
    if (IdxDump != 0) {
        ALOGD("%s(), %s", __FUNCTION__,  SphDumpStr);
    }
#endif
    memcpy(pPackedParamUnit + size, &eParamUnitHdr, sizeof(eParamUnitHdr));
    size += sizeof(eParamUnitHdr);

    IdxInfo = IdxMode & 0xF;
    ALOGV("%s(), add mode IdxInfo=0x%x", __FUNCTION__, IdxInfo);
    IdxTmp = IdxVolume << 4;
    IdxInfo += IdxTmp;
    ALOGV("%s(), add volume<<4 IdxInfo=0x%x, IdxTmp=0x%x", __FUNCTION__, IdxInfo, IdxTmp);

    memcpy(pPackedParamUnit + size, &IdxInfo, sizeof(IdxInfo));
    size += sizeof(IdxInfo);
    //parse network
    for (int u4INetwork = 0; u4INetwork < NumOfNetwork; u4INetwork++) {
        Category *CateNetwork =  appOps->categoryTypeGetCategoryByIndex(categoryNetwork, u4INetwork);
        //parse band
        for (int u4IBand = mSphParamInfo.IndexVoiceBandStart; u4IBand < mSphParamInfo.IndexVoiceBandStart + NumOfBand; u4IBand++) {
            sizeByteFromApp = 0;
            Category *CateBand =  appOps->categoryTypeGetCategoryByIndex(categoryBand, u4IBand);

            DataHeader = SetMDParamDataHdr(eParamUnitHdr, CateBand, CateNetwork);
            memcpy(pPackedParamUnit + size, &DataHeader, sizeof(DataHeader));
            size += sizeof(DataHeader);
            while (!pParamLayerInfo.categoryName.empty()) {
                pParamLayerInfo.categoryName.pop_back();
            }
            //Band
            pParamLayerInfo.categoryName.push_back(String8(CateBand->name));//Band
            //Profile
            pParamLayerInfo.categoryName.push_back(String8(audioType_Speech_CategoryName2[IdxProfile]));
            ALOGV("%s(), IdxProfile=%d, Profile Name=%s", __FUNCTION__, IdxProfile, audioType_Speech_CategoryName2[IdxProfile]);
            //Volume
            if (IdxVolume > 6 || IdxVolume < 0) {
                pParamLayerInfo.categoryName.push_back(String8(audioType_Speech_CategoryName3[3]));//volume
                ALOGE("%s(), Invalid IdxVolume=0x%x, use 3 !!!", __FUNCTION__, IdxVolume);
            } else {
                pParamLayerInfo.categoryName.push_back(String8(audioType_Speech_CategoryName3[IdxVolume]));
            }
            pParamLayerInfo.categoryName.push_back(String8(CateNetwork->name));//Network

            for (u4Index = 0; u4Index < pParamLayerInfo.categoryName.size() ; u4Index++) {
                ALOGV("%s(), categoryName[%d]= %s", __FUNCTION__, u4Index, pParamLayerInfo.categoryName.at(u4Index).string());
            }

            GetSpeechParamFromAppParser(AUDIO_TYPE_SPEECH, &pParamLayerInfo, pPackedParamUnitFromApp, &sizeByteFromApp);

            memcpy(pPackedParamUnit + size, pPackedParamUnitFromApp, sizeByteFromApp);
            size += sizeByteFromApp;

            ALOGV("%s(), u4INetwork=%d, u4IBand=%d, total size byte=%d", __FUNCTION__, u4INetwork, u4IBand, size);

        }
        ALOGD("-%s(), SphUnitMagiNum = 0x%x, SPH_PARAM_VERSION(0x%x), total size byte=%d", __FUNCTION__, eParamUnitHdr.SphUnitMagiNum, eParamUnitHdr.ParamHeader[2], size);

    }

    if (pPackedParamUnitFromApp != NULL) {
        delete[] pPackedParamUnitFromApp;
    }
    return size;
}


status_t SpeechParamParser::SetParamInfo(const String8 &keyParamPairs) {
    ALOGV("+%s(): %s", __FUNCTION__, keyParamPairs.string());
    AudioParameter param = AudioParameter(keyParamPairs);
    int value;
    if (param.getInt(String8("ParamSphLpbk"), value) == NO_ERROR) {
        param.remove(String8("ParamSphLpbk"));
#if defined(MTK_AUDIO_SPH_LPBK_PARAM)
        mSphParamInfo.bLPBK = (value == 1) ? true : false;
#else
        mSphParamInfo.bLPBK = false;

#endif
        ALOGD("%s(): mSphParamInfo.bLPBK = %d", __FUNCTION__, mSphParamInfo.bLPBK);
    }
    if (param.getInt(String8("ParamHeadsetPole"), value) == NO_ERROR) {
        param.remove(String8("ParamHeadsetPole"));
        if (value == 3 || value == 4 || value == 5) {
            mSphParamInfo.uHeadsetPole = value;
        } else {
            mSphParamInfo.uHeadsetPole = 4;
            ALOGE("%s(): Invalid HeadsetPole(%d), set default 4_pole!!!", __FUNCTION__, value);
        }
        ALOGD("%s(): mSphParamInfo.uHeadsetPole = %d", __FUNCTION__, mSphParamInfo.uHeadsetPole);
    }
    if (param.getInt(String8("ParamSphTty"), value) == NO_ERROR) {
        param.remove(String8("ParamSphTty"));
        if (mSphParamSupport.IsTTYSupport) {
            mSphParamInfo.idxTTY = value;
        }
        ALOGD("%s(): mSphParamInfo.idxTTY = %d", __FUNCTION__, mSphParamInfo.idxTTY);
    }
    if (param.getInt(String8("ParamSphSV"), value) == NO_ERROR) {
        param.remove(String8("ParamSphSV"));
        if (mSphParamSupport.IsSuperVolumeSupport) {
            mSphParamInfo.bSV = (value == 1) ? true : false;
        } else {
            mSphParamInfo.bSV = false;
        }
        ALOGD("%s(): mSphParamInfo.bSV = %d", __FUNCTION__, mSphParamInfo.bSV);
    }

    ALOGD("-%s(): %s", __FUNCTION__, keyParamPairs.string());
    return NO_ERROR;
}

bool SpeechParamParser::GetParamStatus(const char *ParamName) {
    bool status = false;
    if (ParamName != NULL) {
        if (strcmp(ParamName, "ParamSphTty") == 0) {
            status = (mSphParamInfo.idxTTY == 0) ? false : true;
        } else if (strcmp(ParamName, "ParamSphSV") == 0) {
            status = mSphParamInfo.bSV;
        } else {
            status = false;
        }
        ALOGD("%s(), %s = %d", __FUNCTION__, ParamName, status);
    } else {
        status = false;
    }
    return status;
}

int SpeechParamParser::GetMagiClarityParamUnit(char *pPackedParamUnit) {
    ALOGV("+%s()", __FUNCTION__);
    uint16_t size = 0, u4Index = 0, u4Index2 = 0, sizeByteFromApp = 0;
    uint16_t DataHeader;
    SPEECH_DYNAMIC_PARAM_UNIT_HDR_STRUCT eParamUnitHdr;
    memset(&eParamUnitHdr, 0, sizeof(eParamUnitHdr));

    eParamUnitHdr.SphParserVer = 1;
    eParamUnitHdr.NumLayer = 0x1;
    eParamUnitHdr.NumEachLayer = 0x1;
    eParamUnitHdr.ParamHeader[0] = 0x1;//Common
    eParamUnitHdr.SphUnitMagiNum = 0xAA04;

    memcpy(pPackedParamUnit + size, &eParamUnitHdr, sizeof(eParamUnitHdr));
    size += sizeof(eParamUnitHdr);

    char *pPackedParamUnitFromApp = new char [MAX_BYTE_PARAM_SPEECH];
    memset(pPackedParamUnitFromApp, 0, MAX_BYTE_PARAM_SPEECH);
    AUDIO_TYPE_SPEECH_LAYERINFO_STRUCT pParamLayerInfo;

    pParamLayerInfo.audioTypeName = (char *) audioTypeNameList[AUDIO_TYPE_SPEECH_MAGICLARITY];
    pParamLayerInfo.numCategoryType = MAX_NUM_CATEGORY_TYPE_SPEECH_MAGICLARITY;//4
    pParamLayerInfo.numParam = MAX_NUM_PARAM_SPEECH_MAGICLARITY;//4

    pParamLayerInfo.categoryType.assign(audioType_SpeechMagiClarity_CategoryType, audioType_SpeechMagiClarity_CategoryType + pParamLayerInfo.numCategoryType);
    pParamLayerInfo.paramName.assign(audioType_SpeechMagiClarity_ParamName, audioType_SpeechMagiClarity_ParamName + pParamLayerInfo.numParam);

    ALOGV("%s(), eParamUnitHdr.SphUnitMagiNum= 0x%x, categoryType.size=%zu, paramName.size=%zu", __FUNCTION__, eParamUnitHdr.SphUnitMagiNum, pParamLayerInfo.categoryType.size(), pParamLayerInfo.paramName.size());
    for (u4Index = 0; u4Index < pParamLayerInfo.paramName.size() ; u4Index++) {
        ALOGV("%s(), paramName[%d]= %s", __FUNCTION__, u4Index, pParamLayerInfo.paramName.at(u4Index).string());
    }
    DataHeader = 0x000F;
    memcpy(pPackedParamUnit + size, &DataHeader, sizeof(DataHeader));
    size += sizeof(DataHeader);

    pParamLayerInfo.categoryName.push_back(String8(audioType_SpeechMagiClarity_CategoryName1[0]));

    GetSpeechParamFromAppParser(AUDIO_TYPE_SPEECH_MAGICLARITY, &pParamLayerInfo, pPackedParamUnitFromApp, &sizeByteFromApp);

    memcpy(pPackedParamUnit + size, pPackedParamUnitFromApp, sizeByteFromApp);
    size += sizeByteFromApp;

    if (pPackedParamUnitFromApp != NULL) {
        delete[] pPackedParamUnitFromApp;
    }
    ALOGD("-%s(), eParamUnitHdr.SphUnitMagiNum= 0x%x, total size byte=%d", __FUNCTION__, eParamUnitHdr.SphUnitMagiNum, size);
    return size;
}

int SpeechParamParser::GetBtDelayTime(const char *btDeviceName) {
    int btDelayMs = 0;
    ALOGD("+%s(), btDeviceName=%s", __FUNCTION__, btDeviceName);
    AppOps *appOps = appOpsGetInstance();
    if (appOps == NULL) {
        ALOGE("Error %s %d", __FUNCTION__, __LINE__);
        ASSERT(0);
        return 0;
    } else {
        /* Get the BT device delay parameter */
        AudioType *audioType = appOps->appHandleGetAudioTypeByName(mAppHandle, "BtInfo");
        if (audioType) {
            String8 categoryPath("BT headset,");
            categoryPath += btDeviceName;

            ParamUnit *paramUnit = appOps->audioTypeGetParamUnit(audioType, categoryPath.string());
            ASSERT(paramUnit);

            Param *param = appOps->paramUnitGetParamByName(paramUnit, "voice_cp_delay_ms");
            ASSERT(param);

            btDelayMs = *(int *)param->data;
        }
        ALOGD("-%s(), btDeviceName=%s, btDelayMs=%d", __FUNCTION__, btDeviceName, btDelayMs);
        return btDelayMs;
    }
}
}

//namespace android
