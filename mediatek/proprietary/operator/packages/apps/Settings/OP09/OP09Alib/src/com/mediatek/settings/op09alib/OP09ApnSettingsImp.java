/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceClickListener;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceScreen;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.ims.ImsManager;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.TelephonyProperties;
import com.android.settings.ApnPreference;

//import com.mediatek.internal.telephony.ITelephonyEx;
import com.mediatek.settings.ext.DefaultApnSettingsExt;
import com.mediatek.telephony.MtkTelephonyManagerEx;

import mediatek.telephony.MtkTelephony;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * APN CT feature.
 */
public class OP09ApnSettingsImp extends DefaultApnSettingsExt {

    private static final String TAG = "OP09ApnSettingsImp";
    private EditTextPreference mPppEditTextPreference;
    private Context mContext;

    private static final String CT_NUMERIC_1 = "46011";
    private static final String CT_NUMERIC_2 = "46003";
    private static final String CT_NUMERIC_3 = "45400";
    private static final String CT_NUMERIC_4 = "45431";
    private static final String CT_NUMERIC_5 = "46001";
    private static final String CT_NUMERIC_6 = "45403";
    private static final String CT_NUMERIC_7 = "45404";
    private static final String CT_NUMERIC_MVNO_3 = "CTExcel";
    private static final String CT_NUMERIC_MVNO_4 = "中国电信";
    private static final String CT_NUMERIC_MVNO_5 = "China Telecom";

    private static final String CT_CTWAP = "CTWAP";
    private static final String CT_CTNET = "CTNET";
    private static final int SOURCE_TYPE_DEFAULT = 0;
    private SwitchPreference mSwitchPreference;
    private static final String IMS_TYPE = "ims";

    /**
     * Constructor method.
     * @param context is Settings's context.
     */
    public OP09ApnSettingsImp(Context context) {
        mContext = context;
    }

    /**
     * Add a preference in the prefernce screen according to the slotId.
     * CT need PPP dialog for connect internet, so add PPP,
     * Customize apn titles
     * @param subId sub id
     * @param root PPP's parent
     */
    @Override
    public void customizePreference(int subId, PreferenceScreen root) {
        Log.d(TAG, "customizePreference subId = " + subId + "  slotId = "
              + SubscriptionManager.getSlotIndex(subId));
        if (PhoneConstants.SIM_ID_1 == SubscriptionManager.getSlotIndex(subId)) {
            mPppEditTextPreference = new EditTextPreference(root.getContext(),
                    null);
            mPppEditTextPreference.setKey(mContext.getResources().getString(
                    R.string.ppp_dialing));
            mPppEditTextPreference.setTitle(mContext.getResources().getString(
                    R.string.ppp_dialing));
            mPppEditTextPreference.setDialogTitle(mContext.getResources()
                    .getString(R.string.ppp_dialing));
            root.addPreference(mPppEditTextPreference);

            // PPP Phone number already added.
            root.findPreference("apn_name").setTitle(
                    mContext.getString(R.string.apn_name));
            root.findPreference("apn_apn").setTitle(
                    mContext.getString(R.string.apn_apn));
            root.findPreference("auth_type").setTitle(
                    mContext.getString(R.string.apn_auth_type));
            root.findPreference("apn_user").setTitle(
                    mContext.getString(R.string.apn_user));
            root.findPreference("apn_password").setTitle(
                    mContext.getString(R.string.apn_password));
        }
    }

    /**
     * Customize apn projection, such as add MtkTelephony.Carriers.PPP Called at
     * onCreate in ApnEditor.
     * @param projection old String[]
     * @return new String[] add PPP
     */
    @Override
    public String[] customizeApnProjection(String[] projection) {
        Log.d(TAG, "customizeApnProjection");

        if (MtkTelephony.Carriers.PPP.equals(projection[projection.length - 1])) {
            return projection;
        }
        Log.d(TAG, "customizeApnProjection, Carriers");
        String[] newStrs = new String[projection.length + 1];
        for (int i = 0; i < projection.length; i++) {
            newStrs[i] = projection[i];
        }
        newStrs[newStrs.length - 1] = MtkTelephony.Carriers.PPP;
        return newStrs;
    }

    /**
     * Save the added apn values called when save the added apn vaule in apnEditor.
     * @param contentValues contentValues
     */
    @Override
    public void saveApnValues(ContentValues contentValues) {
        contentValues.put(MtkTelephony.Carriers.PPP, getPppDialingText());
    }

    /**
     * Set the preference text and summary according to the slotId.
     * called at update UI, in ApnEditor.
     * @param subId sub id
     * @param text text and summary
     */
    @Override
    public void setPreferenceTextAndSummary(int subId, String text) {
        if (PhoneConstants.SIM_ID_1 == SubscriptionManager.getSlotIndex(subId)) {
            mPppEditTextPreference.setText(text);
            mPppEditTextPreference.setSummary(text);
        }
    }

    /**
     * Update the customized status(enable , disable).
     * Called at update screen status
     * @param subId sub id
     * @param sourceType 0 means google default apn
     * @param root PPP's parent
     * @param apnType apn type
     */
    @Override
    public void updateFieldsStatus(int subId, int sourceType, PreferenceScreen root, String apnType) {
        Log.d(TAG, "updateFieldsStatus subId=" + subId + " sourceType= " + sourceType);

        if (sourceType != SOURCE_TYPE_DEFAULT) {
            return;
        }
        if (PhoneConstants.SIM_ID_1 == SubscriptionManager.getSlotIndex(subId)) {
            root.findPreference("apn_mms_proxy").setEnabled(false);
            root.findPreference("apn_mms_port").setEnabled(false);
            root.findPreference("apn_mmsc").setEnabled(false);
            root.findPreference("apn_mcc").setEnabled(false);
            root.findPreference("apn_mnc").setEnabled(false);
        }
        TelephonyManager tm =
            (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String simMccMnc = tm.getSimOperator(subId);
        boolean isCtCard = false;
        if (simMccMnc != null) {
            Log.d(TAG, "updateFieldsStatus simMccMnc =" + simMccMnc);
        }
        if (simMccMnc != null
                && (simMccMnc.equals(CT_NUMERIC_1) || simMccMnc.equals(CT_NUMERIC_2))) {
            isCtCard = true;
        }

        boolean isMvnoCtCard = false;
        if (null != root.findPreference("mvno_match_data")) {
            EditTextPreference mvno_match_data
                = ((EditTextPreference)root.findPreference("mvno_match_data"));
            String mvno_text = mvno_match_data.getText();
            String mvno_summary = mvno_match_data.getSummary().toString();
            Log.d(TAG, "mvno_match_data false != null"
                    + " mvno_text = " + mvno_text
                    + " mvno_summary = " + mvno_summary
                    + " simMccMnc = " + simMccMnc);
            if (CT_NUMERIC_3.equals(simMccMnc)
                    && (CT_NUMERIC_MVNO_3.equals(mvno_text)
                            || CT_NUMERIC_MVNO_3.equals(mvno_summary))) {
                isMvnoCtCard = true;
            }
            if (CT_NUMERIC_4.equals(simMccMnc)
                    && (CT_NUMERIC_MVNO_3.equals(mvno_text)
                            || CT_NUMERIC_MVNO_3.equals(mvno_summary))) {
                isMvnoCtCard = true;
            }
            if (CT_NUMERIC_4.equals(simMccMnc)
                    && (CT_NUMERIC_MVNO_4.equals(mvno_text)
                            || CT_NUMERIC_MVNO_4.equals(mvno_summary))) {
                isMvnoCtCard = true;
            }
            if (CT_NUMERIC_5.equals(simMccMnc)
                    && (CT_NUMERIC_MVNO_5.equals(mvno_text)
                            || CT_NUMERIC_MVNO_5.equals(mvno_summary))) {
                isMvnoCtCard = true;
            }
            if (CT_NUMERIC_6.equals(simMccMnc)
                    && (CT_NUMERIC_MVNO_4.equals(mvno_text)
                            || CT_NUMERIC_MVNO_4.equals(mvno_summary))) {
                isMvnoCtCard = true;
            }
            if (CT_NUMERIC_7.equals(simMccMnc)
                    && (CT_NUMERIC_MVNO_4.equals(mvno_text)
                            || CT_NUMERIC_MVNO_4.equals(mvno_summary))) {
                isMvnoCtCard = true;
            }
        }

        Log.d(TAG, "updateFieldsStatus isCtCard=" + isCtCard + " isMvnoCtCard= " + isMvnoCtCard);

        if (isMvnoCtCard || isCtCard) {
            int count = root.getPreferenceCount();
            for (int i = 0; i < count; i++) {
                root.getPreference(i).setEnabled(false);
            }
        }
    }

    /**
     * M: For CT fature :add ppp_dialing use class.
     * ApnEditor return the ppp_dialing's text
     */
    private String getPppDialingText() {
        if (mPppEditTextPreference != null) {
            return mPppEditTextPreference.getText();
        } else {
            return mContext.getResources().getString(R.string.apn_not_set);
        }
    }

    /**
     * Customize the unselected APN.
     * @param type mms, or (mms,supl) or (default,mms,supl),etc
     * @param mnoApnList the unselectable mno apn list.
     * @param mvnoApnList the unselectable mvno apn list.
     * @param subId the subscription ID.
     */
    @Override
    public void customizeUnselectableApn(String type,
            ArrayList<Preference> mnoApnList, ArrayList<Preference> mvnoApnList, int subId) {
        MtkTelephonyManagerEx telephonyManagerEx = MtkTelephonyManagerEx.getDefault();
        String values[] = telephonyManagerEx.getSupportCardType(
                SubscriptionManager.getSlotIndex(subId));
        Log.d(TAG, "customizeUnselectableApn cardType = "
                + Arrays.toString(values));

        TelephonyManager tm =
            (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        /// isCtCard will be true.
        boolean isCtCard = false;
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                if ("RUIM".equals(values[i])) {
                    isCtCard = true;
                    break;
                } else if ("CSIM".equals(values[i])) {
                    isCtCard = true;
                    break;
                }
            }
        }
        ///simMccMnc means is CT card, 46011 means is CT4G card,46003 means is CT3G card.
        ///networkType means ps data, when is NETWORK_TYPE_LTE or NETWORK_TYPE_EHRPD,
        ///dataMccMnc will be true.
        String simMccMnc = tm.getSimOperator(subId);
        int networkType = tm.getNetworkType(subId);
        boolean dataMccMnc = false;
        if (simMccMnc != null
                && (simMccMnc.equals(CT_NUMERIC_1) || simMccMnc.equals(CT_NUMERIC_2))) {
            if (TelephonyManager.NETWORK_TYPE_LTE == networkType
                    || TelephonyManager.NETWORK_TYPE_EHRPD == networkType ) {
                dataMccMnc = true;
            }
        }

        /// type is mms,supl ,this where will be changed by apns-conf.xml
        boolean isMmsSupltype = false;
        if (type != null) {
            if (type.equals("mms,supl")) {
                isMmsSupltype = true;
            }
        }

        /// for CT volte feature, when volte button off, remove ims apn.
        boolean isIMsType = false;
        int isChecked = Settings.Global.getInt(mContext.getContentResolver(),
                Settings.Global.ENHANCED_4G_MODE_ENABLED, 0);
        int slotId = SubscriptionManager.getSlotIndex(subId);
        int mainPhoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        String curr3GSim = SystemProperties.get("persist.radio.simswitch", "");
        Log.d(TAG, "current 3G Sim = " + curr3GSim);
        if (!TextUtils.isEmpty(curr3GSim)) {
            int curr3GPhoneId = Integer.parseInt(curr3GSim);
            mainPhoneId = curr3GPhoneId - 1;
        }
        boolean isVolteSupport
                = ImsManager.isVolteEnabledByPlatform(mContext, slotId);
        if (simMccMnc != null
                && (simMccMnc.equals(CT_NUMERIC_1) || simMccMnc.equals(CT_NUMERIC_2))
                && IMS_TYPE.equals(type)) {
            if (isChecked != 1 || 0 != slotId || !isVolteSupport) {
                isIMsType = true;
            }
        }

        if ((isCtCard && dataMccMnc && isMmsSupltype)
                || (isCtCard && isIMsType)) {
            if (mnoApnList != null && mnoApnList.size() > 0) {
                mnoApnList.remove(mnoApnList.size() - 1);
            }
        }

        Log.d(TAG, "simMccMnc = " + simMccMnc
                + " dataMccMnc = " + dataMccMnc
                + " networkType = " + networkType
                + " isMmsSupltype = " + isMmsSupltype
                + " isIMsType = " + isIMsType
                + " type = " + type
                + "isChecked = " + isChecked
                + " isVolteSupport = " + isVolteSupport
                + " subId = " + subId
                + " slotId = " + slotId
                + " mainPhoneId = " + mainPhoneId
                + " curr3GSim = " + curr3GSim);
    }

    /**
     * Update the APN name.
     * @param name the APN name to be updated.
     * @param sourcetype the APN source type.
     * @return the updated name.
     */
    @Override
    public String updateApnName(String name, int sourcetype) {
        String name_t = name;

        if (sourcetype == 1) {
            return name_t;
        }
        if (CT_CTWAP.equals(name)) {
            name_t = mContext.getString(R.string.ctAPN_name_ctwap);
        } else if (CT_CTNET.equals(name)) {
            name_t = mContext.getString(R.string.ctAPN_name_ctnet);
        }

        return name_t;
    }

    /**
     * set MVNO preference state, called in ApnEditor.
     * @param mvnoType The mvno preference to set state
     * @param mvnoMatchData The mvnoMatch preference to set state
     */
    @Override
    public void setMvnoPreferenceState(Preference mvnoType, Preference mvnoMatchData) {
        Log.d(TAG, "setMvnoPreferenceState()");
        if ("mvno_type".equals(mvnoType.getKey())) {
            mvnoType.setEnabled(false);
            Log.d(TAG, "Disable MVNO type preference");
        }
        if ("mvno_match_data".equals(mvnoMatchData.getKey())) {
            mvnoMatchData.setEnabled(false);
            Log.d(TAG, "Disable MVNO match data preference");
        }
    }

}
