package com.mediatek.op11.phone;

import android.content.Context;
import android.util.Log;

import com.mediatek.phone.ext.DefaultCallFeaturesSettingExt;


public class Op11CallFeaturesSettingExt extends DefaultCallFeaturesSettingExt {
    private static final String TAG = "Op11CallFeaturesSettingExt";
    private Context mContext;

    public Op11CallFeaturesSettingExt(Context context) {
        mContext = context;
    }

    @Override
    public boolean needShowOpenMobileDataDialog(Context context, int subId) {
        Log.d(TAG, "needShowOpenMobileDataDialog false");
        return false;
    }
}
