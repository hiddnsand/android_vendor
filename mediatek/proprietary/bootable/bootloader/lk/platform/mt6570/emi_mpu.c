/* Copyright Statement:
*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein
* is confidential and proprietary to MediaTek Inc. and/or its licensors.
* Without the prior written permission of MediaTek inc. and/or its licensors,
* any reproduction, modification, use or disclosure of MediaTek Software,
* and information contained herein, in whole or in part, shall be strictly prohibited.
*/
/* MediaTek Inc. (C) 2015. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
* AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
* NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
* SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
* SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
* THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
* THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
* CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
* SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
* CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
* AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
* OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
* MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
* The following software/firmware and/or related documentation ("MediaTek Software")
* have been modified by MediaTek Inc. All revisions are subject to any receiver\'s
* applicable license agreements with MediaTek Inc.
*/

#include <debug.h>
#include <platform/mt_reg_base.h>
#include <platform/mt_typedefs.h>
#include <platform/sync_write.h>
#include <platform/emi_mpu.h>
#include <platform/emi_hw.h>
#include <kernel/thread.h>

#define DBG_EMI(x...) dprintf(CRITICAL, x)

/*============================================================================*/
/* CONSTAND DEFINITIONS                                                       */
/*============================================================================*/
#define MOD "[EMI_DRV]"

#define readl(addr) (*(volatile kal_uint32 *)(addr))
#define writel(b,addr) mt_reg_sync_writel(b,addr)
#define IOMEM(reg) (reg)

/*
 * emi_mpu_set_region_protection: protect a region.
 * @start: start address of the region
 * @end: end address of the region
 * @region: EMI MPU region id
 * @access_permission: EMI MPU access permission
 * Return 0 for success, otherwise negative status code.
 */
int emi_mpu_set_region_protection(unsigned int start, unsigned int end, int region, unsigned int access_permission)
{
	int ret = 0;
	unsigned int tmp;
	unsigned int ax_pm;
	DBG_EMI("%smpu protect region %d 0x%x to 0x %x",
	        MOD, region, start, end);

	if ((end != 0) || (start !=0)) {
		/*Address 64KB alignment*/
		start -= EMI_PHY_OFFSET;
		end -= EMI_PHY_OFFSET;
		start = start >> 16;
		end = end >> 16;

		if (end <= start) {
			ret = -1;
			goto out;
		}
	}

	ax_pm  = (access_permission << 16) >> 16;
	//ax_pm2 = (access_permission >> 16);

	switch (region) {
		case 0:
			tmp = readl(IOMEM(EMI_MPUI)) & 0xFFFF0000;
			writel(0, EMI_MPUI);
			writel((start << 16) | end, EMI_MPUA);
			writel(tmp | ax_pm, EMI_MPUI);

			//print("EMI_MPUA(0x%x)=0x%x\n",EMI_MPUA, readl(IOMEM(EMI_MPUA)));
			//print("EMI_MPUI(0x%x)=0x%x\n",EMI_MPUI, readl(IOMEM(EMI_MPUI)));
			break;

		case 1:
			tmp = readl(IOMEM(EMI_MPUI)) & 0x0000FFFF;
			writel(0, EMI_MPUI);
			writel((start << 16) | end, EMI_MPUB);
			writel(tmp | (ax_pm << 16), EMI_MPUI);
			break;

		case 2:
			tmp = readl(IOMEM(EMI_MPUJ)) & 0xFFFF0000;
			writel(0, EMI_MPUJ);
			writel((start << 16) | end, EMI_MPUC);
			writel(tmp | ax_pm, EMI_MPUJ);
			break;

		case 3:
			tmp = readl(IOMEM(EMI_MPUJ)) & 0x0000FFFF;
			writel(0, EMI_MPUJ);
			writel((start << 16) | end, EMI_MPUD);
			writel(tmp | (ax_pm << 16), EMI_MPUJ);
			break;

		case 4:
			tmp = readl(IOMEM(EMI_MPUK)) & 0xFFFF0000;
			writel(0, EMI_MPUK);
			writel((start << 16) | end, EMI_MPUE);
			writel(tmp | ax_pm, EMI_MPUK);
			break;

		case 5:
			tmp = readl(IOMEM(EMI_MPUK)) & 0x0000FFFF;
			writel(0, EMI_MPUK);
			writel((start << 16) | end, EMI_MPUF);
			writel(tmp | (ax_pm << 16), EMI_MPUK);
			break;

		case 6:
			tmp = readl(IOMEM(EMI_MPUL)) & 0xFFFF0000;
			writel(0, EMI_MPUL);
			writel((start << 16) | end, EMI_MPUG);
			writel(tmp | ax_pm, EMI_MPUL);
			break;

		case 7:
			tmp = readl(IOMEM(EMI_MPUL)) & 0x0000FFFF;
			writel(0, EMI_MPUL);
			writel((start << 16) | end, EMI_MPUH);
			writel(tmp | (ax_pm << 16), EMI_MPUL);
			break;
		default:
			ret = -1;
			break;
	}

out:


	DBG_EMI("[%s]\n", ret ? "FAIL" : "OK");

	return ret;
}

