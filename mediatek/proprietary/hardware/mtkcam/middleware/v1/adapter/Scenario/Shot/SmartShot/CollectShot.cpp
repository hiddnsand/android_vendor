/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/CollectShot"
//
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/def/common.h>
#include <mtkcam/utils/std/Trace.h>
//
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/aaa/IIspMgr.h>
//
#include <mtkcam/middleware/v1/IShot.h>
//
#include "ImpShot.h"
#include "CollectShot.h"
//
#include <mtkcam/utils/hw/CamManager.h>
using namespace NSCam::Utils;
//
#include <mtkcam/middleware/v1/LegacyPipeline/StreamId.h>
#include <mtkcam/middleware/v1/camshot/_params.h>
//
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
//
#include <mtkcam/middleware/v1/LegacyPipeline/ILegacyPipeline.h>
#include <mtkcam/utils/hw/HwInfoHelper.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <utils/Vector.h>
//
#include <mtkcam/aaa/IDngInfo.h>
//
#include <mtkcam/middleware/v1/LegacyPipeline/LegacyPipelineUtils.h>
#include <mtkcam/middleware/v1/LegacyPipeline/LegacyPipelineBuilder.h>
#include <mtkcam/middleware/v1/LegacyPipeline/buffer/StreamBufferProviderFactory.h>

#include <mtkcam/aaa/IHal3A.h>

// IVendor
#include <mtkcam/pipeline/extension/Collect.h>

using namespace android;
using namespace NSShot::NSSmartShot;
using namespace NSCam;
using namespace NSCam::v1;
using namespace NSCam::v1::NSLegacyPipeline;
using namespace NSCamHW;
using namespace NS3Av3;
using namespace NSCam::plugin;

#define CHECK_OBJECT(x)  do{                                        \
    if (x == nullptr) { MY_LOGE("Null %s Object", #x); return MFALSE;} \
} while(0)

#include <cutils/properties.h>
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)
#define FUNC_START                  MY_LOGD("+")
#define FUNC_END                    MY_LOGD("-")

#define FUNCTION_SCOPE          auto __scope_logger__ = create_scope_logger(__FUNCTION__)
#include <memory>
static std::shared_ptr<char> create_scope_logger(const char* functionName)
{
    char* pText = const_cast<char*>(functionName);
    CAM_LOGD("[%s] + ", pText);
    return std::shared_ptr<char>(pText, [](char* p){ CAM_LOGD("[%s] -", p); });
}


#define DEFAULT_MAX_FRAME_NUM       3

/******************************************************************************
 *
 ******************************************************************************/
extern
sp<IShot>
createInstance_CollectShot(
    char const*const    pszShotName,
    uint32_t const      u4ShotMode,
    int32_t const       i4OpenId
)
{
    sp<IShot>      pShot        = NULL;
    sp<SmartShot>  pImpShot     = NULL;
    //
    //  (1.1) new Implementator.
    pImpShot = new CollectShot(pszShotName, u4ShotMode, i4OpenId, false);
    if  ( pImpShot == 0 ) {
        CAM_LOGE("[%s] new CollectShot", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (1.2) initialize Implementator if needed.
    if  ( ! pImpShot->onCreate() ) {
        CAM_LOGE("[%s] onCreate()", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (2)   new Interface.
    pShot = new IShot(pImpShot);
    if  ( pShot == 0 ) {
        CAM_LOGE("[%s] new IShot", __FUNCTION__);
        goto lbExit;
    }
    //
lbExit:
    //
    //  Free all resources if this function fails.
    if  ( pShot == 0 && pImpShot != 0 ) {
        pImpShot->onDestroy();
        pImpShot = NULL;
    }
    //
    return  pShot;
}

/******************************************************************************
 *
 ******************************************************************************/
extern
sp<IShot>
createInstance_ZsdCollectShot(
    char const*const    pszShotName,
    uint32_t const      u4ShotMode,
    int32_t const       i4OpenId
)
{
    sp<IShot>       pShot       = NULL;
    sp<CollectShot>    pImpShot    = NULL;
    //
    //  (1.1) new Implementator.
    pImpShot = new CollectShot(pszShotName, u4ShotMode, i4OpenId, true);
    if  ( pImpShot == 0 ) {
        CAM_LOGE("[%s] new CollectShot", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (1.2) initialize Implementator if needed.
    if  ( ! pImpShot->onCreate() ) {
        CAM_LOGE("[%s] onCreate()", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (2)   new Interface.
    pShot = new IShot(pImpShot);
    if  ( pShot == 0 ) {
        CAM_LOGE("[%s] new IShot", __FUNCTION__);
        goto lbExit;
    }
    //
lbExit:
    //
    //  Free all resources if this function fails.
    if  ( pShot == 0 && pImpShot != 0 ) {
        pImpShot->onDestroy();
        pImpShot = NULL;
    }
    //
    return  pShot;
}


/******************************************************************************
 *
 ******************************************************************************/
CollectShot::
CollectShot(
    char const*const pszShotName,
    uint32_t const   u4ShotMode,
    int32_t const    i4OpenId,
    bool const       isZsd
)
    : SmartShot(pszShotName, u4ShotMode, i4OpenId, isZsd)
{
    m_frameNum = property_get_int32("debug.collect.num", 3);
    MY_LOGD("Data collect frame number(%d)", m_frameNum);
}


/******************************************************************************
 *
 ******************************************************************************/
CollectShot::
~CollectShot()
{
}

/******************************************************************************
 *
 ******************************************************************************/

bool
CollectShot::
onCmd_capture()
{
    CAM_TRACE_NAME("SmartShot onCmd_capture");

    mu4Scenario = mShotParam.muSensorMode;

    if (mu4Scenario == SENSOR_SCENARIO_ID_UNNAMED_START) {
        mu4Scenario = SENSOR_SCENARIO_ID_NORMAL_CAPTURE;
    }
    MY_LOGD("scenario(%d), sensorMode(%d)", mu4Scenario, mShotParam.muSensorMode);

    if ( createNewPipeline() ) {
        MINT32 runtimeAllocateCount = 0;
        beginCapture( runtimeAllocateCount );
        //
        mpManager = NSVendorManager::get(getOpenId());
        CHECK_OBJECT(mpManager);
        //
        constructCapturePipeline();
    }

    //
    plugin::InputInfo inputInfo;
    MINT64 mode = [ this ](MINT32 const type) -> MINT64 {
        switch(type)
        {
            case eShotMode_ZsdHdrShot:
            case eShotMode_HdrShot: {
                return MTK_PLUGIN_MODE_COLLECT;
            }
        }
        MY_LOGW("no mapped vendor for type %d", type);
        return MTK_PLUGIN_MODE_COMBINATION;
    }(getShotMode());

    // set Input
    inputInfo.combination.push_back(mode);
    inputInfo.appCtrl      = mShotParam.mAppSetting;
    inputInfo.halCtrl      = mShotParam.mHalSetting;
    //
    inputInfo.fullRaw      = mpInfo_FullRaw;
    inputInfo.resizedRaw   = mpInfo_ResizedRaw;
    inputInfo.lcsoRaw      = mpInfo_LcsoRaw;
    //
    inputInfo.fullRaw      = mpInfo_FullRaw;
    inputInfo.resizedRaw   = mpInfo_ResizedRaw;
    inputInfo.lcsoRaw      = mpInfo_LcsoRaw;
    inputInfo.jpegYuv      = mpInfo_Yuv;
    inputInfo.thumbnailYuv = mpInfo_YuvThumbnail;
    inputInfo.postview     = mpInfo_YuvPostview;
    inputInfo.jpeg         = mpInfo_Jpeg;

    plugin::OutputInfo outputInfo;
    mpManager->get(plugin::CALLER_SHOT_SMART, inputInfo, outputInfo);
    // if not ZSD, removes delay frame if necessary
    if ( ! mbZsdFlow && outputInfo.frameCount > m_frameNum) {
        int _removesCnt = outputInfo.frameCount - m_frameNum;
        // removes delay frame
        for ( int i = 0; i < _removesCnt; i++)
            outputInfo.settings.pop();
        outputInfo.frameCount = m_frameNum;
    }
    //
    plugin::VendorInfo vInfo;
    vInfo.vendorMode = mode;
    //
    plugin::InputSetting inputSetting;
    inputSetting.pMsgCb = this;
    for ( int i = 0; i < outputInfo.frameCount; ++i ) {
        plugin::FrameInfo info;
        info.frameNo = mCapReqNo + i;
        info.curAppControl = mShotParam.mAppSetting;
        info.curHalControl = mShotParam.mHalSetting;
        info.curAppControl += outputInfo.settings[i].setting.appMeta;
        info.curHalControl += outputInfo.settings[i].setting.halMeta;
        inputSetting.vFrame.push_back(info);
    }
    // update p2 control
    updateSetting(outputInfo, inputSetting);
    //
    mpManager->set(plugin::CALLER_SHOT_SMART, inputSetting);
    //
    MINT32 shotCount = inputSetting.vFrame.size();
    if ( mbZsdFlow ) {
        // submit to zsd preview pipeline
        Vector<SettingSet> vSettings;
        for ( size_t i = 0; i < shotCount; ++i ) {
            SettingSet s;
            s.appSetting = inputSetting.vFrame[i].curAppControl;
            s.halSetting = inputSetting.vFrame[i].curHalControl;
            vSettings.push_back(s);
        }
        applyRawBufferSettings(vSettings, inputSetting.vFrame.size());
    }

    // get multiple raw buffer and send to capture pipeline
    for ( MINT32 i = 0; i < shotCount; ++i ) {
        IMetadata halSetting = mShotParam.mHalSetting;
        if ( mbZsdFlow ) {
            // get Selector Data (buffer & metadata)
            status_t status = OK;
            android::sp<IImageBuffer> pBuf = NULL; // full raw buffer
            IMetadata selectorAppMetadata; // app setting for this raw buffer. Ex.3A infomation
            status = getSelectorData(
                        selectorAppMetadata,
                        halSetting,
                        pBuf
                    );
            if( status != OK ) {
                MY_LOGE("GetSelectorData Fail!");
                return MFALSE;
            }
            //
            {
                Mutex::Autolock _l(mResultMetadataSetLock);
                mResultMetadataSetMap.editValueFor(mCapReqNo).selectorAppMetadata = selectorAppMetadata;
            }
        } else {
            halSetting += inputSetting.vFrame[i].curHalControl;
            IMetadata::IEntry entry(MTK_HAL_REQUEST_SENSOR_SIZE);
            entry.push_back(mSensorSize, Type2Type< MSize >());
            halSetting.update(entry.tag(), entry);
            {
                Mutex::Autolock _l(mResultMetadataSetLock);
                IMetadata resultAppMetadata;
                IMetadata resultHalMetadata;
                IMetadata selectorAppMetadata;
                Vector<ISelector::BufferItemSet> bufferSet;
                mResultMetadataSetMap.add(
                        static_cast<MUINT32>(mCapReqNo),
                        ResultSet_T{static_cast<MUINT32>(mCapReqNo), resultAppMetadata, resultHalMetadata, selectorAppMetadata, bufferSet, NONE_CB_DONE}
                        );
            }
        }
        // submit setting to capture pipeline
        if (OK != submitCaptureSetting(
                    i == 0,
                    inputSetting.vFrame[i].curAppControl,
                    halSetting) ) {
            MY_LOGE("Submit capture setting fail.");
            return MFALSE;
        }
        //
        mCapReqNo++;
    }
    // 1. wait pipeline done
    // 2. set selector back to default zsd selector
    // 3. set full raw buffer count back to normal
    endCapture();
    //
    return MTRUE;

}

// ----------------------------------------------------------------------------

MBOOL
CollectShot::
createStreams()
{
    FUNCTION_SCOPE;
    CAM_TRACE_CALL();
    //
    mu4Scenario = SENSOR_SCENARIO_ID_NORMAL_CAPTURE;
    mu4Bitdepth = getShotRawBitDepth(); // ImpShot
    //
    MUINT32 const openId        = getOpenId();
    MUINT32 const sensorMode    = mu4Scenario;
    MUINT32 const bitDepth      = mu4Bitdepth;
    //
    MINT const yuvfmt_thumbnail = eImgFmt_YUY2;
    MSize const thumbnailsize = MSize(mJpegParam.mi4JpegThumbWidth, mJpegParam.mi4JpegThumbHeight);

    if(mbZsdFlow) {
        mpInfo_ResizedRaw = nullptr;
        Vector<sp<IImageStreamInfo>> rawInputInfos;
        sp<StreamBufferProvider> pConsumer = mpConsumer.promote();
        if( !pConsumer.get() ) return UNKNOWN_ERROR;
        pConsumer->querySelector()->queryCollectImageStreamInfo( rawInputInfos );
        for(size_t i = 0; i < rawInputInfos.size() ; i++)
        {
            if(rawInputInfos[i]->getStreamId() == eSTREAMID_IMAGE_PIPE_RAW_LCSO)
                mpInfo_LcsoRaw = rawInputInfos[i];
            else if(rawInputInfos[i]->getStreamId() == eSTREAMID_IMAGE_PIPE_RAW_OPAQUE)
                mpInfo_FullRaw = rawInputInfos[i];
        }
    }
    else {
        // Fullsize Raw
        {
            HwInfoHelper helper(openId);
            if( ! helper.updateInfos() ) {
                MY_LOGE("cannot properly update infos");
                return MFALSE;
            }
            //
            if( ! helper.getSensorSize( sensorMode, mSensorSize) ||
                    ! helper.getSensorFps( sensorMode, mSensorFps) ||
                    ! helper.queryPixelMode( sensorMode, mSensorFps, mPixelMode)
              ) {
                MY_LOGE("cannot get params about sensor");
                return MFALSE;
            }
            //
            MSize size = mSensorSize;
            MINT format;
            size_t stride;
            MUINT const usage = 0; //not necessary here
            if( ! helper.getImgoFmt(bitDepth, format) ||
                    ! helper.alignPass1HwLimitation(mPixelMode, format, true, size, stride) )
            {
                MY_LOGE("wrong params about imgo");
                return MFALSE;
            }
            //
            sp<IImageStreamInfo>
                pStreamInfo = createRawImageStreamInfo(
                        "SmartShot:Fullraw",
                        eSTREAMID_IMAGE_PIPE_RAW_OPAQUE,
                        eSTREAMTYPE_IMAGE_INOUT,
                        m_frameNum, 1,
                        usage, format, size, stride
                        );
            if( pStreamInfo == nullptr ) {
                return MFALSE;
            }
            mpInfo_FullRaw = pStreamInfo;
        }
        // Lcso Raw
        if( mShotParam.mbEnableLtm )
        {
            NS3Av3::LCSO_Param lcsoParam;
            if ( auto pIspMgr = MAKE_IspMgr() ) {
                pIspMgr->queryLCSOParams(lcsoParam);
            }

            MUINT const usage = eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE; //not necessary here
            //
            sp<IImageStreamInfo>
                pStreamInfo = createRawImageStreamInfo(
                        "SingleShot:LCSraw",
                        eSTREAMID_IMAGE_PIPE_RAW_LCSO,
                        eSTREAMTYPE_IMAGE_INOUT,
                        m_frameNum, 1,
                        usage, lcsoParam.format, lcsoParam.size, lcsoParam.stride
                        );
            if( pStreamInfo == nullptr ) {
                return MFALSE;
            }
            mpInfo_LcsoRaw = pStreamInfo;
        }
    }
    //
    // JPEG YUV: we always use full sensor size as output YUV
    {
        MSize size        = MSize(mSensorSize.w, mSensorSize.h);
        MINT format       = eImgFmt_YUY2;
        MUINT32 transform = 0;
        if (format == 0 || format == eImgFmt_UNKNOWN) {
            MY_LOGE("output format YUV is undefined");
            return MFALSE;
        }
        MUINT const usage = 0; //not necessary here
        MY_LOGD("output YUV %dx%d fmt=%d", size.w, size.h, format);
        sp<IImageStreamInfo>
            pStreamInfo = createImageStreamInfo(
                    "SmartShot:MainYuv",
                    eSTREAMID_IMAGE_PIPE_YUV_JPEG,
                    eSTREAMTYPE_IMAGE_INOUT,
                    2, 1,
                    usage, format, size, transform
                    );
        if( pStreamInfo == nullptr ) {
            return BAD_VALUE;
        }
        mpInfo_Yuv = pStreamInfo;
    }
    //
    // Thumbnail Yuv
    {
        MSize size        = thumbnailsize;
        MINT format       = yuvfmt_thumbnail;
        MUINT const usage = 0; //not necessary here
        MUINT32 transform = mShotParam.mu4Transform;
        sp<IImageStreamInfo>
            pStreamInfo = createImageStreamInfo(
                    "SmartShot:ThumbnailYuv",
                    eSTREAMID_IMAGE_PIPE_YUV_THUMBNAIL,
                    eSTREAMTYPE_IMAGE_INOUT,
                    2, 1,
                    usage, format, size, transform
                    );
        if( pStreamInfo == nullptr ) {
            return BAD_VALUE;
        }
        //
        mpInfo_YuvThumbnail = pStreamInfo;
    }
    // Jpeg
    {
        MSize size        = mJpegsize;
        MINT format       = eImgFmt_BLOB;
        MUINT const usage = 0; //not necessary here
        MUINT32 transform = 0;
        sp<IImageStreamInfo>
            pStreamInfo = createImageStreamInfo(
                    "SmartShot:Jpeg",
                    eSTREAMID_IMAGE_JPEG,
                    eSTREAMTYPE_IMAGE_INOUT,
                    1, 1,
                    usage, format, size, transform
                    );
        if( pStreamInfo == nullptr ) {
            return BAD_VALUE;
        }
        //
        mpInfo_Jpeg = pStreamInfo;
    }

    return MTRUE;
}

// ----------------------------------------------------------------------------

MVOID
CollectShot::
onMetaReceived(
        MUINT32         const requestNo,
        StreamId_T      const streamId,
        MBOOL           const errorResult,
        IMetadata       const result
        )
{
    SmartShot::onMetaReceived(requestNo, streamId, errorResult, result);
}
