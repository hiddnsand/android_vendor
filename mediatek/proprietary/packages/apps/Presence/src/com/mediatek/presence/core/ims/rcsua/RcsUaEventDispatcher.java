package com.mediatek.presence.core.ims.rcsua;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.mediatek.presence.utils.logger.Logger;
import com.mediatek.presence.core.ims.rcsua.RcsUaAdapter;
import com.mediatek.presence.core.ims.rcsua.RcsUaAdapter.RcsUaEvent;

import java.util.ArrayList;

/**
 * The Class RcsUaEventDispatcher.
 */
public class RcsUaEventDispatcher extends Handler {

    private Context mContext;
    private ArrayList<RCSEventDispatcher> mRCSEventDispatcher = new ArrayList<RCSEventDispatcher>();
    private static final String TAG = "PresenceUaEventDispatcher";
    private RcsProxySipHandler mRCSProxySiphandler;
    private RcsProxyRegistrationHandler mRCSRegistrationHandler;

    /**
     * The logger
     */
    private Logger logger = Logger.getLogger("PresenceUaEventDispatcher");

    /**
     * Instantiates a new rcs ua event dispatcher.
     *
     * @param context the context
     */
    public RcsUaEventDispatcher(Context context) {
        mContext = context;
        //create dispatcher
        logger.debug( "Initialize the handlers ");
        //add the RCS Registration handler
        mRCSRegistrationHandler = new RcsProxyRegistrationHandler(mContext);
        mRCSEventDispatcher.add(mRCSRegistrationHandler);
        //add the RCS proxy SIP handler
        mRCSProxySiphandler = new RcsProxySipHandler(mContext);
        mRCSEventDispatcher.add(mRCSProxySiphandler);
    }

    /**
     * Dispatch callback.
     *
     * @param event the event
     */
    void dispatchCallback(RcsUaEvent event) {
        logger.debug( "Event received : " + event.getRequestID());
        switch (event.getRequestID()) {
        case RcsUaAdapter.RCS_PROXY_EVENT_RES_REG_INFO:
            RcsUaAdapter.getInstance().handleRegistrationInfo(event);
            break;
        case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_REGISTER:
        case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_REGISTERING:
        case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_DEREGISTER:
        case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_DEREGISTERING:
        case RcsUaAdapter.RCS_PROXY_EVENT_IND_IMS_DEREG_START:
            mRCSRegistrationHandler.EventCallback(event);
            break;
        case RcsUaAdapter.RCS_PROXY_EVENT_RSP_SIP_Send:
            mRCSProxySiphandler.EventCallback(event);
            break;
        }
    }

    /**
     * The Interface RCSEventDispatcher.
     */
    public interface RCSEventDispatcher {

        /**
         * Event callback.
         *
         * @param event the event
         */
        void EventCallback(RcsUaEvent event);

        /**
         * Enable request.
         */
        void enableRequest();

        /**
         * Disable request.
         */
        void disableRequest();
    }

    /**
     * Enable request.
     */
    void enableRequest() {
        for (RCSEventDispatcher dispatcher : mRCSEventDispatcher) {
            dispatcher.enableRequest();
        }
    }

    /**
     * Disable request.
     */
    void disableRequest() {
        for (RCSEventDispatcher dispatcher : mRCSEventDispatcher) {
            dispatcher.disableRequest();
        }
    }


    /**
     * HANDLE MESSAGES FROM RCS PROXY.
     *
     * @param msg the msg
     */
    @Override
    public void handleMessage(Message msg) {
        dispatchCallback((RcsUaEvent) msg.obj);
    }

    /**
     * Gets the sip event dispatcher.
     *
     * @return the sip event dispatcher
     */
    public RCSEventDispatcher getSipEventDispatcher() {
        return mRCSProxySiphandler;
    }

    /**
     * Gets the registration event handler.
     *
     * @return the registration event handler
     */
    public RCSEventDispatcher getRegistrationEventHandler() {
        return mRCSRegistrationHandler;
    }
}
