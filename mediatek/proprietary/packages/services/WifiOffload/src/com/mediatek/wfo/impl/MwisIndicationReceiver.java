/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.wfo.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.Rlog;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import com.mediatek.internal.telephony.MtkTelephonyIntents;

/**
 * Broadcast receiver for listening the RIL indication from MtkRaido Indication.
 */
public class MwisIndicationReceiver extends BroadcastReceiver {
    private static final String TAG = "MwisIndicationReceiver";

    Context mContext;
    Handler mWifiPdnHandler;
    Handler mWfcHandler;
    Handler mWfcLocationHandler;

    public MwisIndicationReceiver(Context context) {
        mContext = context;
        registerIndicationBroadcast();
    }

    private void registerIndicationBroadcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MtkTelephonyIntents.ACTION_RSSI_THRESHOLD_CHANGED);
        intentFilter.addAction(MtkTelephonyIntents.ACTION_WIFI_PDN_ACTIVATED);
        intentFilter.addAction(MtkTelephonyIntents.ACTION_WFC_PDN_ERROR);
        intentFilter.addAction(MtkTelephonyIntents.ACTION_PDN_HANDOVER);
        intentFilter.addAction(MtkTelephonyIntents.ACTION_PDN_ROVE_OUT);
        intentFilter.addAction(MtkTelephonyIntents.ACTION_REQUEST_GEO_LOCATION);
        intentFilter.addAction(MtkTelephonyIntents.ACTION_WFC_PDN_STATE_CHANGED);
        mContext.registerReceiver(this, intentFilter);
    }

    public void setWifiPdnHandler(WifiPdnHandler wifiPdnHandler) {
        mWifiPdnHandler = wifiPdnHandler.getHandler();
    }

    public void setWfcHandler(WfcHandler wfcHandler) {
        mWfcHandler = wfcHandler.getHandler();
    }

    public void setWfcLocationHandler(WfcLocationHandler wfcLocationHandler) {
        mWfcLocationHandler = wfcLocationHandler.getHandler();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Rlog.d(TAG, "onReceive action: " + intent.getAction());

        String extra;
        int msgId;
        Handler handler;

        switch (intent.getAction()) {
            case MtkTelephonyIntents.ACTION_RSSI_THRESHOLD_CHANGED:
                sendIntentAsMessageToHandler(intent,
                        WifiPdnHandler.EVENT_ON_WIFI_MONITORING_THRESHOLD_CHANGED,
                        mWifiPdnHandler);
                break;
            case MtkTelephonyIntents.ACTION_WIFI_PDN_ACTIVATED:
                sendIntentAsMessageToHandler(intent,
                        WifiPdnHandler.EVENT_ON_WIFI_PDN_ACTIVATE, mWifiPdnHandler);
                break;
            case MtkTelephonyIntents.ACTION_WFC_PDN_ERROR:
                sendIntentAsMessageToHandler(intent,
                        WfcHandler.EVENT_ON_PDN_ERROR, mWfcHandler);
                break;
            case MtkTelephonyIntents.ACTION_PDN_HANDOVER:
                sendIntentAsMessageToHandler(intent,
                        WfcHandler.EVENT_ON_PDN_HANDOVER, mWfcHandler);
                break;
            case MtkTelephonyIntents.ACTION_PDN_ROVE_OUT:
                sendIntentAsMessageToHandler(intent,
                        WfcHandler.EVENT_ON_ROVE_OUT, mWfcHandler);
                break;
            case MtkTelephonyIntents.ACTION_REQUEST_GEO_LOCATION:
                sendIntentAsMessageToHandler(intent,
                        WfcLocationHandler.EVENT_GET_LOCATION_REQUEST, mWfcLocationHandler);
                break;
            case MtkTelephonyIntents.ACTION_WFC_PDN_STATE_CHANGED:
                sendIntentAsMessageToHandler(intent,
                        WfcHandler.EVENT_ON_WFC_PDN_STATE_CHANGED, mWfcHandler);
                break;
            default:
                Rlog.d(TAG, intent.getAction() + " no implements");
        }
    }

    private void sendIntentAsMessageToHandler(Intent intent, int msgId, Handler handler) {
        AsyncResult msgObj;
        msgObj = new AsyncResult(null, intent, null);
        Message msg = mWfcHandler.obtainMessage(msgId, msgObj);
        handler.sendMessage(msg);
    }
}
