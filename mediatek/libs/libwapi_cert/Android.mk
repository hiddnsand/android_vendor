LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/frameworks/opt/wapicertstore))
include $(CLEAR_VARS)
LOCAL_MODULE = libwapi_cert
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_SUFFIX = .so
LOCAL_PROPRIETARY_MODULE = true
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = liblog libwapi libnativehelper
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libwapi_cert.so
include $(BUILD_PREBUILT)
endif
