/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;

import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.android.ims.ImsConfig;
import com.android.ims.ImsManager;
import com.android.ims.internal.IImsConfig;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.ims.config.ImsConfigContract;
import com.mediatek.ims.config.internal.ImsConfigImpl;
import com.mediatek.ims.config.internal.ImsConfigUtils;
import com.mediatek.ims.config.internal.ImsConfigStorage;
import com.mediatek.ims.config.internal.MtkImsConfigImpl;
import com.mediatek.ims.internal.IMtkImsConfig;
import com.mediatek.ims.ril.ImsCommandsInterface;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;

import java.util.HashMap;
import java.util.Map;

public class ImsConfigManager {
    private static final String LOG_TAG = "ImsConfigManager";
    private static final String PROP_FORCE_DEBUG_KEY = "persist.log.tag.tel_dbg";
    private static final boolean DEBUG = TextUtils.equals(Build.TYPE, "eng")
            || (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1);
    private static final boolean SENLOG = TextUtils.equals(Build.TYPE, "user");
    private static final boolean TELDBG = (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1);

    private static final String PROPERTY_CAPABILITY_SWITCH = "persist.radio.simswitch";

    private Context mContext;

    private ImsCommandsInterface [] mImsRILAdapters = null;

    /// M: IMS Configuration
    /// Maintain ImsConfigImpl,mImsConfigStorageMap instance for each phoneId
    private Map<Integer, IImsConfig> mImsConfigInstanceMap =
            new HashMap<Integer, IImsConfig>();
    private Map<Integer, ImsConfigStorage> mImsConfigStorageMap =
            new HashMap<Integer, ImsConfigStorage>();

    /// M: BSP+ feature @{
    private Map<Integer, IMtkImsConfig> mMtkImsConfigInstanceMap =
            new HashMap<Integer, IMtkImsConfig>();
    /// @}

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (ImsConfigContract.ACTION_DYNAMIC_IMS_SWITCH_TRIGGER.equals(intent.getAction())) {
                /// M: Dynamic IMS Switch, intent send from ImsConfigReceiver

                int phoneId = intent.getIntExtra(PhoneConstants.PHONE_KEY, -1);
                String simState = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);

                Log.d(LOG_TAG, "DYNAMIC_IMS_SWITCH_TRIGGER phoneId:" + phoneId +
                            ", simState:" + simState);

                // Should update ImsResCapability value in ImsConfig for different phoneId
                updateImsResrouceCapability(context, intent);

            } else if (TelephonyIntents.ACTION_SET_RADIO_CAPABILITY_DONE.equals(intent.getAction())) {

                Log.d(LOG_TAG, "ACTION_SET_RADIO_CAPABILITY_DONE supportMims:" +
                        ImsCommonUtil.supportMims());

                if (ImsCommonUtil.supportMims() == false) {
                    // 93 sim switch will not sim reset, recalculate main capability phoneId
                    ImsManager.updateImsServiceConfig(context, getMainCapabilityPhoneId(), true);
                }
            } else if (ImsConfig.ACTION_IMS_CONFIG_CHANGED.equals(intent.getAction())) {
                // For N MR1 after to update provisioned in ImsManager
                int configId = intent.getIntExtra(ImsConfig.EXTRA_CHANGED_ITEM, -1);
                int configVal = intent.getIntExtra(ImsConfig.EXTRA_NEW_VALUE, -1);

                if (DEBUG) {
                    Log.d(LOG_TAG, "update configId:" + configId + ", configVal:" + configVal);
                }

                if (ImsConfig.ConfigConstants.VLT_SETTING_ENABLED == configId) {
                    if (DEBUG) Log.d(LOG_TAG, "update VLT provision config");
                    ImsManager.onProvisionedValueChanged(context,
                            ImsConfig.ConfigConstants.VLT_SETTING_ENABLED,
                            Integer.toString(configVal));

                } else if (ImsConfig.ConfigConstants.VOICE_OVER_WIFI_SETTING_ENABLED == configId) {
                    if (DEBUG) Log.d(LOG_TAG, "update WFC provision config");
                    ImsManager.onProvisionedValueChanged(context,
                            ImsConfig.ConfigConstants.VOICE_OVER_WIFI_SETTING_ENABLED,
                            Integer.toString(configVal));

                } else if (ImsConfig.ConfigConstants.LVC_SETTING_ENABLED == configId) {
                    if (DEBUG) Log.d(LOG_TAG, "update LVC provision config");
                    ImsManager.onProvisionedValueChanged(context,
                            ImsConfig.ConfigConstants.LVC_SETTING_ENABLED,
                            Integer.toString(configVal));

                }
            }
        }
    };

    public ImsConfigManager(Context context, ImsCommandsInterface [] imsRILAdapters) {
        if (DEBUG) Log.d(LOG_TAG, "ImsConfigManager Enter");

        mContext = context;
        mImsRILAdapters = imsRILAdapters;

        final IntentFilter filter = new IntentFilter();
        filter.addAction(ImsConfigContract.ACTION_DYNAMIC_IMS_SWITCH_TRIGGER);
        // Recalculate ims feature value after 93 SIM switch
        if (RadioCapabilitySwitchUtil.isDssNoResetSupport()) {
            filter.addAction(TelephonyIntents.ACTION_SET_RADIO_CAPABILITY_DONE);
        }
        filter.addAction(ImsConfig.ACTION_IMS_CONFIG_CHANGED);
        context.registerReceiver(mBroadcastReceiver, filter);
    }

    /**
     * To initial ImsConfigImpl(AOSP) Implementation
     *
     * @return void.
     */
    public void init(int phoneId) {
        ImsConfigStorage storage;
        ImsConfigImpl instance;

        storage = getImsConfigStorage(mContext, mImsRILAdapters, phoneId);

        if (DEBUG) Log.d(LOG_TAG, "init ImsConfigImpl phoneId:" + phoneId);
        synchronized (mImsConfigInstanceMap) {
            instance = new ImsConfigImpl(mContext, mImsRILAdapters[phoneId], storage, phoneId);
            mImsConfigInstanceMap.put(phoneId, instance);
        }
    }

    /**
     * To get and initial ImsConfigImpl(AOSP) Implementation,
     * and API will return IMtkImsConfig
     *
     * @return IImsConfig, AOSP defined interface.
     */
    public IImsConfig get(int phoneId) {
        ImsConfigStorage storage;
        IImsConfig instance = null;

        storage = getImsConfigStorage(mContext, mImsRILAdapters, phoneId);

        synchronized (mImsConfigInstanceMap) {
            if (mImsConfigInstanceMap.containsKey(phoneId)) {
                instance = mImsConfigInstanceMap.get(phoneId);
            } else {
                instance = new ImsConfigImpl(mContext, mImsRILAdapters[phoneId], storage, phoneId);
                mImsConfigInstanceMap.put(phoneId, instance);
            }
        }
        return instance;
    }

    /**
     * To initial ImsConfigImpl(AOSP) and MtkImsConfigImpl(Add-On) Implementation
     *
     * @return void.
     */
    public void initEx(int phoneId) {
        ImsConfig imsConfig;
        ImsConfigStorage storage;
        ImsConfigImpl instance;
        MtkImsConfigImpl instanceEx;

        storage = getImsConfigStorage(mContext, mImsRILAdapters, phoneId);

        if (DEBUG) Log.d(LOG_TAG, "initEx ImsConfigImpl phoneId:" + phoneId);
        synchronized (mImsConfigInstanceMap) {
            instance = new ImsConfigImpl(mContext, mImsRILAdapters[phoneId], storage, phoneId);
            mImsConfigInstanceMap.put(phoneId, instance);
            imsConfig = new ImsConfig(instance, mContext);
        }

        if (DEBUG) Log.d(LOG_TAG, "initEx MtkImsConfigImpl phoneId:" + phoneId);
        synchronized (mMtkImsConfigInstanceMap) {
            instanceEx = new MtkImsConfigImpl(mContext, mImsRILAdapters[phoneId], imsConfig, storage, phoneId);
            mMtkImsConfigInstanceMap.put(phoneId, instanceEx);
        }
    }

    /**
     * To get and initial ImsConfigImpl(AOSP) and MtkImsConfigImpl(Add-On) Implementation,
     * and API will return IMtkImsConfig
     *
     * @return IMtkImsConfig, MTK defined interface.
     */
    public IMtkImsConfig getEx(int phoneId) {
        ImsConfig imsConfig;
        ImsConfigStorage storage;
        IImsConfig instance = null;
        IMtkImsConfig instanceEx = null;

        storage = getImsConfigStorage(mContext, mImsRILAdapters, phoneId);

        synchronized (mImsConfigInstanceMap) {
            if (mImsConfigInstanceMap.containsKey(phoneId)) {
                instance = mImsConfigInstanceMap.get(phoneId);
            } else {
                instance = new ImsConfigImpl(mContext, mImsRILAdapters[phoneId], storage, phoneId);
                mImsConfigInstanceMap.put(phoneId, instance);
            }
            imsConfig = new ImsConfig(instance, mContext);
        }

        synchronized (mMtkImsConfigInstanceMap) {
            if (mMtkImsConfigInstanceMap.containsKey(phoneId)) {
                instanceEx = mMtkImsConfigInstanceMap.get(phoneId);
            } else {
                instanceEx = new MtkImsConfigImpl(mContext, mImsRILAdapters[phoneId],
                                                  imsConfig, storage, phoneId);
                mMtkImsConfigInstanceMap.put(phoneId, instanceEx);
            }
        }
        return instanceEx;
    }

    /**
     * To get ImsConfigStorage from storage map, only create storage for each phone once.
     *
     * @return ImsConfigStorage.
     */
    private ImsConfigStorage getImsConfigStorage(Context context,
            ImsCommandsInterface[] imsRilAdapters, int phoneId) {
        ImsConfigStorage storage;

        synchronized (mImsConfigStorageMap) {
            if (mImsConfigStorageMap.containsKey(phoneId)) {
                storage = mImsConfigStorageMap.get(phoneId);
            } else {
                if (DEBUG) Log.d(LOG_TAG, "init ImsConfigStorage phone:" + phoneId);
                storage = new ImsConfigStorage(context, phoneId, imsRilAdapters);
                mImsConfigStorageMap.put(phoneId, storage);
            }
        }
        return storage;
    }

    /// M: Dynamic IMS Switch @{
    private void updateImsResrouceCapability(Context context, Intent intent) {
        int mcc = 0;
        int mnc = 0;
        IMtkImsConfig imsConfig = null;
        String mccMnc = null;

        String simState = intent.getStringExtra((IccCardConstants.INTENT_KEY_ICC_STATE));
        int phoneId = intent.getIntExtra(PhoneConstants.PHONE_KEY, -1);

        if (!"1".equals(SystemProperties.get("persist.mtk_dynamic_ims_switch"))) {
            if (simState.equalsIgnoreCase(IccCardConstants.INTENT_VALUE_ICC_ABSENT) ||
                    simState.equalsIgnoreCase(IccCardConstants.INTENT_VALUE_ICC_LOADED)) {
                Log.d(LOG_TAG, "updateImsServiceConfig after SIM event, phoneId:" + phoneId);
                // Force update IMS feature values after SIM event.
                updateImsServiceConfig(context, phoneId);
            }
            return;
        }

        Log.d(LOG_TAG, "get MtkImsConfigImpl of phone " + phoneId);
        imsConfig = getEx(phoneId);

        try {
            int volteResVal;
            int vilteResVal;
            int wfcResVal;

            if (simState.equalsIgnoreCase(IccCardConstants.INTENT_VALUE_ICC_ABSENT)) {
                Log.w(LOG_TAG, "setImsResCapability to volte only w/o SIM on phone " + phoneId);
                // Back to volte only w/o SIM card.
                volteResVal = ImsConfig.FeatureValueConstants.ON;
                vilteResVal = ImsConfig.FeatureValueConstants.OFF;
                wfcResVal = ImsConfig.FeatureValueConstants.OFF;

                imsConfig.setImsResCapability(
                        ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE, volteResVal);
                imsConfig.setImsResCapability(
                        ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE, vilteResVal);
                imsConfig.setImsResCapability(
                        ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI, wfcResVal);

                // Force update IMS feature values after SIM absent event.
                // Cover by force update after receive CARRIER_CONFIG_CHANGED after sim ABSENT
                // ImsManager.updateImsServiceConfig(context, phoneId, true);
            } else if (simState.equalsIgnoreCase(IccCardConstants.INTENT_VALUE_ICC_LOADED)) {
                if (!isTestSim(phoneId)) {
                    TelephonyManager tm = (TelephonyManager) context
                            .getSystemService(Context.TELEPHONY_SERVICE);

                    mccMnc = OperatorUtils.getSimOperatorNumericForPhone(phoneId);
                    if (mccMnc == null || mccMnc.isEmpty()) {
                        Log.w(LOG_TAG, "Invalid mccMnc:" + mccMnc);
                        return;
                    }

                    mcc = Integer.parseInt(mccMnc.substring(0, 3));
                    mnc = Integer.parseInt(mccMnc.substring(3));

                    Log.d(LOG_TAG, "SIM loaded on phone " + phoneId + " with mcc: " +
                            mcc + " mnc: " + mnc);

                    int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(phoneId);
                    String iccid = tm.getSimSerialNumber(subId);
                    if (!SENLOG || TELDBG) {
                        Log.d(LOG_TAG, "check iccid:"+ iccid);
                    }
                    // replace mcc mnc id to 46605 for APTG roaming case
                    if (!TextUtils.isEmpty(iccid)) {
                        if (iccid.startsWith("8988605")) {
                            if (DEBUG) Log.d(LOG_TAG, "Replace mccmnc for APTG roaming case");
                            mcc = 466;
                            mnc = 5;
                        }
                        /// M: ALPS03514610 replace mcc mnc id to 46003 for CT roaming case @{
                        else if (iccid.startsWith("898603") || iccid.startsWith("898611")) {
                            mcc = 460;
                            mnc = 3;
                            if (DEBUG) Log.d(LOG_TAG, "Replace mccmnc for CT roaming case");
                        }
                        /// @}
                    }

                    // Retrive resource with specific mcc mnc value.
                    Configuration newConfiguration;
                    Resources res = context.getResources();
                    newConfiguration = res.getConfiguration();
                    newConfiguration.mcc = mcc;
                    newConfiguration.mnc = mnc == 0 ? Configuration.MNC_ZERO : mnc;
                    res.updateConfiguration(newConfiguration, null);

                    if (OperatorUtils.isCTVolteDisabled(phoneId)) {
                        volteResVal = ImsConfig.FeatureValueConstants.OFF;
                        vilteResVal = ImsConfig.FeatureValueConstants.OFF;
                        wfcResVal = ImsConfig.FeatureValueConstants.OFF;
                    } else {
                        volteResVal = mapFeatureValue(res.getBoolean(
                                com.android.internal.R.bool.config_device_volte_available));
                        vilteResVal = mapFeatureValue(res.getBoolean(
                                com.android.internal.R.bool.config_device_vt_available));
                        wfcResVal = mapFeatureValue(res.getBoolean(
                                com.android.internal.R.bool.config_device_wfc_ims_available));
                    }
                } else {
                    // For test SIM cards, forece enable all IMS functions for lab event.
                    Log.w(LOG_TAG, "Found test SIM on phone " + phoneId);
                    volteResVal = ImsConfig.FeatureValueConstants.ON;
                    vilteResVal = ImsConfig.FeatureValueConstants.ON;
                    wfcResVal = ImsConfig.FeatureValueConstants.ON;
                }

                Log.d(LOG_TAG, "Set res capability: volte = " + volteResVal +
                        ", vilte = " + vilteResVal + ", wfc = " + wfcResVal);

                imsConfig.setImsResCapability(
                        ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE, volteResVal);
                imsConfig.setImsResCapability(
                        ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE, vilteResVal);
                imsConfig.setImsResCapability(
                        ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI, wfcResVal);

                // Force update IMS feature values after SIM event.
                // Cover by force update after receive CARRIER_CONFIG_CHANGED after sim LOADED
                // updateImsServiceConfig(context, phoneId);
            }

            // Use to notify App to check platform support status again
            Intent mIntent = new Intent(ImsConfigContract.ACTION_DYNAMIC_IMS_SWITCH_COMPLETE);
            mIntent.putExtra(PhoneConstants.PHONE_KEY, phoneId);
            mIntent.putExtra(IccCardConstants.INTENT_KEY_ICC_STATE, simState);
            context.sendBroadcast(mIntent);
            Log.d(LOG_TAG, "DYNAMIC_IMS_SWITCH_COMPLETE phoneId:" + phoneId +
                        ", simState:" + simState);

        } catch (RemoteException e) {
            Log.e(LOG_TAG, "SetImsCapability fail: " + e);
        }
    }

    private static int mapFeatureValue(boolean value) {
        return (value == true) ? ImsConfig.FeatureValueConstants.ON :
                ImsConfig.FeatureValueConstants.OFF;
    }

    private static boolean isTestSim(int phoneId) {
        boolean isTestSim = false;
        switch (phoneId) {
            case PhoneConstants.SIM_ID_1:
                isTestSim = "1".equals(SystemProperties.get("gsm.sim.ril.testsim", "0"));
                break;
            case PhoneConstants.SIM_ID_2:
                isTestSim = "1".equals(SystemProperties.get("gsm.sim.ril.testsim.2", "0"));
                break;
            case PhoneConstants.SIM_ID_3:
                isTestSim = "1".equals(SystemProperties.get("gsm.sim.ril.testsim.3", "0"));
                break;
            case PhoneConstants.SIM_ID_4:
                isTestSim = "1".equals(SystemProperties.get("gsm.sim.ril.testsim.4", "0"));
                break;
        }
        return isTestSim;
    }
    /// @}

    /**
     * to get main capability phone id.
     *
     * @return The phone id with highest capability.
     */
    private int getMainCapabilityPhoneId() {
        int phoneId = SystemProperties.getInt(PROPERTY_CAPABILITY_SWITCH, 1) - 1;
        if (phoneId < 0 || phoneId >= TelephonyManager.getDefault().getPhoneCount()) {
            phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        }
        return phoneId;
    }

    private void updateImsServiceConfig(Context context, int phoneId) {
        if (ImsCommonUtil.supportMims()) {
            // update for each phoneId
            ImsManager.updateImsServiceConfig(context, phoneId, true);
        } else {
            // Non MIMS: update main capability phoneId only
            if (phoneId == getMainCapabilityPhoneId()) {
                ImsManager.updateImsServiceConfig(context, phoneId, true);
            } else {
                if (DEBUG) Log.d(LOG_TAG, "Do not update if phoneId is not main capability");
            }
        }
    }

}
