LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/hardware/hotknot))
include $(CLEAR_VARS)
LOCAL_MODULE = libhotknot_GT9XX
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_SUFFIX = .so
LOCAL_PROPRIETARY_MODULE = true
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = liblog libutils libmtk_drvb libcutils
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libhotknot_GT9XX.so
include $(BUILD_PREBUILT)
endif
