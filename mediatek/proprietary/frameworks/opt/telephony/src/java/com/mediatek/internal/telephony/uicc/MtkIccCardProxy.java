/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony.uicc;

import com.android.internal.telephony.uicc.IccCardProxy;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.uicc.IccCardApplicationStatus.PersoSubState;
import com.android.internal.telephony.uicc.IccCardApplicationStatus.AppState;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.uicc.IsimRecords;
import com.android.internal.telephony.uicc.IsimUiccRecords;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.IccCardConstants.State;
import com.android.internal.telephony.uicc.IccCardStatus.CardState;
import com.android.internal.telephony.uicc.IccCardStatus.PinState;
import com.android.internal.telephony.uicc.UiccController;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.RILConstants;
import com.android.internal.telephony.MccTable;
import com.mediatek.internal.telephony.MtkIccCardConstants;

import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Message;
import android.os.UserHandle;
import android.os.Registrant;
import android.os.RegistrantList;
import android.os.Handler;
import android.os.AsyncResult;
import android.os.SystemProperties;

import android.telephony.SubscriptionInfo;
import java.util.Arrays;

import android.app.ActivityManagerNative;
import android.telephony.SubscriptionManager;
import android.telephony.Rlog;

public class MtkIccCardProxy extends IccCardProxy implements MtkIccCard {
    private static final boolean DBG = true;
    private static final String LOG_TAG_EX = "MtkIccCardProxy";

    private static final int EVENT_ICC_FDN_CHANGED = 101;
    private RegistrantList mFdnChangedRegistrants = new RegistrantList();

    private static final String ICCID_STRING_FOR_NO_SIM = "N/A";
    private String[] PROPERTY_ICCID_SIM = {
        "ril.iccid.sim1",
        "ril.iccid.sim2",
        "ril.iccid.sim3",
        "ril.iccid.sim4",
    };

    private static final String[] PROPERTY_RIL_FULL_UICC_TYPE = {
        "gsm.ril.fulluicctype",
        "gsm.ril.fulluicctype.2",
        "gsm.ril.fulluicctype.3",
        "gsm.ril.fulluicctype.4",
    };
    // MTK-START: SIM ME LOCK
    private PersoSubState mNetworkLockState = PersoSubState.PERSOSUBSTATE_UNKNOWN;
    // MTK-END
    public MtkIccCardProxy(Context context, CommandsInterface ci, int phoneId) {
        super(context, ci, phoneId);
        if (DBG) mtkLog("ctor: ci=" + ci + " phoneId=" + phoneId);
        setExternalState(State.NOT_READY);
    }

    @Override
    public void dispose() {
        synchronized (mLock) {
            mtkLog("Disposing");
            //Cleanup icc references
            mUiccController.unregisterForIccChanged(this);
            mUiccController = null;
            mCi.unregisterForOn(this);
            mCi.unregisterForOffOrNotAvailable(this);
            // MTK-START
            if (mCdmaSSM != null) {
                mCdmaSSM.dispose(this);
                mCdmaSSM = null;
            }
            // MTK-END
        }
    }

    protected void updateQuietMode() {
        synchronized (mLock) {
            boolean newQuietMode;
            int cdmaSource = Phone.CDMA_SUBSCRIPTION_UNKNOWN;
            if (mCurrentAppType == UiccController.APP_FAM_3GPP) {
                newQuietMode = false;
                if (DBG) mtkLog("updateQuietMode: 3GPP subscription -> newQuietMode=" +
                                 newQuietMode);
            } else {
                // MTK-START
                if (mPhoneId == 0) {
                    String iscttddtest = SystemProperties.get("persist.sys.forcttddtest");
                    if (iscttddtest.equals("1")) {
                        mtkLog("updateQuietMode: force IccCardProxy into 3gpp for cttdd test");
                        mCurrentAppType = UiccController.APP_FAM_3GPP;
                    }
                }
                // MTK-END
                cdmaSource = mCdmaSSM != null ?
                        mCdmaSSM.getCdmaSubscriptionSource() : Phone.CDMA_SUBSCRIPTION_UNKNOWN;

                newQuietMode = (cdmaSource == Phone.CDMA_SUBSCRIPTION_NV)
                        && (mCurrentAppType == UiccController.APP_FAM_3GPP2);
            }

            if (mQuietMode == false && newQuietMode == true) {
                // Last thing to do before switching to quiet mode is
                // broadcast ICC_READY
                mtkLog("Switching to QuietMode.");
                setExternalState(State.READY);
                mQuietMode = newQuietMode;
            } else if (mQuietMode == true && newQuietMode == false) {
                if (DBG) {
                    mtkLog("updateQuietMode: Switching out from QuietMode."
                            + " Force broadcast of current state=" + mExternalState);
                }
                mQuietMode = newQuietMode;
                setExternalState(mExternalState, true);
            } else {
                if (DBG) mtkLog("updateQuietMode: no changes don't setExternalState");
            }
            if (DBG) {
                mtkLog("updateQuietMode: QuietMode is " + mQuietMode + " (app_type="
                    + mCurrentAppType + " cdmaSource=" + cdmaSource + ")");
            }
            mInitialized = true;
            sendMessage(obtainMessage(EVENT_ICC_CHANGED));
        }
    }

    @Override
    public void handleMessage(Message msg) {
         mtkLog("receive message " + msg.what);
         AsyncResult ar = null;

         switch (msg.what) {
             case EVENT_ICC_CHANGED:
                 if (mInitialized) {
                     ar = (AsyncResult) msg.obj;
                     int index = mPhoneId;

                     if (ar != null && ar.result instanceof Integer) {
                         index = ((Integer) ar.result).intValue();
                         mtkLog("handleMessage (EVENT_ICC_CHANGED) , index = " + index);
                     }

                     if (index == mPhoneId) {
                         updateIccAvailability();
                     }
                 }
                 break;
             case EVENT_ICC_FDN_CHANGED:
                 mFdnChangedRegistrants.notifyRegistrants();
                 break;

             case EVENT_RECORDS_LOADED:
                 // Update the MCC/MNC.
                 if (mIccRecords != null) {
                     String operator = mIccRecords.getOperatorNumeric();
                     mtkLog("operator=" + operator + " mPhoneId=" + mPhoneId);

                     if (operator != null) {
                         // Yang: keep codes here, bug fix?
                         // In case of, we need to use 3GPP application's value for daul mode card.
                         // SIMRecords will set this value and RuimRecord will check if CDMA only
                         // to decide if need to set this value.
                         // That is, we don't need to set value here.
                         // mTelephonyManager.setSimOperatorNumericForPhone(mPhoneId, operator);
                         String countryCode = operator.substring(0,3);
                         if (countryCode != null) {
                             // Add try catch for the country code is not number format.
                             try {
                                 mTelephonyManager.setSimCountryIsoForPhone(mPhoneId,
                                         MccTable.countryCodeForMcc(Integer.parseInt(countryCode)));
                             } catch (NumberFormatException e) {
                                 loge("Not number format: " + countryCode);
                             }
                         } else {
                             loge("EVENT_RECORDS_LOADED Country code is null");
                         }
                     } else {
                         loge("EVENT_RECORDS_LOADED Operator name is null");
                     }
                 }
                 if (mUiccCard != null && !mUiccCard.areCarrierPriviligeRulesLoaded()) {
                     mUiccCard.registerForCarrierPrivilegeRulesLoaded(
                         this, EVENT_CARRIER_PRIVILEGES_LOADED, null);
                 } else {
                     onRecordsLoaded();
                 }
                 break;
             default:
                 super.handleMessage(msg);
                 break;
        }
    }

    @Override
    protected void updateExternalState() {
        // mUiccCard could be null at bootup, before valid card states have
        // been received from UiccController.
        if (mUiccCard == null) {
            if (DBG) mtkLog("updateExternalState, broadcast UNKNOWN because UiccCard is null!");
            setExternalState(State.UNKNOWN);
            return;
        }

        if (mUiccCard.getCardState() == CardState.CARDSTATE_ABSENT) {
            // MTK-START
            // We should broadcast absent no mather radio is on or off,
            // since MTK has radio on/off feature.
            if (DBG) mtkLog("updateExternalState, broadcast ABSENT because card state is absent!");
            //if (mRadioOn) {
            //    setExternalState(State.ABSENT);
            //} else {
            //    setExternalState(State.NOT_READY);
            //}
            setExternalState(State.ABSENT);
            // MTK-END
            return;
        }

        if (mUiccCard.getCardState() == CardState.CARDSTATE_ERROR) {
            setExternalState(State.CARD_IO_ERROR);
            return;
        }

        if (mUiccApplication == null) {
            if (DBG) {
                mtkLog(
                    "updateExternalState, broadcast NOT_READY because mUiccApplication is null!");
            }
            setExternalState(State.NOT_READY);
            return;
        }

        switch (mUiccApplication.getState()) {
            case APPSTATE_UNKNOWN:
             /*
              * APPSTATE_UNKNOWN is a catch-all state reported whenever the app
              * is not explicitly in one of the other states. To differentiate the
              * case where we know that there is a card present, but the APP is not
              * ready, we choose NOT_READY here instead of unknown. This is possible
              * in at least two cases:
              * 1) A transient during the process of the SIM bringup
              * 2) There is no valid App on the SIM to load, which can be the case with an
              *    eSIM/soft SIM.
              */
            //O1 migration change.
                setExternalState(State.NOT_READY);
                break;
            case APPSTATE_DETECTED:
                HandleDetectedState();
                break;
            case APPSTATE_PIN:
                setExternalState(State.PIN_REQUIRED);
                break;
            case APPSTATE_PUK:
                setExternalState(State.PUK_REQUIRED);
                break;
            case APPSTATE_SUBSCRIPTION_PERSO:
                // MTK-START: SIM ME LOCK
                // Mediatek platform will set network locked for all
                // of subState (5 type of network locked)
                /*
                if (mUiccApplication.getPersoSubState() ==
                        PersoSubState.PERSOSUBSTATE_SIM_NETWORK) {
                    setExternalState(State.NETWORK_LOCKED);
                } else {
                    setExternalState(State.UNKNOWN);
                }
                */
                setExternalState(State.NETWORK_LOCKED);
                // MTK-END
                break;
            case APPSTATE_READY:
                setExternalState(State.READY);
                break;
            // MTK-START
            default:
                setExternalState(State.UNKNOWN);
                break;
            // MTK-END
        }
    }

    @Override
    protected void registerUiccCardEvents() {
        super.registerUiccCardEvents();
        if (mUiccApplication != null) {
            ((MtkUiccCardApplication)mUiccApplication).registerForFdnChanged(this,
                    EVENT_ICC_FDN_CHANGED, null);
        }
    }

    @Override
    protected void unregisterUiccCardEvents() {
        super.unregisterUiccCardEvents();
        if (mUiccApplication != null) {
            ((MtkUiccCardApplication)mUiccApplication).unregisterForFdnChanged(this);
        }
    }

    @Override
    protected void broadcastInternalIccStateChangedIntent(String value, String reason) {
        // MTK-START
        Intent intent = new Intent(ACTION_INTERNAL_SIM_STATE_CHANGED);
        // MTK-END
        synchronized (mLock) {
            if (mPhoneId == null) {
                loge("broadcastInternalIccStateChangedIntent: Card Index is not set; Return!!");
                return;
            }

            // MTK-START
            // Intent intent = new Intent(ACTION_INTERNAL_SIM_STATE_CHANGED);
            //intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING
            //        | Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);
            intent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);
            // MTK-END
            intent.putExtra(PhoneConstants.PHONE_NAME_KEY, "Phone");
            intent.putExtra(IccCardConstants.INTENT_KEY_ICC_STATE, value);
            intent.putExtra(IccCardConstants.INTENT_KEY_LOCKED_REASON, reason);
            intent.putExtra(PhoneConstants.PHONE_KEY, mPhoneId);  // SubId may not be valid.
            mtkLog("MtkIccCardProxy Sending intent ACTION_INTERNAL_SIM_STATE_CHANGED" +
                    " for mPhoneId : " + mPhoneId);
        }
        // MTK-START
        ActivityManagerNative.broadcastStickyIntent(intent, null, UserHandle.USER_ALL);
        // MTK-END
    }

    @Override
    protected void setExternalState(State newState, boolean override) {
        synchronized (mLock) {
            if (mPhoneId == null || !SubscriptionManager.isValidSlotIndex(mPhoneId)) {
                loge("setExternalState: mPhoneId=" + mPhoneId + " is invalid; Return!!");
                return;
            }

            // MTK-START
            if (DBG) mtkLog("setExternalState(): mExternalState = " + mExternalState +
                    " newState =  " + newState + " override = " + override);
            // MTK-END
            if (!override && newState == mExternalState) {
                // MTK-START: SIM ME LOCK
                if (newState == State.NETWORK_LOCKED &&
                    mNetworkLockState != getNetworkPersoType()) {
                    if (DBG) mtkLog("NetworkLockState =  " + mNetworkLockState);
                } else {
                // MTK-END
                    loge("setExternalState: !override and newstate unchanged from " + newState);
                    return;
                // MTK-START: SIM ME LOCK
                }
                // MTK-END
            }

            mExternalState = newState;
            loge("setExternalState: set mPhoneId=" + mPhoneId + " mExternalState=" +
                    mExternalState);
            // MTK-START: SIM ME LOCK
            mNetworkLockState = getNetworkPersoType();
            // MTK-END
            mTelephonyManager.setSimStateForPhone(mPhoneId, getState().toString());

            // For locked states, we should be sending internal broadcast.
            if (IccCardConstants.INTENT_VALUE_ICC_LOCKED.equals(getIccStateIntentString(
                    mExternalState))) {
                broadcastInternalIccStateChangedIntent(getIccStateIntentString(mExternalState),
                        getIccStateReason(mExternalState));
            } else {
                broadcastIccStateChangedIntent(getIccStateIntentString(mExternalState),
                        getIccStateReason(mExternalState));
            }
            // TODO: Need to notify registrants for other states as well.
            if ( State.ABSENT == mExternalState) {
                mAbsentRegistrants.notifyRegistrants();
            }
        }
    }

    @Override
    protected void setExternalState(State newState) {
        // MTK-START
        if (newState == State.PIN_REQUIRED && mUiccApplication != null) {
            PinState pin1State = mUiccApplication.getPin1State();
            if (pin1State == PinState.PINSTATE_ENABLED_PERM_BLOCKED) {
                if (DBG) mtkLog("setExternalState(): PERM_DISABLED");
                setExternalState(State.PERM_DISABLED);
                return;
            }
        }
        // MTK-END

        setExternalState(newState, false);
    }
    // MTK-START: SIM ME LOCK
    protected String getIccStateReason(State state) {
        if (DBG) mtkLog("getIccStateReason E");

        if (State.NETWORK_LOCKED == state && mUiccApplication != null) {
            switch (mUiccApplication.getPersoSubState()) {
                case PERSOSUBSTATE_SIM_NETWORK:
                    return IccCardConstants.INTENT_VALUE_LOCKED_NETWORK;
                case PERSOSUBSTATE_SIM_NETWORK_SUBSET:
                    return MtkIccCardConstants.INTENT_VALUE_LOCKED_NETWORK_SUBSET;
                case PERSOSUBSTATE_SIM_CORPORATE:
                    return MtkIccCardConstants.INTENT_VALUE_LOCKED_CORPORATE;
                case PERSOSUBSTATE_SIM_SERVICE_PROVIDER:
                    return MtkIccCardConstants.INTENT_VALUE_LOCKED_SERVICE_PROVIDER;
                case PERSOSUBSTATE_SIM_SIM:
                    return MtkIccCardConstants.INTENT_VALUE_LOCKED_SIM;
                default:
                    return null;
            }
        }

        return super.getIccStateReason(state);
    }
    // MTK-END
    @Override
    public void supplyPin(String pin, Message onComplete) {
        synchronized (mLock) {
            if (mUiccApplication != null) {
                mUiccApplication.supplyPin(pin, onComplete);
            } else if (onComplete != null) {
                // MTK-START
                // MTK flight mode power off modem feature and sim hot plug feature might cause
                // this API is called due to extremely time.
                // Return CommandException to ensure AP could handle the error message normally.
                //Exception e = new RuntimeException("ICC card is absent.");
                Exception e = CommandException.fromRilErrno(RILConstants.RADIO_NOT_AVAILABLE);
                mtkLog("Fail to supplyPin, hasIccCard = " + hasIccCard());
                // MTK-END
                AsyncResult.forMessage(onComplete).exception = e;
                onComplete.sendToTarget();
                return;
            }
        }
    }

    @Override
    public void supplyPuk(String puk, String newPin, Message onComplete) {
        synchronized (mLock) {
            if (mUiccApplication != null) {
                mUiccApplication.supplyPuk(puk, newPin, onComplete);
            } else if (onComplete != null) {
                // MTK-START
                // MTK flight mode power off modem feature and sim hot plug feature might cause
                // this API is called due to extremely time.
                // Return CommandException to ensure AP could handle the error message normally.
                //Exception e = new RuntimeException("ICC card is absent.");
                Exception e = CommandException.fromRilErrno(RILConstants.RADIO_NOT_AVAILABLE);
                mtkLog("Fail to supplyPuk, hasIccCard = " + hasIccCard());
                // MTK-END
                AsyncResult.forMessage(onComplete).exception = e;
                onComplete.sendToTarget();
                return;
            }
        }
    }

    @Override
    public void supplyPin2(String pin2, Message onComplete) {
        synchronized (mLock) {
            if (mUiccApplication != null) {
                mUiccApplication.supplyPin2(pin2, onComplete);
            } else if (onComplete != null) {
                // MTK-START
                // MTK flight mode power off modem feature and sim hot plug feature might cause
                // this API is called due to extremely time.
                // Return CommandException to ensure AP could handle the error message normally.
                //Exception e = new RuntimeException("ICC card is absent.");
                Exception e = CommandException.fromRilErrno(RILConstants.RADIO_NOT_AVAILABLE);
                mtkLog("Fail to supplyPin2, hasIccCard = " + hasIccCard());
                // MTK-END
                AsyncResult.forMessage(onComplete).exception = e;
                onComplete.sendToTarget();
                return;
            }
        }
    }

    @Override
    public void supplyPuk2(String puk2, String newPin2, Message onComplete) {
        synchronized (mLock) {
            if (mUiccApplication != null) {
                mUiccApplication.supplyPuk2(puk2, newPin2, onComplete);
            } else if (onComplete != null) {
                // MTK-START
                // MTK flight mode power off modem feature and sim hot plug feature might cause
                // this API is called due to extremely time.
                // Return CommandException to ensure AP could handle the error message normally.
                //Exception e = new RuntimeException("ICC card is absent.");
                Exception e = CommandException.fromRilErrno(RILConstants.RADIO_NOT_AVAILABLE);
                mtkLog("Fail to supplyPuk2, hasIccCard = " + hasIccCard());
                // MTK-END
                AsyncResult.forMessage(onComplete).exception = e;
                onComplete.sendToTarget();
                return;
            }
        }
    }

    @Override
    public void setIccLockEnabled(boolean enabled, String password, Message onComplete) {
        synchronized (mLock) {
            if (mUiccApplication != null) {
                mUiccApplication.setIccLockEnabled(enabled, password, onComplete);
            } else if (onComplete != null) {
                // MTK-START
                // MTK flight mode power off modem feature and sim hot plug feature might cause
                // this API is called due to extremely time.
                // Return CommandException to ensure AP could handle the error message normally.
                //Exception e = new RuntimeException("ICC card is absent.");
                Exception e = CommandException.fromRilErrno(RILConstants.RADIO_NOT_AVAILABLE);
                mtkLog("Fail to setIccLockEnabled, hasIccCard = " + hasIccCard());
                // MTK-END
                AsyncResult.forMessage(onComplete).exception = e;
                onComplete.sendToTarget();
                return;
            }
        }
    }

    @Override
    public void setIccFdnEnabled(boolean enabled, String password, Message onComplete) {
        synchronized (mLock) {
            if (mUiccApplication != null) {
                mUiccApplication.setIccFdnEnabled(enabled, password, onComplete);
            } else if (onComplete != null) {
                // MTK-START
                // MTK flight mode power off modem feature and sim hot plug feature might cause
                // this API is called due to extremely time.
                // Return CommandException to ensure AP could handle the error message normally.
                //Exception e = new RuntimeException("ICC card is absent.");
                Exception e = CommandException.fromRilErrno(RILConstants.RADIO_NOT_AVAILABLE);
                mtkLog("Fail to setIccFdnEnabled, hasIccCard = " + hasIccCard());
                // MTK-END
                AsyncResult.forMessage(onComplete).exception = e;
                onComplete.sendToTarget();
                return;
            }
        }
    }

    @Override
    public void changeIccLockPassword(String oldPassword, String newPassword, Message onComplete) {
        synchronized (mLock) {
            if (mUiccApplication != null) {
                mUiccApplication.changeIccLockPassword(oldPassword, newPassword, onComplete);
            } else if (onComplete != null) {
                // MTK-START
                // MTK flight mode power off modem feature and sim hot plug feature might cause
                // this API is called due to extremely time.
                // Return CommandException to ensure AP could handle the error message normally.
                //Exception e = new RuntimeException("ICC card is absent.");
                Exception e = CommandException.fromRilErrno(RILConstants.RADIO_NOT_AVAILABLE);
                mtkLog("Fail to changeIccLockPassword, hasIccCard = " + hasIccCard());
                // MTK-END
                AsyncResult.forMessage(onComplete).exception = e;
                onComplete.sendToTarget();
                return;
            }
        }
    }

    @Override
    public void changeIccFdnPassword(String oldPassword, String newPassword, Message onComplete) {
        synchronized (mLock) {
            if (mUiccApplication != null) {
                mUiccApplication.changeIccFdnPassword(oldPassword, newPassword, onComplete);
            } else if (onComplete != null) {
                // MTK-START
                // MTK flight mode power off modem feature and sim hot plug feature might cause
                // this API is called due to extremely time.
                // Return CommandException to ensure AP could handle the error message normally.
                //Exception e = new RuntimeException("ICC card is absent.");
                Exception e = CommandException.fromRilErrno(RILConstants.RADIO_NOT_AVAILABLE);
                mtkLog("Fail to changeIccFdnPassword, hasIccCard = " + hasIccCard());
                // MTK-END
                AsyncResult.forMessage(onComplete).exception = e;
                onComplete.sendToTarget();
                return;
            }
        }
    }

    @Override
    public boolean hasIccCard() {
        synchronized (mLock) {
            // MTK-START
            boolean isSimInsert = false;

            // To obtain correct status earily,
            // we use system property value to detemine sim inserted state.
            //if (isSimInsert == false) {
            String iccId = null;
            iccId = SystemProperties.get(PROPERTY_ICCID_SIM[mPhoneId]);

            //if (DBG) mtkLog("iccId = " + iccId);
            if ((iccId != null) && !(iccId.equals("")) &&
                    !(iccId.equals(ICCID_STRING_FOR_NO_SIM))) {
                isSimInsert = true;
            }
            //}

            if (isSimInsert == false && mUiccCard != null &&
                    mUiccCard.getCardState() != CardState.CARDSTATE_ABSENT) {
                isSimInsert = true;
            }

            if (DBG) {
                mtkLog("hasIccCard(): isSimInsert =  " + isSimInsert + " ,CardState = " +
                        ((mUiccCard != null) ? mUiccCard.getCardState() : "") +
                        ", iccId = " + SubscriptionInfo.givePrintableIccid(iccId));
            }

            return isSimInsert;
            // MTK-END
        }
    }

    // MTK-START: SIM ME LOCK
    /**
     * Query the SIM ME Lock type required to unlock.
     *
     * @return SIM ME Lock type
     */
    public PersoSubState getNetworkPersoType() {
        if (DBG) mtkLog("getNetworkPersoType E");
        synchronized (mLock) {
            if (mUiccApplication != null) {
                return mUiccApplication.getPersoSubState();
            }
            return PersoSubState.PERSOSUBSTATE_UNKNOWN;
        }
    }

    /**
     * Check whether ICC network lock is enabled
     * This is an async call which returns lock state to applications directly
     */
    public void queryIccNetworkLock(int category, Message onComplete) {
        if (DBG) mtkLog("queryIccNetworkLock(): category =  " + category);
        synchronized (mLock) {
            if (mUiccApplication != null) {
                ((MtkUiccCardApplication)mUiccApplication).queryIccNetworkLock(category,
                        onComplete);
            } else if (onComplete != null) {
                Exception e = CommandException.fromRilErrno(RILConstants.RADIO_NOT_AVAILABLE);
                mtkLog("Fail to queryIccNetworkLock, hasIccCard = " + hasIccCard());
                AsyncResult.forMessage(onComplete).exception = e;
                onComplete.sendToTarget();
                return;
            }
        }
    }

    /**
     * Set the ICC network lock enabled or disabled
     * When the operation is complete, onComplete will be sent to its handler
     */
    public void setIccNetworkLockEnabled(int category, int lockop, String password,
            String data_imsi, String gid1, String gid2, Message onComplete) {
        if (DBG) mtkLog("SetIccNetworkEnabled(): category = " + category
            + " lockop = " + lockop + " password = " + password
            + " data_imsi = " + data_imsi + " gid1 = " + gid1 + " gid2 = " + gid2);
        synchronized (mLock) {
            if (mUiccApplication != null) {
                ((MtkUiccCardApplication)mUiccApplication).setIccNetworkLockEnabled(
                    category, lockop, password, data_imsi, gid1, gid2, onComplete);
            } else if (onComplete != null) {
                Exception e = CommandException.fromRilErrno(RILConstants.RADIO_NOT_AVAILABLE);
                mtkLog("Fail to setIccNetworkLockEnabled, hasIccCard = " + hasIccCard());
                AsyncResult.forMessage(onComplete).exception = e;
                onComplete.sendToTarget();
                return;
            }
        }
    }
    // MTK-END

    // MTK-START: CMCC DUAL SIM DEPENDENCY LOCK
    /**
     * Used by SIM ME lock related enhancement feature(Modem SML change feature).
     */
    public void repollIccStateForModemSmlChangeFeatrue(boolean needIntent) {
        if (DBG) mtkLog("repollIccStateForModemSmlChangeFeatrue, needIntent = " + needIntent);
        synchronized (mLock) {
            ((MtkUiccController)mUiccController).repollIccStateForModemSmlChangeFeatrue(mPhoneId,
                    needIntent);
        }
    }
    // MTK-END

    // MTK-START
    // retrun usim property or use uicccardapplication app type
    public String getIccCardType() {
        synchronized (mLock) {
            if (mUiccCard != null && mUiccCard.getCardState() != CardState.CARDSTATE_ABSENT) {
                return ((MtkUiccCard)mUiccCard).getIccCardType();
            }
            return "";
        }
    }
    // MTK-START
    /**
     * Notifies handler in case of FDN changed
     */
    @Override
    public void registerForFdnChanged(Handler h, int what, Object obj) {
        synchronized (mLock) {
            synchronized (mLock) {
                Registrant r = new Registrant(h, what, obj);

                mFdnChangedRegistrants.add(r);

                if (getIccFdnEnabled()) {
                    r.notifyRegistrant();
                }
            }
        }
    }

    @Override
    public void unregisterForFdnChanged(Handler h) {
        synchronized (mLock) {
            mFdnChangedRegistrants.remove(h);
        }
    }

     /**
      *Check if card type is cdma only card.
      * @return true : cdma only card  false: not cdma only card
      */
     public boolean isCdmaOnly() {
        String property = null;
        String prop = null;
        String values[] = null;

        if (mPhoneId < 0 || mPhoneId >= PROPERTY_RIL_FULL_UICC_TYPE.length) {
            mtkLog("isCdmaOnly: invalid PhoneId " + mPhoneId);
            return false;
        }
        prop = SystemProperties.get(PROPERTY_RIL_FULL_UICC_TYPE[mPhoneId]);
        if ((prop != null) && (prop.length() > 0)) {
            values = prop.split(",");
        }
        mtkLog("isCdmaOnly PhoneId " + mPhoneId + ", prop value= " + prop +
                ", size= " + ((values != null) ? values.length : 0));
        if (values != null) {
            return !(Arrays.asList(values).contains("USIM")
                    || Arrays.asList(values).contains("SIM"));
        } else {
            return false;
        }
    }
    public void iccGetAtr(Message onComplete) {
        if (mUiccCard != null && mUiccCard.getCardState() != CardState.CARDSTATE_ABSENT) {
            if (mUiccCard instanceof MtkUiccCard) {
                ((MtkUiccCard)mUiccCard).iccGetAtr(onComplete);
            } else {
                loge("Not MtkUiccCard. Fail to call iccGetAtr");
            }
        }
    }
    // MTK-END

    protected void log(String s) {
        Rlog.d(LOG_TAG, s + " (slot " + mPhoneId + ")");
    }
    protected void loge(String msg) {
        Rlog.e(LOG_TAG, msg + " (slot " + mPhoneId + ")");
    }

    protected void mtkLog(String s) {
        Rlog.d(LOG_TAG_EX, s + " (slot " + mPhoneId + ")");
    }
    protected void mtkLoge(String msg) {
        Rlog.e(LOG_TAG, msg + " (slot " + mPhoneId + ")");
    }
}
