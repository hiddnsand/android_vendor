//#define LOG_TAG "FpsInfo"

#include <dlfcn.h>
#include <utils/String8.h>
#include <binder/Parcel.h>
#include <cutils/log.h>

#include "dfps/IFpsInfo.h"

namespace android {

class BpFpsInfo : public BpInterface<IFpsInfo> {
public:
    BpFpsInfo(const sp<IBinder>& impl) : BpInterface<IFpsInfo>(impl)
    {
    }

    virtual status_t cbGetFps(int32_t fps)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsInfo::getInterfaceDescriptor());
        data.writeInt32(fps);
        remote()->transact(INFO_CB_GET_FPS, data, &reply);
        return reply.readInt32();
    }
};

IMPLEMENT_META_INTERFACE(FpsInfo, "FpsInfo");

status_t BnFpsInfo::onTransact(uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags) {
    switch (code)
    {
        case INFO_CB_GET_FPS:
        {
            CHECK_INTERFACE(IFpsInfo, data, reply);
            int32_t fps = data.readInt32();
            status_t result = cbGetFps(fps);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
    }
    return BBinder::onTransact(code, data, reply, flags);
}

};
