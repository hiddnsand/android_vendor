LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_JAVA_LIBRARIES := ext guava

LOCAL_PACKAGE_NAME := ApplicationsProvider
LOCAL_CERTIFICATE := shared

OPTR:= $(word 1,$(subst _,$(space),$(OPTR_SPEC_SEG_DEF)))

ifeq ($(OPTR), OP01)
    include $(BUILD_PACKAGE)

    # Also build our test apk
    include $(call all-makefiles-under,$(LOCAL_PATH))
endif
