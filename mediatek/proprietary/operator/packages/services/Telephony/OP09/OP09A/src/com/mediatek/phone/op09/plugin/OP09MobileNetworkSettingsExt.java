package com.mediatek.phone.op09.plugin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.DialogInterface.OnKeyListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.ContentObserver;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionManager;
import android.telephony.SubscriptionInfo;
import android.telephony.TelephonyManager;
import android.telecom.TelecomManager;
import android.util.Log;
import android.view.KeyEvent;
import android.os.PersistableBundle;
import android.telephony.CarrierConfigManager;

import com.android.ims.ImsManager;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.TelephonyIntents;
import com.android.phone.PhoneGlobals;
import com.android.phone.PhoneUtils;
import com.mediatek.ims.internal.MtkImsManager;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.phone.ext.DefaultMobileNetworkSettingsExt;
import com.mediatek.phone.op09.plugin.R;
import com.mediatek.settings.Enhanced4GLteSwitchPreference;
import com.mediatek.settings.cdma.TelephonyUtilsEx;
import com.mediatek.telephony.MtkTelephonyManagerEx;
import com.mediatek.internal.telephony.MtkGsmCdmaPhone;
import com.mediatek.internal.telephony.MtkIccCardConstants;


import java.util.List;

/**
 * REMOVE ALL,just only TDD data only and APN.
 */
public class OP09MobileNetworkSettingsExt extends DefaultMobileNetworkSettingsExt {

    private static final String TAG = "OP09MobileNetworkSettingsExt";
    private static final String CT_APN_KEY = "pref_ct_apn_key";
    private static final String SINGLE_LTE_DATA = "single_lte_data";
    private static final String INTENT_ACTION_CARD_TYPE =
            TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED;
    private static final String PACKAGE_NAME = "com.mediatek.phone.op09.plugin";
    private static final String BUTTON_4G_LTE_KEY = "ct_enhanced_4g_lte";
    public static final String ACTION_RAT_CHANGED =
        "com.mediatek.intent.action.ACTION_RAT_CHANGED";
    private static Context mOP09TelephonyContext;
    private SwitchPreference mSwitchPreference;
    private IntentFilter mIntentFilter;
    private boolean mSwitching;
    private boolean mIsLteCardType;
    private static MtkTelephonyManagerEx mTelephonyManagerEx = null;

    // TODO:: Work around for ALPS01976671
    private boolean mIsRegister = false;
    private Enhanced4GLteSwitchPreference mEnhancedButton4glte;
    private Dialog mDialog;
    private PreferenceActivity mHostActivity = null;

    public OP09MobileNetworkSettingsExt(Context context) {
        mOP09TelephonyContext = context;
        mTelephonyManagerEx = MtkTelephonyManagerEx.getDefault();
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        Log.i(TAG, "OP09 onPreferenceTreeClick preference = " + preference);

        if (null != (PreferenceScreen) preferenceScreen.findPreference(CT_APN_KEY)
                && preference == (PreferenceScreen) preferenceScreen.findPreference(CT_APN_KEY)) {
            preference.getContext().startActivity(preference.getIntent());
            Log.i(TAG, "OP09 onPreferenceTreeClick preference key = CT_APN");
            return true;
        } else if ((preference == mSwitchPreference) ||
                    preference.getKey().equals(SINGLE_LTE_DATA)) {
            if (preference != mSwitchPreference) {
                Log.i(TAG, "SINGLE_LTE_DATA!!!!!");
            }

            Log.i(TAG, "OP09 onPreferenceTreeClick preference key = singleltedata");
            SwitchPreference swp = (SwitchPreference) preference;
            boolean isChecked = swp.isChecked();
            Log.i(TAG, "onPreferenceTreeClick isChecked = " + isChecked);
            try {
                int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
                Settings.Global.putInt(mOP09TelephonyContext.getContentResolver(),
                        Settings.Global.PREFERRED_NETWORK_MODE + subId,
                        (isChecked ? MtkGsmCdmaPhone.NT_MODE_LTE_TDD_ONLY
                                : Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA));
                switchSvlteRatMode(isChecked);
            } catch (RemoteException e) {
                Log.i(TAG, "op09 only lte data onPreferenceTreeClick RemoteException");
            }
            mSwitching = true;
            updateSwitch();
            return true;
        } else if ((preference == mEnhancedButton4glte) ||
                preference.getKey().equals(BUTTON_4G_LTE_KEY)){
            Log.i(TAG, "OP09 onPreferenceTreeClick preference key = mEnhancedButton4glte");
            return true;
        }
        return false;
    }

    private void switchSvlteRatMode(boolean isChecked) throws RemoteException {
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
        ITelephony telephony = ITelephony.Stub.asInterface(ServiceManager
                .getService(Context.TELEPHONY_SERVICE));

        if (null != telephony) {
            int networkType = (isChecked ? MtkGsmCdmaPhone.NT_MODE_LTE_TDD_ONLY :
                    Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
            Thread changeNetworkType = new Thread(new ChangeNetworkType(subId,
                                                                networkType,
                                                                mOP09TelephonyContext));
            changeNetworkType.start();
            
            Log.i(TAG, "switchSvlteRatMode isChecked = " + isChecked +
                    " set value is " + (isChecked ?
                    MtkGsmCdmaPhone.NT_MODE_LTE_TDD_ONLY :
                    Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA));
        }
    }

    @Override
    public void initOtherMobileNetworkSettings(PreferenceActivity activity, int subId) {
        PreferenceScreen prefScreen = activity.getPreferenceScreen();
        mHostActivity = activity;
        Log.d(TAG, "initOtherMobileNetworkSettings remove all");
        prefScreen.removeAll();
        if (!mIsRegister) {
            mIntentFilter = new IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED);
            mIntentFilter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
            mIntentFilter.addAction(ACTION_RAT_CHANGED);
            mIntentFilter.addAction(INTENT_ACTION_CARD_TYPE);
            mOP09TelephonyContext.registerReceiver(mReceiver, mIntentFilter);
            int subscriptionId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
            mOP09TelephonyContext.getContentResolver().registerContentObserver(
                    Settings.Global.getUriFor(Settings.Global.PREFERRED_NETWORK_MODE
                            + subscriptionId),
                    true, mDataConnectionObserver);
            mOP09TelephonyContext.getContentResolver().registerContentObserver(
                    Settings.Global.getUriFor(Settings.Global.ENHANCED_4G_MODE_ENABLED),
                    true, mContentObserver);

            List<SubscriptionInfo> si =  SubscriptionManager.from(mOP09TelephonyContext)
                .getActiveSubscriptionInfoList();

            if (si != null && si.size() > 0) {
                for (int i = 0; i < si.size(); i++) {
                    SubscriptionInfo subInfo = si.get(i);
                    subId = subInfo.getSubscriptionId();
                    mOP09TelephonyContext.getContentResolver().registerContentObserver(
                        Settings.Global.getUriFor(Settings.Global.MOBILE_DATA + subId),
                        true, mDataConnectionStateObserver);
                }
            }

        TelephonyManager telephonyManager =
            (TelephonyManager) mOP09TelephonyContext.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        telephonyManager.listen(mPhoneStateListener,
            PhoneStateListener.LISTEN_DATA_CONNECTION_STATE);
            telephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_SERVICE_STATE);
            mIsRegister = true;
        }
        /// add TDD data only feature
        int slotId = SubscriptionManager.getSlotIndex(subId);
        if (needLteSingleData() && ((slotId == PhoneConstants.SIM_ID_1)
            || useCtTestcard())) {
            Log.d(TAG, "initOtherMobileNetworkSettings add TDD data only feature" +
                    " with mIsRegister = " + mIsRegister);
            mSwitchPreference = new SwitchPreference(mOP09TelephonyContext);
            mSwitchPreference.setTitle(mOP09TelephonyContext.getString(R.string.only_use_LTE_data));
            mSwitchPreference.setKey(SINGLE_LTE_DATA);
            mSwitchPreference.setSummaryOn(
                mOP09TelephonyContext.getString(R.string.only_use_LTE_data_summary));
            mSwitchPreference.setSummaryOff(
                mOP09TelephonyContext.getString(R.string.only_use_LTE_data_summary));
            prefScreen.addPreference(mSwitchPreference);
        }

        boolean isVolteEnabledByPlatform = isVolteEnabled();
        Log.d(TAG, "initOtherMobileNetworkSettings add volte feature"
                + " with mIsRegister = " + mIsRegister
                + " isVolteEnabledByPlatform = " + isVolteEnabledByPlatform
                + " slotId = " + slotId);
        /// add CT VOLTE feature
        if (isVolteEnabledByPlatform
                && (slotId == PhoneConstants.SIM_ID_1)) {
            Log.d(TAG, "initOtherMobileNetworkSettings add volte feature done");
            mEnhancedButton4glte = new Enhanced4GLteSwitchPreference(mOP09TelephonyContext, subId);
            if (isWfcEnabled() ||
                    (TelephonyUtilsEx.isCtVolteEnabled()
                    && TelephonyUtilsEx.isCtSim(subId))) {
                mEnhancedButton4glte.setTitle(mOP09TelephonyContext.getResources()
                        .getText(R.string.wfc_volte_switch_title));
            } else {
                PersistableBundle carrierConfig =
                    PhoneGlobals.getInstance().getCarrierConfigForSubId(subId);
                    boolean useVariant4glteTitle = carrierConfig.getBoolean(
                            CarrierConfigManager.KEY_ENHANCED_4G_LTE_TITLE_VARIANT_BOOL);
                    int enhanced4glteModeTitleId = useVariant4glteTitle ?
                            R.string.enhanced_4g_lte_mode_title_variant :
                            R.string.enhanced_4g_lte_mode_title;
                    mEnhancedButton4glte.setTitle(mOP09TelephonyContext.getResources()
                            .getString(enhanced4glteModeTitleId));
                    mEnhancedButton4glte.setSummary(mOP09TelephonyContext
                            .getResources().getString(R.string.enhanced_4g_lte_mode_summary));
            }

            mEnhancedButton4glte.setKey(BUTTON_4G_LTE_KEY);
            mEnhancedButton4glte.setSubId(subId);
            prefScreen.addPreference(mEnhancedButton4glte);
            mEnhancedButton4glte.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    Log.i(TAG, "OP09 onPreferenceChange preference = " + preference);
                    if (preference.getKey().equals(BUTTON_4G_LTE_KEY)) {
                        Log.i(TAG, "OP09 onPreferenceChangeee preference = " + preference);
                        Enhanced4GLteSwitchPreference ltePref
                            = (Enhanced4GLteSwitchPreference) preference;
                        /// M: [CT VOLTE] @{
                        if (TelephonyUtilsEx.isCtVolteEnabled()
                                && TelephonyUtilsEx.isCtSim(
                                        MtkSubscriptionManager.getSubIdUsingPhoneId(
                                                PhoneConstants.SIM_ID_1))
                                && !mEnhancedButton4glte.isChecked()) {
                            int type = TelephonyManager.getDefault().getNetworkType(
                                    MtkSubscriptionManager
                                        .getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1));
                            Phone phone = PhoneFactory.getPhone(
                                    SubscriptionManager.getPhoneId(
                                            MtkSubscriptionManager.getSubIdUsingPhoneId(
                                                    PhoneConstants.SIM_ID_1)));
                            if (TelephonyManager.NETWORK_TYPE_LTE != type
                                    && !TelephonyUtilsEx.isRoaming(phone)) {
                                showVolteUnavailableDialog();
                                return true;
                            }
                        }
                        ltePref.setChecked(!ltePref.isChecked());
                        MtkImsManager.setEnhanced4gLteModeSetting(mOP09TelephonyContext,
                                ltePref.isChecked(), PhoneConstants.SIM_ID_1);
                        return true;
                    }
                    return false;
                }
            });
            updateVolteSwitch();
        }

        /// add CT APN feature
        Log.d(TAG, "initOtherMobileNetworkSettings add CT APN");
        PreferenceScreen prefAPN = new PreferenceScreen(mOP09TelephonyContext, null);
        prefAPN.setKey(CT_APN_KEY);
        prefAPN.setTitle(mOP09TelephonyContext.getResources().getText(R.string.apn_settings));
        prefAPN.setPersistent(false);
        Intent targetIntent = new Intent(Settings.ACTION_APN_SETTINGS);
        targetIntent.putExtra("sub_id", subId);
        prefAPN.setIntent(targetIntent);
        prefScreen.addPreference(prefAPN);
        updateSwitch();
    }

    /// udpate the VOLTE switch state.
    private void updateVolteSwitch() {
        Log.d(TAG, "updateVolteSwitch mEnhancedButton4glte = " + mEnhancedButton4glte);
        if (null != mEnhancedButton4glte) {
            int isChecked = Settings.Global.getInt(mOP09TelephonyContext.getContentResolver(),
                    Settings.Global.ENHANCED_4G_MODE_ENABLED, 0);
            mEnhancedButton4glte.setChecked(isChecked == 1);
            boolean inCall = TelecomManager.from(mOP09TelephonyContext).isInCall();
            boolean nontty = MtkImsManager.isNonTtyOrTtyOnVolteEnabled(
                    mOP09TelephonyContext, PhoneConstants.SIM_ID_1);

            /// M: [CT VOLTE] @{
            boolean enableForCtVolte = true;
            int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
            if (TelephonyUtilsEx.isCtVolteEnabled() && TelephonyUtilsEx.isCtSim(subId)) {
                int settingsNetworkMode = android.provider.Settings.Global.getInt(
                        mOP09TelephonyContext.getContentResolver(),
                        android.provider.Settings.Global.PREFERRED_NETWORK_MODE + subId,
                        Phone.PREFERRED_NT_MODE);
                enableForCtVolte = TelephonyUtilsEx.isCt4gSim(subId)
                        && settingsNetworkMode == Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA;
            }
            /// @}
            Log.d(TAG, "updateVolteSwitch isChecked = " + isChecked
                    + " inCall = " + inCall
                    + " nontty = " + nontty
                    + " subId = " + subId
                    + " enableForCtVolte = " + enableForCtVolte);
            mEnhancedButton4glte.setEnabled(!inCall && nontty && enableForCtVolte);
        }
    }

    //TODO: fix build error, should add API in plugin interface @Override
    @Override
    public void unRegister() {
        Log.d(TAG, "unRegister mIsRegister = " + mIsRegister);
        if (mIsRegister) {
            mOP09TelephonyContext.unregisterReceiver(mReceiver);
            mOP09TelephonyContext.getContentResolver().unregisterContentObserver(mDataConnectionObserver);
            mOP09TelephonyContext.getContentResolver().unregisterContentObserver(mContentObserver);
            mOP09TelephonyContext.getContentResolver().unregisterContentObserver(
                mDataConnectionStateObserver);
            TelephonyManager.getDefault().listen(
                mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
            mIsRegister = false;
        }
    }

    /**
     * Resume the plugin.
     * @return void.
     */
    public void onResume() {
        mSwitching = false;
        updateSwitch();
        updateVolteSwitch();

    }

    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            Log.d(TAG, "PhoneStateListener, onCallStateChanged new state=" + state);
            updateSwitch();
            updateVolteSwitch();
        }

        @Override
        public void onDataConnectionStateChanged(int state) {
            super.onDataConnectionStateChanged(state);
            Log.d(TAG, "PhoneStateListener, onDataConnectionStateChanged new state=" + state);
            updateSwitch();
            updateVolteSwitch();
        }

    };

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i(TAG, "on receive broadcast action = " + action);
            if (action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
                Log.i(TAG, "action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED) udpateSwitch");
                updateSwitch();
                updateVolteSwitch();
            } else if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(action)) {
                int slotId = intent.getIntExtra(PhoneConstants.SLOT_KEY, 0);
                if (slotId == PhoneConstants.SIM_ID_1) {
                    Log.i(TAG, "TelephonyIntents.ACTION_SIM_STATE_CHANGED udpateSwitch");
                    updateSwitch();
                    updateVolteSwitch();
                }
            } else if (ACTION_RAT_CHANGED.equals(action)) {
                mSwitching = false;
                updateSwitch();
                updateVolteSwitch();

            } else if (TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED.equals(intent.getAction())) {
                MtkIccCardConstants.CardType cardType = mTelephonyManagerEx.getCdmaCardType(PhoneConstants.SIM_ID_1);
                if (cardType.equals(MtkIccCardConstants.CardType.CT_4G_UICC_CARD)) {
                    mIsLteCardType = true;
                } else {
                    mIsLteCardType = false;
                }

                Log.i(TAG, "intent cardType = " + cardType.toString()
                        + ", mIsLteCardType " + mIsLteCardType);
                updateSwitch();
                updateVolteSwitch();
            }
        }
    };

    private ContentObserver mDataConnectionObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            Log.d(TAG, "onChange selfChange=" + selfChange);
            if (!selfChange) {
                updateSwitch();
                updateVolteSwitch();
            }
        }
    };

    private ContentObserver mDataConnectionStateObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            Log.d(TAG, "mDataConnectionState onChange selfChange=" + selfChange);
            if (!selfChange) {
                updateSwitch();
                updateVolteSwitch();
            }
        }
    };
    private ContentObserver mContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            Log.d(TAG, "volteObserver onChange selfChange=" + selfChange);
            if (!selfChange) {
                updateSwitch();
                updateVolteSwitch();
            }
        }
    };
    private void updateSwitch() {
        if (mSwitchPreference != null) {
            int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
            int patternLteDataOnly = Settings.Global.getInt(
                    mOP09TelephonyContext.getContentResolver(),
                    Settings.Global.PREFERRED_NETWORK_MODE + subId,
                    Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
            mSwitchPreference.setChecked(
                    patternLteDataOnly == MtkGsmCdmaPhone.NT_MODE_LTE_TDD_ONLY);
            boolean simInserted = isSIMInserted(PhoneConstants.SIM_ID_1);
            boolean airPlaneMode = isAirPlaneMode();
            boolean callStateIdle = isCallStateIDLE();
            boolean simStateIsReady = isSimStateReady(mOP09TelephonyContext, PhoneConstants.SIM_ID_1);
            /// if data close && default data is not sim one, tdd data only can not choose
            boolean dataEnable = TelephonyManager.from(mOP09TelephonyContext).getDataEnabled();
            int slotId = SubscriptionManager.getSlotIndex(
                    SubscriptionManager.getDefaultDataSubscriptionId());
            boolean isCTEnableData = (PhoneConstants.SIM_ID_1 == slotId) && dataEnable;
            Phone phone = PhoneFactory.getPhone(
                    SubscriptionManager.getPhoneId(
                            MtkSubscriptionManager.getSubIdUsingPhoneId(
                                    PhoneConstants.SIM_ID_1)));
            boolean roamingEnable  = TelephonyUtilsEx.isRoaming(phone);
            /// [CT VOLTE]
            boolean isCtVolteOn = TelephonyUtilsEx.isCtVolteEnabled()
                    && TelephonyUtilsEx.isCt4gSim(subId)
                    && MtkImsManager.isEnhanced4gLteModeSettingEnabledByUser(
                            mOP09TelephonyContext, PhoneConstants.SIM_ID_1);
            Log.d(TAG, "updateSwitch() simInserted=" + simInserted + ", airPlaneMode="
                    + airPlaneMode
                    + ", callStateIdle=" + callStateIdle
                    + ", dataEnable = " + dataEnable
                    + ", slotId = " + slotId + " isCTEnableData = " + isCTEnableData
                    + ", patternLteDataOnly = " + patternLteDataOnly
                    + ", mIsLteCardType = " + mIsLteCardType
                    + ", simStateIsReady = " + simStateIsReady
                    + ", roamingEnable = " + roamingEnable
                    + ", mSwitching = " + mSwitching
                    + ", isCtVolteOn = " + isCtVolteOn);
            Log.d(TAG, "updata information ");
            if ((simInserted && !isAirPlaneMode() && mIsLteCardType
                && isCallStateIDLE() && isCTEnableData && simStateIsReady
                && (patternLteDataOnly != Phone.NT_MODE_GLOBAL)
                && !roamingEnable && !isCtVolteOn)
                || useCtTestcard()) {
                mSwitchPreference.setEnabled(true);
            } else {
                mSwitchPreference.setEnabled(false);
            }
        }
    }

    ///return true if sim one is insert
    private boolean isSIMInserted(int slotId) {
        try {
            ITelephony tmex = ITelephony.Stub.asInterface(android.os.ServiceManager
                    .getService(Context.TELEPHONY_SERVICE));
            Log.i(TAG, "isSIMInserted = " + (tmex != null && tmex.hasIccCardUsingSlotIndex(slotId)));
            return (tmex != null && tmex.hasIccCardUsingSlotIndex(slotId));
        } catch (RemoteException e) {
            Log.i(TAG, "isSIMInserted = false because RemoteException");
            return false;
        }
    }

    ///return true if air plane mode on
    private boolean isAirPlaneMode() {
        Log.i(TAG, "isAirPlaneMode = " + (Settings.System.getInt(mOP09TelephonyContext.getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, -1) == 1));
        return Settings.System.getInt(mOP09TelephonyContext.getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, -1) == 1;
    }

    private boolean isTargetSimRadioOn(int simId) {
        int[] targetSubId = SubscriptionManager.getSubId(simId);
        if (targetSubId != null && targetSubId.length > 0) {
            for (int i = 0; i < targetSubId.length; i++) {
               if (isTargetSlotRadioOn(targetSubId[i])) {
                   Log.i(TAG, "isTargetSimRadioOn true simId = " + simId);
                   return true;
               }
            }
            Log.i(TAG, "isTargetSimRadioOn false simId = " + simId);
            return false;
        } else {
            Log.i(TAG, "isTargetSimRadioOn false because  targetSubId[] = null" +
                " or targetSubId[].length is 0  simId = " + simId);
            return false;
        }
    }

    static boolean isTargetSlotRadioOn(int subId) {
        boolean radioOn = true;
        try {
            ITelephony iTel = ITelephony.Stub.asInterface(
                    ServiceManager.getService(Context.TELEPHONY_SERVICE));
            if (null == iTel) {
                Log.i(TAG, "isTargetSlotRadioOn = false because iTel = null");
                return false;
            }
            Log.i(TAG, "isTargetSlotRadioOn = " + iTel.isRadioOnForSubscriber(subId, PACKAGE_NAME));
            radioOn = iTel.isRadioOnForSubscriber(subId, PACKAGE_NAME);
        } catch (RemoteException ex) {
            radioOn = false;
            Log.i(TAG, "isTargetSlotRadioOn radioOn = false because catch RemoteException");
        }
        Log.i(TAG, "isTargetSlotRadioOn radioOn = " + radioOn);
        return radioOn;
    }

    /**
     * judge if sim state is ready,use in updateSwitch.
     * sim state:SIM_STATE_UNKNOWN = 0;SIM_STATE_ABSENT = 1
     * SIM_STATE_PIN_REQUIRED = 2;SIM_STATE_PUK_REQUIRED = 3;
     * SIM_STATE_NETWORK_LOCKED = 4;SIM_STATE_READY = 5;
     * SIM_STATE_CARD_IO_ERROR = 6;
     * @param context Context
     * @param simId sim id
     * @return true if is SIM_STATE_READY
     */
    private boolean isSimStateReady(int simId) {
        TelephonyManager mTelephonyManager = TelephonyManager.from(mOP09TelephonyContext);
        Log.i(TAG, "isSimStateReady = " +
                mTelephonyManager.getSimState(simId) +
                ", sim_state_ready == 5");
        return mTelephonyManager.getSimState(simId) == TelephonyManager.SIM_STATE_READY;
    }

    private boolean isCallStateIDLE() {
        TelephonyManager telephonyManager =
            (TelephonyManager) mOP09TelephonyContext.getSystemService(Context.TELEPHONY_SERVICE);
        int currPhoneCallState = telephonyManager.getCallState();
        Log.i(TAG, "op09 mobile isCallStateIDLE = " +
                (currPhoneCallState == TelephonyManager.CALL_STATE_IDLE));
        return currPhoneCallState == TelephonyManager.CALL_STATE_IDLE;
    }

    private boolean useCtTestcard() {
        return Utils.isCTLteTddTestSupport();
    }

    private boolean needLteSingleData() {
        boolean dataOnly = SystemProperties.get("ro.mtk_tdd_data_only_support").equals("1");
        Log.i(TAG, "needLteSingleData, dataOnly" + dataOnly);

        return dataOnly;
    }

    /**
     * judge if sim state is ready.
     * sim state:SIM_STATE_UNKNOWN = 0;SIM_STATE_ABSENT = 1
     * SIM_STATE_PIN_REQUIRED = 2;SIM_STATE_PUK_REQUIRED = 3;
     * SIM_STATE_NETWORK_LOCKED = 4;SIM_STATE_READY = 5;
     * SIM_STATE_CARD_IO_ERROR = 6;
     * @param context Context
     * @param simId sim id
     * @return true if is SIM_STATE_READY
     */
    static boolean isSimStateReady(Context context, int simId) {
        int simState = TelephonyManager.from(context).getSimState(simId);
        Log.i(TAG, "isSimStateReady simState=" + simState);
        return simState == TelephonyManager.SIM_STATE_READY;
    }

    /**
     * Judge if it is CT Plugin.
     * @return true if it it CT Plugin.
     */
    @Override
    public boolean isCtPlugin() {
        Log.i(TAG, "OP09 isCtPlugin  = true");
        return true;
    }

    @Override
    public void onPreferenceChange(Preference preference, Object objValue) {
        Log.i(TAG, "OP09 onPreferenceChange preference = " + preference);

        if (preference.getKey().equals(BUTTON_4G_LTE_KEY)) {
            Log.i(TAG, "OP09 onPreferenceChangeee preference = " + preference);
            Enhanced4GLteSwitchPreference ltePref = (Enhanced4GLteSwitchPreference) preference;
            /// M: [CT VOLTE] @{
            if (TelephonyUtilsEx.isCtVolteEnabled()
                    && TelephonyUtilsEx.isCtSim(
                            MtkSubscriptionManager.getSubIdUsingPhoneId(
                                    PhoneConstants.SIM_ID_1))
                    && !mEnhancedButton4glte.isChecked()) {
                int type = TelephonyManager.getDefault()
                    .getNetworkType(MtkSubscriptionManager
                            .getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1));
                Phone phone = PhoneFactory.getPhone(
                        SubscriptionManager.getPhoneId(
                                MtkSubscriptionManager.getSubIdUsingPhoneId(
                                        PhoneConstants.SIM_ID_1)));
                if (TelephonyManager.NETWORK_TYPE_LTE != type
                        && !TelephonyUtilsEx.isRoaming(phone)) {
                    showVolteUnavailableDialog();
                    return;
                }
            }
            ltePref.setChecked(!ltePref.isChecked());
            MtkImsManager.setEnhanced4gLteModeSetting(
                    mOP09TelephonyContext,
                    ltePref.isChecked(),
                    PhoneConstants.SIM_ID_1);
        }
    }
    /**
     * [CT VOLTE]When network type is not LTE, show dialog.
     */
    private void showVolteUnavailableDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mHostActivity);
        String title = mOP09TelephonyContext.getString(R.string.alert_ct_volte_unavailable,
                PhoneUtils.getSubDisplayName(
                        MtkSubscriptionManager.getSubIdUsingPhoneId(
                                PhoneConstants.SIM_ID_1)));
        Dialog dialog = builder.setMessage(title).setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mEnhancedButton4glte.setChecked(!mEnhancedButton4glte.isChecked());
                    }
                }).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mEnhancedButton4glte.setChecked(mEnhancedButton4glte.isChecked());
                MtkImsManager.setEnhanced4gLteModeSetting(mOP09TelephonyContext,
                        mEnhancedButton4glte.isChecked(), PhoneConstants.SIM_ID_1);
            }
        }).create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (KeyEvent.KEYCODE_BACK == keyCode) {
                    if (null != dialog) {
                        mEnhancedButton4glte.setChecked(!mEnhancedButton4glte.isChecked());
                        dialog.dismiss();
                        return true;
                    }
                }
                return false;
            }
        });
        mDialog = dialog;
        dialog.show();
    }

    private boolean isVolteEnabled() {
        boolean isVolteEnabled = ImsManager.isVolteEnabledByPlatform(mOP09TelephonyContext);
        if (SystemProperties.getInt("persist.mtk_mims_support", 1) > 1) {
            isVolteEnabled = ImsManager.isVolteEnabledByPlatform(mOP09TelephonyContext, PhoneConstants.SIM_ID_1);
        }
        return isVolteEnabled;
    }

    private boolean isWfcEnabled() {
        boolean isWfcEnabled = ImsManager.isWfcEnabledByPlatform(mOP09TelephonyContext);
        if (SystemProperties.getInt("persist.mtk_mims_support", 1) > 1) {
            isWfcEnabled = ImsManager.isWfcEnabledByPlatform(mOP09TelephonyContext, PhoneConstants.SIM_ID_1);
        }
        return isWfcEnabled;
    }
}
