/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/** \file
 * A3M Motion blur renderer
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/motionblurrenderer.h> /* This class's API */
#include <a3m/renderdevice.h>       /* To query the viewport dimensions */
#include <a3m/rendertarget.h>       /* RenderTarget class */
#include <a3m/appearance.h>         /* For appearance class */
#include <a3m/assetcachepool.h>     /* For asset cache pool class */
#include <a3m/camera.h>             /* For camera class */
#include <a3m/solid.h>              /* For solid class */
#include <a3m/sceneutility.h>       /* For visitScene() */
#include <a3m/properties.h>         /* properties namespace */
#include <a3m/noncopyable.h>        /* For NonCopyable */

using namespace a3m;
using namespace properties;

namespace
{
  /* These numbers should correspond to their equivalent definition in the
   * shaders. Any joint over and above the maximum value will be ignored by
   * the shader. */
  const A3M_INT32 MAX_JOINTS = 20;

  typedef std::vector< Solid* > RenderList;

  void renderSolids( RenderContext& context,
                     RenderList const& renderList,
                     Matrix4f const& viewProjection,
                     Matrix4f const& lastViewProjection,
                     a3m::Appearance& appearance,
                     a3m::Appearance& skinnedAppearance )
  {
    context.getProperty( T_VIEW_PROJECTION )->setValue( viewProjection );

    for( RenderList::const_iterator solid = renderList.begin(),
         endSolid = renderList.end();
         solid != endSolid; ++solid )
    {
      Matrix4f const& model = (*solid)->getWorldTransform();
      Matrix3f normalTransform = inverseTranspose(
                                   Matrix3f( Vector3f( model.i ),
                                             Vector3f( model.j ),
                                             Vector3f( model.k ) ) );

      // Set per-solid uniforms
      Matrix4f oldModelViewProjection = lastViewProjection *
                                        (*solid)->getLastWorldTransform();
      Matrix4f modelViewProjection = viewProjection * model;
      context.getProperty( T_OLD_MODEL_VIEW_PROJECTION )->setValue(
        oldModelViewProjection );
      context.getProperty( T_MODEL_VIEW_PROJECTION )->setValue( modelViewProjection );

      if( (*solid)->getJointsPerVertex() == 0 )
      {
        (*solid)->draw(context, appearance);
      }
      else
      {
        (*solid)->draw(context, skinnedAppearance);
      }
    }
  }

}

namespace a3m
{
  MotionBlurRenderer::MotionBlurRenderer(
    RenderContext::Ptr const& context, AssetCachePool::Ptr const& pool) :
    m_context(context),
    m_pool(pool),
    m_time(0.f),
    m_lastTime(0.f),
    m_appearance( new Appearance ),
    m_skinnedAppearance( new Appearance )
  {
    m_appearance->setShaderProgram(
      m_pool->shaderProgramCache()->get( "a3m#gen_velocity.sp" ) );
    m_appearance->setShaderProgram(
      m_pool->shaderProgramCache()->get( "a3m#gen_velocity.sp$SKIN" ) );
    // Register all uniforms defined by the renderer
    // We must do this to ensure that all properties are linked to the
    // shader programs successfully.

    addProperty(*m_context, 0, J_COUNT);
    addPropertyArray<Matrix4f>(*m_context, MAX_JOINTS, J_WORLD);

    addProperty(*m_context, Matrix4f::IDENTITY, T_VIEW_PROJECTION);
    addProperty(*m_context, Matrix4f::IDENTITY, T_OLD_MODEL_VIEW_PROJECTION);
    addProperty(*m_context, Matrix4f::IDENTITY, T_MODEL_VIEW_PROJECTION);

    addProperty(*m_context, 0.0f, TIME);
    addProperty(*m_context, 0.0f, DELTA_TIME);

    addProperty(*m_context, 0.0f, MOTION_BLUR_FACTOR);
  }

  void MotionBlurRenderer::render( Camera* camera,
                                   SceneNode& node,
                                   FlagMask const& renderFlags,
                                   FlagMask const& recursiveFlags )
  {
    m_solidsToRender.clear();

    // Record whether we are using a stereoscopic camera
    A3M_BOOL stereo = camera ? camera->isStereo() : A3M_FALSE;

    // Collect all the solids we need to render.
    // We AND the render and recursive flags to ensure that recursive flags are
    // a subset of render flags.
    visitScene(*this, node, renderFlags, renderFlags & recursiveFlags);

    const A3M_INT32 vpWidth = m_context->getViewportWidth();
    const A3M_INT32 vpHeight = m_context->getViewportHeight();
    const A3M_INT32 vpX = m_context->getViewportLeft();
    const A3M_INT32 vpY = m_context->getViewportBottom();

    const A3M_FLOAT aspect =
      static_cast<A3M_FLOAT>(vpWidth) /
      static_cast<A3M_FLOAT>(vpHeight);

    // Projection and transformation matrices for left/right cameras.
    // Use the "left" one (which will be central) in the mono case.

    Matrix4f camProjection[2]; // Stereo is intrinsically '2' cameras
    Matrix4f camPosTransform[2];
    A3M_INT16 eyeCount = 1;

    /* Get the camera's transform matrices. If there is no camera, then the
     * matrices will remain set to the identity matrix.
     */
    if( stereo )
    {
      camera->getStereoProjection(
        &camProjection[0], &camProjection[1], aspect );
      camera->getStereoWorldTransform(
        &camPosTransform[0], &camPosTransform[1] );
      eyeCount = 2;
    }
    else if( camera )
    {
      camera->getProjection( &camProjection[0], aspect );
      camPosTransform[0] = camera->getWorldTransform();
    }

    // Set per-render uniforms
    m_context->getProperty( TIME ) ->setValue(a3m::fmod( m_time, 60.f ) );
    A3M_FLOAT delta = m_time - m_lastTime;
    m_context->getProperty( DELTA_TIME )->setValue(
      delta == 0.f ? 1.f : (1.f / 60.f) / delta );
    m_lastTime = m_time;

    for( A3M_INT16 eye = 0; eye < eyeCount; eye++ )
    {
      if( stereo )
      {
        // Assuming a side-by-side mode as on the Bird phone, using one
        // framebuffer.  If other modes evolve (like really using two buffers)
        // then we will need to react differently here.
        // Use of vpX and vpY are 'on principle' - this whole scheme is going to
        // have strange effects unless vpX is zero.

        m_context->setViewport( vpX + (eye * vpWidth / 2), vpY,
                                vpWidth / 2, vpHeight );
      }

      Matrix4f viewProjection =
        camProjection[eye] * inverse( camPosTransform[eye] );

      renderSolids( *m_context, m_solidsToRender, viewProjection,
                    m_lastViewProjection[eye], *m_appearance,
                    *m_skinnedAppearance );

      m_lastViewProjection[eye] = viewProjection;
    }

    if( stereo )
    {
      // Recover original viewport set-up
      m_context->setViewport( vpX, vpY, vpWidth, vpHeight );
    }

    // Record current position of all solids to calulate their velocity
    // the next frame.
    for( RenderList::const_iterator solid = m_solidsToRender.begin(),
         endSolid = m_solidsToRender.end();
         solid != endSolid; ++solid )
    {
      (*solid)->updateMotionData();
    }

  }

  /*
   * Add the given solid to the list of things to be rendered.
   */
  void MotionBlurRenderer::visit( Solid* solid )
  {
    m_solidsToRender.push_back( solid );
  }

} /* namespace a3m */
