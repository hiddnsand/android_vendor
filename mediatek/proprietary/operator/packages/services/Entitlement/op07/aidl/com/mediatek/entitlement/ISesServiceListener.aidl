package com.mediatek.entitlement;

import android.os.Bundle;

interface ISesServiceListener {

    /**
     * callback when service has any events happened.
     *
     * @param service string to identify a service
     * @param event, generally could be:
     *         "not-entitled", "entitled", "failed", "pending"
     * @param extras pass extra information in a bundle.
     *         (e.g. {"tc-accepted": false, "emergency-address-filled": false} for 'pending',
     *               {"error-code": '1111'} for 'failed').
     */
    void onEntitlementEvent(String service, String event, in Bundle extras);

    /**
     * callback if needing to show a Websheet for entitlement procedure.
     *
     * Note: Entitlement service will always allow calling this method. UI needs to ensure
     *        to show no more than one Websheet.
     *
     * @param url string of the Websheet server url.
     * @param postData string of the post-data for websheet.
     */
    void onWebsheetPost(String url, String postData);
}
