/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M FlagMask interface.
 */

package com.mediatek.j3m;

/*!
 * \ingroup a3mJusrScenenodes
 * @{
 */
/**
 * FlagMask class for filtering objects containing flags.
 * Flag masks are used to filter objects which contain flags.  SceneNodes
 * contain flags, which act as general purpose switches for things like
 * visibility and shadow casting.  A flag is uniquely identified by its
 * index, and has a default state, which is the state in which the
 * corresponding flag within an object will be by default.
 */
public interface FlagMask {
    /**
     * Unary flag inversion operation.
     * Rather than inverting the flags themselves, which wouldn't make sense,
     * as the mask does not know the default state of flags which are not set,
     * inverting the mask inverts the default state of each of the set flags.
     *
     * @return Result of operation
     */
    FlagMask inverse();

    /**
     * Flag-wise AND operation.
     * Result will contain only flags which exist in both masks.
     * @param other Mask with which to AND this mask
     *
     * @return Result of operation
     */
    FlagMask and (FlagMask other);

    /**
     * Flag-wise OR operation.
     * Result will contain flags which exist in either mask.
     * @param other Mask with which to OR this mask
     *
     * @return Result of operation
     */
    FlagMask or (FlagMask other);

    /**
     * Flag-wise XOR operation.
     * Result will contain flags which exist in only one of the masks.
     * @param other Mask with which to XOR this mask
     *
     * @return Result of operation
     */
    FlagMask xor(FlagMask other);
}

/*! @} */
