#define DEBUG_LOG_TAG "OVL"

#include <stdlib.h>

#include <hardware/hwcomposer.h>

#include "utils/debug.h"
#include "utils/tools.h"

#include "overlay.h"
#include "dispatcher.h"
#include "display.h"
#include "hwdev.h"
#include "queue.h"
#include "composer.h"
#include "bliter.h"
#include "sync.h"

#define OLOGD(x, ...) HWC_LOGD("(%d) " x, m_disp_id, ##__VA_ARGS__)
#define OLOGI(x, ...) HWC_LOGI("(%d) " x, m_disp_id, ##__VA_ARGS__)
#define OLOGW(x, ...) HWC_LOGW("(%d) " x, m_disp_id, ##__VA_ARGS__)
#define OLOGE(x, ...) HWC_LOGE("(%d) " x, m_disp_id, ##__VA_ARGS__)

// ---------------------------------------------------------------------------

OverlayEngine::OverlayInput::OverlayInput()
    : connected_state(OVL_PORT_DISABLE)
    , connected_type(OVL_INPUT_NONE)
    , queue(NULL)
{ }

OverlayEngine::OverlayOutput::OverlayOutput()
    : connected_state(OVL_PORT_DISABLE)
{ }

OverlayEngine::OverlayEngine(int dpy)
    : m_disp_id(dpy)
{
    // create overlay session
    status_t err = DispDevice::getInstance().createOverlaySession(m_disp_id);

    if (err != NO_ERROR)
    {
        m_state = OVL_ENGINE_DISABLED;
        m_max_inputs = 0;

        OLOGE("Failed to create display session");
    }
    else
    {
        m_state = OVL_ENGINE_ENABLED;
        m_max_inputs = DispDevice::getInstance().getMaxOverlayInputNum();

        int avail_inputs = m_max_inputs;
        if (HWC_DISPLAY_PRIMARY == m_disp_id)
        {
            avail_inputs = DispDevice::getInstance().getAvailableOverlayInput(m_disp_id);
            DispDevice::getInstance().initOverlay();
        }

        for (int id = 0; id < m_max_inputs; id++)
        {
            // init input configurations
            OverlayInput* input = new OverlayInput();
            if (id >= avail_inputs)
            {
                input->connected_state = OVL_PORT_ENABLE;
                input->connected_type  = OVL_INPUT_UNKNOWN;
            }
            input->front_queues = &input->outdated_queues[0];
            input->back_queues = &input->outdated_queues[1];
            m_inputs.add(input);
            m_input_params.add(&input->param);
        }
    }
}

OverlayEngine::~OverlayEngine()
{
    for (int id = 0; id < m_max_inputs; id++)
    {
        delete m_inputs[id];
    }
    m_inputs.clear();
    m_input_params.clear();

    DispDevice::getInstance().destroyOverlaySession(m_disp_id);
}

int OverlayEngine::getAvailableInputNum()
{
    int avail_inputs = DispDevice::getInstance().getAvailableOverlayInput(m_disp_id);

    // only main display need to check if AEE exists
    if (HWC_DISPLAY_PRIMARY == m_disp_id)
    {
        int fake_num = DisplayManager::getInstance().getFakeDispNum();
        int num = avail_inputs - fake_num;

        for (int id = num; id < m_max_inputs; id++)
            m_input_params[id]->state = OVL_IN_PARAM_IGNORE;

        return num;
    }

    return avail_inputs;
}

status_t OverlayEngine::prepareInput(OverlayPrepareParam& param)
{
    AutoMutex l(m_lock);

    int id = param.id;

    if (id >= m_max_inputs)
    {
        OLOGE("Failed to prepare invalid overlay input(%d)", id);
        return BAD_INDEX;
    }

    DispDevice::getInstance().prepareOverlayInput(m_disp_id, &param);

    return NO_ERROR;
}

void OverlayEngine::keepOutdatedQueue(int id)
{
    OverlayInput* input = m_inputs[id];

    AutoMutex l(input->outdated_queues_lock);

    if (input->queue != NULL)
    {
        input->back_queues->add(input->queue);
        input->queue = NULL;
    }
}

void OverlayEngine::clearOutdatedQueue(int id)
{
    OverlayInput* input = m_inputs[id];

    AutoMutex l(input->outdated_queues_lock);

    // swap front/back queues
    if (input->back_queues->size() != 0)
    {
        Vector< sp<DisplayBufferQueue> > *tmp_queues;
        tmp_queues = input->front_queues;
        input->front_queues = input->back_queues;
        input->back_queues = tmp_queues;
    }

    // clear outdated queues
    if (input->front_queues->size() != 0)
    {
        OLOGD("Clear outdated queue");
        input->front_queues->clear();
    }
}

status_t OverlayEngine::setInputQueue(int id, sp<DisplayBufferQueue> queue)
{
    AutoMutex l(m_lock);

    if (id >= m_max_inputs)
    {
        OLOGE("Failed to set invalid overlay input(%d)", id);
        return BAD_INDEX;
    }

    if (OVL_INPUT_QUEUE == m_inputs[id]->connected_type)
    {
        OLOGW("Already set overlay input(%d)", id);
        return BAD_INDEX;
    }

    struct InputListener : public DisplayBufferQueue::ConsumerListener
    {
        InputListener(sp<OverlayEngine> ovl, int id)
            : m_engine(ovl)
            , m_id(id)
        { }
    private:
        sp<OverlayEngine> m_engine;
        int m_id;
        virtual void onBufferQueued()
        {
            m_engine->updateInput(m_id);
        }
    };
    queue->setConsumerListener(new InputListener(this, id));

    m_inputs[id]->queue = queue;
    m_inputs[id]->connected_state = OVL_PORT_ENABLE;
    m_inputs[id]->connected_type = OVL_INPUT_QUEUE;

    return NO_ERROR;
}

status_t OverlayEngine::setInputDirect(int id, OverlayPortParam* param)
{
    AutoMutex l(m_lock);

    if (id >= m_max_inputs)
    {
        OLOGE("Failed to set invalid overlay input(%d)", id);
        return BAD_INDEX;
    }

    if (OVL_INPUT_QUEUE == m_inputs[id]->connected_type)
    {
        OLOGI("Overlay input(%d) was used with queue previously", id);
    }

    // keep outdated queue
    keepOutdatedQueue(id);

    m_inputs[id]->connected_state = OVL_PORT_ENABLE;
    m_inputs[id]->connected_type = OVL_INPUT_DIRECT;

    if (param != NULL)
    {
        m_input_params[id]->state = OVL_IN_PARAM_IGNORE;

        if (DisplayManager::m_profile_level & PROFILE_TRIG)
        {
            char atrace_tag[256];
            sprintf(atrace_tag, "set_ovl: input(%d) direct\n", id);
            HWC_ATRACE_NAME(atrace_tag);

            OLOGI("HWC->OVL: input(%d) direct", id);

            DispDevice::getInstance().enableOverlayInput(m_disp_id, param, id);
        }
        else
        {
            DispDevice::getInstance().enableOverlayInput(m_disp_id, param, id);
        }
    }

    return NO_ERROR;
}

status_t OverlayEngine::setInputs()
{
    AutoMutex l(m_lock);

    if (DisplayManager::m_profile_level & PROFILE_TRIG)
    {
        HWC_ATRACE_NAME("set_ovl: all inputs");
        OLOGI("HWC->OVL: all input");

        DispDevice::getInstance().updateOverlayInputs(
            m_disp_id, m_input_params.array(), m_max_inputs);
    }
    else
    {
        DispDevice::getInstance().updateOverlayInputs(
            m_disp_id, m_input_params.array(), m_max_inputs);
    }

    for (int id = 0; id < m_max_inputs; id++)
        clearOutdatedQueue(id);

    return NO_ERROR;
}

status_t OverlayEngine::disableInput(int id)
{
    AutoMutex l(m_lock);

    if (id >= m_max_inputs)
    {
        OLOGE("Failed to disable invalid overlay input(%d)", id);
        return BAD_INDEX;
    }

    if (OVL_INPUT_NONE == m_inputs[id]->connected_type)
    {
        //OLOGW("Not using overlay input(%d)", id);
        return BAD_INDEX;
    }

    disableInputLocked(id);

    return NO_ERROR;
}

void OverlayEngine::disableInputLocked(int id)
{
    // keep outdated queue and release it at flip()
    keepOutdatedQueue(id);

    // set overlay params
    m_input_params[id]->state = OVL_IN_PARAM_DISABLE;

    // clear input infomation
    m_inputs[id]->connected_state = OVL_PORT_DISABLE;
    m_inputs[id]->connected_type = OVL_INPUT_NONE;
}

status_t OverlayEngine::ignoreInput(int id)
{
    AutoMutex l(m_lock);

    if (id >= m_max_inputs)
    {
        OLOGE("Failed to ignore invalid overlay input(%d)", id);
        return BAD_INDEX;
    }

    if (OVL_INPUT_NONE == m_inputs[id]->connected_type)
    {
        return BAD_INDEX;
    }

    m_input_params[id]->state = OVL_IN_PARAM_IGNORE;

    return NO_ERROR;
}

status_t OverlayEngine::prepareOutput(OverlayPrepareParam& param)
{
    AutoMutex l(m_lock);

    param.id = m_max_inputs;
    DispDevice::getInstance().prepareOverlayOutput(m_disp_id, &param);

    return NO_ERROR;
}

status_t OverlayEngine::setOutput(OverlayPortParam* param)
{
    AutoMutex l(m_lock);

    m_output.connected_state = OVL_PORT_ENABLE;

    memcpy(&m_output.param, param, sizeof(OverlayPortParam));

    if (DisplayManager::m_profile_level & PROFILE_TRIG)
    {
        HWC_ATRACE_NAME("set_ovl: output");
        OLOGI("HWC->OVL: output");

        DispDevice::getInstance().enableOverlayOutput(m_disp_id, param);
    }
    else
    {
        DispDevice::getInstance().enableOverlayOutput(m_disp_id, param);
    }

    return NO_ERROR;
}

status_t OverlayEngine::trigger()
{
    AutoMutex l(m_lock);

    if (HWC_DISPLAY_VIRTUAL == m_disp_id)
    {
        if (OVL_PORT_ENABLE != m_output.connected_state)
        {
            if (DisplayManager::m_profile_level & PROFILE_TRIG)
            {
                HWC_ATRACE_NAME("trig_ovl: fail w/o output");
            }

            OLOGE("Try to trigger w/o set output port !!");
            return -EINVAL;
        }
    }

    status_t err;

    if (DisplayManager::m_profile_level & PROFILE_TRIG)
    {
        HWC_ATRACE_NAME("trig_ovl");
        OLOGI("HWC->OVL: trig");

        err = DispDevice::getInstance().triggerOverlaySession(m_disp_id);
    }
    else
    {
        err = DispDevice::getInstance().triggerOverlaySession(m_disp_id);
    }

    return err;
}

OverlayPortParam* const* OverlayEngine::getInputParams()
{
    return m_input_params.array();
}

void OverlayEngine::blank(int blank)
{
    AutoMutex l(m_lock);

    int num = DispDevice::getInstance().getAvailableOverlayInput(m_disp_id);

    if (blank)
    {
        m_state = OVL_ENGINE_PAUSED;

        DispDevice::getInstance().disableOverlaySession(m_disp_id, num);

        for (int id = 0; id < m_max_inputs; id++)
            clearOutdatedQueue(id);

        for (int id = 0; id < num; id++)
        {
            if (OVL_INPUT_NONE != m_inputs[id]->connected_type)
                disableInputLocked(id);
        }
    }
    else
    {
        m_state = OVL_ENGINE_ENABLED;
    }
}

sp<DisplayBufferQueue> OverlayEngine::getInputQueue(int id) const
{
    AutoMutex l(m_lock);

    if (id >= m_max_inputs)
    {
        OLOGE("Failed to get overlay input queue(%d)", id);
        return NULL;
    }

    if (OVL_INPUT_QUEUE != m_inputs[id]->connected_type)
    {
        OLOGW("No overlay input queue(%d)", id);
        return NULL;
    }

    return m_inputs[id]->queue;
}

void OverlayEngine::updateInput(int id)
{
    AutoMutex l(m_lock);

    if (m_inputs[id]->connected_state == OVL_PORT_ENABLE)
    {
        // acquire next buffer
        DisplayBufferQueue::DisplayBuffer buffer;
        m_inputs[id]->queue->acquireBuffer(&buffer);

        // fill struct for enable layer
        OverlayPortParam* param = m_input_params[id];
        param->state          = OVL_IN_PARAM_ENABLE;
        param->va             = buffer.data;
        param->mva            = (void*)buffer.mva;
        param->pitch          = buffer.data_pitch;
        param->format         = buffer.data_format;
        param->src_crop       = buffer.data_info.src_crop;
        param->dst_crop       = buffer.data_info.dst_crop;
        param->is_sharpen     = buffer.data_info.is_sharpen;
        param->fence_index    = buffer.rel_fence_idx;
        param->identity       = HWLAYER_ID_DBQ;
        param->connected_type = OVL_INPUT_QUEUE;
        param->protect        = buffer.protect;
        param->secure         = buffer.secure;
        param->alpha_enable   = buffer.alpha_enable;
        param->alpha          = buffer.alpha;
        param->need_shift     = buffer.need_shift;
        param->sequence       = buffer.sequence;

        if (DisplayManager::m_profile_level & PROFILE_TRIG)
        {
            char atrace_tag[256];
            sprintf(atrace_tag, "set_ovl: input(%d) queue\n", id);
            HWC_ATRACE_NAME(atrace_tag);

            OLOGI("HWC->OVL: input(%d) queue", id);
        }

        // release buffer
        m_inputs[id]->queue->releaseBuffer(buffer.index);
    }
}

void OverlayEngine::flip()
{
    for (int id = 0; id < m_max_inputs; id++)
        clearOutdatedQueue(id);

    // check AEE layer for primary display
    if (HWC_DISPLAY_PRIMARY == m_disp_id)
    {
        AutoMutex l(m_lock);
        int avail_inputs = DispDevice::getInstance().getAvailableOverlayInput(m_disp_id);
        for (int id = avail_inputs; id < m_max_inputs; id++)
        {
            if (OVL_INPUT_NONE != m_inputs[id]->connected_type)
                disableInputLocked(id);
        }
    }
}

void OverlayEngine::dump(struct dump_buff* log, int dump_level)
{
    AutoMutex l(m_lock);

    int total_size = 0;

    dump_printf(log, "\n[HWC Compose State (%d)]\n", m_disp_id);
    for (int id = 0; id < m_max_inputs; id++)
    {
        if (dump_level & DUMP_MM)
        {
            if (OVL_INPUT_QUEUE == m_inputs[id]->connected_type &&
                m_inputs[id]->queue != NULL)
            {
                m_inputs[id]->queue->dump(DisplayBufferQueue::QUEUE_DUMP_LAST_ACQUIRED);
            }
        }

        if (m_inputs[id]->connected_state == OVL_PORT_ENABLE)
        {
            OverlayPortParam* param = m_input_params[id];

            dump_printf(log, "  (%d) f=%d x=%d y=%d w=%d h=%d\n",
                    id, param->format, param->dst_crop.left, param->dst_crop.top,
                    param->dst_crop.getWidth(), param->dst_crop.getHeight());

            int layer_size = param->dst_crop.getWidth() * param->dst_crop.getHeight() * getBitsPerPixel(param->format) / 8;
            total_size += layer_size;

#ifdef MTK_HWC_PROFILING
            if (HWC_LAYER_TYPE_FBT == param->identity)
            {
                dump_printf(log, "  FBT(n=%d, bytes=%d)\n",
                    param->fbt_input_layers, param->fbt_input_bytes + layer_size);
            }
#endif
        }
    }
    dump_printf(log, "  Total size: %d bytes\n", total_size);
}

// --------------------------------------------------------------------------

ANDROID_SINGLETON_STATIC_INSTANCE(ExternalStream);

status_t ExternalStream::prepareInput(OverlayPrepareParam& param)
{
    AutoMutex l(m_lock);

    HdmiDevice::HdmiInputInfo input;
    input.ion_fd = param.ion_fd;

    HdmiDevice::getInstance().prepareInput(input);

    param.fence_index = input.fence_index;
    param.fence_fd = input.fence_fd;

    return NO_ERROR;
}

status_t ExternalStream::setInputDirect(OverlayPortParam& param)
{
    AutoMutex l(m_lock);

    HdmiDevice::HdmiInputInfo input;
    input.va          = param.va;
    input.mva         = param.mva;
    input.pitch       = param.pitch;
    input.format      = param.format;
    input.secure      = param.secure;
    input.fence_index = param.fence_index;

    // asynchronous function call
    HdmiDevice::getInstance().enableInput(
        input, param.src_crop, param.dst_crop);

    return NO_ERROR;
}
