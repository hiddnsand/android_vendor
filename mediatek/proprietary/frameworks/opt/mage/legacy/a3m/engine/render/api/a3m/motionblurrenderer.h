/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/** \file
 * Motion Blur Renderer
 *
 */
#pragma once
#ifndef A3M_MOTIONBLURRENDERER_H
#define A3M_MOTIONBLURRENDERER_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/assetcachepool.h>    /* for AssetCachePool                     */
#include <a3m/renderer.h>          /* Renderer interface                     */
#include <a3m/scenenodevisitor.h>  /* for SceneNodeVisitor                   */
#include <vector>                  /* for std::vector                        */
#include <a3m/rendertarget.h>      /* for reflection render target           */
#include <a3m/appearance.h>        /* for motion blur appearance             */

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  class RenderTarget;

  /** \todo Integrate into documentation (this is not currently in any groups) */


  /** MotionBlurRenderer class.
   * An implementation of the Renderer interface. This renderer uses its own
   * custom shader program (instead of the one attached to each mesh appearance)
   * to generate output representing the velocity of each pixel in screen space.
   */
  class MotionBlurRenderer : public Renderer, public SceneNodeVisitor
  {
  public:
    /** Default constructor.
     */
    MotionBlurRenderer( /** Context to use when rendering the scene */
      RenderContext::Ptr const& context,
      /** Cache pool for loading renderer assets */
      AssetCachePool::Ptr const& pool );

    // Override
    void render( Camera* camera, SceneNode& node, FlagMask const& renderFlags,
                 FlagMask const& recursiveFlags );

    // Override
    void update( A3M_FLOAT time ) { m_time = time; }

    // Override
    RenderContext::Ptr getRenderContext() const { return m_context; }

    // Override
    void visit( Solid* solid );


  private:
    std::vector< Solid* > m_solidsToRender;  /* List of solids to be rendered
                                             (generated each frame). */

    RenderContext::Ptr m_context; // Stores global render state
    AssetCachePool::Ptr m_pool; // Pool for asset creation


    A3M_FLOAT m_time; // Time to set in uniform "u_time"
    A3M_FLOAT m_lastTime; // Time that the last frame was rendered
    Matrix4f m_lastViewProjection[2]; /* The view projection matrix used for
                                      each eye (if stereo) the last time the
                                      scene was rendered. Used to calculate
                                      motion blur */

    Appearance::Ptr m_appearance; /* Appearance containing velocity calculating
                                     shader program for non-skinned meshes. */
    Appearance::Ptr m_skinnedAppearance;  /* Version for skinned meshes. */
  };
  /** @} */

} /* namespace a3m */

#endif /* A3M_MOTIONBLURRENDERER_H */
