LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

# We only want this apk build for tests.
LOCAL_MODULE_TAGS := tests
LOCAL_CERTIFICATE := platform

LOCAL_JAVA_LIBRARIES := android.test.runner \
                        mediatek-framework \
                        telephony-common \
			mediatek-telephony-base \
		        mediatek-telephony-common \
			mediatek-telecom-common \
			mediatek-common

LOCAL_STATIC_JAVA_LIBRARIES := emlibrobotium \
                               engineermodelibannotations \
                               junit \
                               legacy-android-test


# Include all test java files.
LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := EngineerModeTests
LOCAL_MODULE_OWNER := mtk

LOCAL_INSTRUMENTATION_FOR := EngineerMode

include $(BUILD_PACKAGE)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := emlibrobotium:libs/robotium-solo-5.6.0.jar \
                                        engineermodelibannotations:libs/mtkatannotations.jar
include $(BUILD_MULTI_PREBUILT)
