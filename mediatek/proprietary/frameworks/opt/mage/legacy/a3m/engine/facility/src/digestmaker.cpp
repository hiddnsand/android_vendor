/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * MessageDigest implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/detail/digestmaker.h>     /* This class's API     */
#include <a3m/log.h>                    /* Logging              */
#include <a3m/stream.h>                 /* To read/write Digest */

namespace a3m
{
  namespace detail
  {
    /*
     * Construct
     */
    DigestMaker::DigestMaker() : m_finished( A3M_FALSE )
    {
      md5_init( &m_state );
    }

    /*
     * Append data
     */
    void DigestMaker::append( A3M_CHAR8 const* data, A3M_UINT32 length )
    {
      if( m_finished )
      {
        A3M_LOG_ERROR( "Improper Attempt to append to message digest", 0 );
      }
      else
      {
        md5_append( &m_state, reinterpret_cast< md5_byte_t const* >( data ),
                    length );
      }
    }

    /*
     * Get Digest
     */
    DigestMaker::Digest const& DigestMaker::digest()
    {
      if( !m_finished )
      {
        md5_finish( &m_state, m_digest.m_data );
        m_finished = A3M_TRUE;
      }
      return m_digest;
    }

    /*
     * Read from stream
     */
    void DigestMaker::Digest::read( Stream& stream )
    {
      stream.read( m_data, DIGEST_SIZE );
    }

    /*
     * Write to stream
     */
    void DigestMaker::Digest::write( Stream& stream ) const
    {
      stream.write( m_data, DIGEST_SIZE );
    }

    /*
     * Compare with another Digest
     */
    A3M_BOOL DigestMaker::Digest::operator==( Digest const& other ) const
    {
      return std::equal( m_data, m_data + DIGEST_SIZE, other.m_data );
    }
  } /* namespace detail */
} /* namespace a3m */
