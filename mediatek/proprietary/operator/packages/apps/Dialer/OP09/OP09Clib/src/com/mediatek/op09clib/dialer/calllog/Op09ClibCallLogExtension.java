package com.mediatek.op09clib.dialer.calllog;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.CallLog.Calls;
import android.telecom.PhoneAccountHandle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.dialer.app.calllog.CallLogActivity;

import com.mediatek.dialer.ext.DefaultCallLogExtension;
import com.mediatek.dialer.ext.ICallLogAction;
import com.mediatek.dialer.ext.ICallLogExtension;

import com.mediatek.op09clib.dialer.R;
import java.lang.ref.WeakReference;

import java.util.List;

import com.mediatek.calllog.calllogfilter.PhoneAccountUtils;
import com.mediatek.calllog.calllogfilter.PhoneAccountPickerActivity;
import com.mediatek.calllog.calllogfilter.FilterOptions;
import com.mediatek.calllog.calllogfilter.PhoneAccountInfoHelper;

public class Op09ClibCallLogExtension extends DefaultCallLogExtension
        implements PhoneAccountInfoHelper.AccountInfoListener {
    private static final String TAG = "Op09ClibCallLogExtension";

    private WeakReference<ICallLogAction> mICallLogAction;
    private Context mPluginContext = null;

    public static final int PHONE_ACCOUNT_FILTER_MENU_ID = 0;
    public static final int CALL_LOG_MENU_INDEX = 0;

    private static final int NOTICE_ID = 0x10ffffff;
    private static final int ACCOUNT_MENU_ID = 0x2000;

    /**
     * constructor
     * @param context the current context
     */
    public Op09ClibCallLogExtension(Context context) {
        mPluginContext = context;
        PhoneAccountInfoHelper.INSTANCE.onInit(mPluginContext);
    }

    /**
     * for op09
     * called when host create menu, to add plug-in own menu here
     * @param activity the current activity
     * @param menu menu
     * @param viewPagerTabs the ViewPagerTabs used in activity
     * @param callLogAction callback plug-in need if things need to be done by host
     */
    @Override
    public void createCallLogMenu(Activity activity, Menu menu,
            HorizontalScrollView viewPagerTabs, ICallLogAction callLogAction) {
        Log.d(TAG, "createCallLogMenu");
        final Activity fCallLogActivity = activity;
        final ICallLogAction fCallLogAction = callLogAction;

        //add the AccountFilter MenuItem
        createAccountFilterMenuItem(fCallLogActivity, menu);

    }

    /// M: [Call Log Account Filter] @{
    private MenuItem createAccountFilterMenuItem(final Activity activity, Menu menu) {
        MenuItem accountFilterMenuItem = menu.add(Menu.NONE, PHONE_ACCOUNT_FILTER_MENU_ID,
            ACCOUNT_MENU_ID, mPluginContext.getText(R.string.select_account));
        //accountFilterMenuItem.setIcon(mPluginContext.getDrawable(R.drawable.ic_account_chooser));
        //accountFilterMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        accountFilterMenuItem.setOnMenuItemClickListener(
            new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Log.d(TAG, "accountFilterMenu onMenuItemClick");
                Intent intent = new Intent(mPluginContext, PhoneAccountPickerActivity.class);
                intent.putExtra(FilterOptions.FILTER_ACCOUNT_PREFER,
                        PhoneAccountInfoHelper.INSTANCE.getPreferAccountId(mPluginContext));
                activity.startActivityForResult(intent, FilterOptions.ACTIVITY_REQUEST_CODE);
                return true;
            }
        });
        return accountFilterMenuItem;
    }
    /// @}

    /**
     * for op09
     * called when host prepare menu, prepare plug-in own menu here
     * @param activity the current activity
     * @param menu the Menu Created
     * @param fragment the current fragment
     * @param itemDeleteAll the optionsmenu delete all item
     * @param adapterCount adapterCount
     */
    public void prepareCallLogMenu(Activity activity, Menu menu,
            Fragment fragment, MenuItem itemDeleteAll, int adapterCount) {

        /// M :[Call Log Account Filter] @{
        // hide choose account menu if only one or no account
        final MenuItem itemChooseAccount = menu.findItem(PHONE_ACCOUNT_FILTER_MENU_ID);
        if (itemChooseAccount != null) {
            if (fragment != null) {
                itemChooseAccount.setVisible(PhoneAccountUtils
                    .hasMultipleCallCapableAccounts(mPluginContext));
            } else {
                itemChooseAccount.setVisible(false);
            }
        } else {
            if (itemChooseAccount != null) {
                itemChooseAccount.setVisible(false);
            }
        }
        /// @}
    }


    /**
     * for op09
     * @param typeFiler current query type
     * @param builder the query selection Stringbuilder
     * @param selectionArgs the query selection args, modify to change query selection
     */
    @Override
    public void appendQuerySelection(int typeFiler, StringBuilder builder,
            List<String> selectionArgs) {
        if (mICallLogAction == null) {
            return;
        }

        ICallLogAction action = mICallLogAction.get();
        if (action == null) {
            return;
        }

        if (!action.isActivityResumed()) {
            return;
        }

        /// M: [Call Log Account Filter] @{
        String preferAccountId = PhoneAccountInfoHelper.INSTANCE.getPreferAccountId(mPluginContext);
        if (!PhoneAccountInfoHelper.FILTER_ALL_ACCOUNT_ID.equals(preferAccountId)) {
            if (builder.length() > 0) {
                builder.append(" AND ");
            }
            // query the Call Log by account id
            builder.append(String.format("(%s = ?)", Calls.PHONE_ACCOUNT_ID));
            selectionArgs.add(preferAccountId);
        }
        /// @}

        Log.d(TAG, "builder: " + builder);
    }

    /**.
     * for op09
     * plug-in reset the reject mode in the host
     * @param activity the current activity
     * @param bundle bundle
     */
    public void onCreate(Activity activity, Bundle bundle) {
        PhoneAccountInfoHelper.INSTANCE.registerForAccountChange(this);
        PhoneAccountInfoHelper.INSTANCE.setActivity(activity);

        if (activity instanceof ICallLogAction) {
            Log.d(TAG, "onCreate to init ICallLogAction");
            ICallLogAction action = (ICallLogAction) activity;
            mICallLogAction = new WeakReference<ICallLogAction> (action);
        }
    }

    /**.
     * for op09
     * plug-in manage the state and unregister receiver
     * @param activity the current activity
     */
    public void onDestroy(Activity activity) {
        PhoneAccountInfoHelper.INSTANCE.unRegisterForAccountChange(this);
    }


    /**
     * for op09
     * plug-in handle Activity Result
     * @param requestCode requestCode
     * @param resultCode resultCode
     * @param data the intent return by setResult
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult requestCode,,resultCode,,data = " +
                requestCode + ",," + resultCode + ",," + data);
        if (requestCode == FilterOptions.ACTIVITY_REQUEST_CODE) {
            if (resultCode == FilterOptions.ACTIVITY_RESULT_CODE) {
                Bundle bundle = data.getExtras();
                String selectedId = bundle.getString(FilterOptions.SELECTED_ID);
                boolean result = PhoneAccountInfoHelper.INSTANCE.setPreferAccountId(mPluginContext,
                        String.valueOf(selectedId));
                if (result) {
                    PhoneAccountInfoHelper.INSTANCE.notifyPreferAccountChange(selectedId);
                }
            } else {
                Log.e(TAG, "onActivityResult failed");
            }
        }
    }

    @Override
    public void onViewCreated(Fragment fragment, View view) {
        Activity activity = fragment.getActivity();
        Resources resource = activity.getResources();
        String packageName = activity.getPackageName();
        View recyclerView = view.findViewById(resource.getIdentifier("recycler_view",
                "id", packageName));

        if (recyclerView == null) {
            Log.d(TAG, "onViewCreated, recyclerView is null");
            return;
        }

        ViewGroup group = (ViewGroup) recyclerView.getParent();
        if (group == null) {
            Log.d(TAG, "onViewCreated, recyclerView parent is null");
            return;
        }

        addViewContent(group);
    }

    private void addViewContent(ViewGroup groupParent) {
        LayoutInflater inflater;
        inflater = LayoutInflater.from(mPluginContext);
        ViewGroup noticeLayout =
                (ViewGroup) inflater.inflate(R.layout.account_notice_indicator, groupParent, false);
        noticeLayout.setId(NOTICE_ID);
        groupParent.addView(noticeLayout, 1);
    }

    /**.
     * for op09
     * plug-in refresh the CallLogFragment to show th notice
     * @param fragment the current fragment
     */
    @Override
    public void updateNotice(Fragment fragment) {
        View view = fragment.getView();

        Activity activity = fragment.getActivity();
        if (activity != null &&
            !(activity instanceof CallLogActivity)) {
          return;
        }

        ViewGroup viewChild = (ViewGroup) view.findViewById(NOTICE_ID);
        if (viewChild == null) {
            return;
        }

        TextView noticeText = (TextView) viewChild.getChildAt(0);
        View noticeTextDivider = (View) viewChild.getChildAt(1);
        String lable = null;
        int color = -1;
        String id = PhoneAccountInfoHelper.INSTANCE.getPreferAccountId(mPluginContext);
        if (mPluginContext != null && !PhoneAccountInfoHelper.FILTER_ALL_ACCOUNT_ID.equals(id)) {
            PhoneAccountHandle account = PhoneAccountUtils.getPhoneAccountById(mPluginContext, id);
            if (account != null) {
                lable = PhoneAccountUtils.getAccountLabel(mPluginContext, account);
                color = PhoneAccountUtils.getAccountColor(mPluginContext, account);
            }
        }
        Log.d(TAG, "updateNotice, lable = " + lable + ", id = " + id);
        if (!TextUtils.isEmpty(lable) && noticeText != null && noticeTextDivider != null) {
            String noticeString = mPluginContext.getResources().getString(
                    R.string.call_log_via_sim_name_notice, lable);
            SpannableStringBuilder style = applySpannableStyle(noticeString, lable, color);
            noticeText.setText(style);
            noticeText.setVisibility(View.VISIBLE);
            noticeTextDivider.setVisibility(View.VISIBLE);
        } else {
            noticeText.setVisibility(View.GONE);
            noticeTextDivider.setVisibility(View.GONE);
        }
    }

    private SpannableStringBuilder applySpannableStyle(String target, String source, int color) {
        SpannableStringBuilder style = new SpannableStringBuilder(target);

        int start = target.indexOf(source);
        int end = target.length() - 1;
        Log.d(TAG, "applySpannableStyle, start = " + start + ", end = " + end);

        if (end > start && start > 0 && color != -1) {
            style.setSpan(new ForegroundColorSpan(color),
                  start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return style;
    }

    @Override
    public void onPreferAccountChanged(String index) {
        Log.d(TAG, "onPreferAccountChanged");
        if (mICallLogAction == null) {
            return;
        }

        ICallLogAction action = mICallLogAction.get();
        if (action == null) {
            return;
        }

        action.updateCallLogScreen();
    }

    @Override
    public void onAccountInfoUpdate() {
        Log.d(TAG, "onAccountInfoUpdate");
        if (mICallLogAction == null) {
            return;
        }

        ICallLogAction action = mICallLogAction.get();
        if (action == null) {
            return;
        }

        action.updateCallLogScreen();
    }
}
