LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_AAPT_INCLUDE_ALL_RESOURCES := true

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := WapiCertManager

LOCAL_PROPRIETARY_MODULE := true

LOCAL_MODULE_OWNER := mtk

LOCAL_MULTILIB := both

LOCAL_CERTIFICATE := platform

LOCAL_JNI_SHARED_LIBRARIES := libwapi_cert

LOCAL_STATIC_JAVA_LIBRARIES += WapiCertStore

include $(BUILD_PACKAGE)

include $(call all-makefiles-under,$(LOCAL_PATH))
