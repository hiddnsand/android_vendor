/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.cfu;

import android.app.AlertDialog;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemProperties;

import com.android.internal.telephony.Phone;

import com.mediatek.engineermode.Elog;
import com.mediatek.internal.telephony.MtkTelephonyProperties;


public class CfuSettingsCtl extends Handler {
    private static final String TAG = "CFUCtl";

    public static final String SYS_PROP_DEFAULT                 = "0";
    public static final String SYS_PROP_ALWAYS_NOT_QUERY        = "1";
    public static final String SYS_PROP_ALWAYS_QUERY            = "2";
    public static final String SYS_PROP_QUERY_IF_EFCFIS_INVALID = "3";

    public static final int QUERY                   = 1;
    public static final int SET_DEFAULT             = 2;
    public static final int SET_ON                  = 3;
    public static final int SET_OFF                 = 4;
    public static final int REFRESH_AP_QUERY_STATUS = 5;
    public static final int CHECK_BTN_ERROR         = 10;

    public static final String FORE_CMD = "+ESSP:";

    public final static int EVENT_SET_DEFAULT                   = 0;
    public final static int EVENT_SET_ALWAYS_QUERY              = 1;
    public final static int EVENT_SET_ALWAYS_NOT_QUERY          = 2;
    public final static int EVENT_SET_QUERY_IF_EFCFIS_INVALID   = 3;
    public final static int EVENT_QUERY_FROM_MD                 = 4;
    public final static int EVENT_SET_DEFAULT_TO_AP_MD          = 5;
    public final static int EVENT_SET_ALWAYS_QUERY_TO_AP_MD     = 6;
    public final static int EVENT_SET_ALWAYS_NOT_QUERY_TO_AP_MD = 7;
    public final static int EVENT_SHOW_ERROR                    = 8;

    public Phone mPhone;

    public CfuActivity mCfuActivity;

    public CfuSettingsCtl(Looper looper, Phone phone, CfuActivity cfuActivity) {
        super(looper);

        mPhone = phone;
        mCfuActivity = cfuActivity;
    }

    @Override
    public void handleMessage(Message msg) {
        Elog.d(TAG, "handleMessage msg: " + msg.what);
        AsyncResult ar;
        Message callback = (Message) msg.obj;
        switch (msg.what) {
            case EVENT_SET_DEFAULT:
                setCFUQueryTypeSystemProperty(SYS_PROP_DEFAULT);
                Elog.i(TAG, "Set Query CFU Status : default");
                callback.sendToTarget();
                break;
            case EVENT_SET_ALWAYS_QUERY:
                setCFUQueryTypeSystemProperty(SYS_PROP_ALWAYS_QUERY);
                Elog.i(TAG, "Set Query CFU Status : on");
                callback.sendToTarget();
                break;
            case EVENT_SET_ALWAYS_NOT_QUERY:
                setCFUQueryTypeSystemProperty(SYS_PROP_ALWAYS_NOT_QUERY);
                Elog.i(TAG, "Set Query CFU Status : off");
                callback.sendToTarget();
                break;
            case EVENT_SET_QUERY_IF_EFCFIS_INVALID:
                setCFUQueryTypeSystemProperty(SYS_PROP_QUERY_IF_EFCFIS_INVALID);
                Elog.i(TAG, "Set Query CFU Status : query if efcfis is invalid");
                callback.sendToTarget();
                break;
            case EVENT_QUERY_FROM_MD:
                sendAtCommand(QUERY, callback);
                Elog.i(TAG, "Query CFU Status (MD)");
                break;
            case EVENT_SET_DEFAULT_TO_AP_MD:
                sendAtCommand(SET_DEFAULT, callback);
                Elog.i(TAG, "Set Query CFU Status (AP + MD): default");
                break;
            case EVENT_SET_ALWAYS_QUERY_TO_AP_MD:
                sendAtCommand(SET_ON, callback);
                Elog.i(TAG, "Set Query CFU Status (AP + MD): on");
                break;
            case EVENT_SET_ALWAYS_NOT_QUERY_TO_AP_MD:
                sendAtCommand(SET_OFF, callback);
                Elog.i(TAG, "Set Query CFU Status (AP + MD): off");
                break;
            case EVENT_SHOW_ERROR:
                createDialog(CHECK_BTN_ERROR);
                Elog.e(TAG, "Set Query CFU Status (with error) (AP + MD): off");
                break;
            default:
        }
    }

    private void setCFUQueryTypeSystemProperty(String value) {
        SystemProperties.set(MtkTelephonyProperties.CFU_QUERY_TYPE_PROP, value);
    }

    public String getCFUQueryTypeSystemProperty() {
        return SystemProperties.get(MtkTelephonyProperties.CFU_QUERY_TYPE_PROP,
                MtkTelephonyProperties.CFU_QUERY_TYPE_DEF_VALUE);
    }

    private void sendAtCommand(int msg, Message callback) {
        if (mPhone != null) {
            mPhone.invokeOemRilRequestStrings(createCmd(msg), callback);
        }
    }

    /**
     *
     * @param type
     *            AT command type
     * @return AT command
     */
    private String[] createCmd(final int type) {
        String[] cmd = new String[2];
        switch (type) {
        case QUERY:
            cmd[0] = "AT+ESSP?";
            cmd[1] = "+ESSP";
            break;
        case SET_DEFAULT:
            cmd[0] = "AT+ESSP=0";
            cmd[1] = "";
            break;
        case SET_ON:
            cmd[0] = "AT+ESSP=2";
            cmd[1] = "";
            break;
        case SET_OFF:
            cmd[0] = "AT+ESSP=1";
            cmd[1] = "";
            break;
        default:
            cmd[0] = "AT+ESSP?";
            cmd[1] = "+ESSP";
            break;
        }
        Elog.d(TAG, "Send msg:" + cmd[0]);
        return cmd;
    }

    public void createDialog(int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mCfuActivity);
        switch (id) {
        case QUERY:
            break;
        case SET_DEFAULT:
            builder.setTitle("Set Failed!").setMessage("Set Default Status Failed!")
                    .setPositiveButton("OK", null).create()
                    .show();
            break;
        case SET_ON:
            builder.setTitle("Set Failed!").setMessage("Open Query Status Failed!")
                    .setPositiveButton("OK", null).create()
                    .show();
            break;
        case SET_OFF:
            builder.setTitle("Set Failed!").setMessage("Close Query Status Failed!")
                    .setPositiveButton("OK", null).create()
                    .show();
            break;
        case CHECK_BTN_ERROR:
            builder.setTitle("Warning!").setMessage("Please chose a item!")
                    .setPositiveButton("OK", null).create().show();
            break;
        default:
            break;
        }
    }

}