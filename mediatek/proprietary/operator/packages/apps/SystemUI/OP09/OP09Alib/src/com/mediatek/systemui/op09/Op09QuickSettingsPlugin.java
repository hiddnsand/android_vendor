package com.mediatek.systemui.op09;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.mediatek.systemui.op09.R;
import com.mediatek.systemui.ext.DefaultQuickSettingsPlugin;
import com.mediatek.systemui.statusbar.extcb.IconIdWrapper;

/**
 * Customize carrier text.
 */
public class Op09QuickSettingsPlugin extends DefaultQuickSettingsPlugin {
    public static final String MobileDataTile = "dataconnection";
    public static final String SimDataConnectionTile = "simdataconnection";
    public static final String DualSimSettingsTile = "dulsimsettings";
    public static final String ApnSettingsTile = "apnsettings";

    public static final String TAG = "Op09QuickSettingsPlugin";

    private static final int DATA_DISCONNECT = 0;
    private static final int DATA_CONNECT = 1;
    private static final int AIRPLANE_DATA_CONNECT = 2;
    private static final int DATA_CONNECT_DISABLE = 3;
    private static final int DATA_RADIO_OFF = 4;

    /**
     * Constructs a new OP02QuickSettingsPlugin instance with Context.
     * @param context A Context object
     */
    public Op09QuickSettingsPlugin(Context context) {
        super(context);
    }

    @Override
    public boolean customizeDisplayDataUsage(boolean isDisplay) {
        Log.i(TAG, "customizeDisplayDataUsage, " + " return true");
        return true;
    }

    @Override
    public String customizeQuickSettingsTileOrder(String defaultString) {
        String[] tiles = defaultString.split(",");
        String tempTiles = tiles[0];
        if (TelephonyManager.getDefault().getPhoneCount() <= 1) {
            for (int i = 1; i < tiles.length; i++) {
                if (i == 4) {
                    // Op MobileDataTile order index is 4
                    tempTiles = tempTiles + "," + MobileDataTile + "," + tiles[i];
                } else if (i == tiles.length -1) {
                    // end index
                    tempTiles = tempTiles + "," + tiles[i] + "," + ApnSettingsTile;
                } else {
                    tempTiles = tempTiles + "," + tiles[i];
                }
            }
        } else {
            for (int i = 1; i < tiles.length; i++) {
                if (i == 4) {
                    // Op MobileDataTile order index is 4
                    tempTiles = tempTiles + "," + MobileDataTile + "," +
                            SimDataConnectionTile + "," + tiles[i];
                    // end index
                } else if (i == tiles.length -1) {
                    tempTiles = tempTiles + "," + tiles[i] +  "," + DualSimSettingsTile +
                            "," + ApnSettingsTile;
                } else {
                    tempTiles = tempTiles + "," + tiles[i];
                }
            }
        }
        return tempTiles;
    }

    @Override
    public Object customizeAddQSTile(Object qsTile) {
        return qsTile;
    }

    @Override
    public String customizeDataConnectionTile(int dataState, IconIdWrapper icon,
            String orgLabelStr) {
        Log.d(TAG, "customizeDataConnectionTile dataState=" + dataState + "icon= " + icon);
        icon.setResources(this.getResources());
        int iconId = TelephonyIcons.QS_MOBILE_DISABLE;

        switch(dataState) {
            case DATA_DISCONNECT:
                iconId = TelephonyIcons.QS_MOBILE_DISABLE;
                break;
            case DATA_RADIO_OFF:
            case DATA_CONNECT_DISABLE:
            case AIRPLANE_DATA_CONNECT:
                iconId = TelephonyIcons.QS_MOBILE_DISABLE;
                break;
            case DATA_CONNECT:
                iconId = TelephonyIcons.QS_MOBILE_ENABLE;
                break;
            default :
                iconId = TelephonyIcons.QS_MOBILE_DISABLE;
                break;
        }

        icon.setIconId(iconId);

        return mContext.getString(R.string.data_connection_summary_title);
    }

    @Override
    public String customizeDualSimSettingsTile(boolean enable, IconIdWrapper icon,
            String labelStr) {
        Log.i(TAG, "customizeDualSimSettingsTile, enable = " + enable + " icon=" + icon);
        icon.setResources(this.getResources());
        if (enable) {
            icon.setIconId(TelephonyIcons.QS_DUAL_SIM_SETTINGS_ENABLE);
        } else {
            icon.setIconId(TelephonyIcons.QS_DUAL_SIM_SETTINGS_DISABLE);
        }
        return mContext.getString(R.string.dual_sim_settings);
    }

    @Override
    public void customizeSimDataConnectionTile(int state, IconIdWrapper icon) {
        Log.i(TAG, "customizeSimDataConnectionTile, state = " + state + " icon=" + icon);
        icon.setResources(this.getResources());
        icon.setIconId(TelephonyIcons.IC_SIM_CONNECT_INDICATOR[state]);
    }

    @Override
    public String customizeApnSettingsTile(boolean enable, IconIdWrapper icon, String orgLabelStr) {
        Log.i(TAG, "customizeApnSettingsTile, enable = " + enable + " icon=" + icon);
        icon.setResources(this.getResources());
        if (enable) {
            icon.setIconId(TelephonyIcons.QS_APN_SETTINGS_ENABLE);
        } else {
            icon.setIconId(TelephonyIcons.QS_APN_SETTINGS_DISABLE);
        }

        return mContext.getString(R.string.apn_apn);
    }

    @Override
    public String getTileLabel(String tile) {
        String tileString = "";
        if (tile != null && tile.equalsIgnoreCase("apnsettings")) {
            tileString = mContext.getString(R.string.apn_apn);
        } else if (tile != null && tile.equalsIgnoreCase("dulsimsettings")) {
            tileString = mContext.getString(R.string.dual_sim_settings);
        } else if (tile != null && tile.equalsIgnoreCase("mobiledata")) {
            tileString = mContext.getString(R.string.data_connection_summary_title);
        } else if (tile != null && tile.equalsIgnoreCase("simdataconnection")) {
            tileString = mContext.getString(R.string.data_sim_title);
        }
        return tileString;
    }

}
