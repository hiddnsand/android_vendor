package com.mediatek.op12.telecom;

import android.content.Context;
import android.provider.CallLog.Calls;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.server.telecom.Call;
import com.mediatek.server.telecom.ext.DefaultCallMgrExt;
import com.mediatek.telephony.MtkTelephonyManagerEx;

/**
 * Plug in implementation for OP12 Call Mananger interfaces.
 */
public class Op12CallMgrExt extends DefaultCallMgrExt {
    private static final String LOG_TAG = "Op12CallMgrExt";
    private Context mContext = null;
    private static final int FEATURES_VoLTE = 0x4;
    private static final int FEATURES_VoWIFI = 0x8;
    private static final int FEATURES_ViWIFI = 0x10;

    /** Constructor.
     * @param context context
     */
    public Op12CallMgrExt(Context context) {
       mContext = context;
    }

    @Override
    public int getCallFeatures(Object callObj, int callFeatures) {
        Log.d(LOG_TAG, "getCallFeatures");
        Call call = (Call) callObj;
        TelecomManager tmgr = TelecomManager.from(mContext);
        TelephonyManager telephonyManager = TelephonyManager.from(mContext);
        PhoneAccountHandle callPhoneAccountHandle =
                               call.getConnectionManagerPhoneAccount();
        if (callPhoneAccountHandle != null) {
            PhoneAccount phoneAccount = tmgr.getPhoneAccount(callPhoneAccountHandle);
            int subId = telephonyManager.getSubIdForPhoneAccount(phoneAccount);
            boolean volteEnabled = MtkTelephonyManagerEx.getDefault().isVolteEnabled(subId);
            boolean wifiCallingEnabled = MtkTelephonyManagerEx.getDefault()
                                                   .isWifiCallingEnabled(subId);
            Log.d(LOG_TAG, "subId:" + subId + "\nvolteEnabled:" + volteEnabled +
                           "\nwifiCallingEnabled:" + wifiCallingEnabled);
            if (wifiCallingEnabled && (callFeatures == Calls.FEATURES_VIDEO)) {
                Log.d(LOG_TAG, "logNumber Video Over Wifi:" + FEATURES_ViWIFI);
                callFeatures = FEATURES_ViWIFI;
            } else if (wifiCallingEnabled) {
                Log.d(LOG_TAG, "logNumber Wifi Call:" + FEATURES_VoWIFI);
                callFeatures = FEATURES_VoWIFI;
            } else if (volteEnabled && (callFeatures != Calls.FEATURES_VIDEO)) {
                Log.d(LOG_TAG, "logNumber Volte Call:" + FEATURES_VoLTE);
                callFeatures = FEATURES_VoLTE;
            } else {
                 Log.d(LOG_TAG, "logNumber as Normal call:" + callFeatures);
            }
        }
        return callFeatures;
    }
}
