#ifndef GLOBAL_VAR_H
#define GLOBAL_VAR_H

#ifdef LOG_TAG
#undef LOG_TAG
#define LOG_TAG "MobileLogD"
#endif

#define LIB_AED "libaed.so"
typedef int (AEE_SYS_FUNC)(const char *module, const char* path, unsigned int flags, const char *msg,...);

#define CONFIG_DIR          "/data/misc/mblog/"
#define CONFIG_FILE         CONFIG_DIR"mblog_config"
#define MBLOG_HISTORY       CONFIG_DIR"mblog_history"
#define INTER_DIR_FILE      CONFIG_DIR"inter_sd_folder"
#define OUTER_DIR_FILE      CONFIG_DIR"outer_sd_folder"

#define BUILD_TYPE_ENG      "eng"
#define BUILD_TYPE_USER     "user"

#define PATH_INTERNAL       "internal_sd"
#define PATH_EXTERNAL       "external_sd"
#define PATH_PHONE          "/data/"
#define PATH_DEFAULT        "/sdcard/"
#define PATH_EXTSD_PARENT   "/storage/"

#define PATH_TEMP           "/data/log_temp/"
#define PATH_NORMAL         PATH_TEMP"boot/"
#define PATH_IPO            PATH_TEMP"ipo/"
#define PATH_META           PATH_TEMP"meta/"
#define PATH_FACTORY        PATH_TEMP"factory/"
#define PATH_OTHER_BOOT     PATH_TEMP"otherboot/"
#define PATH_SUFFIX         "mtklog/mobilelog/"

#define PROP_RUNNING         "debug.MB.running"
#define PROP_PATH            "debug.MB.realpath"
#define PROP_OLDPATH         "debug.MB.oldpath"
#define PROP_PACKED          "debug.MB.packed"
#define PROP_VERSION         "debug.MB.version"

#define MIN_STORAGE_SIZE    10  //unit:MB
#define STORAGE_FULL        101

#ifndef TEMP_FAILURE_RETRY
/* Used to retry syscalls that can return EINTR. */
#define TEMP_FAILURE_RETRY(exp)({                 \
    typeof (exp) _rc;                             \
    do {                                          \
        _rc = (exp);                              \
    } while (_rc == -1 && errno == EINTR);        \
    _rc;})
#endif

typedef enum {
	SAVE_TO_BOOTUP = 0,
	SAVE_TO_SDCARD,

	STOPPED = 999,
} MBLOGSTATUS;

extern MBLOGSTATUS g_mblog_status;
extern char cur_logging_path[256];

typedef enum mobilog_id {
	MOBILOG_ID_MIN = 0,

	MOBILOG_ID_MAIN = 0,
	MOBILOG_ID_RADIO = 1,
	MOBILOG_ID_EVENTS = 2,
	MOBILOG_ID_SYSTEM = 3,
	MOBILOG_ID_CRASH = 4,
	MOBILOG_ID_SECURITY = 5,
	MOBILOG_ID_KERNEL = 6, /* Third-parties can not use it */

	MOBILOG_ID_ATF = 7,
	MOBILOG_ID_GZ = 8,
	MOBILOG_ID_BSP = 9,
	MOBILOG_ID_MMEDIA = 10,
	MOBILOG_ID_SCP = 11,
	MOBILOG_ID_SCP_B = 12,
	MOBILOG_ID_SSPM = 13,

	MOBILOG_ID_MAX

} mobilog_id_t;

#endif
