#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import sys
import os
import struct
import re
import commands
import subprocess
import fileinput
import ntpath
import shutil

#typedef union
#{
#    struct
#    {
#        unsigned int magic;     /* always IMG_MAGIC */
#        unsigned int dsize;     /* image size, image header is incldued, padding is not included */
#        char name[IMG_NAME_SIZE];
#        unsigned int maddr;     /* image load address in RAM */
#        unsigned int mode;      /* maddr is counted from the beginning or end of RAM */
#        /* extension */
#        unsigned int ext_magic;    /* always EXT_MAGIC */
#        unsigned int hdr_size;     /* header size is 512 bytes currently, but may extend in the future */
#        unsigned int hdr_version;  /* see HDR_VERSION */
#        unsigned int img_type;     /* please refer to #define beginning with IMG_TYPE_ */
#        unsigned int img_list_end; /* end of image list? 0: this image is followed by another image 1: end */
#        unsigned int align_size;   /* image size alignment setting in bytes, 16 by default for AES encryption */
#        unsigned int dsize_extend; /* high word of image size for 64 bit address support */
#        unsigned int maddr_extend; /* high word of image load address in RAM for 64 bit address support */
#    } info;
#    unsigned char data[IMG_HDR_SIZE];
#} IMG_HDR_T;
IMG_HDR_MAGIC = 0x58881688
mkimage_header = "<2I 32s 2I 8I"

def executeShell(sh_command):
	subprocess.call(sh_command, shell=True)

def createOutDir(path):
	dir = os.path.abspath(path)
	print "Create dir:"+dir

	if not os.path.exists(dir):
		os.makedirs(dir)

def createFolder():
	createOutDir(out_path)
	createOutDir(out_cert1_dir)
	createOutDir(out_cert1md_dir)
	createOutDir(cert1_dir)
	createOutDir(cert2_key_dir)
	createOutDir(cert2_pubk_dir)
	return

def setPath():
	global out_path
	global resign_tool_path
	global cert1_dir
	global out_cert1_dir
	global out_cert1md_dir
	global cert2_key_dir
	global img_ver_path
	global bin_tmp_path
	global out_cert1_path
	global out_cert1md_path
	global out
	global cert2_pubk_dir

	if(os.environ.get('OUT') == None):
		out = "out"
	else:
		out = os.environ.get('OUT')
	out_path = os.path.join(out, "resign")
	out_cert1_dir = os.path.join(out_path, "cert", "cert1")
	out_cert1_path= os.path.join(out_cert1_dir, "cert1.der")
	out_cert1md_dir = os.path.join(out_path, "cert", "cert1md")
	out_cert1md_path= os.path.join(out_cert1md_dir, "cert1md.der")

	resign_tool_path = os.path.join("vendor", "mediatek", "proprietary", "scripts", "sign-image_v2", "signtool", "resignTool.py")
	cert1_dir = os.path.join("vendor", "mediatek", "proprietary", "custom", platform, "security", "cert_config", "cert1")
	cert2_key_dir = os.path.join("vendor", "mediatek", "proprietary", "custom", platform, "security", "cert_config", "cert2_key")
	img_ver_path  = os.path.join("vendor", "mediatek", "proprietary", "custom", platform, "security", "cert_config", "img_ver.txt")

	cert2_pubk_dir = os.path.join(out_path, "cert2_pubk")


def fillArgDict(str,key,argDict):
	prefix = str.split("=")[0]
	format = re.compile(key,re.I)
	if(format.search(prefix)):
		val = str.split("=")[1]
		argDict[key] = val
		print key+": "+val
	return argDict

def parseArg(argv):
	global platform
	global argDict
	argDict = {'cert2_key_path': 0 , 'cert1_key_path': 0, 'root_key_padding': 0}

	for str in argv:
		for key in argDict:
			argDict = fillArgDict(str,key,argDict)

	#check input
	platform = argv[1]
	#print argDict


def copyFile(path1,path2):
	shutil.copy2(path1, path2)

def getBinList():
	pattern1= "\["
	format1 = re.compile(pattern1)
	f = open(img_ver_path,'r')
	binList =[]
	for line in f:
		if( not line.strip()):
			continue

		if(format1.match(line)):
			bin_name = line[line.index("[")+1:line.index("]")]
			binList.append(bin_name)

	#print keyList
	f.close()
	return binList

def getImgVerGroup(img_name):
	img_ver = 0
	img_group = 0
	targetLine = 0

	f = open(img_ver_path,'r')

	pattern0= "\["
	format0 = re.compile(pattern0)
	pattern1= "\["+img_name+"\]"
	format1 = re.compile(pattern1)
	pattern2= "img_ver[\w]*"
	format2 = re.compile(pattern2)
	pattern3= "img_group[\w]*"
	format3 = re.compile(pattern3)
	for line in f:
		if( not line.strip()):
			continue
		#print line
		if(format0.match(line)):
			if(format1.match(line)):
				#print img_name
				targetLine = 1
			else:
				targetLine = 0;
		elif(format2.match(line)):
			if(targetLine == 1):
				img_ver = line.split("=")[1].strip()
		elif(format3.match(line)):
			if(targetLine == 1):
				img_group = line.split("=")[1].strip()
		#print targetLine
	f.close()

	return img_ver,img_group


def checkBinType(bin):
	isRaw = 0
	isMD = 0
	binFile = os.path.join(out, bin+".img")
	if not os.path.exists(binFile):
		binFile = os.path.join(out, bin+".bin")
	if not os.path.exists(binFile):
		isRaw =1
		isMD =0
		return isRaw, isMD;
	print binFile

	f = open(binFile, "rb")
	header_size = struct.calcsize(mkimage_header)
	fin = f.read(header_size)

	unpack_array = struct.unpack(mkimage_header,fin)
	f.close()

	magic_number = unpack_array[0]
	dsize		=  unpack_array[1]

	img_type    =  unpack_array[8]
	ext_magic = hex(unpack_array[5])

	#print "a"+str(ext_magic)+" "+str(hdr_size)+" "+str(img_type)

	if( cmp(unpack_array[0],int(IMG_HDR_MAGIC)) == 0):
		img_type_byte0 = img_type & 0xFF
		img_type_byte3 = (img_type >> 24) & 0xFF
		hdr_size =  unpack_array[6]

		if(img_type == 0):
			print "Raw IMG"
			isRaw = 1
		elif(img_type_byte3 ==1):
			#print "Is MD IMG"
			if(img_type_byte0 == 0):
				print "MD IMG:LTE"
				isRaw = 1
				isMD = 1
			elif(img_type_byte0 == 1):
				print "MD IMG:C2K"
				isRaw = 1
				isMD =2

	else :
		print "Not Raw Img"
		isRaw = 0
	return isRaw, isMD


def genCert2Key(binList):
	print "Start gen cert2 key to " + cert2_key_dir

	for bin in binList:
		key = bin +"_privk2.pem"
		print "Gen "+ key
		copyFile(argDict["cert2_key_path"] , os.path.join(cert2_key_dir, key))

def gen_pubk_from_prvk(prvk, pubk):
    cmd = 'openssl rsa'
    cmd += ' -in ' + prvk
    cmd += ' -pubout > ' + pubk
    ret = subprocess.call(cmd, shell=True)
    return ret

def imge_name_to_part_name(img_name):
	if (re.match('md1rom', img_name)):
		part_name = 'md1img'
	elif (re.match('md3rom', img_name)):
		part_name = 'md3img'
	else:
		part_name = img_name

	return part_name

def genCert1(binList):

	isRaw= 0;
	isMD = 0;
	for bin in binList:
		sh_command="";
		cert2_key_path = os.path.join(cert2_key_dir, bin+"_privk2.pem")
		cert2_pubk_path = os.path.join(cert2_pubk_dir, bin+"_pubk2.pem")
		gen_pubk_from_prvk(cert2_key_path,cert2_pubk_path)

		img_ver, img_group = getImgVerGroup(bin)
		isRAW, isMD = checkBinType(bin)
		print "bin:"+str(bin)+" img_ver:"+str(img_ver)+" img_group: "+str(img_group)+" isRaw:"+str(isRAW)+" isMD: "+str(isMD)
		if ( isMD == 0):
			if (argDict["root_key_padding"]):
				sh_command = "python "+ resign_tool_path+ " type=cert1  privk="+argDict["cert1_key_path"] +" pubk="+cert2_pubk_path+" ver="+str(img_ver)+" group="+str(img_group)+" root_key_padding="+str(argDict["root_key_padding"])
			else:
				sh_command = "python "+ resign_tool_path+ " type=cert1  privk="+argDict["cert1_key_path"] +" pubk="+cert2_pubk_path+" ver="+str(img_ver)+" group="+str(img_group)
			tmp_out_path = out_cert1_path
		else:
			part_name = imge_name_to_part_name(bin)
			mdBin = os.path.join(out, part_name+".img")
			if( os.path.isfile(mdBin) ):
				print mdBin+" exist"
				if (argDict["root_key_padding"]):
					sh_command = "python "+ resign_tool_path+ " type=cert1md img="+mdBin+" privk="+argDict["cert1_key_path"] +" pubk="+cert2_pubk_path+" ver="+str(img_ver)+" group="+str(img_group)+" root_key_padding="+str(argDict["root_key_padding"])
				else:
					sh_command = "python "+ resign_tool_path+ " type=cert1md img="+mdBin+" privk="+argDict["cert1_key_path"] +" pubk="+cert2_pubk_path+" ver="+str(img_ver)+" group="+str(img_group)
				tmp_out_path = out_cert1md_path
			else:
				print mdBin+" Not exist"
				print"Bypass md image cert1 Gen!"
		print bin

		if (sh_command is not ""):
			print sh_command
			executeShell(sh_command)
			cert1_path = os.path.join(cert1_dir, bin+"_cert1.der")
			print cert1_path
			copyFile(tmp_out_path,cert1_path)
		print "--------"


def main():

	if(len(sys.argv) < 3):
		print "SecureGen.py <platform> cert1_key_path=<key_path> cert2_key_path=<key_path> root_key_padding=<padding_type>"
		print "For security2.0:"
		print "    e.g. Gen cert1"
		print "         SecureGen.py mt6797 cert1_key_path=privk1.pem"
		print "    e.g. Gen cert2 key"
		print "         SecureGen.py mt6797 cert2_key_path=privk2.pem"
		print "    e.g. Gen cert1 and gen cert2 key"
		print "         SecureGen.py mt6797 cert1_key_path=privk1.pem cert2_key_path=privk2.pem"
		print "For security2.1:"
		print "    e.g. Gen cert1"
		print "         SecureGen.py mt6799 cert1_key_path=privk1.pem root_key_padding=pss"
		print "    e.g. Gen cert2 key"
		print "         SecureGen.py mt6799 cert2_key_path=privk2.pem"
		print "    e.g. Gen cert1 and gen cert2 key"
		print "         SecureGen.py mt6799 cert1_key_path=privk1.pem cert2_key_path=privk2.pem root_key_padding=pss"
		sys.exit()
	parseArg(sys.argv)

	setPath()
	createFolder()
	binList = getBinList()

	if(argDict["cert2_key_path"] !=0) :
		if( os.path.isfile(argDict["cert2_key_path"]) ):
			genCert2Key(binList)
		else:
			print "cert2_key not exists, please check cert2 key path!"

	if(argDict["cert1_key_path"] !=0):

		if( os.path.isfile(argDict["cert1_key_path"]) ):
			genCert1(binList)
		else:
			print "cert1_key not exists, please check cert1 key path!"

	print "Gen pass!"
	sys.exit()


if __name__ == '__main__':
	main()
