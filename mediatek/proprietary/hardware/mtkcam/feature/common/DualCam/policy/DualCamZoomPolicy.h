/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_DUALCAMZOOM_POLICY_H_
#define _MTK_HARDWARE_MTKCAM_DUALCAMZOOM_POLICY_H_

#include <vector>
#include <future>

#include <mtkcam/feature/DualCam/IDualCamPolicy.h>
#include <mtkcam/feature/DualCam/IFrame3AControl.h>
#include <mtkcam/feature/DualCam/DualCam.Common.h>
#include <utils/Mutex.h>

using namespace std;
using namespace android;

#define DISABLE_AAA_SYNC (1)
#define ENABLE_AAA_SYNC  (2)

namespace NSCam
{
class DualCamZoomPolicy : public IDualCamPolicy
{
    public:
        static MINT32                    totalRefCount;
        MINT32                           mRefCount;

        DualCamZoomPolicy();
        ~DualCamZoomPolicy();

        // ************* IDualCamPolicy interface **************************************
    public:
        MVOID setCameraOpenIds(vector<MINT32> ids);
        MVOID setObject(void* obj, MINT32 arg1);
        MVOID setParameter(std::string str);
        MVOID addCallBack(sp<ISyncManagerCallback> Cb);

        MVOID updateSetting(
            MINT32 const i4OpenId,
            IMetadata* appMetadata, IMetadata* halMetadata,
            SyncManagerParams &syncParams);

        MVOID updateAfterP1(MINT32 const i4OpenId, MUINT32 const frameNo,
                            void* obj, MINT32 arg1);

        MVOID updateFeatureMask(
                            MUINT32 &featureMask) override;

        // lock/unlock the state changing
        MVOID lock(void* arg1);
        MVOID unlock(void* arg1);

        MINT32 getPreviewCameraId(MINT32 *dropOthers);

        // ************* internal ******************************************************
    private:
        vector<MINT32>      mOpenIds;
        sp<IFrame3AControl> mIFrame3AControl;

        MRect               mActiveArrayWide;
        MRect               mActiveArrayTele;

        MUINT32             mPreviousZoomRatio;
        MINT32              mWaitStable;
        MINT32              mWaitEnterLP;
        MINT32              mWaitEnterAct;
        MTK_SYNC_CAEMRA_STATE    mWideState;
        MTK_SYNC_CAEMRA_STATE    mTeleState;
        MINT32              mPrevCamID;
        MINT32              mLockStatus;
        sp<ISyncManagerCallback> mMgrCb;
        MTK_DUAL_CAM_AAA_SYNC_STATE    mSet3ASyncMode;

        MINT32              mAFLowCount;
        MINT32              mAFHighCount;
        MINT32              mAELowCount;
        MINT32              mAEHighCount;
        MINT32              mTeleAEHighCount;
        MINT32              mAAAForceCamId;
        MINT32              mAFStrategy;
        MINT32              mAFTeleDac;
        MINT32              mAFTeleState;
        MINT32              mAETeleLv;

        MINT32              mSetSensorStatus;
        MINT32              mTGNum;

        MINT32              mAFDACHighTh;
        MINT32              mAFDACLowTh;
        MINT32              mAFPrevDAC;
        MINT32              mSkipAAACheck;

        mutable Mutex       mLock;
        // fov online calibration
        MINT32              mFovOnlineStatus;
        MINT32              mthresholdZoomRatio;
        MINT32              mthresholdThermal;
        MINT32              mthresholdDACWide;
        MINT32              mthresholdDACTele;
        MINT32              mthresholdISO;
        MINT32              mthresholdExpTime;
        // debug
        MINT32              mLogLevel;
        MINT32              mForceState;
        MINT32              mTeleLowISO;
        MINT32              mTeleHighISO;
        // for strobe
        MINT32              mPauseAFCamId;
        MINT32              mDoCapCamId;
        MINT32              mResumeAFCount;

        static              std::future<void> tPolicySetting;

        enum
        {
            DUALCAM_FORCE_ACTIVE = 1 << 0,
            DUALCAM_FORCE_NO_DROP = 1 << 1,
            DUALCAM_FORCE_NO_SYNCMGR_SYNC = 1 << 2,   // don't send frame sync hint to SyncManger
            DUALCAM_FORCE_NO_FOV_ONLINE = 1 << 3,
            DUALCAM_FORCE_NO_FOV_RULE_CHECK = 1 << 4,
        };

        enum
        {
            DUALCAM_FOV_ONLINE_WIDE = 1 << 0,
            DUALCAM_FOV_ONLINE_TELE = 1 << 1,
        };

        struct AAAInfo
        {
            MINT32 ZoomDrop;
            MINT32 AAAJudgeId;
            MINT32 is4KVideo;
        };

        struct SlaveInfo
        {
            MINT32 Id;
            MINT32 IsStandby;
        };

        struct SlaveInfo mSlaveInfo;


        void                            checkStallPipe(MINT32 const openId, AAAInfo info, MINT32& dropReq);
        MBOOL                           checkFOVOnline(MINT32 const openId, MUINT32 zoomRatio, MINT32 IsRec);

        status_t                        changeCameraMode(
            MINT32  const               openId,
            MINT32                      type,
            MBOOL                       enableAAASync,
            MBOOL                       enableHWSync,
            MINT32                      minFps,
            MINT32                      maxFps,
            IMetadata*                  appMetadata,
            IMetadata*                  halMetadata
        );
        status_t                        setMasterCamera(
            MINT32  const               openId,
            MINT32  const               slaveId,
            IMetadata*                  halMetadata);
        status_t                        setDropReqAndStandby(
            MINT32  const               openId,
            MINT32  const               drop,
            MINT32                      &setStatus,
            IMetadata*                  halMetadata);
        status_t                        setDropReq(
            MINT32  const               openId,
            MINT32  const               drop,
            MINT32  const               setStatus,
            IMetadata*                  halMetadata);
        status_t                        updateStandbyMode(
            MINT32  const               openId,
            MINT32                      &setStatus,
            IMetadata*                  halMetadata);
        MINT32                          selectPrevCamBy3A(
            MINT32  const               openId,
            MINT32  const               swtichTele,
            MTK_SYNC_CAEMRA_STATE  const    wideState,
            MTK_SYNC_CAEMRA_STATE  const    teleState);
        void                            changeCameraState(
            MINT32 const                openId,
            AAAInfo                     info,
            MINT32&                     dropReq,
            IMetadata*                  appMetadata,
            IMetadata*                  halMetadata,
            MTK_SYNC_CAEMRA_STATE&      state);
        status_t                        updateTwinMode(
            MINT32  const               openId,
            IMetadata*                  halMetadata);

};
} // NSCam

#endif // _MTK_HARDWARE_MTKCAM_DUALCAMZOOM_POLICY_H_
