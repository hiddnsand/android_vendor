//#define LOG_TAG "FpsPolicy"

#include <cutils/atomic.h>
#include <cutils/log.h>
#include <cutils/properties.h>

#include "dfps/FpsPolicy.h"

#define UNUSED(param) do { (void)(param); } while(0)

namespace android {

inline uint64_t getUniqueSequence()
{
    static volatile int32_t nextSequence = 0;
    uint64_t sequence = static_cast<uint64_t>(getpid()) << 32;
    sequence |= static_cast<uint32_t>(android_atomic_inc(&nextSequence));
    return sequence;
}

FpsPolicyInfo::FpsPolicyInfo()
    : sequence(0)
    , fps(-1)
    , pid(0)
    , api(FpsPolicy::API_UNKNOWN)
    , mode(FpsPolicy::MODE_DEFAULT)
    , targetPid(-1)
    , glContextId(0)
{
}

FpsPolicy::FpsPolicy(int api, String8& name)
    : mInit(false)
    , mName("")
    , mChannel(NULL)
    , mLooper(NULL)
    , mCb(NULL)
    , mStop(true)
{
    memset(&mPanelInfo, 0, sizeof(PanelInfo));
    mState = assertStateLocked(api, name);
    UNUSED(mInit);
}

FpsPolicy::~FpsPolicy()
{
}

status_t FpsPolicy::assertStateLocked(int api, String8& name)
{
    UNUSED(api);
    UNUSED(name);
    return NO_ERROR;
}

status_t FpsPolicy::setFps(int32_t fps, int32_t mode, int32_t targetPid, uint64_t glContextId)
{
    status_t res = NO_ERROR;
    UNUSED(fps);
    UNUSED(mode);
    UNUSED(targetPid);
    UNUSED(glContextId);

    return res;
}

status_t FpsPolicy::getPanelInfoNumber(int32_t *num)
{
    status_t res = NO_ERROR;
    UNUSED(num);
    return res;
}

status_t FpsPolicy::getPanelInfoNumberLocked(int32_t *num)
{
    status_t res = NO_ERROR;
    UNUSED(num);

    return res;
}

status_t FpsPolicy::getFpsRange(int32_t index, FpsRange *range)
{
    status_t res = NO_ERROR;
    UNUSED(index);
    UNUSED(range);

    return res;
}

void FpsPolicy::updateState(int32_t flag)
{
    UNUSED(flag);
}

status_t FpsPolicy::connectServiceLocked()
{

    return NO_ERROR;
}

int32_t FpsPolicy::isValidFpsLocked(int32_t fps, int32_t mode)
{
    int res = 0;
    UNUSED(fps);
    UNUSED(mode);

    return res;
}

int FpsPolicy::cb_fpsRequestReceiver(int fd, int events, void *data)
{
    int res = 0;
    UNUSED(fd);
    UNUSED(events);
    UNUSED(data);
    return res;
}

int FpsPolicy::fpsRequestReceive(int /*fd*/, int /*events*/)
{
    return 1;
}

bool FpsPolicy::threadLoop()
{
    do {
        {
            Mutex::Autolock l(mLock);
            if (mStop) {
                return false;
            }
            Condition cond;
            cond.wait(mLock);
        }
    } while (true);
    return true;
}

void FpsPolicy::onFirstRef()
{
}

status_t FpsPolicy::registerFpsRequestListener(const sp<FpsRequestListener>& cb)
{
    status_t res = NO_ERROR;
    UNUSED(cb);
    return res;
}

void FpsPolicy::cancelFpsRequestListener()
{
}

const char* FpsPolicy::getApiString(int32_t api)
{
    char const *apiString[FpsPolicy::API_MAXIMUM] = {
        "GIFT",
        "VIDEO",
        "RRC_INPUT",
        "RRC_VIDEO",
        "THERMAL",
        "LOADING",
        "WHITELIST",
    };

    if (api >= API_MAXIMUM) {
        return NULL;
    }

    return apiString[api];
}

const char* FpsPolicy::getModeString(int32_t mode)
{
    char const *apiString[FpsPolicy::MODE_MAXIMUM] = {
        "DEFAULT",
        "FRR",
        "ARR",
        "INTERNAL_SW"
    };

    if (mode >= MODE_MAXIMUM) {
        return NULL;
    }

    return apiString[mode];
}

};
