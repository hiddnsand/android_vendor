/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.os.StatFs;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.content.Context;
import android.widget.EditText;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.os.PowerManager;
import java.lang.System;

import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.cameranew.AutoCalibration;
import com.mediatek.engineermode.cameranew.Camera;
import com.mediatek.storage.StorageManagerEx;

public class CameraNewTest extends ActivityInstrumentationTestCase2<AutoCalibration> {
    private static final String TAG = "CameraNewTest";
    private Instrumentation mInst = null;
    private Activity mActivity = null;
    private Context mContext = null;
    private Solo mSolo = null;
    private ListView mLvStartPreview = null;
    private static final long MIN_STORAGE = 10 * 1024 * 1024;
    private PowerManager.WakeLock mWakeLock = null;
    private Spinner mSensorSp = null;

    public CameraNewTest() {
        super("com.mediatek.engineermode", AutoCalibration.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        mInst = getInstrumentation();
        mContext = mInst.getTargetContext();
        mActivity = getActivity();
        mSolo = new Solo(mInst, mActivity);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    @BasicFuncAnnotation
    public void test01_precondition() {
        Log.d("@M_" + TAG, "testCase01Precondition");
        assertNotNull(mInst);
        assertNotNull(mActivity);
        assertNotNull(mSolo);
        mSolo.assertCurrentActivity("current should be AutoCalibration activity",
                AutoCalibration.class);
        Log.d("@M_" + TAG, "testCase01Precondition pass");
    }

    private void previewAndCapture() {
        mSolo.assertCurrentActivity("current should be AutoCalibration activity",
                AutoCalibration.class);
        while (mSolo.scrollDown())
            ;
        String previewTag = mContext.getResources().getStringArray(R.array.auto_calib_capture)[0];
        mSolo.clickOnText(previewTag);
        mInst.waitForIdleSync();
        mSolo.sleep(200);
        mSolo.assertCurrentActivity("current should be Camara activity", Camera.class);
        Button btn = (Button) mSolo.getView("capture_btn");
        assertNotNull(btn);
        mSolo.clickOnButton(btn.getText().toString());
        mInst.waitForIdleSync();
    }

    @BasicFuncAnnotation
    public void test02_previewAndCapture() {
        Log.d("@M_" + TAG, "testCase02CameraPreview");
        mInst.waitForIdleSync();
        previewAndCapture();
        Button btn = (Button) mSolo.getView("capture_btn");
        while (!btn.isEnabled()) {
            mSolo.sleep(1000);
        }
        mSolo.goBack();
        Log.d("@M_" + TAG, "testCase02CameraPreview pass");
    }

    // @BasicFuncAnnotation
    // public void testCase03CameraCapture() {
    // Log.d("@M_" + TAG, "testCase03CameraCapture");
    // mInst.waitForIdleSync();
    // while (mSolo.scrollDown());
    // String previewTag =
    // mContext.getResources().getStringArray(R.array.auto_calib_capture)[0];
    // mSolo.clickOnText(previewTag);
    // mInst.waitForIdleSync();
    // mSolo.clickOnButton(mActivity.getString(R.string.capture_picture));
    // mSolo.sleep(200);
    // Button btn = (Button)
    // mSolo.getCurrentActivity().findViewById(R.id.capture_btn);
    // while (!btn.isEnabled()) {
    // mSolo.sleep(500);
    // }
    // mSolo.sleep(500);
    // mSolo.goBack();
    // Log.d("@M_" + TAG, "testCase03CameraCapture");
    // }

    @UiAnnotation
    public void test03_evCalibration() {
        Log.d("@M_" + TAG, "test03_evCalibration");
        mSolo.assertCurrentActivity("current should be AutoCalibration activity",
                AutoCalibration.class);

        String targetTag = mSolo.getString("camera_ev_calibration");

        // to visible EV Calibration Text
        while (mSolo.scrollDown())
            ;
        mSolo.scrollUp();

        if (!EmOperate.enterItem(mSolo, targetTag, "Camera"))
            return;

        mSolo.clickOnButton(0);
        mInst.waitForIdleSync();
        mSolo.enterText(0, "5");
        mSolo.clickOnButton(mContext.getResources().getString(android.R.string.ok));
        mInst.waitForIdleSync();
        mSolo.sleep(200);
        mSolo.clickOnButton(mContext.getResources().getString(android.R.string.ok));
        mInst.waitForIdleSync();
        mSolo.assertCurrentActivity("current should be Camera activity", Camera.class);
        mSolo.goBack();
        Log.d("@M_" + TAG, "test03_evCalibration pass");
    }

    @UiAnnotation
    public void test04_subSensor() {
        Log.d("@M_" + TAG, "test04_subSensor");
        mSolo.assertCurrentActivity("current should be AutoCalibration activity",
                AutoCalibration.class);
        Activity activity = mSolo.getCurrentActivity();

        mSensorSp = (Spinner) mSolo.getView("auto_clibr_camera_sensor_spnr");

        int count = mSensorSp.getAdapter().getCount();
        assertTrue(count > 0);
        if (count == 1) {
            return;
        }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSensorSp.setSelection(1);
            }
        });
        mSolo.sleep(200);
        test02_previewAndCapture();
        mSolo.goBack();
        Log.d("@M_" + TAG, "test04_subSensor pss");
    }

    @BasicFuncAnnotation
    public void test05_cameraNoAvailStorage() {
        Log.d("@M_" + TAG, "test05_cameraNoAvailStorage");
        String str = "for em camera auto test string!"
       + "Copyright Statement:This software/firmware"
       + " and related documentation MediaTek Softwareare* protected under relevant "
       + "copyright laws. The information contained herein* is confidential and proprietary"
       + " to MediaTek Inc. and/or its licensors.* Without the prior written permission of "
       + "MediaTek inc. and/or its licensors,* any reproduction, modification, use or "
       + "disclosure of MediaTek Software,* and information contained herein, in whole "
       + "or in part, shall be strictly prohibited. MediaTek Inc. (C) 2010. All rights "
       + "reserved** BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES "
       + "AND AGREES* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS (MEDIATEK SOFTWARE)"
       + "* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON* "
       + "AN AS-IS BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,* EXPRESS"
       + " OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED OF* MERCHANTABILITY,"
       + " FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.* NEITHER DOES MEDIATEK PROVIDE"
       + " ANY WARRANTY WHATSOEVER WITH RESPECT TO THE* SOFTWARE OF ANY THIRD PARTY WHICH MAY "
       + "BE USED BY, INCORPORATED IN, OR* SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER "
       + "AGREES TO LOOK ONLY TO SUCH* THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. "
       + "RECEIVER EXPRESSLY ACKNOWLEDGES* THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN"
       + " FROM ANY THIRD PARTY ALL PROPER LICENSES* CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK "
       + "SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEKSOFTWARE RELEASES MADE TO RECEIVER'S "
       + "SPECIFICATION OR TO CONFORM TO A PARTICULARSTANDARD OR OPEN FORUM. RECEIVER'S SOLE "
       + "AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE ANCUMULATIVE LIABILITY WITH RESPECT TO "
       + "THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,AT MEDIATEK'S OPTION, TO REVISE OR "
       + "REPLACE THE MEDIATEK SOFTWARE AT ISSUE,OR REFUND ANY LICENSE FEES OR SERVICE"
       + " CHARGE PAID BY RECEIVER TOMEDIATEK FOR SUCH MEDIATEK  AT ISSUE.The following"
       + " software/firmware and/or related documentation have been modified"
       + " by MediaTek Inc. All revisions are subject to any receiver'sapplicable license "
       + "agreements with MediaTek Inc.";

        mWakeLock = ((PowerManager) mInst.getContext().getSystemService(Context.POWER_SERVICE))
                .newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP,
                        TAG);
        mSolo.sleep(100);
        mWakeLock.acquire();
        String sdPath = StorageManagerEx.getDefaultPath();
        BufferedWriter writer = null;
        File file = null;
        try {
            file = new File(sdPath + "/EMCameraAutoTest.dat");
            writer = new BufferedWriter(new FileWriter(file));
            long preAvailabeSpace = getSdAvailableSpace(sdPath);
            while (preAvailabeSpace - getSdAvailableSpace(sdPath) < MIN_STORAGE) {
                writer.write(str);
            }
        } catch (IOException e) {
            Log.d("@M_" + "IOException:", e.getMessage());
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    Log.d("@M_" + "IOException:", e.getMessage());
                }
            }
            file.delete();
        }
        test02_previewAndCapture();
        mWakeLock.release();
        Log.d("@M_" + TAG, "test05_cameraNoAvailStorage pass");
    }

    private long getSdAvailableSpace(String sdPath) {
        StatFs stat = new StatFs(sdPath);
        return stat.getAvailableBlocks() * (long) stat.getBlockSize();
    }

    @BasicFuncAnnotation
    public void test06_aeBracket() {
        Log.d("@M_" + TAG, "test06_aeBracket");
        mInst.waitForIdleSync();
        // take capture
        while (mSolo.scrollDown())
            ;
        mSolo.scrollUp();

        Spinner aeSpinner = (Spinner) mSolo.getView("spiner_auto_clibr_ae_mode");
        mSolo.clickOnView(aeSpinner);
        mSolo.clickOnText(aeSpinner.getAdapter().getItem(1).toString());
        EditText etMinAe = (EditText) mSolo.getView("et_auto_clibr_ae_minus");
        mSolo.clearEditText(etMinAe);
        mSolo.enterText(etMinAe, "0");
        mSolo.scrollDown();
        EditText customPara0 = (EditText) mSolo.getView("camera_custom_param_0_edit");
        mSolo.enterText(customPara0, "ae");
        long timeCapture = System.currentTimeMillis();
        Log.d("@M_" + TAG, "timeCapture: " + timeCapture);
        previewAndCapture();
        mSolo.sleep(18 * 1000);
        Button btn = (Button) mSolo.getView("capture_btn");
        while (!btn.isEnabled()) {
            mSolo.sleep(100);
        }
        mSolo.goBack();
        // verify file generated
        String imagePath = StorageManagerEx.getDefaultPath() + "/DCIM/CameraEM/AE/EvBracket/";
        File[] files = new File(imagePath).listFiles(new FileFilter() {
            public boolean accept(File file) {
                if (file.lastModified() - timeCapture > 20 * 1000
                        || file.lastModified() <= timeCapture) {
                    return false;
                }
                Log.d("@M_" + TAG, file.getPath() + " timeCapture: " + file.lastModified());
                if (file.getPath().endsWith(".hwtbl") || file.getPath().endsWith(".sdblk")
                        || file.getPath().endsWith(".bin") || file.getPath().endsWith(".jpg")
                        || file.getPath().endsWith(".txt") || file.getPath().endsWith(".raw")) {
                    return true;
                } else {
                    return false;
                }
            }
        });
        Log.d("@M_" + TAG, "files count: " + files.length);
        assertTrue("test06_aeBracket: no files generated!", files.length > 0);
        boolean exist = false;
        String suffixList[] = new String[] { ".jpg", ".txt", ".raw", ".hwtbl", ".sdblk",
                ".bin" };
        for (String suffix : suffixList) {
            for (File file : files) {
                if (file.getPath().endsWith(suffix)) {
                    exist = true;
                    break;
                }
            }
            if (!exist)
                fail("no files gen for " + suffix);
            exist = false;
        }

        mSolo.goBack();
        Log.d("@M_" + TAG, "test06_aeBracket pass");
    }

}
