package com.mediatek.ims.internal;

import android.content.Context;
import android.telephony.Rlog;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.telephony.SubscriptionManager.OnSubscriptionsChangedListener;
import android.os.AsyncResult;
import android.os.Looper;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import com.mediatek.ims.ImsEventDispatcher;
import com.mediatek.ims.ImsAdapter.VaEvent;
import com.mediatek.ims.ImsAdapter.VaSocketIO;
import com.mediatek.ims.ril.ImsRILAdapter;
import com.mediatek.ims.ril.ImsCommandsInterface;
import com.mediatek.ims.internal.ImsDataSynchronizer.ImsBearerRequest;
import com.android.internal.telephony.PhoneConstants;

import java.util.Arrays;

public class ImsDataTracker implements ImsEventDispatcher.VaEventDispatcher {

    private String TAG = ImsDataTracker.class.getSimpleName();
    //private boolean mIsMdAutoActivate = false;
    private Context mContext;
    private int mPhoneNum;
    private MdCapability mMdCapability;
    public enum MdCapability {
        LEGACY,
        AUTOSETUPIMS
    }

    //92MD
    private DataDispatcher[] mDispatchers;
    private VaSocketIO mSocket;

    public ImsDataTracker(Context context, VaSocketIO IO) {
        mPhoneNum = TelephonyManager.getDefault().getPhoneCount();
        mDispatchers = new DataDispatcher[mPhoneNum];
        for(int i = 0; i < mPhoneNum; i++){
            mDispatchers[i] = new DataDispatcher(context, this, i);
        }
        mSocket = IO;
        mMdCapability = MdCapability.LEGACY;
        SubscriptionManager.from(context).addOnSubscriptionsChangedListener(
                mOnSubscriptionsChangedListener);
    }

    public void enableRequest(int phoneId) {
        logd("receive enableRequest on phone: " + phoneId);
        mDispatchers[phoneId].enableRequest(phoneId);
    }

    public void disableRequest(int phoneId) {
        logd("receive disableRequest on phone: " + phoneId);
        mDispatchers[phoneId].disableRequest(phoneId);
    }

    /**
     * Handle Data related event from IMCB.
     *
     * @param VaEvent event from IMCB.
     */
    public void vaEventCallback(VaEvent event) {
        logd("send event" + event.getRequestID() + " to phone " + event.getPhoneId());
        mDispatchers[event.getPhoneId()].vaEventCallback(event);
    }

    public void sendVaEvent(VaEvent event) {
        mSocket.writeEvent(event);
    }

    //93MD
    private ImsDataSynchronizer[] mSynchronizers;
    private ImsCommandsInterface [] mImsRILAdapters = null;

    public ImsDataTracker(Context context, ImsCommandsInterface [] adapters) {
        mPhoneNum = TelephonyManager.getDefault().getPhoneCount();
        mSynchronizers = new ImsDataSynchronizer[mPhoneNum];
        mContext = context;
        mImsRILAdapters = adapters;

        for(int i = 0; i < mPhoneNum; i++){
            mSynchronizers[i] = new ImsDataSynchronizer(context, this, i);
            mImsRILAdapters[i].registerForBearerActivation(
                    mdHander, ImsDataSynchronizer.EVENT_CONNECT, null);
            mImsRILAdapters[i].registerForBearerDeactivation(
                    mdHander, ImsDataSynchronizer.EVENT_DISCONNECT, null);
            mImsRILAdapters[i].registerForBearerInit(
                    mdHander, ImsDataSynchronizer.EVENT_MD_RESTART, null);
        }
        mMdCapability = MdCapability.AUTOSETUPIMS;
        SubscriptionManager.from(mContext).addOnSubscriptionsChangedListener(
                mOnSubscriptionsChangedListener);
    }

    private Handler mdHander = new Handler() {
        @Override
        synchronized public void handleMessage(Message msg) {
            switch (msg.what) {
                case ImsDataSynchronizer.EVENT_CONNECT:
                    onImsBearerChanged((AsyncResult) msg.obj,
                            ImsDataSynchronizer.EVENT_CONNECT);
                    break;
                case ImsDataSynchronizer.EVENT_DISCONNECT:
                    onImsBearerChanged((AsyncResult) msg.obj,
                            ImsDataSynchronizer.EVENT_DISCONNECT);
                    break;
                case ImsDataSynchronizer.EVENT_MD_RESTART:
                    onMdRestart((AsyncResult) msg.obj);
                    break;
                default:
                    loge("not handle the message: " + msg.what);
                    break;
            }
        }
    };

    private void onImsBearerChanged(AsyncResult ar, int event) {
        logd("onImsBearerChanged");
        String[] bearerInfo = (String[]) ar.result;
        if(bearerInfo != null) {
            if(bearerInfo.length == 3) {
                logd(Arrays.toString(bearerInfo));
                int phoneId = Integer.parseInt(bearerInfo[0]);
                int aid = Integer.parseInt(bearerInfo[1]);
                String capability = bearerInfo[2];
                mSynchronizers[phoneId].notifyMdRequest(
                            new ImsBearerRequest(aid, phoneId, event, capability));
            } else {
                loge("parameter format error: " + Arrays.toString(bearerInfo));
            }
        } else {
            loge("parameter is NULL");
        }
    }

    private void onMdRestart(AsyncResult ar) {
        logd("onMdRestart");
        int phoneId = ((int[]) ar.result)[0];
        logd("onMdRestart, reset phone = " + phoneId + " connection state");
        mSynchronizers[phoneId].notifyMdRestart();
    }

public void responseBearerConfirm(int event, int aid, int status, int phoneId) {
        logd("send to MD, aid:" + aid + ", status:" + status + ", phoneId:" + phoneId);
        switch (event) {
            case ImsDataSynchronizer.EVENT_CONNECT:
                mImsRILAdapters[phoneId].responseBearerActivationDone(aid, status, null);
                break;
            case ImsDataSynchronizer.EVENT_DISCONNECT:
                mImsRILAdapters[phoneId].responseBearerDeactivationDone(aid, status, null);
                break;
        }
    }

    private final OnSubscriptionsChangedListener mOnSubscriptionsChangedListener =
            new OnSubscriptionsChangedListener() {
        @Override
        public void onSubscriptionsChanged() {
            logd("onSubscriptionsChanged");
            switch(mMdCapability) {
                case LEGACY:
                    for(int i = 0; i < mPhoneNum; i++) {
                        mDispatchers[i].onSubscriptionsChanged();
                    }
                    break;
                case AUTOSETUPIMS:
                    for(int i = 0; i < mPhoneNum; i++) {
                        mSynchronizers[i].onSubscriptionsChanged();
                    }
                    break;
            }
        }
    };


    private void logd(String s) {
        Rlog.d(TAG, s);
    }

    private void logi(String s) {
        Rlog.i(TAG, s);
    }

    private void loge(String s) {
        Rlog.e(TAG, s);
    }
}
