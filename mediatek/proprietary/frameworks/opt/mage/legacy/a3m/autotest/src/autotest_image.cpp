/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Texture unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/image.h>                  /* for Image                          */
#include <autotest_common.h>            /* for AutotestTest                   */
#include <memfilesource_autotest.h>     /* for create_memfilesource_autotest  */

using namespace a3m;

namespace
{

  /*
   * Struct containing an image name and bounds.
   */
  struct CroppedImage
  {
    std::string name;
    A3M_INT32 left;
    A3M_INT32 top;
    A3M_INT32 width;
    A3M_INT32 height;

    CroppedImage(
      char const* name,
      A3M_INT32 left,
      A3M_INT32 top,
      A3M_INT32 width,
      A3M_INT32 height) :
      name(name),
      left(left),
      top(top),
      width(width),
      height(height)
    {
    }
  };

  A3M_CHAR8 const* TEST_IMAGE = "autotest#crop_full.png";
  A3M_INT32 const TEST_IMAGE_WIDTH = 16;
  A3M_INT32 const TEST_IMAGE_HEIGHT = 16;
  A3M_INT32 const TEST_IMAGE_CHANNEL_COUNT = 3;
  A3M_UINT8 const TEST_FIRST_PIXEL[] = {178, 70, 70};
  A3M_UINT8 const TEST_LAST_PIXEL[] = {78, 78, 201};

  CroppedImage const CROPPED_IMAGES[] =
  {
    CroppedImage("autotest#crop_full.png", 0, 0, 16, 16),
    CroppedImage("autotest#crop_middle.png", 4, 4, 8, 8),
    CroppedImage("autotest#crop_topleft.png", 0, 0, 4, 3),
    CroppedImage("autotest#crop_topright.png", 12, 0, 4, 3),
    CroppedImage("autotest#crop_bottomleft.png", 0, 13, 4, 3),
    CroppedImage("autotest#crop_bottomright.png", 12, 13, 4, 3)
  };

  /*
   * Test fixture.
   */
  struct A3mImageTest : public AutotestTest
  {
    StreamSource::Ptr source;

    A3mImageTest() :
      source(create_memfilesource_autotest())
    {
    }
  };

} // namespace

/*
 * Test manual construction of image.
 */
TEST_F(A3mImageTest, ManualConstruction)
{
  A3M_UINT8 const data[] = { 1, 2, 3, 4, 5, 6 };
  A3M_INT32 const width = 2;
  A3M_INT32 const height = 1;
  A3M_INT32 const channelCount = 2;

  Image::Ptr image(new Image(width, height, channelCount, data));

  EXPECT_EQ(width, image->width());
  EXPECT_EQ(height, image->height());
  EXPECT_EQ(channelCount, image->channelCount());

  A3M_UINT8 const* newData = image->data();

  ASSERT_FALSE(0 == newData);

  for (A3M_INT32 i = 0; i < width * height * channelCount; ++i)
  {
    EXPECT_EQ(data[i], newData[i]);
  }
}

/*
 * Test uninitialized manual construction of image.
 */
TEST_F(A3mImageTest, UninitializedConstruction)
{
  A3M_INT32 const width = 2;
  A3M_INT32 const height = 1;
  A3M_INT32 const channelCount = 2;

  Image::Ptr image(new Image(width, height, channelCount));

  EXPECT_EQ(width, image->width());
  EXPECT_EQ(height, image->height());
  EXPECT_EQ(channelCount, image->channelCount());

  ASSERT_FALSE(0 == image->data());
}

/*
 * Test passing of invalid parameters to constructor.
 */
TEST_F(A3mImageTest, InvalidConstruction)
{
  A3M_INT32 const width = 2;
  A3M_INT32 const height = 1;
  A3M_INT32 const channelCount = 2;

  {
    Image::Ptr image(new Image(-width, height, channelCount));
    EXPECT_EQ(0, image->data());
  }

  {
    Image::Ptr image(new Image(width, -height, channelCount));
    EXPECT_EQ(0, image->data());
  }

  {
    Image::Ptr image(new Image(-width, height, 0));
    EXPECT_EQ(0, image->data());
  }
}

/*
 * Test loading of image.
 */
TEST_F(A3mImageTest, Load)
{
  Image::Ptr image(new Image(*source->open(TEST_IMAGE)));

  EXPECT_EQ(TEST_IMAGE_WIDTH, image->width());
  EXPECT_EQ(TEST_IMAGE_HEIGHT, image->height());
  EXPECT_EQ(TEST_IMAGE_CHANNEL_COUNT, image->channelCount());

  A3M_UINT8 const* firstPixel = image->data();

  ASSERT_FALSE(0 == firstPixel);

  A3M_UINT8 const* lastPixel =
    firstPixel + (TEST_IMAGE_WIDTH * TEST_IMAGE_HEIGHT - 1) *
    TEST_IMAGE_CHANNEL_COUNT;

  // Just check the first and last pixels to check the image has loaded.
  for (A3M_INT32 i = 0; i < TEST_IMAGE_CHANNEL_COUNT; ++i)
  {
    ASSERT_EQ(TEST_FIRST_PIXEL[i], firstPixel[i]);
    ASSERT_EQ(TEST_LAST_PIXEL[i], lastPixel[i]);
  }
}

/*
 * Test cropping of images.
 */
TEST_F(A3mImageTest, Crop)
{
  Image::Ptr image(new Image(*source->open(TEST_IMAGE)));

  A3M_UINT8* srcData = image->data();

  ASSERT_FALSE(0 == srcData);

  A3M_INT32 count = static_cast<A3M_INT32>(
                      sizeof(CROPPED_IMAGES) / sizeof(CroppedImage));

  for (A3M_INT32 i = 0; i < count; ++i)
  {
    CroppedImage const& info = CROPPED_IMAGES[i];

    Image::Ptr cropped(new Image(*source->open(info.name.c_str())));

    A3M_UINT8* dstData = cropped->data();

    ASSERT_FALSE(0 == dstData);

    ASSERT_EQ(info.width, cropped->width());
    ASSERT_EQ(info.height, cropped->height());
    ASSERT_EQ(TEST_IMAGE_CHANNEL_COUNT, cropped->channelCount());

    for (A3M_INT32 y = 0; y < info.height; ++y)
    {
      for (A3M_INT32 x = 0; x < info.width; ++x)
      {
        for (A3M_INT32 c = 0; c < TEST_IMAGE_CHANNEL_COUNT; ++c)
        {
          A3M_INT32 srcIndex =
            TEST_IMAGE_CHANNEL_COUNT *
            (TEST_IMAGE_WIDTH * (info.top + y) + info.left + x) + c;
          A3M_UINT8 src = srcData[srcIndex];

          A3M_INT32 dstIndex =
            TEST_IMAGE_CHANNEL_COUNT * (info.width * y + x) + c;
          A3M_UINT8 dst = dstData[dstIndex];

          ASSERT_EQ(src, dst);
        }
      }
    }
  }
}
