/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

import java.util.List;

/**
 * DataConnectionEnabler is a helper to manage the Data connection on/off checkbox
 * preference. It turns on/off Data connection and ensures the summary of the
 * preference reflects the current state.
 */
public final class DataConnectionEnabler implements CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "DataConnectionEnabler";
    private static final int ALL_RADIO_OFF = 0;

    private final Context mContext;
    private Switch mSwitch;
    private IntentFilter mIntentFilter;

    private TelephonyManager mTelephonyManager;
    private PhoneStateListener mPhoneStateListener;
    private String mObserveSim1;
    private String mObserveSim2;
    private boolean mUpdateEnable = true;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
                updateSwitcherState();
            } else if (action.equals(TelephonyIntents.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED)) {
                updateSwitcherState();
            }
        }
    };

    private ContentObserver mDataConnectionObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            Log.d(TAG, "onChange selfChange=" + selfChange);
            if (!selfChange) {
                updateSwitcherState();
            }
        }
    };

    private void registerPhoneStateListener() {
        mPhoneStateListener = getPhoneStateListener();
        mTelephonyManager.listen(mPhoneStateListener,
                PhoneStateListener.LISTEN_SERVICE_STATE);
        Log.d(TAG, "Register registerPhoneStateListener");
    }

    private void unregisterPhoneStateListener() {
        mTelephonyManager.listen(mPhoneStateListener,
                PhoneStateListener.LISTEN_NONE);
        Log.d(TAG, "Register unregisterPhoneStateListener");
    }

    /**
     * Context method.
     * @param context context
     * @param switchs switch
     */
    public DataConnectionEnabler(Context context, Switch switchs) {
        mContext = context;
        mTelephonyManager = TelephonyManager.from(context);
        mSwitch = switchs;
        mIntentFilter = new IntentFilter(TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED);
        mIntentFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        mIntentFilter.addAction(TelephonyIntents.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED);
    }

    /**
     * use to resume switch status.
     */
    public void resume() {
        int subId = 0;
        registerPhoneStateListener();
        mContext.registerReceiver(mReceiver, mIntentFilter);

        List<SubscriptionInfo> si = SubscriptionManager.from(mContext)
             .getActiveSubscriptionInfoList();

        if (si != null && si.size() > 0) {
            for (int i = 0; i < si.size(); i++) {
                SubscriptionInfo subInfo = si.get(i);
                subId = subInfo.getSubscriptionId();
                mContext.getContentResolver().registerContentObserver(
                        Settings.Global.getUriFor(Settings.Global.MOBILE_DATA + subId),
                        true, mDataConnectionObserver);
            }
        }
        mSwitch.setOnCheckedChangeListener(this);
        updateSwitcherState();
    }

    /**
     * when pause,unregisterReceiver and changedListener.
     */
    public void pause() {
        unregisterPhoneStateListener();
        mContext.unregisterReceiver(mReceiver);
        mContext.getContentResolver().unregisterContentObserver(mDataConnectionObserver);
        mSwitch.setOnCheckedChangeListener(null);
    }
    /**
     * set if settings app can update mobile data switch.
     * @param flag if settings app can update mobile data switch
     */
    public void setUpdateSwitcherEnable(boolean flag) {
        Log.d(TAG, "setUpdateSwitcherEnable flag: " + flag);
        mUpdateEnable = flag;
    }
    /**
     * when switchbutton checked.
     * @param buttonView switchbutton
     * @param isChecked true if on
     */
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.d(TAG, "onCheckedChanged isChecked: " + isChecked);
        if (isChecked) {
            mTelephonyManager.setDataEnabled(true);
        } else {
            List<SubscriptionInfo> si = SubscriptionManager.from(mContext)
                .getActiveSubscriptionInfoList();

            if (si != null && si.size() > 0) {
                for (int i = 0; i < si.size(); i++) {
                    SubscriptionInfo subInfo = si.get(i);
                    int subId = subInfo.getSubscriptionId();
                    Log.d(TAG, "set subId = " + subId + "false");
                    mTelephonyManager.setDataEnabled(subId, false);
                }
            }
        }
    }

    protected void updateSwitcherState() {
        boolean enabled = isGPRSEnable(mContext);
        mSwitch.setEnabled(enabled);
        boolean dataEnable = mTelephonyManager.getDataEnabled();
        Log.d(TAG, "updateSwitcherState enalbed=" + enabled + ", dataEnable=" + dataEnable +
            ", isChecked=" + mSwitch.isChecked() + ", mUpdateEnable=" + mUpdateEnable);
        if (dataEnable != mSwitch.isChecked() && mUpdateEnable) {
           mSwitch.setChecked(dataEnable);
        }
    }

    private PhoneStateListener getPhoneStateListener() {
        return new PhoneStateListener() {
            @Override
            public void onServiceStateChanged(ServiceState state) {
                Log.d(TAG, "PhoneStateListener: onServiceStateChanged");
                updateSwitcherState();
            }
        };
    }

    /**
     * Returns whether is in airplance, radiooff, No sim card, all sim not ready.
     * @return is airplane or mms is in transaction
     * isRadioOff IS NOT  && (RADIO ON && SIM IS NOT SIM_INDICATOR_LOCKED)
     */
    static boolean isGPRSEnable(Context context) {
        boolean isAirplaneMode = Utils.isAirPlaneMode(context);
        boolean isAllRadioOff = Utils.isAllRadioOff(context);
        int simNumber = Utils.getSimNumber(context);
        boolean isRadioOn = !(isAirplaneMode || isAllRadioOff || (simNumber == 0));
        boolean isSlotRadioOnAndSimReady = isSlotRadioOnAndSimReady(context);
        Log.d(TAG, " isAirplaneMode = " + isAirplaneMode
                + ", isAllRadioOff = " + isAllRadioOff
                + ", simNumber = " + simNumber
                + ", isRadioOn = " + isRadioOn
                + ", isSlotRadioOnAndSimReady = " + isSlotRadioOnAndSimReady);
        return isRadioOn && isSlotRadioOnAndSimReady;
    }

    private static boolean isSlotRadioOnAndSimReady(Context context) {
        for (int simId = PhoneConstants.SIM_ID_1; simId < Utils.getSimNumber(context); simId ++) {
            if (Utils.isTargetSimRadioOn(simId) && (Utils.isSimStateReady(context, simId))) {
                return true;
            }
        }
        return false;
    }

}
