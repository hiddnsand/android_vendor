package com.mediatek.bluetooth.dtt;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.widget.Toast;
import com.nononsenseapps.filepicker.FilePickerActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by MTK01635 on 2015/4/23.
 *
 * TODO permission check ? 主要是 read, mtkbt 要 write ?
 *
 * http://stackoverflow.com/questions/5694933/find-an-external-sd-card-location
 * http://possiblemobile.com/2014/03/android-external-storage/
 *
 * http://stackoverflow.com/questions/14645189/android-get-list-of-all-available-storage
 * http://stackoverflow.com/questions/8133601/android-get-all-available-storage-devices
 */
public class Util {
    public static final String SRC_FILE = "btsnoop_hci.log";
    public static final String SRC_FILE_DIR = "/sdcard/mtklog/btlog/";
    public static final String DST_FILE_DIR = "/sdcard/mtklog/btlog/BTLOG/";
    public static final String FW_SRC_FILE_DIR = "/sdcard/mtklog/btlog/firmware_log/";
    public static final String FW_DST_FILE_DIR = "/sdcard/mtklog/btlog/BTLOG/firmware_log/";

    public static SharedPreferences getSettingPreference(Context context) {
        return context.getSharedPreferences(Const.PREF_NAME, Context.MODE_PRIVATE);
    }

    public static void openFileChooser(Activity activity, int requestCode){

        Intent i = new Intent(activity, FilePickerActivity.class);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);
        i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());
        activity.startActivityForResult(i, requestCode);
    }

/*
    public static boolean writeTestSituation(Context context, DataModel dataModel){

        // TODO error handling
        switch(dataModel.mTestSituation){
            case Const.TS_DEFAULT:
                Util.copyRawResToFile(context, R.raw.btsc_0, Const.SHARE_FILE_DIR + Const.BT_CONF_FILE);
                return true;
            case Const.TS_SQC:
                Util.copyRawResToFile(context, R.raw.btsc_1, Const.SHARE_FILE_DIR + Const.BT_CONF_FILE);
                return true;
            case Const.TS_DEBUG:
                Util.copyRawResToFile(context, R.raw.btsc_2, Const.SHARE_FILE_DIR + Const.BT_CONF_FILE);
                return true;
            case Const.TS_CUST:
                StringBuilder content = new StringBuilder()
                        .append("OverrideConf=").append(dataModel.mBtScCustPath).append("\r\n");
                Util.writeStringToFile(content.toString(),  Const.SHARE_FILE_DIR + Const.BT_CONF_FILE);
                return true;
            default:
                Log.w(Const.TAG, "invalid test situation:" + dataModel.mTestSituation);
                return false;
        }
    }

    private static void writeStringToFile(String content, String filename){
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename, false);
            out.write(content.getBytes("utf8"));
            out.flush();
        }
        catch(Exception ex){
            Log.w(Const.TAG, "writeStringToFile() file:" + filename + " failed!", ex);
        }
        finally {
            try {
                if (out != null)    out.close();
            }
            catch(Exception ex){
                Log.w(Const.TAG, "close file:" + filename + " failed!", ex);
            }
        }
    }

    public static void copyRawResToFile(Context context, int rawResId, String filename){

        InputStream in = context.getResources().openRawResource(rawResId);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename, false);
            byte[] buf = new byte[32];
            int read;
            while((read = in.read(buf)) > 0){
                out.write(buf, 0, read);
            }
        }
        catch(Exception ex){
            Log.w(Const.TAG, "copyToFile() file:" + filename + " failed!", ex);
        }
        finally {
            try {
                if (in != null)     in.close();
            }
            catch(Exception ex){
                Log.w(Const.TAG, "close raw res:" + rawResId + " failed!", ex);
            }
            try {
                if (out != null)    out.close();
            }
            catch(Exception ex){
                Log.w(Const.TAG, "close file:" + filename + " failed!", ex);
            }
        }
    }
*/
     public static void copyFile(Context context, String comepath, String gopath){
        try {
            File wantfile = new File(comepath);
            File newfile = new File(gopath);
            File fwfolder = new File(FW_SRC_FILE_DIR);

            InputStream in = new FileInputStream(wantfile);
            OutputStream out = new FileOutputStream(newfile);
            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();

            if (!fwfolder.exists()){
               Intent intent = new Intent("com.mediatek.mtklogger.BTLOG_MOVE");
               intent.putExtra("cmd_name", "path");
               intent.putExtra("cmd_target", "sdcard/mtklog/btlog/BTLOG");
               context.sendBroadcast(intent);
               Log.i(Const.TAG, "bt_hcilog move completed");
           }
        }
        catch(Exception ex){
            Log.w(Const.TAG, "copyFile() file: failed!", ex);
        }
            Log.i(Const.TAG, "copyFile completed");

        try {
            File fw = new File(FW_SRC_FILE_DIR);
            File[] file=fw.listFiles();
            String FW_FILE = null;
            File goPathfolder = new File(FW_DST_FILE_DIR);

            if (fw.exists()){
                if (!goPathfolder.exists()){
                    goPathfolder.mkdirs();
                }
                for ( int i = 0 ; i < file.length; i++) {
                     FW_FILE = file[i].getName();
                     Log.i(Const.TAG, "FW_FILE: " + FW_FILE);
                }

                File targetfwfile = new File(FW_DST_FILE_DIR+ FW_FILE);

                if (FW_FILE != null && !targetfwfile.exists()) {
                    copyFile(context, FW_SRC_FILE_DIR+ FW_FILE, FW_DST_FILE_DIR+ FW_FILE);
                    Log.i(Const.TAG, "copyFwFile completed");
                } else {
                    Intent intent = new Intent("com.mediatek.mtklogger.BTLOG_MOVE");
                    intent.putExtra("cmd_name", "path");
                    intent.putExtra("cmd_target", "sdcard/mtklog/btlog/BTLOG");
                    context.sendBroadcast(intent);
                    Log.i(Const.TAG, "all btlog move completed");
                }
            }
        }
        catch(Exception ex){
            Log.w(Const.TAG, "copyfwFile() file: failed!", ex);
        }
    }

    public static void moveFiles(Context context, String comePath, String goPath){
        try {
            //( new File(goPath)).mkdirs();
            File goPathfolder = new File(goPath);
            File a= new File(comePath);
            String[] file=a.list();

            File temp= null ;
            for ( int i = 1 ; i < file.length; i++) {
                if (comePath.endsWith(File.separator)){
                    temp= new File(comePath+file[i]);
                    Log.i(Const.TAG, "temp" + temp);
                }
                else
                {
                    temp= new File(comePath+File.separator+file[i]);
                    Log.i(Const.TAG, "else temp" + temp);
                }
                if (temp.isFile()){
                   if (!goPathfolder.exists()){
                       goPathfolder.mkdirs();
                   }
                   File go = new File(goPath + "/" +
                            (temp.getName()).toString());
                   temp.renameTo(go);
                   Log.i(Const.TAG, "temp renameTo" + go);
                }
                if (temp.isDirectory()){
                    if (!goPathfolder.exists()){
                        goPathfolder.mkdirs();
                    }
                    //moveFiles(context, comePath+ "/" +file[i],goPath+ "/" +file[i]);
                    moveFiles(context, comePath+file[i],goPath +file[i]);
                    Log.i(Const.TAG, "isDirectory moveFolder");
                }
            }
        }
        catch (Exception e) {
            Log.e("folder error", e.toString());
        }
            Log.i(Const.TAG, "moveFiles completed");
    }

    public static void deleteFiles(Context context, boolean reserve_curr){
        try {
            File hcifolder = new File(SRC_FILE_DIR);
            File fwfolder = new File(FW_SRC_FILE_DIR);

            if (hcifolder.exists()) {
                File[] fs = hcifolder.listFiles();
                for(File f: fs) {
                    if(reserve_curr && f.getName().equals(SRC_FILE))
                        continue;
                    if(f.isFile())
                        f.delete();
                }
            }

            if (fwfolder.exists()) {
                File[] fs = fwfolder.listFiles();
                for(File f: fs) {
                    if(reserve_curr && f.getName().contains("curr"))
                        continue;
                    if(f.isFile())
                        f.delete();
                }
            }
        }
        catch (Exception e) {
            Log.e("delete folder error", e.toString());
        }
        Log.i(Const.TAG, "deleteFiles completed");
   }

//    public static void snedCmdToMtkLogger(Context context, boolean start){
//        /**
//         * Enable:  adb shell am broadcast -a com.mediatek.mtklogger.ADB_CMD -e cmd_name start --ei cmd_target 7
//         * Disable: adb shell am broadcast -a com.mediatek.mtklogger.ADB_CMD -e cmd_name stop --ei cmd_target 7
//         * MobileLog: 1 / ModemLog: 2 / NetworkLog: 4 / MET: 8 / GPS: 16
//         * 參考: com.mediatek.mtklogger.utils.Utils.java > LOG_TYPE_SET
//         */
//        try {
//            Log.i(Const.TAG, "sendCmdToMtkLogger:" + start);
//            Intent intent = new Intent("com.mediatek.mtklogger.ADB_CMD");
//            intent.putExtra("cmd_name", start ? "start" : "stop");
//            intent.putExtra("cmd_target", 7);
//            context.sendBroadcast(intent);
//        }
//        catch(Exception ex){
//            Log.e(Const.TAG, "sendCmdToMtkLogger() error: ", ex);
//            Toast.makeText(context, "Switch MtkLogger Err:" + ex.getMessage(), Toast.LENGTH_LONG).show();
//        }
//    }

    public static void openMtkLogger(Context context){
        try {
            //Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.mediatek.mtklogger");
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName("com.mediatek.mtklogger", "com.mediatek.mtklogger.MainActivity"));
            context.startActivity(intent);
        }
        catch(Exception ex){
            Log.e(Const.TAG, "openMtkLogger() error: ", ex);
            Toast.makeText(context, "open MtkLogger Err:" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public static void openEngineerMode(Context context){
        try {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName("com.mediatek.engineermode", "com.mediatek.engineermode.EngineerMode"));
            context.startActivity(intent);
        }
        catch(Exception ex){
            Log.e(Const.TAG, "openEngineerMode() error: ", ex);
            Toast.makeText(context, "open EngMode Err:" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
