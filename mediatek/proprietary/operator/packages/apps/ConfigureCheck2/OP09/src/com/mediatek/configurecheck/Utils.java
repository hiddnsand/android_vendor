/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.configurecheck;

import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.mediatek.internal.telephony.IMtkTelephonyEx;
import com.mediatek.internal.telephony.worldphone.WorldPhoneUtil;

public class Utils {
    private static final String TAG = " Utils";
    public static boolean isCdma() {
        return SystemProperties.get("ro.boot.opt_c2k_support").equals("1") == true;
    }
    public static boolean isWorldMode = WorldPhoneUtil.isWorldModeSupport();

    public static boolean isA = "OP09".equals(SystemProperties.get("persist.operator.optr")) &&
                             "SEGDEFAULT".equals(SystemProperties.get("persist.operator.seg"));

    public static boolean isC = "OP09".equals(SystemProperties.get("persist.operator.optr")) &&
                             "SEGC".equals(SystemProperties.get("persist.operator.seg"));

    public static boolean is93Modem() {
        boolean is_support_93_md = "c6m_1rild".equals(SystemProperties.get("ro.mtk_ril_mode"))
                 ? true : false;
        CTSCLog.d(TAG, "This modem is 93 modem: " + is_support_93_md);
        return is_support_93_md;
    }
    public static String isSupportC2kLte() {
        return SystemProperties.get("ro.boot.opt_c2k_lte_mode");
    }
    public static Phone getCdmaPhone() {
        Phone mPhone = null;

        try {
            if (!is93Modem()) {
                String sCdmaSlotId = SystemProperties.get("persist.radio.cdma_slot", "1");
                CTSCLog.d(TAG, "getCdmaPhone(), sCdmaSlotId = " + sCdmaSlotId);
                int iCdmaSlotId = Integer.parseInt(sCdmaSlotId);
                mPhone = PhoneFactory.getPhone(iCdmaSlotId-1);
            } else {
                mPhone = PhoneFactory.getPhone(getCapabilitySim());
            }
        }catch (Exception e) {
             CTSCLog.e(TAG, "get cdma phone fail " + e.getMessage());
        }
        return mPhone;
    }
    public static int getCapabilitySim() {
        try {
            IMtkTelephonyEx iTelEx = IMtkTelephonyEx.Stub.asInterface(
                    ServiceManager.getService("phoneEx"));
            if (iTelEx != null) {
                int phoneId = iTelEx.getMainCapabilityPhoneId();
                CTSCLog.i(TAG, "getMainCapabilityPhoneId() = " + phoneId);
                if (("1".equals(isSupportC2kLte()))
                     || ("2".equals(isSupportC2kLte()))) {
                    if (phoneId == 10) {
                        phoneId = 0;
                    } else if (phoneId == 11) {
                        phoneId = 1;
                    } else if (phoneId <= 3) {
                        return phoneId;
                    } else {
                        CTSCLog.e(TAG, "getCapabilitySim C2K project not support 3 sim");
                        return PhoneConstants.SIM_ID_1;
                    }
                }
                CTSCLog.i(TAG, "Main Capability phoneId = " + phoneId);
                return phoneId;
            } else {
                CTSCLog.e(TAG, "ITelephonyEx iTelEx = null");
                return PhoneConstants.SIM_ID_1;
            }
        } catch (RemoteException e) {
            CTSCLog.e(TAG, "ITelephonyEx RemoteException");
            return PhoneConstants.SIM_ID_1;
        }
    }
    public static String[] getCdmaCmdArr(String[] cmdArray) {
        if (!is93Modem()) {
            return cmdArray;
        } else {
            String[] cmdArrayNew = new String[2];
                cmdArrayNew[0] = cmdArray[0];
                cmdArrayNew[1] = cmdArray[1];
            return cmdArrayNew;
        }
    }
}
