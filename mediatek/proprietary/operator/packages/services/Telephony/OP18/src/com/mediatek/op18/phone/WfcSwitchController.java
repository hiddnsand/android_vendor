package com.mediatek.op18.phone;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.mediatek.ims.internal.MtkImsManagerEx;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;


/**
 * Class to support operator customizations for WFC settings.
 */
public class WfcSwitchController {

    private static final String TAG = "OP18WfcSwitchController";
    private static final String AOSP_CALL_SETTING_WFC_PREFERENCE
            = "button_wifi_calling_settings_key";
    private static final String OP18_WFC_PREFERENCE_KEY = "op18_wfc_pref_switch";
    private static WfcSwitchController sController = null;

    private Context mPluginContext;
    private Context mAppContext;
    private android.preference.SwitchPreference mWfcSwitchCallSettings = null;
    private android.preference.PreferenceScreen mPreferenceScreenCallSettings = null;

    private WfcSwitchController(Context context) {
       mPluginContext = context;
    }

    /** Returns instance of OP18WfcSettings.
         * @param context context
         * @return OP18WfcSettings
         */
    public static WfcSwitchController getInstance(Context context) {

        if (sController == null) {
            sController = new WfcSwitchController(context);
        }
        return sController;
    }

    public void customizedWfcPreference(Context context, Object preferenceScreen) {
        mAppContext = context;
        android.preference.Preference wfcPreferenceCallSettings = null;
        if (!init(preferenceScreen)) {
            return;
        }
        if (mPreferenceScreenCallSettings != null) {
            wfcPreferenceCallSettings =
                    (android.preference.Preference) mPreferenceScreenCallSettings
                    .findPreference(AOSP_CALL_SETTING_WFC_PREFERENCE);
            Log.d(TAG, "wfcPreferenceCallSettings: " + wfcPreferenceCallSettings);
        }

        CharSequence title = null;
        int order = 0;
        if (wfcPreferenceCallSettings != null) {
            mPreferenceScreenCallSettings.removePreference(wfcPreferenceCallSettings);
            title = wfcPreferenceCallSettings.getTitle();
            order = wfcPreferenceCallSettings.getOrder();
        }

        checkAndAddWfcSwitch(title, order);

        int callState = Settings.Global.getInt(context.getContentResolver(),
                    OP18WfcSettings.CALL_STATE, OP18WfcSettings.CALL_STATE_IDLE);
        Log.d(TAG, "call_state: " + callState);
        updateWfcSwitchState(callState);
    }

    private boolean init(Object preferenceScreen) {
        if (preferenceScreen instanceof android.preference.PreferenceScreen) {
            mPreferenceScreenCallSettings =
                    (android.preference.PreferenceScreen) preferenceScreen;
            mWfcSwitchCallSettings =
                    (android.preference.SwitchPreference) mPreferenceScreenCallSettings
                    .findPreference(OP18_WFC_PREFERENCE_KEY);
            Log.d(TAG, "mPreferenceScreenCallSettings: " + mPreferenceScreenCallSettings);

        } else {
            Log.d(TAG, "invalid class of preferenceScreen: " + preferenceScreen);
            return false;
        }
        return true;
    }

    private void checkAndAddWfcSwitch(CharSequence title, int order) {
        Log.d(TAG, "mWfcSwitchCallSettings: " + mWfcSwitchCallSettings);
        if (mWfcSwitchCallSettings == null) {
            if (mPreferenceScreenCallSettings != null) {
                mWfcSwitchCallSettings = new android.preference.SwitchPreference(mAppContext);
                mWfcSwitchCallSettings
                        .setOnPreferenceChangeListener(new WfcSwitchListenerForCallSettings());
                mWfcSwitchCallSettings.setKey(OP18_WFC_PREFERENCE_KEY);
                mWfcSwitchCallSettings.setTitle(title);
                mWfcSwitchCallSettings.setOrder(order);
                mPreferenceScreenCallSettings.addPreference(mWfcSwitchCallSettings);
            }
            ImsManager.setWfcMode(mAppContext,
                    ImsConfig.WfcModeFeatureValueConstants.WIFI_PREFERRED);
        }
    }

    public void updateWfcSwitchState(int callState) {
        // Disable switch if PS call ongoing
        if (mWfcSwitchCallSettings != null) {
            mWfcSwitchCallSettings.setEnabled(callState == OP18WfcSettings.CALL_STATE_PS ?
                    false : true);
            mWfcSwitchCallSettings.setChecked(ImsManager.isWfcEnabledByUser(mPluginContext));
            mWfcSwitchCallSettings.setSummary(ImsManager.isWfcEnabledByUser(mAppContext) == true ?
                    mPluginContext.getResources().getString(R.string.enabled)
                    : mPluginContext.getResources().getString(R.string.disabled));
        }
    }

    private boolean isInSwitchProcess() {
        int imsState = MtkPhoneConstants.IMS_STATE_DISABLED;
        try {
         imsState = MtkImsManagerEx.getInstance().getImsState(RadioCapabilitySwitchUtil
                         .getMainCapabilityPhoneId());
        } catch (ImsException e) {
           return false;
        }
        Log.d(TAG, "isInSwitchProcess , imsState = " + imsState);
        return imsState == MtkPhoneConstants.IMS_STATE_DISABLING
                || imsState == MtkPhoneConstants.IMS_STATE_ENABLING;
    }

    /** Need for the case when rjil specific wfc pref is to be added again after being removed.
     * For rest of wfc setting customization use customizedWfcPreference()
     * @return
     */
    public void addWfcPreference() {
        Log.d(TAG, "addWfcPreference,mPreferenceScreenCallSettings"
                + mPreferenceScreenCallSettings);
        if (mPreferenceScreenCallSettings != null) {
            android.preference.Preference aospWfcSettingsCallPreference =
                    (android.preference.Preference) mPreferenceScreenCallSettings
                    .findPreference(AOSP_CALL_SETTING_WFC_PREFERENCE);
            android.preference.SwitchPreference customizedWfcPreference =
                    (android.preference.SwitchPreference) mPreferenceScreenCallSettings
                    .findPreference(OP18_WFC_PREFERENCE_KEY);
            if (aospWfcSettingsCallPreference == null && customizedWfcPreference == null) {
                mPreferenceScreenCallSettings.addPreference(mWfcSwitchCallSettings);
                ImsManager.setWfcMode(mAppContext,
                    ImsConfig.WfcModeFeatureValueConstants.WIFI_PREFERRED);
                mWfcSwitchCallSettings.setEnabled(Settings.Global
                        .getInt(mPluginContext.getContentResolver(),
                    OP18WfcSettings.CALL_STATE, OP18WfcSettings.CALL_STATE_IDLE) ==
                    OP18WfcSettings.CALL_STATE_PS ? false : true);
                mWfcSwitchCallSettings.setChecked(ImsManager.isWfcEnabledByUser(mAppContext));
            }
        }
    }

    /** Returns instance of OP18WfcSettings.
     * @return
     */
    public void removeWfcPreference() {
        if (mPreferenceScreenCallSettings != null && mWfcSwitchCallSettings != null) {
            mPreferenceScreenCallSettings.removePreference(mWfcSwitchCallSettings);
        }
    }

    private class WfcSwitchListenerForCallSettings implements
            android.preference.Preference.OnPreferenceChangeListener {

        public WfcSwitchListenerForCallSettings() {
        }

        @Override
        public boolean onPreferenceChange(android.preference.Preference preference,
                Object newValue) {
            boolean isChecked = ((Boolean) newValue).booleanValue();
            if (isChecked) {
                mWfcSwitchCallSettings.setChecked(false);
                displayConfirmationDialog();
                return false;
            }
            ImsManager.setWfcSetting(mPluginContext, isChecked);
            return true;
        }
    }

    private void displayConfirmationDialog() {
        new AlertDialog.Builder(mAppContext)
            .setCancelable(true)
            .setTitle(mPluginContext.getText(R.string.confirmation_dialog_title))
            .setMessage(mPluginContext.getText(R.string.confirmation_dialog_message))
            .setPositiveButton(mPluginContext.getText(R.string.enable), new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Log.d(TAG, "Enabling WFC switch");
                    if (isInSwitchProcess()) {
                       Toast.makeText(mPluginContext, R.string.Switch_not_in_use_string,
                                Toast.LENGTH_SHORT).show();
                       return;
                    }
                    if (mWfcSwitchCallSettings != null) {
                        mWfcSwitchCallSettings.setChecked(true);
                        mWfcSwitchCallSettings.setSummary(mPluginContext.getResources()
                                .getString(R.string.enabled));
                    }
                    ImsManager.setWfcSetting(mPluginContext, true);
                }
            })
            .setNegativeButton(mPluginContext.getText(R.string.cancel), new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                }
            })
            .show();
    }
}
