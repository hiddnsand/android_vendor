package com.mediatek.op07.phone;

import android.content.Context;
import android.util.Log;

import com.mediatek.phone.ext.IDisconnectCauseExt;
import com.mediatek.phone.ext.IMobileNetworkSettingsExt;
import com.mediatek.phone.ext.INetworkSettingExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;

/**
 * OP07 Phone Customization Factory.
 */
public class OP07PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {
    private Context mContext;

    /** Constructor.
     * @param context context
     */
    public OP07PhoneCustomizationFactory(Context context) {
        mContext = context;
    }

    @Override
    public IMobileNetworkSettingsExt makeMobileNetworkSettingsExt() {
        Log.i("OP07PhoneCustomizationFactory", "makeMobileNetworkSettingsExt");
        return new OP07MobileNetworkSettingsExt(mContext);
    }

    @Override
    public INetworkSettingExt makeNetworkSettingExt() {
        Log.i("OP07PhoneCustomizationFactory", "makeNetworkSettingsExt");
        return new OP07NetworkSettingExt(mContext);
    }

    @Override
    public IDisconnectCauseExt makeDisconnectCauseExt() {
        Log.d("OP07PhoneCustomizationFactory", "makeDisconnectCauseExt");
        return new OP07DisconnectCauseExt(mContext);
    }
}
