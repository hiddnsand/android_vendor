/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.gallery.op02;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.provider.Settings;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.util.Log;

import com.mediatek.gallery3d.ext.DefaultRewindAndForwardExtension;
import com.mediatek.gallery3d.video.IMoviePlayer;

import com.android.gallery3d.app.MoviePlayer;

import com.mediatek.gallery.op02.R;

public class RewindAndForward extends DefaultRewindAndForwardExtension implements View.OnClickListener {
    private static final String TAG = "RewindAndForward";
    private Context mPluginContext = null;
    private LinearLayout mContollerButtons;
    private ImageView mStop;
    private ImageView mForward;
    private ImageView mRewind;
    private int mButtonWidth;
    private int mButtonHeight;
    private int mControllerButtonPosition;
    private static final int BUTTON_PADDING = 40;
    private int mTimeBarHeight = 0;
    private IMoviePlayer mPlayer;
    private MoviePlayer mMoviePlayer;
    private static final boolean DBG = false;
    public RewindAndForward() {
        super();
    }
    public RewindAndForward(Context context) {
        super();
        mPluginContext = context;
        log("RewindAndForward init");
        //mTimeBarHeight = getPlayer().getTimeBarHight();
        Bitmap button = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_menu_forward);
        mButtonWidth = button.getWidth();
        mButtonHeight = button.getHeight();
        button.recycle();
        mContollerButtons = new LinearLayout(context);
        mContollerButtons.setHorizontalGravity(LinearLayout.HORIZONTAL);
        mContollerButtons.setVisibility(View.VISIBLE);
        mContollerButtons.setGravity(Gravity.CENTER);
        LinearLayout.LayoutParams buttonParam = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mRewind = new ImageView(context);
        mRewind.setImageResource(R.drawable.icn_media_rewind);
        mRewind.setScaleType(ScaleType.CENTER);
        mRewind.setFocusable(true);
        mRewind.setClickable(true);
        mRewind.setOnClickListener(this);
        mContollerButtons.addView(mRewind, buttonParam);
        mStop = new ImageView(context);
        mStop.setImageResource(R.drawable.icn_media_stop);
        mStop.setScaleType(ScaleType.CENTER);
        mStop.setFocusable(true);
        mStop.setClickable(true);
        mStop.setOnClickListener(this);
        LinearLayout.LayoutParams stopLayoutParam = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        stopLayoutParam.setMargins(BUTTON_PADDING, 0, BUTTON_PADDING, 0);
        mContollerButtons.addView(mStop, stopLayoutParam);
        mForward = new ImageView(context);
        mForward.setImageResource(R.drawable.icn_media_forward);
        mForward.setScaleType(ScaleType.CENTER);
        mForward.setFocusable(true);
        mForward.setClickable(true);
        mForward.setOnClickListener(this);
        mContollerButtons.addView(mForward, buttonParam);
    }
    @Override
    public void onClick(View v) {
        log("onClick");
        if (mPlayer != null) {
            //avoid controller to be hidden
            mPlayer.showMovieController();
        }
        if (v == mStop) {
            log("RewindAndForward onClick mStop");
            stopVideo();
        } else if (v == mRewind) {
            log("RewindAndForward onClick mRewind");
            rewind();
        } else if (v == mForward) {
            log("RewindAndForward onClick mForward");
            forward();
        }
    }
    @Override
    public View getView() {
        log("getView() mContollerButtons = " + mContollerButtons);
        return mContollerButtons;
    }
    @Override
    public int getPaddingRight() {
        int ret = mButtonWidth * 3 + BUTTON_PADDING * 2;
        log("getPaddingRight() return = " + ret);
        return ret;
    }
    @Override
    public int getHeight() {
        log("getHeight() mButtonHeight = " + mButtonHeight);
        return mButtonHeight;
    }
    @Override
    public int getControllerButtonPosition() {
        log("getControllerButtonPosition() = " + mControllerButtonPosition);
        return mControllerButtonPosition;
    }
    @Override
    public void hide() {
       log("hide()");
       mContollerButtons.setVisibility(View.INVISIBLE);
    }
    @Override
    public void show() {
       log("show()");
       mContollerButtons.setVisibility(View.VISIBLE);
    }
    @Override
    public void startHideAnimation() {
       log("startHideAnimation()");
       startHideAnimation(mContollerButtons);
    }
    @Override
    public void cancelHideAnimation() {
       log("cancelHideAnimation()");
       mContollerButtons.setAnimation(null);
    }
    @Override
    public void setViewEnabled(boolean isEnabled) {
       log("setViewEnabled() is " + isEnabled);
       mRewind.setEnabled(isEnabled);
       mForward.setEnabled(isEnabled);
    }
    @Override
    public void onLayout(int l, int r, int b, int pr) {
        int cl = (r - l - getPaddingRight()) / 2;
        int cr = cl + getPaddingRight();
        mControllerButtonPosition = cr + pr;
        mContollerButtons.layout(cl + pr, b - mButtonHeight, cr + pr, b);
        int nl = cl + pr;
        int nr = b - mButtonHeight;
        int npr = b;
        int nb =  cr + pr;
        log("onLayout() nl = " + nl + " nr = " + nr + " npr = " + npr + " nb = " + nb);
    }
    @Override
    public void updateView() {
        log("updateView() getPlayer() = " + getPlayer());
        showControllerButtonsView(getPlayer().canStop(),
            getPlayer().canSeekBackward() &&
                getPlayer().getCurrentPosition() > 0 &&
                getPlayer().isTimeBarEnabled(),
            getPlayer().canSeekForward() &&
                (getPlayer().getCurrentPosition() < getPlayer().getDuration()) && getPlayer().isTimeBarEnabled());
    }
    private void stopVideo() {
        if (getPlayer().canStop()) {
            getPlayer().stopVideo();
            showControllerButtonsView(false, false, false);
        }
    }
    private void rewind() {
        log("rewind()");
        if (getPlayer().canSeekBackward()) {
            showControllerButtonsView(
                            getPlayer().canStop(),
                            false,
                            getPlayer().canSeekForward()
                                    && (getPlayer().getCurrentPosition() < getPlayer().getDuration())
                                    && getPlayer().isTimeBarEnabled());
            int stepValue = getStepOptionValue();
            int targetDuration = getPlayer().getCurrentPosition()
                    - stepValue < 0 ? 0 : getPlayer().getCurrentPosition()
                    - stepValue;
            log("rewind targetDuration " + targetDuration);
            getPlayer().seekTo(targetDuration);
        } else {
            showControllerButtonsView(
                            getPlayer().canStop(),
                            false,
                            getPlayer().canSeekForward()
                                    && (getPlayer().getCurrentPosition() < getPlayer()
                                            .getDuration())
                                    && getPlayer().isTimeBarEnabled());
        }
    }
    private void forward() {
        log("forward()");
        if (getPlayer().canSeekForward()) {
            showControllerButtonsView(getPlayer().canStop(),
                            getPlayer().canSeekBackward()
                              && getPlayer().getCurrentPosition() > 0
                              && getPlayer().isTimeBarEnabled(),
                            false);
            int stepValue = getStepOptionValue();
            int targetDuration = getPlayer().getCurrentPosition()
                    + stepValue > getPlayer().getDuration() ? getPlayer()
                    .getDuration() : getPlayer().getCurrentPosition()
                    + stepValue;
            log("forward targetDuration " + targetDuration);
            getPlayer().seekTo(targetDuration);
        } else {
            showControllerButtonsView(
                    getPlayer().canStop(), getPlayer().canSeekBackward()
                            && getPlayer().getCurrentPosition() > 0
                            && getPlayer().isTimeBarEnabled(), false);
        }
    }
    private void showControllerButtonsView(boolean canStop, boolean canRewind, boolean canForward) {
        log("showControllerButtonsView " + canStop + canRewind + canForward);
        // show ui
        mStop.setEnabled(canStop);
        mRewind.setEnabled(canRewind);
        mForward.setEnabled(canForward);
    }
    private int getStepOptionValue() {
        final int stepBase = 3000;
        int step = Settings.System.getInt(getContext().getContentResolver(), "selected_step_option", 0) + 1;
        log("getStepOptionValue step = " + step);
        return step * stepBase;
    }
    private boolean getTimeBarEanbled() {
        return getPlayer().isTimeBarEnabled();
    }
    private void startHideAnimation(View view) {
        if (view.getVisibility() == View.VISIBLE) {
            view.startAnimation(getPlayer().getHideAnimation());
        }
    }
    @Override
    public void setParameter(String key, Object value) {
        super.setParameter(key, value);
        if (value instanceof IMoviePlayer) {
            mPlayer = (IMoviePlayer) value;
        } else if (value instanceof MoviePlayer) {
            mMoviePlayer = (MoviePlayer) value;
        }
    }
    private IMoviePlayer getPlayer() {
        return mPlayer;
    }
    private void log(String msg) {
        if (DBG) {
            Log.v(TAG, msg);
        }
    }
}
