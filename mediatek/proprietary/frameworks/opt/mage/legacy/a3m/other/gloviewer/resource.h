#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define IDD_SCENEGRAPH                          102
#define IDD_LIGHT                               104
#define IDD_CAMERA                              106
#define IDD_ANIMATION                           107
#define IDD_SOLID                               109
#define IDD_STATISTICS                          111
#define IDC_CHECKVISIBLE                        1000
#define IDC_COLOUR                              1000
#define IDC_PLAY_PAUSE                          1000
#define IDC_COLOUR_LABEL                        1001
#define IDC_FOV                                 1002
#define IDC_VERTEX_COUNT                        1002
#define IDC_NEAR                                1004
#define IDC_SCENETREE                           1004
#define IDC_CAMERA_COUNT                        1007
#define IDC_POINT                               1038
#define IDC_DIRECTIONAL                         1039
#define IDC_SPOT                                1040
#define IDC_CHECK_ATTENUATE                     1041
#define IDC_ATTENUATE_START                     1042
#define IDC_ATTENUATE_END                       1043
#define IDC_SPOT_INNER                          1044
#define IDC_POS_X                               1045
#define IDC_SPOT_OUTER                          1045
#define IDC_POS_Y                               1046
#define IDC_POS_Z                               1047
#define IDC_SCALE_X                             1051
#define IDC_SCALE_Y                             1052
#define IDC_SCALE_Z                             1053
#define IDC_FAR                                 1054
#define IDC_UNIFORMS                            1056
#define IDC_UNI1                                1057
#define IDC_UNI2                                1058
#define IDC_UNI3                                1059
#define IDC_UNI4                                1060
#define IDC_LIGHT_COUNT                         1066
#define IDC_SOLID_COUNT                         1067
#define IDC_TRIANGLE_COUNT                      1068
#define IDC_AXIS_X                              1072
#define IDC_AXIS_Y                              1073
#define IDC_AXIS_Z                              1074
#define IDC_ANGLE                               1075
#define IDC_PROGRESS_SLIDER                     1077
#define IDC_STOP                                1082
#define IDC_LOOP                                1083
#define IDC_LOOP_START                          1086
#define IDC_LOOP_END                            1087
#define IDC_PROGRESS                            1088
#define IDC_DURATION                            1089
#define IDC_LIGHT_INTENSITY                     1089
#define IDC_LIGHT_AMBIENT_LEVEL                 1090
#define IDC_LC_R                                1094
#define IDC_LC_B                                1097
#define IDC_LC_G                                1098
#define IDC_LIGHT_COLOUR                        1099
