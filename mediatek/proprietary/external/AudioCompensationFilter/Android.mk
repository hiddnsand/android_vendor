LOCAL_PATH := $(call my-dir)

############################################################################################################### System
include $(CLEAR_VARS)

ifeq ($(findstring MTK_AOSP_ENHANCEMENT,  $(MTK_GLOBAL_CFLAGS)),)
    LOCAL_CFLAGS += -DMTK_BASIC_PACKAGE
endif

LOCAL_SRC_FILES := \
AudioCompFltCustParam.cpp

LOCAL_C_INCLUDES := \
    $(MTK_PATH_SOURCE)/external/audiocustparam \
    $(MTK_PATH_SOURCE)/external/nvram/libnvram \
    $(MTK_PATH_CUSTOM)/cgen/inc \
    $(MTK_PATH_CUSTOM)/cgen/cfgfileinc \
    $(MTK_PATH_CUSTOM)/cgen/cfgdefault



LOCAL_SHARED_LIBRARIES := \
    libcustom_nvram_mtk \
    libnvram_mtk \
    libcutils \
    liblog \
    libutils \
    libaudiocustparam

# MTK Audio Tuning Tool Version
LOCAL_C_INCLUDES += \
        external/libxml2/include \
        external/icu/icu4c/source/common \
        $(JNI_H_INCLUDE) \
        $(MTK_PATH_SOURCE)/external/AudioParamParser/include \
        $(MTK_PATH_SOURCE)/external/AudioParamParser

ifneq ($(MTK_AUDIO_TUNING_TOOL_VERSION),)
  ifneq ($(strip $(MTK_AUDIO_TUNING_TOOL_VERSION)),V1)
    MTK_AUDIO_TUNING_TOOL_V2_PHASE:=$(shell echo $(MTK_AUDIO_TUNING_TOOL_VERSION) | sed 's/V2.//g')
    LOCAL_CFLAGS += -DMTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT
    LOCAL_CFLAGS += -DMTK_AUDIO_TUNING_TOOL_V2_PHASE=$(MTK_AUDIO_TUNING_TOOL_V2_PHASE)
  endif
endif
# MTK Audio Tuning Tool Version

ifeq ($(MTK_STEREO_SPK_ACF_TUNING_SUPPORT),yes)
  LOCAL_CFLAGS += -DMTK_STEREO_SPK_ACF_TUNING_SUPPORT
endif

ifeq ($(MTK_AUDIO_BLOUD_CUSTOMPARAMETER_REV),MTK_AUDIO_BLOUD_CUSTOMPARAMETER_V5)
  LOCAL_CFLAGS += -DMTK_AUDIO_BLOUD_CUSTOMPARAMETER_V5
else
  ifeq ($(strip $(MTK_AUDIO_BLOUD_CUSTOMPARAMETER_REV)),MTK_AUDIO_BLOUD_CUSTOMPARAMETER_V4)
    LOCAL_CFLAGS += -DMTK_AUDIO_BLOUD_CUSTOMPARAMETER_V4
  endif
endif

LOCAL_MODULE := libaudiocompensationfilter

LOCAL_MODULE_TAGS := optional

LOCAL_MULTILIB := both

include $(MTK_SHARED_LIBRARY)

############################################################################################################### Vendor
include $(CLEAR_VARS)

ifeq ($(findstring MTK_AOSP_ENHANCEMENT,  $(MTK_GLOBAL_CFLAGS)),)
    LOCAL_CFLAGS += -DMTK_BASIC_PACKAGE
endif

LOCAL_SRC_FILES := \
AudioCompFltCustParam.cpp

LOCAL_C_INCLUDES := \
    $(MTK_PATH_SOURCE)/external/audiocustparam \
    $(MTK_PATH_SOURCE)/external/nvram/libnvram \
    $(MTK_PATH_CUSTOM)/cgen/inc \
    $(MTK_PATH_CUSTOM)/cgen/cfgfileinc \
    $(MTK_PATH_CUSTOM)/cgen/cfgdefault



LOCAL_SHARED_LIBRARIES := \
    libcustom_nvram \
    libnvram \
    libcutils \
    liblog \
    libutils \
    libaudiocustparam_vendor

# MTK Audio Tuning Tool Version
LOCAL_C_INCLUDES += \
        external/libxml2/include \
        external/icu/icu4c/source/common \
        $(JNI_H_INCLUDE) \
        $(MTK_PATH_SOURCE)/external/AudioParamParser/include \
        $(MTK_PATH_SOURCE)/external/AudioParamParser

ifneq ($(MTK_AUDIO_TUNING_TOOL_VERSION),)
  ifneq ($(strip $(MTK_AUDIO_TUNING_TOOL_VERSION)),V1)
    MTK_AUDIO_TUNING_TOOL_V2_PHASE:=$(shell echo $(MTK_AUDIO_TUNING_TOOL_VERSION) | sed 's/V2.//g')
    LOCAL_CFLAGS += -DMTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT
    LOCAL_CFLAGS += -DMTK_AUDIO_TUNING_TOOL_V2_PHASE=$(MTK_AUDIO_TUNING_TOOL_V2_PHASE)
  endif
endif
# MTK Audio Tuning Tool Version

ifeq ($(MTK_STEREO_SPK_ACF_TUNING_SUPPORT),yes)
  LOCAL_CFLAGS += -DMTK_STEREO_SPK_ACF_TUNING_SUPPORT
endif

ifeq ($(MTK_AUDIO_BLOUD_CUSTOMPARAMETER_REV),MTK_AUDIO_BLOUD_CUSTOMPARAMETER_V5)
  LOCAL_CFLAGS += -DMTK_AUDIO_BLOUD_CUSTOMPARAMETER_V5
else
  ifeq ($(strip $(MTK_AUDIO_BLOUD_CUSTOMPARAMETER_REV)),MTK_AUDIO_BLOUD_CUSTOMPARAMETER_V4)
    LOCAL_CFLAGS += -DMTK_AUDIO_BLOUD_CUSTOMPARAMETER_V4
  endif
endif

LOCAL_MODULE := libaudiocompensationfilter_vendor
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_MODULE_TAGS := optional

LOCAL_MULTILIB := both

include $(MTK_SHARED_LIBRARY)
############################## libaudiocompensationfilterc used by vendor only
include $(CLEAR_VARS)
LOCAL_C_INCLUDES:=  $(LOCAL_PATH)

LOCAL_SRC_FILES := \
    AudioCompFltCustParamc.c


LOCAL_C_INCLUDES := \
    $(MTK_PATH_SOURCE)/external/audiocustparam \
    $(MTK_PATH_SOURCE)/external/nvram/libnvram \
    $(MTK_PATH_CUSTOM)/cgen/inc \
    $(MTK_PATH_CUSTOM)/cgen/cfgfileinc \
    $(MTK_PATH_CUSTOM)/cgen/cfgdefault

LOCAL_SHARED_LIBRARIES := \
    libcustom_nvram \
    libnvram \
    libcutils \
    libutils \
    libaudiocustparam_vendor

# MTK Audio Tuning Tool Version
LOCAL_C_INCLUDES += \
        external/libxml2/include \
        external/icu/icu4c/source/common \
        $(JNI_H_INCLUDE) \
        $(MTK_PATH_SOURCE)/external/AudioParamParser/include \
        $(MTK_PATH_SOURCE)/external/AudioParamParser

ifneq ($(MTK_AUDIO_TUNING_TOOL_VERSION),)
  ifneq ($(strip $(MTK_AUDIO_TUNING_TOOL_VERSION)),V1)
    MTK_AUDIO_TUNING_TOOL_V2_PHASE:=$(shell echo $(MTK_AUDIO_TUNING_TOOL_VERSION) | sed 's/V2.//g')
    LOCAL_CFLAGS += -DMTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT
    LOCAL_CFLAGS += -DMTK_AUDIO_TUNING_TOOL_V2_PHASE=$(MTK_AUDIO_TUNING_TOOL_V2_PHASE)
  endif
endif
# MTK Audio Tuning Tool Version

ifeq ($(MTK_AUDIO_BLOUD_CUSTOMPARAMETER_REV),MTK_AUDIO_BLOUD_CUSTOMPARAMETER_V5)
  LOCAL_CFLAGS += -DMTK_AUDIO_BLOUD_CUSTOMPARAMETER_V5
else
  ifeq ($(strip $(MTK_AUDIO_BLOUD_CUSTOMPARAMETER_REV)),MTK_AUDIO_BLOUD_CUSTOMPARAMETER_V4)
    LOCAL_CFLAGS += -DMTK_AUDIO_BLOUD_CUSTOMPARAMETER_V4
  endif
endif

LOCAL_MODULE := libaudiocompensationfilterc
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_MODULE_TAGS := optional

LOCAL_MULTILIB := both

LOCAL_LDLIBS := -llog
include $(MTK_SHARED_LIBRARY)
