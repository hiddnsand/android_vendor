LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/frameworks/av/libdrm))
include $(CLEAR_VARS)
LOCAL_MODULE = libdrmmtkutil
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_SUFFIX = .so
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libicui18n libicuuc libutils libcutils libdl liblog libcrypto libssl libdrmmtkwhitelist libmtk_drvb
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libdrmmtkutil.so
include $(BUILD_PREBUILT)
endif
