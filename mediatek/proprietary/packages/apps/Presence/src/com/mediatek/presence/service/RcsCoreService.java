/*******************************************************************************
 * Software Name : RCS IMS Stack
 *
 * Copyright (C) 2010 France Telecom S.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.mediatek.presence.service;

import java.util.Vector;
import org.gsma.joyn.Intents;
import org.gsma.joyn.JoynService;
import org.gsma.joyn.capability.ICapabilityService;
import org.gsma.joyn.chat.IChatService;
import org.gsma.joyn.contacts.IContactsService;
import org.gsma.joyn.ft.IFileTransferService;
import org.gsma.joyn.gsh.IGeolocSharingService;
import org.gsma.joyn.ipcall.IIPCallService;
import org.gsma.joyn.ish.IImageSharingService;
import org.gsma.joyn.session.IMultimediaSessionService;
import org.gsma.joyn.vsh.IVideoSharingService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;

import com.mediatek.presence.core.ims.network.NetworkConnectivityApi;
import com.mediatek.presence.core.ims.network.INetworkConnectivityApi;

import com.mediatek.presence.addressbook.AccountChangedReceiver;
import com.mediatek.presence.core.Core;
import com.mediatek.presence.core.CoreListener;
import com.mediatek.presence.core.ims.ImsError;
import com.mediatek.presence.core.ims.network.NetworkApiService;
import com.mediatek.presence.core.ims.protocol.sip.SipRequest;
import com.mediatek.presence.core.ims.service.ContactInfo;
import com.mediatek.presence.core.ims.service.capability.Capabilities;
import com.mediatek.presence.core.ims.service.presence.PresenceUtils;
import com.mediatek.presence.core.ims.service.presence.pidf.CapabilityDetails;
import com.mediatek.presence.core.ims.service.presence.pidf.PidfDocument;
import com.mediatek.presence.core.ims.service.presence.pidf.Tuple;
import com.mediatek.presence.core.ims.service.sip.GenericSipSession;
import com.mediatek.presence.platform.AndroidFactory;
import com.mediatek.presence.provider.eab.ContactsBackupHelper;
import com.mediatek.presence.provider.eab.ContactsManager;
import com.mediatek.presence.provider.settings.RcsSettings;
import com.mediatek.presence.service.api.CapabilityServiceImpl;
import com.mediatek.presence.service.api.ContactsServiceImpl;
import com.mediatek.presence.utils.PhoneUtils;
import com.mediatek.presence.utils.logger.Logger;
import com.mediatek.presence.provisioning.https.HttpsProvisioningSMS;
import com.mediatek.presence.provisioning.https.HttpsProvisioningUtils;

import org.gsma.joyn.ICoreServiceWrapper;
/**
 * RCS core service. This service offers a flat API to any other process (activities)
 * to access to RCS features. This service is started automatically at device boot.
 * 
 * @author Jean-Marc AUFFRET
 */
public class RcsCoreService extends Service implements CoreListener {
    /**
     * Service name
     */
    public final static int RCS_CORE_LOADED = 0;
    public final static int RCS_CORE_FAILED = 1;
    public final static int RCS_CORE_STARTED = 2;
    public final static int RCS_CORE_STOPPED = 3;
    public final static int RCS_CORE_IMS_CONNECTED = 4;
    public final static int RCS_CORE_IMS_TRY_CONNECTION = 5;
    public final static int RCS_CORE_IMS_CONNECTION_FAILED = 6;
    public final static int RCS_CORE_IMS_TRY_DISCONNECT = 7;
    public final static int RCS_CORE_IMS_BATTERY_DISCONNECTED = 8;
    public final static int RCS_CORE_IMS_DISCONNECTED = 9;
    public final static int RCS_CORE_NOT_LOADED = 10;

    public static final String CORE_SERVICE_CURRENT_STATE_NOTIFICATION =
        "com.mediatek.presence.CORE_SERVICE_STATE";

    public static int CURRENT_STATE = RCS_CORE_NOT_LOADED;

    public static final String SERVICE_NAME = "com.mediatek.presence.SERVICE";

    public static final String LABEL = "label";

    public static final String LABEL_ENUM = "label_enum";

    public static final String STATE = "state";

    /**
     * Notification ID
     */
    private final static int SERVICE_NOTIFICATION = 1000;

    public CoreServiceWrapperStub mCoreServiceWrapperStub;

    /**
     * Account changed broadcast receiver
     */
    private AccountChangedReceiver accountChangedReceiver = null;

    // --------------------- RCSJTA API -------------------------
    /**
     * Terms API
     */
    //private TermsApiService termsApi = new TermsApiService();
    /**
     * Contacts API
     */
    private ContactsServiceImpl contactsApi = null;

    /**
     * Capability API
     */
    private CapabilityServiceImpl capabilityApi = null;

    /**
     * M: add for auto-rejoin group chat @{
     */
    private NetworkApiService mNetworkConnectivityApi = new NetworkApiService();
    /** @} */

    /**
     * Account changed broadcast receiver
     */
    private HttpsProvisioningSMS reconfSMSReceiver = null;

    /**
     * The logger
     */
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public void onCreate() {
        // Start the core
        startCore();
    }

    @Override
    public void onDestroy() {

        // Stop the core
        Thread t = new Thread() {
            /**
             * Processing
             */
            public void run() {
                stopCore();
            }
        };
        t.start();
    }

    /**
     * Start core
     */
    public synchronized void startCore() {
        if (Core.getInstance() != null) {
            // Already started
            return;
        }

        try {
            if (logger.isActivated()) {
                logger.debug("Instanciate API");
            }

            // Instantiate the settings manager
            RcsSettings.createInstance(getApplicationContext());

            // Instanciate API
            contactsApi = new ContactsServiceImpl();
            capabilityApi = new CapabilityServiceImpl();

            mCoreServiceWrapperStub = new CoreServiceWrapperStub();
            // Set the logger properties
            Logger.activationFlag = RcsSettings.getInstance().isTraceActivated();
            Logger.traceLevel = RcsSettings.getInstance().getTraceLevel();

            // Instantiate the contacts manager
            ContactsManager.createInstance(getApplicationContext());

            ContactsBackupHelper.createInstance(getApplicationContext());

            // Create the core
            Core.createCore(this);

            // Start the core
            Core.getInstance().startCore();

            if (logger.isActivated()) {
                logger.info("Instantiate the contacts manager");
            }
        } catch(Exception e) {
            // Unexpected error
            if (logger.isActivated()) {
                logger.error("Can't instanciate API and the contacts manager", e);
            }

            // Exit service
            stopSelf();
        }
    }

    /**
     * Stop core
     */
    public synchronized void stopCore() {
        if (Core.getInstance() == null) {
            // Already stopped
            return;
        }

        if (logger.isActivated()) {
            logger.debug("Close APIs");
        }

        // Close APIs
        if (contactsApi != null) {
            contactsApi.close();
        }
        if (capabilityApi != null) {
            capabilityApi.close();
        }

        // Terminate the core in background
        Core.terminateCore();

        if (logger.isActivated()) {
            logger.info("APIs closed with success");
        }
    }

    public class CoreServiceWrapperStub extends ICoreServiceWrapper.Stub {

        String TAG = "CoreServiceWrapperStub";

        @Override
        public IBinder getChatServiceBinder()
        {
            return null;
        }

        @Override
        public IBinder getFileTransferServiceBinder()
        {
            return null;
        }

        @Override
        public IBinder getCapabilitiesServiceBinder()
        {
            if (logger.isActivated()) {
                logger.debug("CoreServiceWrapperStub getCapabilitiesServiceBinder() entry");
            }
            return capabilityApi;
        }

        @Override
        public IBinder getContactsServiceBinder()
        {
            if (logger.isActivated()) {
                logger.debug("CoreServiceWrapperStub getContactsServiceBinder() entry");
            }
            return contactsApi;
        }

        @Override
        public IBinder getGeolocServiceBinder()
        {
            return null;
        }

        @Override
        public IBinder getVideoSharingServiceBinder()
        {
            return null;
        }

        @Override
        public IBinder getImageSharingServiceBinder()
        {
            return null;
        }

        @Override
        public IBinder getNetworkConnectivityApiBinder()
        {
            if (logger.isActivated()) {
                logger.debug("CoreServiceWrapperStub getNetworkConnectivityApiBinder() entry");
            }
            return mNetworkConnectivityApi;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        logger.debug("onBind RCSCoreService");
        return mCoreServiceWrapperStub;
    }

    /**
     * Add RCS service notification
     *
     * @param state Service state (ON|OFF)
     * @param label Label
     */
    public static void addRcsServiceNotification(boolean state, String label, int labelEnum) {
        // Create notification
        //Intent intent = new Intent(Intents.Client.ACTION_VIEW_SETTINGS);
        //intent.putExtra(STATE, state);
        //intent.putExtra(LABEL, label);
        //intent.putExtra(LABEL_ENUM,labelEnum);
        //AndroidFactory.getApplicationContext().sendBroadcast(intent);
        //CURRENT_STATE= labelEnum;
        //RcsSettings.getInstance().setServiceCurrentState(labelEnum);

    }

    /*---------------------------- CORE EVENTS ---------------------------*/

    /**
     * Notify registration status to API
     *
     * @param status Status
     */
    private void notifyRegistrationStatusToApi(boolean status) {
        if (capabilityApi != null) {
            capabilityApi.notifyRegistrationEvent(status);
        }
    }

    /**
     * Core layer has been started
     */
    public void handleCoreLayerStarted() {
        if (logger.isActivated()) {
            logger.debug("Handle event core started");
        }
        CURRENT_STATE = RCS_CORE_STARTED;
        Intent intent = new Intent(CORE_SERVICE_CURRENT_STATE_NOTIFICATION);
        intent.putExtra("status", CURRENT_STATE);
        getApplicationContext().sendStickyBroadcast(intent);
    }

    /**
     * Core layer has been terminated
     */
    public void handleCoreLayerStopped() {
        // Display a notification
        if (logger.isActivated()) {
            logger.debug("Handle event core terminated");
        }
        CURRENT_STATE = RCS_CORE_STOPPED;
        Intent intent = new Intent(CORE_SERVICE_CURRENT_STATE_NOTIFICATION);
        intent.putExtra("status", CURRENT_STATE);
        getApplicationContext().sendStickyBroadcast(intent);
    }

    /**
     * Handle "registration successful" event
     *
     * @param registered Registration flag
     */
    public void handleRegistrationSuccessful() {
        if (logger.isActivated()) {
            logger.debug("Handle event registration ok");
        }
        RcsSettings.getInstance().setRegistrationState(true);
        // Send registration intent
        //Intent intent = new Intent(Intents.Client.SERVICE_UP);
        //intent.putExtra("status", true);
        //getApplicationContext().sendStickyBroadcast(intent);
        // Notify APIs
        notifyRegistrationStatusToApi(true);
    }

    /**M
     * added to how notification of connecting and disconnecting states during registration
     */
    /**
     * Handle "try registration" event
     */
    public void handleTryRegister() {
        if (logger.isActivated()) {
            logger.debug("Handle event try registration");
        }
    }

    /**
     * Handle "try registration" event
     */
    public void handleTryDeregister() {
        if (logger.isActivated()) {
            logger.debug("Handle event try deregistration");
        }
    }
    /** @*/

    /**
     * Handle "registration failed" event
     *
     * @param error IMS error
    */
    public void handleRegistrationFailed(ImsError error) {
        if (logger.isActivated()) {
            logger.debug("Handle event registration failed");
        }
        RcsSettings.getInstance().setRegistrationState(false);
        // Display a notification
        //addRcsServiceNotification(false, getString(R.string.rcs_core_ims_connection_failed), RCS_CORE_IMS_CONNECTION_FAILED);
        //Intent intent = new Intent(Intents.Client.SERVICE_UP);
        //intent.putExtra("status", false);
        //getApplicationContext().sendStickyBroadcast(intent);
        // Notify APIs
        notifyRegistrationStatusToApi(false);
    }

    /**
     * Handle "registration terminated" event
     */
    public void handleRegistrationTerminated() {
        if (logger.isActivated()) {
            logger.debug("Handle event registration terminated");
        }
        RcsSettings.getInstance().setRegistrationState(false);

        // Notify APIs
        notifyRegistrationStatusToApi(false);
    }

    /**
     * A new presence sharing notification has been received
     *
     * @param contact Contact
     * @param status Status
     * @param reason Reason
     */
    public void handlePresenceSharingNotification(String contact, String status, String reason) {
        if (logger.isActivated()) {
            logger.debug("Handle event presence sharing notification for " + contact + " (" + status + ":" + reason + ")");
        }
        // Not used
    }

    /**
     * A new presence info notification has been received
     *
     * @param contact Contact
     * @param presense Presence info document
     */
    public void handlePresenceInfoNotification(String contact, PidfDocument presence) {
        if (logger.isActivated()) {
            logger.debug("Handle event presence info notification for " + contact);
        }

        if (presence != null) {
            // Extract capabilities
            Capabilities capabilities =  new Capabilities();
            int registrationState =  ContactInfo.REGISTRATION_STATUS_UNKNOWN;
            Vector<Tuple> tuples = presence.getTuplesList();

            if(tuples.size()>0)
            {
                //set the registration as true as the device published some capablity
                // We queried via anonymous fetch procedure, so set presence discovery to true
                registrationState =  ContactInfo.REGISTRATION_STATUS_ONLINE;

            for(int i=0; i < tuples.size(); i++) {
                Tuple tuple = (Tuple)tuples.elementAt(i);
                boolean state = false;
                if (tuple.getStatus().getBasic().getValue().equals("open")) {
                    state = true;
                }
                String id = tuple.getService().getId();

                if (id.equalsIgnoreCase(PresenceUtils.FEATURE_RCS2_VIDEO_SHARE)) {
                    capabilities.setVideoSharingSupport(state);
                } else
                if (id.equalsIgnoreCase(PresenceUtils.FEATURE_RCS2_VIDEO_SHARE)) {
                    capabilities.setVideoSharingSupport(state);
                } else
                if (id.equalsIgnoreCase(PresenceUtils.FEATURE_RCS2_IMAGE_SHARE)) {
                    capabilities.setImageSharingSupport(state);
                } else
                if (id.equalsIgnoreCase(PresenceUtils.FEATURE_RCS2_FT)) {
                    //file transfer
                    capabilities.setFileTransferSupport(state);
                } else
                if (id.equalsIgnoreCase(PresenceUtils.FEATURE_RCS2_CS_VIDEO)) {
                    capabilities.setCsVideoSupport(state);
                }
                else
                if (id.equalsIgnoreCase(PresenceUtils.FEATURE_RCS2_IP_VOICE_CALL)) {

                    if(state)
                    {
                        boolean audio = false;
                        boolean video = false;
                        boolean duplexmode = false;

                      for(CapabilityDetails c : tuple.getServiceCapability().getCapabilityList())
                      {
                          //check for audio
                          if(c.getName().equalsIgnoreCase("audio")){
                              if(c.getValue().equalsIgnoreCase("true"))
                              {
                                  audio = true;
                                  continue;
                              }
                          }

                          //check for audio
                          if(c.getName().equalsIgnoreCase("video")){
                              if(c.getValue().equalsIgnoreCase("true"))
                              {
                                  video = true;
                                  continue;
                              }
                          }

                        //check for duplex
                          if(c.getName().equalsIgnoreCase("duplex")){
                              if(c.getValue().equalsIgnoreCase("full"))
                              {
                                  duplexmode = true;
                                  continue;
                              }
                          }
                      }

                      if (logger.isActivated()) {
                          logger.debug("Video capability for : "+contact + "  - audio : "+audio + " ; video : " + video + " ; duplex :- " +duplexmode);
                      }

                      capabilities.setIR94_VoiceCall(audio);
                      capabilities.setIR94_VideoCall(video);
                      capabilities.setIR94_DuplexMode(duplexmode);

                    }

                }
                else
                if (id.equalsIgnoreCase(PresenceUtils.FEATURE_RCS2_DISCOVERY_VIA_PRESENCE)) {
                        capabilities.setPresenceDiscoverySupport(state);
                }
                else
                if ((id.equalsIgnoreCase(PresenceUtils.FEATURE_RCS2_CHAT))||
                    (id.equalsIgnoreCase(PresenceUtils.FEATURE_RCS2_CHAT_2))
                    ) {
                    //IM capability
                    capabilities.setImSessionSupport(state);
                }
            }

          } else {
              if (logger.isActivated()) {
                 logger.debug("No capability tuples found for : "+contact + " ; assuming its offline and update capabilities in database");
              }

              registrationState =  ContactInfo.REGISTRATION_STATUS_OFFLINE;
          }

            if (logger.isActivated()) {
                logger.debug("Set Contact Capability via Presence : "+contact);
            }

            // Update capabilities in database
            ContactsManager.getInstance().setContactCapabilities(contact, capabilities, ContactInfo.RCS_CAPABLE, registrationState);

            // Notify listener
            handleCapabilitiesNotification(contact, capabilities);
        }
        // Not used
    }

    /**
     * Capabilities update notification has been received
     *
     * @param contact Contact
     * @param capabilities Capabilities
     */
    public void handleCapabilitiesNotification(String contact, Capabilities capabilities) {
        if (logger.isActivated()) {
            logger.debug("Handle capabilities update notification for " + contact + " (" + capabilities.toString() + ")");
        }

        // Extract number from contact
        String number = PhoneUtils.extractNumberFromUri(contact);

        // Notify API
        capabilityApi.receiveCapabilities(number, capabilities);
    }

    /**
     * A new presence sharing invitation has been received
     *
     * @param contact Contact
     */
    public void handlePresenceSharingInvitation(String contact) {
        if (logger.isActivated()) {
            logger.debug("Handle event presence sharing invitation");
        }
        // Not used
    }

    /**
     * New message delivery status
     *
     * @param contact Contact
     * @param msgId Message ID
     * @param status Delivery status
     */
    public void handleMessageDeliveryStatus(String contact, String msgId, String status) {
        if (logger.isActivated()) {
            logger.debug("Handle message delivery status");
        }
    }

    /**
     * New message delivery status
     *
     * @param contact Contact
     * @param msgId Message ID
     * @param status Delivery status
     */
    public void handleMessageDeliveryStatus(String contact, String msgId, String status , int errorCode, String statusCode) {
        if (logger.isActivated()) {
            logger.debug("Handle message delivery status");
        }
    }

    /**
     * New file delivery status
     *
     * @param ftSessionId File transfer session ID
     * @param status Delivery status
     */
    public void handleFileDeliveryStatus(String ftSessionId, String status,String contact) {
        if (logger.isActivated()) {
            logger.debug("Handle file delivery status: session " + ftSessionId + " status " + status + "Contact: " + contact);
        }
    }

    /**
     * New SIP session invitation
     *
     * @param intent Resolved intent
     * @param session SIP session
     */
    public void handleSipSessionInvitation(Intent intent, GenericSipSession session) {
        if (logger.isActivated()) {
            logger.debug("Handle event receive SIP session invitation");
        }
    }

    /**
     * New SIP instant message received
     *
     * @param intent Resolved intent
     * @param message Instant message request
     */
    public void handleSipInstantMessageReceived(Intent intent, SipRequest message) {
        if (logger.isActivated()) {
            logger.debug("Handle event receive SIP instant message");
        }

    }

    /**
     * User terms confirmation request
     *
     * @param remote Remote server
     * @param id Request ID
     * @param type Type of request
     * @param pin PIN number requested
     * @param subject Subject
     * @param text Text
     * @param btnLabelAccept Label of Accept button
     * @param btnLabelReject Label of Reject button
     * @param timeout Timeout request
     */
    public void handleUserConfirmationRequest(String remote, String id,
            String type, boolean pin, String subject, String text,
            String acceptButtonLabel, String rejectButtonLabel, int timeout) {
        if (logger.isActivated()) {
            logger.debug("Handle event user terms confirmation request");
        }

        //termsApi.receiveTermsRequest(remote, id, type, pin, subject, text, acceptButtonLabel, rejectButtonLabel, timeout);
    }

    /**
     * User terms confirmation acknowledge
     *
     * @param remote Remote server
     * @param id Request ID
     * @param status Status
     * @param subject Subject
     * @param text Text
     */
    public void handleUserConfirmationAck(String remote, String id, String status, String subject, String text) {
        if (logger.isActivated()) {
            logger.debug("Handle event user terms confirmation ack");
        }

        //termsApi.receiveTermsAck(remote, id, status, subject, text);
    }

    /**
     * User terms notification
     *
     * @param remote Remote server
     * @param id Request ID
     * @param subject Subject
     * @param text Text
     * @param btnLabel Label of OK button
     */
    public void handleUserNotification(String remote, String id, String subject, String text, String okButtonLabel) {
        if (logger.isActivated()) {
            logger.debug("Handle event user terms notification");
        }

        //termsApi.receiveUserNotification(remote, id, subject, text, okButtonLabel);
    }

    /**
     * SIM has changed
     */
    public void handleSimHasChanged() {
        if (logger.isActivated()) {
            logger.debug("Handle SIM has changed");
        }

        // Restart the RCS service
        LauncherUtils.stopRcsService(getApplicationContext());
        LauncherUtils.launchRcsService(getApplicationContext(), true, false);
    }
}
