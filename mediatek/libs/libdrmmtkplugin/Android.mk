LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/frameworks/av/libdrm))
include $(CLEAR_VARS)
LOCAL_MODULE = libdrmmtkplugin
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_RELATIVE_PATH = mtkdrm
LOCAL_MODULE_SUFFIX = .so
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libbinder libicui18n libicuuc libutils libcutils libdl liblog libdrmmtkutil libmtk_drvb
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libdrmmtkplugin.so
include $(BUILD_PREBUILT)
endif
