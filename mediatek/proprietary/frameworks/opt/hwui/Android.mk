# Copyright 2006 The Android Open Source Project

# XXX using libutils for simulator build only...
#

#ifeq ($(MTK_RIL_MODE), c6m_1rild)

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
    MTKDumper.cpp \
    MTKBasicInfo.cpp \
    MTKProgram.cpp \
    MTKProgramCache.cpp \
    MTKProgramAtlas.cpp \
    MTKHwuiExtension.cpp

LOCAL_SHARED_LIBRARIES := \
    liblog libcutils libutils libbinder libprogrambinary libhwui libskia libGLESv2 libEGL

LOCAL_STATIC_LIBRARIES := \
    libprotobuf-c-nano-enable_malloc

LOCAL_C_INCLUDES += $(TOP)/system/core/include/utils \
    system/core/include \
    frameworks/base/libs/hwui/mediatek \
    frameworks/base/libs/hwui \
    external/skia/include/core \
    external/skia/include/config \
    external/skia/src/core \
    external/skia/tools \
    external/skia/include/utils \
    external/skia/include/private

# for asprinf
LOCAL_CFLAGS := -D_GNU_SOURCE

ifeq ($(HAVE_AEE_FEATURE),yes)
LOCAL_SHARED_LIBRARIES += libaed
LOCAL_CFLAGS += -DHAVE_AEE_FEATURE
endif

LOCAL_C_INCLUDES += external/nanopb-c

#build shared library
LOCAL_CFLAGS += -DUSE_OPENGL_RENDERER
LOCAL_CFLAGS += -DEGL_EGLEXT_PROTOTYPES
LOCAL_CFLAGS += -DGL_GLEXT_PROTOTYPES
LOCAL_MODULE:= libhwuiext
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
# Defaults for ATRACE_TAG and LOG_TAG in hwui extension
LOCAL_CFLAGS += -DLOG_TAG=\"HWUIExtension\"

include $(MTK_SHARED_LIBRARY)

# Build all make files under sub-directory
include $(call all-makefiles-under, $(LOCAL_PATH))

#endif
