/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M Mesh interface.
 */

package com.mediatek.j3m;

/*!
 * \ingroup a3mJref
 * @{
 */
/**
 * Version interface.
 * Used to identify and compare different versions of A3M and its parts.
 */
public interface Version {
    /**
     * Returns the major version number.
     *
     * @return Major version number
     */
    int getMajor();

    /**
     * Returns the minor version number.
     *
     * @return Minor version number
     */
    int getMinor();

    /**
     * Returns the patch version number.
     *
     * @return Patch version number
     */
    int getPatch();

    /**
     * Returns the extra information field.
     *
     * @return Extra information string
     */
    String getExtra();

    /**
     * Version equality comparison function.
     * Two versions will test equal each of their respective version numbers
     * are equal.
     *
     * @param other Version with which to compare
     * @return True if the versions are equal
     */

    boolean isEqualTo(Version other);

    /**
     * Version less-than comparison function.
     * Version number comparison is done by comparing first the major, then the
     * minor, then the patch number, each time continuing the check only if the
     * numbers are equal.
     *
     * @param other Version with which to compare
     * @return True if this version is less than the other version
     */
    boolean isLessThan(Version other);

    /**
     * Version greater-than comparison function.
     * Version number comparison is done by comparing first the major, then the
     * minor, then the patch number, each time continuing the check only if the
     * numbers are equal.
     *
     * @param other Version with which to compare
     * @return True if this version is greater than the other version
     */
    boolean isGreaterThan(Version other);
}

/*! @} */
