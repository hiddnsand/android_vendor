package com.mediatek.sensorhub.ui;

import android.content.Intent;
import android.hardware.Sensor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.util.Log;

import com.mediatek.sensorhub.settings.Utils;

public class FusionSensorActivity extends BaseActivity {

    private static final String TAG = "FusionSensorActivity";

    public FusionSensorActivity() {
        super("FusionSensorActivity");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.orginal_gestures_pref);
        setTitle(R.string.fusion_sensor_title);
        initializeAllPreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePreferenceStatus();
    }

    private void initializeAllPreferences() {
        for (int type : Utils.fusionSensorType) {
            Sensor sensor = mSensorKeyMap.get(type);
            if (sensor != null) {
                Utils.createPreference(Utils.TYPE_PREFERENCE, sensor.getName(), String
                        .valueOf(type), getPreferenceScreen(), this);
            }
        }
    }

    private void updatePreferenceStatus() {
        for (int type : Utils.fusionSensorType) {
            String prefKey = String.valueOf(type);
            Preference preference = findPreference(prefKey);
            if (preference != null) {
                int sensorType = Integer.valueOf(preference.getKey());
                if (Sensor.TYPE_GAME_ROTATION_VECTOR == sensorType) {
                    preference
                            .setSummary(Utils
                                    .getSensorStatus(Utils.KEY_GAME_ROTATION_VECTOR_STATUS)
                                    ? R.string.running_summary : R.string.space_summary);
                } else if (Sensor.TYPE_ORIENTATION == sensorType) {
                    preference
                            .setSummary(Utils.getSensorStatus(Utils.KEY_ORIENTATION_STATUS)
                                    ? R.string.running_summary : R.string.space_summary);
                }
            }
        }
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        int sensorType = Integer.valueOf(preference.getKey());
        Log.d(TAG, "onPreferenceTreeClick : " + preference.getTitle() + sensorType);

        Intent intent = new Intent();
        if (Sensor.TYPE_GAME_ROTATION_VECTOR == sensorType) {
            intent.setClass(this, GameRotationSensorActivity.class);
        } else if (Sensor.TYPE_ORIENTATION == sensorType) {
            intent.setClass(this, OrientationSensorActivity.class);
        } else {
            intent = null;
        }
        if (intent != null) {
            startActivity(intent);
        }
        return true;
    }

}
