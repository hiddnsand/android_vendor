package com.mediatek.op06.settings;

import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.PreferenceScreen;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.ims.ImsManager;
import com.mediatek.ims.config.ImsConfigContract;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.settings.ext.DefaultWfcSettingsExt;

/**
 * Plugin implementation for WFC Settings plugin
 */
public class Op06WfcSettingsExt extends DefaultWfcSettingsExt {

    private static final String TAG = "Op06WfcSettingsExt";
    private static final String KEY_WFC_SETTINGS = "wifi_calling_settings";
    private static final String AOSP_BUTTON_WFC_MODE = "wifi_calling_mode";
    private Context mContext;
    private Context mAppContext;
    private PreferenceFragment mPreferenceFragment;
    private ContentObserver mContentObserver;
    private Op06WfcSettings mWfcSettings;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "action:" + action);
            if (!ImsManager.isWfcEnabledByPlatform(context)) {
                Log.d(TAG, "WFC is not enabled in platform");
                return;
            }
            if (action.equals(ImsConfigContract.ACTION_CONFIG_LOADED)) {
                int phoneId = intent.getIntExtra(ImsConfigContract.EXTRA_PHONE_ID, 0);
                if (WifiCallingUtils.isWifiCallingProvisioned(context, phoneId)) {
                    if (mPreferenceFragment != null) {
                        Log.d(TAG, "Config loaded. WFC is provisioned, so add pref.");
                        customizedWfcPreference(mPreferenceFragment.getPreferenceScreen());
                    }
                } else {
                    if (mPreferenceFragment != null) {
                        Log.d(TAG, "Config loaded. WFC is not provisioned, so remove pref.");
                        //Op06WfcSettings wfcSettings = Op06WfcSettings.getInstance(mContext);
                        mWfcSettings.removeWfcPreference(mPreferenceFragment.getPreferenceScreen());
                    }
                }
            } else if (CarrierConfigManager.ACTION_CARRIER_CONFIG_CHANGED.equals(action)) {
                if (mPreferenceFragment != null
                        && TextUtils.equals(mPreferenceFragment.getClass().getSimpleName(),
                                "WirelessSettings")) {
                    customizedWfcPreference(mPreferenceFragment.getPreferenceScreen());
                } else if (mPreferenceFragment != null
                        && TextUtils.equals(mPreferenceFragment.getClass().getSimpleName(),
                                "WifiCallingSettings")) {
                    handleWfcModeUpdation();
                }
            }
        }
    };


    /** Constructor.
     * @param context context
     */
    public Op06WfcSettingsExt(Context context) {
        super();
        mContext = context;
        mWfcSettings = Op06WfcSettings.getInstance(mContext);
        WifiCallingUtils.registerReceiver(mContext, mReceiver);
    }

    @Override
     /** Initialize plugin with essential values.
     * @param
     * @return
     */
    public void initPlugin(PreferenceFragment pf) {
        mPreferenceFragment = pf;
        mAppContext = mPreferenceFragment.getActivity();
    }

    /** Customize WFC pref as per operator requirement, on basis of MCC+MNC
     * Called from WirelessSettings
     * @param preferenceScreen preferenceScreen
     * @return
     */
    public void customizedWfcPreference(PreferenceScreen preferenceScreen) {
        Log.d("@M_" + TAG, "customizedWfcPreference");
        if (ImsManager.isWfcEnabledByPlatform(mContext) &&
                WifiCallingUtils.isWifiCallingProvisioned(mContext,
                SubscriptionManager.getDefaultVoicePhoneId())) {
            //Op06WfcSettings wfcSettings = Op06WfcSettings.getInstance(mContext);
            mWfcSettings.customizedWfcPreference(mAppContext, preferenceScreen);
        } else {
            mWfcSettings.removeWfcPreference(preferenceScreen);
        }
    }

    @Override
    /** Takes operator specific action on wfc list preference:
     * On Switch OFF, disable wfcModePref.
     * @param root
     * @param wfcModePref
     * @param wfcEnabled
     * @param wfcMode
        * @return
        */
    public void updateWfcModePreference(PreferenceScreen root, ListPreference wfcModePref,
            boolean wfcEnabled, int wfcMode) {
        if (ImsManager.isWfcEnabledByPlatform(mContext) &&
                WifiCallingUtils.isWifiCallingProvisioned(mContext,
                SubscriptionManager.getDefaultVoicePhoneId())) {
            Log.d("@M_" + TAG, "wfc_enabled:" + wfcEnabled + " wfcMode:" + wfcMode +
                    "wfcModePref:" + wfcModePref);
            //Op06WfcSettings wfcSettings = Op06WfcSettings.getInstance(mContext);
            mWfcSettings.updateWfcModePreference(wfcModePref, wfcEnabled);
            super.updateWfcModePreference(root, wfcModePref, wfcEnabled, wfcMode);
        }
    }

    //@Override
     /** Called from onPause.
     * Used in WirelessSettings
     * @param event event happened
     * @return
     */
     /*
    public void onWirelessSettingsEvent(int event) {
        Log.d("@M_" + TAG, "Wireless setting event:" + event);
        switch (event) {
            case DefaultWfcSettingsExt.RESUME:
                WifiCallingUtils.registerReceiver(mContext, mReceiver);
                break;

            case DefaultWfcSettingsExt.PAUSE:
                WifiCallingUtils.unRegisterReceiver(mContext, mReceiver);
                break;
            case DefaultWfcSettingsExt.CONFIG_CHANGE:
                // 1) Need to remove wfc preference as Host app will try to remove AOSP pref
                // But if plugin has added switch, it has to be removed as below
                // 2) No need to add explicitly as host app will call customizedWfcPreference()
                // which will add the preference
                if (!ImsManager.isWfcEnabledByPlatform(mContext)) {
                    if (mPreferenceFragment != null) {
                        //OP06WfcSettings wfcSettings = OP06WfcSettings.getInstance(mContext);
                        mWfcSettings.removeWfcPreference(mPreferenceFragment.getPreferenceScreen());
                    }
                }
                break;
            default:
                break;
        }
    }*/

    @Override
     /** Called from onPause/onResume.
     * Used in WifiCallingSettings
     * @param event event happened
     * @return
     */
    public void onWfcSettingsEvent(int event) {
        Log.d("@M_" + TAG, "Wificalling setting event:" + event);
        switch (event) {
            case DefaultWfcSettingsExt.CREATE:
                registerForWfcProvisioningChange();
                break;

            case DefaultWfcSettingsExt.RESUME:
                WifiCallingUtils.registerReceiver(mContext, mReceiver);
                break;

            case DefaultWfcSettingsExt.PAUSE:
                WifiCallingUtils.unRegisterReceiver(mContext, mReceiver);
                break;

            case DefaultWfcSettingsExt.DESTROY:
                unRegisterForWfcProvisioningChange();
                break;

            default:
                break;
        }
    }

    /**
     * @param context Context
     * @param phoneId phoneId
     * @return
     * Customize boolean whether provisioned or not
     */
    @Override
    public boolean isWifiCallingProvisioned(Context context, int phoneId) {
        if (!WifiCallingUtils.isImsProvSupported(context,
                MtkSubscriptionManager.getSubIdUsingPhoneId(phoneId))) {
            Log.d(TAG, "Ims prov feature not supported, phoneId:" + phoneId);
            return super.isWifiCallingProvisioned(context, phoneId);
        }
         boolean isWifiCallingProvisioned = WifiCallingUtils
                .isWifiCallingProvisioned(context, phoneId);
        if (!isWifiCallingProvisioned) {
            if (mPreferenceFragment != null) {
                Log.d(TAG, "WFC is not provisioned. remove WFC Pref");
                //Op06WfcSettings wfcSettings = Op06WfcSettings.getInstance(mContext);
                mWfcSettings.removeWfcPreference(mPreferenceFragment.getPreferenceScreen());
            }
        }
        return isWifiCallingProvisioned;
    }

    /*
    * Observes WFC provision settings changes .
    */
    private void registerForWfcProvisioningChange() {
        Uri contentUri = Uri.parse("content://com.mediatek.ims.config.provider/tb_master/");
        Uri result = ContentUris.withAppendedId(contentUri, SubscriptionManager
                .getDefaultVoicePhoneId());
        final Uri i = Uri.withAppendedPath(result, "VOICE_OVER_WIFI_SETTING_ENABLED");
        Log.d(TAG, "uri:" + i);

        mContentObserver = new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                this.onChange(selfChange, i);
            }

            @Override
            public void onChange(boolean selfChange, Uri uri) {
                if (i != null && i.equals(uri)) {
                    if (!WifiCallingUtils.isWifiCallingProvisioned(mContext,
                            SubscriptionManager.getDefaultVoicePhoneId())) {
                        if (mPreferenceFragment != null
                                && (TextUtils.equals(mPreferenceFragment.getActivity().getClass()
                                    .getSimpleName(), "WifiCallingSettingsActivity")
                                    || TextUtils.equals(mPreferenceFragment.getActivity().getClass()
                                    .getSimpleName(), "SubSettings"))) {
                            Log.d(TAG, "finishing wfcsettings as its deProvisioned");
                            mPreferenceFragment.getActivity().finish();
                            mPreferenceFragment = null;
                        }
                    }
                }
            }
        };
        mContext.getContentResolver().registerContentObserver(i, false, mContentObserver);
    }

    private void unRegisterForWfcProvisioningChange() {
        mContext.getContentResolver().unregisterContentObserver(mContentObserver);
    }

    private void handleWfcModeUpdation() {
        ListPreference wfcModeList = (ListPreference) mPreferenceFragment
                .findPreference(AOSP_BUTTON_WFC_MODE);
        updateWfcModePreference(mPreferenceFragment.getPreferenceScreen(), wfcModeList,
                ImsManager.isWfcEnabledByUser(mContext), ImsManager.getWfcMode(mContext));
    }
}


