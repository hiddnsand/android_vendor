/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.services.telephony;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;

import com.android.internal.os.SomeArgs;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.services.telephony.EmergencyCallStateListener;
import com.android.services.telephony.Log;

import com.mediatek.internal.telephony.RadioManager;

import com.mediatek.telephony.MtkTelephonyManagerEx;

public class MtkEmergencyCallStateListener extends EmergencyCallStateListener {

    /// M: CC: ECC retry @{
    private TelephonyManager mTm;
    private AirplaneModeObserver mAirplaneModeObserver;
    private Context mContext;
    /// @}

    @Override
    public void waitForRadioOn(Phone phone, Callback callback) {
        Log.i(this, "waitForRadioOn: Phone " + phone.getPhoneId());

        if (mPhone != null) {
            // If there already is an ongoing request, ignore the new one!
            return;
        }

        /// M: CC: ECC retry @{
        mTm = TelephonyManager.getDefault();
        mAirplaneModeObserver = new AirplaneModeObserver(mHandler);
        /// @}

        SomeArgs args = SomeArgs.obtain();
        args.arg1 = phone;
        args.arg2 = callback;
        mHandler.obtainMessage(MSG_START_SEQUENCE, args).sendToTarget();
    }

    @Override
    protected void onServiceStateChanged(ServiceState state) {
        Log.i(this, "onServiceStateChanged(), new state = %s, Phone = %s", state,
                mPhone.getPhoneId());

        /// M: CC: ECC retry @{
        Log.i(this, "onServiceStateChanged(), phoneId=" + mPhone.getPhoneId()
                + ", isEmergencyOnly=" + state.isEmergencyOnly()
                + ", phoneType=" + mPhone.getPhoneType()
                + ", hasCard=" + mTm.hasIccCard(mPhone.getPhoneId()));
        /// @}

        // Possible service states:
        // - STATE_IN_SERVICE        // Normal operation
        // - STATE_OUT_OF_SERVICE    // Still searching for an operator to register to,
        //                           // or no radio signal
        // - STATE_EMERGENCY_ONLY    // Phone is locked; only emergency numbers are allowed
        // - STATE_POWER_OFF         // Radio is explicitly powered off (airplane mode)

        if (isOkToCall(state.getState()) || state.isEmergencyOnly()) {
            // Woo hoo!  It's OK to actually place the call.
            Log.i(this, "onServiceStateChanged: ok to call!");

            onComplete(true);
            cleanup();
        } else {
            // The service state changed, but we're still not ready to call yet.
            Log.i(this, "onServiceStateChanged: not ready to call yet, keep waiting.");
        }
    }


    @Override
    protected boolean isOkToCall(int serviceState) {
        /// M: CC: Vzw ECC/hVoLTE redial @{
        if (MtkTelephonyManagerEx.getDefault().useVzwLogic()) {
            return (mPhone.getState() == PhoneConstants.State.OFFHOOK) ||
                (serviceState != ServiceState.STATE_POWER_OFF) ||
                TelephonyManager.from(mContext).isWifiCallingAvailable();
        }
        /// @}

        return ((mPhone.getState() == PhoneConstants.State.OFFHOOK) ||
                /// M: CC: ECC retry @{
                //mPhone.getServiceStateTracker().isRadioOn() ||
                (serviceState == ServiceState.STATE_IN_SERVICE) ||
                (serviceState == ServiceState.STATE_EMERGENCY_ONLY)) ||
                // Allow STATE_OUT_OF_SERVICE if we are at the max number of retries.
                (mNumRetriesSoFar == MAX_NUM_RETRIES &&
                serviceState == ServiceState.STATE_OUT_OF_SERVICE) ||
                /// @}
                /// M: CC: [ALPS02185470] Only retry once for WFC ECC. @{
                TelephonyManager.from(mContext).isWifiCallingAvailable();
                /// @}
    }


    @Override
    protected void onRetryTimeout() {
        int serviceState = mPhone.getServiceState().getState();
        Log.i(this, "onRetryTimeout():  phone state = %s, service state = %d, retries = %d.",
                mPhone.getState(), serviceState, mNumRetriesSoFar);

        /// M: CC: ECC retry @{
        Log.i(this, "onRetryTimeout(), phoneId=" + mPhone.getPhoneId()
                + ", emergencyOnly=" + mPhone.getServiceState().isEmergencyOnly()
                + ", phonetype=" + mPhone.getPhoneType()
                + ", hasCard=" + mTm.hasIccCard(mPhone.getPhoneId()));
        /// @}

        // - If we're actually in a call, we've succeeded.
        // - Otherwise, if the radio is now on, that means we successfully got out of airplane mode
        //   but somehow didn't get the service state change event.  In that case, try to place the
        //   call.
        // - If the radio is still powered off, try powering it on again.

        if (isOkToCall(serviceState) || mPhone.getServiceState().isEmergencyOnly()) {
            Log.i(this, "onRetryTimeout: Radio is on. Cleaning up.");

            // Woo hoo -- we successfully got out of airplane mode.
            onComplete(true);
            cleanup();
        } else {
            // Uh oh; we've waited the full TIME_BETWEEN_RETRIES_MILLIS and the radio is still not
            // powered-on.  Try again.

            mNumRetriesSoFar++;
            Log.i(this, "mNumRetriesSoFar is now " + mNumRetriesSoFar);

            if (mNumRetriesSoFar > MAX_NUM_RETRIES) {
                Log.w(this, "Hit MAX_NUM_RETRIES; giving up.");
                cleanup();
            } else {
                Log.i(this, "Trying (again) to turn on the radio.");
                /// M: CC: ECC retry @{
                // Use RadioManager for powering on radio during ECC [ALPS01785370]
                if (RadioManager.isMSimModeSupport()) {
                    //RadioManager will help to turn on radio even this iccid is off by sim mgr
                    Log.i(this, "isMSimModeSupport true, use RadioManager forceSetRadioPower");
                    RadioManager.getInstance().forceSetRadioPower(true, mPhone.getPhoneId());
                } else {
                    //android's default action
                    Log.i(this, "isMSimModeSupport false, use default setRadioPower");
                    mPhone.setRadioPower(true);
                }
                /// @}
                startRetryTimer();
            }
        }
    }

    @Override
    protected void onComplete(boolean isRadioReady) {
        if (mCallback != null) {
            Callback tempCallback = mCallback;
            mCallback = null;
            tempCallback.onComplete(this, isRadioReady);
            /// M: CC: ECC retry @{
            mContext.getContentResolver().unregisterContentObserver(mAirplaneModeObserver);
            /// @}
        }
    }

    /// M: CC: ECC retry @{
    public void setContext(Context context) {
        mContext = context;
    }

    private class AirplaneModeObserver extends ContentObserver {
        public AirplaneModeObserver(Handler handler) {
            super(handler);
            mContext.getContentResolver().registerContentObserver(
                    Settings.Global.getUriFor(Settings.Global.AIRPLANE_MODE_ON), false, this);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            boolean isAirplaneModeOn = Settings.Global.getInt(
                    mContext.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) > 0;
            Log.i(this, "onChange, isAirplaneModeOn=" + isAirplaneModeOn);
            if (isAirplaneModeOn) {
                cleanup();
            }
        }
    }
    /// @}
}
