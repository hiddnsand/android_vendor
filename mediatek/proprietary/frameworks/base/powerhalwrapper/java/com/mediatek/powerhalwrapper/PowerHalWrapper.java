/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.powerhalwrapper;

//import android.content.Context;
//import android.os.IBinder;
//import android.os.RemoteException;
//import android.os.ServiceManager;
import android.util.Log;
import android.os.Trace;
import android.os.HwBinder;
import android.os.IHwBinder;
/*
import com.mediatek.am.AMEventHookData;
import com.mediatek.am.AMEventHookData.AfterActivityDestroyed;
import com.mediatek.am.AMEventHookData.AfterActivityPaused;
import com.mediatek.am.AMEventHookData.AfterActivityResumed;
import com.mediatek.am.AMEventHookData.AfterActivityStopped;
import com.mediatek.am.AMEventHookData.BeforeActivitySwitch;
import com.mediatek.am.AMEventHookData.StartProcess;
*/
/// MTK power
import vendor.mediatek.hardware.power.V1_1.*;

public class PowerHalWrapper {

    private static final String TAG = "PowerHalWrapper";

    private static final int AMS_BOOST_TIME = 2000;
    private static boolean AMS_BOOST_PROCESS_CREATE = true;
    private static boolean AMS_BOOST_PACK_SWITCH = true;
    private static boolean AMS_BOOST_ACT_SWITCH = true;

    /// M: MTK Power @{
    private static IPower mPowerProxy = null;
    /// M: MTK Power @}

    private static PowerProxyDeathRecipient mPowerDeathRecipient = null;

    public PowerHalWrapper() {
        /// M: MTK power @{
        if(mPowerProxy == null) {
            Log.e(TAG, "PowerHalWrapper");
                try {
                    mPowerProxy = IPower.getService();
                    mPowerDeathRecipient = PowerProxyDeathRecipient.getInstance();
                    if(mPowerProxy != null && mPowerDeathRecipient != null) {
                        Log.e(TAG, "PowerHalWrapperDeathRecipient");
                        mPowerProxy.linkToDeath(
                                     mPowerDeathRecipient, 0);
                    }
                } catch (Exception e) {
                    mPowerProxy = null;
                    mPowerDeathRecipient = null;
                }
        }
        ///  MTK power @}
    }

    private static final class PowerProxyDeathRecipient implements HwBinder.DeathRecipient {
        private static volatile PowerProxyDeathRecipient sInstance = null;
        private static Object lock = new Object();

        public static PowerProxyDeathRecipient getInstance() {
            if (null == sInstance) {
                synchronized (lock) {
                    if (null == sInstance) {
                        sInstance = new PowerProxyDeathRecipient();
                    }
                }
            }
            return sInstance;
        }

        private PowerProxyDeathRecipient() {

        }

        @Override
        public void serviceDied(long cookie) {
             log("Power Hal serviceDied");
             mPowerProxy = null;
             sInstance = null;
             if(mPowerProxy == null) {
                try {
                    mPowerProxy = IPower.getService();
                    mPowerDeathRecipient = PowerProxyDeathRecipient.getInstance();
                    if(mPowerProxy != null && mPowerDeathRecipient != null) {
                        Log.e(TAG, "PowerHalWrapperDeathRecipient");
                        mPowerProxy.linkToDeath(
                                     mPowerDeathRecipient, 0);
                    }
                } catch (Exception e) {
                    mPowerProxy = null;
                }
             }
        }
    }

    private void mtkPowerHint(int hint, int data) {
        if(mPowerProxy != null) {
            try {
                mPowerProxy.mtkPowerHint(hint, data);
            } catch (Exception e) {
                mPowerProxy = null;
            }
        }
    }

    private void mtkCusPowerHint(int hint, int data) {
        if(mPowerProxy != null) {
            try {
                mPowerProxy.mtkCusPowerHint(hint, data);
            } catch (Exception e) {
                mPowerProxy = null;
            }
        }
    }

    public void galleryBoostEnable(int timeoutMs) {
            Log.e(TAG, "<galleryBoostEnable> do boost with " + timeoutMs + "ms");
            mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_GALLERY_BOOST, timeoutMs);
    }

    public void setRotationBoost(int boostTime) {
            Log.e(TAG, "<setRotation> do boost with " + boostTime + "ms");
            mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_APP_ROTATE, boostTime);
    }

    public void setSpeedDownload(int timeoutMs) {
            Log.e(TAG, "<setSpeedDownload> do boost with " + timeoutMs + "ms");
            mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_WIPHY_SPEED_DL, timeoutMs);
    }

    public void setFpsGo(boolean enable) {
            Log.e(TAG, "<setFpsGo> enable:" + enable);
            if (enable)
                mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_FPSGO, MtkHintOp.MTK_HINT_ALWAYS_ENABLE);
            else
                mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_FPSGO, 0);
    }

    public void setInstallationBoost(boolean enable) {
            Log.e(TAG, "<setInstallationBoost> enable:" + enable);
            if (enable)
                mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PMS_INSTALL, 15000);
            else
                mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PMS_INSTALL, 0);
    }


    /* AMS event handler */
    public void amsBoostResume(String lastResumedPackageName, String nextResumedPackageName) {
        //Log.e(TAG, "<amsBoostResume> last:" + lastResumedPackageName +
                                       //", next:" + nextResumedPackageName);

        Trace.asyncTraceBegin(Trace.TRACE_TAG_ACTIVITY_MANAGER, "amPerfBoost", 0);
        if(mPowerProxy != null) {
            try {
                if (lastResumedPackageName == null ||
                    !lastResumedPackageName.equalsIgnoreCase(nextResumedPackageName)) {
                    AMS_BOOST_PACK_SWITCH = true;
                    mPowerProxy.mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PACK_SWITCH,
                        AMS_BOOST_TIME);
                }
                else {
                    AMS_BOOST_ACT_SWITCH = true;
                    mPowerProxy.mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_ACT_SWITCH,
                        AMS_BOOST_TIME);
                }
            } catch (Exception e) {
                mPowerProxy = null;
            }
        }
    }

    public void amsBoostProcessCreate(String hostingType, String packageName) {
        if(hostingType.compareTo("activity") == 0) {
            Trace.asyncTraceBegin(Trace.TRACE_TAG_ACTIVITY_MANAGER, "amPerfBoost", 0);
            AMS_BOOST_PROCESS_CREATE = true;
            if(mPowerProxy != null) {
                try {
                    mPowerProxy.mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PROCESS_CREATE, 2000);
                } catch (Exception e) {
                    mPowerProxy = null;
                }
            }
        }
    }

    public void amsBoostStop() {
        //log("amsBoostStop");
        if (AMS_BOOST_PACK_SWITCH) {
            AMS_BOOST_PACK_SWITCH = false;
            if(mPowerProxy != null) {
               try {
                   mPowerProxy.mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PACK_SWITCH, 0);
               } catch (Exception e) {
                   mPowerProxy = null;
               }
            }
        }

        if (AMS_BOOST_ACT_SWITCH) {
            AMS_BOOST_ACT_SWITCH = false;
            if(mPowerProxy != null) {
                try {
                    mPowerProxy.mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_ACT_SWITCH, 0);
                } catch (Exception e) {
                    mPowerProxy = null;
                }
            }
        }

        if (AMS_BOOST_PROCESS_CREATE) {
            AMS_BOOST_PROCESS_CREATE = false;
            if(mPowerProxy != null) {
                try {
                    mPowerProxy.mtkPowerHint(MtkPowerHint.MTK_POWER_HINT_PROCESS_CREATE, 0);
                } catch (Exception e) {
                    mPowerProxy = null;
                }
            }
        }

        Trace.asyncTraceEnd(Trace.TRACE_TAG_ACTIVITY_MANAGER, "amPerfBoost", 0);
    }

    public void amsBoostNotify(int pid, String activityName, String packageName) {
        //Log.e(TAG, "amsBoostNotify pid:" + pid +
        //      ",activity:" + activityName + ", package:" + packageName);
        if(mPowerProxy != null) {
            try {
                mPowerProxy.notifyAppState(packageName, activityName,
                                            pid, MtkActState.STATE_RESUMED);
            } catch (Exception e) {
                mPowerProxy = null;
            }
        }
    }

    private static void log(String info) {
        Log.d("@M_" + TAG, "[PerfServiceWrapper] " + info + " ");
    }

    private static void loge(String info) {
        Log.e("@M_" + TAG, "[PerfServiceWrapper] ERR: " + info + " ");
    }
}

