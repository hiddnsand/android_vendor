/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.rcs.blacklist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.UserManager;
import android.telephony.PhoneNumberUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * BlacklistSyncBlocked.
 */
public class BlacklistSyncBlocked {

    private static final String TAG = "Blacklist_block";
    /**
     * BLOCKED_URI.
     */
    public static final Uri BLOCKED_URI = Uri.parse("content://com.android.blockednumber/blocked");

    /**
     * Number.
     */
    public static final String COLUMN_BLOCKED_NUMBER = "original_number";

    /**
     * e164_number.
     */
    public static final String COLUMN_BLOCKED_E164_NUMBER = "e164_number";

    /**
     * Projection.
     */
    public static final String[]BLOCKED_PROJECTION = {
                COLUMN_BLOCKED_NUMBER,
                COLUMN_BLOCKED_E164_NUMBER,
                "_id"};

    private static final int EVENT_SYNC_INSERT = 1;
    private static final int EVENT_SYNC_DELETE = 2;
    private static final int EVENT_SYNC_DELETE_WHERE = 3;

    private static BlacklistSyncBlocked sInstance = null;
    Context mContext;
    private Handler mHandler;
    private boolean mDeleting;

    /**
     * Constructor.
     */
    private BlacklistSyncBlocked() {
        HandlerThread thread = new HandlerThread("sync-blocked");
        thread.start();

        mHandler = new Handler(thread.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Log.i(TAG, TAG, "sync handleMessage " + msg.what);
                switch (msg.what) {
                    case EVENT_SYNC_INSERT:
                        doInsertBlockedNumber((String) msg.obj);
                        break;
                    case EVENT_SYNC_DELETE:
                        doDeleteBlockedNumber((String) msg.obj);
                        break;
                    case EVENT_SYNC_DELETE_WHERE:
                        doDeleteNumberByWhere((ArrayList<String>) msg.obj);
                        break;
                    default:
                        break;
                }
            }
        };
    }

    protected static BlacklistSyncBlocked getInstance() {
        if (sInstance == null) {
            sInstance = new BlacklistSyncBlocked();
        }

        return sInstance;
    }

    protected void readBlockedNumber(Context context) {
        Log.i(TAG, this, "[readBlockedNumber] start ");

        Cursor blockedCursor = context.getContentResolver().query(
                    BLOCKED_URI, BLOCKED_PROJECTION, null, null, null);
        try {
            while (blockedCursor != null && blockedCursor.moveToNext()) {
                String blockedNumber = blockedCursor.getString(0);
                Log.d(TAG, this, "read blocked number :" + Log.formatNumber(blockedNumber));
            }
        } finally {
            if (blockedCursor != null) {
                blockedCursor.close();
            }
            Log.i(TAG, this, "[readBlockedNumber] end");
        }
    }

    protected void insertBlockedNumber(Context context, String number) {
        mContext = context;
        final UserManager userManager = UserManager.get(mContext);
        if (!userManager.isPrimaryUser()) {
            return;
        }
        Log.i(TAG, this, "[insertBlockedNumber] :" + Log.formatNumber(number));
        mHandler.obtainMessage(EVENT_SYNC_INSERT, number).sendToTarget();
    }

    private void doInsertBlockedNumber(String number) {
        ContentValues blockedVal = new ContentValues();
        blockedVal.put(COLUMN_BLOCKED_NUMBER, number);
        Log.i(TAG, this, "doInsertBlockedNumber :" + Log.formatNumber(number));
        mContext.getContentResolver().insert(BLOCKED_URI, blockedVal);
    }

    protected void deleteBlockedNumber(Context context, String number) {
        mContext = context;
        final UserManager userManager = UserManager.get(mContext);
        if (!userManager.isPrimaryUser()) {
            return;
        }
        Log.i(TAG, this, "[deleteBlockedNumber] :" + Log.formatNumber(number));
        setDeleting(true);
        //mHandler.obtainMessage(EVENT_SYNC_DELETE, number).sendToTarget();
        doDeleteBlockedNumber(number);
        setDeleting(false);
    }

    private void doDeleteBlockedNumber(String number) {
        String where = COLUMN_BLOCKED_NUMBER + "=" + number;
        Log.i(TAG, this, "[doDeleteBlockedNumber] " + Log.formatNumber(number));
        Cursor blockedCursor = mContext.getContentResolver().query(
                BLOCKED_URI, BLOCKED_PROJECTION, null, null, null);
        try {
            while (blockedCursor != null && blockedCursor.moveToNext()) {
                String blockedNumber = blockedCursor.getString(0);
                String blockedId =  blockedCursor.getString(2);
                if (blockedNumber.equals(number) ||
                    PhoneNumberUtils.compare(number, blockedNumber, false)) {
                    Log.d(TAG, this, "[doDeleteBlockedNumber] delete :" + blockedId);
                    mContext.getContentResolver().delete(
                            Uri.withAppendedPath(BLOCKED_URI, blockedId), null, null);
                }
            }
        } finally {
            if (blockedCursor != null) {
                blockedCursor.close();
            }
        }
    }

    protected void deleteBlockedNumberByWhere(Context context, String where, String[] whereArgs) {
        String ids = null;
        ArrayList<String> idList = new ArrayList<String>();

        mContext = context;
        final UserManager userManager = UserManager.get(mContext);
        if (!userManager.isPrimaryUser()) {
            Log.i(TAG, this, "[insertBlockedNumber] not primary user");
            return;
        }
        Log.i(TAG, this, "[deleteBlockedNumberByWhere]:" + where);
        setDeleting(true);

        if (where != null && where.contains("_id")) {
            if (where.contains("in") || where.contains("IN")) {
                if (whereArgs == null) {
                    where.replaceAll(" ", "");
                    int i = where.indexOf("(");
                    ids = where.substring(i + 1, where.length() - 1);
                } else {
                    int size = whereArgs.length;
                    Log.i(TAG, this, "[deleteBlockedNumberByWhere] whereArgs.size " + size);
                    if (size > 0) {
                        idList.addAll(Arrays.asList(whereArgs));
                    }
                }
            }
        }

        if (ids != null) {
            String[] sp = ids.split(",");
            idList.addAll(java.util.Arrays.asList(sp));
            Log.i(TAG, this, "[deleteBlockedNumberByWhere]id is:" + idList.toString());
        }

        if (idList.size() > 0) {
            //mHandler.obtainMessage(EVENT_SYNC_DELETE_WHERE, idList).sendToTarget();
            //synchronized to call.
            doDeleteNumberByWhere(idList);
        }

        setDeleting(false);
    }

    private void doDeleteNumberByWhere(ArrayList<String> idList) {
        Log.i(TAG, this, "[doDeleteNumberByWhere]idList:" + idList.toString());

        Cursor blackCursor = mContext.getContentResolver().query(
                    BlacklistData.AUTHORITY_URI,
                    new String[] {"_id", BlacklistData.BlacklistTable.PHONE_NUMBER},
                    null, null, null);

        try {
            if (blackCursor == null) {
                return;
            }
            while (blackCursor.moveToNext()) {
                String rawId = blackCursor.getString(0);
                String rawNumber = blackCursor.getString(1);
                Log.d(TAG, this, "[doDeleteNumberByWhere]id is:" + rawId);
                if (idList.contains(rawId)) {
                    doDeleteBlockedNumber(rawNumber);
                }
            }
        } finally {
            if (blackCursor != null) {
                blackCursor.close();
            }
        }
    }

    protected void setDeleting(boolean isDelete) {
        Log.i(TAG, this, "[setDeleting] isDelete:" + isDelete);
        mDeleting = isDelete;
    }

    protected boolean getDeleting() {
        return mDeleting;
    }
}
