/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op.telecom.gtt;

import java.util.Set;

import android.os.Bundle;
import android.util.Log;

import mediatek.telecom.MtkCall;
import mediatek.telecom.MtkTelecomManager;

import com.android.server.telecom.Call;
import com.android.server.telecom.Call.Listener;
import com.mediatek.server.telecom.ext.DefaultGttEventExt;

import com.mediatek.op.telecom.OpTelecomUtils;

public class OpGttEventExt extends DefaultGttEventExt {
    private static final String TAG = "OpGttEventExt";
    private int mGttLocal = MtkTelecomManager.GTT_MODE_OFF;
    private int mGttRemote = MtkTelecomManager.GTT_MODE_OFF;

    public boolean handleGttEvent(String event, Bundle extras,
            Object call, Object listeners) {
        if (!OpTelecomUtils.isGttOn()) {
            return super.handleGttEvent(event, extras, call, listeners);
        }
        Log.d(TAG, "event: " + event + " extras: " + extras + " call: " + call);
        boolean isHandle = false;
        if (event.equals(MtkTelecomManager.EVENT_GTT_MODE_CHANGED)) {
            int gttLocal = extras.getInt(MtkTelecomManager.EXTRA_GTT_MODE_LOCAL,
                    MtkTelecomManager.GTT_MODE_OFF);
            int gttRemote = extras.getInt(MtkTelecomManager.EXTRA_GTT_MODE_REMOTE,
                    MtkTelecomManager.GTT_MODE_OFF);
            if ((mGttLocal != gttLocal) || (mGttRemote != gttRemote)) {
                mGttLocal = gttLocal;
                mGttRemote = gttRemote;
                isHandle = true;
                for (Listener listener : (Set<Listener>) listeners) {
                    listener.forceUpdateCall((Call)call);
                }
            }
        }
        Log.d(TAG, "handleGttEvent: " + isHandle);
        return isHandle;
    }

    public int getGttProperties() {
        if (!OpTelecomUtils.isGttOn()) {
            return super.getGttProperties();
        }
        int properties = 0;
        if (mGttLocal == MtkTelecomManager.GTT_MODE_ON) {
            properties |= MtkCall.MtkDetails.PROPERTY_GTT_LOCAL;
        }
        if (mGttRemote == MtkTelecomManager.GTT_MODE_ON) {
            properties |= MtkCall.MtkDetails.PROPERTY_GTT_REMOTE;
        }
        Log.d(TAG, "getGttProperties: " + properties);
        return properties;
    }
}
