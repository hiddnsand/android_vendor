/*
 * Copyright (C) 2016 MediaTek Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See http://www.gnu.org/licenses/gpl-2.0.html for more details.
 */

#include <stdbool.h> // needed by gatekeeper.h below
#include <hardware/hardware.h>
#include <hardware/gatekeeper.h>
#include "module.h"

/* -------------------------------------------------------------------------
   Module definitions needed for integrtion with Android HAL framework.
   -------------------------------------------------------------------------*/

static struct hw_module_methods_t soft_gatekeeper_module_methods = {
    .open = soft_gatekeeper_open,
};

__attribute__((visibility("default")))
struct gatekeeper_module HAL_MODULE_INFO_SYM = {
    .common =
        {
            .tag = HARDWARE_MODULE_TAG,
            .module_api_version = GATEKEEPER_MODULE_API_VERSION_0_1,
            .hal_api_version = HARDWARE_HAL_API_VERSION,
            .id = GATEKEEPER_HARDWARE_MODULE_ID,
            .name = GATEKEEPER_SOFTWARE_MODULE_NAME,
            .author = "MediaTek",
            .methods = &soft_gatekeeper_module_methods,
            .dso = 0,
            .reserved = {},
        },
};

