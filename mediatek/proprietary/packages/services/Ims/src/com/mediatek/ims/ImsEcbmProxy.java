/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.telephony.Rlog;

import com.android.ims.internal.IImsEcbm;
import com.android.ims.internal.IImsEcbmListener;
import com.mediatek.ims.ril.ImsCommandsInterface;

public class ImsEcbmProxy extends IImsEcbm.Stub {
    private static final String LOG_TAG = "ImsEcbmProxy";

    protected static final int EVENT_ON_ENTER_ECBM = 1;
    protected static final int EVENT_ON_EXIT_ECBM = 2;

    private IImsEcbmListener mListener;
    private ImsCommandsInterface mImsRILAdapter;

    private boolean mEnableImsEcbm = false;

    private final Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EVENT_ON_ENTER_ECBM:
                    onEnterECBM();
                    break;
                case EVENT_ON_EXIT_ECBM:
                    onExitECBM();
                    break;
                default:
                    break;
            }
        }
    };

    ImsEcbmProxy(ImsCommandsInterface adapter) {
        if (!mEnableImsEcbm) {
            Rlog.d(LOG_TAG, "IMS ECBM does not support");
            return;
        }

        Rlog.d(LOG_TAG, "new EcbmProxy");
        mImsRILAdapter = adapter;
        if (mImsRILAdapter == null) {
            return;
        }

        mImsRILAdapter.registerForOnEnterECBM(mHandler, EVENT_ON_ENTER_ECBM, null);
        mImsRILAdapter.registerForOnExitECBM(mHandler, EVENT_ON_EXIT_ECBM, null);

    }

    /**
      * Sets the listener.
      * @param listener ecbm event listenr
      */
    public void setListener(IImsEcbmListener listener) {
        Rlog.d(LOG_TAG, "set Ecbm Listener" + listener);
        mListener = listener;
    }

    /**
     * Requests Modem to come out of ECBM mode.
     */
    public void exitEmergencyCallbackMode() {
       if (mImsRILAdapter != null) {
           Rlog.d(LOG_TAG, "request exit ECBM");
           mImsRILAdapter.requestExitEmergencyCallbackMode(null);
       }
    }

    /**
     * Notify listener enter ECBM.
     */
    public void onEnterECBM() {
        if (mListener != null) {
            Rlog.d(LOG_TAG, "on enter ECBM");
            try {
                mListener.enteredECBM();
            } catch (RemoteException e) {
                Rlog.e(LOG_TAG, "RemoteException: " + e);
            }
        }
    }

    /**
     * Notify listener exit ECBM.
     */
    public void onExitECBM() {
        if (mListener != null) {
            Rlog.d(LOG_TAG, "on exit ECBM");
            try {
                mListener.exitedECBM();
            } catch (RemoteException e) {
                Rlog.e(LOG_TAG, "RemoteException: " + e);
            }
        }
    }
}
