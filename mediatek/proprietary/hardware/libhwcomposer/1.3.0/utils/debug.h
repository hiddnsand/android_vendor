#ifndef UTILS_DEBUG_H_
#define UTILS_DEBUG_H_

#ifdef MTK_LOG_ENABLE
#undef MTK_LOG_ENABLE
#endif
#define MTK_LOG_ENABLE 1
#include <cutils/log.h>
#include <utils/Singleton.h>
#include <utils/String8.h>

#ifndef DEBUG_LOG_TAG
#error "DEBUG_LOG_TAG is not defined!!"
#endif

#ifdef MTK_USER_BUILD
#define HWC_LOGV(x, ...)
#define HWC_LOGD(x, ...)
#else
#define HWC_LOGV(x, ...) ALOGV("[%s] " x, DEBUG_LOG_TAG, ##__VA_ARGS__)
#define HWC_LOGD(x, ...) ALOGD("[%s] " x, DEBUG_LOG_TAG, ##__VA_ARGS__)
#endif

#define HWC_LOGI(x, ...) ALOGI("[%s] " x, DEBUG_LOG_TAG, ##__VA_ARGS__)
#define HWC_LOGW(x, ...) ALOGW("[%s] " x, DEBUG_LOG_TAG, ##__VA_ARGS__)
#define HWC_LOGE(x, ...) ALOGE("[%s] " x, DEBUG_LOG_TAG, ##__VA_ARGS__)

#define HWC_LOG_BUFSIZE 128
#ifndef MTK_USER_BUILD
#define DEFINE_LOG_BUF(buf) \
    char buf[HWC_LOG_BUFSIZE]; \
    char TMP_HWC_COMPACT_BUF[HWC_LOG_BUFSIZE];

#define INIT_LOG_BUF(buf) \
    do { \
        if (HWC_LOG_BUFSIZE > 0) \
            buf[0] = '\0'; \
    } while(0)

#define COMPACT_LOG(output, ...) \
    do { \
        snprintf(TMP_HWC_COMPACT_BUF, HWC_LOG_BUFSIZE, ##__VA_ARGS__); \
        strncat((output), TMP_HWC_COMPACT_BUF, HWC_LOG_BUFSIZE); \
    } while(0)
#else
#define DEFINE_LOG_BUF(buf)
#define INIT_LOG_BUF(buf)
#define COMPACT_LOG(output, ...)
#endif

#define ATRACE_TAG ATRACE_TAG_GRAPHICS
#include <utils/Trace.h>

enum HWC_DEBUG_COMPOSE_LEVEL
{
    COMPOSE_ENABLE_ALL  = 0,
    COMPOSE_DISABLE_MM  = 1,
    COMPOSE_DISABLE_UI  = 2,
    COMPOSE_DISABLE_ALL = 3
};

enum HWC_DEBUG_DUMP_LEVEL
{
    DUMP_NONE = 0,
    DUMP_MM   = 1,
    DUMP_UI   = 2,
    DUMP_SYNC = 4,
    DUMP_ALL  = 7
};

enum HWC_PROFILING_LEVEL
{
    PROFILE_NONE = 0,
    PROFILE_COMP = 1,
    PROFILE_BLT  = 2,
    PROFILE_TRIG = 4,
};

#ifdef USE_SYSTRACE
#define HWC_ATRACE_CALL() android::ScopedTrace ___tracer(ATRACE_TAG, __FUNCTION__)
#define HWC_ATRACE_NAME(name) android::ScopedTrace ___tracer(ATRACE_TAG, name)
#define HWC_ATRACE_INT(name, value) android::Tracer::traceCounter(ATRACE_TAG, name, value)
#else
#define HWC_ATRACE_CALL()
#define HWC_ATRACE_NAME(name)
#define HWC_ATRACE_INT(name, value)
#endif // USE_SYSTRACE


#define DBG_PRE_INFOL(x, ...) Debugger::getInstance().m_prepare_info.appendFormat("L%d-"x, __LINE__,  ##__VA_ARGS__)
#define DBG_PRE_INFO(x, ...) Debugger::getInstance().m_prepare_info.appendFormat(x, ##__VA_ARGS__)
#define DBG_PRE_INFO_INIT(x, ...) (Debugger::getInstance().m_prepare_info = String8::format(x, ##__VA_ARGS__))

using namespace android;

class Debugger : public Singleton<Debugger>
{
public:
    String8 m_prepare_info;
};

#endif // UTILS_DEBUG_H_
