LOCAL_CLANG := true

LOCAL_CFLAGS := -DLOG_TAG=\"SurfaceFlinger\"
LOCAL_CPPFLAGS := -std=c++11
LOCAL_CFLAGS += -Wall -Werror -Wunused -Wunreachable-code

ifeq ($(MTK_DYNAMIC_FPS_SUPPORT),yes)
	LOCAL_CFLAGS += -DMTK_DYNAMIC_FPS_SUPPORT
endif

ifeq ($(MTK_DYNAMIC_FPS_FW_SUPPORT),yes)
	LOCAL_CFLAGS += -DMTK_DYNAMIC_FPS_FRAMEWORK_SUPPORT
endif

LOCAL_C_INCLUDES:= \
	$(TOP)/$(MTK_PATH_SOURCE)/hardware/libgem/inc

LOCAL_SRC_FILES := \
	lib/policy_service/FpsPolicy.cpp \
	lib/policy_service/FpsPolicyService.cpp \
	lib/policy_service/FpsInfo.cpp \
	lib/policy_service/FpsPolicyTracker.cpp

LOCAL_SHARED_LIBRARIES := \
	libutils \
	libcutils \
	liblog
