/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
package com.mediatek.apst.target.activity;

import java.util.ArrayList;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;
import com.mediatek.apst.target.R;

public class PermissionCheckUtil {
    private static final String TAG = "PermissionCheckUtil";
    private static final String[] ALL_PERMISSIONS = {
            Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS, Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_SMS, Manifest.permission.WRITE_SMS,
            Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS,
            Manifest.permission.RECEIVE_MMS, Manifest.permission.READ_CALENDAR,
            Manifest.permission.WRITE_CALENDAR,
            Manifest.permission.WRITE_EXTERNAL_STORAGE };
    private static final String[] REQUIRED_PERMISSIONS = {};
    public static final String PREVIOUS_ACTIVITY_INTENT = "previous_intent";
    public static final String MISSING_PERMISSIONS = "missing_permissions";
    public static int sPermissionsActivityStarted = 0;

    public static boolean requestAllPermissions(Activity activity) {
        return requestPermissions(activity, ALL_PERMISSIONS);
    }

    public static boolean requestRequiredPermissions(Activity activity) {
        return requestPermissions(activity, REQUIRED_PERMISSIONS);
    }

    public static boolean requestPermissions(Activity activity, String[] permissions) {
        ArrayList<String> missingList = getMissingPermissions(activity, permissions);
        return requestPermissions(activity, missingList);
    }

    public static ArrayList<String> getMissingPermissions(Activity activity,
            String[] requiredPermissions) {
        final ArrayList<String> missingList = new ArrayList<String>();
        for (int i = 0; i < requiredPermissions.length; i++) {
            if (!hasPermission(activity, requiredPermissions[i])) {
                missingList.add(requiredPermissions[i]);
            }
        }
        return missingList;
    }

    public static boolean hasNeverGrantedPermissions(Activity activity,
            ArrayList<String> permissionList) {
        boolean isNeverGranted = false;
        for (int i = 0; i < permissionList.size(); i++) {
            if (isNeverGrantedPermission(activity, permissionList.get(i))) {
                isNeverGranted = true;
                Log.d(TAG, " hasNeverGrantedPermissions " + permissionList.get(i)
                        + " is always denied");
                break;
            }
        }
        return isNeverGranted;
    }

    public static boolean isNeverGrantedPermission(Activity activity, String permission)
    {
        return !activity.shouldShowRequestPermissionRationale(permission);
    }

    public static boolean requestPermissions(Activity activity,
            ArrayList<String> missingList) {
        if (missingList.size() == 0) {
            Log.d(TAG, " requestPermissions all permissions granted");
            return false;
        }
        final String[] missingArray = new String[missingList.size()];
        missingList.toArray(missingArray);
        Intent intentPermissions = new Intent(activity, PermissionCheckActivity.class);
        intentPermissions.putExtra(PREVIOUS_ACTIVITY_INTENT, activity.getIntent());
        intentPermissions.putExtra(MISSING_PERMISSIONS, missingArray);
        setPermissionActivityCount(true);
        activity.startActivity(intentPermissions);
        activity.finish();
        return true;
    }

    public static boolean checkAllPermissions(Context context) {
        return checkPermissions(context, ALL_PERMISSIONS);
    }

    public static boolean checkRequiredPermissions(Context context) {
        return checkPermissions(context, REQUIRED_PERMISSIONS);
    }

    public static boolean checkPermissions(Context context, String[] permissions) {
        for (int i = 0; i < permissions.length; i++) {
            if (!hasPermission(context, permissions[i])) {
                Log.d(TAG, "checkPermissions false : " + permissions[i]);
                return false;
            }
        }
        return true;
    }

    public static boolean onRequestPermissionsResult(Activity activity, int requestCode,
            String[] permissions, int[] grantResults, boolean needFinish) {
        boolean result = true;
        boolean hasNeverGranted = false;
        Log.d(TAG, "onRequestPermissionsResult grantResults: "
                + (grantResults == null ? 0 : grantResults.length));
        if (grantResults == null || grantResults.length == 0) {
            Log.d(TAG, "onRequestPermissionsResult return : " + result);
            return result;
        }
        for (int i = 0; i < permissions.length; i++) {
            if (!hasPermission(activity, permissions[i])) {
                Log.d(TAG, "onRequestPermissionsResult return false");
                result = false;
                // Show toast
                if (isRequiredPermission(permissions[i])
                        || isNeverGrantedPermission(activity, permissions[i])) {
                    showNoPermissionsToast(activity);
                    if (needFinish) {
                        activity.finish();
                    }
                    return result;
                }
            }
        }
        Log.d(TAG, "onRequestPermissionsResult return : " + result);
        return result;
    }

    public static boolean hasPermission(Context context, String permission) {
        return context.checkSelfPermission(permission) ==
            PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasSMSPermission(Context context) {
        return context.checkSelfPermission(Manifest.permission.RECEIVE_SMS) ==
                PackageManager.PERMISSION_GRANTED;
    }

    public static void showNoPermissionsToast(Context context) {
        Toast.makeText(context,
                R.string.denied_required_permission,
                Toast.LENGTH_LONG).show();
    }

    public static boolean isPermissionChecking() {
        Log.d(TAG, " isPermissionChecking Activity Count: " +
               sPermissionsActivityStarted);
        return sPermissionsActivityStarted > 0;
    }

    /*
     * It means permission activity would be finished if startActivity is false.
     */
    public static void setPermissionActivityCount(boolean startActivity) {
        if (startActivity) {
            if (sPermissionsActivityStarted < 0) {
                sPermissionsActivityStarted = 0;
            }
            sPermissionsActivityStarted++;
        } else {
            sPermissionsActivityStarted--;
            if (sPermissionsActivityStarted < 0) {
                sPermissionsActivityStarted = 0;
            }
        }
        Log.d(TAG, "setPermissionActivityCount: " + sPermissionsActivityStarted
                + ", start: " + startActivity);
    }

    public static boolean isRequiredPermission(String permission) {
        for (int i = 0; i < REQUIRED_PERMISSIONS.length; i++) {
            if (REQUIRED_PERMISSIONS[i].equals(permission)) {
                Log.d(TAG, "isRequiredPermission: " + permission);
                return true;
            }
        }
        return false;
    }
}
