# Copyright 2006 The Android Open Source Project

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE:= sn
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_TAGS:= optional
LOCAL_INIT_RC := sn.rc

LOCAL_SRC_FILES:= \
    sn.c

LOCAL_SHARED_LIBRARIES := libcutils libc liblog

include $(BUILD_EXECUTABLE)
