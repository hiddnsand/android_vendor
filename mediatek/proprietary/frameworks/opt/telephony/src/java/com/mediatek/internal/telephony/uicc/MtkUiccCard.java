/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony.uicc;

import android.content.Context;
import com.android.internal.telephony.uicc.IccCardStatus;
import com.android.internal.telephony.uicc.UiccCard;
import com.android.internal.telephony.uicc.UiccCarrierPrivilegeRules;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.CommandsInterface.RadioState;
import com.android.internal.telephony.uicc.IccCardStatus.CardState;
import com.android.internal.telephony.uicc.UiccCardApplication;
import com.android.internal.telephony.uicc.IccCardApplicationStatus.AppType;
import com.mediatek.internal.telephony.MtkRIL;
import com.mediatek.internal.telephony.uicc.MtkUiccCarrierPrivilegeRules;

import android.telephony.Rlog;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;


/**
 * {@hide}
 */
public class MtkUiccCard extends UiccCard {
    protected static final String LOG_TAG_EX = "MtkUiccCard";
    protected static final boolean DBG = true;

    private static final int EVENT_GET_ATR_DONE = 100;
    private static final int EVENT_OPEN_CHANNEL_WITH_SW_DONE = 101;

    static final String[] UICCCARD_PROPERTY_RIL_UICC_TYPE = {
        "gsm.ril.uicctype",
        "gsm.ril.uicctype.2",
        "gsm.ril.uicctype.3",
        "gsm.ril.uicctype.4",
    };

    private static final String[]  PROPERTY_RIL_FULL_UICC_TYPE = {
        "gsm.ril.fulluicctype",
        "gsm.ril.fulluicctype.2",
        "gsm.ril.fulluicctype.3",
        "gsm.ril.fulluicctype.4",
    };

    private String mIccType = null; /* Add for USIM detect */

    public MtkUiccCard(Context c, CommandsInterface ci, IccCardStatus ics, int phoneId,
            boolean isUpdateSiminfo) {
        if (DBG) mtkLog("Creating simId " + phoneId + ",isUpdateSiminfo" + isUpdateSiminfo);
        mCardState = ics.mCardState;
        mPhoneId = phoneId;
        update(c, ci, ics, isUpdateSiminfo);
    }
    public MtkUiccCard(Context c, CommandsInterface ci, IccCardStatus ics, int phoneId) {
        super(c, ci, ics, phoneId);
        // MTK-START
        if (DBG) mtkLog("MtkUiccCard Creating");
        // MTK-END
    }

    public void iccExchangeSimIOEx(int fileID, int command,
            int p1, int p2, int p3, String pathID, String data, String pin2, Message onComplete) {
        mCi.iccIO(command, fileID, pathID, p1, p2, p3, data, pin2,
              mHandlerEx.obtainMessage(EVENT_SIM_IO_DONE, onComplete));
    }
    // MTK-START
    public void iccGetAtr(Message onComplete) {
        ((MtkRIL)mCi).getATR(mHandlerEx.obtainMessage(EVENT_GET_ATR_DONE, onComplete));
    }
    // MTK-END
    public String getIccCardType() {
        //int slot = -1;
        //if (SubscriptionController.getInstance() != null) {
        //    slot = SubscriptionController.getInstance().getSlotId(
        //            SubscriptionController.getInstance().getSubIdUsingPhoneId(
        //            mPhoneId));
        //    mIccType = SystemProperties.get(UICCCARD_PROPERTY_RIL_UICC_TYPE[slot]);
        //}
        mIccType = SystemProperties.get(UICCCARD_PROPERTY_RIL_UICC_TYPE[mPhoneId]);
        if (DBG) mtkLog("getIccCardType(): iccType = " + mIccType + ", slot " + mPhoneId);
        return mIccType;
    }

    public String[] getFullIccCardType() {
        return SystemProperties.get(PROPERTY_RIL_FULL_UICC_TYPE[mPhoneId]).split(",");
    }

    public void iccOpenChannelWithSw(String AID, Message onComplete) {
        //TODO: HIDL extension impl.
        //mCi.iccOpenChannelWithSw(AID,
        //    mHandlerEx.obtainMessage(EVENT_OPEN_CHANNEL_WITH_SW_DONE, onComplete));
    }

    public void update(Context c, CommandsInterface ci, IccCardStatus ics,
            boolean isUpdateSimInfo) {
        synchronized (mLock) {
            CardState oldState = mCardState;
            mCardState = ics.mCardState;
            mUniversalPinState = ics.mUniversalPinState;
            mGsmUmtsSubscriptionAppIndex = ics.mGsmUmtsSubscriptionAppIndex;
            mCdmaSubscriptionAppIndex = ics.mCdmaSubscriptionAppIndex;
            mImsSubscriptionAppIndex = ics.mImsSubscriptionAppIndex;
            mContext = c;
            mCi = ci;

            //update applications
            if (DBG) mtkLog(ics.mApplications.length + " applications");
            for ( int i = 0; i < mUiccApplications.length; i++) {
                if (mUiccApplications[i] == null) {
                    //Create newly added Applications
                    if (i < ics.mApplications.length) {
                        mUiccApplications[i] = new MtkUiccCardApplication(this,
                                ics.mApplications[i], mContext, mCi);
                    }
                } else if (i >= ics.mApplications.length) {
                    //Delete removed applications
                    mUiccApplications[i].dispose();
                    mUiccApplications[i] = null;
                } else {
                    //Update the rest
                    mUiccApplications[i].update(ics.mApplications[i], mContext, mCi);
                }
            }

            createAndUpdateCatServiceLocked();

            // Reload the carrier privilege rules if necessary.
            mtkLog("Before privilege rules: " + mCarrierPrivilegeRules + " : " + mCardState);
            if (mCarrierPrivilegeRules == null && mCardState == CardState.CARDSTATE_PRESENT) {
                mCarrierPrivilegeRules = new MtkUiccCarrierPrivilegeRules(this,
                        mHandler.obtainMessage(EVENT_CARRIER_PRIVILEGES_LOADED));
            } else if (mCarrierPrivilegeRules != null
                    && mCardState != CardState.CARDSTATE_PRESENT) {
                mCarrierPrivilegeRules = null;
            }

            sanitizeApplicationIndexesLocked();

            RadioState radioState = mCi.getRadioState();
            if (DBG) mtkLog("update: radioState=" + radioState + " mLastRadioState="
                    + mLastRadioState + "isUpdateSimInfo= " + isUpdateSimInfo);
            // No notifications while radio is off or we just powering up
            if (isUpdateSimInfo) {
                if (radioState == RadioState.RADIO_ON && mLastRadioState == RadioState.RADIO_ON) {
                    if (oldState != CardState.CARDSTATE_ABSENT &&
                            mCardState == CardState.CARDSTATE_ABSENT) {
                        if (DBG) mtkLog("update: notify card removed");
                        mAbsentRegistrants.notifyRegistrants();
                        mHandler.sendMessage(mHandler.obtainMessage(EVENT_CARD_REMOVED, null));
                    } else if (oldState == CardState.CARDSTATE_ABSENT &&
                            mCardState != CardState.CARDSTATE_ABSENT) {
                        if (DBG) mtkLog("update: notify card added");
                        mHandler.sendMessage(mHandler.obtainMessage(EVENT_CARD_ADDED, null));
                    }
                }
            }
            mLastRadioState = radioState;
        }
    }

    // MTK-END

    /**
     * This function makes sure that application indexes are valid
     * and resets invalid indexes. (This should never happen, but in case
     * RIL misbehaves we need to manage situation gracefully)
     */
    protected void sanitizeApplicationIndexesLocked() {
        super.sanitizeApplicationIndexesLocked();
        // MTK-START
        if (DBG) {
            mtkLog("sanitizeApplicationIndexesLocked  GSM index= " + mGsmUmtsSubscriptionAppIndex +
                    "  CDMA index = " + mCdmaSubscriptionAppIndex + "  IMS index = "
                    + mImsSubscriptionAppIndex);
        }
        // MTK-END
    }

    protected int checkIndex(int index, AppType expectedAppType, AppType altExpectedAppType) {
        if (mUiccApplications == null || index >= mUiccApplications.length) {
            mtkLoge("App index " + index + " is invalid since there are no applications");
            return -1;
        }

        if (index < 0) {
            // This is normal. (i.e. no application of this type)
            return -1;
        }

        // MTK-START
        if (mUiccApplications[index] == null) {
            mtkLoge("App index " + index + " is null since there are no applications");
            return -1;
        }

        mtkLog("MtkUiccCard checkIndex mUiccApplications[" + index + "].getType()= "
            + mUiccApplications[index].getType());
        // MTK-END

        if (mUiccApplications[index].getType() != expectedAppType &&
            mUiccApplications[index].getType() != altExpectedAppType) {
            mtkLoge("App index " + index + " is invalid since it's not " +
                    expectedAppType + " and not " + altExpectedAppType);
            return -1;
        }

        // Seems to be valid
        return index;
    }

    protected void onIccSwap(boolean isAdded) {
        // MTK-START: AOSP SIM PLUG IN/OUT
        super.onIccSwap(isAdded);
        // MTK-END
        return;
    }

    public void update(Context c, CommandsInterface ci, IccCardStatus ics) {
        synchronized (mLock) {
            CardState oldState = mCardState;
            mCardState = ics.mCardState;
            mUniversalPinState = ics.mUniversalPinState;
            mGsmUmtsSubscriptionAppIndex = ics.mGsmUmtsSubscriptionAppIndex;
            mCdmaSubscriptionAppIndex = ics.mCdmaSubscriptionAppIndex;
            mImsSubscriptionAppIndex = ics.mImsSubscriptionAppIndex;
            mContext = c;
            mCi = ci;

            //update applications
            if (DBG) mtkLog(ics.mApplications.length + " applications");
            for ( int i = 0; i < mUiccApplications.length; i++) {
                if (mUiccApplications[i] == null) {
                    //Create newly added Applications
                    if (i < ics.mApplications.length) {
                    //MTK BSP+ START
                        mUiccApplications[i] = new MtkUiccCardApplication(this,
                                ics.mApplications[i], mContext, mCi);
                    //MTK BSP+ END
                    }
                } else if (i >= ics.mApplications.length) {
                    //Delete removed applications
                    mUiccApplications[i].dispose();
                    mUiccApplications[i] = null;
                } else {
                    //Update the rest
                    mUiccApplications[i].update(ics.mApplications[i], mContext, mCi);
                }
            }

            createAndUpdateCatServiceLocked();

            // Reload the carrier privilege rules if necessary.
            mtkLog("Before privilege rules: " + mCarrierPrivilegeRules + " : " + mCardState);
            if (mCarrierPrivilegeRules == null && mCardState == CardState.CARDSTATE_PRESENT) {
                mCarrierPrivilegeRules = new MtkUiccCarrierPrivilegeRules(this,
                        mHandler.obtainMessage(EVENT_CARRIER_PRIVILEGES_LOADED));
            } else if (mCarrierPrivilegeRules != null &&
                    mCardState != CardState.CARDSTATE_PRESENT) {
                mCarrierPrivilegeRules = null;
            }

            sanitizeApplicationIndexesLocked();

            RadioState radioState = mCi.getRadioState();
            if (DBG) mtkLog("update: radioState=" + radioState + " mLastRadioState="
                    + mLastRadioState);
            // No notifications while radio is off or we just powering up
            if (radioState == RadioState.RADIO_ON && mLastRadioState == RadioState.RADIO_ON) {
                if (oldState != CardState.CARDSTATE_ABSENT &&
                        mCardState == CardState.CARDSTATE_ABSENT) {
                    if (DBG) mtkLog("update: notify card removed");
                    mAbsentRegistrants.notifyRegistrants();
                    mHandler.sendMessage(mHandler.obtainMessage(EVENT_CARD_REMOVED, null));
                } else if (oldState == CardState.CARDSTATE_ABSENT &&
                        mCardState != CardState.CARDSTATE_ABSENT) {
                    if (DBG) mtkLog("update: notify card added");
                    mHandler.sendMessage(mHandler.obtainMessage(EVENT_CARD_ADDED, null));
                }
            }
            mLastRadioState = radioState;
        }
    }

    @Override
    protected void log(String msg) {
        Rlog.d(LOG_TAG_EX, msg + " (phoneId " + mPhoneId + ")");
    }

    @Override
    protected void loge(String msg) {
        Rlog.e(LOG_TAG_EX, msg + " (phoneId " + mPhoneId + ")");
    }

    protected void mtkLog(String msg) {
        Rlog.d(LOG_TAG_EX, msg + " (phoneId " + mPhoneId + ")");
    }
    protected void mtkLoge(String msg) {
        Rlog.e(LOG_TAG_EX, msg + " (phoneId " + mPhoneId + ")");
    }

    private Handler mHandlerEx = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (DBG) {
                mtkLog("mHandlerEx Received message " + msg + "[" + msg.what+  "]");
            }
            switch (msg.what) {
                case EVENT_SIM_IO_DONE:
                case EVENT_GET_ATR_DONE:
                case EVENT_OPEN_CHANNEL_WITH_SW_DONE:
                    AsyncResult ar = (AsyncResult)msg.obj;
                    if (ar.exception != null) {
                        loge("Error in SIM access with exception" + ar.exception);
                    }
                    AsyncResult.forMessage((Message)ar.userObj, ar.result, ar.exception);
                    ((Message)ar.userObj).sendToTarget();
                    break;
                 default:
                    mtkLoge("Unknown Event " + msg.what);
            }
        }
    };
}
