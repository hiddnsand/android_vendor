LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/frameworks/opt/mdm))
include $(CLEAR_VARS)
LOCAL_MODULE = md_monitor
LOCAL_MODULE_CLASS = EXECUTABLES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libutils libcutils libselinux libccci_util_sys libmdloggerrecycle libc++ liblog
LOCAL_INIT_RC = md_monitor.rc
LOCAL_SRC_FILES = $(call get-prebuilt-src-arch,arm arm64)/md_monitor
include $(BUILD_PREBUILT)
endif
