/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "ModuleSIBCapture"

#define MTK_LOG_ENABLE 1
#include <binder/IBinder.h>
#include <gui/ISurfaceComposer.h>
#include <gui/SurfaceComposerClient.h>

#include <linux/mtkfb.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <cutils/log.h>
#include <hwcomposer_defs.h>
#include "ModuleSIBCapture.h"
#include "RPCClient.h"
extern "C" {
#include "sysenv_utils.h"
}using namespace android;

int ModuleSIBCapture::getSIBCaptureValue(RPCClient* msgSender) {
    char result[RESULT_SIZE] = { 0 };
    const char* val;
    int paraNum = msgSender->ReadInt();
    ALOGD("paraNum =%d \n", paraNum);
    if (paraNum != 0) {
        msgSender->PostMsg((char*) ERROR);
        return -1;
    }

    ALOGD("Enter getSIBCaptureValue()\n");
    val = sysenv_get("md1_phy_cap_gear");
    if (val != NULL) {
        sprintf(result, "%s", val);
    } else {
        sprintf(result, "%s", "-1");
    }
    ALOGD("getSIBCaptureValue() ,result=%s\n", result);
    msgSender->PostMsg(result);
    return 0;
}

int ModuleSIBCapture::setSIBCaptureValue(RPCClient* msgSender) {
    int SIBCaptureValue = 0;
    char value[2] = { 0 };
    int paraNum = msgSender->ReadInt();
    ALOGD("paraNum %d\n", paraNum);
    //1.paraNum
    if (paraNum != 1) {
        msgSender->PostMsg((char*) ERROR);
        return -1;
    }
    int type = msgSender->ReadInt();
    ALOGD("type:%d", type);
    //2.para type
    if (type != PARAM_TYPE_INT) {
        msgSender->PostMsg((char*) ERROR);
        return -1;
    }
    //3.para len
    int len = msgSender->ReadInt();
    ALOGD("len:%d", len);

    //4.para value
    SIBCaptureValue = msgSender->ReadInt();
    value[0] = (char) SIBCaptureValue + 0x30;
    ALOGD("setSIBCaptureValue:%d ,value = %s\n", SIBCaptureValue, value);
    int ret = sysenv_set("md1_phy_cap_gear", value);

    if (ret < 0) {
        msgSender->PostMsg((char*) ERROR);
        return -1;
    } else {
        msgSender->PostMsg((char*) SUCCESS);
        return 1;
    }

    return 0;
}

