package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class StepDetectorSensorActivity extends CustomerSensorBaseActivity {

    public StepDetectorSensorActivity() {
        super(Utils.KEY_STEP_DETECTOR_STATUS, Sensor.TYPE_STEP_DETECTOR);
    }
}
