package com.mediatek.op06.phone;

import android.content.Context;

import com.mediatek.phone.ext.ICallFeaturesSettingExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;

public class Op06PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {
    private Context mContext;

    public Op06PhoneCustomizationFactory(Context context) {
        mContext = context;
    }

    public ICallFeaturesSettingExt makeCallFeaturesSettingExt() {
        return new Op06CallFeaturesSettingExt(mContext);
    }
}
