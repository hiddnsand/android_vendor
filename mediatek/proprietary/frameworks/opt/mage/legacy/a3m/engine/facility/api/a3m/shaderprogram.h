/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * ShaderProgram class
 *
 */
#pragma once
#ifndef A3M_SHADERPROGRAM_H
#define A3M_SHADERPROGRAM_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/detail/programresource.h>  /* for ProgramResource */
#include <a3m/detail/shaderresource.h>  /* for ShaderResource */
#include <a3m/assetcache.h> /* for AssetCache */
#include <a3m/pointer.h>          /* for SharedPtr */
#include <a3m/shaderuniform.h>   /* ShaderUniform */
#include <a3m/detail/resourcecache.h>   /* for ResourceCache */

namespace a3m
{
  /** \defgroup a3mShaderprogram Shader Program
   * \ingroup  a3mRefRender
   * \ingroup  a3mRefAssets
   *
   * The shader program class encapsulates a program running on the GPU
   * defined by a vertex shader and fragment shader pair.
   *
   * ShaderPrograms are Assets and are therefore managed by an AssetCache in the
   * AssetPool.
   *
   * \note
   * Generally, the client will want to use AssetCachePool to create and manage
   * asset caches, rather than creating them manually.
   *
   * \code
   * // Create a shader program cache and add a path from which to load programs
   * ShaderProgramCache::Ptr cache( new ShaderProgramCache() );
   * registerSource( *cache, ".\\assets\\shaders");
   *
   * // Register a loader for the cache to use
   * cache->registerLoader( myLoader );
   *
   * // Load ShaderProgram from ".\assets\shaders"
   * ShaderProgram::Ptr shaderProgram = cache->get( "phong.sp" );
   * \endcode
   *
   *  @{
   */

  /* Forward declarations */
  class RenderContext;
  class ShaderProgramCache;
  class VertexBuffer;

  /** Shader Program class.
   * A ShaderProgram object contains an OpenGL shader program object. Upon
   * construction the supplied vertex and fragment shader source is compiled
   * and linked. The vertex attributes in a VertexBuffer can be bound to those
   * used in the shader program using the bind() method.
   *
   * Uniforms variable names can be acquired, and uniform property names can be
   * set and retrived.  The property value of a uniform can also be set, but
   * the actual uniform is inaccessible.
   */
  class ShaderProgram : public Shared, NonCopyable
  {
  public:
    A3M_NAME_SHARED_CLASS( ShaderProgram )

    /** Smart pointer type */
    typedef a3m::SharedPtr< ShaderProgram > Ptr;

    /** Cache type for this class */
    typedef ShaderProgramCache CacheType;

    /** Select this program for use. */
    void enable(RenderContext& context);

    /** Validate the shader program.
     * Warning: this may be slow and should be used for debugging purposes
     * only.
     * \return A3M_TRUE if the program was successfuly compiled, linked and
     * validated.
     */
    A3M_BOOL isValid();

    /** Bind the VertexBuffer object.
     * Associates any vertex attributes used in the shader program with
     * matching attributes defined in the VertexBuffer.
     */
    void bind( VertexBuffer& buffer /**< Vertex buffer containing
                                         matching vertex attributes */ );

    /** Returns TRUE if a uniform with the given property name exists.
     * \see setUniformPropertyName()
     */
    A3M_BOOL uniformExists(A3M_CHAR8 const* propertyName) const;

    /** Returns the name of the shader uniform variable, as it appears in the
     * shader code, at a given index.
     * \see getUniformCount()
     */
    A3M_CHAR8 const* getUniformInstanceName(A3M_INT32 i) const;

    /** Sets the name of the property associated with the shader uniform at a
     * given index.
     * The property of a uniform is used to identify its function independently
     * of its name in the shader code.
     */
    void setUniformPropertyName(A3M_INT32 i, A3M_CHAR8 const* propertyName);

    /** \see setUniformPropertyName()
     */
    A3M_CHAR8 const* getUniformPropertyName(A3M_INT32 i) const;

    /** Overrides any externally linked uniform value with a custom value.
     * If the uniform is linked to a render context, the link will be
     * maintained, but will be overridden by the value provided.  If a null
     * pointer is set, any linked value will be restored.
     * \see getUniformCount()
     */
    void setUniformPropertyValue(A3M_INT32 i, ShaderUniformBase::Ptr const& value);

    /** Gives the number of shader uniforms in this program.
     */
    A3M_INT32 getUniformCount() const;

    /** Links the shader program uniforms with those a render context.
     * The render context contains global uniform values which can be used by
     * the shader program.  In order to be linked, a uniform must share a
     * "property name", as specified by calling setUniformPropertyName(), with
     * a uniform in the render context.
     */
    void link(RenderContext& context);

    /** Get the compiled binary code for the shader.
     * Use this function to save the compiled shader program. The buffer
     * pointed to by the "binary" parameter should be freed using
     * delete [] when you are finished with it.
     * \return true if successful.
     */
    A3M_BOOL getBinary( A3M_UINT32& size,
                        /**< Length of binary data */
                        A3M_UINT32& format,
                        /**< Format of binary data */
                        A3M_CHAR8*& binary
                        /**< Buffer containing precompiled shader */);

  private:
    friend class ShaderProgramCache; /* Is ShaderProgram's factory class */

    /*
     * An instance of a shader uniform within the program.
     * A3M can link shader uniforms together such that uniforms from different
     * programs share the same value, hence uniform "instances".
     */
    struct UniformInstance
    {
      UniformInstance(
        A3M_CHAR8 const* name,
        A3M_INT32 location,
        A3M_INT32 texUnit,
        ShaderUniformBase::Ptr uniform) :
        instanceName(name),
        location(location),
        texUnit(texUnit),
        currentValue(uniform)
      {
        // linkedValue and propertyValue are originally initialised to null.
      }

      std::string instanceName; /* Name of the variable in the shader code */
      std::string propertyName; /* Name of the property the uniform represents */
      A3M_INT32 location; /* Index of the uniform in the shader program */
      A3M_INT32 texUnit; /* Name of the uniform's texture unit, if any */
      ShaderUniformBase::Ptr linkedValue; /* Value shared with the render context */
      ShaderUniformBase::Ptr propertyValue; /* Value set manually on the program */
      ShaderUniformBase::Ptr currentValue; /* The previously set value */
    };

    /*
     * Structure containing attribute information.
     */
    struct AttributeInfo
    {
      AttributeInfo(A3M_CHAR8 const* name, A3M_INT32 location) :
        name(name),
        location(location)
      {
      }

      std::string name;
      A3M_INT32 location;
    };

    typedef std::vector<UniformInstance> UniformInstanceVector;
    typedef std::vector<AttributeInfo> AttributeVector;

    struct PropertyNameEquals;

    /* Returns TRUE if passed a valid index to a uniform instance.
     * Logs an error if the index is not valid.
     */
    A3M_BOOL indexInRange(A3M_INT32 index) const;

    /*
     * Private constructor.
     * This constructor is called by ShaderProgramCache.
     */
    ShaderProgram(
      detail::ProgramResource::Ptr const& resource /* Program resource */);

    /* Utility function to create a shader uniform. */
    template< class T >
    void createUniform( A3M_CHAR8 const* propertyName,
                        A3M_INT32 location,
                        A3M_INT32 texUnit,
                        A3M_INT32 size );

    /* Builds an internal list of all the uniforms defined by this program.
     */
    void getUniforms();

    /* Builds an internal list of all the attributes defined by this program.
     */
    void getAttributes();

    /* Enables the uniforms whose values have changed.
     */
    void enableUniforms();

    RenderContext* m_context; /* The context the program is currently linked with */
    detail::ProgramResource::Ptr m_resource; /* OpenGL program resource */
    UniformInstanceVector m_uniformInstances; /* List of program uniforms instances */
    AttributeVector m_attributes; /* List of program attributes */
  };

  /**
   * AssetCache specialised for storing and creating ShaderProgram assets.
   */
  class ShaderProgramCache : public AssetCache<ShaderProgram>
  {
  public:
    /** Smart pointer type for this class */
    typedef SharedPtr< ShaderProgramCache > Ptr;

    /** Constructor.
     */
    ShaderProgramCache();

    /** Creates a shader program from source.
     * Compiles and links the given vertex and fragment shader source code into
     * an OpenGL shader program object.
     * \return The shader program, or null if compilation or linking failed
     */
    ShaderProgram::Ptr create(
      A3M_CHAR8 const* vsSource, /**< Source code for vertex shader */
      A3M_CHAR8 const* fsSource, /**< Source code for fragment shader */
      A3M_CHAR8 const* name = 0 /**< Optional name to give the asset. If
                                    omitted, the asset will not be reachable
                                    via the AssetCache::get() function. */);

    /** Creates a shader program from precompiled binary.
     * Loads a precompiled shader program from memory. This function may not be
     * supported on certain platforms, and will return null if so.
     * \return The shader program, or null if loading failed
     */
    ShaderProgram::Ptr create(
      A3M_UINT32 size, /**< Length of binary data */
      A3M_UINT32 format, /**< Format of binary data */
      A3M_CHAR8 const* binary, /**< Buffer containing precompiled shader */
      A3M_CHAR8 const* name = 0 /**< Optional name to give the asset. If
                                    omitted, the asset will not be reachable
                                    via the AssetCache::get() function. */);
  };

  /** Returns whether shader program binaries are supported.
   * \return TRUE if supported
   */
  A3M_BOOL getShaderProgramBinariesSupported();

  /** @} */

} /* end of namespace */

#endif /* A3M_SHADERPROGRAM_H */
