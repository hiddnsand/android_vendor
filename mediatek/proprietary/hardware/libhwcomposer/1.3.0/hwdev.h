#ifndef HWC_HWDEV_H_
#define HWC_HWDEV_H_

#include <ui/Rect.h>
#include <utils/Singleton.h>
#include <utils/threads.h>

#include <linux/mtkfb.h>
#include <linux/disp_svp.h>

#include "display.h"

using namespace android;

struct mtk_dispif_info;
struct OverlayPrepareParam;
struct OverlayPortParam;
class MTKM4UDrv;

// ---------------------------------------------------------------------------

enum FBDEV_DEBUG_FLAG
{
    FBDEV_DEBUG_TIMEOUT = 0x01,
};

class DispDevice : public Singleton<DispDevice>
{
public:
    DispDevice();
    ~DispDevice();

    // initOverlay() initializes overlay related hw setting
    void initOverlay();

    // getMaxOverlayInputNum() gets overlay supported input amount
    int getMaxOverlayInputNum();

    // createOverlaySession() creates overlay composition session
    status_t createOverlaySession(int dpy, int mode = DISP_SESSION_DECOUPLE_MODE);

    // destroyOverlaySession() destroys overlay composition session
    void destroyOverlaySession(int dpy);

    // truggerOverlaySession() used to trigger overlay engine to do composition
    status_t triggerOverlaySession(int dpy);

    // disableOverlaySession() usd to disable overlay session to do composition
    void disableOverlaySession(int dpy, int num);

    // getOverlaySessionInfo() gets specific display device information
    status_t getOverlaySessionInfo(int dpy, disp_session_info* info);

    // getAvailableOverlayInput gets available amount of overlay input
    // for different session
    int getAvailableOverlayInput(int dpy);

    // prepareOverlayInput() gets timeline index and fence fd of overlay input layer
    void prepareOverlayInput(int dpy, OverlayPrepareParam* param);

    // enableOverlayInput() enables single overlay input layer
    void enableOverlayInput(int dpy, OverlayPortParam* param, int id);

    // updateOverlayInputs() updates multiple overlay input layers
    void updateOverlayInputs(int dpy, OverlayPortParam* const* params, int num);

    // prepareOverlayOutput() gets timeline index and fence fd for overlay output buffer
    void prepareOverlayOutput(int dpy, OverlayPrepareParam* param);

    // enableOverlayOutput() enables overlay output buffer
    void enableOverlayOutput(int dpy, OverlayPortParam* param);

private:
    enum
    {
        DISP_INVALID_SESSION = -1,
    };

    int m_dev_fd;

    int m_ovl_input_num;

    disp_session_input_config m_input_config[DisplayManager::MAX_DISPLAYS];
};

// --------------------------------------------------------------------------

class HdmiDevice : public Singleton<HdmiDevice>
{
public:
    HdmiDevice();
    ~HdmiDevice();

    // get specific display device information
    void getDisplayInfo(int dpy, mtk_dispif_info* info);

    struct HdmiInputInfo
    {
        void* va;
        void* mva;
        int ion_fd;
        unsigned int pitch;
        unsigned int format;
        int secure;

        unsigned int fence_index;
        int fence_fd;
    };

    // get fence
    void prepareInput(HdmiInputInfo& param);

    // set buffer
    void enableInput(HdmiInputInfo& param,
            const Rect& src_crop, const Rect& dst_crop);

    // disbale buffer
    void disableInput();

private:
    int m_dev_fd;
};

// --------------------------------------------------------------------------

class MMUDevice : public Singleton<MMUDevice>
{
public:
    MMUDevice();
    ~MMUDevice();

    // enable client
    int enable(int client);

    // map mva
    int map(int client, unsigned int vaddr, int size, unsigned int* mva);

    // unmap mva
    int unmap(int client, unsigned int vaddr, int size, unsigned int mva);

    // flush memory
    int flush(int client, unsigned int vaddr, int size);

    // config to enable or disable port
    int config(int port, bool enabled);

private:
    MTKM4UDrv* m_dev;
};

// --------------------------------------------------------------------------

class IONDevice : public Singleton<IONDevice>
{
public:
    IONDevice();
    ~IONDevice();

    // import handle from ion share fd
    unsigned int import(int ion_fd);

    // allocate memory by ion driver
    unsigned int alloc(int size);

    // close handle
    void close(int ion_hnd);

    // get address mapped by m4u for HW usage
    unsigned int getMMUAddress(int client, int ion_hnd);

    // get address mapped by cpu for SW usage
    unsigned int getCPUAddress(int ion_hnd);

    // flush ion memory
    void flush(int ion_hnd);

private:
    int m_dev_fd;
};
// --------------------------------------------------------------------------

class TEEMemDevice : public Singleton<TEEMemDevice>
{
public:
    TEEMemDevice();
    ~TEEMemDevice();

    // allocate secure memory
    unsigned int alloc(int size);

    // free secure memory
    void free(unsigned int handle);

private:
    // typedef for TEE lib dynamic loading
    typedef enum {
        W_MC_DRV_OK = 0,
    } W_mcResult_t;
    typedef void * W_UREE_SECUREMEM_HANDLE;
    typedef W_mcResult_t FT_tlcMemOpen(void);
    typedef W_mcResult_t FT_tlcMemClose(void);
    typedef W_mcResult_t FT_UREE_AllocSecuremem(W_UREE_SECUREMEM_HANDLE, unsigned int, unsigned int);
    typedef W_mcResult_t FT_UREE_UnreferenceSecuremem(W_UREE_SECUREMEM_HANDLE, unsigned int *);

    // open TEE memory controller
    void open();

    // close TEE memory controller
    void close();

    // reference count lock
    mutable Mutex m_ref_lock;

    // referenct count for tee memory controller
    int m_ref_cnt;

    // is ro.mtk_trustonic_tee_support equals to 1
    int m_is_mtk_trustonic_tee_support;

    // TEE function dynamic loading
    void* m_sec_lib;
    FT_tlcMemOpen* pfnOpen;
    FT_tlcMemClose* pfnClose;
    FT_UREE_AllocSecuremem* pfnAlloc;
    FT_UREE_UnreferenceSecuremem* pfnUnref;
};

#endif // HWC_HWDEV_H_
