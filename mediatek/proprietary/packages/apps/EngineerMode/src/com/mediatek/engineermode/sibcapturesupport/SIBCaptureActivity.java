/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.sibcapturesupport;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Toast;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.emsvr.AFMFunctionCallEx;
import com.mediatek.engineermode.emsvr.FunctionReturn;

import java.io.IOException;
import java.util.ArrayList;

public class SIBCaptureActivity extends Activity implements OnCheckedChangeListener {
    private static final String TAG = "EM/SIBCapture";
    private static final int REBOOT = 4;
    private static final String FILE = "/proc/lk_env";
    public static final int RET_SUCCESS = 1;
    public static final int RET_FAILED = 0;
    private RadioButton mRadioBtnEnabled = null;
    private RadioButton mRadioBtnDisabled = null;
    private Spinner mSpinEnabledValues = null;
    private Button buttonSet = null;
    private Toast mToast = null;
    private RadioGroup mRadioBtn = null;
    private boolean mFirstchecked = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.sib_capture);
        mFirstchecked = true;
        mRadioBtnEnabled = (RadioButton) findViewById(R.id.sib_capture_support_enabled);
        mRadioBtnDisabled = (RadioButton) findViewById(R.id.sib_capture_support_disabled);
        mSpinEnabledValues = (Spinner) findViewById(R.id.allocate_storage_size);
        buttonSet = (Button) findViewById(R.id.sib_capture_support_set_button);
        mRadioBtn = (RadioGroup) findViewById(R.id.sib_radio_group);
        mRadioBtn.setOnCheckedChangeListener(this);
        querySIBCaptureValue();

        buttonSet.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mRadioBtnDisabled.isChecked()) {
                    if (setSIBCaptureValue(0) == RET_SUCCESS) {
                        showToast("set SIBCaptureDisabled succeed!");
                        showDialog(REBOOT);
                    } else {
                        showToast("set SIBCaptureDisabled failed!");
                    }
                    Elog.i(TAG, "SIB Capture Support Disabled button set");

                } else if (mRadioBtnEnabled.isChecked()) {
                    int value = mSpinEnabledValues.getSelectedItemPosition() + 1;
                    Elog.d(TAG, "value : " + value);
                    if (setSIBCaptureValue(value) == RET_SUCCESS) {
                        showToast("set SIBCaptureEnabled succeed!");
                        showDialog(REBOOT);
                    } else {
                        showToast("set SIBCaptureEnabled failed!");
                    }
                    Elog.i(TAG, "SIB Capture Support enabled button set");
                }
            }
        });
    }

    private void querySIBCaptureValue() {
        String[] ret = runCmdInEmSvr(
                AFMFunctionCallEx.FUNCTION_EM_SIB_CAPTURE_GET, 0);
        int value = 0;
        if (ret.length > 0) {
            try {
                value = Integer.valueOf(ret[0]);
            } catch (Exception e) {
                showToast("querySIBCaptureValue failed!");
            }
            Elog.i(TAG, "value: " + value);
            if (value >= 1 && value <= 4) {
                mRadioBtnEnabled.setChecked(true);
                mSpinEnabledValues.setSelection(value - 1);
            }
            else if (value == 0) {
                mRadioBtnDisabled.setChecked(true);
            }
            else {
                mRadioBtnDisabled.setChecked(true);
                Log.d("@M_" + TAG, "md1_phy_cap_gear not found");
            }
        }
        else {
            showToast("querySIBCaptureValue failed!");
        }

    }

    private int setSIBCaptureValue(int value) {
        String[] ret = runCmdInEmSvr(
                AFMFunctionCallEx.FUNCTION_EM_SIB_CAPTURE_SET, 1,
                value);
        if (ret.length > 0 && "SUCCESS".equals(ret[0])) {
            return RET_SUCCESS;
        }
        return RET_FAILED;
    }

    public String[] runCmdInEmSvr(int index, int paramNum, int... param) {
        ArrayList<String> arrayList = new ArrayList<String>();
        AFMFunctionCallEx functionCall = new AFMFunctionCallEx();
        boolean result = functionCall.startCallFunctionStringReturn(index);
        functionCall.writeParamNo(paramNum);
        for (int i : param) {
            functionCall.writeParamInt(i);
        }
        if (result) {
            FunctionReturn r;
            do {
                r = functionCall.getNextResult();
                if (r.mReturnString.isEmpty()) {
                    break;
                }
                arrayList.add(r.mReturnString);
            } while (r.mReturnCode == AFMFunctionCallEx.RESULT_CONTINUE);
            if (r.mReturnCode == AFMFunctionCallEx.RESULT_IO_ERR) {
                Log.d("@M_" + TAG, "AFMFunctionCallEx: RESULT_IO_ERR");
                arrayList.clear();
                arrayList.add("ERROR");
            }
        } else {
            Log.d("@M_" + TAG, "AFMFunctionCallEx return false");
            arrayList.clear();
            arrayList.add("ERROR");
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    public void sendBroadcastReboot() {
        Intent intent = new Intent(Intent.ACTION_REBOOT);
        intent.putExtra("nowait", 1);
        intent.putExtra("interval", 1);
        intent.putExtra("window", 0);
        sendBroadcast(intent);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case REBOOT:
            return new AlertDialog.Builder(this)
                    .setTitle("Hint")
                    .setMessage(
                            "If you want to enable the set,press confirm to reboot phone\n")
                    .setPositiveButton("Confirm", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == DialogInterface.BUTTON_POSITIVE) {
                                sendBroadcastReboot();
                            }
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .create();

        }
        return super.onCreateDialog(id);
    }

    private void showToast(String msg) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        mToast.show();
    }

    @Override
    public void onCheckedChanged(RadioGroup arg0, int checkedId) {
        // TODO Auto-generated method stub

        if (mFirstchecked == true) {
            mFirstchecked = false;
        }
        if (checkedId == R.id.sib_capture_support_enabled) {
            mSpinEnabledValues.setEnabled(true);
            if (mFirstchecked == false) {
                mSpinEnabledValues.setSelection(3);
            }
        } else if (checkedId == R.id.sib_capture_support_disabled) {
            mSpinEnabledValues.setEnabled(false);
        }
    }
}
