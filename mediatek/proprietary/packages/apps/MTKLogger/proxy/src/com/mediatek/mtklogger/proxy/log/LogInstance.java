package com.mediatek.mtklogger.proxy.log;

import com.mediatek.mtklogger.proxy.utils.Utils;

/**
 * Supper class for each log instance, like network log, mobile log and modem.
 * log
 */
/**
 * @author MTK81255
 *
 */
public class LogInstance implements ISocketConnectionListen {

    private static final String TAG = Utils.TAG + "/LogAbstract";

    private String mSocketName = "";
    private LogConnection mLogConnection;
    private String mCommandValue = null;
    private String mCommandResult = null;

    /**
     * @param socketName String
     */
    public LogInstance(String socketName) {
        mSocketName = socketName;
    }

    /**
     * @return boolean
     */
    public boolean connectToSocket() {
        mLogConnection = new LogConnection(mSocketName, this);
        return mLogConnection.connect();
    }

    /**
     * @param commandValue String
     * @return boolean
     */
    public boolean excuteCommand(String commandValue) {
        mCommandValue = commandValue;
        mCommandResult = null;
        if ((mLogConnection == null || mLogConnection.getSocket() == null)) {
            boolean reConnectSuccess = connectToSocket();
            if (!reConnectSuccess) {
                Utils.logw(TAG, "Send " + commandValue + " failed "
                        + "for can not connect to " + mSocketName + "!");
                return false;
            }
        }
        return mLogConnection.sendCmd(commandValue);
    }

    public boolean isCommandDone() {
        return mCommandResult != null;
    }

    public String getCommandResult() {
        return mCommandResult;
    }

    @Override
    public void socketResponse(String respStr) {
        Utils.logi(TAG, "socketResponse respStr = " + respStr);
        if (mCommandValue != null && respStr.startsWith(mCommandValue)) {
            mCommandResult = respStr;
            mCommandValue = null;
        }
    }
}
