package com.mediatek.op18.phone;

import android.content.Context;
import android.provider.Settings;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;
import com.mediatek.phone.ext.DefaultPhoneMiscExt;
import com.mediatek.provider.MtkSettingsExt;

import java.util.Iterator;


/**
 * OP18 add current network feature
 */
public class Op18PhoneMiscExt extends DefaultPhoneMiscExt {

    private static final String TAG = "Op18PhoneMiscExt";
    private Context mContext;
    private boolean is_item_added = false;
    private static TelecomManager telecomManager;


    public Op18PhoneMiscExt(Context context) {
        Log.d(TAG, "OP18PhoneMiscExt constructor");
        mContext = context;
        telecomManager = TelecomManager.from(context);
    }

    /**
     * Whether need to remove "Ask First" item from call with selection list.
     *
     * @param String[] entryValues entryValues
     * @return true if need to remove it.
     */
    @Override
    public String[] addCurrentNetworkToSelectionListIndex(String[] entryValues) {
        Log.d(TAG, "addCurrentNetworkToSelectionListIndex");
        int len = entryValues.length;
        int i;
        len = len + 1;
        if (len < 4) {
            return entryValues;
        }
        String[] entryValues_t = new String[len];
        for (i = 0; i < len - 1; i++) {
            entryValues_t[i] = entryValues[i];
        }
        is_item_added = true;
        entryValues_t[i] = Integer.toString(i);

        return entryValues_t;
    }

    /**
      * remove "Ask First" item value from call with selection list.
      *
      * @param String[] entries entries
      * @return entries after remove object.
      */
    @Override
    public CharSequence[] addCurrentNetworkToSelectionListValue(CharSequence[] entries) {
        Log.d(TAG, "addCurrentNetworkToSelectionListValue");
        int len = entries.length;
        int i;
        len = len + 1;
        if (len < 4) {
            return entries;
        }

        CharSequence[] entries_t = new CharSequence[len];
        for (i = 0; i < len - 1; i++) {
            entries_t[i] = entries[i];
        }
        entries_t[i] =  mContext.getResources().getString(R.string.current_network);
        is_item_added = true;
        return entries_t;
    }

    public boolean onPreferenceChange(int index) {
        Log.d(TAG, "onPreferenceChange, index =" + index);
        if (index == 3 && is_item_added) {
            Settings.Global.putInt(mContext.getContentResolver(),
                                        MtkSettingsExt.Global.CURRENT_NETWORK_CALL, 1);
            setMainCapabilityAccount();
            return true;
        } else {
            Settings.Global.putInt(mContext.getContentResolver(),
                                        MtkSettingsExt.Global.CURRENT_NETWORK_CALL, 0);
        }
        return false;
    }

    public int getCurrentNetworkIndex(int index) {
        Log.d(TAG, "getCurrentNetworkIndex, index =" + index);
        int result = Settings.Global.getInt(mContext.getContentResolver(),
                                            MtkSettingsExt.Global.CURRENT_NETWORK_CALL, 0);
        if (result == 1) {
            return 3;
        } else {
            return index;
        }
    }

              /**
         * Sets phone account of selected subId.
         * @param subId subId
         * @return
         */
    public void setPhoneAccount(int subId) {
        PhoneAccountHandle phoneAccountHandle = subscriptionIdToPhoneAccountHandle(subId);
        if (phoneAccountHandle != null) {
            telecomManager.setUserSelectedOutgoingPhoneAccount(phoneAccountHandle);
            Log.d(TAG, "account set:" + phoneAccountHandle);
        } else {
            Log.d(TAG, "account not set");
        }
    }

    /**
         * Sets Main capability phone account.
         * @return subId
         */
    public int setMainCapabilityAccount() {
        int masterPhoneId = RadioCapabilitySwitchUtil.getMainCapabilityPhoneId();
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(masterPhoneId);
        Log.d(TAG, "main capability account subId:" + subId);
        setPhoneAccount(subId);
        return subId;
    }

    private PhoneAccountHandle subscriptionIdToPhoneAccountHandle(int subId) {
        TelephonyManager sTelephonyManager = TelephonyManager.from(mContext);

        final Iterator<PhoneAccountHandle> phoneAccounts =
                telecomManager.getCallCapablePhoneAccounts().listIterator();

        while (phoneAccounts.hasNext()) {
            final PhoneAccountHandle phoneAccountHandle = phoneAccounts.next();
            final PhoneAccount phoneAccount = telecomManager.getPhoneAccount(phoneAccountHandle);
            if (subId == sTelephonyManager .getSubIdForPhoneAccount(phoneAccount)) {
                return phoneAccountHandle;
            }
        }
        return null;
    }

}
