#include "dfps/SwVsyncController.h"

#include <utils/String16.h>

namespace android {

#define PROTOCOL_SF_CHANGE_MODE 10003

static String16 getInterfaceName(sp<IBinder> binder)
{
    if (binder != NULL) {
        Parcel data, reply;
        status_t err = binder->transact(IBinder::INTERFACE_TRANSACTION, data, &reply);
        if (err == NO_ERROR) {
            return reply.readString16();
        }
    }
    return String16();
}

SwVsyncController::SwVsyncController()
    : mSf(NULL)
    , mIfName("")
    , mState(NAME_NOT_FOUND)
{
}

SwVsyncController::~SwVsyncController()
{
}

status_t SwVsyncController::reconnectLocked()
{
    status_t res = NO_ERROR;
    sp<IServiceManager> sm = defaultServiceManager();
    mSf = sm->checkService(String16("SurfaceFlinger"));
    if (mSf != NULL) {
        mIfName = getInterfaceName(mSf);

        class DeathNotifier : public IBinder::DeathRecipient
        {
        private:
            const wp<SwVsyncController> mSvc;
        public:
            DeathNotifier(const wp<SwVsyncController>& svc)
                : mSvc(svc)
            {
                // since DeathNotifier will not be kept by linkToDeath
                // we have to extend its lifetime manually
                extendObjectLifetime(OBJECT_LIFETIME_WEAK);
            }

            virtual void binderDied(const wp<IBinder>& /*who*/)
            {
                ALOGW("binder of SurfaceFlinger died");

                sp<SwVsyncController> svc = mSvc.promote();
                if (svc != NULL)
                    svc->updateState(STATE_SERVER_DIED);
            }
        };

        sp<IBinder::DeathRecipient> notifier = new DeathNotifier(this);
        if (notifier != NULL)
        {
            mSf->linkToDeath(notifier);
        }
    }
    else {
        ALOGE("SwVsyncController can not find SurfaceFlinger");
        res = NAME_NOT_FOUND;
    }

    return res;
}

status_t SwVsyncController::adjustVSyncMode(int32_t fps, int32_t mode)
{
    status_t res = NO_ERROR;
    switch (mode) {
        case VSYNC_MODE_DEFAULT_SW:
        case VSYNC_MODE_DIRECT_PASS_HW:
        case VSYNC_MODE_INTERNAL_SW:
            res = changeVSyncModeWithFps(fps, mode);
            break;

        default:
            ALOGW("try to use unknown SW VSync mode");
            break;
    }

    return res;
}

void SwVsyncController::updateState(int32_t flag)
{
    Mutex::Autolock l(mStateLock);
    if (flag & STATE_SERVER_DIED) {
        mState = DEAD_OBJECT;
        mSf = NULL;
    }
}

status_t SwVsyncController::changeVSyncModeWithFps(int32_t fps, int32_t mode)
{
    Mutex::Autolock l(mStateLock);
    if (mState != NO_ERROR) {
        mState = reconnectLocked();
    }

    status_t res = mState;
    if (res == NO_ERROR) {
        Parcel data, reply;
        data.writeInterfaceToken(mIfName);
        data.writeInt32(mode);
        data.writeInt32(fps);
        res = mSf->transact(PROTOCOL_SF_CHANGE_MODE, data, &reply);
        if (res != NO_ERROR) {
            ALOGW("failed to change SW Vsync mode(%d) with fps(%d): %d", mode, fps, res);
        }
    }

    return res;
}

};
