// ICallback.aidl
package com.mediatek.duraspeed.presenter;

// Declare any non-default types here with import statements

interface ICallback {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     *
     */
    void onStateChanged(int state);
}
