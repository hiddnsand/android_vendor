package com.mediatek.op07.settings;

import android.content.Context;

import com.mediatek.settings.ext.ISettingsMiscExt;
import com.mediatek.settings.ext.IWfcSettingsExt;
import com.mediatek.settings.ext.IDataUsageSummaryExt;
import com.mediatek.settings.ext.OpSettingsCustomizationFactoryBase;

public class OP07SettingsCustomizationFactory extends OpSettingsCustomizationFactoryBase {
    private Context mContext;

    public OP07SettingsCustomizationFactory(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public IWfcSettingsExt makeWfcSettingsExt() {
        return new Op07WfcSettingsExt(mContext);
    }

    /**
     * Api to make dataUsage setting plugin .
     * @return IDataUsageSummaryExt
     */
    @Override
    public IDataUsageSummaryExt makeDataUsageSummaryExt() {
        return new OP07DataUsageSummaryExt(mContext);
    }

    @Override
    public ISettingsMiscExt makeSettingsMiscExt(Context context) {
        return new Op07SettingsMiscExt(mContext);
    }
}
