package com.mediatek.providers.contacts;

import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import com.mediatek.provider.MtkContactsContract.Aas;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

import com.mediatek.internal.telephony.IMtkTelephonyEx;
import com.mediatek.internal.telephony.phb.IMtkIccPhoneBook;

/**
 * Add this class for SIM support.
 */
public class SimCardUtils {

    public static final String TAG = "ProviderSimCardUtils";
    private static final String ACCOUNT_TYPE_POSTFIX = " Account";
    public static TelephonyManager sTelephonyManager;

    /**
     * M: Structure function.
     * @param context context
     */
    public SimCardUtils(Context context) {
        sTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    }

    /**
     * M: add for mark SIM type.
     */
    public interface SimType {
        String SIM_TYPE_SIM_TAG = "SIM";
        int SIM_TYPE_SIM = 0;

        String SIM_TYPE_USIM_TAG = "USIM";
        int SIM_TYPE_USIM = 1;

        // UIM
        int SIM_TYPE_UIM = 2;
        int SIM_TYPE_CSIM = 3;
        String SIM_TYPE_UIM_TAG = "RUIM";
        // UIM
        // UICC TYPE
        String SIM_TYPE_CSIM_TAG = "CSIM";
        // UICC TYPE
    }

    /**
     * M: [Gemini+] all possible icc card type are put in this array. it's a map
     * of SIM_TYPE => SIM_TYPE_TAG like SIM_TYPE_SIM => "SIM"
     */
    private static final SparseArray<String> SIM_TYPE_ARRAY = new SparseArray<String>();
    static {
        SIM_TYPE_ARRAY.put(SimType.SIM_TYPE_SIM, SimType.SIM_TYPE_SIM_TAG);
        SIM_TYPE_ARRAY.put(SimType.SIM_TYPE_USIM, SimType.SIM_TYPE_USIM_TAG);
        SIM_TYPE_ARRAY.put(SimType.SIM_TYPE_UIM, SimType.SIM_TYPE_UIM_TAG);
        SIM_TYPE_ARRAY.put(SimType.SIM_TYPE_CSIM, SimType.SIM_TYPE_CSIM_TAG);
    }

    /**
     * M: [Gemini+] get the readable sim account type, like "SIM Account".
     *
     * @param simType
     * the integer sim type
     * @return the string like "SIM Account"
     */
    public static String getSimAccountType(int simType) {
        return SIM_TYPE_ARRAY.get(simType) + ACCOUNT_TYPE_POSTFIX;
    }

    /// M: Add for AAS @{
    private static final String MTK_PHONE_BOOK_SERVICE_NAME = "mtksimphonebook";

    private static IMtkIccPhoneBook getIMtkIccPhoneBook() {
        LogUtils.d(TAG, "[getIMtkIccPhoneBook]");
        String serviceName = MTK_PHONE_BOOK_SERVICE_NAME;
        final IMtkIccPhoneBook iIccPhb = IMtkIccPhoneBook.Stub.asInterface(ServiceManager
                .getService(serviceName));
        return iIccPhb;
    }

    /**
     * The function to get AAS by sub id and the index in SIM.
     */
    public static String getAASLabel(String indicator) {
        final String DECODE_SYMBOL = Aas.ENCODE_SYMBOL;
        if (!indicator.contains(DECODE_SYMBOL) || indicator.indexOf(DECODE_SYMBOL) == 0
                || indicator.indexOf(DECODE_SYMBOL) == (indicator.length() - 1)) {
            LogUtils.w(TAG, "[getAASLabel] return;");
            return "";
        }
        String aas = "";
        String keys[] = indicator.split(DECODE_SYMBOL);
        int subId = Integer.valueOf(keys[0]);
        int index = Integer.valueOf(keys[1]);
        LogUtils.d(TAG, "[getAASLabel] subId: " + subId + ",index: " + index);
        if (subId > 0 && index > 0) {//if have the aas label.the index value must > 0.
            try {
                final IMtkIccPhoneBook iIccPhb = getIMtkIccPhoneBook();
                if (iIccPhb != null) {
                    aas = iIccPhb.getUsimAasById(subId, index);
                }
            } catch (RemoteException e) {
                LogUtils.e(TAG, "[getAASLabel] catched exception.");
            }
        }
        if (aas == null) {
            aas = "";
        }
        LogUtils.d(TAG, "[getAASLabel] aas=" + aas);
        return aas;
    }

    /// @}
}

