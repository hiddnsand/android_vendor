package com.mediatek.presence.core.ims.rcsua;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import com.mediatek.presence.utils.logger.Logger;
import com.mediatek.presence.core.ims.rcsua.RcsUaAdapter.RcsUaEvent;

/**
 * The Class RcsProxyRegistrationHandler.
 */
public class RcsProxyRegistrationHandler implements RcsUaEventDispatcher.RCSEventDispatcher {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    private static final int REGISTRATION_TIMER = 1000;
    private static final int MAX_REGISTRATION_TIMER = 2000;

    private static Context mContext;
    private static final String TAG = "PresenceProxyRegistrationHandler";
    private static RcsUaAdapter mRcsuaAdapt = null;
    private static boolean mIsRegistrationSuccessful = false;
    private static boolean mIsDeRegistrationRequestSent = false;
    /**
     * Wait  answer for register/deregister
     */
    private static Object mWaitRegisterResponse = new Object();
    private static Object mWaitDeRegisterResponse = new Object();

    /**
     * Instantiates a new rcs proxy registration handler.
     *
     * @param context the context
     */
    public RcsProxyRegistrationHandler(Context context) {
        if (mRcsuaAdapt == null) {
            mContext = context;
            mRcsuaAdapt = RcsUaAdapter.getInstance();

            IntentFilter filter = new IntentFilter();
            filter.addAction(RcsUaAdapter.ACTION_IMS_DEREG_UNPUBLISH_DONE);
            mContext.registerReceiver(mBroadcastReceiver, filter);
        }
    }

    /**
     * Register.
     *
     * @return true, if successful
     */
    public boolean register() {
        try {
            synchronized (mWaitRegisterResponse) {
                mRcsuaAdapt.setRegisteringState(true);
                logger.debug( TAG+"register : send request");

                //send register request to UAAdapter
                /*
                String rcsCapabilityFeatureTags = RcsUaAdapter.getInstance().getRCSFeatureTag() + "\0";
                RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsuaAdapt.RCS_PROXY_EVENT_RCS_REGISTER);
                event.putString(rcsCapabilityFeatureTags, rcsCapabilityFeatureTags.length());
                mRcsuaAdapt.writeEvent(event);
                */
                logger.debug(TAG+" RCS_UA REGISTERED SUCCESS.");
                mIsRegistrationSuccessful = true;
                mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
                mWaitRegisterResponse.wait(5);
                //wait for 20 seconds to allow status to get updated
                /*
                int i = 0;
                while(mRcsuaAdapt.isRegistering()){
                	i= i+ REGISTRATION_TIMER;

                    if(i>MAX_REGISTRATION_TIMER)
                    	break;

                    mWaitRegisterResponse.wait(REGISTRATION_TIMER);
                }
                */
                //logger.debug(TAG+"  after register request response ; registration status : "+ mIsRegistrationSuccessful+i);
                logger.debug(TAG+"  after register request response ; registration status : "+ mIsRegistrationSuccessful);
            }
        } catch (InterruptedException e) {
            return false;
        }finally{
            mRcsuaAdapt.setRegisteringState(false);
        }
        return mIsRegistrationSuccessful;
    }



    /**
     * Unregister.
     *
     * @return true, if successful
     */
    public boolean deRegister() {
        try {
            synchronized (mWaitDeRegisterResponse) {
                mIsDeRegistrationRequestSent = true;
                logger.debug( "deregister : send request");
                //send register request
                String rcsCapabilityFeatureTags = RcsUaAdapter.getInstance().getRCSFeatureTag()+"\0";
                RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsuaAdapt.RCS_PROXY_EVENT_RCS_DEREGISTER);
                event.putString(rcsCapabilityFeatureTags, rcsCapabilityFeatureTags.length());
                mRcsuaAdapt.writeEvent(event);

                //wait for 3 sec
                mWaitDeRegisterResponse.wait(REGISTRATION_TIMER);
                logger.debug( TAG+"after deregister request response ; deregistration status : "+ mIsDeRegistrationRequestSent);
            }
        } catch (InterruptedException e) {
            mIsRegistrationSuccessful = false;
        }
        return mIsRegistrationSuccessful;
    }

    /**
     * Event callback.
     *
     * @param event the event
     * runs in RCSUA event dispatcher
     */
    public void EventCallback(RcsUaEvent event) {

        try {
            int requestId = event.getRequestID();;
            String state = event.getString(event.getDataLen());

            switch (requestId) {
            case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_REGISTERING:
                logger.debug(TAG+"registering state : " +state);
                 if(state.equals("1")){
                    logger.debug("RCS_UA registering currently. wait for REG Success update");
                 }else{
                    logger.debug("RCS_UA couldn't add capab successfully. stop registering process");
                    synchronized (mWaitRegisterResponse) {
                    //failure set registration as failed.
                        mIsRegistrationSuccessful = false;
                        logger.debug(TAG+"setRegistrationStatus : "+ mIsRegistrationSuccessful);
                        mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
                        mWaitRegisterResponse.notify();
                    }
                 }
                break;

            case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_REGISTER:
                logger.debug(TAG+"RCS_PROXY_EVENT_RSP_RCS_REGISTER , state: "+state);
                //NOTIFY LISTENING MODULE read the content and based on that set the value
                synchronized (mWaitRegisterResponse) {
                    if (state.equals("1")) {
                        mIsRegistrationSuccessful = true;
                    }
                    else {
                        mIsRegistrationSuccessful = false;
                    }
                    //set the registration status and notify
                    mRcsuaAdapt.setRegisteringState(false);
                    logger.debug(TAG+"setRegistrationStatus : "+ mIsRegistrationSuccessful);
                    mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
                    mWaitRegisterResponse.notify();
                }
                break;

            case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_DEREGISTER:
                logger.debug( TAG+"Deregistered successfully");
                //read the content and based on that set the value
                synchronized (mWaitDeRegisterResponse) {
                    //set the registration status and notify
                    mIsRegistrationSuccessful = false;
                    logger.debug(TAG+"setRegistrationStatus : "+ mIsRegistrationSuccessful);
                    mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
                    mWaitDeRegisterResponse.notify();
                }

                //set the registration status
                mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
                break;

            case RcsUaAdapter.RCS_PROXY_EVENT_IND_IMS_DEREG_START:
                int deregId = event.getInt();
                logger.debug(TAG + "Handle IMS_DEREG_START event, deregId: " + deregId);

                Intent intent = new Intent(RcsUaAdapter.ACTION_IMS_DEREG_START);
                intent.putExtra(RcsUaAdapter.EXTRA_DEREG_ID, deregId);
                mContext.sendBroadcast(intent);
                break;

            default:
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Enable request.
     */
    public void enableRequest() {
    }

    /**
     * Disable request.
     */
    public void disableRequest() {
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equalsIgnoreCase(RcsUaAdapter.ACTION_IMS_DEREG_UNPUBLISH_DONE)) {
                int deregId = intent.getIntExtra(RcsUaAdapter.EXTRA_DEREG_ID, 1);
                logger.debug(TAG + "Receive IMS_DEREG_UNPUBLISH_DONE intent, deregId: " + deregId);

                RcsUaEvent event = new RcsUaEvent(RcsUaAdapter.RCS_PROXY_EVENT_CNF_PRESENCE_UNPUBLISH);
                event.putInt(deregId);
                mRcsuaAdapt.writeEvent(event);
            }
        }
    };
}
