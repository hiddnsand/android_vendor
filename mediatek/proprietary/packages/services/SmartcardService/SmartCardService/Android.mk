
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

ifeq ($(findstring x6.,x$(PLATFORM_VERSION)), x6.)
$(shell sed -i 's/protectionLevel="dangerous"/protectionLevel="normal"/g' $(LOCAL_PATH)/AndroidManifest.xml)
else ifeq ($(findstring x5.,x$(PLATFORM_VERSION)), x5.)
$(shell sed -i 's/protectionLevel="normal"/protectionLevel="dangerous"/g' $(LOCAL_PATH)/AndroidManifest.xml)
else ifeq ($(findstring x4.,x$(PLATFORM_VERSION)), x4.)
$(shell sed -i 's/protectionLevel="normal"/protectionLevel="dangerous"/g' $(LOCAL_PATH)/AndroidManifest.xml)
endif

LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_SRC_FILES += $(call all-java-files-under, common/src)
LOCAL_SRC_FILES += $(call all-Iaidl-files-under, common/src)

LOCAL_AIDL_INCLUDES := $(LOCAL_PATH)/common/src/

LOCAL_PACKAGE_NAME := SmartcardService
LOCAL_CERTIFICATE := platform
LOCAL_DEX_PREOPT := false

#PROGUARD start#
LOCAL_PROGUARD_ENABLED := obfuscation
LOCAL_PROGUARD_FLAG_FILES := proguard.flags
#PROGUARD end#

include $(BUILD_PACKAGE)

include $(call all-makefiles-under,$(LOCAL_PATH))

