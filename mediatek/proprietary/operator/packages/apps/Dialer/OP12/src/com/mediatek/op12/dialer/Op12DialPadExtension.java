package com.mediatek.op12.dialer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.view.View;

import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.mediatek.dialer.ext.DefaultDialPadExtension;


public class Op12DialPadExtension extends DefaultDialPadExtension {
    private static final String TAG = "Op12DialPadExtension";
    private Context mContext;
    private View mVideoView = null;
    private boolean mLVCEnabled;
    private boolean mIsVolteProvisioned;
    private IntentFilter mIntentFilter;
    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "onReceive()... " + action);
            if (action.equals("com.android.intent.action.IMS_CONFIG_CHANGED")) {
                 try {
                     ImsConfig imsConfig = ImsManager.getInstance(mContext, SubscriptionManager
                                  .getDefaultVoicePhoneId()).getConfigInterface();
                     mIsVolteProvisioned = ImsManager.isVolteProvisionedOnDevice(mContext);
                     mLVCEnabled = (1 == imsConfig.getProvisionedValue(
                                  ImsConfig.ConfigConstants.LVC_SETTING_ENABLED));
                     Log.d(TAG, "VolteProvisioned:" + mIsVolteProvisioned + "LVC Enabled: "
                                + mLVCEnabled);
                 } catch (ImsException e) {
                     Log.e(TAG, "Advanced settings not updated, ImsConfig null");
                     e.printStackTrace();
                 }
                if (mVideoView != null) {
                    mVideoView.setVisibility(mIsVolteProvisioned && mLVCEnabled ?
                                         View.VISIBLE : View.GONE);
                }
            } else if (action.equals("com.mediatek.intent.action.VT_CHANGED")) {
                boolean vtOption = intent.getBooleanExtra("VT", false);
                boolean advancedCalling = ImsManager
                             .isEnhanced4gLteModeSettingEnabledByUser(mContext);
                Log.d(TAG, " advancedCalling:" + advancedCalling + " VT:" + vtOption);
                if (mIsVolteProvisioned && mLVCEnabled && mVideoView != null) {
                    mVideoView.setAlpha(vtOption && advancedCalling ? 1.0f : 0.3f);
                }
            }
        }
    };

    public Op12DialPadExtension(Context context) {
        mContext = context;
        try {
            ImsConfig imsConfig = ImsManager.getInstance(mContext, SubscriptionManager
                       .getDefaultVoicePhoneId()).getConfigInterface();
            mIsVolteProvisioned = ImsManager.isVolteProvisionedOnDevice(mContext);
            mLVCEnabled = (1 == imsConfig.getProvisionedValue(
                         ImsConfig.ConfigConstants.LVC_SETTING_ENABLED));
        } catch (ImsException e) {
            Log.e(TAG, "Advanced settings not updated, ImsConfig null");
            e.printStackTrace();
        }
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("com.mediatek.intent.action.VT_CHANGED");
        mIntentFilter.addAction("com.android.intent.action.IMS_CONFIG_CHANGED");
        mContext.registerReceiver(mIntentReceiver, mIntentFilter);
    }

    @Override
    public void customizeContactListItemView(View view) {
        Log.d(TAG, "customizeContactListItemView");
        mVideoView = view;
        boolean advancedCalling = ImsManager.isEnhanced4gLteModeSettingEnabledByUser(mContext);
        Log.d(TAG, "customizeContactListItemView: mLVCEnabled: " + mLVCEnabled +
                " advancedCalling:" + advancedCalling);
        view.setVisibility(mIsVolteProvisioned && mLVCEnabled ? View.VISIBLE : View.GONE);
        if (mIsVolteProvisioned && mLVCEnabled) {
            Log.d(TAG, "VolteProvisioned/LVC Enabled, so unhide video button in dialer");
            if (ImsManager.isVtEnabledByUser(mContext) && advancedCalling) {
                Log.d(TAG, "Enable video button in dialer");
                view.setAlpha(1.0f);
            } else {
                Log.d(TAG, "Grey out video button in dialer");
                    view.setAlpha(0.3f);
                }
        } else {
            Log.d(TAG, "VolteDeprovisioned/LVC Disabled, so hide video button in dialer");
        }
    }

    @Override
    public boolean enableVideoButton() {
        Log.d(TAG, "enableVideoButton");

        boolean enablePlatform = ImsManager.isVtEnabledByPlatform(mContext);
        Log.d(TAG, "enablePlatform = " + enablePlatform);
        if (enablePlatform) {
            return true;
        }
        return false;
    }
}
