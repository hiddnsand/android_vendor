/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncResult;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.Rlog;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.android.internal.telephony.CarrierAppUtils;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.IntentBroadcaster;
import com.android.internal.telephony.MccTable;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.RILConstants;
import com.android.internal.telephony.SubscriptionController;
import com.android.internal.telephony.SubscriptionInfoUpdater;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.uicc.IccCardProxy;
import com.android.internal.telephony.uicc.IccConstants;
import com.android.internal.telephony.uicc.IccFileHandler;
import com.android.internal.telephony.uicc.IccRecords;
import com.android.internal.telephony.uicc.IccUtils;
import com.android.internal.telephony.uicc.SpnOverride;

import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.internal.telephony.uicc.MtkIccCardProxy;
import com.mediatek.internal.telephony.uicc.MtkSpnOverride;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.List;


/**
 *@hide
 */
public class MtkSubscriptionInfoUpdater extends SubscriptionInfoUpdater {
    private static final boolean DBG = true;
    private static final String LOG_TAG = "MtkSubscriptionInfoUpdater";

    private static final int EVENT_SIM_READY = 100;
    private static final int EVENT_RADIO_AVAILABLE = 101;
    private static final int EVENT_RADIO_UNAVAILABLE = 102;
    // For the feature SIM Hot Swap with Common Slot
    private static final int EVENT_SIM_NO_CHANGED = 103;
    private static final int EVENT_TRAY_PLUG_IN = 104;
    private static final int EVENT_SIM_PLUG_OUT = 105;

    private static final String ICCID_STRING_FOR_NO_SIM = "N/A";

    private final ExecutorService mDefaultExecutor = Executors.newSingleThreadExecutor();
    private final Object mLock = new Object();

    private CommandsInterface[] mCis = null;

    private int[] mIsUpdateAvailable = new int[PROJECT_SIM_NUM];
    private int mReadIccIdCount = 0;

    // For the feature SIM Hot Swap with Common Slot
    private static final String COMMON_SLOT_PROPERTY = "ro.mtk_sim_hot_swap_common_slot";
    private boolean mCommonSlotResetDone = false;

    private static final boolean MTK_FLIGHTMODE_POWEROFF_MD_SUPPORT
            = "1".equals(SystemProperties.get("ro.mtk_flight_mode_power_off_md"));
    private static final int sReadICCID_retry_time = 1000;
    private static final String[] PROPERTY_ICCID_SIM = {
        "ril.iccid.sim1",
        "ril.iccid.sim2",
        "ril.iccid.sim3",
        "ril.iccid.sim4",
    };

    // A big rule, please add you code in the rear of API and file.
    public MtkSubscriptionInfoUpdater(Looper looper, Context context, Phone[] phone,
            CommandsInterface[] ci) {
        super(looper, context, phone, ci);
        logd("MtkSubscriptionInfoUpdater created");

        // unregister AOSP receiver
        mContext.unregisterReceiver(sReceiver);

        mCis = ci;
        for (int i = 0; i < PROJECT_SIM_NUM; i++) {
            mIsUpdateAvailable[i] = 0;
            mIccId[i] = SystemProperties.get(PROPERTY_ICCID_SIM[i], "");
            if (mIccId[i].length() == 3) {
                logd("No SIM insert :" + i);
            }
            logd("mIccId[" + i + "]:" + SubscriptionInfo.givePrintableIccid(mIccId[i]));
        }

        if (isAllIccIdQueryDone()) {
            mDefaultExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    updateSubscriptionInfoByIccId();
                }
            });
        }

        // register MTK self receiver for funtion extension
        IntentFilter intentFilter = new IntentFilter(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        intentFilter.addAction(IccCardProxy.ACTION_INTERNAL_SIM_STATE_CHANGED);
        intentFilter.addAction(MtkTelephonyIntents.ACTION_COMMON_SLOT_NO_CHANGED);
        if ("OP09".equals(SystemProperties.get("persist.operator.optr"))
                && ("SEGDEFAULT".equals(SystemProperties.get("persist.operator.seg"))
                    || "SEGC".equals(SystemProperties.get("persist.operator.seg")))) {
            intentFilter.addAction(Intent.ACTION_LOCALE_CHANGED);
        }
        mContext.registerReceiver(mMtkReceiver, intentFilter);

        for (int i = 0; i < mCis.length; i++) {
            Integer index = new Integer(i);
            mCis[i].registerForNotAvailable(this, EVENT_RADIO_UNAVAILABLE, index);
            mCis[i].registerForAvailable(this, EVENT_RADIO_AVAILABLE, index);
            if (SystemProperties.get(COMMON_SLOT_PROPERTY).equals("1")) {
                ((MtkRIL)mCis[i]).registerForSimTrayPlugIn(this, EVENT_TRAY_PLUG_IN, index);
                ((MtkRIL)mCis[i]).registerForSimPlugOut(this, EVENT_SIM_PLUG_OUT, index);
            }
        }
    }

    @Override
    protected boolean isAllIccIdQueryDone() {
        for (int i = 0; i < PROJECT_SIM_NUM; i++) {
            // MTK-START
            if (mIccId[i] == null || mIccId[i].equals("")) {
            // MTK-END
                logd("Wait for SIM" + (i + 1) + " IccId");
                return false;
            }
        }
        logd("All IccIds query complete");

        return true;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case EVENT_SIM_LOCKED_QUERY_ICCID_DONE: {
                AsyncResult ar = (AsyncResult)msg.obj;
                QueryIccIdUserObj uObj = (QueryIccIdUserObj) ar.userObj;
                int slotId = uObj.slotId;
                logd("handleMessage : <EVENT_SIM_LOCKED_QUERY_ICCID_DONE> SIM" + (slotId + 1));
                if (ar.exception == null) {
                    if (ar.result != null) {
                        byte[] data = (byte[])ar.result;
                        // MTK-START
                        // mIccId[slotId] = IccUtils.bcdToString(data, 0, data.length);
                        mIccId[slotId] = IccUtils.bchToString(data, 0, data.length);
                        // MTK-END
                    } else {
                        logd("Null ar");
                        mIccId[slotId] = ICCID_STRING_FOR_NO_SIM;
                    }
                } else {
                    // MTK-START
                    if (ar.exception instanceof CommandException &&
                        ((CommandException) (ar.exception)).getCommandError() ==
                                CommandException.Error.RADIO_NOT_AVAILABLE) {
                        mIccId[slotId] = "";
                    } else {
                    // MTK-END
                        mIccId[slotId] = ICCID_STRING_FOR_NO_SIM;
                    // MTK-START
                    }
                    // MTK-END
                    logd("Query IccId fail: " + ar.exception);
                }
                logd("sIccId[" + slotId + "] = " + mIccId[slotId]);
                if (isAllIccIdQueryDone()) {
                    updateSubscriptionInfoByIccId();
                }
                broadcastSimStateChanged(slotId, IccCardConstants.INTENT_VALUE_ICC_LOCKED,
                                         uObj.reason);
                if (!ICCID_STRING_FOR_NO_SIM.equals(mIccId[slotId])) {
                    updateCarrierServices(slotId, IccCardConstants.INTENT_VALUE_ICC_LOCKED);
                }

                // MTK-START
                mDefaultExecutor.execute(new SubscriptionUpdatorThread(
                        new QueryIccIdUserObj(uObj.reason, slotId),
                        SubscriptionUpdatorThread.SIM_LOCKED));
                // MTK-END
                break;
            }

            case EVENT_SIM_LOADED: {
                // MTK-START
                //handleSimLoaded(msg.arg1);

                // Execute updateSubscriptionInfoByIccId by another thread might cause
                // broadcast intent sent before update done.
                // Need to make updateSubscriptionInfoByIccId and send broadcast as a wrapper
                // with the same thread to avoid broadcasting before update done.
                mDefaultExecutor.execute(new SubscriptionUpdatorThread(
                        new QueryIccIdUserObj(null, msg.arg1),
                        SubscriptionUpdatorThread.SIM_LOADED));
                // MTK-END
                break;
            }

            case EVENT_SIM_ABSENT: {
                // MTK-START
                // handleSimAbsent(msg.arg1);
                mDefaultExecutor.execute(new SubscriptionUpdatorThread(
                        new QueryIccIdUserObj(null, msg.arg1),
                        SubscriptionUpdatorThread.SIM_ABSENT));
                // MTK-END
                break;
            }

            // MTK-START
            case EVENT_SIM_READY: {
                mDefaultExecutor.execute(new SubscriptionUpdatorThread(
                        new QueryIccIdUserObj(null, msg.arg1),
                        SubscriptionUpdatorThread.SIM_READY));
                break;
            }

            case EVENT_RADIO_UNAVAILABLE:
                Integer index = getCiIndex(msg);
                logd("handleMessage : <EVENT_RADIO_UNAVAILABLE> SIM" + (index + 1));
                mIsUpdateAvailable[index] = 0;
                if (SystemProperties.get(COMMON_SLOT_PROPERTY).equals("1")) {
                    logd("[Common slot] reset mCommonSlotResetDone in EVENT_RADIO_UNAVAILABLE");
                    mCommonSlotResetDone = false;
                }
                break;

            case EVENT_RADIO_AVAILABLE:
                index = getCiIndex(msg);
                logd("handleMessage : <EVENT_RADIO_AVAILABLE> SIM" + (index + 1));
                mIsUpdateAvailable[index] = 1;

                if (checkIsAvailable()) {
                    mReadIccIdCount = 0;
                    if (!checkAllIccIdReady()) {
                        postDelayed(mReadIccIdPropertyRunnable, sReadICCID_retry_time);
                    } else {
                        updateSubscriptionInfoIfNeed();
                    }
                }
                break;

            case EVENT_SIM_NO_CHANGED: {
                mDefaultExecutor.execute(new SubscriptionUpdatorThread(
                                new QueryIccIdUserObj(null, msg.arg1),
                                SubscriptionUpdatorThread.SIM_NO_CHANGED));
                break;
            }

            case EVENT_TRAY_PLUG_IN: {
                logd("[Common Slot] handle EVENT_TRAY_PLUG_IN " + mCommonSlotResetDone);
                if (!mCommonSlotResetDone) {
                    mCommonSlotResetDone = true;
                    for (int i = 0; i < PROJECT_SIM_NUM; i++) {
                        String vsimEnabled = TelephonyManager.getDefault().getTelephonyProperty(
                                i, MtkTelephonyProperties.PROPERTY_EXTERNAL_SIM_ENABLED, "0");

                        vsimEnabled = ((vsimEnabled.length() == 0) ? "0" : vsimEnabled);
                        logd("vsimEnabled[" + i + "]: (" + vsimEnabled + ")");

                        try {
                            if ("0".equals(vsimEnabled)) {
                                logd("[Common Slot] reset mIccId[" + i + "] to empty.");
                                mIccId[i] = "";
                            }
                        } catch (NumberFormatException e) {
                            logd("[Common Slot] NumberFormatException, reset mIccId[" + i +
                                    "] to empty.");
                            mIccId[i] = "";
                        }
                    }
                }
                break;
            }
            case EVENT_SIM_PLUG_OUT: {
                logd("[Common Slot] handle EVENT_SIM_PLUG_OUT " + mCommonSlotResetDone);
                if (SystemProperties.get(COMMON_SLOT_PROPERTY).equals("1")) {
                    mReadIccIdCount = 0;
                    postDelayed(mReadIccIdPropertyRunnable, sReadICCID_retry_time);
                }
                mCommonSlotResetDone = false;
                break;
            }

            default:
                super.handleMessage(msg);
            // MTK-END
        }
    }

    @Override
    protected void handleSimLocked(int slotId, String reason) {
        // MTK-START
        // [ALPS01981366] Since MTK add new thread for updateSubscriptionInfoByIccId,
        // it might cause NullPointerException if we set mIccId to null without synchronized block.
        synchronized (mLock) {
        // MTK-END
            if (mIccId[slotId] != null && mIccId[slotId].equals(ICCID_STRING_FOR_NO_SIM)) {
                logd("SIM" + (slotId + 1) + " hot plug in");
                mIccId[slotId] = null;
            }


            IccFileHandler fileHandler = mPhone[slotId].getIccCard() == null ? null :
                    mPhone[slotId].getIccCard().getIccFileHandler();

            if (fileHandler != null) {
                String iccId = mIccId[slotId];
                // MTK-START
                if (iccId == null || iccId.equals("")) {
                    // [ALPS02006863]
                    // 1.Execute updateSubscriptionInfoByIccId by another thread might cause
                    //   broadcast intent sent before update done.
                    //   Need to make updateSubscriptionInfoByIccId and send broadcast as a wrapper
                    //   with the same thread to avoid broadcasting before update done.
                    // 2.Use Icc id system property istead SIM IO to query to enhance
                    //   update database performance.
                    mIccId[slotId] = SystemProperties.get(PROPERTY_ICCID_SIM[slotId], "");

                    if (mIccId[slotId] != null && !mIccId[slotId].equals("")) {
                        logd("Use Icc ID system property for performance enhancement");
                        mDefaultExecutor.execute(new SubscriptionUpdatorThread(
                                new QueryIccIdUserObj(reason, slotId),
                                SubscriptionUpdatorThread.SIM_LOCKED));
                    } else {
                // MTK-END
                        logd("Querying IccId");
                        fileHandler.loadEFTransparent(IccConstants.EF_ICCID,
                                obtainMessage(EVENT_SIM_LOCKED_QUERY_ICCID_DONE,
                                        new QueryIccIdUserObj(reason, slotId)));
                // MTK-START
                    }
                // MTK-END

                } else {
                    logd("NOT Querying IccId its already set sIccid[" + slotId + "]=" +
                            SubscriptionInfo.givePrintableIccid(iccId));
                    // MTK-START
                    String tempIccid = SystemProperties.get(PROPERTY_ICCID_SIM[slotId], "");
                    logd("tempIccid:" + SubscriptionInfo.givePrintableIccid(tempIccid) +
                            ", mIccId[slotId]:" +
                            SubscriptionInfo.givePrintableIccid(mIccId[slotId]));
                    if (MTK_FLIGHTMODE_POWEROFF_MD_SUPPORT
                            && !checkAllIccIdReady() && (!tempIccid.equals(mIccId[slotId]))) {
                        logd("All iccids are not ready and iccid changed");
                        mIccId[slotId] = null;
                        mSubscriptionManager.clearSubscriptionInfo();
                    }
                    // MTK-END
                    updateCarrierServices(slotId, IccCardConstants.INTENT_VALUE_ICC_LOCKED);
                    broadcastSimStateChanged(slotId, IccCardConstants.INTENT_VALUE_ICC_LOCKED,
                            reason);
                }
            } else {
                logd("sFh[" + slotId + "] is null, ignore");
                // In the case, SIM card may be removed.
            }
        // MTK-START
        }
        // MTK-END
    }

    @Override
    protected void handleSimLoaded(int slotId) {
        logd("handleSimLoaded: slotId: " + slotId);
        // MTK-START
        boolean needUpdate = false;
        // MTK-END

        // The SIM should be loaded at this state, but it is possible in cases such as SIM being
        // removed or a refresh RESET that the IccRecords could be null. The right behavior is to
        // not broadcast the SIM loaded.
        IccRecords records = mPhone[slotId].getIccCard().getIccRecords();
        if (records == null) {  // Possibly a race condition.
            logd("handleSimLoaded: IccRecords null");
            return;
        }
        // MTK-START
        if (records.getFullIccId() == null) {
        // MTK-END
            logd("handleSimLoaded: IccID null");
            return;
        }

        // MTK-START
        //mIccId[slotId] = records.getIccId();
        String iccId = SystemProperties.get(PROPERTY_ICCID_SIM[slotId], "");
        if (!iccId.equals(mIccId[slotId])) {
            logd("NeedUpdate");
            needUpdate = true;
            mIccId[slotId] = iccId;
        }

        if (isAllIccIdQueryDone()) {
            if (needUpdate) {
                updateSubscriptionInfoByIccId();
            }
            // MTK-END
            int[] subIds = mSubscriptionManager.getActiveSubscriptionIdList();
            for (int subId : subIds) {
                // MTK-START
                // Shoudn't use getDefault, it will suffer permission issue and can't get
                // operator and line1 number correctly.
                //TelephonyManager tm = TelephonyManager.getDefault();
                TelephonyManager tm = TelephonyManager.from(mContext);
                // MTK-END

                String operator = tm.getSimOperatorNumeric(subId);
                // MTK-START
                // AOSP issue, example: slot1 loaded, slot2 locked but iccid is ok that means
                // has add into db. So slot1 load will cause the slot2 runing the code in for loop
                // and broadcast slot2 loaded at last of for loop, but it is loced yet.
                int tempSlotId = SubscriptionManager.INVALID_PHONE_INDEX;
                tempSlotId = MtkSubscriptionController.getMtkInstance().getPhoneId(subId);
                if (tempSlotId != slotId) {
                    continue;
                }
                // MTK-END

                if (!TextUtils.isEmpty(operator)) {
                    if (subId == MtkSubscriptionController.getMtkInstance().getDefaultSubId()) {
                        MccTable.updateMccMncConfiguration(mContext, operator, false);
                    }
                    MtkSubscriptionController.getMtkInstance().setMccMnc(operator, subId);
                } else {
                    logd("EVENT_RECORDS_LOADED Operator name is null");
                }

                String msisdn = tm.getLine1Number(subId);
                ContentResolver contentResolver = mContext.getContentResolver();

                if (msisdn != null) {
                    // MTK-START
                    //ContentValues number = new ContentValues(1);
                    //number.put(SubscriptionManager.NUMBER, msisdn);
                    //contentResolver.update(SubscriptionManager.CONTENT_URI, number,
                    //        SubscriptionManager.UNIQUE_KEY_SUBSCRIPTION_ID + "="
                    //        + Long.toString(subId), null);
                    MtkSubscriptionController.getMtkInstance().setDisplayNumber(msisdn, subId);
                    // MTK-END

                    // refresh Cached Active Subscription Info List
                    SubscriptionController.getInstance()
                            .refreshCachedActiveSubscriptionInfoList();
                }

                SubscriptionInfo subInfo = mSubscriptionManager.getActiveSubscriptionInfo(subId);
                String nameToSet;
                String simCarrierName = tm.getSimOperatorName(subId);
                ContentValues name = new ContentValues(1);

                if (subInfo != null && subInfo.getNameSource() !=
                        SubscriptionManager.NAME_SOURCE_USER_INPUT) {
                // MTK-START
                // Take MVNO into account.
                String simNumeric = tm.getSimOperatorNumeric(subId);
                String simMvnoName = MtkSpnOverride.getInstance().lookupOperatorNameForDisplayName(
                        subId, simNumeric, true, mContext);
                logd("[handleSimLoaded]- simNumeric: " + simNumeric +
                            ", simMvnoName: " + simMvnoName);
                if (!TextUtils.isEmpty(simMvnoName)) {
                    nameToSet = simMvnoName;
                } else {
                    if (!TextUtils.isEmpty(simCarrierName)) {
                        nameToSet = simCarrierName;
                    } else {
                        nameToSet = "CARD " + Integer.toString(slotId + 1);
                    }
                }
                // MTK-END
                    // MTK-START
                    //name.put(SubscriptionManager.DISPLAY_NAME, nameToSet);
                    //logd("sim name = " + nameToSet);
                    //contentResolver.update(SubscriptionManager.CONTENT_URI, name,
                    //        SubscriptionManager.UNIQUE_KEY_SUBSCRIPTION_ID
                    //        + "=" + Long.toString(subId), null);

                    // refresh Cached Active Subscription Info List
                    SubscriptionController.getInstance()
                            .refreshCachedActiveSubscriptionInfoList();
                    mSubscriptionManager.setDisplayName(nameToSet, subId);
                    logd("[handleSimLoaded] subId = " + subId + ", sim name = " + nameToSet);
                    // MTK-END
                }

                /* Update preferred network type and network selection mode on SIM change.
                 * Storing last subId in SharedPreference for now to detect SIM change. */
                SharedPreferences sp =
                        PreferenceManager.getDefaultSharedPreferences(mContext);
                int storedSubId = sp.getInt(CURR_SUBID + slotId, -1);

                if (storedSubId != subId) {
                    // MTK-START
                    int networkType = Settings.Global.getInt(mPhone[slotId].getContext()
                            .getContentResolver(), Settings.Global.PREFERRED_NETWORK_MODE + subId,
                            RILConstants.PREFERRED_NETWORK_MODE);
                    // MTK-END

                    // MTK-START
                    // Set the modem network mode
                    // MTK network mode logic is central controled by GsmSST
                    logd("Possibly a new IMSI. Set sub(" + subId + ") networkType to "
                            + networkType);
                    //mPhone[slotId].setPreferredNetworkType(networkType, null);

                    // Dynamic check if modem chip cannot access 4G START
                    logd("check persist.radio.lte.chip : " +
                        SystemProperties.get("persist.radio.lte.chip"));

                    if (SystemProperties.get("persist.radio.lte.chip").equals("2")) {
                        if (networkType == RILConstants.NETWORK_MODE_LTE_CDMA_EVDO
                                || networkType == RILConstants.NETWORK_MODE_LTE_GSM_WCDMA
                                || networkType == RILConstants.NETWORK_MODE_LTE_CDMA_EVDO_GSM_WCDMA
                                || networkType == RILConstants.NETWORK_MODE_LTE_ONLY
                                || networkType == RILConstants.NETWORK_MODE_LTE_WCDMA
                                || networkType == RILConstants.NETWORK_MODE_LTE_TDSCDMA
                                || networkType == RILConstants.NETWORK_MODE_LTE_TDSCDMA_GSM
                                || networkType == RILConstants.NETWORK_MODE_LTE_TDSCDMA_WCDMA
                                || networkType == RILConstants.NETWORK_MODE_LTE_TDSCDMA_GSM_WCDMA
                                || networkType ==
                                        RILConstants.NETWORK_MODE_LTE_TDSCDMA_CDMA_EVDO_GSM_WCDMA){
                            if (SystemProperties.get("ro.boot.opt_c2k_support").equals("1")) {
                                networkType = RILConstants.NETWORK_MODE_GLOBAL;
                            } else {
                                networkType = RILConstants.NETWORK_MODE_WCDMA_PREF;
                            }
                            logd("Chip limit access 4G,modify PREFERRED_NETWORK_MODE init value to "
                                    + networkType + ",subId = " + subId);
                        }
                    }
                    // Dynamic check if modem chip cannot access 4G END
                    // MTK-END
                    Settings.Global.putInt(mPhone[slotId].getContext().getContentResolver(),
                            Settings.Global.PREFERRED_NETWORK_MODE + subId,
                            networkType);

                    // Only support automatic selection mode on SIM change.
                    mPhone[slotId].getNetworkSelectionMode(
                            obtainMessage(
                            EVENT_GET_NETWORK_SELECTION_MODE_DONE, new Integer(slotId)));

                    // Update stored subId
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putInt(CURR_SUBID + slotId, subId);
                    editor.apply();
                }
                if (!SubscriptionManager.isValidSubscriptionId(subId)) {
                    logd("Invalid subId, could not update ContentResolver, send sim loaded.");
                    // MTK-START
                    MtkSubscriptionController subCtrl = MtkSubscriptionController.getMtkInstance();
                    if (subCtrl != null) {
                        if (!subCtrl.isReady()) {
                            SystemClock.sleep(100);
                            sendMessage(obtainMessage(EVENT_SIM_LOADED, slotId, -1));
                        }
                    } else {
                        logd("subCtrl is null");
                    }
                    // MTK-END
                }

                // Update set of enabled carrier apps now that the privilege rules may have changed.
                CarrierAppUtils.disableCarrierAppsUntilPrivileged(mContext.getOpPackageName(),
                        mPackageManager, TelephonyManager.getDefault(),
                        mContext.getContentResolver(), mCurrentlyActiveUserId);

                broadcastSimStateChanged(slotId, IccCardConstants.INTENT_VALUE_ICC_LOADED, null);
                updateCarrierServices(slotId, IccCardConstants.INTENT_VALUE_ICC_LOADED);
            }
        }
    }

    @Override
    protected void handleSimAbsent(int slotId) {
        if (mIccId[slotId] != null && !mIccId[slotId].equals(ICCID_STRING_FOR_NO_SIM)) {
            logd("SIM" + (slotId + 1) + " hot plug out");
        }

        // MTK-START
        // If card inserted state no changed, no need to update.
        // Return directly to avoid unneccessary update cause timing issue.
        if (mIccId[slotId] != null && mIccId[slotId].equals(ICCID_STRING_FOR_NO_SIM)) {
            logd("SIM" + (slotId + 1) + " absent - card state no changed.");
            updateCarrierServices(slotId, IccCardConstants.INTENT_VALUE_ICC_ABSENT);
            return;
        }
        if (SystemProperties.get(COMMON_SLOT_PROPERTY).equals("1")) {
            if (checkAllIccIdReady()) {
                updateSubscriptionInfoIfNeed();
            }
        } else {
            mIccId[slotId] = ICCID_STRING_FOR_NO_SIM;
            if (isAllIccIdQueryDone()) {
                updateSubscriptionInfoByIccId();
            }
        }
        // MTK-END
        updateCarrierServices(slotId, IccCardConstants.INTENT_VALUE_ICC_ABSENT);
    }

    /**
     * TODO: Simplify more, as no one is interested in what happened
     * only what the current list contains.
     */
    @Override
    synchronized protected void updateSubscriptionInfoByIccId() {
        // MTK-START
        synchronized (mLock) {
            MtkSubscriptionController.updateDBLock.lock();
        // MTK-END
            logd("updateSubscriptionInfoByIccId:+ Start");

            // MTK-START
            // ALPS01933839 timing issue, JE after receiving IPO shutdown
            // do this update
            if (!isAllIccIdQueryDone()) {
                MtkSubscriptionController.updateDBLock.unlock();
                return;
            }
            // MTK-END

            // MTK-START
            // Reset the flag because all sIccId are ready.
            mCommonSlotResetDone = false;
            int simState = 0;
            boolean skipCapabilitySwitch = false;
            // MTK-END
            for (int i = 0; i < PROJECT_SIM_NUM; i++) {
                mInsertSimState[i] = SIM_NOT_CHANGE;
                // MTK-START
                simState = TelephonyManager.from(mContext).getSimState(i);
                if (simState == TelephonyManager.SIM_STATE_PIN_REQUIRED ||
                    simState == TelephonyManager.SIM_STATE_PUK_REQUIRED ||
                    simState == TelephonyManager.SIM_STATE_NETWORK_LOCKED ||
                    simState == TelephonyManager.SIM_STATE_NOT_READY) {
                    logd("skipCapabilitySwitch = " + skipCapabilitySwitch);
                    skipCapabilitySwitch = true;
                }
                // MTK-END
            }

            int insertedSimCount = PROJECT_SIM_NUM;
            for (int i = 0; i < PROJECT_SIM_NUM; i++) {
                if (ICCID_STRING_FOR_NO_SIM.equals(mIccId[i])) {
                    insertedSimCount--;
                    mInsertSimState[i] = SIM_NOT_INSERT;
                }
            }
            logd("insertedSimCount = " + insertedSimCount);

            // We only clear the slot-to-sub map when one/some SIM was removed. Note this is a
            // workaround for some race conditions that the empty map was accessed while we are
            // rebuilding the map.
            /* if (SubscriptionController.getInstance().getActiveSubIdList().length >
                       insertedSimCount) {
                SubscriptionController.getInstance().clearSubInfo();
            } */


            int index = 0;
            for (int i = 0; i < PROJECT_SIM_NUM; i++) {
                if (mInsertSimState[i] == SIM_NOT_INSERT) {
                    continue;
                }
                index = 2;
                for (int j = i + 1; j < PROJECT_SIM_NUM; j++) {
                    if (mInsertSimState[j] == SIM_NOT_CHANGE && mIccId[i].equals(mIccId[j])) {
                        mInsertSimState[i] = 1;
                        mInsertSimState[j] = index;
                        index++;
                    }
                }
            }

            ContentResolver contentResolver = mContext.getContentResolver();
            String[] oldIccId = new String[PROJECT_SIM_NUM];
            for (int i = 0; i < PROJECT_SIM_NUM; i++) {
                oldIccId[i] = null;
                List<SubscriptionInfo> oldSubInfo =
                        MtkSubscriptionController.getMtkInstance()
                        .getSubInfoUsingSlotIndexWithCheck(i,
                        false, mContext.getOpPackageName());
                if (oldSubInfo != null && oldSubInfo.size() > 0) {
                    oldIccId[i] = oldSubInfo.get(0).getIccId();
                    logd("updateSubscriptionInfoByIccId: oldSubId = "
                            + oldSubInfo.get(0).getSubscriptionId());
                    if (mInsertSimState[i] == SIM_NOT_CHANGE && !mIccId[i].equals(oldIccId[i])) {
                        mInsertSimState[i] = SIM_CHANGED;
                    }
                    if (mInsertSimState[i] != SIM_NOT_CHANGE) {
                        // MTK-START
                        MtkSubscriptionController.getMtkInstance().clearSubInfoUsingPhoneId(i);
                        logd("updateSubscriptionInfoByIccId: clearSubInfoUsingPhoneId phoneId = "
                                + i);
                        // MTK-END
                        try {
                            ContentValues value = new ContentValues(1);
                            value.put(SubscriptionManager.SIM_SLOT_INDEX,
                                    SubscriptionManager.INVALID_SIM_SLOT_INDEX);
                            contentResolver.update(SubscriptionManager.CONTENT_URI, value,
                                    SubscriptionManager.UNIQUE_KEY_SUBSCRIPTION_ID + "="
                                    + Integer.toString(oldSubInfo.get(0).getSubscriptionId()),
                                    null);
                        } catch (IllegalArgumentException e) {
                            MtkSubscriptionController.updateDBLock.unlock();
                            throw e;
                        }

                        // refresh Cached Active Subscription Info List
                        SubscriptionController.getInstance()
                                .refreshCachedActiveSubscriptionInfoList();
                    }
                } else {
                    if (mInsertSimState[i] == SIM_NOT_CHANGE) {
                        // no SIM inserted last time, but there is one SIM inserted now
                        mInsertSimState[i] = SIM_CHANGED;
                    }
                    // MTK-START
                    MtkSubscriptionController.getMtkInstance().clearSubInfoUsingPhoneId(i);
                    logd("updateSubscriptionInfoByIccId: clearSubInfoUsingPhoneId phoneId = " + i);
                    // MTK-END
                    oldIccId[i] = ICCID_STRING_FOR_NO_SIM;
                    logd("updateSubscriptionInfoByIccId: No SIM in slot " + i + " last time");
                }
            }

            for (int i = 0; i < PROJECT_SIM_NUM; i++) {
                logd("updateSubscriptionInfoByIccId: oldIccId[" + i + "] = " +
                        SubscriptionInfo.givePrintableIccid(oldIccId[i]) +
                        ", sIccId[" + i + "] = " + SubscriptionInfo.givePrintableIccid(mIccId[i]));
            }

            //check if the inserted SIM is new SIM
            int nNewCardCount = 0;
            int nNewSimStatus = 0;
            for (int i = 0; i < PROJECT_SIM_NUM; i++) {
                if (mInsertSimState[i] == SIM_NOT_INSERT) {
                    logd("updateSubscriptionInfoByIccId: No SIM inserted in slot " + i
                            + " this time");
                } else {
                    if (mInsertSimState[i] > 0) {
                        //some special SIMs may have the same IccIds, add suffix to distinguish them
                        //FIXME: addSubInfoRecord can return an error.
                        mSubscriptionManager.addSubscriptionInfoRecord(mIccId[i]
                                + Integer.toString(mInsertSimState[i]), i);
                        logd("SUB" + (i + 1) + " has invalid IccId");
                    } else /*if (sInsertSimState[i] != SIM_NOT_INSERT)*/ {
                        mSubscriptionManager.addSubscriptionInfoRecord(mIccId[i], i);
                    }
                    if (isNewSim(mIccId[i], oldIccId)) {
                        nNewCardCount++;
                        switch (i) {
                            case PhoneConstants.SUB1:
                                nNewSimStatus |= STATUS_SIM1_INSERTED;
                                break;
                            case PhoneConstants.SUB2:
                                nNewSimStatus |= STATUS_SIM2_INSERTED;
                                break;
                            case PhoneConstants.SUB3:
                                nNewSimStatus |= STATUS_SIM3_INSERTED;
                                break;
                            //case PhoneConstants.SUB3:
                            //    nNewSimStatus |= STATUS_SIM4_INSERTED;
                            //    break;
                            default:
                                break;
                        }

                        mInsertSimState[i] = SIM_NEW;
                    }
                }
            }

            for (int i = 0; i < PROJECT_SIM_NUM; i++) {
                if (mInsertSimState[i] == SIM_CHANGED) {
                    mInsertSimState[i] = SIM_REPOSITION;
                }
                logd("updateSubscriptionInfoByIccId: sInsertSimState[" + i + "] = "
                        + mInsertSimState[i]);
            }

            List<SubscriptionInfo> subInfos = mSubscriptionManager.getActiveSubscriptionInfoList();
            int nSubCount = (subInfos == null) ? 0 : subInfos.size();
            logd("updateSubscriptionInfoByIccId: nSubCount = " + nSubCount);
            for (int i = 0; i < nSubCount; i++) {
                SubscriptionInfo temp = subInfos.get(i);

                // MTK-START
                // Shoudn't use getDefault, it will suffer permission issue and can't get
                // line1 number correctly.
                String msisdn = TelephonyManager.from(mContext).getLine1Number(
                        temp.getSubscriptionId());
                // MTK-END

                if (msisdn != null) {
                    // MTK-START
                    //ContentValues value = new ContentValues(1);
                    //value.put(SubscriptionManager.NUMBER, msisdn);
                    //contentResolver.update(SubscriptionManager.CONTENT_URI, value,
                    //        SubscriptionManager.UNIQUE_KEY_SUBSCRIPTION_ID + "="
                    //        + Integer.toString(temp.getSubscriptionId()), null);
                    MtkSubscriptionController.getMtkInstance()
                            .setDisplayNumber(msisdn, temp.getSubscriptionId());
                    // MTK-END

                    // refresh Cached Active Subscription Info List
                    SubscriptionController.getInstance()
                            .refreshCachedActiveSubscriptionInfoList();
                }
            }

            // MTK-START
            //setAllDefaultSub(subInfos);

            // true if any slot has no SIM this time, but has SIM last time
            boolean hasSimRemoved = false;
            for (int i = 0; i < PROJECT_SIM_NUM; i++) {
                if (mIccId[i] != null && mIccId[i].equals(ICCID_STRING_FOR_NO_SIM)
                        && !oldIccId[i].equals(ICCID_STRING_FOR_NO_SIM)) {
                    hasSimRemoved = true;
                    break;
                }
            }

            Intent intent = null;
            if (nNewCardCount == 0) {
                int i;
                if (hasSimRemoved) {
                    // no new SIM, at least one SIM is removed, check if any SIM is repositioned
                    for (i = 0; i < PROJECT_SIM_NUM; i++) {
                        if (mInsertSimState[i] == SIM_REPOSITION) {
                            logd("No new SIM detected and SIM repositioned");
                            intent = setUpdatedData(
                                    MtkSubscriptionManager.EXTRA_VALUE_REPOSITION_SIM,
                                    nSubCount, nNewSimStatus);
                            break;
                        }
                    }
                    if (i == PROJECT_SIM_NUM) {
                        // no new SIM, no SIM is repositioned => at least one SIM is removed
                        logd("No new SIM detected and SIM removed");
                        intent = setUpdatedData(MtkSubscriptionManager.EXTRA_VALUE_REMOVE_SIM,
                                nSubCount, nNewSimStatus);
                    }
                } else {
                    // no SIM is removed, no new SIM, just check if any SIM is repositioned
                    for (i = 0; i < PROJECT_SIM_NUM; i++) {
                        if (mInsertSimState[i] == SIM_REPOSITION) {
                            logd("No new SIM detected and SIM repositioned");
                            intent = setUpdatedData(
                                    MtkSubscriptionManager.EXTRA_VALUE_REPOSITION_SIM,
                                    nSubCount, nNewSimStatus);
                            break;
                        }
                    }
                    if (i == PROJECT_SIM_NUM) {
                        // all status remain unchanged
                        logd("[updateSimInfoByIccId] All SIM inserted into the same slot");
                        intent = setUpdatedData(MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE,
                                nSubCount, nNewSimStatus);
                    }
                }
            } else {
                logd("New SIM detected");
                intent = setUpdatedData(MtkSubscriptionManager.EXTRA_VALUE_NEW_SIM, nSubCount,
                        nNewSimStatus);
            }
            // MTK-END

            // MTK-START
            //Single project has update in add
            if (PROJECT_SIM_NUM > 1) {
                if (!skipCapabilitySwitch) {
                // MTK-END
                    // Ensure the modems are mapped correctly
                    mSubscriptionManager.setDefaultDataSubId(
                            mSubscriptionManager.getDefaultDataSubscriptionId());
                // MTK-START
                } else {
                    // We only set default data subId here due to capability switch cause
                    // RADIO_UNAVAILABLE on our platform.
                    // Radio module will do capability switch after
                    MtkSubscriptionController.getMtkInstance()
                            .setDefaultDataSubIdWithoutCapabilitySwitch(
                            mSubscriptionManager.getDefaultDataSubscriptionId());
                }
            }
            // MTK-END

            // MTK-START
            //SubscriptionController.getInstance().notifySubscriptionInfoChanged();
            MtkSubscriptionController.updateDBLock.unlock();
            MtkSubscriptionController.getMtkInstance().notifySubscriptionInfoChanged(intent);
            // MTK-END
            logd("updateSubscriptionInfoByIccId:- SsubscriptionInfo update complete");
        // MTK-STRAT
        }
        // MTK-END
    }

    @Override
    protected boolean isNewSim(String iccId, String[] oldIccId) {
        boolean newSim = true;
        for(int i = 0; i < PROJECT_SIM_NUM; i++) {
            // MTK-START
            // Modify for special SIMs have the same IccIds
            if (iccId != null && oldIccId[i] != null) {
                if (oldIccId[i].indexOf(iccId) == 0) {
                    newSim = false;
                    break;
                }
            }
            // MTK-END
        }
        logd("newSim = " + newSim);

        return newSim;
    }

    @Override
    protected void broadcastSimStateChanged(int slotId, String state, String reason) {
        Intent i = new Intent(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        // TODO - we'd like this intent to have a single snapshot of all sim state,
        // but until then this should not use REPLACE_PENDING or we may lose
        // information
        // i.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING
        //         | Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);
        i.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);
        i.putExtra(PhoneConstants.PHONE_NAME_KEY, "Phone");
        i.putExtra(IccCardConstants.INTENT_KEY_ICC_STATE, state);
        i.putExtra(IccCardConstants.INTENT_KEY_LOCKED_REASON, reason);
        SubscriptionManager.putPhoneIdAndSubIdExtra(i, slotId);
        logd("Broadcasting intent ACTION_SIM_STATE_CHANGED " + state + " reason " + reason +
             " for mCardIndex: " + slotId);
        IntentBroadcaster.getInstance().broadcastStickyIntent(i, slotId);
    }

    @Override
    public void dispose() {
        logd("[dispose]");
        // MTK-START
        //mContext.unregisterReceiver(sReceiver);
        mContext.unregisterReceiver(mMtkReceiver);
        mDefaultExecutor.shutdownNow();
        // MTK-END
    }

    // Please add override APIs before this API and add MTK APIs after this API.
    // Notes, please place the MTK code by functions, inner class and member order
    private Intent setUpdatedData(int detectedType, int subCount, int newSimStatus) {

        Intent intent = new Intent(TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED);

        logd("[setUpdatedData]+ ");

        if (detectedType == MtkSubscriptionManager.EXTRA_VALUE_NEW_SIM) {
            intent.putExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                    MtkSubscriptionManager.EXTRA_VALUE_NEW_SIM);
            intent.putExtra(MtkSubscriptionManager.INTENT_KEY_SIM_COUNT, subCount);
            intent.putExtra(MtkSubscriptionManager.INTENT_KEY_NEW_SIM_SLOT, newSimStatus);
        } else if (detectedType == MtkSubscriptionManager.EXTRA_VALUE_REPOSITION_SIM) {
            intent.putExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                    MtkSubscriptionManager.EXTRA_VALUE_REPOSITION_SIM);
            intent.putExtra(MtkSubscriptionManager.INTENT_KEY_SIM_COUNT, subCount);
        } else if (detectedType == MtkSubscriptionManager.EXTRA_VALUE_REMOVE_SIM) {
            intent.putExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                    MtkSubscriptionManager.EXTRA_VALUE_REMOVE_SIM);
            intent.putExtra(MtkSubscriptionManager.INTENT_KEY_SIM_COUNT, subCount);
        } else if (detectedType == MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE) {
            intent.putExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                    MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE);
        }

        logd("[setUpdatedData]- [" + detectedType + ", " + subCount + ", " + newSimStatus + "]");
        return intent;
    }

    private boolean checkAllIccIdReady() {
        String iccId = "";
        logd("checkAllIccIdReady +, retry_count = " + mReadIccIdCount);
        for (int i = 0; i < PROJECT_SIM_NUM; i++) {
            iccId = SystemProperties.get(PROPERTY_ICCID_SIM[i], "");
            if (iccId.length() == 3) {
                logd("No SIM insert :" + i);
            }
            if (iccId.equals("")) {
                return false;
            }
            logd("iccId[" + i + "] = " + SubscriptionInfo.givePrintableIccid(iccId));
        }

        return true;
    }

    private void updateSubscriptionInfoIfNeed() {
        logd("[updateSubscriptionInfoIfNeed]+");
        boolean needUpdate = false;
        for (int i = 0; i < PROJECT_SIM_NUM; i++) {
            if (mIccId[i] == null ||
                    !mIccId[i].equals(SystemProperties.get(PROPERTY_ICCID_SIM[i], ""))) {
                logd("[updateSubscriptionInfoIfNeed] icc id change, slot[" + i + "]");
                mIccId[i] = SystemProperties.get(PROPERTY_ICCID_SIM[i], "");
                needUpdate = true;
            }
        }

        if (isAllIccIdQueryDone()) {
            if (needUpdate) {
                mDefaultExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        updateSubscriptionInfoByIccId();
                    }
                });
            }
        }
        logd("[updateSubscriptionInfoIfNeed]- return: " + needUpdate);
    }

    private Integer getCiIndex(Message msg) {
        AsyncResult ar;
        Integer index = new Integer(PhoneConstants.DEFAULT_CARD_INDEX);

        /*
         * The events can be come in two ways. By explicitly sending it using
         * sendMessage, in this case the user object passed is msg.obj and from
         * the CommandsInterface, in this case the user object is msg.obj.userObj
         */
        if (msg != null) {
            if (msg.obj != null && msg.obj instanceof Integer) {
                index = (Integer) msg.obj;
            } else if (msg.obj != null && msg.obj instanceof AsyncResult) {
                ar = (AsyncResult) msg.obj;
                if (ar.userObj != null && ar.userObj instanceof Integer) {
                    index = (Integer) ar.userObj;
                }
            }
        }
        return index;
    }

    private boolean checkIsAvailable() {
        boolean result = true;
        for (int i = 0; i < PROJECT_SIM_NUM; i++) {
            if (mIsUpdateAvailable[i] <= 0) {
                logd("mIsUpdateAvailable[" + i + "] = " + mIsUpdateAvailable[i]);
                result = false;
                break;
            }
        }
        logd("checkIsAvailable result = " + result);
        return result;
    }

    /**
     * Subscription update thread.
     */
    private class SubscriptionUpdatorThread extends Thread {
        public static final int SIM_ABSENT = 0;
        public static final int SIM_LOADED = 1;
        public static final int SIM_LOCKED = 2;
        public static final int SIM_READY  = 3;
        public static final int SIM_NO_CHANGED = 4;

        private QueryIccIdUserObj mUserObj;
        private int mEventId;

        SubscriptionUpdatorThread(QueryIccIdUserObj userObj, int eventId) {
            mUserObj = userObj;
            mEventId = eventId;
        }

        @Override
        public void run() {
            switch (mEventId) {
                case SIM_ABSENT:
                    handleSimAbsent(mUserObj.slotId);
                    break;

                case SIM_LOADED:
                    handleSimLoaded(mUserObj.slotId);
                    break;

                case SIM_LOCKED:
                    if (isAllIccIdQueryDone()) {
                          updateSubscriptionInfoByIccId();
                    }
                    broadcastSimStateChanged(mUserObj.slotId,
                            IccCardConstants.INTENT_VALUE_ICC_LOCKED, mUserObj.reason);
                    break;
                case SIM_READY:
                    if (checkAllIccIdReady()) {
                        updateSubscriptionInfoIfNeed();
                    }
                    break;
                case SIM_NO_CHANGED:
                    logd("[Common Slot]SubscriptionUpdatorThread run for SIM_NO_CHANGED.");
                    //mIccId[mUserObj.slotId] = ICCID_STRING_FOR_NO_SIM;
                    if (checkAllIccIdReady()) {
                        updateSubscriptionInfoIfNeed();
                    } else {
                        mIccId[mUserObj.slotId] = ICCID_STRING_FOR_NO_SIM;
                        logd("case SIM_NO_CHANGED: set N/A for slot" + mUserObj.slotId);
                        mReadIccIdCount = 0;
                        postDelayed(mReadIccIdPropertyRunnable, sReadICCID_retry_time);
                    }
                    break;
                default:
                    logd("SubscriptionUpdatorThread run with invalid event id.");
                    break;
            }
        }
    };

    // This is a substitue of AOSP's sReceiver for adding MTK feature
    private final BroadcastReceiver mMtkReceiver = new  BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            logd("[Receiver]+");
            String action = intent.getAction();
            logd("Action: " + action);

            if (!action.equals(TelephonyIntents.ACTION_SIM_STATE_CHANGED) &&
                    !action.equals(IccCardProxy.ACTION_INTERNAL_SIM_STATE_CHANGED) &&
                    !action.equals(MtkTelephonyIntents.ACTION_COMMON_SLOT_NO_CHANGED) &&
                    !action.equals(Intent.ACTION_LOCALE_CHANGED)) {
                return;
            }

            int slotIndex = intent.getIntExtra(PhoneConstants.PHONE_KEY,
                    SubscriptionManager.INVALID_SIM_SLOT_INDEX);
            logd("slotIndex: " + slotIndex);
            if (!SubscriptionManager.isValidSlotIndex(slotIndex)) {
                logd("ACTION_SIM_STATE_CHANGED contains invalid slotIndex: " + slotIndex);
                // MTK-START
                // Other MTK intents might has invalid sim slot index
                if (action.equals(TelephonyIntents.ACTION_SIM_STATE_CHANGED) ||
                        action.equals(IccCardProxy.ACTION_INTERNAL_SIM_STATE_CHANGED)) {
                    return;
                }
                // MTK-END
            }

            String simStatus = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
            logd("simStatus: " + simStatus);

            // TODO: All of the below should be converted to ACTION_INTERNAL_SIM_STATE_CHANGED to
            // ensure that the SubscriptionInfo is updated before the broadcasts are sent out.
            if (action.equals(TelephonyIntents.ACTION_SIM_STATE_CHANGED)) {
                if (IccCardConstants.INTENT_VALUE_ICC_ABSENT.equals(simStatus)) {
                    sendMessage(obtainMessage(EVENT_SIM_ABSENT, slotIndex, -1));
                } else if (IccCardConstants.INTENT_VALUE_ICC_UNKNOWN.equals(simStatus)) {
                    sendMessage(obtainMessage(EVENT_SIM_UNKNOWN, slotIndex, -1));
                } else if (IccCardConstants.INTENT_VALUE_ICC_CARD_IO_ERROR.equals(simStatus)) {
                    sendMessage(obtainMessage(EVENT_SIM_IO_ERROR, slotIndex, -1));
                } else if (IccCardConstants.INTENT_VALUE_ICC_CARD_RESTRICTED.equals(simStatus)) {
                    sendMessage(obtainMessage(EVENT_SIM_RESTRICTED, slotIndex, -1));
                // MTK-START
                } else if (IccCardConstants.INTENT_VALUE_ICC_READY.equals(simStatus)) {
                    sendMessage(obtainMessage(EVENT_SIM_READY, slotIndex, -1));
                // MTK-END
                } else {
                    logd("Ignoring simStatus: " + simStatus);
                }
            } else if (action.equals(IccCardProxy.ACTION_INTERNAL_SIM_STATE_CHANGED)) {
                if (IccCardConstants.INTENT_VALUE_ICC_LOCKED.equals(simStatus)) {
                    String reason = intent.getStringExtra(
                        IccCardConstants.INTENT_KEY_LOCKED_REASON);
                    sendMessage(obtainMessage(EVENT_SIM_LOCKED, slotIndex, -1, reason));
                } else if (IccCardConstants.INTENT_VALUE_ICC_LOADED.equals(simStatus)) {
                    sendMessage(obtainMessage(EVENT_SIM_LOADED, slotIndex, -1));
                    // MTK-START
                    mReadIccIdCount = 10;
                    // MTK-END
                } else {
                    logd("Ignoring simStatus: " + simStatus);
                }
            } else if (action.equals(Intent.ACTION_LOCALE_CHANGED)) {
                int[] subIdList = mSubscriptionManager.getActiveSubscriptionIdList();
                for (int subId : subIdList) {
                    updateSubName(subId);
                }
            } else if (action.equals(MtkTelephonyIntents.ACTION_COMMON_SLOT_NO_CHANGED)) {
                logd("[Common Slot] NO_CHANTED, slotId: " + slotIndex);
                sendMessage(obtainMessage(EVENT_SIM_NO_CHANGED, slotIndex, -1));
            }
            logd("[Receiver]-");
        }
    };

    private Runnable mReadIccIdPropertyRunnable = new Runnable() {
        public void run() {
            ++mReadIccIdCount;
            if (mReadIccIdCount <= 10) {
                if (!checkAllIccIdReady()) {
                    postDelayed(mReadIccIdPropertyRunnable, sReadICCID_retry_time);
                } else {
                    updateSubscriptionInfoIfNeed();
                }
            }
        }
    };

    private void updateSubName(int subId) {
        SubscriptionInfo subInfo =
                MtkSubscriptionManager.getSubInfo(null, subId);
        if (subInfo != null
                && subInfo.getNameSource() != SubscriptionManager.NAME_SOURCE_USER_INPUT) {
            MtkSpnOverride spnOverride = MtkSpnOverride.getInstance();
            String nameToSet;
            String carrierName = TelephonyManager.getDefault().getSimOperator(subId);
            int slotId = SubscriptionManager.getSlotIndex(subId);
            logd("updateSubName, carrierName = " + carrierName + ", subId = " + subId);
            if (SubscriptionManager.isValidSlotIndex(slotId)) {
                if (spnOverride.containsCarrierEx(carrierName)) {
                    nameToSet = spnOverride.lookupOperatorName(subId, carrierName,
                        true, mContext);
                    logd("SPN found, name = " + nameToSet);
                } else {
                    nameToSet = "CARD " + Integer.toString(slotId + 1);
                    logd("SPN not found, set name to " + nameToSet);
                }
                mSubscriptionManager.setDisplayName(nameToSet, subId);
            }
        }
    }

    private void logd(String message) {
        Rlog.d(LOG_TAG, message);
    }
}
