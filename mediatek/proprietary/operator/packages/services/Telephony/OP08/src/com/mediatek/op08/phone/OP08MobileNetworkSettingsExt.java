/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.op08.phone;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.android.ims.ImsManager;
import com.android.internal.telephony.Phone;


import com.mediatek.phone.ext.DefaultMobileNetworkSettingsExt;

/**
 * Plugin implementation for OP08.
 */
public class OP08MobileNetworkSettingsExt extends DefaultMobileNetworkSettingsExt {
    private static final String TAG = "OP08MobileNetworkSettingsExt";
    private static final String BUTTON_VOLTE_KEY = "volte_settings";
    private static final String BUTTON_4G_LTE_KEY = "enhanced_4g_lte";
    private static final String BUTTON_VOLTE_TITLE = "VoLTE";
    private Context mContext = null;
    private Preference mButtonVolte;
    /**
     * Plugin implementation for Mobile network settings.
     * @param context context
     */
    public OP08MobileNetworkSettingsExt(Context context) {
        mContext = context;
    }

    /**
     * Updating network mode summary.
     * @param buttonEnabledNetworks list preference
     * @param networkMode network mode
     */
    @Override
    public void updatePreferredNetworkValueAndSummary(ListPreference buttonEnabledNetworks,
            int networkMode) {
        switch (networkMode) {
            case Phone.NT_MODE_LTE_GSM_WCDMA:
            case Phone.NT_MODE_LTE_ONLY:
            case Phone.NT_MODE_LTE_WCDMA:
            case Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA:
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_on));
                break;
            case Phone.NT_MODE_WCDMA_PREF:
            case Phone.NT_MODE_WCDMA_ONLY:
            case Phone.NT_MODE_GSM_UMTS:
            case Phone.NT_MODE_CDMA:
            case Phone.NT_MODE_EVDO_NO_CDMA:
            case Phone.NT_MODE_GLOBAL:
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_off));
                break;
            case Phone.NT_MODE_GSM_ONLY:
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.only_2g));
                break;
            default:
        }
    }

    @Override
    public void initOtherMobileNetworkSettings(PreferenceActivity activity, int subId) {
        Log.d("@M_" + TAG, "Initialize preference activity" + activity);
        int phoneId = SubscriptionManager.getPhoneId(subId);
        PreferenceScreen prefSet = activity.getPreferenceScreen();
        final Preference buttonDataLte = (Preference) prefSet
                .findPreference(BUTTON_4G_LTE_KEY);
        if (buttonDataLte != null) {
            prefSet.removePreference(buttonDataLte);
        }

        mButtonVolte = new Preference(prefSet.getContext());
        mButtonVolte.setKey(BUTTON_VOLTE_KEY);
        mButtonVolte.setTitle(BUTTON_VOLTE_TITLE);
        if (ImsManager.isVolteEnabledByPlatform(activity, phoneId)) {
            prefSet.addPreference(mButtonVolte);
            Intent volteSettingsIntent = new Intent("action.com.mediatek.settings.VolteSettings");
            /*volteSettingsIntent.setComponent(new ComponentName("com.android.settings",
                    "com.mediatek.settings.Settings$VolteSettingsActivity"));*/
            mButtonVolte.setIntent(volteSettingsIntent);
        }

    }


    public boolean isEnhancedLTENeedToAdd(boolean defaultValue, int phoneId) {
        return false;
    }

    /**
     * Updating entry of list preference.
     * @param buttonEnabledNetworks preference activity
     */
    @Override
    public void changeEntries(ListPreference buttonEnabledNetworks) {
        Log.d(TAG, "update entry and entry values of list preference");
        CharSequence cs[] = mContext.getResources().getTextArray(
                R.array.enabled_networks_4g_choices);
        buttonEnabledNetworks.setEntries(cs);
    }
}
