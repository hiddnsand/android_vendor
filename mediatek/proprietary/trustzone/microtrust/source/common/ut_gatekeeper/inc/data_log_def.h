/*
 * Copyright (c) 2015-2016 MICROTRUST Incorporated
 * All rights reserved
 *
 * This file and software is confidential and proprietary to MICROTRUST Inc.
 * Unauthorized copying of this file and software is strictly prohibited.
 * You MUST NOT disclose this file and software unless you get a license
 * agreement from MICROTRUST Incorporated.
 */

#ifndef __DATA_LOG_DEF_H__
#define __DATA_LOG_DEF_H__

typedef unsigned int uint32_t;
typedef unsigned char uint8_t;
typedef unsigned char bool;

#include <android/log.h>

#ifndef LOG_TAG
#define LOG_TAG "microgatekeeper"
#endif

#define LOG_E(fmt, ...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG,fmt, ##__VA_ARGS__); \
                        __android_log_print(ANDROID_LOG_ERROR, LOG_TAG,"\n")

#define LOG_I(fmt, ...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, fmt, ##__VA_ARGS__); \
                        __android_log_print(ANDROID_LOG_INFO, LOG_TAG,"\n")
#endif
