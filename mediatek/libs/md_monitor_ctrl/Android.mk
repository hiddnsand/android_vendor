LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/frameworks/opt/mdm))
include $(CLEAR_VARS)
LOCAL_MODULE = md_monitor_ctrl
LOCAL_MODULE_CLASS = EXECUTABLES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libutils libcutils libselinux libccci_util_sys libmdloggerrecycle libc++ liblog
LOCAL_SRC_FILES = $(call get-prebuilt-src-arch,arm arm64)/md_monitor_ctrl
include $(BUILD_PREBUILT)
endif
