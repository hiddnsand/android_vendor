/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.Registrant;
import android.os.RegistrantList;
import android.os.SystemProperties;
import android.telephony.CarrierConfigManager;
import android.telephony.DisconnectCause;
import android.telephony.PhoneNumberUtils;
import android.telephony.Rlog;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

/// M: CC: For 3G VT only @{
import android.os.RemoteException;
import android.os.SystemProperties;
import android.telecom.VideoProfile;

import com.mediatek.internal.telephony.gsm.GsmVTProvider;
import com.mediatek.internal.telephony.gsm.GsmVTProviderUtil;
import com.mediatek.internal.telephony.gsm.MtkGsmVideoCallProviderWrapper;
import com.mediatek.internal.telephony.gsm.IMtkGsmVideoCallProvider;
/// @}

import com.android.internal.telephony.Call;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.DriverCall;
import com.android.internal.telephony.GsmCdmaCall;
import com.android.internal.telephony.GsmCdmaCallTracker;
import com.android.internal.telephony.GsmCdmaConnection;
import com.android.internal.telephony.GsmCdmaPhone;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyDevController;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.TelephonyProperties;
import com.android.internal.telephony.UUSInfo;
import com.android.internal.telephony.cdma.CdmaCallWaitingNotification;

import com.mediatek.internal.telephony.MtkPhoneNumberUtils;
import com.mediatek.internal.telephony.cdma.pluscode.PlusCodeProcessor;
import com.mediatek.internal.telephony.imsphone.MtkImsPhoneConnection;

import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MtkGsmCdmaCallTracker extends GsmCdmaCallTracker {

    private static final String PROP_LOG_TAG = "GsmCdmaCallTkr";
    public MtkRIL mMtkCi = null;

    protected static final int EVENT_MTK_BASE                      = 1000;
    /// M: CC: Proprietary incoming call handling
    protected static final int EVENT_INCOMING_CALL_INDICATION      = EVENT_MTK_BASE + 0;

    /// M: CC: Modem reset related handling
    protected static final int EVENT_RADIO_OFF_OR_NOT_AVAILABLE    = EVENT_MTK_BASE + 1;

    protected static final int EVENT_DIAL_CALL_RESULT              = EVENT_MTK_BASE + 2;

    /// M: CC: Hangup special handling @{
    protected static final int EVENT_HANG_UP_RESULT                = EVENT_MTK_BASE + 3;
    /// @}
    /// M: CC: CDMA call accepted @{
    protected static final int EVENT_CDMA_CALL_ACCEPTED            = EVENT_MTK_BASE + 4;
    /// @}

    // IMS conference call feature
    protected static final int EVENT_ECONF_SRVCC_INDICATION        = EVENT_MTK_BASE + 5;

    // Set ECC list to MD
    protected static final int EVENT_RADIO_ON                      = EVENT_MTK_BASE + 6;

    private OpTelephonyCustomizationFactoryBase mTelephonyCustomizationFactory = null;
    protected IMtkGsmCdmaCallTrackerExt mMtkGsmCdmaCallTrackerExt = null;

    /// M: CC: For 3G VT only @{
    /* voice&video waiting */
    private boolean hasPendingReplaceRequest = false;
    /// @}

    /// M: CC: Proprietary CRSS handling @{
    boolean mHasPendingSwapRequest = false;
    WaitForHoldToRedial mWaitForHoldToRedialRequest = new WaitForHoldToRedial();
    WaitForHoldToHangup mWaitForHoldToHangupRequest = new WaitForHoldToHangup();
    /// @}

    /// M: CC: Use MtkGsmCdmaCallTrackerHelper @{
    MtkGsmCdmaCallTrackerHelper mHelper;

    /// M: ALPS03359228 @{
    /* This flag is used to indicate that all calls disconnected but not handled for
       some corner cases */
    private boolean bAllCallsDisconnectedButNotHandled = false;
    /// @}

    /// M: CC: Record current phone type
    private int mPhoneType = PhoneConstants.PHONE_TYPE_NONE;

    /// M: CC: Vzw/CTVolte ECC @{
    TelephonyDevController mTelDevController = TelephonyDevController.getInstance();
    private boolean hasC2kOverImsModem() {
        if (mTelDevController != null &&
                mTelDevController.getModem(0) != null &&
                ((MtkHardwareConfig) mTelDevController.getModem(0)).hasC2kOverImsModem() == true) {
                    return true;
        }
        return false;
    }
    /// @}

    // Declare as public for MtkGsmCdmaCallTrackerHelper to use
    public int getMaxConnections() {
        return mPhone.isPhoneTypeGsm() ?
                MAX_CONNECTIONS_GSM :
                MAX_CONNECTIONS_CDMA;
    }
    /// @}

    ///M: For IMS conference SRVCC @{
    // ALPS02019630
    /**
     * For conference host side, need to wait until ECONFSRVCC URC recieved then all participant's
     * address could be known by this URC and the conference XML file.
     */
    protected boolean mNeedWaitImsEConfSrvcc = false;

    /**
     * Identify the host connection for conference host side SRVCC.
     */
    protected Connection mImsConfHostConnection = null;
    private ArrayList<Connection> mImsConfParticipants = new ArrayList<Connection>();

    // for SRVCC purpose, put conference connection Ids temporarily
    private int[] mEconfSrvccConnectionIds = null;
    /// @}

    // Receiver to set ecc list to MD when SIM update
    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TelephonyIntents.ACTION_SIM_STATE_CHANGED)) {
                String simState = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
                int slot = intent.getIntExtra(PhoneConstants.SLOT_KEY, PhoneConstants.SIM_ID_1);
                if (IccCardConstants.INTENT_VALUE_ICC_READY.equals(simState) &&
                        (slot == mPhone.getPhoneId())) {
                    // Set ECC list to MD when SIM ready
                    Rlog.d(LOG_TAG, "Set ECC list to MD when SIM ready, slot: " + slot);
                    mMtkCi.setEccList();
                }
            }
        }
    };

    /// M: CC: Proprietary CRSS handling @{
    class WaitForHoldToRedial {

        private boolean mWaitToRedial = false;
        private String mDialString = null;
        private int mClirMode = 0;
        private UUSInfo mUUSInfo = null;

        WaitForHoldToRedial() {
            resetToRedial();
        }

        boolean isWaitToRedial() {
            return mWaitToRedial;
        }

        void setToRedial() {
            mWaitToRedial = true;
        }

        public void setToRedial(String dialSting, int clir, UUSInfo uusinfo) {
            mWaitToRedial = true;
            mDialString = dialSting;
            mClirMode = clir;
            mUUSInfo = uusinfo;
        }

        public void resetToRedial() {

            proprietaryLog("Reset mWaitForHoldToRedialRequest variables");

            mWaitToRedial = false;
            mDialString = null;
            mClirMode = 0;
            mUUSInfo = null;
        }

        /**
         * Check if there is another action need to be performed after holding request is done.
         *
         * @return Return true if there exists action need to be perform, else return false.
         */
        // Dial -> Switch since ACTIVE call exists -> queue Dial -> Switch done -> resume Dial
        private boolean resumeDialAfterHold() {
            proprietaryLog("resumeDialAfterHold begin");

            // M: CC: For 93, MD can switch phoneType when SIM not inserted,
            // TeleService won't trigger phone switch, so check both SIM's ECC
            boolean isEcc = false;

            if (hasC2kOverImsModem() &&
                    !TelephonyManager.getDefault().hasIccCard(mPhone.getPhoneId())) {
                isEcc = PhoneNumberUtils.isEmergencyNumber(mDialString);
            } else {
                isEcc = PhoneNumberUtils.isEmergencyNumber(mPhone.getSubId(), mDialString);
            }

            if (mWaitToRedial) {
                if (isEcc && !MtkPhoneNumberUtils.isSpecialEmergencyNumber(
                        mPhone.getSubId(), mDialString)) {
                    int serviceCategory = MtkPhoneNumberUtils.getServiceCategoryFromEccBySubId(
                            mDialString, mPhone.getSubId());
                    mMtkCi.setEccServiceCategory(serviceCategory, null);
                    mMtkCi.emergencyDial(mDialString, mClirMode, mUUSInfo,
                            obtainCompleteMessage(EVENT_DIAL_CALL_RESULT));
                } else {
                    mCi.dial(mDialString, mClirMode, mUUSInfo,
                            obtainCompleteMessage(EVENT_DIAL_CALL_RESULT));
                }

                resetToRedial();
                proprietaryLog("resumeDialAfterHold end");
                return true;
            }
            return false;
        }
    }

    // Switch -> queue Hangup -> Switch done -> poll call -> poll call done -> resume Hangup
    // Switch -> Switch done -> poll call -> queue Hangup -> poll call done -> resume Hangup
    // Switch -> Switch done -> poll call -> poll call done -> resume Hangup (X)
    class WaitForHoldToHangup {

        private boolean mWaitToHangup = false;
        private boolean mHoldDone = false;
        private GsmCdmaCall mCall = null;

        WaitForHoldToHangup() {
            resetToHangup();
        }

        boolean isWaitToHangup() {
            return mWaitToHangup;
        }

        boolean isHoldDone() {
            return mHoldDone;
        }

        void setHoldDone() {
            mHoldDone = true;
        }

        void setToHangup() {
            mWaitToHangup = true;
        }

        public void setToHangup(GsmCdmaCall call) {
            mWaitToHangup = true;
            mCall = call;
        }

        public void resetToHangup() {

            proprietaryLog("Reset mWaitForHoldToHangupRequest variables");

            mWaitToHangup = false;
            mHoldDone = false;
            mCall = null;
        }

        /**
         * Check if there is another action need to be performed after holding request is done.
         *
         * @return Return true if there exists action need to be perform, else return false.
         */
        private boolean resumeHangupAfterHold() {
            proprietaryLog("resumeHangupAfterHold begin");

            if (mWaitToHangup) {
                if (mCall != null) {
                    proprietaryLog("resumeHangupAfterHold to hangup call");
                    mWaitToHangup = false;
                    mHoldDone = false;
                    try {
                        hangup(mCall);
                    } catch (CallStateException ex) {
                        Rlog.e(PROP_LOG_TAG, "unexpected error on hangup");
                    }
                    proprietaryLog("resumeHangupAfterHold end");
                    mCall = null;
                    return true;
                }
            }
            resetToHangup();
            return false;
        }

    }
    /// @}

    public MtkGsmCdmaCallTracker(GsmCdmaPhone phone) {
        super(phone);

        mRingingCall = new MtkGsmCdmaCall(this);
        // A call that is ringing or (call) waiting
        mForegroundCall = new MtkGsmCdmaCall(this);
        mBackgroundCall = new MtkGsmCdmaCall(this);

        mMtkCi = (MtkRIL)mCi;
        /// M: CC: Proprietary incoming call handling
        mMtkCi.setOnIncomingCallIndication(this, EVENT_INCOMING_CALL_INDICATION, null);
        /// M: CC: Modem reset related handling
        mMtkCi.registerForOffOrNotAvailable(this, EVENT_RADIO_OFF_OR_NOT_AVAILABLE, null);

        /// M: CC: Use MtkGsmCdmaCallTrackerHelper @{
        mHelper = new MtkGsmCdmaCallTrackerHelper(phone.getContext(), this);
        /// @}

        // For IMS conference SRVCC
        mMtkCi.registerForEconfSrvcc(this, EVENT_ECONF_SRVCC_INDICATION, null);

        // Register receiver for set ECC list
        mMtkCi.registerForOn(this, EVENT_RADIO_ON, null);
        IntentFilter filter = new IntentFilter();
        filter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        phone.getContext().registerReceiver(mIntentReceiver, filter);

        try {
            mTelephonyCustomizationFactory =
                    OpTelephonyCustomizationUtils.getOpFactory(mPhone.getContext());
            mMtkGsmCdmaCallTrackerExt =
                    mTelephonyCustomizationFactory.makeMtkGsmCdmaCallTrackerExt(phone.getContext());
        } catch (Exception e) {
            Rlog.d(LOG_TAG, "mMtkGsmCdmaCallTrackerExt init fail");
            e.printStackTrace();
        }
    }

    @Override
    protected void updatePhoneType(boolean duringInit) {
        /// M: CC: Don't update phone type when phone type is still CDMA @{
        if ((mPhoneType == PhoneConstants.PHONE_TYPE_CDMA)
                && !mPhone.isPhoneTypeGsm()) {
            return;
        }

        if (!duringInit) {
            reset();

            /// M: CC: Vzw/CTVolte ECC @{
            // If there is call exist, it may cause error
            // because mConnections is re-created in following code
            //pollCallsWhenSafe();
            if (hasC2kOverImsModem() || MtkTelephonyManagerEx.getDefault().useVzwLogic()) {
                if (VDBG) Rlog.d(LOG_TAG, "keep AOSP");
                Phone imsPhone = mPhone.getImsPhone();
                if (imsPhone == null ||
                        (imsPhone != null && imsPhone.getHandoverConnection() == null)) {
                    pollCallsWhenSafe();
                } else {
                    Rlog.d(LOG_TAG, "not trigger pollCall since imsCall exists");
                }
            }
            /// @}
        }

        super.updatePhoneType(true);

        /// M: CC: CDMA process call accepted @{
        if (mPhone.isPhoneTypeGsm()) {
            if (mMtkCi == null) {
                mMtkCi = (MtkRIL) mCi;
            }
            mMtkCi.unregisterForCdmaCallAccepted(this);
            /// M: CC: Update current phone type
            mPhoneType = PhoneConstants.PHONE_TYPE_GSM;
        } else {
            if (mMtkCi == null) {
                mMtkCi = (MtkRIL) mCi;
            }
            mMtkCi.unregisterForCdmaCallAccepted(this);
            mMtkCi.registerForCdmaCallAccepted(this, EVENT_CDMA_CALL_ACCEPTED, null);
            /// M: CC: Update current phone type
            mPhoneType = PhoneConstants.PHONE_TYPE_CDMA;
        }
        /// @}
    }

    @Override
    protected void reset() {
        /// M: CC: Vzw/CTVolte ECC @{
        if (hasC2kOverImsModem() || MtkTelephonyManagerEx.getDefault().useVzwLogic()) {
            /// M: ALPS03359228 @{
            /* If all calls disconnected but not handled by handlePollCalls(), need to
                notify connection disconnected to upper layer */
            if (bAllCallsDisconnectedButNotHandled) {
                handlePollCalls(new AsyncResult(null, null,
                        new CommandException(CommandException.Error.RADIO_NOT_AVAILABLE)));
            } else {
                if (VDBG) Rlog.d(LOG_TAG, "keep AOSP");
            }
        } else {
            handlePollCalls(new AsyncResult(null, null,
                    new CommandException(CommandException.Error.RADIO_NOT_AVAILABLE)));
        }
        /// @}

        /// M: ALPS03359228 @{
        /* If all calls disconnected but not handled by handlePollCalls(), need to
             notify connection disconnected to upper layer */
        bAllCallsDisconnectedButNotHandled = false;
        /// @}

        super.reset();
    }

    @Override
    protected synchronized void handlePollCalls(AsyncResult ar) {
        List polledCalls;

        if (VDBG) log("handlePollCalls");
        if (ar.exception == null) {
            polledCalls = (List)ar.result;
        } else if (isCommandExceptionRadioNotAvailable(ar.exception)) {
            // just a dummy empty ArrayList to cause the loop
            // to hang up all the calls
            polledCalls = new ArrayList();
        } else if (mNeedWaitImsEConfSrvcc && !hasParsingCEPCapability()) {
            /// M: For IMS conference SRVCC @{
            // ALPS02019630. Needs to wait +ECONFSRVCC then the call number could be known.
            proprietaryLog("SRVCC: +ECONFSRVCC is still not arrival, skip this poll call.");
            return;
            /// @}
        } else {
            // Radio probably wasn't ready--try again in a bit
            // But don't keep polling if the channel is closed
            pollCallsAfterDelay();
            return;
        }

        Connection newRinging = null; //or waiting
        ArrayList<Connection> newUnknownConnectionsGsm = new ArrayList<Connection>();
        Connection newUnknownConnectionCdma = null;
        boolean hasNonHangupStateChanged = false;   // Any change besides
                                                    // a dropped connection
        boolean hasAnyCallDisconnected = false;
        boolean needsPollDelay = false;
        boolean unknownConnectionAppeared = false;
        int handoverConnectionsSize = mHandoverConnections.size();

        //CDMA
        boolean noConnectionExists = true;

        for (int i = 0, curDC = 0, dcSize = polledCalls.size()
                ; i < mConnections.length; i++) {
            GsmCdmaConnection conn = mConnections[i];
            DriverCall dc = null;

            // polledCall list is sparse
            if (curDC < dcSize) {
                dc = (DriverCall) polledCalls.get(curDC);

                /// M: CC: CDMA plus code @{
                if (!isPhoneTypeGsm()) {
                    dc.number = processPlusCodeForDriverCall(
                            dc.number, dc.isMT, dc.TOA);
                }
                /// @}

                if (dc.index == i+1) {
                    curDC++;
                } else {
                    dc = null;
                }
            }

            //CDMA
            if (conn != null || dc != null) {
                noConnectionExists = false;
            }

            if (DBG_POLL) log("poll: conn[i=" + i + "]=" +
                    conn+", dc=" + dc);

            if (conn == null && dc != null) {

                /// M: CC
                if (DBG_POLL) log("case 1 : new Call appear");

                // Connection appeared in CLCC response that we don't know about
                if (mPendingMO != null && mPendingMO.compareTo(dc)) {

                    if (DBG_POLL) log("poll: pendingMO=" + mPendingMO);

                    /// M: CC: For 3G VT only @{
                    //MO:set id to VT service
                    if (SystemProperties.get("ro.mtk_vt3g324m_support").equals("1")) {
                        if ((((MtkGsmCdmaCall) mForegroundCall).mVTProvider != null)
                                && ((MtkDriverCall) dc).isVideo) {
                            ((MtkGsmCdmaCall) mForegroundCall).mVTProvider.setId(i + 1);
                        }
                    }
                    /// @}

                    // It's our pending mobile originating call
                    mConnections[i] = mPendingMO;
                    mPendingMO.mIndex = i;
                    mPendingMO.update(dc);
                    mPendingMO = null;

                    // Someone has already asked to hangup this call
                    if (mHangupPendingMO) {
                        mHangupPendingMO = false;

                        // M: CC: Allow ECM under GSM
                        // Re-start Ecm timer when an uncompleted emergency call ends
                        if (/*!isPhoneTypeGsm() && */mIsEcmTimerCanceled) {
                            handleEcmTimer(GsmCdmaPhone.RESTART_ECM_TIMER);
                        }

                        try {
                            if (Phone.DEBUG_PHONE) log(
                                    "poll: hangupPendingMO, hangup conn " + i);
                            hangup(mConnections[i]);
                        } catch (CallStateException ex) {
                            Rlog.e(LOG_TAG, "unexpected error on hangup");
                        }

                        // Do not continue processing this poll
                        // Wait for hangup and repoll
                        return;
                    }
                } else {
                    if (Phone.DEBUG_PHONE) {
                        log("pendingMo=" + mPendingMO + ", dc=" + dc);
                    }

                    /// M: CC: Remove handling for MO/MT conflict, not hangup MT @{
                    if (mPendingMO != null && !mPendingMO.compareTo(dc)) {
                        proprietaryLog("MO/MT conflict! MO should be hangup by MD");
                    }
                    /// @}

                    mConnections[i] = new MtkGsmCdmaConnection(mPhone, dc, this, i);

                    /// M: CC: Forwarding number via EAIC @{
                    if (isPhoneTypeGsm()) {
                        //To store forwarding address to connection object.
                        mHelper.setForwardingAddressToConnection(i, mConnections[i]);
                    }
                    /// @}

                    Connection hoConnection = getHoConnection(dc);
                    if (hoConnection != null) {
                        // Single Radio Voice Call Continuity (SRVCC) completed
                        /// M: modified to fulfill IMS conference SRVCC. @{
                        if (hoConnection instanceof MtkImsPhoneConnection &&
                                ((MtkImsPhoneConnection) hoConnection).isMultipartyBeforeHandover()
                                && ((MtkImsPhoneConnection) hoConnection).isConfHostBeforeHandover()
                                && !hasParsingCEPCapability()) {
                            Rlog.i(LOG_TAG, "SRVCC: goes to conference case.");
                            mConnections[i].mOrigConnection = hoConnection;
                            mImsConfParticipants.add(mConnections[i]);
                        } else {
                            Rlog.i(LOG_TAG, "SRVCC: goes to normal call case.");
                            /// @}
                            mConnections[i].migrateFrom(hoConnection);
                            // Updating connect time for silent redial cases (ex: Calls are
                            // transferred from DIALING/ALERTING/INCOMING/WAITING to ACTIVE)
                            if (hoConnection.mPreHandoverState != GsmCdmaCall.State.ACTIVE &&
                                    hoConnection.mPreHandoverState != GsmCdmaCall.State.HOLDING &&
                                    dc.state == DriverCall.State.ACTIVE) {
                                mConnections[i].onConnectedInOrOut();
                            }

                            mHandoverConnections.remove(hoConnection);

                            if (isPhoneTypeGsm()) {
                                for (Iterator<Connection> it = mHandoverConnections.iterator();
                                     it.hasNext(); ) {
                                    Connection c = it.next();
                                    Rlog.i(LOG_TAG, "HO Conn state is " + c.mPreHandoverState);
                                    if (c.mPreHandoverState == mConnections[i].getState()) {
                                        Rlog.i(LOG_TAG, "Removing HO conn "
                                                + hoConnection + c.mPreHandoverState);
                                        it.remove();
                                    }
                                }
                            }

                            /// M: CC: Set ECM timer canceled for SRVCC @{
                            if (mIsInEmergencyCall && !mIsEcmTimerCanceled
                                    && mPhone.isInEcm()) {
                                Rlog.i(LOG_TAG, "Ecm timer has been canceled in IMS, "
                                        + "so set mIsEcmTimerCanceled=true directly");
                                mIsEcmTimerCanceled = true;
                            }
                            /// @}

                            mPhone.notifyHandoverStateChanged(mConnections[i]);
                        }
                    } else {
                        // find if the MT call is a new ring or unknown connection
                        newRinging = checkMtFindNewRinging(dc,i);
                        if (newRinging == null) {
                            unknownConnectionAppeared = true;
                            if (isPhoneTypeGsm()) {
                                newUnknownConnectionsGsm.add(mConnections[i]);
                            } else {
                                newUnknownConnectionCdma = mConnections[i];
                            }
                        }
                    }
                }
                hasNonHangupStateChanged = true;
            } else if (conn != null && dc == null) {

                /// M: CC
                if (DBG_POLL) proprietaryLog("case 2 : old Call disappear");

                if (isPhoneTypeGsm()) {

                    /// M: CC: Convert state from WAITING to INCOMING @{
                    //[ALPS00401290]
                    if (((conn.getCall() == mForegroundCall &&
                            mForegroundCall.mConnections.size() == 1 &&
                            mBackgroundCall.isIdle()) ||
                            (conn.getCall() == mBackgroundCall &&
                            mBackgroundCall.mConnections.size() == 1 &&
                            mForegroundCall.isIdle())) &&
                            mRingingCall.getState() == GsmCdmaCall.State.WAITING) {
                        mRingingCall.mState = GsmCdmaCall.State.INCOMING;
                    }
                    /// @}

                    // Connection missing in CLCC response that we were
                    // tracking.
                    mDroppedDuringPoll.add(conn);

                    // M: CC: Allow ECM under GSM
                    // Re-start Ecm timer when the connected emergency call ends
                    if (mIsEcmTimerCanceled) {
                        handleEcmTimer(GsmCdmaPhone.RESTART_ECM_TIMER);
                    }

                    // Dropped connections are removed from the CallTracker
                    // list but kept in the GsmCdmaCall list
                    mConnections[i] = null;

                    /// M: CC: Proprietary incoming call handling @{
                    mHelper.CallIndicationEnd();
                    /// @}

                    /// M: CC: Forwarding number via EAIC @{
                    //To clear forwarding address if needed
                    mHelper.clearForwardingAddressVariables(i);
                    /// @}
                } else {
                    // This case means the RIL has no more active call anymore and
                    // we need to clean up the foregroundCall and ringingCall.
                    // Loop through foreground call connections as
                    // it contains the known logical connections.
                    int count = mForegroundCall.mConnections.size();
                    for (int n = 0; n < count; n++) {
                        if (Phone.DEBUG_PHONE)
                            log("adding fgCall cn " + n + " to droppedDuringPoll");
                        GsmCdmaConnection cn = (GsmCdmaConnection) mForegroundCall.mConnections
                                .get(n);
                        mDroppedDuringPoll.add(cn);
                    }
                    count = mRingingCall.mConnections.size();
                    // Loop through ringing call connections as
                    // it may contain the known logical connections.
                    for (int n = 0; n < count; n++) {
                        if (Phone.DEBUG_PHONE)
                            log("adding rgCall cn " + n + " to droppedDuringPoll");
                        GsmCdmaConnection cn = (GsmCdmaConnection) mRingingCall.mConnections
                                .get(n);
                        mDroppedDuringPoll.add(cn);
                    }

                    // Re-start Ecm timer when the connected emergency call ends
                    if (mIsEcmTimerCanceled) {
                        handleEcmTimer(GsmCdmaPhone.RESTART_ECM_TIMER);
                    }
                    // If emergency call is not going through while dialing
                    checkAndEnableDataCallAfterEmergencyCallDropped();
                }
                // Dropped connections are removed from the CallTracker
                // list but kept in the Call list
                mConnections[i] = null;
            } else if (conn != null && dc != null && !conn.compareTo(dc) && isPhoneTypeGsm()) {

                /// M: CC
                if (DBG_POLL) proprietaryLog("case 3 : old Call replaced");

                // Connection in CLCC response does not match what
                // we were tracking. Assume dropped call and new call

                mDroppedDuringPoll.add(conn);

                /// M: CC: Fix AOSP bug - to clear mPendingMO @{
                if (mPendingMO != null && mPendingMO.compareTo(dc)) {
                    // Use proprietary tag "GsmCdmaCallTkr" for extra log
                    Rlog.d("GsmCdmaCallTkr",
                            "ringing disc not updated yet & replaced by pendingMo");
                    /// M: CC: For 3G VT only @{
                    // MO:set id to VT service
                    if (SystemProperties.get("ro.mtk_vt3g324m_support").equals("1")) {
                        if ((((MtkGsmCdmaCall) mForegroundCall).mVTProvider != null)
                                && ((MtkDriverCall) dc).isVideo) {
                            ((MtkGsmCdmaCall) mForegroundCall).mVTProvider.setId(i + 1);
                        }
                    }
                    /// @}
                    mConnections[i] = mPendingMO;
                    mPendingMO.mIndex = i;
                    mPendingMO.update(dc);
                    mPendingMO = null;
                } else {
                    mConnections[i] = new MtkGsmCdmaConnection (mPhone, dc, this, i);
                }
                /// @}

                if (mConnections[i].getCall() == mRingingCall) {
                    newRinging = mConnections[i];
                } // else something strange happened
                hasNonHangupStateChanged = true;
            } else if (conn != null && dc != null) { /* implicit conn.compareTo(dc) */
                // Call collision case
                if (!isPhoneTypeGsm() && conn.isIncoming() != dc.isMT) {
                    if (dc.isMT == true) {
                        /// M: CC: Replace MO with MT when they are conflict. @{
                        mConnections[i] = new MtkGsmCdmaConnection(mPhone, dc, this, i);
                        /// @}
                        // Mt call takes precedence than Mo,drops Mo
                        mDroppedDuringPoll.add(conn);
                        // find if the MT call is a new ring or unknown connection
                        newRinging = checkMtFindNewRinging(dc,i);
                        if (newRinging == null) {
                            unknownConnectionAppeared = true;
                            newUnknownConnectionCdma = conn;
                        }
                        checkAndEnableDataCallAfterEmergencyCallDropped();
                    } else {
                        // Call info stored in conn is not consistent with the call info from dc.
                        // We should follow the rule of MT calls taking precedence over MO calls
                        // when there is conflict, so here we drop the call info from dc and
                        // continue to use the call info from conn, and only take a log.
                        Rlog.e(LOG_TAG,"Error in RIL, Phantom call appeared " + dc);
                    }
                } else {

                    /// M: CC
                    if (DBG_POLL) proprietaryLog("case 4 : old Call update");

                    boolean changed;
                    changed = conn.update(dc);
                    hasNonHangupStateChanged = hasNonHangupStateChanged || changed;
                }
            }

            if (REPEAT_POLLING) {
                if (dc != null) {
                    // FIXME with RIL, we should not need this anymore
                    if ((dc.state == DriverCall.State.DIALING
                            /*&& cm.getOption(cm.OPTION_POLL_DIALING)*/)
                        || (dc.state == DriverCall.State.ALERTING
                            /*&& cm.getOption(cm.OPTION_POLL_ALERTING)*/)
                        || (dc.state == DriverCall.State.INCOMING
                            /*&& cm.getOption(cm.OPTION_POLL_INCOMING)*/)
                        || (dc.state == DriverCall.State.WAITING
                            /*&& cm.getOption(cm.OPTION_POLL_WAITING)*/)) {
                        // Sometimes there's no unsolicited notification
                        // for state transitions
                        needsPollDelay = true;
                    }
                }
            }
        }

        // Safety check so that obj is not stuck with mIsInEmergencyCall set to true (and data
        // disabled). This should never happen though.
        if (!isPhoneTypeGsm() && noConnectionExists) {
            checkAndEnableDataCallAfterEmergencyCallDropped();
        }

        // This is the first poll after an ATD.
        // We expect the pending call to appear in the list
        // If it does not, we land here
        if (mPendingMO != null) {
            Rlog.d(LOG_TAG, "Pending MO dropped before poll fg state:"
                    + mForegroundCall.getState());

            mDroppedDuringPoll.add(mPendingMO);
            mPendingMO = null;
            mHangupPendingMO = false;

            // M: CC: Allow ECM under GSM
            if (mPendingCallInEcm) {
                mPendingCallInEcm = false;
            }
            /// M: CC: Re-start Ecm timer when the connected emergency call ends @{
            if (mIsEcmTimerCanceled) {
                handleEcmTimer(GsmCdmaPhone.RESTART_ECM_TIMER);
            }
            /// @}

            if (!isPhoneTypeGsm()) {
                checkAndEnableDataCallAfterEmergencyCallDropped();
            }
        }

        /// M: CC: handle CDMA flash pendingMO not disconnect during radio_off issue @{
        if (polledCalls.size() == 0 && mConnections.length == 0) {
            if (DBG_POLL) proprietaryLog("check whether fgCall or ringCall have mConnections");
            if (!isPhoneTypeGsm()) {
                // This case means the RIL has no more active call anymore and
                // we need to clean up the foregroundCall and ringingCall.
                // Loop through foreground call connections as
                // it contains the known logical connections.
                int count = mForegroundCall.mConnections.size();
                for (int n = 0; n < count; n++) {
                    if (Phone.DEBUG_PHONE) log("adding fgCall cn " + n + " to droppedDuringPoll");
                    GsmCdmaConnection cn = (GsmCdmaConnection) mForegroundCall.mConnections.get(n);
                    mDroppedDuringPoll.add(cn);
                }

                count = mRingingCall.mConnections.size();
                // Loop through ringing call connections as
                // it may contain the known logical connections.
                for (int n = 0; n < count; n++) {
                    if (Phone.DEBUG_PHONE) log("adding rgCall cn " + n + " to droppedDuringPoll");
                    GsmCdmaConnection cn = (GsmCdmaConnection) mRingingCall.mConnections.get(n);
                    mDroppedDuringPoll.add(cn);
                }
            }
        }
        /// @}

        if (newRinging != null) {
            mPhone.notifyNewRingingConnection(newRinging);
            /// M: CC: For 3G VT only @{
            //MT:new VT service
            if (SystemProperties.get("ro.mtk_vt3g324m_support").equals("1")) {
                if (((MtkGsmCdmaConnection)newRinging).isVideo()) {
                    newRinging.setVideoState(VideoProfile.STATE_BIDIRECTIONAL);
                    try {
                        GsmVTProviderUtil.setContext(mPhone.getContext());
                        ((MtkGsmCdmaCall)mRingingCall).mVTProvider = new GsmVTProvider(
                                ((GsmCdmaConnection) newRinging).getGsmCdmaIndex());
                        proprietaryLog("handlePollCalls new GsmVTProvider");
                        IMtkGsmVideoCallProvider gsmVideoCallProvider =
                                ((MtkGsmCdmaCall)mRingingCall).mVTProvider.getInterface();
                        if (gsmVideoCallProvider != null) {
                            MtkGsmVideoCallProviderWrapper gsmVideoCallProviderWrapper =
                                    new MtkGsmVideoCallProviderWrapper(gsmVideoCallProvider);
                            proprietaryLog("handlePollCalls new GsmVideoCallProviderWrapper");
                            newRinging.setVideoProvider(gsmVideoCallProviderWrapper);
                        }
                    } catch (CallStateException ex) {
                    } catch (ClassCastException e) {
                        Rlog.e(PROP_LOG_TAG, "cast to GsmCdmaConnection fail for newRinging " + e);
                    } catch (RemoteException e) {
                        Rlog.e(PROP_LOG_TAG,
                                "handlePollCalls new GsmVideoCallProviderWrapper failed");
                    }
                }
            }
            /// @}
        }

        // clear the "local hangup" and "missed/rejected call"
        // cases from the "dropped during poll" list
        // These cases need no "last call fail" reason
        ArrayList<GsmCdmaConnection> locallyDisconnectedConnections = new ArrayList<>();
        for (int i = mDroppedDuringPoll.size() - 1; i >= 0 ; i--) {
            GsmCdmaConnection conn = mDroppedDuringPoll.get(i);
            //CDMA
            boolean wasDisconnected = false;

            /// M: CC: Modem reset related handling @{
            if (isCommandExceptionRadioNotAvailable(ar.exception)) {
                mDroppedDuringPoll.remove(i);
                hasAnyCallDisconnected |= conn.onDisconnect(DisconnectCause.LOST_SIGNAL);
                wasDisconnected = true;
            /// @}
            } else if (conn.isIncoming() && conn.getConnectTime() == 0) {
                // Missed or rejected call
                int cause;
                if (conn.mCause == DisconnectCause.LOCAL) {
                    cause = DisconnectCause.INCOMING_REJECTED;
                } else {
                    cause = DisconnectCause.INCOMING_MISSED;
                }

                if (Phone.DEBUG_PHONE) {
                    log("missed/rejected call, conn.cause=" + conn.mCause);
                    log("setting cause to " + cause);
                }
                mDroppedDuringPoll.remove(i);
                hasAnyCallDisconnected |= conn.onDisconnect(cause);
                wasDisconnected = true;
                locallyDisconnectedConnections.add(conn);
            } else if (conn.mCause == DisconnectCause.LOCAL
                    || conn.mCause == DisconnectCause.INVALID_NUMBER) {
                mDroppedDuringPoll.remove(i);
                hasAnyCallDisconnected |= conn.onDisconnect(conn.mCause);
                wasDisconnected = true;
                locallyDisconnectedConnections.add(conn);
            }

            if (!isPhoneTypeGsm() && wasDisconnected && unknownConnectionAppeared
                    && conn == newUnknownConnectionCdma) {
                unknownConnectionAppeared = false;
                newUnknownConnectionCdma = null;
            }
        }
        if (locallyDisconnectedConnections.size() > 0) {
            mMetrics.writeRilCallList(mPhone.getPhoneId(), locallyDisconnectedConnections);
        }

        // IMS conference SRVCC
        // Added method to fulfill conference SRVCC for the host side.
        // The conference participant side is handled as normal call SRVCC.
        if (mImsConfHostConnection != null) {
            MtkImsPhoneConnection hostConn = (MtkImsPhoneConnection) mImsConfHostConnection;
            if (mImsConfParticipants.size() >= 2) {
                // Participants >= 2, apply MTK SRVCC solution.

                // Try to restore participants' address, we don't sure if +ECONFSRVCC is arrival.
                restoreConferenceParticipantAddress();

                proprietaryLog("SRVCC: notify new participant connections");
                hostConn.notifyConferenceConnectionsConfigured(mImsConfParticipants);
            } else if (mImsConfParticipants.size() == 1) {
                // Participants = 1, can't be a conference, so apply Google SRVCC solution.
                GsmCdmaConnection participant = (GsmCdmaConnection) mImsConfParticipants.get(0);

                // Conference host side with only one participant case.
                // Due to modem's limitation, we still need to restore the address since modem
                // doesn't notify address information.
                String address = hostConn.getConferenceParticipantAddress(0);
                proprietaryLog("SRVCC: restore participant connection with address: " + address);
                if (participant instanceof MtkGsmCdmaConnection) {
                    ((MtkGsmCdmaConnection) participant).
                            updateConferenceParticipantAddress(address);
                }

                proprietaryLog("SRVCC: only one connection, consider it as a normal call SRVCC");
                mPhone.notifyHandoverStateChanged(participant);
            } else {
                Rlog.e(PROP_LOG_TAG, "SRVCC: abnormal case, no participant connections.");
            }
            mImsConfParticipants.clear();
            mImsConfHostConnection = null;
            mEconfSrvccConnectionIds = null;
        }

        /* Disconnect any pending Handover connections */
        for (Iterator<Connection> it = mHandoverConnections.iterator();
                it.hasNext();) {
            Connection hoConnection = it.next();
            log("handlePollCalls - disconnect hoConn= " + hoConnection +
                    " hoConn.State= " + hoConnection.getState());
            if (hoConnection.getState().isRinging()) {
                hoConnection.onDisconnect(DisconnectCause.INCOMING_MISSED);
            } else {
                hoConnection.onDisconnect(DisconnectCause.NOT_VALID);
            }
            // TODO: Do we need to update these hoConnections in Metrics ?
            it.remove();
        }

        // Any non-local disconnects: determine cause
        /// M: CC: For 3G VT only @{
        if (mDroppedDuringPoll.size() > 0 &&
                !hasPendingReplaceRequest) {
        /// @}
            mMtkCi.getLastCallFailCause(
                obtainNoPollCompleteMessage(EVENT_GET_LAST_CALL_FAIL_CAUSE));
        }

        if (needsPollDelay) {
            pollCallsAfterDelay();
        }

        // Cases when we can no longer keep disconnected Connection's
        // with their previous calls
        // 1) the phone has started to ring
        // 2) A Call/Connection object has changed state...
        //    we may have switched or held or answered (but not hung up)
        if ((newRinging != null || hasNonHangupStateChanged || hasAnyCallDisconnected)
            /// M: CC: Proprietary CRSS handling @{
            && !mHasPendingSwapRequest) {
            /// @}

            internalClearDisconnected();
        }

        if (VDBG) log("handlePollCalls calling updatePhoneState()");
        updatePhoneState();

        if (unknownConnectionAppeared) {
            if (isPhoneTypeGsm()) {
                for (Connection c : newUnknownConnectionsGsm) {
                    log("Notify unknown for " + c);
                    mPhone.notifyUnknownConnection(c);
                }
            } else {
                mPhone.notifyUnknownConnection(newUnknownConnectionCdma);
            }
        }

        if (hasNonHangupStateChanged || newRinging != null || hasAnyCallDisconnected) {
            mPhone.notifyPreciseCallStateChanged();
            updateMetrics(mConnections);
        }

        // If all handover connections are mapped during this poll process clean it up
        if (handoverConnectionsSize > 0 && mHandoverConnections.size() == 0) {
            Phone imsPhone = mPhone.getImsPhone();
            if (imsPhone != null) {
                imsPhone.callEndCleanupHandOverCallIfAny();
            }
        }

        /// M: CC: Convert state from WAITING to INCOMING @{
        //[ALPS00401290]
        if (isPhoneTypeGsm() && mConnections != null
                && mConnections.length == MAX_CONNECTIONS_GSM) {
            if ((mHelper.getCurrentTotalConnections() == 1) &&
                    (mRingingCall.getState() == GsmCdmaCall.State.WAITING)) {
                mRingingCall.mState = GsmCdmaCall.State.INCOMING;
            }
        }
        /// @}

        //dumpState();
    }

    @Override
    protected void dumpState() {
        List l;

        Rlog.i(LOG_TAG,"Phone State:" + mState);

        Rlog.i(LOG_TAG,"Ringing call: " + mRingingCall.toString());

        l = mRingingCall.getConnections();
        for (int i = 0, s = l.size(); i < s; i++) {
            Rlog.i(LOG_TAG,l.get(i).toString());
        }

        Rlog.i(LOG_TAG,"Foreground call: " + mForegroundCall.toString());

        l = mForegroundCall.getConnections();
        for (int i = 0, s = l.size(); i < s; i++) {
            Rlog.i(LOG_TAG,l.get(i).toString());
        }

        Rlog.i(LOG_TAG,"Background call: " + mBackgroundCall.toString());

        l = mBackgroundCall.getConnections();
        for (int i = 0, s = l.size(); i < s; i++) {
            Rlog.i(LOG_TAG,l.get(i).toString());
        }

        /// M: CC: Use GsmCallTrackerHelper @{
        if (isPhoneTypeGsm()) {
            mHelper.LogState();
        }
        // @}
    }

    @Override
    public void handleMessage(Message msg) {
        AsyncResult ar;

        /// M: CC: Use GsmCallTrackerHelper @{
        mHelper.LogerMessage(msg.what);
        /// @}

        switch (msg.what) {
        /// M: CC: Proprietary incoming call handling @{
        case EVENT_INCOMING_CALL_INDICATION:
            mHelper.CallIndicationProcess((AsyncResult) msg.obj);
            break;
        /// @}
        /// M: CC: Modem reset related handling @{
        case EVENT_RADIO_OFF_OR_NOT_AVAILABLE:
            proprietaryLog("Receives EVENT_RADIO_OFF_OR_NOT_AVAILABLE");
            handlePollCalls(new AsyncResult(null, null,
                    new CommandException(CommandException.Error.RADIO_NOT_AVAILABLE)));
            mLastRelevantPoll = null;
        break;
        /// @}
        case EVENT_DIAL_CALL_RESULT:
            ar = (AsyncResult) msg.obj;
            if (ar.exception != null) {
                proprietaryLog("dial call failed!!");
            }
            operationComplete();
        break;
        /// M: CC: Hangup special handling @{
        case EVENT_SWITCH_RESULT:
            if (isPhoneTypeGsm()) {
                ar = (AsyncResult) msg.obj;
                if (ar.exception != null) {
                    /// M: CC: Proprietary CRSS handling @{
                    if (mWaitForHoldToRedialRequest.isWaitToRedial()) {
                        /* mPendingMO may be reset to null, ex: receive
                             EVENT_RADIO_OFF_OR_NOT_AVAILABLE */
                        if (mPendingMO != null) {
                            mPendingMO.mCause = DisconnectCause.LOCAL;
                            mPendingMO.onDisconnect(DisconnectCause.LOCAL);
                            mPendingMO = null;
                            mHangupPendingMO = false;
                            updatePhoneState();
                        }

                        resumeBackgroundAfterDialFailed();
                        mWaitForHoldToRedialRequest.resetToRedial();
                    }
                    /// @}
                    mPhone.notifySuppServiceFailed(getFailedService(msg.what));
                /// M: CC: Proprietary CRSS handling @{
                } else {
                    if (mWaitForHoldToRedialRequest.isWaitToRedial()) {
                        proprietaryLog("Switch success, then resume dial");
                        mWaitForHoldToRedialRequest.resumeDialAfterHold();
                    }
                }

                if (mWaitForHoldToHangupRequest.isWaitToHangup()) {
                    proprietaryLog("Switch ends, wait for poll call done to hangup");
                    mWaitForHoldToHangupRequest.setHoldDone();
                }

                mHasPendingSwapRequest = false;
                /// @}
                operationComplete();
            }
        break;
        /// M: CC: Hangup special handling @{
        case EVENT_HANG_UP_RESULT:
            operationComplete();
        break;

        case EVENT_POLL_CALLS_RESULT:
            Rlog.d(LOG_TAG, "Event EVENT_POLL_CALLS_RESULT Received");

            if (msg == mLastRelevantPoll) {
                if (DBG_POLL) log(
                        "handle EVENT_POLL_CALL_RESULT: set needsPoll=F");
                mNeedsPoll = false;
                mLastRelevantPoll = null;
                handlePollCalls((AsyncResult)msg.obj);

                /// M: CC: Proprietary CRSS handling @{
                if (mWaitForHoldToHangupRequest.isHoldDone()) {
                    proprietaryLog("Switch ends, and poll call done, then resume hangup");
                    mWaitForHoldToHangupRequest.resumeHangupAfterHold();
                }
                /// @}

                /// M: ALPS03359228 @{
                /* Clear flag when poll call result is handled. */
                bAllCallsDisconnectedButNotHandled = false;
                /// @}
            } else {
                /// M: ALPS03359228 @{
                /* To set flag to indicate that all calls disconnected but not handled in
                     handlePollCalls() for some corner cases */
                CheckIfCallDisconnectButNotHandled((AsyncResult) msg.obj);
                /// @}
            }
            break;

        case EVENT_OPERATION_COMPLETE:
            operationComplete();
            /// M: CC: For 3G VT only @{
            if (hasPendingReplaceRequest) {
                hasPendingReplaceRequest = false;
            }
            /// @}
            break;

        /// M: CC: CDMA plus code & CDMA FDN @{
        case EVENT_EXIT_ECM_RESPONSE_CDMA:
            proprietaryLog("Receives EVENT_EXIT_ECM_RESPONSE_CDMA");
            /// M: CC: Resume dial no matter phone type is changed from C2K to GSM
            if (mPendingCallInEcm) {
                final String dialString = (String) ((AsyncResult) msg.obj).userObj;

                if (mPendingMO == null) {
                    mPendingMO = new GsmCdmaConnection(mPhone,
                            checkForTestEmergencyNumber(dialString),
                            this, mForegroundCall, false/*not ECC*/);
                }

                if (!isPhoneTypeGsm()) {
                    /// M: CDMA FDN @{
                    String tmpStr = mPendingMO.getAddress();
                    tmpStr += "," + PhoneNumberUtils.extractNetworkPortionAlt(dialString);
                    /// @}
                    mCi.dial(tmpStr, mPendingCallClirMode, obtainCompleteMessage());
                    /// M: CC: CDMA process plus code @{
                    if (needToConvert(dialString)) {
                        mPendingMO.setConverted(
                                PhoneNumberUtils.extractNetworkPortionAlt(dialString));
                    }
                    /// @}
                } else {
                    Rlog.e(LOG_TAG, "originally unexpected event " + msg.what +
                            " not handled by phone type " + mPhone.getPhoneType());
                    mCi.dial(mPendingMO.getAddress(), mPendingCallClirMode, null/*uusInfo*/,
                            obtainCompleteMessage());
                }
                mPendingCallInEcm = false;
            }
            mPhone.unsetOnEcbModeExitResponse(this);
            break;

        case EVENT_THREE_WAY_DIAL_BLANK_FLASH:
            proprietaryLog("Receives EVENT_THREE_WAY_DIAL_BLANK_FLASH");
            if (!isPhoneTypeGsm()) {
                ar = (AsyncResult) msg.obj;
                if (ar.exception == null) {
                    /// M: CC: CDMA plus code @{
                    final String dialString = (String) ((AsyncResult) msg.obj).userObj;
                    /// @}
                    postDelayed(
                            new Runnable() {
                                public void run() {
                                    if (mPendingMO != null) {
                                        /// M: CDMA FDN @{
                                        String tmpStr = mPendingMO.getAddress();
                                        tmpStr += "," +
                                                PhoneNumberUtils.extractNetworkPortionAlt(
                                                dialString);
                                        /// @}
                                        mCi.sendCDMAFeatureCode(tmpStr,
                                                obtainMessage(EVENT_THREE_WAY_DIAL_L2_RESULT_CDMA));
                                        /// M: CC: CDMA plus code @{
                                        if (needToConvert(dialString)) {
                                            mPendingMO.setConverted(
                                                    PhoneNumberUtils.extractNetworkPortionAlt(
                                                            dialString));
                                        }
                                        /// @}
                                    }
                                }
                            }, m3WayCallFlashDelay);
                } else {
                    mPendingMO = null;
                    Rlog.w(LOG_TAG, "exception happened on Blank Flash for 3-way call");
                }
            } else {
                throw new RuntimeException("unexpected event " + msg.what + " not handled by " +
                        "phone type " + mPhone.getPhoneType());
            }
            break;
        /// @}

        /// M: CC: CDMA call accepted @{
        case EVENT_CDMA_CALL_ACCEPTED:
            proprietaryLog("Receives EVENT_CDMA_CALL_ACCEPTED");
            ar = (AsyncResult) msg.obj;
            if (ar.exception == null) {
                handleCallAccepted();
            }
            break;
        /// @}
        // For IMS conference SRVCC
        case EVENT_ECONF_SRVCC_INDICATION:
            log("Receives EVENT_ECONF_SRVCC_INDICATION");
            if (!hasParsingCEPCapability()) {
                ar = (AsyncResult) msg.obj;
                mEconfSrvccConnectionIds = (int[]) ar.result;

                // Mark ECONFSRVCC is received, then do poll calls now.
                mNeedWaitImsEConfSrvcc = false;
                pollCallsWhenSafe();
            }
            break;
        case EVENT_RADIO_ON:
            log("Receives EVENT_RADIO_ON");
            mMtkCi.setEccList();
            break;

        /// M: Timing issue, mPendingMO could be reset to null in handlePollCalls @{
        case EVENT_THREE_WAY_DIAL_L2_RESULT_CDMA:
            if (!isPhoneTypeGsm()) {
                ar = (AsyncResult)msg.obj;
                if (ar.exception == null && mPendingMO != null) {
                    // Assume 3 way call is connected
                    mPendingMO.onConnectedInOrOut();
                    mPendingMO = null;
                }
            } else {
                super.handleMessage(msg);
            }
            break;
        // @}

        default:
            super.handleMessage(msg);
        }
    }

    /// M: CC: HangupAll for FTA 31.4.4.2 @{
    /**
     * used to release all connections in the MS,
     * release all connections with one reqeust together, not seperated.
     * @throws CallStateException if the callState is unexpected.
     */
    public void hangupAll() throws CallStateException {
        if (Phone.DEBUG_PHONE) proprietaryLog("hangupAll");
        mMtkCi.hangupAll(obtainCompleteMessage());

        if (!mRingingCall.isIdle()) {
            mRingingCall.onHangupLocal();
        }
        if (!mForegroundCall.isIdle()) {
            mForegroundCall.onHangupLocal();
        }
        if (!mBackgroundCall.isIdle()) {
            mBackgroundCall.onHangupLocal();
        }
    }
    /// @}

    public void hangup(GsmCdmaConnection conn) throws CallStateException {
        if (conn.mOwner != this) {
            throw new CallStateException ("GsmCdmaConnection " + conn
                                    + "does not belong to GsmCdmaCallTracker " + this);
        }

        if (conn == mPendingMO) {
            // We're hanging up an outgoing call that doesn't have it's
            // GsmCdma index assigned yet

            if (Phone.DEBUG_PHONE) log("hangup: set hangupPendingMO to true");
            mHangupPendingMO = true;
        } else if (!isPhoneTypeGsm()
                && conn.getCall() == mRingingCall
                && mRingingCall.getState() == GsmCdmaCall.State.WAITING) {
            // Handle call waiting hang up case.
            //
            // The ringingCall state will change to IDLE in GsmCdmaCall.detach
            // if the ringing call connection size is 0. We don't specifically
            // set the ringing call state to IDLE here to avoid a race condition
            // where a new call waiting could get a hang up from an old call
            // waiting ringingCall.
            //
            // PhoneApp does the call log itself since only PhoneApp knows
            // the hangup reason is user ignoring or timing out. So conn.onDisconnect()
            // is not called here. Instead, conn.onLocalDisconnect() is called.
            conn.onLocalDisconnect();

            updatePhoneState();
            mPhone.notifyPreciseCallStateChanged();
            return;
        } else {
            try {
                mMetrics.writeRilHangup(mPhone.getPhoneId(), conn, conn.getGsmCdmaIndex());
                /// M: CC: Hangup special handling @{
                //mCi.hangupConnection (conn.getGsmCdmaIndex(), obtainCompleteMessage());
                mCi.hangupConnection(conn.getGsmCdmaIndex(),
                        obtainCompleteMessage(EVENT_HANG_UP_RESULT));
                /// @}
            } catch (CallStateException ex) {
                // Ignore "connection not found"
                // Call may have hung up already
                Rlog.w(LOG_TAG,"GsmCdmaCallTracker WARN: hangup() on absent connection "
                                + conn);
            }
        }

        conn.onHangupLocal();
    }

    //***** Called from GsmCdmaCall

    public void hangup(GsmCdmaCall call) throws CallStateException {
        if (call.getConnections().size() == 0) {
            throw new CallStateException("no connections in call");
        }

        if (call == mRingingCall) {
            if (Phone.DEBUG_PHONE) log("(ringing) hangup waiting or background");
            logHangupEvent(call);
            /// M: CC: MD not handling AOSP design (CHLD=0), need to specify connection id. @{
            // [ALPS00303482][GCF][51.010-1][26.8.1.3.5.3]
            //mCi.hangupWaitingOrBackground(obtainCompleteMessage());
            hangup((GsmCdmaConnection) (call.getConnections().get(0)));
            /// @}
        } else if (call == mForegroundCall) {
            if (call.isDialingOrAlerting()) {
                if (Phone.DEBUG_PHONE) {
                    log("(foregnd) hangup dialing or alerting...");
                }
                hangup((GsmCdmaConnection)(call.getConnections().get(0)));
            /*
            /// M: CC: Use 1+SEND MMI to release active calls & accept held or waiting call @{
            // [ALPS02087255] [Call]MMI 1 cannot accept the waiting call.
            // 3GPP 22.030 6.5.5
            // "Releases all active calls (if any exist) and accepts
            //  the other (held or waiting) call."
            } else if (isPhoneTypeGsm()
                    && mRingingCall.isRinging()) {
                // Do not auto-answer ringing on CHUP, instead just end active calls
                log("hangup all conns in active/background call, without affecting ringing call");
                hangupAllConnections(call);
            */
            } else {
                logHangupEvent(call);
                /// M: CC: Can not end the ECC call when enable SIM PIN lock
                //[ALPS01431282][ALPS.KK1.MP2.V2.4 Regression Test]
                //hangupForegroundResumeBackground();
                if (Phone.DEBUG_PHONE) log("(foregnd) hangup active");
                if (isPhoneTypeGsm()) {
                    GsmCdmaConnection cn = (GsmCdmaConnection) call.getConnections().get(0);
                    String address = cn.getAddress();

                    // M: CC: For 93, MD can switch phoneType when SIM not inserted,
                    // TeleService won't trigger phone switch, so check both SIM's ECC
                    boolean isEmergencyCall = false;

                    if (hasC2kOverImsModem() &&
                            !TelephonyManager.getDefault().hasIccCard(mPhone.getPhoneId())) {
                        isEmergencyCall = PhoneNumberUtils.isLocalEmergencyNumber(
                                mPhone.getContext(), address);
                    } else {
                        isEmergencyCall = PhoneNumberUtils.isLocalEmergencyNumber(
                                mPhone.getContext(), mPhone.getSubId(), address);
                    }

                    if (isEmergencyCall && !MtkPhoneNumberUtils.isSpecialEmergencyNumber(
                            mPhone.getSubId(), address)) {
                        proprietaryLog("(foregnd) hangup active ECC call by connection index");
                        hangup((GsmCdmaConnection) (call.getConnections().get(0)));
                    } else {
                        /// M: CC: Proprietary CRSS handling @{
                        //hangupForegroundResumeBackground();
                        if (!mWaitForHoldToHangupRequest.isWaitToHangup()) {
                            hangupForegroundResumeBackground();
                        } else {
                            mWaitForHoldToHangupRequest.setToHangup(call);
                        }
                        /// @}
                    }
                } else {
                    hangupForegroundResumeBackground();
                }
            }
        } else if (call == mBackgroundCall) {
            if (mRingingCall.isRinging()) {
                if (Phone.DEBUG_PHONE) {
                    log("hangup all conns in background call");
                }
                hangupAllConnections(call);
            } else {
                if (Phone.DEBUG_PHONE) log("(backgnd) hangup waiting/background");
                /// M: CC: Proprietary CRSS handling @{
                //hangupWaitingOrBackground();
                if (!mWaitForHoldToHangupRequest.isWaitToHangup()) {
                    hangupWaitingOrBackground();
                } else {
                    mWaitForHoldToHangupRequest.setToHangup(call);
                }
                /// @}
            }
        } else {
            throw new RuntimeException ("GsmCdmaCall " + call +
                    "does not belong to GsmCdmaCallTracker " + this);
        }

        call.onHangupLocal();
        mPhone.notifyPreciseCallStateChanged();
    }

    public void hangupWaitingOrBackground() {
        if (Phone.DEBUG_PHONE) log("hangupWaitingOrBackground");
        logHangupEvent(mBackgroundCall);
        /// M: CC: Hangup special handling @{
        //mCi.hangupWaitingOrBackground(obtainCompleteMessage());
        mCi.hangupWaitingOrBackground(obtainCompleteMessage(EVENT_HANG_UP_RESULT));
        /// @}
    }

    public void hangupForegroundResumeBackground() {
        if (Phone.DEBUG_PHONE) log("hangupForegroundResumeBackground");
        /// M: CC: Hangup special handling @{
        //mCi.hangupForegroundResumeBackground(obtainCompleteMessage());
        mCi.hangupForegroundResumeBackground(obtainCompleteMessage(EVENT_HANG_UP_RESULT));
        /// @}
    }

    public void hangupConnectionByIndex(GsmCdmaCall call, int index)
            throws CallStateException {
        int count = call.mConnections.size();
        for (int i = 0; i < count; i++) {
            GsmCdmaConnection cn = (GsmCdmaConnection)call.mConnections.get(i);
            if (!cn.mDisconnected && cn.getGsmCdmaIndex() == index) {
                mMetrics.writeRilHangup(mPhone.getPhoneId(), cn, cn.getGsmCdmaIndex());
                mCi.hangupConnection(index, obtainCompleteMessage());
                return;
            }
        }

        throw new CallStateException("no GsmCdma index found");
    }

    /// M: ALPS03359228   @{
    /* To set flag to indicate that all calls disconnected but not handled in
         handlePollCalls() for some corner cases */
    private void CheckIfCallDisconnectButNotHandled(AsyncResult ar) {
        List polledCalls;
        boolean bCallExist = false;

        if (ar.exception == null) {
            polledCalls = (List) ar.result;
        } else {
            // just a dummy empty ArrayList to cause the loop
            // to hang up all the calls
            polledCalls = new ArrayList();
        }

        for (int i = 0;  i < mConnections.length; i++) {
            GsmCdmaConnection conn = mConnections[i];
            if (conn != null) {
                bCallExist = true;
                break;
            }
        }

        bAllCallsDisconnectedButNotHandled = (bCallExist && (polledCalls.size() == 0));
    }
    /// @}

    void proprietaryLog(String s) {
        Rlog.d(PROP_LOG_TAG, s);
    }

    //GSM
    /**
     * clirMode is one of the CLIR_ constants
     */
    public synchronized Connection dial(String dialString, int clirMode, UUSInfo uusInfo,
                                        Bundle intentExtras)
            throws CallStateException {
        // note that this triggers call state changed notif
        clearDisconnected();

        if (!canDial()) {
            throw new CallStateException("cannot dial in current state");
        }

        String origNumber = dialString;
        dialString = convertNumberIfNecessary(mPhone, dialString);

        // The new call must be assigned to the foreground call.
        // That call must be idle, so place anything that's
        // there on hold
        if (mForegroundCall.getState() == GsmCdmaCall.State.ACTIVE) {
            // this will probably be done by the radio anyway
            // but the dial might fail before this happens
            // and we need to make sure the foreground call is clear
            // for the newly dialed connection

            /// M: CC: Proprietary CRSS handling @{
            mWaitForHoldToRedialRequest.setToRedial();
            /// @}

            switchWaitingOrHoldingAndActive();
            // This is a hack to delay DIAL so that it is sent out to RIL only after
            // EVENT_SWITCH_RESULT is received. We've seen failures when adding a new call to
            // multi-way conference calls due to DIAL being sent out before SWITCH is processed
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                // do nothing
            }

            // Fake local state so that
            // a) foregroundCall is empty for the newly dialed connection
            // b) hasNonHangupStateChanged remains false in the
            // next poll, so that we don't clear a failed dialing call
            fakeHoldForegroundBeforeDial();
        }

        if (mForegroundCall.getState() != GsmCdmaCall.State.IDLE) {
            //we should have failed in !canDial() above before we get here
            throw new CallStateException("cannot dial in current state");
        }

        // M: CC: For 93, MD can switch phoneType when SIM not inserted,
        // TeleService won't trigger phone switch, so check both SIM's ECC
        //boolean isEmergencyCall = PhoneNumberUtils.isLocalEmergencyNumber(mPhone.getContext(),
        //        dialString);
        boolean isEmergencyCall = false;
        boolean isEcc = false;

        if (hasC2kOverImsModem() &&
                !TelephonyManager.getDefault().hasIccCard(mPhone.getPhoneId())) {
            isEmergencyCall = PhoneNumberUtils.isLocalEmergencyNumber(
                    mPhone.getContext(), dialString);
            isEcc = PhoneNumberUtils.isEmergencyNumber(dialString);
        } else {
            isEmergencyCall = PhoneNumberUtils.isLocalEmergencyNumber(
                    mPhone.getContext(), mPhone.getSubId(), dialString);
            isEcc = PhoneNumberUtils.isEmergencyNumber(mPhone.getSubId(), dialString);
        }

        mPendingMO = new MtkGsmCdmaConnection(mPhone, checkForTestEmergencyNumber(dialString),
                this, mForegroundCall, isEmergencyCall);
        mHangupPendingMO = false;
        mMetrics.writeRilDial(mPhone.getPhoneId(), mPendingMO, clirMode, uusInfo);

        /// M: CC: Reconstruct dialString based on extras
        String newDialString = mMtkGsmCdmaCallTrackerExt.convertDialString(
            intentExtras, mPendingMO.getAddress());

        if ( mPendingMO.getAddress() == null || mPendingMO.getAddress().length() == 0
                || mPendingMO.getAddress().indexOf(PhoneNumberUtils.WILD) >= 0) {
            // Phone number is invalid
            mPendingMO.mCause = DisconnectCause.INVALID_NUMBER;

            /// M: CC: Proprietary CRSS handling @{
            mWaitForHoldToRedialRequest.resetToRedial();
            /// @}

            // handlePollCalls() will notice this call not present
            // and will mark it as dropped.
            pollCallsWhenSafe();
        } else {
            // Always unmute when initiating a new call
            setMute(false);

            /// M: CC: Proprietary CRSS handling @{
            if (!mWaitForHoldToRedialRequest.isWaitToRedial()) {
                /// M: CC: Proprietary ECC handling@{
                if (isEcc && !MtkPhoneNumberUtils.isSpecialEmergencyNumber(
                        mPhone.getSubId(), dialString)) {
                    int serviceCategory = MtkPhoneNumberUtils.getServiceCategoryFromEccBySubId(
                            dialString, mPhone.getSubId());
                    mMtkCi.setEccServiceCategory(serviceCategory, null);
                    mMtkCi.emergencyDial(mPendingMO.getAddress(), clirMode, uusInfo,
                            obtainCompleteMessage(EVENT_DIAL_CALL_RESULT));
                /// @}
                } else {
                    /// M: CC: If dialString is reconstructed, need to mark it as converted
                    if (newDialString != null) {
                        mNumberConverted = true;
                    } else {
                        newDialString = mPendingMO.getAddress();
                    }
                    mCi.dial(newDialString, clirMode, uusInfo, obtainCompleteMessage());
                }
            } else {
                /// M: CC: If dialString is reconstructed, need to mark it as converted
                if (newDialString != null) {
                    mNumberConverted = true;
                } else {
                    newDialString = mPendingMO.getAddress();
                }
                mWaitForHoldToRedialRequest.setToRedial(newDialString, clirMode, uusInfo);
            }
            /// @}
        }

        if (mNumberConverted) {
            mPendingMO.setConverted(origNumber);
            mNumberConverted = false;
        }

        updatePhoneState();
        mPhone.notifyPreciseCallStateChanged();

        return mPendingMO;
    }

    public void switchWaitingOrHoldingAndActive() throws CallStateException {
        // Should we bother with this check?
        if (mRingingCall.getState() == GsmCdmaCall.State.INCOMING) {
            throw new CallStateException("cannot be in the incoming state");
        } else {
            if (isPhoneTypeGsm()) {
                /// M: CC: Proprietary CRSS handling @{
                //mCi.switchWaitingOrHoldingAndActive(
                //        obtainCompleteMessage(EVENT_SWITCH_RESULT));
                if (!mHasPendingSwapRequest) {
                    mWaitForHoldToHangupRequest.setToHangup();
                    mCi.switchWaitingOrHoldingAndActive(
                            obtainCompleteMessage(EVENT_SWITCH_RESULT));
                    mHasPendingSwapRequest = true;
                }
                /// @}
            } else {
                if (mForegroundCall.getConnections().size() > 1) {
                    flashAndSetGenericTrue();
                } else {
                    // Send a flash command to CDMA network for putting the other party on hold.
                    // For CDMA networks which do not support this the user would just hear a beep
                    // from the network. For CDMA networks which do support it will put the other
                    // party on hold.
                    mCi.sendCDMAFeatureCode("", obtainMessage(EVENT_SWITCH_RESULT));
                }
            }
        }
    }

    /// M: CC: For 3G VT only @{
    private boolean canVtDial() {
        boolean ret;
        int networkType = mPhone.getServiceState().getVoiceNetworkType();
        proprietaryLog("networkType=" + TelephonyManager.getNetworkTypeName(networkType));

        ret = (networkType == TelephonyManager.NETWORK_TYPE_UMTS ||
                networkType == TelephonyManager.NETWORK_TYPE_HSDPA ||
                networkType == TelephonyManager.NETWORK_TYPE_HSUPA ||
                networkType == TelephonyManager.NETWORK_TYPE_HSPA ||
                networkType == TelephonyManager.NETWORK_TYPE_HSPAP ||
                networkType == TelephonyManager.NETWORK_TYPE_LTE);

        return ret;
    }

    /**
     * clirMode is one of the CLIR_ constants
     */
    public synchronized Connection vtDial(String dialString, int clirMode, UUSInfo uusInfo,
                                        Bundle intentExtras)
            throws CallStateException {
        // note that this triggers call state changed notif
        clearDisconnected();

        if (!canDial()) {
            throw new CallStateException("cannot dial in current state");
        }

        if (!canVtDial()) {
            throw new CallStateException("cannot vtDial under non 3/4G network");
        }

        String origNumber = dialString;
        dialString = convertNumberIfNecessary(mPhone, dialString);

        // Only one call can exist for 3G VT.
        // We assume CallsManager will block adding call if a 3G VT call has already existed.
        if (mForegroundCall.getState() != GsmCdmaCall.State.IDLE ||
            mRingingCall.getState().isRinging()) {
            //we should have failed in !canDial() above before we get here
            throw new CallStateException("cannot vtDial since non-IDLE call already exists");
        }

        // M: CC: For 93, MD can switch phoneType when SIM not inserted,
        // TeleService won't trigger phone switch, so check both SIM's ECC
        //boolean isEmergencyCall = PhoneNumberUtils.isLocalEmergencyNumber(mPhone.getContext(),
        //        dialString);
        boolean isEmergencyCall = false;

        if (hasC2kOverImsModem() &&
                !TelephonyManager.getDefault().hasIccCard(mPhone.getPhoneId())) {
            isEmergencyCall = PhoneNumberUtils.isLocalEmergencyNumber(
                    mPhone.getContext(), dialString);
        } else {
            isEmergencyCall = PhoneNumberUtils.isLocalEmergencyNumber(
                    mPhone.getContext(), mPhone.getSubId(), dialString);
        }

        mPendingMO = new MtkGsmCdmaConnection(mPhone, checkForTestEmergencyNumber(dialString),
                this, mForegroundCall, isEmergencyCall);
        mHangupPendingMO = false;
        ((MtkGsmCdmaConnection)mPendingMO).mIsVideo = true; //for 3G VT

        if ( mPendingMO.getAddress() == null || mPendingMO.getAddress().length() == 0
                || mPendingMO.getAddress().indexOf(PhoneNumberUtils.WILD) >= 0) {
            // Phone number is invalid
            mPendingMO.mCause = DisconnectCause.INVALID_NUMBER;

            // handlePollCalls() will notice this call not present
            // and will mark it as dropped.
            pollCallsWhenSafe();
        } else {
            // Always unmute when initiating a new call
            setMute(false);

            mMtkCi.vtDial(mPendingMO.getAddress(), clirMode, uusInfo,
                    obtainCompleteMessage());

            mPendingMO.setVideoState(VideoProfile.STATE_BIDIRECTIONAL);

            //MO:new VT service
             GsmVTProviderUtil.setContext(mPhone.getContext());
            ((MtkGsmCdmaCall)mForegroundCall).mVTProvider = new GsmVTProvider();
            proprietaryLog("vtDial new GsmVTProvider");
            try {
                IMtkGsmVideoCallProvider gsmVideoCallProvider =
                        ((MtkGsmCdmaCall)mForegroundCall).mVTProvider.getInterface();
                if (gsmVideoCallProvider != null) {
                    MtkGsmVideoCallProviderWrapper gsmVideoCallProviderWrapper =
                            new MtkGsmVideoCallProviderWrapper(gsmVideoCallProvider);
                    proprietaryLog("vtDial new GsmVideoCallProviderWrapper");
                    mPendingMO.setVideoProvider(gsmVideoCallProviderWrapper);
                }
            } catch (RemoteException e) {
                Rlog.e(PROP_LOG_TAG, "vtDial new GsmVideoCallProviderWrapper failed");
            }


        }

        if (mNumberConverted) {
            mPendingMO.setConverted(origNumber);
            mNumberConverted = false;
        }

        updatePhoneState();
        mPhone.notifyPreciseCallStateChanged();

        return mPendingMO;
    }

    public Connection vtDial(String dialString, UUSInfo uusInfo, Bundle intentExtras)
            throws CallStateException {
        return vtDial(dialString, MtkRIL.CLIR_DEFAULT, uusInfo, intentExtras);
    }

    public void acceptCall(int videoState) throws CallStateException {
        // FIXME if SWITCH fails, should retry with ANSWER
        // in case the active/holding call disappeared and this
        // is no longer call waiting

        if (mRingingCall.getState() == GsmCdmaCall.State.INCOMING) {
            Rlog.i("phone", "acceptCall: incoming...");
            // Always unmute when answering a new call
            setMute(false);
            /// M: For both VT and ViLTE @{
            MtkGsmCdmaConnection cn = (MtkGsmCdmaConnection) mRingingCall.mConnections.get(0);
            if (cn.isVideo()) {
                if (videoState == VideoProfile.STATE_AUDIO_ONLY) {
                    mMtkCi.acceptVtCallWithVoiceOnly(cn.getGsmCdmaIndex(), obtainCompleteMessage());
                    cn.setVideoState(VideoProfile.STATE_AUDIO_ONLY);
                    return;
                }
            }
            /// @}
            mCi.acceptCall(obtainCompleteMessage());
        } else if (mRingingCall.getState() == GsmCdmaCall.State.WAITING) {
            if (isPhoneTypeGsm()) {
                setMute(false);
                /// M: For both VT and ViLTE @{
                MtkGsmCdmaConnection cn = (MtkGsmCdmaConnection) mRingingCall.mConnections.get(0);
                if (cn.isVideo()) {
                    MtkGsmCdmaConnection fgCn =
                            (MtkGsmCdmaConnection) mForegroundCall.mConnections.get(0);
                    if (fgCn != null && fgCn.isVideo()) {
                        hasPendingReplaceRequest = true;
                        mMtkCi.replaceVtCall(fgCn.mIndex + 1, obtainCompleteMessage());
                        fgCn.onHangupLocal();
                        return;
                    }
                }
                /// @}
            } else {
                MtkGsmCdmaConnection cwConn = (MtkGsmCdmaConnection) (mRingingCall
                        .getLatestConnection());

                // Since there is no network response for supplimentary
                // service for CDMA, we assume call waiting is answered.
                // ringing Call state change to idle is in GsmCdmaCall.detach
                // triggered by updateParent.
                cwConn.updateParent(mRingingCall, mForegroundCall);
                cwConn.onConnectedInOrOut();
                updatePhoneState();
            }
            switchWaitingOrHoldingAndActive();
        } else {
            throw new CallStateException("phone not ringing");
        }
    }
    /// @}

    /// M: CC: Proprietary CRSS handling @{
    private void resumeBackgroundAfterDialFailed() {
        // We need to make a copy here, since fakeHoldBeforeDial()
        // modifies the lists, and we don't want to reverse the order
        List<Connection> connCopy = (List<Connection>) mBackgroundCall.mConnections.clone();

        for (int i = 0, s = connCopy.size() ; i < s ; i++) {
            MtkGsmCdmaConnection conn = (MtkGsmCdmaConnection) connCopy.get(i);

            conn.resumeHoldAfterDialFailed();
        }
    }
    /// @}

    @Override
    protected void disableDataCallInEmergencyCall(String dialString) {
        // M: CC: For 93, MD can switch phoneType when SIM not inserted,
        // TeleService won't trigger phone switch, so check both SIM's ECC
        //if (PhoneNumberUtils.isLocalEmergencyNumber(mPhone.getContext(), dialString)) {
        boolean isEmergencyCall = false;

        if (hasC2kOverImsModem() &&
                !TelephonyManager.getDefault().hasIccCard(mPhone.getPhoneId())) {
            isEmergencyCall = PhoneNumberUtils.isLocalEmergencyNumber(
                    mPhone.getContext(), dialString);
        } else {
            isEmergencyCall = PhoneNumberUtils.isLocalEmergencyNumber(
                    mPhone.getContext(), mPhone.getSubId(), dialString);
        }


        if (isEmergencyCall) {
            if (Phone.DEBUG_PHONE) log("disableDataCallInEmergencyCall");
            setIsInEmergencyCall();
        }
    }

    // CDMA
    @Override
    protected Connection dial(String dialString, int clirMode) throws CallStateException {
        // note that this triggers call state changed notif
        clearDisconnected();

        if (!canDial()) {
            throw new CallStateException("cannot dial in current state");
        }

        TelephonyManager tm =
                (TelephonyManager) mPhone.getContext().getSystemService(Context.TELEPHONY_SERVICE);
        String origNumber = dialString;
        String operatorIsoContry = tm.getNetworkCountryIsoForPhone(mPhone.getPhoneId());
        String simIsoContry = tm.getSimCountryIsoForPhone(mPhone.getPhoneId());
        boolean internationalRoaming = !TextUtils.isEmpty(operatorIsoContry)
                && !TextUtils.isEmpty(simIsoContry)
                && !simIsoContry.equals(operatorIsoContry);
        if (internationalRoaming) {
            if ("us".equals(simIsoContry)) {
                internationalRoaming = internationalRoaming && !"vi".equals(operatorIsoContry);
            } else if ("vi".equals(simIsoContry)) {
                internationalRoaming = internationalRoaming && !"us".equals(operatorIsoContry);
            }
        }
        if (internationalRoaming) {
            dialString = convertNumberIfNecessary(mPhone, dialString);
        }

        /// M: CC: Get the property by phoneId @{
        //String inEcm = SystemProperties.get(TelephonyProperties.PROPERTY_INECM_MODE, "false");
        String inEcm = TelephonyManager.getTelephonyProperty(
                mPhone.getPhoneId(), TelephonyProperties.PROPERTY_INECM_MODE, "false");
        /// @}
        boolean isPhoneInEcmMode = inEcm.equals("true");


        // M: CC: For 93, MD can switch phoneType when SIM not inserted,
        // TeleService won't trigger phone switch, so check both SIM's ECC
        //boolean isEmergencyCall =
        //        PhoneNumberUtils.isLocalEmergencyNumber(mPhone.getContext(), dialString);
        boolean isEmergencyCall = false;
        boolean isEcc = false;

        if (hasC2kOverImsModem() &&
                !TelephonyManager.getDefault().hasIccCard(mPhone.getPhoneId())) {
            isEmergencyCall = PhoneNumberUtils.isLocalEmergencyNumber(
                    mPhone.getContext(), dialString);
            isEcc = PhoneNumberUtils.isEmergencyNumber(dialString);
        } else {
            isEmergencyCall = PhoneNumberUtils.isLocalEmergencyNumber(
                    mPhone.getContext(), mPhone.getSubId(), dialString);
            isEcc = PhoneNumberUtils.isEmergencyNumber(mPhone.getSubId(), dialString);
        }

        // Cancel Ecm timer if a second emergency call is originating in Ecm mode
        if (isPhoneInEcmMode && isEmergencyCall) {
            handleEcmTimer(GsmCdmaPhone.CANCEL_ECM_TIMER);
        }

        // The new call must be assigned to the foreground call.
        // That call must be idle, so place anything that's
        // there on hold
        if (mForegroundCall.getState() == GsmCdmaCall.State.ACTIVE) {
            return dialThreeWay(dialString);
        }

        mPendingMO = new MtkGsmCdmaConnection(mPhone, checkForTestEmergencyNumber(dialString),
                this, mForegroundCall, isEmergencyCall);
        mHangupPendingMO = false;

        if (mPendingMO.getAddress() == null || mPendingMO.getAddress().length() == 0
                || mPendingMO.getAddress().indexOf(PhoneNumberUtils.WILD) >= 0 ) {
            // Phone number is invalid
            mPendingMO.mCause = DisconnectCause.INVALID_NUMBER;

            // handlePollCalls() will notice this call not present
            // and will mark it as dropped.
            pollCallsWhenSafe();
        } else {
            // Always unmute when initiating a new call
            setMute(false);

            // Check data call
            disableDataCallInEmergencyCall(dialString);

            // In Ecm mode, if another emergency call is dialed, Ecm mode will not exit.
            if(!isPhoneInEcmMode || (isPhoneInEcmMode && isEmergencyCall)) {
                /// M: CC: Proprietary ECC handling @{
                //mCi.dial(mPendingMO.getAddress(), clirMode, obtainCompleteMessage());
                if (isEcc) {
                    mMtkCi.emergencyDial(mPendingMO.getAddress(), clirMode, null,
                            obtainCompleteMessage());
                } else {
                    /// M: CDMA FDN @{
                    String tmpStr = mPendingMO.getAddress();
                    tmpStr += "," + PhoneNumberUtils.extractNetworkPortionAlt(dialString);
                    /// @}
                    mCi.dial(tmpStr, clirMode, obtainCompleteMessage());
                }
                /// @}
                /// M: CC: CDMA plus code @{
                if (needToConvert(dialString)) {
                    mPendingMO.setConverted(PhoneNumberUtils.extractNetworkPortionAlt(dialString));
                }
                /// @}
            } else {
                mPhone.exitEmergencyCallbackMode();
                /// M: CC: CDMA plus code @{
                mPhone.setOnEcbModeExitResponse(this, EVENT_EXIT_ECM_RESPONSE_CDMA, dialString);
                /// @}
                mPendingCallClirMode = clirMode;
                mPendingCallInEcm = true;
            }
        }

        if (mNumberConverted) {
            mPendingMO.setConverted(origNumber);
            mNumberConverted = false;
        }

        updatePhoneState();
        mPhone.notifyPreciseCallStateChanged();

        return mPendingMO;
    }

    @Override
    protected Connection dialThreeWay(String dialString) {
        if (!mForegroundCall.isIdle()) {
            // Check data call and possibly set mIsInEmergencyCall
            disableDataCallInEmergencyCall(dialString);

            // Attach the new connection to foregroundCall
            mPendingMO = new MtkGsmCdmaConnection(mPhone,
                    checkForTestEmergencyNumber(dialString), this, mForegroundCall,
                    mIsInEmergencyCall);
            // Some networks need an empty flash before sending the normal one
            CarrierConfigManager configManager = (CarrierConfigManager)
                    mPhone.getContext().getSystemService(Context.CARRIER_CONFIG_SERVICE);
            PersistableBundle bundle = configManager.getConfig();
            if (bundle != null) {
                m3WayCallFlashDelay =
                        bundle.getInt(CarrierConfigManager.KEY_CDMA_3WAYCALL_FLASH_DELAY_INT);
            } else {
                // The default 3-way call flash delay is 0s
                m3WayCallFlashDelay = 0;
            }
            if (m3WayCallFlashDelay > 0) {
                /// M: CC: CDMA plus code @{
                mCi.sendCDMAFeatureCode("", obtainMessage(EVENT_THREE_WAY_DIAL_BLANK_FLASH,
                        dialString));
                /// @}
            } else {
                /// M: CDMA FDN @{
                String tmpStr = mPendingMO.getAddress();
                tmpStr += "," + PhoneNumberUtils.extractNetworkPortionAlt(dialString);
                /// @}
                mCi.sendCDMAFeatureCode(tmpStr,
                        obtainMessage(EVENT_THREE_WAY_DIAL_L2_RESULT_CDMA));
                /// M: CC: CDMA plus code @{
                if (needToConvert(dialString)) {
                    mPendingMO.setConverted(PhoneNumberUtils.extractNetworkPortionAlt(dialString));
                }
                /// @}
            }
            return mPendingMO;
        }
        return null;
    }

    @Override
    protected void handleCallWaitingInfo(CdmaCallWaitingNotification cw) {
        /// M: CC: CDMA plus code @{
        processPlusCodeForWaitingCall(cw);
        /// @}

        /// M: CC: CDMA waiting call @{
        if (!shouldNotifyWaitingCall(cw)) {
            return;
        }
        /// @}

        // Create a new GsmCdmaConnection which attaches itself to ringingCall.
        new MtkGsmCdmaConnection(mPhone.getContext(), cw, this, mRingingCall);
        updatePhoneState();

        // Finally notify application
        notifyCallWaitingInfo(cw);
    }

    /// M: CC: CDMA call accepted @{
    private void handleCallAccepted() {
        List connections = mForegroundCall.getConnections();
        int count = connections.size();
        proprietaryLog("handleCallAccepted, fgcall count=" + count);
        if (count == 1) {
            GsmCdmaConnection c = (GsmCdmaConnection) connections.get(0);
            if ((c instanceof MtkGsmCdmaConnection)
                    && (mPhone instanceof MtkGsmCdmaPhone)) {
                if (((MtkGsmCdmaConnection) c).onCdmaCallAccepted()) {
                    ((MtkGsmCdmaPhone) mPhone).notifyCdmaCallAccepted();
                }
            }
        }
    }
    /// @}

    /// M: CC: CDMA plus code @{
    private String processPlusCodeForDriverCall(String number, boolean isMt, int typeOfAddress) {
        if (isMt && typeOfAddress == PhoneNumberUtils.TOA_International) {
            if (number != null && number.length() > 0 && number.charAt(0) == '+') {
                number = number.substring(1, number.length());
            }
            number = PlusCodeProcessor.getPlusCodeUtils().removeIddNddAddPlusCode(number);
        }
        number = PhoneNumberUtils.stringFromStringAndTOA(number, typeOfAddress);
        return number;
    }

    private void processPlusCodeForWaitingCall(CdmaCallWaitingNotification cw) {
        String address = cw.number;
        // Make sure there's a leading + on addresses with a TOA of 145
        if (address != null && address.length() > 0) {
            cw.number = processPlusCodeForWaitingCall(address, cw.numberType);
        }
    }

    private String processPlusCodeForWaitingCall(String number, int numberType) {
        String format = PlusCodeProcessor.getPlusCodeUtils().removeIddNddAddPlusCode(number);
        if (format != null) {
            number = format;
            if (numberType == 1 && format.length() > 0 && format.charAt(0) != '+') {
                number = "+" + format;
            }
        }
        return number;
    }

    private boolean needToConvert(String source) {
        String target = GsmCdmaConnection.formatDialString(source);
        return source != null && target != null && !source.equals(target);
    }
    /// @}

    /// M: CC: CDMA waiting call @{
    private boolean shouldNotifyWaitingCall(CdmaCallWaitingNotification cw) {
        String address = cw.number;
        proprietaryLog("shouldNotifyWaitingCall, address=" + address);
        if (address != null && address.length() > 0) {
            GsmCdmaConnection lastRingConn
                    = (GsmCdmaConnection) (mRingingCall.getLatestConnection());
            if (lastRingConn != null) {
                if (address.equals(lastRingConn.getAddress())) {
                    proprietaryLog("handleCallWaitingInfo, skip duplicate waiting call!");
                    return false;
                }
            }
        }
        return true;
    }
    /// @}

    // IMS conference SRVCC
    @Override
    protected void updatePhoneState() {
        PhoneConstants.State oldState = mState;
        if (mRingingCall.isRinging()) {
            mState = PhoneConstants.State.RINGING;
        } else if (mPendingMO != null ||
                !(mForegroundCall.isIdle() && mBackgroundCall.isIdle())) {
            mState = PhoneConstants.State.OFFHOOK;
        } else {
            Phone imsPhone = mPhone.getImsPhone();
            // ALPS02192901
            // If the call is disconnected after CIREPH=1, before +CLCC, the original state is
            // idle and new state is still idle, so callEndCleanupHandOverCallIfAny isn't called.
            // Related CR: ALPS02015368, ALPS02161020, ALPS02192901.
            // if ( mState == PhoneConstants.State.OFFHOOK && (imsPhone != null)){
            if (imsPhone != null) {
                imsPhone.callEndCleanupHandOverCallIfAny();
            }
            mState = PhoneConstants.State.IDLE;
        }

        if (mState == PhoneConstants.State.IDLE && oldState != mState) {
            mVoiceCallEndedRegistrants.notifyRegistrants(
                    new AsyncResult(null, null, null));
        } else if (oldState == PhoneConstants.State.IDLE && oldState != mState) {
            mVoiceCallStartedRegistrants.notifyRegistrants (
                    new AsyncResult(null, null, null));
        }
        if (Phone.DEBUG_PHONE) {
            log("update phone state, old=" + oldState + " new="+ mState);
        }
        if (mState != oldState) {
            mPhone.notifyPhoneStateChanged();
            mMetrics.writePhoneState(mPhone.getPhoneId(), mState);
        }
    }
    @Override
    protected void notifySrvccState(Call.SrvccState state, ArrayList<Connection> c) {
        if (state == Call.SrvccState.STARTED && c != null) {
            // SRVCC started. Prepare handover connections list
            mHandoverConnections.addAll(c);
            // ALPS02019630. For IMS conference SRVCC. ECONFSRVCC is only for conference
            // host side.
            if (!hasParsingCEPCapability()) {
                for (Connection conn : mHandoverConnections) {
                    if (conn.isMultiparty() && conn instanceof MtkImsPhoneConnection
                            && conn.isConferenceHost()) {
                        log("srvcc: mNeedWaitImsEConfSrvcc set True");
                        mNeedWaitImsEConfSrvcc = true;
                        mImsConfHostConnection = conn;
                    }
                }
            }
        } else if (state != Call.SrvccState.COMPLETED) {
            // SRVCC FAILED/CANCELED. Clear the handover connections list
            // Individual connections will be removed from the list in handlePollCalls()
            mHandoverConnections.clear();
        }
        log("notifySrvccState: mHandoverConnections= " + mHandoverConnections.toString());
    }

    @Override
    protected Connection getHoConnection(DriverCall dc) {
        if (dc == null) {
            return null;
        }
        /*
         CallTracker.getHoConnection is used to find the original connection before SRVCC. It finds
         by call number and call state. But in conference SRVCC case, the call number is null and
         the call state might be different.
        */
        if (mEconfSrvccConnectionIds != null && dc != null) {
            int numOfParticipants = mEconfSrvccConnectionIds[0];
            for (int index = 1; index <= numOfParticipants; index++) {
                if (dc.index == mEconfSrvccConnectionIds[index]) {
                    proprietaryLog("SRVCC: getHoConnection for call-id:"
                            + dc.index + " in a conference is found!");
                    if (mImsConfHostConnection == null) {
                        proprietaryLog("SRVCC: but mImsConfHostConnection is null, " +
                                "try to find by callState");
                        break;
                    } else {
                        proprietaryLog("SRVCC: ret= " + mImsConfHostConnection);
                        return mImsConfHostConnection;
                    }
                }
            }
        }

        // ALPS01995466. JE because dc.number is null.
        log("SRVCC: getHoConnection() with dc, number = " + dc.number + " state = " + dc.state);
        if (dc.number != null && !dc.number.isEmpty()) {
            for (Connection hoConn : mHandoverConnections) {
                log("getHoConnection - compare number: hoConn= " + hoConn.toString());
                if (hoConn.getAddress() != null && hoConn.getAddress().contains(dc.number)) {
                    log("getHoConnection: Handover connection match found = " + hoConn.toString());
                    return hoConn;
                }
            }
        }
        for (Connection hoConn : mHandoverConnections) {
            log("getHoConnection: compare state hoConn= " + hoConn.toString());
            if (hoConn.getStateBeforeHandover() == Call.stateFromDCState(dc.state)) {
                log("getHoConnection: Handover connection match found = " + hoConn.toString());
                return hoConn;
            }
        }
        return null;
    }

    /**
     * For conference participants, the call number will be empty after SRVCC.
     * So at handlePollCalls(), it will get new connections without address. We use +ECONFSRVCC
     * and conference XML to restore all addresses.
     *
     * @return true if connections are restored.
     */
    private synchronized boolean restoreConferenceParticipantAddress() {
        if (mEconfSrvccConnectionIds == null) {
            proprietaryLog("SRVCC: restoreConferenceParticipantAddress():" +
                    "ignore because mEconfSrvccConnectionIds is empty");
            return false;
        }

        boolean finishRestore = false;

        // int[] mEconfSrvccConnectionIds = { size, call-ID-1, call-ID-2, call-ID-3, ...}
        int numOfParticipants = mEconfSrvccConnectionIds[0];
        for (int index = 1; index <= numOfParticipants; index++) {

            int participantCallId = mEconfSrvccConnectionIds[index];
            GsmCdmaConnection participantConnection = mConnections[participantCallId - 1];

            if (participantConnection != null) {
                proprietaryLog("SRVCC: found conference connections!");

                MtkImsPhoneConnection hostConnection = null;
                if (participantConnection.mOrigConnection instanceof MtkImsPhoneConnection) {
                    hostConnection = (MtkImsPhoneConnection) participantConnection.mOrigConnection;
                } else {
                    proprietaryLog("SRVCC: host is abnormal, ignore connection: " +
                            participantConnection);
                    continue;
                }

                if (hostConnection == null) {
                    proprietaryLog("SRVCC: no host, ignore connection: " + participantConnection);
                    continue;
                }

                String address = hostConnection.getConferenceParticipantAddress(index - 1);
                if (participantConnection instanceof MtkGsmCdmaConnection) {
                    ((MtkGsmCdmaConnection) participantConnection).
                            updateConferenceParticipantAddress(address);
                }
                finishRestore = true;

                proprietaryLog("SRVCC: restore Connection=" + participantConnection +
                        " with address:" + address);
            }
        }

        return finishRestore;
    }

    boolean hasParsingCEPCapability() {
        MtkHardwareConfig modem =
                ((MtkHardwareConfig) mTelDevController.getModem(0));
        if (modem == null) {
            return false;
        }
        return modem.hasParsingCEPCapability();
    }
    // end of IMS conference SRVCC
}
