/* Copyright Statement:
*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein
* is confidential and proprietary to MediaTek Inc. and/or its licensors.
* Without the prior written permission of MediaTek inc. and/or its licensors,
* any reproduction, modification, use or disclosure of MediaTek Software,
* and information contained herein, in whole or in part, shall be strictly prohibited.
*/
/* MediaTek Inc. (C) 2017. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
* AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
* NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
* SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
* SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
* THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
* THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
* CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
* SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
* CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
* AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
* OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
* MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
* The following software/firmware and/or related documentation ("MediaTek Software")
* have been modified by MediaTek Inc. All revisions are subject to any receiver\'s
* applicable license agreements with MediaTek Inc.
*/

package com.mediatek.op09c.telephony;

import android.telephony.Rlog;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.TelephonyProperties;
import com.mediatek.internal.telephony.SignalStrengthExt;

/**
 * Customization from China Telecom for LTE signal strength level.
 */

public class Op09CSignalStrengthExt extends SignalStrengthExt{

    private static final String TAG = "Op09CSignalStrengthExt";
    private static final String[] CU_PLMN_SIM = {"46001", "46006", "46009", "45407", "00101"};

    @Override
    public int mapLteSignalLevel(int rsrp , int rssnr, int lteSignalStrength) {
        if (!isCUCardInserted()) {
            int rsrpIconLevel = SignalStrength.SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
            int snrIconLevel = SignalStrength.SIGNAL_STRENGTH_NONE_OR_UNKNOWN;

            if (rsrp == SignalStrength.INVALID) {
                rsrpIconLevel = -1;
            } else if (rsrp >= -105) {
                rsrpIconLevel = SignalStrength.SIGNAL_STRENGTH_GREAT;
            } else if (rsrp >= -114) {
                rsrpIconLevel = SignalStrength.SIGNAL_STRENGTH_GOOD;
            } else if (rsrp >= -118) {
                rsrpIconLevel = SignalStrength.SIGNAL_STRENGTH_MODERATE;
            } else if (rsrp >= -123) {
                rsrpIconLevel = SignalStrength.SIGNAL_STRENGTH_POOR;
            } else {
                rsrpIconLevel = SignalStrength.SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
            }

            if (rssnr == SignalStrength.INVALID) {
                snrIconLevel = -1;
            } else if (rssnr >= 90) {
                snrIconLevel = SignalStrength.SIGNAL_STRENGTH_GREAT;
            } else if (rssnr >= 10) {
                snrIconLevel = SignalStrength.SIGNAL_STRENGTH_GOOD;
            } else if (rssnr >= -30) {
                snrIconLevel = SignalStrength.SIGNAL_STRENGTH_MODERATE;
            } else if (rssnr >= -50) {
                snrIconLevel = SignalStrength.SIGNAL_STRENGTH_POOR;
            } else {
                snrIconLevel = SignalStrength.SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
            }

            Rlog.d(TAG, "OP09C_getLTELevel - rsrp:" + rsrp + " snr:" + rssnr + " rsrpIconLevel:"
                    + rsrpIconLevel + " snrIconLevel:" + snrIconLevel);

            // Choose a measurement type to use for notification
            if (snrIconLevel != -1 && rsrpIconLevel != -1) {
                //Use the lower one as the singal strength.
                return (rsrpIconLevel < snrIconLevel ? rsrpIconLevel : snrIconLevel);
            }

            if (snrIconLevel != -1) {
                return snrIconLevel;
            }

            if (rsrpIconLevel != -1) {
                return rsrpIconLevel;
            }
        } else {
            int rsrpIconLevel;
            if (rsrp < -140 || rsrp > -44) {
                rsrpIconLevel = SignalStrength.SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
            } else if (rsrp >= -97) {
                rsrpIconLevel = SignalStrength.SIGNAL_STRENGTH_GREAT;
            } else if (rsrp >= -105) {
                rsrpIconLevel = SignalStrength.SIGNAL_STRENGTH_GOOD;
            } else if (rsrp >= -113) {
                rsrpIconLevel = SignalStrength.SIGNAL_STRENGTH_MODERATE;
            } else if (rsrp >= -120) {
                rsrpIconLevel = SignalStrength.SIGNAL_STRENGTH_POOR;
            } else {
                rsrpIconLevel = SignalStrength.SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
            }

            Rlog.d(TAG, "op09C_CU_mapLteSignalLevel=" + rsrpIconLevel);
            return rsrpIconLevel;
        }

        return SignalStrength.SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
    }

    private boolean isCUCardInserted() {
        int phoneId;
        String simOperator = null;
        for (phoneId = 0; phoneId < TelephonyManager.getDefault().getPhoneCount(); phoneId++) {
            simOperator = TelephonyManager.getTelephonyProperty(
                phoneId, TelephonyProperties.PROPERTY_ICC_OPERATOR_NUMERIC, "");
            if (simOperator != null && !simOperator.equals("")) {
                for (String plmn : CU_PLMN_SIM) {
                    if (simOperator.equals(plmn)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}


