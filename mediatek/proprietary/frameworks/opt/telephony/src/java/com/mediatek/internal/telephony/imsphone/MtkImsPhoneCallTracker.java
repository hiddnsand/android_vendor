/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony.imsphone;

import static com.mediatek.internal.telephony.MtkTelephonyProperties.PROPERTY_TERMINAL_BASED_CALL_WAITING_MODE;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.TERMINAL_BASED_CALL_WAITING_DISABLED;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.TERMINAL_BASED_CALL_WAITING_ENABLED_OFF;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncResult;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.telephony.CarrierConfigManager;
import android.text.TextUtils;
import android.preference.PreferenceManager;
import android.telecom.ConferenceParticipant;
import android.telecom.VideoProfile;
import android.telephony.DisconnectCause;
import android.telephony.PhoneNumberUtils;
import android.telephony.Rlog;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.android.ims.ImsCall;
import com.android.ims.ImsCallProfile;
import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.ims.ImsReasonInfo;
import com.android.ims.ImsSuppServiceNotification;
import com.android.ims.ImsUtInterface;
import com.android.ims.internal.ImsVideoCallProviderWrapper;
import com.android.internal.telephony.Call;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.imsphone.ImsExternalCallTracker;
import com.android.internal.telephony.imsphone.ImsPhone;
import com.android.internal.telephony.imsphone.ImsPhoneCall;
import com.android.internal.telephony.imsphone.ImsPhoneCallTracker;
import com.android.internal.telephony.imsphone.ImsPhoneConnection;
import com.android.internal.telephony.imsphone.ImsPullCall;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyDevController;
import com.android.internal.telephony.TelephonyProperties;
import com.android.internal.telephony.gsm.SuppServiceNotification;
import com.android.internal.telephony.metrics.TelephonyMetrics;
import com.android.internal.telephony.nano.TelephonyProto.TelephonyCallSession;
import com.android.internal.telephony.nano.TelephonyProto.TelephonyCallSession.Event.ImsCommand;

import com.mediatek.ims.MtkImsCall;
import com.mediatek.ims.MtkImsReasonInfo;
import com.mediatek.ims.internal.MtkImsManager;
import com.mediatek.internal.telephony.MtkCallStateException;
import com.mediatek.internal.telephony.MtkHardwareConfig;
import com.mediatek.internal.telephony.MtkPhoneNumberUtils;

import mediatek.telecom.FormattedLog;
import mediatek.telephony.MtkDisconnectCause;

import com.mediatek.internal.telephony.imsphone.op.OpImsPhoneFactoryBase;
import com.mediatek.internal.telephony.imsphone.op.OpImsPhoneCallTracker;

/**
 * {@hide}
 */
public class MtkImsPhoneCallTracker extends ImsPhoneCallTracker implements ImsPullCall {
    static final String LOG_TAG = "MtkImsPhoneCallTracker";
    // Sensitive log task
    private static final String PROP_FORCE_DEBUG_KEY = "persist.log.tag.tel_dbg";
    private static final boolean SENLOG = TextUtils.equals(Build.TYPE, "user");
    private static final boolean TELDBG = (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1);

    {
        mRingingCall = new MtkImsPhoneCall(this, ImsPhoneCall.CONTEXT_RINGING);
        mForegroundCall = new MtkImsPhoneCall(this, ImsPhoneCall.CONTEXT_FOREGROUND);
        mBackgroundCall = new MtkImsPhoneCall(this, ImsPhoneCall.CONTEXT_BACKGROUND);
        mHandoverCall = new MtkImsPhoneCall(this, ImsPhoneCall.CONTEXT_HANDOVER);
    }

    //private MtkImsPhoneConnection mPendingMO;

    /// M: @{
    // Redial as ECC
    private boolean mDialAsECC = false;

    // ALPS02023277 Set this flag to true when there is a resume request not finished.
    // Prevent to accept a waiting call during resuming a background call.
    private boolean mHasPendingResumeRequest = false;

    ///M: ALPS02577419. @{
    private boolean mIsOnCallResumed = false;
    /// @}

    /// M: ALPS02261962. For IMS registration state and capability information.
    private int mImsRegistrationErrorCode;

    private OpImsPhoneCallTracker mOpImsPhoneCallTracker;

    // ALPS02462990 For OP01 requirement: Only one video call allowed.
    private static final int INVALID_CALL_MODE = 0xFF;
    private static final int IMS_VOICE_CALL = 20;
    private static final int IMS_VIDEO_CALL = 21;
    private static final int IMS_VOICE_CONF = 22;
    private static final int IMS_VIDEO_CONF = 23;
    private static final int IMS_VOICE_CONF_PARTS = 24;
    private static final int IMS_VIDEO_CONF_PARTS = 25;
    /// @}

    /// M: CC: Vzw/CTVolte ECC @{
    TelephonyDevController mTelDevController = TelephonyDevController.getInstance();
    private boolean hasC2kOverImsModem() {
        if (mTelDevController != null &&
                mTelDevController.getModem(0) != null &&
                ((MtkHardwareConfig) mTelDevController.getModem(0)).hasC2kOverImsModem() == true) {
                    return true;
        }
        return false;
    }
    /// @}

    public MtkImsPhoneCallTracker(ImsPhone phone) {
        super(phone);
        mOpImsPhoneCallTracker = OpImsPhoneFactoryBase.getInstance().makeOpImsPhoneCallTracker();

        /// M: register the indication receiver. @{
        registerIndicationReceiver();
        /// @}

        ///M: for RTT
        mOpImsPhoneCallTracker.initRtt(mPhone);
    }

    @Override
    public void dispose() {
        if (DBG) log("dispose");
        mRingingCall.dispose();
        mBackgroundCall.dispose();
        mForegroundCall.dispose();
        mHandoverCall.dispose();

        clearDisconnected();
        mPhone.getContext().unregisterReceiver(mReceiver);

        /// M: It needs to unregister Indication receivers here.  @{
        unregisterIndicationReceiver();

        // For WFC-DS, ImsPhone is disposed when SIM-Switch.
        mPhone.setServiceState(ServiceState.STATE_OUT_OF_SERVICE);
        mPhone.setImsRegistered(false);
        for (int i = ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE;
             i <= ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_WIFI; i++) {
            mImsFeatureEnabled[i] = false;
        }
        mPhone.onFeatureCapabilityChanged();
        /// @}

        mPhone.getDefaultPhone().unregisterForDataEnabledChanged(this);
        removeMessages(EVENT_GET_IMS_SERVICE);

        //M: RTT
        mOpImsPhoneCallTracker.disposeRtt(mPhone, mForegroundCall, mSrvccState);
    }

    @Override
    public Connection dial(String dialString, int videoState, Bundle intentExtras) throws
            CallStateException {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mPhone.getContext());
        /// M: Add phone id to support dual sim
        // int oirMode = sp.getInt(Phone.CLIR_KEY, CommandsInterface.CLIR_DEFAULT);
        int oirMode = sp.getInt(Phone.CLIR_KEY + mPhone.getPhoneId(),
                CommandsInterface.CLIR_DEFAULT);
        return dial(dialString, oirMode, videoState, intentExtras);
    }

    @Override
    protected void dialInternal(ImsPhoneConnection conn, int clirMode, int videoState,
                              Bundle intentExtras) {

        if (conn == null) {
            return;
        }

        /// M:  For VoLTE enhanced conference call. @{
        if (conn instanceof MtkImsPhoneConnection == false
                || ((MtkImsPhoneConnection) conn).getConfDialStrings() == null) {
            /// @}
            if (conn.getAddress()== null || conn.getAddress().length() == 0
                    || conn.getAddress().indexOf(PhoneNumberUtils.WILD) >= 0) {
                // Phone number is invalid
                conn.setDisconnectCause(DisconnectCause.INVALID_NUMBER);
                sendEmptyMessageDelayed(EVENT_HANGUP_PENDINGMO, TIMEOUT_HANGUP_PENDINGMO);
                return;
            }
        }

        // Always unmute when initiating a new call
        setMute(false);

        // M: CC: For 93, MD can switch phoneType when SIM not inserted,
        // TeleService won't trigger phone switch, so check both SIM's ECC
        // int serviceType = PhoneNumberUtils.isEmergencyNumber(conn.getAddress()) ?
        boolean isEmergencyNumber = false;

        if (hasC2kOverImsModem() &&
                !TelephonyManager.getDefault().hasIccCard(mPhone.getPhoneId())) {
            isEmergencyNumber = PhoneNumberUtils.isEmergencyNumber(conn.getAddress());
        } else {
            /// M: ALPS02399162
            // ECC number is different for CDMA and GSM. Should carry phone sub id to query if the
            // number is ECC or not
            isEmergencyNumber = PhoneNumberUtils.isEmergencyNumber(mPhone.getSubId(),
                    conn.getAddress());
        }
        /// @}

        int serviceType = isEmergencyNumber ?
                ImsCallProfile.SERVICE_TYPE_EMERGENCY : ImsCallProfile.SERVICE_TYPE_NORMAL;

        // For some operation's request, we need to dial ECC with ATD. At these cases,
        // we will put the special ECC numbers in specialECCnumber list
        if (serviceType == ImsCallProfile.SERVICE_TYPE_EMERGENCY &&
                MtkPhoneNumberUtils.isSpecialEmergencyNumber(conn.getAddress())) {
            serviceType = ImsCallProfile.SERVICE_TYPE_NORMAL;
        }
        // Redial as ECC
        if (mDialAsECC) {
            serviceType = ImsCallProfile.SERVICE_TYPE_EMERGENCY;
            log("Dial as ECC: conn.getAddress(): " + conn.getAddress());
            mDialAsECC = false;
        }
        /// @}
        int callType = ImsCallProfile.getCallTypeFromVideoState(videoState);
        //TODO(vt): Is this sufficient?  At what point do we know the video state of the call?
        conn.setVideoState(videoState);


        ///M: for RTT feature
        mOpImsPhoneCallTracker.setRttTextStream(conn, intentExtras);


        try {
            /// M:  For VoLTE enhanced conference call. @{
            // String[] callees = new String[] { conn.getAddress() };
            String[] callees = null;
            if (conn instanceof MtkImsPhoneConnection
                    && ((MtkImsPhoneConnection) conn).getConfDialStrings() != null) {
                ArrayList<String> dialStrings = ((MtkImsPhoneConnection) conn).getConfDialStrings();
                callees = (String[]) dialStrings.toArray(new String[dialStrings.size()]);
            } else {
                callees = new String[] { conn.getAddress() };
            }
            /// @}
            ImsCallProfile profile = mImsManager.createCallProfile(mServiceId,
                    serviceType, callType);
            profile.setCallExtraInt(ImsCallProfile.EXTRA_OIR, clirMode);
            /// M:  For VoLTE enhanced conference call. @{
            if (conn instanceof MtkImsPhoneConnection
                    && ((MtkImsPhoneConnection) conn).getConfDialStrings() != null) {
                profile.setCallExtraBoolean(ImsCallProfile.EXTRA_CONFERENCE, true);
            }
            /// @}

            // Translate call subject intent-extra from Telecom-specific extra key to the
            // ImsCallProfile key.
            if (intentExtras != null) {
                if (intentExtras.containsKey(android.telecom.TelecomManager.EXTRA_CALL_SUBJECT)) {
                    intentExtras.putString(ImsCallProfile.EXTRA_DISPLAY_TEXT,
                            cleanseInstantLetteringMessage(intentExtras.getString(
                                    android.telecom.TelecomManager.EXTRA_CALL_SUBJECT))
                    );
                }

                if (intentExtras.containsKey(ImsCallProfile.EXTRA_IS_CALL_PULL)) {
                    profile.mCallExtras.putBoolean(ImsCallProfile.EXTRA_IS_CALL_PULL,
                            intentExtras.getBoolean(ImsCallProfile.EXTRA_IS_CALL_PULL));
                    int dialogId = intentExtras.getInt(
                            ImsExternalCallTracker.EXTRA_IMS_EXTERNAL_CALL_ID);
                    conn.setIsPulledCall(true);
                    conn.setPulledDialogId(dialogId);
                }

                // Pack the OEM-specific call extras.
                profile.mCallExtras.putBundle(ImsCallProfile.EXTRA_OEM_EXTRAS, intentExtras);

                // NOTE: Extras to be sent over the network are packed into the
                // intentExtras individually, with uniquely defined keys.
                // These key-value pairs are processed by IMS Service before
                // being sent to the lower layers/to the network.

                //M: RTT
                mOpImsPhoneCallTracker.setRttMode(intentExtras, profile);
            }

            /// M: @{
            // ALPS03110315 When user dial a number with FDN on, will dial failed. And will not
            // receive ECPI '130' URC, and the EXTRA_OI is null. But the address number will be
            // updated as NULL due to null of EXTRA_OI.
            // Set EXTRA_OI before dial to avoid this case.
            if ((callees != null) && (callees.length == 1) &&
                    !profile.getCallExtraBoolean(ImsCallProfile.EXTRA_CONFERENCE)) {
                profile.setCallExtra(ImsCallProfile.EXTRA_OI, callees[0]);
            }
            /// @}

            ImsCall imsCall = mImsManager.makeCall(mServiceId, profile,
                    callees, mMtkImsCallListener);
            conn.setImsCall(imsCall);

            mMetrics.writeOnImsCallStart(mPhone.getPhoneId(),
                    imsCall.getSession());

            setVideoCallProvider(conn, imsCall);
            conn.setAllowAddCallDuringVideoCall(mAllowAddCallDuringVideoCall);
        } catch (ImsException e) {
            loge("dialInternal : " + e);
            conn.setDisconnectCause(DisconnectCause.ERROR_UNSPECIFIED);
            sendEmptyMessageDelayed(EVENT_HANGUP_PENDINGMO, TIMEOUT_HANGUP_PENDINGMO);
            retryGetImsService();
        } catch (RemoteException e) {
        }
    }

    /**
     * Accepts a call with the specified video state.  The video state is the video state that the
     * user has agreed upon in the InCall UI.
     *
     * @param videoState The video State
     * @throws CallStateException
     */
    @Override
    public void acceptCall (int videoState) throws CallStateException {
        // Handle the action during SRVCC.
        if (mSrvccState == Call.SrvccState.STARTED || mSrvccState == Call.SrvccState.COMPLETED) {
            throw new CallStateException(
                    MtkCallStateException.ERROR_INVALID_DURING_SRVCC, "cannot accept call: SRVCC");
        }

        logDebugMessagesWithOpFormat("CC", "Answer", mRingingCall.getFirstConnection(), "");
        super.acceptCall(videoState);
    }

    @Override
    public void rejectCall () throws CallStateException {
        /// M: ALPS02136981. Prints debug logs for ImsPhone.
        logDebugMessagesWithOpFormat("CC", "Reject", mRingingCall.getFirstConnection(), "");
        super.rejectCall();
    }

    @Override
    public void switchWaitingOrHoldingAndActive() throws CallStateException {
        // Handle the action during SRVCC.
        if (mSrvccState == Call.SrvccState.STARTED || mSrvccState == Call.SrvccState.COMPLETED) {
            throw new CallStateException(MtkCallStateException.ERROR_INVALID_DURING_SRVCC,
                    "cannot hold/unhold call: SRVCC");
        }

        // ALPS02136981. Prints debug logs for ImsPhone.
        ImsPhoneConnection conn = null;
        String msg = null;
        if (mForegroundCall.getState() == ImsPhoneCall.State.ACTIVE) {
            conn = mForegroundCall.getFirstConnection();
            if (mBackgroundCall.getState().isAlive()) {
                msg = "switch with background connection:" + mBackgroundCall.getFirstConnection();
            } else {
                msg = "hold to background";
            }
        } else {
            conn = mBackgroundCall.getFirstConnection();
            msg = "unhold to foreground";
        }
        logDebugMessagesWithOpFormat("CC", "Swap", conn, msg);

        super.switchWaitingOrHoldingAndActive();
    }

    @Override
    public void
    conference() {
        /// M: ALPS02136981. Prints debug logs for ImsPhone. @{
        logDebugMessagesWithOpFormat("CC", "Conference", mForegroundCall.getFirstConnection(),
                " merge with " + mBackgroundCall.getFirstConnection());
        /// @}
        super.conference();
    }

    @Override
    public void
    explicitCallTransfer() {
        if (DBG) {
            log("explicitCallTransfer");
        }

        ImsCall fgImsCall = mForegroundCall.getImsCall();
        if (fgImsCall == null) {
            log("explicitCallTransfer no foreground ims call");
            return;
        }

        ImsCall bgImsCall = mBackgroundCall.getImsCall();
        if (bgImsCall == null) {
            log("explicitCallTransfer no background ims call");
            return;
        }

        if (mForegroundCall.getState() != ImsPhoneCall.State.ACTIVE
                || mBackgroundCall.getState() != ImsPhoneCall.State.HOLDING) {
            log("annot transfer call");
            return;
        }

        try {
            ((MtkImsCall)fgImsCall).explicitCallTransfer();
        } catch (ImsException e) {
            log("explicitCallTransfer " + e.getMessage());
        }
    }

    /**
     * Transfers the active to specific number.
     *
     * @param number The transfer target number.
     * @param type ECT type
     */
    public void
    unattendedCallTransfer(String number, int type) {
        if (DBG) {
            if (!SENLOG || TELDBG) {
                log("unattendedCallTransfer number : " + number + ", type : " + type);
            } else {
                log("unattendedCallTransfer number : [hidden], type : " + type);
            }
        }

        ImsCall fgImsCall = mForegroundCall.getImsCall();
        if (fgImsCall == null) {
            log("explicitCallTransfer no foreground ims call");
            return;
        }

        try {
            ((MtkImsCall)fgImsCall).unattendedCallTransfer(number, type);
        } catch (ImsException e) {
            log("explicitCallTransfer " + e.getMessage());
        }
    }

    @Override
    public boolean
    canDial() {
        boolean ret;
        int serviceState = mPhone.getServiceState().getState();
        String disableCall = SystemProperties.get(
                TelephonyProperties.PROPERTY_DISABLE_CALL, "false");

        /// M: add for debug @{
        log("IMS: canDial() serviceState = " + serviceState + ", disableCall = " + disableCall
                + ", mPendingMO = " + mPendingMO + ", Is mRingingCall ringing = "
                + mRingingCall.isRinging() + ", Is mForegroundCall alive = "
                + mForegroundCall.getState().isAlive() +
                ", Is mBackgroundCall alive = " + mBackgroundCall.getState().isAlive());
        /// @}

        return super.canDial();
    }

    //***** Called from ImsPhoneCall
    @Override
    public void hangup (ImsPhoneCall call) throws CallStateException {
        /// M: Handle the action during SRVCC. @{
        if (mSrvccState == Call.SrvccState.STARTED || mSrvccState == Call.SrvccState.COMPLETED) {
            throw new CallStateException(
                    MtkCallStateException.ERROR_INVALID_DURING_SRVCC, "cannot hangup call: SRVCC");
        }
        /// @}
        super.hangup(call);
    }

    @Override
    protected void callEndCleanupHandOverCallIfAny() {
        if (mHandoverCall.mConnections.size() > 0) {
            if (DBG) log("callEndCleanupHandOverCallIfAny, mHandoverCall.mConnections="
                    + mHandoverCall.mConnections);
            /// M: ALPS01979162 make sure all connections of mHandoverCall are removed from
            /// mConnections to prevent leak @{
            for (Connection conn : mHandoverCall.mConnections) {
                log("SRVCC: remove connection=" + conn);
                removeConnection((ImsPhoneConnection) conn);
            }
            /// @}
            mHandoverCall.mConnections.clear();
            mConnections.clear();
            mState = PhoneConstants.State.IDLE;

            /// M: ALPS02192901. @{
            if (mPhone != null && mPhone.mDefaultPhone != null
                    && mPhone.mDefaultPhone.getState() == PhoneConstants.State.IDLE) {
                // If the call is disconnected before GSMPhone poll calls, the phone state of
                // GSMPhone keeps in idle state, so it will not notify phone state changed. In this
                // case, ImsPhone needs to notify by itself.
                log("SRVCC: notify ImsPhone state as idle.");
                mPhone.notifyPhoneStateChanged();
            }
            /// @}
        }
    }

    @Override
    public void sendUSSD (String ussdString, Message response) {
        if (DBG) log("sendUSSD");

        try {
            if (mUssdSession != null) {
                mUssdSession.sendUssd(ussdString);
                AsyncResult.forMessage(response, null, null);
                response.sendToTarget();
                return;
            }

            if (mImsManager == null) {
                mPhone.sendErrorResponse(response, getImsManagerIsNullException());
                return;
            }

            String[] callees = new String[] { ussdString };
            ImsCallProfile profile = mImsManager.createCallProfile(mServiceId,
                    ImsCallProfile.SERVICE_TYPE_NORMAL, ImsCallProfile.CALL_TYPE_VOICE);
            profile.setCallExtraInt(ImsCallProfile.EXTRA_DIALSTRING,
                    ImsCallProfile.DIALSTRING_USSD);

            /// M: Assign return message for UE initiated USSI @{
            mPendingUssd = response;
            /// @}
            mUssdSession = mImsManager.makeCall(mServiceId, profile,
                    callees, mImsUssdListener);
        } catch (ImsException e) {
            loge("sendUSSD : " + e);
            mPhone.sendErrorResponse(response, e);
            retryGetImsService();
        }
    }

    @Override
    protected synchronized void addConnection(ImsPhoneConnection conn) {
        super.addConnection(conn);

        if (conn.isEmergency()) {
            //M: RTT
            mOpImsPhoneCallTracker.stopRttEmcGuardTimer();
        }
    }

    @Override
    protected void processCallStateChange(ImsCall imsCall, ImsPhoneCall.State state, int cause,
                                        boolean ignoreState) {
        super.processCallStateChange(imsCall, state, cause, ignoreState);
        /// M: ALPS02136981. Prints debug logs for ImsPhone.
        ImsPhoneConnection conn = findConnection(imsCall);
        logDebugMessagesWithDumpFormat("CC", conn, "");
    }

    @Override
    public int getDisconnectCauseFromReasonInfo(ImsReasonInfo reasonInfo, Call.State callState) {
        int code = maybeRemapReasonCode(reasonInfo);
        switch (code) {
            /// M: @{
            case MtkImsReasonInfo.CODE_SIP_REDIRECTED_EMERGENCY:
                return MtkDisconnectCause.IMS_EMERGENCY_REREG;
            case MtkImsReasonInfo.CODE_SIP_WIFI_SIGNAL_LOST:
                return MtkDisconnectCause.WFC_WIFI_SIGNAL_LOST;
            case MtkImsReasonInfo.CODE_SIP_WFC_ISP_PROBLEM:
                return MtkDisconnectCause.WFC_ISP_PROBLEM;
            case MtkImsReasonInfo.CODE_SIP_HANDOVER_WIFI_FAIL:
                return MtkDisconnectCause.WFC_HANDOVER_WIFI_FAIL;
            case MtkImsReasonInfo.CODE_SIP_HANDOVER_LTE_FAIL:
                return MtkDisconnectCause.WFC_HANDOVER_LTE_FAIL;
            default:
                break;
        }
        return super.getDisconnectCauseFromReasonInfo(reasonInfo, callState);
    }

    /**
     * Listen to the IMS call state change
     */
    private ImsCall.Listener mMtkImsCallListener = new MtkImsCall.Listener() {
        @Override
        public void onCallProgressing(ImsCall imsCall) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallProgressing(imsCall);
        }

        @Override
        public void onCallStarted(ImsCall imsCall) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallStarted(imsCall);
        }

        @Override
        public void onCallUpdated(ImsCall imsCall) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallUpdated(imsCall);
        }

        /**
         * onCallStartFailed will be invoked when:
         * case 1) Dialing fails
         * case 2) Ringing call is disconnected by local or remote user
         */
        @Override
        public void onCallStartFailed(ImsCall imsCall, ImsReasonInfo reasonInfo) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallStartFailed(imsCall, reasonInfo);
        }

        @Override
        public void onCallTerminated(ImsCall imsCall, ImsReasonInfo reasonInfo) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallTerminated(imsCall, reasonInfo);
        }

        @Override
        public void onCallHeld(ImsCall imsCall) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallHeld(imsCall);
        }

        @Override
        public void onCallHoldFailed(ImsCall imsCall, ImsReasonInfo reasonInfo) {
            if (DBG) log("onCallHoldFailed reasonCode=" + reasonInfo.getCode());

            synchronized (mSyncHold) {
                ImsPhoneCall.State bgState = mBackgroundCall.getState();
                if (reasonInfo.getCode() == ImsReasonInfo.CODE_LOCAL_CALL_TERMINATED) {
                    // disconnected while processing hold
                    /// M: ALPS02147333 Although call is held failed, but we treat it like call
                    /// held succeeded if the call has been terminated @{
                    if ((mForegroundCall.getState() == ImsPhoneCall.State.HOLDING)
                            || (mRingingCall.getState() == ImsPhoneCall.State.WAITING)) {
                        if (DBG) {
                            log("onCallHoldFailed resume background");
                        }
                        sendEmptyMessage(EVENT_RESUME_BACKGROUND);
                    } else {
                        if (mPendingMO != null) {
                            dialPendingMO();
                        }
                    }
                    /// @}
                } else if (bgState == ImsPhoneCall.State.ACTIVE) {
                    mForegroundCall.switchWith(mBackgroundCall);

                    if (mPendingMO != null) {
                        mPendingMO.setDisconnectCause(DisconnectCause.ERROR_UNSPECIFIED);
                        sendEmptyMessageDelayed(EVENT_HANGUP_PENDINGMO, TIMEOUT_HANGUP_PENDINGMO);
                    }
                }
                mPhone.notifySuppServiceFailed(Phone.SuppService.HOLD);
            }
            mMetrics.writeOnImsCallHoldFailed(mPhone.getPhoneId(), imsCall.getCallSession(),
                    reasonInfo);
        }

        @Override
        public void onCallResumed(ImsCall imsCall) {
            if (DBG) log("onCallResumed");

            ///M: ALPS02577419. @{
            mIsOnCallResumed = true;
            /// @}

            // If we are the in midst of swapping FG and BG calls and the call we end up resuming
            // is not the one we expected, we likely had a resume failure and we need to swap the
            // FG and BG calls back.
            if (mSwitchingFgAndBgCalls) {
                if (imsCall != mCallExpectedToResume) {
                    // If the call which resumed isn't as expected, we need to swap back to the
                    // previous configuration; the swap has failed.
                    if (DBG) {
                        log("onCallResumed : switching " + mForegroundCall + " with "
                                + mBackgroundCall);
                    }
                    mForegroundCall.switchWith(mBackgroundCall);
                } else {
                    // The call which resumed is the one we expected to resume, so we can clear out
                    // the mSwitchingFgAndBgCalls flag.
                    if (DBG) {
                        log("onCallResumed : expected call resumed.");
                    }
                }
                mSwitchingFgAndBgCalls = false;
                mCallExpectedToResume = null;
            }
            /// M: ALPS02023277. @{
            /// Reset this flag since call is resumed.
            /// This flag is used to prevent to accept a waiting call,
            /// during resuming a backdground call.
            mHasPendingResumeRequest = false;
            /// @}
            processCallStateChange(imsCall, ImsPhoneCall.State.ACTIVE,
                    DisconnectCause.NOT_DISCONNECTED);
            mMetrics.writeOnImsCallResumed(mPhone.getPhoneId(), imsCall.getCallSession());

            ///M: ALPS02577419. @{
            mIsOnCallResumed = false;
            /// @}
        }

        @Override
        public void onCallResumeFailed(ImsCall imsCall, ImsReasonInfo reasonInfo) {
            /// M: ALPS02023277. For debug purpose. @{
            if (DBG) {
                log("onCallResumeFailed");
            }
            /// @}

            if (mSwitchingFgAndBgCalls) {
                // If we are in the midst of swapping the FG and BG calls and
                // we got a resume fail, we need to swap back the FG and BG calls.
                // Since the FG call was held, will also try to resume the same.
                if (imsCall == mCallExpectedToResume) {
                    if (DBG) {
                        log("onCallResumeFailed : switching " + mForegroundCall + " with "
                                + mBackgroundCall);
                    }
                    mForegroundCall.switchWith(mBackgroundCall);
                    if (mForegroundCall.getState() == ImsPhoneCall.State.HOLDING) {
                        sendEmptyMessage(EVENT_RESUME_BACKGROUND);
                    }
                }

                //Call swap is done, reset the relevant variables
                mCallExpectedToResume = null;
                mSwitchingFgAndBgCalls = false;
            }
            /// M: ALPS02023277. @{
            /// Reset this flag since resume operation is done.
            /// This flag is used to prevent to accept a waiting call,
            /// during resuming a backdground call.
            mHasPendingResumeRequest = false;
            /// @}
            mPhone.notifySuppServiceFailed(Phone.SuppService.RESUME);
            mMetrics.writeOnImsCallResumeFailed(mPhone.getPhoneId(), imsCall.getCallSession(),
                    reasonInfo);
        }

        @Override
        public void onCallResumeReceived(ImsCall imsCall) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallResumeReceived(imsCall);
        }

        @Override
        public void onCallHoldReceived(ImsCall imsCall) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallHoldReceived(imsCall);
        }

        @Override
        public void onCallSuppServiceReceived(ImsCall call,
                ImsSuppServiceNotification suppServiceInfo) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallSuppServiceReceived(call,
                    suppServiceInfo);
        }

        @Override
        public void onCallMerged(final ImsCall call, final ImsCall peerCall, boolean swapCalls) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallMerged(call, peerCall, swapCalls);
            /// M: ALPS02136981. For formatted log, workaround for merge case. @{
            ImsPhoneConnection hostConn = findConnection(call);
            if (hostConn != null && hostConn instanceof MtkImsPhoneConnection) {
                MtkImsPhoneConnection hostConnExt = (MtkImsPhoneConnection)hostConn;
                FormattedLog formattedLog = new FormattedLog.Builder()
                        .setCategory("CC").setServiceName("ImsPhone")
                        .setOpType(FormattedLog.OpType.DUMP)
                        .setCallNumber(hostConn.getAddress())
                        .setCallId(getConnectionCallId(hostConnExt))
                        .setStatusInfo("state", "disconnected")
                        .setStatusInfo("isConfCall", "No")
                        .setStatusInfo("isConfChildCall", "No")
                        .setStatusInfo("parent", hostConnExt.getParentCallName())
                        .buildDumpInfo();

                if (formattedLog != null) {
                    log(formattedLog.toString());
                }
            }
            /// @}
        }

        @Override
        public void onCallMergeFailed(ImsCall call, ImsReasonInfo reasonInfo) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallMergeFailed(call, reasonInfo);
        }

        /**
         * Called when the state of IMS conference participant(s) has changed.
         *
         * @param call the call object that carries out the IMS call.
         * @param participants the participant(s) and their new state information.
         */
        @Override
        public void onConferenceParticipantsStateChanged(ImsCall call,
                List<ConferenceParticipant> participants) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onConferenceParticipantsStateChanged
                    (call, participants);
        }

        @Override
        public void onCallSessionTtyModeReceived(ImsCall call, int mode) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallSessionTtyModeReceived(call, mode);
        }

        @Override
        public void onCallHandover(ImsCall imsCall, int srcAccessTech, int targetAccessTech,
                ImsReasonInfo reasonInfo) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallHandover(imsCall, srcAccessTech,
                    targetAccessTech, reasonInfo);
        }

        @Override
        public void onCallHandoverFailed(ImsCall imsCall, int srcAccessTech, int targetAccessTech,
                                         ImsReasonInfo reasonInfo) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onCallHandoverFailed(imsCall,
                    srcAccessTech, targetAccessTech, reasonInfo);
        }

        /**
         * Handles a change to the multiparty state for an {@code ImsCall}.  Notifies the associated
         * {@link ImsPhoneConnection} of the change.
         *
         * @param imsCall The IMS call.
         * @param isMultiParty {@code true} if the call became multiparty, {@code false}
         *      otherwise.
         */
        @Override
        public void onMultipartyStateChanged(ImsCall imsCall, boolean isMultiParty) {
            MtkImsPhoneCallTracker.super.mImsCallListener.onMultipartyStateChanged(imsCall,
                    isMultiParty);
        }

        /// M: @{
        @Override
        public void onCallInviteParticipantsRequestDelivered(ImsCall call) {
            if (DBG) {
                log("onCallInviteParticipantsRequestDelivered");
            }

            ImsPhoneConnection conn = findConnection(call);
            if (conn != null && conn instanceof MtkImsPhoneConnection) {
                ((MtkImsPhoneConnection) conn).notifyConferenceParticipantsInvited(true);
            }
        }

        @Override
        public void onCallInviteParticipantsRequestFailed(ImsCall call, ImsReasonInfo reasonInfo) {
            if (DBG) {
                log("onCallInviteParticipantsRequestFailed reasonCode=" +
                        reasonInfo.getCode());
            }
            ImsPhoneConnection conn = findConnection(call);
            if (conn != null && conn instanceof MtkImsPhoneConnection) {
                ((MtkImsPhoneConnection) conn).notifyConferenceParticipantsInvited(false);
            }
        }

        @Override
        public void onCallTransferred(ImsCall imsCall) {
            if (DBG) {
                log("onCallTransferred");
            }
        }

        @Override
        public void onCallTransferFailed(ImsCall imsCall, ImsReasonInfo reasonInfo) {
            if (DBG) {
                log("onCallTransferFailed");
            }

            mPhone.notifySuppServiceFailed(Phone.SuppService.TRANSFER);
        }

        @Override
        public void onTextCapabilityChanged(ImsCall call, int localCapability,
                int remoteCapability, int localTextStatus, int realRemoteCapability) {
            ImsPhoneConnection conn = findConnection(call);

            mOpImsPhoneCallTracker.onTextCapabilityChanged(conn, localCapability, remoteCapability,
                    localTextStatus, realRemoteCapability);

            ///M: for RTT
            mOpImsPhoneCallTracker.setIncomingCallRtt(remoteCapability);
            checkRttCallType();
        }
        /// @}
    };

    /**
     * Listen to the IMS call state change
     */
    private ImsCall.Listener mImsUssdListener = new ImsCall.Listener() {
        @Override
        public void onCallStarted(ImsCall imsCall) {
            if (DBG) log("mImsUssdListener onCallStarted");

            if (imsCall == mUssdSession) {
                if (mPendingUssd != null) {
                    AsyncResult.forMessage(mPendingUssd);
                    mPendingUssd.sendToTarget();
                    mPendingUssd = null;
                }
            }
        }

        @Override
        public void onCallStartFailed(ImsCall imsCall, ImsReasonInfo reasonInfo) {
            if (DBG) log("mImsUssdListener onCallStartFailed reasonCode=" + reasonInfo.getCode());

            onCallTerminated(imsCall, reasonInfo);
        }

        @Override
        public void onCallTerminated(ImsCall imsCall, ImsReasonInfo reasonInfo) {
            if (DBG) log("mImsUssdListener onCallTerminated reasonCode=" + reasonInfo.getCode());
            removeMessages(EVENT_CHECK_FOR_WIFI_HANDOVER);

            if (imsCall == mUssdSession) {
                mUssdSession = null;
                if (mPendingUssd != null) {
                    CommandException ex =
                            new CommandException(CommandException.Error.GENERIC_FAILURE);
                    AsyncResult.forMessage(mPendingUssd, null, ex);
                    mPendingUssd.sendToTarget();
                    mPendingUssd = null;
                }
            }
            imsCall.close();
        }

        @Override
        public void onCallUssdMessageReceived(ImsCall call,
                                              int mode, String ussdMessage) {
            if (DBG) log("mImsUssdListener onCallUssdMessageReceived mode=" + mode);

            int ussdMode = -1;

            switch(mode) {
                case ImsCall.USSD_MODE_REQUEST:
                    ussdMode = CommandsInterface.USSD_MODE_REQUEST;
                    break;

                case ImsCall.USSD_MODE_NOTIFY:
                    ussdMode = CommandsInterface.USSD_MODE_NOTIFY;
                    /// M: After USSI notify, close this USSI session. @{
                    if (call == mUssdSession) {
                        mUssdSession = null;
                        call.close();
                    }
                    /// @}
                    break;
                /// M: Clear USSI session when any error from MD @{
                default:
                    if (call == mUssdSession) {
                        log("invalid mode: " + mode + ", clear ussi session");
                        mUssdSession = null;
                        call.close();
                    }
                    break;
                ///@}
            }

            ((MtkImsPhone) mPhone).onIncomingUSSD(ussdMode, ussdMessage);
        }
    };

    @Override
    protected void notifySrvccState(Call.SrvccState state) {

        //M: check SRVCC and CSFB for RTT
        if (mSrvccState == Call.SrvccState.COMPLETED) {
            mOpImsPhoneCallTracker.sendRttSrvccOrCsfbEvent(mForegroundCall);
            mOpImsPhoneCallTracker.sendRttSrvccOrCsfbEvent(mBackgroundCall);
            mOpImsPhoneCallTracker.sendRttSrvccOrCsfbEvent(mRingingCall);
        }
        super.notifySrvccState(state);
        //M: for RTT
        if (mSrvccState == Call.SrvccState.COMPLETED) {
            checkRttCallType();
        }
    }

    //****** Overridden from Handler

    @Override
    public void
    handleMessage (Message msg) {
        AsyncResult ar;
        if (DBG) log("handleMessage what=" + msg.what);

        switch (msg.what) {
             case EVENT_DIAL_PENDINGMO:
                /// M: ALPS02675166 @{
                /// Check if pendingMO have been already dialed, should not dial twice.
                // dialInternal(mPendingMO, mClirMode,
                //         mPendingCallVideoState, mPendingIntentExtras);
                // mPendingIntentExtras = null;
                if (mPendingMO != null && mPendingMO.getImsCall() == null) {
                    dialInternal(mPendingMO, mClirMode,
                            mPendingCallVideoState, mPendingIntentExtras);
                    mPendingIntentExtras = null;
                }
                /// @}
                break;
            default:
                super.handleMessage(msg);
                break;
        }
    }

    @Override
    protected void log(String msg) {
        Rlog.d(LOG_TAG, "[MtkImsPhoneCallTracker] " + msg);
    }

    /* package */
    void loge(String msg) {
        Rlog.e(LOG_TAG, "[MtkImsPhoneCallTracker] " + msg);
    }

    /// M: MTK added functions @{
    /// For ACTION_IMS_INCOMING_CALL_INDICATION, mIndicationReceiver is
    /// responsible to handle it
    private BroadcastReceiver mIndicationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(MtkImsManager.ACTION_IMS_INCOMING_CALL_INDICATION)) {
                if (DBG) {
                    log("onReceive : indication call intent");
                }

                if (mImsManager == null) {
                    if (DBG) {
                        log("no ims manager");
                    }
                    return;
                }

                boolean isAllow = true; /// default value is always allowed to take call
                int serviceId = intent.getIntExtra(ImsManager.EXTRA_SERVICE_ID, -1);
                if (serviceId != mServiceId) {
                    return;
                }

                // ALPS02037830. Needs to use Phone-Id to query call waiting setting.
                String callWaitingSetting = TelephonyManager.getTelephonyProperty(
                        mPhone.getPhoneId(),
                        PROPERTY_TERMINAL_BASED_CALL_WAITING_MODE,
                        TERMINAL_BASED_CALL_WAITING_DISABLED);

                /// M: for ALPS03153860 Need check call state. @{
                if (callWaitingSetting.equals(
                        TERMINAL_BASED_CALL_WAITING_ENABLED_OFF) == true
                        && mPhone.isInCall()) {
                    /// @}
                    log("PROPERTY_TERMINAL_BASED_CALL_WAITING_MODE = "
                            + "TERMINAL_BASED_CALL_WAITING_ENABLED_OFF."
                            + " Reject the call as UDUB ");
                    isAllow = false;
                }

                // Now we handle ECC/MT conflict issue.
                if (isEccExist()) {
                    log("there is an ECC call, dis-allow this incoming call!");
                    isAllow = false;
                }

                if (DBG) {
                    log("setCallIndication : serviceId = " + serviceId
                            + ", intent = " + intent
                            + ", isAllow = " + isAllow);
                }
                try {
                    if (mImsManager instanceof MtkImsManager) {
                        ((MtkImsManager)mImsManager).setCallIndication(mServiceId, intent, isAllow);
                    }
                } catch (ImsException e) {
                    loge("setCallIndication ImsException " + e);
                }
            }
        }
    };

    private void registerIndicationReceiver() {
        if (DBG) {
            log("registerIndicationReceiver");
        }
        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction(MtkImsManager.ACTION_IMS_INCOMING_CALL_INDICATION);
        mPhone.getContext().registerReceiver(mIndicationReceiver, intentfilter);

    }
    private void unregisterIndicationReceiver() {
        if (DBG) {
            log("unregisterIndicationReceiver");
        }
        mPhone.getContext().unregisterReceiver(mIndicationReceiver);
    }

    /**
     * for ALPS01987345, workaround for Modem, block incoming call when ECC exist.
     * @return true if there is ECC.
     */
    private boolean isEccExist() {
        ImsPhoneCall[] allCalls = {mForegroundCall, mBackgroundCall, mRingingCall, mHandoverCall};

        for (int i = 0; i < allCalls.length; i++) {
            if (!allCalls[i].getState().isAlive()) {
                continue;
            }

            ImsCall imsCall = allCalls[i].getImsCall();
            if (imsCall == null) {
                continue;
            }

            ImsCallProfile callProfile = imsCall.getCallProfile();
            if (callProfile != null &&
                    callProfile.mServiceType == ImsCallProfile.SERVICE_TYPE_EMERGENCY) {
                return true;
            }
        }

        log("isEccExist(): no ECC!");
        return false;
    }

    // For VoLTE enhanced conference call.
    /* package */
    Connection dial(List<String> numbers, int videoState) throws CallStateException {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mPhone.getContext());
        int oirMode = sp.getInt(Phone.CLIR_KEY + mPhone.getPhoneId(),
                CommandsInterface.CLIR_DEFAULT);
        return dial(numbers, oirMode, videoState);
    }

    // For VoLTE enhanced conference call.
    /* package */
    synchronized Connection
    dial(List<String> numbers, int clirMode, int videoState) throws CallStateException {
        if (DBG) {
            log("dial clirMode=" + clirMode);
        }

        // note that this triggers call state changed notif
        clearDisconnected();

        if (mImsManager == null) {
            throw new CallStateException("service not available");
        }

        if (!canDial()) {
            throw new CallStateException("cannot dial in current state");
        }

        boolean holdBeforeDial = false;

        // The new call must be assigned to the foreground call.
        // That call must be idle, so place anything that's
        // there on hold
        if (mForegroundCall.getState() == ImsPhoneCall.State.ACTIVE) {
            if (mBackgroundCall.getState() != ImsPhoneCall.State.IDLE) {
                //we should have failed in !canDial() above before we get here
                throw new CallStateException("cannot dial in current state");
            }
            // foreground call is empty for the newly dialed connection
            holdBeforeDial = true;
            switchWaitingOrHoldingAndActive();
        }

        ImsPhoneCall.State fgState = ImsPhoneCall.State.IDLE;
        ImsPhoneCall.State bgState = ImsPhoneCall.State.IDLE;

        mClirMode = clirMode;

        synchronized (mSyncHold) {
            if (holdBeforeDial) {
                fgState = mForegroundCall.getState();
                bgState = mBackgroundCall.getState();

                //holding foreground call failed
                if (fgState == ImsPhoneCall.State.ACTIVE) {
                    throw new CallStateException("cannot dial in current state");
                }

                //holding foreground call succeeded
                if (bgState == ImsPhoneCall.State.HOLDING) {
                    holdBeforeDial = false;
                }
            }

            // Create IMS conference host connection
            mPendingMO = new MtkImsPhoneConnection(mPhone, "",
                    this, mForegroundCall, false);

            ArrayList<String> dialStrings = new ArrayList<String>();
            for (String str : numbers) {
                dialStrings.add(PhoneNumberUtils.extractNetworkPortionAlt(str));
            }
            ((MtkImsPhoneConnection) mPendingMO).setConfDialStrings(dialStrings);
        }
        addConnection(mPendingMO);

        // ALPS02136981. Prints debug logs for ImsPhone.
        StringBuilder sb = new StringBuilder();
        for (String number : numbers) {
            sb.append(number);
            sb.append(", ");
        }
        logDebugMessagesWithOpFormat("CC", "DialConf", mPendingMO, " numbers=" + sb.toString());
        logDebugMessagesWithDumpFormat("CC", mPendingMO, "");

        if (!holdBeforeDial) {
            dialInternal(mPendingMO, clirMode, videoState, null);
        }

        updatePhoneState();
        mPhone.notifyPreciseCallStateChanged();

        return mPendingMO;
    }

    /* package */
    void hangupAll() throws CallStateException {
        if (DBG) {
            log("hangupAll");
        }

        if (mImsManager == null || !(mImsManager instanceof MtkImsManager)) {
            throw new CallStateException("No MtkImsManager Instance");
        }

        try {
            ((MtkImsManager)mImsManager).hangupAllCall(mServiceId);
        } catch (ImsException e) {
            throw new CallStateException(e.getMessage());
        }

        if (!mRingingCall.isIdle()) {
            mRingingCall.onHangupLocal();
        }
        if (!mForegroundCall.isIdle()) {
            mForegroundCall.onHangupLocal();
        }
        if (!mBackgroundCall.isIdle()) {
            mBackgroundCall.onHangupLocal();
        }
    }

    // ALPS02136981. Prints debug logs for ImsPhone.
    /**
     * Logs unified debug log messages for "OP".
     * Format: [category][Module][OP][Action][call-number][local-call-ID] Msg. String.
     * P.S. uses the RIL call ID as the local call ID.
     *
     * @param category currently we only have 'CC' category.
     * @param action the action name. (e.q. Dial, Hold, etc.)
     * @param conn the connection instance.
     * @param msg the optional messages
     * @hide
     */
    @Override
    protected void logDebugMessagesWithOpFormat(
            String category, String action, ImsPhoneConnection conn, String msg) {
        if (category == null || action == null || conn == null) {
            // return if no mandatory tags.
            return;
        }
        MtkImsPhoneConnection connExt = (MtkImsPhoneConnection)conn;
        FormattedLog formattedLog = new FormattedLog.Builder()
                .setCategory(category)
                .setServiceName("ImsPhone")
                .setOpType(FormattedLog.OpType.OPERATION)
                .setActionName(action)
                .setCallNumber(getCallNumber(conn))
                .setCallId(getConnectionCallId(connExt))
                .setExtraMessage(msg)
                .buildDebugMsg();

        if (formattedLog != null) {
            if (!SENLOG || TELDBG) {
                log(formattedLog.toString());
            }
        }
    }

    /**
     * Logs unified debug log messages, for "Dump".
     * format: [CC][Module][Dump][call-number][local-call-ID]-[name:value],[name:value]-Msg. String
     * P.S. uses the RIL call ID as the local call ID.
     *
     * @param category currently we only have 'CC' category.
     * @param conn the ImsPhoneConnection to be dumped.
     * @param msg the optional messages
     * @hide
     */
    @Override
    protected void logDebugMessagesWithDumpFormat(String category, ImsPhoneConnection conn,
            String msg) {
        if (category == null || conn == null || !(conn instanceof MtkImsPhoneConnection)) {
            // return if no mandatory tags.
            return;
        }
        MtkImsPhoneConnection connExt = (MtkImsPhoneConnection)conn;
        FormattedLog formattedLog = new FormattedLog.Builder()
                .setCategory("CC")
                .setServiceName("ImsPhone")
                .setOpType(FormattedLog.OpType.DUMP)
                .setCallNumber(getCallNumber(conn))
                .setCallId(getConnectionCallId(connExt))
                .setExtraMessage(msg)
                .setStatusInfo("state", conn.getState().toString())
                .setStatusInfo("isConfCall", conn.isMultiparty() ? "Yes" : "No")
                .setStatusInfo("isConfChildCall", "No")
                .setStatusInfo("parent", connExt.getParentCallName())
                .buildDumpInfo();

        if (formattedLog != null) {
            if (!SENLOG || TELDBG) {
                log(formattedLog.toString());
            }
        }
    }

    /**
     * get call ID of the imsPhoneConnection. (the same as RIL code ID)
     *
     * @param conn imsPhoneConnection
     * @return call ID string.
     * @hide
     */
    private String getConnectionCallId(MtkImsPhoneConnection conn) {
        if (conn == null) {
            return "";
        }

        int callId = conn.getCallId();
        if (callId == -1) {
            callId = conn.getCallIdBeforeDisconnected();
            if (callId == -1) {
                return "";
            }
        }
        return String.valueOf(callId);
    }

    /**
     * get call number of the imsPhoneConnection.
     *
     * @param conn imsPhoneConnection
     * @return call ID number.
     * @hide
     */
    private String getCallNumber(ImsPhoneConnection conn) {
        if (conn == null) {
            return null;
        }

        if (conn.isMultiparty()) {
            return "conferenceCall";
        } else {
            return conn.getAddress();
        }
    }

    // ALPS02495477: API for unhold specific connection and call.
    /*package*/ void
    unhold (ImsPhoneConnection conn) throws CallStateException {
        if (DBG) {
            log("unhold connection");
        }

        if (conn.getOwner() != this) {
            throw new CallStateException ("ImsPhoneConnection " + conn
                    + "does not belong to MtkImsPhoneCallTracker " + this);
        }

        unhold(conn.getCall());
    }

    private void unhold (ImsPhoneCall call) throws CallStateException {
        if (DBG) {
            log("unhold call");
        }

        if (call.getConnections().size() == 0) {
            throw new CallStateException("no connections");
        }
        ///M: ALPS02577419. @{
        if (mIsOnCallResumed) {
            if (DBG) {
                log("unhold call: drop unhold, an call is processing onCallResumed");
            }
            return;
        }
        /// @}

        try {
            // if call is background call, switch to foreground
            if (call == mBackgroundCall) {
                if (DBG) {
                    log("unhold call: it is bg call, swap fg and bg");
                }
                mSwitchingFgAndBgCalls = true;
                mCallExpectedToResume = mBackgroundCall.getImsCall();
                mForegroundCall.switchWith(mBackgroundCall);
            } else if (call != mForegroundCall) {
                if (DBG) {
                    log("unhold call which is neither background nor foreground call");
                }
                return;
            }
            if (mForegroundCall.getState().isAlive()) {
                if (DBG) {
                    log("unhold call: foreground call is alive; try to resume it");
                }
                ImsCall imsCall = mForegroundCall.getImsCall();
                if (imsCall != null) {
                    imsCall.resume();
                }
            }
        } catch (ImsException e) {
            throw new CallStateException(e.getMessage());
        }
    }

    // ALPS02501206. For operator custumization error causes.
    private boolean isVendorDisconnectCauseNeeded(ImsReasonInfo reasonInfo) {
        if (reasonInfo == null) {
            return false;
        }

        int errorCode = reasonInfo.getCode();
        String errorMsg = reasonInfo.getExtraMessage();

        if (errorMsg == null) {
            log("isVendorDisconnectCauseNeeded = no due to empty errorMsg");
            return false;
        }

        log("isVendorDisconnectCauseNeeded = no, no matched case");
        return false;
    }

    @Override
    public ImsUtInterface getUtInterface() throws ImsException {
        if (mImsManager == null) {
            throw getImsManagerIsNullException();
        }

        ImsUtInterface ut = ((MtkImsManager)mImsManager).getSupplementaryServiceConfiguration(
                mServiceId);
        return ut;
    }

    @Override
    protected ImsPhoneConnection makeImsPhoneConnectionForMO(String dialString, boolean
            isEmergencyNumber) {
        return new MtkImsPhoneConnection(mPhone, checkForTestEmergencyNumber(dialString), this,
                mForegroundCall, isEmergencyNumber);
    }

    @Override
    protected ImsPhoneConnection makeImsPhoneConnectionForMT(ImsCall imsCall, boolean isUnknown) {
        return new MtkImsPhoneConnection(mPhone, imsCall, this, (isUnknown ? mForegroundCall :
                mRingingCall), isUnknown);
    }

    @Override
    protected ImsCall takeCall(Intent intent) throws ImsException {
        return mImsManager.takeCall(mServiceId, intent, mMtkImsCallListener);
    }

    @Override
    protected void mtkNotifyRemoteHeld(ImsPhoneConnection conn, boolean held) {
        if (conn != null && conn instanceof MtkImsPhoneConnection) {
            ((MtkImsPhoneConnection) conn).notifyRemoteHeld(held);
        }
    }

    @Override
    protected boolean isEmergencyNumber(String dialString) {
        // CC: For 93, MD can switch phoneType when SIM not inserted,
        // TeleService won't trigger phone switch, so check both SIM's ECC
        if (hasC2kOverImsModem() &&
                !TelephonyManager.getDefault().hasIccCard(mPhone.getPhoneId())) {
            return PhoneNumberUtils.isEmergencyNumber(dialString);
        } else {
            // ALPS02399162
            // ECC number is different for CDMA and GSM. Should carry phone sub id to query if the
            // number is ECC or not
            return PhoneNumberUtils.isEmergencyNumber(mPhone.getSubId(), dialString);
        }
    }

    @Override
    protected void checkforCsfb() throws CallStateException {
        // ALPS02015368, it should use GSMPhone to dial during SRVCC.
        // When SRVCC, GSMPhone will continue the handover calls until disconnected.
        // It should use GSMPhone to dial to prevent PS/CS call conflict problem.
        // Throw CS_FALLBACK exception here will let GSMPhone to dial.
        if (mHandoverCall.mConnections.size() > 0) {
            log("SRVCC: there are connections during handover, trigger CSFB!");
            throw new CallStateException(ImsPhone.CS_FALLBACK);
        }

        // ALPS02015368 and ALPS02298554.
        /*
         * In ALPS02015368, we should use GSMPhone to dial after SRVCC happened, until the handover
         * calls end. In ALPS02298554, even the IMS over ePDG is connected, but there is a CS MT
         * call happened so we still need to use GSMPhone to dial.
         */
        if (mPhone != null && mPhone.getDefaultPhone() != null) {
            // default phone is GSMPhone or CDMAPhone, which owns the ImsPhone.
            Phone defaultPhone = mPhone.getDefaultPhone();
            if (defaultPhone.getState() != PhoneConstants.State.IDLE
                    && getState() == PhoneConstants.State.IDLE) {
                log("There are CS connections, trigger CSFB!");
                throw new CallStateException(ImsPhone.CS_FALLBACK);
            }
        }
    }

    @Override
    protected void resetFlagWhenSwitchFailed() {
        mSwitchingFgAndBgCalls = false;
        mCallExpectedToResume = null;
    }

    @Override
    protected void setPendingResumeRequest(boolean pendingResumeRequest) {
        if (pendingResumeRequest) {
            log("turn on the resuem pending request lock!");
        }
        mHasPendingResumeRequest = pendingResumeRequest;
    }

    @Override
    protected boolean hasPendingResumeRequest() {
        return mHasPendingResumeRequest;
    }

    @Override
    protected void setRedialAsEcc(int cause) {
        if (cause == MtkDisconnectCause.IMS_EMERGENCY_REREG) {
            mDialAsECC = true;
        }
    }

    @Override
    protected void setVendorDisconnectCause(ImsPhoneConnection conn, ImsReasonInfo reasonInfo) {
        // ALPS02501206. For OP07 requirement.
        if (conn instanceof MtkImsPhoneConnection) {
            ((MtkImsPhoneConnection) conn).setVendorDisconnectCause(
                    reasonInfo.getExtraMessage());
        }
    }

    @Override
    protected int updateDisconnectCause(int cause, ImsPhoneConnection conn ) {
        if (cause == DisconnectCause.ERROR_UNSPECIFIED && conn != null
                && conn.getImsCall().isMerged()) {
            // Call was terminated while it is merged instead of a remote disconnect.
            return DisconnectCause.IMS_MERGED_SUCCESSFULLY;
        }
        return cause;
    }

    @Override
    protected void setMultiPartyState(Connection c) {
        // M: for conference SRVCC.
        if (c instanceof MtkImsPhoneConnection) {
            ((MtkImsPhoneConnection)c).mWasMultiparty = c.isMultiparty();
            ((MtkImsPhoneConnection)c).mWasPreMultipartyHost = c.isConferenceHost();
            log("SRVCC: Connection isMultiparty is " +
                    ((MtkImsPhoneConnection)c).mWasMultiparty + "and isConfHost is " +
                    ((MtkImsPhoneConnection)c).mWasPreMultipartyHost + " before handover");
        }
    }

    @Override
    protected void resetRingBackTone(ImsPhoneCall call) {
        /// M: ALPS02589783 @{
        // If ringback tone flag is set for foreground call, after SRVCC it has no chance to reset.
        // We reset manually when handover happened.
        if (call instanceof MtkImsPhoneCall) {
            ((MtkImsPhoneCall)call).resetRingbackTone();
        }
        /// @}
    }

    @Override
    protected void updateForSrvccCompleted() {
        // ALPS02015368 mPendingMO should be cleared when fake SRVCC/bSRVCC happens,
        // or dial function will fail
        if (mPendingMO != null) {
            log("SRVCC: reset mPendingMO");
            removeConnection(mPendingMO);
            mPendingMO = null;
        }
        // ALPS02530061, after SRVCC complete, update phone state to listener.
        updatePhoneState();

        // reset the SRVCC state for SRVCC pending action handling.
        mSrvccState = Call.SrvccState.NONE;
    }

    /// M: MTK SS start @{
    // Assign return message for terminating USSI.
    public void cancelUSSD(Message response) {
        if (mUssdSession == null) return;
        mPendingUssd = response;
        try {
            mUssdSession.terminate(ImsReasonInfo.CODE_USER_TERMINATED);
        } catch (ImsException e) {
        }
    }
    /// @}

    @Override
    protected void checkIncomingRtt(Intent intent, ImsCall imsCall, ImsPhoneConnection conn) {
        mOpImsPhoneCallTracker.checkIncomingRtt(intent, imsCall, conn);
    }

    @Override
    protected void processRttModifyFailCase(int status, ImsPhoneConnection conn){
        mOpImsPhoneCallTracker.processRttModifyFailCase(status, conn);
    }
    @Override
    protected void checkRttCallType() {
        mOpImsPhoneCallTracker.checkRttCallType(mPhone, mForegroundCall, mSrvccState);
    }
    @Override
    protected void startRttEmcGuardTimer() {
        mOpImsPhoneCallTracker.startRttEmcGuardTimer(mPhone);
    }
}
