LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_JAVA_LIBRARIES := telephony-common
LOCAL_JAVA_LIBRARIES += mediatek-framework mediatek-common
LOCAL_JAVA_LIBRARIES += mediatek-telecom-common
LOCAL_JAVA_LIBRARIES += mediatek-telephony-common mediatek-telephony-base
LOCAL_JAVA_LIBRARIES += mediatek-ims-common ims-common

LOCAL_APK_LIBRARIES := TeleService

LOCAL_STATIC_JAVA_LIBRARIES := \
        guava \

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE := disabled_mediatek-packages-teleservice
LOCAL_MODULE_OWNER := mtk

LOCAL_PROGUARD_FLAG_FILES := proguard.flags

include $(BUILD_JAVA_LIBRARY)

