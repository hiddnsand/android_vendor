package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.widget.Button;
import android.widget.Spinner;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.video.VideoActivity;

import java.util.ArrayList;

public class VideoTest extends ActivityInstrumentationTestCase2<VideoActivity> {
    private static final String TAG = "EMTest/VideoTest";
    private Solo mSolo;
    private Context mContext;
    private Instrumentation mIns;
    private Activity mActivity;

    public VideoTest() {
        super("com.mediatek.engineermode", VideoActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mIns = getInstrumentation();
        mContext = mIns.getTargetContext();
        mActivity = getActivity();
        mSolo = new Solo(mIns, mActivity);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    private void verifyPreconditions() {
        assertTrue(mIns != null);
        assertTrue(mActivity != null);
        assertTrue(mContext != null);
        assertTrue(mSolo != null);
    }

    @BasicFuncAnnotation
    public void testCase01_VerifyPreconditions() {
        verifyPreconditions();
        mSolo.sleep(EmOperate.TIME_MID);
    }

    @UiAnnotation
    public void testCase02_CheckSpinnerItem() throws Exception {

        mSolo.assertCurrentActivity("Not VideoActivity Class", VideoActivity.class);

        mSolo.sleep(EmOperate.TIME_MID);
        mSolo.pressSpinnerItem(0, 1);
        mSolo.sleep(EmOperate.TIME_MID);
        mSolo.pressSpinnerItem(1, 2);

        mSolo.goBack();
    }

    @UiAnnotation
    public void testCase03_CheckSpinnerItemBack() throws Exception {

        mSolo.assertCurrentActivity("Not VideoActivity Class", VideoActivity.class);

        assertTrue("CheckSpinnerItemBack have issues", mSolo.waitForText("1"));
        assertTrue("CheckSpinnerItemBack have issues", mSolo.waitForText("2"));

        mSolo.sleep(EmOperate.TIME_MID);
        mSolo.pressSpinnerItem(0, -1);
        mSolo.sleep(EmOperate.TIME_MID);
        mSolo.pressSpinnerItem(1, -2);

        mSolo.goBack();
    }
}
