/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op01;

import android.content.Context;
import android.os.SystemProperties;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;

import com.mediatek.settings.ext.DefaultStatusExt;

/**
 * Device status info plugin.
 */
public class Op01StatusExt extends DefaultStatusExt {
    private static final String TAG = "Op01StatusExt";

    private static final String STRING_MEID = "MEID";
    private static final String KEY_MIN_NUMBER = "min_number";
    private static final String KEY_PRL_VERSION = "prl_version";
    private static final String KEY_MEID_NUMBER = "meid_number";
    private static final String KEY_IMEI = "imei";
    private static final String KEY_IMEI_SV = "imei_sv";
    private static final String KEY_ICC_ID = "icc_id";
    private static final String s_keyList[] = {
        KEY_MIN_NUMBER
        , KEY_PRL_VERSION
        , KEY_IMEI
        , KEY_IMEI_SV
        , KEY_ICC_ID
    };

    private Context mContext;

    /**
     * Init context.
     * @param context The Context
     */
    public Op01StatusExt(Context context) {
        super();
        mContext = context;
    }

    @Override
    public void customizeImei(String imeiKey, String imeiSvKey,
            PreferenceScreen parent, int slotId) {
        if (SystemProperties.get("ro.mtk_single_imei").equals("1")) {
            if (SystemProperties.get("ro.boot.opt_c2k_support").equals("1")) {
                String meidKey = "_" + KEY_MEID_NUMBER + String.valueOf(slotId);
                Preference meid = parent.findPreference(meidKey);
                if (meid != null) {
                    //for C+G or G+C, CDMAPhone only dispaly MEID
                    Log.d("@M_" + TAG, "meid:" + slotId);
                    // for meid, shall set title without slot id
                    meid.setTitle(STRING_MEID);
                    for (int i = 0; i < s_keyList.length; i++) {
                        String key = "_" + s_keyList[i] + String.valueOf(slotId);
                        Preference pref = parent.findPreference(key);
                        if (pref != null) {
                            parent.removePreference(pref);
                        }
                    }
                    return;
                }

                // G+G, sim2's imei shall be hiden
                if (slotId == 1) {
                    String imei1Key = "_" + KEY_IMEI + "0";
                    Preference imei1 = parent.findPreference(imei1Key);
                    if (imei1 != null) {
                        Log.d("@M_" + TAG, "G+G, hide imei2");
                        Preference imei = parent.findPreference(imeiKey);
                        Preference imeiSv = parent.findPreference(imeiSvKey);
                        if (imei != null) {
                            parent.removePreference(imei);
                        }
                        if (imeiSv != null) {
                            parent.removePreference(imeiSv);
                        }
                    }
                }

                // for imei and imeiSv, shall set title without slot id
                Preference imei = parent.findPreference(imeiKey);
                Preference imeiSv = parent.findPreference(imeiSvKey);
                if (imei != null) {
                    Log.d("@M_" + TAG, "imei:" + slotId);
                    imei.setTitle(mContext.getString(R.string.status_imei));
                }
                if (imeiSv != null) {
                    imeiSv.setTitle(mContext.getString(R.string.status_imei_sv));
                }
            } else {
                Preference imei = parent.findPreference(imeiKey);
                Preference imeiSv = parent.findPreference(imeiSvKey);
                if (slotId == 0) {
                    if (imei != null) {
                        imei.setTitle(mContext.getString(R.string.status_imei));
                    }
                    if (imeiSv != null) {
                        imeiSv.setTitle(mContext.getString(R.string.status_imei_sv));
                    }
                } else {
                    if (imei != null) {
                        parent.removePreference(imei);
                    }
                    if (imeiSv != null) {
                        parent.removePreference(imeiSv);
                    }
                }
            }
        }
    }

}
