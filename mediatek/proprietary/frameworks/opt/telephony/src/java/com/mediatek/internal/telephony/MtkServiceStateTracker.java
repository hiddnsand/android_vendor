/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.hardware.radio.V1_0.CellInfoType;
import android.hardware.radio.V1_0.DataRegStateResult;
import android.hardware.radio.V1_0.VoiceRegStateResult;
import android.os.AsyncResult;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.Build;
import android.os.SystemClock;
import android.os.Registrant;
import android.os.RegistrantList;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.Rlog;
import android.telephony.CarrierConfigManager;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.gsm.GsmCellLocation;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.SignalStrength;
import android.telephony.cdma.CdmaCellLocation;
import android.text.TextUtils;
import android.util.EventLog;
import android.util.TimeUtils;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.GsmCdmaPhone;
import com.android.internal.telephony.ServiceStateTracker;
import com.android.internal.telephony.uicc.IccRecords;
import com.android.internal.telephony.uicc.SIMRecords;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.EventLogTags;
import com.android.internal.telephony.MccTable;
import com.android.internal.telephony.metrics.TelephonyMetrics;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.TelephonyProperties;
import com.android.internal.telephony.cdma.CdmaSubscriptionSourceManager;
import com.android.internal.telephony.HbpcdUtils;
import com.android.internal.telephony.RestrictedState;
import com.android.internal.telephony.dataconnection.DcTracker;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.cdma.EriInfo;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.ProxyController;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.uicc.IccUtils;
import com.android.internal.telephony.uicc.IccRefreshResponse;
import com.android.internal.telephony.uicc.RuimRecords;
import com.android.internal.telephony.uicc.IccCardApplicationStatus.AppState;
import com.android.internal.telephony.uicc.UiccCardApplication;
import com.mediatek.internal.telephony.uicc.MtkIccRefreshResponse;
import com.mediatek.internal.telephony.uicc.MtkIccConstants;
import com.mediatek.internal.telephony.uicc.MtkSIMRecords;

import mediatek.telephony.MtkServiceState;
import mediatek.telephony.MtkCarrierConfigManager;

import com.mediatek.internal.telephony.MtkRIL;
import com.mediatek.internal.telephony.MtkTelephonyIntents;
import com.mediatek.internal.telephony.MtkRILConstants;
///M: [Network][C2K] Add CDMA plus code to parse special MCC/MNC. @{
import com.mediatek.internal.telephony.cdma.pluscode.IPlusCodeUtils;
import com.mediatek.internal.telephony.cdma.pluscode.PlusCodeProcessor;
import com.mediatek.internal.telephony.uicc.MtkSpnOverride;
///  @}
import com.mediatek.internal.telephony.RadioManager;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;
import com.mediatek.internal.telephony.OpTelephonyCustomizationFactoryBase;
import com.mediatek.internal.telephony.OpTelephonyCustomizationUtils;
import com.mediatek.internal.telephony.dataconnection.MtkDcTracker;
import com.mediatek.internal.telephony.uicc.MtkIccCardProxy;
import android.telephony.RadioAccessFamily;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Calendar;

import android.app.Notification;
import android.app.NotificationManager;

public class MtkServiceStateTracker extends ServiceStateTracker {
    private static final String LOG_TAG = "MTKSST";
    private static final boolean DBG = true;
    private static final boolean VDBG = true;  // STOPSHIP if true

    /// M: [Network][C2K] for EriTriggeredPollState @{
    // Always broadcast service state to trigger possible roaming state change
    private boolean mEriTriggeredPollState = false;
    /// @}
    private boolean mEnableERI = false;
    private OpTelephonyCustomizationFactoryBase mTelephonyCustomizationFactory = null;
    private IServiceStateTrackerExt mServiceStateTrackerExt = null;

    private RegistrantList mDataRoamingTypeChangedRegistrants = new RegistrantList();


    /* M: MTK added events begin */
    protected static final int EVENT_CS_NETWORK_STATE_CHANGED = 100;
    protected static final int EVENT_INVALID_SIM_INFO = 101; //ALPS00248788
    protected static final int EVENT_FEMTO_CELL_INFO = 102;
    protected static final int EVENT_PS_NETWORK_STATE_CHANGED = 103;
    protected static final int EVENT_NETWORK_EVENT = 104;
    protected static final int EVENT_MODULATION_INFO = 105;
    protected static final int EVENT_ICC_REFRESH = 106;
    protected static final int EVENT_IMEI_LOCK = 107; /* ALPS00296298 */
    protected static final int EVENT_SIM_OPL_LOADED = 119;
    protected static final int EVENT_RADIO_AVAILABLE = 200;
    /* MTK added events end */

    private Notification.Builder mNotificationBuilder;
    private Notification mNotification;
    public static final int REJECT_NOTIFICATION = 890;
    /** [ALPS01558804] Add notification id for using some spcial icc card*/
    public static final int SPECIAL_CARD_TYPE_NOTIFICATION = 8903;

    private String mSimType = "";
    private boolean mIsImeiLock = false;
    private String mLocatedPlmn = null;
    private int mPsRegState = ServiceState.STATE_OUT_OF_SERVICE;
    private int mPsRegStateRaw = ServiceState.RIL_REG_STATE_NOT_REG;
    private String mHhbName = null;
    private String mCsgId = null;
    private int mFemtocellDomain = 0;
    private int mIsFemtocell = 0;

    public boolean hasPendingPollState = false;

    private String mLastRegisteredPLMN = null;
    private String mLastPSRegisteredPLMN = null;
    private boolean mEverIVSR = false;  /* ALPS00324111: at least one chance to do IVSR  */
    private boolean isCsInvalidCard = false;
    private boolean mMtkVoiceCapable = mPhone.getContext().getResources().getBoolean(
            com.android.internal.R.bool.config_voice_capable);
    private int mLastPhoneGetNitz = SubscriptionManager.INVALID_PHONE_INDEX;

    // Read Speical Roaming Agreement from array instead of resource file.
    private String [][] operatorConsideredNonRoaming = {
        {"20412","20408"},
        {"20605","20610"},
        {"20610","20605"},
        {"20815","20801"},
        {"20826","20801","20810"},
        {"21402","21401","21402","21403","21404","21405","21406","21407","21408","21409","21410",
         "21411","21412","21413","21414","21415","21416","21417","21418","21419","21420","21421"},
        {"21404","21401","21402","21403","21405","21406","21407","21408","21409","21410","21411",
         "21412","21413","21414","21415","21416","21417","21418","21419","21420","21421"},
        {"21405","21401","21402","21403","21404","21406","21407","21408","21409","21410","21411",
         "21412","21413","21414","21415","21416","21417","21418","21419","21420","21421"},
        {"21406","21401","21402","21403","21404","21405","21407","21408","21409","21410","21411",
         "21412","21413","21414","21415","21416","21417","21418","21419","21420","21421"},
        {"21408","21401","21402","21403","21404","21405","21406","21407","21409","21410","21411",
         "21412","21413","21414","21415","21416","21417","21418","21419","21420","21421"},
        {"21409","21401","21402","21403","21404","21405","21406","21407","21408","21410","21411",
         "21412","21413","21414","21415","21416","21417","21418","21419","21420","21421"},
        {"21410","21401","21402","21403","21404","21405","21406","21407","21408","21409","21411",
         "21412","21413","21414","21415","21416","21417","21418","21419","21420","21421"},
        {"21411","21401","21402","21403","21404","21405","21406","21407","21408","21409","21410",
         "21412","21413","21414","21415","21416","21417","21418","21419","21420","21421"},
        {"21412","21401","21402","21403","21404","21405","21406","21407","21408","21409","21410",
         "21411","21413","21414","21415","21416","21417","21418","21419","21420","21421"},
        {"21413","21401","21402","21403","21404","21405","21406","21407","21408","21409","21410",
         "21411","21412","21414","21415","21416","21417","21418","21419","21420","21421"},
        {"21414","21401","21402","21403","21404","21405","21406","21407","21408","21409","21410",
         "21411","21412","21413","21415","21416","21417","21418","21419","21420","21421"},
        {"21415","21401","21402","21403","21404","21405","21406","21407","21408","21409","21410",
         "21411","21412","21413","21414","21416","21417","21418","21419","21420","21421"},
        {"21416","21401","21402","21403","21404","21405","21406","21407","21408","21409","21410",
         "21411","21412","21413","21414","21415","21417","21418","21419","21420","21421"},
        {"21417","21401","21402","21403","21404","21405","21406","21407","21408","21409","21410",
         "21411","21412","21413","21414","21415","21416","21418","21419","21420","21421"},
        {"21418","21401","21402","21403","21404","21405","21406","21407","21408","21409","21410",
         "21411","21412","21413","21414","21415","21416","21417","21419","21420","21421"},
        {"21419","21401","21402","21403","21404","21405","21406","21407","21408","21409","21410",
         "21411","21412","21413","21414","21415","21416","21417","21418","21420","21421"},
        {"21420","21401","21402","21403","21404","21405","21406","21407","21408","21409","21410",
         "21411","21412","21413","21414","21415","21416","21417","21418","21419","21421"},
        {"21421","21401","21402","21403","21404","21405","21406","21407","21408","21409","21410",
         "21411","21412","21413","21414","21415","21416","21417","21418","21419","21420"},
        {"21902","21901"},
        {"23210","23203","23205"},
        {"23211","23201"},
        {"23212","23201"},
        {"23213","23203"},
        {"23408","23430","23433"},
        {"23410","23420","23426","23430","23431","23432","23433","23434","23486"},
        {"23420","23410","23426","23430","23431","23432","23433","23434","23486"},
        {"23426","23410","23420","23430","23431","23432","23433","23434","23486"},
        {"23430","23430","23431","23432","23433","23434","23486"},
        {"23431","23430","23431","23432","23433","23434","23486"},
        {"23432","23430","23431","23432","23433","23434","23486"},
        {"23433","23430","23431","23432","23433","23434","23486"},
        {"23434","23430","23431","23432","23433","23434","23486"},
        {"23486","23430","23431","23432","23433","23434","23486"},
        {"302370","302220","302610","302660","302720","302780"},
        {"302500","302"},
        {"302510","302"},
        {"302610","302"},
        {"302640","302"},
        {"302660","302220","302370","302610","302720","302780"},
        {"302720","302220","302370","302610","302660","302780"},
        {"302780","302"},
        {"310150","310","311","312","313","314","315","316"},
        {"310170","310","311","312","313","314","315","316"},
        {"310380","310","311","312","313","314","315","316"},
        {"310410","310","311","312","313","314","315","316"},
        {"34001","20801","20815"},
        {"42507","42503"},
        {"42508","42502"},
        {"51021","51001"},
        {"72236","72234"},
        {"73001","73010"},
        {"73007","73002"},
        {"73008","73002"},
        {"73010","73001"}
    };

    private String [][] sameNamedOperatorConsideredRoaming = {
        {"310150","310110","310140","310400","310470","311170"},
        {"310170","310110","310140","310400","310470","311170"},
        {"310380","310110","310140","310400","310470","311170"},
        {"310410","310110","310140","310400","310470","311170"},
        {"404","404","405"},
        {"405","404","405"},
        {"520","520"},
        {"53024","53001"}
    };
    private int mLastOperatorConsideredNonRoamingIndex = -1;
    private int mLastSameNamedOperatorConsideredRoamingIndex = -1;

    // Mapping table from iso to time zone id of capital city.
    private String[][] mTimeZoneIdOfCapitalCity = {
        {"au", "Australia/Sydney"},
        {"br", "America/Sao_Paulo"},
        {"ca", "America/Toronto"},
        {"cl", "America/Santiago"},
        {"es", "Europe/Madrid"},
        {"fm", "Pacific/Ponape"},
        {"gl", "America/Godthab"},
        {"id", "Asia/Jakarta"},
        {"kz", "Asia/Almaty"},
        {"mn", "Asia/Ulaanbaatar"},
        {"mx", "America/Mexico_City"},
        {"pf", "Pacific/Tahiti"},
        {"pt", "Europe/Lisbon"},
        {"ru", "Europe/Moscow"},
        {"us", "America/New_York"},
        {"ec", "America/Guayaquil"},
        {"cn", "Asia/Shanghai"}
    };
    private String mSavedGuessTimeZone = null;
    ///M: [Network][C2K] Add CDMA plus code to parse special MCC/MNC. @{
    private IPlusCodeUtils mPlusCodeUtils = PlusCodeProcessor.getPlusCodeUtils();
    /// @}
    ///M: [Network][C2K] Record network existence state for calculate emergency call only state. @{
    private boolean mNetworkExsit = false;
    ///M @}
    ///M: Fix the operator info not update issue.
    private  boolean mForceBroadcastServiceState = false;

    private BroadcastReceiver mMtkIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(
                    CarrierConfigManager.ACTION_CARRIER_CONFIG_CHANGED)) {
                updateLteEarfcnLists();
                return;
            }

            if (!mPhone.isPhoneTypeGsm()) {
                /// M:[Network][C2K] When Sim status changed, should update mMdn to null @{
                if (intent.getAction().equals(TelephonyIntents.ACTION_SIM_STATE_CHANGED)) {
                    int phoneId = intent.getIntExtra(PhoneConstants.PHONE_KEY,
                            SubscriptionManager.INVALID_PHONE_INDEX);
                    String simStatus = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
                    log("[CDMA]phoneId:" + phoneId + ", mPhoneId:" + mPhone.getPhoneId()
                            + ", simStatus:" + (simStatus == null ? "null" : simStatus));
                    if (phoneId == mPhone.getPhoneId()) {
                        if (IccCardConstants.INTENT_VALUE_ICC_ABSENT.equals(simStatus)) {
                            mMdn = null;
                        }
                    }
                    return;
                }
                /// @}
                loge("Ignoring intent " + intent + " received on CDMA phone");
                return;
            }

            if (intent.getAction().equals(Intent.ACTION_LOCALE_CHANGED)) {
                // update emergency string whenever locale changed
                refreshSpn(mSS, mCellLoc);
                updateSpnDisplay();

                /// M: pollState() again to broadcast and update property if needed. @{
                if (mForceBroadcastServiceState) {
                    pollState();
                }
                /// @}
            } else if (intent.getAction().equals(ACTION_RADIO_OFF)) {
                mAlarmSwitch = false;
                DcTracker dcTracker = mPhone.mDcTracker;
                powerOffRadioSafely(dcTracker);
            } else if (intent.getAction().equals(TelephonyIntents.ACTION_SIM_STATE_CHANGED)) {
                String simState = IccCardConstants.INTENT_VALUE_ICC_UNKNOWN;

                int slotId = intent.getIntExtra(PhoneConstants.PHONE_KEY, -1);
                if (slotId == mPhone.getPhoneId()) {
                    simState = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
                    log("SIM state change, slotId: " + slotId + " simState[" + simState + "]");
                }

                // [ALPS01558804] MTK-START: send notification for using some spcial icc card
                if ((simState.equals(IccCardConstants.INTENT_VALUE_ICC_READY))
                        && (mSimType.equals(""))) {
                    mSimType = ((MtkIccCardProxy) mPhone.getIccCard()).getIccCardType();

                    log("SimType= " + mSimType);

                    if ((mSimType != null) && (!mSimType.equals(""))) {
                        if (mSimType.equals("SIM") || mSimType.equals("USIM")) {
                            try {
                                if (mServiceStateTrackerExt.
                                        needIccCardTypeNotification(mSimType)) {
                                    // [ALPS01600557] - start : need to check 3G Capability SIM
                                    if (TelephonyManager.getDefault().getPhoneCount() > 1) {
                                        int raf = mPhone.getRadioAccessFamily();
                                        log("check RAF=" + raf);
                                        if ((raf&RadioAccessFamily.RAF_LTE)
                                                == RadioAccessFamily.RAF_LTE) {
                                            setSpecialCardTypeNotification(mSimType, 0, 0);
                                        }
                                    } else {
                                        setSpecialCardTypeNotification(mSimType, 0, 0);
                                    }
                                }
                            } catch (RuntimeException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                if (simState.equals(IccCardConstants.INTENT_VALUE_ICC_ABSENT) ||
                        simState.equals(IccCardConstants.INTENT_VALUE_ICC_NOT_READY)) {
                    mSimType = "";
                    int raf = mPhone.getRadioAccessFamily();
                    if ((raf&RadioAccessFamily.RAF_LTE) == RadioAccessFamily.RAF_LTE) {
                        NotificationManager notificationManager = (NotificationManager)
                            context.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.cancel(SPECIAL_CARD_TYPE_NOTIFICATION);
                    }
                    // M: IVSR
                    mLastRegisteredPLMN = null;
                    mLastPSRegisteredPLMN = null;
                }
            } // [ALPS01558804] MTK-END: send notification for using some special icc card
        }
    };

    public MtkServiceStateTracker(GsmCdmaPhone phone, CommandsInterface ci) {
        super(phone, ci);

        /// M: unregister AOSP broadcast receiver. @{
        Context context = mPhone.getContext();
        context.unregisterReceiver(mIntentReceiver);
        /// @}

        // Add MTK interested intents
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_LOCALE_CHANGED);
        context.registerReceiver(mMtkIntentReceiver, filter);
        filter = new IntentFilter();
        filter.addAction(ACTION_RADIO_OFF);
        context.registerReceiver(mMtkIntentReceiver, filter);
        filter = new IntentFilter();
        filter.addAction(CarrierConfigManager.ACTION_CARRIER_CONFIG_CHANGED);
        context.registerReceiver(mMtkIntentReceiver, filter);
        /// M: [Network][C2K] add for CDMA update MDN when SIM changed. @{
        filter = new IntentFilter();
        filter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        context.registerReceiver(mMtkIntentReceiver, filter);
        /// @}

        try {
            mTelephonyCustomizationFactory =
                    OpTelephonyCustomizationUtils.getOpFactory(mPhone.getContext());
            mServiceStateTrackerExt =
                    mTelephonyCustomizationFactory.makeServiceStateTrackerExt(mPhone.getContext());
        } catch (Exception e) {
            if (DBG) log("mServiceStateTrackerExt init fail");
            e.printStackTrace();
        }
    }

    @Override
    public void updatePhoneType() {
        mSS = new MtkServiceState();
        mNewSS = new MtkServiceState();
        mLastCellInfoListTime = 0;
        mLastCellInfoList = null;
        mSignalStrength = new SignalStrength();
        mRestrictedState = new RestrictedState();
        mStartedGprsRegCheck = false;
        mReportedGprsNoReg = false;
        mMdn = null;
        mMin = null;
        mPrlVersion = null;
        mIsMinInfoReady = false;
        mNitzUpdatedTime = false;

        //cancel any pending pollstate request on voice tech switching
        cancelPollState();
        // M: MTK added  ALPS02974868
        ((MtkRIL)mCi).registerForCsNetworkStateChanged(this, EVENT_CS_NETWORK_STATE_CHANGED, null);
        if (mPhone.isPhoneTypeGsm()) {
            //clear CDMA registrations first
            if (mCdmaSSM != null) {
                mCdmaSSM.dispose(this);
            }

            mCi.unregisterForCdmaPrlChanged(this);
            mPhone.unregisterForEriFileLoaded(this);
            mCi.unregisterForCdmaOtaProvision(this);
            mPhone.unregisterForSimRecordsLoaded(this);

            mCellLoc = new GsmCellLocation();
            mNewCellLoc = new GsmCellLocation();
            mCi.registerForAvailable(this, EVENT_RADIO_AVAILABLE, null);
            mCi.setOnRestrictedStateChanged(this, EVENT_RESTRICTED_STATE_CHANGED, null);

            // M: MTK added
            ((MtkRIL)mCi).registerForPsNetworkStateChanged(
                    this, EVENT_PS_NETWORK_STATE_CHANGED, null);
            ((MtkRIL)mCi).setInvalidSimInfo(this, EVENT_INVALID_SIM_INFO, null);
            ((MtkRIL)mCi).registerForIccRefresh(this, EVENT_ICC_REFRESH, null);
            ((MtkRIL)mCi).registerForNetworkEvent(this, EVENT_NETWORK_EVENT, null);
            ((MtkRIL)mCi).registerForModulation(this, EVENT_MODULATION_INFO, null);

            if (SystemProperties.get("ro.mtk_femto_cell_support").equals("1")) {
                ((MtkRIL) mCi).registerForFemtoCellInfo(this, EVENT_FEMTO_CELL_INFO, null);
            }

            try {
                if (mServiceStateTrackerExt != null &&
                        mServiceStateTrackerExt.isImeiLocked())
                   ((MtkRIL)mCi).registerForIMEILock(this, EVENT_IMEI_LOCK, null);
            } catch (RuntimeException e) {
               /* BSP must exception here but Turnkey should not exception here */
               loge("No isImeiLocked");
            }

        } else {
            //clear GSM regsitrations first
            mCi.unregisterForAvailable(this);
            mCi.unSetOnRestrictedStateChanged(this);
            ///M: [Network][C2K] Reset Restricted State. @{
            mPsRestrictDisabledRegistrants.notifyRegistrants();
            /// @}
            /// M: [Network][C2K] Clean CDMA event listener  @{
            mCi.unregisterForCdmaPrlChanged(this);
            mPhone.unregisterForEriFileLoaded(this);
            mCi.unregisterForCdmaOtaProvision(this);
            mPhone.unregisterForSimRecordsLoaded(this);
            /// @}

            ((MtkRIL)mCi).unregisterForIccRefresh(this);
            ((MtkRIL)mCi).unregisterForPsNetworkStateChanged(this);
            ((MtkRIL)mCi).unSetInvalidSimInfo(this);
            ((MtkRIL)mCi).unregisterForNetworkEvent(this);
            ((MtkRIL)mCi).unregisterForModulation(this);

            try {
                if (mServiceStateTrackerExt != null &&
                        mServiceStateTrackerExt.isImeiLocked())
                    ((MtkRIL)mCi).unregisterForIMEILock(this);
            } catch (RuntimeException e) {
                /* BSP must exception here but Turnkey should not exception here */
                loge("No isImeiLocked");
            }

            if (mPhone.isPhoneTypeCdmaLte()) {
                mPhone.registerForSimRecordsLoaded(this, EVENT_SIM_RECORDS_LOADED, null);
            }
            mCellLoc = new CdmaCellLocation();
            mNewCellLoc = new CdmaCellLocation();
            mCdmaSSM = CdmaSubscriptionSourceManager.getInstance(mPhone.getContext(), mCi, this,
                    EVENT_CDMA_SUBSCRIPTION_SOURCE_CHANGED, null);
            mIsSubscriptionFromRuim = (mCdmaSSM.getCdmaSubscriptionSource() ==
                    CdmaSubscriptionSourceManager.SUBSCRIPTION_FROM_RUIM);

            mCi.registerForCdmaPrlChanged(this, EVENT_CDMA_PRL_VERSION_CHANGED, null);
            mPhone.registerForEriFileLoaded(this, EVENT_ERI_FILE_LOADED, null);
            mCi.registerForCdmaOtaProvision(this, EVENT_OTA_PROVISION_STATUS_CHANGE, null);

            mHbpcdUtils = new HbpcdUtils(mPhone.getContext());
            // update OTASP state in case previously set by another service
            updateOtaspState();
        }

        // This should be done after the technology specific initializations above since it relies
        // on fields like mIsSubscriptionFromRuim (which is updated above)
        onUpdateIccAvailability();

        mPhone.setSystemProperty(TelephonyProperties.PROPERTY_DATA_NETWORK_TYPE,
                ServiceState.rilRadioTechnologyToString(ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN));
        // Query signal strength from the modem after service tracker is created (i.e. boot up,
        // switching between GSM and CDMA phone), because the unsolicited signal strength
        // information might come late or even never come. This will get the accurate signal
        // strength information displayed on the UI.
        // Remove AOSP getSignalStrength() here because during makeDefaultPhone(),
        // radioProxy might not available, but not null as well.
        // getSignalStrength() will get exception but error handle need default phone,
        // which is not ready yet. If issue happened, suggest to invoke after radio on.
        // mCi.getSignalStrength(obtainMessage(EVENT_GET_SIGNAL_STRENGTH));
        sendMessage(obtainMessage(EVENT_PHONE_TYPE_SWITCHED));

        logPhoneTypeChange();

        // Tell everybody that we've thrown away state and are starting over with
        // empty, detached ServiceStates.
        mVoiceRoamingOffRegistrants.notifyRegistrants();
        mDataRoamingOffRegistrants.notifyRegistrants();
        mDetachedRegistrants.notifyRegistrants();
        notifyDataRegStateRilRadioTechnologyChanged();
    }

    /**
     * Registration point for roaming type changed of mobile data
     * notify when data roaming is true and roaming type differs the previous
     *
     * @param h handler to notify
     * @param what what code of message when delivered
     * @param obj placed in Message.obj
     */
    public void registerForDataRoamingTypeChange(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mDataRoamingTypeChangedRegistrants.add(r);
    }

    public void unregisterForDataRoamingTypeChange(Handler h) {
        mDataRoamingTypeChangedRegistrants.remove(h);
    }

    @Override
    public void dispose() {
        super.dispose();
        // M:
        ((MtkRIL)mCi).unregisterForCsNetworkStateChanged(this);
        if (mPhone.isPhoneTypeGsm()) {
            ((MtkRIL)mCi).unregisterForIccRefresh(this);
            ((MtkRIL)mCi).unregisterForPsNetworkStateChanged(this);
            ((MtkRIL)mCi).unSetInvalidSimInfo(this);
            ((MtkRIL)mCi).unregisterForNetworkEvent(this);
            ((MtkRIL)mCi).unregisterForModulation(this);

            try {
                if (mServiceStateTrackerExt.isImeiLocked())
                    ((MtkRIL)mCi).unregisterForIMEILock(this);
            } catch (RuntimeException e) {
                /* BSP must exception here but Turnkey should not exception here */
                loge("No isImeiLocked");
            }
        }
    }

    @Override
    public void handleMessage(Message msg) {
        AsyncResult ar;
        int[] ints;
        Message message;

        if (VDBG) log("received event " + msg.what);
        switch (msg.what) {
            case EVENT_SIM_RECORDS_LOADED:
                if (mPhone.isPhoneTypeGsm()) {
                    refreshSpn(mSS, mCellLoc);
                }
                super.handleMessage(msg);

                /// M: pollState() again to broadcast and update property if needed. @{
                if (mForceBroadcastServiceState) {
                    pollState();
                }
                /// @}
                break;

            case EVENT_PS_NETWORK_STATE_CHANGED:
                if (mPhone.isPhoneTypeGsm()) {
                    ar = (AsyncResult) msg.obj;
                    onPsNetworkStateChangeResult(ar);
                }
                break;

            case EVENT_ERI_FILE_LOADED:
                /// M: [Network][C2K] for EriTriggeredPollState @{
                mEriTriggeredPollState = true;
                /// @}
                super.handleMessage(msg);
                break;
             case EVENT_RUIM_READY:
                /// M: [Network][C2K] set Ruim subscription as true, when RUIM ready. @{
                if (mPhone.isPhoneTypeCdma() || mPhone.isPhoneTypeCdmaLte()) {
                    mIsSubscriptionFromRuim = true;
                }
                // replace LTE_ON_CDMA mode since google not support CDMA only case. @{
                if (mPhone.isPhoneTypeCdmaLte()) {
                // @}
                    // Subscription will be read from SIM I/O
                    if (DBG) log("Receive EVENT_RUIM_READY");
                    pollState();
                } else {
                    if (DBG) log("Receive EVENT_RUIM_READY and Send Request getCDMASubscription.");
                    getSubscriptionInfoAndStartPollingThreads();
                }
                // Only support automatic selection mode in CDMA.
                mCi.getNetworkSelectionMode(obtainMessage(EVENT_POLL_STATE_NETWORK_SELECTION_MODE));
                break;
                /// @}
            case EVENT_CS_NETWORK_STATE_CHANGED:
                ar = (AsyncResult) msg.obj;
                onNetworkStateChangeResult(ar);
                break;
            case EVENT_RADIO_AVAILABLE:
                log("handle EVENT_RADIO_AVAILABLE");
                //check if we boot up under airplane mode
                RadioManager.getInstance().notifyRadioAvailable(mPhone.getPhoneId());
                break;
            case EVENT_NETWORK_EVENT:
                if (mPhone.isPhoneTypeGsm()) {
                    ar = (AsyncResult) msg.obj;
                    onNetworkEventReceived(ar);
                }
                break;
            case EVENT_MODULATION_INFO:
                if (mPhone.isPhoneTypeGsm()) {
                    ar = (AsyncResult) msg.obj;
                    onModulationInfoReceived(ar);
                }
                break;
            case EVENT_IMEI_LOCK: //ALPS00296298
                if (mPhone.isPhoneTypeGsm()) {
                    log("handle EVENT_IMEI_LOCK GSM");
                    mIsImeiLock = true;
                }
                break;
            case EVENT_ICC_REFRESH:
                if (mPhone.isPhoneTypeGsm()) {
                    ar = (AsyncResult) msg.obj;
                    if (ar.exception == null) {
                        IccRefreshResponse res = ((IccRefreshResponse) ar.result);
                        if (res == null) {
                            log("IccRefreshResponse is null");
                            break;
                        }
                        switch (res.refreshResult) {
                            case MtkIccRefreshResponse.REFRESH_INIT_FULL_FILE_UPDATED:
                            case MtkIccRefreshResponse.REFRESH_SESSION_RESET:
                                // NAA session Reset only applicable for a 3G platform
                                 /* ALPS00949490 */
                                 mLastRegisteredPLMN = null;
                                 mLastPSRegisteredPLMN = null;
                                 log("Reset mLastRegisteredPLMN/mLastPSRegisteredPLMN"
                                         + "for ICC refresh");
                                 break;
                            case IccRefreshResponse.REFRESH_RESULT_FILE_UPDATE:
                            case MtkIccRefreshResponse.REFRESH_INIT_FILE_UPDATED:
                                 if (res.efId == MtkIccConstants.EF_IMSI) {
                                     mLastRegisteredPLMN = null;
                                     mLastPSRegisteredPLMN = null;
                                     log("Reset flag of IVSR for IMSI update");
                                     break;
                                 }
                                 break;
                            default:
                                 log("GSST EVENT_ICC_REFRESH IccRefreshResponse =" + res);
                                 break;
                        }
                    }
                }
                break;
            case EVENT_INVALID_SIM_INFO: //ALPS00248788
                if (mPhone.isPhoneTypeGsm()) {
                    ar = (AsyncResult) msg.obj;
                    onInvalidSimInfoReceived(ar);
                }
                break;

            case EVENT_FEMTO_CELL_INFO:
                ar = (AsyncResult) msg.obj;
                onFemtoCellInfoResult(ar);
                break;

            case EVENT_RADIO_STATE_CHANGED:
            case EVENT_PHONE_TYPE_SWITCHED:
                log("handle EVENT_RADIO_STATE_CHANGED");
                if(!mPhone.isPhoneTypeGsm() &&
                        mCi.getRadioState() == CommandsInterface.RadioState.RADIO_ON) {
                    handleCdmaSubscriptionSource(mCdmaSSM.getCdmaSubscriptionSource());

                    // Signal strength polling stops when radio is off.
                    queueNextSignalStrengthPoll();
                }

                // This will do nothing in the 'radio not available' case
                // setPowerStateToDesired();
                RadioManager.getInstance().setRadioPower(mDesiredPowerState, mPhone.getPhoneId());
                // These events are modem triggered, so pollState() needs to be forced
                modemTriggeredPollState();
                break;
            case EVENT_NITZ_TIME:
                // M: only last phone which get NITZ can update time
                mLastPhoneGetNitz = mPhone.getPhoneId();
                super.handleMessage(msg);
                break;
            // M @{
            case EVENT_ALL_DATA_DISCONNECTED:
                if (SubscriptionManager.getDefaultDataSubscriptionId()
                        == SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
                    int[] subIds = SubscriptionManager.getSubId(
                            RadioCapabilitySwitchUtil.getMainCapabilityPhoneId());
                    if (subIds != null && subIds.length > 0) {
                        ProxyController.getInstance().unregisterForAllDataDisconnected(
                                subIds[0], this);
                    }
                }
                mPhone.unregisterForAllDataDisconnected(this);
                super.handleMessage(msg);
                break;
            // GSM only event.
            case EVENT_SIM_OPL_LOADED:
                ar = (AsyncResult)msg.obj;
                if ((ar != null) && (ar.result != null)) {
                    Integer id = (Integer)ar.result;
                    log("EVENT_SIM_OPL_LOADED: id=" + id);
                    if (id.intValue() == MtkSIMRecords.EVENT_OPL) {
                        if (mPhone.isPhoneTypeGsm()) {
                            refreshSpn(mSS, mCellLoc);
                            if (mForceBroadcastServiceState) {
                                pollState();
                            }
                        } else {
                            loge("EVENT_SIM_OPL_LOADED should not be here");
                        }
                    }
                } else {
                    loge("EVENT_SIM_OPL_LOADED obj is null");
                }
                break;
            // M @}
            default:
                // log("Unhandled message with number: " + msg.what);
                super.handleMessage(msg);
                break;
        }
    }

    void handlePollStateResultMessage(int what, AsyncResult ar) {
        int ints[];
        switch (what) {
            case EVENT_POLL_STATE_REGISTRATION: {
                VoiceRegStateResult voiceRegStateResult = (VoiceRegStateResult) ar.result;
                int registrationState = getRegStateFromHalRegState(voiceRegStateResult.regState);

                mNewSS.setVoiceRegState(regCodeToServiceState(registrationState));
                mNewSS.setRilVoiceRadioTechnology(voiceRegStateResult.rat);
                /// M: [Network] add ril voice state for world phone. @{
                ((MtkServiceState)mNewSS).setRilVoiceRegState(registrationState);
                /// @}

                //Denial reason if registrationState = 3
                int reasonForDenial = voiceRegStateResult.reasonForDenial;
                if (mPhone.isPhoneTypeGsm()) {

                    mGsmRoaming = regCodeIsRoaming(registrationState);

                    boolean isVoiceCapable = mPhone.getContext().getResources()
                            .getBoolean(com.android.internal.R.bool.config_voice_capable);
                    if (((registrationState
                            == ServiceState.RIL_REG_STATE_DENIED_EMERGENCY_CALL_ENABLED)
                            || (registrationState
                            == ServiceState.RIL_REG_STATE_NOT_REG_EMERGENCY_CALL_ENABLED)
                            || (registrationState
                            == ServiceState.RIL_REG_STATE_SEARCHING_EMERGENCY_CALL_ENABLED)
                            || (registrationState
                            == ServiceState.RIL_REG_STATE_UNKNOWN_EMERGENCY_CALL_ENABLED))
                            && isVoiceCapable) {
                        mEmergencyOnly = true;
                    } else {
                        mEmergencyOnly = false;
                    }
                } else {
                    //init with 0, because it is treated as a boolean
                    int cssIndicator = voiceRegStateResult.cssSupported ? 1 : 0;
                    int roamingIndicator = voiceRegStateResult.roamingIndicator;

                    //Indicates if current system is in PR
                    int systemIsInPrl = voiceRegStateResult.systemIsInPrl;

                    //Is default roaming indicator from PRL
                    int defaultRoamingIndicator = voiceRegStateResult.defaultRoamingIndicator;

                    mRegistrationState = registrationState;
                    // When registration state is roaming and TSB58
                    // roaming indicator is not in the carrier-specified
                    // list of ERIs for home system, mCdmaRoaming is true.
                    boolean cdmaRoaming =
                            regCodeIsRoaming(registrationState)
                                    && !isRoamIndForHomeSystem(
                                            Integer.toString(roamingIndicator));
                    mNewSS.setVoiceRoaming(cdmaRoaming);
                    /// M: [Network][C2K]  add ril voice state for world phone. @{
                    if (cdmaRoaming) {
                        ((MtkServiceState)mNewSS).setRilVoiceRegState(
                                    ServiceState.RIL_REG_STATE_ROAMING);
                    }
                    /// @}
                    mNewSS.setCssIndicator(cssIndicator);
                    mRoamingIndicator = roamingIndicator;
                    mIsInPrl = (systemIsInPrl == 0) ? false : true;
                    mDefaultRoamingIndicator = defaultRoamingIndicator;

                    int systemId = 0;
                    int networkId = 0;
                    if (voiceRegStateResult.cellIdentity.cellInfoType == CellInfoType.CDMA
                            && voiceRegStateResult.cellIdentity.cellIdentityCdma.size() == 1) {
                        android.hardware.radio.V1_0.CellIdentityCdma cellIdentityCdma =
                                voiceRegStateResult.cellIdentity.cellIdentityCdma.get(0);
                        systemId = cellIdentityCdma.systemId;
                        networkId = cellIdentityCdma.networkId;
                    }
                    mNewSS.setSystemAndNetworkId(systemId, networkId);

                    if (reasonForDenial == 0) {
                        mRegistrationDeniedReason = ServiceStateTracker.REGISTRATION_DENIED_GEN;
                    } else if (reasonForDenial == 1) {
                        mRegistrationDeniedReason = ServiceStateTracker.REGISTRATION_DENIED_AUTH;
                    } else {
                        mRegistrationDeniedReason = "";
                    }

                    if (mRegistrationState == 3) {
                        if (DBG) log("Registration denied, " + mRegistrationDeniedReason);
                    }
                }

                processCellLocationInfo(mNewCellLoc, voiceRegStateResult);

                if (DBG) {
                    log("handlPollVoiceRegResultMessage: regState=" + registrationState
                            + " radioTechnology=" + voiceRegStateResult.rat);
                }
                break;
            }

            case EVENT_POLL_STATE_GPRS: {
                DataRegStateResult dataRegStateResult = (DataRegStateResult) ar.result;
                int regState = getRegStateFromHalRegState(dataRegStateResult.regState);
                int dataRegState = regCodeToServiceState(regState);
                int newDataRat = dataRegStateResult.rat;
                mNewSS.setDataRegState(dataRegState);
                mNewSS.setRilDataRadioTechnology(newDataRat);
                /// M: [Network] Add for world phone get data registration state. @{
                ((MtkServiceState)mNewSS).setRilDataRegState(regState);
                /// @}

                if (mPhone.isPhoneTypeGsm()) {

                    mNewReasonDataDenied = dataRegStateResult.reasonDataDenied;
                    mNewMaxDataCalls = dataRegStateResult.maxDataCalls;
                    mDataRoaming = regCodeIsRoaming(regState);

                    if (DBG) {
                        log("handlPollStateResultMessage: GsmSST setDataRegState=" + dataRegState
                                + " regState=" + regState
                                + " dataRadioTechnology=" + newDataRat);
                    }
                } else if (mPhone.isPhoneTypeCdma()) {
                    boolean isDataRoaming = regCodeIsRoaming(regState);
                    mNewSS.setDataRoaming(isDataRoaming);
                    // Save the data roaming state reported by modem registration before resource
                    // overlay or carrier config possibly overrides it.
                    mNewSS.setDataRoamingFromRegistration(isDataRoaming);

                    if (DBG) {
                        log("handlPollStateResultMessage: cdma setDataRegState=" + dataRegState
                                + " regState=" + regState
                                + " dataRadioTechnology=" + newDataRat);
                    }
                } else {

                    // If the unsolicited signal strength comes just before data RAT family changes
                    // (i.e. from UNKNOWN to LTE, CDMA to LTE, LTE to CDMA), the signal bar might
                    // display the wrong information until the next unsolicited signal strength
                    // information coming from the modem, which might take a long time to come or
                    // even not come at all.  In order to provide the best user experience, we
                    // query the latest signal information so it will show up on the UI on time.
                    int oldDataRAT = mSS.getRilDataRadioTechnology();
                    if (((oldDataRAT == ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN)
                            && (newDataRat != ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN))
                            || (ServiceState.isCdma(oldDataRAT) && ServiceState.isLte(newDataRat))
                            || (ServiceState.isLte(oldDataRAT)
                            && ServiceState.isCdma(newDataRat))) {
                        mCi.getSignalStrength(obtainMessage(EVENT_GET_SIGNAL_STRENGTH));
                    }
                    /// M: [Network][C2K] Add for world phone and 4G+ icon. @{
                    if (regCodeIsRoaming(regState)) {
                        ((MtkServiceState)mNewSS).setRilDataRegState(
                                    ServiceState.RIL_REG_STATE_ROAMING);
                    }
                    // get data type for AP to update 4G+ icon
                    ((MtkServiceState)mNewSS).setProprietaryDataRadioTechnology(newDataRat);
                    /// @}
                    // voice roaming state in done while handling EVENT_POLL_STATE_REGISTRATION_CDMA
                    boolean isDataRoaming = regCodeIsRoaming(regState);
                    mNewSS.setDataRoaming(isDataRoaming);
                    // Save the data roaming state reported by modem registration before resource
                    // overlay or carrier config possibly overrides it.
                    mNewSS.setDataRoamingFromRegistration(isDataRoaming);
                    if (DBG) {
                        log("handlPollStateResultMessage: CdmaLteSST setDataRegState="
                                + dataRegState + " regState=" + regState + " dataRadioTechnology="
                                + newDataRat);
                    }
                }

                updateServiceStateLteEarfcnBoost(mNewSS, getLteEarfcn(dataRegStateResult));
                break;
            }

            case EVENT_POLL_STATE_OPERATOR: {
                if (mPhone.isPhoneTypeGsm()) {
                    String opNames[] = (String[]) ar.result;

                    if (opNames != null && opNames.length >= 3) {
                        // FIXME: Giving brandOverride higher precedence, is this desired?
                        String brandOverride = mUiccController.getUiccCard(getPhoneId()) != null ?
                                mUiccController.getUiccCard(getPhoneId())
                                .getOperatorBrandOverride() : null;
                        if (brandOverride != null) {
                            log("EVENT_POLL_STATE_OPERATOR: use brandOverride=" + brandOverride);
                            mNewSS.setOperatorName(brandOverride, brandOverride, opNames[2]);
                        // M: START Out of service or Limited service
                        } else if (opNames[0].equals("") && opNames[1].equals("")) {
                            if (opNames[2].equals("")) {
                                // OOS, reset the opNames[2] = null for updateLocatedPlmn
                                opNames[2] = null;
                            } // else keep opNames[2] for limited service
                            // null is as the same as ServiceState.setStateOutOfService
                            mNewSS.setOperatorName(null, null, null);
                        // M: END
                        } else {
                            mNewSS.setOperatorName(opNames[0], opNames[1], opNames[2]);
                        }
                        updateLocatedPlmn(opNames[2]);
                    }
                } else {
                    String opNames[] = (String[])ar.result;

                    if (opNames != null && opNames.length >= 3) {
                        // TODO: Do we care about overriding in this case.
                        // If the NUMERIC field isn't valid use PROPERTY_CDMA_HOME_OPERATOR_NUMERIC
                        if ((opNames[2] == null) || (opNames[2].length() < 5)
                                || ("00000".equals(opNames[2]))
                                ///M: [Network][C2K] change for invalid MCC/MNC and fix it to "". @{
                                || ("N/AN/A".equals(opNames[2]))) {
                            opNames[2] = SystemProperties.get(
                                    GsmCdmaPhone.PROPERTY_CDMA_HOME_OPERATOR_NUMERIC, "");
                            ///  @}
                            if (DBG) {
                                log("RIL_REQUEST_OPERATOR.response[2], the numeric, " +
                                        " is bad. Using SystemProperties '" +
                                        GsmCdmaPhone.PROPERTY_CDMA_HOME_OPERATOR_NUMERIC +
                                        "'= " + opNames[2]);
                            }
                        }
                        ///M: [Network][C2K] Use CDMA plus code to handle special MCC/MNC. @{
                        String numeric = opNames[2];
                        boolean plusCode = false;
                        if (numeric != null && numeric.startsWith("2134") &&
                                numeric.length() == 7) {
                            String tempStr = mPlusCodeUtils.checkMccBySidLtmOff(numeric);
                            if (!tempStr.equals("0")) {
                                opNames[2] = tempStr + numeric.substring(4);
                                numeric = tempStr;
                                log("EVENT_POLL_STATE_OPERATOR_CDMA: checkMccBySidLtmOff: numeric ="
                                        + numeric + ", plmn =" + opNames[2]);
                            }
                            plusCode = true;
                        }
                        /// @}
                        if (!mIsSubscriptionFromRuim) {
                            // NV device (as opposed to CSIM)
                            ///M: [Network][C2K] handle special MCC/MNC. @{
                            if (plusCode) {
                                opNames[1] = lookupOperatorName(mPhone.getContext(),
                                        mPhone.getSubId(), opNames[2], false);
                            }
                            mNewSS.setOperatorName(null, opNames[1], opNames[2]);
                            /// @}
                        } else {
                            String brandOverride = mUiccController.getUiccCard(getPhoneId())
                                    != null ? mUiccController.getUiccCard(getPhoneId())
                                    .getOperatorBrandOverride() : null;
                            if (brandOverride != null) {
                                ///M: [Network][C2K] Add log. @{
                                log("EVENT_POLL_STATE_OPERATOR_CDMA: use brand=" + brandOverride);
                                ///  @}
                                mNewSS.setOperatorName(brandOverride, brandOverride, opNames[2]);
                            } else {
                                mNewSS.setOperatorName(opNames[0], opNames[1], opNames[2]);
                            }
                        }
                        ///M: [Network][C2K] Add updateLocatedPlmn. @{
                        if (opNames[2].length() >= 5 && !(opNames[2].equals("000000"))) {
                            updateLocatedPlmn(opNames[2]);
                        } else {
                            updateLocatedPlmn(null);
                        }
                        ///  @}
                    } else {
                        if (DBG) log("EVENT_POLL_STATE_OPERATOR_CDMA: error parsing opNames");
                    }
                }
                break;
            }

            case EVENT_POLL_STATE_NETWORK_SELECTION_MODE: {
                ints = (int[])ar.result;
                mNewSS.setIsManualSelection(ints[0] == 1);
                if ((ints[0] == 1) && (mPhone.shouldForceAutoNetworkSelect())) {
                        /*
                         * modem is currently in manual selection but manual
                         * selection is not allowed in the current mode so
                         * switch to automatic registration
                         */
                    mPhone.setNetworkSelectionModeAutomatic (null);
                    log(" Forcing Automatic Network Selection, " +
                            "manual selection is not allowed");
                }
                break;
            }

            default:
                loge("handlePollStateResultMessage: Unexpected RIL response received: " + what);
        }
    }

    /**
     * Query the carrier configuration to determine if there any network overrides
     * for roaming or not roaming for the current service state.
     */
    @Override
    protected void updateRoamingState() {
        if (mPhone.isPhoneTypeGsm()) {
            /**
             * Since the roaming state of gsm service (from +CREG) and
             * data service (from +CGREG) could be different, the new SS
             * is set to roaming when either is true.
             *
             * There are exceptions for the above rule.
             * The new SS is not set as roaming while gsm service reports
             * roaming but indeed it is same operator.
             * And the operator is considered non roaming.
             *
             * The test for the operators is to handle special roaming
             * agreements and MVNO's.
             */
            boolean roaming = (mGsmRoaming || mDataRoaming);
            if (mGsmRoaming && !isOperatorConsideredRoaming(mNewSS) &&
                    (isSameNamedOperators(mNewSS) || isOperatorConsideredNonRoaming(mNewSS))) {
                roaming = false;
            }

            // Save the roaming state before carrier config possibly overrides it.
            if (ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN
                    == mNewSS.getRilDataRadioTechnology()) {
                mNewSS.setDataRoamingFromRegistration(mDataRoaming);
            } else {
                mNewSS.setDataRoamingFromRegistration(roaming);
            }

            CarrierConfigManager configLoader = (CarrierConfigManager)
                    mPhone.getContext().getSystemService(Context.CARRIER_CONFIG_SERVICE);

            if (configLoader != null) {
                try {
                    PersistableBundle b = configLoader.getConfigForSubId(mPhone.getSubId());

                    if (alwaysOnHomeNetwork(b)) {
                        log("updateRoamingState: carrier config override always on home network");
                        roaming = false;
                    } else if (isNonRoamingInGsmNetwork(b, mNewSS.getOperatorNumeric())) {
                        log("updateRoamingState: carrier config override set non roaming:"
                                + mNewSS.getOperatorNumeric());
                        roaming = false;
                    } else if (isRoamingInGsmNetwork(b, mNewSS.getOperatorNumeric())) {
                        log("updateRoamingState: carrier config override set roaming:"
                                + mNewSS.getOperatorNumeric());
                        roaming = true;
                    }
                } catch (Exception e) {
                    loge("updateRoamingState: unable to access carrier config service");
                }
            } else {
                log("updateRoamingState: no carrier config service available");
            }

            mNewSS.setVoiceRoaming(roaming);
            if (ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN
                    == mNewSS.getRilDataRadioTechnology()) {
                mNewSS.setDataRoaming(mDataRoaming);
            } else {
                mNewSS.setDataRoaming(roaming);
            }
        } else {

            CarrierConfigManager configLoader = (CarrierConfigManager)
                    mPhone.getContext().getSystemService(Context.CARRIER_CONFIG_SERVICE);
            if (configLoader != null) {
                try {
                    PersistableBundle b = configLoader.getConfigForSubId(mPhone.getSubId());
                    String systemId = Integer.toString(mNewSS.getSystemId());

                    if (alwaysOnHomeNetwork(b)) {
                        log("updateRoamingState: carrier config override always on home network");
                        setRoamingOff();
                    } else if (isNonRoamingInGsmNetwork(b, mNewSS.getOperatorNumeric())
                            || isNonRoamingInCdmaNetwork(b, systemId)) {
                        log("updateRoamingState: carrier config override set non-roaming:"
                                + mNewSS.getOperatorNumeric() + ", " + systemId);
                        setRoamingOff();
                    } else if (isRoamingInGsmNetwork(b, mNewSS.getOperatorNumeric())
                            || isRoamingInCdmaNetwork(b, systemId)) {
                        log("updateRoamingState: carrier config override set roaming:"
                                + mNewSS.getOperatorNumeric() + ", " + systemId);
                        setRoamingOn();
                    }
                } catch (Exception e) {
                    loge("updateRoamingState: unable to access carrier config service");
                }
            } else {
                log("updateRoamingState: no carrier config service available");
            }

            if (Build.IS_DEBUGGABLE && SystemProperties.getBoolean(PROP_FORCE_ROAMING, false)) {
                mNewSS.setVoiceRoaming(true);
                mNewSS.setDataRoaming(true);
            }
        }
    }

    @Override
    protected void handlePollStateResult(int what, AsyncResult ar) {
        // Ignore stale requests from last poll
        if (ar.userObj != mPollingContext) return;

        if (ar.exception != null) {
            CommandException.Error err=null;

            if (ar.exception instanceof CommandException) {
                err = ((CommandException)(ar.exception)).getCommandError();
            }

            if (err == CommandException.Error.RADIO_NOT_AVAILABLE) {
                // Radio has crashed or turned off
                cancelPollState();
                // Invoke pollState again to trigger pollStateDone() if needed
                if (hasPendingPollState) {
                    hasPendingPollState = false;
                    pollState();
                    loge("handlePollStateResult trigger pending pollState()");
                } else if (mCi.getRadioState() != CommandsInterface.RadioState.RADIO_ON) {
                    ///M: fix google issue, clean status and notify to other module @{
                    if (mCi.getRadioState() == CommandsInterface.RadioState.RADIO_UNAVAILABLE) {
                        mNewSS.setStateOutOfService();
                    } else {
                        mNewSS.setStateOff();
                    }
                    mNewCellLoc.setStateInvalid();
                    setSignalStrengthDefaultValues();
                    mGotCountryCode = false;
                    mNitzUpdatedTime = false;
                    mPsRegStateRaw = ServiceState.RIL_REG_STATE_NOT_REG;
                    pollStateDone();
                    if (DBG) loge("Mlog: pollStateDone to notify RADIO_NOT_AVAILABLE");
                    ///@}
                }
                return;
            }

            if (err != CommandException.Error.OP_NOT_ALLOWED_BEFORE_REG_NW) {
                loge("RIL implementation has returned an error where it must succeed" +
                        ar.exception);
            }
        } else try {
            handlePollStateResultMessage(what, ar);
        } catch (RuntimeException ex) {
            loge("Exception while polling service state. Probably malformed RIL response." + ex);
        }

        mPollingContext[0]--;

        if (mPollingContext[0] == 0) {
            if (mPhone.isPhoneTypeGsm()) {
                // PollState Queue optimization
                // If there are URC between 4 pollState Request, need discard this pollStateResult
                // when Oper is null but in service, or oper is not null but out of service
                boolean in_service = ((mNewSS.getVoiceRegState() == ServiceState.STATE_IN_SERVICE)
                        || (mNewSS.getDataRegState() == ServiceState.STATE_IN_SERVICE));
                boolean flight_mode_iwlan = ((mNewSS.getDataRegState()
                        == ServiceState.STATE_IN_SERVICE)
                        && (mNewSS.getVoiceRegState() != ServiceState.STATE_IN_SERVICE)
                        && (mNewSS.getRilDataRadioTechnology()
                        == ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN));
                String oper = mNewSS.getOperatorNumeric();
                if (((in_service == false && oper != null) // no service but has oper
                        || (in_service == true && oper == null)) // in service but no oper
                        && (!flight_mode_iwlan) // not flight mode iwlan
                        && hasPendingPollState) { // flag false means not temp state
                    loge("Temporary service state, need restart PollState");
                    hasPendingPollState = false;
                    cancelPollState();
                    modemTriggeredPollState();
                    return;
                }
                updateRoamingState();
                mNewSS.setEmergencyOnly(mEmergencyOnly);
            } else {
                boolean namMatch = false;
                if (!isSidsAllZeros() && isHomeSid(mNewSS.getSystemId())) {
                    namMatch = true;
                }

                // Setting SS Roaming (general)
                if (mIsSubscriptionFromRuim) {
                    boolean isRoamingBetweenOperators = isRoamingBetweenOperators(
                            mNewSS.getVoiceRoaming(), mNewSS);
                    if (isRoamingBetweenOperators != mNewSS.getVoiceRoaming()) {
                        log("isRoamingBetweenOperators=" + isRoamingBetweenOperators
                                + ". Override CDMA voice roaming to " + isRoamingBetweenOperators);
                        mNewSS.setVoiceRoaming(isRoamingBetweenOperators);
                    }
                }

                /**
                * For CDMA, voice and data should have the same roaming status.
                * If voice is not in service, use TSB58 roaming indicator to set
                * data roaming status. If TSB58 roaming indicator is not in the
                * carrier-specified list of ERIs for home system then set roaming.
                */
                final int dataRat = mNewSS.getRilDataRadioTechnology();
                if (ServiceState.isCdma(dataRat)) {
                    final boolean isVoiceInService =
                            (mNewSS.getVoiceRegState() == ServiceState.STATE_IN_SERVICE);
                    if (isVoiceInService) {
                        boolean isVoiceRoaming = mNewSS.getVoiceRoaming();
                        if (mNewSS.getDataRoaming() != isVoiceRoaming) {
                            log("Data roaming != Voice roaming. Override data roaming to "
                                    + isVoiceRoaming);
                            mNewSS.setDataRoaming(isVoiceRoaming);
                        }
                    } else {
                        /**
                        * As per VoiceRegStateResult from radio types.hal the TSB58
                        * Roaming Indicator shall be sent if device is registered
                        * on a CDMA or EVDO system.
                        */
                        boolean isRoamIndForHomeSystem = isRoamIndForHomeSystem(
                                Integer.toString(mRoamingIndicator));
                        if (mNewSS.getDataRoaming() == isRoamIndForHomeSystem) {
                            log("isRoamIndForHomeSystem=" + isRoamIndForHomeSystem
                                    + ", override data roaming to " + !isRoamIndForHomeSystem);
                            mNewSS.setDataRoaming(!isRoamIndForHomeSystem);
                        }
                    }
                }

                /// M: [Network][C2K] Add for show EccButton when sim out of service. @{
                mEmergencyOnly = false;
                if (mCi.getRadioState().isOn()) {
                    if ((mNewSS.getVoiceRegState() == ServiceState.STATE_OUT_OF_SERVICE)
                            && (mNewSS.getDataRegState() == ServiceState.STATE_OUT_OF_SERVICE)
                            && mNetworkExsit) {
                        mEmergencyOnly = true;
                    }
                }
                if (DBG) {
                    log("[CDMA]handlePollStateResult: set mEmergencyOnly=" + mEmergencyOnly
                            + ", mNetworkExsit=" + mNetworkExsit);
                }
                mNewSS.setEmergencyOnly(mEmergencyOnly);
                /// @}

                // Setting SS CdmaRoamingIndicator and CdmaDefaultRoamingIndicator
                mNewSS.setCdmaDefaultRoamingIndicator(mDefaultRoamingIndicator);
                mNewSS.setCdmaRoamingIndicator(mRoamingIndicator);
                boolean isPrlLoaded = true;
                if (TextUtils.isEmpty(mPrlVersion)) {
                    isPrlLoaded = false;
                }
                if (!isPrlLoaded || (mNewSS.getRilVoiceRadioTechnology()
                        == ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN)) {
                    log("Turn off roaming indicator if !isPrlLoaded or voice RAT is unknown");
                    mNewSS.setCdmaRoamingIndicator(EriInfo.ROAMING_INDICATOR_OFF);
                } else if (!isSidsAllZeros()) {
                    if (!namMatch && !mIsInPrl) {
                        // Use default
                        mNewSS.setCdmaRoamingIndicator(mDefaultRoamingIndicator);
                    } else if (namMatch && !mIsInPrl) {
                        // TODO this will be removed when we handle roaming on LTE
                        // on CDMA+LTE phones
                        if (ServiceState.isLte(mNewSS.getRilVoiceRadioTechnology())) {
                            log("Turn off roaming indicator as voice is LTE");
                            mNewSS.setCdmaRoamingIndicator(EriInfo.ROAMING_INDICATOR_OFF);
                        } else {
                            mNewSS.setCdmaRoamingIndicator(EriInfo.ROAMING_INDICATOR_FLASH);
                        }
                    } else if (!namMatch && mIsInPrl) {
                        // Use the one from PRL/ERI
                        mNewSS.setCdmaRoamingIndicator(mRoamingIndicator);
                    } else {
                        // It means namMatch && mIsInPrl
                        if ((mRoamingIndicator <= 2)) {
                            mNewSS.setCdmaRoamingIndicator(EriInfo.ROAMING_INDICATOR_OFF);
                        } else {
                            // Use the one from PRL/ERI
                            mNewSS.setCdmaRoamingIndicator(mRoamingIndicator);
                        }
                    }
                }

                int roamingIndicator = mNewSS.getCdmaRoamingIndicator();
                mNewSS.setCdmaEriIconIndex(mPhone.mEriManager.getCdmaEriIconIndex(roamingIndicator,
                        mDefaultRoamingIndicator));
                mNewSS.setCdmaEriIconMode(mPhone.mEriManager.getCdmaEriIconMode(roamingIndicator,
                        mDefaultRoamingIndicator));

                // NOTE: Some operator may require overriding mCdmaRoaming
                // (set by the modem), depending on the mRoamingIndicator.

                if (DBG) {
                    log("Set CDMA Roaming Indicator to: " + mNewSS.getCdmaRoamingIndicator()
                            + ". voiceRoaming = " + mNewSS.getVoiceRoaming()
                            + ". dataRoaming = " + mNewSS.getDataRoaming()
                            + ", isPrlLoaded = " + isPrlLoaded
                            + ". namMatch = " + namMatch + " , mIsInPrl = " + mIsInPrl
                            + ", mRoamingIndicator = " + mRoamingIndicator
                            + ", mDefaultRoamingIndicator= " + mDefaultRoamingIndicator);
                }
            }
            pollStateDone();
        }

    }

    @Override
    protected void updateSpnDisplay() {
        updateOperatorNameFromEri();

        String wfcVoiceSpnFormat = null;
        String wfcDataSpnFormat = null;
        if (mPhone.getImsPhone() != null && mPhone.getImsPhone().isWifiCallingEnabled()) {
            // In Wi-Fi Calling mode show SPN+WiFi

            String[] wfcSpnFormats = mPhone.getContext().getResources().getStringArray(
                    com.android.internal.R.array.wfcSpnFormats);
            int voiceIdx = 0;
            int dataIdx = 0;
            CarrierConfigManager configLoader = (CarrierConfigManager)
                    mPhone.getContext().getSystemService(Context.CARRIER_CONFIG_SERVICE);
            if (configLoader != null) {
                try {
                    PersistableBundle b = configLoader.getConfigForSubId(mPhone.getSubId());
                    if (b != null) {
                        voiceIdx = b.getInt(CarrierConfigManager.KEY_WFC_SPN_FORMAT_IDX_INT);
                        dataIdx = b.getInt(
                                CarrierConfigManager.KEY_WFC_DATA_SPN_FORMAT_IDX_INT);
                    }
                } catch (Exception e) {
                    loge("updateSpnDisplay: carrier config error: " + e);
                }
            }

            wfcVoiceSpnFormat = wfcSpnFormats[voiceIdx];
            wfcDataSpnFormat = wfcSpnFormats[dataIdx];
        }

        int combinedRegState = getCombinedRegState();
        if (mPhone.isPhoneTypeGsm()) {
            // The values of plmn/showPlmn change in different scenarios.
            // 1) No service but emergency call allowed -> expected
            //    to show "Emergency call only"
            //    EXTRA_SHOW_PLMN = true
            //    EXTRA_PLMN = "Emergency call only"

            // 2) No service at all --> expected to show "No service"
            //    EXTRA_SHOW_PLMN = true
            //    EXTRA_PLMN = "No service"

            // 3) Normal operation in either home or roaming service
            //    EXTRA_SHOW_PLMN = depending on IccRecords rule
            //    EXTRA_PLMN = plmn

            // 4) No service due to power off, aka airplane mode
            //    EXTRA_SHOW_PLMN = false
            //    EXTRA_PLMN = null

            IccRecords iccRecords = mIccRecords;
            String plmn = null;
            boolean showPlmn = false;
            int rule = (iccRecords != null) ? iccRecords.getDisplayRule(mSS.getOperatorNumeric()) :
                    0;
            if (combinedRegState == ServiceState.STATE_OUT_OF_SERVICE
                    || combinedRegState == ServiceState.STATE_EMERGENCY_ONLY) {
                showPlmn = true;
                if (mEmergencyOnly) {
                    // No service but emergency call allowed
                    plmn = Resources.getSystem().
                            getText(com.android.internal.R.string.emergency_calls_only).toString();
                } else {
                    // No service at all
                    plmn = Resources.getSystem().
                            getText(com.android.internal.R.string.lockscreen_carrier_default).
                            toString();
                }
                if (DBG) log("updateSpnDisplay: radio is on but out " +
                        "of service, set plmn='" + plmn + "'");
            } else if (combinedRegState == ServiceState.STATE_IN_SERVICE) {
                // In either home or roaming service
                plmn = mSS.getOperatorAlpha();
                showPlmn = !TextUtils.isEmpty(plmn) &&
                        ((rule & SIMRecords.SPN_RULE_SHOW_PLMN)
                                == SIMRecords.SPN_RULE_SHOW_PLMN);
            } else {
                // Power off state, such as airplane mode, show plmn as "No service"
                showPlmn = true;
                plmn = Resources.getSystem().
                        getText(com.android.internal.R.string.lockscreen_carrier_default).
                        toString();
                if (DBG) log("updateSpnDisplay: radio is off w/ showPlmn="
                        + showPlmn + " plmn=" + plmn);
            }

            try {
                plmn = mServiceStateTrackerExt.onUpdateSpnDisplay(plmn,
                           (MtkServiceState)mSS, mPhone.getPhoneId());
            } catch (RuntimeException e) {
                e.printStackTrace();
            }

            // If CS not registered , PS registered , add "Data
            // connection only" postfix in PLMN name
            if ((mSS.getVoiceRegState() != ServiceState.STATE_IN_SERVICE) &&
                    (mSS.getDataRegState() == ServiceState.STATE_IN_SERVICE)) {
                //[ALPS01650043]-Start: don't update PLMN name
                // when it is null for backward compatible
                if (plmn != null) {
                    if (getImsServiceState() != ServiceState.STATE_IN_SERVICE) {
                        plmn = plmn + "(" + Resources.getSystem()
                                .getText(com.mediatek.R.string.data_conn_only)
                                .toString() + ")";
                    }
                } else {
                    log("PLMN name is null when CS not registered and PS registered");
                }
            }

            /* ALPS00296298 */
            if (mIsImeiLock) {
                plmn = Resources.getSystem().getText(com.mediatek.R.string.invalid_card).toString();
            }

            // The value of spn/showSpn are same in different scenarios.
            //    EXTRA_SHOW_SPN = depending on IccRecords rule and radio/IMS state
            //    EXTRA_SPN = spn
            //    EXTRA_DATA_SPN = dataSpn
            String spn = (iccRecords != null) ? iccRecords.getServiceProviderName() : "";
            String dataSpn = spn;
            boolean showSpn = !TextUtils.isEmpty(spn)
                    && ((rule & SIMRecords.SPN_RULE_SHOW_SPN)
                    == SIMRecords.SPN_RULE_SHOW_SPN);

            if (!TextUtils.isEmpty(spn) && !TextUtils.isEmpty(wfcVoiceSpnFormat) &&
                    !TextUtils.isEmpty(wfcDataSpnFormat)) {
                // In Wi-Fi Calling mode show SPN+WiFi

                String originalSpn = spn.trim();
                spn = String.format(wfcVoiceSpnFormat, originalSpn);
                dataSpn = String.format(wfcDataSpnFormat, originalSpn);
                showSpn = true;
                showPlmn = false;
            } else if (mSS.getVoiceRegState() == ServiceState.STATE_POWER_OFF
                    || (showPlmn && TextUtils.equals(spn, plmn))) {
                // airplane mode or spn equals plmn, do not show spn
                spn = null;
                showSpn = false;
            }

            //M: don't show SPN when cs/ps not in service
            if (mSS.getVoiceRegState() != ServiceState.STATE_IN_SERVICE &&
                    mSS.getDataRegState() != ServiceState.STATE_IN_SERVICE) {
                spn = null;
                showSpn = false;
            }

            try {
                if (mServiceStateTrackerExt.needSpnRuleShowPlmnOnly() &&
                        !TextUtils.isEmpty(plmn)) {
                    log("origin showSpn:" + showSpn + " showPlmn:" + showPlmn + " rule:" + rule);
                    showSpn = false;
                    showPlmn = true;
                    rule = SIMRecords.SPN_RULE_SHOW_PLMN;
                }
            } catch (RuntimeException e) {
                e.printStackTrace();
            }

            int subId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
            int[] subIds = SubscriptionManager.getSubId(mPhone.getPhoneId());
            if (subIds != null && subIds.length > 0) {
                subId = subIds[0];
            }

            // Update SPN_STRINGS_UPDATED_ACTION IFF any value changes
            if (mSubId != subId ||
                    showPlmn != mCurShowPlmn
                    || showSpn != mCurShowSpn
                    || !TextUtils.equals(spn, mCurSpn)
                    || !TextUtils.equals(dataSpn, mCurDataSpn)
                    || !TextUtils.equals(plmn, mCurPlmn)) {
                /// M: [Network][C2K] for [CT case][TC-IRLAB-02009]. @{
                try {
                    if (!mServiceStateTrackerExt.allowSpnDisplayed()) {
                        log("For CT test case don't show SPN.");
                        if (rule == (SIMRecords.SPN_RULE_SHOW_PLMN
                                | SIMRecords.SPN_RULE_SHOW_SPN)) {
                            showSpn = false;
                            spn = null;
                        }
                    }
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
                /// @}
                if (DBG) {
                    log(String.format("updateSpnDisplay: changed sending intent rule=" + rule +
                            " showPlmn='%b' plmn='%s' showSpn='%b' spn='%s' dataSpn='%s' " +
                            "subId='%d'", showPlmn, plmn, showSpn, spn, dataSpn, subId));
                }
                Intent intent = new Intent(TelephonyIntents.SPN_STRINGS_UPDATED_ACTION);
                intent.putExtra(TelephonyIntents.EXTRA_SHOW_SPN, showSpn);
                intent.putExtra(TelephonyIntents.EXTRA_SPN, spn);
                intent.putExtra(TelephonyIntents.EXTRA_DATA_SPN, dataSpn);
                intent.putExtra(TelephonyIntents.EXTRA_SHOW_PLMN, showPlmn);
                intent.putExtra(TelephonyIntents.EXTRA_PLMN, plmn);
                // Femtocell (CSG) info
                intent.putExtra(MtkTelephonyIntents.EXTRA_HNB_NAME, mHhbName);
                intent.putExtra(MtkTelephonyIntents.EXTRA_CSG_ID, mCsgId);
                intent.putExtra(MtkTelephonyIntents.EXTRA_DOMAIN, mFemtocellDomain);
                // isFemtocell (LTE/C2K)
                intent.putExtra(MtkTelephonyIntents.EXTRA_FEMTO, mIsFemtocell);

                SubscriptionManager.putPhoneIdAndSubIdExtra(intent, mPhone.getPhoneId());
                mPhone.getContext().sendStickyBroadcastAsUser(intent, UserHandle.ALL);

                if (!mSubscriptionController.setPlmnSpn(mPhone.getPhoneId(),
                        showPlmn, plmn, showSpn, spn)) {
                    mSpnUpdatePending = true;
                }
            }

            /* ALPS00357573 for consistent operator name display */
            String operatorLong = mSS.getOperatorAlphaLong();
            if ((showSpn == true) && (showPlmn == false) && (spn != null)) {
                /* When only <spn> is shown , we update with <spn> */
                if (operatorLong == null || !operatorLong.equals(spn)) {
                    mSS.setOperatorAlphaLong(spn);
                    mForceBroadcastServiceState = true;
                }
                log("updateAllOpertorInfo with spn:" + spn
                        + ", mForceBroadcastServiceState=" + mForceBroadcastServiceState);
            }

            mSubId = subId;
            mCurShowSpn = showSpn;
            mCurShowPlmn = showPlmn;
            mCurSpn = spn;
            mCurDataSpn = dataSpn;
            mCurPlmn = plmn;
        } else {
            // mOperatorAlpha contains the ERI text
            String plmn = mSS.getOperatorAlpha();
            boolean showPlmn = false;

            showPlmn = plmn != null;
            ///M: [Network][C2K] handle "" plmn. @{
            if (plmn != null && plmn.equals("")) {
                plmn = null;
            }
            /// @}
            int subId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
            int[] subIds = SubscriptionManager.getSubId(mPhone.getPhoneId());
            if (subIds != null && subIds.length > 0) {
                subId = subIds[0];
            }

            if (!TextUtils.isEmpty(plmn) && !TextUtils.isEmpty(wfcVoiceSpnFormat)) {
                // In Wi-Fi Calling mode show SPN+WiFi

                String originalPlmn = plmn.trim();
                plmn = String.format(wfcVoiceSpnFormat, originalPlmn);
            } else if (mCi.getRadioState() == CommandsInterface.RadioState.RADIO_OFF) {
                // todo: temporary hack; should have a better fix. This is to avoid using operator
                // name from ServiceState (populated in resetServiceStateInIwlanMode()) until
                // wifi calling is actually enabled
                log("updateSpnDisplay: overwriting plmn from " + plmn + " to null as radio " +
                        "state is off");
                plmn = null;
            }

            if (combinedRegState == ServiceState.STATE_OUT_OF_SERVICE
                    || combinedRegState == ServiceState.STATE_POWER_OFF) {
                ///M: [Network][C2K] when out of service, should show plmn as "no service". @{
                showPlmn = true;
                /// @}
                plmn = Resources.getSystem().getText(com.android.internal.R.string
                        .lockscreen_carrier_default).toString();
                if (DBG) {
                    log("updateSpnDisplay: radio is on but out of svc, set plmn='" + plmn + "'");
                }
            }

            ///M: [Network][C2K] Add for update opeartor name as "Emergency Only". @{
            if (mEmergencyOnly && mCi.getRadioState().isOn()) {
                log("[CDMA]updateSpnDisplay:phone show emergency call only, mEmergencyOnly = true");
                showPlmn = true;
                plmn = Resources.getSystem().
                        getText(com.android.internal.R.string.emergency_calls_only).toString();
            }
            /// @}
            ///M: [Network][C2K] Add for PLMN/SPN display. @{
            String spn = "";
            boolean showSpn = false;
            //for [CT case][TC-IRLAB-02009]. @{
            try {
                if (mServiceStateTrackerExt.allowSpnDisplayed()) {
                    int rule = 0;
                    IccRecords r = mPhone.getIccRecords();
                    rule = (r != null) ? r.getDisplayRule(mSS
                            .getOperatorNumeric()) : IccRecords.SPN_RULE_SHOW_PLMN;
                    spn = (r != null) ? r.getServiceProviderName() : "";

                    showSpn = !TextUtils.isEmpty(spn)
                            && ((rule & RuimRecords.SPN_RULE_SHOW_SPN)
                                == RuimRecords.SPN_RULE_SHOW_SPN)
                            && !(mSS.getVoiceRegState() == ServiceState.STATE_POWER_OFF)
                            && !mSS.getRoaming();

                    log("[CDMA]updateSpnDisplay: rule=" + rule + ", spn=" + spn
                            + ", showSpn=" + showSpn);
                }
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            // @}
            if (mSubId != subId ||
                showPlmn != mCurShowPlmn
                || showSpn != mCurShowSpn
                || !TextUtils.equals(spn, mCurSpn)
                || !TextUtils.equals(plmn, mCurPlmn)) {
            /// @}
                // Allow A blank plmn, "" to set showPlmn to true. Previously, we
                // would set showPlmn to true only if plmn was not empty, i.e. was not
                // null and not blank. But this would cause us to incorrectly display
                // "No Service". Now showPlmn is set to true for any non null string.
                /// M: [Network][C2K] Modify for the spn display feature. @{
                showPlmn = plmn != null;

                // Airplane mode, out_of_service, roaming state or spn is null, do not show spn
                try {
                    if (mServiceStateTrackerExt.allowSpnDisplayed()) {
                        if (mSS.getVoiceRegState() == ServiceState.STATE_POWER_OFF
                                || mSS.getVoiceRegState() == ServiceState.STATE_OUT_OF_SERVICE
                                || mSS.getRoaming()
                                || (spn == null || spn.equals(""))) {
                            showSpn = false;
                            showPlmn = true;
                        } else {
                            showSpn = true;
                            showPlmn = false;
                        }
                    }
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
                if (DBG) {
                    log(String.format("[CDMA]updateSpnDisplay: changed sending intent" +
                            " subId='%d' showPlmn='%b' plmn='%s' showSpn='%b' spn='%s'",
                            subId, showPlmn, plmn, showSpn, spn));
                }
                /// @}
                Intent intent = new Intent(TelephonyIntents.SPN_STRINGS_UPDATED_ACTION);
                ///M: [Network][C2K] For multiple SIM support, share the same intent,
                /// do not replace the other one. @{
                if (TelephonyManager.getDefault().getPhoneCount() == 1) {
                    intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
                }
                /// @}
                ///M: [Network][C2K] Add for PLMN/APN display. @{
                intent.putExtra(TelephonyIntents.EXTRA_SHOW_SPN, showSpn);
                intent.putExtra(TelephonyIntents.EXTRA_SPN, spn);
                /// @}
                intent.putExtra(TelephonyIntents.EXTRA_SHOW_PLMN, showPlmn);
                intent.putExtra(TelephonyIntents.EXTRA_PLMN, plmn);
                // Femtocell (CSG) info
                intent.putExtra(MtkTelephonyIntents.EXTRA_HNB_NAME, mHhbName);
                intent.putExtra(MtkTelephonyIntents.EXTRA_CSG_ID, mCsgId);
                intent.putExtra(MtkTelephonyIntents.EXTRA_DOMAIN, mFemtocellDomain);
                // isFemtocell (LTE/C2K)
                intent.putExtra(MtkTelephonyIntents.EXTRA_FEMTO, mIsFemtocell);

                SubscriptionManager.putPhoneIdAndSubIdExtra(intent, mPhone.getPhoneId());
                mPhone.getContext().sendStickyBroadcastAsUser(intent, UserHandle.ALL);
                ///M: [Network][C2K] add for plmn/spn display. @{
                if (!mSubscriptionController.setPlmnSpn(mPhone.getPhoneId(),
                        showPlmn, plmn, showSpn, spn)) {
                    mSpnUpdatePending = true;
                }
                log("[CDMA]updateSpnDisplay: subId=" + subId +
                        ", showPlmn=" + showPlmn +
                        ", plmn=" + plmn +
                        ", showSpn=" + showSpn +
                        ", spn=" + spn +
                        ", mSpnUpdatePending=" + mSpnUpdatePending);
            }

            mSubId = subId;
            mCurShowSpn = showSpn;
            mCurShowPlmn = showPlmn;
            mCurSpn = spn;
            mCurPlmn = plmn;
            /// @}
        }
    }

    @Override
    protected void log(String s) {
        if (mPhone.isPhoneTypeGsm()) {
            Rlog.d(LOG_TAG, "[GsmSST" + mPhone.getPhoneId() + "] " + s);
        } else if (mPhone.isPhoneTypeCdma()) {
            Rlog.d(LOG_TAG, "[CdmaSST" + mPhone.getPhoneId() + "] " + s);
        } else {
            Rlog.d(LOG_TAG, "[CdmaLteSST" + mPhone.getPhoneId() + "] " + s);
        }
    }

    @Override
    protected void loge(String s) {
        if (mPhone.isPhoneTypeGsm()) {
            Rlog.e(LOG_TAG, "[GsmSST" + mPhone.getPhoneId() + "] " + s);
        } else if (mPhone.isPhoneTypeCdma()) {
            Rlog.e(LOG_TAG, "[CdmaSST" + mPhone.getPhoneId() + "] " + s);
        } else {
            Rlog.e(LOG_TAG, "[CdmaLteSST" + mPhone.getPhoneId() + "] " + s);
        }
    }

    private void onNetworkStateChangeResult(AsyncResult ar) {
        String info[];
        if (ar.exception != null || ar.result == null) {
            loge("onNetworkStateChangeResult exception");
            return;
        }
        info = (String[]) ar.result;
        if (mPhone.isPhoneTypeGsm()) {
            int state = -1;
            int lac = -1;
            int cid = -1;
            int Act = -1;
            int cause = -1;

        /* Note: There might not be full +CREG URC info when screen off
                 Full URC format: +CREG: <stat>,<lac>,<cid>,<Act>,<cause> */
            if (info.length > 0) {

                state = Integer.parseInt(info[0]);

                if (info[1] != null && info[1].length() > 0) {
                   lac = Integer.parseInt(info[1], 16);
                }

                if (info[2] != null && info[2].length() > 0) {
                   //TODO: fix JE (java.lang.NumberFormatException: Invalid int: "ffffffff")
                   if (info[2].equals("FFFFFFFF") || info[2].equals("ffffffff")) {
                       log("Invalid cid:" + info[2]);
                       info[2] = "0000ffff";
                   }
                   cid = Integer.parseInt(info[2], 16);
                }

                if (info[3] != null && info[3].length() > 0) {
                   Act = Integer.parseInt(info[3]);
                }

                if (info[4] != null && info[4].length() > 0) {
                   cause = Integer.parseInt(info[4]);
                }

                log("onNetworkStateChangeResult state:" + state + " lac:" + lac + " cid:" + cid
                        + " Act:" + Act + " cause:" + cause);

                try {
                // ALPS00283696 CDR-NWS-241
                    if (mServiceStateTrackerExt.needRejectCauseNotification(cause) == true) {
                        setRejectCauseNotification(cause);
                    }
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }

            } else {
                log("onNetworkStateChangeResult length zero");
            }
        } else {
            ///M: [Network][C2K] Calculate mnetworkExist by modem URC @{
            if (info.length > 5) {
                if (info[5] != null && info[5].length() > 0) {
                    mNetworkExsit = (1 == Integer.parseInt(info[5])) ? true : false;;
                    log("onCdmaNetworkExistStateChanged C2K mNetworkExist=" + mNetworkExsit);
                }
            } else {
                log("onCdmaNetworkExistStateChanged Network existence not reported");
            }
            /// @}
        }
        return;
    }

    private void onPsNetworkStateChangeResult(AsyncResult ar) {
        int info[];
        int newUrcState;

        if (ar.exception != null || ar.result == null) {
           loge("onPsNetworkStateChangeResult exception");
        } else {
            info = (int[]) ar.result;
            newUrcState = regCodeToServiceState(info[0]);
            log("onPsNetworkStateChangeResult, mPsRegState:" + mPsRegState
                    + ",newUrcState:" + newUrcState
                    + ",info[0]:" + info[0]
                    + ",info[1]:" + info[1]
                    + ",info[2]:" + info[2]);
            //get the raw state value for roaming
            mPsRegStateRaw = info[0];
        }
    }

    @Override
    public void pollState(boolean modemTriggered) {
        log("pollState: modemTriggered=" + modemTriggered + ", mPollingContext="
                + (mPollingContext != null ? mPollingContext[0] : -1));

        // [ALPS03020226]
        if ((mPollingContext != null)
                && (mPollingContext[0] != 0)
                && (mCi.getRadioState() != CommandsInterface.RadioState.RADIO_UNAVAILABLE)) {
            hasPendingPollState = true;
            return;
        }

        mPollingContext = new int[1];
        mPollingContext[0] = 0;

        switch (mCi.getRadioState()) {
            case RADIO_UNAVAILABLE:
                mNewSS.setStateOutOfService();
                mNewCellLoc.setStateInvalid();
                setSignalStrengthDefaultValues();
                mGotCountryCode = false;
                mNitzUpdatedTime = false;
                //M: MTK added
                if (mPhone.isPhoneTypeGsm()) {
                    mPsRegStateRaw = ServiceState.RIL_REG_STATE_NOT_REG;
                }
                //M: MTK added end
                pollStateDone();
                break;

            case RADIO_OFF:
                mNewSS.setStateOff();
                mNewCellLoc.setStateInvalid();
                setSignalStrengthDefaultValues();
                mGotCountryCode = false;
                mNitzUpdatedTime = false;
                // don't poll for state when the radio is off
                // EXCEPT, if the poll was modemTrigged (they sent us new radio data)
                // or we're on IWLAN
                if (!modemTriggered && ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN
                        != mSS.getRilDataRadioTechnology()) {
                    //M: MTK added
                    if (mPhone.isPhoneTypeGsm()) {
                        mPsRegStateRaw = ServiceState.RIL_REG_STATE_NOT_REG;
                    }
                    //M: MTK added end
                    pollStateDone();
                    break;
                }

            default:
                // Issue all poll-related commands at once then count down the responses, which
                // are allowed to arrive out-of-order
                mPollingContext[0]++;
                mCi.getOperator(obtainMessage(EVENT_POLL_STATE_OPERATOR, mPollingContext));

                mPollingContext[0]++;
                mCi.getDataRegistrationState(obtainMessage(EVENT_POLL_STATE_GPRS, mPollingContext));

                mPollingContext[0]++;
                mCi.getVoiceRegistrationState(obtainMessage(EVENT_POLL_STATE_REGISTRATION,
                        mPollingContext));

                if (mPhone.isPhoneTypeGsm()) {
                    mPollingContext[0]++;
                    mCi.getNetworkSelectionMode(obtainMessage(
                            EVENT_POLL_STATE_NETWORK_SELECTION_MODE, mPollingContext));
                }
                break;
        }
    }

    protected void pollStateDone() {
        refreshSpn(mNewSS, mNewCellLoc);
        if (!mPhone.isPhoneTypeGsm()) {
            updateRoamingState();
        }

        if (Build.IS_DEBUGGABLE && SystemProperties.getBoolean(PROP_FORCE_ROAMING, false)) {
            mNewSS.setVoiceRoaming(true);
            mNewSS.setDataRoaming(true);
        }
        useDataRegStateForDataOnlyDevices();
        resetServiceStateInIwlanMode();

        if (DBG) {
            log("Poll ServiceState done: "
                    + " oldSS=[" + mSS + "] newSS=[" + mNewSS + "]"
                    + " oldMaxDataCalls=" + mMaxDataCalls
                    + " mNewMaxDataCalls=" + mNewMaxDataCalls
                    + " oldReasonDataDenied=" + mReasonDataDenied
                    + " mNewReasonDataDenied=" + mNewReasonDataDenied);
        }

        boolean hasRegistered =
                mSS.getVoiceRegState() != ServiceState.STATE_IN_SERVICE
                        && mNewSS.getVoiceRegState() == ServiceState.STATE_IN_SERVICE;

        boolean hasDeregistered =
                mSS.getVoiceRegState() == ServiceState.STATE_IN_SERVICE
                        && mNewSS.getVoiceRegState() != ServiceState.STATE_IN_SERVICE;

        boolean hasDataAttached =
                mSS.getDataRegState() != ServiceState.STATE_IN_SERVICE
                        && mNewSS.getDataRegState() == ServiceState.STATE_IN_SERVICE;

        boolean hasDataDetached =
                mSS.getDataRegState() == ServiceState.STATE_IN_SERVICE
                        && mNewSS.getDataRegState() != ServiceState.STATE_IN_SERVICE;

        boolean hasDataRegStateChanged =
                mSS.getDataRegState() != mNewSS.getDataRegState();

        boolean hasVoiceRegStateChanged =
                mSS.getVoiceRegState() != mNewSS.getVoiceRegState();

        boolean hasLocationChanged = !mNewCellLoc.equals(mCellLoc);

        // ratchet the new tech up through it's rat family but don't drop back down
        // until cell change
        if (!hasLocationChanged) {
            mRatRatcheter.ratchetRat(mSS, mNewSS);
        }

        boolean hasRilVoiceRadioTechnologyChanged =
                mSS.getRilVoiceRadioTechnology() != mNewSS.getRilVoiceRadioTechnology();

        boolean hasRilDataRadioTechnologyChanged =
                mSS.getRilDataRadioTechnology() != mNewSS.getRilDataRadioTechnology();

        ///M: Fix the operator info not update issue.
        boolean hasChanged = !mNewSS.equals(mSS) || mForceBroadcastServiceState;

        boolean hasVoiceRoamingOn = !mSS.getVoiceRoaming() && mNewSS.getVoiceRoaming();

        boolean hasVoiceRoamingOff = mSS.getVoiceRoaming() && !mNewSS.getVoiceRoaming();

        boolean hasDataRoamingOn = !mSS.getDataRoaming() && mNewSS.getDataRoaming();

        boolean hasDataRoamingOff = mSS.getDataRoaming() && !mNewSS.getDataRoaming();

        boolean has4gHandoff = false;
        boolean hasMultiApnSupport = false;
        boolean hasLostMultiApnSupport = false;
        if (mPhone.isPhoneTypeCdmaLte()) {
            has4gHandoff = mNewSS.getDataRegState() == ServiceState.STATE_IN_SERVICE
                    && ((ServiceState.isLte(mSS.getRilDataRadioTechnology())
                    && (mNewSS.getRilDataRadioTechnology()
                    == ServiceState.RIL_RADIO_TECHNOLOGY_EHRPD))
                    ||
                    ((mSS.getRilDataRadioTechnology()
                            == ServiceState.RIL_RADIO_TECHNOLOGY_EHRPD)
                            && ServiceState.isLte(mNewSS.getRilDataRadioTechnology())));

            hasMultiApnSupport = ((ServiceState.isLte(mNewSS.getRilDataRadioTechnology())
                    || (mNewSS.getRilDataRadioTechnology()
                    == ServiceState.RIL_RADIO_TECHNOLOGY_EHRPD))
                    &&
                    (!ServiceState.isLte(mSS.getRilDataRadioTechnology())
                            && (mSS.getRilDataRadioTechnology()
                            != ServiceState.RIL_RADIO_TECHNOLOGY_EHRPD)));

            hasLostMultiApnSupport =
                    ((mNewSS.getRilDataRadioTechnology()
                            >= ServiceState.RIL_RADIO_TECHNOLOGY_IS95A)
                            && (mNewSS.getRilDataRadioTechnology()
                            <= ServiceState.RIL_RADIO_TECHNOLOGY_EVDO_A));
        }

        if (DBG) {
            log("pollStateDone:"
                    + " hasRegistered=" + hasRegistered
                    + " hasDeregistered=" + hasDeregistered
                    + " hasDataAttached=" + hasDataAttached
                    + " hasDataDetached=" + hasDataDetached
                    + " hasDataRegStateChanged=" + hasDataRegStateChanged
                    + " hasRilVoiceRadioTechnologyChanged= " + hasRilVoiceRadioTechnologyChanged
                    + " hasRilDataRadioTechnologyChanged=" + hasRilDataRadioTechnologyChanged
                    + " hasChanged=" + hasChanged
                    + " hasVoiceRoamingOn=" + hasVoiceRoamingOn
                    + " hasVoiceRoamingOff=" + hasVoiceRoamingOff
                    + " hasDataRoamingOn=" + hasDataRoamingOn
                    + " hasDataRoamingOff=" + hasDataRoamingOff
                    + " hasLocationChanged=" + hasLocationChanged
                    + " has4gHandoff = " + has4gHandoff
                    + " hasMultiApnSupport=" + hasMultiApnSupport
                    + " hasLostMultiApnSupport=" + hasLostMultiApnSupport);
        }

        // Add an event log when connection state changes
        if (hasVoiceRegStateChanged || hasDataRegStateChanged) {
            EventLog.writeEvent(mPhone.isPhoneTypeGsm() ? EventLogTags.GSM_SERVICE_STATE_CHANGE :
                            EventLogTags.CDMA_SERVICE_STATE_CHANGE,
                    mSS.getVoiceRegState(), mSS.getDataRegState(),
                    mNewSS.getVoiceRegState(), mNewSS.getDataRegState());
        }

        if (mPhone.isPhoneTypeGsm()) {
            // Add an event log when network type switched
            // TODO: we may add filtering to reduce the event logged,
            // i.e. check preferred network setting, only switch to 2G, etc
            if (hasRilVoiceRadioTechnologyChanged) {
                int cid = -1;
                GsmCellLocation loc = (GsmCellLocation) mNewCellLoc;
                if (loc != null) cid = loc.getCid();
                // NOTE: this code was previously located after mSS and mNewSS are swapped, so
                // existing logs were incorrectly using the new state for "network_from"
                // and STATE_OUT_OF_SERVICE for "network_to". To avoid confusion, use a new log tag
                // to record the correct states.
                EventLog.writeEvent(EventLogTags.GSM_RAT_SWITCHED_NEW, cid,
                        mSS.getRilVoiceRadioTechnology(),
                        mNewSS.getRilVoiceRadioTechnology());
                if (DBG) {
                    log("RAT switched "
                            + ServiceState.rilRadioTechnologyToString(
                            mSS.getRilVoiceRadioTechnology())
                            + " -> "
                            + ServiceState.rilRadioTechnologyToString(
                            mNewSS.getRilVoiceRadioTechnology()) + " at cell " + cid);
                }
            }
            mReasonDataDenied = mNewReasonDataDenied;
            mMaxDataCalls = mNewMaxDataCalls;
        }

        /// M: [Network][C2K] The lastest RilDataRadioTechnology.@{
        final int oldRilDataRadioTechnology = mSS.getRilDataRadioTechnology();
        /// @}
        // swap mSS and mNewSS to put new state in mSS
        ServiceState tss = mSS;
        mSS = mNewSS;
        mNewSS = tss;
        // clean slate for next time
        mNewSS.setStateOutOfService();

        // swap mCellLoc and mNewCellLoc to put new state in mCellLoc
        CellLocation tcl = mCellLoc;
        mCellLoc = mNewCellLoc;
        mNewCellLoc = tcl;

        if (hasRilVoiceRadioTechnologyChanged) {
            updatePhoneObject();
        }

        TelephonyManager tm =
                (TelephonyManager) mPhone.getContext().getSystemService(Context.TELEPHONY_SERVICE);

        if (hasRilDataRadioTechnologyChanged) {
            tm.setDataNetworkTypeForPhone(mPhone.getPhoneId(), mSS.getRilDataRadioTechnology());

            if (ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN
                    == mSS.getRilDataRadioTechnology()) {
                log("pollStateDone: IWLAN enabled");
            }

            /// M: [Network][C2K] When signal strength not changed, modem will not report CSQ urc.
            /// Then the signal indicator will not update in time such as IRAT switch.
            /// When RilDataRadioTechnology changed, poll and update signal strength. @{
            if (mPhone.isPhoneTypeCdmaLte()) {
                if (oldRilDataRadioTechnology == ServiceState.RIL_RADIO_TECHNOLOGY_LTE
                      || mSS.getRilDataRadioTechnology() == ServiceState.RIL_RADIO_TECHNOLOGY_LTE) {
                    log("[CDMALTE]pollStateDone: update signal for RAT switch between diff group");
                    sendMessage(obtainMessage(EVENT_POLL_SIGNAL_STRENGTH));
                }
            }
            /// @}
        }

        if (hasRegistered) {
            mNetworkAttachedRegistrants.notifyRegistrants();
            // M: IVSR
            mLastRegisteredPLMN = mSS.getOperatorNumeric() ;
            if (DBG) {
                log("pollStateDone: registering current mNitzUpdatedTime=" + mNitzUpdatedTime
                        + " changing to false");
            }
            mNitzUpdatedTime = false;
        }

        if (hasDeregistered) {
            mNetworkDetachedRegistrants.notifyRegistrants();
        }

        /// M: [Network][C2K] for EriTriggeredPollState @{
        // Force broadcast service state if pollState() is triggered by EVENT_ERI_FILE_LOADED
        if (mPhone.isPhoneTypeCdmaLte()) {
            /// M: [Network][C2K] Sprint roaming control @{
            if (mEriTriggeredPollState || hasChanged) {
                hasChanged = true;
                mEriTriggeredPollState = false;
                String simMccMnc = getSIMOperatorNumeric();
                // Enable ERI only for Sprint SIM
                if (simMccMnc != null &&
                        (simMccMnc.equals("310120") ||
                         simMccMnc.equals("310009") ||
                         simMccMnc.equals("311490") ||
                         simMccMnc.equals("311870"))) {
                    mEnableERI = true;
                } else {
                    mEnableERI = false;
                }
            }
            /// @}
        }
        /// @}

        // M: Check mSignalStrenght when network type may changes.
        if (hasRilVoiceRadioTechnologyChanged || hasRilDataRadioTechnologyChanged) {
            boolean isGsmNew = false;
            boolean isGsmOld = mSignalStrength.isGsm();
            int dataRat = mSS.getRilDataRadioTechnology();
            int voiceRat = mSS.getRilVoiceRadioTechnology();
            int voiceState = mSS.getVoiceRegState();
            int dataState = mSS.getDataRegState();

            if ((dataRat != ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN
                    && ServiceState.isGsm(dataRat))
                    || (voiceRat != ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN
                    && ServiceState.isGsm(voiceRat))) {
                isGsmNew = true;
            }
            // isGsm when type of cs/ps is unknown with GsmPhone
            if (voiceRat == ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN &&
                    dataRat == ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN &&
                    mPhone.isPhoneTypeGsm()) {
                isGsmNew = true;
            }

            if ((isGsmNew != isGsmOld)
                    && ((voiceState == ServiceState.STATE_IN_SERVICE)
                    || (dataState == ServiceState.STATE_IN_SERVICE))) {
                mSignalStrength.setGsm(isGsmNew);
                notifySignalStrength();
                if (DBG) log("pollStateDone: correct the mSignalStrength.isGsm " +
                        "New:" + isGsmNew + " Old:" + isGsmOld);
            }
        }
        // M: END

        if (hasChanged) {
            updateSpnDisplay();

            ///M: Fix the operator info not update issue.
            mForceBroadcastServiceState = false;

            tm.setNetworkOperatorNameForPhone(mPhone.getPhoneId(), mSS.getOperatorAlpha());

            String prevOperatorNumeric = tm.getNetworkOperatorForPhone(mPhone.getPhoneId());
            String operatorNumeric = mSS.getOperatorNumeric();

            if (!mPhone.isPhoneTypeGsm()) {
                // try to fix the invalid Operator Numeric
                if (isInvalidOperatorNumeric(operatorNumeric)) {
                    int sid = mSS.getSystemId();
                    operatorNumeric = fixUnknownMcc(operatorNumeric, sid);
                }
            }

            tm.setNetworkOperatorNumericForPhone(mPhone.getPhoneId(), operatorNumeric);
            updateCarrierMccMncConfiguration(operatorNumeric,
                    prevOperatorNumeric, mPhone.getContext());
            if (isInvalidOperatorNumeric(operatorNumeric)) {
                if (DBG) log("operatorNumeric " + operatorNumeric + " is invalid");
                tm.setNetworkCountryIsoForPhone(mPhone.getPhoneId(), "");
                mGotCountryCode = false;
                mNitzUpdatedTime = false;
            } else {
                String iso = "";
                String mcc = "";
                try {
                    mcc = operatorNumeric.substring(0, 3);
                    iso = MccTable.countryCodeForMcc(Integer.parseInt(mcc));
                } catch (NumberFormatException | StringIndexOutOfBoundsException ex) {
                    loge("pollStateDone: countryCodeForMcc error: " + ex);
                }

                tm.setNetworkCountryIsoForPhone(mPhone.getPhoneId(), iso);
                mGotCountryCode = true;
                // M: still try to guess the timezone when auto is off
                if (!mNitzUpdatedTime && !mcc.equals("000") && !TextUtils.isEmpty(iso)
                        /*&& getAutoTimeZone()*/) {

                    // Test both paths if ignore nitz is true
                    boolean testOneUniqueOffsetPath = SystemProperties.getBoolean(
                            TelephonyProperties.PROPERTY_IGNORE_NITZ, false)
                            && ((SystemClock.uptimeMillis() & 1) == 0);

                    List<String> uniqueZoneIds = TimeUtils.getTimeZoneIdsWithUniqueOffsets(iso);
                    if ((uniqueZoneIds.size() == 1) || testOneUniqueOffsetPath) {
                        String zoneId = uniqueZoneIds.get(0);
                        if (DBG) {
                            log("pollStateDone: no nitz but one TZ for iso-cc=" + iso
                                    + " with zone.getID=" + zoneId
                                    + " testOneUniqueOffsetPath=" + testOneUniqueOffsetPath);
                        }
                        mTimeZoneLog.log("pollStateDone: set time zone=" + zoneId
                                + " mcc=" + mcc + " iso=" + iso);
                        //M: only broadcast when auto is on
                        if(getAutoTimeZone()) setAndBroadcastNetworkSetTimeZone(zoneId);
                        // M: save the guess one, we can use it when user turn on the auto tz.
                        saveGuessTimeZone(zoneId);
                    // M: use capital city to update time zone
                    } else if (uniqueZoneIds.size() > 1) {
                        String allZones = "";
                        for (int i = 0; i < uniqueZoneIds.size(); i++) {
                            allZones = allZones + i + ": " + uniqueZoneIds.get(i) + "  ";
                        }
                        if(DBG) log(allZones);

                        TimeZone zone = getTimeZonesWithCapitalCity(iso);
                        if (DBG) log("getTimeZonesWithCapitalCity=" + zone
                                + " iso= " + iso);
                        if (zone != null) {
                            //M: only broadcast when auto is on
                            if(getAutoTimeZone()) setAndBroadcastNetworkSetTimeZone(zone.getID());
                            saveGuessTimeZone(zone.getID());
                        }
                    // M: end
                    } else {
                        if (DBG) {
                            log("pollStateDone: there are " + uniqueZoneIds.size()
                                    + " unique offsets for iso-cc='" + iso
                                    + " testOneUniqueOffsetPath=" + testOneUniqueOffsetPath
                                    + "', do nothing");
                        }
                    }
                }

                if (!mPhone.isPhoneTypeGsm()) {
                    setOperatorIdd(operatorNumeric);
                }

                if (shouldFixTimeZoneNow(mPhone, operatorNumeric, prevOperatorNumeric,
                        mNeedFixZoneAfterNitz)) {
                    fixTimeZone(iso);
                }
            }

            tm.setNetworkRoamingForPhone(mPhone.getPhoneId(),
                    mPhone.isPhoneTypeGsm() ? mSS.getVoiceRoaming() :
                            (mSS.getVoiceRoaming() || mSS.getDataRoaming()));

            setRoamingType(mSS);
            log("Broadcasting ServiceState : " + mSS);
            mPhone.notifyServiceStateChanged(mSS);

            TelephonyMetrics.getInstance().writeServiceStateChanged(mPhone.getPhoneId(), mSS);
        }

        if (hasDataAttached || has4gHandoff || hasDataDetached || hasRegistered
                || hasDeregistered) {
            logAttachChange();
        }

        if (hasDataAttached || has4gHandoff) {
            mAttachedRegistrants.notifyRegistrants();
            // M: IVSR
            mLastPSRegisteredPLMN = mSS.getOperatorNumeric() ;
        }

        if (hasDataDetached) {
            mDetachedRegistrants.notifyRegistrants();
        }

        if (hasRilDataRadioTechnologyChanged || hasRilVoiceRadioTechnologyChanged) {
            logRatChange();
        }

        mPsRegState = mSS.getDataRegState();

        if (hasDataRegStateChanged || hasRilDataRadioTechnologyChanged) {
            notifyDataRegStateRilRadioTechnologyChanged();

            if (ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN
                    == mSS.getRilDataRadioTechnology()) {
                mPhone.notifyDataConnection(Phone.REASON_IWLAN_AVAILABLE);
            } else {
                mPhone.notifyDataConnection(null);
            }
        }

        if (hasVoiceRoamingOn || hasVoiceRoamingOff || hasDataRoamingOn || hasDataRoamingOff) {
            logRoamingChange();
        }

        if (hasVoiceRoamingOn) {
            mVoiceRoamingOnRegistrants.notifyRegistrants();
        }

        if (hasVoiceRoamingOff) {
            mVoiceRoamingOffRegistrants.notifyRegistrants();
        }

        if (hasDataRoamingOn) {
            mDataRoamingOnRegistrants.notifyRegistrants();
        }

        if (hasDataRoamingOff) {
            mDataRoamingOffRegistrants.notifyRegistrants();
        }

        if (mSS.getDataRoaming() &&
            mNewSS.getDataRoaming() &&
            mSS.getDataRoamingType() != ServiceState.ROAMING_TYPE_UNKNOWN &&
            mNewSS.getDataRoamingType() != ServiceState.ROAMING_TYPE_UNKNOWN &&
            mSS.getDataRoamingType() != mNewSS.getDataRoamingType()) {
            // Criteria
            // 1. International roaming -> domestic roaming -> send notification
            // 2. Domestic roaming -> international roaming -> send notification
            log("notify roaming type change.");

            mDataRoamingTypeChangedRegistrants.notifyRegistrants();
        }

        if (((MtkDcTracker)mPhone.mDcTracker).getPendingDataCallFlag()) {
            ((MtkDcTracker)mPhone.mDcTracker).processPendingSetupData(this);
        }

        if (hasLocationChanged) {
            mPhone.notifyLocationChanged();
        }

        if (mPhone.isPhoneTypeGsm()) {
            if (!isGprsConsistent(mSS.getDataRegState(), mSS.getVoiceRegState())) {
                if (!mStartedGprsRegCheck && !mReportedGprsNoReg) {
                    mStartedGprsRegCheck = true;

                    int check_period = Settings.Global.getInt(
                            mPhone.getContext().getContentResolver(),
                            Settings.Global.GPRS_REGISTER_CHECK_PERIOD_MS,
                            DEFAULT_GPRS_CHECK_PERIOD_MILLIS);
                    sendMessageDelayed(obtainMessage(EVENT_CHECK_REPORT_GPRS),
                            check_period);
                }
            } else {
                mReportedGprsNoReg = false;
            }
        }

        if (hasPendingPollState) {
            hasPendingPollState = false;
            modemTriggeredPollState();
        }
    }

    @Override
    protected void fixTimeZone(String isoCountryCode) {
        TimeZone zone = null;
        // If the offset is (0, false) and the time zone property
        // is set, use the time zone property rather than GMT.
        final String zoneName = SystemProperties.get(TIMEZONE_PROPERTY);
        if (DBG) {
            log("fixTimeZone zoneName='" + zoneName +
                    "' mZoneOffset=" + mZoneOffset + " mZoneDst=" + mZoneDst +
                    " iso-cc='" + isoCountryCode +
                    "' iso-cc-idx=" + Arrays.binarySearch(GMT_COUNTRY_CODES, isoCountryCode));
        }
        if ("".equals(isoCountryCode) && mNeedFixZoneAfterNitz) {
            // Country code not found.  This is likely a test network.
            // Get a TimeZone based only on the NITZ parameters (best guess).
            zone = getNitzTimeZone(mZoneOffset, mZoneDst, mZoneTime);
            if (DBG) log("pollStateDone: using NITZ TimeZone");
        } else if ((mZoneOffset == 0) && (mZoneDst == false) && (zoneName != null)
                && (zoneName.length() > 0)
                && (Arrays.binarySearch(GMT_COUNTRY_CODES, isoCountryCode) < 0)) {
            // For NITZ string without time zone,
            // need adjust time to reflect default time zone setting
            zone = TimeZone.getDefault();
            if (mNeedFixZoneAfterNitz) {
                long ctm = System.currentTimeMillis();
                long tzOffset = zone.getOffset(ctm);
                if (DBG) {
                    log("fixTimeZone: tzOffset=" + tzOffset +
                            " ltod=" + TimeUtils.logTimeOfDay(ctm));
                }
                if (getAutoTime()) {
                    long adj = ctm - tzOffset;
                    if (DBG) log("fixTimeZone: adj ltod=" + TimeUtils.logTimeOfDay(adj));
                    setAndBroadcastNetworkSetTime(adj);
                } else {
                    // Adjust the saved NITZ time to account for tzOffset.
                    mSavedTime = mSavedTime - tzOffset;
                    if (DBG) log("fixTimeZone: adj mSavedTime=" + mSavedTime);
                }
            }
            if (DBG) log("fixTimeZone: using default TimeZone " + zone.getID());
        } else {
            zone = TimeUtils.getTimeZone(mZoneOffset, mZoneDst, mZoneTime, isoCountryCode);
            if (DBG) log("fixTimeZone: using getTimeZone(off, dst, time, iso)");
        }

        final String tmpLog = "fixTimeZone zoneName=" + zoneName + " mZoneOffset=" + mZoneOffset
                + " mZoneDst=" + mZoneDst + " iso-cc=" + isoCountryCode + " mNeedFixZoneAfterNitz="
                + mNeedFixZoneAfterNitz + " zone=" + (zone != null ? zone.getID() : "NULL");
        mTimeZoneLog.log(tmpLog);

        mNeedFixZoneAfterNitz = false;

        if (zone != null) {
            log("fixTimeZone: zone != null zone.getID=" + zone.getID());
            if (getAutoTimeZone()) {
                setAndBroadcastNetworkSetTimeZone(zone.getID());
            } else {
                log("fixTimeZone: skip changing zone as getAutoTimeZone was false");
            }
            // M: saveNitzTimeZone only when got Nitz
            if (mNitzUpdatedTime) saveNitzTimeZone(zone.getID());
        } else {
            log("fixTimeZone: zone == null, do nothing for zone");
        }
    }

    /**
     * nitzReceiveTime is time_t that the NITZ time was posted
     */
    private void setTimeFromNITZString (String nitz, long nitzReceiveTime) {
        // "yy/mm/dd,hh:mm:ss(+/-)tz"
        // tz is in number of quarter-hours

        long start = SystemClock.elapsedRealtime();
        if (DBG) {
            log("NITZ: " + nitz + "," + nitzReceiveTime
                    + " start=" + start + " delay=" + (start - nitzReceiveTime));
        }

        try {
            /* NITZ time (hour:min:sec) will be in UTC but it supplies the timezone
             * offset as well (which we won't worry about until later) */
            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

            c.clear();
            c.set(Calendar.DST_OFFSET, 0);

            String[] nitzSubs = nitz.split("[/:,+-]");

            int year = 2000 + Integer.parseInt(nitzSubs[0]);
            if (year > MAX_NITZ_YEAR) {
                if (DBG) loge("NITZ year: " + year + " exceeds limit, skip NITZ time update");
                return;
            }
            c.set(Calendar.YEAR, year);

            // month is 0 based!
            int month = Integer.parseInt(nitzSubs[1]) - 1;
            c.set(Calendar.MONTH, month);

            int date = Integer.parseInt(nitzSubs[2]);
            c.set(Calendar.DATE, date);

            int hour = Integer.parseInt(nitzSubs[3]);
            c.set(Calendar.HOUR, hour);

            int minute = Integer.parseInt(nitzSubs[4]);
            c.set(Calendar.MINUTE, minute);

            int second = Integer.parseInt(nitzSubs[5]);
            c.set(Calendar.SECOND, second);

            boolean sign = (nitz.indexOf('-') == -1);

            int tzOffset = Integer.parseInt(nitzSubs[6]);

            int dst = (nitzSubs.length >= 8 ) ? Integer.parseInt(nitzSubs[7]) : 0;

            // The zone offset received from NITZ is for current local time,
            // so DST correction is already applied.  Don't add it again.
            //
            // tzOffset += dst * 4;
            //
            // We could unapply it if we wanted the raw offset.

            tzOffset = (sign ? 1 : -1) * tzOffset * 15 * 60 * 1000;

            TimeZone    zone = null;

            // As a special extension, the Android emulator appends the name of
            // the host computer's timezone to the nitz string. this is zoneinfo
            // timezone name of the form Area!Location or Area!Location!SubLocation
            // so we need to convert the ! into /
            if (nitzSubs.length >= 9) {
                String  tzname = nitzSubs[8].replace('!','/');
                zone = TimeZone.getTimeZone( tzname );
            }

            String iso = ((TelephonyManager) mPhone.getContext().
                    getSystemService(Context.TELEPHONY_SERVICE)).
                    getNetworkCountryIsoForPhone(mPhone.getPhoneId());

            if (zone == null) {

                if (mGotCountryCode) {
                    if (iso != null && iso.length() > 0) {
                        zone = TimeUtils.getTimeZone(tzOffset, dst != 0,
                                c.getTimeInMillis(),
                                iso);
                    } else {
                        // We don't have a valid iso country code.  This is
                        // most likely because we're on a test network that's
                        // using a bogus MCC (eg, "001"), so get a TimeZone
                        // based only on the NITZ parameters.
                        zone = getNitzTimeZone(tzOffset, (dst != 0), c.getTimeInMillis());
                    }
                }
            }

            if ((zone == null) || (mZoneOffset != tzOffset) || (mZoneDst != (dst != 0))){
                // We got the time before the country or the zone has changed
                // so we don't know how to identify the DST rules yet.  Save
                // the information and hope to fix it up later.

                mNeedFixZoneAfterNitz = true;
                mZoneOffset  = tzOffset;
                mZoneDst     = dst != 0;
                mZoneTime    = c.getTimeInMillis();
            }

            String tmpLog = "NITZ: nitz=" + nitz + " nitzReceiveTime=" + nitzReceiveTime
                    + " tzOffset=" + tzOffset + " dst=" + dst + " zone="
                    + (zone != null ? zone.getID() : "NULL")
                    + " iso=" + iso + " mGotCountryCode=" + mGotCountryCode
                    + " mNeedFixZoneAfterNitz=" + mNeedFixZoneAfterNitz
                    + " getAutoTimeZone()=" + getAutoTimeZone();
            if (DBG) {
                log(tmpLog);
            }
            mTimeZoneLog.log(tmpLog);

            if (zone != null) {
                if (getAutoTimeZone()) {
                    setAndBroadcastNetworkSetTimeZone(zone.getID());
                }
                saveNitzTimeZone(zone.getID());
            }

            String ignore = SystemProperties.get("gsm.ignore-nitz");
            if (ignore != null && ignore.equals("yes")) {
                log("NITZ: Not setting clock because gsm.ignore-nitz is set");
                return;
            }

            try {
                mWakeLock.acquire();

                if (!mPhone.isPhoneTypeGsm() || getAutoTime()) {
                    long millisSinceNitzReceived
                            = SystemClock.elapsedRealtime() - nitzReceiveTime;

                    if (millisSinceNitzReceived < 0) {
                        // Sanity check: something is wrong
                        if (DBG) {
                            log("NITZ: not setting time, clock has rolled "
                                    + "backwards since NITZ time was received, "
                                    + nitz);
                        }
                        return;
                    }

                    if (millisSinceNitzReceived > Integer.MAX_VALUE) {
                        // If the time is this far off, something is wrong > 24 days!
                        if (DBG) {
                            log("NITZ: not setting time, processing has taken "
                                    + (millisSinceNitzReceived / (1000 * 60 * 60 * 24))
                                    + " days");
                        }
                        return;
                    }

                    // Note: with range checks above, cast to int is safe
                    c.add(Calendar.MILLISECOND, (int)millisSinceNitzReceived);

                    tmpLog = "NITZ: nitz=" + nitz + " nitzReceiveTime=" + nitzReceiveTime
                            + " Setting time of day to " + c.getTime()
                            + " NITZ receive delay(ms): " + millisSinceNitzReceived
                            + " gained(ms): "
                            + (c.getTimeInMillis() - System.currentTimeMillis())
                            + " from " + nitz;
                    if (DBG) {
                        log(tmpLog);
                    }
                    mTimeLog.log(tmpLog);
                    if (mPhone.isPhoneTypeGsm()) {
                        setAndBroadcastNetworkSetTime(c.getTimeInMillis());
                        Rlog.i(LOG_TAG, "NITZ: after Setting time of day");
                    } else {
                        if (getAutoTime()) {
                            /**
                             * Update system time automatically
                             */
                            long gained = c.getTimeInMillis() - System.currentTimeMillis();
                            long timeSinceLastUpdate = SystemClock.elapsedRealtime() - mSavedAtTime;
                            int nitzUpdateSpacing = Settings.Global.getInt(mCr,
                                    Settings.Global.NITZ_UPDATE_SPACING, mNitzUpdateSpacing);
                            int nitzUpdateDiff = Settings.Global.getInt(mCr,
                                    Settings.Global.NITZ_UPDATE_DIFF, mNitzUpdateDiff);

                            if ((mSavedAtTime == 0) || (timeSinceLastUpdate > nitzUpdateSpacing)
                                    || (Math.abs(gained) > nitzUpdateDiff)) {
                                if (DBG) {
                                    log("NITZ: Auto updating time of day to " + c.getTime()
                                            + " NITZ receive delay=" + millisSinceNitzReceived
                                            + "ms gained=" + gained + "ms from " + nitz);
                                }

                                setAndBroadcastNetworkSetTime(c.getTimeInMillis());
                            } else {
                                if (DBG) {
                                    log("NITZ: ignore, a previous update was "
                                            + timeSinceLastUpdate + "ms ago and gained="
                                            + gained + "ms");
                                }
                                return;
                            }
                        }
                    }
                }
                SystemProperties.set("gsm.nitz.time", String.valueOf(c.getTimeInMillis()));
                saveNitzTime(c.getTimeInMillis());
                mNitzUpdatedTime = true;
            } finally {
                if (DBG) {
                    long end = SystemClock.elapsedRealtime();
                    log("NITZ: end=" + end + " dur=" + (end - start));
                }
                mWakeLock.release();
            }
        } catch (RuntimeException ex) {
            loge("NITZ: Parsing NITZ time " + nitz + " ex=" + ex);
        }
    }

    /**
     * @return true when data's rat is IWLAN and IMS is in service
     */
    private final boolean isConcurrentVoiceAndDataAllowedForIwlan() {
        if (mSS.getDataRegState() == ServiceState.STATE_IN_SERVICE
                && mSS.getRilDataRadioTechnology() == ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN
                && getImsServiceState() == ServiceState.STATE_IN_SERVICE) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isConcurrentVoiceAndDataAllowed() {
        if (mPhone.isPhoneTypeGsm()) {
            /// M: For VoLTE concurrent voice and data allow. @{
            if (isConcurrentVoiceAndDataAllowedForVolte()) {
                return true;
            }
            /// @}
            // M: Add for IWLAN
            if (isConcurrentVoiceAndDataAllowedForIwlan()) {
                return true;
            }
            return (mSS.getRilVoiceRadioTechnology() >= ServiceState.RIL_RADIO_TECHNOLOGY_UMTS);
        } else if (mPhone.isPhoneTypeCdma()) {
            // Note: it needs to be confirmed which CDMA network types
            // can support voice and data calls concurrently.
            // For the time-being, the return value will be false.
            return false;
        } else {
            // Using the Conncurrent Service Supported flag for CdmaLte devices.
            /// M: [Network][C2K]For svlte concurrent voice and data allow. @{
            // here just a indicator of concurrent capability, but may be could not concurrent right
            // now, so ignore voice register state.
            if (SystemProperties.getInt("ro.boot.opt_c2k_lte_mode", 0) == 1
                    && mSS.getRilDataRadioTechnology() == ServiceState.RIL_RADIO_TECHNOLOGY_LTE) {
                return true;
                }
            /// @}

            /// M: For VoLTE concurrent voice and data allow. @{
            if (isConcurrentVoiceAndDataAllowedForVolte()) {
                return true;
            }
            /// @}
            return mSS.getCssIndicator() == 1;
        }
    }

    private int calculateDeviceRatMode(int phoneId) {
        int networkType = -1;

        if (mPhone.isPhoneTypeGsm()) {
            int restrictedNwMode = -1 ;
            try {
                if (mServiceStateTrackerExt.isSupportRatBalancing()) {
                    log("networkType is controlled by RAT Blancing,"
                        + " no need to set network type");
                    return -1;
                }
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            try {
                restrictedNwMode = mServiceStateTrackerExt.needAutoSwitchRatMode(
                        phoneId, mLocatedPlmn);
            } catch (RuntimeException e) {
                e.printStackTrace();
            }

            networkType = PhoneFactory.calculatePreferredNetworkType(
                    mPhone.getContext(), mPhone.getSubId());
            log("restrictedNwMode = " + restrictedNwMode);
            if (restrictedNwMode >= Phone.NT_MODE_WCDMA_PREF) {
               if (restrictedNwMode != networkType) {
                   log("Revise networkType to " + restrictedNwMode);
                   networkType = restrictedNwMode;
               }
            }
        } else {
            try {
                networkType = mServiceStateTrackerExt.getNetworkTypeForMota(phoneId);
                log("[CDMA], networkType for mota is: " + networkType);
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            if (networkType == -1) {
                networkType = PhoneFactory.calculatePreferredNetworkType(
                        mPhone.getContext(), mPhone.getSubId());
            }
        }
        log("calculateDeviceRatMode = " + networkType);
        return networkType;
    }

    /**
     * @param phoneId set RAT to specific phone.
     */
    public void setDeviceRatMode(int phoneId) {
        if (SubscriptionManager.isValidSubscriptionId(mPhone.getSubId())) {
            int networkType = calculateDeviceRatMode(phoneId);
            if (networkType >= Phone.NT_MODE_WCDMA_PREF) {
                mPhone.setPreferredNetworkType(networkType, null);
            }
        } else {
            log("Invalid subId, skip setDeviceRatMode!");
        }
    }


    /**
     * Return the current located PLMN string (ex: "46000") or null (ex: flight mode or no signal
     * area).
     * @return mLocatedPlmn return register PLMN id.
     */
    public String getLocatedPlmn() {
        return mLocatedPlmn;
    }

    private void updateLocatedPlmn(String plmn) {
        log("updateLocatedPlmn(),previous plmn= " + mLocatedPlmn + " ,update to: " + plmn);

        if (((mLocatedPlmn == null) && (plmn != null)) ||
            ((mLocatedPlmn != null) && (plmn == null)) ||
            ((mLocatedPlmn != null) && (plmn != null) && !(mLocatedPlmn.equals(plmn)))) {
            Intent intent = new Intent(MtkTelephonyIntents.ACTION_LOCATED_PLMN_CHANGED);
            if (TelephonyManager.getDefault().getPhoneCount() == 1) {
                intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
            }
            intent.putExtra(TelephonyIntents.EXTRA_PLMN, plmn);

            if (plmn != null) {
                int mcc;
                try {
                    mcc = Integer.parseInt(plmn.substring(0, 3));
                    intent.putExtra(MtkTelephonyIntents.EXTRA_ISO, MccTable.countryCodeForMcc(mcc));
                } catch (NumberFormatException ex) {
                    loge("updateLocatedPlmn: countryCodeForMcc error" + ex);
                    intent.putExtra(MtkTelephonyIntents.EXTRA_ISO, "");
                } catch (StringIndexOutOfBoundsException ex) {
                    loge("updateLocatedPlmn: countryCodeForMcc error" + ex);
                    intent.putExtra(MtkTelephonyIntents.EXTRA_ISO, "");
                }
                mLocatedPlmn = plmn;  //[ALPS02198932]
                setDeviceRatMode(mPhone.getPhoneId());
            } else {
                intent.putExtra(MtkTelephonyIntents.EXTRA_ISO, "");
            }

            SubscriptionManager.putPhoneIdAndSubIdExtra(intent, mPhone.getPhoneId());
            mPhone.getContext().sendStickyBroadcastAsUser(intent, UserHandle.ALL);
        }

        mLocatedPlmn = plmn;
    }

    /* Refresh operator name after mcc, mnc and lac are available
     * so we can get EONS correctly
     */
    protected void refreshSpn(ServiceState ss, CellLocation cellLoc) {
        // FIXME: Giving brandOverride higher precedence, is this desired?
        String brandOverride = mUiccController.getUiccCard(getPhoneId()) != null ?
                mUiccController.getUiccCard(getPhoneId()).getOperatorBrandOverride() : null;
        if (brandOverride != null) {
            log("refreshSpn: use brandOverride" + brandOverride);
            ss.setOperatorName(brandOverride, brandOverride, ss.getOperatorNumeric());
        } else {

            String strOperatorLong = null;
            String strOperatorShort = null;
            int lac = (mPhone.getPhoneType() == PhoneConstants.PHONE_TYPE_GSM) ?
                    ((GsmCellLocation)cellLoc).getLac() : -1;

            strOperatorLong = ((MtkRIL)mCi).lookupOperatorName(
                    mPhone.getSubId(),
                    ss.getOperatorNumeric(), true, lac);
            strOperatorShort = ((MtkRIL)mCi).lookupOperatorName(
                    mPhone.getSubId(),
                    ss.getOperatorNumeric(), false, lac);

            if ((!TextUtils.equals(strOperatorLong, mSS.getOperatorAlphaLong())) ||
                (!TextUtils.equals(strOperatorShort, mSS.getOperatorAlphaShort()))) {
                mForceBroadcastServiceState = true;
            }

            log("refreshSpn: " + strOperatorLong
                    + ", " + strOperatorShort
                    + ", mForceBroadcastServiceState=" + mForceBroadcastServiceState);
            ss.setOperatorName(strOperatorLong, strOperatorShort, ss.getOperatorNumeric());
        }
    }

    @Override
    protected void setPowerStateToDesired() {
        if (DBG) {
            log("mDeviceShuttingDown=" + mDeviceShuttingDown +
                    ", mDesiredPowerState=" + mDesiredPowerState +
                    ", getRadioState=" + mCi.getRadioState() +
                    ", mPowerOffDelayNeed=" + mPowerOffDelayNeed +
                    ", mAlarmSwitch=" + mAlarmSwitch);
        }

        if (mPhone.isPhoneTypeGsm() && mAlarmSwitch) {
            if(DBG) log("mAlarmSwitch == true");
            Context context = mPhone.getContext();
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            am.cancel(mRadioOffIntent);
            mAlarmSwitch = false;
        }

        // If we want it on and it's off, turn it on
        if (mDesiredPowerState
                && mCi.getRadioState() == CommandsInterface.RadioState.RADIO_OFF) {
            // [GSM] Send preferred network type before turn on radio power to avoid using wrong rat
            if (mPhone.isPhoneTypeGsm()) {
                setDeviceRatMode(mPhone.getPhoneId());
            }
            RadioManager.getInstance().sendRequestBeforeSetRadioPower(true, mPhone.getPhoneId());
            /// MTK-END
            mCi.setRadioPower(true, null);
        } else if (!mDesiredPowerState && mCi.getRadioState().isOn()) {
            // If it's on and available and we want it off gracefully
            if (mPhone.isPhoneTypeGsm() && mPowerOffDelayNeed) {
                if (mImsRegistrationOnOff && !mAlarmSwitch) {
                    if(DBG) log("mImsRegistrationOnOff == true");
                    Context context = mPhone.getContext();
                    AlarmManager am =
                        (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                    Intent intent = new Intent(ACTION_RADIO_OFF);
                    mRadioOffIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

                    mAlarmSwitch = true;
                    if (DBG) log("Alarm setting");
                    am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                            SystemClock.elapsedRealtime() + 3000, mRadioOffIntent);
                } else {
                    DcTracker dcTracker = mPhone.mDcTracker;
                    powerOffRadioSafely(dcTracker);
                }
            } else {
                DcTracker dcTracker = mPhone.mDcTracker;
                powerOffRadioSafely(dcTracker);
            }
        } else if (mDeviceShuttingDown && mCi.getRadioState().isAvailable()) {
            mCi.requestShutdown(null);
        }
    }

    /**
     * Hang up all voice call and turn off radio. Implemented by derived class.
     */
    @Override
    protected void hangupAndPowerOff() {
        // hang up all active voice calls
        if (!mPhone.isPhoneTypeGsm() || mPhone.isInCall()) {
            mPhone.mCT.mRingingCall.hangupIfAlive();
            mPhone.mCT.mBackgroundCall.hangupIfAlive();
            mPhone.mCT.mForegroundCall.hangupIfAlive();
        }
        //MTK-START some actions must be took before EFUN
        RadioManager.getInstance().sendRequestBeforeSetRadioPower(false, mPhone.getPhoneId());
        //MTK-END
        mCi.setRadioPower(false, null);
    }

    private void onFemtoCellInfoResult(AsyncResult ar) {
        String info[];
        int isCsgCell = 0;

        if (ar.exception != null || ar.result == null) {
           loge("onFemtoCellInfo exception");
        } else {
            info = (String[]) ar.result;

            if (info.length > 0) {
                if (info[0] != null && info[0].length() > 0) {
                    mFemtocellDomain = Integer.parseInt(info[0]);
                    log("onFemtoCellInfo: mFemtocellDomain set to " + mFemtocellDomain);
                }

                if (info[5] != null && info[5].length() > 0) {
                   isCsgCell = Integer.parseInt(info[5]);
                }
                mIsFemtocell = isCsgCell;
                log("onFemtoCellInfo: domain= " + mFemtocellDomain + ",isCsgCell= " + isCsgCell);

                if (isCsgCell == 1) {
                    if (info[6] != null && info[6].length() > 0) {
                        mCsgId = info[6];
                        log("onFemtoCellInfo: mCsgId set to " + mCsgId);
                    }

                    if (info[8] != null && info[8].length() > 0) {
                        mHhbName = new String(IccUtils.hexStringToBytes(info[8]));
                        log("onFemtoCellInfo: mHhbName set from " + info[8] + " to " + mHhbName);
                    } else {
                        mHhbName = null;
                        log("onFemtoCellInfo: mHhbName is not available ,set to null");
                    }
                } else {
                    mCsgId = null;
                    mHhbName = null;
                    log("onFemtoCellInfo: csgId and hnbName are cleared");
                }
                if (isCsgCell != 2 && // ignore LTE & C2K case
                    (info[1] != null && info[1].length() > 0)  &&
                    (info[9] != null && info[0].length() > 0)) {
                    int state = Integer.parseInt(info[1]);
                    int cause = Integer.parseInt(info[9]);
                    try {
                        if (mServiceStateTrackerExt.needIgnoreFemtocellUpdate(
                                state, cause) == true) {
                            log("needIgnoreFemtocellUpdate due to state= " + state + ",cause= "
                                + cause);
                            // return here to prevent update variables and broadcast for CSG
                            return;
                        }
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    }
                }
                Intent intent = new Intent(TelephonyIntents.SPN_STRINGS_UPDATED_ACTION);
                SubscriptionManager.putPhoneIdAndSubIdExtra(intent, mPhone.getPhoneId());

                if (TelephonyManager.getDefault().getPhoneCount() == 1) {
                    intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
                }

                intent.putExtra(TelephonyIntents.EXTRA_SHOW_SPN, mCurShowSpn);
                intent.putExtra(TelephonyIntents.EXTRA_SPN, mCurSpn);
                intent.putExtra(TelephonyIntents.EXTRA_SHOW_PLMN, mCurShowPlmn);
                intent.putExtra(TelephonyIntents.EXTRA_PLMN, mCurPlmn);
                // Femtocell (CSG) info
                intent.putExtra(MtkTelephonyIntents.EXTRA_HNB_NAME, mHhbName);
                intent.putExtra(MtkTelephonyIntents.EXTRA_CSG_ID, mCsgId);
                intent.putExtra(MtkTelephonyIntents.EXTRA_DOMAIN, mFemtocellDomain);
                // isFemtocell (LTE/C2K)
                intent.putExtra(MtkTelephonyIntents.EXTRA_FEMTO, mIsFemtocell);

                mPhone.getContext().sendStickyBroadcastAsUser(intent, UserHandle.ALL);

                int phoneId = mPhone.getPhoneId();
                String plmn = mCurPlmn;
                if ((mHhbName == null) && (mCsgId != null)) {
                    try {
                        if (mServiceStateTrackerExt.needToShowCsgId() == true) {
                            plmn += " - ";
                            plmn += mCsgId;
                        }
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    }
                } else if (mHhbName != null) {
                    plmn += " - ";
                    plmn += mHhbName;
                }
                boolean setResult = mSubscriptionController.setPlmnSpn(phoneId,
                        mCurShowPlmn, plmn, mCurShowSpn, mCurSpn);
                if (!setResult) {
                    mSpnUpdatePending = true;
                }
            }
        }
    }

    private void onInvalidSimInfoReceived(AsyncResult ar) {
        String[] InvalidSimInfo = (String[]) ar.result;
        String plmn = InvalidSimInfo[0];
        int cs_invalid = Integer.parseInt(InvalidSimInfo[1]);
        int ps_invalid = Integer.parseInt(InvalidSimInfo[2]);
        int cause = Integer.parseInt(InvalidSimInfo[3]);
        int testMode = -1;

        // do NOT apply IVSR when in TEST mode
        testMode = SystemProperties.getInt("gsm.gcf.testmode", 0);
        // there is only one test mode in modem. actually it's not SIM dependent , so remove
        // testmode2 property here

        log("onInvalidSimInfoReceived testMode:" + testMode + " cause:" + cause + " cs_invalid:"
                + cs_invalid + " ps_invalid:" + ps_invalid + " plmn:" + plmn
                + " mEverIVSR:" + mEverIVSR);

        //Check UE is set to test mode or not   (CTA =1,FTA =2 , IOT=3 ...)
        if (testMode != 0) {
            log("InvalidSimInfo received during test mode: " + testMode);
            return;
        }

        if (mServiceStateTrackerExt.isNeedDisableIVSR()) {
            log("Disable IVSR");
            return;
        }

        //MTK-ADD Start : for CS not registered , PS regsitered (ex: LTE PS only mode or 2/3G PS
        //only SIM card or CS domain network registeration temporary failure
        if (cs_invalid == 1) {
             isCsInvalidCard = true;
        }
         //MTK-ADD END : for CS not registered , PS regsitered (ex: LTE PS only mode or 2/3G PS
         //only SIM card or CS domain network registeration temporary failure

        /* check if CS domain ever sucessfully registered to the invalid SIM PLMN */
        /* Integrate ALPS00286197 with MR2 data only device state update , not to apply CS domain
           IVSR for data only device */
        if (mMtkVoiceCapable) {
            if ((cs_invalid == 1) && (mLastRegisteredPLMN != null)
                    && (plmn.equals(mLastRegisteredPLMN))) {
                log("InvalidSimInfo reset SIM due to CS invalid");
                setEverIVSR(true);
                mLastRegisteredPLMN = null;
                mLastPSRegisteredPLMN = null;
                ((MtkRIL)mCi).setSimPower(MtkRILConstants.SIM_POWER_RESET, null);
                return;
            }
        }

        /* check if PS domain ever sucessfully registered to the invalid SIM PLMN */
        //[ALPS02261450] - start
        if ((ps_invalid == 1) && (isAllowRecoveryOnIvsr(ar)) &&
                (mLastPSRegisteredPLMN != null) && (plmn.equals(mLastPSRegisteredPLMN))){
        //if ((ps_invalid == 1) && (mLastPSRegisteredPLMN != null) &&
        //              (plmn.equals(mLastPSRegisteredPLMN)))
        //[ALPS02261450] - end
            log("InvalidSimInfo reset SIM due to PS invalid ");
            setEverIVSR(true);
            mLastRegisteredPLMN = null;
            mLastPSRegisteredPLMN = null;
            ((MtkRIL)mCi).setSimPower(MtkRILConstants.SIM_POWER_RESET, null);
            return;
        }

        /* ALPS00324111: to force trigger IVSR */
        /* ALPS00407923  : The following code is to "Force trigger IVSR even
                  when MS never register to the
                  network before"The code was intended to cover the scenario of "invalid
                  SIM NW issue happen
                  at the first network registeration during boot-up".
                  However, it might cause false alarm IVSR ex: certain sim card only register
                  CS domain network , but PS domain is invalid.
                  For such sim card, MS will receive invalid SIM at the first PS domain
                  network registeration In such case , to trigger IVSR will be a false alarm,
                  which will cause  CS domain network
                  registeration time longer (due to IVSR impact)
                  It's a tradeoff. Please think about the false alarm impact
                  before using the code below.*/
        /*
        if ((mEverIVSR == false) && (gprsState != ServiceState.STATE_IN_SERVICE)
                &&(mSS.getVoiceRegState() != ServiceState.STATE_IN_SERVICE))
        {
            log("InvalidSimInfo set TRM due to never set IVSR");
            setEverIVSR(true);
            mLastRegisteredPLMN = null;
            mLastPSRegisteredPLMN = null;
            phone.setTRM(3, null);
            return;
        }
        */
    }

    private void onNetworkEventReceived(AsyncResult ar) {
        if (ar.exception != null || ar.result == null) {
           loge("onNetworkEventReceived exception");
        } else {
            // result[0]: <Act> not used
            // result[1]: <event_type> 0: for RAU event , 1: for TAU event
            int nwEventType = ((int[]) ar.result)[1];
            log("[onNetworkEventReceived] event_type:" + nwEventType);

            Intent intent = new Intent(MtkTelephonyIntents.ACTION_NETWORK_EVENT);
            intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
            intent.putExtra(MtkTelephonyIntents.EXTRA_EVENT_TYPE, nwEventType + 1);

            mPhone.getContext().sendStickyBroadcastAsUser(intent, UserHandle.ALL);
        }
    }

    private void onModulationInfoReceived(AsyncResult ar) {
        if (ar.exception != null || ar.result == null) {
           loge("onModulationInfoReceived exception");
        } else {
            int info[];
            int modulation;
            info = (int[]) ar.result;
            modulation = info[0];
            log("[onModulationInfoReceived] modulation:" + modulation);

            Intent intent = new Intent(MtkTelephonyIntents.ACTION_NOTIFY_MODULATION_INFO);
            intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
            intent.putExtra(MtkTelephonyIntents.EXTRA_MODULATION_INFO, modulation);

            mPhone.getContext().sendStickyBroadcastAsUser(intent, UserHandle.ALL);
        }
    }

    private boolean isAllowRecoveryOnIvsr(AsyncResult ar) {
        if (mPhone.isInCall()){
            log("[isAllowRecoveryOnIvsr] isInCall()=true");
            Message msg;
            msg = obtainMessage();
            msg.what = EVENT_INVALID_SIM_INFO;
            msg.obj = ar;
            sendMessageDelayed(msg, 10 * 1000); //check 10s later
            return false;
        } else {
            log("isAllowRecoveryOnIvsr() return true");
            return true;
        }
    }

    private void setEverIVSR(boolean value) {
        log("setEverIVSR:" + value);
        mEverIVSR = value;

        /* ALPS00376525 notify IVSR start event */
        if (value == true) {
            Intent intent = new Intent(MtkTelephonyIntents.ACTION_IVSR_NOTIFY);
            intent.putExtra(MtkTelephonyIntents.INTENT_KEY_IVSR_ACTION, "start");
            SubscriptionManager.putPhoneIdAndSubIdExtra(intent, mPhone.getPhoneId());

            if (TelephonyManager.getDefault().getPhoneCount() == 1) {
                intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
            }

            log("broadcast ACTION_IVSR_NOTIFY intent");

            mPhone.getContext().sendBroadcastAsUser(intent, UserHandle.ALL);
        }
    }

    //TODO: check each values, which need to be reset.
    private void setNullState() {
        isCsInvalidCard = false;
    }

    protected final boolean IsInternationalRoamingException(String operatorNumeric) {
        String carrierConfig =
                MtkCarrierConfigManager.KEY_CARRIER_INTERNATIONAL_ROAMING_EXCEPTION_LIST_STRINGS;

        CarrierConfigManager configManager = (CarrierConfigManager)
                mPhone.getContext().getSystemService(Context.CARRIER_CONFIG_SERVICE);
        if (configManager == null) {
            Rlog.e(LOG_TAG, "Carrier config service is not available");
            return false;
        }

        PersistableBundle b = configManager.getConfigForSubId(mPhone.getSubId());
        if (b == null) {
            Rlog.e(LOG_TAG, "Can't get the config. subId = " + mPhone.getSubId());
            return false;
        }

        String[] operatorRoamingException = b.getStringArray(carrierConfig);
        if (operatorRoamingException == null) {
            Rlog.e(LOG_TAG, carrierConfig +  " is not available. " + "subId = " +
                    mPhone.getSubId());
            return false;
        }

        HashSet<String> internationalRoamingSet = new HashSet<>(
                Arrays.asList(operatorRoamingException));
        if (DBG) {
            Rlog.d(LOG_TAG, "For subId = " + mPhone.getSubId() +
                    ", international roaming exceptions are " +
                    Arrays.toString(internationalRoamingSet.toArray()) +
                    ", operatorNumeric = " + operatorNumeric);
        }

        // If registered plmn is in list, then roaming type should be international roaming.
        if (internationalRoamingSet.contains(operatorNumeric)) {
            if (DBG) Rlog.d(LOG_TAG, operatorNumeric + " in list.");
            return true;
        }

        if (DBG) Rlog.d(LOG_TAG, operatorNumeric + " is not in list.");
        return false;
    }

    /**
     * Set both voice and data roaming type,
     * judging from the ISO country of SIM VS network.
     */
    @Override
    protected void setRoamingType(ServiceState currentServiceState) {
        final boolean isVoiceInService =
                (currentServiceState.getVoiceRegState() == ServiceState.STATE_IN_SERVICE);
        boolean isInternationalRoaming = false;
        if (isVoiceInService) {
            if (currentServiceState.getVoiceRoaming()) {
                if (mPhone.isPhoneTypeGsm()) {
                    // check roaming type by MCC
                    if (inSameCountry(currentServiceState.getVoiceOperatorNumeric())) {
                        currentServiceState.setVoiceRoamingType(
                                ServiceState.ROAMING_TYPE_DOMESTIC);
                    } else {
                        currentServiceState.setVoiceRoamingType(
                                ServiceState.ROAMING_TYPE_INTERNATIONAL);
                    }

                    // check operator specified international roaming
                    isInternationalRoaming = IsInternationalRoamingException(
                            currentServiceState.getVoiceOperatorNumeric());

                    if (isInternationalRoaming) {
                        log(currentServiceState.getVoiceOperatorNumeric()
                                + " is in operator defined international roaming list");
                        currentServiceState.setVoiceRoamingType(
                                ServiceState.ROAMING_TYPE_INTERNATIONAL);
                    }
                } else {
                    // some carrier defines international roaming by indicator
                    int[] intRoamingIndicators = mPhone.getContext().getResources().getIntArray(
                            com.android.internal.R.array
                            .config_cdma_international_roaming_indicators);
                    if ((intRoamingIndicators != null) && (intRoamingIndicators.length > 0)) {
                        // It's domestic roaming at least now
                        currentServiceState.setVoiceRoamingType(ServiceState.ROAMING_TYPE_DOMESTIC);
                        int curRoamingIndicator = currentServiceState.getCdmaRoamingIndicator();
                        for (int i = 0; i < intRoamingIndicators.length; i++) {
                            if (curRoamingIndicator == intRoamingIndicators[i]) {
                                currentServiceState.setVoiceRoamingType(
                                        ServiceState.ROAMING_TYPE_INTERNATIONAL);
                                break;
                            }
                        }
                    } else {
                        // check roaming type by MCC
                        if (inSameCountry(currentServiceState.getVoiceOperatorNumeric())) {
                            currentServiceState.setVoiceRoamingType(
                                    ServiceState.ROAMING_TYPE_DOMESTIC);
                        } else {
                            currentServiceState.setVoiceRoamingType(
                                    ServiceState.ROAMING_TYPE_INTERNATIONAL);
                        }
                    }
                }
            } else {
                currentServiceState.setVoiceRoamingType(ServiceState.ROAMING_TYPE_NOT_ROAMING);
            }
        }
        final boolean isDataInService =
                (currentServiceState.getDataRegState() == ServiceState.STATE_IN_SERVICE);
        final int dataRegType = currentServiceState.getRilDataRadioTechnology();
        if (isDataInService) {
            if (!currentServiceState.getDataRoaming()) {
                currentServiceState.setDataRoamingType(ServiceState.ROAMING_TYPE_NOT_ROAMING);
            } else {
                if (mPhone.isPhoneTypeGsm()) {
                    if (ServiceState.isGsm(dataRegType)) {
                        if (isVoiceInService) {
                            // GSM data should have the same state as voice
                            currentServiceState.setDataRoamingType(currentServiceState
                                    .getVoiceRoamingType());
                        } else {
                            // we can not decide GSM data roaming type without voice
                            //currentServiceState.setDataRoamingType(ServiceState
                            //        .ROAMING_TYPE_UNKNOWN);
                            // MTK data framework need data roaming type when voice not registered
                            // check roaming type by MCC
                            if (inSameCountry(currentServiceState.getVoiceOperatorNumeric())) {
                                currentServiceState.setVoiceRoamingType(
                                        ServiceState.ROAMING_TYPE_DOMESTIC);
                            } else {
                                currentServiceState.setVoiceRoamingType(
                                        ServiceState.ROAMING_TYPE_INTERNATIONAL);
                            }

                            // check operator specified international roaming
                            isInternationalRoaming = IsInternationalRoamingException(
                                    currentServiceState.getVoiceOperatorNumeric());

                            if (isInternationalRoaming) {
                                log(currentServiceState.getVoiceOperatorNumeric()
                                        + " is in operator defined international roaming list");
                                currentServiceState.setVoiceRoamingType(
                                        ServiceState.ROAMING_TYPE_INTERNATIONAL);
                            }
                        }
                    } else {
                        // we can not decide 3gpp2 roaming state here
                        currentServiceState.setDataRoamingType(ServiceState.ROAMING_TYPE_UNKNOWN);
                    }
                } else {
                    if (ServiceState.isCdma(dataRegType)) {
                        if (isVoiceInService) {
                            // CDMA data should have the same state as voice
                            currentServiceState.setDataRoamingType(currentServiceState
                                    .getVoiceRoamingType());
                        } else {
                            // we can not decide CDMA data roaming type without voice
                            // set it as same as last time
                            currentServiceState.setDataRoamingType(ServiceState
                                    .ROAMING_TYPE_UNKNOWN);
                        }
                    } else {
                        // take it as 3GPP roaming
                        if (inSameCountry(currentServiceState.getDataOperatorNumeric())) {
                            currentServiceState.setDataRoamingType(ServiceState
                                    .ROAMING_TYPE_DOMESTIC);
                        } else {
                            currentServiceState.setDataRoamingType(
                                    ServiceState.ROAMING_TYPE_INTERNATIONAL);
                        }
                    }
                }
            }
        }
    }

    /// M: Add for VOLTE @{
    private final boolean isConcurrentVoiceAndDataAllowedForVolte() {
        if (mSS.getDataRegState() == ServiceState.STATE_IN_SERVICE
                && ServiceState.isLte(mSS.getRilDataRadioTechnology())
                && getImsServiceState() == ServiceState.STATE_IN_SERVICE) {
            return true;
        } else {
            return false;
        }
    }

    private final int getImsServiceState() {
        final Phone imsPhone = mPhone.getImsPhone();
        if (imsPhone != null && imsPhone.isVolteEnabled() && mPhone.isImsUseEnabled()) {
            return imsPhone.getServiceState().getState();
        }
        return ServiceState.STATE_OUT_OF_SERVICE;
    }
    /// @}

    /**
     * Post a notification to NotificationManager for network reject cause
     *
     * @param cause
     */
    private void setRejectCauseNotification(int cause) {
        if (DBG) log("setRejectCauseNotification: create notification " + cause);

        Context context = mPhone.getContext();
        mNotificationBuilder = new Notification.Builder(context);
        mNotificationBuilder.setWhen(System.currentTimeMillis());
        mNotificationBuilder.setAutoCancel(true);
        mNotificationBuilder.setSmallIcon(com.android.internal.R.drawable.stat_sys_warning);

        Intent intent = new Intent();
        mNotificationBuilder.setContentIntent(PendingIntent.
                getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));

        CharSequence details = "";
        CharSequence title = context.getText(com.mediatek.R.string.RejectCauseTitle);
        int notificationId = REJECT_NOTIFICATION;

        switch (cause) {
            case 2:
                details = context.getText(com.mediatek.R.string.MMRejectCause2);;
                break;
            case 3:
                details = context.getText(com.mediatek.R.string.MMRejectCause3);;
                break;
            case 5:
                details = context.getText(com.mediatek.R.string.MMRejectCause5);;
                break;
            case 6:
                details = context.getText(com.mediatek.R.string.MMRejectCause6);;
                break;
            case 13:
                details = context.getText(com.mediatek.R.string.MMRejectCause13);
                break;
            default:
                break;
        }

        if (DBG) log("setRejectCauseNotification: put notification " + title + " / " + details);
        mNotificationBuilder.setContentTitle(title);
        mNotificationBuilder.setContentText(details);

        NotificationManager notificationManager = (NotificationManager)
            context.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotification = mNotificationBuilder.build();
        notificationManager.notify(notificationId, mNotification);
    }

    /**
     * Post a notification to NotificationManager for spcial icc card type
     *
     * @param cause
     */
    //[ALPS01558804] MTK-START: send notification for using some spcial icc card
    private void setSpecialCardTypeNotification(String iccCardType, int titleType, int detailType) {
        if (DBG) log("setSpecialCardTypeNotification: create notification for " + iccCardType);

        //status notification
        Context context = mPhone.getContext();
        int notificationId = SPECIAL_CARD_TYPE_NOTIFICATION;

        mNotificationBuilder = new Notification.Builder(context);
        mNotificationBuilder.setWhen(System.currentTimeMillis());
        mNotificationBuilder.setAutoCancel(true);
        mNotificationBuilder.setSmallIcon(com.android.internal.R.drawable.stat_sys_warning);

        Intent intent = new Intent();
        mNotificationBuilder.setContentIntent(PendingIntent.
                getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
        CharSequence title = "";
        switch (titleType) {
            case 0:
                title = context.getText(
                        com.mediatek.R.string.Special_Card_Type_Title_Lte_Not_Available);
                break;
            default:
                break;
        }

        CharSequence details = "";
        switch (detailType) {
            case 0:
                details = context.getText(com.mediatek.R.string.Suggest_To_Change_USIM);
                break;
            default:
                break;
        }

        if (DBG) log("setSpecialCardTypeNotification: put notification " + title + " / " + details);
        mNotificationBuilder.setContentTitle(title);
        mNotificationBuilder.setContentText(details);


        NotificationManager notificationManager = (NotificationManager)
            context.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotification = mNotificationBuilder.build();
        notificationManager.notify(notificationId, mNotification);
    }
    //[ALPS01558804] MTK-END: send notification for using some spcial icc card

    /* M:
     * Allow only one phone, which got the last nitz, to update the time.
     */
    private boolean checkRightToSetTime() {
        if (mLastPhoneGetNitz != SubscriptionManager.INVALID_PHONE_INDEX &&
                mLastPhoneGetNitz != mPhone.getPhoneId()) {
            if (DBG) log("Other phone(" + mLastPhoneGetNitz + ") got the newest NITZ");
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void setAndBroadcastNetworkSetTime(long time) {
        if (checkRightToSetTime()) {
            super.setAndBroadcastNetworkSetTime(time);
        }
    }

    @Override
    protected void setAndBroadcastNetworkSetTimeZone(String zoneId) {
        if (checkRightToSetTime()) {
            super.setAndBroadcastNetworkSetTimeZone(zoneId);
        }
    }

    @Override
    protected boolean isOperatorConsideredNonRoaming(ServiceState s) {
        String simNumeric = ((TelephonyManager) mPhone.getContext().
                getSystemService(Context.TELEPHONY_SERVICE)).
                getSimOperatorNumericForPhone(getPhoneId());
        String operatorNumeric = s.getOperatorNumeric();
        int simOperatorIndex = mLastOperatorConsideredNonRoamingIndex;

        if (TextUtils.isEmpty(operatorNumeric) || TextUtils.isEmpty(simNumeric)) return false;

        if (simOperatorIndex == -1 ||
                !simNumeric.equals(operatorConsideredNonRoaming[simOperatorIndex][0])) {
            for (int i = 0; i < operatorConsideredNonRoaming.length; i++) {
                if (simNumeric.startsWith(operatorConsideredNonRoaming[i][0])) {
                    mLastOperatorConsideredNonRoamingIndex = i;
                    simOperatorIndex = i;
                    break;
                }
            }
        }

        if (simOperatorIndex == -1) return false;

        for (int i = 1; i < operatorConsideredNonRoaming[simOperatorIndex].length; i++) {
            if (operatorNumeric.startsWith(operatorConsideredNonRoaming[simOperatorIndex][i])) {
                return true;
            }
        }

        return false;
    }

    @Override
    protected boolean isOperatorConsideredRoaming(ServiceState s) {
        String simNumeric = ((TelephonyManager) mPhone.getContext().
                getSystemService(Context.TELEPHONY_SERVICE)).
                getSimOperatorNumericForPhone(getPhoneId());
        String operatorNumeric = s.getOperatorNumeric();
        int simOperatorIndex = mLastSameNamedOperatorConsideredRoamingIndex;

        if (TextUtils.isEmpty(operatorNumeric) || TextUtils.isEmpty(simNumeric)) return false;

        if (simOperatorIndex == -1 ||
                !simNumeric.equals(sameNamedOperatorConsideredRoaming[simOperatorIndex][0])) {
            for (int i = 0; i < sameNamedOperatorConsideredRoaming.length; i++) {
                if (simNumeric.startsWith(sameNamedOperatorConsideredRoaming[i][0])) {
                    mLastSameNamedOperatorConsideredRoamingIndex = i;
                    simOperatorIndex = i;
                    break;
                }
            }
        }

        if (simOperatorIndex == -1) return false;

        for (int i = 1; i < sameNamedOperatorConsideredRoaming[simOperatorIndex].length; i++) {
            if (operatorNumeric.startsWith(sameNamedOperatorConsideredRoaming[simOperatorIndex][i]))
            {
                return true;
            }
        }

        return false;
    }

    /* M:
     * Find timezone of captital city with iso
     */
    private TimeZone getTimeZonesWithCapitalCity(String iso) {
        TimeZone tz = null;
        for (int i = 0; i < mTimeZoneIdOfCapitalCity.length; i++) {
            if (iso.equals(mTimeZoneIdOfCapitalCity[i][0])) {
                tz = TimeZone.getTimeZone(mTimeZoneIdOfCapitalCity[i][1]);
                break;
            }
        }
        return tz;
    }

    /* M:
     * Save the timezone when no NITZ
     * If we got NITZ but still guess tz, tz is saved in mSavedTimeZone.
     * we can use it when user turn on the auto tz.
     */
    private void saveGuessTimeZone(String zoneId) {
        mSavedGuessTimeZone = zoneId;
    }

    @Override
    protected void revertToNitzTimeZone() {
        super.revertToNitzTimeZone();
        // M: update the time zone which is guessed when no NITZ
        if (mSavedTimeZone == null && mSavedGuessTimeZone != null) {
            if (DBG) log("revertToNitzTimeZone - try the guess one: " + mSavedGuessTimeZone);
            setAndBroadcastNetworkSetTimeZone(mSavedGuessTimeZone);
        }
    }

    @Override
    protected void onUpdateIccAvailability() {
        if (mUiccController == null ) {
            return;
        }

        UiccCardApplication newUiccApplication = getUiccCardApplication();
        ///M: [Network][C2K] Add for show EccButton when PIN and PUK status. @{
        if (mPhone.isPhoneTypeCdma() || mPhone.isPhoneTypeCdmaLte()) {
            if (newUiccApplication != null) {
                AppState appState = newUiccApplication.getState();
                if ((appState == AppState.APPSTATE_PIN || appState == AppState.APPSTATE_PUK)
                        && mNetworkExsit) {
                    mEmergencyOnly = true;
                } else {
                    mEmergencyOnly = false;
                }
                log("[CDMA]onUpdateIccAvailability, appstate=" + appState
                        + ", mNetworkExsit=" + mNetworkExsit
                        + ", mEmergencyOnly=" + mEmergencyOnly);
            }
        }
        /// @}
        if (mUiccApplcation != newUiccApplication) {
            if (mUiccApplcation != null) {
                log("Removing stale icc objects.");
                mUiccApplcation.unregisterForReady(this);
                if (mIccRecords != null) {
                    mIccRecords.unregisterForRecordsLoaded(this);
                    if (mPhone.isPhoneTypeGsm()) {
                        mIccRecords.unregisterForRecordsEvents(this);
                    }
                }
                mIccRecords = null;
                mUiccApplcation = null;
            }
            if (newUiccApplication != null) {
                log("New card found");
                mUiccApplcation = newUiccApplication;
                mIccRecords = mUiccApplcation.getIccRecords();
                if (mPhone.isPhoneTypeGsm()) {
                    mUiccApplcation.registerForReady(this, EVENT_SIM_READY, null);
                    if (mIccRecords != null) {
                        mIccRecords.registerForRecordsLoaded(this, EVENT_SIM_RECORDS_LOADED, null);
                        mIccRecords.registerForRecordsEvents(this, EVENT_SIM_OPL_LOADED, null);
                    }
                } else if (mIsSubscriptionFromRuim) {
                    mUiccApplcation.registerForReady(this, EVENT_RUIM_READY, null);
                    if (mIccRecords != null) {
                        mIccRecords.registerForRecordsLoaded(this, EVENT_RUIM_RECORDS_LOADED, null);
                    }
                }
            }
        }
    }

    @Override
    protected void updateOperatorNameFromEri() {
        if (mPhone.isPhoneTypeCdma()) {
            if ((mCi.getRadioState().isOn()) && (!mIsSubscriptionFromRuim)) {
                String eriText;
                // Now the Phone sees the new ServiceState so it can get the new ERI text
                if (mSS.getVoiceRegState() == ServiceState.STATE_IN_SERVICE) {
                    eriText = mPhone.getCdmaEriText();
                } else {
                    // Note that ServiceState.STATE_OUT_OF_SERVICE is valid used for
                    // mRegistrationState 0,2,3 and 4
                    eriText = mPhone.getContext().getText(
                            com.android.internal.R.string.roamingTextSearching).toString();
                }
                mSS.setOperatorAlphaLong(eriText);
            }
        } else if (mPhone.isPhoneTypeCdmaLte()) {
            boolean hasBrandOverride = mUiccController.getUiccCard(getPhoneId()) != null &&
                    mUiccController.getUiccCard(getPhoneId()).getOperatorBrandOverride() != null;
            if (!hasBrandOverride && (mCi.getRadioState().isOn()) && (mPhone.isEriFileLoaded()) &&
                    (!ServiceState.isLte(mSS.getRilVoiceRadioTechnology()) ||
                            mPhone.getContext().getResources().getBoolean(com.android.internal.R.
                            bool.config_LTE_eri_for_network_name)) &&
                            /// M: [Network][C2K] Sprint roaming control @{
                            mEnableERI) {
                            /// @}
                // Only when CDMA is in service, ERI will take effect
                String eriText = mSS.getOperatorAlpha();
                // Now the Phone sees the new ServiceState so it can get the new ERI text
                if (mSS.getVoiceRegState() == ServiceState.STATE_IN_SERVICE) {
                    /// M: [Network][C2K] Sprint roaming control @{
                    // Append ERI text to the end of PLMN
                    // eriText = mPhone.getCdmaEriText();
                    if (mSS.getOperatorAlphaLong().length() == 0) {
                        eriText = mPhone.getCdmaEriText();
                    } else if (mPhone.getCdmaEriText() != null &&
                            !mPhone.getCdmaEriText().equals("") &&
                            (mSS.getCdmaRoamingIndicator() != 1) && //Sprint GTR-SYSSEL-00061
                            (mSS.getCdmaRoamingIndicator() != 160)) { //Sprint GTR-SYSSEL-00063
                        log("Append ERI text to PLMN String");
                        eriText = mSS.getOperatorAlphaLong() + "- " + mPhone.getCdmaEriText();
                    }
                    /// @}
                } else if (mSS.getVoiceRegState() == ServiceState.STATE_POWER_OFF) {
                    eriText = (mIccRecords != null) ? mIccRecords.getServiceProviderName() : null;
                    if (TextUtils.isEmpty(eriText)) {
                        // Sets operator alpha property by retrieving from
                        // build-time system property
                        eriText = SystemProperties.get("ro.cdma.home.operator.alpha");
                    }
                } else if (mSS.getDataRegState() != ServiceState.STATE_IN_SERVICE) {
                    // Note that ServiceState.STATE_OUT_OF_SERVICE is valid used
                    // for mRegistrationState 0,2,3 and 4
                    eriText = mPhone.getContext()
                            .getText(com.android.internal.R.string.roamingTextSearching).toString();
                }
                mSS.setOperatorAlphaLong(eriText);
            }

            if (mUiccApplcation != null && mUiccApplcation.getState() == AppState.APPSTATE_READY &&
                    mIccRecords != null && getCombinedRegState() == ServiceState.STATE_IN_SERVICE
                    && !ServiceState.isLte(mSS.getRilVoiceRadioTechnology())) {
                // SIM is found on the device. If ERI roaming is OFF, and SID/NID matches
                // one configured in SIM, use operator name from CSIM record. Note that ERI, SID,
                // and NID are CDMA only, not applicable to LTE.
                boolean showSpn =
                        ((RuimRecords) mIccRecords).getCsimSpnDisplayCondition();
                int iconIndex = mSS.getCdmaEriIconIndex();

                if (showSpn && (iconIndex == EriInfo.ROAMING_INDICATOR_OFF) &&
                        isInHomeSidNid(mSS.getSystemId(), mSS.getNetworkId()) &&
                        mIccRecords != null) {
                    mSS.setOperatorAlphaLong(mIccRecords.getServiceProviderName());
                }
            }
        }
    }

    @Override
    protected void setOperatorIdd(String operatorNumeric) {
        // Retrieve the current country information
        // with the MCC got from opeatorNumeric.
        /// M: Use try catch to avoid Integer pars exception @{
        String idd = "";
        try {
            idd = mHbpcdUtils.getIddByMcc(
                Integer.parseInt(operatorNumeric.substring(0,3)));
        } catch (NumberFormatException ex) {
            loge("setOperatorIdd: idd error" + ex);
        } catch (StringIndexOutOfBoundsException ex) {
            loge("setOperatorIdd: idd error" + ex);
        }
        /// @}
        if (idd != null && !idd.isEmpty()) {
            mPhone.setSystemProperty(TelephonyProperties.PROPERTY_OPERATOR_IDP_STRING,
                    idd);
        } else {
            // use default "+", since we don't know the current IDP
            mPhone.setSystemProperty(TelephonyProperties.PROPERTY_OPERATOR_IDP_STRING, "+");
        }
    }

    /**
     * Radio power set from carrier action. if set to false means carrier desire to turn radio off
     * and radio wont be re-enabled unless carrier explicitly turn it back on.
     * @param enable indicate if radio power is enabled or disabled from carrier action.
     */
    @Override
    public void setRadioPowerFromCarrier(boolean enable) {
        mRadioDisabledByCarrier = !enable;
        // Replace aosp logic. Using RadioManager to controller the radio logic
        // setPowerStateToDesired();
        RadioManager.getInstance().setRadioPower(enable, mPhone.getPhoneId());
    }

    /// M: [Network][C2K] Sprint roaming control @{
    private String getSIMOperatorNumeric() {
        IccRecords r = mIccRecords;
        String mccmnc;
        String imsi;

        if (r != null) {
            mccmnc = r.getOperatorNumeric();

            if (mccmnc == null) {
                imsi = r.getIMSI();
                if (imsi != null && !imsi.equals("")) {
                    mccmnc = imsi.substring(0, 5);
                    log("get MCC/MNC from IMSI = " + mccmnc);
                }
            }
            if (mPhone.isPhoneTypeGsm()) {
                if (mccmnc == null || mccmnc.equals("")) {
                    String simMccMncProp = "gsm.ril.uicc.mccmnc";
                    if (mPhone.getPhoneId() != 0) {
                        simMccMncProp += "." + mPhone.getPhoneId();
                    }
                    mccmnc = SystemProperties.get(simMccMncProp, "");
                    log("get MccMnc from property(" + simMccMncProp + "): " + mccmnc);
                }
            }
            return mccmnc;
        } else {
            return null;
        }
    }
    /// @}

    // anchor method for powerOffRadioSafely
    @Override
    protected int mtkReplaceDdsIfUnset(int dds) {
        if (dds == SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
            int[] subIds = SubscriptionManager.getSubId(
                    RadioCapabilitySwitchUtil.getMainCapabilityPhoneId());
            if (subIds != null && subIds.length > 0) {
                log("powerOffRadioSafely: replace dds with main protocol sub ");
                dds = subIds[0];
            }
        }
        return dds;
    }

    // anchor method for powerOffRadioSafely
    @Override
    protected void mtkRegisterAllDataDisconnected() {
        synchronized (this) {
            log("powerOffRadioSafely: register EVENT_ALL_DATA_DISCONNECTED for self phone");
            mPhone.registerForAllDataDisconnected(this, EVENT_ALL_DATA_DISCONNECTED, null);
            mPendingRadioPowerOffAfterDataOff = true;
        }
    }

    /**
     * Lookup operator name by numeric for network only.
     *
     * @param context the context used to get resource.
     * @param subId subId the id of the subscription to be queried.
     * @param numeric numeric name (MCC+MNC).
     * @param desireLongName whether is desire long name.
     * @return the operator name for the specific numeric.
     */
    protected static final String lookupOperatorName(Context context, int subId, String numeric,
            boolean desireLongName) {
        final String defaultName = numeric;
        String operName = null;

        // Step 1. check if phone is available.
        Phone phone = PhoneFactory.getPhone(SubscriptionManager.getPhoneId(subId));
        if (phone == null) {
            Rlog.e(LOG_TAG, "lookupOperatorName getPhone null");
            return defaultName;
        }

        // Step 2. get spn from mvno parrtern.
        operName = MtkSpnOverride.getInstance().getSpnByPattern(subId, numeric);

        // Step 3. check if is China Telecom MVNO.
        boolean isChinaTelecomMvno = isChinaTelecomMvno(context, subId, numeric, operName);

        // Step 4. get Spn by numeric from resource and spn_conf.xml
        if (operName == null || isChinaTelecomMvno) {
            operName = MtkSpnOverride.getInstance().getSpnByNumeric(numeric, desireLongName,
                    context);
        }

        // Step5. if didn't found any spn, return default name.
        return ((operName == null) ? defaultName : operName);
    }

    /**
     * Return whether is China Telecom MVNO for the specific numeric.
     *
     * @param context the context used to get resource.
     * @param subId subId the id of the subscription to be queried.
     * @param numeric numeric name (MCC+MNC).
     * @param mvnoOperName MVNO operator name.
     * @return true if is China Telecom MVNO.
     */
    private static final boolean isChinaTelecomMvno(Context context, int subId, String numeric,
            String mvnoOperName) {
        boolean isChinaTelecomMvno = false;
        final String ctName = context.getText(com.mediatek.internal.R.string.ct_name).toString();
        final String simCarrierName = TelephonyManager.from(context).getSimOperatorName(subId);
        if (ctName != null && ctName.equals(mvnoOperName)) {
            isChinaTelecomMvno = true;
        } else if (("20404".equals(numeric) || "45403".equals(numeric))
                && (ctName != null && ctName.equals(simCarrierName))) {
            isChinaTelecomMvno = true;
        }
        return isChinaTelecomMvno;
    }

    @Override
    protected boolean onSignalStrengthResult(AsyncResult ar) {
        String mlog = "";
        if (mSignalStrength != null) {
            mlog = "old:{level:" + mSignalStrength.getLevel()
                + ", raw:"+ mSignalStrength.toString() + "}, ";
        }

        boolean isGsm = false;
        int dataRat = mSS.getRilDataRadioTechnology();
        int voiceRat = mSS.getRilVoiceRadioTechnology();

        // Override isGsm based on currently camped data and voice RATs
        // Set isGsm to true if the RAT belongs to GSM family and not IWLAN
        if ((dataRat != ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN
                && ServiceState.isGsm(dataRat))
                || (voiceRat != ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN
                && ServiceState.isGsm(voiceRat))) {
            isGsm = true;
        }
        // M: isGsm when type of cs/ps is unknown
        if (voiceRat == ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN &&
                dataRat == ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN &&
                mPhone.isPhoneTypeGsm()) {
            isGsm = true;
        }

        // This signal is used for both voice and data radio signal so parse
        // all fields

        if ((ar.exception == null) && (ar.result != null)) {
            mSignalStrength = (SignalStrength) ar.result;
            mSignalStrength.validateInput();
            mSignalStrength.setGsm(isGsm);
            mSignalStrength.setLteRsrpBoost(mSS.getLteEarfcnRsrpBoost());
        } else {
            log("onSignalStrengthResult() Exception from RIL : " + ar.exception);
            mSignalStrength = new SignalStrength(isGsm);
        }

        if (mSignalStrength != null) {
            mlog = mlog + "new:{level:" + mSignalStrength.getLevel()
                + ", raw:"+ mSignalStrength.toString() + "}";
        }
        log(mlog);

        boolean ssChanged = notifySignalStrength();
        return ssChanged;
    }

    @Override
    protected boolean currentMccEqualsSimMcc(ServiceState s) {
        String simNumeric = ((TelephonyManager) mPhone.getContext().
                getSystemService(Context.TELEPHONY_SERVICE)).
                getSimOperatorNumericForPhone(getPhoneId());
        String operatorNumeric = s.getOperatorNumeric();
        //M: default should be false
        boolean equalsMcc = false;

        try {
            equalsMcc = simNumeric.substring(0, 3).
                    equals(operatorNumeric.substring(0, 3));
        } catch (Exception e){
        }
        return equalsMcc;
    }

    // anchor method of powerOffRadioSafely to customized the data disconnected timer
    protected int mtkReplaceDisconnectTimer() {
        if (isDeviceShuttingDown()) {
            log("Shutting down, reduce 30s->5s for data to disconnect, then turn off radio.");
            return 5000;
        } else {
            return 30000;
        }
    }
};
