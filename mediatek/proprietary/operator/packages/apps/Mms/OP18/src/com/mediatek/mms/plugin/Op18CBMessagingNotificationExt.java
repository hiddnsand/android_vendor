package com.mediatek.mms.plugin;

import android.content.Context;
import android.util.Log;
import android.content.SharedPreferences;
import com.mediatek.mms.ext.DefaultOpCBMessagingNotificationExt;

/**
 * Plugin implementation for CB message notification plugin
 */
public class Op18CBMessagingNotificationExt extends DefaultOpCBMessagingNotificationExt {
    private static final String TAG = "Op18CBMessagingNotificationExt";
    private Context mContext;

    /** Constructor.
     * @param context context
     */
    public Op18CBMessagingNotificationExt(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public String getRingtoneStr(int subId, String notificationRingtone,
            SharedPreferences sp, String ringtoneStr) {
        String ringtoneKey = Long.toString(subId) + "_" + notificationRingtone;
        Log.d(TAG, "Ringtone key:" + ringtoneKey);
        String newRingtoneStr = sp.getString(ringtoneKey, null);
        Log.d(TAG, "Ringtone string:" + newRingtoneStr);
        return newRingtoneStr;
    }
}
