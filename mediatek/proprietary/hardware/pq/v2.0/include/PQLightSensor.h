#ifndef __PQ_LIGHT_SENSOR__
#define __PQ_LIGHT_SENSOR__

#include <utils/threads.h>
#include <fcntl.h>
#include <dlfcn.h>

using ::android::Thread;
using ::android::Mutex;

typedef long long millisec_t;

class PQLightSensor : public Thread
{
public:
    typedef void (*ListenerCallback)(void *user, int32_t aliR, int32_t aliG, int32_t aliB, int32_t aliW);
    typedef int  (*PQSensorInit)(void);
    typedef void (*PQSensorDeInit)(void);
    typedef void (*PQSensorLoop)(void);
    typedef int  (*PQSensorEnable)(int enable);
    typedef void (*PQSensorListenser)(void* user, int aliR, int aliG, int aliB, int aliW);
    typedef void (*PQSensorCallback)(PQSensorListenser pqSensorListenser, void *user);

private:
    static millisec_t DEBOUNCE_TIME;
    static millisec_t SHORT_TERM_PERIOD, LONG_TERM_PERIOD;

    Mutex mLock;
    bool mIsEnabled;
    bool mIsSensorInited;
    millisec_t mLastObservedLuxTime;
    int mShortTermAverageLux;
    int mLongTermAverageLux;
    millisec_t mDebounceLuxTime;
    int mDebouncedLux;
    int mNotifiedLux;
    volatile bool mContRunning;

    static millisec_t getTime();

    void updateLuxValue(int alsR, int alsG, int alsB, int alsW);
    static void sensorListener(void *obj, int alsR, int alsG, int alsB, int alsW);
    template <typename T>
    inline T getFunction(const char *name) {
        return reinterpret_cast<T>(dlsym(mLib, name));
    }

    ListenerCallback mListenerCallback;
    void *mListenerUser;
    void *mLib;

    PQSensorInit mCustPQSensorInitFun;
    PQSensorDeInit mCustPQSensorDeInitFun;
    PQSensorLoop mCustPQSensorLoopFun;
    PQSensorEnable mCustPQSensorEnableFun;
    PQSensorCallback mPQSensorCallback;

public:

    PQLightSensor();
    ~PQLightSensor();

    void setListener(ListenerCallback callback, void *user) {
        mListenerCallback = callback;
        mListenerUser = user;
    }

    bool isEnabled() {
        return mIsEnabled;
    }

    bool isRGBWLibGood() {
        return (mLib != NULL);
    }

    void setEnabled(bool enabled);

    int getLuxValue() {
        Mutex::Autolock _l(mLock);
        return mDebouncedLux;
    }

    virtual bool threadLoop();
};

#endif

