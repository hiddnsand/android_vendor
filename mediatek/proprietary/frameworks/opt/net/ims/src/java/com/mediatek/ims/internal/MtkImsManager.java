/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims.internal;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.IBinder;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telecom.TelecomManager;
import android.telephony.CarrierConfigManager;
import android.telephony.Rlog;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.ims.ImsCall;
import com.android.ims.ImsCallProfile;
import com.android.ims.ImsConfig;
import com.android.ims.ImsConnectionStateListener;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.ims.ImsReasonInfo;
import com.android.ims.ImsUt;
import com.android.ims.ImsUtInterface;
import com.android.ims.internal.IImsCallSession;
import com.android.ims.internal.IImsEcbm;
import com.android.ims.internal.IImsEcbmListener;
import com.android.ims.internal.IImsMultiEndpoint;
import com.android.ims.internal.IImsRegistrationListener;
import com.android.ims.internal.IImsService;
import com.android.ims.internal.IImsUt;
import com.android.ims.internal.ImsCallSession;
import com.android.ims.internal.IImsConfig;
import com.android.internal.telephony.IPhoneSubInfo;

import com.mediatek.common.carrierexpress.CarrierExpressManager;
import com.mediatek.ims.internal.ext.IImsManagerExt;
import com.mediatek.ims.internal.ext.OpImsCustomizationUtils;

import com.mediatek.ims.MtkImsCall;
import com.mediatek.ims.MtkImsUt;
import com.mediatek.ims.internal.MtkImsConfig;
import com.mediatek.ims.internal.IMtkImsConfig;
import com.mediatek.ims.internal.IMtkImsService;
import com.mediatek.ims.internal.IMtkImsUt;
import com.mediatek.internal.telephony.IMtkTelephonyEx;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;

import mediatek.telephony.MtkCarrierConfigManager;

/**
 * Provides APIs for MTK IMS services or modified AOSP APIs.
 * @hide
 */
public class MtkImsManager extends ImsManager {

    /// M: IMS VoLTE refactoring. @{
    /**
     * Key to retrieve the sequence number from an incoming call intent.
     * @see #open(PendingIntent, ImsConnectionStateListener)
     * @hide
     */
    public static final String EXTRA_SEQ_NUM = "android:imsSeqNum";

    /*
     * Key to retrieve the sequence number from an incoming call intent.
     * @see #open(PendingIntent, ImsConnectionStateListener)
     * @hide
     */
    public static final String EXTRA_DIAL_STRING = "android:imsDialString";
    /// @}

    /// M: IMS VoLTE refactoring. @{
    /**
     * Key to retrieve the call mode from an incoming call intent.
     * @see #open(PendingIntent, ImsConnectionStateListener)
     * @hide
     */
    public static final String EXTRA_CALL_MODE = "android:imsCallMode";

    /// M: For IMS deregister done notification. @{
    public static final String ACTION_IMS_SERVICE_DEREGISTERED =
            "com.android.ims.IMS_SERVICE_DEREGISTERED";
    /// @}

    /// M: For IMS RTP report notification. @{
    public static final String ACTION_IMS_RTP_INFO =
            "com.android.ims.IMS_RTP_INFO";
    /// @}

    /// M: For IMS radio change notification. @{
    public static final String ACTION_IMS_RADIO_STATE_CHANGED =
            "com.android.ims.IMS_RADIO_STATE_CHANGED";
    /// @}

    /// M: For IMS radio change notification. @{
    public static final String EXTRA_IMS_RADIO_STATE = "android:imsRadioState";
    /// @}

    /**
     * Action for the incoming call indication intent for the Phone app.
     * Internal use only.
     * @hide
     */
    public static final String ACTION_IMS_INCOMING_CALL_INDICATION =
            "com.android.ims.IMS_INCOMING_CALL_INDICATION";

    /* M WFC */
    public static final String EXTRA_IMS_REG_STATE_KEY = "android:regState";
    public static final String EXTRA_IMS_ENABLE_CAP_KEY = "android:enablecap";
    public static final String EXTRA_IMS_DISABLE_CAP_KEY = "android:disablecap";
    public static final String EXTRA_IMS_REG_ERROR_KEY = "android:regError";
    /* M WFC */

    /// M: For IMS RTP report notification. @{
    /**
     * Part of the ACTION_IMS_RTP_INFO intents.
     * @hide
     */
    public static final String EXTRA_RTP_PDN_ID = "android:rtpPdnId";
    public static final String EXTRA_RTP_NETWORK_ID = "android:rtpNetworkId";
    public static final String EXTRA_RTP_TIMER = "android:rtpTimer";
    public static final String EXTRA_RTP_SEND_PKT_LOST = "android:rtpSendPktLost";
    public static final String EXTRA_RTP_RECV_PKT_LOST = "android:rtpRecvPktLost";

    /// M: Telephony global configuration
    private static final String LTE_SUPPORT = "ro.boot.opt_lte_support";

    private static final String MULTI_IMS_SUPPORT = "ro.mtk_multiple_ims_support";

    private static final String PROPERTY_CAPABILITY_SWITCH = "persist.radio.simswitch";

    /// M: MIMS related
    //  Ims user setting keys
    private static final String VOLTE_SETTING = "volte_setting";
    private static final String TTY_MODE = "tty_mode";
    private static final String VILTE_SETTING = "vilte_setting";
    private static final String WFC_SETTING = "wfc_setting";
    private static final String WFC_MODE_SETTING = "wfc_mode_setting";
    private static final String WFC_ROAMING_MODE_SETTING = "wfc_roaming_mode_setting";
    private static final String WFC_ROAMING_SETTING = "wfc_roaming_setting";

    //  SIM ID mapping
    protected static final int SIM_ID_1 = 0;
    protected static final int SIM_ID_2 = 1;
    protected static final int SIM_ID_3 = 2;
    protected static final int SIM_ID_4 = 3;

    //  MIMS Enhanced 4G Settings Key
    public static final String ENHANCED_4G_MODE_ENABLED_SIM2 = "volte_vt_enabled_sim2";
    public static final String ENHANCED_4G_MODE_ENABLED_SIM3 = "volte_vt_enabled_sim3";
    public static final String ENHANCED_4G_MODE_ENABLED_SIM4 = "volte_vt_enabled_sim4";
    //  MIMS VT Settings Key
    public static final String VT_IMS_ENABLED_SIM2 = "vt_ims_enabled_sim2";
    public static final String VT_IMS_ENABLED_SIM3 = "vt_ims_enabled_sim3";
    public static final String VT_IMS_ENABLED_SIM4 = "vt_ims_enabled_sim4";
    //  MIMS WFC Settings Key
    public static final String WFC_IMS_ENABLED_SIM2 = "wfc_ims_enabled_sim2";
    public static final String WFC_IMS_ENABLED_SIM3 = "wfc_ims_enabled_sim3";
    public static final String WFC_IMS_ENABLED_SIM4 = "wfc_ims_enabled_sim4";
    public static final String WFC_IMS_MODE_SIM2 = "wfc_ims_mode_sim2";
    public static final String WFC_IMS_MODE_SIM3 = "wfc_ims_mode_sim3";
    public static final String WFC_IMS_MODE_SIM4 = "wfc_ims_mode_sim4";
    public static final String WFC_IMS_ROAMING_ENABLED_SIM2 = "wfc_ims_roaming_enabled_sim2";
    public static final String WFC_IMS_ROAMING_ENABLED_SIM3 = "wfc_ims_roaming_enabled_sim3";
    public static final String WFC_IMS_ROAMING_ENABLED_SIM4 = "wfc_ims_roaming_enabled_sim4";
    public static final String WFC_IMS_ROAMING_MODE_SIM2 = "wfc_ims_roaming_mode_sim2";
    public static final String WFC_IMS_ROAMING_MODE_SIM3 = "wfc_ims_roaming_mode_sim3";
    public static final String WFC_IMS_ROAMING_MODE_SIM4 = "wfc_ims_roaming_mode_sim4";
    //  MIMS TTY Settings Key
    public static final String PREFERRED_TTY_MODE =
        "preferred_tty_mode";
    public static final String PREFERRED_TTY_MODE_SIM2 =
        "preferred_tty_mode_sim2";
    public static final String PREFERRED_TTY_MODE_SIM3 =
        "preferred_tty_mode_sim3";
    public static final String PREFERRED_TTY_MODE_SIM4 =
        "preferred_tty_mode_sim4";

    /**
     * For accessing the Mediatek IMS related service.
     * Internal use only.
     * @hide
     */
    public static final String MTK_IMS_SERVICE = "mtkIms";

    private static final String TAG = "MtkImsManager";
    private static final boolean DBG = true;
    private IMtkImsService mMtkImsService = null;
    private MtkImsServiceDeathRecipient mMtkDeathRecipient = new MtkImsServiceDeathRecipient();

    // ImsManager plugin instance
    private static IImsManagerExt sImsManagerExt = null;

    private MtkImsUt mMtkUt = null;

    // Interface to get/set ims config items from MtkImsConfig
    private MtkImsConfig mConfigEx = null;

    public static boolean isSupportMims() {
        return (SystemProperties.getInt(MULTI_IMS_SUPPORT, 1) > 1);
    }

    public static int getMainPhoneIdForSingleIms(Context context) {
        // duplication from RadioCapabilitySwitchUtil
        int phoneId = 0;
        phoneId = SystemProperties.getInt(PROPERTY_CAPABILITY_SWITCH, 1) - 1;
        log("[getMainPhoneIdForSingleIms] : " + phoneId);
        return phoneId;
    }

    /**
     * MIMS
     */
    public static boolean isEnhanced4gLteModeSettingEnabledByUser(Context context, int phoneId) {
        // If user can't edit Enhanced 4G LTE Mode, it assumes Enhanced 4G LTE Mode is always true.
        // If user changes SIM from editable mode to uneditable mode, need to return true.

        int enabled = ImsConfig.FeatureValueConstants.OFF;

        if (isSupportMims() == false) {
            if (!ImsManager.getBooleanCarrierConfig(context,
                        CarrierConfigManager.KEY_EDITABLE_ENHANCED_4G_LTE_BOOL)) {
                return true;
            }

            enabled = android.provider.Settings.Global.getInt(
                    context.getContentResolver(),
                    android.provider.Settings.Global.ENHANCED_4G_MODE_ENABLED,
                    ImsConfig.FeatureValueConstants.ON);
        } else {
            ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if(imsManager != null) {
                return imsManager.isEnhanced4gLteModeSettingEnabledByUserForSlot();
            } else {
                loge("isEnhanced4gLteModeSettingEnabledByUser");
                loge("getInstance null for phoneId=" + phoneId);
            }
        }

        return (enabled == 1) ? true : false;
    }

    /**
     * Returns the user configuration of Enhanced 4G LTE Mode setting for slot.
     */
    @Override
    public boolean isEnhanced4gLteModeSettingEnabledByUserForSlot() {
        // If user can't edit Enhanced 4G LTE Mode, it assumes Enhanced 4G LTE Mode is always true.
        // If user changes SIM from editable mode to uneditable mode, need to return true.
        if (!getBooleanCarrierConfigForSlot(
                CarrierConfigManager.KEY_EDITABLE_ENHANCED_4G_LTE_BOOL)) {
            return true;
        }
        int enabled = getSettingValueByKeyForSlot(VOLTE_SETTING);

        return (enabled == 1);
    }

    /**
     * MIMS
     */
    public static void setEnhanced4gLteModeSetting(Context context, boolean enabled, int phoneId) {
        int value = enabled ? 1 : 0;

        if (isSupportMims() == false) {
            phoneId = getMainPhoneIdForSingleIms(context);
            android.provider.Settings.Global.putInt(
                    context.getContentResolver(),
                    android.provider.Settings.Global.ENHANCED_4G_MODE_ENABLED, value);

            if (isNonTtyOrTtyOnVolteEnabled(context, phoneId)) {
                ImsManager imsManager = ImsManager.getInstance(context, phoneId);
                if (imsManager != null) {
                    try {
                        imsManager.setAdvanced4GMode(enabled);
                    } catch (ImsException ie) {
                        // do nothing
                    }
                }
            }
        } else {
            sImsManagerExt = getImsManagerPluginInstance(context);
            if (sImsManagerExt != null) {
                phoneId = sImsManagerExt.getImsPhoneId(context, phoneId);
            }
            ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if(imsManager != null) {
                imsManager.setEnhanced4gLteModeSettingForSlot(enabled);
            } else {
                loge("setEnhanced4gLteModeSetting");
                loge("getInstance null for phoneId=" + phoneId);
            }
        }

    }

    /**
     * Change persistent Enhanced 4G LTE Mode setting. If the the option is not editable
     * ({@link CarrierConfigManager#KEY_EDITABLE_ENHANCED_4G_LTE_BOOL} is false), this method will
     * always set the setting to true.
     *
     */
    @Override
    public void setEnhanced4gLteModeSettingForSlot(boolean enabled) {
        // If false, we must always keep advanced 4G mode set to true (1).
        int value = getBooleanCarrierConfigForSlot(
                CarrierConfigManager.KEY_EDITABLE_ENHANCED_4G_LTE_BOOL) ? (enabled ? 1: 0) : 1;

        int prevSetting = getSettingValueByKeyForSlot(VOLTE_SETTING);
        if (prevSetting == value) {
            // Don't trigger setAdvanced4GMode if the setting hasn't changed.
            return;
        }

        setSettingValueByKeyForSlot(VOLTE_SETTING, value);
        if (isNonTtyOrTtyOnVolteEnabledForSlot()) {
            try {
                setAdvanced4GMode(enabled);
            } catch (ImsException ie) {
                // do nothing
            }
        }
    }

    /**
     * MIMS
     */
    public static boolean isNonTtyOrTtyOnVolteEnabled(Context context, int phoneId) {

        if (isSupportMims() == false) {
            if (ImsManager.getBooleanCarrierConfig(context,
                    CarrierConfigManager.KEY_CARRIER_VOLTE_TTY_SUPPORTED_BOOL)) {
                return true;
            }

            return Settings.Secure.getInt(context.getContentResolver(),
                    Settings.Secure.PREFERRED_TTY_MODE, TelecomManager.TTY_MODE_OFF)
                    == TelecomManager.TTY_MODE_OFF;
        }

        ImsManager imsManager = ImsManager.getInstance(context, phoneId);
        if(imsManager != null) {
            return imsManager.isNonTtyOrTtyOnVolteEnabledForSlot();
        } else {
            loge("isNonTtyOrTtyOnVolteEnabled");
            loge("getInstance null for phoneId=" + phoneId);
        }

        return false;
    }

    /**
     * Indicates whether the call is non-TTY or if TTY - whether TTY on VoLTE is
     * supported on a per slot basis.
     */
    @Override
    public boolean isNonTtyOrTtyOnVolteEnabledForSlot() {
        if (getBooleanCarrierConfigForSlot(
                CarrierConfigManager.KEY_CARRIER_VOLTE_TTY_SUPPORTED_BOOL)) {
            return true;
        }

        return (getSettingValueByKeyForSlot(TTY_MODE) == TelecomManager.TTY_MODE_OFF);
    }

    /**
     * Returns a platform configuration for VoLTE which may override the user setting on a per Slot
     * basis.
     */
    @Override
    public boolean isVolteEnabledByPlatformForSlot() {
        if (SystemProperties.getInt(PROPERTY_DBG_VOLTE_AVAIL_OVERRIDE,
                PROPERTY_DBG_VOLTE_AVAIL_OVERRIDE_DEFAULT) == 1) {
            return true;
        }

        // Dynamic IMS Switch
        boolean isResourceSupport = true;
        boolean isCarrierConfigSupport = true;
        boolean isGbaValidSupport = true;
        boolean isFeatureEnableByPlatformExt = true;

        isResourceSupport = isImsResourceSupport(
                ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE);
        isCarrierConfigSupport = getBooleanCarrierConfigForSlot(
                CarrierConfigManager.KEY_CARRIER_VOLTE_AVAILABLE_BOOL);
        isGbaValidSupport = isGbaValidForSlot();
        isFeatureEnableByPlatformExt = isFeatureEnabledByPlatformExt
                (ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE);

        log("Volte, isResourceSupport:" + isResourceSupport + ", isCarrierConfigSupport:" +
                isCarrierConfigSupport + ", isGbaValidSupport:" + isGbaValidSupport +
                ", isFeatureEnableByPlatformExt:" + isFeatureEnableByPlatformExt);

        return (SystemProperties.getInt("persist.mtk_volte_support", 0) == 1)
                && (SystemProperties.getInt(LTE_SUPPORT, 0) == 1)
                && isResourceSupport
                && isCarrierConfigSupport
                && isGbaValidSupport
                && isFeatureEnableByPlatformExt;
    }


    /**
     * Change VoLTE Preference setting (AT+CEVDP)
     */
    public static void setVoltePreferSetting(Context context, int mode, int phoneId) {
        if (isSupportMims() == false) {
            phoneId = getMainPhoneIdForSingleIms(context);
        }
        sImsManagerExt = getImsManagerPluginInstance(context);
        if (sImsManagerExt != null) {
            phoneId = sImsManagerExt.getImsPhoneId(context, phoneId);
        }
        log("setVoltePreferSetting() : mode=" + mode + ", phoneId=" + phoneId);
        ImsManager imsManager = ImsManager.getInstance(context, phoneId);
        if (imsManager != null) {
            try {
                MtkImsConfig config = ((MtkImsManager)imsManager).getConfigInterfaceEx(phoneId, context);
                if (config != null) {
                    config.setVoltePreference(mode);
                }
            } catch (ImsException e) {
                loge("setVoltePreferSetting(): " + e);
            }
        }
    }

    /**
     * Returns a platform configuration for VT which may override the user setting.
     *
     * Note: VT presumes that VoLTE is enabled (these are configuration settings
     * which must be done correctly).
     */
    @Override
    public boolean isVtEnabledByPlatformForSlot() {
        if (SystemProperties.getInt(PROPERTY_DBG_VT_AVAIL_OVERRIDE,
                PROPERTY_DBG_VT_AVAIL_OVERRIDE_DEFAULT) == 1) {
            return true;
        }

        // Dynamic IMS Switch
        boolean isResourceSupport = true;
        boolean isCarrierConfigSupport = true;
        boolean isGbaValidSupport = true;
        boolean isFeatureEnableByPlatformExt = true;

        isResourceSupport = isImsResourceSupport(
                ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE);
        isCarrierConfigSupport = getBooleanCarrierConfigForSlot(
                CarrierConfigManager.KEY_CARRIER_VT_AVAILABLE_BOOL);
        isGbaValidSupport = isGbaValidForSlot();
        isFeatureEnableByPlatformExt = isFeatureEnabledByPlatformExt
                (ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE);

        log("Vt, isResourceSupport:" + isResourceSupport + ", isCarrierConfigSupport:" +
                isCarrierConfigSupport + ", isGbaValidSupport:" + isGbaValidSupport +
                ", isFeatureEnableByPlatformExt:" + isFeatureEnableByPlatformExt);

        return (SystemProperties.getInt("persist.mtk_vilte_support", 0) == 1) &&
                (SystemProperties.getInt(LTE_SUPPORT, 0) == 1) &&
                isResourceSupport &&
                isCarrierConfigSupport &&
                isGbaValidSupport &&
                isFeatureEnableByPlatformExt;
    }

    /**
     * MIMS
     */
    public static boolean isVtEnabledByUser(Context context, int phoneId) {

        int enabled = ImsConfig.FeatureValueConstants.OFF;

        if (isSupportMims() == false) {
            enabled = android.provider.Settings.Global.getInt(context.getContentResolver(),
                    android.provider.Settings.Global.VT_IMS_ENABLED,
                    ImsConfig.FeatureValueConstants.ON);
        } else {
            ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if(imsManager != null) {
                return imsManager.isVtEnabledByUserForSlot();
            } else {
                loge("isVtEnabledByUser");
                loge("getInstance null for phoneId=" + phoneId);
            }
        }
        return (enabled == 1) ? true : false;
    }

    /**
     * Returns the user configuration of VT setting per slot.
     */
    @Override
    public boolean isVtEnabledByUserForSlot() {
        int enabled = getSettingValueByKeyForSlot(VILTE_SETTING);
        return (enabled == 1);
    }

    /**
     * MIMS
     */
    public static void setVtSetting(Context context, boolean enabled, int phoneId) {
        int value = enabled ? 1 : 0;

        if (isSupportMims() == false) {
            phoneId = getMainPhoneIdForSingleIms(context);
            android.provider.Settings.Global.putInt(context.getContentResolver(),
                    android.provider.Settings.Global.VT_IMS_ENABLED, value);

            ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if (imsManager != null) {
                try {
                    ImsConfig config = imsManager.getConfigInterface();
                    config.setFeatureValue(ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE,
                            TelephonyManager.NETWORK_TYPE_LTE,
                            enabled ? ImsConfig.FeatureValueConstants.ON
                            : ImsConfig.FeatureValueConstants.OFF,
                            imsManager.mImsConfigListener);

                    if (enabled) {
                        log("setVtSetting() : turnOnIms");
                        imsManager.turnOnIms();
                    } else if (ImsManager.isTurnOffImsAllowedByPlatform(context)
                            && (!imsManager.isVolteEnabledByPlatformForSlot()
                                || !isEnhanced4gLteModeSettingEnabledByUser(context, phoneId))) {
                        log("setVtSetting() : imsServiceAllowTurnOff -> turnOffIms");
                        imsManager.turnOffIms();
                    }
                } catch (ImsException e) {
                    loge("setVtSetting(): ", e);
                }
            }
        } else {
            sImsManagerExt = getImsManagerPluginInstance(context);
            if (sImsManagerExt != null) {
                phoneId = sImsManagerExt.getImsPhoneId(context, phoneId);
            }
            ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if(imsManager != null) {
                imsManager.setVtSettingForSlot(enabled);
            } else {
                loge("setVtSettingForSlot");
                loge("getInstance null for phoneId=" + phoneId);
            }
        }
    }

    /**
     * Change persistent VT enabled setting for slot.
     */
    @Override
    public void setVtSettingForSlot(boolean enabled) {
        int value = enabled ? 1 : 0;
        setSettingValueByKeyForSlot(VILTE_SETTING, value);

        try {
            ImsConfig config = getConfigInterface();
            config.setFeatureValue(ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE,
                    TelephonyManager.NETWORK_TYPE_LTE,
                    enabled ? ImsConfig.FeatureValueConstants.ON
                            : ImsConfig.FeatureValueConstants.OFF,
                    mImsConfigListener);

            if (enabled) {
                log("setVtSettingForSlot() : turnOnIms");
                turnOnIms();
            } else if (isTurnOffImsAllowedByPlatformForSlot()
                    && (!isVolteEnabledByPlatformForSlot()
                    || !isEnhanced4gLteModeSettingEnabledByUserForSlot())) {
                log("setVtSettingForSlot() : imsServiceAllowTurnOff -> turnOffIms");
                turnOffIms();
            }
        } catch (ImsException e) {
            loge("setVtSettingForSlot(): ", e);
        }
    }


    /**
     * MIMS
     */
    public static boolean isWfcEnabledByUser(Context context, int phoneId) {

        int enabled = ImsConfig.FeatureValueConstants.OFF;

        int isDefaultWFCIMSEnabledByCarrier =
            ImsManager.getBooleanCarrierConfig(context,
                    CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_ENABLED_BOOL) ?
            ImsConfig.FeatureValueConstants.ON : ImsConfig.FeatureValueConstants.OFF;

        if (isSupportMims() == false) {
            enabled = android.provider.Settings.Global.getInt(context.getContentResolver(),
                    android.provider.Settings.Global.WFC_IMS_ENABLED,
                    isDefaultWFCIMSEnabledByCarrier);
        } else {
            ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if(imsManager != null) {
                return imsManager.isWfcEnabledByUserForSlot();
            } else {
                loge("isWfcEnabledByUserForSlot");
                loge("getInstance null for phoneId=" + phoneId);
            }
        }

        if (DBG) {
            log("isWfcEnabledByUser(), phoneId=" + phoneId +
                    " isDefaultWFCIMSEnabledByCarrier=" + isDefaultWFCIMSEnabledByCarrier +
                    " enable=" + enabled);
        }
        return (enabled == 1) ? true : false;
    }

    /**
     *      * Returns the user configuration of WFC setting for slot.
     */
    @Override
    public boolean isWfcEnabledByUserForSlot() {
        int enabled = getSettingValueByKeyForSlot(WFC_SETTING);
        if (DBG) {
            log("isWfcEnabledByUserForSlot(), mPhoneId=" + mPhoneId + " enable=" + enabled);
        }
        return (enabled == 1);
    }

    /**
     * MIMS
     */
    public static void setWfcSetting(Context context, boolean enabled, int phoneId) {
        int value = enabled ? 1 : 0;

        if (isSupportMims() == false) {
            phoneId = getMainPhoneIdForSingleIms(context);
            android.provider.Settings.Global.putInt(context.getContentResolver(),
                    android.provider.Settings.Global.WFC_IMS_ENABLED, value);

            ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if (imsManager != null) {
                try {
                    ImsConfig config = imsManager.getConfigInterface();
                    config.setFeatureValue(ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI,
                            TelephonyManager.NETWORK_TYPE_IWLAN,
                            enabled ? ImsConfig.FeatureValueConstants.ON
                            : ImsConfig.FeatureValueConstants.OFF,
                            imsManager.mImsConfigListener);

                    if (enabled) {
                        log("setWfcSetting(): turnOnIms, phoneId:" + phoneId);
                        imsManager.turnOnIms();
                    } else if (ImsManager.isTurnOffImsAllowedByPlatform(context)
                            && (!imsManager.isVolteEnabledByPlatformForSlot()
                                || !isEnhanced4gLteModeSettingEnabledByUser(context, phoneId))) {
                        log("setWfcSetting(): turnOffIms, phoneId:" + phoneId);
                        imsManager.turnOffIms();
                    }

                    // Force IMS to register over LTE when turning off WFC
                    setWfcModeInternal(context, enabled
                            ? getWfcMode(context, phoneId)
                            : ImsConfig.WfcModeFeatureValueConstants.CELLULAR_PREFERRED,
                            phoneId);
                } catch (ImsException e) {
                    loge("setWfcSetting(): ", e);
                }
            }
        } else {

            sImsManagerExt = getImsManagerPluginInstance(context);
            if (sImsManagerExt != null) {
                phoneId = sImsManagerExt.getImsPhoneId(context, phoneId);
            }
            ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if(imsManager != null) {
                imsManager.setWfcSettingForSlot(enabled);
            } else {
                loge("setWfcSettingForSlot");
                loge("getInstance null for phoneId=" + phoneId);
            }
        }
    }

    /**
     * Change persistent WFC enabled setting for slot.
     */
    @Override
    public void setWfcSettingForSlot(boolean enabled) {
        int value = enabled ? 1 : 0;
        setSettingValueByKeyForSlot(WFC_SETTING, value);

        try {
            ImsConfig config = getConfigInterface();
            config.setFeatureValue(ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI,
                    TelephonyManager.NETWORK_TYPE_IWLAN,
                    enabled ? ImsConfig.FeatureValueConstants.ON
                            : ImsConfig.FeatureValueConstants.OFF,
                    mImsConfigListener);

            if (enabled) {
                log("setWfcSettingForSlot() : turnOnIms");
                turnOnIms();
            } else if (isTurnOffImsAllowedByPlatformForSlot()
                    && (!isVolteEnabledByPlatformForSlot()
                    || !isEnhanced4gLteModeSettingEnabledByUserForSlot())) {
                log("setWfcSettingForSlot() : imsServiceAllowTurnOff -> turnOffIms");
                turnOffIms();
            }

            // Force IMS to register over LTE when turning off WFC
            setWfcModeInternalForSlot(enabled
                    ? getWfcModeForSlot()
                    : ImsConfig.WfcModeFeatureValueConstants.CELLULAR_PREFERRED);
        } catch (ImsException e) {
            loge("setWfcSettingForSlot(): ", e);
        }
    }

    /**
     * MIMS
     */
    public static int getWfcMode(Context context, int phoneId) {
        int setting = ImsConfig.WfcModeFeatureValueConstants.CELLULAR_PREFERRED;

        int defaultWFCIMSModeByCarrier =
                ImsManager.getIntCarrierConfig(context,
                        CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_MODE_INT);

        if (isSupportMims() == false) {
            setting = android.provider.Settings.Global.getInt(context.getContentResolver(),
                    android.provider.Settings.Global.WFC_IMS_MODE,
                    defaultWFCIMSModeByCarrier);
        } else {
            sImsManagerExt = getImsManagerPluginInstance(context);
            if (sImsManagerExt != null) {
                phoneId = sImsManagerExt.getImsPhoneId(context, phoneId);
            }
            ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if(imsManager != null) {
                return imsManager.getWfcModeForSlot();
            } else {
                loge("getWfcMode");
                loge("getInstance null for phoneId=" + phoneId);
            }
        }

        if (DBG) {
            log("getWfcMode(), phoneId:" + phoneId +
                    ", defaultWFCIMSModeByCarrier:" + defaultWFCIMSModeByCarrier +
                    ", result:" + setting);
        }
        return setting;
    }

    /**
     * Returns the user configuration of WFC preference setting
     */
    @Override
    public int getWfcModeForSlot() {
        int setting = getSettingValueByKeyForSlot(WFC_MODE_SETTING);
        if (DBG) log("getWfcModeForSlot - setting=" + setting);
        return setting;
    }

    /**
     * MIMS
     */
    public static void setWfcMode(Context context, int wfcMode, int phoneId) {
        if (DBG) log("setWfcMode(), setting=" + wfcMode + ", phoneId:" + phoneId);

        if (isSupportMims() == false) {
            phoneId = getMainPhoneIdForSingleIms(context);
            android.provider.Settings.Global.putInt(context.getContentResolver(),
                    android.provider.Settings.Global.WFC_IMS_MODE, wfcMode);

            setWfcModeInternal(context, wfcMode, phoneId);
        } else {
            sImsManagerExt = getImsManagerPluginInstance(context);
            if (sImsManagerExt != null) {
                phoneId = sImsManagerExt.getImsPhoneId(context, phoneId);
            }
            ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if(imsManager != null) {
                imsManager.setWfcModeForSlot(wfcMode);
            } else {
                loge("setWfcMode");
                loge("getInstance null for phoneId=" + phoneId);
            }
        }
    }

    /**
     * Change persistent WFC preference setting for slot.
     */
    @Override
    public void setWfcModeForSlot(int wfcMode) {
        if (DBG) log("setWfcModeForSlot - setting=" + wfcMode);
        setSettingValueByKeyForSlot(WFC_MODE_SETTING, wfcMode);

        setWfcModeInternalForSlot(wfcMode);
    }

    /**
     * MIMS
     */
    public static int getWfcMode(Context context, boolean roaming, int phoneId) {
        int setting = 0;
        if (!roaming) {
            return getWfcMode(context, phoneId);
        } else {
            int defaultWFCIMSRoamingModeByCarrier =
                ImsManager.getIntCarrierConfig(context,
                        CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_ROAMING_MODE_INT);

            if (isSupportMims() == false) {
                setting = android.provider.Settings.Global.getInt(context.getContentResolver(),
                        android.provider.Settings.Global.WFC_IMS_ROAMING_MODE,
                        defaultWFCIMSRoamingModeByCarrier);
            } else {
                sImsManagerExt = getImsManagerPluginInstance(context);
                if (sImsManagerExt != null) {
                    phoneId = sImsManagerExt.getImsPhoneId(context, phoneId);
                }
                ImsManager imsManager = ImsManager.getInstance(context, phoneId);
                if(imsManager != null) {
                    return imsManager.getWfcModeForSlot(roaming);
                } else {
                    loge("getWfcMode(roaming)");
                    loge("getInstance null for phoneId=" + phoneId);
                }
            }

            if (DBG) {
                log("getWfcMode(roaming), phoneId:" + phoneId +
                        ", defaultWFCIMSRoamingModeByCarrier:" + defaultWFCIMSRoamingModeByCarrier +
                        ", result:" + setting);
            }
        }
        return setting;
    }

    /**
     * Returns the user configuration of WFC preference setting for slot
     *
     * @param roaming {@code false} for home network setting, {@code true} for roaming  setting
     */
    @Override
    public int getWfcModeForSlot(boolean roaming) {
        int setting = 0;
        if (!roaming) {
            setting = getWfcModeForSlot();
            if (DBG) log("getWfcModeForSlot - setting=" + setting);
        } else {
            setting = getSettingValueByKeyForSlot(WFC_ROAMING_MODE_SETTING);
            if (DBG) log("getWfcModeForSlot (roaming) - setting=" + setting);
        }
        return setting;
    }

    /**
     * MIMS
     */
    public static void setWfcMode(Context context, int wfcMode, boolean roaming, int phoneId) {
        if (DBG) log("setWfcMode(), wfcMode: " + wfcMode +
                     ", roaming:" + roaming + ", phoneId:" + phoneId);

        if (!roaming) {
            setWfcMode(context, wfcMode, phoneId);
        } else {
            if (isSupportMims() == false) {
                phoneId = getMainPhoneIdForSingleIms(context);
                android.provider.Settings.Global.putInt(context.getContentResolver(),
                        android.provider.Settings.Global.WFC_IMS_ROAMING_MODE, wfcMode);

                /* Currently UI only displays Calling preference and doesn't display roaming preference
                   UI always call setWfcMode(roaming == false). We chooses to make Calling preference work
                   both in home and roaming environment, so always set Calling preference to Modem

               TelephonyManager tm = (TelephonyManager)
                   context.getSystemService(Context.TELEPHONY_SERVICE);
               if (roaming == tm.isNetworkRoaming()) {
                   setWfcModeInternal(context, wfcMode, phoneId);
               }
               */
                setWfcModeInternal(context, wfcMode, phoneId);
            } else {
                ImsManager imsManager = ImsManager.getInstance(context, phoneId);
                if(imsManager != null) {
                    imsManager.setWfcModeForSlot(wfcMode, roaming);
                } else {
                    loge("setWfcMode(roaming)");
                    loge("getInstance null for phoneId=" + phoneId);
                }
            }
        }
    }

    /**
     * Change persistent WFC preference setting
     *
     * @param roaming {@code false} for home network setting, {@code true} for roaming setting
     */
    @Override
    public void setWfcModeForSlot(int wfcMode, boolean roaming) {
        if (!roaming) {
            if (DBG) log("setWfcModeForSlot - setting=" + wfcMode);
            setWfcModeForSlot(wfcMode);
        } else {
            if (DBG) log("setWfcModeForSlot (roaming) - setting=" + wfcMode);
            setSettingValueByKeyForSlot(WFC_ROAMING_MODE_SETTING, wfcMode);
        }

        /* Currently UI only displays Calling preference and doesn't display roaming preference
           UI always call setWfcMode(roaming == false). We chooses to make Calling preference work
           both in home and roaming environment, so always set Calling preference to Modem

        int[] subIds = SubscriptionManager.getSubId(mPhoneId);
        int subId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
        if (subIds != null && subIds.length >= 1) {
            subId = subIds[0];
        }
        TelephonyManager tm = (TelephonyManager)
                mContext.getSystemService(Context.TELEPHONY_SERVICE);
        if (roaming == tm.isNetworkRoaming(subId)) {
            setWfcModeInternalForSlot(wfcMode);
        }
        */
        setWfcModeInternalForSlot(wfcMode);
    }

    /**
     * no static setWfcModeInternal
     */
    @Override
    protected void setWfcModeInternal(int wfcMode) {
        setWfcModeInternal(mContext, wfcMode, mPhoneId);
    }

    /**
     * MIMS
     */
    private static void setWfcModeInternal(Context context, int wfcMode, int phoneId) {
        if (isSupportMims() == false) {
            phoneId = getMainPhoneIdForSingleIms(context);
        }

        ImsManager imsManager = ImsManager.getInstance(context, phoneId);
        if (imsManager != null) {
            imsManager.setWfcModeInternalForSlot(wfcMode);
        }
    }

    @Override
    public void setWfcModeInternalForSlot(int wfcMode) {
        final int value = wfcMode;

        setWfcModeConfigForSlot(wfcMode);

        Thread thread = new Thread(() -> {
                try {
                    getConfigInterface().setProvisionedValue(
                            ImsConfig.ConfigConstants.VOICE_OVER_WIFI_MODE,
                            value);
                } catch (ImsException e) {
                    // do nothing
                }
        });
        thread.start();
    }

    protected void setWfcModeConfigForSlot(int wfcMode) {
        log("setWfcModeConfig wfcMode:" + wfcMode + ", phoneId:" + mPhoneId);

        // Sync wifi preferred mode to modem instead of via provisioning.
        try {
            getConfigInterfaceEx(mPhoneId, mContext).setWfcMode(wfcMode);
        } catch (ImsException e) {
            // do nothing
        }
    }

    /**
     * MIMS
     */
    public static boolean isWfcRoamingEnabledByUser(Context context, int phoneId) {

        int enabled = ImsConfig.FeatureValueConstants.OFF;

        int isRoamingEnableByCarrier =
                ImsManager.getBooleanCarrierConfig(context,
                        CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_ROAMING_ENABLED_BOOL) ?
                        ImsConfig.FeatureValueConstants.ON :
                        ImsConfig.FeatureValueConstants.OFF;

        if (isSupportMims() == false) {
            enabled = android.provider.Settings.Global.getInt(context.getContentResolver(),
                   android.provider.Settings.Global.WFC_IMS_ROAMING_ENABLED,
                   isRoamingEnableByCarrier);
        } else {
            ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if(imsManager != null) {
                return imsManager.isWfcRoamingEnabledByUserForSlot();
            } else {
                loge("isWfcRoamingEnabledByUser");
                loge("getInstance null for phoneId=" + phoneId);
            }
        }

        if (DBG) {
            log("isWfcRoamingEnabledByUser(), phoneId=" + phoneId +
                    " isRoamingEnableByCarrier=" + isRoamingEnableByCarrier +
                    " enable=" + enabled);
        }
        return (enabled == 1) ? true : false;
    }

    /**
     * Returns the user configuration of WFC roaming setting for slot
     */
    @Override
    public boolean isWfcRoamingEnabledByUserForSlot() {
        int enabled = getSettingValueByKeyForSlot(WFC_ROAMING_SETTING);
        if (DBG) {
            log("isWfcRoamingEnabledByUserForSlot(), phoneId=" + mPhoneId +
                    " enabled=" + enabled);
        }
        return (enabled == 1);
    }

    /**
     * MIMS
     */
    public static void setWfcRoamingSetting(Context context, boolean enabled, int phoneId) {
        if (DBG) log("setWfcRoamingSetting(), enabled: " + enabled + ", phoneId:" + phoneId);

        if (isSupportMims() == false) {
            phoneId = getMainPhoneIdForSingleIms(context);
            android.provider.Settings.Global.putInt(context.getContentResolver(),
                    android.provider.Settings.Global.WFC_IMS_ROAMING_ENABLED,
                    enabled ? ImsConfig.FeatureValueConstants.ON
                            : ImsConfig.FeatureValueConstants.OFF);

            final ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if (imsManager != null) {
                imsManager.setWfcRoamingSettingInternal(enabled);
            }
        } else {
            ImsManager imsManager = ImsManager.getInstance(context, phoneId);
            if(imsManager != null) {
                imsManager.setWfcRoamingSettingForSlot(enabled);
            } else {
                loge("setWfcRoamingSetting");
                loge("getInstance null for phoneId=" + phoneId);
            }
        }

    }

    /**
     * Change persistent WFC roaming enabled setting
     */
    @Override
    public void setWfcRoamingSettingForSlot(boolean enabled) {
        int value = enabled ? 1 : 0;
        setSettingValueByKeyForSlot(WFC_ROAMING_SETTING, value);
        setWfcRoamingSettingInternal(enabled);
    }

    /**
     * Returns a platform configuration for WFC which may override the user
     * setting per slot. Note: WFC presumes that VoLTE is enabled (these are
     * configuration settings which must be done correctly).
     */
    @Override
    public boolean isWfcEnabledByPlatformForSlot() {
        if (SystemProperties.getInt(PROPERTY_DBG_WFC_AVAIL_OVERRIDE,
                PROPERTY_DBG_WFC_AVAIL_OVERRIDE_DEFAULT) == 1) {
            return true;
        }

        // Dynamic IMS Switch
        boolean isResourceSupport = true;
        boolean isCarrierConfigSupport = true;
        boolean isGbaValidSupport = true;
        boolean isFeatureEnableByPlatformExt = true;

        isResourceSupport = isImsResourceSupport(
                ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI);
        isCarrierConfigSupport = getBooleanCarrierConfigForSlot(
                CarrierConfigManager.KEY_CARRIER_WFC_IMS_AVAILABLE_BOOL);
        isGbaValidSupport = isGbaValidForSlot();
        isFeatureEnableByPlatformExt = isFeatureEnabledByPlatformExt
                (ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI);

        log("Wfc, isResourceSupport:" + isResourceSupport + ", isCarrierConfigSupport:" +
                isCarrierConfigSupport + ", isGbaValidSupport:" + isGbaValidSupport +
                ", isFeatureEnableByPlatformExt:" + isFeatureEnableByPlatformExt);

        return (SystemProperties.getInt("persist.mtk_wfc_support", 0) == 1) &&
               (SystemProperties.getInt(LTE_SUPPORT, 0) == 1) &&
               isResourceSupport &&
               isCarrierConfigSupport &&
               isGbaValidSupport &&
               isFeatureEnableByPlatformExt;
    }

    /**
     * If carrier requires that IMS is only available if GBA capable SIM is used,
     * then this function checks GBA bit in EF IST.
     *
     * Format of EF IST is defined in 3GPP TS 31.103 (Section 4.2.7).
     */
    @Override
    protected boolean isGbaValidForSlot() {
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(mPhoneId);
        if (getBooleanCarrierConfigForSlot(
                    CarrierConfigManager.KEY_CARRIER_IMS_GBA_REQUIRED_BOOL)) {
            final MtkTelephonyManagerEx tm = MtkTelephonyManagerEx.getDefault();
            String efIst = tm.getIsimIst(subId);
            if (efIst == null) {
                loge("isGbaValidForSlot - ISF is NULL");
                return true;
            }
            boolean result = efIst != null && efIst.length() > 1 &&
                (0x02 & (byte)efIst.charAt(1)) != 0;
            if (DBG) log("isGbaValidForSlot - GBA capable=" + result + ", ISF=" + efIst
                    + ", phoneId=" + mPhoneId);
            return result;
        }
        return true;
    }

    private String getVolteSettingKeyForSlot() {
        if (isSupportMims() == false) {
            return android.provider.Settings.Global.ENHANCED_4G_MODE_ENABLED;
        }
        if (mPhoneId == SIM_ID_2) {
            return ENHANCED_4G_MODE_ENABLED_SIM2;
        } else if (mPhoneId == SIM_ID_3) {
            return ENHANCED_4G_MODE_ENABLED_SIM3;
        } else if (mPhoneId == SIM_ID_4) {
            return ENHANCED_4G_MODE_ENABLED_SIM4;
        }
        return android.provider.Settings.Global.ENHANCED_4G_MODE_ENABLED;
    }

    private String getTtyModeSettingKeyForSlot() {
        if (isSupportMims() == false) {
            return Settings.Secure.PREFERRED_TTY_MODE;
        }
        if (mPhoneId == SIM_ID_2) {
            return PREFERRED_TTY_MODE_SIM2;
        } else if (mPhoneId == SIM_ID_3) {
            return PREFERRED_TTY_MODE_SIM3;
        } else if (mPhoneId == SIM_ID_4) {
            return PREFERRED_TTY_MODE_SIM4;
        }
        return Settings.Secure.PREFERRED_TTY_MODE;
    }

    private String getVtSettingKeyForSlot() {
        if (isSupportMims() == false) {
            return android.provider.Settings.Global.VT_IMS_ENABLED;
        }
        if (mPhoneId == SIM_ID_2) {
            return VT_IMS_ENABLED_SIM2;
        } else if (mPhoneId == SIM_ID_3) {
            return VT_IMS_ENABLED_SIM3;
        } else if (mPhoneId == SIM_ID_4) {
            return VT_IMS_ENABLED_SIM4;
        }
        return android.provider.Settings.Global.VT_IMS_ENABLED;
    }

    private String getWfcSettingKeyForSlot() {
        if (isSupportMims() == false) {
            return android.provider.Settings.Global.WFC_IMS_ENABLED;
        }
        if (mPhoneId == SIM_ID_2) {
            return WFC_IMS_ENABLED_SIM2;
        } else if (mPhoneId == SIM_ID_3) {
            return WFC_IMS_ENABLED_SIM3;
        } else if (mPhoneId == SIM_ID_4) {
            return WFC_IMS_ENABLED_SIM4;
        }
        return android.provider.Settings.Global.WFC_IMS_ENABLED;
    }

    private String getWfcModeSettingKeyForSlot() {
        if (isSupportMims() == false) {
            return android.provider.Settings.Global.WFC_IMS_MODE;
        }
        if (mPhoneId == SIM_ID_2) {
            return WFC_IMS_MODE_SIM2;
        } else if (mPhoneId == SIM_ID_3) {
            return WFC_IMS_MODE_SIM3;
        } else if (mPhoneId == SIM_ID_4) {
            return WFC_IMS_MODE_SIM4;
        }
        return android.provider.Settings.Global.WFC_IMS_MODE;
    }

    private String getWfcRoamingSettingKeyForSlot() {
        if (isSupportMims() == false) {
            return android.provider.Settings.Global.WFC_IMS_ROAMING_ENABLED;
        }
        if (mPhoneId == SIM_ID_2) {
            return WFC_IMS_ROAMING_ENABLED_SIM2;
        } else if (mPhoneId == SIM_ID_3) {
            return WFC_IMS_ROAMING_ENABLED_SIM3;
        } else if (mPhoneId == SIM_ID_4) {
            return WFC_IMS_ROAMING_ENABLED_SIM4;
        }
        return android.provider.Settings.Global.WFC_IMS_ROAMING_ENABLED;
    }

    private String getWfcRoamingModeSettingKeyForSlot() {
        if (isSupportMims() == false) {
            return android.provider.Settings.Global.WFC_IMS_ROAMING_MODE;
        }
        if (mPhoneId == SIM_ID_2) {
            return WFC_IMS_ROAMING_MODE_SIM2;
        } else if (mPhoneId == SIM_ID_3) {
            return WFC_IMS_ROAMING_MODE_SIM3;
        } else if (mPhoneId == SIM_ID_4) {
            return WFC_IMS_ROAMING_MODE_SIM4;
        }
        return android.provider.Settings.Global.WFC_IMS_ROAMING_MODE;
    }

    private int getSettingValueByKeyForSlot(String key) {

        if(key.equals(VOLTE_SETTING)) {
            return android.provider.Settings.Global.getInt(
                    mContext.getContentResolver(),
                    getVolteSettingKeyForSlot(),
                    ImsConfig.FeatureValueConstants.ON);
        } else if (key.equals(TTY_MODE)) {
            return Settings.Secure.getInt(mContext.getContentResolver(),
                    getTtyModeSettingKeyForSlot(), TelecomManager.TTY_MODE_OFF);
        } else if (key.equals(VILTE_SETTING)) {
            return android.provider.Settings.Global.getInt(mContext.getContentResolver(),
                    getVtSettingKeyForSlot(),
                    ImsConfig.FeatureValueConstants.ON);
        } else if (key.equals(WFC_SETTING)) {
            int isDefaultWFCIMSEnabledByCarrier =
                getBooleanCarrierConfigForSlot(
                        CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_ENABLED_BOOL) ?
                ImsConfig.FeatureValueConstants.ON : ImsConfig.FeatureValueConstants.OFF;
            return android.provider.Settings.Global.getInt(mContext.getContentResolver(),
                    getWfcSettingKeyForSlot(),
                    isDefaultWFCIMSEnabledByCarrier);
        }else if (key.equals(WFC_MODE_SETTING)) {
            int defaultWFCIMSModeByCarrier =
                getIntCarrierConfigForSlot(
                        CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_MODE_INT);
            return android.provider.Settings.Global.getInt(mContext.getContentResolver(),
                    getWfcModeSettingKeyForSlot(),
                    defaultWFCIMSModeByCarrier);
        }else if (key.equals(WFC_ROAMING_MODE_SETTING)) {
            int defaultWFCIMSRoamingModeByCarrier =
                getIntCarrierConfigForSlot(
                        CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_ROAMING_MODE_INT);
            return android.provider.Settings.Global.getInt(mContext.getContentResolver(),
                    getWfcRoamingModeSettingKeyForSlot(),
                    defaultWFCIMSRoamingModeByCarrier);
        } else if (key.equals(WFC_ROAMING_SETTING)) {
            int isRoamingEnableByCarrier =
                getBooleanCarrierConfigForSlot(
                        CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_ROAMING_ENABLED_BOOL) ?
                ImsConfig.FeatureValueConstants.ON :
                ImsConfig.FeatureValueConstants.OFF;

            return android.provider.Settings.Global.getInt(mContext.getContentResolver(),
                    getWfcRoamingSettingKeyForSlot(),
                    isRoamingEnableByCarrier);
        }
        return -1;
    }

    private void setSettingValueByKeyForSlot(String key, int value) {

        if(key.equals(VOLTE_SETTING)) {
            android.provider.Settings.Global.putInt(
                    mContext.getContentResolver(),
                    getVolteSettingKeyForSlot(), value);
        } else if (key.equals(VILTE_SETTING)) {
            android.provider.Settings.Global.putInt(mContext.getContentResolver(),
                    getVtSettingKeyForSlot(), value);
        } else if (key.equals(WFC_SETTING)) {
            android.provider.Settings.Global.putInt(mContext.getContentResolver(),
                    getWfcSettingKeyForSlot(), value);
        } else if (key.equals(WFC_MODE_SETTING)) {
            android.provider.Settings.Global.putInt(mContext.getContentResolver(),
                    getWfcModeSettingKeyForSlot(), value);
        } else if (key.equals(WFC_ROAMING_SETTING)) {
            android.provider.Settings.Global.putInt(mContext.getContentResolver(),
                    getWfcRoamingSettingKeyForSlot(),
                    value);
        } else if (key.equals(WFC_ROAMING_MODE_SETTING)) {
            android.provider.Settings.Global.putInt(mContext.getContentResolver(),
                    getWfcRoamingModeSettingKeyForSlot(), value);
        }
    }

    public MtkImsManager(Context context, int phoneId) {
        super(context, phoneId);
        createMtkImsService(true);

        final IntentFilter filter = new IntentFilter();
        filter.addAction(CarrierExpressManager.ACTION_OPERATOR_CONFIG_CHANGED);
        context.registerReceiver(mBroadcastReceiver, filter);
    }

    /*
     * Returns a flag indicating whether the IMS service is available.
     */
    @Override
    public boolean isServiceAvailable() {
        boolean available = true;
        if (super.isServiceAvailable() == false) {
            logw("ImsService binder is not available and rebind again");
            createImsService();
        }
        if (mMtkImsService == null) {
            createMtkImsService(true);
        }
        // checking MtkImsService should also cover ImsService
        available = (mMtkImsService != null);
        log("isServiceAvailable=" + available);
        return available;
    }

    /**
     * Opens the IMS service for making calls and/or receiving generic IMS calls.
     * The caller may make subsquent calls through {@link #makeCall}.
     * The IMS service will register the device to the operator's network with the credentials
     * (from ISIM) periodically in order to receive calls from the operator's network.
     * When the IMS service receives a new call, it will send out an intent with
     * the provided action string.
     * The intent contains a call ID extra {@link getCallId} and it can be used to take a call.
     *
     * @param serviceClass a service class specified in {@link ImsServiceClass}
     *      For VoLTE service, it MUST be a {@link ImsServiceClass#MMTEL}.
     * @param incomingCallPendingIntent When an incoming call is received,
     *        the IMS service will call {@link PendingIntent#send(Context, int, Intent)} to
     *        send back the intent to the caller with {@link #INCOMING_CALL_RESULT_CODE}
     *        as the result code and the intent to fill in the call ID; It cannot be null
     * @param listener To listen to IMS registration events; It cannot be null
     * @return identifier (greater than 0) for the specified service
     * @throws NullPointerException if {@code incomingCallPendingIntent}
     *      or {@code listener} is null
     * @throws ImsException if calling the IMS service results in an error
     * @see #getCallId
     * @see #getServiceId
     */
    @Override
    public int open(int serviceClass, PendingIntent incomingCallPendingIntent,
            ImsConnectionStateListener listener) throws ImsException {
        checkAndThrowExceptionIfServiceUnavailable();

        /// M: add for debug @{
        if (DBG) log("open: phoneId=" + mPhoneId);
        /// @}

        if (incomingCallPendingIntent == null) {
            throw new NullPointerException("incomingCallPendingIntent can't be null");
        }

        if (listener == null) {
            throw new NullPointerException("listener can't be null");
        }

        int result = 0;

        try {
            result = mImsServiceProxy.startSession(incomingCallPendingIntent,
                    mRegistrationListenerProxy);
            synchronized (mHasRegisteredLock) {
                mHasRegisteredForProxy = true;
                addRegistrationListener(listener);
            }
            if (DBG) log("open: result=" + result);
        } catch (RemoteException e) {
            throw new ImsException("open()", e,
                    ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN);
        }

        if (result <= 0) {
            // If the return value is a minus value,
            // it means that an error occurred in the service.
            // So, it needs to convert to the reason code specified in ImsReasonInfo.
            throw new ImsException("open()", (result * (-1)));
        }

        return result;
    }

    /**
     * Closes the specified service ({@link ImsServiceClass}) not to make/receive calls.
     * All the resources that were allocated to the service are also released.
     *
     * @param serviceId a service id to be closed which is obtained from {@link ImsManager#open}
     * @throws ImsException if calling the IMS service results in an error
     */
    @Override
    public void close(int sessionId) throws ImsException {
        checkAndThrowExceptionIfServiceUnavailable();

        /// M: add for debug @{
        if (DBG) log("close");
        /// @}

        try {
            mImsServiceProxy.endSession(sessionId);
        } catch (RemoteException e) {
            throw new ImsException("close()", e,
                    ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN);
        } finally {
            mUt = null;
            mConfig = null;
            mConfigEx = null;
            mEcbm = null;
            mMtkUt = null;
            mMultiEndpoint = null;
        }
    }

    /**
     * Gets the configuration interface to provision / withdraw the supplementary service settings.
     *
     * @param serviceId a service id which is obtained from {@link ImsManager#open}
     * @return the Ut interface instance
     * @throws ImsException if getting the Ut interface results in an error
     */
    @Override
    public ImsUtInterface getSupplementaryServiceConfiguration()
            throws ImsException {
        // FIXME: manage the multiple Ut interfaces based on the service id
        if (mUt == null || !mImsServiceProxy.isBinderAlive()) {
            checkAndThrowExceptionIfServiceUnavailable();

            try {
                IImsUt iUt = mImsServiceProxy.getUtInterface();

                if (iUt == null) {
                    throw new ImsException("getSupplementaryServiceConfiguration()",
                            ImsReasonInfo.CODE_UT_NOT_SUPPORTED);
                }

                mUt = new ImsUt(iUt);
            } catch (RemoteException e) {
                throw new ImsException("getSupplementaryServiceConfiguration()", e,
                        ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN);
            }
        }
        return mUt;
    }

    /**
     * Gets the configuration interface to provision / withdraw the supplementary service settings.
     *
     * @param serviceId a service id which is obtained from {@link ImsManager#open}
     * @return the Ut interface instance
     * @throws ImsException if getting the Ut interface results in an error
     */
    public ImsUtInterface getSupplementaryServiceConfiguration(int serviceId)
            throws ImsException {
        // FIXME: manage the multiple Ut interfaces based on the service id
        if (mMtkUt == null || !mImsServiceProxy.isBinderAlive()) {
            checkAndThrowExceptionIfServiceUnavailable();

            try {
                IImsUt iUt = mMtkImsService.getUtInterface(serviceId);
                IMtkImsUt iMtkUt = mMtkImsService.getMtkUtInterface(serviceId);

                if (iUt == null) {
                    throw new ImsException("getSupplementaryServiceConfiguration()",
                            ImsReasonInfo.CODE_UT_NOT_SUPPORTED);
                }

                mMtkUt = new MtkImsUt(iUt, iMtkUt);
            } catch (RemoteException e) {
                throw new ImsException("getSupplementaryServiceConfiguration()", e,
                        ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN);
            }
        }
        return mMtkUt;
    }

    /**
     * Creates a {@link ImsCall} to make a call.
     *
     * @param sessionId a session id which is obtained from {@link ImsManager#open}
     * @param profile a call profile to make the call
     *      (it contains service type, call type, media information, etc.)
     * @param participants participants to invite the conference call
     * @param listener listen to the call events from {@link ImsCall}
     * @return a {@link ImsCall} object
     * @throws ImsException if calling the IMS service results in an error
     */
    @Override
    public ImsCall makeCall(int sessionId, ImsCallProfile profile, String[] callees,
            ImsCall.Listener listener) throws ImsException {
        if (DBG) {
            log("makeCall :: sessionId=" + sessionId
                    + ", profile=" + profile + ", callees=" + Arrays.toString(callees));
        }

        checkAndThrowExceptionIfServiceUnavailable();

        ImsCall call = new MtkImsCall(mContext, profile);

        call.setListener(listener);
        ImsCallSession session = createCallSession(sessionId, profile);

        if ((callees != null) && (callees.length == 1) &&
                /// M:  For VoLTE enhanced conference call. @{
                !profile.getCallExtraBoolean(ImsCallProfile.EXTRA_CONFERENCE)) {
                /// @}
            call.start(session, callees[0]);
        } else {
            call.start(session, callees);
        }

        return call;
    }

    /**
     * Gets the config interface to get/set service/capability parameters.
     *
     * @return the ImsConfig instance.
     * @throws ImsException if getting the setting interface results in an error.
     */
    @Override
    public ImsConfig getConfigInterface() throws ImsException {

        /// M: Remove cache of ImsConfig in AOSP, always get ImsConfig with mainCapability Id
        // TODO: Follow AOSP, sync feature value with partial single instance in ImsConfig DB
//        if (mConfig == null || !mImsServiceProxy.isBinderAlive()) {
        /// @}
            checkAndThrowExceptionIfServiceUnavailable();

            try {
                IImsConfig config = mImsServiceProxy.getConfigInterface();
                if (config == null) {
                    throw new ImsException("getConfigInterface()",
                            ImsReasonInfo.CODE_LOCAL_SERVICE_UNAVAILABLE);
                }
                mConfig = new ImsConfig(config, mContext);
            } catch (RemoteException e) {
                throw new ImsException("getConfigInterface()", e,
                        ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN);
            }
//      }
        if (DBG) log("getConfigInterface(), mConfig= " + mConfig);
        return mConfig;
    }

    /**
     * Binds the IMS service only if the service is not created.
     */
    private void checkAndThrowExceptionIfServiceUnavailable()
            throws ImsException {
        if (mImsServiceProxy == null || !mImsServiceProxy.isBinderAlive()) {
            createImsService();

            if (mImsServiceProxy == null) {
                throw new ImsException("Service is unavailable",
                        ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN);
            }
        }

        if (mMtkImsService == null) {
            createMtkImsService(true);

            if (mMtkImsService == null) {
                throw new ImsException("MtkImsService is unavailable",
                        ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN);
            }
        }
    }

    private static String getMtkImsServiceName(int phoneId) {
        return MTK_IMS_SERVICE;
    }

    /**
     * Binds the IMS service to make/receive the call.
     */
    private void createMtkImsService(boolean checkService) {
        if (checkService) {
            IBinder binder = ServiceManager.checkService(getMtkImsServiceName(mPhoneId));

            if (binder == null) {
                /// M: add for debug @{
                if (DBG) log("createMtkImsService binder is null");
                /// @}
                return;
            }
        }

        IBinder b = ServiceManager.getService(getMtkImsServiceName(mPhoneId));

        if (b != null) {
            try {
                b.linkToDeath(mMtkDeathRecipient, 0);
            } catch (RemoteException e) {
            }
        }

        mMtkImsService = IMtkImsService.Stub.asInterface(b);
        /// M: add for debug @{
        if (DBG) log("mMtkImsService = " + mMtkImsService);
        /// @}
    }

    private static void log(String s) {
        Rlog.d(TAG, s);
    }

    private static void logw(String s) {
        Rlog.w(TAG, s);
    }

    private static void loge(String s) {
        Rlog.e(TAG, s);
    }

    private static void loge(String s, Throwable t) {
        Rlog.e(TAG, s, t);
    }

    /**
     * Death recipient class for monitoring IMS service.
     */
    private class MtkImsServiceDeathRecipient extends ImsServiceDeathRecipient
            implements IBinder.DeathRecipient {
        @Override
        public void binderDied() {
            mMtkImsService = null;
            mConfigEx = null;
        }
    }

    /**
     * Gets the call Num from the specified incoming call broadcast intent.
     *
     * @param incomingCallIntent the incoming call broadcast intent
     * @return the call Num or null if the intent does not contain it
     */
    private String getCallNum(Intent incomingCallIntent) {
        if (incomingCallIntent == null) {
            return null;
        }

        return incomingCallIntent.getStringExtra(EXTRA_DIAL_STRING);
    }

    /**
     * Gets the sequence number from the specified incoming call broadcast intent.
     *
     * @param incomingCallIntent the incoming call broadcast intent
     * @return the sequence number or null if the intent does not contain it
     * @hide
     */
    private int getSeqNum(Intent incomingCallIntent) {
        if (incomingCallIntent == null) {
            return (-1);
        }

        return incomingCallIntent.getIntExtra(EXTRA_SEQ_NUM, -1);
    }


    /**
     * To Allow or refuse incoming call indication to send to App.
     *
     * @param serviceId a service id which is obtained from {@link ImsManager#open}
     * @param incomingCallIndication the incoming call broadcast intent.
     * @param isAllow to indication to allow or refuse the incoming call indication.
     * @throws ImsException if set call indication results in an error.
     * @hide
     */
    public void setCallIndication(int serviceId, Intent incomingCallIndication,
            boolean isAllow) throws ImsException {
        if (DBG) {
            log("setCallIndication :: serviceId=" + serviceId
                    + ", incomingCallIndication=" + incomingCallIndication);
        }

        checkAndThrowExceptionIfServiceUnavailable();

        if (incomingCallIndication == null) {
            throw new ImsException("Can't retrieve session with null intent",
                    ImsReasonInfo.CODE_LOCAL_ILLEGAL_ARGUMENT);
        }

        int incomingServiceId = getServiceId(incomingCallIndication);

        if (serviceId != incomingServiceId) {
            throw new ImsException("Service id is mismatched in the incoming call intent",
                    ImsReasonInfo.CODE_LOCAL_ILLEGAL_ARGUMENT);
        }

        String callId = getCallId(incomingCallIndication);

        if (callId == null) {
            throw new ImsException("Call ID missing in the incoming call intent",
                    ImsReasonInfo.CODE_LOCAL_ILLEGAL_ARGUMENT);
        }

        String callNum = getCallNum(incomingCallIndication);

        if (callNum == null) {
            throw new ImsException("Call Num missing in the incoming call intent",
                    ImsReasonInfo.CODE_LOCAL_ILLEGAL_ARGUMENT);
        }

        int seqNum = getSeqNum(incomingCallIndication);

        if (seqNum == -1) {
            throw new ImsException("seqNum missing in the incoming call intent",
                    ImsReasonInfo.CODE_LOCAL_ILLEGAL_ARGUMENT);
        }

        try {
            mMtkImsService.setCallIndication(serviceId, callId, callNum, seqNum, isAllow);
        } catch (RemoteException e) {
            throw new ImsException("setCallIndication()", e,
                    ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN);
        }
    }

    /**
    * To hangup all calls.
    * @throws ImsException if getting the ims status result in an error.
    * @hide
    */
    public void hangupAllCall(int serviceId) throws ImsException {

        checkAndThrowExceptionIfServiceUnavailable();

        try {
            mMtkImsService.hangupAllCall(serviceId);
        } catch (RemoteException e) {
            throw new ImsException("hangupAll()", e, ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN);
        }
    }

    /**
     * Gets the service type from the specified incoming call broadcast intent.
     *
     * @param incomingCallIntent the incoming call broadcast intent
     * @return the service identifier or -1 if the intent does not contain it
     */
    private static int getServiceId(Intent incomingCallIntent) {
        if (incomingCallIntent == null) {
            return (-1);
        }

        return incomingCallIntent.getIntExtra(EXTRA_SERVICE_ID, -1);
    }

    /// M: Dynamic IMS Switch @{
    /**
     * Returns a feature value by currently phone id's resource.
     */
    protected boolean isImsResourceSupport(int feature) {
        // Don't care if Dynamic IMS Switch not enabled.
        boolean support = true;
        log("isImsResourceSupport, feature:" + feature);
        if ("1".equals(SystemProperties.get("persist.mtk_dynamic_ims_switch"))) {

            if (!SubscriptionManager.isValidPhoneId(mPhoneId)) {
                loge("Invalid main phone " + mPhoneId + ", return true as don't care");
                return support;
            }
            try {
                MtkImsConfig config = getConfigInterfaceEx(mPhoneId, mContext);
                if (config != null) {
                    support = (config.getImsResCapability(feature) !=
                            ImsConfig.FeatureValueConstants.ON)? false : true;
                }
            } catch (ImsException e) {
                loge("isImsResourceSupport() failed!" + e);
            }
            if (DBG) {
                log("isImsResourceSupport(" + feature + ") return " +
                        support + " on phone: " + mPhoneId);
            }
        }

        return support;
    }

    /**
     * Get the boolean config for sub id from carrier config manager.
     *
     * @param context the context to get carrier service
     * @param key config key defined in CarrierConfigManager
     * @param phoneId phone id
     * @return boolean value of corresponding key.
     */
    /**
     * TODO: Phase out this one and use getBooleanCarrierConfigForSlot instead
     */
    protected boolean getBooleanCarrierConfig(String key, int phoneId) {
        CarrierConfigManager configManager = (CarrierConfigManager) mContext.getSystemService(
                Context.CARRIER_CONFIG_SERVICE);
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(phoneId);
        log("getBooleanCarrierConfig: phoneId=" + phoneId + " subId=" + subId);
        PersistableBundle b = null;
        if (configManager != null) {
            b = configManager.getConfigForSubId(subId);
        }
        if (b != null) {
            log("getBooleanCarrierConfig: key=" + key + " value=" + b.getBoolean(key));
            return b.getBoolean(key);
        } else {
            log("Return static default defined in CarrierConfigManager.");
            // Return static default defined in CarrierConfigManager.
            return CarrierConfigManager.getDefaultConfig().getBoolean(key);
        }
    }

    /**
     * Get the boolean config from carrier config manager.
     *
     * @param key config key defined in CarrierConfigManager
     * @return boolean value of corresponding key.
     */
    @Override
    public boolean getBooleanCarrierConfigForSlot(String key) {
        int[] subIds = SubscriptionManager.getSubId(mPhoneId);
        int subId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
        if (subIds != null && subIds.length >= 1) {
            subId = subIds[0];
        }
        log("getBooleanCarrierConfigForSlot: mPhoneId=" + mPhoneId + " subId=" + subId);
        PersistableBundle b = null;
        if (mConfigManager != null) {
            // If an invalid subId is used, this bundle will contain default values.
            b = mConfigManager.getConfigForSubId(subId);
        }
        if (b != null) {
            return b.getBoolean(key);
        } else {
            // Return static default defined in CarrierConfigManager.
            return CarrierConfigManager.getDefaultConfig().getBoolean(key);
        }
    }

    /**
     * Get the int config from carrier config manager.
     *
     * @param key config key defined in CarrierConfigManager
     * @return integer value of corresponding key.
     */
    @Override
    public int getIntCarrierConfigForSlot(String key) {
        int[] subIds = SubscriptionManager.getSubId(mPhoneId);
        int subId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
        if (subIds != null && subIds.length >= 1) {
            subId = subIds[0];
        }
        log("getIntCarrierConfigForSlot: mPhoneId=" + mPhoneId + " subId=" + subId);
        PersistableBundle b = null;
        if (mConfigManager != null) {
            // If an invalid subId is used, this bundle will contain default values.
            b = mConfigManager.getConfigForSubId(subId);
        }
        if (b != null) {
            return b.getInt(key);
        } else {
            // Return static default defined in CarrierConfigManager.
            return CarrierConfigManager.getDefaultConfig().getInt(key);
        }
    }

    /**
     * MIMS
     * Resets ImsManager settings back to factory defaults.
     * @hide
     */
    public static void factoryReset(Context context, int phoneId) {

        log("factoryReset: phoneId=" + phoneId);
        if (isSupportMims() == false) {
            int mainPhoneId = getMainPhoneIdForSingleIms(context);
            /// M: CT VOLTE setting is managed by sim, no need reset if it is not main sim @{
            if (SystemProperties.get("persist.mtk_ct_volte_support").equals("1")) {
                if (mainPhoneId != phoneId) {
                    log("factoryReset: mainPhoneId=" + mainPhoneId);
                    return;
                }
            }
            /// @}

            MtkImsManager imsManager = (MtkImsManager)(ImsManager.getInstance(context, mainPhoneId));
            // Set VoLTE to default
            android.provider.Settings.Global.putInt(context.getContentResolver(),
                    android.provider.Settings.Global.ENHANCED_4G_MODE_ENABLED,
                    (SystemProperties.get("persist.mtk_ct_volte_support").equals("1") &&
                            !imsManager.getBooleanCarrierConfig(
                            MtkCarrierConfigManager.MTK_KEY_DEFAULT_ENHANCED_4G_MODE_BOOL,
                            mainPhoneId)) ?
                            ImsConfig.FeatureValueConstants.OFF :
                            ImsConfig.FeatureValueConstants.ON);

            // Set VoWiFi to default
            android.provider.Settings.Global.putInt(context.getContentResolver(),
                    android.provider.Settings.Global.WFC_IMS_ENABLED,
                    imsManager.getBooleanCarrierConfig(
                            CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_ENABLED_BOOL,
                            mainPhoneId) ?
                            ImsConfig.FeatureValueConstants.ON :
                            ImsConfig.FeatureValueConstants.OFF);

            // Set VoWiFi mode to default
            android.provider.Settings.Global.putInt(context.getContentResolver(),
                    android.provider.Settings.Global.WFC_IMS_MODE,
                    imsManager.getIntCarrierConfigForSlot(
                            CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_MODE_INT));

            // Set VoWiFi roaming to default
            android.provider.Settings.Global.putInt(context.getContentResolver(),
                    android.provider.Settings.Global.WFC_IMS_ROAMING_ENABLED,
                    imsManager.getBooleanCarrierConfig(
                            CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_ROAMING_ENABLED_BOOL,
                            mainPhoneId) ?
                            ImsConfig.FeatureValueConstants.ON :
                            ImsConfig.FeatureValueConstants.OFF);

            // Set VT to default
            android.provider.Settings.Global.putInt(context.getContentResolver(),
                    android.provider.Settings.Global.VT_IMS_ENABLED,
                    ImsConfig.FeatureValueConstants.ON);

            // Push settings to ImsConfig
            ImsManager.updateImsServiceConfig(context,
                    getMainPhoneIdForSingleIms(context), true);
        } else {
            /*
             * Reset specific phone IMS setting for MIMS project
             * VoLTE Setting: ImsConfig.FeatureValueConstants.ON
             * WFC Setting: CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_ENABLED_BOOL
             * WFC MODE Setting: CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_MODE_INT)
             * WFC Roaming Setting:
             *              CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_ROAMING_ENABLED_BOOL
             * VT Setting: ImsConfig.FeatureValueConstants.ON
             */
            MtkImsManager imsManager = (MtkImsManager)(ImsManager.getInstance(context, phoneId));
            if (imsManager != null) {
                // Set VoLTE to default
                int value = (SystemProperties.get("persist.mtk_ct_volte_support").equals("1") &&
                        !imsManager.getBooleanCarrierConfigForSlot(
                            MtkCarrierConfigManager.MTK_KEY_DEFAULT_ENHANCED_4G_MODE_BOOL)) ?
                    ImsConfig.FeatureValueConstants.OFF :
                    ImsConfig.FeatureValueConstants.ON;
                imsManager.setSettingValueByKeyForSlot(VOLTE_SETTING, value);

                // Set VoWiFi to default
                value = imsManager.getBooleanCarrierConfigForSlot(
                        CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_ENABLED_BOOL) ?
                    ImsConfig.FeatureValueConstants.ON : ImsConfig.FeatureValueConstants.OFF;
                imsManager.setSettingValueByKeyForSlot(WFC_SETTING, value);

                // Set VoWiFi mode to default
                value = imsManager.getIntCarrierConfigForSlot(
                        CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_MODE_INT);
                imsManager.setSettingValueByKeyForSlot(WFC_MODE_SETTING, value);

                // Set VoWiFi roaming to default
                value = imsManager.getBooleanCarrierConfigForSlot(
                        CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_ROAMING_ENABLED_BOOL) ?
                    ImsConfig.FeatureValueConstants.ON :
                    ImsConfig.FeatureValueConstants.OFF;
                imsManager.setSettingValueByKeyForSlot(WFC_ROAMING_SETTING, value);

                // Set VT to default
                imsManager.setSettingValueByKeyForSlot(VILTE_SETTING,
                        ImsConfig.FeatureValueConstants.ON);
            }

            // Push settings to ImsConfig
            ImsManager.updateImsServiceConfig(context, phoneId, true);
        }
    }

    /**
     * Static version API to get the config interface.
     */
    private MtkImsConfig getConfigInterfaceEx(int phoneId,
            Context context) throws ImsException {
        MtkImsConfig config = null;

        checkAndThrowExceptionIfServiceUnavailable();

        try {
            IMtkImsConfig binder = mMtkImsService.getConfigInterfaceEx(phoneId);
            if (binder == null) {
                throw new ImsException("getConfigInterfaceEx()",
                            ImsReasonInfo.CODE_LOCAL_SERVICE_UNAVAILABLE);
            }
            config = new MtkImsConfig(binder, context);
        } catch (RemoteException e) {
            throw new ImsException("getConfigInterfaceEx()", e,
                ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN);
        }
        return config;
    }
    /// @}

    /**
     * Creates a {@link ImsCall} to take an incoming call.
     *
     * @param sessionId a session id which is obtained from {@link ImsManager#open}
     * @param incomingCallIntent the incoming call broadcast intent
     * @param listener to listen to the call events from {@link ImsCall}
     * @return a {@link ImsCall} object
     * @throws ImsException if calling the IMS service results in an error
     */
    @Override
    public ImsCall takeCall(int sessionId, Intent incomingCallIntent,
                            ImsCall.Listener listener) throws ImsException {
        if (DBG) {
            log("takeCall :: sessionId=" + sessionId
                    + ", incomingCall=" + incomingCallIntent);
        }

        checkAndThrowExceptionIfServiceUnavailable();

        if (incomingCallIntent == null) {
            throw new ImsException("Can't retrieve session with null intent",
                    ImsReasonInfo.CODE_LOCAL_ILLEGAL_ARGUMENT);
        }

        int incomingServiceId = getImsSessionId(incomingCallIntent);

        if (sessionId != incomingServiceId) {
            throw new ImsException("Service id is mismatched in the incoming call intent",
                    ImsReasonInfo.CODE_LOCAL_ILLEGAL_ARGUMENT);
        }

        String callId = getCallId(incomingCallIntent);

        if (callId == null) {
            throw new ImsException("Call ID missing in the incoming call intent",
                    ImsReasonInfo.CODE_LOCAL_ILLEGAL_ARGUMENT);
        }

        try {
            IMtkImsCallSession session = mMtkImsService.getPendingMtkCallSession(sessionId, callId);

            if (session == null) {
                throw new ImsException("No pending IMtkImsCallSession for the call",
                        ImsReasonInfo.CODE_LOCAL_NO_PENDING_CALL);
            }

            ImsCallProfile callProfile = session.getCallProfile();
            if (callProfile == null) {
                throw new ImsException("takeCall(): profile is null", ImsReasonInfo
                        .CODE_UNSPECIFIED);
            }
            ImsCall call = new MtkImsCall(mContext, callProfile);

            call.attachSession(new MtkImsCallSession(session));
            call.setListener(listener);

            return call;
        } catch (Throwable t) {
            throw new ImsException("takeCall()", t, ImsReasonInfo.CODE_UNSPECIFIED);
        }
    }

    /**
     * Creates a {@link ImsCallSession} with the specified call profile.
     * Use other methods, if applicable, instead of interacting with
     * {@link ImsCallSession} directly.
     *
     * @param serviceId a service id which is obtained from {@link ImsManager#open}
     * @param profile a call profile to make the call
     */
    @Override
    protected ImsCallSession createCallSession(int serviceId,
                                               ImsCallProfile profile) throws ImsException {
        try {
            return new MtkImsCallSession(mMtkImsService.createCallSession(serviceId, profile, null));
        } catch (RemoteException e) {
            Rlog.w(TAG, "CreateCallSession: Error, remote exception: " + e.getMessage());
            throw new ImsException("createCallSession()", e,
                    ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN);
        }
    }

    @Override
    protected boolean isImsTurnOffAllowed() {
        log("CarrierConfig:" + getBooleanCarrierConfigForSlot(
                CarrierConfigManager.KEY_CARRIER_ALLOW_TURNOFF_IMS_BOOL) +
                ", wfcendablebyplateform:" + isWfcEnabledByPlatformForSlot() +
                ", wfcenablebyUser:" + isWfcEnabledByUserForSlot());
        return isTurnOffImsAllowedByPlatformForSlot()
                && (!isWfcEnabledByPlatformForSlot()
                || !isWfcEnabledByUserForSlot());
    }

    @Override
    protected void setLteFeatureValues(boolean turnOn) {
        log("MTK setLteFeatureValues: " + turnOn);
        try {
            ImsConfig config = getConfigInterface();
            if (config != null) {
                config.setFeatureValue(ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE,
                        TelephonyManager.NETWORK_TYPE_LTE, turnOn ? 1 : 0, mImsConfigListener);

                if (isVolteEnabledByPlatformForSlot()) {
                    boolean ignoreDataEnabledChanged = getBooleanCarrierConfig(
                            CarrierConfigManager.KEY_IGNORE_DATA_ENABLED_CHANGED_FOR_VIDEO_CALLS,
                            mPhoneId);
                    boolean enableViLte = turnOn && isVtEnabledByUserForSlot() &&
                            (ignoreDataEnabledChanged || isDataEnabled());
                    config.setFeatureValue(ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE,
                            TelephonyManager.NETWORK_TYPE_LTE,
                            enableViLte ? 1 : 0,
                            mImsConfigListener);
                }
            }
        } catch (ImsException e) {
            loge("setLteFeatureValues: exception ", e);
        }
    }

    /**
      * An API to customization platform enabled status.
      * @param context The context for retrive plug-in.
      * @param feature The IMS feature defined in ImsConfig.FeatureConstants.
      *
      * @return return enabled status.
      */
    private boolean isFeatureEnabledByPlatformExt(int feature) {
        boolean isEnabled = true;
        if (mContext == null) {
            logw("Invalid: context=" + mContext + ", return " + isEnabled);
            return isEnabled;
        }
        sImsManagerExt = getImsManagerPluginInstance(mContext);
        if (sImsManagerExt == null) {
            logw("plugin null=" + sImsManagerExt + ", return " + isEnabled);
            return isEnabled;
        }
        isEnabled = sImsManagerExt.isFeatureEnabledByPlatform(mContext, feature, mPhoneId);
        log("isFeatureEnabledByPlatformExt(), feature:" + feature + ", isEnabled:" + isEnabled);
        return isEnabled;
    }

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            log("[onReceive] action=" + intent.getAction());
            ///M : CarrierExpress dynamic switch @{
            if (CarrierExpressManager.ACTION_OPERATOR_CONFIG_CHANGED.equals(intent.getAction())) {
                log("[onReceive] intent =" + intent.getAction());
                sImsManagerExt = null;
            }
            /// @}
        }
    };

    /**
     * Get the phone id with main capability.
     */
    @Override
    protected int getMainCapabilityPhoneId(Context context) {
        return getMainPhoneIdForSingleIms(context);
    }

    private static IImsManagerExt getImsManagerPluginInstance(Context context) {
        log("getImsManagerPluginInstance");
        if (sImsManagerExt == null) {
            sImsManagerExt = OpImsCustomizationUtils.getOpFactory(context)
                .makeImsManagerExt(context);
            if (sImsManagerExt == null) {
                log("Unable to create ImsManagerPluginInstane");
            }
        }
        return sImsManagerExt;
    }

    /// M: VILTE enable not dependent on data enable @{
    @Override
    protected boolean isTestSim() {
        int phoneId = mPhoneId;
        if (SystemProperties.getInt(MULTI_IMS_SUPPORT, 1) == 1) {
            phoneId = getMainCapabilityPhoneId(mContext);
        }
        boolean isTestSim = false;
        switch (phoneId) {
            case SIM_ID_1:
                isTestSim = "1".equals(SystemProperties.get("gsm.sim.ril.testsim", "0"));
                break;
            case SIM_ID_2:
                isTestSim = "1".equals(SystemProperties.get("gsm.sim.ril.testsim.2", "0"));
                break;
            case SIM_ID_3:
                isTestSim = "1".equals(SystemProperties.get("gsm.sim.ril.testsim.3", "0"));
                break;
            case SIM_ID_4:
                isTestSim = "1".equals(SystemProperties.get("gsm.sim.ril.testsim.4", "0"));
                break;
        }
        return isTestSim;
    }
    /// @}
}
