/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.media.AudioSystem;
import android.os.SystemProperties;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.audio.Audio;
import com.mediatek.engineermode.audio.AudioVolume;
import com.mediatek.engineermode.audio.AudioModeSetting;
import com.mediatek.engineermode.audio.AudioSpeechEnhancementV1;
import com.mediatek.engineermode.audio.AudioSpeechEnhancementV2;
import com.mediatek.engineermode.audio.AudioDebugInfo;
import com.mediatek.engineermode.audio.AudioSpeechLoggerXV1;
import com.mediatek.engineermode.audio.AudioSpeechLoggerXV2;
import com.mediatek.engineermode.audio.AudioDebugSession;
import com.mediatek.engineermode.audio.AudioAudioLogger;
import com.mediatek.engineermode.touchscreen.TsHandWriting;
import com.mediatek.engineermode.R;

import java.util.ArrayList;

public class AudioTest extends ActivityInstrumentationTestCase2<Audio> {

    private static final int LIST_ITEMS_COUNT = 9;
    private static final int FIR_SPINNER_COUNT = 6;
    private static final String CURRENT_MODE = "CurrentMode";
    private static final String AUDIO_VERSION_COMMAND = "GET_AUDIO_VOLUME_VERSION";
    private static final String AUDIO_VERSION_1 = "GET_AUDIO_VOLUME_VERSION=1";
    private static final String AUDIO_TURNING_VER = "ro.mtk_audio_tuning_tool_ver";
    private static final String AUDIO_TURNING_VER_V2_1 = "V2.1";
    private static final String AUDIO_TURNING_VER_V2_2 = "V2.2";
    private static final String NORMAL_MODE_NAME = "Normal Mode";
    private static final String HEADSET_MODE_NAME = "Headset Mode";
    private static final String LOUDSPEAKER_MODE_NAME = "LoudSpeaker Mode";
    private static final String HEADSET_LOUDSPEAKER_MODE_NAME = "Headset_LoudSpeaker Mode";
    private static final String VOLUME_NAME = "Volume";
    private static final String SPEECH_ENHANCE_NAME = "Speech Enhancement";
    private static final String DEBUG_INFO_NAME = "Debug Info";
    private static final String DEBUG_SESSION_NAME = "Debug Session";
    private static final String SPEECH_LOGGER_NAME = "Speech Logger";
    private static final String AUDIO_LOGGER_NAME = "Audio Logger";
    private static final String AUDIO_WAKELOCK_NAME = "Audio Wake Lock";

    private static final String VOLUME_VOICE_NAME = "Voice";
    private static final String VOLUME_VOIP_NAME = "VoIP";
    private static final String VOLUME_PLAYBACK_NAME = "Audio Playback";
    private static final String VOLUME_RECORD_NAME = "Audio Record";

    private Boolean[] checkBoxStatus = new Boolean[10];

    private Solo mSolo;
    private Context mContext;
    private Instrumentation mIns;
    private Activity mActivity;

    private String mVersion;

    /**
     * Audio tuning version.
     *
     */
    enum AudioTuningVer {
        VER_1, VER_2_1, VER_2_2
    };

    private static AudioTuningVer sAudioTuningVer = AudioTuningVer.VER_1;

    private void initAudioTunVer() {

        sAudioTuningVer = AudioTuningVer.VER_1;
        String value = SystemProperties.get(AUDIO_TURNING_VER);
        Elog.d("Audio/Test", "initAudioTurnVer:" + value);

        if (value != null) {
            if (AUDIO_TURNING_VER_V2_1.equalsIgnoreCase(value)) {
                sAudioTuningVer = AudioTuningVer.VER_2_1;
            }

            if (AUDIO_TURNING_VER_V2_2.equalsIgnoreCase(value)) {
                sAudioTuningVer = AudioTuningVer.VER_2_2;
            }
        }
        Elog.d("Audio/Test", "sAudioTuningVer:" + sAudioTuningVer);
    }

    private boolean searchScrollList(String tag) {
        boolean found = false;
        do {
            if (mSolo.searchText(tag)) {
                found = true;
                break;
            }
        } while (mSolo.scrollDown());
        return found;
    }

    public AudioTest() {
        super("com.mediatek.engineermode", Audio.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mIns = getInstrumentation();
        mContext = mIns.getTargetContext();
        mActivity = getActivity();
        mSolo = new Solo(mIns, mActivity);
        mVersion = AudioSystem.getParameters(AUDIO_VERSION_COMMAND);
        initAudioTunVer();
    }

    @BasicFuncAnnotation
    public void testCase01_ActivityAudio() {
        verifyPreconditions();
    }

    @UiAnnotation
    public void testCase02_TestVolume() {
        if (sAudioTuningVer == AudioTuningVer.VER_1) {
            return;
        }
        if (!EmOperate.enterItem(mSolo, VOLUME_NAME, "AudioVolume"))
            return;

        voiceTest();
        voIPTest();
        playbackTest();
        RecordTest();
        mSolo.goBack();
    }

    public void voiceTest() {

        if (!EmOperate.enterItem(mSolo, VOLUME_VOICE_NAME, "AudioVolumeVoice"))
            return;

        mSolo.clearEditText(0);
        mSolo.enterText(0, "160");
        mSolo.clickOnButton("Set");
        mSolo.sleep(EmOperate.TIME_MID);
        assertTrue("voiceTest have issues", mSolo.waitForText("Set succeeded"));
        mSolo.goBack();
        mSolo.goBack();

        EmOperate.enterItem(mSolo, VOLUME_VOICE_NAME, "AudioVolumeVoice");

        assertTrue("voiceTest have issues", mSolo.waitForText("160"));
        mSolo.clearEditText(0);
        mSolo.enterText(0, "164");
        mSolo.clickOnButton("Set");
        mSolo.goBack();
        mSolo.goBack();
    }

    public void voIPTest() {

        if (!EmOperate.enterItem(mSolo, VOLUME_VOIP_NAME, "AudioVolumeVoIP"))
            return;
        mSolo.clearEditText(0);
        mSolo.enterText(0, "200");
        mSolo.clickOnButton("Set");
        mSolo.sleep(EmOperate.TIME_MID);
        assertTrue("voIPTest have issues", mSolo.waitForText("Set succeeded"));
        mSolo.goBack();
        mSolo.goBack();
        EmOperate.enterItem(mSolo, VOLUME_VOIP_NAME, "AudioVolumeVoIP");
        assertTrue("voIPTest have issues", mSolo.waitForText("200"));
        mSolo.clearEditText(0);
        mSolo.enterText(0, "208");
        mSolo.clickOnButton("Set");
        mSolo.goBack();
        mSolo.goBack();
    }

    public void playbackTest() {

        if (!EmOperate.enterItem(mSolo, VOLUME_PLAYBACK_NAME, "AudioVolumePlayback"))
            return;
        mSolo.clearEditText(0);
        mSolo.enterText(0, "120");
        mSolo.clickOnButton("Set");
        mSolo.sleep(EmOperate.TIME_MID);
        assertTrue("audioPlaybackTest have issues", mSolo.waitForText("Set succeeded"));
        mSolo.goBack();
        mSolo.goBack();
        EmOperate.enterItem(mSolo, VOLUME_PLAYBACK_NAME, "AudioVolumePlayback");
        assertTrue("audioPlaybackTest have issues", mSolo.waitForText("120"));
        mSolo.clearEditText(0);
        mSolo.enterText(0, "124");
        mSolo.clickOnButton("Set");
        mSolo.goBack();
        mSolo.goBack();
    }

    public void RecordTest() {

        if (!EmOperate.enterItem(mSolo, VOLUME_RECORD_NAME, "AudioVolumeRecord"))
            return;
        mSolo.clearEditText(0);
        mSolo.enterText(0, "200");
        mSolo.clickOnButton("Set");
        mSolo.sleep(EmOperate.TIME_MID);
        assertTrue("RecordTest have issues", mSolo.waitForText("Set succeeded"));
        mSolo.goBack();
        mSolo.goBack();
        EmOperate.enterItem(mSolo, VOLUME_RECORD_NAME, "AudioVolumeRecord");
        assertTrue("RecordTest have issues", mSolo.waitForText("200"));
        mSolo.clearEditText(0);
        mSolo.enterText(0, "208");
        mSolo.clickOnButton("Set");
        mSolo.goBack();
        mSolo.goBack();
    }

    @UiAnnotation
    public void testCase03_TestSpeechEnhancement() {
        if (sAudioTuningVer == AudioTuningVer.VER_1) {
            return;
        }

        if (!EmOperate.enterItem(mSolo, SPEECH_ENHANCE_NAME, "AudioSpeechEnhancementV2"))
            return;

        commonParameterTest();
        debugInfoTest();
        NarowBandTest();
        WideBandTest();
        mSolo.goBack();
    }

    public void commonParameterTest() {
        mSolo.pressSpinnerItem(0, 0);
        mSolo.clearEditText(0);
        mSolo.enterText(0, "1357");
        mSolo.clickOnButton("Set");
        mSolo.sleep(EmOperate.TIME_MID);
        assertTrue("commonParameterTest have issues", mSolo.waitForText("Set parameter done"));
        mSolo.goBack();

        mSolo.clickOnText(SPEECH_ENHANCE_NAME);
        mSolo.pressSpinnerItem(0, 0);
        assertTrue("commonParameterTest have issues", mSolo.waitForText("1357"));

        mSolo.clearEditText(0);
        mSolo.enterText(0, "0");
        mSolo.clickOnButton("Set");
        mSolo.goBack();
    }

    public void debugInfoTest() {
        mSolo.clickOnText(SPEECH_ENHANCE_NAME);
        mSolo.pressSpinnerItem(0, 1);
        mSolo.clearEditText(0);
        mSolo.enterText(0, "1357");
        mSolo.clickOnButton("Set");
        mSolo.sleep(EmOperate.TIME_MID);
        assertTrue("debugInfoTest have issues", mSolo.waitForText("Set parameter done"));
        mSolo.goBack();

        mSolo.clickOnText(SPEECH_ENHANCE_NAME);
        mSolo.pressSpinnerItem(0, 1);
        assertTrue("debugInfoTest have issues", mSolo.waitForText("1357"));

        mSolo.clearEditText(0);
        mSolo.enterText(0, "0");
        mSolo.clickOnButton("Set");
        mSolo.goBack();
    }

    public void NarowBandTest() {
        mSolo.clickOnText(SPEECH_ENHANCE_NAME);
        mSolo.pressSpinnerItem(0, 2);
        mSolo.clearEditText(0);
        mSolo.enterText(0, "1357");
        mSolo.clickOnButton("Set");
        mSolo.sleep(EmOperate.TIME_MID);
        assertTrue("NarowBandTest have issues", mSolo.waitForText("Set parameter done"));
        mSolo.goBack();

        mSolo.clickOnText(SPEECH_ENHANCE_NAME);
        mSolo.pressSpinnerItem(0, 2);
        assertTrue("NarowBandTest have issues", mSolo.waitForText("1357"));

        mSolo.clearEditText(0);
        mSolo.enterText(0, "128");
        mSolo.clickOnButton("Set");
        mSolo.goBack();
    }

    public void WideBandTest() {
        mSolo.clickOnText(SPEECH_ENHANCE_NAME);
        mSolo.pressSpinnerItem(0, 3);
        mSolo.pressSpinnerItem(1, 1);

        mSolo.clearEditText(0);

        mSolo.enterText(0, "1357");
        mSolo.clickOnButton("Set");
        mSolo.sleep(EmOperate.TIME_MID);
        assertTrue("NarowBandTest have issues", mSolo.waitForText("Set parameter done"));
        mSolo.goBack();

        mSolo.clickOnText(SPEECH_ENHANCE_NAME);
        mSolo.pressSpinnerItem(0, 3);
        mSolo.pressSpinnerItem(1, 1);
        assertTrue("NarowBandTest have issues", mSolo.waitForText("1357"));

        mSolo.clearEditText(0);
        mSolo.enterText(0, "96");
        mSolo.clickOnButton("Set");
        mSolo.goBack();
    }

    @UiAnnotation
    public void testCase04_TestDebugSession() {

        if (sAudioTuningVer == AudioTuningVer.VER_1) {
            return;
        }

        if (!EmOperate.enterItem(mSolo, DEBUG_SESSION_NAME, "AudioDebugSession"))
            return;

        ArrayList<CheckBox> checkBoxs = mSolo.getCurrentViews(CheckBox.class);
        for (int i = 0; i < checkBoxs.size(); i++) {
            CheckBox check = checkBoxs.get(i);
            if (check.isEnabled()) {
                mSolo.clickOnView(check);
                mSolo.sleep(EmOperate.TIME_SHORT);
                checkBoxStatus[i] = check.isChecked();
            }
        }
        mSolo.goBack();

        EmOperate.enterItem(mSolo, DEBUG_SESSION_NAME, "AudioDebugSession");

        ArrayList<CheckBox> checkBoxs2 = mSolo.getCurrentViews(CheckBox.class);

        for (int i = 0; i < checkBoxs2.size(); i++) {
            CheckBox check = checkBoxs2.get(i);
            if (check.isEnabled()) {
                assertTrue("TestDebugSession have issues",
                        check.isChecked() == checkBoxStatus[i]);
                mSolo.sleep(EmOperate.TIME_SHORT);
                mSolo.clickOnView(check);
            }
        }

        mSolo.clickOnButton("Detect");
        mSolo.sleep(200);
        mSolo.goBack();
    }

    @UiAnnotation
    public void testCase05_TestSpeechLog() {
        if (sAudioTuningVer == AudioTuningVer.VER_1) {
            return;
        }
        if (!EmOperate.enterItem(mSolo, SPEECH_LOGGER_NAME, "AudioSpeechLoggerXV2"))
            return;

        ArrayList<CheckBox> checkBoxs = mSolo.getCurrentViews(CheckBox.class);
        for (int i = 0; i < checkBoxs.size(); i++) {
            CheckBox check = checkBoxs.get(i);
            if (check.isShown()) {
                mSolo.clickOnView(check);
                mSolo.sleep(EmOperate.TIME_SHORT);
                checkBoxStatus[i] = check.isChecked();
            }
        }
        mSolo.goBack();

        EmOperate.enterItem(mSolo, SPEECH_LOGGER_NAME, "AudioSpeechLoggerXV2");

        ArrayList<CheckBox> checkBoxs2 = mSolo.getCurrentViews(CheckBox.class);

        for (int i = 0; i < checkBoxs2.size(); i++) {
            CheckBox check = checkBoxs2.get(i);
            if (check.isShown()) {
                // assertTrue("TestSpeechLog have issues", check.isChecked() ==
                // checkBoxStatus[i]);
                mSolo.sleep(EmOperate.TIME_SHORT);
                mSolo.clickOnView(check);
            }
        }

        mSolo.clickOnButton("Dump Speech Debug Info");
        assertTrue("Audio_Dump_SpeechDbgInfo have issues",
                mSolo.waitForText("set parameter success."));

        mSolo.goBack();
    }

    @UiAnnotation
    public void testCase06_TestAudioLog() {

        if (sAudioTuningVer == AudioTuningVer.VER_1) {
            return;
        }
        if (!EmOperate.enterItem(mSolo, AUDIO_LOGGER_NAME, "AudioAudioLogger"))
            return;

        ArrayList<CheckBox> checkBoxs = mSolo.getCurrentViews(CheckBox.class);
        for (int i = 1; i < 3; i++) {
            CheckBox check = checkBoxs.get(i);
            if (check.isShown()) {
                mSolo.clickOnView(check);
                //assertTrue("TestAudioLog have issues", mSolo.waitForText("Ret: -38"));
                mSolo.goBack();
                checkBoxStatus[i] = check.isChecked();
            }
        }

        mSolo.goBack();

        EmOperate.enterItem(mSolo, AUDIO_LOGGER_NAME, "AudioAudioLogger");
        mSolo.sleep(EmOperate.TIME_MID);
        ArrayList<CheckBox> checkBoxs2 = mSolo.getCurrentViews(CheckBox.class);

        for (int i = 1; i < 3; i++) {
            CheckBox check = checkBoxs2.get(i);
            if (check.isShown()) {
                assertTrue("TestAudioLog have issues", check.isChecked() == checkBoxStatus[i]);
                mSolo.clickOnView(check);
                mSolo.goBack();
            }
        }

        ArrayList<Button> button = mSolo.getCurrentViews(Button.class);

        mSolo.clickOnText("Get", 1);
        assertTrue("Get Audiocmd have issues", mSolo.waitForText("ret :"));
        mSolo.goBack();

        mSolo.clickOnText("Set", 1);
        assertTrue("Get Audiocmd have issues", mSolo.waitForText("ret :"));
        mSolo.goBack();

        mSolo.clickOnText("Get", 4);
        mSolo.sleep(EmOperate.TIME_MID);
        mSolo.goBack();

        mSolo.clickOnText("Set", 2);
        assertTrue("Get Audiocmd have issues", mSolo.waitForText("ret :"));
        mSolo.goBack();

        mSolo.goBack();
    }

    private void verifyPreconditions() {
        assertTrue(mIns != null);
        assertTrue(mActivity != null);
        assertTrue(mContext != null);
        assertTrue(mSolo != null);
    }

}
