/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony;

import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.Rlog;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;

import com.android.internal.telephony.DefaultPhoneNotifier;
import com.android.internal.telephony.IOnSubscriptionsChangedListener;
import com.android.internal.telephony.ITelephonyRegistry;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.mediatek.internal.telephony.IMtkTelephonyRegistryEx;
import com.mediatek.internal.telephony.MtkSubscriptionController;
import com.mediatek.internal.telephony.MtkTelephonyRegistryEx;

import mediatek.telephony.MtkServiceState;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * broadcast intents
 */
public class MtkPhoneNotifier extends DefaultPhoneNotifier {
    private static final String LOG_TAG = "MtkPhoneNotifr";
    private static final boolean DBG = false; // STOPSHIP if true

    protected IMtkTelephonyRegistryEx mMtkRegistry;

    private final AtomicInteger mPendingActionKey;
    // TelephonyRegistry will drop the notify with this mFakeSub and valid phone id.
    private final int mFakeSub = Integer.MAX_VALUE - 1;
    // Map of users that want to listen sub change.
    private Map<Integer, Runnable> mPendingActionMap = new ConcurrentHashMap<Integer, Runnable>();

    public MtkPhoneNotifier() {
        super();
        Rlog.d(LOG_TAG, "constructor");
        MtkTelephonyRegistryEx.init();
        mMtkRegistry = IMtkTelephonyRegistryEx.Stub.asInterface(ServiceManager.getService(
                "telephony.mtkregistry"));

        mPendingActionKey = new AtomicInteger(0);
        try {
            mRegistry.addOnSubscriptionsChangedListener("MtkPhoneNotifier",
                    mOnSubscriptionsChangedListener);
        } catch (RemoteException ex) {
            Rlog.d(LOG_TAG, "constructor: addOnSubscriptionsChangedListener Error");
        }
    }

    /**
     * get a unique and valid key for the adding action.
     * @return key
     */
    private Integer getKeyForPendingAction() {
        if (mPendingActionKey.get() == Integer.MAX_VALUE) {
            mPendingActionKey.set(0);
        }
        return mPendingActionKey.getAndIncrement();
    }

    /**
     * add pended action into pending queue.
     * @param key pending key value in map.
     * @param value pending action, a runnable object.
     */
    private void addToPendingQueue(Integer key, Runnable value) {
        mPendingActionMap.put(key, value);
    }

    private final IOnSubscriptionsChangedListener mOnSubscriptionsChangedListener =
            new IOnSubscriptionsChangedListener.Stub() {
        @Override
        public void onSubscriptionsChanged() {
            if (DBG) Rlog.d(LOG_TAG, "MtkPhoneNotifier: onSubscriptionsChanged");
            if (MtkSubscriptionController.getMtkInstance() != null) {
                if (MtkSubscriptionController.getMtkInstance().tryLockSubscriptionInfo()) {
                    if (DBG) Rlog.d(LOG_TAG, "MtkPhoneNotifier: tryLock success");
                    if (MtkSubscriptionController.getMtkInstance().isReady()) {
                        for (Iterator<Entry<Integer, Runnable>> iterator
                                = mPendingActionMap.entrySet().iterator(); iterator.hasNext(); ) {
                            Entry<Integer, Runnable> entry = iterator.next();

                            Runnable r = entry.getValue();
                            if (r != null) {
                                r.run();
                            }
                            iterator.remove();
                        }
                    }
                    if (DBG) Rlog.d(LOG_TAG, "MtkPhoneNotifier: tryLock unlock");
                    MtkSubscriptionController.getMtkInstance().unlockSubscriptionInfo();
                } else {
                    if (DBG) Rlog.d(LOG_TAG,
                            "MtkPhoneNotifier: onSubscriptionsChanged, tryLock fail");
                }
            } else {
                if (DBG) Rlog.d(LOG_TAG,
                        "MtkPhoneNotifier: MtkSubscriptionController.getMtkInstance() == null");
            }
        }
    };

    private boolean checkSubIdPhoneId(Phone s) {
        if (s.getSubId() < 0) {
            int defaultFallbackSubId =
                    MtkSubscriptionController.getMtkInstance().getDefaultFallbackSubId();
            int defaultFallbackPhoneId =
                    MtkSubscriptionController.getMtkInstance().getPhoneId(defaultFallbackSubId);
            Rlog.d(LOG_TAG, "checkSubIdPhoneId subId:" + defaultFallbackSubId
                    + " phoneId:" + defaultFallbackPhoneId);
            // sub is invalid, do check sender's phoneId and defaultFallbackPhoneId
            // return true for doing broadcast.
            return (defaultFallbackPhoneId == s.getPhoneId() ||
                    // first boot without SIM
                    defaultFallbackPhoneId == SubscriptionManager.INVALID_PHONE_INDEX ||
                    // plug out the only one SIM
                    defaultFallbackPhoneId == SubscriptionManager.DEFAULT_PHONE_INDEX);
        } else {
            // sub is valid
            return true;
        }
    }

    @Override
    public void notifyServiceState(Phone sender) {
        if (!MtkSubscriptionController.getMtkInstance().isReady()) {
            Rlog.d(LOG_TAG, "MtkPhoneNotifier put notifyServiceState to pending action");
            final Phone s = sender;
            addToPendingQueue(getKeyForPendingAction(), new Runnable() {
                @Override
                public void run() {
                    notifyServiceState(s);
                }
            });
        } else if (checkSubIdPhoneId(sender)) {
            super.notifyServiceState(sender);
        } else {
            ServiceState ss = sender.getServiceState();
            int phoneId = sender.getPhoneId();
            int subId = mFakeSub;

            if (ss == null) {
                ss = new MtkServiceState();
                ss.setStateOutOfService();
            }

            Rlog.d(LOG_TAG, "MtkPhoneNotifier notifyServiceState"
                    + " phoneId:" + phoneId
                    + " fake subId: " + subId
                    + " ServiceState: " + ss);

            try {
                if (mRegistry != null) {
                    mRegistry.notifyServiceStateForPhoneId(phoneId, subId, ss);
                }
            } catch (RemoteException ex) {
                // system process is dead
            }
        }
    }

    @Override
    public void notifySignalStrength(Phone sender) {
        if (!MtkSubscriptionController.getMtkInstance().isReady()) {
            Rlog.d(LOG_TAG, "MtkPhoneNotifier put notifySignalStrength to pending action");
            final Phone s = sender;
            addToPendingQueue(getKeyForPendingAction(), new Runnable() {
                @Override
                public void run() {
                    notifySignalStrength(s);
                }
            });
        } else if (checkSubIdPhoneId(sender)) {
            super.notifySignalStrength(sender);
        } else {
            int phoneId = sender.getPhoneId();
            int subId = mFakeSub;

            Rlog.d(LOG_TAG, "MtkPhoneNotifier notifySignalStrength"
                    + " phoneId:" + phoneId
                    + " fake subId: " + subId
                    + " signal: " + sender.getSignalStrength());

            try {
                if (mRegistry != null) {
                    mRegistry.notifySignalStrengthForPhoneId(phoneId, subId,
                            sender.getSignalStrength());
                }
            } catch (RemoteException ex) {
                // system process is dead
            }
        }
    }

    @Override
    public void notifyDataConnection(Phone sender, String reason, String apnType,
            PhoneConstants.DataState state) {
        if (sender.getActiveApnHost(apnType) == null &&
                !(PhoneConstants.APN_TYPE_DEFAULT.equals(apnType)
                || PhoneConstants.APN_TYPE_EMERGENCY.equals(apnType))) {
            return;
        }

         super.notifyDataConnection(sender, reason, apnType, state);
    }
}
