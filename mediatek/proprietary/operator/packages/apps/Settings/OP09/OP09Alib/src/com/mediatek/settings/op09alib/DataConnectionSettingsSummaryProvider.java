/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;
import com.android.settings.dashboard.SummaryLoader;

import com.mediatek.internal.telephony.MtkSubscriptionManager;

/**
 * For dynamic show summary for DataConnectionSettings.
 */
public class DataConnectionSettingsSummaryProvider {

    public static final SummaryLoader.SummaryProviderFactory SUMMARY_PROVIDER_FACTORY
            = new SummaryLoader.SummaryProviderFactory() {
        @Override
        public SummaryProvider createSummaryProvider(Activity activity,
                SummaryLoader summaryLoader) {
            return new SummaryProvider(activity, summaryLoader);
        }
    };

    /**
     * For dynamic show summary implement.
     * @see com.android.settings.dashboard.SummaryLoader
     */
    private static class SummaryProvider implements SummaryLoader.SummaryProvider {
        private static final String TAG = "DataConnectionSettingsSummaryProvider";
        private final Context mContext;
        private final SummaryLoader mSummaryLoader;
        private Context mDesContext;
        private boolean mIsRegObserver = false;
        private PhoneStateListener mPhoneStateListener;
        TelephonyManager mTelephonyManager;

        public SummaryProvider(Context context, SummaryLoader summaryLoader) {
            mContext = context;
            mSummaryLoader = summaryLoader;
            try {
                mDesContext = mContext.createPackageContext(
                        "com.mediatek.settings.op09alib",
                        Context.CONTEXT_INCLUDE_CODE
                                | Context.CONTEXT_IGNORE_SECURITY);
            } catch (NameNotFoundException e) {
                Log.d(TAG, "NameNotFoundException", e);
            }
        }
        @Override
        public void setListening(boolean listening) {
            if (listening) {
                IntentFilter filter = new IntentFilter();
                filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
                filter.addAction(TelephonyIntents.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED);
//                filter.addAction(TelephonyIntents.ACTION_SET_PHONE_RAT_FAMILY_DONE);
//                filter.addAction(TelephonyIntents.ACTION_SET_PHONE_RAT_FAMILY_FAILED);
                filter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
                filter.addAction(ChangeNetworkType.ACTION_RAT_CHANGED);
                mContext.registerReceiver(mReceiver, filter);
                registerObserver();
                createPhoneStateListener();
                updateSummary();
                mTelephonyManager = (TelephonyManager)
                        mContext.getSystemService(Context.TELEPHONY_SERVICE);
                mTelephonyManager.listen(mPhoneStateListener,
                        PhoneStateListener.LISTEN_DATA_CONNECTION_STATE);
            } else {
                mContext.unregisterReceiver(mReceiver);
                unregisterObserver();
                if (mTelephonyManager != null && mPhoneStateListener != null) {
                    mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
                }
            }
        }

        private void createPhoneStateListener() {
            mPhoneStateListener = new PhoneStateListener() {
                public void onDataConnectionStateChanged(int state) {
                    Log.d(TAG, "onDataConnectionStateChanged: updateSummary");
                    updateSummary();
                }
            };
        }
        private void unregisterObserver() {
            mContext.getContentResolver().unregisterContentObserver(mDataConnectionObserver);
            if (mIsRegObserver) {
                mContext.getContentResolver().unregisterContentObserver(mDataSlot1Observer);
                mContext.getContentResolver().unregisterContentObserver(mDataSlot2Observer);
                mIsRegObserver = false;
            }
        }
        private void registerObserver() {
            Log.d(TAG, "registerObserver: " + mIsRegObserver);
            if (mIsRegObserver) {
                return;
            }
            mContext.getContentResolver().registerContentObserver(
                    Settings.Global.getUriFor(Settings.Global.MOBILE_DATA),
                    true, mDataConnectionObserver);
            int subId1 = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
            int subId2 = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_2);
            if (SubscriptionManager.isValidSubscriptionId(subId1) &&
                SubscriptionManager.isValidSubscriptionId(subId2)) {
                Log.d(TAG, "register observer!");
                mIsRegObserver = true;
                mContext.getContentResolver().registerContentObserver(
                        Settings.Global.getUriFor(Settings.Global.MOBILE_DATA + subId1),
                        true, mDataSlot1Observer);
                mContext.getContentResolver().registerContentObserver(
                        Settings.Global.getUriFor(Settings.Global.MOBILE_DATA + subId2),
                        true, mDataSlot2Observer);
            }
        }
        private ContentObserver mDataConnectionObserver = new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                Log.d(TAG, "mDataConnectionObserver onChange selfChange=" + selfChange);
                if (!selfChange) {
                    updateSummary();
                }
            }
        };

        private ContentObserver mDataSlot1Observer = new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                Log.d(TAG, "mDataSlot1Observer onChange selfChange = " + selfChange);
                if (!selfChange) {
                    updateSummary();
                }
            }
        };
        private ContentObserver mDataSlot2Observer = new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                Log.d(TAG, "mDataSlot2Observer onChange selfChange=" + selfChange);
                if (!selfChange) {
                    updateSummary();
                }
            }
        };
        private void updateSummary() {
            mSummaryLoader.setSummary(this, getSummary());
        }
        private String getSummary() {
            String summary = "Unknown";
            Log.d(TAG, "getSummary Utils.isAllSlotRadioOff ="
                    + Utils.isAllSlotRadioOff(mContext));
            if (Utils.isAllSlotRadioOff(mContext)) {
                summary = mDesContext.getResources()
                    .getString(R.string.dataconnect_summary_disable);
            } else {
                boolean dataEnabled = TelephonyManager.getDefault().getDataEnabled();
                Log.d(TAG, "getSummary dataEnabled=" + dataEnabled);
                if (dataEnabled) {
                    summary = mDesContext.getResources()
                        .getString(R.string.dataconnect_summary_on);
                } else {
                    summary = mDesContext.getResources()
                        .getString(R.string.dataconnect_summary_off);
                }
            }
            return summary;
        }

        private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Log.i(TAG, "onReceive action = " + action);
                if (action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
                    updateSummary();
                } else if (action
                        .equals(TelephonyIntents.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED)) {
                    updateSummary();
//                } else if (action.equals(TelephonyIntents.ACTION_SET_PHONE_RAT_FAMILY_DONE) ||
//                        action.equals(TelephonyIntents.ACTION_SET_PHONE_RAT_FAMILY_FAILED)) {
//                    updateSummary();
                } else if (action.equals(TelephonyIntents.ACTION_SIM_STATE_CHANGED)) {
                    updateSummary();
                } else if (action.equals(ChangeNetworkType.ACTION_RAT_CHANGED)) {
                    updateSummary();
                }
            }
        };
    }

}
