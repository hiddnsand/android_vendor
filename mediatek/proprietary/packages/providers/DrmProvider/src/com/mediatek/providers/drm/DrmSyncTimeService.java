/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mediatek.providers.drm;

import static com.mediatek.providers.drm.OmaDrmHelper.DEBUG;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.drm.DrmManagerClient;
import android.net.SntpClient;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Using service to sync time, and start with foreground to avoid low memory kill.
 * 1. sync secure timer
 * 2. update time offset
 */
public class DrmSyncTimeService extends Service {
    private static final String TAG = "DRM/DrmSyncTimeService";
    // Message which handle by DrmSyncTimeHandler
    private static final int MSG_SYNC_SECURE_TIMER = 1;
    private static final int MSG_UPDATE_TIME_OFFSET = 2;
    public static final int INVALID_OFFSET = 0x7fffffff;
    public static final int SERVER_TIMEOUT = 3000;
    private DrmSyncTimeHandler mDrmSyncTimeHandler = null;
    private Context mContext;
    private DrmManagerClient mDrmManagerClient;
    private List<String> mNtpServers;

    // Sync with these SNTP host servers for different countries.
    private static String[] sHostList = new String[] {
        "pool.ntp.org",
        "2.android.pool.ntp.org",
        "time-a.nist.gov",
        "t1.hshh.org",
        "t2.hshh.org",
        "t3.hshh.org",
        "clock.via.net",
        "asia.pool.ntp.org",
        "europe.pool.ntp.org",
        "north-america.pool.ntp.org",
        "oceania.pool.ntp.org",
        "south-america.pool.ntp.org",
        "hshh.org"
    };

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        mContext = getApplicationContext();
        mDrmManagerClient = new DrmManagerClient(mContext);
        // Set as foreground process to avoid LMK when sync secure timer
        // use 0 does not show notification
        Notification notification = new Notification.Builder(mContext).build();
        startForeground(0, notification);

        // start sub thread to access network
        HandlerThread handlerThread = new HandlerThread("DrmSyncTimeThread");
        handlerThread.start();
        mDrmSyncTimeHandler = new DrmSyncTimeHandler(handlerThread.getLooper());

        // Get default ntp server from system and init sync secure time servers list
        String defaultServer = Settings.Global.getString(mContext.getContentResolver(),
                Settings.Global.NTP_SERVER);
        if (DEBUG) Log.d(TAG, "defaultServer: " + defaultServer);
        mNtpServers = new ArrayList<String>(sHostList.length + 1);
        for (String host : sHostList) {
            mNtpServers.add(host);
            if (host.equals(defaultServer)) {
                defaultServer = null;
            }
        }
        if (defaultServer != null) {
            mNtpServers.add(0, defaultServer);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (DEBUG) Log.d(TAG, "onStartCommand, intent:" + intent + ", startId:" + startId);
        // Intent is null, return not stick to avoid service restart again.
        if (intent == null) {
            // Just stop start id, because there may be some new start command coming when we
            // call stopself() to stop current service.
            stopSelf(startId);
            Log.d(TAG, "onStartCommand with null intent, stop service and avoid restart again");
            return START_NOT_STICKY;
        }
        String action = intent.getStringExtra(OmaDrmHelper.KEY_ACTION);
        int what = -1;
        if (OmaDrmHelper.ACTION_SYNC_SECURE_TIME.equals(action)) {
            what = MSG_SYNC_SECURE_TIMER;
        } else if (OmaDrmHelper.ACTION_UPDATE_TIME_OFFSET.equals(action)) {
            what = MSG_UPDATE_TIME_OFFSET;
        }
        mDrmSyncTimeHandler.removeMessages(what);
        Message msg = mDrmSyncTimeHandler.obtainMessage(what, startId, -1);
        msg.sendToTarget();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (DEBUG) Log.d(TAG, "onDestroy");
        // Set process as background and stop handler thread
        stopForeground(true);
        mDrmSyncTimeHandler.getLooper().quit();
        mDrmSyncTimeHandler = null;
        mContext = null;
        mDrmManagerClient.close();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Use handler thread to access network to avoid blocking main thread.
     */
    private class DrmSyncTimeHandler extends Handler {

        DrmSyncTimeHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SYNC_SECURE_TIMER:
                    syncSecureTime();
                    break;

                case MSG_UPDATE_TIME_OFFSET:
                    OmaDrmHelper.updateOffset(mDrmManagerClient);
                    break;

                default:
                    break;
            }
            // stop with start id of you self, so that service will be stopped only when
            // all job finish
            if (DEBUG) Log.d(TAG, "Stop DrmSyncTimeService with startId = " + msg.arg1);
            stopSelf(msg.arg1);
        }
    }

    private void syncSecureTime() {
        // If test on CT case, disable to sync secure timer to avoid send ntp package
        if (OmaDrmHelper.isRequestDisableSyncSecureTime()) {
            if (DEBUG) Log.d(TAG, "workaround for OP case, disable sync secure time");
            return;
        }

        for (String serverName: mNtpServers) {
            // 1. If current server not available, check next server
            if (!isServerAvailable(serverName)) {
                Log.d(TAG, "syncSecureTime with server not available for " + serverName);
                continue;
            }

            // 2. If sync offset is invalid, check next server
            SntpClient sntpClient = new SntpClient();
            Long sntpTime = (long) INVALID_OFFSET;
            if (!sntpClient.requestTime(serverName, SERVER_TIMEOUT)) {
                Log.d(TAG, "syncSecureTime failed with server " + serverName);
                continue;
            }
            sntpTime = (long) (sntpClient.getNtpTime() + SystemClock.elapsedRealtime()
                    - sntpClient.getNtpTimeReference());

            int offset = (int) ((sntpTime - System.currentTimeMillis()) / 1000);
            Log.d(TAG, "syncSecureTime: offset = " + offset);

            // 3. If secure time is not valid after update clock with offset, check next server
            if (!OmaDrmHelper.saveSecureTime(mDrmManagerClient, offset)) {
                Log.d(TAG, "syncSecureTime save failed with " + serverName);
                continue;
            }

            // Success sync secure time, break loop
            Log.d(TAG, "syncSecureTime success with host server " + serverName);
            break;
        }
    }

    private boolean isServerAvailable(String serverName) {
        InetAddress addr = null;
        try {
            addr = InetAddress.getByName(serverName);
        } catch (UnknownHostException e) {
            Log.w(TAG, "isServerAvailable with " + e);
        }
        if (DEBUG) Log.d(TAG, "isServerAvailable:serverName = " + serverName + ", addr = " + addr);
        return addr != null;
    }
}
