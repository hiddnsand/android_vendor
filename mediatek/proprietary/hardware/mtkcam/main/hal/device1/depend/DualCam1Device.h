/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_PLATFORM_HARDWARE_MTKCAM_V1_DEVICE_DUALCAM1DEVICE_H_
#define _MTK_PLATFORM_HARDWARE_MTKCAM_V1_DEVICE_DUALCAM1DEVICE_H_

#include "Cam1DeviceBase.h"
//
//#include <IResManager.h>
#include <mtkcam/drv/IHalSensor.h>
//
#if '1'==MTKCAM_HAVE_3A_HAL
#include <mtkcam/aaa/IHal3A.h>
#endif
//
#include <pthread.h>

#include <mtkcam/feature/DualCam/ISyncManager.h>
#include <mtkcam/utils/hw/IScenarioControl.h>


/******************************************************************************
 *
 ******************************************************************************/
namespace android
{

/******************************************************************************
 *
 ******************************************************************************/
class ImgBufProviderClientBridge : public IImgBufProviderClient
{
    public:
        ImgBufProviderClientBridge() {}
        virtual                     ~ImgBufProviderClientBridge() {}
        //
        /**
         * Notify when IImgBufProvider is created.
         */
        virtual bool                onImgBufProviderCreated(sp<IImgBufProvider>const& rpProvider);
        /**
         * Notify when IImgBufProvider is destroyed.
         */
        virtual void                onImgBufProviderDestroyed(int32_t const i4ProviderId);

        virtual void                addImgBufProviderClient(sp<IImgBufProviderClient> rpClient);
        virtual void                clearImgBufProviderClient();
    private:
        mutable Mutex                        mLock;
        Vector<sp<IImgBufProviderClient>>    mvClient;

};

/******************************************************************************
 *
 ******************************************************************************/
class DualCam1Device : public Cam1DeviceBase,
                             public NSCam::ISyncManagerCallback
{
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //  Data Members.
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    protected:  ////
        //
#define USER_NAME               "DualCam1Device"
        //
        NSCam::IHalSensor*              mpHalSensor;
        NSCam::IHalSensor*              mpHalSensor_2;
        //
#if '1'==MTKCAM_HAVE_3A_HAL
        NS3Av3::IHal3A*                 mpHal3a;
        NS3Av3::IHal3A*                 mpHal3a_2;
#endif
        bool                            mbThreadRunning;
        bool                            mRet;
        pthread_t                       mThreadHandle;
        bool                            mDisableWaitSensorThread;

        // + for dual cam
        sp<ICamAdapter>                 mpCamAdapter_W;
        sp<ICamAdapter>                 mpCamAdapter_T;
        sp<NSCam::IScenarioControl>     mpScenarioCtrl_main1;
        sp<NSCam::IScenarioControl>     mpScenarioCtrl_main2;
        sp<IParamsManagerV3>            mpParamsMgrV3_Sup;
        Mutex                           mSyncLock;

        int32_t                         mSensorId_Main2 = -1;
        bool                            mb4K2KVideoRecord;
        sp<ImgBufProviderClientBridge>  mpBridge;
        sp<NSCam::ISyncManager>                mSyncMgr;
        bool                            mbIsUninit = false;
        // - for dual cam
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //  Operations.
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public:     ////                    Instantiation.

        virtual                         ~DualCam1Device();
        DualCam1Device(
            String8 const&          rDevName,
            int32_t const           i4OpenId
        );

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //  [Template method] Operations.
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    protected:  ////                    Operations.

        /**
         * [Template method] Called by initialize().
         * Initialize the device resources owned by this object.
         */
        virtual bool                    onInit();

        /**
         * [Template method] Called by uninitialize().
         * Uninitialize the device resources owned by this object. Note that this is
         * *not* done in the destructor.
         */
        virtual bool                    onUninit();

        /**
         * [Template method] Called by startPreview().
         */
        virtual bool                    onStartPreview();

        /**
         * [Template method] Called by stopPreview().
         */
        virtual void                    onStopPreview();

        /**
         * [Template method] Called by setParameters().
         */
        virtual status_t                onSetParameters(const char* params);

        /**
         * to power on the sensor
         */
        virtual bool                    powerOnSensor();

        /**
         * the init function to be called in the thread
         */
        static void*                    doThreadInit(void* arg);

        /**
         * Wait for initialization in the thread done.
         */
        virtual bool                    waitThreadInitDone();

        // overwrite Cam1DeviceBase API
        virtual status_t                startPreview();
        virtual void                    stopPreview();
        virtual status_t                setParameters(const char* params);
        virtual bool                    initDualCameraAdapter();
        virtual status_t                initDisplayClient(preview_stream_ops* window);
        virtual status_t                enableDisplayClient();
        virtual status_t                startRecording();
        virtual void                    stopRecording();
        virtual status_t                takePicture();
        //
        virtual status_t                autoFocus();
        virtual status_t                cancelAutoFocus();

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //  Cam1Device Interfaces.
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public:     ////
        virtual int32_t                 queryDisplayBufCount() const    {
            return 6;
        }
        virtual bool                    disableWaitSensorThread(bool diable);

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //  SyncMgr callback Interfaces.
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public:
        virtual MINT32 onEvent(
            MINT32 const i4OpenId,
            MINT32 arg1, MINT32 arg2, void* arg3);

    private:
        int32_t     getOpenId_Main2() const {return mSensorId_Main2;}
        status_t    updateSensorId();
        status_t    setFeatureMode();
        MINT32 getModeFromParamsMgr();
        MBOOL  needReconstructRecordingPipe(sp<IParamsManagerV3> param, MBOOL isRecordMode);
        void   updateDeviceState(MINT32 state);
        status_t                        powerOffSensor(
                                            int32_t openId,
                                            NSCam::IHalSensor*& halSensor);
        status_t                        uninit3A(
                                            int32_t openId,
                                            NS3Av3::IHal3A*& hal3A);

};


/******************************************************************************
 *
 ******************************************************************************/
};  //namespace android
#endif  //_MTK_PLATFORM_HARDWARE_MTKCAM_V1_DEVICE_DEFAULTCAM1DEVICE_H_

