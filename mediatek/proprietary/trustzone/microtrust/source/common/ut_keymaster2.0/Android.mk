# Copyright (c) 2015-2016 MICROTRUST Incorporated
# All rights reserved
#
# This file and software is confidential and proprietary to MICROTRUST Inc.
# Unauthorized copying of this file and software is strictly prohibited.
# You MUST NOT disclose this file and software unless you get a license
# agreement from MICROTRUST Incorporated.

$(info ut_keymaster MICROTRUST_TEE_SUPPORT=$(MICROTRUST_TEE_SUPPORT))

ifeq ($(MICROTRUST_TEE_SUPPORT), yes)

LOCAL_PATH    := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := keystore.$(TARGET_BOARD_PLATFORM)
#LOCAL_CFLAGS := -DANDROID_CHANGES -DSOTER
LOCAL_CFLAGS := -DANDROID_CHANGES -Werror
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_RELATIVE_PATH := hw

PLATFORM_VERSION_MAJOR := $(word 1,$(subst .,$(space),$(PLATFORM_VERSION)))
LOCAL_CFLAGS+=-DPLATFORM_VERSION_MAJOR=$(PLATFORM_VERSION_MAJOR)
ifneq ($(PLATFORM_VERSION_MAJOR), 6)
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
endif

ifeq ($(strip $(MTK_SOTER_SUPPORT)), yes)
LOCAL_CFLAGS+=-DSOTER
endif

# Add new source files here
LOCAL_SRC_FILES +=\
    ut_km_api.c \
    ut_km_ioctl.c \
    ut_keymaster.cpp

LOCAL_C_INCLUDES +=\
    $(LOCAL_PATH)/inc \
    external/openssl/include \
    system/core/include \

LOCAL_SHARED_LIBRARIES := libimsg_log

include $(BUILD_SHARED_LIBRARY)

##### for kmsetkey ###########

include $(CLEAR_VARS)

LOCAL_MODULE := kmsetkey.$(TARGET_BOARD_PLATFORM)

LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_RELATIVE_PATH := hw

LOCAL_SRC_FILES +=\
    ut_km_ioctl.c \
    ut_kmsetkey.cpp

LOCAL_C_INCLUDES +=\
    $(LOCAL_PATH)/inc \

LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)
LOCAL_SHARED_LIBRARIES := libimsg_log

LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)

endif
