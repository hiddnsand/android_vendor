/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;

import com.mediatek.internal.telephony.MtkGsmCdmaPhone;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.util.List;
/**
 * replace MTK_FEATION_OPTION.
 */
public class Utils {
    private static final String TAG = "Utils";
    private static final String PACKAGE_NAME = "com.mediatek.settings.op09alib";
    private final static String ONE = "1";
    private final static String TWO = "2";
    // Check if mtk gemini feature is enabled
    public static boolean isGeminiSupport() {
        int count = TelephonyManager.getDefault().getPhoneCount();
        Log.i(TAG, "isGeminiSupport phone count = " + count);
        if (count > 1) {
            return true;
        }
        return false;
    }

    public static boolean isMtkSharedSdcardSupport() {
        return SystemProperties.get("ro.mtk_shared_sdcard").equals("1");
    }

    /**
     * judge if sim state is ready.
     * sim state:SIM_STATE_UNKNOWN = 0;SIM_STATE_ABSENT = 1
     * SIM_STATE_PIN_REQUIRED = 2;SIM_STATE_PUK_REQUIRED = 3;
     * SIM_STATE_NETWORK_LOCKED = 4;SIM_STATE_READY = 5;
     * SIM_STATE_CARD_IO_ERROR = 6;
     * @param context Context
     * @param simId sim id
     * @return true if is SIM_STATE_READY
     */
    public static boolean isSimStateReady(Context context, int simId) {
        TelephonyManager mTelephonyManager = TelephonyManager.from(context);
        Log.i(TAG, "isSimStateReady = " + mTelephonyManager.getSimState(simId));
        return mTelephonyManager.getSimState(simId) == TelephonyManager.SIM_STATE_READY;
    }

    /**
     * judge if sim radio on or not.
     * @param simId simid
     * @return true if sim radio on
     */
    public static boolean isTargetSimRadioOn(int simId) {
        int[] targetSubId = SubscriptionManager.getSubId(simId);
        if (targetSubId != null && targetSubId.length > 0) {
            for (int i = 0; i < targetSubId.length; i++) {
               if (isTargetSlotRadioOn(targetSubId[i])) {
                   Log.i(TAG, "isTargetSimRadioOn true simId = " + simId);
                   return true;
               }
            }
            Log.i(TAG, "isTargetSimRadioOn false simId = " + simId);
            return false;
        } else {
            Log.i(TAG, "isTargetSimRadioOn false because " +
                    "targetSubId[] = null or targetSubId[].length is 0  simId =" + simId);

            return false;
        }
    }

    /**
     * judge subid is radio on or not.
     * @param subId subId
     * @return true if this subId is radio on
     */
    public static boolean isTargetSlotRadioOn(int subId) {
        boolean radioOn = true;
        try {
            ITelephony iTel = ITelephony.Stub.asInterface(
                    ServiceManager.getService(Context.TELEPHONY_SERVICE));
            if (null == iTel) {
                Log.i(TAG, "isTargetSlotRadioOn = false because iTel = null");
                return false;
            }
            Log.i(TAG, "isTargetSlotRadioOn = " + iTel.isRadioOnForSubscriber(subId, PACKAGE_NAME));
            radioOn = iTel.isRadioOnForSubscriber(subId, PACKAGE_NAME);
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }
        Log.i(TAG, "isTargetSlotRadioOn radioOn = " + radioOn);
        return radioOn;
    }

    /**
     * Get the slotId insert or not.
     * @param slotId Slot id
     * @return true if slot card is insert.
     */
    public static boolean isSIMInserted(int slotId) {
        try {
            ITelephony tmex = ITelephony.Stub.asInterface(android.os.ServiceManager
                    .getService(Context.TELEPHONY_SERVICE));
            Log.i(TAG, "isSIMInserted slotId = " + slotId + "return" +
                    (tmex != null && tmex.hasIccCardUsingSlotIndex(slotId)));
            return (tmex != null && tmex.hasIccCardUsingSlotIndex(slotId));
        } catch (RemoteException e) {
            e.printStackTrace();
            Log.i(TAG, "isSIMInserted return false because catch RemoteException");
            return false;
        }
    }

    /**
     * Judge if it is SIM one.
     * @param subscriptionInfo the subscription information.
     * @return true if it is test card.
     */
    public static boolean phoneConstantsIsSimOne(SubscriptionInfo subscriptionInfo) {
        if (subscriptionInfo != null) {
            int slotId = SubscriptionManager.getSlotIndex(subscriptionInfo.getSubscriptionId());
            Log.i(TAG, "phoneConstantsIsSimOne slotIsSimOne = "
                  + (PhoneConstants.SIM_ID_1 == slotId));
            return PhoneConstants.SIM_ID_1 == slotId;
        }
        return false;
    }

    /**
     * finds a record with subId.
     * Since the number of SIMs are few, an array is fine.
     * @param context plugin context
     * @param subId sud id of the sim
     * @return subinfo of the subId
     */
    public static SubscriptionInfo findRecordBySubId(Context context, final int subId) {
        final List<SubscriptionInfo> subInfoList =
                SubscriptionManager.from(context).getActiveSubscriptionInfoList();
        if (subInfoList != null) {
            final int subInfoLength = subInfoList.size();

            for (int i = 0; i < subInfoLength; ++i) {
                final SubscriptionInfo sir = subInfoList.get(i);
                if (sir != null && sir.getSubscriptionId() == subId) {
                    return sir;
                }
            }
        }

        return null;
    }

    /**
     * finds a record with slotId.
     * Since the number of SIMs are few, an array is fine.
     * @param context plugin context
     * @param slotId solt id of the sim
     * @return subinfo of the subId
     */
    public static SubscriptionInfo findRecordBySlotId(Context context, final int slotId) {
        final List<SubscriptionInfo> subInfoList =
                SubscriptionManager.from(context).getActiveSubscriptionInfoList();
        if (subInfoList != null) {
            final int subInfoLength = subInfoList.size();

            for (int i = 0; i < subInfoLength; ++i) {
                final SubscriptionInfo sir = subInfoList.get(i);
                if (sir.getSimSlotIndex() == slotId) {
                    //Right now we take the first subscription on a SIM.
                    return sir;
                }
            }
        }
        return null;
    }

    /**
     * Judge phone is airPlane mode or not.
     * @param context just for getContentResolver.
     * @return true if phone is air plane mode.
     */
    public static boolean isAirPlaneMode(Context context) {
        return Settings.System.getInt(context.getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, -1) == 1;
    }

    /**
     * Judge if phone is calling or not.
     * @param context just for getSystemService.
     * @return true if phone is not calling status.
     */
    public static boolean isCallStateIDLE(Context context) {
        TelephonyManager telephonyManager =
            (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int currPhoneCallState = telephonyManager.getCallState();
        Log.i(TAG, "use lte isCallStateIDLE = " +
                (currPhoneCallState == TelephonyManager.CALL_STATE_IDLE));
        return currPhoneCallState == TelephonyManager.CALL_STATE_IDLE;
    }

    /**
     * For CT use test card or not.
     * Todo: Need refactory use CR02719923.
     * @return true if insert CT test card.
     */
    public static boolean isCtTestcard() {
        boolean testCard = SystemProperties.get("persist.sys.forcttestcard").equals("1");
        Log.i(TAG, "useCtTestcard, testCard = " + testCard);
        return testCard;
    }

    /**
     * 4g LTE pattern or not.
     * @param context context for getContentResolver.
     * @return true if 4g LTE pattern use. false if 4g LTE off.
     */
    public static boolean isLTEPatternSupport(Context context) {
        boolean isLtePatternSupport = false;
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
        int pattern = Settings.Global.getInt(context.getContentResolver(),
                            Settings.Global.PREFERRED_NETWORK_MODE + subId,
                            Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
        Log.d(TAG, "updateSwitch pattern = " + pattern);
        if (pattern == Phone.NT_MODE_LTE_CDMA_AND_EVDO
                || pattern == Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA
                || pattern == MtkGsmCdmaPhone.NT_MODE_LTE_TDD_ONLY) {
            isLtePatternSupport = true;
        } else {
            isLtePatternSupport = false;
        }
        return isLtePatternSupport;
    }

    /**
     * check all slot radio on.
     * @param context context
     * @return is all slots radio on;
     */
    public static boolean isAllSlotRadioOn(Context context) {
        boolean isAllRadioOn = true;
        int[] subs = SubscriptionManager.from(context).getActiveSubscriptionIdList();
        for (int i = 0; i < subs.length; ++i) {
            isAllRadioOn = isAllRadioOn && Utils.isTargetSlotRadioOn(subs[i]);
        }
        Log.d(TAG, "isAllSlotRadioOn(), isAllRadioOn: " + isAllRadioOn);
        return isAllRadioOn;
    }

    /**
     * CT A feature.
     * use in OP09SimManagementExt and SlotSettingsFragement.
     * if all sim card radio on,
     * if default data in simSlot if equals choose simSlot,
     * setDefaultDataSubId to not choose simSlot,
     * setDataEnabled true to not choose simSlot,
     * setDataEnabled false to choose simSlot.
     * @param context just context
     * @param simSlot which user choose simSlot.
     */
    public static void setToClosedSimSlot(Context context, int simSlot) {
        Log.d(TAG, "setToClosedSimSlot = " + simSlot);
        SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        TelephonyManager mTelephonyManager = TelephonyManager.from(context);
        int subId = subscriptionManager.getDefaultDataSubscriptionId();
        int subIdClose = MtkSubscriptionManager.getSubIdUsingPhoneId(simSlot);
        int simCount = mTelephonyManager.getSimCount();
        boolean enableBefore = mTelephonyManager.getDataEnabled();
        int subIdTmp;
        Log.d(TAG, "setToClosedSimSlot: subId = " + subId
                + "subId_close=" + subIdClose
                + "setToClosedSimSlot: simCount = " + simCount);
        if (subIdClose != subId) {
            return;
        }
        for (int i = 0; i < simCount; i++) {
            final SubscriptionInfo sir = Utils.findRecordBySlotId(context, i);
            if (sir != null) {
                subIdTmp = sir.getSubscriptionId();
                Log.d(TAG, "setToClosedSimSlot: sir subId_t = " + subIdTmp);
                if (subIdTmp != subId) {
                     subscriptionManager.setDefaultDataSubId(subIdTmp);
                     if (enableBefore) {
                         mTelephonyManager.setDataEnabled(subIdTmp, true);
                         mTelephonyManager.setDataEnabled(subId, false);
                     } else {
                         mTelephonyManager.setDataEnabled(subIdTmp, false);
                     }
                }
            }
        }
    }

    /**
     * check all slot radio off.
     * @param context context
     * @return is all slots radio off;
     */
    public static boolean isAllSlotRadioOff(Context context) {
        boolean isAllRadioOff = true;
        int[] subs = SubscriptionManager.from(context).getActiveSubscriptionIdList();
        for (int i = 0; i < subs.length; ++i) {
            isAllRadioOff = isAllRadioOff && !Utils.isTargetSlotRadioOn(subs[i]);
        }
        Log.d(TAG, "isAllSlotRadioOff(), isAllRadioOff: " + isAllRadioOff);
        return isAllRadioOff;
    }

    /**
     * CT A feature.
     * use in OP09SimManagementExt and SlotSettingsFragement.
     * if all sim card radio off,
     * if default data in simSlot if not choose simSlot,
     * setDefaultDataSubId to choose simSlot,
     * setDataEnabled true to choose simSlot,
     * setDataEnabled false to not choose simSlot.
     * @param context just context
     * @param simSlot which user choose simSlot.
     */
    public static void setToOpenSimSlot(Context context, int simSlot) {
        Log.d(TAG, "setToOpenSimSlot = " + simSlot);

        SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        TelephonyManager mTelephonyManager = TelephonyManager.from(context);
        int subId = subscriptionManager.getDefaultDataSubscriptionId();
        int subIdOpen = MtkSubscriptionManager.getSubIdUsingPhoneId(simSlot);
        int subIdTmp;
        int simCount = mTelephonyManager.getSimCount();
        boolean enableBefore = mTelephonyManager.getDataEnabled();
        Log.d(TAG, "subId= " + subId + " subOpen=" + subIdOpen + " enableBefore=" + enableBefore);
        if (subIdOpen == subId) {
            return;
        }
        Log.d(TAG, "setToOpenSimSlot: simCount = " + simCount);

        for (int i = 0; i < simCount; i++) {
            final SubscriptionInfo sir = Utils.findRecordBySlotId(context, i);
            if (sir != null) {
                subIdTmp = sir.getSubscriptionId();
                Log.d(TAG, "setToOpenSimSlot: sir subId_t = " + subIdTmp);
                if (subIdTmp == subIdOpen) {
                     subscriptionManager.setDefaultDataSubId(subIdOpen);
                     if (enableBefore) {
                         mTelephonyManager.setDataEnabled(subIdOpen, true);
                         mTelephonyManager.setDataEnabled(subId, false);
                     }
                }
            }
        }
    }

    public static boolean isCTLteTddTestSupport() {
        String[] type = MtkTelephonyManagerEx.getDefault()
            .getSupportCardType(PhoneConstants.SIM_ID_1);
        if (type == null) {
            return false;
        }
        boolean isUsimOnly = false;
        if ((type.length == 1) && ("USIM".equals(type[0]))) {
            isUsimOnly = true;
        }
        return isMtkSvlteSupport()
                && (ONE.equals(SystemProperties.get("persist.sys.forcttddtest", "0")))
                && isUsimOnly;
    }

    public static boolean isMtkSvlteSupport() {
        boolean isSupport = ONE.equals(
                SystemProperties.get("ro.boot.opt_c2k_lte_mode")) ? true : false;
        Log.d(TAG, "isMtkSvlteSupport(): " + isSupport);
        return isSupport;
    }

    public static boolean isAllRadioOff(Context context) {
        boolean isAllRadioOff = Settings.System.getInt(context.getContentResolver(),
                Settings.System.MSIM_MODE_SETTING, -1) == 0;
        Log.d(TAG, "isAllRadioOff(): " + isAllRadioOff);
        return isAllRadioOff;
    }

    public static int getSimNumber(Context context) {
        int simCount = TelephonyManager.from(context).getSimCount();
        Log.d(TAG, "getSimNumber = " + simCount);
        return simCount;
    }

}
