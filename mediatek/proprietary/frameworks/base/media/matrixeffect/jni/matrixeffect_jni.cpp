/*
* Copyright (C) 2011-2015 MediaTek Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#define LOG_TAG "Lomo_J_151022_09"  // reduce log msg
#include <string.h>
#include <android/log.h>
#include <jni.h>
#include <android/bitmap.h>
#include <gui/Surface.h>
#include <ui/Region.h>
#include <utils/CallStack.h>
#include <utils/RefBase.h>
#include <cstdio>
#include <stdio.h>
#include "JNIHelp.h"
#include "android_runtime/AndroidRuntime.h"
#include <android_runtime/android_graphics_SurfaceTexture.h>
#include <android_runtime/android_view_Surface.h>
#include <cutils/properties.h>
#include <utils/Vector.h>
#include <utils/StrongPointer.h>
#include <utils/Thread.h>
#include <utils/ThreadDefs.h>
#include <utils/Log.h>
#include <linux/ion_drv.h>
#include <ion/ion.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <errno.h>

// debug: begin
#include "libion_mtk/include/ion.h"
#include <sys/mman.h>
// debug: end

#ifndef MBOOL
typedef int MBOOL;
#endif

#ifndef MFALSE
#define MFALSE 0
#endif

#ifndef MTRUE
#define MTRUE 1
#endif

#ifndef MINT32
typedef signed int MINT32;
#endif

#ifndef ION_HEAP_MULTIMEDIA_MASK
#define ION_HEAP_TYPE_MULTIMEDIA 10
#define ION_HEAP_MULTIMEDIA_MASK (1 << ION_HEAP_TYPE_MULTIMEDIA)
#endif

#ifndef MAP_FAILED
#define MAP_FAILED ((void *)-1) macro
#endif

#include <sys/time.h>
#include <cutils/log.h>
class LomoTimer {
public:
    inline MINT32 getUsTime() {
        struct timeval tv;
        gettimeofday(&tv, NULL);

        return tv.tv_sec * 1000000 + tv.tv_usec;
    }

    LomoTimer(const char* info, MINT32 sensorDevId, MBOOL enable)
        : mInfo(info), mIdx(sensorDevId), mEnable(enable), mStartTime(0) {
        if (mEnable) mStartTime = getUsTime();
    }

    // used by global/static variables
    void start(const char* info, MINT32 sensorDevId, MBOOL enable) {
        mInfo = info;
        mIdx = sensorDevId;
        mEnable = enable;
        if (mEnable) mStartTime = getUsTime();
    }

    void printTime() {
        if (!mEnable) return;
        MINT32 endTime = getUsTime();
        ALOGD("[LomoTimer: %s,(ms) Id: %d] : %f\n",
            mInfo, mIdx,((double)(endTime - mStartTime)) / 1000);
    }

    void fps(MINT32 count) {
        if (!mEnable) return;
        MINT32 endTime = getUsTime();
        double lddiff = double(endTime - mStartTime);
        if(lddiff==0) {
            lddiff = 0.1;
            ALOGD("[LomoTimer: %s,count-%d (fps) Id: %d] too fast! : %f\n",
                mInfo, count , mIdx, (double)1000/lddiff*1000);
        } else {
            ALOGD("[LomoTimer: %s,count-%d (fps) Id: %d] non-zero: %f\n",
                mInfo, count , mIdx, (double)1000/lddiff*1000);
        }
    }
    ~LomoTimer() {
    }

protected:
    const char* mInfo;
    MINT32 mIdx;
    MBOOL mEnable;
    MINT32 mStartTime;
};

#ifdef MTK_CAM_LOMO_SUPPORT
    #include <vendor/mediatek/hardware/camera/lomoeffect/1.0/ILomoEffect.h>
    using namespace android;
    using vendor::mediatek::hardware::camera::lomoeffect::V1_0::ILomoEffect;

    sp<ILomoEffect> lomo_effect=NULL;
    static MBOOL bAddrDumpEnable = MFALSE;
    static MBOOL bFpsDumpEnable = MFALSE;
#endif

#define RECIPIENT_COOKIE 1481
class MyDeathRecipient : public android::hardware::hidl_death_recipient {
    virtual void serviceDied(uint64_t cookie,
                                 const android::wp<::android::hidl::base::V1_0::IBase>& who) {
       // Deal with the fact that the service died
       ALOGE("[MyDeathRecipient-%s] serviceDied, cookie = %l, who = %p, lomo_effect = %p",
              __func__, cookie, &who, &lomo_effect);
       android::CallStack cs(LOG_TAG);
    }
};
sp<android::hardware::hidl_death_recipient> recipient = new MyDeathRecipient();

using ::android::sp;
using ::android::hardware::camera::common::V1_0::Status;
using ::android::hardware::hidl_handle;
using ::android::hardware::Return;
using namespace android;

jclass mLomoEffectClass;
jobject mLomoEffectWeakObject;
jmethodID mPostEventFromNativeMethodId;

#define DEBUG_LOMO_DUMPSRC_ENABLED "debug.lomo.dumpsrc.enabled"
#define DEBUG_LOMO_DUMPSRC_COUNT "debug.lomo.dumpsrc.yv12.count"
#define DEBUG_LOMO_LOG_ENABLED "debug.lomo.log.enabled"
int g_dumpSrcEnabled = ::property_get_int32(DEBUG_LOMO_DUMPSRC_ENABLED, 0);
int g_dumpSrcImageMaxCount = ::property_get_int32(DEBUG_LOMO_DUMPSRC_COUNT, 0);
int g_dumpSrcImageCurrCount = 0;
int g_logEnabled = ::property_get_int32(DEBUG_LOMO_LOG_ENABLED, 0);

int g_previewWidth;
int g_previewHeight;
int g_bufferFormat;
int g_bufferWidth;
int g_bufferHeight;

int g_previewBufferSize;
int g_effectBufferSize;
int g_bufferCurson = 0;
int *g_currentEffectIds;
int g_effectNumOfPage;
int g_numOfBufferBlock;

int g_displayIndex = 0;
bool g_isReayToRun = false;
bool g_isFirstFrameDisplayed = false;

Condition g_condition;
Mutex g_mutex;

struct timeval tv, tv2;
unsigned long long g_display_startTime, g_display_endTime;
int g_displayFrame = 0;
unsigned long long g_input_startTime, g_input_endTime;
int g_inputFrame = 0;

unsigned char* u32lEffectBuffARRAY[12][3];//uint32_t u32lEffectBuffARRAY[12][3];
uint32_t u32lEffectIdx[12];
uint32_t u32lEffectIdxCount;

struct YUVData {
    // image buffer size to process
    int width;
    int height;
    int size;
    int ysize;
    int uvsize;
    YUVData() {
        width  = -1;
        height = -1;
        size   = -1;
        ysize  = -1;
        uvsize = -1;
    }
} gYUVData;

struct IonInfo {
    int mIonDevFd;;
    int32_t mIonFD;
    int mSize;
    void *mVirtualAddr;
    ion_user_handle_t mIonAllocHandle;

    native_handle_t *pNativeHandle;
    hidl_handle hidlIonShareFd;

    IonInfo() {
        mIonDevFd = -1; // this one is ionDriFD, created somewhere else
        mIonFD = -1;
        mSize = 0;
        mVirtualAddr = NULL;
        mIonAllocHandle = -1;

        pNativeHandle = nullptr;
        hidlIonShareFd = nullptr;
        if (g_logEnabled != 0) {
            ALOGD("[IonInfo-%s] Info constructor done!", __func__);
        }
    }

    ~IonInfo() {
        if (pNativeHandle != nullptr) {
            native_handle_close(pNativeHandle);
            native_handle_delete(pNativeHandle);
            pNativeHandle = NULL;
        }
        hidlIonShareFd = nullptr;

        if (mVirtualAddr != NULL) {
            munmap(mVirtualAddr, mSize);
        }
        if (mIonFD != -1) {
            close(mIonFD);
            mIonFD = -1;
        }

        if (mIonAllocHandle != -1) {
            ion_free(mIonDevFd, mIonAllocHandle);
            mIonAllocHandle = -1;
        }
        mIonDevFd = -1; // delete somewhere else

        hidlIonShareFd = nullptr;
        if (g_logEnabled != 0) {
            ALOGD("[IonInfo-%s] Info destructor done!", __func__);
        }
    }
};

struct EffectBuffer {
    void *m_bffer;
    bool m_dirty;
    int m_effectId;
    int m_id;

    IonInfo *m_pIonInfo;

    EffectBuffer() {
        m_pIonInfo = NULL;
    }

    ~EffectBuffer() {
        // pNativeHandle & pHidlIonShareFd should be destryoed in IonInfo
        if (g_logEnabled != 0) {
            ALOGD("[%s] m_pIonInfo = %p", __func__, m_pIonInfo);
        }
        m_pIonInfo = NULL;
    }
};
Vector<EffectBuffer *> g_EffectBuffers;

struct NativeWindow {
    ANativeWindow *m_nativeWindow;
    bool m_isReady;
};
Vector<NativeWindow *> g_NativeWindows;
Vector<IonInfo *> g_IonInfos;
int32_t g_IonDriFD = -1;

IonInfo* g_LomoSrcImageIonInfo = NULL;

/**
 * callback to AP framework after effect processed by algorithm
 */
void postEventToSuper(int msg) {
    ALOGD("[%s] +", __func__);
    JNIEnv *env = AndroidRuntime::getJNIEnv();
    env->CallStaticVoidMethod(
        mLomoEffectClass, mPostEventFromNativeMethodId, mLomoEffectWeakObject, msg);
    ALOGD("[%s] -", __func__);
}

void dumpBuffer(unsigned char* buffer, int width, int height, int bufferSize, char* fileNamePrefix) {
    if (g_dumpSrcEnabled == 0) {
        return;
    }

    char path[128] = "/sdcard/matrixEffect";
    int result = mkdir(path, S_IRWXU | S_IRWXG | S_IRWXO);
    if ((result == -1) && (errno != EEXIST)) {
        ALOGD("[%s] mkdir fail, error %d, return", __func__, errno);
        return;
    }

    if (g_dumpSrcImageCurrCount < g_dumpSrcImageMaxCount) {
        char fileName[128];
        sprintf(fileName, "%s/matrixEffect_%.4d_%s_w%d_h%d_s%d.yv12", path,
            ++g_dumpSrcImageCurrCount, fileNamePrefix, width, height, width);
        FILE *fp = fopen(fileName, "wb");
        if (NULL == fp) {
            ALOGE("[%s] fail to open file: %s", __func__, fileName);
        } else {
            int total_write = 0;
            while(total_write < bufferSize) {
                int write_size = fwrite(buffer+total_write, 1, bufferSize-total_write, fp);
                if (write_size <= 0) {
                    ALOGE("[%s] write_size=%d, fileName=%s", __func__, write_size, fileName);
                }
                total_write += write_size;
            }
            ALOGE("[%s] total_write: %d, fileName=%s", __func__, total_write, fileName);
            fclose(fp);
        }
    }
}

void displayEffectEx(uint8_t *data, int surfaceNumber) {
    if (g_logEnabled != 0) {
        ALOGD("[%s] + surfaceNumber = %d", __func__, surfaceNumber);
    }
    ANativeWindowBuffer *nativeWindowBuffer  = NULL;
    sp<GraphicBuffer> graphicbuffer  = NULL;
    void *ptr  = NULL;
    uint8_t *raw_data = data;

    int err = NO_ERROR;

    NativeWindow *nativeWindow = g_NativeWindows.itemAt(surfaceNumber);
    if (nativeWindow->m_isReady == false) {
        ALOGE("[%s] native window, not ready, return", __func__);
        return;
    }

    ANativeWindow *aNativeWindow = nativeWindow->m_nativeWindow;
    err = aNativeWindow->dequeueBuffer_DEPRECATED(aNativeWindow, &nativeWindowBuffer);
    if (NO_ERROR != err) {
        ALOGE("[%s] dequeue failed (after): %s, return", __func__, strerror(-err));
        return;
    }

    graphicbuffer = GraphicBuffer::from(nativeWindowBuffer);
    err = graphicbuffer->lock(GRALLOC_USAGE_SW_WRITE_OFTEN, &ptr);
    if (NO_ERROR != err) {
        graphicbuffer->unlock();
        ALOGE("[%s] lock buffer failed (after): %s, return", __func__, strerror(-err));
        return;
    }
    YUVData &yuv_data = gYUVData;
    memcpy(ptr, raw_data, yuv_data.size);

    graphicbuffer->unlock();
    aNativeWindow->queueBuffer_DEPRECATED(aNativeWindow, nativeWindowBuffer);
    if (!g_isFirstFrameDisplayed) {
        postEventToSuper(100);
        g_isFirstFrameDisplayed = true;
    }
    if (g_logEnabled) {
        ALOGD("[%s] -", __func__);
    }
}

void writeEffectDataToBuffers(int size, jint *ids) {
    if (g_logEnabled) {
        ALOGD("[%s] +", __func__);
    }
    JNIEnv *env = AndroidRuntime::getJNIEnv();
    if (env == NULL) {
        ALOGE("[%s] env == NULL, return", __func__);
        return;
    }
    int start = g_bufferCurson * g_effectNumOfPage;

    if (g_logEnabled) {
        ALOGD("[%s] g_bufferCurson=%d, g_effectNumOfPage=%d",
            __func__, g_bufferCurson, g_effectNumOfPage);
    }

    EffectBuffer *top_buffer = g_EffectBuffers.itemAt(start);
    static int FirstTimeCheck = 1;

    while (top_buffer->m_dirty == false) {
        usleep(5*1000);
    }

    u32lEffectIdxCount = 0;
    #ifdef MTK_CAM_LOMO_SUPPORT
        LomoTimer ldequeTmr("lmjava do effects",start,1);
    #endif
    uint32_t arrayPlaneOffset[3];
    arrayPlaneOffset[0] = 0;
    arrayPlaneOffset[1] = (size*4/6);
    arrayPlaneOffset[2] = (size*5/6);
    for (int i = start; i < start + g_effectNumOfPage; i++) {
        EffectBuffer *buffer =  g_EffectBuffers.itemAt(i);
        buffer->m_effectId = ids[i - start];
        if (1) {
            unsigned char* dstBuffer = (unsigned char*)buffer->m_bffer;
            unsigned char* u8lEffectBuff[3];
            int effectId=buffer->m_effectId;
            u8lEffectBuff[0]=(unsigned char*)dstBuffer;
            u8lEffectBuff[1]=(unsigned char*)(dstBuffer+(size*4/6));
            u8lEffectBuff[2]=(unsigned char*)(dstBuffer+(size*5/6));
            if(effectId!=(-1))
            {
                u32lEffectIdxCount++;
                u32lEffectBuffARRAY[i - start][0] = u8lEffectBuff[0];
                u32lEffectIdx[i - start] = effectId;
                #ifdef MTK_CAM_LOMO_SUPPORT
                    if (buffer->m_pIonInfo->pNativeHandle== nullptr)
                    {
                        if (g_logEnabled) {
                            ALOGD("[%s] Number=%d --> the first time to allocate native_handle",
                                __func__, i);
                        }
                        buffer->m_pIonInfo->pNativeHandle = native_handle_create(1, 0);
                        buffer->m_pIonInfo->pNativeHandle->data[0] = buffer->m_id;
                        buffer->m_pIonInfo->hidlIonShareFd = buffer->m_pIonInfo->pNativeHandle;
                        Return<Status> ret = lomo_effect->allocLomoDstImage(
                            i, size, arrayPlaneOffset, buffer->m_pIonInfo->hidlIonShareFd);
                        if (!ret.isOk()) {
                            ALOGE("[%s] isOk return false when allocLomoDstImage, lomo_effect = %p",
                                  __func__, &lomo_effect);
                        }
                        ret = lomo_effect->lomoImageEnque(i, arrayPlaneOffset,effectId);
                        if (!ret.isOk()) {
                            ALOGE("[%s] isOk return false when lomoImageEnque, lomo_effect = %p",
                                  __func__, &lomo_effect);
                        }
                    } else {
                        if (g_logEnabled) {
                            ALOGD("[%s] Number=%d --> use the last assigned ion_share_id, hidl->nativeHandle=%p",
                                __func__, i, buffer->m_pIonInfo->pNativeHandle);
                        }
                        // TODO: nullptr --> mean to use the last assigned ion_share_id
                        Return<Status> ret = lomo_effect->allocLomoDstImage(i, size, arrayPlaneOffset, nullptr);
                        if (!ret.isOk()) {
                            ALOGE("[%s] isOk return false when allocLomoDstImage, lomo_effect = %p",
                                   __func__, &lomo_effect);
                        }
                        if (g_logEnabled) {
                            ALOGD("[%s] LomoImageEnque begin", __func__);
                        }
                        ret = lomo_effect->lomoImageEnque(i, arrayPlaneOffset,effectId);
                        if (!ret.isOk()) {
                            ALOGE("[%s] isOk return false when lomoImageEnque, lomo_effect = %p",
                                   __func__, &lomo_effect);
                        }
                        if (g_logEnabled) {
                            ALOGD("[%s] LomoImageEnque done", __func__);
                        }
                    }
                    #ifdef MTK_LOMO_1_by_1_DEQUE // define in Android.mk
                        Return<Status> ret = lomo_effect->lomoImageDeque(i, arrayPlaneOffset,effectId);
                        if (!ret.isOk()) {
                            ALOGE("[%s] isOk return false when lomoImageDeque, lomo_effect = %p",
                                   __func__, &lomo_effect);
                        }
                    #endif
                #endif
            } else {
                u32lEffectBuffARRAY[i - start][0] = u8lEffectBuff[0];
                u32lEffectIdx[i - start] = effectId;
            }
        }
        FirstTimeCheck = 0;
    }
    #ifdef MTK_CAM_LOMO_SUPPORT
        #ifndef MTK_LOMO_1_by_1_DEQUE // define in Android.mk
            for (int i = start; i < start + g_effectNumOfPage; i++)
            {
                if (u32lEffectIdx[i - start] != ((unsigned int)(-1) ))
                {
                    Return<Status> ret = lomo_effect->lomoImageDeque(i, arrayPlaneOffset,u32lEffectIdx[i - start]);
                    if (!ret.isOk()) {
                        ALOGE("[%s] isOk return false when lomoImageDeque, lomo_effect = %p",
                               __func__, &lomo_effect);
                    }
                }
            }
            #elif 0
            for(unsigned int  i = 0; i < u32lEffectIdxCount; i++)
            {
                Return<Status> ret = lomo_effect->doPostProcessEffectToDstImage(
                    u32lEffectBuffARRAY[i], u32lEffectIdx[i]);
                if (!ret.isOk()) {
                    ALOGE("[%s] isOk return false when doPostProcessEffectToDstImage, lomo_effect = %p",
                           __func__, &lomo_effect);
                }
            }
            #else
        #endif
        if(bFpsDumpEnable)
        {
            ldequeTmr.printTime();
            ldequeTmr.fps(u32lEffectIdxCount);
        }
    #endif
    top_buffer->m_dirty = false;
    g_bufferCurson = (g_bufferCurson + 1) % g_numOfBufferBlock;

    if (g_inputFrame == 0) {
        gettimeofday(&tv,NULL);
        g_input_startTime = tv.tv_sec * 1000000 + tv.tv_usec;
    }
    g_inputFrame ++ ;
    if (g_inputFrame % 20 == 0) {
        gettimeofday(&tv2,NULL);
        g_input_endTime = tv2.tv_sec * 1000000 + tv2.tv_usec;
        ALOGD("[%s] inputFps = %llu\n",
            __func__, 20 * 1000000 / (g_input_endTime - g_input_startTime));
        g_input_startTime = g_input_endTime;
    }
    if (g_logEnabled) {
        ALOGD("[%s] -", __func__);
    }
}

class DisplayThread : public Thread {
public:
    DisplayThread(uint32_t id) {
        ALOGD("[DisplayThread-%s] +", __func__);
        mName = "[LomoEffect] DisplayThread";
        mIsRunning = false;
        mId = id;
        ALOGD("[DisplayThread-%s] -", __func__);
    }

    ~DisplayThread() {
        ALOGD("[DisplayThread-%s]", __func__);
    }

    virtual void requestExit() {
         mIsRunning = false;
    }

    void start() {
        if (g_logEnabled) {
            ALOGD("[DisplayThread-%s]", __func__);
        }
        run(mName.string(), PRIORITY_URGENT_DISPLAY, 0);
    }

private:
    String8 mName;
    bool mIsRunning;
    uint32_t mId;

    virtual void onFirstRef() {
        ALOGD("[DisplayThread-%s]", __func__);
        mIsRunning = true;
    }

    virtual status_t readyToRun() {
        return NO_ERROR;
    }

    virtual bool threadLoop() {
        if (g_logEnabled) {
            ALOGD("[DisplayThread-%s] +", __func__);
        }
        #ifdef MTK_CAM_LOMO_SUPPORT
            LomoTimer ldispLoopTmr("lmjava dispLoop", g_displayIndex*g_effectNumOfPage, 1);
        #endif
        g_mutex.lock();
        if (false == mIsRunning) {
            ALOGD("[DisplayThread-%s] thread is going to stop", __func__);
            g_condition.broadcast();
            g_mutex.unlock();
            ALOGD("[DisplayThread-%s] exit threadLoop, return false", __func__);
            return false;
        }
        g_mutex.unlock();

        JNIEnv *env = AndroidRuntime::getJNIEnv();
        int step = g_effectNumOfPage;
        int startPoint = g_displayIndex * step;
        EffectBuffer *top_buffer = g_EffectBuffers.itemAt(startPoint);

        while (top_buffer->m_dirty == true && mIsRunning == true) {
            usleep(3 * 1000);
        }
        for (int i = startPoint; i < startPoint + step; i++) {
            if (false == mIsRunning) {
                break;
            }
            EffectBuffer *buffer = g_EffectBuffers.itemAt(i);
            if (*(g_currentEffectIds + (i % step)) == buffer->m_effectId
                    && buffer->m_effectId >= 0) {
                char prefix[128];
                sprintf(prefix, "effectBuffer%d", buffer->m_effectId);
                dumpBuffer((unsigned char*)buffer->m_bffer,
                    g_bufferWidth, g_bufferHeight, g_effectBufferSize, prefix);
                displayEffectEx((uint8_t*)buffer->m_bffer, i % step);  // take buffer display out
            }
        }
        #ifdef MTK_CAM_LOMO_SUPPORT
            if(bFpsDumpEnable) {
                ldispLoopTmr.fps(step);
            }
            if (g_logEnabled) {
                ldispLoopTmr.printTime();
            }
        #endif
        top_buffer->m_dirty = true;
        g_displayIndex = ( g_displayIndex + 1 ) % g_numOfBufferBlock;

        if (g_displayFrame == 0) {
            gettimeofday(&tv,NULL);
            g_display_startTime = tv.tv_sec * 1000000 + tv.tv_usec;
        }
        g_displayFrame ++;
        if (g_displayFrame % 20 == 0) {
            gettimeofday(&tv2,NULL);
            g_display_endTime = tv2.tv_sec * 1000000 + tv2.tv_usec;
            ALOGD("[DisplayThread-%s] displayFps = %llu\n",
                __func__, 20 * 1000000 / (g_display_endTime - g_display_startTime));
            g_display_startTime = g_display_endTime;
        }
        if (g_logEnabled) {
            ALOGD("[DisplayThread-%s] -", __func__);
        }
        return true;
    }
};
sp<DisplayThread> g_displayThread;

void SetupWindow(ANativeWindow *w) {
    if (g_logEnabled != 0) {
        ALOGD("[%s] +", __func__);
    }
    YUVData &np = gYUVData;
    native_window_api_connect(w, NATIVE_WINDOW_API_MEDIA);
    native_window_set_buffers_dimensions(w, np.width, np.height);
    native_window_set_buffers_format(w, HAL_PIXEL_FORMAT_YV12);
    native_window_set_usage(w, GRALLOC_USAGE_SW_WRITE_OFTEN | GRALLOC_USAGE_HW_TEXTURE);
    native_window_set_scaling_mode(w, NATIVE_WINDOW_SCALING_MODE_SCALE_TO_WINDOW);
    native_window_set_buffer_count(w, 5);
    if (g_logEnabled != 0) {
        ALOGD("[%s] -", __func__);
    }
}

void setup(JNIEnv *env, jobject /**thiz**/, jobject weak_this) {
    ALOGD("[%s] +", __func__);
    mLomoEffectWeakObject = env->NewGlobalRef(weak_this);
    jclass clazz = env->FindClass("com/mediatek/matrixeffect/MatrixEffect");
    mLomoEffectClass = (jclass)env->NewGlobalRef(clazz);
    mPostEventFromNativeMethodId = env->GetStaticMethodID(
        clazz, "postEventFromNative", "(Ljava/lang/Object;I)V");
    ALOGD("[%s] -", __func__);
}

void setSurfaceToNative(JNIEnv *env, jobject /**thiz**/, jobject jsurface, jint surfaceNumber) {
    if (g_logEnabled != 0) {
        ALOGD("[%s] +", __func__);
    }
    sp<Surface> surface = NULL;
    if (jsurface != NULL) {
        if (g_logEnabled) {
            ALOGD("[%s] begin convert jaftersurface to native, surfaceNumber:%d",
                __func__, surfaceNumber);
        }
        surface = android_view_Surface_getSurface(env, jsurface);
        if (surface == NULL) {
            ALOGE("[%s] surface is null!", __func__);
        }
        NativeWindow *nativeWindow = g_NativeWindows.itemAt(surfaceNumber);
        nativeWindow->m_nativeWindow = surface.get();

        YUVData &yuv_data = gYUVData;
        yuv_data.width      = g_bufferWidth;
        yuv_data.height     = g_bufferHeight;
        yuv_data.size       = yuv_data.width * yuv_data.height * 3 / 2;
        yuv_data.ysize      = yuv_data.width * yuv_data.height;
        yuv_data.uvsize     = yuv_data.width * yuv_data.height / 4;
        SetupWindow(nativeWindow->m_nativeWindow);
        nativeWindow->m_isReady = true;
        if (g_logEnabled) {
            ALOGD("[%s] end convert jaftersurface to native", __func__);
        }
    }
    if (g_logEnabled != 0) {
        ALOGD("[%s] -", __func__);
    }
}

void displayEffect(
    JNIEnv */**env**/, jobject /**thiz**/, jbyteArray /**yuvData**/, jint /**surfaceNumber**/) {
}

void initializeEffect(JNIEnv */**env**/, jclass /**thiz**/, jint previewWidth, jint previewHeight,
        jint effectNumOfPage, jint format) {
    ALOGD("[%s] +", __func__);
    // output: preview size, preview buffer format
    g_previewWidth = previewWidth;
    g_previewHeight = previewHeight;
    g_bufferFormat = format;
    g_previewBufferSize = (g_previewWidth * g_previewHeight * 3 /2);
    g_effectNumOfPage = effectNumOfPage;
    g_currentEffectIds = (int *)malloc(sizeof(int) * g_effectNumOfPage);
    for (int i = 0; i < g_effectNumOfPage; i++) {
        *(g_currentEffectIds + i) = i;
    }

    for (int i = 0; i < g_effectNumOfPage; i++) {
        NativeWindow *window = (NativeWindow*)malloc(sizeof(NativeWindow));
        window->m_nativeWindow = NULL;
        window->m_isReady = false;
        g_NativeWindows.push(window);
    }
    g_displayIndex = 0;
    g_displayFrame = 0;
    g_inputFrame = 0;
    g_bufferCurson = 0 ;
    u32lEffectIdxCount = 0;
    g_isFirstFrameDisplayed = false;
    g_displayThread = new DisplayThread(0);

    #ifdef MTK_CAM_LOMO_SUPPORT
        lomo_effect = ILomoEffect::getService("internal/0");
        ALOGI("[%s] lomo_effect = %p", __func__, &lomo_effect);
        lomo_effect->linkToDeath(recipient, RECIPIENT_COOKIE);
        Return<Status> ret = lomo_effect->init();
        if (!ret.isOk()) {
            ALOGE("[%s] isOk return false when init, lomo_effect = %p",
                   __func__, &lomo_effect);
        }
        char valueAddrDump[PROPERTY_VALUE_MAX] = {'\0'};
        property_get("lomoeffect.jni.addrdump", valueAddrDump, "0");
        bAddrDumpEnable = atoi(valueAddrDump);
        char valueFpsDump[PROPERTY_VALUE_MAX] = {'\0'};
        property_get("lomoeffect.jni.fpsdump", valueFpsDump, "0");
        bFpsDumpEnable = atoi(valueFpsDump);
    #endif
    ALOGD("[%s] -", __func__);
}

IonInfo* allocateIon(int length, int32_t ionDriFD) {
    if (g_logEnabled != 0) {
        ALOGD("[%s] +", __func__);
    }
    int ion_handle = ionDriFD;
    ion_user_handle_t ion_allocHandle;
    int32_t ionFD;
    int alloc_ret = ion_alloc(ion_handle, length, 1, ION_HEAP_MULTIMEDIA_MASK, 0, &ion_allocHandle);
    if (alloc_ret) {
        ALOGE("[%s] Ion allocate failed, alloc_ret:%d, return", __func__, alloc_ret);
    }
    int share_ret = ion_share(ion_handle, ion_allocHandle, &ionFD);
    if (share_ret) {
        ALOGE("[%s] Ion share failed, share_ret:%d, return", __func__, share_ret);
    }
    void *virtualAddr = mmap(0, length, PROT_READ | PROT_WRITE, MAP_SHARED, ionFD, 0);
    if (virtualAddr == MAP_FAILED) {
        ALOGE("[%s] mmap failed fd = %d, addr = 0x%p, len = %zu, prot = %d, flags = %d, share_fd = %d, 0x%p: %s\n",
            __func__, ion_handle, (void *)0, (size_t)length, PROT_READ | PROT_WRITE, MAP_SHARED,
            ionFD, (void *)0, strerror(errno));
    }
    IonInfo *info = new IonInfo();
    info->mIonDevFd= ion_handle;
    info->mIonAllocHandle = ion_allocHandle;
    info->mIonFD = ionFD;
    info->mVirtualAddr = virtualAddr;
    info->mSize = length;
    if (g_logEnabled != 0) {
        ALOGD("[%s] new IonInfo and init", __func__);
    }
    info->pNativeHandle = nullptr;
    if (g_logEnabled != 0) {
        ALOGD("[%s] info->hidlIonShareFd.mHandle=%p", __func__, info->hidlIonShareFd.getNativeHandle());
    }
    info->hidlIonShareFd = nullptr;

    if (g_logEnabled != 0) {
        ALOGD("[%s] -", __func__);
    }
    return info;
}

void destroyIon(IonInfo * info) {
    if (g_logEnabled != 0) {
        ALOGD("[%s] +", __func__);
    }
    delete info;
    if (g_logEnabled != 0) {
        ALOGD("[%s] -", __func__);
    }
}

void registerEffectBuffers(
    JNIEnv *env, jclass /**thiz**/, jint bufferWidth, jint bufferHeight, jobjectArray buffers) {
    ALOGD("[%s] +", __func__);
    g_bufferWidth = bufferWidth;
    g_bufferHeight = bufferHeight;
    g_effectBufferSize = g_bufferWidth * g_bufferHeight * 3 / 2;
    ALOGD("[%s] g_bufferWidth,g_bufferHeight %d,%d",__func__, g_bufferWidth,g_bufferHeight);
    int row = env->GetArrayLength(buffers);
    ALOGD("[%s] g_bufferWidth,g_bufferHeight %d,%d, row: %d, g_effectNumOfPage: %d",
        __func__, g_bufferWidth,g_bufferHeight, row, g_effectNumOfPage);
    g_IonDriFD = ion_open();
    if (g_IonDriFD < 0) {
        ALOGE("[%s] Open ion driver failed, return", __func__);
        return;
    }
    for (int i = 0; i < row; i++) {
        jbyteArray bufferArray = (jbyteArray)env->GetObjectArrayElement(buffers, i);
        IonInfo *ionInfo = allocateIon(g_effectBufferSize, g_IonDriFD);
        g_IonInfos.push(ionInfo);

        EffectBuffer *buffer = new EffectBuffer();
        buffer->m_bffer = ionInfo->mVirtualAddr;
        buffer->m_dirty = true;
        buffer->m_effectId = -1;
        buffer->m_id = ionInfo->mIonFD;
        buffer->m_pIonInfo = ionInfo;
        buffer->m_pIonInfo->pNativeHandle = nullptr;
        buffer->m_pIonInfo->hidlIonShareFd = nullptr;

        g_EffectBuffers.push(buffer);
    }
    g_numOfBufferBlock = row / g_effectNumOfPage;

    #ifdef MTK_CAM_LOMO_SUPPORT
        Return<Status> ret = lomo_effect->allocLomoSrcImage(
            g_previewWidth, g_previewHeight, g_bufferFormat , g_bufferWidth, g_bufferHeight);
        if (!ret.isOk()) {
            ALOGE("[%s] isOk return false when allocLomoSrcImage, lomo_effect = %p",
                   __func__, &lomo_effect);
        }
    #endif
    ALOGD("[%s] -", __func__);
}

void processEffect(JNIEnv *env, jclass /**thiz**/, jbyteArray data, jintArray ids) {
    if (g_logEnabled) {
        ALOGD("[%s] +", __func__);
    }
    jbyte* previewBuffer = (jbyte*)env->GetByteArrayElements(data, NULL);
    jint* effectIds = (jint*)env->GetIntArrayElements(ids, NULL);
    int size = env->GetArrayLength(ids);

    uint32_t arrayPlaneOffset[3];
    arrayPlaneOffset[0] = 0;
    arrayPlaneOffset[1]= (g_previewBufferSize*4/6);
    arrayPlaneOffset[2]= (g_previewBufferSize*5/6);

    #ifdef MTK_CAM_LOMO_SUPPORT
        if (g_LomoSrcImageIonInfo == NULL) {
            g_LomoSrcImageIonInfo = allocateIon(g_previewBufferSize, g_IonDriFD);
        }
        if (g_logEnabled) {
            ALOGD("[%s] hidl->nativeHandle=%p, srdc_native_handle=%p",
                __func__, g_LomoSrcImageIonInfo->hidlIonShareFd.getNativeHandle(),
                g_LomoSrcImageIonInfo->pNativeHandle);
        }
        memcpy(g_LomoSrcImageIonInfo->mVirtualAddr, previewBuffer, g_previewBufferSize);

        char prefix[128] = "srcImg";
        dumpBuffer((unsigned char*)g_LomoSrcImageIonInfo->mVirtualAddr,
            g_previewWidth, g_previewHeight, g_previewBufferSize, prefix);

        if (g_LomoSrcImageIonInfo->pNativeHandle== nullptr) {
            if (g_logEnabled) {
                ALOGD("[%s] SrcHandle --> the first time to allocate", __func__);
            }
            g_LomoSrcImageIonInfo->pNativeHandle = native_handle_create(1, 0);
            g_LomoSrcImageIonInfo->pNativeHandle->data[0] = g_LomoSrcImageIonInfo->mIonFD;
            g_LomoSrcImageIonInfo->hidlIonShareFd = g_LomoSrcImageIonInfo->pNativeHandle;
            Return<Status> ret = lomo_effect->uploadLomoSrcImage(
                g_LomoSrcImageIonInfo->hidlIonShareFd, arrayPlaneOffset);
            if (!ret.isOk()) {
                ALOGE("[%s] isOk return false when uploadLomoSrcImage, lomo_effect = %p",
                       __func__, &lomo_effect);
            }
        } else {
            if (g_logEnabled) {
                ALOGD("[%s] SrcHandle --> use the last assigned ion_share_fd: %d",
                    __func__, g_LomoSrcImageIonInfo->pNativeHandle->data[0]);
            }
            Return<Status> ret = lomo_effect->uploadLomoSrcImage(nullptr, arrayPlaneOffset);
            if (!ret.isOk()) {
                ALOGE("[%s] isOk return false when uploadLomoSrcImage, lomo_effect = %p",
                      __func__, &lomo_effect);
            }
        }
    #endif

    for (int i = 0; i < size; i++) {
        int effectId = effectIds[i];
        *(g_currentEffectIds + i) = effectId;
    }

    writeEffectDataToBuffers(g_effectBufferSize, effectIds);
    env->ReleaseByteArrayElements(data, previewBuffer, JNI_ABORT);
    env->ReleaseIntArrayElements(ids, effectIds, JNI_ABORT);
    g_displayThread->start();
    if (g_logEnabled) {
        ALOGD("[%s] -", __func__);
    }
}

// release resource
void releaseEffect(JNIEnv */**env**/, jclass /**thiz**/) {
    ALOGD("[%s] +", __func__);

    g_mutex.lock();
    g_displayThread->requestExit();
    if (g_displayThread->isRunning()) {
        g_condition.wait(g_mutex);
    }
    g_mutex.unlock();

    while (!g_EffectBuffers.isEmpty()) {
        EffectBuffer *buffer = g_EffectBuffers.top();
        g_EffectBuffers.pop();
        delete buffer;
    }

    while (!g_NativeWindows.isEmpty()) {
        NativeWindow *window = g_NativeWindows.top();
        if (window->m_nativeWindow) {
            native_window_api_disconnect(window->m_nativeWindow, NATIVE_WINDOW_API_MEDIA);
        }
        g_NativeWindows.pop();
        free(window);
    }

    if (g_LomoSrcImageIonInfo != NULL) {
        destroyIon(g_LomoSrcImageIonInfo);
        g_LomoSrcImageIonInfo = NULL;
    }

    while (!g_IonInfos.isEmpty()) {
        IonInfo *info = g_IonInfos.top();
        destroyIon(info);
        info = NULL;
        g_IonInfos.pop();
    }

    ion_close(g_IonDriFD);
    ALOGD("[%s] Close ion driver, driver FD:%d", __func__, g_IonDriFD);
    g_IonDriFD = -1;

    free(g_currentEffectIds);
    #ifdef MTK_CAM_LOMO_SUPPORT
        ALOGI("[%s] lomo_effect = %p", __func__, &lomo_effect);
        Return<Status> ret = lomo_effect->freeLomoSrcImage();
        if (!ret.isOk()) {
            ALOGE("[%s] isOk return false when freeLomoSrcImage, lomo_effect = %p",
                   __func__, &lomo_effect);
        }
        ret = lomo_effect->freeLomoDstImage();
        if (!ret.isOk()) {
            ALOGE("[%s] isOk return false when freeLomoDstImage, lomo_effect = %p",
                   __func__, &lomo_effect);
        }
        ret = lomo_effect->uninit();
        if (!ret.isOk()) {
            ALOGE("[%s] isOk return false when uninit, lomo_effect = %p",
                   __func__, &lomo_effect);
        }
        lomo_effect->unlinkToDeath(recipient);
        lomo_effect = NULL;
    #endif
    ALOGD("[%s] -", __func__);
}

const char *classPathName = "com/mediatek/matrixeffect/MatrixEffect";

JNINativeMethod methods[] = {
    {"native_setup", "(Ljava/lang/Object;)V", (void*)setup},
    {"native_setSurfaceToNative", "(Landroid/view/Surface;I)V", (void*)setSurfaceToNative },
    {"native_displayEffect", "([BI)V",(void*)displayEffect},
    {"native_initializeEffect", "(IIII)V", (void*)initializeEffect},
    {"native_registerEffectBuffers", "(II[[B)V",(void*)registerEffectBuffers},
    {"native_processEffect", "([B[I)V", (void*)processEffect},
    {"native_releaseEffect", "()V",(void*)releaseEffect},
};

/*
 * Register several native methods for one class.
 */
static int registerNativeMethods(JNIEnv* env, const char* className,
    JNINativeMethod* gMethods, int numMethods) {
    ALOGD("[%s] +", __func__);
    jclass clazz;
    clazz = env->FindClass(className);
    if (clazz == NULL) {
        ALOGE("[%s] Native registration unable to find class '%s', return JNI_FALSE",
            __func__, className);
        return JNI_FALSE;
    }
    if (env->RegisterNatives(clazz, gMethods, numMethods) < 0) {
        ALOGE("[%s] RegisterNatives failed for '%s', return JNI_FALSE", __func__, className);
        return JNI_FALSE;
    }
    ALOGD("[%s] return JNI_TRUE", __func__);
    return JNI_TRUE;
}

/*
 * Register native methods for all classes we know about.
 *
 * returns JNI_TRUE on success.
 */
static int registerNatives(JNIEnv* env) {
    ALOGD("[%s] +", __func__);
    if (!registerNativeMethods(env, classPathName,
        methods, sizeof(methods) / sizeof(methods[0]))) {
        ALOGE("[%s] return JNI_FALSE ", __func__);
        return JNI_FALSE;
    }
    ALOGD("[%s] return JNI_TRUE ", __func__);
    return JNI_TRUE;
}


// ----------------------------------------------------------------------------
/*
 * This is called by the VM when the shared library is first loaded.
 */
typedef union {
    JNIEnv* env;
    void* venv;
} UnionJNIEnvToVoid;

jint JNI_OnLoad(JavaVM* vm, void* /**reserved**/) {
    ALOGD("[%s] +", __func__);
    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    jint result = -1;
    JNIEnv* env = NULL;

    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_4) != JNI_OK) {
        ALOGE("[%s] ERROR: GetEnv failed", __func__);
        goto bail;
    }
    env = uenv.env;

    if (registerNatives(env) != JNI_TRUE) {
        ALOGE("[%s] ERROR: registerNatives failed", __func__);
        goto bail;
    }

    result = JNI_VERSION_1_4;

bail:
    ALOGD("[%s] -", __func__);
    return result;
}
