/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony;


import android.content.Intent;

public class MtkTelephonyIntents {

    /// M: eMBMS feature
    /**
     * Broadcast Action: The eMBMS sessions are changed.
     * @hide
     * The intent will have the following extra values:<p>
     * <ul>
     *   <li><em>EXTRA_IS_ACTIVE</em> - There are any active eMBMS sessions.</li>
     * </ul>
     */
    public static final String ACTION_EMBMS_SESSION_STATUS_CHANGED
            = "mediatek.intent.action.EMBMS_SESSION_STATUS_CHANGED";
    /** @hide */
    public static final String EXTRA_IS_ACTIVE  = "isActived";
    /// M: eMBMS end

    /**
     * This event is broadcasted when the located PLMN is changed
     * @internal
     */
    public static final String ACTION_LOCATED_PLMN_CHANGED
            = "mediatek.intent.action.LOCATED_PLMN_CHANGED";
    public static final String EXTRA_ISO = "iso";

    /**
     * M:
     * <p>Broadcast Action: The user has switched the mutiple SIM mode of phone. One or
     * more radios have been turned off or on. The intent will have the following extra value:</p>
     * <ul>
     *   <li><em>state</em> - A boolean value indicating whether Airplane Mode is on. If true,
     *   then cell radio and possibly other radios such as bluetooth or WiFi may have also been
     *   turned off</li>
     * </ul>
     *
     * <p class="note">This is a protected intent that can only be sent
     * by the system.
     * @hide
     * @internal
     */
    public static final String ACTION_MSIM_MODE_CHANGED = "android.intent.action.MSIM_MODE";

    /**
     * M:
     * A int associated with a {@link #ACTION_MSIM_MODE_CHANGED} activity
     * describing the latest mode. 1 for sim1 only, 2 for sim2 only, 3 for dual sim
     * @hide
     * @internal
     */
    public static final String EXTRA_MSIM_MODE = "mode";

    public static final String ACTION_IVSR_NOTIFY
            = "mediatek.intent.action.IVSR_NOTIFY";

    public static final String INTENT_KEY_IVSR_ACTION = "action";

    /**
    * Broadcast Action: Occurs when special network event has happened.
    * The intent will have the following extra values:</p>
    * <ul>
    *    <li><em>event type</em> - 1: for RAU event; 2: for TAU event;
    *                              3: reserved for future use. </li>
    * </ul>
    */
    public static final String ACTION_NETWORK_EVENT
            = "android.intent.action.ACTION_NETWORK_EVENT";
    public static final String EXTRA_EVENT_TYPE = "eventType";

    public static final String ACTION_NOTIFY_MODULATION_INFO =
            "mediatek.intent.action.ACTION_NOTIFY_MODULATION_INFO";
    public static final String EXTRA_MODULATION_INFO = "modulation_info";

    /**
     * Broadcast Action: The world mode changed.
     * The intent will have the following extra values:</p>
     * <ul>
     *   <li><em>worldModeState</em> - An int with one of the following values:
     *          {@link com.mediatek.internal.telephony.worldphone.WorldMode#MD_WM_CHANGED_START} or
     *          {@link com.mediatek.internal.telephony.worldphone.WorldMode#MD_WM_CHANGED_END}
     *   </li>
     * </ul>
     */
    public static final String ACTION_WORLD_MODE_CHANGED
            = "mediatek.intent.action.ACTION_WORLD_MODE_CHANGED";
    /**
     * Broadcast world mode change state.
     */
    public static final String EXTRA_WORLD_MODE_CHANGE_STATE = "worldModeState";

    /**
     * Broadcast Action: The radio state changed. The intent will have the following extra values:
     * <ul>
     *   <li><em>radioState</em> - An enum with one of the following values:
     *     {@link com.android.internal.telephony.CommandsInterface.RadioState.RADIO_OFF}
     *     {@link com.android.internal.telephony.CommandsInterface.RadioState.RADIO_UNAVAILABLE}
     *     {@link com.android.internal.telephony.CommandsInterface.RadioState.RADIO_ON}
     *   </li>
     *   <li><em>subId</em> - sub Id </li>
     * </ul>
     *
     * <p class="note">
     * Requires the READ_PHONE_STATE permission.
     *
     * <p class="note">This is a protected intent that can only be sent
     * by the system.
     */
    public static final String ACTION_RADIO_STATE_CHANGED =
            "com.mediatek.intent.action.RADIO_STATE_CHANGED";

    // Femtocell (CSG) START
    public static final String EXTRA_HNB_NAME  = "hnbName";
    public static final String EXTRA_CSG_ID    = "csgId";
    public static final String EXTRA_DOMAIN    = "domain";
    public static final String EXTRA_FEMTO     = "femtocell";
    // Femtocell (CSG) END

    /**
     * Broadcast Action: The modem type changed.
     * @internal
     * The intent will have the following extra values:</p>
     * <ul>
     *   <li><em>ready</em> - The modem type after switched.</li>
     * </ul>
     */
    public static final String ACTION_MD_TYPE_CHANGE
            = "mediatek.intent.action.ACTION_MD_TYPE_CHANGE";
    /** @internal */
    public static final String EXTRA_MD_TYPE = "mdType";

    // MTK-START: SIM
    // ALPS00302702 RAT balancing
    /**
     * This event is broadcasted when EF-RAT Mode is changed.
     * @internal
     */
    public static final String ACTION_EF_RAT_CONTENT_NOTIFY
            = "com.mediatek.phone.ACTION_EF_RAT_CONTENT_NOTIFY";

    // ALPS00302698 ENS
    /**
     * This event is broadcasted when CSP PLMN is changed
     * @internal
     */
    public static final String ACTION_EF_CSP_CONTENT_NOTIFY =
            "com.mediatek.phone.ACTION_EF_CSP_CONTENT_NOTIFY";
    public static final String EXTRA_PLMN_MODE_BIT = "plmn_mode_bit";
    // MTK-END
    /**
     * Broadcast Action: The PHB state has changed.
     * The intent will have the following extra values:</p>
     * <ul>
     *   <li><em>ready</em> - The PHB ready state.  True for ready, false for not ready</li>
     *   <li><em>simId</em> - The SIM ID</li>
     * </ul>
     *
     * <p class="note">
     * Requires the READ_PHONE_STATE permission.
     *
     * <p class="note">This is a protected intent that can only be sent
     * by the system.
     * @internal
     */
     public static final String ACTION_PHB_STATE_CHANGED
            = "mediatek.intent.action.PHB_STATE_CHANGED";

    // MTK-START: CMCC DUAL SIM DEPENDENCY LOCK
    /**
     * <p>Broadcast Action: To activate an application to unlock SIM lock.
     * The intent will have the following extra value:</p>
     * <dl>
     *   <dt>reason</dt><dd>The reason why ss is {@code LOCKED}; null otherwise.</dd>
     *   <dl>
     *       <dt>{@code PIN}</dt><dd>locked on PIN1</dd>
     *       <dt>{@code PUK}</dt><dd>locked on PUK1</dd>
     *       <dt>{@code NETWORK}</dt><dd>locked on network personalization</dd>
     *       <dt>{@code NETWORK_SUBSET}</dt><dd>locked on network subset personalization</dd>
     *       <dt>{@code CORPORATE}</dt><dd>locked on corporate personalization</dd>
     *       <dt>{@code SERVICE_PROVIDER}</dt><dd>locked on service proiver personalization</dd>
     *       <dt>{@code SIM}</dt><dd>locked on SIM personalization</dd>
     *   </dl>
     * </dl>
     * @internal
     */
     public static final String ACTION_UNLOCK_SIM_LOCK
            = "com.mediatek.phone.ACTION_UNLOCK_SIM_LOCK";
    // MTK-END

    // M: [LTE][Low Power][UL traffic shaping] @{
    /**
     * Broadcast Action: The LTE access stratum state has changed.
     * The intent will have the following extra values:</p>
     * <dl>
     *   <dt>phoneName</dt><dd>A string version of the phone name.</dd>
     *   <dt>state</dt><dd>One of {@code UNKNOWN}, {@code IDLE},
     *      or {@code CONNECTED}.</dd>
     * </dl>
     *
     * <p class="note">
     * Requires the READ_PHONE_STATE permission.
     *
     * <p class="note">This is a protected intent that can only be sent
     * by the system.
     */
    public static final String ACTION_LTE_ACCESS_STRATUM_STATE_CHANGED
            = "mediatek.intent.action.LTE_ACCESS_STRATUM_STATE_CHANGED";

    /**
     * Broadcast Action: The PS network type has changed for low power feature on.
     * The intent will have the following extra values:</p>
     * <dl>
     *   <dt>phoneName</dt><dd>A string version of the phone name.</dd>
     *   <dt>nwType</dt><dd>One of
     *          {@code NETWORK_TYPE_UNKNOWN},
     *          {@code NETWORK_TYPE_GPRS},
     *          {@code NETWORK_TYPE_EDGE},
     *          {@code NETWORK_TYPE_UMTS},
     *          {@code NETWORK_TYPE_HSDPA},
     *          {@code NETWORK_TYPE_HSUPA},
     *          {@code NETWORK_TYPE_HSPA} or
     *          {@code NETWORK_TYPE_LTE}.</dd>
     * </dl>
     *
     * <p class="note">
     * Requires the READ_PHONE_STATE permission.
     *
     * <p class="note">This is a protected intent that can only be sent
     * by the system.
     */
    public static final String ACTION_PS_NETWORK_TYPE_CHANGED
            = "mediatek.intent.action.PS_NETWORK_TYPE_CHANGED";

    /**
     * Broadcast Action: The shared default apn state has changed.
     * The intent will have the following extra values:</p>
     * <dl>
     *   <dt>phoneName</dt><dd>A string version of the phone name.</dd>
     *   <dt>state</dt><dd>One of {@code TRUE} or {@code FALSE}.</dd>
     * </dl>
     *
     * <p class="note">
     * Requires the READ_PHONE_STATE permission.
     *
     * <p class="note">This is a protected intent that can only be sent
     * by the system.
     */
    public static final String ACTION_SHARED_DEFAULT_APN_STATE_CHANGED
            = "mediatek.intent.action.SHARED_DEFAULT_APN_STATE_CHANGED";
    // M: [LTE][Low Power][UL traffic shaping] @}

    // MTK-START: SIM MISSING/RECOVER
    /**
    * Do SIM Recovery Done.
    */
    public static final String ACTION_SIM_RECOVERY_DONE =
            "com.mediatek.phone.ACTION_SIM_RECOVERY_DONE";
    // MTK-END
    // MTK-START: SIM COMMON SLOT
    public static final String ACTION_COMMON_SLOT_NO_CHANGED =
            "com.mediatek.phone.ACTION_COMMON_SLOT_NO_CHANGED";
    // MTK-END

    // M: [VzW] Data Framework
    public static final String ACTION_CARRIER_SIGNAL_PCO_VALUE_AFTER_ATTACHED
            = "mediatek.intent.action.CARRIER_SIGNAL_PCO_VALUE_AFTER_ATTACHED";

    public static final String EXTRA_DATA_APN_KEY = "apn";

    public static final String ACTION_APC_INFO_NOTIFY =
            "com.mediatek.phone.ACTION_APC_INFO_NOTIFY";
    public static final String EXTRA_APC_PHONE = "phoneId";
    public static final String EXTRA_APC_INFO = "info";

    // M: Ims Data Framework
    public static final String ACTION_ANY_DEDICATE_DATA_CONNECTION_STATE_CHANGED
            = "com.mediatek.phone.ACTION_ANY_DEDICATE_DATA_CONNECTION_STATE_CHANGED";

    ///M: MwiService @{
    public static final String ACTION_RSSI_THRESHOLD_CHANGED =
        "com.mediatek.phone.ACTION_RSSI_THRESHOLD_CHANGED";
    public static final String EXTRA_RSSI_THRESHOLDS = "rssiThresholds";

    public static final String ACTION_WIFI_PDN_ACTIVATED =
        "com.mediatek.phone.ACTION_WIFI_PDN_ACTIVATED";
    public static final String EXTRA_WIFI_PDN_ACTIVATED = "wifiPdn";

    public static final String ACTION_WFC_PDN_ERROR =
        "com.mediatek.phone.ACTION_WFC_PDN_ERROR";
    public static final String EXTRA_WFC_PDN_ERROR = "wfcPdnError";

    public static final String ACTION_PDN_HANDOVER =
        "com.mediatek.phone.ACTION_PDN_HANDOVER";
    public static final String EXTRA_PDN_HANDOVER = "pdnHandover";

    public static final String ACTION_PDN_ROVE_OUT =
        "com.mediatek.phone.ACTION_PDN_ROVE_OUT";
    public static final String EXTRA_ROVE_OUT = "roveOut";

    public static final String ACTION_REQUEST_GEO_LOCATION =
        "com.mediatek.phone.ACTION_REQUEST_GEO_LOCATION";
    public static final String EXTRA_LOCATION_REQ = "locationReq";

    public static final String ACTION_WFC_PDN_STATE_CHANGED =
        "com.mediatek.phone.ACTION_WFC_PDN_STATE_CHANGED";
    public static final String EXTRA_WFC_PDN_STATE = "wfcPdnState";
    ///@}
}
