package com.mediatek.op01.dialer.speeddial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


public class PermissionCheckActivity extends Activity {
    private static final String TAG = "PermissionCheckActivity";
    private static final int REQUIRED_PERMISSIONS_REQUEST_CODE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, " onCreate");
        final String[] missingArray
                = getIntent().getStringArrayExtra(PermissionUtils.MISSING_PERMISSIONS);
        requestPermissions(missingArray, REQUIRED_PERMISSIONS_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult enter");
        finish();

        if (requestCode == REQUIRED_PERMISSIONS_REQUEST_CODE) {
            if (grantResults == null || grantResults.length == 0) {
                return;
            }
            for (int i = 0; i < permissions.length; i++) {
                if (!PermissionUtils.hasPermission(this, permissions[i])) {
                    Log.d(TAG, "");
                    if (isNeverGrantedPermission(this, permissions[i])) {
                        showNoPermissionsToast(this);
                    }
                    Log.d(TAG, "Have no granted, so finish !!");
                    finish();
                    return;
                }
            }
       }

       Intent previousActivityIntent = (Intent) getIntent().getExtras().get(
                            PermissionUtils.PREVIOUS_ACTIVITY_INTENT);
       startActivity(previousActivityIntent);
    }

    public static boolean isNeverGrantedPermission(Activity activity, String permission) {
        return !activity.shouldShowRequestPermissionRationale(permission);
    }

    public static void showNoPermissionsToast(Activity context) {
            //Toast.makeText(context, com.mediatek.internal.R.string.denied_required_permission,
               //Toast.LENGTH_LONG).show();
    }
}

