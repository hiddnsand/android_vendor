/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.deviceregister.utils;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

import com.mediatek.common.dm.DmAgent;
import com.mediatek.deviceregister.Const;

public class AgentProxy {

    private static final String TAG = Const.TAG_PREFIX + "AgentProxy";

    private static AgentProxy sInstance;
    private DmAgent mAgent;

    private AgentProxy(DmAgent agent) {
        mAgent = agent;
    }

    public static AgentProxy getInstance() {
        if (sInstance == null) {

            IBinder binder = ServiceManager.getService("DmAgent");
            if (binder == null) {
                Log.e(TAG, "binder is null, please check");
                return null;
            }
            DmAgent agent = DmAgent.Stub.asInterface(binder);

            if (agent == null) {
                Log.e(TAG, "agent is null, please check");
                return null;
            }
            sInstance = new AgentProxy(agent);
        }
        return sInstance;
    }

    //------------------------------------------------------
    // Feature enable state
    //------------------------------------------------------

    public boolean isFeatureEnabled() {
        int result = 1;

        try {
            byte[] value = mAgent.getRegisterSwitch();

            if (value != null && value.length > 0) {
                result = Integer.parseInt(new String(value));
            }

        } catch (RemoteException | NumberFormatException e) {
            Log.e(TAG, "Remote/NumberFormatException " + e);
            e.printStackTrace();
        }

        return (result == 1);
    }

    //------------------------------------------------------
    // Register flag
    //------------------------------------------------------

    public void resetRegisterFlag() {
        setRegisterFlag(false);
    }

    public boolean isRegistered() {
        return getRegisterFlag();
    }

    public boolean setRegisterFlag(boolean flag) {
        Log.i(TAG, "setRegisterFlag " + flag);

        String value = flag ? "1" : "0";
        boolean result = false;

        try {
            byte[] bytes = value.getBytes();
            result = mAgent.setRegisterFlag(bytes, bytes.length);

        } catch (RemoteException e) {
            Log.e(TAG, "RemoteException " + e);
            e.printStackTrace();
        }

        return result;
    }

    private boolean getRegisterFlag() {
        int result = 0;

        try {
            byte[] value = mAgent.readRegisterFlag();
            if (value != null && value.length > 0) {
                result = Integer.parseInt(new String(value));
            }

        } catch (RemoteException | NumberFormatException e) {
            Log.e(TAG, "Remote/NumberFormat " + e);
            e.printStackTrace();
        }

        return result == 1;
    }

    //------------------------------------------------------
    // IMSI
    //------------------------------------------------------

    public String[] getSavedImsi(int slotNumber) {
        String[] imsiArray = new String[slotNumber];

        try {
            byte[] imsi1 = mAgent.readImsi1();
            if (imsi1 != null) {
                imsiArray[0] = new String(imsi1);
            }

            if (slotNumber > 1) {
                byte[] imsi2 = mAgent.readImsi2();
                if (imsi2 != null) {
                    imsiArray[1] = new String(imsi2);
                }
            }

        } catch (RemoteException e) {
            Log.e(TAG, "RemoteException " + e);
        }

        return imsiArray;
    }

    public Boolean setSavedImsi(String[] imsi) {
        Boolean result = false;

        try {
            byte[] bytes = imsi[0].getBytes();
            result = mAgent.writeImsi1(bytes, bytes.length);

            if (imsi.length > 1) {
                bytes = imsi[1].getBytes();
                result = result & mAgent.writeImsi2(bytes, bytes.length);
            }

        } catch (RemoteException | NullPointerException e) {
            Log.e(TAG, "Remote/NullPointerException " + e);
            e.printStackTrace();
        }

        return result;
    }
}
