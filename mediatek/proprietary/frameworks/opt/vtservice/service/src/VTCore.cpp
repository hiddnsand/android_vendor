/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
#include <sys/prctl.h>
#include "VTCore.h"
#include "VTCoreHelper.h"
#include "VTAVSync.h"

// for get user ID
#include <unistd.h>
#include <sys/types.h>

using namespace android;
using android::INVALID_OPERATION;
using android::ALREADY_EXISTS;

namespace VTService {

    vt_srv_cntx_struct g_vt;
    sp<VTCoreHelper> VTCore::mHelper = NULL;

    VTCore::VTCore(const sp<IVTServiceCallBack> & user) {
        mHelper = new VTCoreHelper();

        mHelper->logFlow("create", "", VT_IVD, "", VT_SRV_LOG_I);

        // init cntx
        mUser     = user;
        g_vt.core = this;

        if (mHelper->isVoLTEon()) {
            g_vt.avSync = new VTAVSync();
        }

        mHelper->init();

        mUserID   = getuid();
        mHelper->logFlow("create", "User ID is : ", mUserID, "", VT_SRV_LOG_I);

        // init call flow locks
        for (int i = 0; i < VT_SRV_MA_NR; i++) {
            mCallFlowLocks[i] = new Mutex();
        }
    }

    VTCore::~VTCore() {
        mUser     = NULL;
        mHelper   = NULL;
        g_vt.core = NULL;
        g_vt.avSync = NULL;

        mHelper->logFlow("~VTCore ", " error ~VTCore() here, abort to get backtrace", VT_IVD, "", VT_SRV_LOG_E);
        abort();
    }

    status_t VTCore::initialization(int mode, int call_id) {
        if (VTCoreHelper::isUT()) {
            // ================================================================
            // Step 0
            open((VT_SRV_CALL_MODE)mode, call_id);
            // ================================================================

            // ================================================================
            // Step 1
            // get Cap
            VT_IMCB_CAP *ua = &g_vt.ua_cap;
            media_config_t  ma;

            vt_rtp_codec_2_ua((VT_SRV_CALL_MODE)mode, ua, GET_SIM_ID(call_id), GET_SIM_ID(call_id));
            // ================================================================

            // ================================================================
            // Step 2
            // put the same config back
            vt_ut_ua_2_rtp((VT_SRV_CALL_MODE)mode, ua, &(ma.rtp_rtcp_cap));
            vt_ut_ua_2_codec((VT_SRV_CALL_MODE)mode, ua, &(ma.codec_param));
            mHelper->setParam(call_id, VT_SRV_PARAM_MA_CONFIG, reinterpret_cast<void *>(&ma));

            // create fake ua config
            VT_IMCB_CONFIG ua_config;

            memset(reinterpret_cast<void *>(&ua_config), 0, sizeof(VT_IMCB_CONFIG));

            ua_config.setting.ebi                       = 100;
            ua_config.setting.mode                      = 1;
            ua_config.config.call_id                    = call_id;
            ua_config.config.rtp_direction              = VT_SRV_DATA_PATH_SOURCE_SINK;
            ua_config.config.camera_direction           = 1;

            mHelper->setParam(call_id, VT_SRV_PARAM_UA_CONFIG, reinterpret_cast<void *>(&ua_config));
            // ================================================================

            return VT_SRV_RET_OK;
        }

        return open((VT_SRV_CALL_MODE)mode, call_id);
    }

    status_t VTCore::finalization(int call_id) {
        mHelper->logFlow("finalization", "call id", call_id, "start", VT_SRV_LOG_I);

        close(call_id, MA_SOURCE_SINK);

        mHelper->logFlow("finalization", "call id", call_id, "finish", VT_SRV_LOG_I);

        return VT_SRV_RET_OK;
    }

    status_t VTCore::open(VT_SRV_CALL_MODE mode, const int call_id) {
        Mutex::Autolock malLock(*getCallFlowLock(call_id));

        mHelper->logFlow("open", "call id", call_id, "start", VT_SRV_LOG_I);

        if (VT_SRV_CALL_4G == mode) {

            mHelper->logFlow("open", "call id", call_id, "4G mode", VT_SRV_LOG_I);

            // ====================================================================
            // create MA , add to map, init MA
            sp<VTMALStub> currentMa = NULL;
            //if (VTCoreHelper::isUT()) {
            //    mHelper->logFlow("open", "call id", call id, "dummy MA", VT_SRV_LOG_I);
            //    currentMa = new MaWrapper(MaWrapper::DUMMY_MA_PASS_MODE);
            //} else {
                mHelper->logFlow("open", "call id", call_id, "real MA", VT_SRV_LOG_I);
                currentMa = new VTMALStub(VTMALStub::MA_NORMAL_MODE_4G, call_id, this);
            //}

            mLastRet = currentMa->Init(MA_SOURCE_SINK);
            if (mLastRet) goto error;

            // every operation need to after ImsMa::Init()
            // add() will set Msg to MA
            mLastRet = mHelper->add(call_id, currentMa);
            if (mLastRet) goto error;

            //Notify default local size to VTP for preview
            int defaultW, defaultH;
            mHelper->getDefaultLocalSize(&defaultW, &defaultH);

            notifyCallback(
                call_id,
                VT_SRV_NOTIFY_DEFAULT_LOCAL_SIZE,
                defaultW,
                defaultH,
                0,
                String8(""),
                String8(""),
                NULL);

        } else {
            goto error;
        }

        // init force stop param
        // ====================================================================
        bool *isForceStop;

        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_IS_FORCE_STOP, reinterpret_cast<void **>(&isForceStop));
        if (mLastRet) goto error;

        (*isForceStop) = VT_FALSE;

        // init call param
        // ====================================================================
        vt_srv_call_update_info_struct *info;

        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
        if (mLastRet) goto error;

        info->mIsHold               = 0;
        info->mIsResume             = 0;
        info->mIsHeld               = 0;
        info->mIsResumed            = 0;
        info->mIsTurnOffVideo       = 0;
        info->mIsInTurnOffVideo     = 0;
        info->mIsTurnOnVideo        = 0;
        info->mIsTurnOffVideoByPeer = 0;
        info->mIsTurnOnVideoByPeer  = 0;
        info->mVdoRtpPort           = 0;
        info->mVdoRtcpPort          = 0;
        info->mIsInHold             = 0;
        info->mIsInHeld             = 0;

        mLastRet = mHelper->setParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void *>(info));
        if (mLastRet) goto error;
        // ====================================================================

        mHelper->setState(call_id, VT_SRV_MA_STATE_OPENED);

        mHelper->logFlow("open", "call id", call_id, "finish", VT_SRV_LOG_I);

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "open");
            return mLastRet;
    }

    status_t VTCore::init(const int call_id) {
        Mutex::Autolock malLock(*getCallFlowLock(call_id));

        media_config_t* ma_config;

        mHelper->logFlow("init", "call id", call_id, "start", VT_SRV_LOG_I);

        VT_SRV_MA_STATE state = mHelper->getState(call_id);
        mLastRet = mHelper->checkRange(state, VT_SRV_MA_STATE_OPENED, VT_SRV_MA_STATE_OPENED);
        if (mLastRet) goto error;

        // ====================================================================
        // if voice call, no need to InitMediaConfig(), we set it until update to video call
        VT_IMCB_CONFIG * ua;
        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UA_CONFIG, reinterpret_cast<void **>(&ua));
        if (mLastRet) goto error;

        if (ua->setting.mode == 0) {
            mHelper->logFlow("init", "call id", call_id, "voice call", VT_SRV_LOG_I);
            mHelper->logFlow("init", "call id", call_id, "finish", VT_SRV_LOG_I);
            return VT_SRV_RET_OK;
        }
        // ====================================================================

        mLastRet = init_internal(call_id);
        if (mLastRet) goto error;

        mHelper->logFlow("init", "call id", call_id, "finish", VT_SRV_LOG_I);

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "init");
            return mLastRet;
    }

    status_t VTCore::init_internal(const int call_id) {
        mHelper->logFlow("init internal", "call id", call_id, "start", VT_SRV_LOG_I);

        VT_SRV_CALL_MODE mode = mHelper->getMode(call_id);

        if (VT_SRV_CALL_4G == mode) {
            media_config_t* ma_config;

            // ====================================================================
            // Config MA
            mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_MA_CONFIG, reinterpret_cast<void **>(&ma_config));
            if (mLastRet) goto error;

            mLastRet = mHelper->get(call_id)->InitMediaConfig(ma_config);
            if (mLastRet) goto error;
            // ===================================================================

            vt_srv_call_update_info_struct *info;
            mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
            if (mLastRet) goto error;

        } else {
            goto error;
        }

        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_INITED);
        if (mLastRet) goto error;

        mHelper->logFlow("init internal", "call id", call_id, "finish", VT_SRV_LOG_I);

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "init internal");
            return mLastRet;
    }

    status_t VTCore::update(const int call_id) {
        Mutex::Autolock malLock(*getCallFlowLock(call_id));

        mHelper->logFlow("update", "call id", call_id, "start", VT_SRV_LOG_I);

        if (VT_SRV_MA_STATE_OPENED == mHelper->getState(call_id)) {

            VT_IMCB_CONFIG * ua;

            mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UA_CONFIG, reinterpret_cast<void **>(&ua));
            if (mLastRet) goto error;

            if (ua->setting.mode != 0) {
                // it mean not init and uddate directly
                // it is possible for
                //   1. if no bandwidth
                //   2. voice call firstly and then upgrade to video call
                init_internal(call_id);
            } else {
                // it mean a initial volte and still volte, do nothing
                return VT_SRV_RET_OK;
            }

        } else if (VT_SRV_MA_STATE_STOPED == mHelper->getState(call_id) ||
                   VT_SRV_MA_STATE_PRE_STOP == mHelper->getState(call_id)) {
            // it mean the call has end up
            return VT_SRV_RET_OK;
        }

        mLastRet = updateCallMode(call_id);
        if (mLastRet) goto error;

        mHelper->logFlow("update", "call id", call_id, "finish", VT_SRV_LOG_I);

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "update");
            return mLastRet;
    }

    status_t VTCore::updateCallMode(const int call_id) {
        VT_IMCB_CONFIG * ua;
        media_config_t* ma_config;
        vt_srv_call_update_info_struct *info;

        mHelper->logFlow("updateCallMode", "call id", call_id, "start", VT_SRV_LOG_I);

        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UA_CONFIG, reinterpret_cast<void **>(&ua));
        if (mLastRet) goto error;

        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_MA_CONFIG, reinterpret_cast<void **>(&ma_config));
        if (mLastRet) goto error;

        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
        if (mLastRet) goto error;

        mHelper->logFlow("updateCallMode", "ua->setting.mode", call_id, mHelper->getCallModeString(ua->setting.mode), VT_SRV_LOG_I);
        mHelper->logFlow("updateCallMode", "info->mIsTurnOffVideo", call_id, mHelper->getOnOffString(info->mIsTurnOffVideo), VT_SRV_LOG_I);
        mHelper->logFlow("updateCallMode", "info->mIsTurnOnVideo", call_id, mHelper->getOnOffString(info->mIsTurnOnVideo), VT_SRV_LOG_I);
        mHelper->logFlow("updateCallMode", "info->mIsTurnOffVideoByPeer", call_id, mHelper->getOnOffString(info->mIsTurnOffVideoByPeer), VT_SRV_LOG_I);
        mHelper->logFlow("updateCallMode", "info->mIsTurnOnVideoByPeer", call_id, mHelper->getOnOffString(info->mIsTurnOnVideoByPeer), VT_SRV_LOG_I);
        mHelper->logFlow("updateCallMode", "info->mIsHold", call_id, mHelper->getOnOffString(info->mIsHold), VT_SRV_LOG_I);
        mHelper->logFlow("updateCallMode", "info->mIsHeld", call_id, mHelper->getOnOffString(info->mIsHeld), VT_SRV_LOG_I);
        mHelper->logFlow("updateCallMode", "info->mIsResume", call_id, mHelper->getOnOffString(info->mIsResume), VT_SRV_LOG_I);
        mHelper->logFlow("updateCallMode", "info->mIsResumed", call_id, mHelper->getOnOffString(info->mIsResumed), VT_SRV_LOG_I);
        mHelper->logFlow("updateCallMode", "info->mIsInHold", call_id, mHelper->getOnOffString(info->mIsInHold), VT_SRV_LOG_I);
        mHelper->logFlow("updateCallMode", "info->mIsInHeld", call_id, mHelper->getOnOffString(info->mIsInHeld), VT_SRV_LOG_I);

        // ====================================================================
        if (ua->setting.mode == 0) {
            // if become VoLTE call, just close MA
            mLastRet = close_internal(call_id, MA_DOWNGRADE);
            if (mLastRet) goto error;

            return VT_SRV_RET_OK;
        } else {
            // We should only set config to MA when ViLTE call
            // or the parameter may be mess and cause error.
            mLastRet = mHelper->get(call_id)->UpdateMediaConfig(ma_config);
            if (mLastRet) goto error;

            // if still ViLTE Call, update MA accoring to direction
            mLastRet = updateTxRxMode(call_id, ua->config.rtp_direction, info);
            if (mLastRet) goto error;
        }
        // ====================================================================

        mHelper->logFlow("updateCallMode", "call id", call_id, "finish", VT_SRV_LOG_I);

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "updateCallMode");
            return mLastRet;
    }

    status_t VTCore::updateTxRxMode(const int call_id, int new_mode, vt_srv_call_update_info_struct *info) {
        mHelper->logFlow("updateTxRxMode", "call id", call_id, "start", VT_SRV_LOG_I);
        mHelper->logFlow("updateTxRxMode", "new_mode", call_id, mHelper->getVideoStateString(new_mode), VT_SRV_LOG_I);

        VT_SRV_CALL_MODE mode = mHelper->getMode(call_id);
        sp<VTMALStub> currentMa = mHelper->get(call_id);
        VT_SRV_MA_STATE curr_state = mHelper->getState(call_id);

        mHelper->logFlow("updateTxRxMode", "curr_state", call_id, mHelper->getStateString(curr_state), VT_SRV_LOG_I);

        // ====================================================================
        // wait for set all surface done
        // we design the Surface status to be the same with MA mode
        // so we can go on until mode == surface status
        VT_SRV_SURFACE_STATE * setSfState;
        int loop_counter = 0;
        do {
            // init force stop param
            // ====================================================================
            bool *isForceStop;
            VT_BOOL *isForceCancel;
            VT_BOOL *isDowngraded;

            mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_IS_FORCE_STOP, reinterpret_cast<void **>(&isForceStop));
            if (mLastRet || (*isForceStop) == VT_TRUE) {
                mHelper->logFlow("updateTxRxMode", "call id", call_id, "Force close!!!", VT_SRV_LOG_I);
                mHelper->logFlow("updateTxRxMode", "call id", call_id, "finish", VT_SRV_LOG_I);
                return VT_SRV_RET_OK;
            }

            mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_IS_FORCE_CANCEL, reinterpret_cast<void **>(&isForceCancel));
            if ((VT_SRV_RET_OK == mLastRet) && (*isForceCancel) == VT_TRUE) {
                mHelper->logFlow("updateTxRxMode", "Force cancel!!!", VT_IVD, "", VT_SRV_LOG_I);
                *isForceCancel = VT_FALSE;
                return VT_SRV_RET_OK;
            }

            mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_IS_DOWNGRADE, reinterpret_cast<void **>(&isDowngraded));
            if ((VT_SRV_RET_OK == mLastRet) && (*isDowngraded) == VT_TRUE) {
                mHelper->logFlow("updateTxRxMode", "Already downgrade!!!", VT_IVD, "", VT_SRV_LOG_I);
                return VT_SRV_RET_OK;
            }

            mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_SURFACE_STATE, reinterpret_cast<void **>(&setSfState));
            if (mLastRet) goto error;

            mHelper->logFlow("updateTxRxMode", "call id", call_id, mHelper->getSurfaceString(*setSfState), VT_SRV_LOG_I);

            usleep(200 * 1000);

        } while ((new_mode & (*setSfState)) != new_mode); // all surface need in this mode is set
        // ====================================================================

        if (VT_SRV_CALL_4G == mode) {

            // We need to make sure that
            // pause/resume operation cannot overlapping with operation (ex: setCamera) between different MAs
            // or the camera status inside MA may get into a mess
            mMAOperationLock.lock();

            if (info->mIsTurnOffVideo || info->mIsTurnOnVideo || info->mIsTurnOffVideoByPeer || info->mIsTurnOnVideoByPeer || info->mIsHold || info->mIsHeld || info->mIsResume || info->mIsResumed) {
                // update video state (local)
                if (info->mIsTurnOffVideo) {
                    info->mUpdateInfo.turnOffVideo.direction = MA_TURN_OFF_VIDEO_BY_LOCAL;
                    info->mUpdateInfo.mode = MA_PAUSE_RESUME_TURN_OFF_VIDEO;
                    mLastRet = currentMa->Pause(MA_SOURCE, &(info->mUpdateInfo));
                    info->mIsInTurnOffVideo = VT_TRUE;
                } else if (info->mIsTurnOnVideo) {
                    info->mUpdateInfo.turnOffVideo.direction = MA_TURN_OFF_VIDEO_BY_LOCAL;
                    info->mUpdateInfo.mode = MA_PAUSE_RESUME_TURN_OFF_VIDEO;
                    mLastRet = currentMa->Resume(MA_SOURCE, &(info->mUpdateInfo));
                    info->mIsInTurnOffVideo = VT_FALSE;
                }
                if (mLastRet) goto error;

                // update video state (peer)
                if (info->mIsTurnOffVideoByPeer) {
                    info->mUpdateInfo.turnOffVideo.direction = MA_TURN_OFF_VIDEO_BY_PEER;
                    info->mUpdateInfo.mode = MA_PAUSE_RESUME_TURN_OFF_VIDEO;
                    mLastRet = currentMa->Pause(MA_SINK, &(info->mUpdateInfo));
                } else if (info->mIsTurnOnVideoByPeer) {
                    info->mUpdateInfo.turnOffVideo.direction = MA_TURN_OFF_VIDEO_BY_PEER;
                    info->mUpdateInfo.mode = MA_PAUSE_RESUME_TURN_OFF_VIDEO;
                    mLastRet = currentMa->Resume(MA_SINK, &(info->mUpdateInfo));
                }
                if (mLastRet) goto error;

                // for hold call, we do special handle
                // because spec said dir = tx only but we still don't send anything
                if (info->mIsHold) {
                    info->mUpdateInfo.hold.direction = MA_HOLD_BY_LOCAL;
                    info->mUpdateInfo.mode = MA_PAUSE_RESUME_HOLD;
                    mLastRet = currentMa->Pause(MA_SINK, &(info->mUpdateInfo));
                    if (mLastRet) goto error;
                    mLastRet = currentMa->Pause(MA_SOURCE, &(info->mUpdateInfo));
                 } else if (info->mIsResume) {
                    info->mUpdateInfo.hold.direction = MA_HOLD_BY_LOCAL;
                    info->mUpdateInfo.mode = MA_PAUSE_RESUME_HOLD;
                    mLastRet = currentMa->Resume(MA_SINK, &(info->mUpdateInfo));
                    if (mLastRet) goto error;
                    mLastRet = currentMa->Resume(MA_SOURCE, &(info->mUpdateInfo));
                    info->mIsInHold = VT_FALSE;
                }
                if (mLastRet) goto error;

                if (info->mIsHeld) {
                    info->mUpdateInfo.hold.direction = MA_HOLD_BY_PEER;
                    info->mUpdateInfo.mode = MA_PAUSE_RESUME_HOLD;
                    mLastRet = currentMa->Pause(MA_SOURCE, &(info->mUpdateInfo));
                } else if (info->mIsResumed) {
                    info->mUpdateInfo.hold.direction = MA_HOLD_BY_PEER;
                    info->mUpdateInfo.mode = MA_PAUSE_RESUME_HOLD;
                    mLastRet = currentMa->Resume(MA_SOURCE, &(info->mUpdateInfo));
                    info->mIsInHeld = VT_FALSE;
                }
                if (mLastRet) goto error;

                // update state as rtp direction
                if (VT_SRV_DATA_PATH_NONE == new_mode) {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_NN);
                    if (mLastRet) goto error;
                } else if (VT_SRV_DATA_PATH_SOURCE == new_mode) {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UN);
                    if (mLastRet) goto error;
                } else if (VT_SRV_DATA_PATH_SINK == new_mode) {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_ND);
                    if (mLastRet) goto error;
                } else if (VT_SRV_DATA_PATH_SOURCE_SINK == new_mode) {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UD);
                    if (mLastRet) goto error;
                }

            } else {

                mHelper->logFlow("updateTxRxMode", "call id", call_id, "other action update", VT_SRV_LOG_I);

                if (VT_SRV_MA_STATE_INITED == curr_state) {
                    if (VT_SRV_DATA_PATH_NONE == new_mode) {
                        // do nothing
                    } else if (VT_SRV_DATA_PATH_SOURCE == new_mode) {
                        mLastRet = currentMa->Start(MA_SOURCE);
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UI);
                        if (mLastRet) goto error;

                    } else if (VT_SRV_DATA_PATH_SINK == new_mode) {
                        mLastRet = currentMa->Start(MA_SINK);
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_ID);
                        if (mLastRet) goto error;

                    } else if (VT_SRV_DATA_PATH_SOURCE_SINK == new_mode) {
                        mLastRet = currentMa->Start(MA_SOURCE_SINK);
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UD);
                        if (mLastRet) goto error;
                    }

                } else if (VT_SRV_MA_STATE_START_IN == curr_state) {
                    if (VT_SRV_DATA_PATH_NONE == new_mode) {
                        // do nothing
                    } else if (VT_SRV_DATA_PATH_SOURCE == new_mode) {
                        mLastRet = currentMa->Start(MA_SOURCE);
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UN);
                        if (mLastRet) goto error;

                    } else if (VT_SRV_DATA_PATH_SINK == new_mode) {
                        mLastRet = currentMa->Resume(MA_SINK, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_ID);
                        if (mLastRet) goto error;

                    } else if (VT_SRV_DATA_PATH_SOURCE_SINK == new_mode) {
                        mLastRet = currentMa->Start(MA_SOURCE);
                        if (mLastRet) goto error;
                        mLastRet = currentMa->Resume(MA_SINK, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UD);
                        if (mLastRet) goto error;
                    }

                } else if (VT_SRV_MA_STATE_START_NI == curr_state) {
                    if (VT_SRV_DATA_PATH_NONE == new_mode) {
                        // do nothing
                    } else if (VT_SRV_DATA_PATH_SOURCE == new_mode) {
                        mLastRet = currentMa->Resume(MA_SOURCE, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UI);
                        if (mLastRet) goto error;

                    } else if (VT_SRV_DATA_PATH_SINK == new_mode) {
                        mLastRet = currentMa->Start(MA_SINK);
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_ND);
                        if (mLastRet) goto error;

                    } else if (VT_SRV_DATA_PATH_SOURCE_SINK == new_mode) {
                        mLastRet = currentMa->Resume(MA_SOURCE, &(info->mUpdateInfo));
                        if (mLastRet) goto error;
                        mLastRet = currentMa->Start(MA_SINK);
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UD);
                        if (mLastRet) goto error;
                    }

                } else if (VT_SRV_MA_STATE_START_ID == curr_state) {
                    if (VT_SRV_DATA_PATH_NONE == new_mode) {
                        mLastRet = currentMa->Pause(MA_SINK, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_IN);
                        if (mLastRet) goto error;

                    } else if (VT_SRV_DATA_PATH_SOURCE == new_mode) {
                        mLastRet = currentMa->Start(MA_SOURCE);
                        if (mLastRet) goto error;
                        mLastRet = currentMa->Pause(MA_SINK, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UD);
                        if (mLastRet) goto error;

                    } else if (VT_SRV_DATA_PATH_SINK == new_mode) {
                        // do nothing
                    } else if (VT_SRV_DATA_PATH_SOURCE_SINK == new_mode) {
                        mLastRet = currentMa->Start(MA_SOURCE);
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UD);
                        if (mLastRet) goto error;
                    }

                } else if (VT_SRV_MA_STATE_START_UI == curr_state) {
                    if (VT_SRV_DATA_PATH_NONE == new_mode) {
                        mLastRet = currentMa->Pause(MA_SOURCE, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_NI);
                        if (mLastRet) goto error;

                     } else if (VT_SRV_DATA_PATH_SOURCE == new_mode) {
                        // do nothing
                     } else if (VT_SRV_DATA_PATH_SINK == new_mode) {
                        mLastRet = currentMa->Pause(MA_SOURCE, &(info->mUpdateInfo));
                        if (mLastRet) goto error;
                        mLastRet = currentMa->Start(MA_SINK);
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_ND);
                        if (mLastRet) goto error;

                     } else if (VT_SRV_DATA_PATH_SOURCE_SINK == new_mode) {
                        mLastRet = currentMa->Start(MA_SINK);
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UD);
                        if (mLastRet) goto error;
                     }

                } else if (VT_SRV_MA_STATE_START_NN == curr_state) {
                    if (VT_SRV_DATA_PATH_NONE == new_mode) {
                         // do nothing
                     } else if (VT_SRV_DATA_PATH_SOURCE == new_mode) {
                        mLastRet = currentMa->Resume(MA_SOURCE, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UN);
                        if (mLastRet) goto error;

                     } else if (VT_SRV_DATA_PATH_SINK == new_mode) {
                        mLastRet = currentMa->Resume(MA_SINK, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_ND);
                        if (mLastRet) goto error;

                     } else if (VT_SRV_DATA_PATH_SOURCE_SINK == new_mode) {
                        mLastRet = currentMa->Resume(MA_SOURCE, &(info->mUpdateInfo));
                        if (mLastRet) goto error;
                        mLastRet = currentMa->Resume(MA_SINK, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UD);
                        if (mLastRet) goto error;
                     }

                } else if (VT_SRV_MA_STATE_START_ND == curr_state) {
                    if (VT_SRV_DATA_PATH_NONE == new_mode) {
                        mLastRet = currentMa->Pause(MA_SINK, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_NN);
                        if (mLastRet) goto error;

                     } else if (VT_SRV_DATA_PATH_SOURCE == new_mode) {
                        mLastRet = currentMa->Resume(MA_SOURCE, &(info->mUpdateInfo));
                        if (mLastRet) goto error;
                        mLastRet = currentMa->Pause(MA_SINK, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UN);
                        if (mLastRet) goto error;

                     } else if (VT_SRV_DATA_PATH_SINK == new_mode) {
                        // do nothing
                     } else if (VT_SRV_DATA_PATH_SOURCE_SINK == new_mode) {
                        mLastRet = currentMa->Resume(MA_SOURCE, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UD);
                        if (mLastRet) goto error;
                     }

                } else if (VT_SRV_MA_STATE_START_UN == curr_state) {
                    if (VT_SRV_DATA_PATH_NONE == new_mode) {
                        mLastRet = currentMa->Pause(MA_SOURCE, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_NN);
                        if (mLastRet) goto error;

                     } else if (VT_SRV_DATA_PATH_SOURCE == new_mode) {
                        // do nothing
                     } else if (VT_SRV_DATA_PATH_SINK == new_mode) {
                        mLastRet = currentMa->Pause(MA_SOURCE, &(info->mUpdateInfo));
                        if (mLastRet) goto error;
                        mLastRet = currentMa->Resume(MA_SINK, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_ND);
                        if (mLastRet) goto error;

                     } else if (VT_SRV_DATA_PATH_SOURCE_SINK == new_mode) {
                        mLastRet = currentMa->Resume(MA_SINK, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UD);
                        if (mLastRet) goto error;
                     }

                } else if (VT_SRV_MA_STATE_START_UD == curr_state) {
                    if (VT_SRV_DATA_PATH_NONE == new_mode) {
                        mLastRet = currentMa->Pause(MA_SOURCE, &(info->mUpdateInfo));
                        if (mLastRet) goto error;
                        mLastRet = currentMa->Pause(MA_SINK, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_NN);
                        if (mLastRet) goto error;

                     } else if (VT_SRV_DATA_PATH_SOURCE == new_mode) {
                        mLastRet = currentMa->Pause(MA_SINK, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_UN);
                        if (mLastRet) goto error;

                     } else if (VT_SRV_DATA_PATH_SINK == new_mode) {
                        mLastRet = currentMa->Pause(MA_SOURCE, &(info->mUpdateInfo));
                        if (mLastRet) goto error;

                        mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_START_ND);
                        if (mLastRet) goto error;

                     } else if (VT_SRV_DATA_PATH_SOURCE_SINK == new_mode) {
                        // do nothing
                     }
                }
            }

            mHelper->logFlow("updateTxRxMode", "call id", call_id, "finish", VT_SRV_LOG_I);
            mMAOperationLock.unlock();
            return VT_SRV_RET_OK;

        } else {
            goto error;
        }

        error:
            if (VT_SRV_CALL_4G == mode) {
                mMAOperationLock.unlock();
            }
            notifyError(call_id, "updateTxRxMode");
            return mLastRet;
    }

    status_t VTCore::close(const int call_id, int close_mode) {

        mHelper->logFlow("close", "call id", call_id, "start", VT_SRV_LOG_I);

        // if still blocked at update
        // use this flag to break out to prevent from dead lock
        // need to set before lock
        bool *isForceStop;
        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_IS_FORCE_STOP, reinterpret_cast<void **>(&isForceStop));
        if (mLastRet) {
            mHelper->logFlow("close", "call id", call_id, "this call has removed", VT_SRV_LOG_E);
            return VT_SRV_RET_OK;
        }
        (*isForceStop) = VT_TRUE;

        Mutex::Autolock malLock(*getCallFlowLock(call_id));
        return close_internal(call_id, close_mode);
    }

    Mutex* VTCore::getCallFlowLock(int callId) {
        mLastRet = mHelper->checkRange(callId, 1, VT_SRV_MA_NR);
        if (mLastRet) {
            mHelper->logFlow("getCallFlowLock", "call id", callId, "this call id is invalid", VT_SRV_LOG_E);
            // Should be never happen, if then return the first lock.
            return mCallFlowLocks[0];
        }

        return mCallFlowLocks[callId - 1];
    }

    status_t VTCore::close_internal(const int call_id, int close_mode) {
        Mutex::Autolock malLock(mMAOperationLock);

        mHelper->logFlow("close internal", "call id", call_id, "start", VT_SRV_LOG_I);
        mHelper->logFlow("close internal", "call id", call_id, mHelper->getVideoStateString(close_mode), VT_SRV_LOG_I);

        VT_SRV_CALL_MODE mode = mHelper->getMode(call_id);
        VT_SRV_MA_STATE state = mHelper->getState(call_id);

        mLastRet = mHelper->checkRange(state, VT_SRV_MA_STATE_OPENED, VT_SRV_MA_STATE_END - 1);

        if (mLastRet) {
            //When camera crash happen, call state will set to STOPED but not delete record in mHelper.
            //Need delete MA record here.
            if (VT_SRV_RET_OK == mHelper->check(call_id) && VT_SRV_MA_STATE_STOPED == state) {
                vt_srv_call_update_info_struct *info;

                mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
                if (mLastRet) goto error;

                mLastRet = mHelper->del(call_id);
                if (mLastRet) goto error;

                mHelper->logFlow("close internal", "current call count", mHelper->getUsedNr(), "", VT_SRV_LOG_I);

                notifyCallback(call_id, VT_SRV_NOTIFY_CALL_END);
            } else {
                mHelper->logFlow("close internal", "call id", call_id, "has close or deleted, do nothing just skip", VT_SRV_LOG_E);
                mHelper->logFlow("close internal", "call id", call_id, "finish", VT_SRV_LOG_I);
            }

            return VT_SRV_RET_OK;
        }

        if (VT_SRV_CALL_4G == mode) {
            if (close_mode != MA_DOWNGRADE) {
                if (close_mode == MA_SOURCE_SINK) {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_PRE_STOP);
                    if (mLastRet) goto error;
                }
                mLastRet = mHelper->get(call_id)->Stop((ma_datapath_t) close_mode);
                if (mLastRet) goto error;
            } else {
                vt_srv_call_update_info_struct *info;

                mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
                if (mLastRet) goto error;

                if (info->mIsInTurnOffVideo) {

                    imsma_pause_resume_params_t updateInfo;
                    updateInfo.mode = MA_PAUSE_RESUME_TURN_OFF_VIDEO;
                    updateInfo.turnOffVideo.direction = MA_TURN_OFF_VIDEO_BY_LOCAL;
                    mLastRet = mHelper->get(call_id)->Resume(MA_SOURCE, &updateInfo);

                    info->mIsInTurnOffVideo = VT_FALSE;
                }

                mLastRet = mHelper->get(call_id)->Stop(MA_SOURCE_SINK);
                if (mLastRet) goto error;
            }

            mHelper->logFlow("close internal", "call id", call_id, "Stop done", VT_SRV_LOG_I);

            if (close_mode == MA_SOURCE) {

                // video call once, and already set local surface null
                if (mHelper->getState(call_id) == VT_SRV_MA_STATE_STOPED_D) {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_INITED);
                    if (mLastRet) goto error;

                // keep voice call, and already set local surface null
                } else if (mHelper->getState(call_id) == VT_SRV_MA_STATE_VOICE_STOPED_D) {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_OPENED);
                    if (mLastRet) goto error;

                // video call once
                } else if (mHelper->getState(call_id) > VT_SRV_MA_STATE_OPENED) {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_STOPED_U);
                    if (mLastRet) goto error;

                // keep voice call
                } else {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_VOICE_STOPED_U);
                    if (mLastRet) goto error;
                }

            } else if (close_mode == MA_SINK) {

                // video call once, and already set peer surface null
                if (mHelper->getState(call_id) == VT_SRV_MA_STATE_STOPED_U) {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_INITED);
                    if (mLastRet) goto error;

                // keep voice call, and already set peer surface null
                } else if (mHelper->getState(call_id) == VT_SRV_MA_STATE_VOICE_STOPED_U) {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_OPENED);
                    if (mLastRet) goto error;

                // video call once
                } else if (mHelper->getState(call_id) > VT_SRV_MA_STATE_OPENED) {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_STOPED_D);
                    if (mLastRet) goto error;

                // keep voice call
                } else {
                    mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_VOICE_STOPED_D);
                    if (mLastRet) goto error;
                }

            } else if (close_mode == MA_SOURCE_SINK) {
                mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_STOPED);
                if (mLastRet) goto error;
            } else if (close_mode == MA_DOWNGRADE) {
                mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_INITED);
                if (mLastRet) goto error;
            }

            mHelper->logFlow("close internal", "call id", call_id, "Update state", VT_SRV_LOG_I);

            if (VT_SRV_MA_STATE_STOPED == mHelper->getState(call_id)) {

                vt_srv_call_update_info_struct *info;

                mLastRet = mHelper->get(call_id)->Reset(MA_SOURCE_SINK);
                if (mLastRet) goto error;

                mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
                if (mLastRet) goto error;

                mLastRet = mHelper->del(call_id);
                if (mLastRet) goto error;

                notifyCallback(call_id, VT_SRV_NOTIFY_CALL_END);

            } else {
                mHelper->logFlow("close internal", "call id", call_id, "reset IsForceStop", VT_SRV_LOG_I);

                // for downgrade case, there will be update event after this
                // so we need reset the flag or update will skip foever.
                bool *isForceStop;
                mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_IS_FORCE_STOP, reinterpret_cast<void **>(&isForceStop));
                if (mLastRet) goto error;

                (*isForceStop) = VT_FALSE;
            }

        } else {
            goto error;
        }

        mHelper->logFlow("close internal", "call id", call_id, "finish", VT_SRV_LOG_I);

        return VT_SRV_RET_OK;

        error:
            // no matter what kind of error, need to clear entry or the next call won't fail
            mLastRet = mHelper->del(call_id);

            notifyError(call_id, "close");
            return mLastRet;
    }

    status_t VTCore::clearAll(void) {
        for (int i = 0; i < VT_SRV_MA_NR; i++) {
            Mutex::Autolock malLock(*mCallFlowLocks[i]);
        }

        Mutex::Autolock malLock(mMAOperationLock);

        mHelper->logFlow("clearAll", "call id", VT_IVD, "", VT_SRV_LOG_I);

        sp<VTMALStub> currentMa = NULL;
        vt_srv_call_update_info_struct *info;

        while (1) {
            currentMa = mHelper->pop();

            if (currentMa.get() == NULL) {
                break;
            }

            int callId = currentMa->getId();

            if (VT_SRV_CALL_NONE != mHelper->getMode(currentMa->getId()) &&
                VT_SRV_MA_STATE_STOPED != mHelper->getState(currentMa->getId()) &&
                VT_SRV_MA_STATE_PRE_STOP != mHelper->getState(currentMa->getId())) {

                mLastRet = currentMa->Stop(MA_SOURCE_SINK);
                if (mLastRet) goto error;
                mLastRet = currentMa->Reset(MA_SOURCE_SINK);
                if (mLastRet) goto error;
            }

            mLastRet = mHelper->getParam(callId, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
            if (mLastRet) goto error;

            mHelper->del(callId);
        }

        mLastRet = mHelper->init();
        if (mLastRet) goto error;

        return VT_SRV_RET_OK;

        error:
            notifyError(0, "clearAll");
            return mLastRet;
    }

    status_t VTCore::setHandoverState(bool state) {
        int idx = 0;
        mHelper->logAction("setHandoverState", "call id", VT_IVD, mHelper->getHOString(state), VT_SRV_LOG_I);

        //set handover for all calls
        for (idx = 0; idx < VT_SRV_MA_NR; idx++) {
            if (mHelper->isUsed(idx)) {
                mLastRet = mHelper->getFromIndex(idx)->setHandoverState(state);
                if (mLastRet) goto error;
            }
        }
        return VT_SRV_RET_OK;

        error:
            notifyError(mHelper->getCallId(idx), "setHandoverState");
            return mLastRet;
    }

    status_t VTCore::setCamera(int call_id, int cam) {
        mHelper->logAction("setCamera", "call id", call_id, mHelper->getCamString(cam), VT_SRV_LOG_I);

        if (VT_SRV_MA_STATE_PRE_STOP == mHelper->getState(call_id) ||
            VT_SRV_MA_STATE_STOPED == mHelper->getState(call_id)) {
            mHelper->logAction("setCamera", "call id", call_id, "Call has stopped", VT_SRV_LOG_I);
            return VT_SRV_RET_OK;
        }

        mLastRet = mHelper->get(call_id)->SetCameraSensor(cam);
        if (mLastRet) goto error;

        if (VTCoreHelper::isUT()) {
            if (VT_SRV_MA_STATE_OPENED == mHelper->getState(call_id)) {
                init(call_id);
            }
        }

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "setCamera");
            return mLastRet;
    }

    status_t VTCore::setPreviewSurface(int call_id, const sp<VTSurface> & surface) {
        VT_SRV_SURFACE_STATE* state;
        VT_SRV_SURFACE_STATE pre_state;
        vt_srv_call_update_info_struct *info;

        mHelper->logAction("setPreviewSurface", "call id", call_id, "", VT_SRV_LOG_I);

        if (VT_SRV_MA_STATE_PRE_STOP == mHelper->getState(call_id) ||
            VT_SRV_MA_STATE_STOPED == mHelper->getState(call_id)) {
            mHelper->logAction("setPreviewSurface", "call id", call_id, "Call has stopped", VT_SRV_LOG_I);
            return VT_SRV_RET_OK;
        }

        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_SURFACE_STATE, reinterpret_cast<void **>(&state));
        if (mLastRet) goto error;

        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
        if (mLastRet) goto error;

        mHelper->logAction("setPreviewSurface", "state (before)", call_id, mHelper->getSurfaceString((*state)), VT_SRV_LOG_I);

        pre_state = (*state);

        if (NULL != surface.get()) {

            // The resume and stop operation may come at the same time.
            // We need to add flow lock to guarantee the correct order.
            mMAOperationLock.lock();

            mLastRet = mHelper->get(call_id)->SetLocalSurface(surface);
            if (mLastRet) {
                mMAOperationLock.unlock();
                goto error;
            }

            (*state) = (VT_SRV_SURFACE_STATE) ((static_cast<int>(*state)) | VT_SRV_SURFACE_STATE_LOCAL);

            mHelper->logAction("setPreviewSurface", "state (after)", call_id, mHelper->getSurfaceString((*state)), VT_SRV_LOG_I);

            // We Pause MA instead od Stop MA when clear surrface
            // Because it may happen during conference but not call end or dowgrade
            // We want to keep use MA and keep memory of previous operation
            // so we need to resume it when set surface
            imsma_pause_resume_params_t updateInfo;
            updateInfo.mode = MA_PAUSE_RESUME_HOLD;
            updateInfo.hold.direction = MA_HOLD_BY_LOCAL;

            // we skip the 1st set surface and the case set surface continuoisly
            if (VT_SRV_MA_STATE_INITED < mHelper->getState(call_id) &&
                VT_SRV_MA_STATE_START_IN != mHelper->getState(call_id) &&
                VT_SRV_MA_STATE_START_ID != mHelper->getState(call_id) &&
                VT_SRV_MA_STATE_PRE_STOP != mHelper->getState(call_id) &&
                pre_state != (*state)) {
                mLastRet = mHelper->get(call_id)->Resume(MA_SOURCE, &updateInfo);
            }

            mMAOperationLock.unlock();

            // we don't update state here, because it is not rtp direction change
            // if we modify state, it may cause additional operation when UA notify the direction change
            // so we just change silencely and only need to make sure the pair of set/reset surface

        } else {
            (*state) = (VT_SRV_SURFACE_STATE) ((static_cast<int>(*state)) & ~VT_SRV_SURFACE_STATE_LOCAL);

            mHelper->logAction("setPreviewSurface", "state (after)", call_id, mHelper->getSurfaceString((*state)), VT_SRV_LOG_I);

            // The pause and stop operation may come at the same time.
            // We need to add flow lock to guarantee the correct order.
            mMAOperationLock.lock();

            // We Pause MA instead od Stop MA when clear surrface
            // Because it may happen during conference but not call end or dowgrade
            // We want to keep use MA and keep memory of previous operation
            //
            // Now MTK app will not clear surface when conference call.
            // It will clean surface only when call end
            // We keep this to prevent 3rd app still clean surface when conference call
            imsma_pause_resume_params_t updateInfo;
            updateInfo.mode = MA_PAUSE_RESUME_HOLD;
            updateInfo.hold.direction = MA_HOLD_BY_LOCAL;

            // We skip the case MA has stop or hold call and set surface continuoisly
            if (VT_SRV_MA_STATE_INITED < mHelper->getState(call_id) &&
                VT_SRV_MA_STATE_START_IN != mHelper->getState(call_id) &&
                VT_SRV_MA_STATE_START_ID != mHelper->getState(call_id) &&
                VT_SRV_MA_STATE_PRE_STOP != mHelper->getState(call_id) &&
                VT_TRUE != info->mIsInHold &&
                pre_state != (*state)) {
                mLastRet = mHelper->get(call_id)->Pause(MA_SOURCE, &updateInfo);
            }

            mMAOperationLock.unlock();

            // we don't update state here, because it is not rtp direction change
            // if we modify state, it may cause additional operation when UA notify the direction change
            // so we just change silencely and only need to make sure the pair of set/reset surface
        }

        if (VTCoreHelper::isUT()) {
            if (VT_SRV_SURFACE_STATE_PEER_LOCAL == (*state)) {
                update(call_id);
            }
        }

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "setPreviewSurface");
            return mLastRet;
    }

    status_t VTCore::setDisplaySurface(int call_id, const sp<VTSurface> & surface) {
        VT_SRV_SURFACE_STATE* state;
        VT_SRV_SURFACE_STATE pre_state;
        vt_srv_call_update_info_struct *info;

        mHelper->logAction("setDisplaySurface", "call id", call_id, "", VT_SRV_LOG_I);

        if (VTCoreHelper::isUT()) {
            if (VT_SRV_MA_STATE_OPENED == mHelper->getState(call_id)) {
                init(call_id);
            }
        }

        if (VT_SRV_MA_STATE_PRE_STOP == mHelper->getState(call_id) ||
            VT_SRV_MA_STATE_STOPED == mHelper->getState(call_id)) {
            mHelper->logAction("setDisplaySurface", "call id", call_id, "MA has stopped", VT_SRV_LOG_I);
            return VT_SRV_RET_OK;
        }

        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_SURFACE_STATE, reinterpret_cast<void **>(&state));
        if (mLastRet) goto error;

        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
        if (mLastRet) goto error;

        mHelper->logAction("setDisplaySurface", "state (before)", call_id, mHelper->getSurfaceString((*state)), VT_SRV_LOG_I);

        pre_state = (*state);

        if (NULL != surface.get()) {

            // The resume and stop operation may come at the same time.
            // We need to add flow lock to guarantee the correct order.
            mMAOperationLock.lock();

            mLastRet = mHelper->get(call_id)->SetPeerSurface(surface);
            if (mLastRet) {
                mMAOperationLock.unlock();
                goto error;
            }

            (*state) = (VT_SRV_SURFACE_STATE) ((static_cast<int>(*state)) | VT_SRV_SURFACE_STATE_PEER);

            mHelper->logAction("setDisplaySurface", "state (after)", call_id, mHelper->getSurfaceString((*state)), VT_SRV_LOG_I);

            // We Pause MA instead od Stop MA when clear surrface
            // Because it may happen during conference but not call end or dowgrade
            // We want to keep use MA and keep memory of previous operation
            // so we need to resume it when set surface
            imsma_pause_resume_params_t updateInfo;
            updateInfo.mode = MA_PAUSE_RESUME_HOLD;
            updateInfo.hold.direction = MA_HOLD_BY_LOCAL;

            // we skip the 1st set surface and the case set surface continuoisly
            if (VT_SRV_MA_STATE_INITED < mHelper->getState(call_id) &&
                VT_SRV_MA_STATE_START_NI != mHelper->getState(call_id) &&
                VT_SRV_MA_STATE_START_UI != mHelper->getState(call_id) &&
                VT_SRV_MA_STATE_PRE_STOP != mHelper->getState(call_id) &&
                pre_state != (*state)) {
                mLastRet = mHelper->get(call_id)->Resume(MA_SINK, &updateInfo);
            }

            mMAOperationLock.unlock();

            // we don't update state here, because it is not rtp direction change
            // if we modify state, it may cause additional operation when UA notify the direction change
            // so we just change silencely and only need to make sure the pair of set/reset surface

        } else {
            (*state) = (VT_SRV_SURFACE_STATE) ((static_cast<int>(*state)) & ~VT_SRV_SURFACE_STATE_PEER);

            mHelper->logAction("setDisplaySurface", "state (after)", call_id, mHelper->getSurfaceString((*state)), VT_SRV_LOG_I);

            // The pause and stop operation may come at the same time.
            // We need to add flow lock to guarantee the correct order.
            mMAOperationLock.lock();

            // We Pause MA instead od Stop MA when clear surrface
            // Because it may happen during conference but not call end or dowgrade
            // We want to keep use MA and keep memory of previous operation
            //
            // Now MTK app will not clear surface when conference call.
            // It will clean surface only when call end
            // We keep this to prevent 3rd app still clean surface when conference call
            imsma_pause_resume_params_t updateInfo;
            updateInfo.mode = MA_PAUSE_RESUME_HOLD;
            updateInfo.hold.direction = MA_HOLD_BY_LOCAL;

            // We skip the case MA has stop or hold call and set surface continuoisly
            if (VT_SRV_MA_STATE_INITED < mHelper->getState(call_id) &&
                VT_SRV_MA_STATE_START_NI != mHelper->getState(call_id) &&
                VT_SRV_MA_STATE_START_UI != mHelper->getState(call_id) &&
                VT_SRV_MA_STATE_PRE_STOP != mHelper->getState(call_id) &&
                VT_TRUE != info->mIsInHold &&
                pre_state != (*state)) {
                mLastRet = mHelper->get(call_id)->Pause(MA_SINK, &updateInfo);
            }

            mMAOperationLock.unlock();

            // we don't update state here, because it is not rtp direction change
            // if we modify state, it may cause additional operation when UA notify the direction change
            // so we just change silencely and only need to make sure the pair of set/reset surface
        }

        if (VTCoreHelper::isUT()) {
            if (VT_SRV_SURFACE_STATE_PEER_LOCAL == (*state)) {
                update(call_id);
            }
        }

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "setDisplaySurface");
            return mLastRet;
    }

    status_t VTCore::setCameraParameters(int sim_id, int major_sim_id, int sensorCnt, sensor_info_vilte_t *sensor) {
        mHelper->logAction("setCameraParameters", "call id", sim_id, "", VT_SRV_LOG_I);
        VT_LOGI("sim_id:%d, major_sim_id:%d, sensorCnt:%d", sim_id, major_sim_id, sensorCnt);

        for(int i = 0; i < sensorCnt; i++) {
            VT_LOGI("sensor[%d]: id=%d, width=%d, height=%d, degree=%d, facing=%d",
                i, sensor[i].index, sensor[i].max_width, sensor[i].max_height, sensor[i].degree, sensor[i].facing);
        }

        ImsMa::setSensorParameters(sensor, sensorCnt);

        VT_IMCB_CAP *ua = &g_vt.ua_cap;

        if (0 == sensorCnt) {
            // If cannot get camera cap, just response empty ua cap
            memset(ua, 0, sizeof(VT_IMCB_CAP));
            ua->sim_slot_id = sim_id;
        } else {
            vt_rtp_codec_2_ua(VT_SRV_CALL_4G, ua, sim_id, major_sim_id);
        }

        // ================================================================
        // send MSG to UA via IPC
        // ================================================================
        int ret = VT_Send(
                    VT_SRV_CALL_4G,
                    MSG_ID_WRAP_IMSVT_IMCB_GET_CAP_RSP,
                    reinterpret_cast<void *>(&g_vt.ua_cap),
                    sizeof(VT_IMCB_CAP));
        if (ret) goto error;

        return VT_SRV_RET_OK;

        error:
            mHelper->logAction("setCameraParameters", "call id", sim_id, "send fail", VT_SRV_LOG_E);
            return mLastRet;
    }

    status_t VTCore::setDeviceOrientation(int call_id, int rotation) {
        mHelper->logAction("setDeviceOrientation", "call id", call_id, "", VT_SRV_LOG_I);

        // we need check call state, if target call going to stop flow,
        // should not set device orientation.
        if (VT_SRV_MA_STATE_PRE_STOP == mHelper->getState(call_id) ||
            VT_SRV_MA_STATE_STOPED == mHelper->getState(call_id)) {
            mHelper->logAction("setDeviceOrientation", "call id", call_id, "MA has stopped", VT_SRV_LOG_I);
            return VT_SRV_RET_OK;
        }

        mLastRet = mHelper->get(call_id)->setDeviceRotationDegree(rotation);
        if (mLastRet) goto error;

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "setDeviceOrientation");
            return mLastRet;
    }

    status_t VTCore::setUIMode(int call_id, VT_SRV_UI_MODE mode) {

        mHelper->logAction("setUIMode", "call id", call_id, mHelper->getUIModeString(mode), VT_SRV_LOG_I);

        if (mode == VT_SRV_UI_MODE_FG) {

            mHelper->get(call_id)->setUIMode(1);

            vt_srv_call_ui_config_struct *config;
            mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_PEER_UI_CONFIG, reinterpret_cast<void **>(&config));
            if (mLastRet) goto error;

            if (config->mWidth == 0 || config->mHeight == 0) {
                mHelper->logAction("setUIMode", "call id", call_id, "MA not notify yet, ignore", VT_SRV_LOG_W);
            } else {
                notifyCallback(
                        call_id,
                        VT_SRV_NOTIFY_PEER_SIZE_CHANGED,
                        config->mWidth,
                        config->mHeight,
                        config->mRotation,
                        String8(""),
                        String8(""),
                        NULL);
            }
        } else if (mode == VT_SRV_UI_MODE_BG) {
            mHelper->get(call_id)->setUIMode(0);

        } else if(mode == VT_SRV_UI_MODE_FULL_SCREEN) {
            ImsMa::setLowPowerMode(VT_TRUE);

        } else if(mode == VT_SRV_UI_MODE_NORMAL_SCREEN) {
            ImsMa::setLowPowerMode(VT_FALSE);
        }

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "setUIMode");
            return mLastRet;
    }

    status_t VTCore::requestCancelSessionModify(int call_id, sp<VideoProfile> Vp) {
        VT_IMCB_CANCEL_REQ request;
        memset(&request, 0, sizeof(request));

        mHelper->logAction("requestCancelSessionModify", "call id", call_id, "", VT_SRV_LOG_I);

        int cause = (Vp->getState() == VT_SRV_VIDEO_STATE_CANCEL)? 0:1;

        request.call_id = GET_CALL_ID(call_id);
        request.sim_slot_id = GET_SIM_ID(call_id);
        request.cause = cause;

        if (VTCoreHelper::isUT()) {
            return VT_SRV_RET_OK;
        }

        mLastRet = mHelper->setParam(call_id, VT_SRV_PARAM_CANCEL_SESSION_REQ, reinterpret_cast<void *>(&request));
        if (mLastRet) goto error;

        mLastRet = VT_Send(
                        VT_SRV_CALL_4G,
                        MSG_ID_WRAP_IMSVT_IMCB_MODIFY_SESSION_CANCEL_REQ,
                        reinterpret_cast<void *>(&request),
                        sizeof(VT_IMCB_CANCEL_REQ));

        if (mLastRet) goto error;

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "requestCancelSessionModify");
            return mLastRet;
    }

    status_t VTCore::requestSessionModify(int call_id, const String8 &  config) {
        mHelper->logAction("requestSessionModify", "call id", call_id, "", VT_SRV_LOG_I);

        bool *isForceStop;
        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_IS_FORCE_STOP, reinterpret_cast<void **>(&isForceStop));
        if ((VT_SRV_RET_OK == mLastRet) && (*isForceStop) == VT_TRUE) {
            mHelper->logAction("requestSessionModify", "call id", call_id, "isForceStopd, ignore session modify", VT_SRV_LOG_W);
            return VT_SRV_RET_OK;
        }

        sp<VideoProfile> Vp = mHelper->unPackToVdoProfile(config);
        if (mHelper->isCancelRequest(Vp)) {
            return requestCancelSessionModify(call_id, Vp);
        }

        mHelper->logAction("requestSessionModify", "call id", call_id, mHelper->getVideoStateString(Vp->getState()), VT_SRV_LOG_I);

        VT_BOOL *isForceCancel;
        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_IS_FORCE_CANCEL, reinterpret_cast<void **>(&isForceCancel));
        if ((VT_SRV_RET_OK == mLastRet) && (*isForceCancel) == VT_TRUE) {
            mHelper->logFlow("requestSessionModify", "clear force cancel!", VT_IVD, "", VT_SRV_LOG_I);
            *isForceCancel = VT_FALSE;
        }

        VT_IMCB_REQ request;
        memset(&request, 0, sizeof(VT_IMCB_REQ));

        if (VTCoreHelper::isUT()) {
            return VT_SRV_RET_OK;
        }

        mHelper->getReqFromProfile(&request, Vp, call_id);

        mLastRet = mHelper->setParam(call_id, VT_SRV_PARAM_SESSION_REQ, reinterpret_cast<void *>(&request));
        if (mLastRet) goto error;

        mLastRet = VT_Send(
                        VT_SRV_CALL_4G,
                        MSG_ID_WRAP_IMSVT_IMCB_MODIFY_SESSION_REQ,
                        reinterpret_cast<void *>(&request),
                        sizeof(VT_IMCB_REQ));
        if (mLastRet) goto error;

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "requestSessionModify");
            return mLastRet;
    }

    status_t VTCore::responseSessionModify(int call_id, const String8 &  config) {
        mHelper->logAction("responseSessionModify", "call id", call_id, "", VT_SRV_LOG_I);

        sp<VideoProfile> Vp = mHelper->unPackToVdoProfile(config);

        mHelper->logAction("responseSessionModify", "call id", call_id, mHelper->getVideoStateString(Vp->getState()), VT_SRV_LOG_I);

        VT_IMCB_RSP response;
        memset(&response, 0, sizeof(VT_IMCB_RSP));

        mHelper->getRspFromProfile(&response, Vp, call_id);

        mLastRet = VT_Send(
                        VT_SRV_CALL_4G,
                        MSG_ID_WRAP_IMSVT_IMCB_MODIFY_SESSION_RSP,
                        reinterpret_cast<void *>(&response),
                        sizeof(VT_IMCB_RSP));
        if (mLastRet) goto error;

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "responseSessionModify");
            return mLastRet;
    }

    status_t VTCore::snapshot(int call_id, VT_SRV_SNAPSHOT_TYPE type, String8 savingImgURI) {
        mHelper->logAction("snapshot", "call id", call_id, mHelper->getSnapshotString(type), VT_SRV_LOG_I);

        VT_SRV_MA_STATE state = mHelper->getState(call_id);
        mLastRet = mHelper->checkRange(state, VT_SRV_MA_STATE_OPENED, VT_SRV_MA_STATE_START_UD);
        if (mLastRet) {
            mHelper->logAction("snapshot", "call id", call_id, "call has stopped", VT_SRV_LOG_E);
            goto error;
        }

        mLastRet = mHelper->checkRange(type, VT_SRV_SNAPSHOT_LOCAL, VT_SRV_SNAPSHOT_LOCAL_PEER);
        if (mLastRet) goto error;

        mLastRet = mHelper->get(call_id)->SnapShot(savingImgURI.string(), (snapshot_mode_t) type);
        if (mLastRet) goto error;

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "snapshot");
            return mLastRet;
    }

    status_t VTCore::startRecording(int call_id, VT_SRV_RECORD_TYPE type, String8 path, int maxSize) {
        mHelper->logAction("startRecording", "call id", call_id, mHelper->getRecordString(type), VT_SRV_LOG_I);

        VT_SRV_MA_STATE state = mHelper->getState(call_id);
        mLastRet = mHelper->checkRange(state, VT_SRV_MA_STATE_OPENED, VT_SRV_MA_STATE_START_UD);
        if (mLastRet) {
            mHelper->logAction("startRecording", "call id", call_id, "call has stopped", VT_SRV_LOG_E);
            goto error;
        }

        mLastRet = mHelper->checkRange(type, VT_SRV_RECORD_VIDEO, VT_SRV_RECORD_VIDEO_AUDIO);
        if (mLastRet) goto error;

        mLastRet = mHelper->get(call_id)->SetRecordParameters(MA_RECQUALITY_MEDIUM, (char *)path.string());
        if (mLastRet) goto error;

        mLastRet = mHelper->get(call_id)->StartRecord((record_mode_t) type);
        if (mLastRet) goto error;

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "startRecording");
            return mLastRet;
    }

    status_t VTCore::stopRecording(int call_id) {
        mHelper->logAction("stopRecording", "call id", call_id, "", VT_SRV_LOG_I);

        VT_SRV_MA_STATE state = mHelper->getState(call_id);
        mLastRet = mHelper->checkRange(state, VT_SRV_MA_STATE_OPENED, VT_SRV_MA_STATE_START_UD);
        if (mLastRet) {
            mHelper->logAction("stopRecording", "call id", call_id, "call has stopped", VT_SRV_LOG_E);
            goto error;
        }

        mLastRet = mHelper->get(call_id)->StopRecord();
        if (mLastRet) goto error;

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "stopRecording");
            return mLastRet;
    }

    status_t VTCore::switchFeature(int call_id, int feature, int isOn) {
        VT_IMCB_CONFIG * ua;

        mHelper->logAction("switchFeature", "call id", call_id, "", VT_SRV_LOG_I);
        mHelper->logAction("switchFeature", "isOn", isOn, "", VT_SRV_LOG_I);

        //Do nothing when VoLTE
        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UA_CONFIG, reinterpret_cast<void **>(&ua));
        if (mLastRet) goto error;
        if (ua->setting.mode == 0) {
            return VT_SRV_RET_OK;
        }

        // LTE case
        if (TAG_VILTE_MOBILE == feature) {

            if (isOn) {

                // do nothing

            } else {

               // now we do nothing here
            }

        // WiFi case
        } else if (TAG_VILTE_WIFI== feature) {

            if (isOn) {

                // do nothing

            } else {

                // do nothing
            }
        }

        return VT_SRV_RET_OK;

        error:
            notifyError(call_id, "switchFeature");
            return mLastRet;
    }

    void VTCore::notifyError(int call_id, const char* action) {

        if (mLastRet == VT_SRV_RET_ERR_MA) {

            mLastNotify = VT_SRV_ERROR_CAMERA_CRASHED;

            if (VT_SRV_MA_STATE_STOPED != mHelper->getState(call_id) &&
                    VT_SRV_MA_STATE_PRE_STOP != mHelper->getState(call_id)) {

                mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_PRE_STOP);

                mHelper->get(call_id)->Stop(MA_SOURCE_SINK);
                mHelper->get(call_id)->Reset(MA_SOURCE_SINK);

                mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_STOPED);

                // if still blocked at update
                // use this flag to break out to prevent from dead lock
                bool *isForceStop;
                mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_IS_FORCE_STOP, reinterpret_cast<void **>(&isForceStop));
                if (mLastRet) {
                    mHelper->logFlow("close", "call id", call_id, "this call has removed", VT_SRV_LOG_E);
                }
                (*isForceStop) = VT_TRUE;

                mHelper->setMode(call_id, VT_SRV_CALL_NONE);
            }

        } else {
            mLastNotify = VT_SRV_ERROR_SERVICE;
        }

        mHelper->logFlow(action, "call id", call_id, mHelper->getErrorString(mLastRet), VT_SRV_LOG_E);
        notifyCallback(call_id, mLastNotify);
    }

    void VTCore::notifyCallback(int32_t call_id, int32_t msgType) {
        notifyCallback(
                call_id,
                msgType,
                0,
                0,
                0,
                String8(""),
                String8(""),
                NULL);
    }

    void VTCore::notifyCallback(
            int32_t call_id,
            int32_t msgType,
            int32_t arg1,
            int32_t arg2,
            int32_t arg3,
            const String8 & obj1,
            const String8 & obj2,
            const sp<IGraphicBufferProducer> & obj3) {

        if (msgType > VT_SRV_ERROR_BASE) {
            Mutex::Autolock ntyLock(mNotifyErrorLock);
        } else {
            Mutex::Autolock ntyLock(mNotifyLock);
        }

        mHelper->logMsg(
                    msgType,
                    "",
                    call_id,
                    arg1,
                    arg2,
                    arg3,
                    obj1.string(),
                    obj2.string(),
                    VT_SRV_LOG_I);

        if (msgType == VT_SRV_NOTIFY_AVPF_TMMBR_MBR_DL) {
            VT_IMCB_BW *BW = &g_vt.ma_bw;

            BW->call_id         = GET_CALL_ID(call_id);
            BW->sim_slot_id     = GET_SIM_ID(call_id);
            BW->video_bandwidth = arg1;

            VT_Send(
                    VT_SRV_CALL_4G,
                    MSG_ID_WRAP_IMSVT_IMCB_MOD_BW_REQ,
                    reinterpret_cast<void *>(&g_vt.ma_bw),
                    sizeof(VT_IMCB_BW));
            return;

        } else if (msgType == VT_SRV_ERROR_CODEC || msgType == VT_SRV_ERROR_CAMERA_CRASHED) {

            if (VT_SRV_MA_STATE_STOPED != mHelper->getState(call_id) &&
                    VT_SRV_MA_STATE_PRE_STOP != mHelper->getState(call_id)) {

                mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_PRE_STOP);

                mHelper->get(call_id)->Stop(MA_SOURCE_SINK);
                mHelper->get(call_id)->Reset(MA_SOURCE_SINK);

                mLastRet = mHelper->setState(call_id, VT_SRV_MA_STATE_STOPED);

                // if still blocked at update
                // use this flag to break out to prevent from dead lock
                bool *isForceStop;
                mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_IS_FORCE_STOP, reinterpret_cast<void **>(&isForceStop));
                if (mLastRet) {
                    mHelper->logFlow("close", "call id", call_id, "this call has removed", VT_SRV_LOG_E);
                }
                (*isForceStop) = VT_TRUE;

                mHelper->setMode(call_id, VT_SRV_CALL_NONE);
                mLastNotify = msgType;
            }
        }

        if (mUser == 0) {
            mHelper->logAction("notifyCallback", "call id", call_id, "User is NULL", VT_SRV_LOG_E);
            return;
        }

        mUser->notifyCallback(call_id, msgType, arg1, arg2, arg3, obj1, obj2, obj3);
        return;
    }

    void VTCore::notifyCallback(
       int call_id,
       int msg,
       int arg1,
       int arg2) {

        notifyCallback(call_id, msg, arg1, arg2, 0, String8(""), String8(""), NULL);
    }

    void VTCore::resetUpdateInfo(int call_id) {
        Mutex::Autolock malLock(*getCallFlowLock(call_id));

        mHelper->logAction("resetUpdateInfo", "call id", call_id, "start", VT_SRV_LOG_I);

        vt_srv_call_update_info_struct *info;

        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
        if (mLastRet) goto error;

        info->mUpdateInfo.mode = MA_PAUSE_RESUME_NORMAL;
        info->mIsHold = VT_FALSE;
        info->mIsResume = VT_FALSE;
        info->mIsTurnOffVideo = VT_FALSE;
        info->mIsTurnOnVideo = VT_FALSE;
        info->mIsTurnOffVideoByPeer = VT_FALSE;
        info->mIsTurnOnVideoByPeer = VT_FALSE;

        mLastRet = mHelper->setParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void *>(info));
        mHelper->logAction("resetUpdateInfo", "call id", call_id, "finish", VT_SRV_LOG_E);

        return;

        error:
            notifyError(call_id, "resetUpdateInfo");
            return;
    }

    void VTCore::getUpdateInfo(int call_id, VT_IMCB_UPD* pre_config, VT_IMCB_UPD* new_config) {
        Mutex::Autolock malLock(*getCallFlowLock(call_id));

        mHelper->logAction("getUpdateInfo", "call id", call_id, "start", VT_SRV_LOG_I);

        vt_srv_call_update_info_struct *info;
        imsma_turn_off_video_mode_t turnoffvideo_local = MA_TURN_OFF_VIDEO_DISABLE;
        imsma_turn_off_video_mode_t turnoffvideo_peer = MA_TURN_OFF_VIDEO_DISABLE;

        if (VT_SRV_MA_STATE_STOPED == mHelper->getState(call_id) ||
            VT_SRV_MA_STATE_PRE_STOP == mHelper->getState(call_id)) {
            mHelper->logAction("getUpdateInfo", "call id", call_id, "MA has stopped", VT_SRV_LOG_I);
            return;
        }

        mLastRet = mHelper->getParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
        if (mLastRet) goto error;

        mHelper->logAction("getUpdateInfo", "old bHold", call_id, mHelper->getOnOffString(pre_config->config.bHold), VT_SRV_LOG_I);
        mHelper->logAction("getUpdateInfo", "new bHold", call_id, mHelper->getOnOffString(new_config->config.bHold), VT_SRV_LOG_I);
        mHelper->logAction("getUpdateInfo", "old bHeld", call_id, mHelper->getOnOffString(pre_config->config.bHeld), VT_SRV_LOG_I);
        mHelper->logAction("getUpdateInfo", "new bHeld", call_id, mHelper->getOnOffString(new_config->config.bHeld), VT_SRV_LOG_I);

        // set default value or it may be change on one condition and reset on the other case
        info->mUpdateInfo.mode = MA_PAUSE_RESUME_NORMAL;
        info->mIsHold = VT_FALSE;
        info->mIsResume = VT_FALSE;

        // ===================================================================
        // check local hold Call
        if (!pre_config->config.bHold && new_config->config.bHold) {
            info->mUpdateInfo.mode = MA_PAUSE_RESUME_HOLD;
            info->mUpdateInfo.hold.direction = MA_HOLD_BY_LOCAL;
            info->mIsHold = VT_TRUE;
            info->mIsInHold = VT_TRUE;
        }

        if(strcmp(pre_config->config.remote_rtp_address, "0.0.0.0") &&
           !strcmp(new_config->config.remote_rtp_address, "0.0.0.0")) {
            info->mUpdateInfo.mode = MA_PAUSE_RESUME_HOLD;
            info->mUpdateInfo.hold.direction = MA_HOLD_BY_LOCAL;
            info->mIsHold = VT_TRUE;
            info->mIsInHold = VT_TRUE;
        }
        // ===================================================================

        // ===================================================================
        // check local resume Call
        if (pre_config->config.bHold && !new_config->config.bHold) {
            info->mUpdateInfo.mode = MA_PAUSE_RESUME_HOLD;
            info->mUpdateInfo.hold.direction = MA_HOLD_BY_LOCAL;
            info->mIsResume = VT_TRUE;
        }

        if(!strcmp(pre_config->config.remote_rtp_address, "0.0.0.0") &&
           strcmp(new_config->config.remote_rtp_address, "0.0.0.0")) {
            info->mUpdateInfo.mode = MA_PAUSE_RESUME_HOLD;
            info->mUpdateInfo.hold.direction = MA_HOLD_BY_LOCAL;
            info->mIsResume = VT_TRUE;
        }
        // ===================================================================

        // ===================================================================
        // check remote hold Call
        if (!pre_config->config.bHeld && new_config->config.bHeld) {
            info->mUpdateInfo.mode = MA_PAUSE_RESUME_HOLD;
            info->mUpdateInfo.hold.direction = MA_HOLD_BY_PEER;
            info->mIsHeld = VT_TRUE;
            info->mIsInHeld = VT_TRUE;
        } else {
            info->mIsHeld = VT_FALSE;
        }
        // ===================================================================

        // ===================================================================
        // check remote resume Call
        if (pre_config->config.bHeld && !new_config->config.bHeld) {
            info->mUpdateInfo.mode = MA_PAUSE_RESUME_HOLD;
            info->mUpdateInfo.hold.direction = MA_HOLD_BY_PEER;
            info->mIsResumed = VT_TRUE;
        } else {
            info->mIsResumed = VT_FALSE;
        }
        // ===================================================================

        // In some case, MA stop/reset and getUpdateInfo are occur at the same time.
        // We do not know when the MA will be set NULL.
        // We need to get MA parameters firstly and save in local to use them.
        turnoffvideo_local = mHelper->get(call_id)->GetTurnOffVideoByLocalState();
        turnoffvideo_peer = mHelper->get(call_id)->GetTurnOffVideoByPeerState();

        mHelper->logAction("getUpdateInfo", "Local Camera State", call_id, mHelper->getLocalCameraStateString(turnoffvideo_local), VT_SRV_LOG_I);
        mHelper->logAction("getUpdateInfo", "Peer Camera State", call_id, mHelper->getPeerCameraStateString(turnoffvideo_peer), VT_SRV_LOG_I);
        mHelper->logAction("getUpdateInfo", "New Camera Direction", call_id, mHelper->getVideoStateString(new_config->config.camera_direction), VT_SRV_LOG_I);

        // if previous state is voice call, the camera should not turn off
        // so all flags is false to prevent it go to hide me/you state
        // or it will cause error if upgrade to one way
        if (pre_config->setting.mode == 0) {
            mHelper->logAction("getUpdateInfo", "call id", call_id, "pre is vocie call, skip hide me/you check", VT_SRV_LOG_I);

            info->mIsTurnOffVideo = VT_FALSE;
            info->mIsInTurnOffVideo = VT_FALSE;
            info->mIsTurnOnVideo = VT_FALSE;
            info->mIsTurnOffVideoByPeer = VT_FALSE;
            info->mIsTurnOnVideoByPeer = VT_FALSE;

        } else {

            // In the case,
            // the camera direction changes from send_recv to inactive in a update.
            // We will view as NW action but user operation.
            // So we will skip this change and let rtp_direction to pause MA.
            //
            // There is two assumption :
            // rtp_direction will be also inactive (from UA' code)
            // NW will be back to send_recv
            if (turnoffvideo_local == MA_TURN_OFF_VIDEO_DISABLE &&
                turnoffvideo_peer == MA_TURN_OFF_VIDEO_DISABLE &&
                new_config->config.camera_direction == VT_DIR_INACTIVE) {
                info->mIsTurnOffVideo = VT_FALSE;
                info->mIsTurnOnVideo = VT_FALSE;
                info->mIsTurnOffVideoByPeer = VT_FALSE;
                info->mIsTurnOnVideoByPeer = VT_FALSE;
                info->mUpdateInfo.mode = MA_PAUSE_RESUME_NORMAL;
                info->mUpdateInfo.turnOffVideo.direction = MA_TURN_OFF_VIDEO_BY_LOCAL;

                mLastRet = mHelper->setParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void *>(info));
                mHelper->logAction("getUpdateInfo", "call id", call_id, "finish", VT_SRV_LOG_E);
                return;
            }
            VT_SRV_MA_STATE curr_state = mHelper->getState(call_id);

            // ===================================================================
            // check Hide me
            if (turnoffvideo_local == MA_TURN_OFF_VIDEO_DISABLE
                && (VT_SRV_MA_STATE_START_UI == curr_state || VT_SRV_MA_STATE_START_UN == curr_state || VT_SRV_MA_STATE_START_UD == curr_state)
                && (new_config->config.camera_direction == VT_DIR_INACTIVE || new_config->config.camera_direction == VT_DIR_RECV_ONLY)) {

                info->mUpdateInfo.mode = MA_PAUSE_RESUME_TURN_OFF_VIDEO;
                info->mUpdateInfo.turnOffVideo.direction = MA_TURN_OFF_VIDEO_BY_LOCAL;
                info->mIsTurnOffVideo = VT_TRUE;
            } else {
                info->mIsTurnOffVideo = VT_FALSE;
            }

            if (turnoffvideo_local == MA_TURN_OFF_VIDEO_ENABLE
                && (VT_SRV_MA_STATE_START_NI == curr_state || VT_SRV_MA_STATE_START_NN == curr_state || VT_SRV_MA_STATE_START_ND == curr_state)
                && (new_config->config.camera_direction == VT_DIR_SEND_RECV || new_config->config.camera_direction == VT_DIR_SEND_ONLY)) {

                info->mUpdateInfo.mode = MA_PAUSE_RESUME_TURN_OFF_VIDEO;
                info->mUpdateInfo.turnOffVideo.direction = MA_TURN_OFF_VIDEO_BY_LOCAL;
                info->mIsTurnOnVideo = VT_TRUE;
            } else {
                info->mIsTurnOnVideo = VT_FALSE;
            }

            // ===================================================================

            // ===================================================================
            // check Hide you
            if (turnoffvideo_peer == MA_TURN_OFF_VIDEO_DISABLE
                && (VT_SRV_MA_STATE_START_ID == curr_state || VT_SRV_MA_STATE_START_ND == curr_state || VT_SRV_MA_STATE_START_UD == curr_state)
                && (new_config->config.camera_direction == VT_DIR_INACTIVE || new_config->config.camera_direction == VT_DIR_SEND_ONLY)) {

                info->mUpdateInfo.mode = MA_PAUSE_RESUME_TURN_OFF_VIDEO;
                info->mUpdateInfo.turnOffVideo.direction = MA_TURN_OFF_VIDEO_BY_PEER;
                info->mIsTurnOffVideoByPeer = VT_TRUE;
            } else {
                info->mIsTurnOffVideoByPeer = VT_FALSE;
            }

            if (turnoffvideo_peer == MA_TURN_OFF_VIDEO_ENABLE
                && (VT_SRV_MA_STATE_START_IN == curr_state || VT_SRV_MA_STATE_START_NN == curr_state || VT_SRV_MA_STATE_START_UN == curr_state)
                && (new_config->config.camera_direction == VT_DIR_SEND_RECV || new_config->config.camera_direction == VT_DIR_RECV_ONLY)) {

                info->mUpdateInfo.mode = MA_PAUSE_RESUME_TURN_OFF_VIDEO;
                info->mUpdateInfo.turnOffVideo.direction = MA_TURN_OFF_VIDEO_BY_PEER;
                info->mIsTurnOnVideoByPeer = VT_TRUE;
            } else {
                info->mIsTurnOnVideoByPeer = VT_FALSE;
            }
            // ===================================================================
        }

        mLastRet = mHelper->setParam(call_id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void *>(info));
        if (mLastRet) goto error;

        mHelper->logAction("getUpdateInfo", "call id", call_id, "finish", VT_SRV_LOG_E);

        return;

        error:
            notifyError(call_id, "getUpdateInfo");
            return;
    }

    void vt_callback(int type, void *data, int len) {
        sp<VTCore> core = g_vt.core;
        sp<VTCoreHelper> helper = VTCore::mHelper;
        int id = -1;
        int ret;
        int notify_type = VT_SRV_ERROR_SERVICE;

        if (VTCoreHelper::isUT()) {
            if (type == 5566) {
                sp<VTCoreHelper> helper = new VTCoreHelper();
                helper->logAction("vt_callback", "call id", VT_IVD, "Loopback start", VT_SRV_LOG_I);
                helper->init();
                helper->logAction("vt_callback", "call id", VT_IVD, "Loopback end", VT_SRV_LOG_I);
                return;
            }
        }

        // get cap case
        if (type == MSG_ID_WRAP_IMSVT_IMCB_GET_CAP_IND) {

            VT_IMCB_CAPIND CapInd;

            memset(&CapInd, 0, sizeof(VT_IMCB_CAPIND));
            memcpy(&CapInd, reinterpret_cast<VT_IMCB_CAPIND *>(data), len);

            id = CapInd.sim_slot_id;

            helper->logAction("vt_callback", "call id", id, "MSG_ID_WRAP_IMSVT_IMCB_GET_CAP_IND", VT_SRV_LOG_W);

            core->notifyCallback(
                id,
                VT_SRV_NOTIFY_GET_CAP,
                0, //Default get camera 0
                0,
                0,
                String8(""),
                String8(""),
                NULL);

        // init config case
        } else if (type == MSG_ID_WRAP_IMSVT_IMCB_CONFIG_INIT_IND) {

            media_config_t ma;
            VT_IMCB_INIT   ua;

            memset(&ma, 0, sizeof(media_config_t));
            memset(&ua, 0, sizeof(VT_IMCB_INIT));

            // ================================================================
            // copy to ua config
            // ================================================================
            memcpy(&ua, reinterpret_cast<VT_IMCB_INIT *>(data), len);

            id = CONSTRUCT_SIM_CALL_ID(ua.setting.sim_slot_id, ua.config.call_id);
            helper->logAction("vt_callback", "call id", id, "MSG_ID_WRAP_IMSVT_IMCB_CONFIG_INIT_IND", VT_SRV_LOG_W);

            // ================================================================
            // Wait for CC FWK create provider
            // or this call will fail
            // ================================================================
            int count = 0;
            while (count < 100 && helper->check(id) != VT_SRV_RET_OK) {
                helper->logAction("vt_callback", "MSG_ID_WRAP_IMSVT_IMCB_CONFIG_INIT_IND", id, "this id is not init yet, wait for CC FWK", VT_SRV_LOG_E);
                usleep(200 * 1000);
                count++;
            }

            // ================================================================
            // copy ua config to ma config
            // ================================================================
            vt_ua_2_rtp(VT_SRV_CALL_4G, &ua, &(ma.rtp_rtcp_cap));
            vt_ua_2_codec(VT_SRV_CALL_4G, &ua, &(ma.codec_param));

            // store non-zero value for close
            // Both rtp/rtcp port will be 0 when voice call, not be 0 when ViLTE call
            if (0 != ua.config.local_rtp_port && 0 != ua.config.local_rtcp_port) {
                vt_srv_call_update_info_struct *info;

                ret = helper->getParam(id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
                if (ret) goto error;

                info->mVdoRtpPort = ua.config.local_rtp_port;
                info->mVdoRtcpPort = ua.config.local_rtcp_port;

                ret = helper->setParam(id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void *>(info));
                if (ret) goto error;
            }

            helper->logAction("vt_callback", "MSG_ID_WRAP_IMSVT_IMCB_CONFIG_INIT_IND, mode = ", id, helper->getCallModeString(ua.setting.mode), VT_SRV_LOG_W);

            // ================================================================
            // update ua config to table
            // ================================================================
            ret = helper->setParam(id, VT_SRV_PARAM_UA_CONFIG, reinterpret_cast<void *>(&ua));
            if (ret) goto error;

            // ================================================================
            // update ma config to table
            // ================================================================
            ret = helper->setParam(id, VT_SRV_PARAM_MA_CONFIG, reinterpret_cast<void *>(&ma));
            if (ret) goto error;

            core->init(id);

            VT_BOOL hasReceived = VT_TRUE;
            ret = helper->setParam(id, VT_SRV_PARAM_INIT_INFO, reinterpret_cast<void *>(&hasReceived));

        // update config case
        } else if (type == MSG_ID_WRAP_IMSVT_IMCB_CONFIG_UPDATE_IND) {

            media_config_t ma;
            VT_IMCB_UPD ua;
            VT_IMCB_UPD *pre_ua;

            memset(&ma, 0, sizeof(media_config_t));
            memset(&ua, 0, sizeof(VT_IMCB_UPD));

            // ================================================================
            // copy to ua config
            // ================================================================
            memcpy(&ua, reinterpret_cast<VT_IMCB_UPD *>(data), len);

            id = CONSTRUCT_SIM_CALL_ID(ua.setting.sim_slot_id, ua.config.call_id);
            helper->logAction("vt_callback", "call id", id, "MSG_ID_WRAP_IMSVT_IMCB_CONFIG_UPDATE_IND", VT_SRV_LOG_W);

            bool *isForceStop;
            ret = helper->getParam(id, VT_SRV_PARAM_IS_FORCE_STOP, reinterpret_cast<void **>(&isForceStop));

            if (ret) goto error;

            if ((*isForceStop) == VT_TRUE) {
                helper->logAction("vt_callback", "call id", id, "(MSG_ID_WRAP_IMSVT_IMCB_CONFIG_UPDATE_IND : isForceStopd !)", VT_SRV_LOG_W);
                return;
            }

            // for early media case, it may start only one way
            // it should be always send_recv after call is connected really.
            helper->logAction("vt_callback", "rtp_direction (before)", id, helper->getVideoStateString(ua.config.rtp_direction), VT_SRV_LOG_W);
            helper->logAction("vt_callback", "early_media_direction", id, helper->getVideoStateString(ua.setting.early_media_direction), VT_SRV_LOG_W);
            ua.config.rtp_direction = ua.config.rtp_direction & ua.setting.early_media_direction;
            helper->logAction("vt_callback", "rtp_direction (after)", id, helper->getVideoStateString(ua.config.rtp_direction), VT_SRV_LOG_W);

            // ================================================================
            // copy ua config to ma config
            // ================================================================
            vt_ua_2_rtp(VT_SRV_CALL_4G, &ua, &(ma.rtp_rtcp_cap));
            vt_ua_2_codec(VT_SRV_CALL_4G, &ua, &(ma.codec_param));

            // store non-zero value for close
            // Both rtp/rtcp port will be 0 when voice call, not be 0 when ViLTE call
            if (0 != ua.config.local_rtp_port && 0 != ua.config.local_rtcp_port) {
                vt_srv_call_update_info_struct *info;

                ret = helper->getParam(id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void **>(&info));
                if (ret) goto error;

                info->mVdoRtpPort = ua.config.local_rtp_port;
                info->mVdoRtcpPort = ua.config.local_rtcp_port;

                ret = helper->setParam(id, VT_SRV_PARAM_UPDATE_INFO, reinterpret_cast<void *>(info));
                if (ret) goto error;
            }

            if (ua.setting.mode == 0) {
                VT_BOOL isDowngraded = VT_FALSE;
                helper->setParam(id, VT_SRV_PARAM_IS_DOWNGRADE, reinterpret_cast<void *>(&isDowngraded));
            }

            // ================================================================
            // compare with old config to know what's difference
            // ================================================================
            ret  = helper->getParam(id, VT_SRV_PARAM_UA_CONFIG, reinterpret_cast<void**>(&pre_ua));

            core->getUpdateInfo(id, pre_ua, &ua);

            // ================================================================
            // update ua config to table
            // ================================================================
            ret = helper->setParam(id, VT_SRV_PARAM_UA_CONFIG, reinterpret_cast<void *>(&ua));
            if (ret) goto error;

            // ================================================================
            // update ma config to table
            // ================================================================
            ret = helper->setParam(id, VT_SRV_PARAM_MA_CONFIG, reinterpret_cast<void *>(&ma));
            if (ret) goto error;

            core->update(id);

            // ================================================================
            // clear flag
            // ================================================================
            core->resetUpdateInfo(id);

        } else if (type == MSG_ID_WRAP_IMSVT_IMCB_CONFIG_DEINIT_IND) {

            VT_IMCB_DEINIT ua;

            memset(&ua, 0, sizeof(VT_IMCB_DEINIT));

            // ================================================================
            // copy to ua config
            // ================================================================
            memcpy(&ua, reinterpret_cast<VT_IMCB_DEINIT *>(data), len);

            id = CONSTRUCT_SIM_CALL_ID(ua.setting.sim_slot_id, ua.config.call_id);
            helper->logAction("vt_callback", "call id", id, "MSG_ID_WRAP_IMSVT_IMCB_CONFIG_DEINIT_IND", VT_SRV_LOG_W);

            VT_BOOL *hasReceived;
            ret = helper->getParam(id, VT_SRV_PARAM_INIT_INFO, reinterpret_cast<void **>(&hasReceived));

            if (ret) return;

            if ((*hasReceived)) {
                (*hasReceived) = VT_FALSE;
                ret = helper->setParam(id, VT_SRV_PARAM_INIT_INFO, reinterpret_cast<void *>(hasReceived));
                core->close(id, MA_SOURCE_SINK);
            }

        } else if (type == MSG_ID_WRAP_IMSVT_IMCB_MODIFY_SESSION_IND) {

            VT_IMCB_IND Indication;

            memset(&Indication, 0, sizeof(VT_IMCB_IND));
            memcpy(&Indication, reinterpret_cast<VT_IMCB_IND *>(data), len);

            id = CONSTRUCT_SIM_CALL_ID(Indication.sim_slot_id, Indication.call_id);
            helper->logAction("vt_callback", "call id", id, "MSG_ID_WRAP_IMSVT_IMCB_MODIFY_SESSION_IND", VT_SRV_LOG_W);

            VT_BOOL *isForceCancel;
            int ret = helper->getParam(id, VT_SRV_PARAM_IS_FORCE_CANCEL, reinterpret_cast<void **>(&isForceCancel));
            if ((VT_SRV_RET_OK == ret) && (*isForceCancel) == VT_TRUE) {
                helper->logFlow("MSG_ID_WRAP_IMSVT_IMCB_MODIFY_SESSION_IND", "clear force cancel!", VT_IVD, "", VT_SRV_LOG_I);
                *isForceCancel = VT_FALSE;
            }

            sp<VideoProfile> Vp = helper->getProfileFromInd(&Indication);

            helper->logAction("vt_callback", "Vp->getState", id, helper->getVideoStateString(Vp->getState()), VT_SRV_LOG_W);

            ret = helper->setParam(id, VT_SRV_PARAM_REMOTE_SESSION_REQ, reinterpret_cast<void *>(&Indication));
            if (ret) goto error;

            core->notifyCallback(
                id,
                VT_SRV_NOTIFY_RECV_SESSION_CONFIG_REQ,
                0,
                0,
                0,
                helper->packFromVdoProfile(Vp),
                String8(""),
                NULL);

        } else if (type == MSG_ID_WRAP_IMSVT_IMCB_MODIFY_SESSION_CNF) {

            VT_IMCB_CNF confrim;

            memset(&confrim, 0, sizeof(VT_IMCB_CNF));
            memcpy(&confrim, reinterpret_cast<VT_IMCB_CNF *>(data), len);

            id = CONSTRUCT_SIM_CALL_ID(confrim.sim_slot_id, confrim.call_id);
            helper->logAction("vt_callback", "call id", id, "MSG_ID_WRAP_IMSVT_IMCB_MODIFY_SESSION_CNF", VT_SRV_LOG_W);

            sp<VideoProfile> Vp = helper->getProfileFromCnf(&confrim);

            helper->logAction("vt_callback", "Vp->getState()", id, helper->getVideoStateString(Vp->getState()), VT_SRV_LOG_W);
            helper->logAction("vt_callback", "Vp->getResult()", id, helper->getSessionModifyResultString(Vp->getResult()), VT_SRV_LOG_W);

            VT_IMCB_REQ * request;
            ret = helper->getParam(id, VT_SRV_PARAM_SESSION_REQ, reinterpret_cast<void **>(&request));
            if (ret) goto error;

            sp<VideoProfile> Vp_r = helper->getProfileFromCnf(request);

            helper->logAction("vt_callback", "Vp_r->getState", id, helper->getVideoStateString(Vp_r->getState()), VT_SRV_LOG_W);

            core->notifyCallback(
                id,
                VT_SRV_NOTIFY_RECV_SESSION_CONFIG_RSP,
                Vp->getResult(),
                0,
                0,
                helper->packFromVdoProfile(Vp_r),
                helper->packFromVdoProfile(Vp),
                NULL);
        }  else if (type == MSG_ID_WRAP_IMSVT_IMCB_HANDOVER_START_IND) {

            helper->logAction("vt_callback", "MSG_ID_WRAP_IMSVT_IMCB_HANDOVER_START_IND", VT_IVD, "", VT_SRV_LOG_W);

            VT_IMCB_HOSTART hostStart;

            memset(&hostStart, 0, sizeof(VT_IMCB_HOSTART));
            memcpy(&hostStart, reinterpret_cast<VT_IMCB_HOSTART *>(data), len);

            core->setHandoverState(VT_TRUE);

        }  else if (type == MSG_ID_WRAP_IMSVT_IMCB_HANDOVER_STOP_IND) {

            helper->logAction("vt_callback", "MSG_ID_WRAP_IMSVT_IMCB_HANDOVER_STOP_IND", VT_IVD, "", VT_SRV_LOG_W);

            VT_IMCB_HOSTOP hostStop;

            memset(&hostStop, 0, sizeof(VT_IMCB_HOSTOP));
            memcpy(&hostStop, reinterpret_cast<VT_IMCB_HOSTOP *>(data), len);

            core->setHandoverState(VT_FALSE);

        } else if (type == MSG_ID_WRAP_IMSVT_IMCB_MODIFY_SESSION_CANCEL_IND) {

            helper->logAction("vt_callback", "MSG_ID_WRAP_IMSVT_IMCB_MODIFY_CANCEL_IND", VT_IVD, "", VT_SRV_LOG_W);

            VT_IMCB_CANCEL_IND ind;

            memset(&ind, 0, sizeof(VT_IMCB_CANCEL_IND));
            memcpy(&ind, reinterpret_cast<VT_IMCB_CANCEL_IND *>(data), len);

            id = CONSTRUCT_SIM_CALL_ID(ind.sim_slot_id, ind.call_id);

            sp<VideoProfile> Vp = helper->getProfileFromCancelInd(&ind);

            helper->logFlow("Cancel", "id", id, "", VT_SRV_LOG_I);

            VT_BOOL *mIsForceCancel;
            ret = helper->getParam(id, VT_SRV_PARAM_IS_FORCE_CANCEL, reinterpret_cast<void **>(&mIsForceCancel));
            if (ret) {
                helper->logFlow("Cancel", "this call has removed, id", id, "", VT_SRV_LOG_E);
                return;
            }

            (*mIsForceCancel) = VT_TRUE;

            core->notifyCallback(
                id,
                VT_SRV_NOTIFY_RECV_CANCEL_SESSION_IND,
                0,
                0,
                0,
                helper->packFromVdoProfile(Vp),
                String8(""),
                NULL);

        } else if (type == MSG_ID_WRAP_IMSVT_IMCB_MODIFY_SESSION_CANCEL_CNF) {

            helper->logAction("vt_callback", "MSG_ID_WRAP_IMSVT_IMCB_MODIFY_CANCEL_CNF", VT_IVD, "", VT_SRV_LOG_W);

            VT_IMCB_CANCEL_CNF confirm;

            memset(&confirm, 0, sizeof(VT_IMCB_CANCEL_CNF));
            memcpy(&confirm, reinterpret_cast<VT_IMCB_CANCEL_CNF *>(data), len);

            id = CONSTRUCT_SIM_CALL_ID(confirm.sim_slot_id, confirm.call_id);

            sp<VideoProfile> Vp = helper->getProfileFromCancelCnf(&confirm);

            helper->logAction("vt_callback", "Vp->getResult", Vp->getResult(), "", VT_SRV_LOG_W);

            VT_IMCB_CANCEL_REQ * request;
            ret = helper->getParam(id, VT_SRV_PARAM_CANCEL_SESSION_REQ, reinterpret_cast<void **>(&request));
            if (ret) goto error;

            core->notifyCallback(
                id,
                VT_SRV_NOTIFY_RECV_SESSION_CONFIG_RSP,
                Vp->getResult(),
                0,
                0,
                helper->packFromVdoProfile(Vp),
                helper->packFromVdoProfile(Vp),
                NULL);

        } else if (type == MSG_ID_WRAP_IMSVT_IMCB_EVENT_LOCAL_BW_READY_IND) {

            helper->logAction("vt_callback", "MSG_ID_WRAP_IMSVT_IMCB_EVENT_LOCAL_BW_READY_IND", VT_IVD, "", VT_SRV_LOG_W);

            VT_IMCB_UPGRADE_BW_READY_IND bw_ind;

            memset(&bw_ind, 0, sizeof(VT_IMCB_UPGRADE_BW_READY_IND));
            memcpy(&bw_ind, reinterpret_cast<VT_IMCB_UPGRADE_BW_READY_IND *>(data), len);

            id = CONSTRUCT_SIM_CALL_ID(bw_ind.sim_slot_id, bw_ind.call_id);

            core->notifyCallback(
                id,
                VT_SRV_NOTIFY_BW_READY_IND);
        }

        return;

        error:
            helper->logFlow("vt_callback", "call id", id, helper->getErrorString(ret), VT_SRV_LOG_E);
            core->notifyCallback(id, notify_type);
            return;
    }

    void vt_rtp_codec_2_ua(int mode, VT_IMCB_CAP * ua, int ssid, int major_ssid) {

        if (mode == VT_SRV_CALL_4G) {
            ua->sim_slot_id = ssid;

            // ====================================================================
            // rtcp part
            // ====================================================================
            rtp_rtcp_capability_t *rtpCap;
            int rtp_num = ImsMa::getRtpRtcpCapability(&rtpCap);

            // ====================================================================
            // codec part
            // ====================================================================
            video_codec_fmtp_t *codecCap;
            int codec_num = ImsMa::getCodecCapability(&codecCap, NULL, major_ssid);

            while (codec_num == 0) {
                usleep(100 * 1000);

                VT_LOGI("[SRV] [vt_rtp_codec_2_ua] codec_num = %d, retry!!", codec_num);
                codec_num = ImsMa::getCodecCapability(&codecCap, NULL, major_ssid);
            }

            VT_LOGI("[SRV] [vt_rtp_codec_2_ua] codec_num = %d", codec_num);

            //Save default local size W/H for preview, must always notify w>h
            sp<VTCoreHelper> helper = VTCore::mHelper;
            int w = 320, h = 240;
            if (codec_num != 0 && codecCap != NULL) {
                w = MAX(codecCap[0].width, codecCap[0].height);
                h = MIN(codecCap[0].width, codecCap[0].height);
            }
            helper->setDefaultLocalSize(w, h);
            VT_LOGI("[SRV] [vt_rtp_codec_2_ua] Save default local size W=%d, H=%d)", w, h);

            // ====================================================================
            // SPS part
            // ====================================================================
            uint32_t levelCapNumbers;
            video_codec_level_fmtp_t *codeclevelcap;

            ImsMa::getCodecParameterSets(VIDEO_H264, &levelCapNumbers, &codeclevelcap, major_ssid);

            VT_LOGI("[SRV] [vt_rtp_codec_2_ua] (H264) levelCapNumbers = %d (need <= %d)", levelCapNumbers, VT_MAX_PS_NUM);

            for (int i = 0; i < levelCapNumbers && i < VT_MAX_PS_NUM; i++) {
                ua->h264_ps[i].profile_level_id             = codeclevelcap[i].codec_level_fmtp.h264_codec_level_fmtp.profile_level_id;
                memcpy(ua->h264_ps[i].sprop_parameter_sets  , codeclevelcap[i].codec_level_fmtp.h264_codec_level_fmtp.sprop_parameter_sets, VT_MAX_SDP_PARAMETER_SET_LENGTH);
            }

            ImsMa::getCodecParameterSets(VIDEO_HEVC, &levelCapNumbers, &codeclevelcap, major_ssid);

            VT_LOGI("[SRV] [vt_rtp_codec_2_ua] (H265) levelCapNumbers = %d (need <= %d)", levelCapNumbers, VT_MAX_PS_NUM);

            for (int i = 0; i < levelCapNumbers && i < VT_MAX_PS_NUM; i++) {
                ua->hevc_ps[i].profile_id                   = codeclevelcap[i].codec_level_fmtp.hevc_codec_level_fmtp.profile_id;
                ua->hevc_ps[i].level_id                     = codeclevelcap[i].codec_level_fmtp.hevc_codec_level_fmtp.level_id;
                memcpy(ua->hevc_ps[i].sprop_vps             , codeclevelcap[i].codec_level_fmtp.hevc_codec_level_fmtp.sprop_vps, VT_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(ua->hevc_ps[i].sprop_sps             , codeclevelcap[i].codec_level_fmtp.hevc_codec_level_fmtp.sprop_sps, VT_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(ua->hevc_ps[i].sprop_pps             , codeclevelcap[i].codec_level_fmtp.hevc_codec_level_fmtp.sprop_pps, VT_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(ua->hevc_ps[i].sprop_sei             , codeclevelcap[i].codec_level_fmtp.hevc_codec_level_fmtp.sprop_sei, VT_MAX_SDP_PARAMETER_SET_LENGTH);
            }

            // ====================================================================
            // bit rate table part
            // ====================================================================
            unsigned int tableCnt;
            video_media_bitrate_t *bitrateTable;
            ImsMa::getCodecBitrateTable(&tableCnt, &bitrateTable, major_ssid);

            VT_LOGI("[SRV] [vt_rtp_codec_2_ua] bit rate table Cnt     = %d (need <= %d)", tableCnt, VT_MAX_LEVEL_SIZE);

            for (int k = 0; k < tableCnt && k < VT_MAX_LEVEL_SIZE; k++) {
                ua->bitrate_info[k].format                  = bitrateTable[k].format;
                ua->bitrate_info[k].profile                 = bitrateTable[k].profile;
                ua->bitrate_info[k].level                   = bitrateTable[k].level;
                ua->bitrate_info[k].minbitrate              = bitrateTable[k].minbitrate;
                ua->bitrate_info[k].bitrate                 = bitrateTable[k].bitrate;
            }

            // ====================================================================
            // merge rtp/codec with type
            // ====================================================================
            int final_num = 0;
            for (int i = 0; i < codec_num; i++) {
                for (int j =0; j < rtp_num; j++) {
                    if (codecCap[i].format == rtpCap[j].mime_Type) {

                        VT_LOGI("[SRV] [vt_rtp_codec_2_ua] find match type = %d, count = %d", rtpCap[j].mime_Type, final_num);

                        vt_srv_video_capability_t *cap = &(ua->video_cap[final_num]);

                        cap->mime_Type = codecCap[i].format;
                        if (cap->mime_Type == VIDEO_H264) {
                            cap->codec_cap.h264_codec.profile_level_id                  = codecCap[i].codec_fmtp.h264_codec_fmtp.profile_level_id;
                            cap->codec_cap.h264_codec.max_recv_level                    = codecCap[i].codec_fmtp.h264_codec_fmtp.max_recv_level;
                            cap->codec_cap.h264_codec.redundant_pic_cap                 = codecCap[i].codec_fmtp.h264_codec_fmtp.redundant_pic_cap;
                            memcpy(cap->codec_cap.h264_codec.sprop_parameter_sets       , codecCap[i].codec_fmtp.h264_codec_fmtp.sprop_parameter_sets       , VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                            memcpy(cap->codec_cap.h264_codec.sprop_level_parameter_sets , codecCap[i].codec_fmtp.h264_codec_fmtp.sprop_level_parameter_sets , VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                            cap->codec_cap.h264_codec.max_mbps                          = codecCap[i].codec_fmtp.h264_codec_fmtp.max_mbps;
                            cap->codec_cap.h264_codec.max_smbps                         = codecCap[i].codec_fmtp.h264_codec_fmtp.max_smbps;
                            cap->codec_cap.h264_codec.max_fs                            = codecCap[i].codec_fmtp.h264_codec_fmtp.max_fs;
                            cap->codec_cap.h264_codec.max_cpb                           = codecCap[i].codec_fmtp.h264_codec_fmtp.max_cpb;
                            cap->codec_cap.h264_codec.max_dpb                           = codecCap[i].codec_fmtp.h264_codec_fmtp.max_dpb;
                            cap->codec_cap.h264_codec.max_br                            = codecCap[i].codec_fmtp.h264_codec_fmtp.max_br;
                            cap->codec_cap.h264_codec.max_rcmd_nalu_size                = codecCap[i].codec_fmtp.h264_codec_fmtp.max_rcmd_nalu_size;
                            cap->codec_cap.h264_codec.sar_understood                    = codecCap[i].codec_fmtp.h264_codec_fmtp.sar_understood;
                            cap->codec_cap.h264_codec.sar_supported                     = codecCap[i].codec_fmtp.h264_codec_fmtp.sar_supported;
                            cap->codec_cap.h264_codec.in_band_parameter_sets            = 0;
                            cap->codec_cap.h264_codec.level_asymmetry_allowed           = 0;

                            cap->codec_cap.h264_codec.framerate                         = codecCap[i].fps;
                            cap->codec_cap.h264_codec.frame_width                       = codecCap[i].width;
                            cap->codec_cap.h264_codec.frame_height                      = codecCap[i].height;
                            for (int k = 0; k < VT_MAX_IMAGE_NUM; k++ ) {
                                cap->codec_cap.h264_codec.image_send[k].x               = codecCap[i].image_send[k].x;
                                cap->codec_cap.h264_codec.image_send[k].y               = codecCap[i].image_send[k].y;
                                memcpy(cap->codec_cap.h264_codec.image_send[k].sar      , codecCap[i].image_send[k].sar, VT_MAX_SDP_EGBYTE_LENGTH);
                                memcpy(cap->codec_cap.h264_codec.image_send[k].par      , codecCap[i].image_send[k].par, VT_MAX_SDP_EGBYTE_LENGTH);
                                memcpy(cap->codec_cap.h264_codec.image_send[k].q        , codecCap[i].image_send[k].q,   VT_MAX_SDP_EGBYTE_LENGTH);
                            }
                            for (int k = 0; k < VT_MAX_IMAGE_NUM; k++ ) {
                                cap->codec_cap.h264_codec.image_recv[k].x               = codecCap[i].image_recv[k].x;
                                cap->codec_cap.h264_codec.image_recv[k].y               = codecCap[i].image_recv[k].y;
                                memcpy(cap->codec_cap.h264_codec.image_recv[k].sar      , codecCap[i].image_recv[k].sar, VT_MAX_SDP_EGBYTE_LENGTH);
                                memcpy(cap->codec_cap.h264_codec.image_recv[k].par      , codecCap[i].image_recv[k].par, VT_MAX_SDP_EGBYTE_LENGTH);
                                memcpy(cap->codec_cap.h264_codec.image_recv[k].q        , codecCap[i].image_recv[k].q,   VT_MAX_SDP_EGBYTE_LENGTH);
                            }

                        } else if (cap->mime_Type == VIDEO_HEVC) {
                            cap->codec_cap.hevc_codec.profile_space                     = codecCap[i].codec_fmtp.hevc_codec_fmtp.profile_space;
                            cap->codec_cap.hevc_codec.profile_id                        = codecCap[i].codec_fmtp.hevc_codec_fmtp.profile_id;
                            cap->codec_cap.hevc_codec.tier_flag                         = codecCap[i].codec_fmtp.hevc_codec_fmtp.tier_flag;
                            cap->codec_cap.hevc_codec.level_id                          = codecCap[i].codec_fmtp.hevc_codec_fmtp.level_id;

                            for (int k = 0; k < 6; k++) {
                                cap->codec_cap.hevc_codec.interop_constraints[k]        = codecCap[i].codec_fmtp.hevc_codec_fmtp.interop_constraints[k];
                            }
                            for (int k = 0; k < 4; k++) {
                                cap->codec_cap.hevc_codec.profile_comp_ind[k]           = codecCap[i].codec_fmtp.hevc_codec_fmtp.profile_comp_ind[k];
                            }

                            cap->codec_cap.hevc_codec.sprop_sub_layer_id                = codecCap[i].codec_fmtp.hevc_codec_fmtp.sprop_sub_layer_id;
                            cap->codec_cap.hevc_codec.recv_sub_layer_id                 = codecCap[i].codec_fmtp.hevc_codec_fmtp.recv_sub_layer_id;
                            cap->codec_cap.hevc_codec.max_recv_level_id                 = codecCap[i].codec_fmtp.hevc_codec_fmtp.max_recv_level_id;
                            cap->codec_cap.hevc_codec.tx_mode                           = codecCap[i].codec_fmtp.hevc_codec_fmtp.tx_mode;

                            memcpy(cap->codec_cap.hevc_codec.sprop_vps                  , codecCap[i].codec_fmtp.hevc_codec_fmtp.sprop_vps, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                            memcpy(cap->codec_cap.hevc_codec.sprop_sps                  , codecCap[i].codec_fmtp.hevc_codec_fmtp.sprop_sps, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                            memcpy(cap->codec_cap.hevc_codec.sprop_pps                  , codecCap[i].codec_fmtp.hevc_codec_fmtp.sprop_pps, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                            memcpy(cap->codec_cap.hevc_codec.sprop_sei                  , codecCap[i].codec_fmtp.hevc_codec_fmtp.sprop_sei, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);

                            cap->codec_cap.hevc_codec.max_lsr                           = codecCap[i].codec_fmtp.hevc_codec_fmtp.max_lsr;
                            cap->codec_cap.hevc_codec.max_lps                           = codecCap[i].codec_fmtp.hevc_codec_fmtp.max_lps;
                            cap->codec_cap.hevc_codec.max_cpb                           = codecCap[i].codec_fmtp.hevc_codec_fmtp.max_cpb;
                            cap->codec_cap.hevc_codec.max_dpb                           = codecCap[i].codec_fmtp.hevc_codec_fmtp.max_dpb;
                            cap->codec_cap.hevc_codec.max_br                            = codecCap[i].codec_fmtp.hevc_codec_fmtp.max_br;
                            cap->codec_cap.hevc_codec.max_tr                            = codecCap[i].codec_fmtp.hevc_codec_fmtp.max_tr;
                            cap->codec_cap.hevc_codec.max_tc                            = codecCap[i].codec_fmtp.hevc_codec_fmtp.max_tc;
                            cap->codec_cap.hevc_codec.max_fps                           = codecCap[i].codec_fmtp.hevc_codec_fmtp.max_fps;
                            cap->codec_cap.hevc_codec.sprop_max_don_diff                = codecCap[i].codec_fmtp.hevc_codec_fmtp.sprop_max_don_diff;
                            cap->codec_cap.hevc_codec.sprop_depack_buf_nalus            = codecCap[i].codec_fmtp.hevc_codec_fmtp.sprop_depack_buf_nalus;
                            cap->codec_cap.hevc_codec.sprop_depack_buf_bytes            = codecCap[i].codec_fmtp.hevc_codec_fmtp.sprop_depack_buf_bytes;
                            cap->codec_cap.hevc_codec.depack_buf_cap                    = codecCap[i].codec_fmtp.hevc_codec_fmtp.depack_buf_cap;
                            cap->codec_cap.hevc_codec.sprop_seg_id                      = codecCap[i].codec_fmtp.hevc_codec_fmtp.sprop_seg_id;
                            cap->codec_cap.hevc_codec.sprop_spatial_seg_idc             = codecCap[i].codec_fmtp.hevc_codec_fmtp.sprop_spatial_seg_idc;

                            memcpy(cap->codec_cap.hevc_codec.dec_parallel_cap           , codecCap[i].codec_fmtp.hevc_codec_fmtp.dec_parallel_cap, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                            memcpy(cap->codec_cap.hevc_codec.include_dph                , codecCap[i].codec_fmtp.hevc_codec_fmtp.include_dph     , VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);

                            cap->codec_cap.hevc_codec.framerate                         = codecCap[i].fps;
                            cap->codec_cap.hevc_codec.frame_width                       = codecCap[i].width;
                            cap->codec_cap.hevc_codec.frame_height                      = codecCap[i].height;
                            for (int k = 0; k < VT_MAX_IMAGE_NUM; k++ ) {
                                cap->codec_cap.hevc_codec.image_send[k].x               = codecCap[i].image_send[k].x;
                                cap->codec_cap.hevc_codec.image_send[k].y               = codecCap[i].image_send[k].y;
                                memcpy(cap->codec_cap.hevc_codec.image_send[k].sar      , codecCap[i].image_send[k].sar, VT_MAX_SDP_EGBYTE_LENGTH);
                                memcpy(cap->codec_cap.hevc_codec.image_send[k].par      , codecCap[i].image_send[k].par, VT_MAX_SDP_EGBYTE_LENGTH);
                                memcpy(cap->codec_cap.hevc_codec.image_send[k].q        , codecCap[i].image_send[k].q,   VT_MAX_SDP_EGBYTE_LENGTH);
                            }
                            for (int k = 0; k < VT_MAX_IMAGE_NUM; k++ ) {
                                cap->codec_cap.hevc_codec.image_recv[k].x               = codecCap[i].image_recv[k].x;
                                cap->codec_cap.hevc_codec.image_recv[k].y               = codecCap[i].image_recv[k].y;
                                memcpy(cap->codec_cap.hevc_codec.image_recv[k].sar      , codecCap[i].image_recv[k].sar, VT_MAX_SDP_EGBYTE_LENGTH);
                                memcpy(cap->codec_cap.hevc_codec.image_recv[k].par      , codecCap[i].image_recv[k].par, VT_MAX_SDP_EGBYTE_LENGTH);
                                memcpy(cap->codec_cap.hevc_codec.image_recv[k].q        , codecCap[i].image_recv[k].q,   VT_MAX_SDP_EGBYTE_LENGTH);
                            }
                        }

                        cap->media_type                  = rtpCap[j].media_type;
                        cap->mime_Type                   = rtpCap[j].mime_Type;
                        cap->rtp_profile                 = rtpCap[j].rtp_profile;
                        cap->sample_rate                 = rtpCap[j].sample_rate;
                        cap->packetize_mode              = rtpCap[j].packetize_mode;
                        cap->rtcp_rsize                  = rtpCap[j].rtcp_reduce_size;

                        cap->rtcp_fb_param_num           = rtpCap[j].rtcp_fb_param_num;
                        memcpy(cap->rtcp_fb_type         , rtpCap[j].rtcp_fb_type, sizeof(rtcp_fb_param_type_t) * VT_MAX_RTCP_FB_SIZE);

                        cap->extmap_num = rtpCap[j].rtp_header_extension_num;
                        VT_LOGI("[SRV] [vt_rtp_codec_2_ua] extmap_num             = %d", cap->extmap_num);
                        for (int k = 0; k < cap->extmap_num; k++) {
                            cap->extmap[k].ext_id        = rtpCap[j].rtp_ext_map[k].extension_id;
                            cap->extmap[k].direction     = rtpCap[j].rtp_ext_map[k].direction;
                            memcpy(cap->extmap[k].ext_uri, rtpCap[j].rtp_ext_map[k].extension_uri, VT_MAX_EXTMAP_URI_SIZE);
                            VT_LOGI("[SRV] [vt_rtp_codec_2_ua] ext_id                 = %d", cap->extmap[k].ext_id);
                            VT_LOGI("[SRV] [vt_rtp_codec_2_ua] direction              = %d", cap->extmap[k].direction);
                            VT_LOGI("[SRV] [vt_rtp_codec_2_ua] ext_uri                = %s", cap->extmap[k].ext_uri);
                        }
                        VT_LOGI("[SRV] [vt_rtp_codec_2_ua] cap.media_type         = %d", cap->media_type);
                        VT_LOGI("[SRV] [vt_rtp_codec_2_ua] cap.mime_Type          = %d", cap->mime_Type);

                        final_num++;
                    }
                }
            }
            ua->video_cap_num = final_num;

        }
    }

    void vt_ua_2_rtp(int mode, VT_IMCB_CONFIG * ua, rtp_rtcp_config_t * rtp) {

        if (mode == VT_SRV_CALL_4G) {
            // ====================================================================
            // rtcp part
            // ====================================================================
            vt_srv_imcb_msg_config_t *cap = &(ua->config);

            VT_LOGI("[SRV] [vt_ua_2_rtp] media_type              = %d", cap->media_type);
            VT_LOGI("[SRV] [vt_ua_2_rtp] mime_Type               = %d", cap->mime_Type);
            VT_LOGI("[SRV] [vt_ua_2_rtp] packetize_mode          = %d", cap->packetize_mode);
            VT_LOGI("[SRV] [vt_ua_2_rtp] rtcp_sender_bandwidth   = %d", cap->video_b_rs);
            VT_LOGI("[SRV] [vt_ua_2_rtp] rtcp_receiver_bandwidth = %d", cap->video_b_rr);
            VT_LOGI("[SRV] [vt_ua_2_rtp] ebi                     = %d", ua->setting.ebi);
            VT_LOGI("[SRV] [vt_ua_2_rtp] network_id              = %d", ua->setting.network_id);
            VT_LOGI("[SRV] [vt_ua_2_rtp] rtp_direction           = %d", cap->rtp_direction);

            // cap->call_id;
            // cap->camera_direction;

            rtp->media_type                         = cap->media_type;
            rtp->mime_Type                          = cap->mime_Type;
            rtp->rtp_payload_type                   = cap->video_payload_type;
            rtp->rtp_profile                        = cap->rtp_profile;
            rtp->sample_rate                        = cap->sample_rate;
            rtp->rtp_packet_bandwidth               = cap->video_b_as;
            rtp->packetize_mode                     = cap->packetize_mode;
            //rtp->rtp_header_extension_num;          = ;
            //rtp->rtp_ext_map[0]                     = ;
            rtp->rtcp_sender_bandwidth              = cap->video_b_rs;
            rtp->rtcp_receiver_bandwidth            = cap->video_b_rr;
            rtp->rtcp_reduce_size                   = cap->rtcp_rsize;

            rtp->rtcp_fb_param_num                  = cap->rtcp_fb_param_num;
            memcpy(rtp->rtcp_fb_type                , cap->rtcp_fb_type, sizeof(vt_srv_rtcp_fb_param_type_t) * VT_MAX_RTCP_FB_SIZE);

            rtp->network_info.ebi                   = ua->setting.ebi;
            rtp->network_info.interface_type        = ua->setting.video_type;
            rtp->network_info.dscp                  = cap->video_dscp;
            rtp->network_info.soc_priority          = cap->video_soc_priority;

            rtp->network_info.remote_rtp_port       = cap->remote_rtp_port;
            rtp->network_info.remote_rtcp_port      = cap->remote_rtcp_port;
            rtp->network_info.local_rtp_port        = cap->local_rtp_port;
            rtp->network_info.local_rtcp_port       = cap->local_rtcp_port;
            memcpy(rtp->network_info.remote_rtp_address       , cap->remote_rtp_address,    VT_ADDR_LENGTH);
            memcpy(rtp->network_info.remote_rtcp_address      , cap->remote_rtcp_address,   VT_ADDR_LENGTH);
            memcpy(rtp->network_info.local_rtp_address        , cap->local_address,         VT_ADDR_LENGTH);
            memcpy(rtp->network_info.local_rtcp_address       , cap->local_address,         VT_ADDR_LENGTH);
            rtp->network_info.remote_addr_type      = cap->remote_addr_type;
            rtp->network_info.local_addr_type       = cap->remote_addr_type;

            rtp->network_info.network_id            = ua->setting.network_id;
            memcpy(rtp->network_info.ifname                   , ua->setting.if_name,        VT_MAX_IF_NAME_LENGTH);
            rtp->network_info.uid                   = g_vt.core->mUserID;
            rtp->network_info.rtp_direction         = cap->rtp_direction;

            // Video over LTE case
            int id = CONSTRUCT_SIM_CALL_ID(ua->setting.sim_slot_id, ua->config.call_id);

            if (ua->setting.video_type == 0) {

                rtp->network_info.tag = TAG_VILTE_MOBILE + id;

            // Video over Wifi case
            } else {

                rtp->network_info.tag = TAG_VILTE_WIFI + id;
            }

            for (int i = 0; i < VILTE_BIND_FD_NUMBER; i++) {
                rtp->network_info.socket_fds[i]     = -1;
            }

            rtp->network_info.MBR_DL                = ua->setting.nw_assigned_dl_bw;
            rtp->network_info.MBR_UL                = ua->setting.nw_assigned_ul_bw;

            uint32_t rtp_header_extension_num;
            rtp_ext_map_t rtp_ext_map[1];//now only CVO support

            rtp->rtp_header_extension_num           = 1;
            for (int i = 0; i < rtp->rtp_header_extension_num; i++) {
                rtp->rtp_ext_map[i].extension_id    = cap->extmap.ext_id ;
                rtp->rtp_ext_map[i].direction       = cap->extmap.direction;
                memcpy(rtp->rtp_ext_map[i].extension_uri, cap->extmap.ext_uri, VT_MAX_EXTMAP_URI_SIZE);
                VT_LOGI("[SRV] [vt_ua_2_rtp] extension_id            = %d", cap->extmap.ext_id);
                VT_LOGI("[SRV] [vt_ua_2_rtp] direction               = %d", cap->extmap.direction);
                VT_LOGI("[SRV] [vt_ua_2_rtp] extension_uri           = %s", cap->extmap.ext_uri);
            }

        }
    }

    void vt_ua_2_codec(int mode, VT_IMCB_CONFIG * ua, video_codec_fmtp_t * codec) {

        if (mode == VT_SRV_CALL_4G) {

            // ====================================================================
            // codec part
            // ====================================================================
            vt_srv_video_codec_cap_cfg_t *codec_cfg = &(ua->config.codec_cfg);

            VT_LOGI("[SRV] [vt_ua_2_codec] format                = %d", ua->config.mime_Type);
            VT_LOGI("[SRV] [vt_ua_2_codec] profile_level_id      = %d", codec_cfg->h264_codec.profile_level_id);

            codec->format                           = (video_format_t) ua->config.mime_Type;
            codec->fps                              = -1;

            if (VIDEO_H264 == codec->format) {

                VT_LOGI("[SRV] [vt_ua_2_codec] format                = H264");
                VT_LOGI("[SRV] [vt_ua_2_codec] profile_level_id      = %d", codec_cfg->h264_codec.profile_level_id);

                codec->codec_fmtp.h264_codec_fmtp.profile_level_id                 = codec_cfg->h264_codec.profile_level_id;
                codec->codec_fmtp.h264_codec_fmtp.max_recv_level                   = codec_cfg->h264_codec.max_recv_level;
                codec->codec_fmtp.h264_codec_fmtp.redundant_pic_cap                = codec_cfg->h264_codec.redundant_pic_cap;

                memcpy(codec->codec_fmtp.h264_codec_fmtp.sprop_parameter_sets      , codec_cfg->h264_codec.sprop_parameter_sets,       VT_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(codec->codec_fmtp.h264_codec_fmtp.sprop_level_parameter_sets, codec_cfg->h264_codec.sprop_level_parameter_sets, VT_MAX_SDP_PARAMETER_SET_LENGTH);

                codec->codec_fmtp.h264_codec_fmtp.max_mbps                         = codec_cfg->h264_codec.max_mbps;
                codec->codec_fmtp.h264_codec_fmtp.max_smbps                        = codec_cfg->h264_codec.max_smbps;
                codec->codec_fmtp.h264_codec_fmtp.max_fs                           = codec_cfg->h264_codec.max_fs;
                codec->codec_fmtp.h264_codec_fmtp.max_cpb                          = codec_cfg->h264_codec.max_cpb;
                codec->codec_fmtp.h264_codec_fmtp.max_dpb                          = codec_cfg->h264_codec.max_dpb;
                codec->codec_fmtp.h264_codec_fmtp.max_br                           = codec_cfg->h264_codec.max_br;

                codec->codec_fmtp.h264_codec_fmtp.max_rcmd_nalu_size               = codec_cfg->h264_codec.max_rcmd_nalu_size;
                codec->codec_fmtp.h264_codec_fmtp.sar_understood                   = codec_cfg->h264_codec.sar_understood;
                codec->codec_fmtp.h264_codec_fmtp.sar_supported                    = codec_cfg->h264_codec.sar_supported;

                VT_LOGI("[SRV] [vt_ua_2_codec] video_type            = %d", ua->setting.video_type);
                VT_LOGI("[SRV] [vt_ua_2_codec] video_b_as            = %d", ua->config.video_b_as);
                VT_LOGI("[SRV] [vt_ua_2_codec] nw_assigned_ul_bw     = %d", ua->setting.nw_assigned_ul_bw);

                // Video over LTE case
                if (ua->setting.video_type == 0) {

                    // if nw_assigned_ul_bw = 0, just use another one
                    if (ua->setting.nw_assigned_ul_bw != 0) {
                        if (ua->setting.nw_assigned_ul_bw > ua->config.video_b_as) {
                            codec->codec_fmtp.h264_codec_fmtp.video_b_as           = ua->config.video_b_as;
                        } else {
                            codec->codec_fmtp.h264_codec_fmtp.video_b_as           = ua->setting.nw_assigned_ul_bw;
                        }
                    } else {
                        codec->codec_fmtp.h264_codec_fmtp.video_b_as               = ua->config.video_b_as;
                    }

                // Video over Wifi case
                } else {
                    codec->codec_fmtp.h264_codec_fmtp.video_b_as                   = ua->config.video_b_as;
                }

            } else if (VIDEO_HEVC == codec->format) {

                VT_LOGI("[SRV] [vt_ua_2_codec] format                = H265");
                VT_LOGI("[SRV] [vt_ua_2_codec] profile_id            = %d", codec_cfg->hevc_codec.profile_id);

                codec->codec_fmtp.hevc_codec_fmtp.profile_space                    = codec_cfg->hevc_codec.profile_space;
                codec->codec_fmtp.hevc_codec_fmtp.tier_flag                        = codec_cfg->hevc_codec.tier_flag;
                codec->codec_fmtp.hevc_codec_fmtp.profile_id                       = codec_cfg->hevc_codec.profile_id;
                codec->codec_fmtp.hevc_codec_fmtp.level_id                         = codec_cfg->hevc_codec.level_id;

                for (int i = 0; i < 6;i++) {
                    codec->codec_fmtp.hevc_codec_fmtp.interop_constraints[i]       = codec_cfg->hevc_codec.interop_constraints[i];
                }

                for (int i = 0; i < 4;i++) {
                    codec->codec_fmtp.hevc_codec_fmtp.profile_comp_ind[i]          = codec_cfg->hevc_codec.profile_comp_ind[i];
                }

                codec->codec_fmtp.hevc_codec_fmtp.sprop_sub_layer_id               = codec_cfg->hevc_codec.sprop_sub_layer_id;
                codec->codec_fmtp.hevc_codec_fmtp.recv_sub_layer_id                = codec_cfg->hevc_codec.recv_sub_layer_id;
                codec->codec_fmtp.hevc_codec_fmtp.max_recv_level_id                = codec_cfg->hevc_codec.max_recv_level_id;
                codec->codec_fmtp.hevc_codec_fmtp.tx_mode                          = codec_cfg->hevc_codec.tx_mode;

                memcpy(codec->codec_fmtp.hevc_codec_fmtp.sprop_vps                 , codec_cfg->hevc_codec.sprop_vps, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(codec->codec_fmtp.hevc_codec_fmtp.sprop_sps                 , codec_cfg->hevc_codec.sprop_sps, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(codec->codec_fmtp.hevc_codec_fmtp.sprop_pps                 , codec_cfg->hevc_codec.sprop_pps, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(codec->codec_fmtp.hevc_codec_fmtp.sprop_sei                 , codec_cfg->hevc_codec.sprop_sei, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);

                codec->codec_fmtp.hevc_codec_fmtp.max_lsr                          = codec_cfg->hevc_codec.max_lsr;
                codec->codec_fmtp.hevc_codec_fmtp.max_lps                          = codec_cfg->hevc_codec.max_lps;
                codec->codec_fmtp.hevc_codec_fmtp.max_cpb                          = codec_cfg->hevc_codec.max_cpb;
                codec->codec_fmtp.hevc_codec_fmtp.max_dpb                          = codec_cfg->hevc_codec.max_dpb;
                codec->codec_fmtp.hevc_codec_fmtp.max_br                           = codec_cfg->hevc_codec.max_br;
                codec->codec_fmtp.hevc_codec_fmtp.max_tr                           = codec_cfg->hevc_codec.max_tr;
                codec->codec_fmtp.hevc_codec_fmtp.max_tc                           = codec_cfg->hevc_codec.max_tc;
                codec->codec_fmtp.hevc_codec_fmtp.max_fps                          = codec_cfg->hevc_codec.max_fps;
                codec->codec_fmtp.hevc_codec_fmtp.sprop_max_don_diff               = codec_cfg->hevc_codec.sprop_max_don_diff;
                codec->codec_fmtp.hevc_codec_fmtp.sprop_depack_buf_nalus           = codec_cfg->hevc_codec.sprop_depack_buf_nalus;
                codec->codec_fmtp.hevc_codec_fmtp.sprop_depack_buf_bytes           = codec_cfg->hevc_codec.sprop_depack_buf_bytes;
                codec->codec_fmtp.hevc_codec_fmtp.depack_buf_cap                   = codec_cfg->hevc_codec.depack_buf_cap;
                codec->codec_fmtp.hevc_codec_fmtp.sprop_seg_id                     = codec_cfg->hevc_codec.sprop_seg_id;
                codec->codec_fmtp.hevc_codec_fmtp.sprop_spatial_seg_idc            = codec_cfg->hevc_codec.sprop_spatial_seg_idc;

                memcpy(codec->codec_fmtp.hevc_codec_fmtp.dec_parallel_cap          , codec_cfg->hevc_codec.dec_parallel_cap , VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(codec->codec_fmtp.hevc_codec_fmtp.include_dph               , codec_cfg->hevc_codec.include_dph      , VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);

                VT_LOGI("[SRV] [vt_ua_2_codec] video_type            = %d", ua->setting.video_type);
                VT_LOGI("[SRV] [vt_ua_2_codec] video_b_as            = %d", ua->config.video_b_as);
                VT_LOGI("[SRV] [vt_ua_2_codec] nw_assigned_ul_bw     = %d", ua->setting.nw_assigned_ul_bw);

                // Video over LTE case
                if (ua->setting.video_type == 0) {

                    // if nw_assigned_ul_bw = 0, just use another one
                    if (ua->setting.nw_assigned_ul_bw == 0) {
                        if (ua->setting.nw_assigned_ul_bw > ua->config.video_b_as) {
                            codec->codec_fmtp.hevc_codec_fmtp.video_b_as           = ua->config.video_b_as;
                        } else {
                            codec->codec_fmtp.hevc_codec_fmtp.video_b_as           = ua->setting.nw_assigned_ul_bw;
                        }
                    } else {
                        codec->codec_fmtp.hevc_codec_fmtp.video_b_as               = ua->config.video_b_as;
                    }

                // Video over Wifi case
                } else {
                    codec->codec_fmtp.hevc_codec_fmtp.video_b_as                   = ua->config.video_b_as;
                }
            }

        }
    }

    void vt_ut_ua_2_rtp(int mode, VT_IMCB_CAP * ua, rtp_rtcp_config_t * rtp) {

        if (mode == VT_SRV_CALL_4G) {

            // ====================================================================
            // rtcp part
            // ====================================================================
            vt_srv_video_capability_t *cap = &(ua->video_cap[0]);

            VT_LOGI("[SRV] [vt_ut_ua_2_rtp] cap.mime_Type        = %d", cap->mime_Type);
            VT_LOGI("[SRV] [vt_ut_ua_2_rtp] cap.media_type       = %d", cap->media_type);

            rtp->media_type                         = cap->media_type;
            rtp->mime_Type                          = cap->mime_Type;
            rtp->rtp_payload_type                   = 56;
            rtp->rtp_profile                        = IMSMA_RTP_AVPF;
            rtp->sample_rate                        = cap->sample_rate;
            //rtp->rtp_packet_bandwidth               = cap->video_b_as;
            rtp->packetize_mode                     = IMSMA_NON_INTERLEAVED_MODE;
            //rtp->rtcp_sender_bandwidth              = cap->video_b_rs;
            //rtp->rtcp_receiver_bandwidth            = cap->video_b_rr;
            rtp->rtcp_reduce_size                   = cap->rtcp_rsize;

            rtp->rtcp_fb_param_num                  = cap->rtcp_fb_param_num;
            memcpy(rtp->rtcp_fb_type                , cap->rtcp_fb_type, sizeof(VT_MAX_RTCP_FB_SIZE) * sizeof(vt_srv_rtcp_fb_param_type_t));

            rtp->network_info.ebi                   = -1;
            rtp->network_info.interface_type        = -1;
            rtp->network_info.dscp                  = -1;
            rtp->network_info.soc_priority          = -1;

            rtp->network_info.remote_rtp_port       = 40000;
            rtp->network_info.remote_rtcp_port      = 40001;
            rtp->network_info.local_rtp_port        = 40000;
            rtp->network_info.local_rtcp_port       = 40001;
            rtp->network_info.remote_rtp_address[0] = 127;
            rtp->network_info.remote_rtp_address[1] = 0;
            rtp->network_info.remote_rtp_address[2] = 0;
            rtp->network_info.remote_rtp_address[3] = 1;
            rtp->network_info.remote_rtcp_address[0] = 127;
            rtp->network_info.remote_rtcp_address[1] = 0;
            rtp->network_info.remote_rtcp_address[2] = 0;
            rtp->network_info.remote_rtcp_address[3] = 1;
            rtp->network_info.local_rtp_address[0] = 127;
            rtp->network_info.local_rtp_address[1] = 0;
            rtp->network_info.local_rtp_address[2] = 0;
            rtp->network_info.local_rtp_address[3] = 1;
            rtp->network_info.local_rtcp_address[0] = 127;
            rtp->network_info.local_rtcp_address[1] = 0;
            rtp->network_info.local_rtcp_address[2] = 0;
            rtp->network_info.local_rtcp_address[3] = 1;
            rtp->network_info.remote_addr_type      = 0;
            rtp->network_info.local_addr_type       = 0;
            rtp->network_info.network_id            = 0;
            rtp->network_info.uid                   = g_vt.core->mUserID;
            rtp->network_info.rtp_direction         = -1;
            rtp->network_info.tag                   = 0xFF000000;

            for (int i = 0; i < VILTE_BIND_FD_NUMBER; i++) {
                rtp->network_info.socket_fds[i] = -1;
            }

            rtp->rtp_header_extension_num           = 1;
            for (int i = 0; i < rtp->rtp_header_extension_num; i++) {
                rtp->rtp_ext_map[i].extension_id    = cap->extmap[i].ext_id ;
                rtp->rtp_ext_map[i].direction       = cap->extmap[i].direction;
                memcpy(rtp->rtp_ext_map[i].extension_uri, cap->extmap[i].ext_uri, VT_MAX_EXTMAP_URI_SIZE);
            }

            rtp->network_info.MBR_DL                = 0;
            rtp->network_info.MBR_DL                = 0;

        }
    }

    void vt_ut_ua_2_codec(int mode, VT_IMCB_CAP * ua, video_codec_fmtp_t * codec) {

        if (mode == VT_SRV_CALL_4G) {

            // ====================================================================
            // codec part
            // ====================================================================
            vt_srv_video_codec_cap_cfg_t *codec_cfg = &(ua->video_cap[0].codec_cap);

            codec->format                           = (video_format_t) ua->video_cap[0].mime_Type;
            codec->fps                              = 15;

            if (VIDEO_H264 == codec->format) {
                codec->codec_fmtp.h264_codec_fmtp.profile_level_id                 = codec_cfg->h264_codec.profile_level_id;
                codec->codec_fmtp.h264_codec_fmtp.max_recv_level                   = codec_cfg->h264_codec.max_recv_level;
                codec->codec_fmtp.h264_codec_fmtp.redundant_pic_cap                = codec_cfg->h264_codec.redundant_pic_cap;

                memcpy(codec->codec_fmtp.h264_codec_fmtp.sprop_parameter_sets      , codec_cfg->h264_codec.sprop_parameter_sets,       VT_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(codec->codec_fmtp.h264_codec_fmtp.sprop_level_parameter_sets, codec_cfg->h264_codec.sprop_level_parameter_sets, VT_MAX_SDP_PARAMETER_SET_LENGTH);

                codec->codec_fmtp.h264_codec_fmtp.max_mbps                         = codec_cfg->h264_codec.max_mbps;
                codec->codec_fmtp.h264_codec_fmtp.max_smbps                        = codec_cfg->h264_codec.max_smbps;
                codec->codec_fmtp.h264_codec_fmtp.max_fs                           = codec_cfg->h264_codec.max_fs;
                codec->codec_fmtp.h264_codec_fmtp.max_cpb                          = codec_cfg->h264_codec.max_cpb;
                codec->codec_fmtp.h264_codec_fmtp.max_dpb                          = codec_cfg->h264_codec.max_dpb;
                codec->codec_fmtp.h264_codec_fmtp.max_br                           = codec_cfg->h264_codec.max_br;

                codec->codec_fmtp.h264_codec_fmtp.max_rcmd_nalu_size               = codec_cfg->h264_codec.max_rcmd_nalu_size;
                codec->codec_fmtp.h264_codec_fmtp.sar_understood                   = codec_cfg->h264_codec.sar_understood;
                codec->codec_fmtp.h264_codec_fmtp.sar_supported                    = codec_cfg->h264_codec.sar_supported;
                codec->codec_fmtp.h264_codec_fmtp.video_b_as                       = 384;
                //codec_cfg->h264_codec.in_band_parameter_sets;
                //codec_cfg->h264_codec.level_asymmetry_allowed;

            } else if (VIDEO_HEVC == codec->format) {

                codec->codec_fmtp.hevc_codec_fmtp.profile_space                    = codec_cfg->hevc_codec.profile_space;
                codec->codec_fmtp.hevc_codec_fmtp.tier_flag                        = codec_cfg->hevc_codec.tier_flag;
                codec->codec_fmtp.hevc_codec_fmtp.profile_id                       = codec_cfg->hevc_codec.profile_id;
                codec->codec_fmtp.hevc_codec_fmtp.level_id                         = codec_cfg->hevc_codec.level_id;

                for (int i = 0; i < 6;i++) {
                    codec->codec_fmtp.hevc_codec_fmtp.interop_constraints[i]       = codec_cfg->hevc_codec.interop_constraints[i];
                }

                for (int i = 0; i < 4;i++) {
                    codec->codec_fmtp.hevc_codec_fmtp.profile_comp_ind[i]          = codec_cfg->hevc_codec.profile_comp_ind[i];
                }

                codec->codec_fmtp.hevc_codec_fmtp.sprop_sub_layer_id               = codec_cfg->hevc_codec.sprop_sub_layer_id;
                codec->codec_fmtp.hevc_codec_fmtp.recv_sub_layer_id                = codec_cfg->hevc_codec.recv_sub_layer_id;
                codec->codec_fmtp.hevc_codec_fmtp.max_recv_level_id                = codec_cfg->hevc_codec.max_recv_level_id;
                codec->codec_fmtp.hevc_codec_fmtp.tx_mode                          = codec_cfg->hevc_codec.tx_mode;

                memcpy(codec->codec_fmtp.hevc_codec_fmtp.sprop_vps                 , codec_cfg->hevc_codec.sprop_vps, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(codec->codec_fmtp.hevc_codec_fmtp.sprop_sps                 , codec_cfg->hevc_codec.sprop_sps, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(codec->codec_fmtp.hevc_codec_fmtp.sprop_pps                 , codec_cfg->hevc_codec.sprop_pps, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(codec->codec_fmtp.hevc_codec_fmtp.sprop_sei                 , codec_cfg->hevc_codec.sprop_sei, VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);

                codec->codec_fmtp.hevc_codec_fmtp.max_lsr                          = codec_cfg->hevc_codec.max_lsr;
                codec->codec_fmtp.hevc_codec_fmtp.max_lps                          = codec_cfg->hevc_codec.max_lps;
                codec->codec_fmtp.hevc_codec_fmtp.max_cpb                          = codec_cfg->hevc_codec.max_cpb;
                codec->codec_fmtp.hevc_codec_fmtp.max_dpb                          = codec_cfg->hevc_codec.max_dpb;
                codec->codec_fmtp.hevc_codec_fmtp.max_br                           = codec_cfg->hevc_codec.max_br;
                codec->codec_fmtp.hevc_codec_fmtp.max_tr                           = codec_cfg->hevc_codec.max_tr;
                codec->codec_fmtp.hevc_codec_fmtp.max_tc                           = codec_cfg->hevc_codec.max_tc;
                codec->codec_fmtp.hevc_codec_fmtp.max_fps                          = codec_cfg->hevc_codec.max_fps;
                codec->codec_fmtp.hevc_codec_fmtp.sprop_max_don_diff               = codec_cfg->hevc_codec.sprop_max_don_diff;
                codec->codec_fmtp.hevc_codec_fmtp.sprop_depack_buf_nalus           = codec_cfg->hevc_codec.sprop_depack_buf_nalus;
                codec->codec_fmtp.hevc_codec_fmtp.sprop_depack_buf_bytes           = codec_cfg->hevc_codec.sprop_depack_buf_bytes;
                codec->codec_fmtp.hevc_codec_fmtp.depack_buf_cap                   = codec_cfg->hevc_codec.depack_buf_cap;
                codec->codec_fmtp.hevc_codec_fmtp.sprop_seg_id                     = codec_cfg->hevc_codec.sprop_seg_id;
                codec->codec_fmtp.hevc_codec_fmtp.sprop_spatial_seg_idc            = codec_cfg->hevc_codec.sprop_spatial_seg_idc;

                memcpy(codec->codec_fmtp.hevc_codec_fmtp.dec_parallel_cap          , codec_cfg->hevc_codec.dec_parallel_cap , VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                memcpy(codec->codec_fmtp.hevc_codec_fmtp.include_dph               , codec_cfg->hevc_codec.include_dph      , VOLTE_MAX_SDP_PARAMETER_SET_LENGTH);
                codec->codec_fmtp.hevc_codec_fmtp.video_b_as                       = 384;
            }

        }
    }
}
