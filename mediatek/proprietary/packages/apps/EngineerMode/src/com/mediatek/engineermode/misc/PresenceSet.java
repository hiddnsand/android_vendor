/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.misc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.os.AsyncResult;
import android.provider.Telephony;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;

import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.wifi.ChannelInfo.ChannelData;
import android.os.SystemProperties;
import java.util.HashMap;

//import com.mediatek.presence.provider.settings.RcsSettingsData;

public class PresenceSet extends Activity implements OnClickListener {
    private static final String TAG = "EM/PresenceSet";

    public static final String ACTION_PRESENCE_RESET_ETAG = "android.intent.presence.RESET_ETAG";
    public static final String ACTION_PRESENCE_489_STATE =
                                                       "android.intent.presence.RESET_489_STATE";
    public static final Uri PRESENCE_SETTINGS_URI = Uri
            .parse("content://com.mediatek.presence.settings/settings");

    private EditText mEd489TimerValue;
    private EditText mEdCapabilityPollingPeriod;
    private EditText mEdCapabilityExpiryTime;
    private EditText mEdPublishExpiryTime;
    private EditText mEdMaxSubscriptionList;

    private Button mBtResetETAG;
    private Button mBtReset489State;

    private Button mBt489TimerValue;
    private Button mBtCapabilityPollingPeriod;
    private Button mBtCapabilityExpiryTime;
    private Button mBtPublishExpiryTime;
    private Button mBtMaxSubscriptionList;

    private String mPort = null;
    private String mRWType = null;
    private String mUsid = null;
    private String mAddress = null;
    private String mData = null;
    private String mMipiMode = null;
    private Phone mPhone = null;
    private Toast mToast = null;
    private static HashMap<String, String> mPresenceSetDataMap = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.presence_set);

        mEd489TimerValue = (EditText) findViewById(R.id.presence_set_489_timer_value);
        mEdCapabilityPollingPeriod = (EditText)
                                      findViewById(R.id.presence_set_capability_polling_period);
        mEdCapabilityExpiryTime = (EditText)
                                      findViewById(R.id.presence_set_capability_expiry_time);
        mEdPublishExpiryTime = (EditText) findViewById(R.id.presence_set_publish_expiry_time);
        mEdMaxSubscriptionList = (EditText) findViewById(R.id.presence_set_max_subscription_list);

        mBt489TimerValue = (Button) findViewById(R.id.presence_set_489_timer_value_button);
        mBtCapabilityPollingPeriod = (Button)
                               findViewById(R.id.presence_set_capability_polling_period_button);
        mBtCapabilityExpiryTime = (Button)
                               findViewById(R.id.presence_set_capability_expiry_time_button);
        mBtPublishExpiryTime = (Button) findViewById(R.id.presence_set_publish_expiry_time_button);
        mBtMaxSubscriptionList = (Button)
                               findViewById(R.id.presence_set_max_subscription_list_button);

        mBtResetETAG = (Button) findViewById(R.id.presence_set_reset_ETAG);
        mBtReset489State = (Button) findViewById(R.id.presence_set_reset_489_state);

        mBt489TimerValue.setOnClickListener(this);
        mBtCapabilityPollingPeriod.setOnClickListener(this);
        mBtCapabilityExpiryTime.setOnClickListener(this);
        mBtPublishExpiryTime.setOnClickListener(this);
        mBtMaxSubscriptionList.setOnClickListener(this);

        mBtResetETAG.setOnClickListener(this);
        mBtReset489State.setOnClickListener(this);
        if (mPresenceSetDataMap == null) {
            mPresenceSetDataMap = new HashMap<String, String>();
            mPresenceSetDataMap.put("489ExpiredTime", "0");
            mPresenceSetDataMap.put("CapabilityPollingPeriod", "0");
            mPresenceSetDataMap.put("CapabilityExpiryTimeout", "0");
            mPresenceSetDataMap.put("PublishExpirePeriod", "0");
            mPresenceSetDataMap.put("MaxSubscriptionPresenceList", "0");
        }
        queryPresenceDB();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void queryPresenceDB() {

        Cursor cursor = this.getContentResolver().query(PRESENCE_SETTINGS_URI, null, null,
                null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            // get uri
            if (cursor.getCount() > 0) {
                do {
                    String key = cursor.getString(cursor.getColumnIndex("key"));
                    String value = cursor.getString(cursor.getColumnIndex("value"));

                    if (mPresenceSetDataMap.containsKey(key)) {
                        Elog.d(TAG, key + " = " + value);
                        mPresenceSetDataMap.put(key, value);
                    }
                } while (cursor.moveToNext());

            }
            cursor.close();
        } else
            Elog.d(TAG, "cursor is null!");

        mEdCapabilityPollingPeriod.setText(mPresenceSetDataMap.get("CapabilityPollingPeriod"));
        mEdCapabilityExpiryTime.setText(mPresenceSetDataMap.get("CapabilityExpiryTimeout"));
        mEdPublishExpiryTime.setText(mPresenceSetDataMap.get("PublishExpirePeriod"));
        mEdMaxSubscriptionList.setText(mPresenceSetDataMap.get("MaxSubscriptionPresenceList"));
        mEd489TimerValue.setText(mPresenceSetDataMap.get("489ExpiredTime"));
    }

    private void setPresenceDB(String settins, String value) {
        String where = "key = \"" + settins + "\"";
        if (!value.equals("")) {
            ContentValues values = new ContentValues();
            values.put("value", value);
            Elog.d(TAG, "where = " + where);
            this.getContentResolver().update(PRESENCE_SETTINGS_URI, values, where, null);
            showToast("set " + settins + " to " + value + " succeed");
            return;
        }
        showToast("The value input is error!");
    }

    private int checkValue(String value) {
        int msg = 0;
        try {
            msg = Integer.valueOf(value);
        } catch (Exception e) {
            Elog.d(TAG, "The value input is error!");
            showToast("The value input is error!");
        }
        return msg;
    }

    public void onClick(View arg0) {
        switch (arg0.getId()) {
        case R.id.presence_set_reset_ETAG:
            SendBroadcast(this, ACTION_PRESENCE_RESET_ETAG, -1);
            Elog.d(TAG, "sendBroadcast = " + ACTION_PRESENCE_RESET_ETAG);
            showToast("sendBroadcast = " + ACTION_PRESENCE_RESET_ETAG + " succeed");
            break;
        case R.id.presence_set_reset_489_state:
            SendBroadcast(this, ACTION_PRESENCE_489_STATE, -1);
            Elog.d(TAG, "sendBroadcast = " + ACTION_PRESENCE_489_STATE);
            showToast("sendBroadcast = " + ACTION_PRESENCE_489_STATE + " succeed");
            break;
        case R.id.presence_set_489_timer_value_button:
            int msg = 0;
            msg = checkValue(mEd489TimerValue.getText().toString());
            SendBroadcast(this, ACTION_PRESENCE_489_STATE, msg);
            mPresenceSetDataMap.put("489ExpiredTime", "" + msg);
            Elog.d(TAG, "sendBroadcast = " + ACTION_PRESENCE_489_STATE + ",489ExpiredTime = "
                    + msg);
            showToast("sendBroadcast = " + ACTION_PRESENCE_489_STATE + " , " + msg + " succeed");
            break;
        case R.id.presence_set_capability_polling_period_button:
            setPresenceDB("CapabilityPollingPeriod", mEdCapabilityPollingPeriod.getText()
                    .toString());
            break;
        case R.id.presence_set_capability_expiry_time_button:
            setPresenceDB("CapabilityExpiryTimeout", mEdCapabilityExpiryTime.getText()
                    .toString());
            break;
        case R.id.presence_set_publish_expiry_time_button:
            setPresenceDB("PublishExpirePeriod", mEdPublishExpiryTime.getText().toString());
            break;
        case R.id.presence_set_max_subscription_list_button:
            setPresenceDB("MaxSubscriptionPresenceList", mEdMaxSubscriptionList.getText()
                    .toString());
            break;

        }
    }

    private void SendBroadcast(Context mcontext, String intentID, int msg) {
        Intent intent = new Intent();
        intent.setAction(intentID);
        if (msg != -1) {
            intent.putExtra("489ExpiredTime", msg);
            Elog.d(TAG, "sendBroadcast result = " + msg);
        }
        if (mcontext != null) {
            mcontext.sendBroadcast(intent);
        }
    }

    private void showToast(String msg) {
        mToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        mToast.show();
    }

}
