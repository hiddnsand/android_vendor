package com.mediatek.op08.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.support.v14.preference.PreferenceFragment;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.ims.ImsManager;
import com.android.settings.SettingsActivity;
import com.android.settings.widget.SwitchBar;
import com.mediatek.ims.WfcReasonInfo;
import com.mediatek.settings.ext.DefaultWfcSettingsExt;
import com.mediatek.telephony.MtkTelephonyManagerEx;


/**
 * Plugin implementation for WFC Settings plugin
 */
public class Op08WfcSettingsExt extends DefaultWfcSettingsExt {

    private static final String TAG = "Op08WfcSettingsExt";
    private static final String KEY_WFC_SETTINGS = "wifi_calling_settings";
    private static final String TUTORIALS = "Tutorials";
    private static final String TOP_QUESTIONS = "Top_questions";
    private static final String WFC_MODE = "Wfc_mode";
    private static final String AOSP_BUTTON_WFC_MODE = "wifi_calling_mode";
    private static final String TUTORIAL_ACTION = "mediatek.settings.WFC_TUTORIALS";
    private static final String QA_ACTION = "mediatek.settings.WFC_QA";
    private static final String WFC_MODE_DIALOG_ACTION = "mediatek.settings.SHOW_WFC_MODE_DIALOG";
    private static final int FEATURE_CONFIGURED = 99;
    private static final int FEATURE_UNCONFIGURED = 100;
    private static final String ENABLE_WFC = "enable_wifi_calling";

    private Context mContext;
    private PreferenceFragment mPreferenceFragment;
    private WfcSummary mWfcSummary;
    private Preference mWfcModePreference;
    private SwitchPreference mWfcSwitch;

    private ContentObserver mWfcModeChangeObserver;


    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("@M_" + TAG, "onReceive:" + action);
            if (!ImsManager.isWfcEnabledByPlatform(mContext)) {
                Log.d("@M_" + TAG, "Wfc not present in platform");
            }
            if (action.equals(WfcSummary.ACTION_WFC_SUMMARY_CHANGE)) {
                PreferenceScreen wfcPrefScreen = (PreferenceScreen) mPreferenceFragment
                            .findPreference(KEY_WFC_SETTINGS);
                Log.d("@M_" + TAG, "wfcPrefScreen:" + wfcPrefScreen);
                if (wfcPrefScreen != null) {
                    wfcPrefScreen.setSummary(intent
                            .getStringExtra(WfcSummary.EXTRA_SUMMARY));
                }
            }
        }
    };

    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            // TODO Auto-generated method stub
            Log.d(TAG, "Phone state:" + state);
            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    if (mWfcSwitch != null) {
                        mWfcSwitch.setEnabled(true);
                    }
                    /* Enable preference, only if wfc switch is ON */
                    mWfcModePreference.setEnabled(mWfcSwitch.isChecked() ? true : false);
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                case TelephonyManager.CALL_STATE_RINGING:
                    if (mWfcSwitch != null) {
                        mWfcSwitch.setEnabled(false);
                    }
                    mWfcModePreference.setEnabled(false);
                    break;
                default:
                    break;
            }
        }
    };


    /** Constructor.
     * @param context default summary res id
     */
    public Op08WfcSettingsExt(Context context) {
        super();
        mContext = context;
        mWfcSummary = new WfcSummary(context);
        Log.d(TAG, "Op08WfcSettingsExt");
    }

    @Override
     /** Initialize plugin with essential values.
     * @param
     * @return
     */
    public void initPlugin(PreferenceFragment pf) {
        mPreferenceFragment = pf;
    }

    @Override
    /** get operator specific customized summary for WFC button.
     * Used in WirelessSettings
     * @param context context
     * @param defaultSummaryResId default summary res id
     * @return String
     */
    public String getWfcSummary(Context context, int defaultSummaryResId) {
        boolean isWfcRegistered = MtkTelephonyManagerEx.getDefault()
                .isWifiCallingEnabled(SubscriptionManager.getDefaultVoiceSubscriptionId());
        Log.d(TAG, "[getWfcSummary]isWfcRegistered:" + isWfcRegistered);
        int wfcState = WfcReasonInfo.CODE_WFC_DEFAULT;
        if (isWfcRegistered) {
            wfcState = WfcReasonInfo.CODE_WFC_SUCCESS;
        }
        return mWfcSummary.getWfcSummaryText(wfcState);
    }


    //@Override
     /** Called from onPause.
     * Used in WirelessSettings
     * @param event event happened
     * @return
     */
    public void onWirelessSettingsEvent(int event) {
        Log.d("@M_" + TAG, "Wireless setting event:" + event);
        //if (event == DefaultWfcSettingsExt.CONFIG_CHANGE) {
          //  if (ImsManager.isWfcEnabledByPlatform(mContext)) {
            //    event = FEATURE_CONFIGURED;
            //} else {
              //  event = FEATURE_UNCONFIGURED;
            //}
        //}
        switch (event) {
            case DefaultWfcSettingsExt.RESUME:
            //case FEATURE_CONFIGURED:
                {
                    IntentFilter filter = new IntentFilter(WfcSummary.ACTION_WFC_SUMMARY_CHANGE);
                    mContext.registerReceiver(mReceiver, filter);
                    mWfcSummary.onResume();
                    PreferenceScreen wfcPrefScreen = (PreferenceScreen) mPreferenceFragment
                            .findPreference(KEY_WFC_SETTINGS);
                    Log.d("@M_" + TAG, "onResume, wfcPreferenceScreen:" + wfcPrefScreen);
                    if (wfcPrefScreen == null) {
                        return;
                    }
                    boolean isWfcRegistered = MtkTelephonyManagerEx.getDefault()
                      .isWifiCallingEnabled(SubscriptionManager.getDefaultVoiceSubscriptionId());
                    Log.d(TAG, "[getWfcSummary]isWfcRegistered:" + isWfcRegistered);
                    int wfcState = WfcReasonInfo.CODE_WFC_DEFAULT;
                    if (isWfcRegistered) {
                       wfcState = WfcReasonInfo.CODE_WFC_SUCCESS;
                    }
                    wfcPrefScreen.setSummary(mWfcSummary.getWfcSummaryText(wfcState));
                }
                break;

            case DefaultWfcSettingsExt.PAUSE:
            //case FEATURE_UNCONFIGURED:
                {
                    mContext.unregisterReceiver(mReceiver);
                    mWfcSummary.onPause();
                }
                break;
            case DefaultWfcSettingsExt.CONFIG_CHANGE:
                if (ImsManager.isWfcEnabledByPlatform(mContext)) {
                    PreferenceScreen wfcPrefScreen = (PreferenceScreen) mPreferenceFragment
                            .findPreference(KEY_WFC_SETTINGS);
                    Log.d("@M_" + TAG, "onResume, wfcPreferenceScreen:" + wfcPrefScreen);
                    if (wfcPrefScreen == null) {
                        return;
                    }
                    boolean isWfcRegistered = MtkTelephonyManagerEx.getDefault()
                      .isWifiCallingEnabled(SubscriptionManager.getDefaultVoiceSubscriptionId());
                    Log.d(TAG, "[getWfcSummary]isWfcRegistered:" + isWfcRegistered);
                    int wfcState = WfcReasonInfo.CODE_WFC_DEFAULT;
                    if (isWfcRegistered) {
                       wfcState = WfcReasonInfo.CODE_WFC_SUCCESS;
                    }
                    wfcPrefScreen.setSummary(mWfcSummary.getWfcSummaryText(wfcState));
                }
                break;
            default:
                break;
        }
    }

    /*******************        ************************/

    /***********    Plugin for  WifiCallingSettings     *******/

    //@Override
    /** Called from onPause/onResume.
     * @param event event happened
     * @return
     */
    public void onWfcSettingsEvent(int event) {
        Log.d("@M_" + TAG, "WfcSeting event:" + event);
        switch (event) {
            case DefaultWfcSettingsExt.RESUME:
                mWfcSwitch = (SwitchPreference) mPreferenceFragment.findPreference(ENABLE_WFC);
                ((TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE))
                        .listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
                mContext.getContentResolver().registerContentObserver(android.provider
                        .Settings.Global.getUriFor(android.provider
                                .Settings.Global.WFC_IMS_MODE),
                        false, mWfcModeChangeObserver);
                break;
            case DefaultWfcSettingsExt.PAUSE:
                ((TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE))
                        .listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
                mContext.getContentResolver().unregisterContentObserver(mWfcModeChangeObserver);
                break;
            default:
                break;
        }
    }

    //@Override
    /** Add WFC tutorial prefernce, if any.
     * @param
     * @return
     */
    public void addOtherCustomPreference() {
        PreferenceScreen ps = mPreferenceFragment.getPreferenceScreen();
        /* remove AOSP wfc mode preference */
        ps.removePreference(mPreferenceFragment.findPreference(AOSP_BUTTON_WFC_MODE));

        /* Add cutomized wfc mode preference */
        mWfcModePreference = new Preference(ps.getContext());
        mWfcModePreference.setKey(WFC_MODE);
        mWfcModePreference.setTitle(mContext.getText(R.string.wfc_mode_preference_title));
        mWfcModePreference.setSummary(WfcUtils.getWfcModeSummary(ImsManager.getWfcMode(mContext)));
        mWfcModePreference.setIntent(new Intent(WFC_MODE_DIALOG_ACTION));
        ps.addPreference(mWfcModePreference);
        /* Register content observer on Wfc Mode to change summary of this pref on mode change */
        registerForWfcModeChange(new Handler());

        Preference wfcTutorialPreference = new Preference(ps.getContext());
        wfcTutorialPreference.setKey(TUTORIALS);
        wfcTutorialPreference.setTitle(mContext.getText(R.string.Tutorials));
        wfcTutorialPreference.setIntent(new Intent(TUTORIAL_ACTION));
        ps.addPreference(wfcTutorialPreference);

        Preference wfcQAPreference = new Preference(ps.getContext());
        wfcQAPreference.setKey(TOP_QUESTIONS);
        wfcQAPreference.setTitle(mContext.getText(R.string.Top_questions));
        wfcQAPreference.setIntent(new Intent(QA_ACTION));
        ps.addPreference(wfcQAPreference);
    }

    @Override
    /** Takes operator specific action on wfc list preference on switch change:
     * On Switch OFF, disable wfcModePref.
     * @param root
     * @param wfcModePref
     * @return
     */
    public void updateWfcModePreference(PreferenceScreen root, ListPreference wfcModePref,
            boolean wfcEnabled, int wfcMode) {
        Log.d("@M_" + TAG, "wfc_enabled:" + wfcEnabled + " wfcMode:" + wfcMode);
        mWfcModePreference.setSummary(WfcUtils.getWfcModeSummary(wfcMode));
        mWfcModePreference.setEnabled(wfcEnabled);
        // Remove Wfc mode preference because host app adds it if wfc is enabled
        if (wfcEnabled) {
            root.removePreference(wfcModePref);
        }
    }

    /*
    * Observes WFC mode changes to change summary of preference.
    */
    private void registerForWfcModeChange(Handler handler) {
        mWfcModeChangeObserver = new ContentObserver(handler) {

            @Override
            public void onChange(boolean selfChange) {
                this.onChange(selfChange,
                        android.provider.Settings.Global
                        .getUriFor(android.provider.Settings.Global.WFC_IMS_MODE));
            }

            @Override
            public void onChange(boolean selfChange, Uri uri) {
                Uri i = android.provider.Settings.Global
                        .getUriFor(android.provider.Settings.Global.WFC_IMS_MODE);
                Log.d("@M_" + TAG, "wfc mode:" + ImsManager.getWfcMode(mContext));
                if (i != null && i.equals(uri)) {
        //            if (ImsManager.isWfcEnabledByPlatform(mContext)) {
                        /* set summary */
                        mWfcModePreference.setSummary(WfcUtils
                                .getWfcModeSummary(ImsManager.getWfcMode(mContext)));
          //          }
                }
            }
        };
//        mContext.getContentResolver().registerContentObserver(
  //              android.provider.Settings.Global.getUriFor(android.provider
    //                    .Settings.Global.WFC_IMS_MODE),
      //          false, mWfcModeChangeObserver);
    }
    /***************************        *********************/
}


