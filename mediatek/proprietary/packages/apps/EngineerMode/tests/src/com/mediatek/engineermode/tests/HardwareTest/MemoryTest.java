/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.test.SingleLaunchActivityTestCase;
import android.util.Log;
import android.widget.ListView;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.memory.EmiRegister;
import com.mediatek.engineermode.memory.Memory;
import com.mediatek.engineermode.memory.Flash;
import com.mediatek.engineermode.power.ChargeBattery;
import com.mediatek.engineermode.R;

import java.io.File;

public class MemoryTest extends SingleLaunchActivityTestCase<Memory> {

    private static final String TAG = "EMTest/memory";
    private static final int MEMORY_ITEM_COUNT = 2;
    private static final String EMMC_PROC_FILE = "/proc/emmc";
    private static final String EMMC_ID_HEADER = "emmc ID:";
    private static final String EMMC_PARTITION = "Part_Name";
    private static final String EMMC_NAME = "Name";
    private static final String FLASH_FS_TAG = "rootfs";
    private static final String FLASH_PARTITION_TAG = "major";
    private static final String FLASH_PART_EMMC = "emmc_p";
    private static final String FLASH_PART_MTD = "mtd";
    private static Solo sSolo = null;
    private static Activity sActivity = null;
    private static Context sContext = null;
    private static Instrumentation sInst = null;
    private static boolean sHasEmmc = true;
    private static boolean sFinished = false;

    public MemoryTest() {
        super("com.mediatek.engineermode", Memory.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        sInst = getInstrumentation();
        sContext = sInst.getTargetContext();
        sActivity = getActivity();
        sSolo = new Solo(sInst, sActivity);
        sHasEmmc = new File(EMMC_PROC_FILE).exists();
    }

    @Override
    protected void tearDown() throws Exception {
        if (sFinished) {
            sSolo.finishOpenedActivities();
        }
        super.tearDown();
    }

    @UiAnnotation
    public void test01_Precondition() {
        assertNotNull(sInst);
        assertNotNull(sContext);
        assertNotNull(sActivity);
        assertNotNull(sSolo);
    }

    @UiAnnotation
    public void test02_FlashActivity() {

        if (!EmOperate.enterItem(sSolo, "Flash", "Flash"))
            return;

        sSolo.clickOnText("File System Info");
        sSolo.sleep(EmOperate.TIME_LONG);
        boolean isFoundText = false;
        for (int i = 0; i < 5; i++) {
            isFoundText = sSolo.searchText(FLASH_FS_TAG);
            if (isFoundText == true)
                break;
        }
        assertEquals(true, isFoundText);

        sSolo.clickOnText("Partition Info");
        sSolo.sleep(EmOperate.TIME_LONG);

        isFoundText = false;
        for (int i = 0; i < 5; i++) {
            isFoundText = sSolo.searchText(FLASH_PARTITION_TAG);
            if (isFoundText == true)
                break;
        }
        assertEquals(true, isFoundText);
    }
}
