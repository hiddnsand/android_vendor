package com.mediatek.engineermode.cameranew;

public class AeSettings {
    private AeMode_e aeMode = AeMode_e.Auto;
    private int minus = 20;
    private int plus = 20;
    private int interval = 20;

    public void setAeMode(AeMode_e aeMode) {
        this.aeMode = aeMode;
    }

    public void setMinus(int minus) {
        this.minus = minus;
    }

    public void setPlus(int plus) {
        this.plus = plus;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public AeMode_e getAeMode() {
        return aeMode;
    }

    public int getMinus() {
        return minus;
    }

    public int getPlus() {
        return plus;
    }

    public int getInterval() {
        return interval;
    }

    public void reset() {
        aeMode = AeMode_e.Auto;
        minus = 20;
        plus = 20;
        interval = 10;
    }

    @Override
    public String toString() {
        return "AeMode: " + aeMode.getDesc() + "; minus: " + minus + "; plus: " + plus
                + "; interval: " + interval;
    }
}
