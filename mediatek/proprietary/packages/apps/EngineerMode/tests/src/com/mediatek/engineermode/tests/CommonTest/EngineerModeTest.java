/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.net.ConnectivityManager;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.KeyEvent;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.EngineerMode;

public class EngineerModeTest extends ActivityInstrumentationTestCase2<EngineerMode> {

    private static final String TAG = "EMTest/engineermodeMainUI";
    private Solo mSolo = null;
    private Activity mActivity = null;
    private Context mContext = null;
    private Instrumentation mInst = null;

    public EngineerModeTest() {
        super(EngineerMode.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mInst = getInstrumentation();
        mContext = mInst.getTargetContext();
        mActivity = getActivity();
        mSolo = new Solo(mInst, mActivity);
    }

    @Override
    protected void tearDown() throws Exception {
        mSolo.finishOpenedActivities();
        super.tearDown();
    }

    private boolean isWifiOnly() {
        ConnectivityManager connManager = (ConnectivityManager) mActivity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean bWifiOnly = false;
        if (null != connManager) {
            bWifiOnly = !connManager.isNetworkSupported(ConnectivityManager.TYPE_MOBILE);
            Log.i("@M_" + TAG, "bWifiOnly: " + bWifiOnly);
        }
        return bWifiOnly;
    }

    @BasicFuncAnnotation
    public void test01_Precondition() {
        assertNotNull(mInst);
        assertNotNull(mContext);
        assertNotNull(mActivity);
        assertNotNull(mSolo);
    }

    @UiAnnotation
    public void test02_ItemCount() throws Exception {
        boolean pageFound;

        if (!isWifiOnly()) {
            pageFound = mSolo.waitForText("(?i).*?Telephony");
            assertTrue("Telephony page is not found", pageFound);
            mSolo.sendKey(KeyEvent.KEYCODE_DPAD_RIGHT);
        }

        pageFound = mSolo.waitForText("(?i).*?Connectivity");
        assertTrue("Connectivity page is not found", pageFound);

        mSolo.sendKey(KeyEvent.KEYCODE_DPAD_RIGHT);
        pageFound = mSolo.waitForText("(?i).*?Hardware Testing");
        assertTrue("Hardware Testing page is not found", pageFound);

        mSolo.sendKey(KeyEvent.KEYCODE_DPAD_RIGHT);
        pageFound = mSolo.waitForText("(?i).*?Location");
        assertTrue("Location page is not found", pageFound);

        mSolo.sendKey(KeyEvent.KEYCODE_DPAD_RIGHT);
        pageFound = mSolo.waitForText("(?i).*?Log and Debugging");
        assertTrue("Log and Debugging page is not found", pageFound);

        mSolo.sendKey(KeyEvent.KEYCODE_DPAD_RIGHT);
        pageFound = mSolo.waitForText("(?i).*?Others");
        assertTrue("Others page is not found", pageFound);

        mSolo.sendKey(KeyEvent.KEYCODE_DPAD_LEFT);
        mSolo.sendKey(KeyEvent.KEYCODE_DPAD_LEFT);
        mSolo.sendKey(KeyEvent.KEYCODE_DPAD_LEFT);
        mSolo.sendKey(KeyEvent.KEYCODE_DPAD_LEFT);
        mSolo.sendKey(KeyEvent.KEYCODE_DPAD_LEFT);
        mSolo.sleep(EmOperate.TIME_SHORT);
    }

}
