package com.mediatek.connectivity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mediatek.connectivity.CdsEmbmsService.EmbmsBinder;

import java.text.DateFormat;
import java.util.Date;

/**
  * main class to handle eMBMS AT command information activity.
  *
  */
public class CdsEmbmsEmActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "CdsEmbms";

    private static final int MSG_UPDATE_UI = 0x3001;
    private static final String DEFAULT_REFRESH_INTERVAL = "3";

    private Context mContext;
    private CdsEmbmsService mService;

    private boolean mBound;
    private boolean mIsAutoRefresh;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cds_embms_em);

        mContext = this.getBaseContext();

        Button btn = (Button) findViewById(R.id.embmsAutoRefreshBtn);
        btn.setOnClickListener(this);

        btn = (Button) findViewById(R.id.embmsRefreshBtn);
        btn.setOnClickListener(this);

        EditText eTxt = (EditText) findViewById(R.id.embmsRefreshInterval);
        eTxt.setText(DEFAULT_REFRESH_INTERVAL);
        mIsAutoRefresh = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, CdsEmbmsService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUiInfo();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
                case R.id.embmsAutoRefreshBtn:
                    autoRefreshUi();
                break;
                case R.id.embmsRefreshBtn:
                    updateUiInfo();
                break;
            default:
            break;
        }
    }

    private void autoRefreshUi() {
        if (mIsAutoRefresh) {
            if (mHandler.hasMessages(MSG_UPDATE_UI)) {
                mHandler.removeMessages(MSG_UPDATE_UI);
            }
            mIsAutoRefresh = false;
            return;
        } else {
            keepUpdateUi();
            mIsAutoRefresh = true;
        }
    }

    private void keepUpdateUi() {
        EditText eTxt = (EditText) findViewById(R.id.embmsRefreshInterval);
        int interval = 0;
        try {
            String intervalStr = eTxt.getText().toString();
            interval = Integer.parseInt(intervalStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (interval != 0) {
            if (!mHandler.hasMessages(MSG_UPDATE_UI)) {
                Message msg = mHandler.obtainMessage();
                msg.what = MSG_UPDATE_UI;
                mHandler.sendMessageDelayed(msg, interval * 1000);
            }
        } else {
            mIsAutoRefresh = false;
        }
    }

    private void updateUiInfo() {
        if (mService == null) {
            Log.e(TAG, "No embms service");
            return;
        }

        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        TextView txt = (TextView) findViewById(R.id.embmsUpdateTime);
        txt.setText(currentDateTimeString + "\n\n");

        updateInfoByEntry(R.id.embmsSrvStatus, R.string.embms_service_status, "AT+EMSEN?");
        updateInfoByEntry(R.id.embmsSrvAreaStatus, R.string.embms_em_area, "AT+EMSRV?");
        updateInfoByEntry(R.id.embmsSrvTimeInfo, R.string.embms_em_time, "AT+EMTSI?");
        updateInfoByEntry(R.id.embmsSessionList, R.string.embms_em_slu, "AT+EMSLU?");
        updateInfoByEntry(R.id.embmsSaiList, R.string.embms_em_sail, "AT+EMSAIL?");
        updateInfoByEntry(R.id.embmsSessionInfo, R.string.embms_em_session, "AT+EMSESS?");
        updateInfoByEntry(R.id.embmsSrvPri, R.string.embms_em_pri, "AT+EMPRI?");
        updateInfoByEntry(R.id.embmsFreqInfo, R.string.embms_em_freq, "AT+EMFRQ?");
        updateInfoByEntry(R.id.embmsNwIfc, R.string.embms_em_ifc, "AT+EMBIND?");
    }

    private void updateInfoByEntry(int id, int stringId, String atCmd) {
        TextView txt = (TextView) findViewById(id);
        String cmdRsp = mService.sendAtcmd(atCmd);
        txt.setText(getString(stringId) + ":" + cmdRsp);
    }

    /** Defines callbacks for service binding, passed to bindService(). */
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            Log.i(TAG, "onServiceConnected");
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            EmbmsBinder binder = (EmbmsBinder) service;
            mService = binder.getService();
            if (mIsAutoRefresh) {
                keepUpdateUi();
            } else {
                Message msg = mHandler.obtainMessage();
                msg.what = MSG_UPDATE_UI;
                mHandler.sendMessage(msg);
            }
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    // Define the Handler that receives messages from the thread and update the
    // progress
    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {

            switch (msg.what) {
            case MSG_UPDATE_UI:
                updateUiInfo();
                if (mIsAutoRefresh) {
                    keepUpdateUi();
                }
                break;
            default:
                break;
            }
        }
    };
}