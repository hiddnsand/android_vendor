/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.bluetooth.dtt;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

public class BluetoothReceiver extends BroadcastReceiver {
    private static final String TAG = "BluetoothReceiver";
    private Util mUtil;

    @Override
    public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i(Const.TAG, "received : " + action);

            // Change test situation to usertrial
            if (action.equals("com.mediatek.bluetooth.dtt.BTLOG_CONFIG")) {
                int enable = intent.getIntExtra("cmd_target", 1);
                int disable = intent.getIntExtra("cmd_target", 0);

                if (enable == 1) {
                    Log.i(Const.TAG, "IS enabled. Change test situation");
                    DataModel mModel = new DataModel();
                    mModel.mBtHostTestSituation = Const.TS_SQC;
                    mModel.mBtFwTestSituation = Const.TS_SQC;
                    mModel.pushTestSituation();
                }
                if (disable == 0) {
                    Log.i(Const.TAG, "IS disabled : Revert test situation");
                    DataModel mModel = new DataModel();
                    mModel.load(context);
                    mModel.pushTestSituation();
                }
            // move files
            } else if (action.equals("com.mediatek.bluetooth.dtt.BTLOG_MOVE")) {
                mUtil.moveFiles(context, mUtil.SRC_FILE_DIR , mUtil.DST_FILE_DIR);
                mUtil.copyFile(context, mUtil.SRC_FILE_DIR + mUtil.SRC_FILE, mUtil.DST_FILE_DIR +mUtil.SRC_FILE);
            } else if (action.equals("com.mediatek.mtklogger.to.btlog")) {
                String operation = intent.getStringExtra("btlog_operate");
                if (operation.equals("clear_logs")) {
                    BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
                    boolean reserve_curr = mAdapter.getState() == BluetoothAdapter.STATE_ON ? true: false;
                    mUtil.deleteFiles(context, reserve_curr);
                }
            }
    }
}
