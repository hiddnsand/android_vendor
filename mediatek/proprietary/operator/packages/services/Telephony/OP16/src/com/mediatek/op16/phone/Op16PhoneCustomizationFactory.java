package com.mediatek.op16.phone;

import android.content.Context;

import com.mediatek.phone.ext.ICallFeaturesSettingExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;

public class Op16PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {
    private Context mContext;

    public Op16PhoneCustomizationFactory(Context context) {
        mContext = context;
    }

    public ICallFeaturesSettingExt makeCallFeaturesSettingExt() {
        return new Op16CallFeaturesSettingExt(mContext);
    }
}
