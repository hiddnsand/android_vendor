package com.mediatek.op18.settings;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.preference.Preference;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceScreen;
import android.provider.Settings;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.android.internal.telephony.TelephonyIntents;


import com.mediatek.settings.ext.DefaultSimManagementExt;


/**
 * Handle selection of non-lte sim for default data and disable SIM dialog at bootup.
 */
public class Op18SimManagementExt extends DefaultSimManagementExt {

    private static final String TAG = "Op18SimManagementExt";
    private static TelecomManager telecomManager;

    private Context mContext;
    PreferenceFragment mPrefFragment;
    private AlertDialog mAlertDlg;
    Preference smartCallPref;

    private static final String SMART_CALL_FWD_KEY = "smart_call_fwd_key";
    private final String SMART_CALL_PREF_CAT_KEY = "smart_call_activities";
    private final String SMART_CALL_FWD_SETTINGS = "mediatek.settings.SMART_CALL_FWD_SETTINGS";
    // Subinfo record change listener.
    private BroadcastReceiver mSubReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Log.d(TAG, "mSubReceiver action = " + action);
                if (action.equals(TelephonyIntents.ACTION_SUBINFO_CONTENT_CHANGE)
                        || action.equals(TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED)) {
                        int[] subids = SubscriptionManager.from(mContext).
                                getActiveSubscriptionIdList();
                        if (subids == null || subids.length <= 1) {
                            if (mAlertDlg != null && mAlertDlg.isShowing()) {
                                Log.d(TAG, "onReceive dealWithDataConnChanged dismiss AlertDlg");
                                mAlertDlg.dismiss();
                                if (mPrefFragment != null) {
                                    mPrefFragment.getActivity().unregisterReceiver(mSubReceiver);
                                }
                            }
                        }
                }
            }
    };

    /**
     * Initialize plugin context.
     * @param context context
     */
    public Op18SimManagementExt(Context context) {
        super();
        mContext = context;
        Log.d(TAG, "mContext = " + mContext);
        telecomManager = TelecomManager.from(context);
    }

    @Override
    /** Common method to check what prefernce has been clicked.
    * @param
    * @return
    */
    public void handleEvent(PreferenceFragment pf, Context context, final Preference preference) {
        if (pf.findPreference(SMART_CALL_FWD_KEY) == preference) {
            handleEventSmartCall(context);
        }
        else {
            // Other preference code can be added here
        }
}
/********************* Smart Call forward feature ***********************/

    @Override
     /** Initialize plugin with essential values.
     * @param
     * @return
     */
    public void initPlugin(PreferenceFragment pf) {
        PreferenceScreen ps = pf.getPreferenceScreen();
        final TelecomManager telecomManager = TelecomManager.from(mContext);
        int accoutSum = telecomManager.getCallCapablePhoneAccounts().size();
        Log.d(TAG, "accoutSum:" + accoutSum);
        /* Add smart call forward preference */
        if (accoutSum > 1) {
            PreferenceCategory prefCat = new PreferenceCategory(ps.getContext());
            prefCat.setKey(SMART_CALL_PREF_CAT_KEY);
            prefCat.setTitle("General");
            ps.addPreference(prefCat);
            Log.d(TAG, "Add smartCallPref");
            smartCallPref = new Preference(ps.getContext());
            smartCallPref.setKey(SMART_CALL_FWD_KEY);
            smartCallPref.setTitle(mContext.getText(R.string.smart_call_fwd_settings_title));
            smartCallPref.setSummary(mContext.getText(R.string.smart_call_fwd_settings_summary));
            prefCat.addPreference(smartCallPref);
            boolean mIsAirplaneMode = isAirplaneModeOn(mContext);
            Log.d(TAG, "mIsAirplaneMode" + mIsAirplaneMode);
            smartCallPref.setEnabled(!mIsAirplaneMode);
        }
    }

    public void handleEventSmartCall(Context context) {
        Log.d(TAG, "handleEvent");
        final Context ctx = context;
        final Intent i = new Intent(SMART_CALL_FWD_SETTINGS);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(mContext.getText(R.string.smart_call_fwd_enable_alert_dlg_title));
        builder.setMessage(mContext.getText(R.string.smart_call_fwd_alert_dlg_new));
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
            Log.d(TAG, "launch smartCallFwd activity");
            ctx.startActivity(i);
        }
        });
        builder.setNegativeButton(android.R.string.no, null);
        builder.show();
    }

    /** updatePrefState.
     * @param enable preference
     * @return
     */
    public void updatePrefState() {
        boolean mIsAirplaneMode = isAirplaneModeOn(mContext);
        Log.d(TAG, "updatePrefState airplane status: " + mIsAirplaneMode
            + "smartCallPref" + smartCallPref);
        if (smartCallPref != null) {
            smartCallPref.setEnabled(!mIsAirplaneMode);
        }
        return;
    }

    private static boolean isAirplaneModeOn(Context context) {
        return Settings.System.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }
}
