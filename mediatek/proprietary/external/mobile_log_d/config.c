#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <log/log.h>
#include <string.h>
#include <sys/stat.h>
#include <cutils/properties.h>
#include "config.h"
#include "global_var.h"
#include "mlog.h"
#include "libfunc.h"
#include "bootmode.h"

sd_context_t sdContext = {INTERNAL_SD, PATH_DEFAULT};

pthread_mutex_t config_lock = PTHREAD_MUTEX_INITIALIZER;


/*
 *  erase leading or trailing spaces
 */
char *strim(char *str)
{
	size_t size;
	char *last_end,*end;

	size = strlen(str);
	if (!size)
		return str;

	end = str + size -1;
	last_end = end;
	while (end > str && ((*end == ' ') || (*end == '\n')))
		end --;
	if (last_end != end) {
		*(end+1) = '\0';
	}

	while (*str == ' ')
		str++;

	return str;
}

char *default_value(const char *key)
{
	if (!strcmp(key, KEY_BOOT)) {
		char buf[PROPERTY_VALUE_MAX];
		property_get(PROP_BUILD_TYPE,buf,BUILD_TYPE_USER);
		if (!strcmp(buf, BUILD_TYPE_ENG)) {
			return ENABLE;
		} else {
			return DISABLE;
		}
	} else if (!strcmp(key, KEY_PATH)) {
		return PATH_INTERNAL;
	} else if (!strcmp(key, KEY_SIZE)) {
		return SIZE_DEFAULT;
	} else if (!strcmp(key, KEY_TOTAL_SIZE)) {
		return TOTAL_SIZE_DEFAULT;
	} else if (!strcmp(key, KEY_ANDROID)) {
		return ENABLE;
	} else if (!strcmp(key, KEY_KERNEL)) {
		return ENABLE;
	} else if (!strcmp(key, KEY_ATF)) {
		return ENABLE;
	} else if (!strcmp(key, KEY_BSP)) {
		return ENABLE;
	} else if (!strcmp(key, KEY_MMEDIA)) {
		return ENABLE;
	} else if (!strcmp(key, KEY_SCP)) {
		return ENABLE;
	} else if (!strcmp(key, KEY_SSPM)) {
		return ENABLE;
	}

	MLOGE_DATA("not a valid key:%s", key);
	return NULL;
}

char *read_config_value(const char *key)
{
	int i;
	FILE *fp;
	char buf[255];
	static char value[255];

	if (access(CONFIG_FILE, F_OK) != 0) {
		MLOGE_DATA("config file not exist");
		if (create_config_file() == 0) {
			MLOGE_DATA("create config file failed");
			return default_value(key);
		}
	}

	pthread_mutex_lock(&config_lock);
	if ((fp = fopen(CONFIG_FILE,"r")) == NULL) {
		MLOGE_BOTH("open config file fail,%s",strerror(errno));
		pthread_mutex_unlock(&config_lock);
		return default_value(key);
	}

	memset(value, 0x0, sizeof(value));
	while (!feof(fp)) {
		char *p;
		memset(buf,0x0,sizeof(buf));
		if (fgets(buf,sizeof(buf),fp) == NULL) {
			MLOGE_BOTH("not find key:%s",key);
			create_config_file();
			snprintf(value, sizeof(value), "%s", default_value(key));
			break;
		}

		if (strstr(buf, key)) {
			int len = strlen(buf);
			if (buf[len-1] == '\n')
				buf[len-1] = '\0';
			p = strchr(buf, '=');
			if (p) {
				snprintf(value, sizeof(value), "%s", p+1);
			} else {
				MLOGE_BOTH("config file error %s",buf);
				create_config_file();
				snprintf(value, sizeof(value), "%s", default_value(key));
			}
			break;
		}
	}
	fclose(fp);
	pthread_mutex_unlock(&config_lock);
	return value;
}

void update_config_value(char *key, char *new_value)
{
	int i,num;
	FILE *fp;
	char temp[KEY_NUM*2][255],*real_key;

	// MLOGI_BOTH("thread %d comes", gettid());
	pthread_mutex_lock(&config_lock);
	if ((fp = fopen(CONFIG_FILE, "r+")) == NULL) {
		MLOGE_BOTH("update config(%s=%s) fail(%s)",key,new_value,strerror(errno));
		pthread_mutex_unlock(&config_lock);
		return;
	}

	real_key = key;
	if (strstr(key, "sublog_")) {
		real_key = strchr(key, '_') + 1;
	}

	i=num=0;
	memset(temp, 0x0, sizeof(temp));
	while (!feof(fp)) {
		if (fgets(temp[num], sizeof(temp[num]), fp) == NULL)
			break;
		if (!strncmp(temp[num], real_key,strlen(real_key))) {
			memset(temp[num],0x0,sizeof(temp[num]));
			snprintf(temp[num],sizeof(temp[num]),"%s=%s\n",real_key,new_value);
		}
		num++;
		if (num >= KEY_NUM*2) {
			MLOGE_BOTH("too many config");
			break;
		}
	}

	if (num > KEY_NUM) {
		MLOGE_DATA("why here");
	}
	freopen(CONFIG_FILE, "w", fp);
	for (i=0; i<num; i++) {
		if (fputs(temp[i],fp) == EOF)
			MLOGE_BOTH("update config item [%s], fputs fail, (%s)", temp[i], strerror(errno));
	}
	if (fflush(fp) != 0)
		MLOGE_BOTH("update config, fflush fail, (%s)", strerror(errno));
	if (fsync(fileno(fp)) < 0)
		MLOGE_BOTH("update config, fsync fail, (%s)", strerror(errno));

	fclose(fp);
	pthread_mutex_unlock(&config_lock);
	//MLOGI_BOTH("thread %d ends", gettid());
}

#define CUSTOM_NUM (4)
int create_config_file()
{
	FILE *fp,*fp2;
	char *p,buf[255],c_buf[255];

	if (access(CONFIG_DIR, F_OK) != 0) {
		create_dir(CONFIG_DIR);
	}

	if ((fp = fopen(CONFIG_FILE,"w")) == NULL) {
		MLOGE_BOTH("open %s fail %s",CONFIG_FILE,strerror(errno));
		return 0;
	}

	if (access(CUSTOM_FILE,F_OK) == 0) {
		int i=0,ret;
		char *q = buf;

		if ((fp2 = fopen(CUSTOM_FILE,"r")) == NULL) {
			MLOGE_BOTH("open %s fail %s",CUSTOM_FILE,strerror(errno));
			goto default_set;
		}
		memset(buf,0x0,sizeof(buf));
		while (!feof(fp2)) {
			ret = 0;
			if (fgets(c_buf,sizeof(c_buf),fp2) == NULL) {
				break;
			}
			if (strstr(c_buf,CUSTOM_BOOT)) {
				if (strstr(c_buf, "true")) {
					ret = snprintf(q,sizeof(buf),"%s=1\n",KEY_BOOT);
				} else {
					ret = snprintf(q,sizeof(buf),"%s=0\n",KEY_BOOT);
				}
			} else if (strstr(c_buf, CUSTOM_SIZE)) {
				p = strchr(c_buf,'=');
				ret = snprintf(q,sizeof(buf),"%s=%s\n",KEY_SIZE,strim(p+1));
			} else if (strstr(c_buf, CUSTOM_TOTALSIZE)) {
				p = strchr(c_buf,'=');
				ret = snprintf(q,sizeof(buf),"%s=%s\n",KEY_TOTAL_SIZE,strim(p+1));
			} else if (strstr(c_buf, CUSTOM_PATH)) {
				p = strchr(c_buf,'=');
				ret = snprintf(q,sizeof(buf),"%s=%s\n",KEY_PATH,strim(p+1));
			}

			if (ret > 0) {
				q = q+ret;
				i++;
			}
		}
		fclose(fp2);
		if (i == CUSTOM_NUM) {
			fputs(buf,fp);
			goto left;
		} else {
			MLOGE_DATA("only get %d configs from custom file,use default",i);
		}
	} else {
		MLOGE_BOTH("acess %s fail",CUSTOM_FILE);
	}

default_set:
	memset(buf,0x0,sizeof(buf));
	snprintf(buf,255,"%s=%s\n%s=%s\n%s=%s\n%s=%s\n",
	         KEY_BOOT, default_value(KEY_BOOT),
	         KEY_SIZE, default_value(KEY_SIZE),
	         KEY_TOTAL_SIZE, default_value(KEY_TOTAL_SIZE),
	         KEY_PATH, default_value(KEY_PATH));
	if (fputs(buf,fp) == EOF)
		MLOGE_BOTH("fputs config global items fail, (%s)", strerror(errno));
	if (fflush(fp) != 0)
		MLOGE_BOTH("config global items, fflush fail, (%s)", strerror(errno));
	if (fsync(fileno(fp)) < 0)
		MLOGE_BOTH("config global items, fsync fail, (%s)", strerror(errno));

left:
	memset(buf,0x0,sizeof(buf));
	snprintf(buf, 255, "%s=%s\n%s=%s\n%s=%s\n%s=%s\n%s=%s\n%s=%s\n%s=%s\n",
	         KEY_ANDROID, default_value(KEY_ANDROID),
	         KEY_KERNEL, default_value(KEY_KERNEL),
	         KEY_ATF, default_value(KEY_ATF),
	         KEY_BSP, default_value(KEY_BSP),
	         KEY_MMEDIA, default_value(KEY_MMEDIA),
	         KEY_SCP, default_value(KEY_SCP),
	         KEY_SSPM, default_value(KEY_SSPM));
	if (fputs(buf,fp) == EOF)
		MLOGE_BOTH("fputs config sublog items fail, (%s)", strerror(errno));
	if (fflush(fp) != 0)
		MLOGE_BOTH("config sublog items, fflush fail, (%s)", strerror(errno));
	if (fsync(fileno(fp)) < 0)
		MLOGE_BOTH("config sublog items, fsync fail, (%s)", strerror(errno));
	fclose(fp);

	if (chmod(CONFIG_FILE,0660) < 0)
		MLOGE_BOTH("chmod %s faile(%s)", CONFIG_FILE, strerror(errno));
	MLOGI_DATA("create configure file done");

	return 1;
}

int update_sd_context(const char* path)
{
	#define ROOTDIR  PATH_EXTSD_PARENT  //  "/storage/"
	size_t len1 = strlen(ROOTDIR);
	size_t len2 = strlen(path);

	if (len2 < sizeof(sdContext.sdpath) - 1)
		snprintf(sdContext.sdpath, sizeof(sdContext.sdpath), "%s", path);
	else {
		MLOGE_DATA("Update sdpath fail, the length of path is too long");
		return 0;
	}

	if (sdContext.sdpath[len2 - 1] != '/') {
		sdContext.sdpath[len2] = '/';
		sdContext.sdpath[len2 + 1] = '\0';
	}

	// check sd path type
	if ((len1 + 9 + 1 == strlen(sdContext.sdpath)) && !strncmp(path, ROOTDIR, len1)) { // ROOTDIR + XXXX-XXXX + /
		if (*(path + len1 + 4) == '-') { // XXXX-XXXX (external sdcard uuid)
			sdContext.location = EXTERNAL_SD;
			update_config_value(KEY_PATH, PATH_EXTERNAL);
			return 1;
		}
	}

	sdContext.location = INTERNAL_SD;
	update_config_value(KEY_PATH, PATH_INTERNAL);
	return 1;
}

