package com.mediatek.op08.settings;

import android.content.Context;

import com.mediatek.settings.ext.IWfcSettingsExt;
import com.mediatek.settings.ext.OpSettingsCustomizationFactoryBase;

public class Op08SettingsCustomizationFactory extends OpSettingsCustomizationFactoryBase {
    private Context mContext;

    public Op08SettingsCustomizationFactory(Context context) {
        super(context);
        mContext = context;
    }

    public IWfcSettingsExt makeWfcSettingsExt() {
        return new Op08WfcSettingsExt(mContext);
    }
}
