#ifndef __FPSPOLICY_H__
#define __FPSPOLICY_H__

#include <vector>

#include <utils/String8.h>
#include <utils/KeyedVector.h>
#include <utils/Singleton.h>
#include <utils/Looper.h>

#include "dfps/DupBitTube.h"

namespace android {

class FpsPolicyInfo {
public:
    FpsPolicyInfo();
    uint64_t sequence;
    int32_t fps;
    int32_t pid;
    int32_t api;
    int32_t mode;
    int32_t targetPid;
    uint64_t glContextId;
};

class FpsRange {
public:
    int32_t index;
    int32_t min;
    int32_t max;
};

class PanelInfo {
public:
    int32_t support120;
    int32_t support90;
    int32_t num;
};

class FpsRequest {
public:
    int32_t fps;
    int32_t mode;
    int32_t validInfo;
    int32_t transientState;
    int32_t num_policy;
    FpsPolicyInfo info;
    int32_t forbidVsync;
};

class FpsPolicy : public Thread {
public:
    class FpsRequestListener : public virtual RefBase{
    public:
        virtual ~FpsRequestListener() {};
        virtual void onRequestChanged(const FpsRequest& request) = 0;
    };

    // FpsPolicy is used to adjust system fps. Each api has different priority,
    // so module shall use corrent api, or else it may cause that system will
    // choose wrong fps setting.
    FpsPolicy(int32_t api, String8& name);
    virtual ~FpsPolicy();

    // setFps() is used to set expected fps, but this api does not guarantee
    // that the system will change refresh rate to expected fps.
    status_t setFps(int32_t fps, int32_t mode = MODE_DEFAULT, int32_t targetPid = 0,
                    uint64_t glContextId = 0);

    // getPanelInfoNumber() is used to get the number of FpsRange
    status_t getPanelInfoNumber(int32_t *num);

    // getFpsRange() is used to get the fps range of specific index
    status_t getFpsRange(int32_t index, FpsRange *range);

    // updateState() is used to update the service state. Binder exception will
    // update this info, so does not call it directly.
    void updateState(int32_t flag);

    status_t registerFpsRequestListener(const sp<FpsRequestListener>& cb);

    void cancelFpsRequestListener();

    // getApiString() is used to get the description string of each api
    static const char* getApiString(int32_t api);

    // getModeString() is used to get the description string of each mode
    static const char* getModeString(int32_t mode);

    // this definition need to be the same with rrc_drv.h
    enum {
        API_UNKNOWN = -1,
        API_GIFT = 0,
        API_VIDEO,
        API_RRC_INPUT,
        API_RRC_VIDEO,
        API_THERMAL,
        API_LOADING,
        API_WHITELIST,
        API_MAXIMUM,
    };

    enum {
        MODE_DEFAULT = 0,
        MODE_FRR,
        MODE_ARR,
        MODE_INTERNAL_SW,
        MODE_MAXIMUM,
    };

    enum {
        STATE_SERVER_DIED = 0x01,
    };

private:
    status_t assertStateLocked(int api, String8& name);
    status_t connectServiceLocked();
    int32_t isValidFpsLocked(int32_t fps, int32_t mode);
    status_t getPanelInfoNumberLocked(int32_t *num);
    static int cb_fpsRequestReceiver(int fd, int events, void* data);
    int fpsRequestReceive(int fd, int events);

    virtual bool threadLoop();
    virtual void onFirstRef();

    mutable Mutex mLock;
    bool mInit;
    FpsPolicyInfo mInfo;
    String8 mName;
    status_t mState;
    sp<DupBitTube> mChannel;
    sp<Looper> mLooper;
    FpsRequest mRequest;
    sp<FpsRequestListener> mCb;
    bool mStop;

    PanelInfo mPanelInfo;
    std::vector<FpsRange> mRange;
};

};

#endif
