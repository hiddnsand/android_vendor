LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/frameworks/opt/mdlogger))
include $(CLEAR_VARS)
LOCAL_MODULE = libmdloggerrecycle
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX = .so
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libc++ liblog
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libmdloggerrecycle.so
include $(BUILD_PREBUILT)
endif
