package com.mediatek.op02.incallui;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.android.incallui.call.DialerCall;

import com.android.incallui.InCallPresenter;
import com.android.incallui.call.TelecomAdapter;

import com.mediatek.incallui.ext.DefaultVilteAutoTestHelperExt;


public class Op02VilteAutoTestHelperExt extends DefaultVilteAutoTestHelperExt {

    private static final String TAG = "Op02VilteAutoTestHelperExt";
    private Context mContext;
    private InCallPresenter mInCallPresenter;
    private TelecomAdapter  mTelecomAdapter;

    public static final String ACTION_ACCEPT_UPGRADE
            = "mediatek.intent.action.ACTION_ACCEPT_UPGRADE";
    public static final String ACTION_REJECT_UPGRADE
            = "mediatek.intent.action.ACTION_REJECT_UPGRADE";
    public static final String ACTION_UPGRADE_VIDEO
            = "mediatek.intent.action.ACTION_UPGRADE_VIDEO";
    public static final String ACTION_DOWNGRADE_AUDIO
            = "mediatek.intent.action.ACTION_DOWNGRADE_AUDIO";
    public static final String ACTION_PAUSE_VIDEO
            = "mediatek.intent.action.ACTION_PAUSE_VIDEO";
    public static final String ACTION_RESTART_VIDEO
            = "mediatek.intent.action.ACTION_RESTART_VIDEO";
    public static final String ACTION_MERGE
            = "mediatek.intent.action.ACTION_MERGE";
    public static final String ACTION_MUTE
            = "mediatek.intent.action.ACTION_MUTE";
    public static final String ACTION_UNMUTE
            = "mediatek.intent.action.ACTION_UNMUTE";

    private static int videoState = 3;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override  
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
                Log.d(TAG,"action: " + action);
                if (action.equals(ACTION_UPGRADE_VIDEO)) {
                    processUpgraeAndDowngrade(true);
                }else if (action.equals(ACTION_DOWNGRADE_AUDIO)) {
                    processUpgraeAndDowngrade(false);
                }else if (action.equals(ACTION_ACCEPT_UPGRADE)) {
                    processAcceptAndDeclineUpgrade(true);
                }else if (action.equals(ACTION_REJECT_UPGRADE)) {
                    processAcceptAndDeclineUpgrade(false);
                }else if (action.equals(ACTION_PAUSE_VIDEO)) {
                    processPauseAndRestartVideo(true);
                }else if (action.equals(ACTION_RESTART_VIDEO)) {
                    processPauseAndRestartVideo(false);
                }else if (action.equals(ACTION_MERGE)) {
                    processMergeCall();
                }else if (action.equals(ACTION_MUTE)) {
                    mTelecomAdapter.mute(true);
                }else if (action.equals(ACTION_UNMUTE)) {
                    mTelecomAdapter.mute(false);
                }
        }
    };

    public void processUpgraeAndDowngrade(boolean shouldUpgrade) {
        DialerCall call = mInCallPresenter.getCallList().getActiveCall();
        if (call != null && call.getVideoCall() != null) {
             if (shouldUpgrade == true ){
                Log.d(TAG,"send upgrade request");
                call.getVideoTech().upgradeToVideo();
             }else if (call.isVideoCall() == true ){
                 Log.d(TAG,"send downgrade request");
                 call.getVideoTech().downgradeToAudio();
             }
        }
    }

    public void processPauseAndRestartVideo(boolean shouldPause) {
        DialerCall call = mInCallPresenter.getCallList().getActiveCall();
        if (call != null && call.isVideoCall() == true) {
            if (shouldPause == true) {
                Log.d(TAG,"change to Video");
                call.getVideoTech().stopTransmission();
            }else {
                Log.d(TAG,"restart Video");
                call.getVideoTech().resumeTransmission();
            }
        }
    }

    public void processMergeCall() {
         DialerCall call = mInCallPresenter.getCallList().getActiveCall();
         if (call != null) {
              Log.d(TAG,"merge call");
              mTelecomAdapter.merge(call.getId());
         }
    }

    public void processAcceptAndDeclineUpgrade(boolean shouldAccept) {
          DialerCall call = mInCallPresenter.getCallList().getVideoUpgradeRequestCall();
          if (call != null && call.getVideoCall() != null) {
              if(shouldAccept == true) {
                  Log.d(TAG,"accept upgrade request");
                  call.getVideoTech().acceptVideoRequest();
              }else {
               Log.d(TAG,"decline upgrade request");
               call.getVideoTech().declineVideoRequest();
              }
          }
    }

    @Override
    public void registerReceiver(Context context, Object obj1,Object obj2) {
        mContext = context;
        if (obj1 instanceof InCallPresenter) {
            log("get InCallPresenter");
            mInCallPresenter = (InCallPresenter) obj1;
        }
        if (obj2 instanceof TelecomAdapter) {
            log("get TelecomAdapter");
            mTelecomAdapter = (TelecomAdapter) obj2;
        }
        IntentFilter filter= new IntentFilter(ACTION_UPGRADE_VIDEO);
        filter.addAction(ACTION_DOWNGRADE_AUDIO);
        filter.addAction(ACTION_ACCEPT_UPGRADE);
        filter.addAction(ACTION_REJECT_UPGRADE);
        filter.addAction(ACTION_PAUSE_VIDEO);
        filter.addAction(ACTION_RESTART_VIDEO);
        filter.addAction(ACTION_MERGE);
        filter.addAction(ACTION_MUTE);
        filter.addAction(ACTION_UNMUTE);
        mContext.registerReceiver(mReceiver, filter);
        log("register up/down/pasue/restart/merge/mute receiver");
    }


    @Override
    public void unregisterReceiver( ) {
        if (mReceiver != null ) {
            mContext.unregisterReceiver(mReceiver);
            mInCallPresenter = null;
            mTelecomAdapter = null ;
            mContext =null;
            log("unregister up/down/pasue/restart/merge/mute receiver");
        }
    }

    public void log(String msg) {
        Log.d(TAG, msg);
    }

}

