/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.telephony.Rlog;

import com.android.ims.ImsCallProfile;
import com.android.ims.ImsReasonInfo;
import com.android.ims.internal.IImsCallSession;
import com.android.ims.internal.IImsCallSessionListener;

import com.mediatek.ims.internal.IMtkImsCallSession;
import com.mediatek.ims.internal.IMtkImsCallSessionListener;
import com.mediatek.ims.ril.ImsCommandsInterface;

import com.mediatek.ims.internal.op.ImsCallSessionProxyExt;
import com.mediatek.ims.internal.op.OpImsServiceFactoryBase;
import com.mediatek.ims.internal.op.OpImsServiceCall;

public class MtkImsCallSessionProxy extends IMtkImsCallSession.Stub {
    private static final String LOG_TAG = "MtkImsCallSessionProxy";
    private static final boolean DBG = true;
    private ImsCallSessionProxy mCallSessionImpl;
    private IMtkImsCallSessionListener mMtkListener;

    private OpImsServiceCall mOpImsServiceCall;
    private ImsCallSessionProxyExt mOpExt;

    MtkImsCallSessionProxy(Context context, ImsCallProfile profile,
                           IImsCallSessionListener listener, ImsService imsService,
                           Handler handler, ImsCommandsInterface imsRILAdapter,
                           String callId, int phoneId) {
        mCallSessionImpl = new ImsCallSessionProxy(context, profile, listener,
                imsService, handler, imsRILAdapter, callId, phoneId, this);
        mOpImsServiceCall =
                OpImsServiceFactoryBase.getInstance().makeOpImsServiceCall(context, phoneId);
        mOpExt =
                OpImsServiceFactoryBase.getInstance().makeImsCallSessionProxyExt();
    }

    //Constructor for MO call
    MtkImsCallSessionProxy(Context context, ImsCallProfile profile,
                           IImsCallSessionListener listener, ImsService imsService,
                           Handler handler, ImsCommandsInterface imsRILAdapter, int phoneId) {
        this(context, profile, listener, imsService, handler, imsRILAdapter, null, phoneId);
        if (DBG) {
            Rlog.d(LOG_TAG, "MtkImsCallSessionProxy RILAdapter:" + imsRILAdapter);
        }
    }

    @Override
    public void close() {
        if (DBG) {
            Rlog.d(LOG_TAG, "MtkImsCallSessionProxy is going to be closed!!! ");
        }
        mCallSessionImpl.close();
        mCallSessionImpl = null;
    }

    @Override
    public String getCallId() {
        if (mCallSessionImpl == null) {
            Rlog.d(LOG_TAG, "mCallSessionImpl is null");
            return "";
        }
        return mCallSessionImpl.getCallId();
    }

    @Override
    public ImsCallProfile getCallProfile(){
        if (mCallSessionImpl == null) {
            Rlog.d(LOG_TAG, "mCallSessionImpl is null");
            return null;
        }
        return mCallSessionImpl.getCallProfile();
    }

    @Override
    public void setListener(IMtkImsCallSessionListener listener) {
        mMtkListener = listener;
    }

    @Override
    public IImsCallSession getIImsCallSession() {
        return mCallSessionImpl;
    }

    @Override
    public boolean isIncomingCallMultiparty() {
        if (mCallSessionImpl == null) {
            Rlog.d(LOG_TAG, "mCallSessionImpl is null");
            return false;
        }
        return mCallSessionImpl.isIncomingCallMultiparty();
    }

    @Override
    public void sendDtmfbyTarget(char c, Message result, Messenger target) {
        if (mCallSessionImpl == null) {
            Rlog.d(LOG_TAG, "mCallSessionImpl is null");
            return;
        }
        mCallSessionImpl.sendDtmfbyTarget(c, result, target);
    }

    @Override
    public void explicitCallTransfer() {
        if (mCallSessionImpl == null) {
            Rlog.d(LOG_TAG, "mCallSessionImpl is null");
            return;
        }
        mCallSessionImpl.explicitCallTransfer();
    }

    @Override
    public void unattendedCallTransfer(String number, int type) {
        if (mCallSessionImpl == null) {
            Rlog.d(LOG_TAG, "mCallSessionImpl is null");
            return;
        }
        mCallSessionImpl.unattendedCallTransfer(number, type);
    }

    void notifyTransferred() {
        if (mMtkListener == null) {
            Rlog.d(LOG_TAG, "notifyTransferred() mMtkListener is null");
            return;
        }
        try {
            mMtkListener.callSessionTransferred(this);
        } catch (RemoteException e) {
            Rlog.e(LOG_TAG, "RemoteException callSessionTransferred()");
        }
    }

    void notifyTransferFailed(ImsReasonInfo reasonInfo) {
        if (mMtkListener == null) {
            Rlog.d(LOG_TAG, "notifyTransferFailed() mMtkListener is null");
            return;
        }
        try {
            mMtkListener.callSessionTransferFailed(this, reasonInfo);
        } catch (RemoteException e) {
            Rlog.e(LOG_TAG, "RemoteException callSessionTransferFailed()");
        }
    }

    public void notifyTextCapabilityChanged(int localCapability, int remoteCapability,
            int localTextStatus, int realRemoteCapability) {
        mOpExt.notifyTextCapabilityChanged(mMtkListener, this,
                localCapability, remoteCapability,
                localTextStatus, realRemoteCapability);
    }

    void notifyCallSessionMergeStarted(IMtkImsCallSession mtkConfSession,
                                       ImsCallProfile imsCallProfile) {
        if (mMtkListener == null) {
            Rlog.d(LOG_TAG, "notifyCallSessionMergeStarted() mMtkListener is null");
            return;
        }
        try {
            mMtkListener.callSessionMergeStarted(MtkImsCallSessionProxy.this,
                    mtkConfSession, imsCallProfile);
        } catch (RemoteException e) {
            Rlog.e(LOG_TAG, "RemoteException when MTK session merged started");
        }
    }

    void notifyCallSessionMergeComplete(IMtkImsCallSession mtkConfSession) {
        if (mMtkListener == null) {
            Rlog.d(LOG_TAG, "notifyCallSessionMergeComplete() mMtkListener is null");
            return;
        }
        try {
            mMtkListener.callSessionMergeComplete(mtkConfSession);
        } catch (RemoteException e) {
            Rlog.e(LOG_TAG, "RemoteException when MTK session merged started");
        }
    }

    ImsCallSessionProxy.ConferenceEventListener getConfEvtListener() {
        if (mCallSessionImpl == null) {
            Rlog.d(LOG_TAG, "mCallSessionImpl is null");
            return null;
        }
        return mCallSessionImpl.getConfEvtListener();
    }

    void terminate(int reason) {
        if (mCallSessionImpl == null) {
            return;
        }
        mCallSessionImpl.terminate(reason);
    }
}
