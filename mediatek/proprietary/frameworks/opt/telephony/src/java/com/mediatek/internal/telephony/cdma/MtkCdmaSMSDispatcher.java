/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony.cdma;

import android.app.ActivityManager;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncResult;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.UserHandle;
import android.telephony.CarrierConfigManager;
import android.telephony.Rlog;
import android.telephony.ServiceState;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;

import static android.telephony.SmsManager.RESULT_ERROR_GENERIC_FAILURE;
import static android.telephony.SmsManager.RESULT_ERROR_NULL_PDU;
import static android.telephony.SmsManager.STATUS_ON_ICC_READ;
import static android.telephony.SmsManager.STATUS_ON_ICC_SENT;
import static android.telephony.SmsManager.STATUS_ON_ICC_UNREAD;
import static android.telephony.SmsManager.STATUS_ON_ICC_UNSENT;


import com.android.internal.telephony.GsmAlphabet.TextEncodingDetails;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.IccCardConstants.State;
import com.android.internal.telephony.ImsSMSDispatcher;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.SMSDispatcher.SmsTracker;
import com.android.internal.telephony.SmsConstants;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.SmsResponse;
import com.android.internal.telephony.SmsUsageMonitor;
import com.android.internal.telephony.TelephonyProperties;
import com.android.internal.telephony.cdma.CdmaSMSDispatcher;
import com.android.internal.telephony.cdma.SmsMessage;
import com.android.internal.telephony.cdma.sms.UserData;
import com.android.internal.telephony.uicc.IccUtils;

import com.mediatek.internal.telephony.MtkPhoneNumberUtils;
import com.mediatek.internal.telephony.MtkRIL;
import com.mediatek.internal.telephony.MtkSmsCommonEventHelper;
import com.mediatek.internal.telephony.ppl.PplSmsFilterExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Sub class to enhance the AOSP class CdmaSMSDispatcher.
 */
public class MtkCdmaSMSDispatcher extends CdmaSMSDispatcher {
    private static final String TAG = "MtkCdmaMoSms";
    private static final boolean VDBG = false;
    protected Object mLock = new Object();
    private boolean mCopied = false;
    protected boolean mSuccess = true;
    private static final boolean ENG = "eng".equals(Build.TYPE);
    private static final int RESULT_ERROR_SUCCESS = 0;
    private static final int EVENT_COPY_TEXT_MESSAGE_DONE = 106;
    private static final String EXTRA_PARAMS_VALIDITY_PERIOD = "validity_period";
    private static final String EXTRA_PARAMS_ENCODING_TYPE = "encoding_type";
    private static final int RESULT_ERROR_RUIM_PLUG_OUT = 107;
    private static final int WAKE_LOCK_TIMEOUT = 500;
    private static final String MTK_KEY_CDMA_SMS_7_BIT_ENCODING_TYPE_INT =
            "mtk_cdma_sms_7bit_encoding_type_int";
    /** Mobile manager service for phone privacy lock. */
    private PplSmsFilterExtension mPplSmsFilter = null;

    /**
     * Create a new Mtk Cdma SMS dispatcher.
     * @param phone the Phone to use
     * @param usageMonitor the SmsUsageMonitor to use
     * @param imsSMSDispatcher the object of ImsSMSDispatcher
     */
    public MtkCdmaSMSDispatcher(Phone phone, SmsUsageMonitor usageMonitor,
            ImsSMSDispatcher imsSMSDispatcher) {
        super(phone, usageMonitor, imsSMSDispatcher);
        Rlog.d(TAG, "MtkCdmaSMSDispatcher created");
    }

    @Override
    public void sendData(String destAddr, String scAddr, int destPort,
            byte[] data, PendingIntent sentIntent, PendingIntent deliveryIntent) {
        String preProcessAddr = MtkPhoneNumberUtils.cdmaCheckAndProcessPlusCodeForSms(destAddr);
        super.sendData(destAddr, scAddr, destPort, data, sentIntent, deliveryIntent);
    }

    @Override
    public void sendText(String destAddr, String scAddr, String text, PendingIntent sentIntent,
            PendingIntent deliveryIntent, Uri messageUri, String callingPkg,
            boolean persistMessage) {
        String preProcessAddr = MtkPhoneNumberUtils.cdmaCheckAndProcessPlusCodeForSms(destAddr);
        super.sendText(preProcessAddr, scAddr, text, sentIntent, deliveryIntent,
                messageUri, callingPkg, persistMessage);
    }

    @Override
    protected SmsTracker getNewSubmitPduTracker(String destinationAddress, String scAddress,
            String message, SmsHeader smsHeader, int encoding,
            PendingIntent sentIntent, PendingIntent deliveryIntent, boolean lastPart,
            AtomicInteger unsentPartCount, AtomicBoolean anyPartFailed, Uri messageUri,
            String fullMessageText) {
        UserData uData = new UserData();
        uData.payloadStr = message;
        uData.userDataHeader = smsHeader;
        if (encoding == SmsConstants.ENCODING_7BIT) {
            // MTK-START: according to the spec of China Telecom, we need to use
            // 7BIT_ASCII as the 7bit encoding type, otherwise, we may meet the issue
            // that the long sms cannot be received in Shenzhen
            /*
            uData.msgEncoding = UserData.ENCODING_GSM_7BIT_ALPHABET;
            */
            uData.msgEncoding = get7bitEncodingType();
            // MTK-END
        } else { // assume UTF-16
            uData.msgEncoding = UserData.ENCODING_UNICODE_16;
        }
        uData.msgEncodingSet = true;

        String preProcessAddr =
                MtkPhoneNumberUtils.cdmaCheckAndProcessPlusCodeForSms(destinationAddress);

        /* By setting the statusReportRequested bit only for the
         * last message fragment, this will result in only one
         * callback to the sender when that last fragment delivery
         * has been acknowledged. */
        SmsMessage.SubmitPdu submitPdu = MtkSmsMessage.getSubmitPdu(preProcessAddr,
                uData, (deliveryIntent != null) && lastPart);
        if (submitPdu != null) {

            HashMap map = getSmsTrackerMap(preProcessAddr, scAddress,
                    message, submitPdu);
            return getSmsTracker(map, sentIntent, deliveryIntent,
                    getFormat(), unsentPartCount, anyPartFailed, messageUri, smsHeader,
                    false /*isExpextMore*/, fullMessageText, true /*isText*/,
                    true /*persistMessage*/);
        } else {
            Rlog.e(TAG, "getNewSubmitPduTracker(), returned null");
            return null;
        }
    }

    @Override
    protected void sendSubmitPdu(SmsTracker tracker) {
        String ecmModeStr = TelephonyManager.getTelephonyProperty(
                mPhone.getPhoneId(), TelephonyProperties.PROPERTY_INECM_MODE, "false");
        boolean ecmMode = Boolean.parseBoolean(ecmModeStr);
        if (ecmMode) {
            if (VDBG) {
                Rlog.d(TAG, "Block SMS in Emergency Callback mode");
            }
            tracker.onFailed(mContext, SmsManager.RESULT_ERROR_NO_SERVICE, 0/*errorCode*/);
            return;
        }
        sendRawPdu(tracker);
    }

    /** {@inheritDoc} */
    @Override
    protected void sendSmsByPstn(SmsTracker tracker) {
        int ss = mPhone.getServiceState().getState();
        // if sms over IMS is not supported on data and voice is not available...
        if (!isIms() && ss != ServiceState.STATE_IN_SERVICE) {
            // MTK-START
            //tracker.onFailed(mContext, getNotInServiceError(ss), 0/*errorCode*/);
            // When CDMA card is plug out, we will power off MD3 to save power.
            // As a result, the service state will be STATE_POWER_OFF.
            // However, when GSM card is plug out, we will not power off MD1.
            // In order to keep the same behavior as the GSM.
            // We need to check the card status here.
            if (isSimAbsent()) {
                tracker.onFailed(mContext, RESULT_ERROR_GENERIC_FAILURE, 0/*errorCode*/);
            } else {
                tracker.onFailed(mContext, getNotInServiceError(ss), 0/*errorCode*/);
            }
            // MTK-END
            return;
        }
        super.sendSmsByPstn(tracker);
        if (CdmaOmhSmsUtils.isOmhCard(mPhone.getSubId())) {
            CdmaOmhSmsUtils.saveMessageIdToCard(mPhone.getSubId());
        }
    }

    @Override
    protected SmsTracker getSmsTracker(HashMap<String, Object> data, PendingIntent sentIntent,
            PendingIntent deliveryIntent, String format, AtomicInteger unsentPartCount,
            AtomicBoolean anyPartFailed, Uri messageUri, SmsHeader smsHeader,
            boolean isExpectMore, String fullMessageText, boolean isText, boolean persistMessage) {
        // Get calling app package name via UID from Binder call
        PackageManager pm = mContext.getPackageManager();
        String[] packageNames = pm.getPackagesForUid(Binder.getCallingUid());

        final int userId = UserHandle.getCallingUserId();
        // Get package info via packagemanager
        PackageInfo appInfo = null;
        if (packageNames != null && packageNames.length > 0) {
            try {
                // XXX this is lossy- apps can share a UID
                // MTK-START
                /* Because it may have multiple apks use the same uid, ex. Mms.apk and omacp.apk,
                        * we need to exactly find the correct calling apk.
                        * We should use running process to check the correct apk.
                        * If we could not find the process via pid, this apk may be killed.
                        * We will use the default behavior, find the first package name via uid.
                        */
                String packageName = getPackageNameViaProcessId(packageNames);
                if (packageName != null) {
                    packageNames[0] = packageName;
                }
                Rlog.d(TAG, "SmsTrackerFactory and get the package name via process id: " +
                        packageNames[0]);
                // MTK-END
                appInfo = pm.getPackageInfoAsUser(
                        packageNames[0], PackageManager.GET_SIGNATURES, userId);
            } catch (PackageManager.NameNotFoundException e) {
                // error will be logged in sendRawPdu
            }
        }
        // Strip non-digits from destination phone number before checking for short codes
        // and before displaying the number to the user if confirmation is required.
        String destAddr = MtkPhoneNumberUtils.extractNetworkPortion((String) data.get("destAddr"));
        SmsTracker tracker = new SmsTracker(data, sentIntent, deliveryIntent, appInfo, destAddr,
                format, unsentPartCount, anyPartFailed, messageUri, smsHeader, isExpectMore,
                fullMessageText, getSubId(), isText, persistMessage, userId);
        filterOutByPpl(mContext, tracker);
        return tracker;
    }


    // MTK-START
    /**
     * Send a data based SMS to a specific application port with original port.
     *
     * @param destAddr the address to send the message to
     * @param scAddr is the service center address or null to use
     *  the current default SMSC
     * @param destPort the port to deliver the message to
     * @param originalPort the original port the message from
     * @param data the body of the message to send
     * @param sentIntent if not NULL this <code>PendingIntent</code> is
     *  broadcast when the message is successfully sent, or failed.
     *  The result code will be <code>Activity.RESULT_OK</code> for success,
     *  or one of these errors:<br>
     *  <code>RESULT_ERROR_GENERIC_FAILURE</code><br>
     *  <code>RESULT_ERROR_RADIO_OFF</code><br>
     *  <code>RESULT_ERROR_NULL_PDU</code><br>
     *  <code>RESULT_ERROR_NO_SERVICE</code><br>.
     *  For <code>RESULT_ERROR_GENERIC_FAILURE</code> the sentIntent may include
     *  the extra "errorCode" containing a radio technology specific value,
     *  generally only useful for troubleshooting.<br>
     *  The per-application based SMS control checks sentIntent. If sentIntent
     *  is NULL the caller will be checked against all unknown applications,
     *  which cause smaller number of SMS to be sent in checking period.
     * @param deliveryIntent if not NULL this <code>PendingIntent</code> is
     *  broadcast when the message is delivered to the recipient.  The
     *  raw pdu of the status report is in the extended data ("pdu").
     */

    public void sendData(String destAddr, String scAddr, int destPort, int originalPort,
            byte[] data, PendingIntent sentIntent, PendingIntent deliveryIntent) {
        SmsMessage.SubmitPdu pdu = MtkSmsMessage.getSubmitPdu(
                scAddr, destAddr, destPort, originalPort, data, (deliveryIntent != null));
        if (pdu != null) {
            HashMap map = getSmsTrackerMap(destAddr, scAddr, destPort, data, pdu);
            SmsTracker tracker = getSmsTracker(map, sentIntent, deliveryIntent, getFormat(),
                    null /*messageUri*/, false /*isExpectMore*/, null /*fullMessageText*/,
                    false /*isText*/, true /*persistMessage*/);

            String carrierPackage = getCarrierAppPackageName();
            if (carrierPackage != null) {
                Rlog.d(TAG, "Found carrier package.");
                DataSmsSender smsSender = new DataSmsSender(tracker);
                smsSender.sendSmsByCarrierApp(carrierPackage, new SmsSenderCallback(smsSender));
            } else {
                Rlog.v(TAG, "No carrier package.");
                sendSubmitPdu(tracker);
            }
        } else {
            Rlog.e(TAG, "CdmaSMSDispatcher.sendData(): getSubmitPdu() returned null");
            if (sentIntent != null) {
                try {
                    sentIntent.send(SmsManager.RESULT_ERROR_GENERIC_FAILURE);
                } catch (CanceledException ex) {
                    Rlog.e(TAG, "Intent has been canceled!");
                }
            }
        }
    }

    /**
     * Copy a text SMS to the ICC.
     *
     * @param scAddress Service center address
     * @param address   Destination address or original address
     * @param text      List of message text
     * @param status    message status (STATUS_ON_ICC_READ, STATUS_ON_ICC_UNREAD,
     *                  STATUS_ON_ICC_SENT, STATUS_ON_ICC_UNSENT)
     * @param timestamp Timestamp when service center receive the message
     * @return success or not
     *
     */
    public int copyTextMessageToIccCard(String scAddress, String address, List<String> text,
                    int status, long timestamp) {
        mSuccess = true;

        int msgCount = text.size();
        Rlog.d(TAG, "copyTextMessageToIccCard status = " + status + ", msgCount = " + msgCount);
        if ((status != STATUS_ON_ICC_READ &&
            status != STATUS_ON_ICC_UNREAD &&
            status != STATUS_ON_ICC_SENT &&
            status != STATUS_ON_ICC_UNSENT) || (msgCount < 1)) {
            return RESULT_ERROR_GENERIC_FAILURE;
        }
        for (int i = 0; i < msgCount; ++i) {
            if (mSuccess == false) {
                return RESULT_ERROR_GENERIC_FAILURE;
            }
            SmsMessage.SubmitPdu pdu = MtkSmsMessage.createEfPdu(address, text.get(i), timestamp);
            if (pdu != null) {
                mCi.writeSmsToRuim(status, IccUtils.bytesToHexString(pdu.encodedMessage),
                        obtainMessage(EVENT_COPY_TEXT_MESSAGE_DONE));
            } else {
                return RESULT_ERROR_GENERIC_FAILURE;
            }
            synchronized (mLock) {
                mCopied = false;
                try {
                    while (!mCopied) {
                        mLock.wait();
                    }
                } catch (InterruptedException e) {
                    return RESULT_ERROR_GENERIC_FAILURE;
                }
            }
        }
        return mSuccess ? RESULT_ERROR_SUCCESS : RESULT_ERROR_GENERIC_FAILURE;
    }

    /**
     * Send an SMS with specified encoding type.
     *
     * @param destAddr the address to send the message to
     * @param scAddr the SMSC to send the message through, or NULL for the
     *  default SMSC
     * @param text the body of the message to send
     * @param encodingType the encoding type of content of message(GSM 7-bit, Unicode or Automatic)
     * @param sentIntent if not NULL this <code>PendingIntent</code> is
     *  broadcast when the message is sucessfully sent, or failed.
     *  The result code will be <code>Activity.RESULT_OK</code> for success,
     *  or one of these errors:<br>
     *  <code>RESULT_ERROR_GENERIC_FAILURE</code><br>
     *  <code>RESULT_ERROR_RADIO_OFF</code><br>
     *  <code>RESULT_ERROR_NULL_PDU</code><br>
     *  For <code>RESULT_ERROR_GENERIC_FAILURE</code> the sentIntent may include
     *  the extra "errorCode" containing a radio technology specific value,
     *  generally only useful for troubleshooting.<br>
     *  The per-application based SMS control checks sentIntent. If sentIntent
     *  is NULL the caller will be checked against all unknown applications,
     *  which cause smaller number of SMS to be sent in checking period.
     * @param deliveryIntent if not NULL this <code>PendingIntent</code> is
     *  broadcast when the message is delivered to the recipient.  The
     *  raw pdu of the status report is in the extended data ("pdu").
     * @param messageUri optional URI of the message if it is already stored in the system
     * @param callingPkg the calling package name
     * @param persistMessage whether to save the sent message into SMS DB for a
     *   non-default SMS app.
     */
    public void sendTextWithEncodingType(String destAddr, String scAddr, String text,
            int encodingType, PendingIntent sentIntent, PendingIntent deliveryIntent,
            Uri messageUri, String callingPkg, boolean persistMessage) {

        Rlog.d(TAG, "sendTextWithEncodingType");

        int encoding = encodingType;
        Rlog.d(TAG, "want to use encoding = " + encoding);

        // check is a valid encoding type
        if (encoding < 0x00 || encoding > 0x0A) {
            Rlog.w(TAG, "unavalid encoding = " + encoding);
            Rlog.w(TAG, "to use the unkown default.");
            encoding = android.telephony.SmsMessage.ENCODING_UNKNOWN;
        }

        if (encoding == android.telephony.SmsMessage.ENCODING_UNKNOWN) {
            Rlog.d(TAG, "unkown encoding, to find one best.");
            TextEncodingDetails details = MtkSmsMessage.calculateLength(text, false, encoding);
            encoding = details.codeUnitSize;
        }

        UserData uData = new UserData();
        uData.payloadStr = text;
        if (encoding == android.telephony.SmsMessage.ENCODING_7BIT) {
            uData.msgEncoding = UserData.ENCODING_7BIT_ASCII;
        } else if (encoding == android.telephony.SmsMessage.ENCODING_8BIT) {
            uData.msgEncoding = UserData.ENCODING_OCTET;
        } else {
            uData.msgEncoding = UserData.ENCODING_UNICODE_16;
        }
        uData.msgEncodingSet = true;


        SmsMessage.SubmitPdu pdu = MtkSmsMessage.getSubmitPdu(destAddr,
                uData, (deliveryIntent != null));

        if (pdu != null) {
            HashMap map = getSmsTrackerMap(destAddr, scAddr, text, pdu);
            SmsTracker tracker = getSmsTracker(map, sentIntent, deliveryIntent, getFormat(),
                    messageUri, false /*isExpectMore*/, text, true /*isText*/, persistMessage);

            String carrierPackage = getCarrierAppPackageName();
            if (carrierPackage != null) {
                Rlog.d(TAG, "Found carrier package.");
                TextSmsSender smsSender = new TextSmsSender(tracker);
                smsSender.sendSmsByCarrierApp(carrierPackage, new SmsSenderCallback(smsSender));
            } else {
                Rlog.v(TAG, "No carrier package.");
                sendSubmitPdu(tracker);
            }
        } else {
            Rlog.d(TAG, "submitPdu is null");
            if (sentIntent != null) {
                try {
                    sentIntent.send(RESULT_ERROR_NULL_PDU);
                } catch (CanceledException ex) {
                    Rlog.e(TAG, "failed to send back RESULT_ERROR_NULL_PDU");
                }
            }
        }
    }

    /**
     * Send a multi-part text based SMS with specified encoding type.
     *
     * @param destAddr the address to send the message to
     * @param scAddr is the service center address or null to use
     *   the current default SMSC
     * @param parts an <code>ArrayList</code> of strings that, in order,
     *   comprise the original message
     * @param encodingType the encoding type of content of message(GSM 7-bit, Unicode or Automatic)
     * @param sentIntents if not null, an <code>ArrayList</code> of
     *   <code>PendingIntent</code>s (one for each message part) that is
     *   broadcast when the corresponding message part has been sent.
     *   The result code will be <code>Activity.RESULT_OK</code> for success,
     *   or one of these errors:
     *   <code>RESULT_ERROR_GENERIC_FAILURE</code>
     *   <code>RESULT_ERROR_RADIO_OFF</code>
     *   <code>RESULT_ERROR_NULL_PDU</code>.
     * @param deliveryIntents if not null, an <code>ArrayList</code> of
     *   <code>PendingIntent</code>s (one for each message part) that is
     *   broadcast when the corresponding message part has been delivered
     *   to the recipient.  The raw pdu of the status report is in the
     *   extended data ("pdu").
     * @param messageUri optional URI of the message if it is already stored in the system
     * @param callingPkg the calling package name
     * @param persistMessage whether to save the sent message into SMS DB for a
     *   non-default SMS app.
     */
    public void sendMultipartTextWithEncodingType(String destAddr, String scAddr,
            ArrayList<String> parts, int encodingType, ArrayList<PendingIntent> sentIntents,
            ArrayList<PendingIntent> deliveryIntents, Uri messageUri, String callingPkg,
            boolean persistMessage) {
        if (parts == null) {
            Rlog.e(TAG, "sendMultipartTextWithEncodingType:" +
                    " Cannot send multipart text. parts=" + parts);
            return;
        }

        final String fullMessageText = getMultipartMessageText(parts);

        // impl
        Rlog.d(TAG, "CdmaSMSDispatcher, implemented by for interfaces needed." +
                " sendMultipartTextWithEncodingType");
        int refNumber = getNextConcatenatedRef() & 0x00FF;
        int msgCount = parts.size();
        int encoding = encodingType;
        Rlog.d(TAG, "want to use encoding = " + encoding);

        // check is a valid encoding type
        if (encoding < 0x00 || encoding > 0x0A) {
            Rlog.w(TAG, "unavalid encoding = " + encoding);
            Rlog.w(TAG, "to use the unkown default.");
            encoding = android.telephony.SmsMessage.ENCODING_UNKNOWN;
        }

        TextEncodingDetails[] encodingForParts = new TextEncodingDetails[msgCount];
        if (encoding == android.telephony.SmsMessage.ENCODING_UNKNOWN) {
            Rlog.d(TAG, "unkown encoding, to find one best.");
            for (int i = 0; i < msgCount; i++) {
                TextEncodingDetails details = calculateLength(parts.get(i), false);
                if (encoding != details.codeUnitSize
                        && (encoding == android.telephony.SmsMessage.ENCODING_UNKNOWN
                        || encoding == android.telephony.SmsMessage.ENCODING_7BIT)) {
                    encoding = details.codeUnitSize;
                }
                encodingForParts[i] = details;
            }
        } else {
            Rlog.d(TAG, "APP want use specified encoding type.");
            for (int i = 0; i < msgCount; i++) {
                TextEncodingDetails details =
                        MtkSmsMessage.calculateLength(parts.get(i), false, encoding);
                details.codeUnitSize = encoding;
                encodingForParts[i] = details;
            }
        }

        SmsTracker[] trackers = new SmsTracker[msgCount];

        // States to track at the message level (for all parts)
        final AtomicInteger unsentPartCount = new AtomicInteger(msgCount);
        final AtomicBoolean anyPartFailed = new AtomicBoolean(false);

        Rlog.d(TAG, "now to send one by one, msgCount = " + msgCount);
        for (int i = 0; i < msgCount; i++) {
            SmsHeader.ConcatRef concatRef = new SmsHeader.ConcatRef();
            concatRef.refNumber = refNumber;
            concatRef.seqNumber = i + 1;  // 1-based sequence
            concatRef.msgCount = msgCount;
            // TODO: We currently set this to true since our messaging app will never
            // send more than 255 parts (it converts the message to MMS well before that).
            // However, we should support 3rd party messaging apps that might need 16-bit
            // references
            // Note:  It's not sufficient to just flip this bit to true; it will have
            // ripple effects (several calculations assume 8-bit ref).
            concatRef.isEightBits = true;
            SmsHeader smsHeader = new SmsHeader();
            smsHeader.concatRef = concatRef;

            PendingIntent sentIntent = null;
            if (sentIntents != null && sentIntents.size() > i) {
                sentIntent = sentIntents.get(i);
            }

            PendingIntent deliveryIntent = null;
            if (deliveryIntents != null && deliveryIntents.size() > i) {
                deliveryIntent = deliveryIntents.get(i);
            }

            trackers[i] =
                getNewSubmitPduTracker(destAddr, scAddr, parts.get(i), smsHeader,
                        encodingForParts[i].codeUnitSize,
                        sentIntent, deliveryIntent, (i == (msgCount - 1)),
                        unsentPartCount, anyPartFailed, messageUri, fullMessageText);
        }

        if (trackers == null || trackers.length == 0
                    || trackers[0] == null) {
            Rlog.e(TAG, "sendMultipartTextWithEncodingType:" +
                    " Cannot send multipart text. parts=" + parts);
            return;
        }

        String carrierPackage = getCarrierAppPackageName();
        if (carrierPackage != null) {
            Rlog.d(TAG, "sendMultipartTextWithEncodingType: Found carrier package.");
            MultipartSmsSender smsSender = new MultipartSmsSender(parts, trackers);
            smsSender.sendSmsByCarrierApp(carrierPackage,
                    new MultipartSmsSenderCallback(smsSender));
        } else {
            Rlog.v(TAG, "sendMultipartTextWithEncodingType: No carrier package.");
            for (SmsTracker tracker : trackers) {
                if (tracker != null) {
                    sendSubmitPdu(tracker);
                } else {
                    Rlog.e(TAG, "sendMultipartTextWithEncodingType: Null tracker.");
                }
            }
        }
    }

    /**
     * Send an SMS with specified encoding type.
     *
     * @param destAddr the address to send the message to
     * @param scAddr the SMSC to send the message through, or NULL for the
     *  default SMSC
     * @param text the body of the message to send
     * @param extraParams extra parameters, such as validity period, encoding type
     * @param sentIntent if not NULL this <code>PendingIntent</code> is
     *  broadcast when the message is sucessfully sent, or failed.
     * @param deliveryIntent if not NULL this <code>PendingIntent</code> is
     *  broadcast when the message is delivered to the recipient.  The
     *  raw pdu of the status report is in the extended data ("pdu").
     * @param messageUri optional URI of the message if it is already stored in the system
     * @param callingPkg the calling package name
     * @param persistMessage whether to save the sent message into SMS DB for a
     *   non-default SMS app.
     */
    public void sendTextWithExtraParams(String destAddr, String scAddr, String text,
            Bundle extraParams, PendingIntent sentIntent, PendingIntent deliveryIntent,
            Uri messageUri, String callingPkg, boolean persistMessage) {

        Rlog.d(TAG, "sendTextWithExtraParams");

        int validityPeriod;
        int priority;
        int encoding;

        if (extraParams == null) {
            Rlog.d(TAG, "extraParams == null, will encoding with no extra feature.");
            validityPeriod = -1;
            priority = -1;
            encoding = android.telephony.SmsMessage.ENCODING_UNKNOWN;
        } else {
            validityPeriod = extraParams.getInt(EXTRA_PARAMS_VALIDITY_PERIOD, -1);
            if ((validityPeriod > 244) && (validityPeriod <= 255)) {
                validityPeriod = 244;
            }
            priority = extraParams.getInt("priority", -1);
            encoding = extraParams.getInt(EXTRA_PARAMS_ENCODING_TYPE, 0);
        }

        Rlog.d(TAG, "validityPeriod is " + validityPeriod);
        Rlog.d(TAG, "priority is " + priority);
        Rlog.d(TAG, "want to use encoding = " + encoding);

        // check is a valid encoding type
        if (encoding < 0x00 || encoding > 0x0A) {
            Rlog.w(TAG, "unavalid encoding = " + encoding);
            Rlog.w(TAG, "to use the unkown default.");
            encoding = android.telephony.SmsMessage.ENCODING_UNKNOWN;
        }

        if (encoding == android.telephony.SmsMessage.ENCODING_UNKNOWN) {
            Rlog.d(TAG, "unkown encoding, to find one best.");
            TextEncodingDetails details = calculateLength(text, false);
            encoding = details.codeUnitSize;
        }


        SmsMessage.SubmitPdu pdu =
                MtkSmsMessage.getSubmitPdu(scAddr, destAddr, text,
                (deliveryIntent != null), null, encoding, validityPeriod, priority);

        if (pdu != null) {
            HashMap map = getSmsTrackerMap(destAddr, scAddr, text, pdu);
            SmsTracker tracker = getSmsTracker(map, sentIntent, deliveryIntent, getFormat(),
                    messageUri, false /*isExpectMore*/, text, true /*isText*/, persistMessage);

            String carrierPackage = getCarrierAppPackageName();
            if (carrierPackage != null) {
                Rlog.d(TAG, "Found carrier package.");
                TextSmsSender smsSender = new TextSmsSender(tracker);
                smsSender.sendSmsByCarrierApp(carrierPackage, new SmsSenderCallback(smsSender));
            } else {
                Rlog.v(TAG, "No carrier package.");
                sendSubmitPdu(tracker);
            }
        } else {
            Rlog.d(TAG, "submitPdu is null");
            if (sentIntent != null) {
                try {
                    sentIntent.send(RESULT_ERROR_NULL_PDU);
                } catch (CanceledException ex) {
                    Rlog.e(TAG, "failed to send back RESULT_ERROR_NULL_PDU");
                }
            }
        }
    }

    /**
     * Send a multi-part text based SMS with specified encoding type.
     *
     * @param destAddr the address to send the message to
     * @param scAddr is the service center address or null to use
     *   the current default SMSC
     * @param parts an <code>ArrayList</code> of strings that, in order,
     *   comprise the original message
     * @param extraParams extra parameters, such as validity period, encoding type
     * @param sentIntents if not null, an <code>ArrayList</code> of
     *   <code>PendingIntent</code>s (one for each message part) that is
     *   broadcast when the corresponding message part has been sent.
     * @param deliveryIntents if not null, an <code>ArrayList</code> of
     *   <code>PendingIntent</code>s (one for each message part) that is
     *   broadcast when the corresponding message part has been delivered
     *   to the recipient.  The raw pdu of the status report is in the
     *   extended data ("pdu").
     * @param messageUri optional URI of the message if it is already stored in the system
     * @param callingPkg the calling package name
     * @param persistMessage whether to save the sent message into SMS DB for a
     *   non-default SMS app.
     */
    public void sendMultipartTextWithExtraParams(String destAddr, String scAddr,
            ArrayList<String> parts, Bundle extraParams, ArrayList<PendingIntent> sentIntents,
            ArrayList<PendingIntent> deliveryIntents, Uri messageUri, String callingPkg,
            boolean persistMessage) {

        if (parts == null) {
            Rlog.e(TAG, "sendMultipartTextWithExtraParams: Cannot send multipart text. parts=" +
                    parts);
            return;
        }

        final String fullMessageText = getMultipartMessageText(parts);

        // impl
        Rlog.d(TAG, "CdmaSMSDispatcher, implemented by for interfaces needed." +
                " sendMultipartTextWithExtraParams");
        int validityPeriod;
        int priority;
        int encoding;

        if (extraParams == null) {
            Rlog.d(TAG, "extraParams == null, will encoding with no extra feature.");
            validityPeriod = -1;
            priority = -1;
            encoding = android.telephony.SmsMessage.ENCODING_UNKNOWN;
        } else {
            validityPeriod = extraParams.getInt(EXTRA_PARAMS_VALIDITY_PERIOD, -1);
            if ((validityPeriod > 244) && (validityPeriod <= 255)) {
                validityPeriod = 244;
            }
            priority = extraParams.getInt("priority", -1);
            encoding = extraParams.getInt(EXTRA_PARAMS_ENCODING_TYPE, 0);
        }

        Rlog.d(TAG, "validityPeriod is " + validityPeriod);
        Rlog.d(TAG, "priority is " + priority);
        Rlog.d(TAG, "want to use encoding = " + encoding);

        int refNumber = getNextConcatenatedRef() & 0x00FF;
        int msgCount = parts.size();

        // check is a valid encoding type
        if (encoding < 0x00 || encoding > 0x0A) {
            Rlog.w(TAG, "unavalid encoding = " + encoding);
            Rlog.w(TAG, "to use the unkown default.");
            encoding = android.telephony.SmsMessage.ENCODING_UNKNOWN;
        }

        TextEncodingDetails[] encodingForParts = new TextEncodingDetails[msgCount];
        if (encoding == android.telephony.SmsMessage.ENCODING_UNKNOWN) {
            Rlog.d(TAG, "unkown encoding, to find one best.");
            for (int i = 0; i < msgCount; i++) {
                TextEncodingDetails details = calculateLength(parts.get(i), false);
                if (encoding != details.codeUnitSize
                        && (encoding == android.telephony.SmsMessage.ENCODING_UNKNOWN
                        || encoding == android.telephony.SmsMessage.ENCODING_7BIT)) {
                    encoding = details.codeUnitSize;
                }
                encodingForParts[i] = details;
            }
        } else {
            Rlog.d(TAG, "APP want use specified encoding type.");
            for (int i = 0; i < msgCount; i++) {
                TextEncodingDetails details =
                        MtkSmsMessage.calculateLength(parts.get(i), false, encoding);
                details.codeUnitSize = encoding;
                encodingForParts[i] = details;
            }
        }

        SmsTracker[] trackers = new SmsTracker[msgCount];

        // States to track at the message level (for all parts)
        final AtomicInteger unsentPartCount = new AtomicInteger(msgCount);
        final AtomicBoolean anyPartFailed = new AtomicBoolean(false);

        Rlog.d(TAG, "now to send one by one, msgCount = " + msgCount);
        for (int i = 0; i < msgCount; i++) {
            SmsHeader.ConcatRef concatRef = new SmsHeader.ConcatRef();
            concatRef.refNumber = refNumber;
            concatRef.seqNumber = i + 1;  // 1-based sequence
            concatRef.msgCount = msgCount;
            // TODO: We currently set this to true since our messaging app will never
            // send more than 255 parts (it converts the message to MMS well before that).
            // However, we should support 3rd party messaging apps that might need 16-bit
            // references
            // Note:  It's not sufficient to just flip this bit to true; it will have
            // ripple effects (several calculations assume 8-bit ref).
            concatRef.isEightBits = true;
            SmsHeader smsHeader = new SmsHeader();
            smsHeader.concatRef = concatRef;

            PendingIntent sentIntent = null;
            if (sentIntents != null && sentIntents.size() > i) {
                sentIntent = sentIntents.get(i);
            }

            PendingIntent deliveryIntent = null;
            if (deliveryIntents != null && deliveryIntents.size() > i) {
                deliveryIntent = deliveryIntents.get(i);
            }

            trackers[i] =
                getNewSubmitPduTracker(destAddr, scAddr, parts.get(i), smsHeader,
                        encodingForParts[i].codeUnitSize,
                        sentIntent, deliveryIntent, (i == (msgCount - 1)),
                        unsentPartCount, anyPartFailed, messageUri, fullMessageText,
                        validityPeriod, priority);
        }

        if (trackers == null || trackers.length == 0
                    || trackers[0] == null) {
            Rlog.e(TAG, "sendMultipartTextWithExtraParams: Cannot send multipart text. parts=" +
                    parts);
            return;
        }

        String carrierPackage = getCarrierAppPackageName();
        if (carrierPackage != null) {
            Rlog.d(TAG, "sendMultipartTextWithExtraParams: Found carrier package.");
            MultipartSmsSender smsSender = new MultipartSmsSender(parts, trackers);
            smsSender.sendSmsByCarrierApp(carrierPackage,
                    new MultipartSmsSenderCallback(smsSender));
        } else {
            Rlog.v(TAG, "sendMultipartTextWithExtraParams: No carrier package.");
            for (SmsTracker tracker : trackers) {
                if (tracker != null) {
                    sendSubmitPdu(tracker);
                } else {
                    Rlog.e(TAG, "sendMultipartTextWithExtraParams: Null tracker.");
                }
            }
        }
    }


    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case EVENT_COPY_TEXT_MESSAGE_DONE:
            {
                AsyncResult ar;
                ar = (AsyncResult) msg.obj;
                synchronized (mLock) {
                    mSuccess = (ar.exception == null);
                    mCopied = true;
                    mLock.notifyAll();
                }
                break;
            }

            default:
                super.handleMessage(msg);
        }
    }

    @Override
    protected void handleSendComplete(AsyncResult ar) {
        SmsTracker tracker = (SmsTracker) ar.userObj;
        if (ar.exception != null) {
            if (PhoneConstants.PHONE_TYPE_CDMA == mPhone.getPhoneType()) {
                if (ar.result != null) {
                    int errorCode = ((SmsResponse) ar.result).mErrorCode;
                    if (errorCode == RESULT_ERROR_RUIM_PLUG_OUT) {
                        Rlog.d(TAG, "RUIM card is plug out");
                        tracker.onFailed(mContext, RESULT_ERROR_GENERIC_FAILURE, errorCode);
                        return;
                    }
                }
            }
        }
        super.handleSendComplete(ar);
    }

    private boolean isSimAbsent() {
        IccCard card = PhoneFactory.getPhone(mPhone.getPhoneId()).getIccCard();
        State state;;
        if (card == null) {
            state = IccCardConstants.State.UNKNOWN;
        } else {
            state = card.getState();
        }
        boolean ret =  (state == IccCardConstants.State.ABSENT ||
                state == IccCardConstants.State.NOT_READY);
        Rlog.d(TAG, "isSimAbsent state = " + state + " ret=" + ret);
        return ret;
    }

    private SmsTracker getNewSubmitPduTracker(String destinationAddress, String scAddress,
            String message, SmsHeader smsHeader, int encoding,
            PendingIntent sentIntent, PendingIntent deliveryIntent, boolean lastPart,
            AtomicInteger unsentPartCount, AtomicBoolean anyPartFailed, Uri messageUri,
            String fullMessageText, int validityPeriod, int priority) {
        SmsMessage.SubmitPdu submitPdu = MtkSmsMessage.getSubmitPdu(scAddress, destinationAddress,
                    message, (deliveryIntent != null) && lastPart, smsHeader,
                    encoding, validityPeriod, priority);
        if (submitPdu != null) {
            HashMap map =  getSmsTrackerMap(destinationAddress, scAddress,
                    message, submitPdu);
            return getSmsTracker(map, sentIntent, deliveryIntent,
                    getFormat(), unsentPartCount, anyPartFailed, messageUri, smsHeader,
                    false /*isExpextMore*/, fullMessageText, true /*isText*/,
                    true);
        } else {
            Rlog.e(TAG, "CDMASMSDispatcher.getNewSubmitPduTracker(), returned null, B");
            return null;
        }
    }

    private int get7bitEncodingType() {
        int type;
        CarrierConfigManager configMgr = (CarrierConfigManager)
                mPhone.getContext().getSystemService(Context.CARRIER_CONFIG_SERVICE);
        PersistableBundle b = configMgr.getConfigForSubId(mPhone.getSubId());
        if (b != null) {
            type = b.getInt(MTK_KEY_CDMA_SMS_7_BIT_ENCODING_TYPE_INT,
                    UserData.ENCODING_GSM_7BIT_ALPHABET);
        } else {
            type = UserData.ENCODING_GSM_7BIT_ALPHABET;
        }
        Rlog.d(TAG, "get7bitEncodingType = " + type);
        return type;
    }

    /**
     * Filter out the MO sms by phone privacy lock.
     * For mobile manager service, the native apk needs to send a special sms to server and
     * doesn't want to show to end user. But sms frameworks will help to write to database if
     * app is not default sms application.
     * Therefore, sms framework need to filter out this kind of sms and not showing to end user.
     *
     * @param destAddr destination address
     * @param text content of message
     *
     * @return true filter out by ppl; false not filter out by ppl
     */
    private void filterOutByPpl(Context context, SmsTracker tracker) {
        // Create the instance for phone privacy lock
        if (mPplSmsFilter == null) {
            mPplSmsFilter = new PplSmsFilterExtension(context);
        }

        boolean pplResult = false;

        if (!MtkSmsCommonEventHelper.isPrivacyLockSupport()) {
            return;
        }

        // Start to check phone privacy check if it does not need to write to database
        if (ENG) {
            Rlog.d(TAG, "[PPL] Phone privacy check start");
        }

        Bundle pplData = new Bundle();
        pplData.putString(mPplSmsFilter.KEY_MSG_CONTENT, tracker.mFullMessageText);
        pplData.putString(mPplSmsFilter.KEY_DST_ADDR, tracker.mDestAddress);
        pplData.putString(mPplSmsFilter.KEY_FORMAT, tracker.mFormat);
        pplData.putInt(mPplSmsFilter.KEY_SUB_ID, tracker.mSubId);
        pplData.putInt(mPplSmsFilter.KEY_SMS_TYPE, 1);

        pplResult = mPplSmsFilter.pplFilter(pplData);
        if (pplResult) {
            tracker.mPersistMessage = false;
        }

        if (ENG) {
            Rlog.d(TAG, "[PPL] Phone privacy check end, Need to filter(result) = "
                    + pplResult);
        }
    }

    /**
     * Because it may have multiple apks use the same uid, ex. Mms.apk and omacp.apk, we need to
     * exactly find the correct calling apk. We should use running process to check the correct
     * apk. If we could not find the process via pid, this apk may be killed. We will use the
     * default behavior, find the first package name via uid.
     *
     * @param packageNames package names query from user id
     * @return null if package names length is zero or could not match the process id;
     *         package name match the process id
     */
    private String getPackageNameViaProcessId(String[] packageNames) {
        String packageName = null;

        if (packageNames.length == 1) {
            packageName = packageNames[0];
        } else if (packageNames.length > 1) {
            int callingPid = Binder.getCallingPid();
            Iterator index = null;

            ActivityManager am = (ActivityManager) mContext.getSystemService(
                    Context.ACTIVITY_SERVICE);
            List processList = am.getRunningAppProcesses();
            if (processList != null) {
                index = processList.iterator();
                while (index.hasNext()) {
                    ActivityManager.RunningAppProcessInfo processInfo =
                            (ActivityManager.RunningAppProcessInfo) (index.next());
                    if (callingPid == processInfo.pid) {
                        for (String pkgInProcess : processInfo.pkgList) {
                            for (String pkg : packageNames) {
                                if (pkg.equals(pkgInProcess)) {
                                    packageName = pkg;
                                    break;
                                }
                            }
                            if (packageName != null) {
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }

        return packageName;
    }
}
