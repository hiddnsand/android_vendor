package com.mediatek.op18.phone;

import android.content.Context;
import android.util.Log;



import com.mediatek.phone.ext.ICallFeaturesSettingExt;
import com.mediatek.phone.ext.IMobileNetworkSettingsExt;
import com.mediatek.phone.ext.IPhoneMiscExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;

public class Op18PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {
    private Context mContext;

    public Op18PhoneCustomizationFactory(Context context) {
        mContext = context;
    }

    public ICallFeaturesSettingExt makeCallFeaturesSettingExt() {
        return new Op18CallFeaturesSettingExt(mContext);
    }

    public IPhoneMiscExt makePhoneMiscExt() {
        return new Op18PhoneMiscExt(mContext);
    }


    public IMobileNetworkSettingsExt makeMobileNetworkSettingsExt() {
        Log.i("OP18PhoneCustomizationFactory", "makeMobileNetworkSettingsExt");
        return new OP18MobileNetworkSettingsExt(mContext);
    }
}
