#ifndef UTILS_TOOLS_H_
#define UTILS_TOOLS_H_

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>

#include <utils/Errors.h>

#include <hardware/hwcomposer.h>

#include "ui/gralloc_extra.h"
#include "graphics_mtk_defs.h"

#define LOCK_FOR_HW (GRALLOC_USAGE_SW_READ_RARELY | \
                     GRALLOC_USAGE_SW_WRITE_NEVER | \
                     GRALLOC_USAGE_HW_TEXTURE)

#define SWAP(a, b) do { typeof(a) __a = (a); (a) = (b); (b) = __a; } while (0)

#define WIDTH(rect) ((rect).right - (rect).left)
#define HEIGHT(rect) ((rect).bottom - (rect).top)
#define SIZE(rect) (WIDTH(rect) * HEIGHT(rect))

inline int grallocExtraQuery(buffer_handle_t handle, GRALLOC_EXTRA_ATTRIBUTE_QUERY attribute, void * out_pointer)
{
    //query if handle !=0
    if(handle != 0){
        return gralloc_extra_query(handle, attribute, out_pointer);
    }else{
        HWC_LOGE("Error: handle = 0: %s (%d) " , __FILE__, __LINE__);
        return 0;
    }
}

inline int grallocExtraPerform(buffer_handle_t handle, GRALLOC_EXTRA_ATTRIBUTE_PERFORM attribute, void * in_pointer)
{
    //query if handle !=0
    if(handle != 0){
        return gralloc_extra_perform(handle, attribute, in_pointer);
    }else{
        HWC_LOGE("Error: handle = 0: %s (%d) " , __FILE__, __LINE__);
        return 0;
    }
}

inline int getBitsPerPixel(const uint32_t format)
{
    switch (format)
    {
        case HAL_PIXEL_FORMAT_RGBA_8888:
        case HAL_PIXEL_FORMAT_RGBX_8888:
        case HAL_PIXEL_FORMAT_BGRA_8888:
        case HAL_PIXEL_FORMAT_IMG1_BGRX_8888:
            return 32;

        case HAL_PIXEL_FORMAT_RGB_888:
            return 24;

        case HAL_PIXEL_FORMAT_RGB_565:
            return 16;

        case HAL_PIXEL_FORMAT_NV12_BLK_FCM:
        case HAL_PIXEL_FORMAT_NV12_BLK:
        case HAL_PIXEL_FORMAT_I420:
        case HAL_PIXEL_FORMAT_YV12:
        case HAL_PIXEL_FORMAT_YUV_PRIVATE:
            return 16;

        default:
            HWC_LOGW("Not support format(%d) for bpp", format);
            return 0;
    }
}

inline int getSrcLeft(hwc_layer_1_t* layer)
{
    return (int)(ceilf(layer->sourceCropf.left));
}

inline int getSrcTop(hwc_layer_1_t* layer)
{
    return (int)(ceilf(layer->sourceCropf.top));
}

inline int getSrcWidth(hwc_layer_1_t* layer)
{
    int left = (int)(ceilf(layer->sourceCropf.left));
    int right = (int)(floorf(layer->sourceCropf.right));
    return (right - left);
}

inline int getSrcHeight(hwc_layer_1_t* layer)
{
    int top = (int)(ceilf(layer->sourceCropf.top));
    int bottom = (int)(floorf(layer->sourceCropf.bottom));
    return (bottom - top);
}

inline bool scaled(hwc_layer_1_t* layer)
{
    int w = getSrcWidth(layer);
    int h = getSrcHeight(layer);

    if (layer->transform & HWC_TRANSFORM_ROT_90)
        SWAP(w, h);

    return (WIDTH(layer->displayFrame) != w || HEIGHT(layer->displayFrame) != h);
}

struct PrivateHandle
{
    PrivateHandle()
/*
        : ion_fd(-1)
        , fb_mva(0)
        , sec_handle(0)
        , roi_bytes(-1)
*/
    { }

    // TODO: enable after phasing out deprecated gralloc_extra_XXX() methods in HWC
/*
    int ion_fd;
    void* fb_mva;
    uintptr_t* sec_handle;
    int roi_bytes;

    unsigned int width;
    unsigned int height;
    unsigned int stride;
    unsigned int vstride;
    unsigned int format;
    int size; // total bytes allocated by gralloc
    int usage;
*/
    gralloc_extra_ion_sf_info_t ext_info;
};

inline int getPrivateHandleInfo(
    buffer_handle_t handle, PrivateHandle* priv_handle)
{
    int err = 0;
    // TODO: enable after phasing out deprecated gralloc_extra_XXX() methods in HWC
/*
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_WIDTH, &priv_handle->width);
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_HEIGHT, &priv_handle->height);
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_STRIDE, &priv_handle->stride);
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_VERTICAL_STRIDE, &priv_handle->vstride);
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_FORMAT, &priv_handle->format);
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_ALLOC_SIZE, &priv_handle->size);
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_USAGE, &priv_handle->usage);
*/
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_IOCTL_ION_SF_INFO, &priv_handle->ext_info);

    if (err) HWC_LOGE("%s err(%x), (handle=%p)", __func__, err, handle);

    return err;
}

inline int getIonFd(buffer_handle_t handle)
{
    int ion_fd = -1;
    int err = grallocExtraQuery(handle, GRALLOC_EXTRA_GET_ION_FD, &ion_fd);

    if (GRALLOC_EXTRA_OK != err)
    {
        HWC_LOGE("ION fd is invalid !! (handle=%p) !!", handle);
    }
    //else
    //{
    //    HWC_LOGD("Get ION fd=%d, handle=%p", ion_fd, handle);
    //}

    return ion_fd;
}

typedef struct hwc_buffer_info_t
{
    int width;
    int height;
    int stride;
    int format;
    int usage;
    int status;
} hwc_buffer_info_t;

inline int getBufferInfo(buffer_handle_t handle, hwc_buffer_info_t* info)
{
    int err = 0;
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_WIDTH, &info->width);
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_HEIGHT, &info->height);
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_STRIDE, &info->stride);
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_FORMAT, &info->format);
    err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_USAGE, &info->usage);

    if(info->usage & GRALLOC_USAGE_HW_FB)
    {
        info->status = 0;
    }
    else
    {
        int ion_fd = -1;
        ion_fd = getIonFd(handle);
        gralloc_extra_ion_sf_info_t sfinfo;
        err |= grallocExtraQuery(handle, GRALLOC_EXTRA_GET_IOCTL_ION_SF_INFO, &sfinfo);
	    info->status = sfinfo.status;
    }

    return err;
}

inline void clearListFbt(struct hwc_display_contents_1* list)
{
    list->retireFenceFd = -1;
    if (list->outbufAcquireFenceFd != -1) ::close(list->outbufAcquireFenceFd);

    hwc_layer_1_t* layer = &list->hwLayers[list->numHwLayers - 1];
    layer->releaseFenceFd = -1;
    if (layer->acquireFenceFd != -1) ::close(layer->acquireFenceFd);
}

inline void clearListAll(struct hwc_display_contents_1* list)
{
    list->retireFenceFd = -1;
    if (list->outbufAcquireFenceFd != -1) ::close(list->outbufAcquireFenceFd);

    for (uint32_t i = 0; i < list->numHwLayers; i++)
    {
        hwc_layer_1_t* layer = &list->hwLayers[i];
        layer->releaseFenceFd = -1;
        if (layer->acquireFenceFd != -1) ::close(layer->acquireFenceFd);
    }
}

struct dump_buff
{
    char *msg;
    int msg_len;
    int len;
};

static void dump_printf(struct dump_buff* buff, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    buff->len += vsnprintf(buff->msg + buff->len, buff->msg_len - buff->len, fmt, ap);
    va_end(ap);
}

#endif // UTILS_TOOLS_H_
