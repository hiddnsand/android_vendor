package com.mediatek.op01.dialer.speeddial;


import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.content.pm.PackageManager;
import android.Manifest;
import android.util.Log;
import java.util.ArrayList;

public class PermissionUtils {

    private static final String TAG = "PermissionUtils";

    public static final String[] ALL_PERMISSIONS = {
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.READ_CONTACTS,
        Manifest.permission.WRITE_CONTACTS};

    private static final int REQUIRED_PERMISSIONS_REQUEST_CODE = 1;

    public static final String PREVIOUS_ACTIVITY_INTENT = "previous_activity_intent";
    public static final String MISSING_PERMISSIONS = "missing_permissions";

    public static boolean requestAllPermissions(Activity activity) {
        Log.d(TAG, "requestAllPermissions");
        return requestPermissions(activity, ALL_PERMISSIONS);
    }

    public static boolean requestPermissions(Activity activity, String[] requiredPermissions) {
        Log.d(TAG, "requestPermissions");
        ArrayList<String> needPermissions = getNeedPermissions(activity, requiredPermissions);

        if (needPermissions.size() == 0) {
            Log.d(TAG, "requestAllPermissions, granted!");
            return false;
        }

        final String[] missingArray = new String[needPermissions.size()];
        needPermissions.toArray(missingArray);

        Intent intentPermissions = new Intent(activity, PermissionCheckActivity.class);
        intentPermissions.putExtra(PREVIOUS_ACTIVITY_INTENT, activity.getIntent());
        intentPermissions.putExtra(MISSING_PERMISSIONS, missingArray);

        activity.startActivity(intentPermissions);
        activity.finish();
        return true;
    }

    public static ArrayList<String> getNeedPermissions(
            Activity activity, String[] requiredPermissions) {
        final ArrayList<String> needPermissions = new ArrayList<String>();

        for (int i = 0; i < requiredPermissions.length; i++) {
            if (!hasPermission(activity, requiredPermissions[i])) {
                needPermissions.add(requiredPermissions[i]);
            }
        }
        return needPermissions;
    }

    public static boolean hasPermission(Context context, String permission) {
        return context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }
}


