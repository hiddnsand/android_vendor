#ifndef __IFPSPOLICYSERVICE_H__
#define __IFPSPOLICYSERVICE_H__

#include <binder/IInterface.h>
#include <binder/Parcel.h>
#include <binder/BinderService.h>

namespace android {
class IFpsPolicy;
class String8;
class FpsPolicyInfo;
class HwcInfo;
class SimpleInputWindowInfo;
class IFpsInfo;
class PanelInfo;
class FpsRange;
class DupBitTube;

class IFpsPolicyService : public IInterface
{
protected:
    enum {
        FPS_POLICY_REGPOLICY = IBinder::FIRST_CALL_TRANSACTION,
        FPS_POLICY_UNREGPOLICY,
        FPS_POLICY_SETFPS,
        FPS_POLICY_SETSINGLELAYER,
        FPS_POLICY_SETNUMBERDISPLAY,
        FPS_POLICY_SETHWCINFO,
        FPS_POLICY_SETINPUTWINDOWS,
        FPS_POLICY_REGINFO,
        FPS_POLICY_UNREGINFO,
        FPS_POLICY_GETPANELINFO,
        FPS_POLICY_SETFOREGROUNDINFO,
        FPS_POLICY_GETFPSRANGE,
        FPS_POLICY_SETWINDOWFLAG,
        FPS_POLICY_ENABLETRACKER,
        FPS_POLICY_FORBIDADJUSTINGVSYNC,
    };

public:
    DECLARE_META_INTERFACE(FpsPolicyService);

    virtual status_t regPolicy(const sp<IFpsPolicy>& policy, const String8& name,
                               const FpsPolicyInfo& info, sp<DupBitTube>& channel) = 0;
    virtual status_t unregPolicy(uint64_t sequence) = 0;
    virtual status_t setFps(const FpsPolicyInfo& info) = 0;
    virtual status_t setSingleLayer(int32_t single) = 0;
    virtual status_t setNumberDisplay(int32_t num) = 0;
    virtual status_t setHwcInfo(const HwcInfo& info) = 0;
    virtual status_t setInputWindows(const String8& name, const SimpleInputWindowInfo& info) = 0;
    virtual status_t regInfo(const sp<IFpsInfo>& info, uint64_t id) = 0;
    virtual status_t unregInfo(uint64_t id) = 0;
    virtual status_t getPanelInfo(PanelInfo& info) = 0;
    virtual status_t setForegroundInfo(int32_t pid, String8& packageName) = 0;
    virtual status_t getFpsRange(FpsRange& range) = 0;
    virtual status_t setWindowFlag(int32_t flag, int32_t mask) = 0;
    virtual status_t enableTracker(int32_t enable) = 0;
    virtual status_t forbidAdjustingVsync(int32_t forbid) = 0;
};

class BnFpsPolicyService : public BnInterface<IFpsPolicyService>
{
public:
    virtual status_t onTransact(uint32_t code,
                                const Parcel& data,
                                Parcel* reply,
                                uint32_t flags = 0);
};

sp<IFpsPolicyService> checkFpsPolicyService();

};

#endif
