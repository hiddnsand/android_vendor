/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/* //device/libs/android_runtime/android_media_MediaPlayer.cpp
 **
 ** Copyright 2007, The Android Open Source Project
 **
 ** Licensed under the Apache License, Version 2.0 (the "License");
 ** you may not use this file except in compliance with the License.
 ** You may obtain a copy of the License at
 **
 **     http://www.apache.org/licenses/LICENSE-2.0
 **
 ** Unless required by applicable law or agreed to in writing, software
 ** distributed under the License is distributed on an "AS IS" BASIS,
 ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ** See the License for the specific language governing permissions and
 ** limitations under the License.
 */

//#define LOG_NDEBUG 0
//#define LOG_TAG "emBtTest-JNI"
//#include "utils/Log.h"


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <limits.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <utils/threads.h>
#include <dlfcn.h>
#include <string.h>
#include <sys/types.h>
#include "jni.h"
#include "JNIHelp.h"
#include "android_runtime/AndroidRuntime.h"
#include "utils/Errors.h"  // for status_t
#undef LOG_NDEBUG
#undef NDEBUG

#ifdef LOG_TAG
#undef LOG_TAG
#define LOG_TAG "emBtTest-JNI"
#endif

#include "bt_em.h"

#include <cutils/log.h>
#include <cutils/sockets.h>
#include <cutils/properties.h>
//typedef int BOOL;

extern "C" void RELAYER_exit(void);
extern "C" BOOL RELAYER_start(int serial_port, int serial_speed);


//----------------------------------------------------------------------------
using namespace android;

//----------------------------------------------------------------------------

struct fields_t {
    jfieldID patter;
    jfieldID channels;
    jfieldID pocketType;
    jfieldID pocketTypeLen;
    jfieldID freq;
    jfieldID power;
};
static fields_t fields;

static unsigned char Pattern_Map[] = { 0x01, 0x02, 0x03, 0x04, 0x09 };
static unsigned char PocketType_Map[] = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
        0x09, 0x0A, 0x0B, 0x0E, 0x0F, 0x17, 0x1C, 0x1D, 0x24, 0x28, 0x2A, 0x2B, 0x2E, 0x2F, 0x36,
        0x37, 0x3C, 0x3D };
static unsigned char Channels_Map[] = { 0x00, 0x01 };


static bool DoBTReset() {
    ALOGE("Enter DoBTReset()...\n");
    unsigned char ucEvent[512];
    unsigned int u4ResultLen = 0;
    unsigned char HCI_RESET[] = { 0x01, 0x03, 0x0c, 0x0 };

    if (false == EM_BT_write(HCI_RESET, sizeof(HCI_RESET))) {
        ALOGE("Leave DoBTReset() due to EM_BT_write() failure...\n");
        EM_BT_deinit();
        return false;
    }

    ALOGD("EM_BT_write");
    for (unsigned int i = 0; i < sizeof(HCI_RESET); i++) {
        ALOGD("%1$02x    ", HCI_RESET[i]);
    }

    if (false == EM_BT_read(ucEvent, sizeof(ucEvent), &u4ResultLen)) {
        ALOGE("Leave DoBTReset() due to EM_BT_read() failure...\n");
        EM_BT_deinit();
        return false;
    }

    ALOGD("EM_BT_read, length = %d", u4ResultLen);
    for (unsigned int i = 0; i < u4ResultLen; i++) {
        ALOGE("%1$02x    ", ucEvent[i]);
    }

    ALOGE("Leave DoBTReset()...\n");
    return true;
}

static bool process_init_param(JNIEnv *env, jobject thiz) {
    ALOGE("Enter process_init_param()...\n");
    jclass clazz = env->GetObjectClass(thiz);
    if (clazz == NULL) {
        ALOGE("Can't find com/mediatek/engineermode/BtTest");
        ALOGE("Leave process_init_param() due to Can't find com/mediatek/engineermode/BtTest...\n");
        jniThrowException(env, "java/lang/Exception", NULL);
        return false;
    }

    fields.patter = env->GetFieldID(clazz, "mPatter", "I");
    if (NULL == fields.patter) {
        ALOGE("Leave process_init_param() due to Can't find Bluetooth.iPatter...\n");
        jniThrowException(env, "java/lang/RuntimeException", "Can't find Bluetooth.mPatter");
        return false;
    }

    fields.channels = env->GetFieldID(clazz, "mChannels", "I");

    if (NULL == fields.channels) {
        ALOGE("Leave process_init_param() due to Can't find Bluetooth.iChannels...\n");
        jniThrowException(env, "java/lang/RuntimeException", "Can't find Bluetooth.mChannels");
        return false;
    }

    fields.pocketType = env->GetFieldID(clazz, "mPocketType", "I");
    if (NULL == fields.pocketType) {
        ALOGE("Leave process_init_param() due to Can't find Bluetooth.iPocketType...\n");
        jniThrowException(env, "java/lang/RuntimeException", "Can't find Bluetooth.mPocketType");
        return false;
    }

    fields.pocketTypeLen = env->GetFieldID(clazz, "mPocketTypeLen", "I");
    if (NULL == fields.pocketTypeLen) {
        ALOGE("Leave process_init_param() due to Can't find Bluetooth.iPocketTypeLen...\n");
        jniThrowException(env, "java/lang/RuntimeException", "Can't find Bluetooth.mPocketTypeLen");
        return false;
    }

    fields.freq = env->GetFieldID(clazz, "mFreq", "I");
    if (NULL == fields.freq) {
        ALOGE("Leave process_init_param() due to Can't find Bluetooth.iFreq...\n");
        jniThrowException(env, "java/lang/RuntimeException", "Can't find Bluetooth.mFreq");
        return false;
    }
    fields.power = env->GetFieldID(clazz, "mPower", "I");
    if (NULL == fields.power) {
        ALOGE("Leave process_init_param() due to Can't find Bluetooth.iPower...\n");
        jniThrowException(env, "java/lang/RuntimeException", "Can't find Bluetooth.mPower");
        return false;
    }
    ALOGE("Leave process_init_param()...\n");
    return true;
}
static int post_process_txtest(JNIEnv *, jobject) {
    ALOGE("Enter post_process_txtest()...\n");
    EM_BT_deinit();
    ALOGE("Leave post_process_txtest()...\n");
    return 0;
}

static int process_txtest(JNIEnv *env, jobject thiz) {
    ALOGE("Enter process_txtest()...\n");
    if (!process_init_param(env, thiz)) {
        ALOGE("Leave process_txtest() due to process_init_param() failure...\n");
        return -1;
    }

    int nPatter = (int) env->GetIntField(thiz, fields.patter);
    int nChannels = (int) env->GetIntField(thiz, fields.channels);
    int nPocketType = (int) env->GetIntField(thiz, fields.pocketType);
    int nPocketTypeLen = (int) env->GetIntField(thiz, fields.pocketTypeLen);
    int nFreq = (int) env->GetIntField(thiz, fields.freq);

    if (EM_BT_init() == false) {
        ALOGE("Leave process_txtest() due to EM_BT_init() failure...\n");
        return -1;
    }

    unsigned char ucEvent[512];
    unsigned int u4ResultLen = 0;

    unsigned char peer_write_buf[] = { 0x01, 0x0D, 0xfc, 0x17, 0x00, 0x00, Pattern_Map[nPatter],
            Channels_Map[nChannels], (unsigned char) nFreq, 0x00, 0x00, 0x01,
            PocketType_Map[nPocketType], ((unsigned char) (nPocketTypeLen & 0xff)),
            ((unsigned char) ((0xff00 & nPocketTypeLen) >> 8)), 0x02, 0x00, 0x01, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    if (false == EM_BT_write(peer_write_buf, sizeof(peer_write_buf))) {
        ALOGE("Leave process_txtest() due to EM_BT_write() failure...\n");
        EM_BT_deinit();
        return -1;
    }

    ALOGD("EM_BT_write:");
    for (unsigned int i = 0; i < sizeof(peer_write_buf); i++) {
        ALOGD("%1$02x    ", peer_write_buf[i]);
    }

    memset(ucEvent, 0, sizeof(ucEvent));
    if (false == EM_BT_read(ucEvent, sizeof(ucEvent), &u4ResultLen)) {
        ALOGE("Leave process_txtest() due to EM_BT_read() failure...\n");
        EM_BT_deinit();
        return -1;
    }

    ALOGE("EM_BT_read:length = %d", u4ResultLen);
    for (unsigned int i = 0; i < u4ResultLen; i++) {
        ALOGE("%1$02x    ", ucEvent[i]);
    }
    // this means there are more than one event data in this message, if u4ResultLen > 3 + ucEvent[2]
    // if there are 2 piece of event data in this message, we donot need to read event again
    if (!(u4ResultLen > 3 + ucEvent[2])) {

        memset(ucEvent, 0, sizeof(ucEvent));
        if (false == EM_BT_read(ucEvent, sizeof(ucEvent), &u4ResultLen)) {
            ALOGE("Leave process_txtest() due to EM_BT_read() failure...\n");
            EM_BT_deinit();
            return -1;
        }

        ALOGE("EM_BT_read:length = %d", u4ResultLen);
        for (unsigned int i = 0; i < u4ResultLen; i++) {
            ALOGE("%1$02x    ", ucEvent[i]);
        }
    }

    ALOGE("Leave process_txtest()...\n");

    return 0;
}

static int post_process_modetest(JNIEnv *, jobject) {
    ALOGE("Enter post_process_modetest()...\n");
    EM_BT_deinit();
    ALOGE("Leave post_process_modetest()...\n");
    return 0;
}


static int process_modetest(JNIEnv *env, jobject thiz) {
    ALOGE("Enter process_modetest()...\n");

    unsigned char ucEvent[512];
    unsigned int u4ResultLen = 0;
    if (!process_init_param(env, thiz)) {
        ALOGE("Leave process_modetest() due to process_init_param() failure...\n");
        return -1;
    }
    if (EM_BT_init() == false) {
        ALOGE("Leave process_modetest() due to EM_BT_init() failure...\n");
        // jniThrowException(env, "BT init failed", NULL);
        return -1;
    }

    // ==============set power==============
    int nPower = (int) env->GetIntField(thiz, fields.power);
    unsigned char power_write[] = { 0x01, 0x79, 0xfc, 0x06, 0x07, 0x80, 0x00, 0x06, 0x05, 0x07 };
    if (nPower <= 7 && nPower >= 0) {
        power_write[4] = (unsigned char) nPower;
        power_write[9] = (unsigned char) nPower;
    }
    ALOGE("Power val %1$02x:", power_write[4]);

    if (false == EM_BT_write(power_write, sizeof(power_write))) {
        ALOGE("Leave process_modetest() due to power_write EM_BT_write() failure... (3)\n");
        EM_BT_deinit();
        return -1;
    }
    if (false == EM_BT_read(ucEvent, sizeof(ucEvent), &u4ResultLen)) {
        ALOGE("Leave process_modetest() due to power_write EM_BT_read() failure. (3)\n");
        EM_BT_deinit();
        return -1;
    }

    ALOGE("Power EM_BT_read:");
    for (unsigned int i = 0; i < u4ResultLen; i++) {
        ALOGE("%1$02x    ", ucEvent[i]);
    }

    unsigned char peer_write_buf2[] = { 0x01, 0x03, 0x18, 0x00 };

    if (false == EM_BT_write(peer_write_buf2, sizeof(peer_write_buf2))) {
        ALOGE("Leave process_modetest() due to EM_BT_write() failure... (2)\n");
        EM_BT_deinit();
        return -1;
    }

    ALOGD("EM_BT_write:");
    for (unsigned int i = 0; i < sizeof(peer_write_buf2); i++) {
        ALOGD("%1$02x    ", peer_write_buf2[i]);
    }

    if (false == EM_BT_read(ucEvent, sizeof(ucEvent), &u4ResultLen)) {
        ALOGE("Leave process_modetest() due to EM_BT_read() failure... (2)\n");
        EM_BT_deinit();
        return -1;
    }

    ALOGE("EM_BT_read:length = %d", u4ResultLen);
    for (unsigned int i = 0; i < u4ResultLen; i++) {
        ALOGE("%1$02x    ", ucEvent[i]);
    }

    unsigned char peer_write_buf3[] = { 0x01, 0x1A, 0x0C, 0x01, 0x03 };

    if (false == EM_BT_write(peer_write_buf3, sizeof(peer_write_buf3))) {
        ALOGE("Leave process_modetest() due to EM_BT_write() failure... (3)\n");
        EM_BT_deinit();
        return -1;
    }

    ALOGD("EM_BT_write:");
    for (unsigned int i = 0; i < sizeof(peer_write_buf3); i++) {
        ALOGD("%1$02x    ", peer_write_buf3[i]);
    }

    if (false == EM_BT_read(ucEvent, sizeof(ucEvent), &u4ResultLen)) {
        ALOGE("Leave process_modetest() due to EM_BT_read() failure. (3)\n");
        EM_BT_deinit();
        return -1;
    }

    ALOGE("EM_BT_read:length = %d", u4ResultLen);
    for (unsigned int i = 0; i < u4ResultLen; i++) {
        ALOGE("%1$02x    ", ucEvent[i]);
    }
    unsigned char peer_write_buf4[] = { 0x01, 0x05, 0x0C, 0x03, 0x02, 0x00, 0x02 };

    if (false == EM_BT_write(peer_write_buf4, sizeof(peer_write_buf4))) {
        ALOGE("Leave process_modetest() due to EM_BT_write() failure...  (4) \n");
        EM_BT_deinit();
        return -1;
    }

    ALOGD("EM_BT_write:");
    for (unsigned int i = 0; i < sizeof(peer_write_buf4); i++) {
        ALOGD("%1$02x    ", peer_write_buf4[i]);
    }

    if (false == EM_BT_read(ucEvent, sizeof(ucEvent), &u4ResultLen)) {
        ALOGE("Leave process_modetest() due to EM_BT_read() fail...  (4)\n");
        EM_BT_deinit();
        return -1;
    }

    ALOGE("EM_BT_read:length = %d", u4ResultLen);
    for (unsigned int i = 0; i < u4ResultLen; i++) {
        ALOGE("%1$02x    ", ucEvent[i]);
    }


    ALOGE("Leave process_modetest()...\n");
    return 0;
}

/*
 -1: means error occurs
 0: BLE feature is not support
 -1:BLE feature is support
 */
static jint BtTest_isBLESupport(JNIEnv *, jobject) {
    unsigned char ucEvent[512] = { 0 };
    unsigned int u4ResultLen = 0;
    int result = -1;

    if (EM_BT_init() == false) {
        ALOGE("Leave isBLESupport() due to EM_BT_init() failure...\n");
        return -1;
    }
    unsigned char peer_write_buf4[] = { 0x01, 0x03, 0x10, 0x00 };
    ALOGD("Enter isBLESupport()...\n");
    if (false == EM_BT_write(peer_write_buf4, sizeof(peer_write_buf4))) {
        ALOGE("Leave isBLESupport() due to EM_BT_write() failure...  (4) \n");
        EM_BT_deinit();
        return -1;
    }

    ALOGD("EM_BT_write:");
    for (unsigned int i = 0; i < sizeof(peer_write_buf4); i++) {
        ALOGD("%1$02x    ", peer_write_buf4[i]);
    }

    if (false == EM_BT_read(ucEvent, sizeof(ucEvent), &u4ResultLen)) {
        ALOGE("Leave isBLESupport() due to EM_BT_read() fail...  (4)\n");
        EM_BT_deinit();
        return -1;
    }
    ALOGE("EM_BT_read: event length = %d", u4ResultLen);
    for (unsigned int i = 0; i < u4ResultLen; i++) {
        ALOGE("%1$02x    ", ucEvent[i]);
    }
    if (u4ResultLen > 11) {
        result = (ucEvent[11] & (1 << 6)) == 0 ? 0 : 1;
    } else {
        result = 0;
    }
    EM_BT_deinit();
    ALOGD("Leave isBLESupport()...\n");
    return result;
}

static jcharArray BtTest_HCIReadEvent(JNIEnv *env, jobject) {
    unsigned int i = 0;
    unsigned int u4ResultLen = 0;
    unsigned char ucResultArray[512] = { 0 };
    unsigned short usResultArray[512] = { 0 };
    jcharArray resultArray = NULL;
    ALOGD("Enter BtTest_HCIReadEvent()...\n");

    if (false == EM_BT_read(ucResultArray, sizeof(ucResultArray), &u4ResultLen)) {
        ALOGE("EM_BT_read call failed, Leave BtTest_HCIReadEvent()...\n");
        return NULL;
    }
    ALOGD("EM_BT_read:  event length = %d", u4ResultLen);
    for (unsigned i = 0; i < u4ResultLen; i++) {
        ALOGD("%1$02x", ucResultArray[i]);
        usResultArray[i] = ucResultArray[i];
    }
    resultArray = env->NewCharArray((int) u4ResultLen);
    jsize start = 0;
    jsize end = (int) u4ResultLen;
    if (NULL != resultArray) {
        env->SetCharArrayRegion(resultArray, start, end, (const jchar *) usResultArray);
    }
    ALOGD("Leave BtTest_HCIReadEvent()...\n");
    return resultArray;
}

static jcharArray BtTest_HCICommandRun(JNIEnv *env, jobject, jcharArray HCICmdBuffer,
        jint HCICmdLength) {
    // call EM_BT_write to send HCI command
    unsigned int u4ResultLen = 0;
    unsigned char ucResultArray[512] = { 0 };
    unsigned short usResultArray[512] = { 0 };
    jchar* jniParameter = NULL;
    jcharArray resultArray = NULL;
    unsigned char *ucHCICmdBuffer = new unsigned char[HCICmdLength];
    ALOGD("Enter BtTest_HCICommandRun()...\n");
    // parameter validation check
    if (HCICmdBuffer) {
        jniParameter = (jchar *) env->GetCharArrayElements(HCICmdBuffer, NULL);
        if (jniParameter == NULL) {
            ALOGE("Error retrieving source of EM paramters");
            delete[] ucHCICmdBuffer;
            return NULL;  // out of memory or no data to load
        }
    } else {
        ALOGE("NULL java array of readEEPRom16");
        delete[] ucHCICmdBuffer;
        return NULL;
    }
    ALOGD("EM_BT_write:");
    for (int i = 0; i < HCICmdLength; i++) {
        ucHCICmdBuffer[i] = (unsigned char) *(jniParameter + i);
        ALOGD("%1$02x", ucHCICmdBuffer[i]);
    }
    if (!EM_BT_write(ucHCICmdBuffer, HCICmdLength)) {
        ALOGE("EM_BT_write call failed, Leave BtTest_HCICommandRun()...\n");
        delete[] ucHCICmdBuffer;
        return NULL;
    }

    if (false == EM_BT_read(ucResultArray, sizeof(ucResultArray), &u4ResultLen)) {
        ALOGE("EM_BT_read call failed, Leave BtTest_HCICommandRun()...\n");
        delete[] ucHCICmdBuffer;
        return NULL;
    }
    ALOGD("EM_BT_read:  event length = %d", u4ResultLen);
    for (unsigned int i = 0; i < u4ResultLen; i++) {
        ALOGD("%1$02x", ucResultArray[i]);
        usResultArray[i] = ucResultArray[i];
    }
    resultArray = env->NewCharArray((int) u4ResultLen);
    jsize start = 0;
    jsize end = (int) u4ResultLen;
    if (NULL != resultArray) {
        env->SetCharArrayRegion(resultArray, start, end, (const jchar *) usResultArray);
        ALOGE("Leave BtTest_HCICommandRun()...\n");
    } else {
        ALOGE("Leave BtTest_HCICommandRun()...\n");
    }
    env->ReleaseCharArrayElements(HCICmdBuffer, jniParameter, JNI_ABORT);

    delete[] ucHCICmdBuffer;
    return resultArray;
}

static jint BtTest_isComboSupport(JNIEnv *, jobject) {

#ifdef MTK_COMBO_SUPPORT
    return 1;
#else
    return 0;
#endif
}

static int BtTest_Init(JNIEnv *, jobject) {
    ALOGE("Enter BtTest_Init\n");
    return EM_BT_init() ? 0 : -1;
}

static int BtTest_UnInit(JNIEnv *, jobject) {
    ALOGE("Enter BtTest_UnInit\n");
    EM_BT_deinit();
    return 0;
}

static int BtTest_doBtTest(JNIEnv *env, jobject thiz, jint kind) {
    ALOGE("Enter BtTest_doBtTest(kind=%x)... \n", kind);

    if (0 == kind) {
        return process_txtest(env, thiz);
    } else if (1 == kind) {
        return process_modetest(env, thiz);
    } else if (2 == kind) {
        return post_process_modetest(env, thiz);
    } else if (3 == kind) {
        return post_process_txtest(env, thiz);
    }

    ALOGE("Leave BtTest_doBtTest()...\n");
    return 0;
}

static unsigned char NoSigRxBBMap[] = { 0x01, 0x02, 0x03, 0x04, 0x09 };
static unsigned char NoSigRxRRMap[] = { 0x03, 0x04, 0x0A, 0x0B, 0x0E, 0x0F, 0x24, 0x28, 0x2A, 0x2B,
        0x2E, 0x2F };
static jintArray BtTest_EndNoSigRxTest(JNIEnv *env, jobject) {
    ALOGE("Enter BtTest_EndNoSigRxTest.");

    unsigned char ucEvent[512];
    unsigned int u4ResultLen = 0;
//=============================GET result===
    unsigned char peer_write_buf2[] = { 0x01, 0x0D, 0xFC, 0x17, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00,
            0x00, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00 };

    if (false == EM_BT_write(peer_write_buf2, sizeof(peer_write_buf2))) {
        ALOGE("Leave BtTest_NoSigRxTest() due to EM_BT_write(2) failure...\n");
        EM_BT_deinit();
        return 0;
    }

    ALOGD("EM_BT_write:");
    for (unsigned int i = 0; i < sizeof(peer_write_buf2); i++) {
        ALOGD("%1$02x    ", peer_write_buf2[i]);
    }

    memset(ucEvent, 0, sizeof(ucEvent));
    if (false == EM_BT_read(ucEvent, sizeof(ucEvent), &u4ResultLen)) {
        ALOGE("Leave BtTest_NoSigRxTest() due to EM_BT_read() failure...\n");
        EM_BT_deinit();
        return 0;
    }

    ALOGE("EM_BT_read:");
    for (unsigned int i = 0; i < u4ResultLen; i++) {
        ALOGE("%1$02x    ", ucEvent[i]);
    }

    int iRes[4];
    iRes[0] = *(int*) (&ucEvent[7]);
    iRes[1] = *(int*) (&ucEvent[11]) / 10000;  // 1.24 -> 124
    iRes[2] = *(int*) (&ucEvent[15]);
    iRes[3] = *(int*) (&ucEvent[19]) / 10000;
    jintArray result = env->NewIntArray(4);
    env->SetIntArrayRegion(result, 0, 4, iRes);

    EM_BT_deinit();
    ALOGE("Leave BtTest_EndNoSigRxTest.");
    return result;
}
static jboolean BtTest_StartNoSigRxTest(JNIEnv *, jobject, jint nPatternIdx, jint nPocketTypeIdx,
        jint nFreq, jint nAddress) {
    ALOGE("Enter BtTest_NoSigRxTest.");

    unsigned char ucEvent[512];
    unsigned int u4ResultLen = 0;

    if (EM_BT_init() == false) {
        ALOGE("Leave BtTest_NoSigRxTest() due to EM_BT_init() failure\n");
        return 0;
    }

    unsigned char peer_write_buf[] = { 0x01, 0x0D, 0xFC, 0x17, 0x00, NoSigRxBBMap[nPatternIdx],
            0x0B, 0x00, 0x00, (char) nFreq, 0x00, 0x01, NoSigRxRRMap[nPocketTypeIdx], 0x00, 0x00,
            0x02, 0x00, 0x01, 0x00, 0x00, 0x00, ((unsigned char) ((0xff000000 & nAddress) >> 24)),
            ((unsigned char) ((0xff0000 & nAddress) >> 16)), ((unsigned char) ((0xff00 & nAddress)
                    >> 8)), ((unsigned char) (nAddress & 0xff)), 0x00, 0x00 };

    if (false == EM_BT_write(peer_write_buf, sizeof(peer_write_buf))) {
        ALOGE("Leave BtTest_NoSigRxTest() due to EM_BT_write() failure...\n");
        EM_BT_deinit();
        return 0;
    }

    ALOGD("EM_BT_write:");
    for (unsigned int i = 0; i < sizeof(peer_write_buf); i++) {
        ALOGD("%1$02x    ", peer_write_buf[i]);
    }

    memset(ucEvent, 0, sizeof(ucEvent));
    if (false == EM_BT_read(ucEvent, sizeof(ucEvent), &u4ResultLen)) {
        ALOGE("Leave BtTest_NoSigRxTest() due to EM_BT_read() failure...\n");
        EM_BT_deinit();
        return 0;
    }

    ALOGE("EM_BT_read:");
    for (unsigned int i = 0; i < u4ResultLen; i++) {
        ALOGE("%1$02x    ", ucEvent[i]);
    }

    // receive it again
    if (u4ResultLen == 14) {
        memset(ucEvent, 0, sizeof(ucEvent));
        if (false == EM_BT_read(ucEvent, sizeof(ucEvent), &u4ResultLen)) {
            ALOGE("Leave BtTest_NoSigRxTest() due to EM_BT_read() failure...\n");
            EM_BT_deinit();
            return 0;
        }

        ALOGE("EM_BT_read:");
        for (unsigned int i = 0; i < u4ResultLen; i++) {
            ALOGE("%1$02x    ", ucEvent[i]);
        }
    }

    return 1;
}

// chip info
static BT_CHIP_ID chip_id;
static BT_HW_ECO eco_num;
static jlong back_len;
static bool getInfoFlag = false;
//

static jcharArray BtTest_GetPatchId(JNIEnv *env, jobject) {
    if (!getInfoFlag) {
        EM_BT_getChipInfo(&chip_id, &eco_num);
    }
    char patch_id[30] = { 'a' };
    unsigned short usResultArray[30] = { 0 };
    unsigned int patch_len = 30;
    EM_BT_getPatchInfo(patch_id, &patch_len);

    jsize start = 0;
    jsize end = strlen(patch_id);
    jcharArray resultArray = env->NewCharArray(end);

    ALOGI("strlen =%d \n    ", end);
    for (int i = 0; i < end; i++) {
        usResultArray[i] = patch_id[i];
        ALOGI("i=%d --%c    \n", i, usResultArray[i]);
    }
    if (NULL != resultArray) {
        env->SetCharArrayRegion(resultArray, start, end, (const jchar *) usResultArray);
        ALOGD("Leave BtTest_GetPatchId()...\n");
    }
    ALOGI("return resultArray\n");
    return resultArray;
}

static int BtTest_relayerStart(JNIEnv *, jobject, jint portNumber, jint serialSpeed) {
    ALOGI("enter RELAYER_start serialSpeed =%d portNumber =%d\n", serialSpeed, portNumber);
    return RELAYER_start(portNumber, serialSpeed) ? 0 : -1;
}

static int BtTest_relayerExit(JNIEnv *, jobject) {
    ALOGI("enter RELAYER_exit\n");
    RELAYER_exit();
    return 0;
}

static int BtTest_pollingStop(JNIEnv *, jobject) {
    ALOGI("enter pollingStop\n");
    EM_BT_polling_stop();
    return 1;
}

static int BtTest_pollingStart(JNIEnv *, jobject) {
    ALOGI("enter pollingStart\n");
    EM_BT_polling_start();
    return 1;
}

static JNINativeMethod mehods[] = {
    { "isBLESupport", "()I", (void *) BtTest_isBLESupport },
    { "init", "()I", (void *) BtTest_Init },
    { "doBtTest", "(I)I", (void *) BtTest_doBtTest },
    { "unInit", "()I", (void *) BtTest_UnInit },
    { "hciCommandRun", "([CI)[C", (void *) BtTest_HCICommandRun },
    { "hciReadEvent", "()[C", (void *) BtTest_HCIReadEvent },
    { "noSigRxTestStart", "(IIII)Z", (void *) BtTest_StartNoSigRxTest },
    { "noSigRxTestResult", "()[I", (void *) BtTest_EndNoSigRxTest },
    {"getPatchId", "()[C", (void *) BtTest_GetPatchId },
    {"relayerStart", "(II)I", (void *) BtTest_relayerStart },
    {"relayerExit", "()I", (void *) BtTest_relayerExit },
    {"isComboSupport", "()I", (void *)BtTest_isComboSupport },
    {"pollingStart", "()I", (void*)BtTest_pollingStart },
    {"pollingStop", "()I", (void*)BtTest_pollingStop }
};

// This function only registers the native methods
static int register_com_mediatek_bluetooth_bttest(JNIEnv *env) {
    ALOGE("Register: register_com_mediatek_bluetooth_bttest()...\n");
    return AndroidRuntime::registerNativeMethods(env, "com/mediatek/engineermode/bluetooth/BtTest",
            mehods, NELEM(mehods));
}

jint JNI_OnLoad(JavaVM* vm, void*) {
    JNIEnv* env = NULL;
    jint result = -1;

    ALOGD("Enter JNI_OnLoad()...\n");
    if (vm->GetEnv((void**) &env, JNI_VERSION_1_4) != JNI_OK) {
        ALOGE("ERROR: GetEnv failed\n");
        goto bail;
    }
    assert(env != NULL);

    if (register_com_mediatek_bluetooth_bttest(env) < 0) {
        ALOGE("ERROR: Bluetooth native registration failed\n");
        goto bail;
    }

    /* success -- return valid version number */
    result = JNI_VERSION_1_4;

    ALOGD("Leave JNI_OnLoad()...\n");
    bail: return result;
}
