#ifndef HWC_BLITER_H_
#define HWC_BLITER_H_

#include "DpBlitStream.h"

#include <utils/KeyedVector.h>

#include <ui/Rect.h>
#include "composer.h"

using namespace android;

struct HWLayer;
class DisplayBufferQueue;

// ---------------------------------------------------------------------------

class BliterHandler : public LayerHandler
{
public:
    BliterHandler(int dpy, sp<OverlayEngine> ovl_engine);
    virtual ~BliterHandler();

    // set() in BliterHandler is used to create release fence
    // in order to notify upper producers that layers are already consumed
    virtual void set(struct hwc_display_contents_1* list, DispatcherJob* job);

    // process() in BliterHandler is used to utilize DpFramework
    // to rescale and rotate input layers and pass the results to display driver
    virtual void process(DispatcherJob* job);

    // dump() in BliterHandler is used to dump debug data
    virtual int dump(char* buff, int buff_len, int dump_level);

private:
    // releaseFence() is used to release source fence
    void releaseFence(hwc_layer_1* layer, int ovl_in);

    // bypassBlit() is used to check if source buffer is dirty, or
    // there has any updated source buffer could be used
    bool bypassBlit(HWLayer* hw_layer, int ovl_in);

    struct BufferConfig
    {
        // identify if this config is valid
        bool is_valid;

        // buffer information in native handle
        int gralloc_width;
        int gralloc_height;
        int gralloc_stride;
        int gralloc_cbcr_align;
        int gralloc_vertical_stride;
        int gralloc_format;
        int gralloc_private_format;

        // src buffer setting for DpFramework
        DpColorFormat src_dpformat;
        unsigned int  src_pitch;
        unsigned int  src_pitch_uv;
        unsigned int  src_height;
        unsigned int  src_plane;
        unsigned int  src_size[3];
        unsigned int  src_offset[2];

        // dst buffer setting for DpFramework
        DpColorFormat dst_dpformat;
        unsigned int  dst_width;
        unsigned int  dst_height;
        unsigned int  dst_pitch;
        unsigned int  dst_pitch_uv;
        unsigned int  dst_size;
        unsigned int  dst_ovl_id;

        // sequence is used as a sequence number for profiling latency purpose
        unsigned int  sequence;
    };

    // setDpConfig() is used to prepare configuration for DpFramwork
    status_t setDpConfig(HWLayer* hw_layer, BufferConfig* config, int ovl_in);

    // computeBufferCrop is used to calculate src and dst crop region
    status_t computeBufferCrop(HWLayer* hw_layer, BufferConfig* config,
        Rect& src_crop, Rect& dst_crop, int ovl_in);

    // getDisplayBufferQueue is used to get workable display buffer queue
    sp<DisplayBufferQueue> getDisplayBufferQueue(HWLayer* hw_layer,
        BufferConfig* config, int ovl_in) const;

    // invalidate() is used to ask DpFramework to do bliting
    void invalidate(
        HWLayer* hw_layer, BufferConfig* config, sp<DisplayBufferQueue> queue,
        Rect& src_crop, Rect& dst_crop, int ovl_in);

    // m_dp_configs stores src/dst buffer and overlay input information
    BufferConfig* m_dp_configs;

    // m_blit_stream is a bit blit stream
    DpBlitStream m_blit_stream;
};

#endif // HWC_BLITER_H_
