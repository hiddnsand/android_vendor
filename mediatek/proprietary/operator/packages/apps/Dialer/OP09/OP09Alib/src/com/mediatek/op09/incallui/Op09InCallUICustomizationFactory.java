package com.mediatek.op09.incallui;
import android.content.Context;
import com.mediatek.incallui.ext.ICallCardExt;
import com.mediatek.incallui.ext.IInCallExt;
import com.mediatek.incallui.ext.IVilteAutoTestHelperExt;
import com.mediatek.incallui.ext.IVideoCallExt;

import com.mediatek.incallui.ext.OpInCallUICustomizationFactoryBase;

public class Op09InCallUICustomizationFactory extends OpInCallUICustomizationFactoryBase {
    private Context mContext;
    public Op09InCallUICustomizationFactory(Context context) {
        mContext = context;
    }
    public IVilteAutoTestHelperExt getVilteAutoTestHelperExt() {
        return new Op09VilteAutoTestHelperExt();
    }

    public IVideoCallExt getVideoCallExt() {
        return new Op09VideoCallExt(mContext);
    }

    public IInCallExt getInCallExt() {
        return new Op09InCallExt(mContext);
    }

    public ICallCardExt getCallCardExt() {
        return new Op09CallCardExt(mContext);
    }

}
