LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/external/volte_rcs))
include $(CLEAR_VARS)
LOCAL_MODULE = volte_rcs_ua
LOCAL_MODULE_CLASS = EXECUTABLES
LOCAL_MODULE_OWNER = mtk
LOCAL_PROPRIETARY_MODULE = true
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libbase libc libcutils libdl libfmq liblog libutils libhardware libhidlbase libhidltransport libhwbinder vendor.mediatek.hardware.rcs@1.0_vendor libhardware_legacy
LOCAL_INIT_RC = volte_rcs_ua.rc
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/volte_rcs_ua
include $(BUILD_PREBUILT)
endif
