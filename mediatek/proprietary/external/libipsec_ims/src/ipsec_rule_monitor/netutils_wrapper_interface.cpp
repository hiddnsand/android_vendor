/*
* Copyright (C) 2014 MediaTek Inc.
* Modification based on code covered by the mentioned copyright
* and/or permission notice(s).
*/
/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <net/if.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <stdarg.h>
#include "utils.h"
#define LOG_TAG "ipsec_policy_mont"
#include <cutils/log.h>
#include <logwrap/logwrap.h>
#include <string>
#include <list>

const char * const IPTABLES_PATH = "/system/bin/iptables-wrapper-1.0";
const char * const IP6TABLES_PATH = "/system/bin/ip6tables-wrapper-1.0";
const char * const IP_PATH = "/system/bin/ip-wrapper-1.0";
const char * const NDC_PATH = "/system/bin/ndc-wrapper-1.0";


static void logExecError(const char* argv[], int res, int status) {
    const char** argp = argv;
    std::string args = "";
    while (*argp) {
        args += *argp;
        args += ' ';
        argp++;
    }
    ALOGE("exec() res=%d, status=%d for %s", res, status, args.c_str());
}

static int execCommand(int argc, const char *argv[], bool silent) {
    int res;
    int status;

    res = android_fork_execvp(argc, (char **)argv, &status, false, !silent);
    if (res || !WIFEXITED(status) || WEXITSTATUS(status)) {
        if (!silent) {
            logExecError(argv, res, status);
        }
        if (res)
            return res;
        if (!WIFEXITED(status))
            return ECHILD;
    }
    return WEXITSTATUS(status);
}

static int execIptables(IptablesTarget target, bool silent, va_list args) {
    /* Read arguments from incoming va_list; we expect the list to be NULL terminated. */
    std::list<const char*> argsList;
    argsList.push_back(NULL);
    const char* arg;

    // Wait to avoid failure due to another process holding the lock
    argsList.push_back("-w");

    do {
        arg = va_arg(args, const char *);
        argsList.push_back(arg);
    } while (arg);

    int i = 0;
    const char* argv[argsList.size()];
    std::list<const char*>::iterator it;
    for (it = argsList.begin(); it != argsList.end(); it++, i++) {
        argv[i] = *it;
    }

    const char** temp = argv + 1; //skip argv[0]
    std::string debug = "";
    while (*temp) {
        debug += *temp;
        debug += " ";
        temp++;
    }
    ALOGI("execIptables %s\n", debug.c_str());

    int res = 0;
    if (target == V4 || target == V4V6) {
        argv[0] = IPTABLES_PATH;
        res |= execCommand(argsList.size(), argv, silent);
    }
    if (target == V6 || target == V4V6) {
        argv[0] = IP6TABLES_PATH;
        res |= execCommand(argsList.size(), argv, silent);
    }
    return res;
}

int execIptables(IptablesTarget target, ...) {
    va_list args;
    va_start(args, target);
    int res = execIptables(target, false, args);
    va_end(args);
    return res;
}

int execIptablesSilently(IptablesTarget target, ...) {
    va_list args;
    va_start(args, target);
    int res = execIptables(target, true, args);
    va_end(args);
    return res;
}

static int execNdcCmd(const char *command, bool silent, va_list args) {
    /* Read arguments from incoming va_list; we expect the list to be NULL terminated. */
    std::list<const char*> argsList;
    argsList.push_back(NULL);
    const char* arg;
    argsList.push_back(command);
    do {
        arg = va_arg(args, const char *);
        argsList.push_back(arg);
    } while (arg);

    int i = 0;
    const char* argv[argsList.size()];
    std::list<const char*>::iterator it;
    for (it = argsList.begin(); it != argsList.end(); it++, i++) {
        argv[i] = *it;
    }

    const char** temp = argv + 1; //skip argv[0]
    std::string debug = "";
    while (*temp) {
        debug += *temp;
        debug += " ";
        temp++;
    }
    ALOGI("execNdcCmd %s\n", debug.c_str());

    int res = 0;
    argv[0] = NDC_PATH;
    res = execCommand(argsList.size(), argv, silent);
    return res;
  
}

int execNdcCmd(const char *command, ...) {
    va_list args;
    va_start(args, command);
    int res = execNdcCmd(command, false, args);
    va_end(args);
    return res;
}

static int execIpCmd(int family, bool silent, va_list args) {
    /* Read arguments from incoming va_list; we expect the list to be NULL terminated. */
    std::list<const char*> argsList;
    argsList.push_back(NULL);
    argsList.push_back(NULL);
    const char* arg;

    do {
        arg = va_arg(args, const char *);
        argsList.push_back(arg);
    } while (arg);

    int i = 0;
    const char* argv[argsList.size()];
    std::list<const char*>::iterator it;
    for (it = argsList.begin(); it != argsList.end(); it++, i++) {
        argv[i] = *it;
    }


    const char** temp = argv + 2; //skip argv[0] and argv[1]
    std::string debug = "";
    while (*temp) {
        debug += *temp;
        debug += " ";
        temp++;
    }
    ALOGI("execIpCmd %s\n", debug.c_str());

    int res = 0;
    argv[0] = IP_PATH;
    if (family == AF_INET) {
        argv[1] = "-4";
        res |= execCommand(argsList.size(), argv, silent);
    }
    if (family == AF_INET6) {
        argv[1] = "-6";
        res |= execCommand(argsList.size(), argv, silent);
    }
    return res;
  
}

int execIpCmd(int family, ...) {
    va_list args;
    va_start(args, family);
    int res = execIpCmd(family, false, args);
    va_end(args);
    return res;
}

