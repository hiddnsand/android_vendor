package com.mediatek.op06.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.ims.ImsManager;
import com.android.phone.CallForwardEditPreference;
import com.android.phone.EditPhoneNumberPreference;

import com.mediatek.ims.config.ImsConfigContract;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;
import com.mediatek.phone.ext.DefaultCallFeaturesSettingExt;


public class Op06CallFeaturesSettingExt extends DefaultCallFeaturesSettingExt {
    private static final String TAG = "Op06CallFeaturesSettingExt";
    private static final String AOSP_WFC_PREFERENCE = "button_wifi_calling_settings_key";
    private Context mContext;

    private PreferenceActivity mCallSettingActivity = null;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("@M_" + TAG, "onReceive:" + action);
            if (!ImsManager.isWfcEnabledByPlatform(context)) {
                Log.d(TAG, "WFC is not enabled in platform");
            }
            if (action.equals(ImsConfigContract.ACTION_CONFIG_LOADED)) {
                if (mCallSettingActivity != null) {
                    int phoneId = intent.getIntExtra(ImsConfigContract.EXTRA_PHONE_ID, 0);
                    if (WifiCallingUtils.isWifiCallingProvisioned(context, phoneId)) {
                        Log.d(TAG, "Config loaded. WFC is provisioned.");
                        if (mCallSettingActivity != null) {
                            initOtherCallFeaturesSetting(mCallSettingActivity);
                        }
                    } else {
                        Log.d(TAG, "Config loaded. WFC is not provisioned.");
                        if (mCallSettingActivity != null) {
                            OP06WfcSettings wfcSettings = OP06WfcSettings.getInstance(mContext);
                            wfcSettings.removeWfcPreference(mCallSettingActivity
                                .getPreferenceScreen());
                        }
                    }
                }
            } else if (CarrierConfigManager.ACTION_CARRIER_CONFIG_CHANGED.equals(action)) {
                if (mCallSettingActivity != null) {
                    initOtherCallFeaturesSetting(mCallSettingActivity);
                }
            }
        }
    };

    public Op06CallFeaturesSettingExt(Context context) {
        Log.d(TAG, "OP06 Constructor call");
        mContext = context;
    }

   /**
    * Get whether the IMS is IN_SERVICE.
    * @param subId the sub which one user selected.
    * @return true if the ImsPhone is IN_SERVICE, else false.
    */
    private static boolean isImsServiceAvailable(Context context, int subId) {
        boolean isImsReg = false;
        boolean isImsEnabled = ImsManager.isVolteEnabledByPlatform(context);
        Log.d(TAG, "[isImsServiceAvailable] isImsEnabled : " + isImsEnabled);
        if (isImsEnabled) {
            isImsReg = ((TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE)).isVolteAvailable();
            Log.d(TAG, "[isImsServiceAvailable] isImsReg = " + isImsReg);
        }
        return isImsReg;
    }

    @Override
   /**
     * Get whether the IMS is IN_SERVICE.
     * @param subId the sub which one user selected.
     * @return true if the ImsPhone is IN_SERVICE, else false.
     */
   public boolean needShowOpenMobileDataDialog(Context context, int subId) {
        Log.d(TAG, "ImsService Not Available, plugin returns true else false");
        return !isImsServiceAvailable(context, subId);
    }

    @Override
    /**
      * Keep the preference enabled after the preference is SET
      * @param subId the sub which one user selected.
      */
    public void onError(Preference preference) {
        preference.setEnabled(true);
        if (preference instanceof CallForwardEditPreference) {
            ((EditPhoneNumberPreference) preference).setToggled(false);
        }
        Log.d(TAG, "onError, set preference true even after error");
    }

    @Override
    public void initOtherCallFeaturesSetting(PreferenceActivity activity) {
        mCallSettingActivity = activity;
        if (TextUtils.equals(activity.getClass().getSimpleName(), "CallFeaturesSetting")) {
                OP06WfcSettings wfcSettings = OP06WfcSettings.getInstance(mContext);
            if (!WifiCallingUtils.isImsProvSupported(mContext,
                    SubscriptionManager.getDefaultVoiceSubscriptionId())) {
                Log.d(TAG, "Ims prov feature not supported, defSubId:" + SubscriptionManager
                        .getDefaultVoiceSubscriptionId());
                return;
            }
            if (ImsManager.isWfcEnabledByPlatform(activity)
                    && WifiCallingUtils.isWifiCallingProvisioned(activity,
                    RadioCapabilitySwitchUtil.getMainCapabilityPhoneId())) {
                wfcSettings.customizedWfcPreference(activity, activity.getPreferenceScreen());
            } else {
                wfcSettings.removeWfcPreference(mCallSettingActivity.getPreferenceScreen());
            }
        }
    }

    @Override
    /** Called from onPause
     * @param activity
     * @return
     */
    public void onCallFeatureSettingsEvent(int event) {
        Log.d(TAG, "event:" + event);
        switch (event) {
            case DefaultCallFeaturesSettingExt.RESUME:
                Log.d(TAG, "mCallSettingActivity: " + mCallSettingActivity);
                if (mCallSettingActivity != null) {
                    Preference wifiCallingSettings
                            = mCallSettingActivity.findPreference(AOSP_WFC_PREFERENCE);
                    Log.d(TAG, "wifiCallingSettings: " + wifiCallingSettings);
                    if (wifiCallingSettings != null) {
                        wifiCallingSettings.setSummary(WifiCallingUtils
                                .getWfcModeSummary(ImsManager.getWfcMode(mContext)));
                    }
                }
                WifiCallingUtils.registerReceiver(mContext, mReceiver);
                break;
            case DefaultCallFeaturesSettingExt.PAUSE:
                WifiCallingUtils.unRegisterReceiver(mContext, mReceiver);
                break;
            default:
                break;
        }
    }
}
