/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#ifndef _MTK_PLATFORM_HARDWARE_MTKCAM_V3_HWPIPELINE_PIPELINEDEFAULTIMP_H_
#define _MTK_PLATFORM_HARDWARE_MTKCAM_V3_HWPIPELINE_PIPELINEDEFAULTIMP_H_
//
#include "MyUtils.h"
//
#include "PipelineModel_Default.h"
#include <mtkcam/pipeline/utils/streaminfo/MetaStreamInfo.h>
#include <mtkcam/pipeline/utils/streaminfo/ImageStreamInfo.h>
#include <mtkcam/pipeline/utils/streambuf/StreamBuffers.h>
#include <mtkcam/pipeline/pipeline/PipelineContext.h>
//
#include <mtkcam/pipeline/extension/IVendorManager.h>
//
#include <mtkcam/utils/hw/HwInfoHelper.h>
#include <mtkcam/feature/eis/EisInfo.h>
#include <mtkcam/utils/hw/CamManager.h>
#if MTKCAM_HAVE_ADV_SETTING
#include <mtkcam/feature/advCamSetting/AdvCamSettingMgr.h>
#endif
#define  SUPPORT_IVENDOR                    (1)

#include <mtkcam/pipeline/hwnode/P2Node.h>

using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::Utils;
using namespace NSCam::v3::NSPipelineContext;


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {

class PipelineDefaultImp
    : public PipelineModel_Default
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Definitions.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////
    typedef Utils::HalMetaStreamBuffer::Allocator
                                    HalMetaStreamBufferAllocatorT;

    struct MyConfigParams
    {
        PipeConfigParams            configParams;
        //
        // internal setting
        MBOOL                       skipJpeg;
    };
    struct MyRecordParams
    {
        MBOOL                       hasVRConsumer           = false;
        MBOOL                       has4KVR                 = false;
        MSize                       videoSize               = MSize(-1,-1);
    };
    struct ReprocParams
    {
        MBOOL                       mbOpaqueReproc          = false;
        MBOOL                       mbYuvReproc             = false;
    };
    struct CHSvrParams
    {
        // constrained high speed video related
        MINT32                      mDefaultBusrstNum       = 1;
        MINT32                      mAeTargetFpsMin         = -1;
        MINT32                      mAeTargetFpsMax         = -1;
        MINT32                      mAeTargetFpsMin_Req     = -1;
        MBOOL                       mResetAeTargetFps_Req   = false;
    };
    struct PipelineConfigParams
    {
        // constrained high speed video related
        MUINT32                     mOperation_mode         = 0;
        // Stream infos
        MSize                       mVideoSize              = MSize(-1,-1);
        MBOOL                       mbHasRecording          = false;
        MBOOL                       mb4KRecording           = false;
        MBOOL                       mbHasRaw                = false;
        MBOOL                       mbHasJpeg               = false;
        MSize                       mMaxStreamSize          = MSize(-1,-1);
        MBOOL                       mbHasLcso               = false;
        // node related
        MBOOL                       mbUseP1Node             = false;
        MBOOL                       mbUseP2Node             = false;
        MBOOL                       mbUseP2TSNode           = false;
        MBOOL                       mbUseFDNode             = false;
        MBOOL                       mbUseJpegNode           = false;
        MBOOL                       mbUseRaw16Node          = false;
    };
    struct PipelineStreamSet
    {
        // meta: hal
        android::sp<IMetaStreamInfo>    mpHalMeta_Control;
        android::sp<IMetaStreamInfo>    mpHalMeta_DynamicP1;
        android::sp<IMetaStreamInfo>    mpHalMeta_DynamicP2;
        // meta: app
        android::sp<IMetaStreamInfo>    mpAppMeta_DynamicP1;
        android::sp<IMetaStreamInfo>    mpAppMeta_DynamicP2;
        android::sp<IMetaStreamInfo>    mpAppMeta_DynamicFD;
        android::sp<IMetaStreamInfo>    mpAppMeta_DynamicJpeg;
        android::sp<IMetaStreamInfo>    mpAppMeta_Control;
        // image: hal
        android::sp<IImageStreamInfo>   mpHalImage_P1_Raw;
        android::sp<IImageStreamInfo>   mpHalImage_P1_ResizerRaw;
        android::sp<IImageStreamInfo>   mpHalImage_P1_Lcso;
        android::sp<IImageStreamInfo>   mpHalImage_P1_Rsso;
        android::sp<IImageStreamInfo>   mpHalImage_FD_YUV;
        android::sp<IImageStreamInfo>   mpHalImage_Jpeg_YUV;
        android::sp<IImageStreamInfo>   mpHalImage_Thumbnail_YUV;

        // image: app
        android::sp<IImageStreamInfo>   mpAppImage_Yuv_In;
        android::sp<IImageStreamInfo>   mpAppImage_Opaque_In;
        android::sp<IImageStreamInfo>   mpAppImage_Opaque_Out;
        android::KeyedVector <
                StreamId_T, android::sp<IImageStreamInfo>
                        >               mvAppYuvImage;
        android::sp<IImageStreamInfo>   mpAppImage_Jpeg;
        android::sp<IImageStreamInfo>   mpAppImage_RAW16;

        //// raw/yuv stream mapping
        StreamSet                       mvYuvStreams_Fullraw;
        StreamSet                       mvYuvStreams_Resizedraw;

        android::KeyedVector < StreamId_T, MINT64 >
                                        mvStreamDurations;
    };
    struct HwParams
    {
        // sensor
        MUINT                       mSensorMode;
        MSize                       mSensorSize             = MSize(-1,-1);;
        MUINT                       mSensorFps;
        // pass1
        MUINT32                     mPixelMode;
        MINT                        mFullrawFormat;
        MSize                       mFullrawSize;
        size_t                      mFullrawStride;
        MINT                        mResizedrawFormat;
        MSize                       mResizedrawSize;
        size_t                      mResizedrawStride;
    };
    struct StreamingPipeParams
    {
        MUINT32                     mVhdrMode;
        MBOOL                       mbHasRsso;
        MBOOL                       mbIsEIS;
        NSCam::EIS::EisInfo         mEisInfo;
#if MTKCAM_HAVE_ADV_SETTING
        sp<AdvCamSetting>           mpAdvSetting            = NULL;
        MINT32                      mForceEnableIMGO;
#endif
    };
    //
    class MyProcessedParams
    {
    public:
        MINT32 const                mOpenId;
        /*  input params  */
        PipeConfigParams            mConfigParams;
        // internal setting
        MBOOL                       mSkipJpeg;
        //
        /*  processed params  */
        // opaque/yuv reprocessing
        ReprocParams                mReprocParams;
        //
        // constrained high speed video related
        CHSvrParams                 mCHSvrParams;
        //
        // Stream / Node related
        PipelineConfigParams        mPipelineConfigParams;
        //
        // streaminfo set
        PipelineStreamSet           mStreamSet;
        //
        // sensor / pass1 hw related
        HwParams                    mHwParams;
        //
        // streaming pipe related
        StreamingPipeParams         mStreamingPipeParams;

        //
        NSCamHW::HwInfoHelper       mHwInfoHelper;

    public:
                                    MyProcessedParams(MINT32 const openId/*, NSCamHW::HwInfoHelper& helper*/);
        virtual                     ~MyProcessedParams() {}
        MERROR                      update(
                                        PipeConfigParams const& rParams,
                                        MBOOL skipJpeg
                                        )
                                    {
                                        // update config params
                                        mConfigParams = rParams;
                                        mSkipJpeg     = skipJpeg; //TODO: use this?
                                        //
                                        MERROR err;
                                        if( OK != (err = querySensorStatics())  ||
                                            OK != (err = preprocess())          ||
                                            OK != (err = decideSensor())        ||
                                            OK != (err = decideP1())
                                          )
                                            return err;
                                        return OK;
                                    }
        //
        static MyRecordParams       genRecordParam( PipeConfigParams const& configParams );
        //
        MBOOL                       isSupportAdvP2() const;
        //
    protected:
        virtual MERROR              querySensorStatics();
        virtual MERROR              preprocess();
        virtual MERROR              decideSensor();
        virtual MERROR              decideP1();
        virtual MBOOL               isNeedLcso();
        virtual MBOOL               isNeedRsso(MUINT32 eisMode) const;
        virtual MUINT32             getMinRrzoEisW();
        virtual MUINT32             getEisRatio100X() const;
        virtual MUINT32             getEisMode() const;
    };

    struct parsedAppRequest
    {
        // original AppRequest
        AppRequest* const           pRequest;
        // in
        KeyedVector< StreamId_T, sp<IImageStreamInfo> >
                                    vIImageInfos_Raw;
        KeyedVector< StreamId_T, sp<IImageStreamInfo> >
                                    vIImageInfos_Opaque;
        KeyedVector< StreamId_T, sp<IImageStreamInfo> >
                                    vIImageInfos_Yuv;
        // out
        KeyedVector< StreamId_T, sp<IImageStreamInfo> >
                                    vOImageInfos_Raw;
        KeyedVector< StreamId_T, sp<IImageStreamInfo> >
                                    vOImageInfos_Opaque;
        KeyedVector< StreamId_T, sp<IImageStreamInfo> >
                                    vOImageInfos_Yuv;
        KeyedVector< StreamId_T, sp<IImageStreamInfo> >
                                    vOImageInfos_Jpeg;
        //
                                    parsedAppRequest(AppRequest* pRequest)
                                        : pRequest(pRequest)
                                    {}
                                    ~parsedAppRequest() {}
        //
        MERROR                      parse();
    };

    struct evaluateRequestResult
    {
        struct reprocessInfo
        {
            bool useYuvIn     = false;
            bool useOpaqueIn  = false;
            bool useOpaqueOut = false;
        };

        DefaultKeyedVector<
            StreamId_T,
            sp<IImageStreamInfo>
                >                               vUpdatedImageInfos;
        //
        NodeSet                                 roots;
        NodeEdgeSet                             edges;
        bool                                    isTSflow = false;
        bool                                    forceFullRaw = false;
        reprocessInfo                           reprocInfos;
        //
        DefaultKeyedVector<NodeId_T, IOMapSet>  nodeIOMapImage;
        DefaultKeyedVector<NodeId_T, IOMapSet>  nodeIOMapMeta;
        //
        DefaultKeyedVector<StreamId_T, sp<IImageStreamBuffer> >
                                                vAppImageBuffers;
        DefaultKeyedVector<StreamId_T, sp<NSCam::v3::Utils::HalImageStreamBuffer> >
                                                vHalImageBuffers;
        DefaultKeyedVector<StreamId_T, sp<IMetaStreamBuffer> >
                                                vAppMetaBuffers;
        DefaultKeyedVector<StreamId_T, sp<NSCam::v3::Utils::HalMetaStreamBuffer> >
                                                vHalMetaBuffers;
#if MTKCAM_HAVE_IVENDOR_SUPPORT
        KeyedVector< MINT64, plugin::VendorInfo > vVendorInfo;
#endif
        MBOOL                                   hasEncOut = MFALSE;
        MBOOL                                   isRepeating = MFALSE;
        //
        bool                                    isOpaqueReprocOut()
                                                { return reprocInfos.useOpaqueOut; }
        bool                                    isOpaqueReprocIn()
                                                { return reprocInfos.useOpaqueIn; }
        bool                                    isYuvReprocIn()
                                                { return reprocInfos.useYuvIn; }
    };

    struct evaluateSubRequestResult
    {
        MUINT32 subRequetNumber;
        //
        Vector<evaluateRequestResult> subRequestList;
        //
        evaluateSubRequestResult()
            : subRequetNumber(0)
        {}
    };

public:
    class   ConfigHandler;
    class   RequestHandler;
    class   VendorHandler;

    struct CommonInfo
    {
                                        CommonInfo(MINT32 const openId, android::String8 const& name)
                                            : mOpenId(openId), mName(name)
                                        {}
        MINT32 const                    mOpenId;
        android::String8 const          mName;

        MINT32                          mLogLevel;
        MBOOL                           mJpegRotationEnable;
        sp<PipelineContext>             mpPipelineContext = nullptr;

#if MTKCAM_HAVE_ADV_SETTING
        AdvCamSettingMgr*               mpAdvSettingMgr = nullptr;
        MBOOL                           mbFirstReqReceived = false;
#endif
    };

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Implementations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////                    Data Members.
    std::shared_ptr<CommonInfo>     mCommonInfo = nullptr;
    std::shared_ptr<MyProcessedParams>
                                    mParams = nullptr;
    android::RWLock                 mRWLock;

    sp<PipelineDefaultImp::ConfigHandler>
                                    mConfigHandler = nullptr;
    sp<PipelineDefaultImp::RequestHandler>
                                    mRequestHandler = nullptr;
    sp<PipelineDefaultImp::VendorHandler>
                                    mVendorHandler = nullptr;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IPipelineModel Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////                    Operations.
    virtual char const*             getName() const { return mCommonInfo->mName.string(); }
    virtual MINT32                  getOpenId() const { return mCommonInfo->mOpenId; }

    virtual MERROR                  submitRequest(AppRequest& request);

    virtual MERROR                  beginFlush();

    virtual MVOID                   endFlush();

    virtual MVOID                   endRequesting() {}

    virtual MVOID                   waitDrained();

    virtual sp<PipelineContext>     getContext();

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  PipelineModel_Default Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////                    Operations.
    virtual MERROR                  configure(
                                        PipeConfigParams const& rConfigParams,
                                        android::sp<IPipelineModel> pOldPipeline
                                    );
    virtual void                    onLastStrongRef(const void* id);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Implementations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////                    Instantiation.
                                    ~PipelineDefaultImp();
                                    PipelineDefaultImp(
                                        MINT32 const openId,
                                        android::String8 const& name,
                                        wp<IPipelineModelMgr::IAppCallback> pAppCallback
                                    );

private:    ////                    Operations.
    MBOOL                           skipStream(
                                        MBOOL skipJpeg,
                                        IImageStreamInfo* pStreamInfo
                                    ) const;
};

class PipelineDefaultImp::ConfigHandler
    : public android::RefBase
{
public:
    struct pass1Resource
    {
        enum StreamStatus
        {
            eStatus_Uninit  = 0,
            eStatus_Inited,
            eStatus_Reuse,
            eStatus_NoNeed,
            eNUM_Status
        };
        //
        KeyedVector< StreamId_T, StreamStatus>
                                    vStreamStatus;

        MBOOL                       bStreamReused;

                                    pass1Resource()
                                        : vStreamStatus()
                                        , bStreamReused()
                                    {}

                                    ~pass1Resource()
                                    {}

        MINT32                      setCapacity(size_t size) { return vStreamStatus.setCapacity(size); }

        MVOID                       setReuseFlag(MBOOL flag) { bStreamReused = flag; }

        MBOOL                       getReuseFlag() { return bStreamReused; }

        MVOID                       updateStreamStatus(
                                        StreamId_T const streamId,
                                        StreamStatus eStatus
                                    )
                                    {
                                        ssize_t const index = vStreamStatus.indexOfKey(streamId);
                                        if ( index < 0 )
                                            vStreamStatus.add(streamId, eStatus);
                                        else
                                            vStreamStatus.replaceValueFor(streamId, eStatus);
                                    }

        MBOOL                       checkStreamsReusable()
                                    {
                                        MBOOL bRet = MTRUE;
                                        for ( size_t i=0; i<vStreamStatus.size(); i++) {
                                            if ( vStreamStatus.valueAt(i) < StreamStatus::eStatus_Reuse )
                                                return MFALSE;
                                        }
                                        return MTRUE;
                                    }
    };

public:
                                    ConfigHandler(
                                        std::shared_ptr<CommonInfo> pCommonInfo,
                                        std::shared_ptr<MyProcessedParams> pParams,
                                        sp<PipelineDefaultImp::VendorHandler> pVendorHandler
                                    );
                                    ~ConfigHandler();

    MERROR                          configureLocked(
                                        PipeConfigParams const& rConfigParams,
                                        android::sp<IPipelineModel> pOldPipeline
                                    );

    MERROR                          reconfigPipelineLocked(parsedAppRequest const& request);

protected:

    MVOID                           evaluatePreviewSize(
                                        PipeConfigParams const& rConfigParams,
                                        MSize &rSize
                                    );

    MERROR                          setupAppStreamsLocked(
                                        PipeConfigParams const& rConfigParams
                                    );

    MERROR                          setupHalStreamsLocked(
                                        PipeConfigParams const& rConfigParams
                                    );

    MERROR                          configContextLocked_Streams(sp<PipelineContext> pContext);
    MERROR                          configContextLocked_Nodes(sp<PipelineContext> pContext, MBOOL isReConfig = MFALSE);
    MERROR                          configContextLocked_Pipeline(sp<PipelineContext> pContext);
    //
    MERROR                          checkPermission();
    MERROR                          configRequestRulesLocked();
    MERROR                          configScenarioCtrlLocked();
    //
    MERROR                          configContextLocked_P1Node(sp<PipelineContext> pContext, MBOOL isReConfig = MFALSE);
    MERROR                          configContextLocked_P2Node(sp<PipelineContext> pContext, MBOOL isReConfig = MFALSE);
    MERROR                          configContextLocked_P2TSNode(sp<PipelineContext> pContext, MBOOL isReConfig = MFALSE);
    MERROR                          configContextLocked_FdNode(sp<PipelineContext> pContext, MBOOL isReConfig = MFALSE);
    MERROR                          configContextLocked_JpegNode(sp<PipelineContext> pContext, MBOOL isReConfig = MFALSE);
    MERROR                          configContextLocked_Raw16Node(sp<PipelineContext> pContext, MBOOL isReConfig = MFALSE);

    //
    template <class INITPARAM_T, class CONFIGPARAM_T>
    MERROR                          compareParamsLocked_P1Node(
                                        INITPARAM_T const& initParam1,  INITPARAM_T const& initParam2,
                                        CONFIGPARAM_T const& cfgParam1, CONFIGPARAM_T const& cfgParam2
                                    ) const;

private:
    // // utility functions
    sp<NSCam::v3::Utils::ImageStreamInfo>
                                    createRawImageStreamInfo(
                                        char const*         streamName,
                                        StreamId_T          streamId,
                                        MUINT32             streamType,
                                        size_t              maxBufNum,
                                        size_t              minInitBufNum,
                                        MUINT               usageForAllocator,
                                        MINT                imgFormat,
                                        MSize const&        imgSize,
                                        size_t const        stride
                                    ) const;

    P2Common::UsageHint             prepareP2Usage(
                                        P2Node::ePass2Type type
                                    ) const;

protected:
    std::shared_ptr<CommonInfo>     mCommonInfo = nullptr;

    std::shared_ptr<MyProcessedParams>
                                    mParams = nullptr;

    sp<PipelineDefaultImp::VendorHandler>
                                    mVendorHandler = nullptr;

    sp<PipelineContext>             mpOldCtx = nullptr;

private:
    pass1Resource                   mPass1Resource;

    CamManager*                     mpCamMgr = nullptr;

    sp<CamManager::UsingDeviceHelper>
                                    mpDeviceHelper = nullptr;

};

class PipelineDefaultImp::RequestHandler
    : public IPipelineBufferSetFrameControl::IAppCallback
{
public: // structure
    enum ePath
    {
        eImagePathP1,
        eImagePathP2Resized,
        eImagePathP2Full,
        eImagePathP2TS,
        eImagePathRaw16,
        eImagePathJpeg,
        eImagePathFD,
        //
        eMetaPathP1,
        eMetaPathP2,
        eMetaPathP2TS,
        eMetaPathRaw16,
        eMetaPathJpeg,
        eMetaPathFD,
        //
        ePathCount,
    };
    //
    struct IOMapColloctor
    {
                                    IOMapColloctor() {}

        IOMap                       maps[ePathCount];
        DefaultKeyedVector<
            StreamId_T,
            sp<IImageStreamInfo>
                >                   vUpdatedImageInfos;
        //
        IOMap&                      editIOMap(ePath path) { return maps[path]; }
        MVOID                       addIn(ePath path, sp<IStreamInfo> pInfo) {
                                        editIOMap(path).addIn(pInfo->getStreamId());
                                    }
        MVOID                       addOut(ePath path, sp<IStreamInfo> pInfo) {
                                        editIOMap(path).addOut(pInfo->getStreamId());
                                    }
        size_t                      sizeIn(ePath path) {
                                        return editIOMap(path).sizeIn();
                                    }
        size_t                      sizeOut(ePath path) {
                                        return editIOMap(path).sizeOut();
                                    }
        MBOOL                       isConfigured(ePath path) {
                                        return editIOMap(path).vIn.size() ||
                                            editIOMap(path).vOut.size();
                                    }
        MVOID                       updateStreamInfo(sp<IImageStreamInfo> pInfo) {
                                        vUpdatedImageInfos.add(pInfo->getStreamId(), pInfo);
                                    }
    };

public:
                                    RequestHandler(
                                        std::shared_ptr<CommonInfo> pCommonInfo,
                                        std::shared_ptr<MyProcessedParams> pParams,
                                        wp<IPipelineModelMgr::IAppCallback> pAppCallback,
                                        sp<PipelineDefaultImp::ConfigHandler> pConfigHandler,
                                        sp<PipelineDefaultImp::VendorHandler> pVendorHandler
                                    );
                                    ~RequestHandler();

    MERROR                          submitRequestLocked(AppRequest& request);

protected:

    static inline MBOOL             isStream(sp<IStreamInfo> pStreamInfo, StreamId_T streamId )
                                    {
                                        return pStreamInfo.get() && pStreamInfo->getStreamId()==streamId;
                                    }

    MERROR                          evaluateRequestLocked(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result
                                    );

    MERROR                          evaluateRequestLocked_Img_Reproc(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result,
                                        IOMapColloctor& aCollector
                                    );

    MERROR                          evaluateRequestLocked_Img_Raw16(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result,
                                        IOMapColloctor& aCollector
                                    );

    MERROR                          evaluateRequestLocked_Img_Lcso(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result,
                                        IOMapColloctor& aCollector
                                    );

    MERROR                          evaluateRequestLocked_Img_Rsso(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result,
                                        IOMapColloctor& aCollector
                                    );

    MERROR                          evaluateRequestLocked_Img_Jpeg(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result,
                                        IOMapColloctor& aCollector
                                    );

    MERROR                          evaluateRequestLocked_Img_AppYuvs(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result,
                                        IOMapColloctor& aCollector
                                    );

    MERROR                          evaluateRequestLocked_Img_FD(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result,
                                        IOMapColloctor& aCollector
                                    );

    MERROR                          evaluateRequestLocked_Opt(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result,
                                        IOMapColloctor& aCollector
                                    );

    MERROR                          evaluateRequestLocked_Metadata(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result,
                                        IOMapColloctor& aCollector
                                    );

    MERROR                          evaluateRequestLocked_updateIOMap(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result,
                                        IOMapColloctor& aCollector
                                    );

    MERROR                          evaluateRequestLocked_updateStreamBuffers(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result,
                                        IOMapColloctor& aCollector
                                    );

    MERROR                          refineRequestMetaStreamBuffersLocked(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result
                                    );

    sp<IPipelineFrame>              buildPipelineFrameLocked(
                                        MUINT32 requestNo,
                                        evaluateRequestResult&  result
                                    );

    MERROR                          createStreamInfoLocked_Thumbnail_YUV(
                                        IMetadata const* pMetadata,
                                        android::sp<IImageStreamInfo>& rpStreamInfo
                                    ) const;

    MERROR                          createStreamInfoLocked_Jpeg_YUV(
                                        IMetadata const* pMetadata,
                                        android::sp<IImageStreamInfo>& rpStreamInfo
                                    ) const;

    MSize                           calcThumbnailYuvSize(
                                        MSize const rPicSize,
                                        MSize const rThumbnailsize
                                    ) const;

    MBOOL                           isFdEnable(
                                        parsedAppRequest const& request,
                                        evaluateRequestResult& result
                                    );

    MBOOL                           isFdEnable(
                                        IMetadata const* pMetadata
                                    );

    MBOOL                           isTimeSharingForJpegSource(
                                        parsedAppRequest const& request
                                    ) const;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IPipelineBufferSetFrameControl::IAppCallback Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////                    Operations.
    virtual MVOID                   updateFrame(
                                        MUINT32 const frameNo,
                                        MINTPTR const userId,
                                        Result const& result
                                    );

protected:
    android::wp<IPipelineModelMgr::IAppCallback> const
                                    mpAppCallback;

    std::shared_ptr<CommonInfo>     mCommonInfo = nullptr;

    std::shared_ptr<MyProcessedParams>
                                    mParams = nullptr;

    sp<PipelineDefaultImp::ConfigHandler>
                                    mConfigHandler = nullptr;
    sp<PipelineDefaultImp::VendorHandler>
                                    mVendorHandler = nullptr;

    MBOOL                           mPrevFDEn;

};

class PipelineDefaultImp::VendorHandler
    : public android::RefBase
{
public: // structure
                                    VendorHandler(
                                        std::shared_ptr<CommonInfo> pCommonInfo,
                                        std::shared_ptr<MyProcessedParams> pParams
                                    );
                                    ~VendorHandler();

    MVOID                           getVendorCfg(
                                        android::sp<NSCam::plugin::IVendorManager>& pVendor,
                                        MUINT64& userId
                                    );


    MBOOL                           refinePluginRequestMetaStreamBuffersLocked(
                                        evaluateRequestResult& result,
                                        evaluateSubRequestResult& subResult
                                    );

    MERROR                          setPluginResult(
                                        MUINT32 startFrameNo,
                                        evaluateRequestResult&    result,
                                        evaluateSubRequestResult& subResult
                                    );

    sp<IPipelineFrame>              buildPipelineFrameLocked(
                                        MUINT32 requestNo,
                                        evaluateRequestResult&  result
                                    );

    sp<IPipelineFrame>              buildSubPipelineFrameLocked(
                                        MUINT32 requestNo,
                                        evaluateRequestResult& result
                                    );

protected:
    MERROR                          updateCombination(
                                        plugin::InputInfo&     input,
                                        evaluateRequestResult& result
                                    );
protected:
#if SUPPORT_IVENDOR
    sp<NSCam::plugin::IVendorManager>
                                    mpVendorMgr = nullptr;

    MUINT64                         mUserId;
#endif
    std::shared_ptr<CommonInfo>     mCommonInfo = nullptr;

    std::shared_ptr<MyProcessedParams>
                                    mParams = nullptr;

};

/******************************************************************************
 *
 ******************************************************************************/
};  //namespace v3
};  //namespace NSCam

#endif  //_MTK_PLATFORM_HARDWARE_MTKCAM_V3_HWPIPELINE_PIPELINEDEFAULTIMP_H_
