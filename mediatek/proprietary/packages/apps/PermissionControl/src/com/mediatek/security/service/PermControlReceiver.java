package com.mediatek.security.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.mediatek.security.datamanager.CheckedPermRecord;
import com.mediatek.security.ui.AutoBootManager;
import android.os.UserHandle;

public class PermControlReceiver extends BroadcastReceiver {
    private static final String TAG = "PermControlReceiver";
    public static final String IS_PACKAGE_ADD = "exta_is_package_add";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            Log.d(TAG, "action = " + action);
            switch(action) {
            case Intent.ACTION_BOOT_COMPLETED:
            {
                if (PermControlUtils.isCellularDataControlOn(context)) {
                    startService(context);
                }
                break;
            }
            case Intent.ACTION_PACKAGE_ADDED:
            {
                String pkg = getPackageName(intent);
                if (pkg != null) {
                    startService(context, pkg, true);
                }
                break;
            }
            case Intent.ACTION_PACKAGE_REMOVED:
            {
                String pkg = getPackageName(intent);
                if (pkg != null) {
                    startService(context, pkg, false);
                }
                break;
            }
            case Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE:
            {
                String[] pkgList = intent.getStringArrayExtra(Intent.EXTRA_CHANGED_PACKAGE_LIST);
                if (pkgList != null) {
                    for (String pkg : pkgList) {
                        startService(context, pkg, true);
                    }
                }
                break;
            }
            case Intent.ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE:
            {
                String[] pkgList = intent.getStringArrayExtra(Intent.EXTRA_CHANGED_PACKAGE_LIST);
                if (pkgList != null) {
                    for (String pkg : pkgList) {
                        startService(context, pkg, false);
                    }
                }
                break;
            }
            }
        }
    }

    private void startService(Context context){
        Log.d(TAG, "start service");
        Intent intent = new Intent();
        intent.setClass(context, PermControlService.class);
        context.startService(intent);
    }

    private void startService(Context context, String pkg, boolean isAdd) {
        Intent intent = new Intent(PermControlUtils.ACTION_PACKAGE_UPDATE);
        intent.putExtra(PermControlUtils.PACKAGE_NAME, pkg);
        intent.putExtra(IS_PACKAGE_ADD, isAdd);
        intent.setClass(context, PermControlService.class);
        context.startService(intent);
    }

    private String getPackageName(Intent intent) {
        Uri uri = intent.getData();
        String pkg = uri != null ? uri.getSchemeSpecificPart() : null;
        Log.d(TAG, "pkg = " + pkg);
        return pkg;
    }
}
