/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include <cutils/properties.h>
#include <utils/Mutex.h>
#include <vendor/mediatek/hardware/radio/2.0/IRadio.h>
#include <vendor/mediatek/hardware/radio/2.0/IAtciIndication.h>
#include <vendor/mediatek/hardware/radio/2.0/IAtciResponse.h>
#include "atcid.h"
#include "atcid_adaptation.h"

#define MAX_SLOT_NUM 4
#define RIL1_SERVICE_NAME "slot1"
#define RIL2_SERVICE_NAME "slot2"
#define RIL3_SERVICE_NAME "slot3"
#define RIL4_SERVICE_NAME "slot4"

using namespace android::hardware::radio::V1_0;
using namespace vendor::mediatek::hardware::radio::V2_0;
using ::android::Mutex;
using ::android::hardware::Return;
using ::android::hardware::hidl_death_recipient;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Void;
using ::android::hidl::base::V1_0::IBase;
using ::android::sp;
using ::android::wp;
namespace NS_VENDOR = vendor::mediatek::hardware::radio::V2_0;

class AtcidAdaptation {
private:
    sp<NS_VENDOR::IRadio> mRadio[MAX_SLOT_NUM];
    sp<IAtciResponse> mAtciResponse;
    sp<IAtciIndication> mAtciIndication;
    sp<hidl_death_recipient> mRadioDeathRecipient;
    int mFd = -1;
    Mutex mLock;
    static AtcidAdaptation* sInstance;

public:
    static AtcidAdaptation *getInstance();
    void setSocketFd(int fd) { mFd = fd; }
    bool sendRequest(const char *data, int size);
    void sendResponse(const char *data, int size);
    void serviceDied();

private:
    AtcidAdaptation();
    void setUp(int slotId);
    int getSlotId();
};

class AtciResponse : public IAtciResponse {
private:
    AtcidAdaptation *mAtci;

public:
    explicit AtciResponse(AtcidAdaptation *atcid) : mAtci(atcid) {}

    virtual Return<void> sendAtciResponse(const RadioResponseInfo& info,
            const hidl_vec<uint8_t>& data) override {
        UNUSED(info);
        const uint8_t *str = data.data();
        LOGATCI(LOG_INFO, "%s, %lu", str, data.size());
        mAtci->sendResponse((const char *)str, data.size());
        return Void();
    }
};

class AtciIndication : public IAtciIndication {
private:
    AtcidAdaptation *mAtci;

public:
    explicit AtciIndication(AtcidAdaptation *atcid) : mAtci(atcid) {}

    virtual Return<void> atciInd(RadioIndicationType info,
            const hidl_vec<uint8_t>& data) override {
        UNUSED(info);
        const uint8_t *str = data.data();
        LOGATCI(LOG_INFO, "%s, %lu", str, data.size());
        mAtci->sendResponse((const char *)str, data.size());
        return Void();
    }
};

class RadioDeathRecipient : public hidl_death_recipient {
private:
    AtcidAdaptation *mAtci;

public:
    explicit RadioDeathRecipient(AtcidAdaptation *atcid) : mAtci(atcid) {}

    virtual void serviceDied(uint64_t cookie, const wp<IBase>& who) override {
        UNUSED(cookie);
        UNUSED(who);
        LOGATCI(LOG_ERR, "enter");
        mAtci->serviceDied();
    }
};

AtcidAdaptation* AtcidAdaptation::sInstance = NULL;

AtcidAdaptation* AtcidAdaptation::getInstance() {
    if (sInstance == NULL) {
        sInstance = new AtcidAdaptation();
    }
    return sInstance;
}

AtcidAdaptation::AtcidAdaptation() {
    mAtciResponse = new AtciResponse(this);
    mAtciIndication = new AtciIndication(this);
    mRadioDeathRecipient = new RadioDeathRecipient(this);
}

void AtcidAdaptation::setUp(int slotId) {
    const char *serviceNames[MAX_SLOT_NUM] = {
        RIL1_SERVICE_NAME,
        RIL2_SERVICE_NAME,
        RIL3_SERVICE_NAME,
        RIL4_SERVICE_NAME
    };
    if (slotId >= 0 && slotId < MAX_SLOT_NUM) {
        mRadio[slotId] = NS_VENDOR::IRadio::getService(serviceNames[slotId]);
        if (mRadio[slotId] != NULL) {
            mRadio[slotId]->setResponseFunctionsForAtci(mAtciResponse, mAtciIndication);
            mRadio[slotId]->linkToDeath(mRadioDeathRecipient, 0);
        } else {
            LOGATCI(LOG_ERR, "getService %s failed", serviceNames[slotId]);
        }
    }
}

void AtcidAdaptation::serviceDied() {
    LOGATCI(LOG_INFO, "enter");
    Mutex::Autolock autoLock(mLock);
    for (int i = 0; i < MAX_SLOT_NUM; i++) {
        mRadio[i] = NULL;
    }
}

bool AtcidAdaptation::sendRequest(const char *data, int size) {
    LOGATCI(LOG_INFO, "%s", data);
    Mutex::Autolock autoLock(mLock);
    int slotId = getSlotId();
    if (slotId >= 0 && slotId < MAX_SLOT_NUM) {
        if (mRadio[slotId] == NULL) {
            setUp(slotId);
        }
        if (mRadio[slotId] != NULL) {
            hidl_vec<uint8_t> vec;
            vec.setToExternal((uint8_t *)data, size);
            Return<void> ret = mRadio[slotId]->sendAtciRequest(0xFFFFFFFF, vec);
            if (ret.isOk()) {
                return true;
            } else {
                mRadio[slotId] = NULL;
            }
        }
    }
    return false;
}

void AtcidAdaptation::sendResponse(const char *data, int size) {
    LOGATCI(LOG_INFO, "%s", data);
    Mutex::Autolock autoLock(mLock);
    if (data != NULL) {
        int sendLen = send(mFd, data, size, 0);
        LOGATCI(LOG_INFO, "sendLen = %d", sendLen);
    }
}

int AtcidAdaptation::getSlotId() {
    char simNo[PROPERTY_VALUE_MAX] = {0};
    property_get("persist.service.atci.sim", simNo, "0");
    LOGATCI(LOG_DEBUG, "simNo: %s", simNo);
    int slotId = atoi(simNo);
#if defined(WIFI_ONLY)
    slotId = -1;
#endif
    LOGATCI(LOG_DEBUG, "slotId: %d", slotId);

    if (slotId >= 0 && slotId < MAX_SLOT_NUM) {
        return slotId;
    } else if (slotId == 9) {
        char tempstr[PROPERTY_VALUE_MAX] = { 0 };
        memset(tempstr, 0, sizeof(tempstr));
        property_get("persist.radio.cdma_slot", tempstr, "1");
        int cdmaSlotId = atoi(tempstr) - 1;
        return cdmaSlotId;
    }
    LOGATCI(LOG_ERR, "unsupported slot number.");
    return -1;
}

void setSocketFdForAdaptation(int fd) {
    AtcidAdaptation::getInstance()->setSocketFd(fd);
}

bool sendRequest(const char *data, int size) {
    return AtcidAdaptation::getInstance()->sendRequest(data, size);
}
