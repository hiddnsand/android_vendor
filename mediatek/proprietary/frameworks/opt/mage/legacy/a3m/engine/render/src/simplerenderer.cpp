/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * A3M Baseline render loop
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/simplerenderer.h>   /* This class's API */
#include <a3m/renderdevice.h>     /* To query the viewport dimensions */
#include <a3m/rendertarget.h>     /* RenderTarget class */
#include <a3m/appearance.h>       /* For appearance class */
#include <a3m/assetcachepool.h>   /* For asset cache pool class */
#include <a3m/camera.h>           /* For camera class */
#include <a3m/solid.h>            /* For solid class */
#include <a3m/light.h>            /* For light class */
#include <a3m/sceneutility.h>     /* For visitScene() */
#include <a3m/properties.h>       /* properties namespace */
#include <a3m/noncopyable.h>      /* For NonCopyable */

using namespace a3m;
using namespace properties;

#ifdef A3M_USE_LIGHT_ZORDER
#include <a3me/a3me.h>
#endif

namespace
{
  /* These numbers should correspond to their equivalent definition in the
   * shaders.  Any light or joint over and above the maximum value will be
   * ignored by the shader. */
  const A3M_INT32 MAX_LIGHTS = 5;
  const A3M_INT32 MAX_JOINTS = 20;

  // Prevent an arithmetic overflow by ensuring the near and far attenuation are
  // separated by at least this amount.
  // Max mediump float = 16383,  1/0.001 = 1000 (so there is some room for other
  // operations in the vertex/fragment shader).
  const A3M_FLOAT MIN_LIGHT_ATT_SEPARATION = 0.001f;

  Vector4f toVector( Colour4f const& c )
  {
    return Vector4f( c.r, c.g, c.b, c.a );
  }

  void setLightProperties( RenderContext& context,
                           SimpleRenderer::LightList const& lightList )
  {
    A3M_INT32 lightCount =
      std::min( static_cast< A3M_INT32 >(lightList.size()), MAX_LIGHTS );
    context.getProperty( L_COUNT )->setValue( lightCount );

    for( A3M_INT32 i = 0; i != lightCount; ++i )
    {
      // Using the same trick as the OpenGL fixed pipeline, if the w
      // component of the light position is 0 the light is treated as
      // directional. Otherwise it is a point or spot. The vector actually
      // points in the opposite direction to the light to avoid a negation
      // in the shader code when we dot-product with the surface normal.
      if( lightList[i]->getLightType() == a3m::Light::LIGHTTYPE_DIRECTIONAL )
      {
        context.getProperty( L_POSITION )->setValue(
          normalize( lightList[i]->getWorldTransform().k ),
          i );
      }
      else
      {
        context.getProperty( L_POSITION )->setValue(
          lightList[i]->getWorldTransform().t, i );
      }
      context.getProperty( L_AMBIENT_COLOUR )->setValue(
        toVector( lightList[i]->getColour() )
        * lightList[i]->getAmbientLevel()
        * lightList[i]->getIntensity(), i );
      context.getProperty( L_DIFFUSE_COLOUR )->setValue(
        toVector( lightList[i]->getColour() )
        * lightList[i]->getIntensity(), i );
      context.getProperty( L_SPECULAR_COLOUR )->setValue(
        toVector( lightList[i]->getColour() )
        * lightList[i]->getIntensity(), i );

      if( lightList[i]->getIsAttenuated() )
      {
        A3M_FLOAT scale = length( lightList[i]->getWorldTransform().i );
        A3M_FLOAT attNear = lightList[i]->getAttenuationNear() * scale;
        context.getProperty( L_ATTENUATION_NEAR ) ->setValue(attNear, i );

        A3M_FLOAT attFar = lightList[i]->getAttenuationFar() * scale;
        if( attFar < ( attNear + MIN_LIGHT_ATT_SEPARATION ) )
        {
          attFar = attNear + MIN_LIGHT_ATT_SEPARATION;
        }
        context.getProperty( L_ATTENUATION_RECIPROCAL ) ->setValue(1.0f /
            (attFar - attNear), i );
      }
      else
      {
        context.getProperty( L_ATTENUATION_RECIPROCAL ) ->setValue(0.0f, i );
      }

      if( lightList[i]->getLightType() == a3m::Light::LIGHTTYPE_SPOT )
      {
        context.getProperty( L_SPOT_INNER_COS )->setValue(
          cos( 0.5f * lightList[i]->getSpotInnerAngle() ),
          i );
        context.getProperty( L_SPOT_OUTER_COS )->setValue(
          cos( 0.5f * lightList[i]->getSpotOuterAngle() ),
          i );
        context.getProperty( L_SPOT_DIRECTION ) ->setValue(normalize( Vector3f(
              lightList[i]->getWorldTransform().k ) ), i );
      }
      else
      {
        // These values represent the cones coming from the spot light
        // wrapping all the way around to the back of the light so
        // the same code used for calculating spot light fall-off can
        // be used for a point light with no fall-off (although more
        // efficient to test explicitly for the inner cos == -1).
        context.getProperty( L_SPOT_INNER_COS ) ->setValue(-1.0f, i );
        context.getProperty( L_SPOT_OUTER_COS ) ->setValue(-1.0f, i );
      }
    }
  }


  void renderSolids( RenderContext& context,
                     SimpleRenderer::RenderList const& renderList,
                     Matrix4f const& inverseCamera,
                     Matrix4f const& projection )
  {
    ShaderProgram::Ptr lastProgramUsed; // record the last shader program used
    // to avoid setting the light uniforms
    // for every solid.

    Matrix4f viewProjection = projection * inverseCamera;
    context.getProperty( T_VIEW_PROJECTION ) ->setValue(viewProjection );

    for( SimpleRenderer::RenderList::const_iterator solid = renderList.begin(),
         endSolid = renderList.end();
         solid != endSolid; ++solid )
    {
      Matrix4f const& model = (*solid)->getWorldTransform();
      Matrix4f modelView( inverseCamera * model );
      Matrix3f normalTransform = inverseTranspose(
                                   Matrix3f( Vector3f( model.i ),
                                             Vector3f( model.j ),
                                             Vector3f( model.k ) ) );

      // Set per-solid uniforms
      context.getProperty( T_MODEL_VIEW_PROJECTION )->setValue(
        projection * modelView );
      context.getProperty( T_MODEL_VIEW ) ->setValue(modelView );
      context.getProperty( T_MODEL ) ->setValue(model );
      context.getProperty( T_NORMAL_MODEL ) ->setValue(normalTransform );

      (*solid)->draw(context);
    }
  }

  class DepthSorter
  {
  public:
    DepthSorter( Vector4f const& direction ) : m_direction( direction ) {}
    A3M_BOOL operator()( Solid* a, Solid* b )
    {
      return dot( m_direction, Vector3f( a->getWorldTransform().t ) ) <
             dot( m_direction, Vector3f( b->getWorldTransform().t ) );
    }
  private:
    Vector3f m_direction;
  };


  void renderReflection(
    RenderContext& context,
    Solid& mirror,
    SimpleRenderer::RenderList const& opaqueSolidsToRender,
    SimpleRenderer::RenderList const& blendedSolidsToRender,
    AssetCachePool& pool,
    SharedPtr< RenderTarget >& reflectionTarget,
    Matrix4f const& cameraPosTrans,
    Matrix4f const& projection,
    Matrix4f const& inverseCamera )
  {
    if (!reflectionTarget)
    {
      Texture2D::Ptr colourTexture =
        pool.texture2DCache()->createFromBackbuffer(context, Texture::RGB);
      reflectionTarget = RenderTarget::Ptr( new RenderTarget(
                                              colourTexture,
                                              Texture2D::Ptr() ) );
    }

    A3M_INT32 oldVx = context.getViewportLeft();
    A3M_INT32 oldVy = context.getViewportBottom();
    A3M_INT32 oldVw = context.getViewportWidth();
    A3M_INT32 oldVh = context.getViewportHeight();
    // Note these will be 'half-width's in stereo case

    reflectionTarget->enable();

    context.setViewport(
      0, 0,
      reflectionTarget->getColourTexture()->getWidth(),
      reflectionTarget->getColourTexture()->getHeight() );

    context.clear();

    // Reflect in the plane passing through object's position oriented in the z
    // direction.

    Matrix4f const& mirrorTrans = mirror.getWorldTransform();

    Vector3f normal = normalize( Vector3f( mirrorTrans.k ) );

    A3M_FLOAT x = normal.x, y = normal.y, z = normal.z;
    A3M_FLOAT distToOrigin = dot( normal, Vector3f( mirrorTrans.t ) );

    Matrix4f reflect(
      Vector4f(  1.f - 2.f * x * x, -2.f * x * y,       -2.f * x * z,       0 ),
      Vector4f( -2.f * x * y,        1.f - 2.f * y * y, -2.f * y * z,       0 ),
      Vector4f( -2.f * x * z,       -2.f * y * z,        1.f - 2.f * z * z, 0 ),
      Vector4f( 2.f * distToOrigin * normal, 1.0 ) );

    Matrix4f inverseReflectedCamera = inverse( reflect * cameraPosTrans );

    // Draw everything upside-down to undo the winding reversal effects of the
    // reflection.
    Matrix4f reflectedProjection =  projection * scale( 1.f, -1.f, 1.f, 1.f );

    renderSolids( context, opaqueSolidsToRender,
                  inverseReflectedCamera, reflectedProjection );
    renderSolids( context, blendedSolidsToRender,
                  inverseReflectedCamera, reflectedProjection );

    reflectionTarget->disable();

    context.setViewport( oldVx, oldVy, oldVw, oldVh );

    mirror.getAppearance()->setProperty( M_MIRROR_TEXTURE,
                                         reflectionTarget->getColourTexture() );

    // put the mirror solid on a list to be rendered normally.
    SimpleRenderer::RenderList tempList;
    tempList.push_back( &mirror );
    renderSolids( context, tempList, inverseCamera, projection );
  }
}

namespace a3m
{
#ifdef A3ME_SUPPORT
    const A3M_INT32 MaxCount = 50;
    const A3M_INT32 ThresholdCount = 25;
#endif

  SimpleRenderer::SimpleRenderer(
    RenderContext::Ptr const& context, AssetCachePool::Ptr const& pool) :
    m_context(context),
    m_pool(pool),
    m_time(0.f),
    m_lightZOrder(0),
    m_lightCounter(0)
  {
#ifdef A3M_USE_LIGHT_ZORDER
    m_lightZOrder = a3m_drvb_f0();
#endif

    // Register all uniforms defined by the renderer
    // We must do this to ensure that all properties are linked to the
    // shader programs successfully.
    addProperty(*m_context, 0, L_COUNT);
    addPropertyArray<Vector4f>(*m_context, MAX_LIGHTS, L_DIFFUSE_COLOUR);
    addPropertyArray<Vector4f>(*m_context, MAX_LIGHTS, L_AMBIENT_COLOUR);
    addPropertyArray<Vector4f>(*m_context, MAX_LIGHTS, L_SPECULAR_COLOUR);
    addPropertyArray<Vector4f>(*m_context, MAX_LIGHTS, L_POSITION);
    addPropertyArray<A3M_FLOAT>(*m_context, MAX_LIGHTS, L_ATTENUATION_NEAR);
    addPropertyArray<A3M_FLOAT>(*m_context, MAX_LIGHTS, L_ATTENUATION_RECIPROCAL);
    addPropertyArray<A3M_FLOAT>(*m_context, MAX_LIGHTS, L_SPOT_INNER_COS);
    addPropertyArray<A3M_FLOAT>(*m_context, MAX_LIGHTS, L_SPOT_OUTER_COS);
    addPropertyArray<Vector3f>(*m_context, MAX_LIGHTS, L_SPOT_DIRECTION);

    addProperty(*m_context, 0, J_COUNT);
    addPropertyArray<Matrix4f>(*m_context, MAX_JOINTS, J_WORLD);

    addProperty(*m_context, Matrix4f::IDENTITY, T_MODEL);
    addProperty(*m_context, Matrix4f::IDENTITY, T_VIEW);
    addProperty(*m_context, Matrix4f::IDENTITY, T_MODEL_VIEW);
    addProperty(*m_context, Matrix4f::IDENTITY, T_VIEW_PROJECTION);
    addProperty(*m_context, Matrix4f::IDENTITY, T_MODEL_VIEW_PROJECTION);
    addProperty(*m_context, Matrix3f::IDENTITY, T_NORMAL_MODEL);
    addProperty(*m_context, Vector4f(0.0f, 0.0f, 0.0f, 1.0f), T_CAMERA_POSITION);

    addProperty(*m_context, 0.0f, TIME);
    addProperty(*m_context, 0.0f, FOG_DENSITY);
    addProperty(*m_context, Vector4f(0.0f, 0.0f, 0.0f, 1.0f), FOG_COLOUR);

    // System textures
    addProperty(
      *m_context, m_pool->texture2DCache()->get("a3m#black.png"), M_BLACK_TEXTURE);
    addProperty(
      *m_context, m_pool->texture2DCache()->get("a3m#white.png"), M_WHITE_TEXTURE);
    addProperty(
      *m_context, m_pool->texture2DCache()->get("a3m#error.png"), M_ERROR_TEXTURE);
  }

  void SimpleRenderer::render( Camera* camera,
                               SceneNode& node,
                               FlagMask const& renderFlags,
                               FlagMask const& recursiveFlags )
  {
#ifdef A3M_USE_LIGHT_ZORDER
    if (m_lightZOrder != 0)
    {
        // Not MTK platform, draw nothing
#ifndef A3ME_SUPPORT
        return;
#endif

#ifdef A3ME_SUPPORT
        // Flash the rendering if it's not MTK platform
        m_lightCounter ++;
        if (m_lightCounter % MaxCount > ThresholdCount)
        {
            return;
        }
#endif
    }
#endif

    m_opaqueSolidsToRender.clear();
    m_blendedSolidsToRender.clear();
    m_reflectionSolidsToRender.clear();
    m_lights.clear();

    // Record whether we are using a stereoscopic camera
    A3M_BOOL stereo = camera ? camera->isStereo() : A3M_FALSE;

    // Collect all the solids we need to render.
    // We AND the render and recursive flags to ensure that recursive flags are
    // a subset of render flags.
    visitScene(*this, node, renderFlags, renderFlags & recursiveFlags);

    const A3M_INT32 vpWidth = m_context->getViewportWidth();
    const A3M_INT32 vpHeight = m_context->getViewportHeight();
    const A3M_INT32 vpX = m_context->getViewportLeft();
    const A3M_INT32 vpY = m_context->getViewportBottom();

    const A3M_FLOAT aspect =
      static_cast<A3M_FLOAT>(vpWidth) /
      static_cast<A3M_FLOAT>(vpHeight);

    // Projection and transformation matrices for left/right cameras.
    // Use the "left" one (which will be central) in the mono case.

    Matrix4f camProjection[2]; // Stereo is intrinsically '2' cameras
    Matrix4f camPosTransform[2];
    A3M_INT16 eyeCount = 1;

    /* Get the camera's transform matrices. If there is no camera, then the
     * matrices will remain set to the identity matrix.
     */
    if( stereo )
    {
      camera->getStereoProjection(
        &camProjection[0], &camProjection[1], aspect );
      camera->getStereoWorldTransform(
        &camPosTransform[0], &camPosTransform[1] );
      eyeCount = 2;
    }
    else if( camera )
    {
      camera->getProjection( &camProjection[0], aspect );
      camPosTransform[0] = camera->getWorldTransform();
    }

    // Set per-render uniforms
    m_context->getProperty( TIME )->setValue( a3m::fmod( m_time, 60.f ) );
    setLightProperties( *m_context, m_lights );

    for( A3M_INT16 eye = 0; eye < eyeCount; eye++ )
    {
      if( stereo )
      {
        // Assuming a side-by-side mode as on the Bird phone, using one
        // framebuffer.  If other modes evolve (like really using two buffers)
        // then we will need to react differently here.
        // Use of vpX and vpY are 'on principle' - this whole scheme is going to
        // have strange effects unless vpX is zero.

        m_context->setViewport( vpX + (eye * vpWidth / 2), vpY,
                                vpWidth / 2, vpHeight );
      }

      Matrix4f inverseCamera = inverse( camPosTransform[eye] );

      // Set per-camera uniforms
      m_context->getProperty( T_CAMERA_POSITION )->setValue( camPosTransform[eye].t );
      m_context->getProperty( T_VIEW )->setValue( inverseCamera );

      std::sort( m_blendedSolidsToRender.begin(), m_blendedSolidsToRender.end(),
                 DepthSorter( camPosTransform[eye].k ) );

      renderSolids( *m_context, m_opaqueSolidsToRender,
                    inverseCamera, camProjection[eye] );

      for( SimpleRenderer::RenderList::const_iterator
           mirror = m_reflectionSolidsToRender.begin(),
           endMirror = m_reflectionSolidsToRender.end();
           mirror != endMirror; ++mirror )
      {
        renderReflection( *m_context, **mirror, m_opaqueSolidsToRender,
                          m_blendedSolidsToRender, *m_pool, m_reflectionTarget,
                          camPosTransform[eye], camProjection[eye], inverseCamera );
      }

      renderSolids( *m_context, m_blendedSolidsToRender,
                    inverseCamera, camProjection[eye] );
    }

    if( stereo )
    {
      // Recover original viewport set-up
      m_context->setViewport( vpX, vpY, vpWidth, vpHeight );
    }
  }

  /*
   * Add the given solid to the list of things to be rendered.
   */
  void SimpleRenderer::visit( Solid* solid )
  {
    Appearance::Ptr appearance = solid->getAppearance();
    A3M_ASSERT( appearance );

    ShaderProgram::Ptr program = appearance->getShaderProgram();
    A3M_ASSERT( program );

    if( program->uniformExists( M_MIRROR_TEXTURE ) )
    {
      m_reflectionSolidsToRender.push_back( solid );
    }
    else if( solid->getAppearance()->isOpaque() )
    {
      m_opaqueSolidsToRender.push_back( solid );
    }
    else
    {
      m_blendedSolidsToRender.push_back( solid );
    }
  }

  /*
   * Add the given light to the list of things to be rendered.
   */
  void SimpleRenderer::visit( Light* light )
  {
    m_lights.push_back( light );
  }

} /* namespace a3m */
