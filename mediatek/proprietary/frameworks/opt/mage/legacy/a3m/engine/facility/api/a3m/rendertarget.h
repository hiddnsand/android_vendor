/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * RenderTarget class
 *
 */

#pragma once
#ifndef A3M_RENDERTARGET_H
#define A3M_RENDERTARGET_H

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/texture2d.h>     /* for Texture2D                             */

/******************************************************************************
 * A3M Namespace
 ******************************************************************************/
namespace a3m
{
  /** \defgroup a3mRentarget Render Target
   * \ingroup  a3mRefScene
   * A texture to render to.
   *
   * @{
   */

  /**
   * A render target object contains a texture which may be rendered to and then
   * used as a normal texture. A RenderTarget object is constructed with a
   * Texture2D to be used as the colour buffer and parameters to specify whether
   * a depth buffer and/or stencil buffer is also required. Note that whilst it
   * is also possible to specify a texture to use as the depth buffer, not all
   * devices support this functionality (the GL_OES_depth_texture extension must
   * be present).
   *
   * After a RenderTarget has been constructed you may direct future draw
   * operations to it by calling the enable() method. You will typically want to
   * set the view port with RenderDevice::setViewport() so that screen space
   * coordinates are correctly mapped to the RenderTarget. Call disable() when
   * you have finished drawing to the render target and getColourTexture() to
   * access the texture. You will usually bind the texture to a ShaderUniform.
   *
   * \code
   * TextureParameters params(
   *   TextureParameters::NEAREST,
   *   TextureParameters::NEAREST,
   *   TextureParameters::CLAMP,
   *   TextureParameters::CLAMP );
   *
   * Texture2D::Ptr depthTexture( new Texture2D( SHADOW_MAP_SIZE,
   *   SHADOW_MAP_SIZE, 0, a3m::Texture::RGBA, a3m::Texture::UNSIGNED_BYTE,
   *   params ) );
   *
   * RenderTarget::Ptr renderTarget( new RenderTarget( depthTexture,
   *                                                   Texture2D::Ptr() ) );
   *
   * renderTarget->enable();
   *
   * RenderDevice::setViewport( 0, 0, SHADOW_MAP_SIZE, SHADOW_MAP_SIZE );
   *
   * Background bg;
   * bg.setColour( 1.0, 1.0, 1.0, 1.0 );
   * RenderDevice::clear(bg);
   *
   * // More draw code would go here.
   *
   * renderTarget->disable();
   *
   * // Now use the render target texture whilst drawing on the device
   * appearance.setUniformValue( "u_l_shadowMap",
   *                             renderTarget->getColourTexture() );
   * \endcode
   */
  class RenderTarget : public Shared, NonCopyable
  {
  public:
    A3M_NAME_SHARED_CLASS( RenderTarget )

    /** Smart pointer type for this class */
    typedef SharedPtr< RenderTarget > Ptr;

    /**
     * Constructor
     */
    RenderTarget(
      /** colour texture target */
      Texture2D::Ptr const& colourTexture,
      /** depth texture target */
      Texture2D::Ptr const& depthTexture,
      /** Whether use depth buffer if there's no depth texture */
      A3M_BOOL depthBuffer = A3M_TRUE,
      /** Whether to use stencil buffer */
      A3M_BOOL stencilBuffer = A3M_FALSE );

    /**
     * Destructor
     */
    ~RenderTarget();

    /**
     * Colour texture access
     * \return shared pointer to colour texture
     */
    Texture2D::Ptr const& getColourTexture() const { return m_colourTexture; }

    /**
     * Depth texture access
     * \return shared pointer to depth texture
     */
    Texture2D::Ptr const& getDepthTexture() const { return m_depthTexture; }

    /**
     * Set new colour texture
     */
    void setColourTexture( /** new colour texture */
      Texture2D::Ptr const& texture );

    /**
     * Set new depth texture
     */
    void setDepthTexture( /** new depth texture */
      Texture2D::Ptr const& texture );

    /** Enable this render target.
     * Make this the current render target. Subsequently drawn primitives will
     * be rendered to this target until disable is called.
     */
    void enable();

    /** Disable this render target.
     * Stop using this render target. Subsequently drawn primitives will be
     * rendered to the device buffer.
     */
    void disable() const;

    /** Validate the render target
     * \return A3M_TRUE if the render target is valid and complete
     */
    A3M_BOOL isValid() { return m_valid; }

  private:
    A3M_UINT32 m_framebuffer;
    Texture2D::Ptr m_colourTexture;
    Texture2D::Ptr m_depthTexture;
    A3M_UINT32 m_depthRenderbuffer;
    A3M_UINT32 m_stencilRenderbuffer;
    A3M_BOOL m_valid;
  };

  /** @} */
} // End of namespace a3m

#endif // A3M_RENDERTARGET_H
