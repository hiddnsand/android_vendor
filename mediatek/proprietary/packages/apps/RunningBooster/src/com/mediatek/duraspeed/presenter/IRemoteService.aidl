// IRemoteService.aidl
package com.mediatek.duraspeed.presenter;

// Declare any non-default types here with import statements
import com.mediatek.duraspeed.presenter.ICallback;

interface IRemoteService {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    boolean registerCallback(ICallback callback);
    boolean unregisterCallback(ICallback callback);
}
