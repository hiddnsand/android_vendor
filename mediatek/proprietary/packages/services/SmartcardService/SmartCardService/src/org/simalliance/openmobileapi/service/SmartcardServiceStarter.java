package org.simalliance.openmobileapi.service;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
/// M: Add process name checking @{
import android.os.Process;
import android.os.SystemProperties;
import android.os.UserHandle;
/// M: Add process name checking @}
import android.util.Log;

import java.util.List;

public class SmartcardServiceStarter extends Application {

    public final static String TAG = "SmartcardServiceStarter";

    private boolean sStart = false;

    /// M: Do not use main thread too long @{
    private final String MAIN_PROCESS = "org.simalliance.openmobileapi.service:remote";
    /// M: Do not use main thread too long @}

    @Override
    public void onCreate() {
        /// M: Add process name checking @{
        boolean isMainProcess = false;
        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<RunningAppProcessInfo> processes = am.getRunningAppProcesses();
        if (processes == null) {
            Log.v(TAG, "Cannot get RunningAppProcessInfo list");
            // ingore process check
            isMainProcess = true;
        } else {
            for (RunningAppProcessInfo info : processes) {
                if (info.pid == Process.myPid()) {
                    isMainProcess = info.processName.equals(MAIN_PROCESS);
                    break;
                }
            }
        }

        if (sStart == false && UserHandle.myUserId() == 0 && isMainProcess == true) {
            final String off = SystemProperties.get("persist.radio.seapi.off", "0");

            Log.v(TAG, "Starting smartcard service when process onCreate, off=" + off);
            if (off.equals("0")) {
                Intent serviceIntent = new Intent(this,
                    org.simalliance.openmobileapi.service.SmartcardService.class);
                startService(serviceIntent);
            }
            sStart = true;
        }
        /// M: Add process name checking @}
    }
}
