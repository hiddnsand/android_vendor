/* Copyright Statement:
*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein
* is confidential and proprietary to MediaTek Inc. and/or its licensors.
* Without the prior written permission of MediaTek inc. and/or its licensors,
* any reproduction, modification, use or disclosure of MediaTek Software,
* and information contained herein, in whole or in part, shall be strictly prohibited.
*/
/* MediaTek Inc. (C) 2017. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
* AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
* NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
* SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
* SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
* THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
* THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
* CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
* SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
* CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
* AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
* OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
* MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
* The following software/firmware and/or related documentation ("MediaTek Software")
* have been modified by MediaTek Inc. All revisions are subject to any receiver\'s
* applicable license agreements with MediaTek Inc.
*/

package com.mediatek.op09a.telephony;

import android.content.Context;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telephony.Rlog;
import android.telephony.SubscriptionManager;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.RILConstants;

import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.ServiceStateTrackerExt;

public class Op09AServiceStateTrackerExt extends ServiceStateTrackerExt{

    private static final String TAG = "Op09AServiceStateTrackerExt";

    @Override
    public boolean needSpnRuleShowPlmnOnly() {
        return true;
    }

    @Override
    public boolean allowSpnDisplayed() {
        return false;
    }

    @Override
    public int needAutoSwitchRatMode(int phoneId,String nwPlmn){
        int currentNetworkMode = -1;

        //get sim switch status
        int switchStatus = Integer.valueOf(
                SystemProperties.get(MtkPhoneConstants.PROPERTY_CAPABILITY_SWITCH, "1"));

        Rlog.d(TAG, "needSwitchRatMode: phoneId=" + phoneId+", switchStatus="+switchStatus+
                ",SubscriptionManager.isValidPhoneId(phoneId)="+
            SubscriptionManager.isValidPhoneId(phoneId));

        if (SubscriptionManager.isValidPhoneId(phoneId)) {
            if (phoneId == (switchStatus - 1)) {
                return currentNetworkMode;
            } else {
                //for L+W DSDS project, If not major SIM then need set to GSM only
                return Phone.NT_MODE_GSM_ONLY;
            }
        }
        return currentNetworkMode;
    }

    /**
     * For Mota upgrade, exchange the oldtype(android L) to new type(android M).
     *
     * @param slotId the slotId zero or one
     * @return the new networkType for android M
     */
    @Override
    public int getNetworkTypeForMota(int slotId) {
        int networkType = -1;

        if (slotId == 0) {
            int oldType = Settings.Global.getInt(mContext.getContentResolver(),
                           Settings.Global.LTE_ON_CDMA_RAT_MODE, -1);
            if (oldType != -1) {
                if (oldType == 0) { // 4G
                    networkType = RILConstants.NETWORK_MODE_LTE_CDMA_EVDO_GSM_WCDMA;
                } else if (oldType == 1) { // 3G
                    networkType = RILConstants.NETWORK_MODE_GLOBAL;
                }
                // After get the old rat, step1: set the old type as -1
                Settings.Global.putInt(mContext.getContentResolver(),
                    Settings.Global.LTE_ON_CDMA_RAT_MODE, -1);
                // step2: set the new type as the old type
                if (networkType != -1) {
                    int subId = SubscriptionManager.DEFAULT_SUBSCRIPTION_ID;
                    int[] subIds = SubscriptionManager.getSubId(slotId);
                    if (subIds != null) {
                        subId = subIds[0];
                    }

                    Settings.Global.putInt(mContext.getContentResolver(),
                       android.provider.Settings.Global.PREFERRED_NETWORK_MODE + subId,
                       networkType);
                    Rlog.d(TAG, "[setDeviceRatMode], set new type, subId is: " + subId);
                }
            }
            Rlog.d(TAG, "[setDeviceRatMode], getNetworkTypeForMota, oldType is: "
                + oldType + ", networkType is: " + networkType);
        }
        return networkType;
    }
}
