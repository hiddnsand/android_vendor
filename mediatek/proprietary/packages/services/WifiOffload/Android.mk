LOCAL_PATH:= $(call my-dir)

# interface lib
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, lib/src)

LOCAL_AIDL_INCLUDES := $(LOCAL_PATH)/lib/src

# aidl
LOCAL_SRC_FILES += \
    lib/src/com/mediatek/wfo/IWifiOffloadService.aidl \
    lib/src/com/mediatek/wfo/IWifiOffloadListener.aidl \
    lib/src/com/mediatek/wfo/IMwiService.aidl

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := wfo-common
LOCAL_PROPRIETARY_MODULE := false

include $(BUILD_STATIC_JAVA_LIBRARY)

# apk
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_JAVA_LIBRARIES += telephony-common ims-common mediatek-telephony-common
LOCAL_JAVA_LIBRARIES += mediatek-common

# Add for Plug-in, include the plug-in framework
LOCAL_JAVA_LIBRARIES += mediatek-framework
LOCAL_JAVA_LIBRARIES += mediatek-telephony-base

LOCAL_STATIC_JAVA_LIBRARIES += wfo-common
LOCAL_STATIC_JAVA_LIBRARIES += vendor.mediatek.hardware.wfo-V1.0-java-static
LOCAL_STATIC_JAVA_LIBRARIES += com.mediatek.wfo.op

LOCAL_MODULE_TAGS := optional
LOCAL_PACKAGE_NAME := WfoService
LOCAL_PROPRIETARY_MODULE := false
LOCAL_MODULE_OWNER := mtk
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true

LOCAL_PROGUARD_ENABLED := disabled
LOCAL_PROGUARD_FLAGS := $(proguard.flags)

include $(BUILD_PACKAGE)

# HIDL & operator extension
include $(call all-makefiles-under,$(LOCAL_PATH))
