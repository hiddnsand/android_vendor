/*
 * Copyright (C) 2017 MediaTek Inc.
 *
 * Modification based on code covered by the mentioned copyright
 * and/or permission notice(s).
 */
/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mediatek.net.connectivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.INetdEventCallback;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.metrics.INetdEventListener;
import android.os.Binder;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.util.Log;
import android.util.SparseBooleanArray;

/**
 * Implement Medaitek IpConnectivityMetrics service.
 * {@hide}
 *
*/
final public class MtkIpConnectivityMetrics {
    private static final String TAG = MtkIpConnectivityMetrics.class.getSimpleName();
    private static final boolean DBG = true;

    private static final String SERVICE_NAME = "MtkIpConnectivityMetrics";

    private static final boolean FEATURE_SUPPORTED =
            SystemProperties.getInt("ro.mtk_mobile_management", 0) == 1;

    /* The implementation of IpConnectivityMetrics */
    public Impl mImpl;

    private Context mContext;

    /* Construction function of MtkIpConnectivityMetrics. */
    public MtkIpConnectivityMetrics(Context ctx) {
        Log.d(TAG, "MtkIpConnectivityMetrics is created:" + FEATURE_SUPPORTED);
        mContext = ctx;
        if (FEATURE_SUPPORTED) {
            mImpl = new Impl(mContext);
        }
    }

    /* Utility callback function to inject with the AOSP service */
    public INetdEventListener getNetdEventListener() {
        if (FEATURE_SUPPORTED) {
            return mNetdEventListener;
        }
        return null;
    }

    public IBinder getMtkIpConnSrv() {
        return (IBinder) mImpl;
    }

    private final INetdEventListener mNetdEventListener = new INetdEventListener.Stub() {

        @Override
        // Called concurrently by multiple binder threads.
        // This method must not block or perform long-running operations.
        public synchronized void onDnsEvent(int netId, int eventType, int returnCode, int latencyMs,
            String hostname, String[] ipAddresses, int ipAddressesCount, int uid)
            throws RemoteException {
            mImpl.onDnsEvent(netId, uid);
        }

        @Override
        public synchronized void onConnectEvent(int netId, int error, int latencyMs, String ipAddr,
            int port, int uid) throws RemoteException {
            mImpl.onConnectEvent(netId, uid);
        }

        @Override
        public synchronized void onWakeupEvent(String prefix, int uid, int gid, long timestampNs) {
        }
    };


    public final class Impl extends IMtkIpConnectivityMetrics.Stub {
        private Context mContext;
        private ConnectivityManager mCm;
        private INetdEventCallback mNetdEventCallback;
        private SparseBooleanArray mUidSocketRules =
                    new SparseBooleanArray();
        final Object mUidSockeRulestLock = new Object();

        /* Construction function. */
        public Impl(Context ctx) {
            mContext = ctx;
            mCm = mContext.getSystemService(ConnectivityManager.class);
        }

        @Override
        public boolean registerMtkNetdEventCallback(INetdEventCallback callback) {
            // Check package name
            if (isPermissionAllowed()) {
                Log.d(TAG, "registerMtkNetdEventCallback");
                mNetdEventCallback = callback;
                return true;
            }
            return false;
        }

        @Override
        public boolean unregisterMtkNetdEventCallback() {
            if (isPermissionAllowed()) {
                Log.d(TAG, "unregisterMtkNetdEventCallback");
                mNetdEventCallback = null;
                return true;
            }
            return false;
        }

        @Override
        public void updateCtaAppStatus(int uid, boolean isNotified) {
            if (isPermissionAllowed()) {
                if (uid < Process.FIRST_APPLICATION_UID) {
                    return;
                }
                synchronized (mUidSockeRulestLock) {
                    Log.d(TAG, "updateCtaAppStatus:" + uid + ":" + isNotified);
                    mUidSocketRules.put(uid, isNotified);
                }
            }
        }

        private void onDnsEvent(int netId, int uid) {
            if (mNetdEventCallback == null) return;
            if (uid < android.os.Process.FIRST_APPLICATION_UID) {
                return;
            }
            boolean isNotified = true;
            synchronized (mUidSockeRulestLock) {
                isNotified = mUidSocketRules.get(uid, true);
                if (DBG) Log.d(TAG, "onDnsEvent:" + uid + ":" + isNotified);
                if (isNotified) {
                    NetworkCapabilities nc = mCm.getNetworkCapabilities(new Network(netId));
                    if (nc == null) {
                        return;
                    }
                    if (nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        try {
                            mNetdEventCallback.onDnsEvent("", null, 0, 0L, uid);
                        } catch (Exception e) {
                        }
                    }
                }
            }
        }

        private void onConnectEvent(int netId, int uid) {
            if (mNetdEventCallback == null) return;
            if (uid < android.os.Process.FIRST_APPLICATION_UID) {
                return;
            }
            boolean isNotified = true;
            synchronized (mUidSockeRulestLock) {
                isNotified = mUidSocketRules.get(uid, true);
                if (DBG) Log.d(TAG, "onDnsEvent:" + uid + ":" + isNotified);
                if (isNotified) {
                    NetworkCapabilities nc = mCm.getNetworkCapabilities(new Network(netId));
                    if (nc == null) {
                        return;
                    }
                    if (nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        try {
                            mNetdEventCallback.onConnectEvent("", 0, 0L, uid);
                        } catch (Exception e) {
                        }
                    }
                }
            }
        }

        private boolean isPermissionAllowed() {
            if (Binder.getCallingUid() != Process.SYSTEM_UID) {
                Log.d(TAG, "No permission:" + Binder.getCallingUid());
                return false;
            }
            return true;
        }
    }
}
