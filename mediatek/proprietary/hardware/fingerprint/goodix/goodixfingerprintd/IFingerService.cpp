/*
 * Copyright (C) 2013-2016, Shenzhen Huiding Technology Co., Ltd.
 * All Rights Reserved.
 */

#include <inttypes.h>

#include <binder/IPCThreadState.h>
#include <binder/IServiceManager.h>
#include <utils/String16.h>
#include <utils/Looper.h>
#include <hardware/hardware.h>
#include <utils/Log.h>
#include "IFingerService.h"

namespace android {

const String16 FINGER_DESCRIPTOR("android.hardware.fingerprint.IFingerprintService");
sp<IBinder> gToken = new BBinder();

class BpFingerService: public android::BpInterface<IFingerService>
{
    enum
    {
        FIRST_CALL_TRANSACTION = 1,
        TRANSACTION_remove = (android::IBinder::FIRST_CALL_TRANSACTION + 4),
        TRANSACTION_getEnrolledFingerprints = (android::IBinder::FIRST_CALL_TRANSACTION + 6)
    };
public:
    BpFingerService(const android::sp<android::IBinder>& impl): android::BpInterface<IFingerService>(impl)
        {
        }
    int32_t getEnrolledFingerprints(uint32_t groupId, uint32_t** list) {
        if (NULL == list) {
            return 0;
        }
        Parcel data, reply;
        data.writeInterfaceToken(FINGER_DESCRIPTOR);
        data.writeInt32(groupId);
        data.writeString16(String16("system"));
        remote()->transact(TRANSACTION_getEnrolledFingerprints, data, &reply, 0);

        uint32_t code = reply.readExceptionCode();
        if (0 != code)
        {
            return -1;
        }

        int32_t count = reply.readInt32();
        if (count <= 0) {
            return 0;
        }
        *list = (uint32_t*)malloc(count * sizeof(uint32_t));
        if (NULL == *list) {
            return -1;
        }
        memset(*list, 0, count * sizeof(uint32_t));
        for (int i = 0; i < count; i++) {
            if (reply.readInt32() == 1) {
                reply.readString16();
                reply.readInt32();
                (*list)[i] = reply.readInt32();
                reply.readInt64();
            }
        }
        ALOGV("count = %d", count);
        return count;
    }

    int32_t remove(uint32_t groupId, uint32_t fingerId) {
        Parcel data, reply;
        data.writeInterfaceToken(FINGER_DESCRIPTOR);
        data.writeStrongBinder(gToken);
        data.writeInt32(fingerId);
        data.writeInt32(groupId);
        data.writeInt32(0);
        remote()->transact(TRANSACTION_remove, data, &reply);

        uint32_t code = reply.readExceptionCode();
        return code;
    }

};
IMPLEMENT_META_INTERFACE(FingerService, FINGER_DESCRIPTOR)
}; // namespace android
