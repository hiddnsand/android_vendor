package com.mediatek.bluetooth.dtt;

/**
 * Created by MTK01635 on 2015/4/23.
 */
public class Const {

    // log tag
    public static final String TAG = "BT.DTT";

    // global preference name
    protected static final String PREF_NAME = "btdtt.conf";

    public static final int STATE_INIT = 0;
    public static final int STATE_READY = 1;
    public static final int STATE_BUSY = 2;

    // Test Situation
    public static final String TS_DEFAULT = "default";
    public static final String TS_SQC = "sqc";
    public static final String TS_DEBUG = "debug";
    public static final String TS_CUST = "cust";
    public static final String TS_USERTRIAL = "usertrial";

    // default file & log file and directory
    public static final String SHARE_FILE_DIR = "/sdcard/";
    public static final String DEFAULT_CONFIG_DIR = "/etc/bluetooth/";
    public static final String DEFAULT_STACK_CONF = "bt_stack.conf";

    public static final String FW_LOG_CONF_PROPERTY = "persist.bluetooth.fwloglevel";
    public static final String HOST_LOG_CONF_PROPERTY = "persist.bluetooth.hostloglevel";
    public static final String LOG_TOO_MUCH_PROPERTY = "persist.logmuch.detect";

    public static final String IS_ENABLE = "IS_Enabled";
}
