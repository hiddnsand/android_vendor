/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 *  Renderer class
 */
#pragma once
#ifndef A3M_RENDERER_H
#define A3M_RENDERER_H

#include <a3m/base_types.h>    /* included for A3M_FLOAT */
#include <a3m/pointer.h>       /* for SharedPtr          */
#include <a3m/rendercontext.h> /* for RenderContext      */
#include <a3m/vector4.h>       /* for Vector4f           */

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  /** \defgroup a3mRenderer A3M Renderer
   * \ingroup  a3mRefRender
   *
   * Abstract base class for renderers.
   *
   * @{
   */

  class SceneNode;
  class Solid;
  class Camera;
  class Light;
  class FlagMask;

  /**
   * Renderer class
   *
   */
  class Renderer : public Shared
  {
  public:
    /** Smart pointer type for this class */
    typedef SharedPtr< Renderer > Ptr;
    /**
     * Virtual destructor so that subclasses are destroyed properly.
     */
    virtual ~Renderer() {}

    /**
     * Render the scene graph starting at the given node.
     *
     * A null pointer may be passed as the camera parameter. In this case
     * the identity matrix will be used for the camera transforms (view and
     * projection). This is generally useful only for post-processing stages
     * where the only thing being rendered is a single, untransformed quad.
     */
    virtual void render(
      Camera* camera,  /**< Camera used to render the scene */
      SceneNode& node, /**< Root of the rendered scene graph */
      FlagMask const& renderFlags, /**< Mask for specifying which scene nodes
                                          to include in render */
      FlagMask const& recursiveFlags /**< Mask specifying whether flags affect
                                            the scene graph recursively when in
                                            their non-default state */ ) = 0;

    /**
     * Update the time for shader based effects.
     */
    virtual void update( A3M_FLOAT timeInSeconds
                         /**< Time (mod 60) to set in uniform u_time */ ) {}

    virtual RenderContext::Ptr getRenderContext() const = 0;
  };
  /** @} */

} /* namespace a3m */

#endif /* A3M_RENDERER_H */
