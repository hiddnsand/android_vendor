/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 */

#ifndef _MTK_PROGRAM_CACHE_H
#define _MTK_PROGRAM_CACHE_H

#include "Extensions.h"
#include "ProgramCache.h"

namespace android {
namespace uirenderer {

///////////////////////////////////////////////////////////////////////////////
// Cache
///////////////////////////////////////////////////////////////////////////////

/// M: [ProgramBinaryAtlas] For using program atlas.
class IProgramAtlas;

/**
 * Generates and caches program. Programs are generated based on
 * ProgramDescriptions.
 */
class MTKProgramCache : public ProgramCache {
public:
    /// M: for ProgramBinaryAtlas @{
    MTKProgramCache(const Extensions& extensions, bool autoDeleteAtlas = true);
    ~MTKProgramCache();
    /// @}

    /**
     * M: [ProgramBinaryAtlas] Create program and its mapping table, return the total memory size
     * for caching programs binaries, and update the correct mapLength.
     *
     * The mapping will be ProgramKey, Offset, Length, "ProgramId", ProgramKey, Offset...
     */
    int createPrograms(int64_t* map, int* mapLength);

    /**
     * M: [ProgramBinaryAtlas] Load program binaries to the buffer, delete programs, and update the map
     *
     * The mapping will be ProgramKey, Offset, Length, "Format", ProgramKey, Offset...
     */
    static void loadProgramBinariesAndDelete(int64_t* map, int mapLength, void* buffer, int length);

    /**
     * M: Initialize program cache.
     */
    void init();

    /**
     * M: save program keys to disk, keys are used to preload programs when initializing
     */
    void saveToDisk();

private:
    Program* generateProgram(const ProgramDescription& description, programid key);

    const bool mAutoDeleteAtlas;

    /// M: [ProgramBinaryAtlas] Program atlas for caching program binaries.
    IProgramAtlas* programAtlas;
}; // class MTKProgramCache

}; // namespace uirenderer
}; // namespace android

#endif // _MTK_PROGRAM_CACHE_H
