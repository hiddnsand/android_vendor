/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <sys/types.h>
#include <sys/stat.h>

#include "cam_fdvt.h"

#include "pic/fdvt_in_frame01_400x300.h"
#include "pic/fdvt_in2_frame01_400x300.h"
#include "pic/fdvt_in_frame01_320x240.h"
#include "pic/fdvt_in2_frame01_320x240.h"
#include "pic/fdvt_in_frame01_640x480.h"
#include "pic/fdvt_in2_frame01_640x480.h"

#include <imem_drv.h>
#include <time.h>

#define ABS(a)		((a < 0) ? -a : a )
#define IMAGE_SRC_FROM_PHONE 0
#define IMAGE_ONE_PLANE      0
IMemDrv *ImemDrv = NULL;
IMEM_BUF_INFO Imem_Buffer_Y;
IMEM_BUF_INFO Imem_Buffer_UV;
IMEM_BUF_INFO Imem_Buffer_Info;
MUINT8	*Imem_pLogVir_Y=NULL, *Imem_pLogVir_UV=NULL;
MINT32	 Imem_MemID_Y, Imem_MemID_UV;
MUINT32  Imem_Size_Y, Imem_Size_UV;
MUINT32  Imem_Alloc_Num = 0;

MINT32 Imem_alloc(MUINT32 size,MINT32 *memId,MUINTPTR *vAddr,MUINTPTR *pAddr)
{
    if ( NULL == ImemDrv ) {        
        ImemDrv = IMemDrv::createInstance();
        ImemDrv->init();
    }
    //
    Imem_Buffer_Info.size = size;
    //Imem_Buffer_Info.useNoncache = 1;
    ImemDrv->allocVirtBuf(&Imem_Buffer_Info);
    *memId = Imem_Buffer_Info.memID;
    *vAddr = (MUINTPTR)Imem_Buffer_Info.virtAddr;
    //
    ImemDrv->mapPhyAddr(&Imem_Buffer_Info);
    *pAddr = (MUINTPTR)Imem_Buffer_Info.phyAddr;
    Imem_Alloc_Num ++;

    printf("Imem_Alloc_Num(%d)\n",Imem_Alloc_Num);
    printf("vAddr(0x%p) pAddr(0x%p) Imem_Alloc_Num(%d)\n",*vAddr,*pAddr,Imem_Alloc_Num);

    return 0;
}


MINT32 Imem_free(MUINT8 *vAddr, MUINTPTR phyAddr,MUINT32 size,MINT32 memId)
{
    Imem_Buffer_Info.size = size;
    Imem_Buffer_Info.memID = memId;
    Imem_Buffer_Info.virtAddr = (MUINTPTR)vAddr;
    Imem_Buffer_Info.phyAddr = (MUINTPTR)phyAddr;
    //

    if(ImemDrv)
    {
        ImemDrv->unmapPhyAddr(&Imem_Buffer_Info);
        //
        ImemDrv->freeVirtBuf(&Imem_Buffer_Info);

        if(Imem_Alloc_Num) 
        {
            Imem_Alloc_Num--;
            if(Imem_Alloc_Num==0)
            {
                ImemDrv->uninit();
                ImemDrv = NULL;
            }
        }
    }
    else
    {
        printf("Warning! unmapPhyAddr Fail!");
    }

    printf("Imem_Alloc_Num(%d)\n",Imem_Alloc_Num);
    
    return 0;
}

void Allocate_TestPattern_Buffer(int subcase)
{
    #if IMAGE_SRC_FROM_PHONE
    Imem_Size_Y = 640*480*2;
    #else
    Imem_Size_Y = sizeof(fdvt_in_frame01_640x480); // 16-byte alignment
    #endif
    Imem_Size_UV = sizeof(fdvt_in2_frame01_640x480); // 16-byte alignment
    //Imem_Buffer_Y.useNoncache = 1;
    //Imem_Buffer_UV.useNoncache = 1;

    Imem_alloc(Imem_Size_Y, &Imem_MemID_Y, &Imem_Buffer_Y.virtAddr, &Imem_Buffer_Y.phyAddr);
    Imem_alloc(Imem_Size_UV, &Imem_MemID_UV, &Imem_Buffer_UV.virtAddr, &Imem_Buffer_UV.phyAddr);
    //Imem_Buffer_Y.virtAddr=(MUINTPTR)Imem_pLogVir_Y;
    //Imem_Buffer_UV.virtAddr=(MUINTPTR)Imem_pLogVir_UV;
    Imem_Buffer_Y.memID=Imem_MemID_Y;
    Imem_Buffer_UV.memID=Imem_MemID_UV;
    Imem_Buffer_Y.size=Imem_Size_Y;
    Imem_Buffer_UV.size=Imem_Size_UV;

    #if IMAGE_SRC_FROM_PHONE
    {
        FILE* fd1;
        fd1 = fopen("/sdcard/test.raw", "r+b");
        fread((void *)Imem_Buffer_Y.virtAddr, 1, 640*360*2, fd1);
        fclose(fd1);
    }
    #else
    memcpy( (MUINT8*)(Imem_Buffer_Y.virtAddr), (MUINT8*)(fdvt_in_frame01_640x480), Imem_Size_Y);
    #endif

    memcpy( (MUINT8*)(Imem_Buffer_UV.virtAddr), (MUINT8*)(fdvt_in2_frame01_640x480), Imem_Size_UV);

    printf("Imem_Buffer_Y PA/VA: 0x%016p/0x%016p\n", Imem_Buffer_Y.phyAddr, Imem_Buffer_Y.virtAddr);  
}

void Free_TestPattern_Buffer()
{
    Imem_free((MUINT8 *)Imem_Buffer_Y.virtAddr, Imem_Buffer_Y.phyAddr, Imem_Buffer_Y.size, Imem_Buffer_Y.memID);
    Imem_free((MUINT8 *)Imem_Buffer_UV.virtAddr, Imem_Buffer_UV.phyAddr, Imem_Buffer_UV.size, Imem_Buffer_UV.memID);
}

static bool saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)
{
    int nw, cnt = 0;
    uint32_t written = 0;

    //LOG_INF("(name, buf, size) = (%s, %x, %d)", fname, buf, size);
    //LOG_INF("opening file [%s]\n", fname);
    int fd = ::open(fname, O_RDWR | O_CREAT, S_IRWXU);
    if (fd < 0) {
        printf(": failed to create file [%s]: %s \n", fname, ::strerror(errno));
        return false;
    }

    //LOG_INF("writing %d bytes to file [%s]\n", size, fname);
    while (written < size) {
        nw = ::write(fd,
                     buf + written,
                     size - written);
        if (nw < 0) {
            printf(": failed to write to file [%s]: %s\n", fname, ::strerror(errno));
            break;
        }
        written += nw;
        cnt++;
    }
    //LOG_INF("done writing %d bytes to file [%s] in %d passes\n", size, fname, cnt);
    ::close(fd);
    return true;
}

static bool appendBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)
{
    int nw, cnt = 0;
    uint32_t written = 0;

    //LOG_INF("(name, buf, size) = (%s, %x, %d)", fname, buf, size);
    //LOG_INF("opening file [%s]\n", fname);
    int fd = ::open(fname, O_RDWR | O_APPEND , S_IRWXU);
    if (fd < 0) {
        printf(": failed to create file [%s]: %s \n", fname, ::strerror(errno));
        return false;
    }

    //LOG_INF("writing %d bytes to file [%s]\n", size, fname);
    while (written < size) {
        nw = ::write(fd,
                     buf + written,
                     size - written);
        if (nw < 0) {
            printf(": failed to write to file [%s]: %s\n", fname, ::strerror(errno));
            break;
        }
        written += nw;
        cnt++;
    }
    //LOG_INF("done writing %d bytes to file [%s] in %d passes\n", size, fname, cnt);
    ::close(fd);
    return true;
}

void MarkFacePosition(int subcase, FdDrv_output_struct *FdDrv_output)
{
    MUINT32 loop = 0;
    MUINT32 width, height;
    MUINT32 i, j;
    //MUINT8  *markPoint = "0xFF";
    MUINT32 position_x_0;
    MUINT32 position_y_0;
    MUINT32 position_x_1;
    MUINT32 position_y_1;
    MUINT32 stride = 640;
#if IMAGE_ONE_PLANE
    stride *= 2;
#endif
    const MUINT8 markPoint[]={0xFF};

    for(loop = 0; loop < FdDrv_output->new_face_number; loop ++)
    {
        position_x_0 = FdDrv_output->face_candi_pos_x0[loop];
        position_y_0 = FdDrv_output->face_candi_pos_y0[loop];
        position_x_1 = FdDrv_output->face_candi_pos_x1[loop];
        position_y_1 = FdDrv_output->face_candi_pos_y1[loop];

        width = ABS(position_x_1 - position_x_0);
        height = ABS(position_y_1 - position_y_0);

        printf("output[%3d]: x_0: %3d, y_0: %3d, x_1: %3d, y_1: %3d\n", loop, position_x_0, position_y_0, position_x_1, position_y_1);

        #if IMAGE_ONE_PLANE
        for (i = 0; i < width; i ++)
        {
            for (j = 0; j < height; j ++)
            {
                if(i == 0 || i == (width-1) || j == 0 || j == (height-1))
                {
                    memcpy( (MUINT8*)(Imem_Buffer_Y.virtAddr + stride * (position_y_0 + j) + (position_x_0 + i)*2), (MUINT8*)(markPoint), sizeof(markPoint));
                }
            }
        }
        #else
        for (i = 0; i < width; i ++)
        {
            for (j = 0; j < height; j ++)
            {
                if(i == 0 || i == (width-1) || j == 0 || j == (height-1))
                {
                    memcpy( (MUINT8*)(Imem_Buffer_Y.virtAddr + stride * (position_y_0 + j) + (position_x_0 + i)), (MUINT8*)(markPoint), sizeof(markPoint));
                }
            }
        }
        #endif
    }

    char filename[256];
    if(subcase == 0)
    {
        sprintf(filename, "/data/FD_RESULT_RAW%dx%d.raw", 640, 480);
    }
    else if(subcase == 1)
    {
        sprintf(filename, "/data/FD_RESULT_RAW%dx%d.raw", 400, 300);
    }
    else if(subcase == 2)
    {
        sprintf(filename, "/data/FD_RESULT_RAW%dx%d.raw", 320, 240);
    }

    saveBufToFile(filename, reinterpret_cast<MUINT8*>(Imem_Buffer_Y.virtAddr), Imem_Size_Y);
    //appendBufToFile(filename, reinterpret_cast<MUINT8*>(Imem_Buffer_UV.virtAddr), Imem_Size_UV);
    
}

int test_fd(int subcase_index,int para1=0, int para2=0, int para3=0)
{
#if 0
    printf("getchar()");
    getchar();
#endif
        clock_t start_tick, end_tick;
        double elapsed;

        printf("=====> start of test_fd (subcase_index: %d  para1: %d  para2: %d  para3: %d)\n", subcase_index, para1, para2, para3);

        FdDrv_input_struct *FdDrv_input = (FdDrv_input_struct *)malloc(sizeof(FdDrv_input_struct));
        FdDrv_output_struct *FdDrv_output = (FdDrv_output_struct *)malloc(sizeof(FdDrv_output_struct));    
    	int ret = 0;
        if(subcase_index == 0)
        {
            FdDrv_input->fd_mode = 0;
            //MUINT16 source_img_width[FD_SCALE_NUM] = {640, 640, 512, 410, 328, 262, 210, 168, 134, 108, 86, 70, 56, 46, 38};
            //MUINT16 source_img_height[FD_SCALE_NUM] = {480, 480, 384, 307, 245, 196, 157, 125, 100, 80, 64, 52, 41, 33, 27};

            MUINT16 source_img_width[FD_SCALE_NUM] = {640, 640, 510, 400, 320, 260, 210, 170, 140, 100, 80, 70, 50, 44, 36};
            MUINT16 source_img_height[FD_SCALE_NUM] = {480, 480, 380, 300, 240, 200, 150, 125, 100, 80, 60, 50, 40, 32, 26};

            memcpy(&FdDrv_input->source_img_width, &source_img_width, sizeof(FdDrv_input->source_img_width));
            memcpy(&FdDrv_input->source_img_height, &source_img_height, sizeof(FdDrv_input->source_img_height));
        }
        else if(subcase_index == 1)
        {
            FdDrv_input->fd_mode = 1;
            MUINT16 source_img_width[FD_SCALE_NUM] = {640, 400, 328, 262, 210, 168, 134, 108, 86, 70, 56, 46, 38, 38, 38};
            MUINT16 source_img_height[FD_SCALE_NUM] = {480, 300, 245, 196, 157, 125, 100, 80, 64, 52, 41, 33, 27, 27, 27};
            memcpy(&FdDrv_input->source_img_width, &source_img_width, sizeof(FdDrv_input->source_img_width));
            memcpy(&FdDrv_input->source_img_height, &source_img_height, sizeof(FdDrv_input->source_img_height));
        }
        else if(subcase_index == 2)
        {
            FdDrv_input->fd_mode = 2;
            MUINT16 source_img_width[FD_SCALE_NUM] = {640, 320, 262, 210, 168, 134, 108, 86, 70, 56, 46, 38, 38, 38, 38};
            MUINT16 source_img_height[FD_SCALE_NUM] = {480, 240, 196, 157, 125, 100, 80, 64, 52, 41, 33, 27, 27, 27, 27};
            memcpy(&FdDrv_input->source_img_width, &source_img_width, sizeof(FdDrv_input->source_img_width));
            memcpy(&FdDrv_input->source_img_height, &source_img_height, sizeof(FdDrv_input->source_img_height));
        }

        FdDrv_input->feature_threshold = 0;
        FdDrv_input->GFD_skip = 0;
        FdDrv_input->GFD_skip_V = 0;
        FdDrv_input->RIP_feature = 1;
        FdDrv_input->scale_from_original = 0;
        FdDrv_input->source_img_fmt = FMT_YUV_2P;
        FdDrv_input->scale_manual_mode = para1;
        FdDrv_input->scale_num_from_user = para2;

        Allocate_TestPattern_Buffer(subcase_index);

        FdDrv_input->source_img_address = (MUINT64*)Imem_Buffer_Y.phyAddr;
        FdDrv_input->source_img_address_UV = (MUINT64*)Imem_Buffer_UV.phyAddr;
        printf("source_img_address: 0x%016p  source_img_address_UV: 0x%016p\n", FdDrv_input->source_img_address, FdDrv_input->source_img_address_UV);

        printf("=====> Initial Driver Start\n");
        FDVT_OpenDriverWithUserCount(0);

//////////////////////////// First Time Enque/Deque //////////////////////////////////
        start_tick = clock();

        printf("=====> FDVT Enque Start\n");
        FDVT_Enque(FdDrv_input);

        end_tick = clock();
        elapsed = (double) (end_tick - start_tick) / CLOCKS_PER_SEC;
        printf("FD Enque Elapsed Time: %f\n", elapsed);

        //printf("Push enter to deque FD\n");
        //getchar();

        start_tick = clock();

        printf("=====> FDVT Deque Start\n");
        FDVT_Deque(FdDrv_output);
        //getchar();

        end_tick = clock();
        elapsed = (double) (end_tick - start_tick) / CLOCKS_PER_SEC;
        printf("FD Deque Elapsed Time: %f\n", elapsed);
//////////////////////////////////////////////////////////////


/*
///////////////////////////// Second Time Enque/Deque /////////////////////////////////
        start_tick = clock();

        printf("=====> FDVT Enque Start\n");
        FDVT_Enque(FdDrv_input);

        end_tick = clock();
        elapsed = (double) (end_tick - start_tick) / CLOCKS_PER_SEC;
        printf("FD Enque Elapsed Time: %f\n", elapsed);

        //printf("Push enter to deque FD\n");
        //getchar();

        start_tick = clock();

        printf("=====> FDVT Deque Start\n");
        FDVT_Deque(FdDrv_output);
        //getchar();

        end_tick = clock();
        elapsed = (double) (end_tick - start_tick) / CLOCKS_PER_SEC;
        printf("FD Deque Elapsed Time: %f\n", elapsed);
//////////////////////////////////////////////////////////////
*/

        printf("=====> Uninitial Driver Start\n");
        FDVT_CloseDriverWithUserCount();

        printf("Detected Face Number: %d\n", FdDrv_output->new_face_number);

        //printf("=====> Mark Position Start\n");
        //MarkFacePosition(subcase_index, FdDrv_output);

        printf("=====> Free TestPattern Buffer Start\n");
        Free_TestPattern_Buffer();

        printf("=====> end of test_fd\n");
        return ret;
}
