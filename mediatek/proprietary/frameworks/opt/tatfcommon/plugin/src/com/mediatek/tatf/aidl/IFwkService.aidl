package com.mediatek.tatf.aidl;

import android.os.Bundle;

interface IFwkService {
  boolean dataSwitch(boolean open, int subId);
  boolean isDataConnected(int subId);
  boolean openUrl(String url);
  boolean call(int phoneId, String phoneNumber);
  int getPreferredNetworkType(int phoneId);
  boolean setPreferredNetworkType(int phoneId, int networkType);
  Bundle getAvailableNetworks(int phoneId);
  int getServiceState(int phoneId);
  int getDataServiceState(int phoneId);
  int getCapabilityPhoneId();
  boolean plugInSimCard(int phoneId);
  boolean plugOutSimCard(int phoneId);
  boolean enableFdn(boolean enable, int subId);
  boolean getIccFdnEnabled(int subId);
  // TK only API.
  int moCall(int phoneId, String phoneNumber, int type);
  String sendATCommand(int subId, String cmd);
  Bundle sendCommand(String cmd, in Bundle param);
}
