
LOCAL_ACP_UNAVAILABLE := true
INTERNAL_LOCAL_CLANG_EXCEPTION_PROJECTS += vendor/mediatek/proprietary/trustzone/common/hal/source
INTERNAL_LOCAL_CLANG_EXCEPTION_PROJECTS += vendor/mediatek/proprietary/trustzone/common/hal/secure

# FIXME start
# DRSDK_DIR_INC
ifeq ($(strip $(TRUSTONIC_TEE_SUPPORT)),yes)


ifeq ($(strip $(TRUSTZONE_PROJECT_MAKEFILE)),)
# For mmm ta/drv build, manually include .mk (copy from common_config.mk)
TRUSTZONE_PROJECT_MAKEFILE := $(wildcard $(addprefix vendor/mediatek/proprietary/trustzone/custom/build/project/,common.mk $(MTK_PLATFORM_DIR).mk $(MTK_BASE_PROJECT).mk $(MTK_TARGET_PROJECT).mk))
include $(TRUSTZONE_PROJECT_MAKEFILE)
$(info MTK_TEE_GP_SUPPORT = $(MTK_TEE_GP_SUPPORT))
$(info MTK_IN_HOUSE_TEE_SUPPORT = $(MTK_IN_HOUSE_TEE_SUPPORT))
$(info TRUSTONIC_TEE_SUPPORT = $(TRUSTONIC_TEE_SUPPORT))
$(info TRUSTONIC_TEE_VERSION = $(TRUSTONIC_TEE_VERSION))
$(info MICROTRUST_TEE_SUPPORT = $(MICROTRUST_TEE_SUPPORT))
$(info MTK_GOOGLE_TRUSTY_SUPPORT = $(MTK_GOOGLE_TRUSTY_SUPPORT))
$(info MTK_SOTER_SUPPORT = $(MTK_SOTER_SUPPORT))
else
# For mmm trustzone/full build, already includes mk in common_config.mk, do nothing!
endif

ifneq ($(wildcard vendor/mediatek/proprietary/trustzone/trustonic/source/bsp/platform/$(MTK_PLATFORM_DIR)),)
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/trustzone/trustonic/source/bsp/platform/$(MTK_PLATFORM_DIR)/t-sdk/DrSdk/Out/Public
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/trustzone/trustonic/source/bsp/platform/$(MTK_PLATFORM_DIR)/t-sdk/DrSdk/Out/Public/MobiCore/inc
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/trustzone/trustonic/source/bsp/platform/$(MTK_PLATFORM_DIR)/t-sdk/TlSdk/Out/Public
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/trustzone/trustonic/source/bsp/platform/$(MTK_PLATFORM_DIR)/t-sdk/TlSdk/Out/Public/MobiCore/inc
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/trustzone/trustonic/source/bsp/platform/$(MTK_PLATFORM_DIR)/t-sdk/TlSdk/Out/Public/GPD_TEE_Internal_API
else ifneq ($(wildcard vendor/mediatek/proprietary/trustzone/trustonic/source/bsp/common/$(TRUSTONIC_TEE_VERSION)/t-sdk),)
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/trustzone/trustonic/source/bsp/common/$(TRUSTONIC_TEE_VERSION)/t-sdk/DrSdk/Out/Public
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/trustzone/trustonic/source/bsp/common/$(TRUSTONIC_TEE_VERSION)/t-sdk/DrSdk/Out/Public/MobiCore/inc
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/trustzone/trustonic/source/bsp/common/$(TRUSTONIC_TEE_VERSION)/t-sdk/TlSdk/Out/Public
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/trustzone/trustonic/source/bsp/common/$(TRUSTONIC_TEE_VERSION)/t-sdk/TlSdk/Out/Public/MobiCore/inc
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/trustzone/trustonic/source/bsp/common/$(TRUSTONIC_TEE_VERSION)/t-sdk/TlSdk/Out/Public/GPD_TEE_Internal_API
endif

# For compatibility
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/trustzone/common/hal/source/tee/common/include

# drv fwk
LOCAL_C_INCLUDES += $(TEE_ALL_MODULES.msee_fwk_drv.lib.PATH)/Locals/Code/public
LOCAL_C_INCLUDES += $(TEE_ALL_MODULES.msee_fwk_ta.lib.PATH)/Locals/Code/public

else ifeq ($(strip $(MICROTRUST_TEE_SUPPORT)),yes)
# To support mmm way of building ta/drv
ifeq ($(strip $(UT_SDK_HOME)),)
UT_SDK_HOME := vendor/mediatek/proprietary/trustzone/microtrust/source/platform/$(MTK_PLATFORM_DIR)/ut_sdk
endif

#Native CA
LOCAL_C_INCLUDES += $(UT_SDK_HOME)/api/gptee_client/include

# TA
LOCAL_C_INCLUDES += $(UT_SDK_HOME)/api/app/bta/include
LOCAL_C_INCLUDES += $(UT_SDK_HOME)/api/pf/libDRMSecDrAPI/include
LOCAL_C_INCLUDES += $(UT_SDK_HOME)/api/sys/base/include
LOCAL_C_INCLUDES += $(UT_SDK_HOME)/api/app/bta/include

# Secure Driver
LOCAL_C_INCLUDES += $(UT_SDK_HOME)/api/pf/libDrmFramework/include
LOCAL_C_INCLUDES += $(UT_SDK_HOME)/api/c/include
LOCAL_C_INCLUDES += $(UT_SDK_HOME)/api/sys/base/include
LOCAL_C_INCLUDES += $(UT_SDK_HOME)/api/sys/thread/include
LOCAL_C_INCLUDES += $(UT_SDK_HOME)/api/sys/irq/include
LOCAL_C_INCLUDES += $(UT_SDK_HOME)/api/sys/io/include

# For compatibility
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/trustzone/common/hal/source/tee/common/include

endif
# FIXME end

ifneq ($(TRUSTZONE_GCC_PREFIX),)
  LOCAL_CC := $(TRUSTZONE_GCC_PREFIX)gcc
else ifeq ($(strip $(TRUSTONIC_TEE_SUPPORT)),yes)
  LOCAL_CC := prebuilts/gcc/linux-x86/arm/gcc-arm-none-eabi-4_8-2014q3/bin/arm-none-eabi-gcc
  LOCAL_CFLAGS += -D__TRUSTONIC_TEE__=1
else ifeq ($(strip $(MICROTRUST_TEE_SUPPORT)),yes)
  LOCAL_CC := vendor/mediatek/proprietary/trustzone/microtrust/source/platform/$(MTK_PLATFORM_DIR)/ut_sdk/tools/arm-2011.03/bin/arm-none-linux-gnueabi-gcc
  LOCAL_CFLAGS += -D__MICROTRUST_TEE__=1
else ifeq ($(strip $(MTK_GOOGLE_TRUSTY_SUPPORT)),yes)
  LOCAL_CC := prebuilts/gcc/linux-x86/arm/gcc-arm-none-eabi-4_8-2014q3/bin/arm-none-eabi-gcc
  LOCAL_CFLAGS += -D__TRUSTY_TEE__=1
endif

# FIXME start
ifeq ($(MTK_TRUSTZONE_PLATFORM),)
# ARM_OPT_CC
LOCAL_CFLAGS += -DPLAT=ARMV7_A_STD
LOCAL_CFLAGS += -DARM_ARCH=ARMv7 -D__ARMv7__
LOCAL_CFLAGS += -D__ARMV7_A__
LOCAL_CFLAGS += -D__ARMV7_A_STD__
LOCAL_CFLAGS += -DARMV7_A_SHAPE=STD
#LOCAL_CFLAGS += -DTEE_MACH_TYPE_$(shell echo $(TEE_MACH_TYPE) | tr a-z A-Z)
# CPU_OPT_CC_NO_NEON
ifeq ($(strip $(TRUSTONIC_TEE_SUPPORT)),yes)
LOCAL_CFLAGS += -mcpu=generic-armv7-a
endif
LOCAL_CFLAGS += -mfpu=vfp
LOCAL_CFLAGS += -mfloat-abi=soft
endif
# CC_OPTS_BASE
LOCAL_CFLAGS += -mthumb
LOCAL_CFLAGS += -O3
LOCAL_CFLAGS += -mthumb-interwork
LOCAL_CFLAGS += -D__THUMB__
ifeq ($(TARGET_BUILD_VARIANT),eng)
LOCAL_CFLAGS += -DDEBUG --debug
endif
#LOCAL_CFLAGS += -Werror
#LOCAL_CFLAGS += -Wall
#LOCAL_CFLAGS += -Wextra
# FIXME end

LOCAL_CLANG := false

# FIXME start
# C99
LOCAL_CONLYFLAGS += -std=c99
# FIXME end

LOCAL_CXX_STL := none

LOCAL_FORCE_STATIC_EXECUTABLE := true

LOCAL_IS_HOST_MODULE :=

# FIXME start
# LINK_OPTS
LOCAL_LDFLAGS += --verbose --gc-sections --fatal-warnings --no-wchar-size-warning
# STD_LIBS
#LOCAL_LDFLAGS += -lm -lc -lgcc
# FIXME end

# .a/.lib
LOCAL_MODULE_SUFFIX := .lib

LOCAL_MULTILIB := 32

LOCAL_NO_CRT := true

LOCAL_NO_DEFAULT_COMPILER_FLAGS := true

LOCAL_NO_LIBGCC := true

LOCAL_SANITIZE := never

LOCAL_CFLAGS_32 := -fno-PIC

ifeq ($(strip $(MICROTRUST_TEE_SUPPORT)),yes)
LOCAL_CFLAGS += -fms-extensions
endif

include $(BUILD_STATIC_LIBRARY)
