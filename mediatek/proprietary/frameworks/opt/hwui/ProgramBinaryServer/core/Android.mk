LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

# Only build program binaries when USE_OPENGL_RENDERER is
# defined in the current device/board configuration
ifeq ($(USE_OPENGL_RENDERER),true)
  LOCAL_SRC_FILES := \
    ProgramBinaryClient.cpp \
    ProgramBinaryService.cpp

  intermediates := $(call intermediates-dir-for,STATIC_LIBRARIES,TARGET,)

  LOCAL_C_INCLUDES += \
    system/core/include

  LOCAL_CFLAGS := -DUSE_OPENGL_RENDERER
  LOCAL_SHARED_LIBRARIES := libutils libbinder liblog libcutils
  LOCAL_MODULE_CLASS := SHARED_LIBRARIES
  LOCAL_MODULE := libprogrambinary
LOCAL_MODULE_OWNER := mtk
  LOCAL_MODULE_TAGS := optional

  # Defaults for ATRACE_TAG and LOG_TAG for programbinary
  LOCAL_CFLAGS += -DLOG_TAG=\"ProgramBinary\/Service\"

  include $(BUILD_SHARED_LIBRARY)
endif