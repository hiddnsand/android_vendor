//#define LOG_TAG "FpsPolicy"

#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/Vector.h>
#include <utils/Timers.h>
#include <utils/String8.h>

#include <binder/Parcel.h>
#include <binder/IInterface.h>

#include <cutils/log.h>

#include "dfps/IFpsPolicyService.h"
#include "dfps/IFpsPolicy.h"
#include "dfps/IFpsInfo.h"
#include "dfps/FpsPolicy.h"
#include "dfps/FpsInfo.h"
#include "dfps/DupBitTube.h"

namespace android {
class BpFpsPolicyService : public BpInterface<IFpsPolicyService>
{
public:
    BpFpsPolicyService(const sp<IBinder>& impl) : BpInterface<IFpsPolicyService>(impl)
    {
    }

    virtual status_t regPolicy(const sp<IFpsPolicy>& policy, const String8& name,
                               const FpsPolicyInfo& info, sp<DupBitTube>& channel)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.writeStrongBinder(policy->asBinder(policy));
        data.writeString8(name);
        data.write(&info, sizeof(info));
        remote()->transact(FPS_POLICY_REGPOLICY, data, &reply);
        channel = new DupBitTube(reply);
        return reply.readInt32();
    }

    virtual status_t unregPolicy(uint64_t sequence)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.writeUint64(sequence);
        remote()->transact(FPS_POLICY_UNREGPOLICY, data, &reply);
        return reply.readInt32();
    }

    virtual status_t setFps(const FpsPolicyInfo& info)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.write(&info, sizeof(info));
        remote()->transact(FPS_POLICY_SETFPS, data, &reply);
        return reply.readInt32();
    }

    virtual status_t setSingleLayer(int32_t single)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.writeInt32(single);
        remote()->transact(FPS_POLICY_SETSINGLELAYER, data, &reply);
        return reply.readInt32();
    }

    virtual status_t setNumberDisplay(int32_t number)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.writeInt32(number);
        remote()->transact(FPS_POLICY_SETNUMBERDISPLAY, data, &reply);
        return reply.readInt32();
    }

    virtual status_t setHwcInfo(const HwcInfo& info)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.write(&info, sizeof(info));
        remote()->transact(FPS_POLICY_SETHWCINFO, data, &reply);
        return reply.readInt32();
    }

    virtual status_t setInputWindows(const String8& name, const SimpleInputWindowInfo& info)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.writeString8(name);
        data.write(&info, sizeof(info));
        remote()->transact(FPS_POLICY_SETINPUTWINDOWS, data, &reply);
        return reply.readInt32();
    }

    virtual status_t regInfo(const sp<IFpsInfo>& info, uint64_t id)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.writeStrongBinder(info->asBinder(info));
        data.writeUint64(id);
        remote()->transact(FPS_POLICY_REGINFO, data, &reply);
        return reply.readInt32();
    }

    virtual status_t unregInfo(uint64_t id)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.writeUint64(id);
        remote()->transact(FPS_POLICY_UNREGINFO, data, &reply);
        return reply.readInt32();
    }

    virtual status_t getPanelInfo(PanelInfo& info)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        remote()->transact(FPS_POLICY_GETPANELINFO, data, &reply);
        reply.read(&info, sizeof(info));
        return reply.readInt32();
    }

    virtual status_t setForegroundInfo(int32_t pid, String8& packageName)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.writeInt32(pid);
        data.writeString8(packageName);
        remote()->transact(FPS_POLICY_SETFOREGROUNDINFO, data, &reply);
        return reply.readInt32();
    }

    virtual status_t getFpsRange(FpsRange& range)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.write(&range, sizeof(range));
        remote()->transact(FPS_POLICY_GETFPSRANGE, data, &reply);
        reply.read(&range, sizeof(range));
        return reply.readInt32();
    }

    virtual status_t setWindowFlag(int32_t flag, int32_t mask)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.writeInt32(flag);
        data.writeInt32(mask);
        remote()->transact(FPS_POLICY_SETWINDOWFLAG, data, &reply);
        return reply.readInt32();
    }

    virtual status_t enableTracker(int32_t enable)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.writeInt32(enable);
        remote()->transact(FPS_POLICY_ENABLETRACKER, data, &reply);
        return reply.readInt32();
    }

    virtual status_t forbidAdjustingVsync(int32_t forbid)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IFpsPolicyService::getInterfaceDescriptor());
        data.writeInt32(forbid);
        remote()->transact(FPS_POLICY_FORBIDADJUSTINGVSYNC, data, &reply);
        return reply.readInt32();
    }
};

IMPLEMENT_META_INTERFACE(FpsPolicyService, "FpsPolicyService");

status_t BnFpsPolicyService::onTransact(uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags)
{
    switch (code)
    {
        case FPS_POLICY_REGPOLICY:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            sp<IFpsPolicy> policy = interface_cast<IFpsPolicy>(data.readStrongBinder());
            String8 name = data.readString8();
            FpsPolicyInfo info;
            data.read(&info, sizeof(info));
            sp<DupBitTube> channel;
            status_t result = regPolicy(policy, name, info, channel);
            channel->writeToParcel(reply);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_UNREGPOLICY:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            uint64_t sequence = data.readUint64();
            status_t result = unregPolicy(sequence);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_SETFPS:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            FpsPolicyInfo info;
            data.read(&info, sizeof(info));
            status_t result = setFps(info);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_SETSINGLELAYER:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            int32_t single = data.readInt32();
            status_t result = setSingleLayer(single);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_SETNUMBERDISPLAY:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            int32_t number = data.readInt32();
            status_t result = setNumberDisplay(number);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_SETHWCINFO:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            HwcInfo info;
            data.read(&info, sizeof(info));
            status_t result = setHwcInfo(info);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_SETINPUTWINDOWS:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            String8 name = data.readString8();
            SimpleInputWindowInfo info;
            data.read(&info, sizeof(info));
            status_t result = setInputWindows(name, info);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        case FPS_POLICY_REGINFO:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            sp<IFpsInfo> info = interface_cast<IFpsInfo>(data.readStrongBinder());
            uint64_t id = data.readUint64();
            status_t result = regInfo(info, id);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_UNREGINFO:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            uint64_t id = data.readUint64();
            status_t result = unregInfo(id);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_GETPANELINFO:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            PanelInfo info;
            status_t result = getPanelInfo(info);
            reply->write(&info, sizeof(info));
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_SETFOREGROUNDINFO:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            int32_t pid = data.readInt32();
            String8 packageName = data.readString8();
            status_t result = setForegroundInfo(pid, packageName);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_GETFPSRANGE:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            FpsRange range;
            data.read(&range, sizeof(range));
            status_t result = getFpsRange(range);
            reply->write(&range, sizeof(range));
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_SETWINDOWFLAG:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            int32_t flag = data.readInt32();
            int32_t mask = data.readInt32();
            status_t result = setWindowFlag(flag, mask);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_ENABLETRACKER:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            int32_t enable = data.readInt32();
            status_t result = enableTracker(enable);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
        case FPS_POLICY_FORBIDADJUSTINGVSYNC:
        {
            CHECK_INTERFACE(IFpsPolicyService, data, reply);
            int32_t forbid = data.readInt32();
            status_t result = forbidAdjustingVsync(forbid);
            reply->writeInt32(result);
            return NO_ERROR;
        }
        break;
    }
    return BBinder::onTransact(code, data, reply, flags);
}

sp<IFpsPolicyService> checkFpsPolicyService()
{
    const sp<IServiceManager> sm = defaultServiceManager();
    if (sm != NULL) {
        // use non-blocking way to get GuiExtService with ServiceManager
        sp<IBinder> binder = sm->checkService(String16("FpsPolicyService"));
        if (binder != NULL) {
            return interface_cast<IFpsPolicyService>(binder);
        }
        ALOGW("Cannot find FpsPolicyService");
        return NULL;
    }
    ALOGW("Cannot find default ServiceManager");
    return NULL;
}

};
