/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.server.stk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.Log;

public class StkManagerImpl extends StkManager {
    private static final String TAG = "StkManagerImpl";

    public static final boolean DEBUG = false;
    /// M: [ALPS00062902]THE INTENT of STK UserActivity
    public static final String STK_USERACTIVITY =
        "mediatek.intent.action.stk.USER_ACTIVITY";
    public static final String STK_USERACTIVITY_ENABLE =
        "mediatek.intent.action.stk.USER_ACTIVITY.enable";
    /// M: [ALPS00062902]The global variable to save the state of stk.enable.user_activity
    boolean mIsStkUserActivityEnabled = false;
    /// M: [ALPS00062902]Protect mIsStkUserActivityEnabled be accessed at the multiple places
    private Object mStkLock = new Object();

    /// M: [ALPS00062902] BroadcastReceiver
    BroadcastReceiver mStkUserActivityEnReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            Log.v(TAG, "mStkUserActivityEnReceiver -- onReceive -- entry");

            synchronized (mStkLock) {
                if (action.equals(STK_USERACTIVITY_ENABLE)) {
                    if (DEBUG) {
                        Log.v(TAG, "Receive STK_ENABLE");
                    }
                    boolean enabled = intent.getBooleanExtra("state", false);
                    if (enabled != mIsStkUserActivityEnabled) {
                        mIsStkUserActivityEnabled = enabled;
                    }
                } else {
                    if (DEBUG) {
                        Log.e(TAG, "Receive Fake Intent");
                    }
                }
                if (DEBUG) {
                    Log.v(TAG, "mStkUserActivityEnReceiver -- onReceive -- exist "
                                + mIsStkUserActivityEnabled);
                }
            }
        }
    };

    public StkManagerImpl() {}

    @Override
    public void registerStkReceiver(Context context) {
        /// M: [ALPS00062902]register for stk  events @{
        IntentFilter stkUserActivityFilter = new IntentFilter();
        stkUserActivityFilter.addAction(STK_USERACTIVITY_ENABLE);
        context.registerReceiver(mStkUserActivityEnReceiver, stkUserActivityFilter);
        /// @}
    }

    @Override
    public void sendStkBroadcast(final Context context, Handler handler) {
        /// M:[ALPS00062902] When the user activiy flag is enabled,
        /// it notifies the intent "STK_USERACTIVITY" @{
        synchronized (mStkLock) {
            if (mIsStkUserActivityEnabled) {
                /// M: [ALPS00062902][ALPS00389865]Avoid deadlock @{
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(STK_USERACTIVITY);
                        context.sendBroadcast(intent);
                    }
                });
                /// @}
            }
        }
        /// @}
    }
}

