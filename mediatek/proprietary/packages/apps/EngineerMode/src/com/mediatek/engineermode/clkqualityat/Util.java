/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * public under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.clkqualityat;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Point;
import android.hardware.Camera.CameraInfo;
import android.location.Location;
import android.location.LocationListener;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.text.Html;
import android.util.Log;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.ModemCategory;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.ShellExe;
import com.mediatek.engineermode.bandselect.BandModeContent;
import com.mediatek.engineermode.clkqualityat.ClkQualityAtActivity.AtcMsg;
import com.mediatek.engineermode.emsvr.AFMFunctionCallEx;
import com.mediatek.engineermode.emsvr.FunctionReturn;
import com.mediatek.engineermode.wifi.WiFiStateManager;
import com.mediatek.storage.StorageManagerEx;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Common functions.
 */
public class Util {
    public static final String TAG = "ClkQualityAT/Util";

    private static final String SHELL_CMD = "/system/bin/sh";
    private static final String SHELL_CAT = "-c";
    private static final String CMD_LCD_ON = "echo 255 > /sys/class/leds/lcd-backlight/brightness";
    private static final String CMD_LCD_OFF = "echo 0 > /sys/class/leds/lcd-backlight/brightness";
    private static final String FAIL_STRING = "FFFFFFFF";
    static final String HISTORY_PATH = "desense_at_history_data";
    private static final int RETURN_FAIL = -1;
    public static final int RETURN_SUCCESS = 0;
    private static final int FB0_LCM_POWER_ON = 4;
    private static final int FB0_LCM_POWER_OFF = 5;
    private static final int ID_FINISH_NOTIFICATION = 10;
    static final CharSequence TESTING = Html.fromHtml("<font color='#FFFF00'>Testing</font>");
    static final CharSequence PASS = Html.fromHtml("<font color='#00FF00'>PASS</font>");
    static final CharSequence VENIAL = Html.fromHtml("<font color='#FFFF00'>Warning</font>");
    static final CharSequence SERIOUS = Html.fromHtml("<font color='#FF0000'>Warning</font>");
    static final CharSequence FAIL = Html.fromHtml("<font color='#FF0000'>FAIL</font>");
    static final CharSequence CONN_FAIL = Html.fromHtml("<font color='#FF0000'>CONN FAIL</font>");
    static final CharSequence CNR_FAIL = Html.fromHtml("<font color='#FF0000'>CNR FAIL</font>");
    private static int sWidth;
    private static int sHeight;


    private static final LocationListener sLocListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    static void setResolution(int x, int y) {
        Elog.i(TAG, "setResolution: " + x + "  " + y);
        sWidth = x;
        sHeight = y;
    }

    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static void sendAtCommand(Context context, String cmd, boolean isCDMA, int msgId) {
        Elog.i(TAG, "sendAtCommand. str: " + cmd);
        Intent intent = new Intent(ClkQualityAtActivity.ATC_SEND_ACTION);
        intent.putExtra(ClkQualityAtActivity.ATC_EXTRA_CMD, cmd);
        intent.putExtra(ClkQualityAtActivity.ATC_EXTRA_MODEM_TYPE, isCDMA);
        intent.putExtra(ClkQualityAtActivity.ATC_EXTRA_MSG_ID, msgId);
        context.sendBroadcast(intent);
    }

    static boolean isIntentAvailable(Context context, Intent intent) {
        final PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent,
                PackageManager.GET_ACTIVITIES);
        return list.size() > 0;
    }

    static boolean isCameraSupport(boolean front) {
        int availCameraNumber = android.hardware.Camera.getNumberOfCameras();
        for (int i = 0; i < availCameraNumber; i++) {
            CameraInfo info = new CameraInfo();
            android.hardware.Camera.getCameraInfo(i, info);
            if (info.facing == (front ? CameraInfo.CAMERA_FACING_FRONT :
                CameraInfo.CAMERA_FACING_BACK)) {
                return true;
            }
        }

        return false;
    }

    static void flightMode(Context context, boolean isOpen) {
        Elog.d(TAG, "flightMode start, switch: " + isOpen);

        BandTest.sAtcDone = false;
        if (!isOpen) {
            sendAtCommand(context, "AT+CFUN=1,1", false,
                          ClkQualityAtActivity.AtcMsg.REBOOT_LTE.getValue());
        } else {
            if (FeatureSupport.is93Modem()) {
                sendAtCommand(context, "AT+EFUN=0", false,
                        ClkQualityAtActivity.AtcMsg.FLIGHT_MODE.getValue());
            } else {
                sendAtCommand(context, "AT+CFUN=4", false,
                        ClkQualityAtActivity.AtcMsg.FLIGHT_MODE.getValue());
                while (true) {
                    if (BandTest.sAtcDone) {
                        Elog.d(TAG, "@Util.flightMode(), BandTest.sAtcDone: true");
                        break;
                    }
                    sleep(10);
                }

                if (FeatureSupport.isSupported(FeatureSupport.FK_MTK_C2K_SUPPORT)) {
                    BandTest.sAtcDone = false;
                    sendAtCommand(context, "AT+CPOF", true,
                            ClkQualityAtActivity.AtcMsg.FLIGHT_MODE_CDMA.getValue());
                }
            }
        }

        while (true) {
            if (BandTest.sAtcDone) {
                Elog.d(TAG, "@Util.flightMode(), BandTest.sAtcDone: true");
                break;
            }
            sleep(10);
        }
        if (!isOpen) {
            sleep(10*1000);
            Elog.d(TAG, "flightMode switch end, "+isOpen);
            return ;
        }

        Elog.d(TAG, "flightMode switch end, "+isOpen);
    }

    static void enterFlightMode(Context context) {
        Elog.d(TAG, "enterFlightMode");
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", true);
        context.sendBroadcast(intent);
    }

    static void leaveFlightMode(Context context) {
        Elog.d(TAG, "leaveFlightMode");
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", false);
        context.sendBroadcast(intent);
    }

    static WakeLock sWakeLock;
    static boolean enterSleepMode(Context context) {
        Elog.d(TAG, "enterSleepMode");
        sWakeLock = new WakeLock();
        sWakeLock.acquire(context);
        try {
            return (ShellExe.execCommand("input keyevent 26", true) == RETURN_SUCCESS);
        } catch (IOException e) {
            return false;
        }
    }

    static boolean leaveSleepMode(Context context) {
        Elog.d(TAG, "leaveSleepMode");
        try {
            int result = ShellExe.execCommand("input keyevent 26", true);
            sWakeLock.release();
            return (result == RETURN_SUCCESS);
        } catch (IOException e) {
            return false;
        }
    }

    static void clickPoint(Point point, boolean landscape) {

        Point clickPoint = getClickPoint(point, landscape);
        String cmd = "input tap " + clickPoint.x + " " + clickPoint.y;
        try {
            ShellExe.execCommand(cmd, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Wake lock manager class.
     *
     */
    static class WakeLock {
        private PowerManager.WakeLock mScreenWakeLock = null;
        private PowerManager.WakeLock mCpuWakeLock = null;

        /**
         * Acquire CPU wake lock.
         *
         * @param context
         *            Global information about an application environment
         */
        void acquireCpuWakeLock(Context context) {
            Elog.v(TAG, "Acquiring cpu wake lock");
            if (mCpuWakeLock != null) {
                return;
            }

            PowerManager pm = (PowerManager) context
                    .getSystemService(Context.POWER_SERVICE);

            mCpuWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK
                    | PowerManager.ACQUIRE_CAUSES_WAKEUP, TAG);
            // | PowerManager.ON_AFTER_RELEASE, TAG);
            mCpuWakeLock.acquire();
        }

        /**
         * Acquire wake lock.
         *
         * @param context
         *            Global information about an application environment
         */
        void acquire(Context context) {
            acquireCpuWakeLock(context);
        }

        /**
         * Release wake lock.
         */
        void release() {
            Elog.v(TAG, "Releasing wake lock");
            if (mCpuWakeLock != null) {
                mCpuWakeLock.release();
                mCpuWakeLock = null;
            }
            if (mScreenWakeLock != null) {
                mScreenWakeLock.release();
                mScreenWakeLock = null;
            }
        }
    }

    static void enableWifi(Context context) {
        Elog.d(TAG, "enableWifi");
        new WiFiStateManager(context).checkState(context);
    }

    static void disableWifi(Context context) {
        Elog.d(TAG, "disableWifi");
        new WiFiStateManager(context).disableWiFi();
    }



    static boolean lcmOff() {
        Elog.d(TAG, "lcmOff");
        return (controlFb0(FB0_LCM_POWER_OFF) == RETURN_SUCCESS);
//        EmDsenseLcmAssit.lcmSetTm(0);
    }

    static boolean lcmOn() {
        Elog.d(TAG, "lcmOn");
        return (controlFb0(FB0_LCM_POWER_ON) == RETURN_SUCCESS);
//        EmDsenseLcmAssit.lcmSetTm(1);
    }

    /**
     * TRANSMIT command to driver file.
     *
     * @return result
     */
    private static int controlFb0(int... param) {
        int valueRet = RETURN_FAIL;
        AFMFunctionCallEx fb0Ctrl = new AFMFunctionCallEx();
        // boolean result = fb0Ctrl
        // .startCallFunctionStringReturn(AFMFunctionCallEx.FUNCTION_EM_FB0_IOCTL);
        //
        // if (!result) {
        // return RETURN_FAIL;
        // }

        if (fb0Ctrl
                .startCallFunctionStringReturn(AFMFunctionCallEx.FUNCTION_EM_FB0_IOCTL)) {
            fb0Ctrl.writeParamNo(param.length);
            for (int i : param) {
                fb0Ctrl.writeParamInt(i);
            }

            FunctionReturn resultStr;
            do {
                resultStr = fb0Ctrl.getNextResult();
                // if (resultStr.returnString == "") {
                if (resultStr.mReturnString.isEmpty()) {
                    break;
                } else {
                    if (resultStr.mReturnString.equalsIgnoreCase(FAIL_STRING)) {
                        valueRet = RETURN_FAIL;
                        break;
                    }
                    try {
                        valueRet = Integer.valueOf(resultStr.mReturnString);
                    } catch (NumberFormatException e) {
                        Elog.d(TAG, resultStr.mReturnString);
                        valueRet = RETURN_FAIL;
                    }
                }

            } while (resultStr.mReturnCode == AFMFunctionCallEx.RESULT_CONTINUE);

            if (resultStr.mReturnCode == AFMFunctionCallEx.RESULT_IO_ERR) {
                // error
                valueRet = RETURN_FAIL;
            }
        }
        return valueRet;

    }
    static boolean backlightOff() {

        Elog.d(TAG, "backlightOff");
        String[] cmd = {SHELL_CMD, SHELL_CAT, CMD_LCD_OFF };
        try {
            return (ShellExe.execCommand(cmd) == RETURN_SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    static boolean backlightOn() {
        Elog.d(TAG, "backlightOn");
        String[] cmd = {SHELL_CMD, SHELL_CAT, CMD_LCD_ON };
        try {
            return (ShellExe.execCommand(cmd) == RETURN_SUCCESS);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    static void pressBack() {
        try {
            ShellExe.execCommand("input keyevent 4", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void pressKey(int id) {
        try {
            ShellExe.execCommand("input keyevent " + id, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static boolean isFileExist(String filepath) {
        if (filepath == null) {
            return false;
        }
        File file = new File(filepath);
        return (file != null && file.exists());
    }

    static String getTestFilePath(Context context, String filepath) {
        String path = StorageManagerEx.getDefaultPath() +  "/" + filepath;
        Elog.i(TAG, "path :" + path);
        return path;
    }

    private static String getStoragePath(Context context, int index) {
        //return Environment.getExternalStorageDirectory().getPath();
        StorageVolume sv = getMountedVolumeById(context, index);
        if (sv != null) {
             Elog.i(TAG, "sv.getPath() :" + sv.getPath());
            return sv.getPath();
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    private static StorageVolume getMountedVolumeById(Context context, int index) {
        StorageManager storageMgr = (StorageManager) context.
                getSystemService(Context.STORAGE_SERVICE);
        StorageVolume[] volumes = storageMgr.getVolumeList();
        int mountedIndx = 0;
        for (int i = 0; i < volumes.length; i++) {
            String path = volumes[i].getPath();
            String state = storageMgr.getVolumeState(path);
            if (state.equals(Environment.MEDIA_MOUNTED)) {
                if (mountedIndx == index) {
                    return volumes[i];
                }
                mountedIndx++;
            }
        }
        return null;
    }

    static Point getClickPoint(Point p720p, boolean landscape) {
        if (landscape) {
            Point point = new Point();
            point.x = sHeight * p720p.x / 1280;
            point.y = sWidth - sWidth * p720p.y / 720;
            return point;
        } else {
            Point point = new Point();
            point.x = sWidth * p720p.x / 720;
            point.y = sHeight * p720p.y / 1280;
            return point;
        }
    }

    static void notifyFinish(Context context) {

        NotificationManager nm = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = new Notification.Builder(context)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentText(context.getResources().
                        getString(R.string.clk_quality_at_finish_notification))
                .setContentTitle(context.getResources().getString(R.string.clk_quality_at))
                .setAutoCancel(true)
                .build();
        notification.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notification.flags |= Notification.FLAG_INSISTENT;
        nm.notify(ID_FINISH_NOTIFICATION, notification);
    }

    static long[] getSupportedBand(String[] strInfo) {
        long[] values = new long[TestItem.INDEX_BAND_MAX];
        for (int i=0; i<TestItem.INDEX_BAND_MAX; ++i)
            values[i] = 0;
        for (final String value : strInfo) {
            Elog.i(TAG, "getSupportedBand strInfo: " + value);
            if (!value.substring(0, ClkQualityAtActivity.SAME_COMMAND.length())
                    .equals(ClkQualityAtActivity.SAME_COMMAND))
                continue;
            final String splitString = value.substring(ClkQualityAtActivity.SAME_COMMAND.length());
            final String[] getDigitalVal = splitString.split(",");
            if (getDigitalVal != null && getDigitalVal.length > 1) {
                for (int i = 0; i < values.length; i++) {
                    if (i >= getDigitalVal.length  || getDigitalVal[i] == null) {
                        values[i] = 0;
                        continue;
                    }
                    try {
                        values[i] = Long.valueOf(getDigitalVal[i].trim());
                        Elog.i(TAG, "getSupportedBand #" + i + ": " + values[i]);
                    } catch (NumberFormatException e) {
                        values[i] = 0;
                    }
                }
                break;
            }
        }
        return values;
    }

    static long getSupportedBandCdma(String[] bandStr) {
        Log.d(TAG, "query SupportedBandCdma get string array: " + Arrays.toString(bandStr));
        String splitString = bandStr[0].substring(BandModeContent.SAME_COMMAND_CDMA.length());
        final String[] getDigitalVal = splitString.split(",");
        long[] value = new long[2];
        try {
            for (int i = 0; i < 2; i++) {
                if (getDigitalVal[i] != null) {
                    value[i] = Integer.parseInt(getDigitalVal[i].substring(2), 16);
                }
            }
        } catch (NumberFormatException e) {
            value[0] = 0;
        }
        Log.d(TAG, "getSupportedBandCdma return: " + value[0]);
        return value[0];
    }
}
