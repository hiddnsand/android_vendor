/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2011 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * A3M FontLoader Implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <cstdio> /* sscanf */
#include <string> /* std::string */
#include <vector> /* std::vector - used in tempBitmap */

#include <a3m/detail/assetpath.h>     /* for AssetPath                       */
#include <a3m/log.h>                  /* for logging()                       */
#include <a3m/stb_truetype.h>         /* Third-party STB TrueType library    */
#include <fileutility.h>              /* for FileToString etc.               */
#include <fontloader.h>               /* FontLoader API                      */
#include <stdstringutility.h>         /* for endsWithI()                     */

namespace
{
  const A3M_INT32 FIRST_CHARACTER = 32;
  const A3M_INT32 N_CHARACTERS = 96;
  const A3M_INT32 BITMAP_WIDTH = 512;
  /*
   * function based on stbtt_BakeFontBitmap - calculates height of bitmap
   * needed for a given width. Returns true if successful
   */
  A3M_BOOL measureFont( A3M_UINT8 const* data, // raw file data
                        A3M_INT32 offset,      // font location
                        A3M_FLOAT pixelHeight, // height in pixels
                        A3M_INT32 width,       // width of bitmap
                        A3M_INT32 firstChar,   // characters to measure
                        A3M_INT32 numChars,
                        A3M_INT32& bitmapHeight, // calculated height
                        A3M_FLOAT& ascent,       // returned ascent height
                        A3M_FLOAT& descent,      // returned descent height
                        A3M_FLOAT& lineGap )     // returned gap between lines
  {
    A3M_FLOAT scale;
    A3M_INT32 x, y, bottomY, i;
    stbtt_fontinfo f;
    if( stbtt_InitFont(&f, data, offset) == 0 )
    {
      return A3M_FALSE;
    }
    x = 1;
    y = 1;
    bottomY = 1;

    scale = stbtt_ScaleForPixelHeight(&f, pixelHeight);
    A3M_INT32 ascentInt, descentInt, lineGapInt;
    stbtt_GetFontVMetrics( &f, &ascentInt, &descentInt, &lineGapInt );
    ascent = scale * ascentInt;
    descent = -scale * descentInt; //This is negative as returned by stbtt_GetFontVMetrics
    lineGap = scale * lineGapInt;

    for (i = 0; i < numChars; ++i)
    {
      A3M_INT32 advance, lsb, xbb0 , ybb0 , xbb1 , ybb1 , gw , gh;
      A3M_INT32 g = stbtt_FindGlyphIndex(&f, firstChar + i);
      stbtt_GetGlyphHMetrics(&f, g, &advance, &lsb);
      stbtt_GetGlyphBitmapBox(&f, g, scale, scale, &xbb0, &ybb0, &xbb1, &ybb1);
      gw = xbb1 - xbb0;
      gh = ybb1 - ybb0;
      if( ( x + gw + 1 ) >= width )
      {
        y = bottomY; // advance to next row
        x = 1;
      }
      x += ( gw + 2 );
      if( ( y + gh + 2 ) > bottomY ) { bottomY = y + gh + 2; }
    }
    bitmapHeight = bottomY;
    return A3M_TRUE;
  }

  /*
   * find the next larger power of two. Code taken from page:
   * http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
   */
  A3M_UINT32 nextLargerPowerOfTwo( A3M_UINT32 i )
  {
    i--;
    i |= i >> 1;
    i |= i >> 2;
    i |= i >> 4;
    i |= i >> 8;
    i |= i >> 16;
    i++;

    return i;
  }

  /**
   * Splits a font asset name into the font name and size, using a colon as a
   * separator.
   */
  A3M_BOOL getFontNameAndSize(std::string name, std::string& fontName,
                              A3M_INT32& fontSize)
  {
    A3M_UINT32 length = name.length();
    A3M_UINT32 end = name.find(';');

    // If string doesn't contain a semicolon, or the semicolon in the final
    // character, then the size can't be parsed.
    if (end == std::string::npos || end == length - 1)
    {
      return A3M_FALSE;
    }

    // Name is everything before the semicolon
    fontName = name.substr(0, end);

    // Size is everything after the semicolon (convert it to integer)
    std::string fontSizeStr = name.substr(end + 1, name.length() - end - 1);

    // Use temporary variable to avoid spurious Klocwork issue (ALPS01396258)
    A3M_INT32 size = 0;
    A3M_INT32 fontSizeValid = sscanf(fontSizeStr.c_str(), "%d", &size);

    if (fontSizeValid == 1)
    {
      fontSize = size;
      return A3M_TRUE;
    }

    return A3M_FALSE;
  }

} /* anonymous namespace */

namespace a3m
{
  Font::Ptr FontLoader::load(
    FontCache& cache,
    A3M_CHAR8 const* name)
  {
    // Split the asset name into font name and font size
    std::string fontName;
    A3M_INT32 fontSize;

    if( !getFontNameAndSize(name, fontName, fontSize) )
    {
      A3M_LOG_ERROR( "Font name %s is invalid", name );
      return Font::Ptr();
    }

    Stream::Ptr typeface = cache.getStream( fontName.c_str() );
    if( !typeface )
    {
      return Font::Ptr();
    }

    FileToString buffer( *typeface );

    A3M_INT32 width = BITMAP_WIDTH;
    A3M_INT32 height;
    A3M_FLOAT ascent, descent, lineGap;
    if( !measureFont( buffer,
                      0,  // font location
                      A3M_FLOAT( fontSize ),
                      width,       // width of bitmap
                      FIRST_CHARACTER, N_CHARACTERS,
                      height,
                      ascent,
                      descent,
                      lineGap ) )
    {
      A3M_LOG_ERROR( "Error reading Font \"%s\"", fontName.c_str() );
      return Font::Ptr();
    }

    height = static_cast<A3M_INT32>(
               nextLargerPowerOfTwo( static_cast<A3M_UINT32>( height ) ) );

    std::vector< A3M_UINT8 >
    tempBitmap( static_cast<A3M_UINT32>( width * height ) );

    stbtt_bakedchar cdata[N_CHARACTERS];


    stbtt_BakeFontBitmap( buffer, 0, A3M_FLOAT( fontSize ),
                          &tempBitmap[0], width, height, FIRST_CHARACTER,
                          N_CHARACTERS, cdata );

    Texture2D::Ptr texturePtr = cache.texture2DCache()->create(
                                  static_cast<A3M_UINT32>( width ),
                                  static_cast<A3M_UINT32>( height ),
                                  Texture::ALPHA,
                                  Texture::UNSIGNED_BYTE,
                                  &tempBitmap[0]);

    Font::CharacterData characterData[N_CHARACTERS];

    // NOTE: The texture as created by stbtt_BakeFontBitmap is upsidedown
    // according to OpenGL, but so are the bounding boxes, so this cancels out

    for( A3M_INT32 i = 0; i < 96; ++i )
    {
      characterData[i].left = A3M_FLOAT( cdata[i].x0 );
      characterData[i].top = A3M_FLOAT( cdata[i].y0 );
      characterData[i].right = A3M_FLOAT( cdata[i].x1 );
      characterData[i].bottom = A3M_FLOAT( cdata[i].y1 );
      characterData[i].xoff = cdata[i].xoff;
      characterData[i].yoff = cdata[i].yoff;
      characterData[i].xadvance = cdata[i].xadvance;
    }

    return cache.create(texturePtr, fontSize, ascent, descent,
                        lineGap, FIRST_CHARACTER, 96, characterData, name );
  }

  A3M_BOOL FontLoader::isKnown(A3M_CHAR8 const* name)
  {
    return endsWithI(name, ".ttf");
  }

} /* namespace a3m */
