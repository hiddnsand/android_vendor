package com.mediatek.engineermode.boot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.INetworkManagementService;
import android.os.ServiceManager;
import android.os.RemoteException;

import com.mediatek.engineermode.ChipSupport;
import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.wifi.EmPerformanceWrapper;
import com.mediatek.engineermode.wifi.WifiLogSwitchActivity;
import com.mediatek.engineermode.ehrpdbgdata.EhrpdBgData;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.bypass.BypassService;
import com.mediatek.engineermode.bypass.BypassSettings;

/**
 * a broadcast Receiver of EM boot.
 * @author: mtk81238
 */
public class EmBootupReceiver extends BroadcastReceiver {

    private static final String TAG = "EM/BootupReceiver";
    private static final String MODEM_FILTER_SHAREPRE= "telephony_modem_filter_settings";
    private static final String MODEM_NOTIFICATION_SHAREPRE =
        "telephony_modem_notification_settings";
    private static final String EHRPD_BG_DATA_SHREDPRE_NAME = "ehrpdBgData";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
            onBootupCompleted(context, intent);
        }
    }

    private void onBootupCompleted(Context context, Intent intent) {
        Elog.d(TAG, "Start onBootupCompleted");

        if (ChipSupport.isFeatureSupported(ChipSupport.MTK_WLAN_SUPPORT)) {
            if (EmPerformanceWrapper.isPerfSettingEnabled(context)) {
                EmBootStartService.requestStartService(context, new WifiSpeedUpBootHandler());
            }
        }

        //MD EM filter
        writeSharedPreference(context, false);

        //MD notification
        ResetMDNotificationSharedPreference(context);

        setEhrpdBackgroundData(context);

        tryInvokeBypassService(context);

        WifiLogSwitchActivity.onWifiStateChanged(context);
        Elog.d(TAG, "End onBootupCompleted");
    }
    private void tryInvokeBypassService(Context context) {
        SharedPreferences settings = context
            .getSharedPreferences(BypassSettings.PREF_SERV_ENABLE, 0);
        boolean prefServEnable = settings.getBoolean(BypassSettings.PREF_SERV_ENABLE, false);
        Elog.w(TAG, "prefServEnable : " + prefServEnable);

        if (prefServEnable) {
            Intent servIntent = new Intent(context, BypassService.class);
            Elog.w(TAG, "ready to start BypassService");
            context.startService(servIntent);
        }

    }

    private void writeSharedPreference(Context context, boolean flag) {
        final SharedPreferences modemFilterSh = context.getSharedPreferences(
                         MODEM_FILTER_SHAREPRE, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = modemFilterSh.edit();
        editor.putBoolean(context.getString(R.string.enable_md_filter_sim1), flag);
        editor.putBoolean(context.getString(R.string.enable_md_filter_sim2), flag);
        editor.commit();
    }

    private void ResetMDNotificationSharedPreference(Context context) {
        final SharedPreferences modemNotificationSh = context.getSharedPreferences(
            MODEM_NOTIFICATION_SHAREPRE, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = modemNotificationSh.edit();
        editor.putInt(context.getString(R.string.md_checkedshare_value), 0);
        editor.putBoolean(context.getString(R.string.md_notification_value), false);
        editor.commit();
    }

    private void setEhrpdBackgroundData(Context context) {
        final String BUTTON_FLAG = "flag";

        final SharedPreferences EhrpdBgDataSh = context.getSharedPreferences(
                EHRPD_BG_DATA_SHREDPRE_NAME,
                android.content.Context.MODE_PRIVATE);
        boolean mEhrpdBgDataEnable = EhrpdBgDataSh.getBoolean(BUTTON_FLAG, false);
        Elog.d(TAG, "mEhrpdBgDataEnable = " + mEhrpdBgDataEnable);
        if (mEhrpdBgDataEnable) {
            Elog.d(TAG, "setIotFirewall");
            EhrpdBgData.setDataDisable(mEhrpdBgDataEnable);
        }
    }
}
