/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncResult;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;

import android.telecom.VideoProfile;

import android.telephony.CarrierConfigManager;
import android.telephony.ims.stub.ImsCallSessionImplBase;
import android.telephony.ims.stub.ImsCallSessionListenerImplBase;
import android.telephony.PhoneNumberUtils;
import android.telephony.Rlog;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;

import android.text.TextUtils;

import com.android.ims.ImsCall;
import com.android.ims.ImsCallProfile;
import com.android.ims.ImsConferenceState;
import com.android.ims.ImsConfig;
import com.android.ims.ImsManager;
import com.android.ims.ImsReasonInfo;
import com.android.ims.ImsStreamMediaProfile;
import com.android.ims.internal.IImsCallSessionListener;
import com.android.ims.internal.IImsCallSession;
import com.android.ims.internal.IImsVideoCallProvider;
import com.android.ims.internal.ImsCallSession;

import com.android.internal.telephony.CallFailCause;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandException.Error;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.LastCallFailCause;
import com.android.internal.telephony.TelephonyIntents;
import com.mediatek.ims.ril.ImsCommandsInterface;
import com.mediatek.ims.ril.ImsCommandsInterface.RadioState;
import com.mediatek.ims.ril.ImsCallInfo;
import com.mediatek.ims.WfcReasonInfo;
import com.mediatek.ims.OperatorUtils;

import com.mediatek.ims.internal.CallControlDispatcher;
// For ViLTE feature.
import com.mediatek.ims.internal.ImsVTProvider;
import com.mediatek.ims.internal.ImsVTProviderUtil;

import com.mediatek.internal.telephony.MtkCallFailCause;
import com.mediatek.internal.telephony.MtkRILConstants;
import com.mediatek.internal.telephony.MtkSubscriptionManager;

import com.mediatek.wfo.DisconnectCause;
import com.mediatek.wfo.IWifiOffloadService;
import com.mediatek.wfo.WifiOffloadManager;
import com.mediatek.wfo.IMwiService;
import com.mediatek.wfo.MwisConstants;

// ALPS02136981. Prints fomatted debug logs.
import mediatek.telecom.FormattedLog;
import mediatek.telephony.MtkCarrierConfigManager;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.BufferedInputStream;

import java.lang.Math;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

// For operator add-on
import com.mediatek.ims.internal.op.OpImsServiceFactoryBase;
import com.mediatek.ims.internal.op.ImsCallSessionProxyExt;

public class ImsCallSessionProxy extends ImsCallSessionImplBase {
    private static final String LOG_TAG = "ImsCallSessionProxy";
    private static final boolean DBG = true;
    private static final boolean VDBG = false; // STOPSHIP if true

    // Sensitive log task
    private static final String PROP_FORCE_DEBUG_KEY = "persist.log.tag.tel_dbg";
    private static final boolean SENLOG = TextUtils.equals(Build.TYPE, "user");
    private static final boolean TELDBG = (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1);

    private String mCallId;
    private int mPhoneId;
    private int mState = ImsCallSession.State.IDLE;
    private Context mContext;
    private ImsService mImsService;
    private ImsCommandsInterface mImsRILAdapter;
    private ImsCallProfile mCallProfile;
    private IImsCallSessionListener mListener;
    private final Handler mHandler;
    private final Handler mServiceHandler;
    private boolean mHasPendingMo = false;
    private boolean mHasPendingDisconnect;
    private int mPendingDisconnectReason;
    private boolean mIsMerging = false;
    private boolean mIsOnTerminated = false;
    private boolean mIsAddRemoveParticipantsCommandOK = false;
    private String[] mPendingParticipantInfo ;
    private int mPendingParticipantInfoIndex = 0;
    private int mPendingParticipantStatistics = 0;
    private boolean mIsHideHoldEventDuringMerging = false;
    private String mMergeCallId = "";
    private ImsCallInfo.State mMergeCallStatus = ImsCallInfo.State.INVALID;
    private String mMergedCallId = "";
    private ImsCallInfo.State mMergedCallStatus = ImsCallInfo.State.INVALID;
    // normal call merge normal call
    private boolean mNormalCallsMerge = false;
    // at least one call is merged successfully
    private boolean mThreeWayMergeSucceeded = false;
    // count for +ECONF number in normal call merge normal call case
    private int mEconfCount = 0;
    private ImsCallSessionProxy mConfSession;
    private MtkImsCallSessionProxy mMtkConfSession;

    private String mCallNumber;

    // WFC
    private IWifiOffloadService mWfoService;
    private int mRatType = WifiOffloadManager.RAN_TYPE_MOBILE_3GPP;
    private static final int WFC_GET_CAUSE_FAILED = -1;

    // For ViLTE.
    private ImsVTProvider mVTProvider;
    private ImsCallProfile mLocalCallProfile;
    private ImsCallProfile mRemoteCallProfile;

    // Cached for SIP Error codes, update it when +ESIPCPI.
    private int mLastSipMethod;
    private int mLastSipCode;
    private String mLastSIPReasonHeader;

    /// M: ALPS02502328, cache the special terminate reason.
    private int mTerminateReason = -1;

    /// M: Enhance for force hangup.
    private int mHangupCount = 0;

    private boolean mIsOneKeyConf = false;
    private ConferenceEventListener mConfEvtListener;

    private enum CallErrorState {
        IDLE, DIAL, DISCONNECT;
    };

    private CallErrorState mCallErrorState = CallErrorState.IDLE;

    private Message mDtmfMsg = null;
    private Messenger mDtmfTarget = null;

    IWifiOffloadListenerProxy mWosListener;

    // For ECT, UA will put the active call on hold first.
    // Ignore the held event (ECPI 131) to avoid 2 held calls on Fwk in ECT period.
    private boolean mIsHideHoldDuringECT = false;

    private ImsReasonInfo mImsReasonInfo = null;
    private boolean mShouldUpdateAddressByPau = true;

    private MtkImsCallSessionProxy mMtkImsCallSession;

    // For operator add-on
    private ImsCallSessionProxyExt mOpExt = null;

    private static final String VT_PROVIDER_ID = "video_provider_id";
    private static final String REMOTE_DECLINE_HEADER
            = "Another device sent All Devices Busy response";
    private static final String COMPETION_ELSEWHERE_HEADER = "Call Completion Elsewhere";
    private static final String PULLED_AWAY_HEADER = "Call Has Been Pulled by Another Device";
    private static final String VIDEO_CALL_NOT_AVAILABLE_HEADER
            = "Video call is currently not available";
    private static final String VIDEO_CALL_UNAVAILABLE_HEADER
            = "Video calling is unavailable";

    private static final int INVALID_CALL_MODE = 0xFF;
    private static final int IMS_VOICE_CALL = 20;
    private static final int IMS_VIDEO_CALL = 21;
    private static final int IMS_VOICE_CONF = 22;
    private static final int IMS_VIDEO_CONF = 23;
    private static final int IMS_VOICE_CONF_PARTS = 24;
    private static final int IMS_VIDEO_CONF_PARTS = 25;

    private static final int SIP_CODE_FOR_REQUEST = 0;
    private static final int SIP_DIR_SEND = 0;
    private static final int SIP_DIR_RECEIVE = 1;
    private static final int SIP_TYPE_REQUEST = 0;
    private static final int SIP_TYPE_RESPONSE = 1;
    private static final int SIP_METHOD_INVITE = 1;
    private static final int SIP_METHOD_CANCEL = 4;
    private static final int SIP_METHOD_BYE = 7;

    // WFC
    private static final int IMS_CALL_TYPE_LTE = 1;
    private static final int IMS_CALL_TYPE_WIFI = 2;
    // For UE initial USSI request or response network USSI
    private static final int USSI_REQUEST = 1;
    private static final int USSI_RESPONSE = 2;

    //***** Events URC
    private static final int EVENT_POLL_CALLS_RESULT             = 101;
    private static final int EVENT_CALL_INFO_INDICATION          = 102;
    //private static final int EVENT_RINGBACK_TONE                 = 103;
    private static final int EVENT_ECONF_RESULT_INDICATION       = 104;
    private static final int EVENT_GET_LAST_CALL_FAIL_CAUSE      = 105;
    private static final int EVENT_CALL_MODE_CHANGE_INDICATION   = 106;
    private static final int EVENT_VIDEO_CAPABILITY_INDICATION   = 107;
    // For incoming USSI event
    private static final int EVENT_ON_USSI                       = 108;
    private static final int EVENT_ECT_RESULT_INDICATION         = 109;
    private static final int EVENT_GTT_CAPABILITY_INDICATION     = 110;

    //***** Events Operation result
    private static final int EVENT_DIAL_RESULT                   = 201;
    private static final int EVENT_ACCEPT_RESULT                 = 202;
    private static final int EVENT_HOLD_RESULT                   = 203;
    private static final int EVENT_RESUME_RESULT                 = 204;
    private static final int EVENT_MERGE_RESULT                  = 205;
    private static final int EVENT_ADD_CONFERENCE_RESULT         = 206;
    private static final int EVENT_REMOVE_CONFERENCE_RESULT      = 207;
    private static final int EVENT_SIP_CODE_INDICATION           = 208;
    private static final int EVENT_DIAL_CONFERENCE_RESULT        = 209;
    private static final int EVENT_SWAP_BEFORE_MERGE_RESULT      = 210;
    private static final int EVENT_RETRIEVE_MERGE_FAIL_RESULT    = 211;
    private static final int EVENT_DTMF_DONE    = 212;
    // For return message of sending/cancel USSI
    private static final int EVENT_SEND_USSI_COMPLETE            = 213;
    private static final int EVENT_CANCEL_USSI_COMPLETE          = 214;
    private static final int EVENT_ECT_RESULT                    = 215;
    private static final int EVENT_PULL_CALL_RESULT              = 216;

    private class ImsCallProfileEx {
        public static final String EXTRA_MPTY = "mpty";
        public static final String EXTRA_INCOMING_MPTY = "incoming_mpty";
    }

    private class ImsReasonInfoEx {
        public static final int CODE_SIP_WIFI_SIGNAL_LOST = 905;
        public static final int CODE_SIP_WFC_ISP_PROBLEM = 906;
        public static final int CODE_SIP_HANDOVER_WIFI_FAIL = 907;
        public static final int CODE_SIP_HANDOVER_LTE_FAIL = 908;
        public static final int CODE_SIP_503_ECC_OVER_WIFI_UNSUPPORTED = 1500;
        public static final int CODE_SIP_403_WFC_UNAVAILABLE_IN_CURRENT_LOCATION = 1501;
        public static final int CODE_SIP_REDIRECTED_EMERGENCY = 329;
    }

    private class ImsManagerEx {
        public static final String ACTION_IMS_RADIO_STATE_CHANGED =
                "com.android.ims.IMS_RADIO_STATE_CHANGED";
        public static final String EXTRA_IMS_RADIO_STATE = "android:imsRadioState";
        public static final String EXTRA_DIAL_STRING = "android:imsDialString";
    }

    private class VtProviderListener implements ImsVTProvider.VideoProviderStateListener {
        public void onReceivePauseState(boolean isPause) {
            if (mCallProfile == null) {
                return;
            }
            Rlog.d(LOG_TAG, "onReceivePauseState(): " + isPause);
            if (isPause) {
                mCallProfile.mMediaProfile.mVideoDirection =
                        ImsStreamMediaProfile.DIRECTION_INACTIVE;
            } else {
                mCallProfile.mMediaProfile.mVideoDirection =
                        ImsStreamMediaProfile.DIRECTION_SEND_RECEIVE;
            }

            notifyCallSessionUpdated();
        }
    }

    private VtProviderListener mVtProviderListener = new VtProviderListener();

    //Constructor for MT call
    ImsCallSessionProxy(Context context, ImsCallProfile profile,
                        IImsCallSessionListener listener, ImsService imsService,
                        Handler handler, ImsCommandsInterface imsRILAdapter,
                        String callId, int phoneId,
                        MtkImsCallSessionProxy mtkImsCallSession) {

        this(context, profile, listener, imsService, handler, imsRILAdapter, callId, phoneId);
        mMtkImsCallSession = mtkImsCallSession;
    }

    ImsCallSessionProxy(Context context, ImsCallProfile profile,
                        IImsCallSessionListener listener, ImsService imsService,
                        Handler handler, ImsCommandsInterface imsRILAdapter,
                        String callId, int phoneId) {
        if (DBG) {
            Rlog.d(LOG_TAG, "ImsSessionProxy RILAdapter:" + imsRILAdapter + "imsService:" +
                    imsService + " callID:" + callId + " phoneId: " + phoneId);
        }
        mPhoneId = phoneId;
        mServiceHandler = handler;
        mHandler = new MyHandler(handler.getLooper());
        mContext = context;
        mCallProfile = profile;
        mLocalCallProfile = new ImsCallProfile(profile.mServiceType, profile.mCallType);
        mRemoteCallProfile = new ImsCallProfile(profile.mServiceType, profile.mCallType);
        mListener = listener;
        mImsService = imsService;
        mImsRILAdapter = imsRILAdapter;
        mCallId = callId;

        // For operator add-on
        mOpExt = OpImsServiceFactoryBase.getInstance().makeImsCallSessionProxyExt();

        mImsRILAdapter.registerForCallInfo(mHandler, EVENT_CALL_INFO_INDICATION, null);
        /// M: Register for updating conference call merged/added result.
        mImsRILAdapter.registerForEconfResult(mHandler, EVENT_ECONF_RESULT_INDICATION, null);
        mImsRILAdapter.registerForCallProgressIndicator(mHandler, EVENT_SIP_CODE_INDICATION, null);
        mImsRILAdapter.registerForCallModeChangeIndicator(mHandler,
                EVENT_CALL_MODE_CHANGE_INDICATION, null);
        mImsRILAdapter.registerForVideoCapabilityIndicator(mHandler,
                EVENT_VIDEO_CAPABILITY_INDICATION, null);
        mImsRILAdapter.registerForEctResult(mHandler, EVENT_ECT_RESULT_INDICATION, null);
        mImsRILAdapter.registerForGttCapabilityIndicator(mHandler, EVENT_GTT_CAPABILITY_INDICATION,
                null);

        if (SystemProperties.get("persist.mtk_vilte_support").equals("1")) {
            Rlog.d(LOG_TAG, "start new VTProvider");
            if (mCallId != null) {
                //MT:new VT service
                mVTProvider = new ImsVTProvider();
                ImsVTProviderUtil.bind(mVTProvider, Integer.parseInt(mCallId), mPhoneId);
            } else {
                //MO:new VT service
                mVTProvider = new ImsVTProvider();
            }
            mVTProvider.addVideoProviderStateListener(mVtProviderListener);
            Rlog.d(LOG_TAG, "end new VTProvider");
        }
        /// M: Listen to incoming USSI event @{
        mImsRILAdapter.setOnUSSI(mHandler, EVENT_ON_USSI, null);
        /// @}

        // WFC: Registers the listener to WifiOffloadService for handover event and get rat type
        // from WifiOffloadService.
        IBinder b = ServiceManager.getService(WifiOffloadManager.WFO_SERVICE);
        if (b != null) {
            mWfoService = IWifiOffloadService.Stub.asInterface(b);
        } else {
            b = ServiceManager.getService(MwisConstants.MWI_SERVICE);
            try {
                if (b != null) {
                    mWfoService = IMwiService.Stub.asInterface(b).getWfcHandlerInterface();
                } else {
                    Rlog.e(LOG_TAG, "No MwiService exist");
                }
            } catch (RemoteException e) {
                Rlog.e(LOG_TAG, "can't get MwiService", e);
            }
        }
        if (mWfoService != null) {
            try {
                if (mWosListener == null) {
                    mWosListener = new IWifiOffloadListenerProxy();
                }
                mWfoService.registerForHandoverEvent(mWosListener);
                updateRat(mWfoService.getRatType(mPhoneId));

                // Update WiFiOffloadService call status for MT or conference case.
                if (callId != null) {
                    updateCallStateForWifiOffload(ImsCallSession.State.ESTABLISHING);
                }
            } catch (RemoteException e) {
                Rlog.e(LOG_TAG, "RemoteException ImsCallSessionProxy()");
            }
        }
        Rlog.d(LOG_TAG, "[WFC]mRatType is " + mRatType);

        mConfSession = null;
        mMtkConfSession = null;

        final IntentFilter filter = new IntentFilter();
        filter.addAction(ImsManagerEx.ACTION_IMS_RADIO_STATE_CHANGED);
        mContext.registerReceiver(mBroadcastReceiver, filter);
    }

    // Constructor for MO call
    ImsCallSessionProxy(Context context, ImsCallProfile profile,
                        IImsCallSessionListener listener, ImsService imsService,
                        Handler handler, ImsCommandsInterface imsRILAdapter, int phoneId) {
        this(context, profile, listener, imsService, handler, imsRILAdapter, null, phoneId);
        if (DBG) {
            Rlog.d(LOG_TAG, "ImsCallSessionProxy RILAdapter:" + imsRILAdapter);
        }
    }

    @Override
    public void close() {
        if (DBG) {
            Rlog.d(LOG_TAG, "ImsCallSessionProxy is closed!!! ");
        }

        if (mState == ImsCallSession.State.INVALID) {
            Rlog.d(LOG_TAG, "ImsCallSessionProxy already closed");
            return;
        }
        mState = ImsCallSession.State.INVALID;
        mImsRILAdapter.unregisterForCallInfo(mHandler);
        mImsRILAdapter.unregisterForEconfResult(mHandler);
        mImsRILAdapter.unregisterForCallProgressIndicator(mHandler);
        mImsRILAdapter.unregisterForCallModeChangeIndicator(mHandler);
        mImsRILAdapter.unregisterForVideoCapabilityIndicator(mHandler);
        mImsRILAdapter.unregisterForEctResult(mHandler);
        mImsRILAdapter.unregisterForGttCapabilityIndicator(mHandler);

        /// M: Unregister listening incoming USSI event @{
        mImsRILAdapter.unSetOnUSSI(mHandler);
        /// @}
        mContext.unregisterReceiver(mBroadcastReceiver);

        IImsVideoCallProvider vtProvider = getVideoCallProvider();

        if (vtProvider != null) {
            Rlog.d(LOG_TAG, "Start VtProvider setUIMode");
            ImsVTProviderUtil.setUIMode(mVTProvider, ImsVTProviderUtil.UI_MODE_DESTROY);
            mVTProvider.removeVideoProviderStateListener(mVtProviderListener);
            mVTProvider = null;
            Rlog.d(LOG_TAG, "End VtProvider setUIMode");
        }

        ImsConferenceHandler.getInstance().closeConference(mCallId);
        mConfEvtListener = null;

        // unregister the wfo listener
        if (mWfoService != null && mWosListener != null) {
            try {
                mWfoService.unregisterForHandoverEvent(mWosListener);
            } catch (RemoteException e) {
                Rlog.e(LOG_TAG, "RemoteException ImsCallSessionProxy()");
            }
        }
    }

    @Override
    public String getCallId() {
        return mCallId;
    }

    @Override
    public ImsCallProfile getCallProfile(){
        return mCallProfile;
    }

    @Override
    public ImsCallProfile getLocalCallProfile() {
        return mLocalCallProfile;
    }

    @Override
    public ImsCallProfile getRemoteCallProfile() {
        return mRemoteCallProfile;
    }

    @Override
    public String getProperty(String name) {
        return mCallProfile.getCallExtra(name);
    }

    @Override
    public int getState() {
        return mState;
    }

    @Override
    public boolean isInCall() {
        return false;
    }

    @Override
    public void setListener(IImsCallSessionListener listener) {
        mListener = listener;
    }

    @Override
    public void setMute(boolean muted) {
        mImsRILAdapter.setMute(muted, null);
    }

    @Override
    public void start(String callee, ImsCallProfile profile) {
        if (isCallPull(profile)) {
            pullCall(callee, profile);
            return;
        }

        int clirMode = profile.getCallExtraInt(ImsCallProfile.EXTRA_OIR, 0);
        // ALPS03435385, *82 is higher priority than CLIR invocation.
        boolean isNAPriorityClirSupported = getBooleanFromCarrierConfig(MtkCarrierConfigManager.
                MTK_KEY_CARRIER_NOURTH_AMERICA_HIGH_PRIORITY_CLIR_PREFIX_SUPPORTED);
        if (isNAPriorityClirSupported
                && clirMode == CommandsInterface.CLIR_INVOCATION && callee.startsWith("*82")) {
            Rlog.d(LOG_TAG, "Prior CLIR supported, *82 is higher priority than CLIR invocation.");
            clirMode = CommandsInterface.CLIR_DEFAULT;
        }
        boolean isEmergencyNumber = false;
        Message result = mHandler.obtainMessage(EVENT_DIAL_RESULT);

        /// M: For UE initiated USSI @{
        if (profile.getCallExtraInt(ImsCallProfile.EXTRA_DIALSTRING,
                ImsCallProfile.DIALSTRING_NORMAL) == ImsCallProfile.DIALSTRING_USSD) {
            Message msg = mHandler.obtainMessage(EVENT_SEND_USSI_COMPLETE, USSI_REQUEST, 0);
            mCallProfile = profile;
            mImsRILAdapter.sendUSSI(USSI_REQUEST, callee, msg);
            return;
        }
        /// @}

        if (profile.mServiceType == ImsCallProfile.SERVICE_TYPE_EMERGENCY) {
            isEmergencyNumber = true;
        }

        mImsRILAdapter.start(callee, clirMode, isEmergencyNumber, isVideoCall(profile), mPhoneId,
                result);
        mHasPendingMo = true;
        mCallNumber = callee;
        updateShouldUpdateAddressByPau();
    }

    @Override
    public void startConference(
                String[] participants, ImsCallProfile profile) {
        int clirMode = profile.getCallExtraInt(ImsCallProfile.EXTRA_OIR, 0);
        Message result = mHandler.obtainMessage(EVENT_DIAL_CONFERENCE_RESULT);

        mImsRILAdapter.startConference(participants, clirMode, isVideoCall(profile), mPhoneId,
                result);
        mHasPendingMo = true;
        mIsOneKeyConf = true;
        updateShouldUpdateAddressByPau();
    }

    @Override
    public void accept(int callType, ImsStreamMediaProfile profile) {
        Rlog.d(LOG_TAG, "accept - original call Type:" + mCallProfile.mCallType
                + "accept as:" + callType);

        /// M: For network initiated USSI @{
        if (mCallProfile.getCallExtraInt(ImsCallProfile.EXTRA_DIALSTRING,
            ImsCallProfile.DIALSTRING_NORMAL) == ImsCallProfile.DIALSTRING_USSD) {
            String m = mCallProfile.getCallExtra("m");
            String n = mCallProfile.getCallExtra("n");
            String str = mCallProfile.getCallExtra("str");
            Rlog.d(LOG_TAG, "accept: " + m + "," + n + "," + str);
            if (mListener != null) {
                try {
                    mListener.callSessionUssdMessageReceived(
                        ImsCallSessionProxy.this, Integer.parseInt(n), str);
                } catch (RemoteException e) {
                    Rlog.e(LOG_TAG, "RemoteException: " + e);
                }
            }
            return;
        }
        /// @}

        if (mCallProfile.mCallType == ImsCallProfile.CALL_TYPE_VOICE) {
            mImsRILAdapter.accept();
        } else {
            int videoMode;
            /* We set the videoMode base on AT+EVTA mode value.
             * AT+EVTA=<mode>,<call id>
             * Mode  =1 , accept as audio
             * Mode  =2 , accept as one way only video (Rx)
             * Mode  =3 , accept as one way only video (Tx)
             * For videoMode = 0, we will use ATA command to accept this video call.
             */
            switch (callType) {
                case ImsCallProfile.CALL_TYPE_VT:
                    videoMode = 0;
                    break;
                case ImsCallProfile.CALL_TYPE_VOICE:
                    videoMode = 1;
                    break;
                case ImsCallProfile.CALL_TYPE_VT_RX:
                    videoMode = 2;
                    break;
                case ImsCallProfile.CALL_TYPE_VT_TX:
                    videoMode = 3;
                    break;
                default:
                    videoMode = 0;
                    break;
            }
            mImsRILAdapter.acceptVideoCall(videoMode, Integer.parseInt(mCallId));
        }
    }

    @Override
    public void reject(int reason) {
        if (mCallId != null) {
            mImsRILAdapter.reject(Integer.parseInt(mCallId));
        } else {
            Rlog.e(LOG_TAG, "Reject Call fail since there is no call ID. Abnormal Case");
        }
    }

    @Override
    public void terminate(int reason) {
        /// M: For terminated USSI @{
        if (mCallProfile.getCallExtraInt(ImsCallProfile.EXTRA_DIALSTRING,
            ImsCallProfile.DIALSTRING_NORMAL) == ImsCallProfile.DIALSTRING_USSD) {
            Message msg = mHandler.obtainMessage(EVENT_CANCEL_USSI_COMPLETE);
            mImsRILAdapter.cancelPendingUssi(msg);
            return;
        }
        /// @}

        if (mCallId != null) {
            /// M: Support force hangup. @{
            if (mHangupCount > 0) {
                mImsRILAdapter.forceHangup(Integer.parseInt(mCallId));
            } else {
                mImsRILAdapter.terminate(Integer.parseInt(mCallId));
                if (mIsMerging) {
                    terminateConferenceSession();
                }
            }
            /// @}
            /// M: ALPS02502328, cache the terminate reason.
            mTerminateReason = reason;
            ++mHangupCount;
        } else {
            Rlog.e(LOG_TAG, "Terminate Call fail since there is no call ID. Abnormal Case");
            if (mHasPendingMo) {
                Rlog.e(LOG_TAG, "Pending M0, wait for assign call id");
                mHasPendingDisconnect = true;
                mPendingDisconnectReason = reason;
            }
        }
    }

    @Override
    public void hold(ImsStreamMediaProfile profile) {
        Message result = mHandler.obtainMessage(EVENT_HOLD_RESULT);
        mImsRILAdapter.hold(Integer.parseInt(mCallId), result);
    }

    @Override
    public void resume(ImsStreamMediaProfile profile) {
        Message result = mHandler.obtainMessage(EVENT_RESUME_RESULT);
        mImsRILAdapter.resume(Integer.parseInt(mCallId), result);
    }

    @Override
    public void merge() {
        Message result;
        Rlog.e(LOG_TAG, "Merge callId:" + mCallId);
        ImsCallInfo myCallInfo = mImsRILAdapter.getCallInfo(mCallId);
        ImsCallInfo beMergedCallInfo = null;

        boolean needSwapConfToFg = getBooleanFromCarrierConfig(
                MtkCarrierConfigManager.MTK_KEY_CARRIER_SWAP_CONFERENCE_TO_FOREGROUND_BEFORE_MERGE);

        if (myCallInfo == null) {
            Rlog.e(LOG_TAG, "can't find this call callInfo");
            mergeFailed();
            return;
        }

        if (myCallInfo.mState == ImsCallInfo.State.ACTIVE) {
            beMergedCallInfo = mImsRILAdapter.getCallInfo(ImsCallInfo.State.HOLDING);
        } else if (myCallInfo.mState == ImsCallInfo.State.HOLDING) {
            beMergedCallInfo = mImsRILAdapter.getCallInfo(ImsCallInfo.State.ACTIVE);
        }

        if (beMergedCallInfo == null) {
            Rlog.e(LOG_TAG, "can't find another call's callInfo");
            mergeFailed();
            return;
        }

        Rlog.d(LOG_TAG, "merge command- my call: conference type=" + myCallInfo.mIsConference +
                " call status=" + myCallInfo.mState + " beMergedCall: conference type=" +
                beMergedCallInfo.mIsConference + " call status=" + beMergedCallInfo.mState +
                " needSwapConfToFg=" + needSwapConfToFg);

        mMergeCallId = myCallInfo.mCallId;
        mMergeCallStatus = myCallInfo.mState;
        mMergedCallId = beMergedCallInfo.mCallId;
        mMergedCallStatus = beMergedCallInfo.mState;

        mIsMerging = true;

        ImsConferenceHandler confHdler = ImsConferenceHandler.getInstance();

        if (myCallInfo.mIsConference == false && beMergedCallInfo.mIsConference == false) {
            //Case 1: Normal call merge normal call
            result = mHandler.obtainMessage(EVENT_MERGE_RESULT);
            mImsRILAdapter.merge(result);
            mIsHideHoldEventDuringMerging = true;
            mNormalCallsMerge = true;
            // keep the conf numbers
            confHdler.firstMerge(myCallInfo.mCallNum, beMergedCallInfo.mCallNum);
        } else if (myCallInfo.mIsConference == true && beMergedCallInfo.mIsConference == true) {
            // Case 2: conference call merge conference call
            Rlog.d(LOG_TAG, "conference call merge conference call");
            result = mHandler.obtainMessage(EVENT_ADD_CONFERENCE_RESULT);
            mImsRILAdapter.inviteParticipantsByCallId(Integer.parseInt(mCallId),
                    beMergedCallInfo.mCallId, result);
            return;
        } else {
            // Keep the adding conf number
            confHdler.tryAddParticipant(
                (myCallInfo.mIsConference)? beMergedCallInfo.mCallNum : myCallInfo.mCallNum);
            if (needSwapConfToFg == false) {
                // normal case
                if (myCallInfo.mIsConference) {
                    Rlog.d(LOG_TAG, "active conference call merge background normal call");
                    result = mHandler.obtainMessage(EVENT_ADD_CONFERENCE_RESULT);
                    mImsRILAdapter.inviteParticipantsByCallId(Integer.parseInt(mCallId),
                            beMergedCallInfo.mCallId, result);
                } else {
                    Rlog.d(LOG_TAG, "active normal call merge background conference call");
                    result = mHandler.obtainMessage(EVENT_ADD_CONFERENCE_RESULT);
                    mImsRILAdapter.inviteParticipantsByCallId(
                                    Integer.parseInt(beMergedCallInfo.mCallId),
                                    myCallInfo.mCallId, result);
                }
            } else {
                // OP16 workaround
                if (myCallInfo.mIsConference && myCallInfo.mState == ImsCallInfo.State.ACTIVE) {
                    Rlog.d(LOG_TAG, "active conference call merge background normal call");
                    result = mHandler.obtainMessage(EVENT_ADD_CONFERENCE_RESULT);
                    mImsRILAdapter.inviteParticipantsByCallId(Integer.parseInt(mCallId),
                            beMergedCallInfo.mCallId, result);
                } else if (beMergedCallInfo.mIsConference &&
                        beMergedCallInfo.mState == ImsCallInfo.State.ACTIVE) {
                    // bemerged is conference call and merge background normal call
                    Rlog.d(LOG_TAG, "beMergedCall in foreground merge bg normal call");
                    result = mHandler.obtainMessage(EVENT_ADD_CONFERENCE_RESULT);
                    mImsRILAdapter.inviteParticipantsByCallId(
                                    Integer.parseInt(beMergedCallInfo.mCallId),
                                    myCallInfo.mCallId, result);
                } else {
                    Rlog.d(LOG_TAG, "swapping before merge");
                    result = mHandler.obtainMessage(EVENT_SWAP_BEFORE_MERGE_RESULT);
                    mImsRILAdapter.swap(result);
                }
            }
        }
    }

    @Override
    public void update(int callType, ImsStreamMediaProfile profile) {
        // currently MD not support for video downgrade or audio upgrade.
    }

    @Override
    public void extendToConference(String[] participants) {
        // currently MD not support to join multiple participants to join conference call.
    }

    void explicitCallTransfer() {
        Message result = mHandler.obtainMessage(EVENT_ECT_RESULT);
        mImsRILAdapter.explicitCallTransfer(result);
        mIsHideHoldDuringECT = true;
    }

    void unattendedCallTransfer(String number, int type) {
        Message result = mHandler.obtainMessage(EVENT_ECT_RESULT);
        mImsRILAdapter.unattendedCallTransfer(number, type, result);
        mIsHideHoldDuringECT = true;
    }

    @Override
    public void inviteParticipants(String[] participants) {
        Message result = mHandler.obtainMessage(EVENT_ADD_CONFERENCE_RESULT);
        mPendingParticipantInfoIndex = 0;
        mPendingParticipantInfo = participants;
        mPendingParticipantStatistics = participants.length;
        if (mCallId != null || mPendingParticipantStatistics == 0) {
            mImsRILAdapter.inviteParticipants(Integer.parseInt(mCallId),
                    mPendingParticipantInfo[mPendingParticipantInfoIndex], result);
        } else {
            Rlog.e(LOG_TAG, "inviteParticipants fail since no call ID or participants is null" +
                    " CallID=" + mCallId + " Participant number=" + mPendingParticipantStatistics);
            if (mListener != null) {
                try {
                    mListener.callSessionInviteParticipantsRequestFailed(
                            ImsCallSessionProxy.this, new ImsReasonInfo());
                } catch (RemoteException e) {
                    Rlog.e(LOG_TAG, "RemoteException occurs when InviteParticipantsRequestFailed");
                }
            }
        }
    }

    @Override
    public void removeParticipants(String[] participants) {
        Message result = mHandler.obtainMessage(EVENT_REMOVE_CONFERENCE_RESULT);
        mPendingParticipantInfoIndex = 0;
        mPendingParticipantInfo = participants;
        mPendingParticipantStatistics = participants.length;
        if (mCallId != null || mPendingParticipantStatistics == 0) {
            String addr = mPendingParticipantInfo[mPendingParticipantInfoIndex];
            String participantUri = ImsConferenceHandler.getInstance().getConfParticipantUri(addr);
            mImsRILAdapter.removeParticipants(Integer.parseInt(mCallId), participantUri, result);

            ImsConferenceHandler.getInstance().tryRemoveParticipant(addr);
        } else {
            Rlog.e(LOG_TAG, "removeParticipants fail since no call ID or participants is null" +
                    " CallID=" + mCallId + " Participant number=" + mPendingParticipantStatistics);
            if (mListener != null) {
                try {
                    mListener.callSessionRemoveParticipantsRequestFailed(
                            ImsCallSessionProxy.this, new ImsReasonInfo());
                } catch (RemoteException e) {
                    Rlog.e(LOG_TAG, "RemoteException occurs when RemoveParticipantsRequestFailed");
                }
            }
        }
    }

    @Override
    public void sendDtmf(char c, Message result) {
        mImsRILAdapter.sendDtmf(c, result);
    }

    @Override
    public void startDtmf(char c) {
        mImsRILAdapter.startDtmf(c, null);
    }

    @Override
    public void stopDtmf() {
        mImsRILAdapter.stopDtmf(null);
    }


    // Google issue. Original sendDtmf could not pass Message.target to another process,
    // because Message.writeToParcel didn't write target. Workaround this issue by adding
    // a new API which passes target by Messenger.
    void sendDtmfbyTarget(char c, Message result, Messenger target) {
        mDtmfMsg = result;
        mDtmfTarget = target;
        // Use ImsCallSessionProxy handler to send result back to original Message target.
        Message local_result = mHandler.obtainMessage(EVENT_DTMF_DONE);
        mImsRILAdapter.sendDtmf(c, local_result);
    }

    @Override
    public void sendUssd(String ussdMessage) {
        /// M: Send USSI request to RIL @{
        Message msg = mHandler.obtainMessage(EVENT_SEND_USSI_COMPLETE, USSI_RESPONSE, 0);
        mImsRILAdapter.sendUSSI(USSI_RESPONSE, ussdMessage, msg);
        /// @}
    }

    @Override
    public IImsVideoCallProvider getVideoCallProvider() {
        Rlog.d(LOG_TAG, "getVideoCallProvider: mVTProvider= " + mVTProvider);
        if (mVTProvider != null) {
            return mVTProvider.getInterface();
        } else {
            return null;
        }
    }

    @Override
    public boolean isMultiparty() {
        return mCallProfile.getCallExtraInt(ImsCallProfileEx.EXTRA_MPTY, 0) == 1;
    }

    public boolean isIncomingCallMultiparty() {
        return mCallProfile.getCallExtraInt(ImsCallProfileEx.EXTRA_INCOMING_MPTY, 0) == 1;
    }

    @Override
    public void sendRttMessage(String rttMessage) {
        mOpExt.sendRttMessage(mCallId, mImsRILAdapter, rttMessage);
    }

    @Override
    public void sendRttModifyRequest(ImsCallProfile to) {
        mOpExt.sendRttModifyRequest(mCallId, mImsRILAdapter, to);
    }

    @Override
    public void sendRttModifyResponse(boolean response) {
        mOpExt.sendRttModifyResponse(mCallId, mImsRILAdapter, response);
    }

    private class MyHandler extends Handler {

        private static final String PAU_NUMBER_FIELD = "<tel:";
        private static final String PAU_NAME_FIELD = "<name:";
        private static final String PAU_SIP_NUMBER_FIELD = "<sip:";
        private static final String PAU_END_FLAG_FIELD = ">";

        public MyHandler(Looper looper) {
            super(looper, null, true);
        }

        /// M: ALPS02492264, parse display name from pau @{
        private String getDisplayNameFromPau(String pau) {
            String value = "";
            if (TextUtils.isEmpty(pau)) {
                Rlog.d(LOG_TAG, "getDisplayNameFromPau()... pau is null !");
                return value;
            }
            for (int index = 0; index < pau.length(); index++) {
                char aChar = pau.charAt(index);
                if (aChar == '"') {
                    continue;
                }
                if (aChar == '<') {
                    break;
                }
                value += aChar;
            }
            return value;
        }
        /// @}

        private String getFieldValueFromPau(String pau, String field) {
            String value = "";
            if (TextUtils.isEmpty(pau) || TextUtils.isEmpty(field)) {
                Rlog.d(LOG_TAG, "getFieldValueFromPau()... pau or field is null !");
                return value;
            }

            if (!pau.contains(field)) {
                Rlog.d(LOG_TAG, "getFieldValueFromPau()... There is no such field in pau !"
                        + " field / pau :" + field + " / " + pau);
                return value;
            }

            int startIndex = pau.indexOf(field);
            startIndex += field.length();
            int endIndex = pau.indexOf(PAU_END_FLAG_FIELD, startIndex);
            value = pau.substring(startIndex, endIndex);
            return value;
        }

        private int imsReasonInfoCodeFromRilReasonCode(int causeCode) {
            Rlog.d(LOG_TAG, "imsReasonInfoCodeFromRilReasonCode: causeCode = " + causeCode);

            switch (causeCode) {
                case CallFailCause.USER_BUSY:
                    return ImsReasonInfo.CODE_SIP_BUSY;

                case CallFailCause.TEMPORARY_FAILURE:
                case CallFailCause.CHANNEL_NOT_AVAIL:
                    return ImsReasonInfo.CODE_SIP_TEMPRARILY_UNAVAILABLE;

                case CallFailCause.QOS_NOT_AVAIL:
                    return ImsReasonInfo.CODE_SIP_NOT_ACCEPTABLE;

                case CallFailCause.NO_CIRCUIT_AVAIL:
                case MtkCallFailCause.FACILITY_NOT_IMPLEMENT:
                case MtkCallFailCause.PROTOCOL_ERROR_UNSPECIFIED:
                    return ImsReasonInfo.CODE_SIP_SERVER_INTERNAL_ERROR;

                case CallFailCause.ACM_LIMIT_EXCEEDED:
                    return ImsReasonInfo.CODE_LOCAL_CALL_EXCEEDED;

                case CallFailCause.CALL_BARRED:
                    return ImsReasonInfo.CODE_LOCAL_ILLEGAL_STATE;
                case CallFailCause.FDN_BLOCKED:
                    return ImsReasonInfo.CODE_FDN_BLOCKED;

                case CallFailCause.BEARER_NOT_AVAIL:
                    if (mWfoService != null && mRatType == WifiOffloadManager.RAN_TYPE_WIFI) {
                        try {
                            if (!mWfoService.isWifiConnected()) {
                                Rlog.d(LOG_TAG,
                                        "Rat is Wifi, Wifi is disconnected, ret=SIGNAL_LOST");
                                return ImsReasonInfoEx.CODE_SIP_WIFI_SIGNAL_LOST;
                            }
                        } catch (RemoteException e) {
                            Rlog.e(LOG_TAG, "RemoteException in isWifiConnected()");
                        }
                    }

                    // return CODE_SIP_SERVER_ERROR by default.
                    return ImsReasonInfo.CODE_SIP_SERVER_ERROR;
                case MtkCallFailCause.INTERWORKING_UNSPECIFIED:
                /* sip 510 not implemented */
                case MtkCallFailCause.FACILITY_REJECTED:
                /* sip 502 bad gateway */
                case MtkCallFailCause.ACCESS_INFORMATION_DISCARDED:
                    return ImsReasonInfo.CODE_SIP_SERVER_ERROR;

                case MtkCallFailCause.NO_USER_RESPONDING:
                    return ImsReasonInfo.CODE_TIMEOUT_NO_ANSWER;

                case MtkCallFailCause.USER_ALERTING_NO_ANSWER:
                    return ImsReasonInfo.CODE_USER_NOANSWER;

                case MtkCallFailCause.CALL_REJECTED:
                    return ImsReasonInfo.CODE_SIP_USER_REJECTED;

                case CallFailCause.NORMAL_UNSPECIFIED:
                    return ImsReasonInfo.CODE_USER_TERMINATED_BY_REMOTE;

                case CallFailCause.UNOBTAINABLE_NUMBER:
                case MtkCallFailCause.INVALID_NUMBER_FORMAT:
                    return ImsReasonInfo.CODE_SIP_BAD_ADDRESS;

                case MtkCallFailCause.RESOURCE_UNAVAILABLE:
                case CallFailCause.SWITCHING_CONGESTION:
                case MtkCallFailCause.SERVICE_NOT_AVAILABLE:
                case MtkCallFailCause.NETWORK_OUT_OF_ORDER:
                case MtkCallFailCause.INCOMPATIBLE_DESTINATION:
                    return ImsReasonInfo.CODE_SIP_SERVICE_UNAVAILABLE;

                case MtkCallFailCause.BEARER_NOT_AUTHORIZED:
                case MtkCallFailCause.INCOMING_CALL_BARRED_WITHIN_CUG:
                    return ImsReasonInfo.CODE_SIP_FORBIDDEN;

                case MtkCallFailCause.CHANNEL_UNACCEPTABLE:
                case MtkCallFailCause.BEARER_NOT_IMPLEMENT:
                    return ImsReasonInfo.CODE_SIP_NOT_ACCEPTABLE;

                case MtkCallFailCause.NO_ROUTE_TO_DESTINATION:
                    return ImsReasonInfo.CODE_SIP_NOT_FOUND;

                case CallFailCause.OPERATOR_DETERMINED_BARRING:
                    return ImsReasonInfo.CODE_SIP_REQUEST_CANCELLED;

                case MtkCallFailCause.RECOVERY_ON_TIMER_EXPIRY:
                    return ImsReasonInfo.CODE_SIP_REQUEST_TIMEOUT;

                /* SIP 481: call/transaction doesn't exist */
                case MtkCallFailCause.INVALID_TRANSACTION_ID_VALUE:
                    return ImsReasonInfo.CODE_SIP_CLIENT_ERROR;

                /* [VoLTE]Normal call failed, need to dial as ECC */
                case MtkCallFailCause.IMS_EMERGENCY_REREG:
                    return MtkImsReasonInfo.CODE_SIP_REDIRECTED_EMERGENCY;

                case CallFailCause.ERROR_UNSPECIFIED:
                case CallFailCause.NORMAL_CLEARING:
                default:
                    // WFC: Because +CEER doesn't carry fail cause for WifiCalling, we need to get
                    // fail cause from WifiOffloadService
                    int wfcReason = getWfcDisconnectCause(causeCode);
                    if (wfcReason != WFC_GET_CAUSE_FAILED) {
                        return wfcReason;
                    }

                    int serviceState = mImsService.getImsServiceState(mPhoneId);

                    Rlog.d(LOG_TAG, "serviceState = " + serviceState);

                    if (serviceState == ServiceState.STATE_POWER_OFF) {
                        return ImsReasonInfo.CODE_LOCAL_POWER_OFF;
                    } else if (serviceState == ServiceState.STATE_OUT_OF_SERVICE) {
                        return ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN;
                    } else if (causeCode == CallFailCause.NORMAL_CLEARING) {
                        return ImsReasonInfo.CODE_USER_TERMINATED_BY_REMOTE;
                    } else {
                        // If nothing else matches, report unknown call drop reason
                        // to app, not NORMAL call end.
                        return ImsReasonInfo.CODE_UNSPECIFIED;
                    }
                }
        }

        private void updateImsReasonInfo(AsyncResult ar) {
            /* ESIPCPI: <call_id>,<dir>,<SIP_msg_type>,<method>,<response_code>[,<reason_text>] */
            String[] sipMessage = (String[]) ar.result;
            if (sipMessage == null || sipMessage[3] == null || sipMessage[4] == null
                    || mCallId == null || sipMessage[0].equals(mCallId) == false) {
                return;
            }
            Rlog.d(LOG_TAG, "receive sip method = " + sipMessage[3]
                    + "cause = " + sipMessage[4] + "reason header = " + sipMessage[5]);
            int sipDir = Integer.parseInt(sipMessage[1]);
            int sipType = Integer.parseInt(sipMessage[2]);
            int sipMethod = Integer.parseInt(sipMessage[3]);
            int sipCode = Integer.parseInt(sipMessage[4]);
            String reasonHeader = sipMessage[5];

            switch (sipCode) {
                case 403:
                    /// M: ALPS02501206. For OP07 requirement. @{
                    if (sipMethod == 1  // Invite
                            && reasonHeader != null && reasonHeader.length() != 0) {
                        mImsReasonInfo = new ImsReasonInfo(
                                ImsReasonInfoEx.CODE_SIP_403_WFC_UNAVAILABLE_IN_CURRENT_LOCATION,
                                0, reasonHeader);
                    }
                    /// @}
                    break;
                case 503:
                    /// M: ALPS02501206. For OP07 requirement. @{
                    if (sipMethod == 1  // Invite
                            && reasonHeader != null && reasonHeader.length() != 0) {
                        mImsReasonInfo = new ImsReasonInfo(
                                ImsReasonInfoEx.CODE_SIP_503_ECC_OVER_WIFI_UNSUPPORTED,
                                0, reasonHeader);
                    }
                    /// @}
                    break;
                case SIP_CODE_FOR_REQUEST:
                    if (sipMethod == SIP_METHOD_CANCEL && reasonHeader != null
                            && reasonHeader.equals(REMOTE_DECLINE_HEADER)) {
                        mImsReasonInfo = new ImsReasonInfo(
                                ImsReasonInfo.CODE_REMOTE_CALL_DECLINE,
                                0, reasonHeader);
                    } else if (sipMethod == SIP_METHOD_CANCEL && reasonHeader != null
                            && reasonHeader.equals(COMPETION_ELSEWHERE_HEADER)) {
                        mImsReasonInfo = new ImsReasonInfo(
                                ImsReasonInfo.CODE_ANSWERED_ELSEWHERE,
                                0, reasonHeader);
                    }  else if (sipMethod == SIP_METHOD_BYE && reasonHeader != null
                            && reasonHeader.equals(PULLED_AWAY_HEADER)) {
                        mImsReasonInfo = new ImsReasonInfo(
                                ImsReasonInfo.CODE_CALL_END_CAUSE_CALL_PULL,
                                0, reasonHeader);
                    } else if (sipDir == SIP_DIR_SEND && sipType == SIP_TYPE_REQUEST
                            && sipMethod == SIP_METHOD_INVITE && reasonHeader != null
                            && (reasonHeader.equals(VIDEO_CALL_NOT_AVAILABLE_HEADER)
                            || reasonHeader.equals(VIDEO_CALL_UNAVAILABLE_HEADER))) {
                        Rlog.d(LOG_TAG, reasonHeader);
                    }
                    break;
                default:
                    break;
            }
        }

        private boolean isCallModeUpdated(int callMode, int videoState) {
            Rlog.d(LOG_TAG, "updateCallMode- callMode:" + callMode + "videoState:" + videoState);
            boolean isChanged = false;
            int oldCallMode = mCallProfile.mCallType;

            if (callMode == IMS_VIDEO_CALL || callMode == IMS_VIDEO_CONF ||
                    callMode == IMS_VIDEO_CONF_PARTS) {
                switch(videoState) {
                    case 0:  //pause
                        mCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT_NODIR;
                        break;
                    case 1:  //send only
                        mCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT_TX;
                        break;
                    case 2:  // recv only
                        mCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT_RX;
                        break;
                    case 3:  // send and recv
                        mCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT;
                        break;
                    default:
                        mCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT;
                        break;
                }

                if (mCallProfile.mCallType != oldCallMode) {
                    isChanged = true;
                }
            } else if (callMode == IMS_VOICE_CALL || callMode == IMS_VOICE_CONF ||
                    callMode == IMS_VOICE_CONF_PARTS) {
                mCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VOICE;
                if (mCallProfile.mCallType != oldCallMode) {
                    isChanged = true;
                    Rlog.d(LOG_TAG, "Start setUIMode old: " + oldCallMode);
                    ImsVTProviderUtil.setUIMode(mVTProvider, ImsVTProviderUtil.UI_MODE_RESET);
                    Rlog.d(LOG_TAG, "End setUIMode new: " + mCallProfile.mCallType);
                }
            }

            return isChanged;
        }

        private void retrieveMergeFail() {

            ImsCallInfo mergeCallInfo = null;
            ImsCallInfo mergedCallInfo = null;
            boolean     isNotifyMergeFail = false;

            ImsConferenceHandler.getInstance().modifyParticipantFailed();

            Rlog.d(LOG_TAG, "retrieveMergeFail");
            if (mMergeCallId != null && !mMergeCallId.equals("")) {
                mergeCallInfo = mImsRILAdapter.getCallInfo(mMergeCallId);
            }

            if (mMergedCallId != null && !mMergedCallId.equals("")) {
                mergedCallInfo = mImsRILAdapter.getCallInfo(mMergedCallId);
            }

            if (mergeCallInfo != null && mergedCallInfo != null) {
                Rlog.d(LOG_TAG, "retrieveMergeFail- MergeCallInfo: callId=" + mergeCallInfo.mCallId
                        + " call status=" + mergeCallInfo.mState + " MergedCallInfo: callId=" +
                        mergedCallInfo.mCallId + " call status=" + mergedCallInfo.mState);
                if (mergeCallInfo.mState == ImsCallInfo.State.ACTIVE &&
                        mergedCallInfo.mState == ImsCallInfo.State.HOLDING) {
                    //Nothing Change
                    isNotifyMergeFail = true;
                } else if (mergeCallInfo.mState == ImsCallInfo.State.ACTIVE &&
                        mergedCallInfo.mState == ImsCallInfo.State.ACTIVE) {
                    // 2 active call and hold the merged call
                    Rlog.d(LOG_TAG, "retrieveMergeFail- two active call and hold merged call");
                    Message result = mHandler.obtainMessage(EVENT_RETRIEVE_MERGE_FAIL_RESULT);
                    mImsRILAdapter.hold(Integer.parseInt(mMergedCallId), result);
                } else if (mergeCallInfo.mState == ImsCallInfo.State.HOLDING &&
                        mergedCallInfo.mState == ImsCallInfo.State.HOLDING) {
                    // 2 hold call and resume merge call
                    Rlog.d(LOG_TAG, "retrieveMergeFail- two hold call and resume merge call");
                    Message result = mHandler.obtainMessage(EVENT_RETRIEVE_MERGE_FAIL_RESULT);
                    mImsRILAdapter.resume(Integer.parseInt(mMergeCallId), result);
                } else {
                    /*
                     *Sincemerge call is become hold and merged call is become active,
                     *we need to swap two calls
                     */
                    Rlog.d(LOG_TAG, "retrieveMergeFail- swap two calls");
                    Message result = mHandler.obtainMessage(EVENT_RETRIEVE_MERGE_FAIL_RESULT);
                    mImsRILAdapter.swap(result);
                }
            } else if (mergeCallInfo == null || mergedCallInfo == null) {
                //Only one call is exist and maintain the call state to original state
                if (mergeCallInfo != null) {
                    Rlog.d(LOG_TAG, "retrieveMergeFail- only merge call is left");
                    if (mergeCallInfo.mState != ImsCallInfo.State.ACTIVE) {
                        Message result = mHandler.obtainMessage(EVENT_RETRIEVE_MERGE_FAIL_RESULT);
                        mImsRILAdapter.resume(Integer.parseInt(mMergeCallId), result);
                    } else {
                        isNotifyMergeFail = true;
                    }
                } else if (mergedCallInfo != null) {
                    Rlog.d(LOG_TAG, "retrieveMergeFail- only merged call is left");
                    if (mergedCallInfo.mState != ImsCallInfo.State.HOLDING) {
                        Message result = mHandler.obtainMessage(EVENT_RETRIEVE_MERGE_FAIL_RESULT);
                        mImsRILAdapter.hold(Integer.parseInt(mMergedCallId), result);
                    } else {
                        isNotifyMergeFail = true;
                    }
                // There is the case merge call and merged call are disconnected just before
                // merge failed message occur. Should notify merge failed event or else InCallUI
                // will hang for a lot of time.
                } else {
                    isNotifyMergeFail = true;
                }
            }

            if (isNotifyMergeFail) {
                mergeFailed();
            }
        }

        @Override
        public void handleMessage(Message msg) {
            AsyncResult ar;
            int callMode = INVALID_CALL_MODE;
            if (DBG) {
                Rlog.d(LOG_TAG, "receive message by ImsCallSessionProxy - CallId:" + mCallId);
            }

            ImsConferenceHandler confHdler = ImsConferenceHandler.getInstance();
            switch (msg.what) {
                case EVENT_CALL_INFO_INDICATION:
                    /* +ECPI:<call_id>, <msg_type>, <is_ibt>, <is_tch>, <dir>, <call_mode>[, <number>, <toa>], "",<cause>
                     *
                     * if msg_type = DISCONNECT_MSG or ALL_CALLS_DISC_MSG,
                     * +ECPI:<call_id>, <msg_type>, <is_ibt>, <is_tch>,,,"",,"",<cause>
                     *
                     * if others,
                     * +ECPI:<call_id>, <msg_type>, <is_ibt>, <is_tch>, <dir>, <call_mode>[, <number>, <toa>], ""
                     *
                     *      0  O  CSMCC_SETUP_MSG
                     *      1  X  CSMCC_DISCONNECT_MSG
                     *      2  O  CSMCC_ALERT_MSG
                     *      3  X  CSMCC_CALL_PROCESS_MSG
                     *      4  X  CSMCC_SYNC_MSG
                     *      5  X  CSMCC_PROGRESS_MSG
                     *      6  O  CSMCC_CALL_CONNECTED_MSG
                     *   129  X  CSMCC_ALL_CALLS_DISC_MSG
                     *   130  O  CSMCC_MO_CALL_ID_ASSIGN_MSG
                     *   131  O  CSMCC_STATE_CHANGE_HELD
                     *   132  O  CSMCC_STATE_CHANGE_ACTIVE
                     *   133  O  CSMCC_STATE_CHANGE_DISCONNECTED
                     *   134  X  CSMCC_STATE_CHANGE_MO_DISCONNECTING
                     *   135  O  CSMCC_STATE_CHANGE_REMOTE_HOLD
                     *   136  O  CSMCC_STATE_CHANGE_REMOTE_RESUME
                     */
                    ar = (AsyncResult) msg.obj;
                    String[] callInfo = (String[]) ar.result;
                    int msgType = 0;
                    boolean isCallProfileUpdated = false;

                    if (DBG) Rlog.d(LOG_TAG, "receive EVENT_CALL_INFO_INDICATION");
                    if ((callInfo[1] != null) && (!callInfo[1].equals(""))) {
                        msgType = Integer.parseInt(callInfo[1]);
                    }

                    if ((callInfo[5] != null) && (!callInfo[5].equals(""))) {
                        callMode = Integer.parseInt(callInfo[5]);
                    }

                    boolean isCallDisplayUpdated = false;

                    if (mIsMerging && (!callInfo[0].equals(mCallId))) {
                        switch (msgType) {
                            case 130:
                                Rlog.d(LOG_TAG, "IMS: +ECPI : conference assign call id");
                                ImsCallProfile imsCallProfile = new ImsCallProfile();
                                // Update call type to conference call profile.
                                if ((callInfo[5] != null) && (!callInfo[5].equals(""))) {
                                    callMode = Integer.parseInt(callInfo[5]);
                                }

                                if (callMode == IMS_VIDEO_CALL || callMode == IMS_VIDEO_CONF ||
                                        callMode == IMS_VIDEO_CONF_PARTS) {
                                    imsCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT;
                                } else {
                                    imsCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VOICE;
                                }

                                if ((callInfo[6] != null) && (!callInfo[6].equals(""))) {
                                    // setup number as address for new conference
                                    imsCallProfile.setCallExtra(ImsCallProfile.EXTRA_OI,
                                        callInfo[6]);
                                    /*
                                    *we assume the remote uri information is same as telephone
                                    * number and update the remote ui information after getting pau.
                                    */
                                    imsCallProfile.setCallExtra(ImsCallProfile.EXTRA_REMOTE_URI,
                                            callInfo[6]);
                                    imsCallProfile.setCallExtraInt(ImsCallProfile.EXTRA_OIR,
                                            ImsCallProfile.OIR_PRESENTATION_NOT_RESTRICTED);
                                } else {
                                    imsCallProfile.setCallExtraInt(ImsCallProfile.EXTRA_OIR,
                                            ImsCallProfile.OIR_PRESENTATION_NOT_RESTRICTED);
                                }
                                createConferenceSession(imsCallProfile, callInfo[0]);
                                break;
                            default:
                                break;
                        }
                    } else if (mCallId != null && mCallId.equals(callInfo[0])) {
                        if (TextUtils.isEmpty(mCallNumber)) {
                            mCallNumber = callInfo[6];
                        }
                        isCallDisplayUpdated =
                            updateCallDisplayFromNumberOrPau(mCallNumber, callInfo[8]);
                        switch (msgType) {
                            case 0:
                                mState = ImsCallSession.State.ESTABLISHING;
                                Rlog.d(LOG_TAG, "IMS: +ECPI : incoming call");
                                if ((callInfo[5] != null) && (!callInfo[5].equals(""))) {
                                    callMode = Integer.parseInt(callInfo[5]);
                                }

                                if (callMode == IMS_VIDEO_CALL || callMode == IMS_VIDEO_CONF ||
                                        callMode == IMS_VIDEO_CONF_PARTS) {
                                    mCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT;
                                } else {
                                    mCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VOICE;
                                }

                                updateMultipartyState(callMode, true);

                                // If the Pau & Num are both empty, set OIR as restricted.
                                // UI will indicate as private number.
                                String parmPau = callInfo[8];
                                String parmNum = callInfo[6];
                                if (TextUtils.isEmpty(parmPau) && TextUtils.isEmpty(parmNum)) {
                                    mCallProfile.setCallExtraInt(ImsCallProfile.EXTRA_OIR,
                                        ImsCallProfile.OIR_PRESENTATION_RESTRICTED);
                                } else {
                                    mCallProfile.setCallExtraInt(ImsCallProfile.EXTRA_OIR,
                                        ImsCallProfile.OIR_PRESENTATION_NOT_RESTRICTED);
                                }
                                /* ServiceID may need to refine in the future */
                                int serviceId = mImsService.mapPhoneIdToServiceId(mPhoneId);

                                if (!SENLOG || TELDBG) {
                                    Rlog.d(LOG_TAG, "IMS: sendIncomingCallIntent() call_id = "
                                            + mCallId + " dialString = " +  callInfo[6]);
                                } else {
                                    Rlog.d(LOG_TAG, "IMS: sendIncomingCallIntent() call_id = "
                                            + mCallId + " dialString = [hidden]");
                                }

                                //ALPS02136981. Prints debug messages for ImsPhone.
                                logDebugMessagesWithNotifyFormat("CC", "MT", mCallNumber, "");

                                Intent intent = new Intent(ImsManager.ACTION_IMS_INCOMING_CALL);
                                intent.putExtra(ImsManager.EXTRA_CALL_ID, mCallId);
                                intent.putExtra(ImsManagerEx.EXTRA_DIAL_STRING, callInfo[6]);
                                intent.putExtra(ImsManager.EXTRA_SERVICE_ID, serviceId);
                                mContext.sendBroadcast(intent);
                                break;
                            case 2: // CSMCC_ALERT_MSG
                                int isIbt = 1;

                                if (callInfo[2] != null) {
                                    isIbt = Integer.parseInt(callInfo[2]);
                                }

                                updateMultipartyState(callMode, false);

                                logDebugMessagesWithNotifyFormat(
                                        "CC", "Alerting", mCallNumber, " isIbt= " + isIbt);

                                if (isIbt == 0) {
                                    mCallProfile.mMediaProfile.mAudioDirection =
                                            ImsStreamMediaProfile.DIRECTION_INACTIVE;
                                } else {
                                    mCallProfile.mMediaProfile.mAudioDirection =
                                            ImsStreamMediaProfile.DIRECTION_RECEIVE;
                                }

                                if (mListener != null) {
                                    try {
                                        mListener.callSessionProgressing(ImsCallSessionProxy.this,
                                                mCallProfile.mMediaProfile);
                                    } catch (RemoteException e) {
                                        Rlog.e(LOG_TAG, "RemoteException callSessionProgressing");
                                    }
                                }
                                mHasPendingMo = false;
                                break;
                            case 6: //CSMCC_CALL_CONNECTED_MSG
                                mState = ImsCallSession.State.ESTABLISHED;
                                mCallProfile.mMediaProfile.mAudioDirection =
                                        ImsStreamMediaProfile.DIRECTION_SEND_RECEIVE;

                                updateMultipartyState(callMode, false);

                                // ALPS02136981. Prints debug messages for ImsPhone.
                                logDebugMessagesWithNotifyFormat("CC", "Active", mCallNumber, "");

                                if (mListener != null) {
                                    try {
                                        /* There may not has alerting message while dial
                                         * conference call. We need to reset mHasPendingMO.
                                         */
                                        if (mHasPendingMo) {
                                            mListener.callSessionProgressing(
                                                    ImsCallSessionProxy.this,
                                                    mCallProfile.mMediaProfile);
                                        }
                                        mListener.callSessionStarted(ImsCallSessionProxy.this,
                                                mCallProfile);
                                    } catch (RemoteException e) {
                                        Rlog.e(LOG_TAG, "RemoteException callSessionStarted()");
                                    }
                                }
                                mHasPendingMo = false;

                                boolean notifyCallSessionUpdate = false;
                                // ViLTE loopback mode
                                if (SystemProperties.get("persist.ims.simulate").equals("1")
                                        && SystemProperties.get("persist.radio.vilte_ut_support")
                                                .equals("1")) {
                                    mCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT;
                                    mLocalCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT;
                                    mRemoteCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT;
                                    notifyCallSessionUpdate = true;
                                // Check if call type changed
                                } else {
                                    Rlog.e(LOG_TAG, "update call type: " + callMode);
                                    int oldCallType = mCallProfile.mCallType;
                                    if (callMode == IMS_VIDEO_CALL || callMode == IMS_VIDEO_CONF ||
                                        callMode == IMS_VIDEO_CONF_PARTS) {
                                        mCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT;
                                    } else {
                                        mCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VOICE;
                                    }

                                    if (mCallProfile.mCallType != oldCallType) {
                                        notifyCallSessionUpdate = true;
                                    }
                                }

                                if (mListener != null && notifyCallSessionUpdate) {
                                    try {
                                        mListener.callSessionUpdated(ImsCallSessionProxy.this,
                                                mCallProfile);
                                    } catch (RemoteException e) {
                                        Rlog.e(LOG_TAG, "RemoteException callSessionUpdated()");
                                    }
                                }

                                // Update WiFiOffloadService call status for call started case.
                                updateCallStateForWifiOffload(mState);
                                break;
                            case 131: //CSMCC_STATE_CHANGE_HELD
                                // ALPS02136981. Prints debug messages for ImsPhone.
                                logDebugMessagesWithNotifyFormat("CC", "Onhold", mCallNumber, "");

                                if (mListener != null) {
                                    if (!mIsHideHoldEventDuringMerging && !mIsHideHoldDuringECT) {
                                        try {
                                            mListener.callSessionHeld(ImsCallSessionProxy.this,
                                                    mCallProfile);
                                        } catch (RemoteException e) {
                                            Rlog.e(LOG_TAG, "RemoteException callSessionHeld");
                                        }
                                    } else if (isCallDisplayUpdated) {
                                        notifyCallSessionUpdated();
                                    }
                                }
                                break;
                            case 132: //CSMCC_STATE_CHANGE_ACTIVE
                                if (mListener != null) {
                                    if (mState == ImsCallSession.State.ESTABLISHED) {
                                        try {
                                            // ALPS02136981. Prints debug messages for ImsPhone.
                                            logDebugMessagesWithNotifyFormat(
                                                "CC", "Active", mCallNumber, "");

                                            mListener.callSessionResumed(
                                                    ImsCallSessionProxy.this, mCallProfile);
                                        } catch (RemoteException e) {
                                            Rlog.e(LOG_TAG, "RemoteException SessionResumed");
                                        }
                                    } else if (isCallDisplayUpdated) {
                                        notifyCallSessionUpdated();
                                    }
                                }
                                break;
                            case 133: //CSMCC_STATE_CHANGE_DISCONNECTED
                                // ALPS02136981. Prints debug messages for ImsPhone.
                                logDebugMessagesWithNotifyFormat(
                                    "CC", "Disconnected", mCallNumber, "");

                                if (mState == ImsCallSession.State.INVALID) {
                                    Rlog.d(LOG_TAG, "mState is INVALID, return");
                                    break;
                                }

                                mIsOnTerminated = true;
                                mState = ImsCallSession.State.TERMINATED;
                                if (mHasPendingMo) {
                                    mHasPendingMo = false;
                                    mCallErrorState = CallErrorState.DIAL;
                                } else {
                                    mCallErrorState = CallErrorState.DISCONNECT;
                                }

                                if (mImsReasonInfo != null) {
                                    Rlog.d(LOG_TAG, "get disconnect cause directly from +ESIPCPI");
                                    notifyCallSessionTerminated(mImsReasonInfo);
                                } else {
                                    Rlog.d(LOG_TAG, "get disconnect cause from AT+CEER");
                                    Message result = mHandler.obtainMessage(
                                            EVENT_GET_LAST_CALL_FAIL_CAUSE);
                                    mImsRILAdapter.getLastCallFailCause(mPhoneId, result);
                                }

                                // Update WiFiOffloadService call status for call terminated case.
                                updateCallStateForWifiOffload(mState);
                                break;
                            case 135: //CSMCC_STATE_CHANGE_REMOTE_HOLD
                                notifyRemoteHeld();
                                if (isCallDisplayUpdated) {
                                    notifyCallSessionUpdated();
                                }
                                break;
                            case 136: //CSMCC_STATE_CHANGE_REMOTE_RESUME
                                notifyRemoteResumed();
                                if (isCallDisplayUpdated) {
                                    notifyCallSessionUpdated();
                                }
                                break;
                            default:
                                break;
                        }
                    } else if (mCallId == null && msgType == 130) {
                        Rlog.d(LOG_TAG, "IMS: receive 130 URC, call_id = " + callInfo[0]);
                        isCallDisplayUpdated =
                            updateCallDisplayFromNumberOrPau(mCallNumber, callInfo[8]);
                        mState = ImsCallSession.State.ESTABLISHING;
                        mCallId = callInfo[0];
                        if (mVTProvider != null) {
                            ImsVTProviderUtil
                                    .bind(mVTProvider, Integer.parseInt(mCallId), mPhoneId);
                        }
                        if (mIsOneKeyConf) {
                            ImsConferenceHandler.getInstance().startConference(
                                    mContext, new ConferenceEventListener(), callInfo[0]);
                        }
                        // Should update the first OI to fwk
                        if (isCallDisplayUpdated) {
                            notifyCallSessionUpdated();
                        }

                        // Update WiFiOffloadService call status for MO case.
                        updateCallStateForWifiOffload(ImsCallSession.State.ESTABLISHING);
                        if (mHasPendingDisconnect) {
                            mHasPendingDisconnect = false;
                            terminate(mPendingDisconnectReason);
                        }
                    }
                    break;
                case EVENT_ECONF_RESULT_INDICATION:
                    ar = (AsyncResult) msg.obj;
                    handleEconfIndication((String[]) ar.result);
                    break;
                case EVENT_DIAL_RESULT:
                case EVENT_DIAL_CONFERENCE_RESULT:
                case EVENT_PULL_CALL_RESULT:
                    handleDialResult((AsyncResult) msg.obj);
                    break;
                case EVENT_HOLD_RESULT:
                    ar = (AsyncResult) msg.obj;
                    if (DBG) {
                        Rlog.d(LOG_TAG, "receive EVENT_HOLD_RESULT");
                    }
                    if (mListener != null) {
                        if (ar.exception != null) {
                            if (DBG) {
                                Rlog.d(LOG_TAG, "hold call failed!!");
                            }
                            try {
                                ImsReasonInfo imsReasonInfo = new ImsReasonInfo();
                                mListener.callSessionHoldFailed(ImsCallSessionProxy.this,
                                        imsReasonInfo);
                            } catch (RemoteException e) {
                                Rlog.e(LOG_TAG, "RemoteException callSessionHoldFailed()");
                            }
                        } else {
                            if (DBG) {
                                Rlog.d(LOG_TAG, "hold call successed!!");
                            }
                        }
                    }
                    break;
                case EVENT_RESUME_RESULT:
                    ar = (AsyncResult) msg.obj;
                    if (DBG) {
                        Rlog.d(LOG_TAG, "receive EVENT_RESUME_RESULT");
                    }
                    if (mListener != null) {
                        if (ar.exception != null) {
                            if (DBG) {
                                Rlog.d(LOG_TAG, "resume call failed!!");
                            }
                            try {
                                mListener.callSessionResumeFailed(ImsCallSessionProxy.this,
                                        new ImsReasonInfo());
                            } catch (RemoteException e) {
                                Rlog.e(LOG_TAG, "RemoteException callSessionResumeFailed()");
                            }
                        } else {
                            if (DBG) {
                                Rlog.d(LOG_TAG, "resume call successed");
                            }
                        }
                    }
                    break;
                case EVENT_MERGE_RESULT:
                    ar = (AsyncResult) msg.obj;
                    if (DBG) {
                        Rlog.d(LOG_TAG, "receive EVENT_MERGE_RESULT");
                    }
                    if (mListener != null) {
                        if (ar.exception != null) {
                            if (DBG) {
                                Rlog.d(LOG_TAG, "merge call failed!!");
                            }
                            // ALPS02136981. Prints debug messages for ImsPhone.
                            logDebugMessagesWithNotifyFormat(
                                    "CC", "ConfCreated", "conferenceCall", " failed");

                            retrieveMergeFail();
                        }
                    }
                    break;
                case EVENT_SWAP_BEFORE_MERGE_RESULT:
                    ar = (AsyncResult) msg.obj;
                    if (DBG) {
                        Rlog.d(LOG_TAG, "receive EVENT_SWAP_BEFORE_MERGE_RESULT");
                    }
                    if (ar.exception != null) {
                        if (DBG) {
                            Rlog.d(LOG_TAG, "swap call failed!!");
                        }
                        retrieveMergeFail();
                    } else {
                        if (DBG) {
                            Rlog.d(LOG_TAG, "swap call successed");
                        }
                        Message result;
                        ImsCallInfo myCallInfo = mImsRILAdapter.getCallInfo(mCallId);

                        // get call info of another peer.
                        ImsCallInfo beMergedCallInfo = mImsRILAdapter.getCallInfo(mMergedCallId);
                        if (beMergedCallInfo == null) {
                            retrieveMergeFail();
                            break;
                        }
                        /// M: ALPS02475154, invite participants using call id. @{
                        if (myCallInfo.mIsConference) {
                            Rlog.d(LOG_TAG, "myCallI is conference, merge normal call");
                            result = mHandler.obtainMessage(EVENT_ADD_CONFERENCE_RESULT);
                            mImsRILAdapter.inviteParticipantsByCallId(Integer.parseInt(mCallId),
                                        beMergedCallInfo.mCallId, result);
                        } else {
                            Rlog.d(LOG_TAG, "bg conference is foreground now, merge normal call");
                            result = mHandler.obtainMessage(EVENT_ADD_CONFERENCE_RESULT);
                            mImsRILAdapter.inviteParticipantsByCallId(
                                        Integer.parseInt(beMergedCallInfo.mCallId),
                                        myCallInfo.mCallId, result);
                        }
                        /// @}
                    }
                    break;
                case EVENT_RETRIEVE_MERGE_FAIL_RESULT:

                    if (DBG) {
                        Rlog.d(LOG_TAG, "receive EVENT_RETRIEVE_MERGE_FAIL_RESULT");
                    }
                    // Don't care the retrieve result and just notify merge fail to ImsPhone.
                    mergeFailed();
                    break;
                case EVENT_ADD_CONFERENCE_RESULT:
                    ar = (AsyncResult) msg.obj;
                    if (DBG) {
                        Rlog.d(LOG_TAG, "receive EVENT_ADD_CONFERENCE_RESULT");
                    }

                    if (mIsMerging) {
                        if (ar.exception != null) {
                            retrieveMergeFail();
                        } else {
                            /*
                             * We only know the merge command is accepted by server for now.
                             * The merge result will be notified by receiving +ECONF URC.
                             */
                        }
                    } else {
                        if (mIsOnTerminated == true) {
                            break;
                        }

                        if (ar.exception == null) {
                            mIsAddRemoveParticipantsCommandOK = true;
                        }
                        mPendingParticipantInfoIndex ++;

                        if (mPendingParticipantInfoIndex < mPendingParticipantStatistics) {
                            Message result = mHandler.obtainMessage(EVENT_ADD_CONFERENCE_RESULT);

                            mImsRILAdapter.inviteParticipants(Integer.parseInt(mCallId),
                                    mPendingParticipantInfo[mPendingParticipantInfoIndex], result);

                        } else {
                            if (mListener != null) {
                                if (mIsAddRemoveParticipantsCommandOK == false) {
                                    try {
                                        mListener.callSessionInviteParticipantsRequestFailed(
                                                ImsCallSessionProxy.this, new ImsReasonInfo());
                                    } catch (RemoteException e) {
                                        Rlog.e(LOG_TAG, "RemoteException InviteFailed()");
                                    }
                                } else {
                                    try {
                                        mListener.callSessionInviteParticipantsRequestDelivered(
                                                ImsCallSessionProxy.this);
                                    } catch (RemoteException e) {
                                        Rlog.e(LOG_TAG, "RemoteException InviteDelivered()");
                                    }
                                }
                            }
                            mIsAddRemoveParticipantsCommandOK = false;
                        }
                    }
                    break;
                case EVENT_REMOVE_CONFERENCE_RESULT:
                    ar = (AsyncResult) msg.obj;
                    if (DBG) {
                        Rlog.d(LOG_TAG, "receive EVENT_REMOVE_CONFERENCE_RESULT");
                    }

                    if (mIsOnTerminated == true) {
                        break;
                    }

                    if (ar.exception == null) {
                        mIsAddRemoveParticipantsCommandOK = true;
                    }

                    mPendingParticipantInfoIndex ++;
                    if (mPendingParticipantInfoIndex < mPendingParticipantStatistics) {
                        Message result = mHandler.obtainMessage(EVENT_ADD_CONFERENCE_RESULT);

                        mImsRILAdapter.removeParticipants(Integer.parseInt(mCallId),
                                mPendingParticipantInfo[mPendingParticipantInfoIndex], result);
                    } else {
                        if (mListener != null) {
                            if (mIsAddRemoveParticipantsCommandOK == false) {
                                try {
                                    mListener.callSessionRemoveParticipantsRequestFailed(
                                            ImsCallSessionProxy.this, new ImsReasonInfo());
                                } catch (RemoteException e) {
                                    Rlog.e(LOG_TAG, "RemoteException RemoveFailed()");
                                }
                            } else {
                                try {
                                    mListener.callSessionRemoveParticipantsRequestDelivered(
                                            ImsCallSessionProxy.this);
                                } catch (RemoteException e) {
                                    Rlog.e(LOG_TAG, "RemoteException RemoveDelivered()");
                                }
                            }
                        }
                        mIsAddRemoveParticipantsCommandOK = false;
                    }
                    break;
                case EVENT_GET_LAST_CALL_FAIL_CAUSE:
                    ar = (AsyncResult) msg.obj;
                    ImsReasonInfo imsReasonInfo;
                    int sipCauseCode = ImsReasonInfo.CODE_UNSPECIFIED;
                    if (DBG) {
                        Rlog.d(LOG_TAG, "receive EVENT_GET_LAST_CALL_FAIL_CAUSE");
                    }

                    if (ar.exception != null) {
                        imsReasonInfo = new ImsReasonInfo();
                    /// M: ALPS02502328, use the illegal state as terminate reason. @{
                    } else if (mTerminateReason == ImsReasonInfo.CODE_LOCAL_ILLEGAL_STATE) {
                        Rlog.d(LOG_TAG, "Terminate conference due to mMergeHost is null");
                        imsReasonInfo = new ImsReasonInfo(mTerminateReason, 0);
                        mTerminateReason = -1;
                    /// @}
                    } else {
                        // Check SIP code/ReasonHeader first in case +ESIPCPI come after ECPI 133.
                        if (mImsReasonInfo == null) {
                            Rlog.d(LOG_TAG, "get disconnect cause from +CEER");

                            LastCallFailCause failCause = (LastCallFailCause) ar.result;
                            sipCauseCode = imsReasonInfoCodeFromRilReasonCode(failCause.causeCode);
                            /// M: Release 12, ECC redial with NW assigned category @{
                            if (sipCauseCode == ImsReasonInfoEx.CODE_SIP_REDIRECTED_EMERGENCY) {
                                try {
                                    int cat = Integer.valueOf(failCause.vendorCause);
                                    //ToDo: O migration
                                    //PhoneNumberUtils.setSpecificEccCategory(cat);
                                } catch (NumberFormatException e) {
                                    Rlog.e(LOG_TAG, "ECC redirect report is not a number: " +
                                            failCause.vendorCause);
                                }
                            }
                            /// @}
                            imsReasonInfo = new ImsReasonInfo(sipCauseCode, 0);
                        } else {
                            Rlog.d(LOG_TAG, "get disconnect cause directly from +ESIPCPI");
                            imsReasonInfo = mImsReasonInfo;
                        }
                    }

                    notifyCallSessionTerminated(imsReasonInfo);
                    break;
                case EVENT_SIP_CODE_INDICATION:
                    updateImsReasonInfo((AsyncResult) msg.obj);
                    break;
                case EVENT_CALL_MODE_CHANGE_INDICATION:
                    ar = (AsyncResult) msg.obj;
                    String[] callModeInfo = (String[]) ar.result;

                    /* +EIMSCMODE: <call id>,<call mode>,<video state>,<audio direction>,<PAU>  */
                    if (callModeInfo != null && callModeInfo[0].equals(mCallId)) {
                        int videoState = 2; // assum video state: send_recv is default value
                        if ((callModeInfo[1] != null) && (!callModeInfo[1].equals(""))) {
                            callMode = Integer.parseInt(callModeInfo[1]);
                        }
                        if ((callModeInfo[2] != null) && (!callModeInfo[2].equals(""))) {
                            videoState = Integer.parseInt(callModeInfo[2]);
                        }

                        if (DBG) {
                            Rlog.d(LOG_TAG, "receive EVENT_CALL_MODE_CHANGE_INDICATION mode=" +
                                    callMode + "video state:" + videoState);
                        }

                        boolean isCallModeChanged = isCallModeUpdated(callMode, videoState);
                        isCallDisplayUpdated  = ((callModeInfo.length >= 5) &&
                            handlePauUpdate(callModeInfo[4]));

                        boolean shouldUpdateExtras = (callMode == IMS_VIDEO_CONF_PARTS);
                        if (shouldUpdateExtras) {
                            mCallProfile.setCallExtra(VT_PROVIDER_ID, mCallId);
                            Rlog.e(LOG_TAG, "setCallIDAsExtras: " + VT_PROVIDER_ID +"= " + mCallId);
                        }

                        if (isCallModeChanged || isCallDisplayUpdated || shouldUpdateExtras) {
                            notifyCallSessionUpdated();
                            if (isCallModeChanged) {
                                // Update WiFiOffloadService call status for call mode change case.
                                updateCallStateForWifiOffload(mState);
                            }
                        }
                        notifyMultipartyStateChanged(callMode);
                    }
                    break;
                case EVENT_VIDEO_CAPABILITY_INDICATION:
                    ar = (AsyncResult) msg.obj;
                    String[] videoCapabilityInfo = (String[]) ar.result;
                    //+EIMSVCAP: <call ID>, <local video capability>, <remote video capability>
                    int  lVideoCapability = 0;
                    int  rVideoCapability = 0;
                    if (videoCapabilityInfo != null &&
                            videoCapabilityInfo[0].equals(mCallId)) {
                        if ((videoCapabilityInfo[1] != null) &&
                                (!videoCapabilityInfo[1].equals(""))) {
                            lVideoCapability = Integer.parseInt(videoCapabilityInfo[1]);
                            if (lVideoCapability == 1) {
                                mLocalCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT;
                            } else {
                                mLocalCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VOICE;
                            }
                        }

                        if ((videoCapabilityInfo[2] != null) &&
                                (!videoCapabilityInfo[2].equals(""))) {
                            rVideoCapability = Integer.parseInt(videoCapabilityInfo[2]);
                            if (rVideoCapability == 1) {
                                mRemoteCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VT;
                            } else {
                                mRemoteCallProfile.mCallType = ImsCallProfile.CALL_TYPE_VOICE;
                            }
                        }

                        if (DBG) {
                            Rlog.d(LOG_TAG, "receive EVENT_VIDEO_CAPABILITY_INDICATION local " +
                                    "video capability:" + lVideoCapability +
                                    " remote video capability:" + rVideoCapability);
                        }

                        if (mListener != null) {
                            try {
                                mListener.callSessionUpdated(ImsCallSessionProxy.this,
                                        mCallProfile);
                            } catch (RemoteException e) {
                                if (DBG) {
                                    Rlog.e(LOG_TAG, "RemoteException callSessionUpdated()");
                                }
                            }
                        }
                    }
                    break;
                case EVENT_DTMF_DONE:
                    // Send message to original target handler.
                    if (mDtmfTarget != null && mDtmfMsg != null) {
                        try {
                            mDtmfTarget.send(mDtmfMsg);
                        } catch (RemoteException e) {
                            Rlog.e(LOG_TAG, "RemoteException handleMessge() for DTMF");
                        }
                    }
                    mDtmfTarget = null;
                    mDtmfMsg = null;
                    break;
                /// M: Messages for USSI @{
                case EVENT_SEND_USSI_COMPLETE:
                    ar = (AsyncResult) msg.obj;
                    if (DBG) {
                        Rlog.d(LOG_TAG, "receive EVENT_SEND_USSI_COMPLETE");
                    }
                    if (mListener != null) {
                        try {
                            if (msg.arg1 == USSI_REQUEST) {
                                if (ar != null && ar.exception != null) {
                                    mListener.callSessionStartFailed(
                                        ImsCallSessionProxy.this, new ImsReasonInfo());
                                } else {
                                    mListener.callSessionStarted(
                                        ImsCallSessionProxy.this, mCallProfile);
                                }
                            } else {
                                if (ar != null && ar.exception != null) {
                                    mListener.callSessionTerminated(
                                        ImsCallSessionProxy.this, new ImsReasonInfo());
                                }
                            }
                        } catch (RemoteException e) {
                            Rlog.e(LOG_TAG, "RemoteException: " + e);
                        }
                    }
                    break;
                case EVENT_CANCEL_USSI_COMPLETE:
                    if (DBG) {
                        Rlog.d(LOG_TAG, "receive EVENT_CANCEL_USSI_COMPLETE");
                    }
                    if (mListener != null) {
                        try {
                            mListener.callSessionTerminated(
                                ImsCallSessionProxy.this, new ImsReasonInfo());
                        } catch (RemoteException e) {
                            Rlog.e(LOG_TAG, "RemoteException: " + e);
                        }
                    }
                    break;
                case EVENT_ON_USSI:
                    ar = (AsyncResult) msg.obj;
                    /*
                     * USSI response from the network, or network initiated operation.
                       +EIUSD: <class>,<status>,<str>,<lang>,<error_code>,<alertingpattern>,<sip_cause>
                         <class>:
                         1   USSD notify
                         2   SS notify
                         3   MD execute result
                         <status>:  if class=1
                         0   no further user action required
                         1   further user action required
                         2   USSD terminated by network
                         3   other local client has responded
                         4   operation not supported
                         5   network time out
                         <status>:if class=3, value is return value of MD
                         0   execute success
                         1   common error
                         2   IMS unregistered
                         3   IMS busy
                         4   NW error response
                         5   session not exist
                         6   NW not support(404)
                    */
                    String[] eiusd = (String[]) ar.result;
                    if (DBG) {
                        Rlog.d(LOG_TAG, "receive EVENT_ON_USSI: " + eiusd[0] + "," + eiusd[1]
                            + "," + eiusd[2] + "," + eiusd[3] + "," + eiusd[4] + "," + eiusd[5]
                            + "," + eiusd[6] + "," + eiusd[7]);
                    }
                    int ussiClass = Integer.parseInt(eiusd[0]);
                    int ussiStatus = Integer.parseInt(eiusd[1]);
                    int phoneId = Integer.parseInt(eiusd[7]);
                    int mode = -1;
                    if (phoneId != mPhoneId) {
                        break;
                    }
                    if (ussiClass == 1) {
                        if (ussiStatus == 0 || ussiStatus == 2) {
                            mode = ImsCall.USSD_MODE_NOTIFY;
                        }
                        if (ussiStatus == 1) {
                            mode = ImsCall.USSD_MODE_REQUEST;
                        }
                    } else if (ussiClass == 3) {
                        if (ussiStatus == 0) {
                            mode = ImsCall.USSD_MODE_NOTIFY;
                        }
                    }
                    if (mListener != null) {
                        try {
                            mListener.callSessionUssdMessageReceived(
                                ImsCallSessionProxy.this, mode, eiusd[2]);
                        } catch (RemoteException e) {
                            Rlog.e(LOG_TAG, "RemoteException: " + e);
                        }
                    }
                    break;
                /// @}
                case EVENT_ECT_RESULT:
                    handleEctResult((AsyncResult) msg.obj);
                    break;
                case EVENT_ECT_RESULT_INDICATION:
                    handleEctIndication((AsyncResult) msg.obj);
                    break;
                default:
                    mOpExt.handleMessage(msg, mMtkImsCallSession, mCallId);
                    break;
            }
        }

        private void handleEconfIndication(String[] result) {
            // +ECONF:<conf_call_id>,<op>,<num>,<result>,<cause>[,<joined_call_id>]
            if (DBG) {
                Rlog.d(LOG_TAG, "receive EVENT_ECONF_RESULT_INDICATION mCallId:" + mCallId
                        + ", conf_call_id:" + result[0] + "joined_call_id:" + result[5]);
            }

            // Prevent timing issue in ImsCall.processMergeComplete(), it will check if the
            // session is still alive, by marking this session "terminating"
            // TODO: check which parameter means original call id
            if (mCallId != null && mCallId.equals(result[5]) && result[3].equals("0")) {
                mState = ImsCallSession.State.TERMINATING;
            }

            if (mIsMerging != true) {
                return;
            }

            if (mNormalCallsMerge) {
                // normal call merge normal call
                mEconfCount++;
                if (result[3].equals("0")) {
                    mThreeWayMergeSucceeded = true;
                }
                if (mEconfCount == 2) {
                    String ret = mThreeWayMergeSucceeded ? " successful" : " failed";
                    if (DBG) {
                        Rlog.d(LOG_TAG, "3 way conference merge result is" + ret);
                    }
                    // ALPS02136981. Prints debug messages for ImsPhone.
                    logDebugMessagesWithNotifyFormat("CC", "ConfCreated", "conferenceCall", ret);

                    if (mThreeWayMergeSucceeded) {
                        mergeCompleted();
                    } else {
                        retrieveMergeFail();
                        /// ALPS02383993: Terminate the conference if merge failed @{
                        int confCallId = Integer.parseInt(result[0]);
                        mImsRILAdapter.terminate(confCallId);
                        /// @}
                    }
                    //reset mEconfCount in order to merge again,
                    //when one of them failed to merge.
                    mEconfCount = 0;
                    mNormalCallsMerge = false;
                    mThreeWayMergeSucceeded = false;
                }
            } else {
                // conference call merge normal call
                if (result[3].equals("0")) {
                    if (DBG) {
                        Rlog.d(LOG_TAG, "conference call merge normal call successed");
                    }
                    // ALPS02136981. Prints debug messages for ImsPhone.
                    logDebugMessagesWithNotifyFormat(
                            "CC", "ConfCreated", "conferenceCall", " successed");

                    mergeCompleted();
                } else {
                    if (DBG) {
                        Rlog.d(LOG_TAG, "conference call merge normal call failed!!");
                    }
                    // ALPS02136981. Prints debug messages for ImsPhone.
                    logDebugMessagesWithNotifyFormat(
                            "CC", "ConfCreated", "conferenceCall", " failed");

                    retrieveMergeFail();
                }
            }
        }

        private void handleEctResult(AsyncResult ar) {
            if (ar == null || mListener == null) {
                if (DBG) {
                    Rlog.d(LOG_TAG, "handleEctResult ar or mListener is null");
                }
                return;
            }

            if (ar.exception != null) {
                if (DBG) {
                    Rlog.d(LOG_TAG, "explicit call transfer failed!!");
                }
                mIsHideHoldDuringECT = false;
                if (mMtkImsCallSession == null) {
                    return;
                }
                mMtkImsCallSession.notifyTransferFailed(new ImsReasonInfo());
            } else {
                if (DBG) {
                    Rlog.d(LOG_TAG, "explicit call transfer succeeded!!");
                }
            }
        }

        private void handleEctIndication(AsyncResult ar) {
            // +ECT: <call_id>, <ect_result>, <cause>
            // <ect_result>: 0  Fail
            //               1  Success
            Rlog.d(LOG_TAG, "handleEctIndication");
            mIsHideHoldDuringECT = false;
            if (ar == null || mMtkImsCallSession == null) {
                if (DBG) {
                    Rlog.d(LOG_TAG, "handleEctIndication ar or mMtkImsCallSession is null");
                }
                return;
            }

            int[] result = (int[]) ar.result;
            if (mCallId == null || result[0] != Integer.parseInt(mCallId)) {
                return;
            }

            if (result[1] == 0) {
                mMtkImsCallSession.notifyTransferFailed(new ImsReasonInfo());
            } else if (result[1] == 1) {
                mMtkImsCallSession.notifyTransferred();
            }
        }

        private void handleDialResult(AsyncResult ar) {
            if (ar == null || mListener == null) {
                if (DBG) {
                    Rlog.d(LOG_TAG, "handleDialResult ar or mListener is null");
                }
                return;
            }

            if (DBG) {
                Rlog.d(LOG_TAG,
                        "receive DIAL_RESULT or DIAL_CONFERENCE_RESULT or PULL_CALL_RESULT");
            }
            if (ar.exception != null) {
                if (DBG) {
                    Rlog.d(LOG_TAG, "dial call failed!!");
                }
                Message result = mHandler.obtainMessage(EVENT_GET_LAST_CALL_FAIL_CAUSE);
                mCallErrorState = CallErrorState.DIAL;
                mImsRILAdapter.getLastCallFailCause(mPhoneId, result);
                mHasPendingMo = false;
            }
        }

        private boolean updateMultipartyState(int callMode, boolean isIncoming) {
            boolean isMultipartyMode = (callMode == IMS_VOICE_CONF || callMode == IMS_VIDEO_CONF
                    || callMode == IMS_VOICE_CONF_PARTS || callMode == IMS_VIDEO_CONF_PARTS);

            boolean mptyState = isMultiparty();
            String mptyExtra = ImsCallProfileEx.EXTRA_MPTY;

            if (isIncoming) {
                mptyState = isIncomingCallMultiparty();
                mptyExtra = ImsCallProfileEx.EXTRA_INCOMING_MPTY;
            }

            if (mptyState == isMultipartyMode) {
                return false;
            }
            mCallProfile.setCallExtraInt(mptyExtra, (isMultipartyMode)? 1: 0);
            return true;
        }

        private void notifyMultipartyStateChanged(int callMode) {
            boolean stateChanged = updateMultipartyState(callMode, false);
            if (stateChanged == false) {
                return;
            }
            if (DBG) {
                Rlog.d(LOG_TAG, "notifyMultipartyStateChanged isMultiparty(): " + isMultiparty());
            }
            /*
            if (mListener != null) {
                try {
                    mListener.callSessionMultipartyStateChanged(ImsCallSessionProxy.this,
                            isMultiparty());
                } catch (RemoteException e) {
                    Rlog.e(LOG_TAG, "RemoteException callSessionMultipartyStateChanged()");
                }
            }
            */
        }

        private boolean updateAddressFromPau(String pau) {
            if (!mShouldUpdateAddressByPau) {
                Rlog.d(LOG_TAG, "MO call, should not update addr by PAU");
                return false;
            }

            boolean changed = false;

            String sipField = getFieldValueFromPau(pau, PAU_SIP_NUMBER_FIELD);
            if (!SENLOG || TELDBG) {
                Rlog.d(LOG_TAG, "updatePau()... sipNumber: " + sipField);
            } else {
                Rlog.d(LOG_TAG, "updatePau()... sipNumber: [hidden]");
            }
            String telField = getFieldValueFromPau(pau, PAU_NUMBER_FIELD);
            if (!SENLOG || TELDBG) {
                Rlog.d(LOG_TAG, "updatePau()... telNumber: " + telField);
            } else {
                Rlog.d(LOG_TAG, "updatePau()... telNumber: [hidden]");
            }
            // use the sip field first, then use the tel field
            String addr = (!TextUtils.isEmpty(sipField))? sipField : telField;
            String[] split = addr.split("[;@,]+");
            addr = split[0];

            String currentOI = mCallProfile.getCallExtra(ImsCallProfile.EXTRA_OI);
            if (!TextUtils.isEmpty(addr)) {
                if (!PhoneNumberUtils.compareLoosely(currentOI, addr)) {
                    changed = true;
                    mCallProfile.setCallExtra(ImsCallProfile.EXTRA_OI, addr);
                    if (!SENLOG || TELDBG) {
                        Rlog.d(LOG_TAG, "updatePau()... addr: " + addr);
                    } else {
                        Rlog.d(LOG_TAG, "updatePau()... addr: [hidden]");
                    }
                }
            }
            return changed;
        }

        private boolean updateDisplayNameFromPau(String pau) {
            boolean changed = false;
            String displayName = getDisplayNameFromPau(pau);
            String currentDisplayName = mCallProfile.getCallExtra(ImsCallProfile.EXTRA_CNA);
            if (!TextUtils.isEmpty(displayName)) {
                mCallProfile.setCallExtraInt(ImsCallProfile.EXTRA_CNAP,
                        ImsCallProfile.OIR_PRESENTATION_NOT_RESTRICTED);
                if (!PhoneNumberUtils.compareLoosely(currentDisplayName, displayName)) {
                    mCallProfile.setCallExtra(ImsCallProfile.EXTRA_CNA, displayName);
                    mCallProfile.setCallExtraInt(ImsCallProfile.EXTRA_CNAP,
                                            ImsCallProfile.OIR_PRESENTATION_NOT_RESTRICTED);
                    Rlog.d(LOG_TAG, "updateDisplayNameFromPau()... diaplayName: " + displayName);
                    changed = true;
                }
            }
            return changed;
        }

        private boolean handlePauUpdate(String pau) {
            if (TextUtils.isEmpty(pau)) {
                return false;
            }

            String sipNumber = getFieldValueFromPau(pau, PAU_SIP_NUMBER_FIELD);
            mCallProfile.setCallExtra(ImsCallProfile.EXTRA_REMOTE_URI, sipNumber);
            boolean ret = updateAddressFromPau(pau);
            ret |= updateDisplayNameFromPau(pau);
            return ret;
        }

        private boolean updateCallDisplayFromNumberOrPau(String number, String pau) {
            boolean changed = false;
            // if pau is empty and number is not empty
            if (TextUtils.isEmpty(pau) && !TextUtils.isEmpty(number)) {
                String curOI = mCallProfile.getCallExtra(ImsCallProfile.EXTRA_OI);
                if (!curOI.equals(number)) {
                    if (!SENLOG || TELDBG) {
                        Rlog.d(LOG_TAG, "updateOIFromNumber()... number: " + number);
                    } else {
                        Rlog.d(LOG_TAG, "updateOIFromNumber()... number: [hidden]");
                    }
                    mCallProfile.setCallExtra(ImsCallProfile.EXTRA_OI, number);
                    mCallProfile.setCallExtra(ImsCallProfile.EXTRA_REMOTE_URI, number);
                    changed = true;
                }
                return changed;
            }
            // use pau
            return handlePauUpdate(pau);
        }

        private int getWfcDisconnectCause(int causeCode) {
            Rlog.d(LOG_TAG, "[WFC] getWfcDisconnectCause mRatType = " + mRatType);
            if (mWfoService == null || mRatType != WifiOffloadManager.RAN_TYPE_WIFI
                    || causeCode == CallFailCause.NORMAL_CLEARING) {
                return WFC_GET_CAUSE_FAILED;
            }

            DisconnectCause disconnectCause = null;
            try {
                disconnectCause = mWfoService.getDisconnectCause(mPhoneId);
            } catch (RemoteException e) {
                Rlog.e(LOG_TAG, "RemoteException in getWfcDisconnectCause()");
            }
            if (disconnectCause == null) {
                return WFC_GET_CAUSE_FAILED;
            }
            int wfcErrorCause = disconnectCause.getErrorCause();
            Rlog.d(LOG_TAG, "[WFC] wfcErrorCause = " + wfcErrorCause);
            if (wfcErrorCause == WfcReasonInfo.CODE_WFC_WIFI_SIGNAL_LOST) {
                return ImsReasonInfoEx.CODE_SIP_WIFI_SIGNAL_LOST;
            } else if ((wfcErrorCause == WfcReasonInfo.CODE_WFC_UNABLE_TO_COMPLETE_CALL)
                    || (wfcErrorCause == WfcReasonInfo.CODE_WFC_UNABLE_TO_COMPLETE_CALL_CD)) {
                return ImsReasonInfoEx.CODE_SIP_HANDOVER_WIFI_FAIL;
            } else if (wfcErrorCause ==
                    WfcReasonInfo.CODE_WFC_NO_AVAILABLE_QUALIFIED_MOBILE_NETWORK) {
                return ImsReasonInfoEx.CODE_SIP_HANDOVER_LTE_FAIL;
            } else {
                return WFC_GET_CAUSE_FAILED;
            }
        }
    }

    private void mergeCompleted() {
        notifyCallSessionMergeComplete();
        mIsMerging = false;
        mIsHideHoldEventDuringMerging = false;
        ImsConferenceHandler.getInstance().modifyParticipantComplete();
        //clear mConfSession
        //When host failed to merge, and merge it again as host.
        //It will change to normal call merge conference call,
        //this should be cleared.
        mConfSession = null;
        mMtkConfSession = null;
    }

    private void mergeFailed() {
        if (mListener != null) {
            try {
                mListener.callSessionMergeFailed(ImsCallSessionProxy.this,
                        new ImsReasonInfo());
            } catch (RemoteException e) {
                Rlog.e(LOG_TAG, "RemoteException callSessionMergeFailed()");
            }
        }
        mMergeCallId = "";
        mMergeCallStatus = ImsCallInfo.State.INVALID;
        mMergedCallId = "";
        mMergedCallStatus = ImsCallInfo.State.INVALID;

        mIsMerging = false;
        mIsHideHoldEventDuringMerging = false;
        closeConferenceSession();
    }

    /**
     * Logs unified debug log messages, for "Notify".
     * Format: [category][Module][Notify][Action][call-number][local-call-ID] Msg. String.
     * P.S. uses the RIL call ID as the local call ID.
     *
     * @param category currently we only have 'CC' category.
     * @param action the action name. (e.q. Active, MT, Onhold, etc.)
     * @param callNumber call number
     * @param msg the optional messages
     * @hide
     */
    void logDebugMessagesWithNotifyFormat(
            String category, String action, String callNumber, String msg) {
        if (category == null || action == null) {
            // return if no mandatory tags.
            return;
        }

        if(isMultiparty() || isIncomingCallMultiparty()) {
            callNumber = "conferenceCall";
        }

        FormattedLog formattedLog = new FormattedLog.Builder()
                .setCategory(category)
                .setServiceName("ImsPhone")
                .setOpType(FormattedLog.OpType.NOTIFY)
                .setActionName(action)
                .setCallNumber(callNumber)
                .setCallId(getCallId())
                .setExtraMessage(msg)
                .buildDebugMsg();

        if (formattedLog != null) {
            if (!SENLOG || TELDBG) {
                Rlog.d(LOG_TAG, formattedLog.toString());
            }
        }
    }

    /**
     * A listener type for receiving notification on WFC handover events.
     */
    private class IWifiOffloadListenerProxy extends WifiOffloadManager.Listener {
        @Override
        public void onHandover(int simIdx, int stage, int ratType) {
            if(simIdx != mPhoneId) {
                return;
            }

            if (ratType == mRatType || stage != WifiOffloadManager.HANDOVER_END) {
                return;
            }

            updateRat(ratType);
            if (mListener != null) {
                try {
                    if (DBG) {
                        Rlog.d(LOG_TAG, "onHandover");
                    }
                    mListener.callSessionUpdated(ImsCallSessionProxy.this, mCallProfile);
                    mListener.callSessionHandover(ImsCallSessionProxy.this, mRatType, ratType,
                            new ImsReasonInfo());
                } catch (RemoteException e) {
                    Rlog.e(LOG_TAG, "RemoteException onHandover()");
                }
            }
        }

        @Override
        public void onRequestImsSwitch(int simIdx, boolean isImsOn) {

            // no-op
        }
    }

    private void updateCallStateForWifiOffload(int callState) {
        if (mWfoService == null) {
            Rlog.d(LOG_TAG, "updateCallStateForWifiOffload: skip, no WOS!");
            return;
        }

        if (mCallId == null) {
            Rlog.d(LOG_TAG, "updateCallStateForWifiOffload: skip, no call ID!");
            return;
        }

        int callId = Integer.parseInt(mCallId);
        int callType = WifiOffloadManager.CALL_TYPE_VOICE;

        if (mCallProfile.mCallType == ImsCallProfile.CALL_TYPE_VOICE ||
                mCallProfile.mCallType == ImsCallProfile.CALL_TYPE_VOICE_N_VIDEO) {
            callType = WifiOffloadManager.CALL_TYPE_VOICE;
        } else {
            callType =  WifiOffloadManager.CALL_TYPE_VIDEO;
        }

        int wosCallState;
        switch (callState) {
            case ImsCallSession.State.ESTABLISHING:
            case ImsCallSession.State.INITIATED:
            case ImsCallSession.State.NEGOTIATING:
            case ImsCallSession.State.REESTABLISHING:
            case ImsCallSession.State.RENEGOTIATING:
                wosCallState = WifiOffloadManager.CALL_STATE_ESTABLISHING;
                break;
            case ImsCallSession.State.ESTABLISHED:
                wosCallState = WifiOffloadManager.CALL_STATE_ACTIVE;
                break;
            case ImsCallSession.State.TERMINATED:
            case ImsCallSession.State.TERMINATING:
            case ImsCallSession.State.IDLE:
                wosCallState = WifiOffloadManager.CALL_STATE_END;
                break;
            default:
                Rlog.d(LOG_TAG, "updateCallStateForWifiOffload: skip, unexpected state: "
                        + callState);
                return;
        }

        try {
            mWfoService.updateCallState(mPhoneId, callId, callType, wosCallState);
        } catch (RemoteException e) {
            Rlog.e(LOG_TAG, "RemoteException in Wos.updateCallState()");
        }
    }

    private void notifyCallSessionTerminated(ImsReasonInfo info) {
        if (mListener == null) {
            Rlog.d(LOG_TAG, "notifyCallSessionTerminated close()");
            close();
            return;
        }

        if (mIsMerging && mTerminateReason == ImsReasonInfo.CODE_USER_TERMINATED) {
            Rlog.d(LOG_TAG, "notifyCallSessionTerminated close while merging");
            mergeFailed();
        }

        switch (mCallErrorState) {
            case DIAL :
                if (mListener != null) {
                    try {
                        mListener.callSessionStartFailed(ImsCallSessionProxy.this, info);
                    } catch (RemoteException e) {
                        Rlog.e(LOG_TAG, "RemoteException callSessionStartFailed()");
                    }
                }
                break;
            case DISCONNECT :
                if (mListener != null) {
                    try {
                        mListener.callSessionTerminated(ImsCallSessionProxy.this, info);
                    } catch (RemoteException e) {
                        Rlog.e(LOG_TAG, "RemoteException callSessionTerminated()");
                    }
                }
                break;
            default:
                break;
        }
    }

    private boolean updateRat(int ratType) {
        String radioTech;
        if (ratType == WifiOffloadManager.RAN_TYPE_MOBILE_3GPP) {
            radioTech = String.valueOf(ServiceState.RIL_RADIO_TECHNOLOGY_LTE);
        } else if (ratType == WifiOffloadManager.RAN_TYPE_WIFI) {
            radioTech = String.valueOf(ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN);
        } else {
            radioTech = String.valueOf(ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN);
        }
        if (mRatType == ratType
                && mCallProfile.getCallExtra(ImsCallProfile.EXTRA_CALL_RAT_TYPE).equals(radioTech)) {
            return false;
        }
        mRatType = ratType;
        mCallProfile.setCallExtra(ImsCallProfile.EXTRA_CALL_RAT_TYPE, radioTech);
        return true;
    }

    private void notifyRemoteHeld() {
        if (mListener != null) {
            try {
                mListener.callSessionHoldReceived(ImsCallSessionProxy.this, mCallProfile);
            } catch (RemoteException e) {
                Rlog.e(LOG_TAG, "RemoteException callSessionHoldReceived");
            }
        }
    }

    private void notifyRemoteResumed() {
        if (mListener != null) {
            try {
                mListener.callSessionResumeReceived(ImsCallSessionProxy.this, mCallProfile);
            } catch (RemoteException e) {
                Rlog.e(LOG_TAG, "RemoteException callSessionResumeReceived");
            }
        }
    }

    private void notifyCallSessionUpdated() {
        if (mListener == null) {
            return;
        }
        try {
            mListener.callSessionUpdated(ImsCallSessionProxy.this, mCallProfile);
        } catch (RemoteException e) {
            Rlog.e(LOG_TAG, "RemoteException callSessionUpdated");
        }
    }

    ConferenceEventListener getConfEvtListener() {
        if (mConfEvtListener == null) {
            mConfEvtListener = new ConferenceEventListener();
        }
        return mConfEvtListener;
    }

    class ConferenceEventListener extends ImsConferenceHandler.Listener {

        @Override
        public void onParticipantsUpdate(ImsConferenceState confState) {
            Rlog.d(LOG_TAG, "onParticipantsUpdate()");
            if (mListener == null) {
                return;
            }
            try {
                mListener.callSessionConferenceStateUpdated(ImsCallSessionProxy.this,
                        confState);
            } catch (RemoteException e) {
                Rlog.e(LOG_TAG, "RemoteException occurs when callSessionConferenceStateUpdated()");
            }
        };

        @Override
        public void onAutoTerminate() {
            Rlog.d(LOG_TAG, "onAutoTerminate()");
            terminate(ImsReasonInfo.CODE_UNSPECIFIED);
        };
    }

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (ImsManagerEx.ACTION_IMS_RADIO_STATE_CHANGED.equals(intent.getAction())) {
                RadioState radioState =
                    (RadioState) intent.getSerializableExtra(ImsManagerEx.EXTRA_IMS_RADIO_STATE);
                int phoneId = intent.getIntExtra(ImsManager.EXTRA_PHONE_ID, -1);
                Rlog.d(LOG_TAG, "radio state= " + radioState +" phoneId= " +phoneId);
                if (phoneId != mPhoneId || radioState.isAvailable()) {
                    return;
                }
                Rlog.d(LOG_TAG, "Radio not available");
                if (mIsOnTerminated) {
                    Rlog.d(LOG_TAG, "ignore");
                    return;
                }
                if (mHasPendingMo) {
                    mHasPendingMo = false;
                    mCallErrorState = CallErrorState.DIAL;
                } else {
                    mCallErrorState = CallErrorState.DISCONNECT;
                }
                notifyCallSessionTerminated(
                    new ImsReasonInfo(ImsReasonInfo.CODE_LOCAL_IMS_SERVICE_DOWN, 0));
            }
        }
    };

    private boolean isCallPull(ImsCallProfile profile) {
        if (profile == null || profile.mCallExtras == null) {
            return false;
        }
        Bundle extras = profile.mCallExtras.getBundle(ImsCallProfile.EXTRA_OEM_EXTRAS);
        if (extras == null) {
            return false;
        }
        boolean isCallPull = extras.getBoolean(ImsCallProfile.EXTRA_IS_CALL_PULL, false);
        return isCallPull;
    }

    private void pullCall(String target, ImsCallProfile profile) {
        Message result = mHandler.obtainMessage(EVENT_PULL_CALL_RESULT);
        mImsRILAdapter.pullCall(target, isVideoCall(profile), result);
    }

    private boolean isVideoCall(ImsCallProfile profile) {
        if (profile == null) {
            return false;
        }
        if (ImsCallProfile.getVideoStateFromImsCallProfile(profile) !=
                VideoProfile.STATE_AUDIO_ONLY) {
            return true;
        }
        return false;
    }

    private void updateShouldUpdateAddressByPau() {
        // Do not use PAU to update address for JIO SIM
        mShouldUpdateAddressByPau = getBooleanFromCarrierConfig(
                MtkCarrierConfigManager.MTK_KEY_CARRIER_UPDATE_DIALING_ADDRESS_FROM_PAU);
    }

    private void createConferenceSession(ImsCallProfile imsCallProfile, String callId) {
        if (mMtkImsCallSession != null) {
            // server for MTK framework
            createMtkConferenceSession(imsCallProfile, callId);
        } else {
            // server for AOSP framework
            createAospConferenceSession(imsCallProfile, callId);
        }
    }

    private void createMtkConferenceSession(ImsCallProfile imsCallProfile, String callId) {
        mMtkConfSession = new MtkImsCallSessionProxy(mContext, imsCallProfile,
                null, mImsService, mServiceHandler, mImsRILAdapter,
                callId, mPhoneId);
        ConferenceEventListener confEvtListener =
                mMtkConfSession.getConfEvtListener();
        ImsConferenceHandler.getInstance().startConference(mContext, confEvtListener, callId);
        mMtkImsCallSession.notifyCallSessionMergeStarted(mMtkConfSession, mCallProfile);
    }

    private void createAospConferenceSession(ImsCallProfile imsCallProfile, String callId) {
        mConfSession = new ImsCallSessionProxy(mContext, imsCallProfile,
                null, mImsService, mServiceHandler, mImsRILAdapter,
                callId, mPhoneId);
        ConferenceEventListener confEvtListener = mConfSession.getConfEvtListener();
        ImsConferenceHandler.getInstance().startConference(mContext, confEvtListener, callId);
        try {
            mListener.callSessionMergeStarted(ImsCallSessionProxy.this,
                    mConfSession, mCallProfile);
        } catch (RemoteException e) {
            Rlog.e(LOG_TAG, "RemoteException when session merged started");
        }
    }

    private void terminateConferenceSession() {
        if (mMtkConfSession != null) {
            Rlog.d(LOG_TAG, "Hangup Conference: Hangup host while merging.");
            MtkImsCallSessionProxy confSession = mMtkConfSession;
            confSession.terminate(ImsReasonInfo.CODE_LOCAL_ILLEGAL_STATE);
        } else if (mConfSession != null) {
            Rlog.d(LOG_TAG, "Hangup Conference: Hangup host while merging.");
            ImsCallSessionProxy confSession = mConfSession;
            confSession.terminate(ImsReasonInfo.CODE_LOCAL_ILLEGAL_STATE);
        }
    }

    private void closeConferenceSession() {
        if (mMtkConfSession != null) {
            mMtkConfSession.close();
            mMtkConfSession = null;
        } else if (mConfSession != null) {
            mConfSession.close();
            // ALPS02588163, mConfSession should set to null, or it might be
            // closed twice.
            mConfSession = null;
        }
    }

    private void notifyCallSessionMergeComplete() {
        if (mMtkImsCallSession != null) {
            mMtkImsCallSession.notifyCallSessionMergeComplete(mMtkConfSession);
        } else if (mListener != null) {
            try {
                mListener.callSessionMergeComplete(mConfSession);
            } catch (RemoteException e) {
                Rlog.e(LOG_TAG, "RemoteException callSessionMerged()");
            }
        }
    }

    private boolean getBooleanFromCarrierConfig(String key) {
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(mPhoneId);
        CarrierConfigManager configMgr = (CarrierConfigManager) mContext.
                                                getSystemService(Context.CARRIER_CONFIG_SERVICE);
        PersistableBundle carrierConfig =
                configMgr.getConfigForSubId(subId);
        boolean result = carrierConfig.getBoolean(key);
        Rlog.d(LOG_TAG, "getBooleanFromCarrierConfig() key: " + key + " result: " + result);
        return result;
    }
}

