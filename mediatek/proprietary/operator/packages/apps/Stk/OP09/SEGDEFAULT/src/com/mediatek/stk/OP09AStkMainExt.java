/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.stk.op09;

import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;

import com.android.internal.telephony.cat.AppInterface;
import com.android.internal.telephony.cat.TextMessage;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;

import com.mediatek.stk.ext.DefaultStkMainExt;
import com.mediatek.internal.telephony.cat.MtkCatLog;
import com.mediatek.internal.telephony.uicc.MtkIccCardProxy;


public class OP09AStkMainExt extends DefaultStkMainExt {

    private static final String TAG = "OP09AStkMainExt";

    /**
     * For op09,no need show entrance for CDMA card.
     *
     * @param slotId: the current used slot id
     * @return false if meet condition,default return true.
     * @internal
     */
    public boolean isShowEntrance(int slotId) {
        int subId[] = SubscriptionManager.getSubId(slotId);
        int phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        if (subId != null) {
            phoneId = SubscriptionManager.getPhoneId(subId[0]);
        }
        Phone myPhone = PhoneFactory.getPhone(phoneId);
        String cardType =((MtkIccCardProxy) myPhone.getIccCard()).getIccCardType();
        MtkCatLog.d(TAG, " slotId = " + slotId + ", cardType == " + cardType);
        if (cardType.contains("RUIM") || cardType.contains("CSIM")) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * For CT A/C lib requirement, when do BIP test if there is no message return
     * from SIM card,no need to show toast.
     * @param cmdType: the current commmand type,e.g.:OPEN CHANNEL,CLOSE CHANNEL
     * @param msg: the text message of the current command
     * @return false if meet condition,default return true.
     * @internal
     */
    public boolean isShowBipToast(AppInterface.CommandType cmdType, TextMessage msg) {
        MtkCatLog.d(TAG, "isShowBipToast,cmdtype :" + cmdType + " msg.iconSelfExplanatory:" +
             msg.iconSelfExplanatory + " msg.text:" + msg.text);
        if (!msg.iconSelfExplanatory
                && msg.text == null
                && (cmdType == AppInterface.CommandType.OPEN_CHANNEL
                        || cmdType == AppInterface.CommandType.CLOSE_CHANNEL
                        || cmdType == AppInterface.CommandType.RECEIVE_DATA
                        || cmdType == AppInterface.CommandType.SEND_DATA
                        || cmdType == AppInterface.CommandType.GET_CHANNEL_STATUS)) {
            MtkCatLog.d(TAG, "Will no show BIP related toast");
            return false;
        }
        return true;
    }
}
