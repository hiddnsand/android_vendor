package com.mediatek.ims.config.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.ims.ImsConfig;
import com.android.ims.ImsManager;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.ims.config.ImsConfigContract;
import com.mediatek.ims.config.internal.ImsConfigUtils;
import com.mediatek.ims.internal.MtkImsManager;
import com.mediatek.ims.OperatorUtils;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;

import mediatek.telephony.MtkCarrierConfigManager;

public class ImsConfigReceiver extends BroadcastReceiver {
    private static final String TAG = "ImsConfigReceiver";
    private static final String PROP_FORCE_DEBUG_KEY = "persist.log.tag.tel_dbg";
    private static final boolean DEBUG = TextUtils.equals(Build.TYPE, "eng")
            || (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1);

    private Handler mHandler;
    private final int mPhoneId;

    public ImsConfigReceiver(Handler handler, int phoneId) {
        super();
        mPhoneId = phoneId;
        mHandler = handler;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case TelephonyIntents.ACTION_SIM_STATE_CHANGED:
                String state = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
                int phoneId = intent.getIntExtra(PhoneConstants.PHONE_KEY,
                        SubscriptionManager.INVALID_PHONE_INDEX);
                if (phoneId == mPhoneId) {
                    Message msg;
                    Log.d(TAG, "Update LatestSimState, phoneId = " + mPhoneId +
                            ", state = " + state);
                    ImsConfigProvider.LatestSimState.put(mPhoneId, state);
                    switch (state) {
                        case IccCardConstants.INTENT_VALUE_ICC_ABSENT:
                            mHandler.removeMessages(ImsConfigStorage.MSG_SIM_ABSENT_ECC_BROADCAST);
                            msg = new Message();
                            Log.d(TAG, "Sim state changed, event = " + state +
                                    ", check for ECC flag");
                            msg.what = ImsConfigStorage.MSG_SIM_ABSENT_ECC_BROADCAST;
                            mHandler.sendMessage(msg);
                            // SIM Absent should also send MSG_RESET_BROADCAST_FLAG message
                        case IccCardConstants.INTENT_VALUE_ICC_NOT_READY:
                            mHandler.removeMessages(ImsConfigStorage.MSG_RESET_BROADCAST_FLAG);
                            msg = new Message();
                            Log.d(TAG, "Sim state changed, event = " + state +
                                    ", reset broadcast flag");
                            msg.what = ImsConfigStorage.MSG_RESET_BROADCAST_FLAG;
                            mHandler.sendMessage(msg);
                            break;
                        case IccCardConstants.INTENT_VALUE_ICC_CARD_IO_ERROR:
                        case IccCardConstants.INTENT_VALUE_ICC_UNKNOWN:
                        case IccCardConstants.INTENT_VALUE_ICC_LOCKED:
                            break;
                        case IccCardConstants.INTENT_VALUE_ICC_LOADED:
                            // reload config by CarrierConfig change notify.
                            mHandler.removeMessages(ImsConfigStorage.MSG_RESET_WFC_MODE_FLAG);
                            msg = new Message();
                            Log.d(TAG, "Sim state changed, event = " + state +
                                    ", reset wfcmode flag");
                            msg.what = ImsConfigStorage.MSG_RESET_WFC_MODE_FLAG;
                            mHandler.sendMessage(msg);
                            break;
                        case IccCardConstants.INTENT_VALUE_ICC_READY:
                            if (ImsConfigUtils.isWfcEnabledByUser(context, mPhoneId) &&
                                    (ImsConfigUtils.getWfcMode(context, mPhoneId) ==
                                    ImsConfig.WfcModeFeatureValueConstants.WIFI_ONLY)) {
                                ImsConfigUtils.sendWifiOnlyModeIntent(context, true);
                                Log.d(TAG, "Turn OFF radio, after sim ready and wfc mode is wifi_only");
                            }
                            break;
                    }

                    // Trigger to update Dynamic Ims Switch
                    if (state.equals(IccCardConstants.INTENT_VALUE_ICC_ABSENT) ||
                            state.equals(IccCardConstants.INTENT_VALUE_ICC_LOADED)) {
                        Intent mIntent = new Intent(ImsConfigContract
                                .ACTION_DYNAMIC_IMS_SWITCH_TRIGGER);
                        mIntent.putExtra(PhoneConstants.PHONE_KEY, mPhoneId);
                        mIntent.putExtra(IccCardConstants.INTENT_KEY_ICC_STATE, state);
                        context.sendBroadcast(mIntent);
                    }
                }
                break;

            case CarrierConfigManager.ACTION_CARRIER_CONFIG_CHANGED:
                handleCarrierConfigChanged(context, intent);
                break;

            case TelephonyIntents.ACTION_SET_RADIO_CAPABILITY_DONE:
                Message msg;
                if (RadioCapabilitySwitchUtil.isDssNoResetSupport()) {
                    mHandler.removeMessages(ImsConfigStorage.MSG_RESET_WFC_MODE_FLAG);
                    msg = new Message();
                    Log.d(TAG, "SET_RADIO_CAPABILITY_DONE, reset wfcmode flag phoneId:" + mPhoneId);
                    msg.what = ImsConfigStorage.MSG_RESET_WFC_MODE_FLAG;
                    mHandler.sendMessage(msg);
                }
                break;
        }
    }

    private void handleCarrierConfigChanged(Context context, Intent intent) {
        int phoneId = intent.getIntExtra(PhoneConstants.PHONE_KEY,
                SubscriptionManager.INVALID_PHONE_INDEX);
        if (phoneId != mPhoneId) {
            return;
        }
        boolean removeWfcPrefMode = ImsConfigUtils.getBooleanCarrierConfig(context,
                MtkCarrierConfigManager.MTK_KEY_WFC_REMOVE_PREFERENCE_MODE_BOOL, phoneId);
        boolean wfcModeEditable = ImsConfigUtils.getBooleanCarrierConfig(context,
                CarrierConfigManager.KEY_EDITABLE_WFC_MODE_BOOL, phoneId);
        Log.d(TAG, "KEY_WFC_REMOVE_PREFERENCE_MODE_BOOL = " + removeWfcPrefMode);
        Log.d(TAG, "KEY_EDITABLE_WFC_MODE_BOOL = " + wfcModeEditable);
        if (removeWfcPrefMode || !wfcModeEditable) {
            int wfcMode = ImsConfigUtils.getIntCarrierConfig(context,
                    CarrierConfigManager.KEY_CARRIER_DEFAULT_WFC_IMS_MODE_INT, phoneId);
            Log.d(TAG, "ACTION_CARRIER_CONFIG_CHANGED: set wfc mode = " + wfcMode);
            MtkImsManager.setWfcMode(context, wfcMode, phoneId);
        }

        int operatorCode = ImsConfigUtils.getIntCarrierConfig(context,
                MtkCarrierConfigManager.KEY_OPERATOR_ID_INT, phoneId);
        // Remove previous load event
        mHandler.removeMessages(ImsConfigStorage.MSG_LOAD_CONFIG_STORAGE);
        Message msg = new Message();
        Log.d(TAG, "carrier config changed, operatorCode = " + operatorCode +
                " on phone " + phoneId);
        msg.what = ImsConfigStorage.MSG_LOAD_CONFIG_STORAGE;
        msg.obj = operatorCode;
        mHandler.sendMessage(msg);
    }
}
