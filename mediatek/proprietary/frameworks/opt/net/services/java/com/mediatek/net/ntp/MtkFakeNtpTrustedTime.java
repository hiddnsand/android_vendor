/*
 * Copyright (C) 2017 MediaTek Inc.
 *
 * Modification based on code covered by the mentioned copyright
 * and/or permission notice(s).
 */
/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mediatek.net.ntp;

import android.content.Context;
import android.util.Log;
import android.util.TrustedTime;

/**
 * MtkFakeNtpTrustedTime that connects with a dummy NTP server as its trusted
 * time source. This is a dummy NTP class.
 *
 * @hide
 */
public class MtkFakeNtpTrustedTime implements TrustedTime {
    private static final String TAG = "MtkFakeNtpTrustedTime";
    private static final boolean LOGD = true;

    private static MtkFakeNtpTrustedTime sSingleton;

    private MtkFakeNtpTrustedTime(String server, long timeout) {
        if (LOGD) Log.d(TAG, "creating MtkFakeNtpTrustedTime using " + server);
    }

    public static synchronized MtkFakeNtpTrustedTime getInstance(Context context) {
        if (sSingleton == null) {
            sSingleton = new MtkFakeNtpTrustedTime(null, 0);
        }
        return sSingleton;
    }

    @Override
    public boolean forceRefresh() {
        return true;
    }

    @Override
    public boolean hasCache() {
        return false;
    }

    @Override
    public long getCacheAge() {
        return 0L;
    }

    @Override
    public long getCacheCertainty() {
        return 0L;
    }

    @Override
    public long currentTimeMillis() {
        return 0L;
    }
}
