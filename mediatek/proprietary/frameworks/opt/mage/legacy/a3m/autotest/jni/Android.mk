$(info A3M-AUTOTEST-JNI ANDROID.MK)
#
# Used by NDK builds and by makeMtk builds.
#
#

LOCAL_PATH := $(call my-dir)/..

include $(CLEAR_VARS)

# Generate source files containing built-in memory files
$(shell python $(LOCAL_PATH)/../buildtools/genmemfiles.py \
    -n autotest -i $(LOCAL_PATH)/testassets -o $(LOCAL_PATH)/testassets)

# Here we give our module name and source file(s)
LOCAL_MODULE := liba3mautotest

LOCAL_MODULE_TAGS := eng

TARGET_PLATFORM := android-9

LOCAL_CFLAGS := \
    -Werror \
    -Wall \
    -Wextra \
    -Wformat=2 \
    -Winit-self \
    -Wlogical-op \
    -Wmissing-include-dirs \
    -Wno-conversion-null \
    -Wno-multichar \
    -Wno-unused-parameter \
    -DGTEST_HAS_EXCEPTIONS=0 \
    -DGTEST_HAS_RTTI=0 \
    -DGTEST_HAS_TR1_TUPLE=0 \
    -DGTEST_OUTPUT="xml" \

LOCAL_CPPFLAGS := \
    -Wsign-promo \

LOCAL_SRC_FILES := \
    jni/autotest_wrap.cpp \
    src/autotest_animation.cpp \
    src/autotest_appearance.cpp \
    src/autotest_background.cpp \
    src/autotest_camera.cpp \
    src/autotest_image.cpp \
    src/autotest_light.cpp \
    src/autotest_main.cpp \
    src/autotest_mesh.cpp \
    src/autotest_plane.cpp \
    src/autotest_quaternion.cpp \
    src/autotest_rendercontext.cpp \
    src/autotest_renderdevice.cpp \
    src/autotest_rendertarget.cpp \
    src/autotest_scenenode.cpp \
    src/autotest_scopedptr.cpp \
    src/autotest_shaderprogram.cpp \
    src/autotest_sharedptr.cpp \
    src/autotest_sphere.cpp \
    src/autotest_square.cpp \
    src/autotest_texture.cpp \
    src/autotest_version.cpp \
    src/autotest_vertexbuffer.cpp \
    src/gtest/gtest-all.cpp \
    testassets/src/memfilesource_autotest.cpp

LOCAL_C_INCLUDES := \
    $(JNI_H_INCLUDE) \
    $(LOCAL_PATH)/../engine/collision/api \
    $(LOCAL_PATH)/../engine/common/api \
    $(LOCAL_PATH)/../engine/facility/api \
    $(LOCAL_PATH)/../engine/facility/src \
    $(LOCAL_PATH)/../engine/maths/api \
    $(LOCAL_PATH)/../engine/pss/api \
    $(LOCAL_PATH)/../engine/render/api \
    $(LOCAL_PATH)/src \
    $(LOCAL_PATH)/testassets/src \
    bionic \
    external/stlport/stlport

# This line required for NDK build (Note no -lstlport)
LOCAL_LDFLAGS = -L$(LOCAL_PATH)/../engine/libs/armeabi \
                -landroid \
                -la3m \
                -lcutils

LOCAL_PRELINK_MODULE := false

# This line needed for makeMtk build
LOCAL_SHARED_LIBRARIES := libGLESv2 \
                          libandroid \
                          liba3m \
                          libcutils \
                          libstlport \

include $(BUILD_SHARED_LIBRARY)
