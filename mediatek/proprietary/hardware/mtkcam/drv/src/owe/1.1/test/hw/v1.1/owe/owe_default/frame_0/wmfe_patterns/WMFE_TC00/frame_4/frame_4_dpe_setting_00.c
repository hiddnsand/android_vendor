#include "frame_4_dpe_setting_00.h"
#include "memmap.h"
#include "dpe_common.h"
#include "mmu.h"
#include "api.h"

#define DVE_ENABLE 0x0
#define WMFE_ENABLE 0x1
#define WMFE_ENABLE_0 0x1
#define WMFE_ENABLE_1 0x1
#define WMFE_ENABLE_2 0x1
#define WMFE_MASK_EN_0 0x1
#define WMFE_MASK_EN_1 0x1
#define WMFE_MASK_EN_2 0x0
int frame_4_golden_l_start_x = 0;
int frame_4_golden_l_start_y = 0;
int frame_4_golden_l_end_x = 0;
int frame_4_golden_l_end_y = 0;
int frame_4_golden_r_start_x = 0;
int frame_4_golden_r_start_y = 0;
int frame_4_golden_r_end_x = 0;
int frame_4_golden_r_end_y = 0;
MUINT32 frame_4_golden_DVE_HORZ_SV = 0;
MUINT32 frame_4_golden_DVE_VERT_SV = 0;


bool frame_4_DPE_Config()
{


	//TODO: Manually enable IMGSYS CG here
	
	//TODO: Manually Disable SMI LARB here
	
	int offset;                                                     
	#define IMGSYS_LARB 0x15017000                                  
	remap_mem_range(0x14020000, 0x14020000,0x3FFFFFFF, MT_UNCACHED);
	remap_mem_range(DPE_BASE, DPE_BASE,0x1000, MT_UNCACHED);        
	                                                                
	remap_mem_range(IMGSYS_LARB, IMGSYS_LARB,0x1000, MT_UNCACHED);  
	for (offset=0; offset < 32; offset++){                          
	    pUtilityDPEDrv->writeDPEReg(IMGSYS_LARB+0x380+offset*4,0x0);
	    pUtilityDPEDrv->writeDPEReg(IMGSYS_LARB+0xF80+offset*4,0x0);
	}                                                               
	                                                                
	remap_mem_range(SMI_LARB5, SMI_LARB5,0x1000, MT_UNCACHED);      
	for (offset=0; offset < 25; offset++){                          
	    pUtilityDPEDrv->writeDPEReg(SMI_LARB5+0x380+offset*4,0x0);  
	}                                                               
	                                                                
	remap_mem_range(SMI_LARB2, SMI_LARB2,0x1000, MT_UNCACHED);      
	for (offset=0; offset < 3; offset++){                           
	    pUtilityDPEDrv->writeDPEReg(SMI_LARB2+0x380+offset*4,0x0);  
	}                                                               
	
	
	int golden_l_start_x = frame_4_golden_l_start_x;
	int golden_l_start_y = frame_4_golden_l_start_y;
	int golden_l_end_x = frame_4_golden_l_end_x;
	int golden_l_end_y = frame_4_golden_l_end_y;
	int golden_r_start_x = frame_4_golden_r_start_x;
	int golden_r_start_y = frame_4_golden_r_start_y;
	int golden_r_end_x = frame_4_golden_r_end_x;
	int golden_r_end_y = frame_4_golden_r_end_y;
	MUINT32 golden_DVE_HORZ_SV = frame_4_golden_DVE_HORZ_SV;
	MUINT32 golden_DVE_VERT_SV = frame_4_golden_DVE_VERT_SV;
	//copy the pointer of golden answer of DPE
	unsigned long golden_dpe_dvo_l_frame;
	unsigned long golden_dpe_dvo_r_frame;
	unsigned long golden_dpe_confo_l_frame;
	unsigned long golden_dpe_confo_r_frame;
	unsigned long golden_dpe_respo_l_frame;
	unsigned long golden_dpe_respo_r_frame;
	unsigned long golden_dpe_wmf_dpo_frame_0;
	unsigned long golden_dpe_wmf_dpo_frame_1;
	unsigned long golden_dpe_wmf_dpo_frame_2;
	getframe_4GoldPointer(
	&golden_dpe_dvo_l_frame,
	&golden_dpe_dvo_r_frame,
	&golden_dpe_confo_l_frame,
	&golden_dpe_confo_r_frame,
	&golden_dpe_respo_l_frame,
	&golden_dpe_respo_r_frame,
	&golden_dpe_wmf_dpo_frame_0,
	&golden_dpe_wmf_dpo_frame_1,
	&golden_dpe_wmf_dpo_frame_2
	);
	//input frame pointer
	char* dpe_imgi_l_frame;
	char* dpe_imgi_r_frame;
	char* dpe_dvi_l_frame;
	char* dpe_dvi_r_frame;
	char* dpe_maski_l_frame;
	char* dpe_maski_r_frame;
	char* dpe_wmf_imgi_frame_0;
	char* dpe_wmf_dpi_frame_0;
	char* dpe_wmf_tbli_frame_0;
	char* dpe_wmf_maski_frame_0;
	char* dpe_wmf_imgi_frame_1;
	char* dpe_wmf_dpi_frame_1;
	char* dpe_wmf_tbli_frame_1;
	char* dpe_wmf_maski_frame_1;
	char* dpe_wmf_imgi_frame_2;
	char* dpe_wmf_dpi_frame_2;
	char* dpe_wmf_tbli_frame_2;
	char* dpe_wmf_maski_frame_2;
#if DVE_ENABLE
	dpe_imgi_l_frame = &frame_4_dpe_imgi_l_frame_00[0];
	dpe_imgi_r_frame = &frame_4_dpe_imgi_r_frame_00[0];
	dpe_dvi_l_frame = &frame_4_dpe_dvi_l_frame_00[0];
	dpe_dvi_r_frame = &frame_4_dpe_dvi_r_frame_00[0];
	dpe_maski_l_frame = &frame_4_dpe_maski_l_frame_00[0];
	dpe_maski_r_frame = &frame_4_dpe_maski_r_frame_00[0];
#endif
#if WMFE_ENABLE_0
	dpe_wmf_imgi_frame_0 = &frame_4_in_dpe_wmf_imgi_frame_0[0];
	dpe_wmf_dpi_frame_0 = &frame_4_in_dpe_wmf_dpi_frame_0[0];
	dpe_wmf_tbli_frame_0 = &frame_4_in_dpe_wmf_tbli_frame_0[0];
#if WMFE_MASK_EN_0
	dpe_wmf_maski_frame_0 = &frame_4_in_dpe_wmf_maski_frame_0[0];
#endif
#endif
#if WMFE_ENABLE_1
	dpe_wmf_imgi_frame_1 = &frame_4_in_dpe_wmf_imgi_frame_1[0];
	dpe_wmf_dpi_frame_1 = &frame_4_in_dpe_wmf_dpi_frame_1[0];
	dpe_wmf_tbli_frame_1 = &frame_4_in_dpe_wmf_tbli_frame_1[0];
#if WMFE_MASK_EN_1
	dpe_wmf_maski_frame_1 = &frame_4_in_dpe_wmf_maski_frame_1[0];
#endif
#endif
#if WMFE_ENABLE_2
	dpe_wmf_imgi_frame_2 = &frame_4_in_dpe_wmf_imgi_frame_2[0];
	dpe_wmf_dpi_frame_2 = &frame_4_in_dpe_wmf_dpi_frame_2[0];
	dpe_wmf_tbli_frame_2 = &frame_4_in_dpe_wmf_tbli_frame_2[0];
#if WMFE_MASK_EN_2
	dpe_wmf_maski_frame_2 = &frame_4_in_dpe_wmf_maski_frame_2[0];
#endif
#endif

	//allocate the memory to be used the Target of DPE
	char* dpe_dvo_l_frame;
	char* dpe_dvo_r_frame;
	char* dpe_confo_l_frame;
	char* dpe_confo_r_frame;
	char* dpe_respo_l_frame;
	char* dpe_respo_r_frame;
	char* dpe_wmf_dpo_frame_0;
	char* dpe_wmf_dpo_frame_1;
	char* dpe_wmf_dpo_frame_2;

	printf("#########################################################\n");
	printf("###########frame_4Start to Test !!!!###########\n");
	printf("#########################################################\n");
#if DVE_ENABLE
	dpe_dvo_l_frame = (char* )MEM_Allocate_NC( frame_4_golden_dpe_dvo_l_size, MEM_USER_CAM);
	dpe_dvo_r_frame = (char* )MEM_Allocate_NC( frame_4_golden_dpe_dvo_r_size, MEM_USER_CAM);
	dpe_confo_l_frame = (char* )MEM_Allocate_NC( frame_4_golden_dpe_confo_l_size, MEM_USER_CAM);
	dpe_confo_r_frame = (char* )MEM_Allocate_NC( frame_4_golden_dpe_confo_r_size, MEM_USER_CAM);
	dpe_respo_l_frame = (char* )MEM_Allocate_NC( frame_4_golden_dpe_respo_l_size, MEM_USER_CAM);
	dpe_respo_r_frame = (char* )MEM_Allocate_NC( frame_4_golden_dpe_respo_r_size, MEM_USER_CAM);
#endif
#if WMFE_ENABLE_0
	dpe_wmf_dpo_frame_0 = (char* )MEM_Allocate_NC( frame_4_golden_dpe_wmf_dpo_0_size, MEM_USER_CAM);
#endif
#if WMFE_ENABLE_1
	dpe_wmf_dpo_frame_1 = (char* )MEM_Allocate_NC( frame_4_golden_dpe_wmf_dpo_1_size, MEM_USER_CAM);
#endif
#if WMFE_ENABLE_2
	dpe_wmf_dpo_frame_2 = (char* )MEM_Allocate_NC( frame_4_golden_dpe_wmf_dpo_2_size, MEM_USER_CAM);
#endif
#if DVE_ENABLE
	printf("golden_dpe_dvo_l_size:%d, golden_dpe_dvo_r_size:%d\n", frame_4_golden_dpe_dvo_l_size, frame_4_golden_dpe_dvo_r_size);
	printf("golden_dpe_confo_l_size:%d, golden_dpe_confo_r_size:%d\n",frame_4_golden_dpe_confo_l_size , frame_4_golden_dpe_confo_r_size);
	printf("golden_dpe_respo_l_size:%d, golden_dpe_respo_r_size:%d\n",frame_4_golden_dpe_respo_l_size , frame_4_golden_dpe_respo_r_size);

#endif
	//Disable m4u MMU
	pUtilityDPEDrv->writeDPEReg( 0x14020008, 0xFFFFFFFF);
	pUtilityDPEDrv->writeDPEReg( 0x1402c004 , 0x0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c008 , 0x80000001 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c8ac , 0x80000020 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c8b0 , 0x200020 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c8b4 , 0x200020 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c84c , 0x80000020 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c850 , 0x200020 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c854 , 0x200020 );
	//
	//Start WMFE thread
	pUtilityDPEDrv->writeDPEReg( 0x1402c224 , 0x80000001 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c230 , 0xff0d31e1 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c234 , 0x8700f0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c238 , (MUINT32)dpe_wmf_imgi_frame_0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c23c , 0x1e0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c240 , (MUINT32)dpe_wmf_dpi_frame_0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c244 , 0xf0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c248 , (MUINT32)dpe_wmf_tbli_frame_0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c24c , 0x100 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c250 , (MUINT32)dpe_wmf_dpi_frame_0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c254 , 0xf0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c258 , (MUINT32)dpe_wmf_dpo_frame_0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c25c , 0xf0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c270 , 0xff0e31e1 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c274 , 0x8700f0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c278 , (MUINT32)dpe_wmf_imgi_frame_1 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c27c , 0x1e0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c280 , (MUINT32)dpe_wmf_dpi_frame_1 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c284 , 0xf0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c288 , (MUINT32)dpe_wmf_tbli_frame_1 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c28c , 0x100 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c290 , (MUINT32)dpe_wmf_dpi_frame_1 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c294 , 0xf0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c298 , (MUINT32)dpe_wmf_dpo_frame_1 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c29c , 0xf0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2b0 , 0xff083121 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2b4 , 0x8700f0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2b8 , (MUINT32)dpe_wmf_imgi_frame_2 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2bc , 0x1e0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2c0 , (MUINT32)dpe_wmf_dpi_frame_2 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2c4 , 0xf0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2c8 , (MUINT32)dpe_wmf_tbli_frame_2 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2cc , 0x100 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2d0 , (MUINT32)dpe_wmf_dpi_frame_2 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2d4 , 0xf0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2d8 , (MUINT32)dpe_wmf_dpo_frame_2 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2dc , 0xf0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2f0 , 0xff083521 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2f4 , 0x10e01e0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2f8 , 0x22000000 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c2fc , 0x3c0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c300 , 0x23000000 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c304 , 0x3c0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c308 , 0x24000000 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c30c , 0x100 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c310 , 0x33000000 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c314 , 0x1e0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c318 , 0x25000000 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c31c , 0x1e0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c330 , 0xff083131 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c334 , 0x10e01e0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c338 , 0x26000000 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c33c , 0x3c0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c340 , 0x27000000 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c344 , 0xf0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c348 , 0x28000000 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c34c , 0x100 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c350 , 0x34000000 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c354 , 0x1e0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c358 , 0x29000000 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c35c , 0x1e0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c4f8 , 0x0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c4fc , 0x0 );
	pUtilityDPEDrv->writeDPEReg( 0x1402c220 , 0x1 );



#if DVE_ENABLE
	pUtilityDPEDrv->writeDPEReg( 0x14028020 , 0x00000001 );
	MUINT32 DVE_INT_STATUS;
	do
	{
		DVE_INT_STATUS = pUtilityDPEDrv->readDPEReg(0x14028028);
		CTP_Wait_usec(10);
		if (DVE_INT_STATUS & 0x1 == 0x01)
		{
			pUtilityDPEDrv->writeDPEReg(0x14028028, DVE_INT_STATUS); //write clear
			break;
		}
	}while (1);
#endif

#if WMFE_ENABLE
	pUtilityDPEDrv->writeDPEReg( 0x1402C220 , 0x00000001 );
	MUINT32 WMFE_INT_STATUS;
	do
	{
		WMFE_INT_STATUS = pUtilityDPEDrv->readDPEReg(0x1402C228);
		CTP_Wait_usec(10);
		if (WMFE_INT_STATUS & 0x1 == 0x01)
		{
			pUtilityDPEDrv->writeDPEReg(0x1402C228, WMFE_INT_STATUS); //write clear
			break;
		}
	}while (1);
#endif

	

	MUINT32 REG_DPE_DVE_ORG_SIZE = pUtilityDPEDrv->readDPEReg(DPE_DVE_ORG_SIZE);
	MUINT32 REG_DPE_DVE_CTRL = pUtilityDPEDrv->readDPEReg(DPE_DVE_CTRL);
	MUINT32 DVE_ORG_WDITH = (REG_DPE_DVE_ORG_SIZE & 0x1FFF);
	MUINT32 DVE_ORG_HEIGHT = ((REG_DPE_DVE_ORG_SIZE >> 16) & 0x1FFF);
	MUINT32 DVE_HORZ_DS_MODE = (REG_DPE_DVE_CTRL & 0x2) >> 1;
	MUINT32 DVE_VERT_DS_MODE = (REG_DPE_DVE_CTRL & 0x4) >> 2;
	INT32 int_data_dma_0, int_data_dma_1, int_data_dma_2, int_data_dma_3;
	MUINT32 blk_width;
	MUINT32 blk_height;
	MUINT32 golden_l_start_blk_x;
	INT32 golden_l_end_blk_x;
	MUINT32 golden_l_start_blk_y;
	INT32 golden_l_end_blk_y;

	MUINT32 golden_r_start_blk_x;
	INT32 golden_r_end_blk_x;
	MUINT32 golden_r_start_blk_y;
	INT32 golden_r_end_blk_y;

	int err_cnt_dma;
	MUINT32 REG_DPE_WMFE_SIZE_0 = pUtilityDPEDrv->readDPEReg(DPE_WMFE_SIZE_0);
	MUINT32 REG_DPE_WMFE_SIZE_1 = pUtilityDPEDrv->readDPEReg(DPE_WMFE_SIZE_1);
	MUINT32 REG_DPE_WMFE_SIZE_2 = pUtilityDPEDrv->readDPEReg(DPE_WMFE_SIZE_2);
	MUINT32 wmfe_curr_width;
	MUINT32 wmfe_curr_height;

	if ( 0 == DVE_HORZ_DS_MODE)
	{
		blk_width = (DVE_ORG_WDITH+7) >> 3;
		golden_l_start_blk_x = (golden_l_start_x >> 3);
		if (golden_l_start_x & 0x7)
		{
			golden_l_start_blk_x += 1;
		}
		golden_l_end_blk_x  = (golden_l_end_x >> 3);
		golden_r_start_blk_x = (golden_r_start_x >> 3);
		if (golden_r_start_x & 0x7)
		{
			golden_r_start_blk_x += 1;
		}
		golden_r_end_blk_x  = (golden_r_end_x >> 3);
	}
	else
	{
		blk_width = (DVE_ORG_WDITH+3) >> 2;
		golden_l_start_blk_x = (golden_l_start_x >> 2);
		if (golden_l_start_x & 0x3)
		{
			golden_l_start_blk_x += 1;
		}
		golden_l_end_blk_x  = (golden_l_end_x >> 2);
		golden_r_start_blk_x = (golden_r_start_x >> 2);
		if (golden_r_start_x & 0x3)
		{
			golden_r_start_blk_x += 1;
		}
		golden_r_end_blk_x  = (golden_r_end_x >> 2);
	}
	
	if ( 0 == DVE_VERT_DS_MODE)
	{
		blk_height = (DVE_ORG_HEIGHT+7) >> 3;
		golden_l_start_blk_y = (golden_l_start_y >> 3);
		if (golden_l_start_y & 0x7)
		{
			golden_l_start_blk_y += 1;
		}
		golden_l_end_blk_y  = (golden_l_end_y >> 3);
		golden_r_start_blk_y = (golden_r_start_y >> 3);
		if (golden_r_start_y & 0x7)
		{
			golden_r_start_blk_y += 1;
		}
		golden_r_end_blk_y  = (golden_r_end_y >> 3);

	}
	else
	{
		blk_height = (DVE_ORG_HEIGHT+3) >> 2;
		golden_l_start_blk_y = (golden_l_start_y >> 2);
		if (golden_l_start_y & 0x3)
		{
			golden_l_start_blk_y += 1;
		}
		golden_l_end_blk_y  = (golden_l_end_y >> 2);
		golden_r_start_blk_y = (golden_r_start_y >> 2);
		if (golden_r_start_y & 0x3)
		{
			golden_r_start_blk_y += 1;
		}
		golden_r_end_blk_y  = (golden_r_end_y >> 2);

	}

	if (golden_l_end_blk_x == blk_width)
		golden_l_end_blk_x = blk_width - 1;
	if (golden_l_end_blk_y == blk_height)
		golden_l_end_blk_y = blk_height - 1; 
	if (golden_r_end_blk_x == blk_width)
		golden_r_end_blk_x = blk_width - 1;
	if (golden_r_end_blk_y == blk_height)
		golden_r_end_blk_y = blk_height - 1; 
		
	//Compare dpe_confo_l_frame_
#if DVE_ENABLE
	int_data_dma_0 = golden_l_start_blk_x;
	int_data_dma_1 = golden_l_start_blk_y;
	if (golden_l_end_x < 0)
	{
		int_data_dma_2 = -1;
	}
	else
	{
		int_data_dma_2 = golden_l_end_blk_x;
	}
	if (golden_l_end_y < 0)
	{
		int_data_dma_3 = -1;
	}
	else
	{
		int_data_dma_3 = golden_l_end_blk_y;
	}		
		
	err_cnt_dma = comp_roi_mem_with_file(golden_dpe_confo_l_frame, 
							1, 
							pUtilityDPEDrv->readDPEReg(DPE_DVE_CONFO_L_BASE_ADDR),
							blk_width,
							blk_height,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_CONFO_L_STRIDE),
							int_data_dma_0,
							int_data_dma_1,
							int_data_dma_2,
							int_data_dma_3,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_MASKI_L_BASE_ADDR),
							1,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_MASKI_L_STRIDE)
							);	
	if (err_cnt_dma)
	{
		//Error
		printf("dpe left confo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
	}
	else
	{
		//Pass
		printf("dpe left confo bit true pass!!!\n");
	}

	//Compare the dpe_respo_l_frame_
	err_cnt_dma = comp_roi_mem_with_file(golden_dpe_respo_l_frame, 
							1, 
							pUtilityDPEDrv->readDPEReg(DPE_DVE_RESPO_L_BASE_ADDR),
							blk_width,
							blk_height,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_RESPO_L_STRIDE),
							int_data_dma_0,
							int_data_dma_1,
							int_data_dma_2,
							int_data_dma_3,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_MASKI_L_BASE_ADDR),
							1,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_MASKI_L_STRIDE)
							);	
	if (err_cnt_dma)
	{
		//Error
		printf("dpe left respo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
	}
	else
	{
		//Pass
		printf("dpe left respo bit true pass!!!\n");
	}



	//Compare dpe_confo_r_frame_
	int_data_dma_0 = golden_r_start_blk_x;
	int_data_dma_1 = golden_r_start_blk_y;
	if (golden_r_end_x < 0)
	{
		int_data_dma_2 = -1;
	}
	else
	{
		int_data_dma_2 = golden_r_end_blk_x;
	}
	if (golden_r_end_y < 0)
	{
		int_data_dma_3 = -1;
	}
	else
	{
		int_data_dma_3 = golden_r_end_blk_y;
	}		
	comp_roi_mem_with_file(golden_dpe_confo_r_frame, 
							1, 
							pUtilityDPEDrv->readDPEReg(DPE_DVE_CONFO_R_BASE_ADDR),
							blk_width,
							blk_height,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_CONFO_R_STRIDE),
							int_data_dma_0,
							int_data_dma_1,
							int_data_dma_2,
							int_data_dma_3,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_MASKI_R_BASE_ADDR),
							1,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_MASKI_R_STRIDE)
							);	
	if (err_cnt_dma)
	{
		//Error
		printf("dpe right confo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
	}
	else
	{
		//Pass
		printf("dpe right confo bit true pass!!!\n");
	}


	//Compare the dpe_respo_r_frame_
	err_cnt_dma = comp_roi_mem_with_file(golden_dpe_respo_r_frame, 
							1, 
							pUtilityDPEDrv->readDPEReg(DPE_DVE_RESPO_R_BASE_ADDR),
							blk_width,
							blk_height,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_RESPO_R_STRIDE),
							int_data_dma_0,
							int_data_dma_1,
							int_data_dma_2,
							int_data_dma_3,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_MASKI_R_BASE_ADDR),
							1,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_MASKI_R_STRIDE)
							);	
	if (err_cnt_dma)
	{
		//Error
		printf("dpe right respo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
	}
	else
	{
		//Pass
		printf("dpe right respo bit true pass!!!\n");
	}


	//Compare the dpe_dvo_l_frame_
	err_cnt_dma = comp_roi_mem_with_file(golden_dpe_dvo_l_frame, 
							0, 
							pUtilityDPEDrv->readDPEReg(DPE_DVE_DVO_L_BASE_ADDR),
							(blk_width << 1),
							blk_height,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_DVO_L_STRIDE),
							0,
							0,
							((blk_width << 1)-1),
							blk_height-1,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_MASKI_L_BASE_ADDR),
							2,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_MASKI_L_STRIDE)
							);	
	if (err_cnt_dma)
	{
		//Error
		printf("dpe left dvo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
	}
	else
	{
		//Pass
		printf("dpe left dvo bit true pass!!!\n");
	}


	//Compare the dpe_dvo_r_frame_
	err_cnt_dma = comp_roi_mem_with_file(golden_dpe_dvo_r_frame, 
							0, 
							pUtilityDPEDrv->readDPEReg(DPE_DVE_DVO_R_BASE_ADDR),
							(blk_width << 1),
							blk_height,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_DVO_R_STRIDE),
							0,
							0,
							((blk_width << 1)-1),
							blk_height-1,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_MASKI_R_BASE_ADDR),
							2,
							pUtilityDPEDrv->readDPEReg(DPE_DVE_MASKI_R_STRIDE)
							);	
	if (err_cnt_dma)
	{
		//Error
		printf("dpe right dvo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
	}
	else
	{
		//Pass
		printf("dpe right dvo bit true pass!!!\n");
	}


	//Compare the DVE Statistic Result 0
	MUINT32 REG_DPE_DVE_STA = pUtilityDPEDrv->readDPEReg(DPE_DVE_STA);
	MUINT32 DVE_HORZ_SV = REG_DPE_DVE_STA & 0x3ff;
	MUINT32 DVE_VERT_SV = (REG_DPE_DVE_STA >> 16) & 0x3f;

	if ( (golden_DVE_HORZ_SV == DVE_HORZ_SV) &&
		 (golden_DVE_VERT_SV == DVE_VERT_SV) )
	{
		//Pass
		printf("dpe DVE Statistic Result 0 bit true pass!!!\n");
	}
	else
	{
		//Erro
		printf("dpe DVE Statistic Result 0 bit true fail, DVE_HORZ_SV:(%d), DVE_VERT_SV:(%d)!!!\n", DVE_HORZ_SV, DVE_VERT_SV);
	}
#endif

	//Start WMFE DRAM comparison !!
	//Compare dpe_wmf_dpo_frame_ 0
#if WMFE_ENABLE_0
	int_data_dma_0 = 0;
	int_data_dma_1 = 0;
	wmfe_curr_width = (REG_DPE_WMFE_SIZE_0 & 0xfff);
	wmfe_curr_height = ((REG_DPE_WMFE_SIZE_0 >> 16) & 0xfff);
	int_data_dma_2 = wmfe_curr_width-1;
	int_data_dma_3 = wmfe_curr_height-1;

	err_cnt_dma = comp_roi_mem_with_file(golden_dpe_wmf_dpo_frame_0, 
							0, 
							pUtilityDPEDrv->readDPEReg(DPE_WMFE_DPO_BASE_ADDR_0),
							wmfe_curr_width,
							wmfe_curr_height,
							pUtilityDPEDrv->readDPEReg(DPE_WMFE_DPO_STRIDE_0),
							int_data_dma_0,
							int_data_dma_1,
							int_data_dma_2,
							int_data_dma_3,
							0,
							0,
							0
							);	
	if (err_cnt_dma)
	{
		//Error
		printf("dpe WMFE DPO Frame0 bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
	}
	else
	{
		//Pass
		printf("dpe WMFE DPO Frame0 bit true pass!!!\n");
	}
#endif

	//Compare dpe_wmf_dpo_frame_ 1
#if WMFE_ENABLE_1
	int_data_dma_0 = 0;
	int_data_dma_1 = 0;
	wmfe_curr_width = (REG_DPE_WMFE_SIZE_1 & 0xfff);
	wmfe_curr_height = ((REG_DPE_WMFE_SIZE_1 >> 16) & 0xfff);
	int_data_dma_2 = wmfe_curr_width-1;
	int_data_dma_3 = wmfe_curr_height-1;

	err_cnt_dma = comp_roi_mem_with_file(golden_dpe_wmf_dpo_frame_1, 
							0, 
							pUtilityDPEDrv->readDPEReg(DPE_WMFE_DPO_BASE_ADDR_1),
							wmfe_curr_width,
							wmfe_curr_height,
							pUtilityDPEDrv->readDPEReg(DPE_WMFE_DPO_STRIDE_1),
							int_data_dma_0,
							int_data_dma_1,
							int_data_dma_2,
							int_data_dma_3,
							0,
							0,
							0
							);	
	if (err_cnt_dma)
	{
		//Error
		printf("dpe WMFE DPO Frame1 bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
	}
	else
	{
		//Pass
		printf("dpe WMFE DPO Frame1 bit true pass!!!\n");
	}
#endif

	//Compare dpe_wmf_dpo_frame_ 2
#if WMFE_ENABLE_2
	int_data_dma_0 = 0;
	int_data_dma_1 = 0;
	wmfe_curr_width = (REG_DPE_WMFE_SIZE_2 & 0xfff);
	wmfe_curr_height = ((REG_DPE_WMFE_SIZE_2 >> 16) & 0xfff);
	int_data_dma_2 = wmfe_curr_width-1;
	int_data_dma_3 = wmfe_curr_height-1;

	err_cnt_dma = comp_roi_mem_with_file(golden_dpe_wmf_dpo_frame_2, 
							0, 
							pUtilityDPEDrv->readDPEReg(DPE_WMFE_DPO_BASE_ADDR_2),
							wmfe_curr_width,
							wmfe_curr_height,
							pUtilityDPEDrv->readDPEReg(DPE_WMFE_DPO_STRIDE_2),
							int_data_dma_0,
							int_data_dma_1,
							int_data_dma_2,
							int_data_dma_3,
							0,
							0,
							0
							);	
	if (err_cnt_dma)
	{
		//Error
		printf("dpe WMFE DPO Frame2 bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
	}
	else
	{
		//Pass
		printf("dpe WMFE DPO Frame2 bit true pass!!!\n");
	}
#endif


#if DVE_ENABLE
	MEM_Release_NC( (void*)dpe_dvo_l_frame);
	MEM_Release_NC( (void*)dpe_dvo_r_frame);
	MEM_Release_NC( (void*)dpe_confo_l_frame);
	MEM_Release_NC( (void*)dpe_confo_r_frame);
	MEM_Release_NC( (void*)dpe_respo_l_frame);
	MEM_Release_NC( (void*)dpe_respo_r_frame);
#endif
#if WMFE_ENABLE_0
	MEM_Release_NC( (void*)dpe_wmf_dpo_frame_0);
#endif
#if WMFE_ENABLE_1
	MEM_Release_NC( (void*)dpe_wmf_dpo_frame_1);
#endif
#if WMFE_ENABLE_2
	MEM_Release_NC( (void*)dpe_wmf_dpo_frame_2);
#endif

	return true;
}
