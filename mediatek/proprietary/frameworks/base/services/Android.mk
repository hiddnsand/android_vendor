LOCAL_PATH:= $(call my-dir)

# merge all required services into one jar
# ============================================================
include $(CLEAR_VARS)

LOCAL_MODULE := mediatek-services
#LOCAL_DEX_PREOPT_APP_IMAGE := true
#LOCAL_DEX_PREOPT_GENERATE_PROFILE := true
#LOCAL_DEX_PREOPT_PROFILE_CLASS_LISTING := $(LOCAL_PATH)/art-profile

LOCAL_SRC_FILES := $(call all-java-files-under,core/java/com/mediatek/server)
LOCAL_SRC_FILES += core/java/com/mediatek/server/anr/EventLogTags.logtags
LOCAL_SRC_FILES += $(call all-java-files-under,core/java/com/mediatek/stk)

# Uncomment to enable output of certain warnings (deprecated, unchecked)
# LOCAL_JAVACFLAGS := -Xlint

# Services that will be built as part of meidatek-services.jar
# These should map to directory names relative to this
# Android.mk.
services := \
       mtksearchengineservice \
       datashapingservice \

# The convention is to name each service module 'services.$(module_name)'
LOCAL_STATIC_JAVA_LIBRARIES := $(addprefix services.,$(services)) \
    vendor.mediatek.hardware.power-V1.1-java-static \
    duraspeed

LOCAL_JAVA_LIBRARIES := services.core mediatek-framework \
    android.hardware.power-V1.0-java \
    vendor.mediatek.hardware.power-V1.1-java
LOCAL_STATIC_JAVA_LIBRARIES += com_mediatek_amplus

LOCAL_MODULE_PATH = $(PRODUCT_OUT)/system/framework

include $(BUILD_JAVA_LIBRARY)


ifeq (,$(ONE_SHOT_MAKEFILE))
# A full make is happening, so make everything.
include $(call all-makefiles-under,$(LOCAL_PATH))
else
# If we ran an mm[m] command, we still want to build the individual
# services that we depend on. This differs from the above condition
# by only including service makefiles and not any tests or other
# modules.
include $(patsubst %,$(LOCAL_PATH)/%/Android.mk,$(services))
endif

