package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class GameRotationSensorActivity extends CustomerSensorBaseActivity {

    public GameRotationSensorActivity() {
        super(Utils.KEY_GAME_ROTATION_VECTOR_STATUS, Sensor.TYPE_GAME_ROTATION_VECTOR);
    }
}
