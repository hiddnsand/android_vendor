#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>

#include <mrdump_user.h>
#include <mrdump_support_ext4.h>

static void usage(const char *prog) __attribute__((noreturn));
static void usage(const char *prog)
{
    printf("Usage\n"
           "\t%1$s is-supported\n\n"
	   "\t%1$s status-get\n"
	   "\t%1$s status-log\n"
	   "\t%1$s status-clear\n\n"
	   "\t%1$s file-setup\n"
	   "\t%1$s file-allocate n\n"
	   "\t\tn is 0(disable file output) 1(halfmem) 2(fullmem) >256\n"
	   "\t%1$s file-extract-core [-r] filename\n"
	   "\t\t-r\tre-init pre-allocated file\n"
	   "\t%1$s file-info\n\n"
	   "\t%1$s mem-size-set n\n"
	   "\t\tn is between 64 ~ 16384(m), 0 is output total-mem-size\n\n"
	   "\t%1$s output-set output\n"
	   "\t\tsetting mrdump output device (none, null, internal-storage:ext4, internal-storage:vfat)\n",
	   prog);
    exit(1);
}

static void error(const char *msg, ...) __attribute__((noreturn));
static void error(const char *msg, ...)
{
    va_list ap;
    char msgbuf[128];

    va_start(ap, msg);
    vsnprintf(msgbuf, sizeof(msgbuf), msg, ap);
    printf("Error: %s", msgbuf);
    exit(2);
}

static void dump_status_ok(const struct mrdump_status_result *result)
{
    printf("Ok\n");
    printf("\tMode: %s\n\tOutput: ", result->mode);

    switch (result->output) {
    case MRDUMP_OUTPUT_NULL:
        printf("null\n");
        break;
    case MRDUMP_OUTPUT_USB:
        printf("usb\n");
        break;
    case MRDUMP_OUTPUT_EXT4_DATA:
        printf("ext4/data partition\n");
        break;
    case MRDUMP_OUTPUT_VFAT_INT_STORAGE:
        printf("vfat/internal partition\n");
        break;
    }
}

static int lkenv_write(const char *params, ...)
{
    if (!mrdump_is_supported()) {
        error("MT-RAMDUMP not support\n");
    }

    FILE *fp = fopen("/proc/lk_env", "w");
    if (fp != NULL) {
	va_list ap;

	va_start(ap, params);
	int n = vfprintf(fp, params, ap);
	fputc('\n', fp);
	va_end(ap);
	if (n < 0) {
	    error("Write to lk env failed\n");
	}
	fclose(fp);
    }
    else {
	error("Can't open lk env device\n");
    }
    return 1;
}

static int file_setup_command(int argc, char * __attribute__((unused)) argv[])
{
    if (argc != 1) {
	error("Invalid file-setup command argument\n");
    }
    mrdump_file_setup(false);
    return 0;
}

static void file_allocate_command(int argc, char *argv[])
{
    if (argc != 2) {
	error("Invaid file-allocate command argument\n");
    }
    int size_m = atoi(argv[1]);

    if (size_m < 0)
	size_m = 0;

    if ((size_m <= 2) || (size_m >= MRDUMP_EXT4_MIN_ALLOCATE)) {
	// enable condition: only 0, 1, 2, >256
	mrdump_file_set_maxsize(size_m);
    }
    else {
	error("Invalid dump size %d\n", size_m);
    }
}

static int file_extract_core_command(int argc, char *argv[])
{
    int opt;
    bool reinit = false;

    while ((opt = getopt(argc, argv, "r")) != -1) {
	switch (opt) {
	case 'r':
	    reinit = true;
	    break;
	default:
	    error("Invalid file-extract-core parameter\n");
	}
    }
    if (optind >= argc) {
	    error("Expected filename after options\n");
    }

    const char *fn = argv[optind];
    if (mrdump_file_fetch_zip_coredump(fn)) {
        if (reinit) {
            mrdump_file_setup(true);
        }
        return 0;
    }
    error("Fetching Coredump data failed\n");
}

static int file_info_command(int argc, char * __attribute__((unused)) argv[])
{
    if (argc != 1) {
	error("Invalid file-info command argument\n");
    }

    struct mrdump_file_info info;
    if (mrdump_file_get_info(MRDUMP_EXT4_ALLOCATE_FILE, &info)) {
        printf("\tfile size %" PRIu64 "\n\tcoredump size %" PRIu64 "\n",
               info.filesize, info.coredump_size);
	return 0;
    }
    else {
	error("Cannot get pre-allocate file info\n");
    }
}

static void mem_size_set_command(int argc, char *argv[])
{
    if (argc != 2) {
	error("Invaid mem-size-set command argument\n");
    }

    int size_m = atoi(argv[1]);
    if ((size_m == 0) || ((size_m >= 64) && (size_m <= 16 * 1024))) {
	if (size_m != 0) {
	    lkenv_write("mrdump_mem_size=%d", size_m);
	}
	else {
	    lkenv_write("mrdump_mem_size=");
	}
    }
    else {
	error("Invalid memory dump size\n");
    }
}

int main(int argc, char *argv[])
{
    if (argc < 2) {
	usage(argv[0]);
    }

    if (strcmp(argv[1], "is-supported") == 0) {
	if (mrdump_is_supported()) {
	    printf("MT-RAMDUMP support ok\n");
	}
	else {
	    printf("MT-RAMDUMP not support\n");
	}
    }
    else if (strcmp(argv[1], "status-get") == 0) {
	struct mrdump_status_result result;
	if (mrdump_status_get(&result)) {
	    printf("MT-RAMDUMP\n\tStatus:");
	    switch (result.status) {
	    case MRDUMP_STATUS_NONE:
		printf("None\n");
		break;
	    case MRDUMP_STATUS_FAILED:
		printf("Failed\n");
		break;
	    case MRDUMP_STATUS_OK:
		dump_status_ok(&result);
		break;
	    }
	}
	else {
	    printf("MT-RAMDUMP get status failed\n");
	}
    }
    else if (strcmp(argv[1], "status-log") == 0) {
	struct mrdump_status_result result;
	if (!mrdump_status_get(&result)) {
	    printf("MT-RAMDUMP get status failed\n");
	}
	printf("=>status line:\n%s\n=>log:\n%s\n", result.status_line, result.log_buf);
    }
    else if (strcmp(argv[1], "status-clear") == 0) {
	if (!mrdump_is_supported()) {
	    error("MT-RAMDUMP not support\n");
	}
	if (!mrdump_status_clear()) {
	    printf("MT-RAMDUMP Status clear failed\n");
	}
    }
    else if (strcmp(argv[1], "file-setup") == 0) {
	file_setup_command(argc - 1, &argv[1]);
    }
    else if (strcmp(argv[1], "file-allocate") == 0) {
	file_allocate_command(argc - 1, &argv[1]);
    }
    else if (strcmp(argv[1], "file-extract-core") == 0) {
	file_extract_core_command(argc - 1, &argv[1]);
    }
    else if (strcmp(argv[1], "file-info") == 0) {
	file_info_command(argc - 1, &argv[1]);
    }
    else if (strcmp(argv[1], "mem-size-set") == 0) {
	mem_size_set_command(argc - 1, &argv[1]);
    }
    else if (strcmp(argv[1], "output-set") == 0) {
	const char *output_dev = argv[2];
	if ((strcmp(output_dev, "none") == 0) ||
	    (strcmp(output_dev, "null") == 0) ||
	    (strcmp(output_dev, "internal-storage:ext4") == 0) ||
	    (strcmp(output_dev, "internal-storage:vfat") == 0)) {
	    lkenv_write("mrdump_output=%s", output_dev);
	}
	else if (strcmp(output_dev, "default") == 0) {
	    lkenv_write("mrdump_output=");
	}
	else {
	    error("Invalid output device, valid input [none, null, internal-storage:ext4, internal-storage:vfat]\n");
	}
    }
    else {
      error("Unknown command %s\n", argv[1]);
    }

    return 0;
}
