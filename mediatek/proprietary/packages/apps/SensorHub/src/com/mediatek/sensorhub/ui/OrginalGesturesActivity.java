package com.mediatek.sensorhub.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.hardware.Sensor;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.SwitchPreference;
import android.util.Log;

import com.mediatek.sensorhub.settings.Utils;

public class OrginalGesturesActivity extends BaseActivity implements OnPreferenceChangeListener,
        OnSharedPreferenceChangeListener {

    private static final String TAG = "OrginalGesturesActivity";

    public OrginalGesturesActivity() {
        super(TAG);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.orginal_gestures_pref);
        initializeAllPreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePreferenceStatus();
        Utils.getSharedPreferences(this, Utils.SHARED_PREF_SENSOR_HUB)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        Utils.getSharedPreferences(this, Utils.SHARED_PREF_SENSOR_HUB)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    private void initializeAllPreferences() {
        for (int type : Utils.orginalGestureType) {
            Sensor sensor = mSensorKeyMap.get(type);
            if (sensor != null) {
                Utils.createPreference(Utils.TYPE_SWITCH, sensor.getName(), String.valueOf(type),
                        getPreferenceScreen(), this);
                String autoEnabled = getString(R.string.auto_enabled_title);
                Utils.createPreference(Utils.TYPE_SWITCH, autoEnabled, String.valueOf(type) + "_"
                        + autoEnabled, getPreferenceScreen(), this);
            }
        }
    }

    private void updatePreferenceStatus() {
        String autoEnabled = getString(R.string.auto_enabled_title);
        for (int type : Utils.orginalGestureType) {
            String prefKey = String.valueOf(type);
            SwitchPreference preference = (SwitchPreference) findPreference(prefKey);
            if (preference != null) {
                preference.setChecked(Utils.getSensorStatus(prefKey));
            }
            prefKey = prefKey + "_" + autoEnabled;
            preference = (SwitchPreference) findPreference(prefKey);
            if (preference != null) {
                preference.setChecked(Utils.getSensorStatus(prefKey));
            }
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean bNewValue = (Boolean) newValue;
        String prefKey = preference.getKey();
        Utils.setSensorStatus(prefKey, bNewValue);
        String autoEnabled = getString(R.string.auto_enabled_title);
        if (!prefKey.contains(autoEnabled)) {
            if (mBound) {
                mSensorService.registerSensor(preference.getKey(), bNewValue);
            }
        }
        return true;
    }

    private void suspend() {
        Log.d(TAG, "suspend");
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        try {
            powerManager.goToSleep(SystemClock.uptimeMillis());
            Log.d(TAG, "Was able to call PowerManager.goToSleep.");
        } catch (SecurityException e) {
            Log.d(TAG, "Was unable to call PowerManager.goToSleep.");
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        boolean status = sharedPreferences.getBoolean(key, false);
        Log.d(TAG, "onSharedPreferenceChanged : " + key + " status " + status);
        SwitchPreference preference = (SwitchPreference) findPreference(key);
        if (preference != null) {
            preference.setChecked(status);
        }
    }
}
