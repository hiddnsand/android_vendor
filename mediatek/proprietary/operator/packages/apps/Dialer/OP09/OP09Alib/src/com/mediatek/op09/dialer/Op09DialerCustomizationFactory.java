package com.mediatek.op09.dialer;

import android.content.Context;

import com.mediatek.dialer.ext.ICallDetailExtension;
import com.mediatek.dialer.ext.ICallLogExtension;
import com.mediatek.dialer.ext.IDialPadExtension;
import com.mediatek.dialer.ext.IDialerSearchExtension;
import com.mediatek.dialer.ext.OpDialerCustomizationFactoryBase;

import com.mediatek.op09.dialer.calllog.Op09CallDetailExtension;
import com.mediatek.op09.dialer.calllog.Op09CallLogExtension;
import com.mediatek.op09.dialer.dialersearch.Op09DialerSearchExtension;
import com.mediatek.op09.dialer.dialpad.Op09DialPadExtension;

public class Op09DialerCustomizationFactory extends OpDialerCustomizationFactoryBase {
    public Context mContext;
    public Op09DialerCustomizationFactory(Context context){
        mContext = context;
    }

    @Override
    public ICallDetailExtension makeCallDetailExt() {
        return new Op09CallDetailExtension();
    }

    @Override
    public ICallLogExtension makeCallLogExt() {
        return new Op09CallLogExtension();
    }

    @Override
    public IDialPadExtension makeDialPadExt() {
        return new Op09DialPadExtension(mContext);
    }

    @Override
    public IDialerSearchExtension makeDialerSearchExt() {
        return new Op09DialerSearchExtension();
    }
}
