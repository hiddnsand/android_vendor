#define DEBUG_LOG_TAG "BLT"

#include <hardware/hwcomposer.h>
#include "gralloc_mtk_defs.h"
#include <hardware/gralloc.h>

#include "utils/debug.h"
#include "utils/tools.h"

#include "Transform.h"

#include "bliter.h"
#include "display.h"
#include "platform.h"
#include "overlay.h"
#include "dispatcher.h"
#include "worker.h"
#include "queue.h"
#include "sync.h"
#include "hwdev.h"
#include "hwc.h"
#include "hwc_priv.h"

#include <sync/sync.h>

#include <utils/String8.h>

#include <ui/GraphicBuffer.h>

#define ALIGN_FLOOR(x,a)    ((x) & ~((a) - 1L))
#define ALIGN_CEIL(x,a)     (((x) + (a) - 1L) & ~((a) - 1L))

#define NOT_PRIVATE_FORMAT -1

#define BLOGD(i, x, ...) HWC_LOGD("(%d:%d) " x, m_disp_id, i, ##__VA_ARGS__)
#define BLOGI(i, x, ...) HWC_LOGI("(%d:%d) " x, m_disp_id, i, ##__VA_ARGS__)
#define BLOGW(i, x, ...) HWC_LOGW("(%d:%d) " x, m_disp_id, i, ##__VA_ARGS__)
#define BLOGE(i, x, ...) HWC_LOGE("(%d:%d) " x, m_disp_id, i, ##__VA_ARGS__)

// ---------------------------------------------------------------------------

// Because the transformation of rotation and flipping is NOT COMMUTATIVE,
// if the order of rotation and flipping would affect the final orientation,
// we need to map the relationship between display subsys and GLES.
//
// NOTE:
// display subsys: ROT  -> FLIP
//           GLES: FLIP -> ROT
unsigned int mapDpOrientation(const uint32_t transform)
{
    unsigned int orientation = DpBlitStream::ROT_0;

    // special case
    switch (transform)
    {
        // logically equivalent to (ROT_270 + FLIP_V)
        case (Transform::ROT_90 | Transform::FLIP_H):
            return (DpBlitStream::ROT_90 | DpBlitStream::FLIP_V);

        // logically equivalent to (ROT_270 + FLIP_H)
        case (Transform::ROT_90 | Transform::FLIP_V):
            return (DpBlitStream::ROT_90 | DpBlitStream::FLIP_H);
    }

    // general case
    if (Transform::FLIP_H & transform)
        orientation |= DpBlitStream::FLIP_H;

    if (Transform::FLIP_V & transform)
        orientation |= DpBlitStream::FLIP_V;

    if (Transform::ROT_90 & transform)
        orientation |= DpBlitStream::ROT_90;

    return orientation;
}

// ---------------------------------------------------------------------------

BliterHandler::BliterHandler(int dpy, sp<OverlayEngine> ovl_engine)
    : LayerHandler(dpy, ovl_engine)
{
    int num = m_ovl_engine->getMaxInputNum();
    m_dp_configs = (BufferConfig*)calloc(1, sizeof(BufferConfig) * num);
}

BliterHandler::~BliterHandler()
{
    free(m_dp_configs);
}

void BliterHandler::set(
    struct hwc_display_contents_1* list,
    DispatcherJob* job)
{
    uint32_t total_num = job->num_layers;

    for (uint32_t i = 0; i < total_num; i++)
    {
        HWLayer* hw_layer = &job->hw_layers[i];

        // this layer is not enable
        if (!hw_layer->enable) continue;

        // skip non mm layers
        if (HWC_LAYER_TYPE_MM != hw_layer->type) continue;

        // get release fence from sync fence
        // and get disp fence from display driver
        hwc_layer_1_t* layer = &list->hwLayers[hw_layer->index];
        {
            layer->releaseFenceFd = m_sync_fence->create();
            if (-1 == layer->releaseFenceFd)
            {
                BLOGE(i, "Create releaseFence fail");
            }

            OverlayPrepareParam prepare_param;
            prepare_param.id            = i;
            prepare_param.ion_fd        = DISP_NO_ION_FD;
            prepare_param.is_need_flush = 0;

            status_t err = m_ovl_engine->prepareInput(prepare_param);
            if (NO_ERROR != err)
            {
                prepare_param.fence_index = 0;
                prepare_param.fence_fd = -1;
            }
            hw_layer->fence_index = prepare_param.fence_index;

            if (prepare_param.fence_fd <= 0)
            {
                BLOGE(i, "Failed tp get Display Fence !!");
            }
            hw_layer->disp_fence = prepare_param.fence_fd;
        }
        memcpy(&hw_layer->layer, layer, sizeof(hwc_layer_1_t));

        BLOGD(i, "releaseFence (fd=%d) acquireFence (fd=%d) "
            "dispFence(fd=%d, idx=%d) (handle=%p)",
            layer->releaseFenceFd, layer->acquireFenceFd,
            hw_layer->disp_fence, hw_layer->fence_index, layer->handle);

        layer->acquireFenceFd = -1;
    }
}

void BliterHandler::releaseFence(hwc_layer_1* layer, int ovl_in)
{
    int fd = layer->releaseFenceFd;
    if (fd != -1)
    {
        BLOGD(ovl_in, "Release releaseFence (fd=%d)", fd);

        status_t err = m_sync_fence->inc(fd);
        if (err)
        {
            BLOGE(ovl_in, "Failed to release releaseFence (fd=%d): %s(%d)",
                fd, strerror(-err), err);
        }
    }
}

bool BliterHandler::bypassBlit(HWLayer* hw_layer, int ovl_in)
{
    int num_dpy = 1;
    DisplayManager::getInstance().query(DisplayManager::DISP_CURRENT_NUM, &num_dpy);
    // TODO: multiple display could bypass bliting, to
    /*
    if (!hw_layer->dirty && num_dpy == 1)
    {
        HWC_ATRACE_NAME("bypass");
        BLOGD(ovl_in, "bypass hnd:%p", hw_layer->layer.handle);
        return true;
    }
    */
    return false;
}

status_t BliterHandler::setDpConfig(
    HWLayer* hw_layer, BufferConfig* config, int ovl_in)
{
    hwc_buffer_info_t* buffer_info = &hw_layer->buffer_info;

    // check if private color format is changed
    bool private_format_change = false;
    int curr_private_format = NOT_PRIVATE_FORMAT;
    if (HAL_PIXEL_FORMAT_YUV_PRIVATE == buffer_info->format ||
        HAL_PIXEL_FORMAT_YCbCr_420_888 == buffer_info->format)
    {
        curr_private_format = (buffer_info->ext_info.status & GRALLOC_EXTRA_MASK_CM);
        if (HAL_PIXEL_FORMAT_YUV_PRIVATE == config->gralloc_format ||
            HAL_PIXEL_FORMAT_YCbCr_420_888 == config->gralloc_format)
        {
            private_format_change = (config->gralloc_private_format != curr_private_format);
        }
    }

    if (config->gralloc_stride == buffer_info->y_stride &&
        config->gralloc_width == buffer_info->width &&
        config->gralloc_height == buffer_info->height &&
        config->gralloc_cbcr_align == buffer_info->cbcr_align &&
        config->gralloc_vertical_stride == buffer_info->vertical_stride &&
        config->gralloc_format == buffer_info->format &&
        private_format_change  == false)
    {
        // data format is not changed
        if (!config->is_valid)
            return -EINVAL;

        return NO_ERROR;
    }

    BLOGD(ovl_in, "Format Change (w=%d h=%d f=0x%x s=%d vs=%d) -> (w=%d h=%d f=0x%x s=%d vs=%d pf=0x%x)",
        config->gralloc_width, config->gralloc_height, config->gralloc_format, config->gralloc_stride, config->gralloc_vertical_stride,
        buffer_info->width, buffer_info->height, buffer_info->format, buffer_info->y_stride, buffer_info->vertical_stride, curr_private_format);

    // remember current buffer data format for next comparison
    config->gralloc_width  = buffer_info->width;
    config->gralloc_height = buffer_info->height;
    config->gralloc_stride = buffer_info->y_stride;
    config->gralloc_cbcr_align = buffer_info->cbcr_align;
    config->gralloc_vertical_stride = buffer_info->vertical_stride;
    config->gralloc_format = buffer_info->format;
    config->gralloc_private_format = (buffer_info->ext_info.status & GRALLOC_EXTRA_MASK_CM);

    //
    // set DpFramework configuration
    //
    unsigned int src_size_luma = 0;
    unsigned int src_size_chroma = 0;

    unsigned int width  = buffer_info->width;
    unsigned int height = buffer_info->height;
    unsigned int y_stride = buffer_info->y_stride;
    unsigned int vertical_stride = buffer_info->vertical_stride;

    // reset uv pitch since RGB does not need it
    config->src_pitch_uv = 0;

    int input_format = grallocColor2HalColor(buffer_info->format, curr_private_format);;
    if (input_format == 0)
    {
        BLOGE(ovl_in, "Private Format is invalid (0x%x)", curr_private_format);
        config->is_valid = false;
        return -EINVAL;
    }

    // remember real height since height should be aligned with 32 for NV12_BLK
    config->src_height = height;

    switch (input_format)
    {
        case HAL_PIXEL_FORMAT_RGBA_8888:
        case HAL_PIXEL_FORMAT_RGBX_8888:
            config->src_pitch = y_stride * 4;
            config->src_plane = 1;
            config->src_size[0] = config->src_pitch * height * 4;
            config->src_dpformat = DP_COLOR_RGBA8888;
            config->dst_dpformat = DP_COLOR_RGBX8888;
            break;

        case HAL_PIXEL_FORMAT_BGRA_8888:
        case HAL_PIXEL_FORMAT_IMG1_BGRX_8888:
            config->src_pitch = y_stride * 4;
            config->src_plane = 1;
            config->src_size[0] = config->src_pitch * height * 4;
            config->src_dpformat = DP_COLOR_BGRA8888;
            config->dst_dpformat = DP_COLOR_RGBX8888;
            break;

        case HAL_PIXEL_FORMAT_RGB_565:
            config->src_pitch = y_stride * 2;
            config->src_plane = 1;
            config->src_size[0] = config->src_pitch * height * 2;
            config->src_dpformat = DP_COLOR_RGB565;
            config->dst_dpformat = DP_COLOR_RGB565;
            break;

        case HAL_PIXEL_FORMAT_NV12_BLK_FCM:
            // mtk decoder does not use gralloc stride
            config->src_pitch    = y_stride * 32;
            config->src_pitch_uv = ALIGN_CEIL((y_stride / 2), buffer_info->cbcr_align ? buffer_info->cbcr_align : 16) * 2 * 16;
            config->src_plane = 2;
            config->src_height = vertical_stride;
            src_size_luma = y_stride * vertical_stride;
            config->src_size[0] = src_size_luma;
            config->src_size[1] = src_size_luma / 4 * 2;
            config->src_offset[0] = src_size_luma;
            config->src_dpformat = DP_COLOR_420_BLKI;
            config->dst_dpformat = DP_COLOR_YUYV;
            break;

        case HAL_PIXEL_FORMAT_YCbCr_422_I:
            // mtk decoder does not use gralloc stride
            config->src_pitch    = y_stride * 2;
            config->src_pitch_uv = 0;
            config->src_plane = 1;
            src_size_luma = y_stride * vertical_stride;
            config->src_size[0] = src_size_luma * 2;
            config->src_dpformat = DP_COLOR_YUYV;
            config->dst_dpformat = DP_COLOR_YUYV;
            config->dst_pitch    = ALIGN_CEIL(config->dst_width * 2,64);
            break;

        case HAL_PIXEL_FORMAT_NV12_BLK:
            // mtk decoder does not use gralloc stride
            config->src_pitch    = y_stride * 32;
            config->src_pitch_uv = ALIGN_CEIL((y_stride / 2), buffer_info->cbcr_align ? buffer_info->cbcr_align : 16) * 2 * 16;
            config->src_plane = 2;
            config->src_height = vertical_stride;
            src_size_luma = y_stride * vertical_stride;
            config->src_size[0] = src_size_luma;
            config->src_size[1] = src_size_luma / 4 * 2;
            config->src_offset[0] = src_size_luma;
            config->src_dpformat = DP_COLOR_420_BLKP;
            config->dst_dpformat = DP_COLOR_YUYV;
            break;

        case HAL_PIXEL_FORMAT_I420:
            // mtk decoder does not use gralloc stride
            config->src_pitch    = y_stride;
            config->src_pitch_uv = ALIGN_CEIL((y_stride / 2), buffer_info->cbcr_align ? buffer_info->cbcr_align : 1);
            config->src_plane = 3;
            src_size_luma = config->src_pitch * vertical_stride;
            src_size_chroma = config->src_pitch_uv * vertical_stride / 2;
            config->src_size[0] = src_size_luma;
            config->src_size[1] = src_size_chroma;
            config->src_size[2] = src_size_chroma;
            config->src_offset[0] = src_size_luma;
            config->src_offset[1] = src_size_luma + src_size_chroma;
            config->src_dpformat = DP_COLOR_I420;
            config->dst_dpformat = DP_COLOR_YUYV;
            break;

        case HAL_PIXEL_FORMAT_YV12:
            config->src_pitch    = y_stride;
            config->src_pitch_uv = ALIGN_CEIL((y_stride / 2), buffer_info->cbcr_align ? buffer_info->cbcr_align : 16);
            config->src_plane = 3;
            src_size_luma = config->src_pitch * vertical_stride;
            src_size_chroma = config->src_pitch_uv * (vertical_stride / 2);
            config->src_size[0] = src_size_luma;
            config->src_size[1] = src_size_chroma;
            config->src_size[2] = src_size_chroma;
            config->src_offset[0] = src_size_luma;
            config->src_offset[1] = src_size_luma + src_size_chroma;
            config->src_dpformat = DP_COLOR_YV12;
            config->dst_dpformat = DP_COLOR_YUYV;
            break;

        default:
            BLOGE(ovl_in, "Color format is not supported (0x%x)", input_format);
            config->is_valid = false;
            return -EINVAL;
    }

    config->is_valid = true;
    return NO_ERROR;
}

status_t BliterHandler::computeBufferCrop(
    HWLayer* hw_layer, BufferConfig* config,
    Rect& src_crop, Rect& dst_crop, int ovl_in)
{
    hwc_layer_1_t* layer = &hw_layer->layer;
    Rect dst_rect = Rect(config->dst_width, config->dst_height);
    Rect dst_base(*(Rect *)&(layer->displayFrame));

    // calculate dst crop
    dst_rect.intersect(dst_base, &dst_crop);
    if (dst_crop.isEmpty())
    {
        BLOGE(ovl_in, "empty dst crop:w=%d h=%d", dst_crop.getWidth(), dst_crop.getHeight());
        return -EINVAL;
    }

    // check inverse transform
    Rect in_base(dst_base);
    Rect in_crop(dst_crop);
    uint32_t inv_transform = layer->transform;
    if (Transform::ROT_0 != inv_transform && dst_base != dst_crop)
    {
        if (Transform::ROT_90 & inv_transform)
            inv_transform ^= (Transform::FLIP_H | Transform::FLIP_V);

        Transform tr(inv_transform);
        in_base = tr.transform(in_base);
        in_crop = tr.transform(in_crop);
    }

    // map dst crop to src crop
    // [NOTE]
    // Since OVL does not support float crop, adjust coordinate to interger
    // as what SurfaceFlinger did with hwc before version 1.2
    hwc_frect_t* src_cropf = &layer->sourceCropf;
    int l = (int)(ceilf(src_cropf->left));
    int t = (int)(ceilf(src_cropf->top));
    int r = (int)(floorf(src_cropf->right));
    int b = (int)(floorf(src_cropf->bottom));
    Rect src_base(l, t, r, b);
    if (src_base.isEmpty())
    {
        BLOGE(ovl_in, "empty src crop:w=%d h=%d", src_crop.getWidth(), src_crop.getHeight());
        return -EINVAL;
    }

    // calculate rectangle ratio between two rectangles
    // horizontally and vertically
    const float ratio_h = src_base.getWidth() /
            static_cast<float>(in_base.getWidth());
    const float ratio_v = src_base.getHeight() /
            static_cast<float>(in_base.getHeight());

    // get result of the corresponding crop rectangle
    // add 0.5f to round the result to the nearest whole number
    src_crop.left = src_base.left + 0.5f +
            (in_crop.left - in_base.left) * ratio_h;
    src_crop.top  = src_base.top + 0.5f +
            (in_crop.top - in_base.top) * ratio_v;
    src_crop.right = src_base.left + 0.5f +
            (in_crop.right - in_base.left) * ratio_h;
    src_crop.bottom = src_base.top + 0.5f +
            (in_crop.bottom - in_base.top) * ratio_v;

    return NO_ERROR;
}

sp<DisplayBufferQueue> BliterHandler::getDisplayBufferQueue(
    HWLayer* hw_layer, BufferConfig* config, int ovl_in) const
{
    sp<DisplayBufferQueue> queue = m_ovl_engine->getInputQueue(ovl_in);
    if (queue == NULL)
    {
        queue = new DisplayBufferQueue(DisplayBufferQueue::QUEUE_TYPE_BLT);
        queue->setSynchronousMode(false);

        // connect to OverlayEngine
        m_ovl_engine->setInputQueue(ovl_in, queue);
    }

    hwc_layer_1_t* layer = &hw_layer->layer;
    int format = hw_layer->buffer_info.format;
    int bpp = getBitsPerPixel(format);

    // set buffer format to buffer queue

    // [NOTE] for mt6582 hw limitation
    // should be removed after mt6582 phasing out
    int status = DevicePlatform::COND_NOT_ALIGN;
    const int pitch_buf = m_disp_data->width * bpp / 8;
    int pitch_align = pitch_buf;
    if (DevicePlatform::m_config.platform & OVL_GEN_6582)
    {
        Rect dst_crop;
        Rect dst_rect = Rect(m_disp_data->width, m_disp_data->height);
        Rect dst_base(*(Rect *)&(layer->displayFrame));
        dst_rect.intersect(dst_base, &dst_crop);
        const int pitch_crop = WIDTH(dst_crop) * bpp / 8;

        pitch_align = DevicePlatform::getInstance().computePitch(pitch_buf, pitch_crop, &status);
    }

    // TODO: should refer to layer->displayFrame
    DisplayBufferQueue::BufferParam buffer_param;
    buffer_param.width = m_disp_data->width;
    buffer_param.height = m_disp_data->height;
    buffer_param.pitch = pitch_align * 8 / bpp;
    buffer_param.format = format;
    buffer_param.size = pitch_align * m_disp_data->height;
    buffer_param.protect = (hw_layer->buffer_info.usage & GRALLOC_USAGE_PROTECTED);
    buffer_param.secure = (hw_layer->buffer_info.usage & GRALLOC_USAGE_SECURE);
    buffer_param.need_shift = (status == DevicePlatform::COND_ALIGNED_SHIFT);
    queue->setBufferParam(buffer_param);

    // TODO: should refer to layer->displayFrame
    config->dst_width = m_disp_data->width;
    config->dst_height = m_disp_data->height;
    config->dst_pitch = pitch_align;

    return queue;
}

void BliterHandler::invalidate(
    HWLayer* hw_layer, BufferConfig* config, sp<DisplayBufferQueue> queue,
    Rect& src_crop, Rect& dst_crop, int ovl_in)
{
    HWC_ATRACE_CALL();

    hwc_layer_1_t* layer = &hw_layer->layer;

    BLOGD(ovl_in, "Invalidate(hnd:%p) with acquireFence (fd=%d) dispFence(fd=%d, idx=%d)",
        layer->handle, layer->acquireFenceFd, hw_layer->disp_fence, hw_layer->fence_index);

    // wait until producer's job is really finished
    m_sync_fence->wait(layer->acquireFenceFd, 1000, DEBUG_LOG_TAG);

    if (HWCMediator::getInstance().m_features.enhance)
        m_blit_stream.setTdshp(1);

    DpSecure is_dp_secure = DP_SECURE_NONE;
    status_t err = NO_ERROR;

    const bool is_svp_support = (1 == HWCMediator::getInstance().m_features.svp);
    if (is_svp_support && (hw_layer->buffer_info.usage & GRALLOC_USAGE_SECURE))
    {
        is_dp_secure = DP_SECURE;

        int sec_handle = 0;
        err = grallocExtraQuery(layer->handle, GRALLOC_EXTRA_GET_SECURE_HANDLE, &sec_handle);
        if (NO_ERROR != err || sec_handle == 0)
        {
            BLOGE(ovl_in, "Failed to get secure buffer !! (handle=%p, err=%d)",
                layer->handle, err);
            return;
        }

        unsigned int src_addr[3];
        src_addr[0] = (unsigned int)sec_handle;
        src_addr[1] = (unsigned int)sec_handle;
        src_addr[2] = (unsigned int)sec_handle;

        BLOGD(ovl_in, "Set secure buffer handle(handle=%p, sec=0x%x)",
            layer->handle, sec_handle);

        m_blit_stream.setSrcBuffer(
            (void**)src_addr,
            config->src_size, config->src_plane);
    }
    else
    {
        int fd = getIonFd(layer->handle);
        if (fd < 0)
        {
            BLOGE(ovl_in, "Failed to get ION fd !! (handle=%p)", layer->handle);
            return;
        }

        m_blit_stream.setSrcBuffer(fd, config->src_size, config->src_plane);
    }

    // [NOTE] width and height for DP should be 2 byte-aligned
    DpRect src_roi;
    src_roi.x = src_crop.left;
    src_roi.y = src_crop.top;
    src_roi.w = ALIGN_FLOOR(src_crop.getWidth(), 2);
    src_roi.h = ALIGN_FLOOR(src_crop.getHeight(), 2);

    // [NOTE] setSrcConfig provides y and uv pitch configuration
    // // if uv pitch is 0, DP would calculate it according to y pitch
    m_blit_stream.setSrcConfig(
        config->gralloc_stride, config->src_height,
        config->src_pitch, config->src_pitch_uv,
        config->src_dpformat, DP_PROFILE_BT601,
        eInterlace_None, &src_roi, is_dp_secure, false);

    // display buffer configuration
    DisplayBufferQueue::DisplayBuffer disp_buffer;
    err = queue->dequeueBuffer(&disp_buffer);
    if (NO_ERROR != err)
    {
        BLOGE(ovl_in, "Failed to dequeue buffer...");
        return;
    }

    // [NOTE] for mt6582 hw limitation
    // should be removed after mt6582 phasing out
    if (is_svp_support && (DP_SECURE == is_dp_secure && disp_buffer.need_shift))
        is_dp_secure = DP_SECURE_SHIFT;

    // [NOTE]
    // * width and height for DP should be 2 byte-aligned
    // * DpBlitStream only fill dst from (0, 0)
    //   and the start position will be set in OVL param
    // * use ALIGN_CEIL() for dst size
    //   even enlarge a little, QA seems to prefer to fill all the region of surface by overlay
    DpRect dst_roi;
    dst_roi.x = 0;
    dst_roi.y = 0;
    dst_roi.w = ALIGN_CEIL(dst_crop.getWidth(), 2);
    dst_roi.h = ALIGN_CEIL(dst_crop.getHeight(), 2);

    // check for OVL limitation
    // if dst region is out of boundary, should adjust it.
    if ((dst_crop.left + dst_roi.w) > (int32_t)m_disp_data->width)
        dst_roi.w -= 2;

    if ((dst_crop.top + dst_roi.h) > (int32_t)m_disp_data->height)
        dst_roi.h -= 2;

    config->dst_size = config->dst_pitch * dst_roi.h;
    m_blit_stream.setDstBuffer(
        (void**)&(disp_buffer.data),
        (void**)&(disp_buffer.mva),
        &(config->dst_size), 1);

    // [NOTE] setDstConfig provides y and uv pitch configuration
    // if uv pitch is 0, DP would calculate it according to y pitch
    // currently, roi is ignored by DP
    m_blit_stream.setDstConfig(
        dst_roi.w, dst_roi.h,
        config->dst_pitch, 0,
        config->dst_dpformat, DP_PROFILE_BT601,
        eInterlace_None, &dst_roi, is_dp_secure, false);

    if (dst_roi.w == 0 || dst_roi.h == 0)
    {
        BLOGE(ovl_in, "empty dst roi: w=%d h=%d (dst crop: w=%d h=%d)",
            dst_roi.w, dst_roi.h, dst_crop.getWidth(), dst_crop.getHeight());

        queue->cancelBuffer(disp_buffer.index);
        // TODO: cancel disp fence
        close(hw_layer->disp_fence);
        return;
    }

    m_blit_stream.setOrientation(mapDpOrientation(layer->transform));

    {
        HWC_ATRACE_NAME("dp_invalidate");

        DP_STATUS_ENUM status = m_blit_stream.invalidate();
        if (DP_STATUS_RETURN_SUCCESS != status)
        {
            BLOGE(ovl_in, "Failed to invalidate blit stream (%d)", status);

            queue->cancelBuffer(disp_buffer.index);
            //  TODO: cancel disp fence
            close(hw_layer->disp_fence);
            return;
        }
    }

    // TODO: remember data address in order for dump

    // DpBlitStream only fill data from (0, 0)
    Rect base_crop(Rect(0, 0, dst_roi.w, dst_roi.h));
    disp_buffer.data_info.src_crop = base_crop;
    disp_buffer.data_info.dst_crop = base_crop.offsetTo(dst_crop.left, dst_crop.top);
    disp_buffer.data_info.is_sharpen = false;
    disp_buffer.alpha_enable = 1;
    disp_buffer.alpha        = layer->planeAlpha;

    if (DisplayManager::m_profile_level & PROFILE_BLT)
    {
        disp_buffer.sequence = config->sequence;
    }

    // TODO: fill acquire_fence as layer->releaseFenceFd
    // when dpFramework supports fence
    disp_buffer.acquire_fence = -1;
    disp_buffer.release_fence = hw_layer->disp_fence;
    disp_buffer.rel_fence_idx = hw_layer->fence_index;

    // send buffer back to buffer queue after blit
    disp_buffer.handle = (void *)layer->handle;
    queue->queueBuffer(&disp_buffer);
}

void BliterHandler::process(DispatcherJob* job)
{
    uint32_t total_num = job->num_layers;

    for (uint32_t i = 0; i < total_num; i++)
    {
        HWLayer* hw_layer = &job->hw_layers[i];

        // this layer is not enable
        if (!hw_layer->enable) continue;

        // skip non mm layers
        if (HWC_LAYER_TYPE_MM != hw_layer->type) continue;

        hwc_layer_1_t* layer = &hw_layer->layer;
        BufferConfig* config = &m_dp_configs[i];

        if (DisplayManager::m_profile_level & PROFILE_BLT)
        {
            config->sequence = job->sequence;
        }

        // get extra data information
        status_t err = getBufferInfo(layer->handle, &hw_layer->buffer_info);
        if (NO_ERROR == err)
        {
            if (!bypassBlit(hw_layer, i)) {
                // get display buffer queue
                sp<DisplayBufferQueue> queue = getDisplayBufferQueue(hw_layer, config, i);

                // prepare configuration for later usage
                if (NO_ERROR == setDpConfig(hw_layer, config, i))
                {
                    // calculate valid src and dst region
                    Rect src_crop, dst_crop;
                    if (NO_ERROR == computeBufferCrop(hw_layer, config, src_crop, dst_crop, i))
                    {
                        // trigger bliting
                        invalidate(hw_layer, config, queue, src_crop, dst_crop, i);
                    }
                }
            }
            else
            {
                m_sync_fence->wait(layer->acquireFenceFd, 1000, DEBUG_LOG_TAG);
                close(hw_layer->disp_fence);
                m_ovl_engine->ignoreInput(i);
            }
        }
        else
        {
            BLOGE(i, "Failed to get buffer info !!");
            m_ovl_engine->ignoreInput(i);
        }

        // release releaseFence
        releaseFence(layer, i);
    }
}

int BliterHandler::dump(char* buff, int buff_len, int dump_level)
{
    if (dump_level & DUMP_SYNC)
        m_sync_fence->dump(-1);

    return 0;
}
