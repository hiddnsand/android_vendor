package com.mediatek.internal.telephony.selfactivation;

import android.os.Bundle;

public interface ISelfActivation {
    /**
     * Trigger actions for self activation scenario
     * @param action The action to take
     *  0: add data service
     *  1: MO call
     * @param param The parameters for the action
     *
     * @return Return nagtive value if failed
     */
    public int selfActivationAction(int action, Bundle param);

    /**
     * Query the activation state
     *
     * @return Return the state
     *  0: STATE_NONE (no activation required)
     *  1: STATE_ACTIVATED
     *  2: STATE_NOT_ACTIVATED
     *  -1: STATE_UNKNOWN
     */
    public int getSelfActivateState();
}
