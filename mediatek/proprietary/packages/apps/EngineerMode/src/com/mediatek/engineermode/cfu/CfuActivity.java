/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.cfu;

import android.app.Activity;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;
import android.widget.TextView;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;

import com.android.internal.telephony.uicc.IccRecords;
import com.android.internal.telephony.PhoneFactory;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.ModemCategory;

import com.mediatek.internal.telephony.uicc.MtkSIMRecords;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mediatek.engineermode.cfu.CfuSettingsCtl.*;

/**
 * Description: To set CFU status.
 */
public class CfuActivity extends Activity {
    private static final String TAG = "CFU";

    private CfuSettingsCtl mCfuSettingsCtl = null;

    private HandlerThread mHandlerThread = null;

    private static final String EFCFIS_STATUS_ERR     = "Get error.";
    private static final String EFCFIS_STATUS_VALID   = "EFCFIS valid";
    private static final String EFCFIS_STATUS_INVALID = "EFCFIS invalid";

    private RadioButton mRadioBtnApDe;
    private RadioButton mRadioBtnApOn;
    private RadioButton mRadioBtnApOff;
    private RadioButton mRadioBtnApIfEFCFISInvalid;
    private Button      mButtonApOk;

    private RadioButton mRadioBtnDe;
    private RadioButton mRadioBtnOn;
    private RadioButton mRadioBtnOff;
    private Button      mButtonOk;

    private TextView mCurrentTypeAP;
    private TextView mCurrentTypeMD;
    private TextView mTvEfcfisStatus;

    private Phone mPhone;
    private int mSlot = PhoneConstants.SIM_ID_1;

    private IccRecords mIccRecords = null;

    private void init() {
        if (TelephonyManager.getDefault().getPhoneCount() > 1) {
            mPhone = PhoneFactory.getPhone(PhoneConstants.SIM_ID_1);
        } else {
            mPhone = PhoneFactory.getDefaultPhone();
        }

        mHandlerThread = new HandlerThread("CfuSettingsCtl");
        mHandlerThread.start();
        Looper looper = mHandlerThread.getLooper();
        mCfuSettingsCtl = new CfuSettingsCtl(looper, mPhone, this);
    }

    private void refreshRadioBtn() {
        Elog.d(TAG, "refreshRadioBtn.");

        String cfuSetting = mCfuSettingsCtl.getCFUQueryTypeSystemProperty();

        Elog.d(TAG, "cfuSetting: " + cfuSetting);

        if (cfuSetting.equals(SYS_PROP_DEFAULT)) {
            mRadioBtnApDe.setChecked(true);
        } else if (cfuSetting.equals(SYS_PROP_ALWAYS_QUERY)) {
            mRadioBtnApOn.setChecked(true);
        } else if (cfuSetting.equals(SYS_PROP_ALWAYS_NOT_QUERY)) {
            mRadioBtnApOff.setChecked(true);
        } else if (cfuSetting.equals(SYS_PROP_QUERY_IF_EFCFIS_INVALID)) {
            mRadioBtnApIfEFCFISInvalid.setChecked(true);
        } else {
            Toast.makeText(CfuActivity.this, "Invalid status : " + cfuSetting, Toast.LENGTH_SHORT)
                    .show();
        }

        if (cfuSetting.equals(SYS_PROP_DEFAULT)) {
            mRadioBtnDe.setChecked(true);
        } else if (cfuSetting.equals(SYS_PROP_ALWAYS_QUERY)) {
            mRadioBtnOn.setChecked(true);
        } else if (cfuSetting.equals(SYS_PROP_ALWAYS_NOT_QUERY)) {
            mRadioBtnOff.setChecked(true);
        } else if (cfuSetting.equals(SYS_PROP_QUERY_IF_EFCFIS_INVALID)) {
            Elog.i(TAG, "MD not support query if efcfis is invalid.");
        } else {
            Toast.makeText(CfuActivity.this, "Invalid status : " + cfuSetting, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void refreshAPCurrentCFUQueryType() {
        String apStatus = mCfuSettingsCtl.getCFUQueryTypeSystemProperty();
        String apStatusStr = "";

        if (apStatus.equals(SYS_PROP_DEFAULT)) {
            apStatusStr = getResources().getString(R.string.cfu_ap_default);
        } else if (apStatus.equals(SYS_PROP_ALWAYS_QUERY)) {
            apStatusStr = getResources().getString(R.string.cfu_ap_turn_on);
        } else if (apStatus.equals(SYS_PROP_ALWAYS_NOT_QUERY)) {
            apStatusStr = getResources().getString(R.string.cfu_ap_turn_off);
        } else if (apStatus.equals(SYS_PROP_QUERY_IF_EFCFIS_INVALID)) {
            apStatusStr = getResources().getString(R.string.cfu_ap_query_if_efcfis_invalid);
        }

        String status =
                getResources().getString(R.string.cfu_cur_ap_textView, apStatusStr);

        mCurrentTypeAP.setText(status);
    }

    private void refreshMDCurrentCFUQueryType() {
        Message msg = mCfuSettingsCtl.obtainMessage(EVENT_QUERY_FROM_MD,
                mResponseHander.obtainMessage(QUERY));
        msg.sendToTarget();
    }

    private void refreshCurrentCFUQueryType() {
        refreshAPCurrentCFUQueryType();
        refreshMDCurrentCFUQueryType();
    }

    private void updateMDCurrentCFUQueryType(String mdStatus) {
        String mdStatusStr = "";

        if (mdStatus.equals(SYS_PROP_DEFAULT)) {
            mdStatusStr = getResources().getString(R.string.cfu_ap_default);
        } else if (mdStatus.equals(SYS_PROP_ALWAYS_QUERY)) {
            mdStatusStr = getResources().getString(R.string.cfu_ap_turn_on);
        } else if (mdStatus.equals(SYS_PROP_ALWAYS_NOT_QUERY)) {
            mdStatusStr = getResources().getString(R.string.cfu_ap_turn_off);
        }

        String status =
                getResources().getString(R.string.cfu_cur_md_textView, mdStatusStr);

        mCurrentTypeMD.setText(status);
    }

    private void initView() {
        Elog.d(TAG, "initView.");

        mRadioBtnApDe = (RadioButton) findViewById(R.id.cfu_ap_default_radio);
        mRadioBtnApOn = (RadioButton) findViewById(R.id.cfu_ap_on_radio);
        mRadioBtnApOff = (RadioButton) findViewById(R.id.cfu_ap_off_radio);
        mRadioBtnApIfEFCFISInvalid = (RadioButton) findViewById(
                R.id.cfu_ap_query_if_efcfis_invalid_radio);

        mButtonApOk = (Button) findViewById(R.id.cfu_ap_set_button);

        mButtonApOk.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (mRadioBtnApDe.isChecked()) {
                    Message msg = mCfuSettingsCtl.obtainMessage(EVENT_SET_DEFAULT,
                            mResponseHander.obtainMessage(REFRESH_AP_QUERY_STATUS));
                    msg.sendToTarget();
                } else if (mRadioBtnApOn.isChecked()) {
                    Message msg = mCfuSettingsCtl.obtainMessage(EVENT_SET_ALWAYS_QUERY,
                            mResponseHander.obtainMessage(REFRESH_AP_QUERY_STATUS));
                    msg.sendToTarget();
                } else if (mRadioBtnApOff.isChecked()) {
                    Message msg = mCfuSettingsCtl.obtainMessage(EVENT_SET_ALWAYS_NOT_QUERY,
                            mResponseHander.obtainMessage(REFRESH_AP_QUERY_STATUS));
                    msg.sendToTarget();
                } else if (mRadioBtnApIfEFCFISInvalid.isChecked()) {
                    Message msg = mCfuSettingsCtl.obtainMessage(EVENT_SET_QUERY_IF_EFCFIS_INVALID,
                            mResponseHander.obtainMessage(REFRESH_AP_QUERY_STATUS));
                    msg.sendToTarget();
                } else {
                    Message msg = mCfuSettingsCtl.obtainMessage(EVENT_SHOW_ERROR);
                    msg.sendToTarget();
                }
            }
        });

        /////////////////////////////

        mRadioBtnDe = (RadioButton) findViewById(R.id.cfu_default_radio);
        mRadioBtnOn = (RadioButton) findViewById(R.id.cfu_on_radio);
        mRadioBtnOff = (RadioButton) findViewById(R.id.cfu_off_radio);

        mButtonOk = (Button) findViewById(R.id.cfu_set_button);

        mButtonOk.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (mRadioBtnDe.isChecked()) {
                    Message msg = mCfuSettingsCtl.obtainMessage(
                            EVENT_SET_DEFAULT_TO_AP_MD,
                            mResponseHander.obtainMessage(SET_DEFAULT));
                    msg.sendToTarget();
                } else if (mRadioBtnOn.isChecked()) {
                    Message msg = mCfuSettingsCtl.obtainMessage(
                            EVENT_SET_ALWAYS_QUERY_TO_AP_MD,
                            mResponseHander.obtainMessage(SET_ON));
                    msg.sendToTarget();
                } else if (mRadioBtnOff.isChecked()) {
                    Message msg = mCfuSettingsCtl.obtainMessage(
                            EVENT_SET_ALWAYS_NOT_QUERY_TO_AP_MD,
                            mResponseHander.obtainMessage(SET_OFF));
                    msg.sendToTarget();
                } else {
                    Message msg = mCfuSettingsCtl.obtainMessage(EVENT_SHOW_ERROR);
                    msg.sendToTarget();
                }
            }
        });

        /////////////////////////////

        refreshRadioBtn();

        /////////////////////////////

        mTvEfcfisStatus = (TextView) findViewById(R.id.cfu_efcfis_status_tv);
        Button buttonGetEFCFIS = (Button) findViewById(R.id.cfu_get_efcfis_button);

        String efcfisStatus = getEFCFISStatus();
        mTvEfcfisStatus.setText(efcfisStatus);

        buttonGetEFCFIS.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                String efcfisStatus = getEFCFISStatus();
                mTvEfcfisStatus.setText(efcfisStatus);
            }
        });

        mCurrentTypeAP = (TextView) findViewById(R.id.cfu_cur_ap_tv);
        mCurrentTypeMD = (TextView) findViewById(R.id.cfu_cur_md_tv);

        refreshCurrentCFUQueryType();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.cfu_activity);

        init();
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Elog.d(TAG, "onDestroy()");

        Looper looper = mHandlerThread.getLooper();
        looper.quit();
    }


    private void initIccRecords() {
        mIccRecords = PhoneFactory.getPhone(mSlot).getIccRecords();
    }

    private String getEFCFISStatus() {
        if (mIccRecords == null) {
            initIccRecords();
        }

        if (mIccRecords == null) {
            return EFCFIS_STATUS_ERR + " IccRecords is null, please try again later.";
        } else if (!(mIccRecords instanceof MtkSIMRecords)) {
            return EFCFIS_STATUS_ERR + " EFCFIS checking does not support non GSM SIM card.";
        }

        if (((MtkSIMRecords)mIccRecords).checkEfCfis()) {
            return EFCFIS_STATUS_VALID;
        } else {
            return EFCFIS_STATUS_INVALID;
        }
    }

    private final Handler mResponseHander = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Elog.i(TAG, "Receive msg from modem");
            AsyncResult ar = (AsyncResult) msg.obj;
            switch (msg.what) {
            case QUERY:
                if (null == ar.exception) {
                    String[] receiveDate = (String[]) ar.result;

                    if (null == receiveDate) {
                        Toast.makeText(CfuActivity.this, "Warning: Received data is null!",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        if (null != receiveDate[0] && receiveDate[0].startsWith(FORE_CMD)) {
                            // receiveDate[0].substring(6).split(",");
                            receiveDate[0] = receiveDate[0].substring(FORE_CMD.length(),
                                    receiveDate[0].length());
                            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
                            Matcher m0 = p.matcher(receiveDate[0]);
                            receiveDate[0] = m0.replaceAll("");
                            if (receiveDate[0].equals(SYS_PROP_DEFAULT)) {
                                mRadioBtnDe.setChecked(true);
                            } else if (receiveDate[0].equals(SYS_PROP_ALWAYS_NOT_QUERY)) {
                                mRadioBtnOff.setChecked(true);
                            } else if (receiveDate[0].equals(SYS_PROP_ALWAYS_QUERY)) {
                                mRadioBtnOn.setChecked(true);
                            } else {
                                Toast.makeText(CfuActivity.this,
                                        "Invalid status : " + receiveDate[0], Toast.LENGTH_SHORT)
                                        .show();
                            }

                            updateMDCurrentCFUQueryType(receiveDate[0]);
                        } else {
                            Toast.makeText(CfuActivity.this, "Warning: Invalid return",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(CfuActivity.this, "Query Failed!", Toast.LENGTH_SHORT).show();
                }
                break;
            case SET_DEFAULT:
                if (null == ar.exception) {
                    Toast.makeText(CfuActivity.this, "Success!", Toast.LENGTH_SHORT).show();
                } else {
                    mCfuSettingsCtl.createDialog(SET_DEFAULT);
                }

                refreshCurrentCFUQueryType();
                break;
            case SET_ON:
                if (null == ar.exception) {
                    Toast.makeText(CfuActivity.this, "Success!", Toast.LENGTH_SHORT).show();
                } else {
                    mCfuSettingsCtl.createDialog(SET_ON);
                }

                refreshCurrentCFUQueryType();
                break;
            case SET_OFF:
                if (null == ar.exception) {
                    Toast.makeText(CfuActivity.this, "Success!", Toast.LENGTH_SHORT).show();
                } else {
                    mCfuSettingsCtl.createDialog(SET_OFF);
                }

                refreshCurrentCFUQueryType();
                break;
            case REFRESH_AP_QUERY_STATUS:
                refreshAPCurrentCFUQueryType();
                Toast.makeText(CfuActivity.this, "Success!", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
            }
        }
    };
}
