#ifndef __IFPSPOLICY_H__
#define __IFPSPOLICY_H__

#include <binder/IInterface.h>

namespace android {

class IFpsPolicy : public IInterface {
protected:
    enum {
        POLICY_INIT = IBinder::FIRST_CALL_TRANSACTION
    };

public:
    DECLARE_META_INTERFACE(FpsPolicy);
};

class BnFpsPolicy : public BnInterface<IFpsPolicy>
{
    virtual status_t onTransact(uint32_t code,
                                const Parcel& data,
                                Parcel* reply,
                                uint32_t flags = 0);
};

};

#endif
