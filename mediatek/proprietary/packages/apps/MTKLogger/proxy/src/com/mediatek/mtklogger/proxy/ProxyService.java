package com.mediatek.mtklogger.proxy;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.FileObserver;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

import com.mediatek.mtklogger.proxy.log.LogInstance;
import com.mediatek.mtklogger.proxy.utils.FileMonitor;
import com.mediatek.mtklogger.proxy.utils.Utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author MTK81255
 *
 */
public class ProxyService extends Service {

    private static final String TAG = Utils.TAG + "/ProxyService";

    private static final int MSG_DEAL_COMMAND = 1;

    private SharedPreferences mSharedPreferences = null;
    private Handler mServiceHandler;
    private FileMonitor mFileMonitor;
    private LogManager mLogManager;

    @Override
    public void onCreate() {
        Utils.logi(TAG, "-->onCreate()");
        mSharedPreferences = getSharedPreferences(Utils.CONFIG_FILE_NAME, Context.MODE_PRIVATE);
        Utils.sApplicationContext = this.getApplicationContext();
        HandlerThread handlerThread = new HandlerThread(this.getClass().getName());
        handlerThread.start();
        mServiceHandler = new ServiceHandler(handlerThread.getLooper());
        mLogManager = new LogManager();
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Utils.logi(TAG, "-->onStartCommand()");
        try {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_FOREGROUND);
        } catch (IllegalArgumentException iae) {
            Utils.logw(TAG, "Set mtkloggerproxy service to foreground failed!");
        } catch (SecurityException se) {
            Utils.logw(TAG, "Set mtkloggerproxy service to foreground failed!");
        }
        if (intent != null) {
            mServiceHandler.obtainMessage(MSG_DEAL_COMMAND, intent).sendToTarget();
        } else {
            String monitorFile = mSharedPreferences.getString(Utils.CONFIG_KEY_MONITOR_FILE, "");
            String copyToFile = mSharedPreferences.getString(Utils.CONFIG_KEY_COPY_TO_FILE, "");
            if (isStringEmpty(monitorFile) || isStringEmpty(copyToFile)) {
                Utils.logw(TAG, "The monitorFile or copyToFile is empty,"
                        + " No need restart Monitor!");
            } else {
                intent = new Intent();
                intent.setAction(Utils.PROXY_ACTION_RECEIVER);
                intent.putExtra(Utils.PROXY_EXTRA_CMD_OPERATE,
                        Utils.PROXY_CMD_OPERATE_MONITOR_FILE);
                intent.putExtra(Utils.PROXY_EXTRA_CMD_TARGET, monitorFile);
                intent.putExtra(Utils.PROXY_EXTRA_CMD_VALUE, copyToFile);
                mServiceHandler.obtainMessage(MSG_DEAL_COMMAND, intent).sendToTarget();
            }
        }
        if (Utils.BUILD_TYPE.equals("eng")) {
            return Service.START_STICKY;
        } else {
            return Service.START_NOT_STICKY;
        }
    }

    /**
     * @author MTK81255
     *
     */
    class ServiceHandler extends Handler {

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            int what = msg.what;
            Utils.logi(TAG, " ServiceHandler handleMessage." + " What=" + what);
            if (what == MSG_DEAL_COMMAND) {
                if (msg.obj instanceof Intent) {
                    dealWithCommands((Intent) msg.obj);
                }
            }
        }
    }

    private void dealWithCommands(Intent intent) {
        String action = intent.getAction();
        if (!Utils.PROXY_ACTION_RECEIVER.equalsIgnoreCase(action)) {
            Utils.logw(TAG, "The action:" + action + " is not support!");
            return;
        }
        String cmdOperate = intent.getStringExtra(Utils.PROXY_EXTRA_CMD_OPERATE);
        String cmdTarget = intent.getStringExtra(Utils.PROXY_EXTRA_CMD_TARGET);
        String cmdValue = intent.getStringExtra(Utils.PROXY_EXTRA_CMD_VALUE);
        Utils.logi(TAG, "-->dealWithCommands(). Action = " + action + ", cmdOperate = "
                + cmdOperate + ", cmdTarget = " + cmdTarget + ", cmdSource = " + cmdValue);
        if (isStringEmpty(cmdOperate) || !Utils.CMD_OPERATE_SET.contains(cmdOperate)) {
            Utils.logw(TAG, "The command operate is empty or not support, just return!");
            doResponse(intent, false);
            return;
        }
        if (Utils.PROXY_CMD_OPERATE_MONITOR_FILE.equalsIgnoreCase(cmdOperate)) {
            /**
             * Monitor file broadcast format: adb shell am broadcast -a
             * com.mediatek.mtklogger.proxy.COMMAND -e CMD_OPERATE monitorFile -e
             * CMD_TARGET sourcePath -e CMD_VALUE targetPath
             */
            boolean isOK = doMonitorFile(cmdTarget, cmdValue);
            doResponse(intent, isOK);
        } else if (Utils.PROXY_CMD_OPERATE_COPY_FILE.equalsIgnoreCase(cmdOperate)) {
            /**
             * Copy file broadcast format: adb shell am broadcast -a
             * com.mediatek.mtklogger.proxy.COMMAND -e CMD_OPERATE copyFile -e
             * CMD_TARGET sourcePath -e CMD_VALUE targetPath
             */
            boolean isOK = Utils.doCopy(cmdTarget, cmdValue);
            doResponse(intent, isOK);
        } else if (Utils.PROXY_CMD_OPERATE_CONNECT_SOCKET.equalsIgnoreCase(cmdOperate)) {
            /**
             * Connect socket broadcast format: adb shell am broadcast -a
             * com.mediatek.mtklogger.proxy.COMMAND -e CMD_OPERATE connectSocket -e
             * CMD_TARGET socket_name -e CMD_VALUE socket_name
             */
            if (isStringEmpty(cmdTarget)) {
                Utils.logw(TAG, "The socket_name for " + cmdOperate + " is empty, ingnore!");
                doResponse(intent, false);
                return;
            }
            boolean isOK = mLogManager.connectToSocket(cmdTarget);
            doResponse(intent, isOK);
        } else if (Utils.PROXY_CMD_OPERATE_SEND_COMMAND.equalsIgnoreCase(cmdOperate)) {
            /**
             * Send command to log broadcast format: adb shell am broadcast -a
             * com.mediatek.mtklogger.proxy.COMMAND -e CMD_OPERATE sendCommand -e
             * CMD_TARGET socket_name -e CMD_VALUE command_str
             */
            if (isStringEmpty(cmdTarget) || isStringEmpty(cmdValue)) {
                Utils.logw(TAG, "The socket_name or command_str for "
                        + cmdOperate + " is empty, ingnore!");
                doResponse(intent, false);
                return;
            }
            String resultStr = mLogManager.sendCommand(cmdTarget, cmdValue);
            if (resultStr == null) {
                doResponse(intent, false);
            } else {
                doResponse(intent, resultStr);
            }
        }
    }

    private void doResponse(Intent intent, boolean result) {
        doResponse(intent, result ? Utils.PROXY_RESULT_OK : Utils.PROXY_RESULT_FAILED);
    }

    private void doResponse(Intent intent, String result) {
        Intent rsIntent = new Intent(Utils.PROXY_ACTION_RECEIVER_RETURN);
        rsIntent.putExtras(intent.getExtras());
        rsIntent.putExtra(Utils.PROXY_EXTRA_CMD_RESULT, result);
        Utils.logi(TAG, "-->doResponse(). Action = " + Utils.PROXY_ACTION_RECEIVER_RETURN
                + ", CMD_RESULT = " + result);
        Utils.sendBroadCast(rsIntent);
    }

    private boolean doMonitorFile(final String monitorFilePath, final String copyToFilePath) {
        if (isStringEmpty(monitorFilePath) || isStringEmpty(copyToFilePath)) {
            Utils.logw(TAG, "The monitorFile or copyToFile is empty, just return!");
            return false;
        }
        mSharedPreferences.edit().putString(Utils.CONFIG_KEY_MONITOR_FILE, monitorFilePath).apply();
        mSharedPreferences.edit().putString(Utils.CONFIG_KEY_COPY_TO_FILE, copyToFilePath).apply();
        if (mFileMonitor != null) {
            mFileMonitor.stopWatching();
        }
        Utils.doCopy(monitorFilePath, copyToFilePath);
        mFileMonitor = new FileMonitor(monitorFilePath, FileObserver.MODIFY) {
            @Override
            protected void notifyModified() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Utils.doCopy(monitorFilePath, copyToFilePath);
            }
        };
        mFileMonitor.startWatching();
        return true;
    }

    private boolean isStringEmpty(String str) {
        return str == null || str.isEmpty();
    }

    @Override
    public void onDestroy() {
        if (mFileMonitor != null) {
            mFileMonitor.stopWatching();
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    /**
     * @author MTK81255
     *
     */
    class LogManager {
        Map<String, LogInstance> mLogArray = new HashMap<String, LogInstance>();

        public LogManager() {
        }

        private LogInstance getLogInstance(String socketName) {
            LogInstance logInstance = mLogArray.get(socketName);
            if (logInstance == null) {
                logInstance = new LogInstance(socketName);
                mLogArray.put(socketName, logInstance);
            }
            return logInstance;
        }

        public boolean connectToSocket(String socketName) {
            boolean result = true;
            LogInstance logInstance = getLogInstance(socketName);
            if (logInstance == null || !logInstance.connectToSocket()) {
                result = false;
                Utils.logw(TAG, "connectToSocket " + socketName + " failed!");
                return result;
            }
            return result;
        }

        synchronized public String sendCommand(String socketName, String commandValue) {
            String resultStr = null;
            LogInstance logInstance = getLogInstance(socketName);
            if (logInstance == null || !logInstance.excuteCommand(commandValue)) {
                resultStr = Utils.PROXY_RESULT_FAILED;
                Utils.logw(TAG, "Send " + commandValue + " to " + socketName + " failed!");
                return resultStr;
            }
            long timeout = 20000;
            long checkPeriod = 500;
            while (resultStr == null && timeout > 0) {
                resultStr = logInstance.getCommandResult();
                try {
                    Thread.sleep(checkPeriod);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                timeout -= checkPeriod;
            }
            Utils.logi(TAG, commandValue + " to " + socketName + " result is " + resultStr);
            return resultStr;
        }
    }

}
