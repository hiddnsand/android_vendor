#define DEBUG_LOG_TAG "WKR"

#include <hardware/hwcomposer.h>

#include "utils/debug.h"
#include "dispatcher.h"
#include "overlay.h"
#include "worker.h"
#include "display.h"
#include "composer.h"
#include "bliter.h"
#include "sync.h"

// ---------------------------------------------------------------------------

status_t HWCThread::readyToRun()
{
    m_state = HWC_THREAD_IDLE;

    return NO_ERROR;
}

void HWCThread::waitLocked()
{
    while (m_state == HWC_THREAD_TRIGGER)
    {
        if (m_condition.waitRelative(m_lock, ms2ns(16)) == TIMED_OUT)
        {
            HWC_LOGW("Timed out waiting for %s...", m_thread_name);
        }
    }
}

void HWCThread::wait()
{
    AutoMutex l(m_lock);
    HWC_LOGD("Wating for %s...", m_thread_name);
    waitLocked();
}

// ---------------------------------------------------------------------------

ComposeThreadBase::ComposeThreadBase(int dpy, sp<SyncControl> sync_ctrl)
    : HWCThread()
    , m_disp_id(dpy)
    , m_handler(NULL)
    , m_sync_ctrl(sync_ctrl)
    , m_next_job(NULL)
{ }

ComposeThreadBase::~ComposeThreadBase()
{
    m_handler = NULL;
}

void ComposeThreadBase::onFirstRef()
{
    run(m_thread_name, PRIORITY_URGENT_DISPLAY);
}

void ComposeThreadBase::loopHandlerLocked()
{
    if (m_next_job == NULL)
        return;

    // check if need to wait other compose thread
    barrier(m_next_job);

    m_handler->process(m_next_job);

    // TODO: should do set overlay for each display
    if (m_disp_id != HWC_DISPLAY_EXTERNAL)
        m_sync_ctrl->setOverlay(m_next_job);

    m_next_job = NULL;
}

bool ComposeThreadBase::threadLoop()
{
    sem_wait(&m_event);

    {
        AutoMutex l(m_lock);

#ifndef MTK_USER_BUILD
        HWC_ATRACE_NAME(m_trace_tag);
#endif

        loopHandlerLocked();

        m_state = HWC_THREAD_IDLE;
        m_condition.signal();
    }

    return true;
}

void ComposeThreadBase::set(
    struct hwc_display_contents_1* list,
    DispatcherJob* job)
{
    if (m_handler != NULL) m_handler->set(list, job);
}

void ComposeThreadBase::trigger(DispatcherJob* job)
{
    AutoMutex l(m_lock);

    m_next_job = job;

    m_state = HWC_THREAD_TRIGGER;

    sem_post(&m_event);
}

// ---------------------------------------------------------------------------

UILayerComposer::UILayerComposer(
    int dpy, sp<SyncControl> sync_ctrl, sp<OverlayEngine> ovl_engine)
    : ComposeThreadBase(dpy, sync_ctrl)
{
    // TODO: ExtComposerHandler is a temp solution for external display
    if (dpy != HWC_DISPLAY_EXTERNAL)
        m_handler = new ComposerHandler(m_disp_id, ovl_engine);
    else
        m_handler = new ExtComposerHandler(ovl_engine);

    snprintf(m_trace_tag, sizeof(m_trace_tag), "compose1_%d", dpy);

    snprintf(m_thread_name, sizeof(m_thread_name), "UIComposeThread_%d", dpy);
}

void UILayerComposer::barrier(DispatcherJob* job)
{
    m_sync_ctrl->wait(job);
}

// ---------------------------------------------------------------------------

MMLayerComposer::MMLayerComposer(
    int dpy, sp<SyncControl> sync_ctrl, sp<OverlayEngine> ovl_engine)
    : ComposeThreadBase(dpy, sync_ctrl)
{
    m_handler = new BliterHandler(m_disp_id, ovl_engine);

    snprintf(m_trace_tag, sizeof(m_trace_tag), "compose2_%d", dpy);

    snprintf(m_thread_name, sizeof(m_thread_name), "MMComposeThread_%d", dpy);
}
