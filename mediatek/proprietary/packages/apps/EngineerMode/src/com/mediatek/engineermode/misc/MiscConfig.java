/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.misc;

import android.content.Intent;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;

import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;

import android.util.Log;

import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.ModemCategory;
import com.mediatek.engineermode.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Misc feature config Activity.
 */
public class MiscConfig extends PreferenceActivity implements
        Preference.OnPreferenceChangeListener {
    private static final String TAG = "EM/MiscConfig";
    public static final String FK_MTK_MISC_SELF_REG_CONFIG = "persist.radio.selfreg";
    public static final String FK_MTK_MISC_VIBRATE_CONFIG = "persist.radio.telecom.vibrate";

    private static final int MSG_QUERY = 0;
    private static final int MSG_SET = 1;
    private static final int MSG_QUERY_HVOLTE = 2;
    private static final int MSG_QUERY_SLIENT = 3;
    private static final int MSG_QUERY_VOLTE_HYS = 4;
    private static final int MSG_QUERY_VDM_IMS_CONFIG = 5;

    private static final String CMD_QUERY = "AT+ECFGGET=\"sms_over_sgs\"";
    private static final String CMD_SET = "AT+ECFGSET=\"sms_over_sgs\"";
    private static final String VALUE_ENABLE = "1";
    private static final String VALUE_DISABLE = "0";
    private static final String[] mEntries = { "bSRLTE", "hVolte" };
    private static final String[] mEntriesValue = { "1", "3" };
    private static final String[] mATCmdValue93md = { "AT+EIMSCFG=0,0,0,0,1,1",
                                                      "AT+EIMSCFG=1,0,0,0,1,1" };
    private static final String[] mATCmdValue9192md = { "AT+EIMSVOICE=0",
                                                        "AT+EIMSVOICE=1" };

    private static final String[] mEGCMDEntries = { "Disable", "Enable" };
    private static final String[] mEGCMDEntriesValue = { "\"00\"", "\"01\"" };

    private Toast mToast;
    private Phone mPhone = null;
    private CheckBoxPreference mSelfRegPreference;
    private CheckBoxPreference mSmsSgsPreference;
    private CheckBoxPreference mVibratePreference;
    private CheckBoxPreference mPresenceSet;

    private ListPreference mHVolteDeviceModePreference;
    private CheckBoxPreference mSilentRedialPreference;
    private EditTextPreference mTVolteHysPreference;
    private ListPreference mVdmImsReconfigPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.misc);

        // Vibrate
        mVibratePreference = new CheckBoxPreference(this);
        mVibratePreference.setSummary(getString(R.string.misc_config_vibrate));
        mVibratePreference.setPersistent(false);
        getPreferenceScreen().addPreference(mVibratePreference);

        if (FeatureSupport.isSupported(FeatureSupport.FK_CT4GREG_APP)) {
            // self register
            mSelfRegPreference = new CheckBoxPreference(this);
            mSelfRegPreference.setSummary(getString(R.string.misc_config_selfreg));
            mSelfRegPreference.setPersistent(false);
            getPreferenceScreen().addPreference(mSelfRegPreference);
        }
        else {
            Log.d(TAG, "Not show entry for CT4GREG.");
        }

        // sgs
        mSmsSgsPreference = new CheckBoxPreference(this);
        mSmsSgsPreference.setSummary(getString(R.string.misc_config_sgs));
        mSmsSgsPreference.setPersistent(false);
        getPreferenceScreen().addPreference(mSmsSgsPreference);

        // presence_set
        String mOptr = SystemProperties.get("persist.operator.optr");
        Log.d(TAG, "mOptr = " + mOptr);
        if ("OP07".equals(mOptr) || "OP08".equals(mOptr)) {
            mPresenceSet = new CheckBoxPreference(this);
            mPresenceSet.setSummary(getString(R.string.presence_set));
            mPresenceSet.setPersistent(false);
            getPreferenceScreen().addPreference(mPresenceSet);
        }
        Log.d(TAG, "0");
        if( !( FeatureSupport.is90Modem() || FeatureSupport.is3GOnlyModem()) ){
            Log.d(TAG, "1");
            // volte
            mHVolteDeviceModePreference = new ListPreference(this);
            mHVolteDeviceModePreference.setSummary(getString(R.string.misc_config_hvolte));
            mHVolteDeviceModePreference.setPersistent(false);
            mHVolteDeviceModePreference.setEntries(mEntries);
            mHVolteDeviceModePreference.setEntryValues(mEntriesValue);
            mHVolteDeviceModePreference.setOnPreferenceChangeListener(this);
            getPreferenceScreen().addPreference(mHVolteDeviceModePreference);

            mSilentRedialPreference = new CheckBoxPreference(this);
            mSilentRedialPreference.setSummary(getString(R.string.misc_config_silent));
            mSilentRedialPreference.setPersistent(false);
            getPreferenceScreen().addPreference(mSilentRedialPreference);

            mTVolteHysPreference = new EditTextPreference(this);
            mTVolteHysPreference.setSummary(getString(R.string.misc_config_tvolte));
            mTVolteHysPreference.setPersistent(false);
            mTVolteHysPreference.setOnPreferenceChangeListener(this);
            getPreferenceScreen().addPreference(mTVolteHysPreference);

            mVdmImsReconfigPreference = new ListPreference(this);
            mVdmImsReconfigPreference.setSummary(getString(R.string.misc_config_vdm));
            mVdmImsReconfigPreference.setPersistent(false);
            mVdmImsReconfigPreference.setEntries(mEGCMDEntries);
            mVdmImsReconfigPreference.setEntryValues(mEGCMDEntriesValue);
            mVdmImsReconfigPreference.setOnPreferenceChangeListener(this);
            getPreferenceScreen().addPreference(mVdmImsReconfigPreference);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        mPhone = PhoneFactory.getPhone(ModemCategory.getCapabilitySim());
        if (FeatureSupport.isSupported(FeatureSupport.FK_CT4GREG_APP)) {
            querySelfRegValue();
        }
        queryVibrateValue();
        querySmsSgsValue();
        if( !( FeatureSupport.is90Modem() || FeatureSupport.is3GOnlyModem()) ){
            queryVolteValue();
        }
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
            Preference preference) {
        if (preference == mSelfRegPreference) {
            boolean sFlag = mSelfRegPreference.isChecked();
            Log.v(TAG, "set SelfReg flag is :" + sFlag);
            SystemProperties.set(FK_MTK_MISC_SELF_REG_CONFIG, sFlag ? "1" :"0");
        }

        if (preference == mVibratePreference) {
            boolean sFlag = mVibratePreference.isChecked();
            Log.v(TAG, "set VibrateValue flag is :" + sFlag);
            SystemProperties.set(FK_MTK_MISC_VIBRATE_CONFIG, sFlag ? "1" :"0");
        }

        if (preference == mSmsSgsPreference) {
            setSgsValue(mSmsSgsPreference.isChecked() ? VALUE_ENABLE : VALUE_DISABLE);
        }

        if (preference == mPresenceSet) {
            Intent intent = new Intent(this, PresenceSet.class);
            startActivity(intent);
        }

        if (preference == mSilentRedialPreference) {
            String enable = mSilentRedialPreference.isChecked() ? VALUE_ENABLE : VALUE_DISABLE;
            sendATCommand(new String[] { "AT+EHVOLTE=4," + enable, "" }, MSG_SET);
        }

        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object objValue) {
        if (preference == mHVolteDeviceModePreference) {
            int index = mHVolteDeviceModePreference.findIndexOfValue((String) objValue);
            Log.v(TAG, "mHVolteDeviceModePreference index is :" + index);
            String value = "";
           if(FeatureSupport.is93Modem()) {
               value = mATCmdValue93md[index];
           }
           else if(FeatureSupport.is92Modem() || FeatureSupport.is91Modem()){
               value = mATCmdValue9192md[index];
           }

            sendATCommand(new String[] { value, "" }, MSG_SET);

            sendATCommand(new String[] {
                    "AT+CEVDP=" + mEntriesValue[index], "" }, MSG_SET);
            mHVolteDeviceModePreference.setSummary(mEntries[index]);
        }
        if (preference == mVdmImsReconfigPreference) {
            int index = mVdmImsReconfigPreference.findIndexOfValue((String) objValue);
            Log.v(TAG, "mVdmImsReconfigPreference index is :" + index);
            sendATCommand(new String[] {
                    "AT+EGCMD=499,0," + mEGCMDEntriesValue[index], "" }, MSG_SET);
            mHVolteDeviceModePreference.setSummary(mEGCMDEntries[index]);
        }

        if (preference == mTVolteHysPreference) {
            String values = (String) objValue ;
            Log.d(TAG, "Volte Hys value = " + values);

           sendATCommand(new String[] {
                        "AT+EVZWT=1,11," + values, "" }, MSG_SET);

            if (mTVolteHysPreference.getText() != null) {
                mTVolteHysPreference.setSummary(
                        getString(R.string.misc_config_tvolte) + " : " + values);
            } else {
                Log.d(TAG, "mTVolteHysPreference.getText() = null");
            }

        }
        return true;
    }

    private void queryVibrateValue() {
        final String sFlag = SystemProperties.get(FK_MTK_MISC_VIBRATE_CONFIG, "1");
        Log.v(TAG, "queryVibrateValue flag is :" + sFlag);

        if (sFlag.equals("0")) {
            mVibratePreference.setChecked(false);
        } else {
            mVibratePreference.setChecked(true);
        }
    }

    private void querySelfRegValue() {
        final String sFlag = SystemProperties.get(FK_MTK_MISC_SELF_REG_CONFIG, "1");
        Log.v(TAG, "querySelfRegValue flag is :" + sFlag);

        if (sFlag.equals("0")) {
            mSelfRegPreference.setChecked(false);
        } else {
            mSelfRegPreference.setChecked(true);
        }
    }
    private void queryVolteValue() {
        queryHVolteDeviceMode();
        querySilentRedialMode();
        queryTVolteHys();
    }

    private void queryHVolteDeviceMode() {
        sendATCommand(new String[] { "AT+CEVDP?", "+CEVDP:" }, MSG_QUERY_HVOLTE);
    }

    private void querySilentRedialMode() {
        sendATCommand(new String[] { "AT+EHVOLTE=4,2", "+EHVOLTE:" }, MSG_QUERY_SLIENT);
    }

    private void queryTVolteHys() {
        sendATCommand(new String[] { "AT+EVZWT=0,11", "+EVZWT:" }, MSG_QUERY_VOLTE_HYS);
    }

    private void querySmsSgsValue() {

        Message msg = mHandler.obtainMessage(MSG_QUERY);
        if (mPhone != null) {
            mPhone.invokeOemRilRequestStrings(new String[] { CMD_QUERY, "+ECFGGET:" }, msg);
        }
        Log.i(TAG, "send " + CMD_QUERY );

    }

    private void setSgsValue(String value) {
        Message msg = mHandler.obtainMessage(MSG_SET);
        mPhone.invokeOemRilRequestStrings(
                new String[] { CMD_SET + ",\"" + value + "\"", "" }, msg);
        Log.i(TAG, "send " + CMD_SET + ",\"" + value + "\"");
    }

    private void parseSmsSgsValue(String data) {
        mSmsSgsPreference.setChecked(VALUE_ENABLE.equals(parseCommandResponse(data)));
    }

    private String parseCommandResponse(String data) {
        Log.d(TAG, "reply data: " + data);
        Pattern p = Pattern.compile("\\+ECFGGET:\\s*\".*\"\\s*,\\s*\"(.*)\"");
        Matcher m = p.matcher(data);
        while (m.find()) {
            String value = m.group(1);
            Log.d(TAG, "sms over sgs support value: " + value);
            return value;
        }
        Log.e(TAG, "wrong format: " + data);
        showToast("wrong format: " + data);
        return "";
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (msg.what == MSG_QUERY) {
                AsyncResult ar = (AsyncResult) msg.obj;
                if (ar.exception == null && ar.result != null) {
                    if (ar.result instanceof String[]) {
                        String[] data = (String[]) ar.result;
                        if (data.length > 0 && (data[0] != null)) {
                            parseSmsSgsValue(data[0]);
                            return;
                        }
                    }
                }
                else{
                    showToast("Query failed");
                }
            } else if (msg.what == MSG_SET) {
                AsyncResult ar = (AsyncResult) msg.obj;
                if (ar.exception == null) {
                    showToast("Set successful");
                    Log.v(TAG, "Set successful");
                } else {
                    showToast("Set failed");
                    Log.v(TAG, "Set failed");
                }
            } else if (msg.what == MSG_QUERY_HVOLTE) {
                AsyncResult ar = (AsyncResult) msg.obj;
                if (ar.exception == null && ar.result != null) {
                    if (ar.result instanceof String[]) {
                        String[] data = (String[]) ar.result;
                        if (data.length > 0 && (data[0] != null)) {
                            String hVolte = "0";
                            try {
                                hVolte = data[0].replace(" ", "")
                                        .substring(("+CEVDP:").length());
                             } catch (Exception e) {
                                 Log.v(TAG, "CEVDP failed ");
                             }
                            Log.v(TAG, "hVolte = " + hVolte);
                            mHVolteDeviceModePreference.setValue(hVolte);
                            if (mHVolteDeviceModePreference.getEntry() != null) {
                                Log.d(TAG, "mHVolteDeviceModePreference.getEntry() = " +
                                        mHVolteDeviceModePreference.getEntry());
                                mHVolteDeviceModePreference.setSummary(
                                        mHVolteDeviceModePreference.getEntry());
                            } else {
                                Log.d(TAG, "mHVolteDeviceModePreference.getEntry() = null");
                            }
                        }
                    }
                }
            } else if (msg.what == MSG_QUERY_SLIENT) {
                AsyncResult ar = (AsyncResult) msg.obj;
                if (ar.exception == null && ar.result != null) {
                    if (ar.result instanceof String[]) {
                        String[] data = (String[]) ar.result;
                        if (data.length > 0 && (data[0] != null)) {
                            Log.v(TAG, "EHVOLTE data[0] = " + data[0]);
                            String slient = "0";
                            try {
                                 slient = data[0].replace(" ", "")
                                         .substring(("+EHVOLTE:4,").length());
                             } catch (Exception e) {
                                 Log.v(TAG, "+EHVOLTE: failed ");
                             }
                            Log.v(TAG, "EHVOLTE data = " + slient);
                            mSilentRedialPreference.setChecked(slient.equals("1") ? true
                                    : false);
                        }
                    }
                }
            } else if (msg.what == MSG_QUERY_VOLTE_HYS) {
                AsyncResult ar = (AsyncResult) msg.obj;
                if (ar.exception == null && ar.result != null) {
                    if (ar.result instanceof String[]) {
                        String[] data = (String[]) ar.result;
                        if (data.length > 0 && (data[0] != null)) {
                            String volteHys = "0";
                            Log.v(TAG, "volteHys data[0] = " + data[0]);
                            try {
                                volteHys = data[0].replace(" ", "")
                                        .substring(("+EVZWT:11,").length());
                            } catch (Exception e) {
                                Log.v(TAG, "+EVZWT:11 failed ");
                            }
                            Log.v(TAG, "volteHys data = " + volteHys);
                            mTVolteHysPreference.setText( volteHys);
                            mTVolteHysPreference.setSummary(
                                    getString(R.string.misc_config_tvolte) + " : " + volteHys);

                        }
                    }
                }
            }
        }
    };

    private void showToast(String msg) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        mToast.show();
    }

    private void sendATCommand(String[] atCommand, int msg) {
        Log.d(TAG, "send at cmd : " + atCommand[0]);
        if (mPhone != null) {
            mPhone.invokeOemRilRequestStrings(atCommand, mHandler.obtainMessage(msg));
        } else {
            showToast("The phone is null!");
        }
    }
}
