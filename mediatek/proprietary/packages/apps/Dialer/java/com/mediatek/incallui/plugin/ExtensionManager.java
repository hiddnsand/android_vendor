/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.incallui.plugin;

import android.annotation.NonNull;
import android.content.Context;

import com.android.dialer.common.Assert;
import com.android.incallui.Log;

import com.mediatek.incallui.ext.ICallCardExt;
import com.mediatek.incallui.ext.IEmergencyCallCardExt;
import com.mediatek.incallui.ext.IInCallButtonExt;
import com.mediatek.incallui.ext.IInCallExt;
import com.mediatek.incallui.ext.IRCSeCallButtonExt;
import com.mediatek.incallui.ext.IRCSeCallCardExt;
import com.mediatek.incallui.ext.IRCSeInCallExt;
import com.mediatek.incallui.ext.IStatusBarExt;
import com.mediatek.incallui.ext.IVideoCallExt;
import com.mediatek.incallui.ext.IVilteAutoTestHelperExt;
import com.mediatek.incallui.ext.OpInCallUICustomizationFactoryBase;

public final class ExtensionManager {
    private static Context sApplicationContext;

    private ExtensionManager() {
    }

    public static void registerApplicationContext(@NonNull Context context) {
        if (sApplicationContext == null) {
            sApplicationContext = context;
        }
    }

    private static IRCSeCallButtonExt sRCSeCallButtonExt;
    private static IRCSeCallCardExt sRCSeCallCardExt;
    private static IRCSeInCallExt sRCSeInCallExt;
    private static IInCallExt sInCallExt;
    private static ICallCardExt sCallCardExt;
    private static IEmergencyCallCardExt sEmergencyCallCardExt;
    private static IVideoCallExt sVideoCallExt;
    private static IInCallButtonExt sInCallButtonExt;
    private static IStatusBarExt sStatusBarExt;
    private static IVilteAutoTestHelperExt sVilteAutoTestHelperExt;

    public static IRCSeCallButtonExt getRCSeCallButtonExt() {
        synchronized (IRCSeCallButtonExt.class) {
            if (sRCSeCallButtonExt == null) {
                Assert.checkState(sApplicationContext != null);
                sRCSeCallButtonExt = OpInCallUICustomizationFactoryBase
                        .getRcsFactory(sApplicationContext).getRCSeCallButtonExt(sApplicationContext);
            }
        }
        return sRCSeCallButtonExt;
    }

    public static IRCSeCallCardExt getRCSeCallCardExt() {
        synchronized (IRCSeCallCardExt.class) {
            if (sRCSeCallCardExt == null) {
                Assert.checkState(sApplicationContext != null);
                sRCSeCallCardExt = OpInCallUICustomizationFactoryBase
                        .getRcsFactory(sApplicationContext).getRCSeCallCardExt(sApplicationContext);
            }
        }
        return sRCSeCallCardExt;
    }

    public static IRCSeInCallExt getRCSeInCallExt() {
        synchronized (IRCSeInCallExt.class) {
            if (sRCSeInCallExt == null) {
                Assert.checkState(sApplicationContext != null);
                sRCSeInCallExt = OpInCallUICustomizationFactoryBase
                        .getRcsFactory(sApplicationContext).getRCSeInCallExt(sApplicationContext);
            }
        }
        return sRCSeInCallExt;
    }

    /**
     * retrieve the instance of IICallExt.
     * @return the instance of IICallExt.
     */
    public static IInCallExt getInCallExt() {
        synchronized (IInCallExt.class) {
            if (sInCallExt == null) {
                Assert.checkState(sApplicationContext != null);
                sInCallExt = OpInCallUICustomizationFactoryBase
                        .getOpFactory(sApplicationContext).getInCallExt();
            }
        }
        return sInCallExt;
    }

    public static ICallCardExt getCallCardExt() {
        synchronized (ICallCardExt.class) {
            if (sCallCardExt == null) {
                Assert.checkState(sApplicationContext != null);
                sCallCardExt = OpInCallUICustomizationFactoryBase
                        .getOpFactory(sApplicationContext).getCallCardExt();
            }
        }
        return sCallCardExt;
    }

    public static IEmergencyCallCardExt getEmergencyCallCardExt() {
        synchronized (IEmergencyCallCardExt.class) {
            if (sEmergencyCallCardExt == null) {
                Assert.checkState(sApplicationContext != null);
                sEmergencyCallCardExt = OpInCallUICustomizationFactoryBase
                        .getOpFactory(sApplicationContext).getEmergencyCallCardExt();
            }
        }
        return sEmergencyCallCardExt;
    }

    public static IVideoCallExt getVideoCallExt() {
        synchronized (IVideoCallExt.class) {
            if (sVideoCallExt == null) {
                Assert.checkState(sApplicationContext != null);
                sVideoCallExt = OpInCallUICustomizationFactoryBase
                        .getOpFactory(sApplicationContext).getVideoCallExt();
            }
        }
        return sVideoCallExt;
    }

    public static IInCallButtonExt getInCallButtonExt() {
        synchronized (IInCallButtonExt.class) {
            if (sInCallButtonExt == null) {
                Assert.checkState(sApplicationContext != null);
                sInCallButtonExt = OpInCallUICustomizationFactoryBase
                        .getOpFactory(sApplicationContext).getInCallButtonExt();
            }
        }
        return sInCallButtonExt;
    }

    public static IStatusBarExt getStatusBarExt() {
        synchronized (IStatusBarExt.class) {
            if (sStatusBarExt == null) {
                Assert.checkState(sApplicationContext != null);
                sStatusBarExt = OpInCallUICustomizationFactoryBase
                        .getOpFactory(sApplicationContext).getStatusBarExt();
            }
        }
        return sStatusBarExt;
    }

     public static IVilteAutoTestHelperExt getVilteAutoTestHelperExt() {
        synchronized (IVilteAutoTestHelperExt.class) {
            if (sVilteAutoTestHelperExt == null) {
                Assert.checkState(sApplicationContext != null);
                sVilteAutoTestHelperExt = OpInCallUICustomizationFactoryBase
                        .getOpFactory(sApplicationContext).getVilteAutoTestHelperExt();
            }
        }
        return sVilteAutoTestHelperExt;
    }
}
