/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * PvrTextureLoader Implementation
 *
 */

#define ENABLE_OGL_ERROR_CHECK

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <pvrtextureloader.h>             /* This class's API                */
#include <a3m/PVRTTextureAPI.h>           /* For LoadPVR                     */
#include <a3m/log.h>                      /* for logging                     */
#include <a3m/detail/assetpath.h>         /* AssetPath                       */
#include <fileutility.h>                  /* FileToString etc.               */
#include <stdstringutility.h>             /* for endsWithI()                 */

namespace a3m
{
  /*
   * Loads a new 2D texture from the file system.
   */
  Texture2D::Ptr PvrTextureLoader::load(
    Texture2DCache& cache, A3M_CHAR8 const* name )
  {
    Stream::Ptr image = cache.getStream(name);
    if (!image)
    {
      return Texture2D::Ptr();
    }
    FileToString buffer( *image );

    Texture2D::Ptr texturePtr;

    A3M_UINT32 id, width, height, bitsPerPixel, mipMapLevels;

    if( LoadPVR( buffer.get(), id, width, height, bitsPerPixel, mipMapLevels ) )
    {
      A3M_BOOL hasMipMaps = (mipMapLevels > 0);

      texturePtr = cache.create(
                     width,
                     height,
                     static_cast<A3M_FLOAT>(bitsPerPixel) / 8.0f,
                     hasMipMaps,
                     id,
                     name );
    }

    /* Return a null pointer if failed to load */
    return texturePtr;
  }

  A3M_BOOL PvrTextureLoader::isKnown(A3M_CHAR8 const* name)
  {
    return endsWithI(name, ".pvr");
  }

} /* namespace a3m */
