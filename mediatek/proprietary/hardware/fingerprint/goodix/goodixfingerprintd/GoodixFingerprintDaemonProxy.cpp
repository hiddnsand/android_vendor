/*
 * Copyright (C) 2013-2016, Shenzhen Huiding Technology Co., Ltd.
 * All Rights Reserved.
 */

/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "goodixfingerprintd"

#include <binder/IServiceManager.h>
#include <hardware/hardware.h>
#include <utils/Log.h>

#include "GoodixFingerprintDaemonProxy.h"
#include "gf_hal.h"
#include "IFingerService.h"
#ifdef SUPPORT_FIDO
#include "fido_ca_entry.h"
#include "fido_type_define.h"
#endif


namespace android {

GoodixFingerprintDaemonProxy* GoodixFingerprintDaemonProxy::sInstance = NULL;
static uint32_t g_group_id = 0;
static uint32_t g_sync_done = 0;

GoodixFingerprintDaemonProxy::GoodixFingerprintDaemonProxy() : mDevice(NULL), mCallback(NULL), mTestCallback(NULL) {
#ifdef SUPPORT_FIDO
    ALOGD("begin to open fido session.");
    fido_error_t ret = fido_ca_open_session();
    ALOGD(" open fido session ret(%d).", ret);
#endif
}

GoodixFingerprintDaemonProxy::~GoodixFingerprintDaemonProxy() {
    closeHal();
#ifdef SUPPORT_FIDO
    ALOGD("begin to close fido session.");
    fido_ca_close_session();
#endif
}

void GoodixFingerprintDaemonProxy::hal_notify_callback(const gf_fingerprint_msg_t *msg) {
    GoodixFingerprintDaemonProxy* instance = GoodixFingerprintDaemonProxy::getInstance();
    const sp<IGoodixFingerprintDaemonCallback> callback = instance->mCallback;
    const sp<IGoodixFingerprintDaemonCallback> testCallback = instance->mTestCallback;
    if (callback == NULL) {
        ALOGE("Invalid callback object");
        return;
    }
    const int64_t device = (int64_t) instance->mDevice;
    switch (msg->type) {
        case GF_FINGERPRINT_ERROR:
            ALOGD("onError(%d)", msg->data.error);
            callback->onError(device, msg->data.error);
            break;
        case GF_FINGERPRINT_ACQUIRED:
            ALOGD("onAcquired(%d)", msg->data.acquired.acquired_info);
            callback->onAcquired(device, msg->data.acquired.acquired_info);
            break;
        case GF_FINGERPRINT_AUTHENTICATED:
            ALOGD("onAuthenticated(fid=%u, gid=%u)",
                    msg->data.authenticated.finger.fid,
                    msg->data.authenticated.finger.gid);
            callback->onAuthenticated(device,
                    msg->data.authenticated.finger.fid,
                    msg->data.authenticated.finger.gid,
                    reinterpret_cast<const uint8_t *>(&msg->data.authenticated.hat),
                    sizeof(gf_hw_auth_token_t));
            break;
        case GF_FINGERPRINT_TEMPLATE_ENROLLING:
            ALOGD("onEnrollResult(fid=%u, gid=%u, rem=%u)",
                    msg->data.enroll.finger.fid,
                    msg->data.enroll.finger.gid,
                    msg->data.enroll.samples_remaining);
            callback->onEnrollResult(device,
                    msg->data.enroll.finger.fid,
                    msg->data.enroll.finger.gid,
                    msg->data.enroll.samples_remaining);
            if(0 == msg->data.enroll.samples_remaining) {
                g_sync_done = 0;
            }
            break;
        case GF_FINGERPRINT_TEMPLATE_REMOVED:
            ALOGD("onRemove(fid=%u, gid=%u)",
                    msg->data.removed.finger.fid,
                    msg->data.removed.finger.gid);
            callback->onRemoved(device,
                    msg->data.removed.finger.fid,
                    msg->data.removed.finger.gid);
            break;
        case GF_FINGERPRINT_HBD:
            ALOGD("onHBD.");
            if(testCallback == NULL) {
                ALOGE("Invalid testCallback object");
                return;
            }

            testCallback->onHbdData(device, msg->data.hbd.heart_beat_rate, msg->data.hbd.status,
                    msg->data.hbd.display_data, msg->data.hbd.data_len, msg->data.hbd.raw_data, msg->data.hbd.raw_data_len);
            break;
        case GF_FINGERPRINT_ENUMERATED:
            ALOGD("onEnumerate.");
            callback->onEnumerate(device, msg->data.enumerated.finger.fid, msg->data.enumerated.finger.gid,
                    msg->data.enumerated.remaining_templates);
            break;
        case GF_FINGERPRINT_AUTHENTICATED_FIDO: {
                ALOGD("Authenticated Fido fingerId: %d", msg->data.authenticated_fido.finger_id);
                if(testCallback == NULL) {
                   ALOGE("Invalid testCallback object");
                   break;
               }
                testCallback->onAuthenticatedFido(device, msg->data.authenticated_fido.finger_id, msg->data.authenticated_fido.uvt_data,
                        msg->data.authenticated_fido.uvt_len);
                break;
            }

        default:
            ALOGE("invalid msg type: %u", msg->type);
            return;
    }
}

void GoodixFingerprintDaemonProxy::hal_test_notify_callback(const gf_fingerprint_msg_t *msg) {
    GoodixFingerprintDaemonProxy* instance = GoodixFingerprintDaemonProxy::getInstance();
    const sp<IGoodixFingerprintDaemonCallback> testCallback = instance->mTestCallback;
    if (testCallback == NULL) {
        ALOGE("Invalid callback object");
        return;
    }
    const int64_t device = (int64_t) instance->mDevice;
    switch (msg->type) {
        case GF_FINGERPRINT_ERROR:
            ALOGD("onError(%d)", msg->data.error);
            testCallback->onError(device, msg->data.error);
            break;
        case GF_FINGERPRINT_ACQUIRED:
            ALOGD("onAcquired(%d)", msg->data.acquired.acquired_info);
            testCallback->onAcquired(device, msg->data.acquired.acquired_info);
            break;
        case GF_FINGERPRINT_AUTHENTICATED:
            ALOGD("onAuthenticated(fid=%u, gid=%u)",
                    msg->data.authenticated.finger.fid,
                    msg->data.authenticated.finger.gid);
            testCallback->onAuthenticated(device,
                    msg->data.authenticated.finger.fid,
                    msg->data.authenticated.finger.gid,
                    reinterpret_cast<const uint8_t *>(&msg->data.authenticated.hat),
                    sizeof(gf_hw_auth_token_t));
            break;
        case GF_FINGERPRINT_TEMPLATE_ENROLLING:
            ALOGD("onEnrollResult(fid=%u, gid=%u, rem=%u)",
                    msg->data.enroll.finger.fid,
                    msg->data.enroll.finger.gid,
                    msg->data.enroll.samples_remaining);
            testCallback->onEnrollResult(device,
                    msg->data.enroll.finger.fid,
                    msg->data.enroll.finger.gid,
                    msg->data.enroll.samples_remaining);
            break;
        case GF_FINGERPRINT_TEMPLATE_REMOVED:
            ALOGD("onRemove(fid=%u, gid=%u)",
                    msg->data.removed.finger.fid,
                    msg->data.removed.finger.gid);
            testCallback->onRemoved(device,
                    msg->data.removed.finger.fid,
                    msg->data.removed.finger.gid);
            break;
        case GF_FINGERPRINT_TEST_CMD:
            ALOGD("onTestCmd(cmd_id = %u, result = %s, result_len = %u)",
                    msg->data.test.cmd_id, msg->data.test.result, msg->data.test.result_len);
            if(testCallback == NULL) {
                ALOGE("Invalid testCallback object");
                return;
            }
            testCallback->onTestCmd(device, msg->data.test.cmd_id,
                    (const int8_t *)(msg->data.test.result),
                    msg->data.test.result_len);
            break;
        case GF_FINGERPRINT_HBD:
            ALOGD("onHBD.");
            if(testCallback == NULL) {
                ALOGE("Invalid testCallback object");
                return;
            }

            testCallback->onHbdData(device, msg->data.hbd.heart_beat_rate, msg->data.hbd.status,
                    msg->data.hbd.display_data, msg->data.hbd.data_len, msg->data.hbd.raw_data, msg->data.hbd.raw_data_len);
            break;
        case GF_FINGERPRINT_ENUMERATED:
            ALOGD("onEnumerate.");
            testCallback->onEnumerate(device, msg->data.enumerated.finger.fid, msg->data.enumerated.finger.gid,
                    msg->data.enumerated.remaining_templates);
            break;
        case GF_FINGERPRINT_AUTHENTICATED_FIDO: {
            ALOGD("Authenticated Fido fingerId: %d", msg->data.authenticated_fido.finger_id);
            if(testCallback == NULL) {
               ALOGE("Invalid testCallback object");
               break;
           }

            testCallback->onAuthenticatedFido(device, msg->data.authenticated_fido.finger_id, msg->data.authenticated_fido.uvt_data,
                    msg->data.authenticated_fido.uvt_len);
            break;
        }
        default:
            ALOGE("invalid msg type: %u", msg->type);
            return;
    }
}

void GoodixFingerprintDaemonProxy::hal_dump_notify_callback(const gf_fingerprint_dump_msg_t __unused *msg) {
    GoodixFingerprintDaemonProxy* instance = GoodixFingerprintDaemonProxy::getInstance();
    const sp<IGoodixFingerprintDaemonCallback> testCallback = instance->mTestCallback;
    if (testCallback == NULL) {
        ALOGE("Invalid callback object");
        return;
    }
    const int64_t device = (int64_t) instance->mDevice;

    if(testCallback == NULL) {
        ALOGE("Invalid testCallback object");
        return;
    }
    testCallback->onDump(device, msg->cmd_id, (const int8_t *)(msg->data), msg->data_len);
}

void GoodixFingerprintDaemonProxy::init(const sp<IGoodixFingerprintDaemonCallback>& callback) {
    if (mCallback != NULL && IInterface::asBinder(callback) != IInterface::asBinder(mCallback)) {
        IInterface::asBinder(mCallback)->unlinkToDeath(this);
    }
    IInterface::asBinder(callback)->linkToDeath(this);
    mCallback = callback;
}

int32_t GoodixFingerprintDaemonProxy::enroll(const uint8_t* token, ssize_t tokenSize, int32_t groupId,
        int32_t timeout) {
    ALOG(LOG_VERBOSE, LOG_TAG, "enroll(gid=%d, timeout=%d)\n", groupId, timeout);
    if (tokenSize != sizeof(gf_hw_auth_token_t) ) {
        ALOG(LOG_VERBOSE, LOG_TAG, "enroll() : invalid token size %zu\n", tokenSize);
        return -1;
    }
    const gf_hw_auth_token_t* authToken = reinterpret_cast<const gf_hw_auth_token_t*>(token);
    return gf_hal_enroll(mDevice, authToken, groupId, timeout);
}

uint64_t GoodixFingerprintDaemonProxy::preEnroll() {
    this->syncFingerList();
    return gf_hal_pre_enroll(mDevice);
}

int32_t GoodixFingerprintDaemonProxy::postEnroll() {
    return gf_hal_post_enroll(mDevice);
}

int32_t GoodixFingerprintDaemonProxy::stopEnrollment() {
    ALOG(LOG_VERBOSE, LOG_TAG, "google cancel\n");
    return gf_hal_cancel(mDevice);
}

int32_t GoodixFingerprintDaemonProxy::authenticate(uint64_t sessionId, uint32_t groupId) {
    ALOG(LOG_VERBOSE, LOG_TAG, "authenticate(gid=%d)\n", groupId);
    int32_t ret = gf_hal_authenticate(mDevice, sessionId, groupId);
    this->syncFingerList();
    return ret;
}

int32_t GoodixFingerprintDaemonProxy::stopAuthentication() {
    ALOG(LOG_VERBOSE, LOG_TAG, "google cancel\n");
    return gf_hal_cancel(mDevice);
}

int32_t GoodixFingerprintDaemonProxy::remove(int32_t fingerId, int32_t groupId) {
    ALOG(LOG_VERBOSE, LOG_TAG, "remove(fid=%d, gid=%d)\n", fingerId, groupId);
    return gf_hal_remove(mDevice, groupId, fingerId);
}

uint64_t GoodixFingerprintDaemonProxy::getAuthenticatorId() {
    return gf_hal_get_auth_id(mDevice);
}

int32_t GoodixFingerprintDaemonProxy::setActiveGroup(int32_t groupId, const uint8_t* path,
        ssize_t pathlen) {
    if (pathlen >= PATH_MAX || pathlen <= 0) {
        ALOGE("Bad path length: %zd", pathlen);
        return -1;
    }

    // Convert to null-terminated string
    char path_name[PATH_MAX];
    memcpy(path_name, path, pathlen);
    path_name[pathlen] = '\0';
    ALOG(LOG_VERBOSE, LOG_TAG, "setActiveGroup(%d, %s, %zu)", groupId, path_name, pathlen);
    g_group_id = groupId;

    int32_t err = gf_hal_set_active_group(mDevice, groupId);
    return err;
}

int32_t GoodixFingerprintDaemonProxy::setSafeClass(uint32_t safeClass) {
    return gf_hal_set_safe_class(mDevice, (gf_safe_class_t)safeClass);
}

int32_t GoodixFingerprintDaemonProxy::navigate(uint32_t navMode) {
    ALOGD("navigate()\n");
    return gf_hal_navigate(mDevice, (gf_nav_mode_t)navMode);
}

int32_t GoodixFingerprintDaemonProxy::stopNavigation() {
    ALOGD("stopNavigation()\n");
    return gf_hal_test_cancel(mDevice);
}

int32_t GoodixFingerprintDaemonProxy::enableFingerprintModule(uint8_t enableFlag) {
    return gf_hal_enable_fingerprint_module(mDevice, enableFlag);
}

int32_t GoodixFingerprintDaemonProxy::cameraCapture() {
    return gf_hal_camera_capture(mDevice);
}

int32_t GoodixFingerprintDaemonProxy::stopCameraCapture() {
    return gf_hal_test_cancel(mDevice);
}

int32_t GoodixFingerprintDaemonProxy::enableFfFeature(uint8_t enableFlag) {
    return gf_hal_enable_ff_feature(mDevice, enableFlag);
}

void GoodixFingerprintDaemonProxy::testInit(const sp<IGoodixFingerprintDaemonCallback>& callback) {
    if (mTestCallback != NULL && IInterface::asBinder(callback) != IInterface::asBinder(mTestCallback)) {
        IInterface::asBinder(mTestCallback)->unlinkToDeath(this);
    }
    IInterface::asBinder(callback)->linkToDeath(this);
    mTestCallback = callback;
}

int32_t GoodixFingerprintDaemonProxy::testCmd(uint32_t cmdId, const uint8_t* param, uint32_t paramLen) {
    return gf_hal_test_cmd(mDevice, cmdId, param, paramLen);
}

int32_t GoodixFingerprintDaemonProxy::screenOn() {
    return gf_hal_screen_on();
}

int32_t GoodixFingerprintDaemonProxy::screenOff() {
    return gf_hal_screen_off();
}

int32_t GoodixFingerprintDaemonProxy::startHbd(){
    return gf_hal_start_hbd(mDevice);
}

int32_t GoodixFingerprintDaemonProxy::stopHbd(){
    ALOG(LOG_VERBOSE, LOG_TAG, "stopHbd()\n");
    return gf_hal_test_cancel(mDevice);
}

int32_t GoodixFingerprintDaemonProxy::enumerate() {
    ALOG(LOG_VERBOSE, LOG_TAG, "enumerate()\n");
    return gf_hal_enumerate_with_callback(mDevice);
}

int32_t GoodixFingerprintDaemonProxy::reset_lockout(const uint8_t *token, ssize_t tokenLength) {
    ALOG(LOG_VERBOSE, LOG_TAG, "reset_lockout()\n");
    if (tokenLength != sizeof(gf_hw_auth_token_t) ) {
        ALOG(LOG_VERBOSE, LOG_TAG, "enroll() : invalid token size %zu\n", tokenLength);
        return -1;
    }
    const gf_hw_auth_token_t* authToken = reinterpret_cast<const gf_hw_auth_token_t*>(token);
    return gf_hal_reset_lockout(mDevice, authToken);
}

int32_t GoodixFingerprintDaemonProxy::enableBioAssayFeature(uint8_t enableFlag) {
    return gf_hal_enable_bio_assay_feature(mDevice, enableFlag);
}

void GoodixFingerprintDaemonProxy::syncFingerList() {
#if ((defined SYNC_FINGER_LIST_BASED_ON_SETTINGS) || (defined SYNC_FINGER_LIST_BASED_ON_TEE))
    if(1 == g_sync_done){
        return;
     }

    ALOG(LOG_VERBOSE, LOG_TAG, "syncFingerList()\n");

    sp < IServiceManager > sm = defaultServiceManager();
    sp < IBinder > binder = sm->getService(String16(FINGER_SERVICE));
    sp < IFingerService > fingerservice = interface_cast < IFingerService > (binder);
    if (fingerservice == NULL) {
        ALOG(LOG_VERBOSE, LOG_TAG, "can't get IFingerService\n");
        return;
    }
    uint32_t* list = NULL;
    uint32_t tee_finger_list[10];
    int32_t count = 0;
    int32_t tee_finger_count = 10;

    count = fingerservice->getEnrolledFingerprints(g_group_id, &list);
    if (count < 0) {
        ALOG(LOG_VERBOSE, LOG_TAG, "can't get list from framework\n");
        return;
    }

    gf_error_t err = gf_hal_get_id_list(mDevice, g_group_id, tee_finger_list, &tee_finger_count);

    if (GF_SUCCESS != err) {
        ALOG(LOG_VERBOSE, LOG_TAG, "get id list error\n");
        return;
    }

    do {
#ifdef SYNC_FINGER_LIST_BASED_ON_SETTINGS
        for (int32_t i = 0; i < tee_finger_count; i++) {
            ALOG(LOG_DEBUG, LOG_TAG, "[%s] lower list, finger_id[%d]=%u", __func__, i, tee_finger_list[i]);
            int32_t flag = 0;
            for (int32_t j = 0; j < count; j++) {
                ALOG(LOG_DEBUG, LOG_TAG, "[%s] upper list, finger_id[%d]=%u", __func__, j, list[j]);
                if (tee_finger_list[i] == list[j]) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                err = gf_hal_remove(mDevice, g_group_id, tee_finger_list[i]);

                if (err != GF_SUCCESS) {
                   ALOG(LOG_ERROR, LOG_TAG, "[%s] something wrong happens.", __func__);
                }
            }
        }
#endif
#ifdef SYNC_FINGER_LIST_BASED_ON_TEE
        for (int32_t i = 0; i < count; i++) {
            ALOG(LOG_DEBUG, LOG_TAG, "[%s] upper list, finger_id[%d]=%u", __func__, i, list[i]);
            int32_t flag = 0;
            for (int32_t j = 0; j < tee_finger_count; j++) {
                ALOG(LOG_DEBUG, LOG_TAG, "[%s] lower list, finger_id[%d]=%u", __func__, j, tee_finger_list[j]);
                if (list[i] == tee_finger_list[j]) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                int32_t err = fingerservice->remove(g_group_id, list[i]);
                if(err != GF_SUCCESS){
                    ALOG(LOG_ERROR, LOG_TAG, "remval cant' be done! code:%d\n", err);
                }
                else{
                    ALOG(LOG_DEBUG, LOG_TAG, "removal is done!\n");
                }
            }
        }
#endif
    } while (0);

    if (NULL != list) {
        free(list);
    }

    g_sync_done = 1;
#endif
}

int32_t GoodixFingerprintDaemonProxy::dumpCmd(uint32_t cmdId, const uint8_t* param, uint32_t paramLen) {
    return gf_hal_dump_cmd(mDevice, cmdId, param, paramLen);
}

int32_t GoodixFingerprintDaemonProxy::authenticateFido(uint32_t groupId, uint8_t *aaid,
    uint32_t aaidLen, uint8_t *finalChallenge, uint32_t challengeLen) {
    ALOG(LOG_VERBOSE, LOG_TAG, "authenticateFido()\n");
    return gf_hal_authenticate_fido(mDevice, groupId, aaid, aaidLen, finalChallenge, challengeLen);
}

int32_t GoodixFingerprintDaemonProxy::stopAuthenticateFido() {
    ALOG(LOG_VERBOSE, LOG_TAG, "stopAuthenticateFido()\n");
    return gf_hal_test_cancel(mDevice);
}

int32_t GoodixFingerprintDaemonProxy::isIdValid(uint32_t groupId, uint32_t fingerId) {
    ALOG(LOG_VERBOSE, LOG_TAG, "isIdValid()\n");
    return gf_hal_is_id_valid(mDevice, groupId, fingerId);
}

int32_t GoodixFingerprintDaemonProxy::getIdList(uint32_t groupId, int32_t *list, int32_t *count) {
    ALOG(LOG_VERBOSE, LOG_TAG, "getIdList()\n");
    return gf_hal_get_id_list(mDevice, groupId, (uint32_t *)list, count);
}

#ifdef SUPPORT_FIDO
int32_t GoodixFingerprintDaemonProxy::invoke_fido_command(uint8_t *in_buf, uint32_t in_buf_len,
        uint8_t *out_buf, uint32_t *out_buf_len) {

    ALOGD("invoke_fido_command.");
    int32_t ret = 0;

    do {
        ALOGD(" in_buf_len:%d, out_buf_len:%d", in_buf_len, *out_buf_len);
        if (in_buf_len > MAX_BUFFER_LENGTH) {
            ALOGE(" in_buf_len:%d is larger than MAX_BUFFER_LENGTH:%d.", in_buf_len, MAX_BUFFER_LENGTH);
            ret = FIDO_ERROR_BAD_PARAMS;
            break;
        }

        fido_cmd_t cmd;
        cmd.cmd_id = FIDO_CMD_AK_PROCCESS;
        cmd.data.ak_process.in_buf_len = in_buf_len;
        memcpy(cmd.data.ak_process.in_buf, in_buf, in_buf_len);

        memset(cmd.data.ak_process.out_buf, 0, MAX_BUFFER_LENGTH);
        cmd.data.ak_process.out_buf_len = *out_buf_len;

        fido_error_t err = fido_ca_invoke_command(&cmd, sizeof(cmd));
        if (FIDO_SUCCESS != err) {
            ALOGD(" fido_ca_invoke_command ret:%d", ret);
            ret = err;
            break;
        }

        if (cmd.data.ak_process.out_buf_len > *out_buf_len) {
            ALOGE(" ak proccess out buf len:%d is larger than *out_buf_len:%d.", cmd.data.ak_process.out_buf_len, *out_buf_len);
            ret = FIDO_ERROR_BAD_PARAMS;
            break;
        }

        *out_buf_len = cmd.data.ak_process.out_buf_len;
        memcpy(out_buf, cmd.data.ak_process.out_buf, *out_buf_len);

    } while(0);

    ALOGD(" exit, ret:%d.", ret);
    return ret;
}
#else
int32_t GoodixFingerprintDaemonProxy::invoke_fido_command(uint8_t __unused *in_buf, uint32_t __unused in_buf_len,
        uint8_t __unused *out_buf, uint32_t __unused *out_buf_len) {
    ALOGD("fido is not supported.");
    return 0;
}
#endif

int64_t GoodixFingerprintDaemonProxy::openHal() {
    ALOG(LOG_VERBOSE, LOG_TAG, "nativeOpenHal()\n");
    mDevice = (gf_fingerprint_device_t *)malloc(sizeof(gf_fingerprint_device_t));
    memset(mDevice, 0, sizeof(gf_fingerprint_device_t));
    mDevice->notify = hal_notify_callback;
    mDevice->test_notify = hal_test_notify_callback;
    mDevice->dump_notify = hal_dump_notify_callback;
    gf_error_t err = gf_hal_open(mDevice);
    if (GF_SUCCESS != err) {
        return -EINVAL;
    }

    return 0;
}

int32_t GoodixFingerprintDaemonProxy::closeHal() {
    ALOG(LOG_VERBOSE, LOG_TAG, "nativeCloseHal()\n");
    gf_hal_close(mDevice);

    if(mDevice != NULL) {
        free(mDevice);
        mDevice = NULL;
    }

    return 0;
}

void GoodixFingerprintDaemonProxy::binderDied(const wp<IBinder>& who) {
    ALOGD("binder died");
    if (IInterface::asBinder(mTestCallback) == who) {
        mTestCallback = NULL;
        return;
    }

    int err;
    if (0 != (err = closeHal())) {
        ALOGE("Can't close fingerprint device, error: %d", err);
    }
    if (IInterface::asBinder(mCallback) == who) {
        mCallback = NULL;
    }
}

}
