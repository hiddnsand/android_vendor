/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 */

#ifndef _MTK_PROGRAM_H
#define _MTK_PROGRAM_H

#include "Program.h"

namespace android {
namespace uirenderer {

/**
 * Describe the features required for a given program. The features
 * determine the generation of both the vertex and fragment shaders.
 * A ProgramDescription must be used in conjunction with a ProgramCache.
 */
struct MTKProgramDescription : ProgramDescription {
    void set(programid key) {
        reset();
        if (key & PROGRAM_KEY_TEXTURE) hasTexture = true;
        if (key & PROGRAM_KEY_A8_TEXTURE) hasAlpha8Texture = true;
        if (key & PROGRAM_KEY_BITMAP) hasBitmap = true;
        if (key & PROGRAM_KEY_BITMAP_EXTERNAL) isShaderBitmapExternal = true;
        if (key & PROGRAM_KEY_BITMAP_NPOT) useShaderBasedWrap = true;
        if (useShaderBasedWrap) {
            if (key & (getEnumForWrap(GL_REPEAT) << PROGRAM_BITMAP_WRAPS_SHIFT)) {
                bitmapWrapS = GL_REPEAT;
            } else if (key & (getEnumForWrap(GL_MIRRORED_REPEAT) << PROGRAM_BITMAP_WRAPS_SHIFT)) {
                bitmapWrapS = GL_MIRRORED_REPEAT;
            }
            if (key & (getEnumForWrap(GL_REPEAT) << PROGRAM_BITMAP_WRAPT_SHIFT)) {
                bitmapWrapT = GL_REPEAT;
            } else if (key & (getEnumForWrap(GL_MIRRORED_REPEAT) << PROGRAM_BITMAP_WRAPT_SHIFT)) {
                bitmapWrapT = GL_MIRRORED_REPEAT;
            }
        }
        if (key & PROGRAM_KEY_GRADIENT) hasGradient = true;
        if (key & (programid(kGradientCircular) << PROGRAM_GRADIENT_TYPE_SHIFT)) gradientType = kGradientCircular;
        else if (key & (programid(kGradientSweep) << PROGRAM_GRADIENT_TYPE_SHIFT)) gradientType = kGradientSweep;
        if (key & PROGRAM_KEY_BITMAP_FIRST) isBitmapFirst = true;
        if (hasBitmap && hasGradient) {
            shadersMode = (SkBlendMode)((key >> PROGRAM_XFERMODE_SHADER_SHIFT) & PROGRAM_MAX_XFERMODE);
        }
        if (key & PROGRAM_KEY_COLOR_MATRIX) colorOp = ColorFilterMode::Matrix;
        else if (key & PROGRAM_KEY_COLOR_BLEND) colorOp = ColorFilterMode::Blend;
        if (colorOp == ColorFilterMode::Blend) {
            colorMode = (SkBlendMode)((key >> PROGRAM_XFERMODE_COLOR_OP_SHIFT) & PROGRAM_MAX_XFERMODE);
        }

        framebufferMode = (SkBlendMode)((key >> PROGRAM_XFERMODE_FRAMEBUFFER_SHIFT) & PROGRAM_MAX_XFERMODE);
        if (key & programid(0x1) << PROGRAM_KEY_SWAP_SRC_DST_SHIFT) swapSrcDst = true;
        if (key & programid(0x1) << PROGRAM_MODULATE_SHIFT) modulate = true;
        if (key & programid(0x1) << PROGRAM_HAS_VERTEX_ALPHA_SHIFT) hasVertexAlpha = true;
        if (key & programid(0x1) << PROGRAM_USE_SHADOW_ALPHA_INTERP_SHIFT) useShadowAlphaInterp = true;
        if (key & programid(0x1) << PROGRAM_HAS_EXTERNAL_TEXTURE_SHIFT) hasExternalTexture = true;
        if (key & programid(0x1) << PROGRAM_HAS_TEXTURE_TRANSFORM_SHIFT) hasTextureTransform = true;
        if (key & programid(0x1) << PROGRAM_IS_SIMPLE_GRADIENT) isSimpleGradient = true;
        if (key & programid(0x1) << PROGRAM_HAS_COLORS) hasColors = true;
        if (key & programid(0x1) << PROGRAM_HAS_DEBUG_HIGHLIGHT) hasDebugHighlight = true;
        if (key & programid(0x1) << PROGRAM_HAS_ROUND_RECT_CLIP) hasRoundRectClip = true;
        if (key & programid(0x1) << PROGRAM_HAS_GAMMA_CORRECTION) hasGammaCorrection = true;
        if (key & programid(0x1) << PROGRAM_HAS_LINEAR_TEXTURE) hasLinearTexture = true;
        if (key & programid(0x1) << PROGRAM_HAS_COLOR_SPACE_CONVERSION) hasColorSpaceConversion = true;
        if (key & (programid(TransferFunctionType::Full) << PROGRAM_TRANSFER_FUNCTION))
            transferFunction = TransferFunctionType::Full;
        else if (key & (programid(TransferFunctionType::Limited) << PROGRAM_TRANSFER_FUNCTION))
            transferFunction = TransferFunctionType::Limited;
        else if (key & (programid(TransferFunctionType::Gamma) << PROGRAM_TRANSFER_FUNCTION))
            transferFunction = TransferFunctionType::Gamma;
        if (key & programid(0x1) << PROGRAM_HAS_TRANSLUCENT_CONVERSION) hasTranslucentConversion = true;
    }
}; // struct MTKProgramDescription

/**
 * A program holds a vertex and a fragment shader. It offers several utility
 * methods to query attributes and uniforms.
 */
class MTKProgram : public Program {
public:
    /**
     * M: [ProgramBinaryAtlas] Creates a new program with the specified binary
     */
    MTKProgram(const ProgramDescription& description, void* binary, GLint length, GLenum format);

    /**
     * Creates a new program with the specified vertex and fragment
     * shaders sources.
     */
    MTKProgram(const ProgramDescription& description, const char* vertex, const char* fragment);
    //virtual ~Program();
}; // class MTKProgram

}; // namespace uirenderer
}; // namespace android

#endif // _MTK_PROGRAM_H
