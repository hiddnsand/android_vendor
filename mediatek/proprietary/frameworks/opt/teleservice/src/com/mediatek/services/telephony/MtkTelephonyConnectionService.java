/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.services.telephony;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telecom.Conference;
import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import android.telecom.DisconnectCause;
import android.telecom.ParcelableConnection;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.VideoProfile;
import android.telecom.Logging.Session;
import android.telephony.CarrierConfigManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.android.internal.os.SomeArgs;
import com.android.internal.telecom.IVideoProvider;
import com.android.internal.telephony.Call;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.GsmCdmaPhone;
import com.android.internal.telephony.IccCard;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.SubscriptionController;
import com.android.internal.telephony.TelephonyDevController;
import com.android.phone.MMIDialogActivity;
import com.android.phone.PhoneUtils;
import com.android.phone.R;
import com.android.services.telephony.DisconnectCauseUtil;
import com.android.services.telephony.EmergencyCallHelper;
import com.android.services.telephony.EmergencyCallStateListener;
import com.android.services.telephony.EmergencyTonePlayer;
import com.android.services.telephony.Log;
import com.android.services.telephony.TelephonyConnection;
import com.android.services.telephony.TelephonyConnectionService;
import com.android.services.telephony.TelecomAccountRegistry;
import com.android.services.telephony.TelephonyConferenceController;

import com.mediatek.internal.telecom.IMtkConnectionService;
import com.mediatek.internal.telecom.IMtkConnectionServiceAdapter;
import com.mediatek.internal.telephony.MtkHardwareConfig;
import com.mediatek.internal.telephony.RadioManager;

import com.mediatek.services.telephony.MtkTelephonyConnectionServiceUtil;

import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.util.ArrayList;
import java.util.List;

import mediatek.telecom.MtkConnection;
import mediatek.telecom.MtkTelecomManager;

public class MtkTelephonyConnectionService extends TelephonyConnectionService {

    private static final String TAG = "TeleConnService";

    private final MtkCdmaConferenceController mMtkCdmaConferenceController =
            new MtkCdmaConferenceController(this);
    private MtkConnectionServiceBinder mMtkBinder = null;

    /// M: CC: ECC retry @{
    private MtkSwitchPhoneHelper mSwitchPhoneHelper;
    /// @}

    /// M: CC: Vzw/CTVolte ECC @{
    TelephonyDevController mTelDevController = TelephonyDevController.getInstance();
    private boolean hasC2kOverImsModem() {
        if (mTelDevController != null &&
                mTelDevController.getModem(0) != null &&
                ((MtkHardwareConfig) mTelDevController.getModem(0)).hasC2kOverImsModem() == true) {
                    return true;
        }
        return false;
    }

    /// @}

    @Override
    protected IBinder getConnectionServiceBinder() {
        if (mMtkBinder == null) {
            Log.d(this, "init MtkConnectionServiceBinder");
            mMtkBinder = new MtkConnectionServiceBinder();

            mTelephonyConferenceController = new MtkTelephonyConferenceController(
                    mTelephonyConnectionServiceProxy);
        }
        return (IBinder) mMtkBinder;
    }

    private static final int MTK_MSG_BASE = 1000;

    /// M: CC: Interface for ECT
    private static final int MSG_ECT = MTK_MSG_BASE + 0;
    /// M: CC: HangupAll for FTA 31.4.4.2
    private static final int MSG_HANGUP_ALL = MTK_MSG_BASE + 1;

    /// M: CC: For DSDS/DSDA Two-action operation.
    private static final int MSG_HANDLE_ORDERED_USER_OPERATION = MTK_MSG_BASE + 4;

    /**
     * Gets the name of current connectionService.
     * Ex: "TelephonyConnectionService" -> return "Telephony"
     *
     * @return name of this connectionService.
     * @hide
     */
    private String getConnectionServiceName() {
        return "MtkTelephony";
    }

    @Override
    public void onCreate() {
        super.onCreate();
        /// M: CC: Use MtkTelephonyConnectionServiceUtil
        MtkTelephonyConnectionServiceUtil.getInstance().setService(this);
    }

    /// M: CC: Use MtkTelephonyConnectionServiceUtil @{
    @Override
    public void onDestroy() {
        /// M: CC: to check whether the device has on-going ECC
        RadioManager.getInstance().forceSetEccState(false);
        MtkTelephonyConnectionServiceUtil.getInstance().unsetService();
        /// M: CC: Destroy CDMA conference controller.
        mMtkCdmaConferenceController.onDestroy();
        /// M: CC: ECC retry @{
        if (mSwitchPhoneHelper != null) {
            mSwitchPhoneHelper.onDestroy();
        }
        /// @}
        super.onDestroy();
    }
    /// @}

    private final Handler mMtkHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            /// M: CC: HangupAll for FTA 31.4.4.2 @{
            case MSG_HANGUP_ALL:
                hangupAll((String) msg.obj);
                break;
            /// @}

            /// M: CC: For DSDS/DSDA Two-action operation @{
            case MSG_HANDLE_ORDERED_USER_OPERATION: {
                SomeArgs args = (SomeArgs) msg.obj;
                try {
                    String callId = (String) args.arg1;
                    String currentOperation = (String) args.arg2;
                    String pendingOperation = (String) args.arg3;
                    if (MtkConnection.OPERATION_DISCONNECT_CALL.equals(currentOperation)) {
                        disconnect(callId, pendingOperation);
                    }
                } finally {
                    args.recycle();
                }
                break;
            }
                /// @}
            /// M: CC: Interface for ECT @{
            case MSG_ECT:
                explicitCallTransfer((String) msg.obj);
                break;
            /// @}
            default:
                Log.d(this, "mMtkHandler default return (msg.what=%d)", msg.what);
                break;
            }
        }
    };

    private class MtkConnectionServiceBinder extends IMtkConnectionService.Stub {

        public void addMtkConnectionServiceAdapter(IMtkConnectionServiceAdapter adapter,
                Session.Info sessionInfo) {
            Log.d(this, "MtkConnectionServiceBinder add IMtkConnectionServiceAdapter");
            try {
                adapter.setIConnectionServiceBinder(mBinder);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return;
        }

        /// M: CC: HangupAll for FTA 31.4.4.2 @{
        @Override
        public void hangupAll(String callId) {
            mMtkHandler.obtainMessage(MSG_HANGUP_ALL, callId).sendToTarget();
        }
        /// @}

        /// M: CC: For MSMS/MSMA ordered user operations.
        @Override
        public void handleOrderedOperation(
                String callId, String currentOperation, String pendingOperation) {
            //mHandler.obtainMessage(MSG_DISCONNECT, callId).sendToTarget();
            SomeArgs args = SomeArgs.obtain();
            args.arg1 = callId;
            args.arg2 = currentOperation;
            args.arg3 = pendingOperation;
            mMtkHandler.obtainMessage(MSG_HANDLE_ORDERED_USER_OPERATION, args).sendToTarget();
        }
        /// @}

        /// M: CC: Interface for ECT @{
        @Override
        public void explicitCallTransfer(String callId) {
            mMtkHandler.obtainMessage(MSG_ECT, callId).sendToTarget();
        }
        /// @}

        /// M: For VoLTE, 1 key conference @{
        @Override
        public void inviteConferenceParticipants(String conferenceCallId, List<String> numbers) {
            // ToDo, migration
        }

        @Override
        public void createConference(
            final PhoneAccountHandle connectionManagerPhoneAccount,
            final String conferenceCallId,
            final ConnectionRequest request,
            final List<String> numbers,
            boolean isIncoming,
            Session.Info sessionInfo) {
            // ToDo, migration
        }
        /// @}
    };

    @Override
    protected Connection getTelephonyConnection(final ConnectionRequest request,
            final String number, boolean isEmergencyNumber, final Uri handle, Phone phone) {

        if (phone == null) {
            final Context context = getApplicationContext();
            if (context.getResources().getBoolean(R.bool.config_checkSimStateBeforeOutgoingCall)) {
                // Check SIM card state before the outgoing call.
                // Start the SIM unlock activity if PIN_REQUIRED.
                final Phone defaultPhone = mPhoneFactoryProxy.getDefaultPhone();
                final IccCard icc = defaultPhone.getIccCard();
                IccCardConstants.State simState = IccCardConstants.State.UNKNOWN;
                if (icc != null) {
                    simState = icc.getState();
                }
                if (simState == IccCardConstants.State.PIN_REQUIRED) {
                    final String simUnlockUiPackage = context.getResources().getString(
                            R.string.config_simUnlockUiPackage);
                    final String simUnlockUiClass = context.getResources().getString(
                            R.string.config_simUnlockUiClass);
                    if (simUnlockUiPackage != null && simUnlockUiClass != null) {
                        Intent simUnlockIntent = new Intent().setComponent(new ComponentName(
                                simUnlockUiPackage, simUnlockUiClass));
                        simUnlockIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        try {
                            context.startActivity(simUnlockIntent);
                        } catch (ActivityNotFoundException exception) {
                            Log.e(this, exception, "Unable to find SIM unlock UI activity.");
                        }
                    }
                    return Connection.createFailedConnection(
                            DisconnectCauseUtil.toTelecomDisconnectCause(
                                    android.telephony.DisconnectCause.OUT_OF_SERVICE,
                                    "SIM_STATE_PIN_REQUIRED"));
                }
            }

            Log.d(this, "onCreateOutgoingConnection, phone is null");
            /// M: CC: Error message due to CellConnMgr checking @{
            log("onCreateOutgoingConnection, use default phone for cellConnMgr");
            if (MtkTelephonyConnectionServiceUtil.getInstance().
                    cellConnMgrShowAlerting(PhoneFactory.getDefaultPhone().getSubId())) {
                log("onCreateOutgoingConnection, cellConnMgrShowAlerting() check fail");
                return Connection.createFailedConnection(
                        DisconnectCauseUtil.toTelecomDisconnectCause(
                                mediatek.telephony.MtkDisconnectCause.OUTGOING_CANCELED_BY_SERVICE,
                                "cellConnMgrShowAlerting() check fail"));
            }
            /// @}
            return Connection.createFailedConnection(
                    DisconnectCauseUtil.toTelecomDisconnectCause(
                            android.telephony.DisconnectCause.OUT_OF_SERVICE, "Phone is null"));
        }

        /// M: CC: Timing issue, radio maybe on even airplane mode on @{
        boolean isAirplaneModeOn = false;
        if (Settings.Global.getInt(phone.getContext().getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) > 0) {
            isAirplaneModeOn = true;
        }
        /// @}

        /// M: CC: TDD data only @{
        if (!isAirplaneModeOn
                && MtkTelephonyConnectionServiceUtil.getInstance().isDataOnlyMode(phone)) {
            /// M: CC: ECC Retry @{
            // Assume only one ECC exists. Don't trigger retry
            // since Modem fails to power on should be a bug
            if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                Log.i(this, "4G data only, ECC retry: clear ECC param");
                MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
            }
            /// @}

            Log.d(this, "getTelephonyConnection, phoneId=" + phone.getPhoneId()
                    + " is in TDD data only mode.");
            return Connection.createFailedConnection(
                    DisconnectCauseUtil.toTelecomDisconnectCause(
                    android.telephony.DisconnectCause.OUTGOING_CANCELED, null));
        }
        /// @}

        // Check both voice & data RAT to enable normal CS call,
        // when voice RAT is OOS but Data RAT is present.
        int state = phone.getServiceState().getState();
        if (state == ServiceState.STATE_OUT_OF_SERVICE) {
            int dataNetType = phone.getServiceState().getDataNetworkType();
            if (dataNetType == TelephonyManager.NETWORK_TYPE_LTE ||
                    dataNetType == TelephonyManager.NETWORK_TYPE_LTE_CA) {
                state = phone.getServiceState().getDataRegState();
            }
        }

        // If we're dialing a non-emergency number and the phone is in ECM mode, reject the call if
        // carrier configuration specifies that we cannot make non-emergency calls in ECM mode.
        if (!isEmergencyNumber && phone.isInEcm()) {
            boolean allowNonEmergencyCalls = true;
            CarrierConfigManager cfgManager = (CarrierConfigManager)
                    phone.getContext().getSystemService(Context.CARRIER_CONFIG_SERVICE);
            if (cfgManager != null) {
                allowNonEmergencyCalls = cfgManager.getConfigForSubId(phone.getSubId())
                        .getBoolean(CarrierConfigManager.KEY_ALLOW_NON_EMERGENCY_CALLS_IN_ECM_BOOL);
            }

            if (!allowNonEmergencyCalls) {
                return Connection.createFailedConnection(
                        DisconnectCauseUtil.toTelecomDisconnectCause(
                                android.telephony.DisconnectCause.CDMA_NOT_EMERGENCY,
                                "Cannot make non-emergency call in ECM mode."
                        ));
            }
        }

        if (!isEmergencyNumber) {

            /// M: CC: Error message due to CellConnMgr checking @{
            if (MtkTelephonyConnectionServiceUtil.getInstance().
                    cellConnMgrShowAlerting(phone.getSubId())) {
                log("onCreateOutgoingConnection, cellConnMgrShowAlerting() check fail");
                return Connection.createFailedConnection(
                        DisconnectCauseUtil.toTelecomDisconnectCause(
                                mediatek.telephony.MtkDisconnectCause.OUTGOING_CANCELED_BY_SERVICE,
                                "cellConnMgrShowAlerting() check fail"));
            }
            /// @}

            switch (state) {
                case ServiceState.STATE_IN_SERVICE:
                case ServiceState.STATE_EMERGENCY_ONLY:
                    break;
                case ServiceState.STATE_OUT_OF_SERVICE:
                    if (phone.isUtEnabled() && number.endsWith("#")) {
                        Log.d(this, "onCreateOutgoingConnection dial for UT");
                        break;
                    } else {
                        /// M: CC: FTA requires call should be dialed out even out of service @{
                        if (SystemProperties.getInt("gsm.gcf.testmode", 0) == 2) {
                            break;
                        }
                        /// @}
                        return Connection.createFailedConnection(
                                DisconnectCauseUtil.toTelecomDisconnectCause(
                                        android.telephony.DisconnectCause.OUT_OF_SERVICE,
                                        "ServiceState.STATE_OUT_OF_SERVICE"));
                    }
                case ServiceState.STATE_POWER_OFF:
                    return Connection.createFailedConnection(
                            DisconnectCauseUtil.toTelecomDisconnectCause(
                                    android.telephony.DisconnectCause.POWER_OFF,
                                    "ServiceState.STATE_POWER_OFF"));
                default:
                    Log.d(this, "onCreateOutgoingConnection, unknown service state: %d", state);
                    return Connection.createFailedConnection(
                            DisconnectCauseUtil.toTelecomDisconnectCause(
                                    android.telephony.DisconnectCause.OUTGOING_FAILURE,
                                    "Unknown service state " + state));
            }

            /// M: CC: TelephonyConnectionService canDial check @{
            if (!canDial(request.getAccountHandle(), number)) {
                log("onCreateOutgoingConnection, canDial() check fail");
                return Connection.createFailedConnection(
                        DisconnectCauseUtil.toTelecomDisconnectCause(
                                android.telephony.DisconnectCause.OUTGOING_FAILURE,
                                "canDial() check fail"));
            }
            /// @}
        }

        final Context context = getApplicationContext();
        if (VideoProfile.isVideo(request.getVideoState()) && isTtyModeEnabled(context) &&
                !isEmergencyNumber) {
            return Connection.createFailedConnection(DisconnectCauseUtil.toTelecomDisconnectCause(
                    android.telephony.DisconnectCause.VIDEO_CALL_NOT_ALLOWED_WHILE_TTY_ENABLED));
        }

        // Check for additional limits on CDMA phones.
        final Connection failedConnection = checkAdditionalOutgoingCallLimits(phone);
        if (failedConnection != null) {
            return failedConnection;
        }

        // Check roaming status to see if we should block custom call forwarding codes
        if (blockCallForwardingNumberWhileRoaming(phone, number)) {
            return Connection.createFailedConnection(
                    DisconnectCauseUtil.toTelecomDisconnectCause(
                            android.telephony.DisconnectCause.DIALED_CALL_FORWARDING_WHILE_ROAMING,
                            "Call forwarding while roaming"));
        }


        final TelephonyConnection connection =
                createConnectionFor(phone, null, true /* isOutgoing */, request.getAccountHandle(),
                        request.getTelecomCallId(), request.getAddress(), request.getVideoState());
        if (connection == null) {
            /// M: CC: ECC retry @{
            // Not trigger retry since connection is null should be a bug
            // Assume only one ECC exists
            if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                Log.i(this, "Fail to create connection, ECC retry: clear ECC param");
                MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
            }
            /// @}
            return Connection.createFailedConnection(
                    DisconnectCauseUtil.toTelecomDisconnectCause(
                            android.telephony.DisconnectCause.OUTGOING_FAILURE,
                            "Invalid phone type"));
        }

        /// M: CC: ECC Retry @{
        if (connection instanceof MtkTelephonyConnection) {
            ((MtkTelephonyConnection) connection).setEmergencyCall(isEmergencyNumber);
        }
        /// @}

        /// M: CC: Set PhoneAccountHandle for ECC @{
        //[ALPS01794357]
        if (isEmergencyNumber) {
            final PhoneAccountHandle phoneAccountHandle;
            /// M: CC: Get iccid from system property @{
            // when IccRecords is null, (updated as RILD is reinitialized).
            // [ALPS02312211] [ALPS02325107]
            String phoneIccId = phone.getFullIccSerialNumber();
            int slotId = SubscriptionController.getInstance().getSlotIndex(phone.getSubId());
            if (slotId != SubscriptionManager.INVALID_SIM_SLOT_INDEX) {
                phoneIccId = !TextUtils.isEmpty(phoneIccId) ? phoneIccId
                        : TelephonyManager.getDefault().getSimSerialNumber(phone.getSubId());
            }
            /// @}
            if (TextUtils.isEmpty(phoneIccId)) {
                // If No SIM is inserted, the corresponding IccId will be null,
                // take phoneId as PhoneAccountHandle::mId which is IccId originally
                phoneAccountHandle = PhoneUtils.makePstnPhoneAccountHandle(
                        Integer.toString(phone.getPhoneId()));
            } else {
                phoneAccountHandle = PhoneUtils.makePstnPhoneAccountHandle(phoneIccId);
            }
            log("ECC PhoneAccountHandle mId: " + phoneAccountHandle.getId() +
                    ", iccId: " + phoneIccId);
            ((MtkTelephonyConnection)connection).setAccountHandle(phoneAccountHandle);
        }
        /// @}

        connection.setAddress(handle, PhoneConstants.PRESENTATION_ALLOWED);
        connection.setInitializing();
        connection.setVideoState(request.getVideoState());

        return connection;
    }

    @Override
    public Connection onCreateOutgoingConnection(
            PhoneAccountHandle connectionManagerPhoneAccount,
            final ConnectionRequest request) {
        Log.i(this, "onCreateOutgoingConnection, request: " + request);

        Uri handle = request.getAddress();
        if (handle == null) {
            Log.d(this, "onCreateOutgoingConnection, handle is null");
            return Connection.createFailedConnection(
                    DisconnectCauseUtil.toTelecomDisconnectCause(
                            android.telephony.DisconnectCause.NO_PHONE_NUMBER_SUPPLIED,
                            "No phone number supplied"));
        }

        /// M: CC: ECC retry @{
        if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
            int phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
            try {
                phoneId = Integer.parseInt(request.getAccountHandle().getId());
            } catch (NumberFormatException e) {
                phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
            } finally {
                if (PhoneFactory.getPhone(phoneId) == null) {
                   Log.i(this, "ECC retry: clear ECC param due to unmatched phoneId");
                   MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
                   /// M: CC: to check whether the device has on-going ECC
                   RadioManager.getInstance().forceSetEccState(false);
                   Log.i(this, "onCreateOutgoingConnection, phone is null");
                   return Connection.createFailedConnection(
                            DisconnectCauseUtil.toTelecomDisconnectCause(
                                    android.telephony.DisconnectCause.OUT_OF_SERVICE,
                                    "Phone is null"));
                }
            }
        }
        /// @}

        String scheme = handle.getScheme();
        String number;
        if (PhoneAccount.SCHEME_VOICEMAIL.equals(scheme)) {
            // TODO: We don't check for SecurityException here (requires
            // CALL_PRIVILEGED permission).
            final Phone phone = getPhoneForAccount(request.getAccountHandle(), false);
            if (phone == null) {
                Log.d(this, "onCreateOutgoingConnection, phone is null");
                return Connection.createFailedConnection(
                        DisconnectCauseUtil.toTelecomDisconnectCause(
                                android.telephony.DisconnectCause.OUT_OF_SERVICE,
                                "Phone is null"));
            }
            number = phone.getVoiceMailNumber();
            if (TextUtils.isEmpty(number)) {
                Log.d(this, "onCreateOutgoingConnection, no voicemail number set.");
                return Connection.createFailedConnection(
                        DisconnectCauseUtil.toTelecomDisconnectCause(
                                android.telephony.DisconnectCause.VOICEMAIL_NUMBER_MISSING,
                                "Voicemail scheme provided but no voicemail number set."));
            }

            // Convert voicemail: to tel:
            handle = Uri.fromParts(PhoneAccount.SCHEME_TEL, number, null);
        } else {
            if (!PhoneAccount.SCHEME_TEL.equals(scheme)) {
                Log.d(this, "onCreateOutgoingConnection, Handle %s is not type tel", scheme);
                return Connection.createFailedConnection(
                        DisconnectCauseUtil.toTelecomDisconnectCause(
                                android.telephony.DisconnectCause.INVALID_NUMBER,
                                "Handle scheme is not type tel"));
            }

            number = handle.getSchemeSpecificPart();
            if (TextUtils.isEmpty(number)) {
                Log.d(this, "onCreateOutgoingConnection, unable to parse number");
                return Connection.createFailedConnection(
                        DisconnectCauseUtil.toTelecomDisconnectCause(
                                android.telephony.DisconnectCause.INVALID_NUMBER,
                                "Unable to parse number"));
            }

            /// M: CC: ECC retry @{
            //final Phone phone = getPhoneForAccount(request.getAccountHandle(), false);
            Phone phone = null;
            if (!MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                phone = getPhoneForAccount(request.getAccountHandle(), false);
            }
            /// @}

            if (phone != null && CDMA_ACTIVATION_CODE_REGEX_PATTERN.matcher(number).matches()) {
                // Obtain the configuration for the outgoing phone's SIM. If the outgoing number
                // matches the *228 regex pattern, fail the call. This number is used for OTASP, and
                // when dialed could lock LTE SIMs to 3G if not prohibited..
                boolean disableActivation = false;
                CarrierConfigManager cfgManager = (CarrierConfigManager)
                        phone.getContext().getSystemService(Context.CARRIER_CONFIG_SERVICE);
                if (cfgManager != null) {
                    disableActivation = cfgManager.getConfigForSubId(phone.getSubId())
                            .getBoolean(CarrierConfigManager.KEY_DISABLE_CDMA_ACTIVATION_CODE_BOOL);
                }

                if (disableActivation) {
                    return Connection.createFailedConnection(
                            DisconnectCauseUtil.toTelecomDisconnectCause(
                                    android.telephony.DisconnectCause
                                            .CDMA_ALREADY_ACTIVATED,
                                    "Tried to dial *228"));
                }
            }
        }

        // Convert into emergency number if necessary
        // This is required in some regions (e.g. Taiwan).
        if (!PhoneNumberUtils.isLocalEmergencyNumber(this, number)) {
            final Phone phone = getPhoneForAccount(request.getAccountHandle(), false);
            // We only do the conversion if the phone is not in service. The un-converted
            // emergency numbers will go to the correct destination when the phone is in-service,
            // so they will only need the special emergency call setup when the phone is out of
            // service.
            if (phone == null || phone.getServiceState().getState()
                    != ServiceState.STATE_IN_SERVICE) {
                String convertedNumber = PhoneNumberUtils.convertToEmergencyNumber(this, number);
                if (!TextUtils.equals(convertedNumber, number)) {
                    Log.i(this, "onCreateOutgoingConnection, converted to emergency number");
                    number = convertedNumber;
                    handle = Uri.fromParts(PhoneAccount.SCHEME_TEL, number, null);
                }
            }
        }
        final String numberToDial = number;

        final boolean isEmergencyNumber =
                PhoneNumberUtils.isLocalEmergencyNumber(this, numberToDial);

        /// M: CC: ECC retry @{
        if (!isEmergencyNumber && MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
            Log.i(this, "ECC retry: clear ECC param due to SIM state/phone type change, not ECC");
            MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
            /// M: CC: to check whether the device has on-going ECC
            RadioManager.getInstance().forceSetEccState(false);
            Log.i(this, "onCreateOutgoingConnection, phone is null");
            return Connection.createFailedConnection(
                    DisconnectCauseUtil.toTelecomDisconnectCause(
                            android.telephony.DisconnectCause.OUT_OF_SERVICE,
                            "Phone is null"));
        }
        /// @}

        //if (isEmergencyNumber && !isRadioOn()) {
        if (isEmergencyNumber) {
            /// M: CC: to check whether the device has on-going ECC
            RadioManager.getInstance().forceSetEccState(true);
            final Uri emergencyHandle = handle;
            // By default, Connection based on the default Phone, since we need to return to Telecom
            // now.
            final int defaultPhoneType = mPhoneFactoryProxy.getDefaultPhone().getPhoneType();
            final Connection emergencyConnection = getTelephonyConnection(request, numberToDial,
                    isEmergencyNumber, emergencyHandle, mPhoneFactoryProxy.getDefaultPhone());

            /// M: CC: Return the failed connection directly @{
            if (!(emergencyConnection instanceof TelephonyConnection)) {
                Log.i(this, "onCreateOutgoingConnection, create emergency connection failed");
                return emergencyConnection;
            }
            /// @}

            /// M: CC: Vzw/CTVolte ECC
            MtkTelephonyConnectionServiceUtil.getInstance().setEmergencyNumber(numberToDial);

            if (hasC2kOverImsModem() || MtkTelephonyManagerEx.getDefault().useVzwLogic()) {
                mSwitchPhoneHelper = null;
            } else if (mSwitchPhoneHelper == null) {
                mSwitchPhoneHelper = new MtkSwitchPhoneHelper(this, number);
            }

            if (mSwitchPhoneHelper != null && mSwitchPhoneHelper.needToPrepareForDial()) {
                mSwitchPhoneHelper.prepareForDial(
                        new MtkSwitchPhoneHelper.Callback() {
                            @Override
                            public void onComplete(boolean success) {
                                if (emergencyConnection.getState()
                                        == Connection.STATE_DISCONNECTED) {
                                    Log.i(this, "prepareForDial, connection disconnect");
                                    /// M: CC: to check whether the device has on-going ECC
                                    RadioManager.getInstance().forceSetEccState(false);
                                    return;
                                } else if (success) {
                                    Log.i(this, "startTurnOnRadio");
                                    startTurnOnRadio(emergencyConnection, request,
                                            emergencyHandle, numberToDial);
                                } else {
                                    /// M: CC: ECC retry @{
                                    // Assume only one ECC exists. Don't trigger retry
                                    // since MD fails to power on should be a bug.
                                    if (MtkTelephonyConnectionServiceUtil.getInstance()
                                            .isEccRetryOn()) {
                                        Log.i(this, "ECC retry: clear ECC param");
                                        MtkTelephonyConnectionServiceUtil.getInstance()
                                                .clearEccRetryParams();
                                    }
                                    /// @}
                                    Log.i(this, "prepareForDial, failed to turn on radio");
                                    emergencyConnection.setDisconnected(
                                            DisconnectCauseUtil.toTelecomDisconnectCause(
                                            android.telephony.DisconnectCause.POWER_OFF,
                                            "Failed to turn on radio."));
                                    /// M: CC: to check whether the device has on-going ECC
                                    RadioManager.getInstance().forceSetEccState(false);
                                    emergencyConnection.destroy();
                                }
                            }
                        });
                // Return the still unconnected GsmConnection and wait for the Radios to boot before
                // connecting it to the underlying Phone.
                return emergencyConnection;
            }

            /// M: ECC special handle, select phone by ECC rule @{
            final Phone defaultPhone = getPhoneForAccount(request.getAccountHandle(),
                    isEmergencyNumber);
            final Phone phone = MtkTelephonyConnectionServiceUtil.getInstance()
                    .selectPhoneBySpecialEccRule(request.getAccountHandle(),
                    numberToDial, defaultPhone);
            /// @}

            // Radio maybe on even airplane mode on
            boolean isAirplaneModeOn = false;
            if (Settings.Global.getInt(this.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) > 0) {
                isAirplaneModeOn = true;
            }

            /**
             * Use EmergencyCallHelper when phone is in any case below:
             * 1. in airplane mode.
             * 2. phone is radio off.
             * 3. phone is not in service and the emergency only is false.
             */
            if (isAirplaneModeOn || !phone.isRadioOn()
                    || (phone.getServiceState().getState() != ServiceState.STATE_IN_SERVICE
                            && !phone.getServiceState().isEmergencyOnly())) {
                if (mEmergencyCallHelper == null) {
                    mEmergencyCallHelper = new MtkEmergencyCallHelper(this);
                }
                mEmergencyCallHelper.enableEmergencyCalling(new EmergencyCallStateListener.Callback() {
                    @Override
                    public void onComplete(EmergencyCallStateListener listener, boolean isRadioReady) {
                        // Make sure the Call has not already been canceled by the user.
                        if (emergencyConnection.getState() == Connection.STATE_DISCONNECTED) {
                            Log.i(this, "Emergency call disconnected before the outgoing call was " +
                                    "placed. Skipping emergency call placement.");
                            /// M: CC: to check whether the device has on-going ECC
                            RadioManager.getInstance().forceSetEccState(false);
                            return;
                        }
                        if (isRadioReady) {
                            // Get the right phone object since the radio has been turned on
                            // successfully.
                            //final Phone phone = getPhoneForAccount(request.getAccountHandle(),
                            //        isEmergencyNumber);
                            Phone newDefaultPhone = getPhoneForAccount(request.getAccountHandle(),
                                    isEmergencyNumber);
                            /// @}

                            /// M: CC: Vzw/CTVolte ECC @{
                            Phone newPhone = MtkTelephonyConnectionServiceUtil.getInstance()
                                    .selectPhoneBySpecialEccRule(request.getAccountHandle(),
                                    numberToDial, newDefaultPhone);
                            Log.i(this, "Select phone after turning off airplane mode again"
                                    + ", orig phone=" + phone + " , new phone=" + newPhone);
                            /// @}

                            /// M: CC: ECC retry @{
                            if (!MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                                Log.i(this, "ECC retry: set param with Intial ECC.");
                                MtkTelephonyConnectionServiceUtil.getInstance().setEccRetryParams(
                                        request, newPhone.getPhoneId());
                            }
                            /// @}

                            /// M: CC: TDD data only @{
                            if (MtkTelephonyConnectionServiceUtil.getInstance().
                                    isDataOnlyMode(newPhone)) {
                                Log.i(this, "enableEmergencyCalling, phoneId=" + newPhone.getPhoneId()
                                        + " is in TDD data only mode.");
                                /// M: CC: ECC retry @{
                                // Assume only one ECC exists
                                if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                                    Log.i(this, "ECC retry: clear ECC param");
                                    MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
                                }
                                /// @}
                                emergencyConnection.setDisconnected(
                                        DisconnectCauseUtil.toTelecomDisconnectCause(
                                        android.telephony.DisconnectCause.OUTGOING_CANCELED, null));
                                emergencyConnection.destroy();
                                /// M: CC: to check whether the device has on-going ECC
                                RadioManager.getInstance().forceSetEccState(false);
                                return;
                            }
                            /// @}

                            /// M: CC: Vzw/CTVolte ECC @{
                            if (TelephonyManager.getDefault().getPhoneCount() > 1) {
                                MtkTelephonyConnectionServiceUtil.getInstance()
                                        .enterEmergencyMode(newPhone, 1/*airplane*/);
                            }
                            /// @}

                            // If the PhoneType of the Phone being used is different than the Default
                            // Phone, then we need create a new Connection using that PhoneType and
                            // replace it in Telecom.
                            if (newPhone.getPhoneType() != defaultPhoneType) {
                                Connection repConnection = getTelephonyConnection(request, numberToDial,
                                        isEmergencyNumber, emergencyHandle, newPhone);
                                /// Modify the follow to handle the no sound issue. @{
                                // 1. Add the new connection into Telecom;
                                // 2. Disconnect the old connection;
                                // 3. Place the new connection.
                                if (repConnection instanceof TelephonyConnection) {
                                    // Notify Telecom of the new Connection type.
                                    // TODO: Switch out the underlying connection instead of
                                    // creating a new one and causing UI Jank.
                                    addExistingConnection(PhoneUtils.makePstnPhoneAccountHandle(newPhone),
                                            repConnection);
                                    // Reset the emergency call flag for destroying old connection.
                                    resetTreatAsEmergencyCall(emergencyConnection);
                                    emergencyConnection.setDisconnected(
                                            DisconnectCauseUtil.toTelecomDisconnectCause(
                                                    android.telephony.DisconnectCause.OUTGOING_CANCELED,
                                                    "Reconnecting outgoing Emergency Call."));
                                } else {
                                    /// M: CC: ECC retry @{
                                    // Assume only one ECC exists
                                    if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                                        Log.i(this, "ECC retry: clear ECC param");
                                        MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
                                    }
                                    /// @}
                                    emergencyConnection.setDisconnected(repConnection.getDisconnectCause());
                                    /// M: CC: to check whether the device has on-going ECC
                                    RadioManager.getInstance().forceSetEccState(false);
                                }
                                emergencyConnection.destroy();
                                /// @}

                                // If there was a failure, the resulting connection will not be a
                                // TelephonyConnection, so don't place the call, just return!
                                if (repConnection instanceof TelephonyConnection) {
                                    placeOutgoingConnection((TelephonyConnection) repConnection,
                                            newPhone, request);
                                }
                            } else {
                                placeOutgoingConnection((TelephonyConnection) emergencyConnection,
                                        newPhone, request);
                            }
                        } else {
                            /// M: CC: ECC retry @{
                            // Assume only one ECC exists. Don't trigger retry
                            // since Modem fails to power on should be a bug
                            if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                                Log.i(this, "ECC retry: clear ECC param");
                                MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
                            }
                            /// @}

                            Log.w(this, "onCreateOutgoingConnection, failed to turn on radio");
                            emergencyConnection.setDisconnected(
                                    DisconnectCauseUtil.toTelecomDisconnectCause(
                                            android.telephony.DisconnectCause.POWER_OFF,
                                            "Failed to turn on radio."));
                            /// M: CC: to check whether the device has on-going ECC
                            RadioManager.getInstance().forceSetEccState(false);
                            emergencyConnection.destroy();
                        }
                    }
                });
            } else {
                /// M: CC: Vzw/CTVolte ECC @{
                MtkTelephonyConnectionServiceUtil.getInstance()
                        .enterEmergencyMode(phone, 0/*airplane*/);
                /// @}

                /// M: CC: ECC retry @{
                if (!MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                    Log.i(this, "ECC retry: set param with Intial ECC.");
                    MtkTelephonyConnectionServiceUtil.getInstance().setEccRetryParams(
                            request,
                            phone.getPhoneId());
                }
                /// @}

                // If the PhoneType of the Phone being used is different than the Default
                // Phone, then we need create a new Connection using that PhoneType and
                // replace it in Telecom.
                if (phone.getPhoneType() != defaultPhoneType) {
                    Connection repConnection = getTelephonyConnection(request, numberToDial,
                            isEmergencyNumber, emergencyHandle, phone);
                    // If there was a failure, the resulting connection will not be a
                    // TelephonyConnection, so don't place the call, just return!
                    if (repConnection instanceof TelephonyConnection) {
                        placeOutgoingConnection((TelephonyConnection) repConnection, phone,
                                request);
                    /// M: CC: ECC retry @{
                    } else if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                        // Assume only one ECC exists
                        Log.i(this, "ECC retry: clear ECC param");
                        MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
                        /// M: CC: to check whether the device has on-going ECC
                        RadioManager.getInstance().forceSetEccState(false);
                    /// @}
                    }

                    /// Reset the emergency call flag for destroying old connection.
                    resetTreatAsEmergencyCall(emergencyConnection);

                    // Notify Telecom of the new Connection type.
                    // TODO: Switch out the underlying connection instead of creating a new
                    // one and causing UI Jank.
                    // addExistingConnection(PhoneUtils.makePstnPhoneAccountHandle(phone),
                    //         repConnection);
                    // Remove the old connection from Telecom after.
                    emergencyConnection.setDisconnected(
                            DisconnectCauseUtil.toTelecomDisconnectCause(
                            android.telephony.DisconnectCause.OUTGOING_CANCELED,
                            "Reconnecting outgoing Emergency Call."));
                    emergencyConnection.destroy();
                    // Return the new connection to Telecom directly.
                    return repConnection;
                } else {
                    placeOutgoingConnection((TelephonyConnection) emergencyConnection,
                            phone, request);
                }
            }
            // Return the still unconnected GsmConnection and wait for the Radios to boot before
            // connecting it to the underlying Phone.
            return emergencyConnection;
        /// @}
        } else {
            if (!canAddCall() && !isEmergencyNumber) {
                Log.d(this, "onCreateOutgoingConnection, cannot add call .");
                return Connection.createFailedConnection(
                        new DisconnectCause(DisconnectCause.ERROR,
                                getApplicationContext().getText(
                                        R.string.incall_error_cannot_add_call),
                                getApplicationContext().getText(
                                        R.string.incall_error_cannot_add_call),
                                "Add call restricted due to ongoing video call"));
            }

            // Get the right phone object from the account data passed in.
            final Phone phone = getPhoneForAccount(request.getAccountHandle(), isEmergencyNumber);
            Connection resultConnection = getTelephonyConnection(request, numberToDial,
                    isEmergencyNumber, handle, phone);
            // If there was a failure, the resulting connection will not be a TelephonyConnection,
            // so don't place the call!
            if(resultConnection instanceof TelephonyConnection) {
                placeOutgoingConnection((TelephonyConnection) resultConnection, phone, request);
            }
            return resultConnection;
        }
    }

    protected void createConnection(
            final PhoneAccountHandle callManagerAccount,
            final String callId,
            final ConnectionRequest request,
            boolean isIncoming,
            boolean isUnknown) {
        Log.d(this, "createConnection, callManagerAccount: %s, callId: %s, request: %s, " +
                        "isIncoming: %b, isUnknown: %b", callManagerAccount, callId, request,
                isIncoming,
                isUnknown);

        Connection connection = isUnknown ? onCreateUnknownConnection(callManagerAccount, request)
                : isIncoming ? onCreateIncomingConnection(callManagerAccount, request)
                : onCreateOutgoingConnection(callManagerAccount, request);
        Log.d(this, "createConnection, connection: %s", connection);
        if (connection == null) {
            connection = Connection.createFailedConnection(
                    new DisconnectCause(DisconnectCause.ERROR));
        }

        connection.setTelecomCallId(callId);
        if (connection.getState() != Connection.STATE_DISCONNECTED) {
            addConnection(callId, connection);
        }

        Uri address = connection.getAddress();
        String number = address == null ? "null" : address.getSchemeSpecificPart();
        Log.v(this, "createConnection, number: %s, state: %s, capabilities: %s, properties: %s",
                Connection.toLogSafePhoneNumber(number),
                Connection.stateToString(connection.getState()),
                Connection.capabilitiesToString(connection.getConnectionCapabilities()),
                Connection.propertiesToString(connection.getConnectionProperties()));

        Log.d(this, "createConnection, calling handleCreateConnectionSuccessful %s", callId);
        /// M: CC: Set PhoneAccountHandle for ECC @{
        //[ALPS01794357]
        PhoneAccountHandle handle = null;
        if (connection instanceof MtkTelephonyConnection) {
            handle = ((MtkTelephonyConnection)connection).getAccountHandle();
        }
        if (handle == null) {
            handle = request.getAccountHandle();
        } else {
            Log.d(this, "createConnection, set back phone account:%s", handle);
        }
        //// @}
        mAdapter.handleCreateConnectionComplete(
                callId,
                request,
                new ParcelableConnection(
                        handle,  /* M: CC: Set PhoneAccountHandle for ECC [ALPS01794357] */
                        connection.getState(),
                        connection.getConnectionCapabilities(),
                        connection.getConnectionProperties(),
                        connection.getSupportedAudioRoutes(),
                        connection.getAddress(),
                        connection.getAddressPresentation(),
                        connection.getCallerDisplayName(),
                        connection.getCallerDisplayNamePresentation(),
                        connection.getVideoProvider() == null ?
                                null : connection.getVideoProvider().getInterface(),
                        connection.getVideoState(),
                        connection.isRingbackRequested(),
                        connection.getAudioModeIsVoip(),
                        connection.getConnectTimeMillis(),
                        connection.getConnectElapsedTimeMillis(),
                        connection.getStatusHints(),
                        connection.getDisconnectCause(),
                        createIdList(connection.getConferenceables()),
                        connection.getExtras()));

        if (isIncoming && request.shouldShowIncomingCallUi() &&
                (connection.getConnectionProperties() & Connection.PROPERTY_SELF_MANAGED) ==
                        Connection.PROPERTY_SELF_MANAGED) {
            // Tell ConnectionService to show its incoming call UX.
            connection.onShowIncomingCallUi();
        }
        if (isUnknown) {
            triggerConferenceRecalculate();
        }
        /// M: CC: Proprietary CRSS handling @{
        // [ALPS01956888] For FailureSignalingConnection, CastException JE will happen.
        if (connection.getState() != Connection.STATE_DISCONNECTED) {
            forceSuppMessageUpdate(connection);
        }
        /// @}

    }

    /// M: CC: ECC retry @{
    /**
     * createConnection for ECC Retry
     *
     * @param callId The call Id.
     * @param request The connection request.
     */
    public void createConnectionInternal(
            final String callId,
            final ConnectionRequest request) {
        Log.i(this, "createConnectionInternal, callId=" + callId + ", request=" + request);

        Connection connection = onCreateOutgoingConnection(null, request);
        Log.i(this, "createConnectionInternal, connection=", connection);
        if (connection == null) {
            connection = Connection.createFailedConnection(
                    new DisconnectCause(DisconnectCause.ERROR));
        }

        connection.setTelecomCallId(callId);
        if (connection.getState() != Connection.STATE_DISCONNECTED) {
            addConnection(callId, connection);
        }

        Uri address = connection.getAddress();
        String number = address == null ? "null" : address.getSchemeSpecificPart();
        Log.i(this, "createConnectionInternal"
                + ", number=" + Connection.toLogSafePhoneNumber(number)
                + ", state=" + Connection.stateToString(connection.getState())
                + ", capabilities="
                + Connection.capabilitiesToString(connection.getConnectionCapabilities())
                + ", properties="
                + Connection.propertiesToString(connection.getConnectionProperties()));

        Log.i(this, "createConnectionInternal, calling handleCreateConnectionComplete"
                + " for callId=" + callId);
        /// M: CC: Set PhoneAccountHandle for ECC @{
        //[ALPS01794357]
        PhoneAccountHandle handle = null;
        if (connection instanceof MtkTelephonyConnection) {
            handle = ((MtkTelephonyConnection)connection).getAccountHandle();
        }
        if (handle == null) {
            handle = request.getAccountHandle();
        } else {
            Log.i(this, "createConnectionInternal, set back phone account=" + handle);
        }
        //// @}
        mAdapter.handleCreateConnectionComplete(
                callId,
                request,
                new ParcelableConnection(
                        handle,  /* M: CC: Set PhoneAccountHandle for ECC [ALPS01794357] */
                        connection.getState(),
                        connection.getConnectionCapabilities(),
                        connection.getConnectionProperties(),
                        connection.getSupportedAudioRoutes(),
                        connection.getAddress(),
                        connection.getAddressPresentation(),
                        connection.getCallerDisplayName(),
                        connection.getCallerDisplayNamePresentation(),
                        connection.getVideoProvider() == null ?
                                null : connection.getVideoProvider().getInterface(),
                        connection.getVideoState(),
                        connection.isRingbackRequested(),
                        connection.getAudioModeIsVoip(),
                        connection.getConnectTimeMillis(),
                        connection.getConnectElapsedTimeMillis(),
                        connection.getStatusHints(),
                        connection.getDisconnectCause(),
                        createIdList(connection.getConferenceables()),
                        connection.getExtras()));
    }

    /**
     * Remove Connection without removing callId from Telecom
     *
     * @param connection The connection.
     * @return String The callId mapped to the removed connection.
     * @hide
     */
    protected String removeConnectionInternal(Connection connection) {
        String id = mIdByConnection.get(connection);
        connection.unsetConnectionService(this);
        connection.removeConnectionListener(mConnectionListener);
        mConnectionById.remove(mIdByConnection.get(connection));
        mIdByConnection.remove(connection);
        Log.i(this, "removeConnectionInternal, callId=" + id + ", connection=" + connection);
        return id;
    }
    /// @}

    protected TelephonyConnection createConnectionFor(
            Phone phone,
            com.android.internal.telephony.Connection originalConnection,
            boolean isOutgoing,
            PhoneAccountHandle phoneAccountHandle,
            String telecomCallId,
            Uri address,
            int videoState) {
        TelephonyConnection returnConnection = null;
        int phoneType = phone.getPhoneType();
        boolean allowsMute = allowsMute(phone);
        returnConnection = new MtkGsmCdmaConnection(phoneType, originalConnection, telecomCallId,
                mEmergencyTonePlayer, allowsMute, isOutgoing);

        if (returnConnection != null) {
            // Listen to Telephony specific callbacks from the connection
            returnConnection.addTelephonyConnectionListener(mTelephonyConnectionListener);
            returnConnection.setVideoPauseSupported(
                    TelecomAccountRegistry.getInstance(this).isVideoPauseSupported(
                            phoneAccountHandle));
        }
        return returnConnection;
    }

    @Override
    public void removeConnection(Connection connection) {
        /// M: CC: ECC retry @{
        //super.removeConnection(connection);
        boolean handleEcc = false;
        if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
            if (connection instanceof TelephonyConnection) {
                if (((TelephonyConnection) connection).shouldTreatAsEmergencyCall()) {
                    handleEcc = true;
                }
            }
        }

        if (handleEcc) {
            Log.i(this, "ECC retry: remove connection.");
            MtkTelephonyConnectionServiceUtil.getInstance().setEccRetryCallId(
                    removeConnectionInternal(connection));
        } else { //Original flow
            super.removeConnection(connection);
        }
        /// @}

        if (connection instanceof TelephonyConnection) {
            TelephonyConnection telephonyConnection = (TelephonyConnection) connection;
            telephonyConnection.removeTelephonyConnectionListener(mTelephonyConnectionListener);
        }
    }

    /**
     * When a {@link TelephonyConnection} has its underlying original connection configured,
     * we need to add it to the correct conference controller.
     *
     * @param connection The connection to be added to the controller
     */
    public void addConnectionToConferenceController(TelephonyConnection connection) {
        int connPhoneType = PhoneConstants.PHONE_TYPE_NONE;
        if (connection instanceof MtkGsmCdmaConnection) {
            connPhoneType = ((MtkGsmCdmaConnection) connection)
                    .getPhoneType();
        }
        // TODO: Need to revisit what happens when the original connection for the
        // TelephonyConnection changes.  If going from CDMA --> GSM (for example), the
        // instance of TelephonyConnection will still be a CdmaConnection, not a GsmConnection.
        // The CDMA conference controller makes the assumption that it will only have CDMA
        // connections in it, while the other conference controllers aren't as restrictive.  Really,
        // when we go between CDMA and GSM we should replace the TelephonyConnection.
        if (connection.isImsConnection()) {
            Log.d(this, "Adding IMS connection to conference controller: " + connection);
            mImsConferenceController.add(connection);
            mTelephonyConferenceController.remove(connection);
            if (connPhoneType == PhoneConstants.PHONE_TYPE_CDMA) {
                mMtkCdmaConferenceController.remove((MtkGsmCdmaConnection) connection);
            }
        } else {
            int phoneType = connection.getCall().getPhone().getPhoneType();
            if (phoneType == TelephonyManager.PHONE_TYPE_GSM) {
                Log.d(this, "Adding GSM connection to conference controller: " + connection);
                mTelephonyConferenceController.add(connection);
                if (connPhoneType == PhoneConstants.PHONE_TYPE_CDMA) {
                    mMtkCdmaConferenceController.remove((MtkGsmCdmaConnection) connection);
                }
            } else if (phoneType == TelephonyManager.PHONE_TYPE_CDMA &&
                    connPhoneType == PhoneConstants.PHONE_TYPE_CDMA) {
                Log.d(this, "Adding CDMA connection to conference controller: " + connection);
                mMtkCdmaConferenceController.add((MtkGsmCdmaConnection) connection);
                mTelephonyConferenceController.remove(connection);
            }
            Log.d(this, "Removing connection from IMS conference controller: " + connection);
            mImsConferenceController.remove(connection);
        }
    }

    protected void placeOutgoingConnection(
            TelephonyConnection connection, Phone phone, int videoState, Bundle extras) {
        String number = connection.getAddress().getSchemeSpecificPart();

        /// M: CC: Set PhoneAccountHandle for ECC @{
        //[ALPS01794357]
        boolean isEmergencyNumber = PhoneNumberUtils.isLocalEmergencyNumber(this, number);
        if (isEmergencyNumber) {
            final PhoneAccountHandle phoneAccountHandle;
            String phoneIccId = phone.getFullIccSerialNumber();
            int slotId = SubscriptionController.getInstance().getSlotIndex(phone.getSubId());
            if (slotId != SubscriptionManager.INVALID_SIM_SLOT_INDEX) {
                phoneIccId = !TextUtils.isEmpty(phoneIccId) ? phoneIccId
                        : TelephonyManager.getDefault().getSimSerialNumber(phone.getSubId());
            }
            if (TextUtils.isEmpty(phoneIccId)) {
                // If No SIM is inserted, the corresponding IccId will be null,
                // take phoneId as PhoneAccountHandle::mId which is IccId originally
                phoneAccountHandle = PhoneUtils.makePstnPhoneAccountHandle(
                        Integer.toString(phone.getPhoneId()));
            } else {
                phoneAccountHandle = PhoneUtils.makePstnPhoneAccountHandle(phoneIccId);
            }
            log("placeOutgoingConnection, set back account mId: " + phoneAccountHandle.getId() +
                    ", iccId: " + phoneIccId);
            ((MtkTelephonyConnection)connection).setAccountHandle(phoneAccountHandle);
        }
        /// @}

        com.android.internal.telephony.Connection originalConnection = null;
        try {
            if (phone != null) {
                originalConnection = phone.dial(number, null, videoState, extras);

                if (phone instanceof GsmCdmaPhone) {
                    GsmCdmaPhone gsmCdmaPhone = (GsmCdmaPhone) phone;
                    if (gsmCdmaPhone.isNotificationOfWfcCallRequired(number)) {
                        // Send connection event to InCall UI to inform the user of the fact they
                        // are potentially placing an international call on WFC.
                        Log.i(this, "placeOutgoingConnection - sending international call on WFC " +
                                "confirmation event");
                        connection.sendConnectionEvent(
                                TelephonyManager.EVENT_NOTIFY_INTERNATIONAL_CALL_ON_WFC, null);
                    }
                }
            }
        } catch (CallStateException e) {
            Log.e(this, e, "placeOutgoingConnection, phone.dial exception: " + e);
            int cause = android.telephony.DisconnectCause.OUTGOING_FAILURE;
            if (e.getError() == CallStateException.ERROR_OUT_OF_SERVICE) {
                cause = android.telephony.DisconnectCause.OUT_OF_SERVICE;
            } else if (e.getError() == CallStateException.ERROR_POWER_OFF) {
                cause = android.telephony.DisconnectCause.POWER_OFF;
            }
            /// M: CC: ECC retry @{
            // Assume only one ECC exists
            if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                Log.i(this, "ECC retry: clear ECC param");
                MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
            }
            /// @}
            connection.setDisconnected(DisconnectCauseUtil.toTelecomDisconnectCause(
                    cause, e.getMessage()));
            /// M: CC: to check whether the device has on-going ECC
            RadioManager.getInstance().forceSetEccState(false);
            /// M: CC: Destroy TelephonyConnection if framework fails to dial @{
            connection.destroy();
            /// @}
            return;
        }

        if (originalConnection == null) {
            int telephonyDisconnectCause = android.telephony.DisconnectCause.OUTGOING_FAILURE;
            // On GSM phones, null connection means that we dialed an MMI code
            if (phone.getPhoneType() == PhoneConstants.PHONE_TYPE_GSM) {
                Log.d(this, "dialed MMI code");
                int subId = phone.getSubId();
                Log.d(this, "subId: "+subId);
                telephonyDisconnectCause = android.telephony.DisconnectCause.DIALED_MMI;
                final Intent intent = new Intent(this, MMIDialogActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                if (SubscriptionManager.isValidSubscriptionId(subId)) {
                    intent.putExtra(PhoneConstants.SUBSCRIPTION_KEY, subId);
                }
                startActivity(intent);
            }
            Log.d(this, "placeOutgoingConnection, phone.dial returned null");
            /// M: CC: ECC retry @{
            // Assume only one ECC exists
            if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                Log.i(this, "ECC retry: clear ECC param");
                MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
            }
            /// @}
            connection.setDisconnected(DisconnectCauseUtil.toTelecomDisconnectCause(
                    telephonyDisconnectCause, "Connection is null"));
            /// M: CC: to check whether the device has on-going ECC
            RadioManager.getInstance().forceSetEccState(false);
            /// M: CC: Destroy TelephonyConnection if framework fails to dial @{
            connection.destroy();
            /// @}
        } else {
            connection.setOriginalConnection(originalConnection);
        }
    }

    /// M: CC: TelephonyConnectionService canDial check @{
    public TelephonyConnection getFgConnection() {

        for (Connection c : getAllConnections()) {

            if (!(c instanceof TelephonyConnection)) {
                // the connection may be ConferenceParticipantConnection.
                continue;
            }

            TelephonyConnection tc = (TelephonyConnection) c;

            if (tc.getCall() == null) {
                continue;
            }

            Call.State s = tc.getCall().getState();

            // it assume that only one Fg call at the same time
            if (s == Call.State.ACTIVE || s == Call.State.DIALING || s == Call.State.ALERTING) {
                return tc;
            }
        }
        return null;
    }

    protected List<TelephonyConnection> getBgConnection() {

        ArrayList<TelephonyConnection> connectionList = new ArrayList<TelephonyConnection>();

        for (Connection c : getAllConnections()) {

            if (!(c instanceof TelephonyConnection)) {
                // the connection may be ConferenceParticipantConnection.
                continue;
            }

            TelephonyConnection tc = (TelephonyConnection) c;

            if (tc.getCall() == null) {
                continue;
            }

            Call.State s = tc.getCall().getState();

            // it assume the ringing call won't have more than one connection
            if (s == Call.State.HOLDING) {
                connectionList.add(tc);
            }
        }
        return connectionList;
    }

    protected List<TelephonyConnection> getRingingConnection() {

        ArrayList<TelephonyConnection> connectionList = new ArrayList<TelephonyConnection>();

        for (Connection c : getAllConnections()) {

            if (!(c instanceof TelephonyConnection)) {
                // the connection may be ConferenceParticipantConnection.
                continue;
            }

            TelephonyConnection tc = (TelephonyConnection) c;

            if (tc.getCall() == null) {
                continue;
            }

            // it assume the ringing call won't have more than one connection
            if (tc.getCall().getState().isRinging()) {
                connectionList.add(tc);
            }
        }
        return connectionList;
    }

    protected int getFgCallCount() {
        if (getFgConnection() != null) {
            return 1;
        }
        return 0;
    }

    protected int getBgCallCount() {
        return getBgConnection().size();
    }

    protected int getRingingCallCount() {
        return getRingingConnection().size();
    }

    public boolean canDial(PhoneAccountHandle accountHandle, String dialString) {

        boolean hasRingingCall = (getRingingCallCount() > 0);
        boolean hasActiveCall = (getFgCallCount() > 0);
        boolean bIsInCallMmiCommands = isInCallMmiCommands(dialString);
        Call.State fgCallState = Call.State.IDLE;

        Phone phone = getPhoneForAccount(accountHandle, false);

        /* bIsInCallMmiCommands == true only when dialphone == activephone */
        if (bIsInCallMmiCommands && hasActiveCall) {
            /// M: ALPS02123516. IMS incall MMI checking. @{
            /// M: ALPS02344383. null pointer check. @{
            if (phone != null && phone != getFgConnection().getPhone()
                    && phone.getImsPhone() != null
                    && phone.getImsPhone() != getFgConnection().getPhone()) {
                bIsInCallMmiCommands = false;
                log("phone is different, set bIsInCallMmiCommands to false");
            }
            /// @}
        }

        TelephonyConnection fConnection = getFgConnection();
        if (fConnection != null) {
            Call fCall = fConnection.getCall();
            if (fCall != null) {
                fgCallState = fCall.getState();
            }
        }

        /* Block dial if one of the following cases happens
        * 1. ECC exists in either phone
        * 2. has ringing call and the current dialString is not inCallMMI
        * 3. foreground connections in TelephonyConnectionService (both phones) are DISCONNECTING
        *
        * Different from AOSP canDial() in CallTracker which only checks state of current phone
        */
        boolean isECCExists = MtkTelephonyConnectionServiceUtil.getInstance().isECCExists();
        boolean result = (!isECCExists
                && !(hasRingingCall && !bIsInCallMmiCommands)
                && (fgCallState != Call.State.DISCONNECTING));

        if (result == false) {
            log("canDial"
                    + " hasRingingCall=" + hasRingingCall
                    + " hasActiveCall=" + hasActiveCall
                    + " fgCallState=" + fgCallState
                    + " getFgConnection=" + fConnection
                    + " getRingingConnection=" + getRingingConnection()
                    + " bECCExists=" + isECCExists);
        }
        return result;
    }

    private boolean isInCallMmiCommands(String dialString) {
        boolean result = false;
        char ch = dialString.charAt(0);

        switch (ch) {
            case '0':
            case '3':
            case '4':
            case '5':
                if (dialString.length() == 1) {
                    result = true;
                }
                break;

            case '1':
            case '2':
                if (dialString.length() == 1 || dialString.length() == 2) {
                    result = true;
                }
                break;

            default:
                break;
        }

        return result;
    }
    /// @}

    /// M: CC: HangupAll for FTA 31.4.4.2 @{
    private void hangupAll(String callId) {
        Log.d(this, "hangupAll %s", callId);
        if (mConnectionById.containsKey(callId)) {
            ((MtkTelephonyConnection)findConnectionForAction(callId, "hangupAll")).onHangupAll();
        } else {
            ((MtkTelephonyConference)findConferenceForAction(callId, "hangupAll")).onHangupAll();
        }
    }
    /// @}

    /// M: CC: For MSMS/MSMA ordered user operations.
    private void disconnect(String callId, String pendingOperation) {
        Log.d(this, "disconnect %s, pending call action %s", callId, pendingOperation);
        if (mConnectionById.containsKey(callId)) {
            ((MtkTelephonyConnection)findConnectionForAction(callId,
                    MtkConnection.OPERATION_DISCONNECT_CALL)).onDisconnect();
        } else {
            Conference conf = findConferenceForAction(callId,
                    MtkConnection.OPERATION_DISCONNECT_CALL);
            if (conf instanceof MtkTelephonyConference) {
                ((MtkTelephonyConference)conf).onDisconnect(pendingOperation);
            } else if (conf instanceof MtkCdmaConference) {
                ((MtkCdmaConference)conf).onDisconnect();
            }
        }
    }
    /// @}

    @Override
    protected void addConnection(String callId, Connection connection) {
        connection.setTelecomCallId(callId);
        mConnectionById.put(callId, connection);
        mIdByConnection.put(connection, callId);
        connection.addConnectionListener(mConnectionListener);
        connection.setConnectionService(this);
        /// M: CC: Force updateState for Connection once its ConnectionService is set @{
        // Forcing call state update after ConnectionService is set
        // to keep capabilities up-to-date.
        ((MtkTelephonyConnection)connection).fireOnCallState();
        /// @}
    }

    private void log(String s) {
        Log.d(TAG, s);
    }
    /// M: CC: Proprietary CRSS handling @{
    /**
     * Base class for forcing SuppMessage update after ConnectionService is set,
     * see {@link ConnectionService#addConnection}
     * To be overrided by children classes.
     * @hide
     */
    protected void forceSuppMessageUpdate(Connection conn) {
        MtkTelephonyConnectionServiceUtil.getInstance().forceSuppMessageUpdate(
                (MtkTelephonyConnection) conn);
    }
    /// @}

    /// M: CC: Interface for ECT @{
    private void explicitCallTransfer(String callId) {
        if (!canTransfer(mConnectionById.get(callId))) {
            Log.d(this, "explicitCallTransfer %s fail", callId);
            return;
        }
        Log.d(this, "explicitCallTransfer %s", callId);
        ((MtkTelephonyConnection) findConnectionForAction(callId, "explicitCallTransfer"))
                .onExplicitCallTransfer();
    }

    /**
      * Check whether onExplicitCallTransfer() can be performed on a certain connection.
      * Default implementation, need to be overrided.
      * @param bgConnection
      * @return true allowed false disallowed
      * @hide
      */
    public boolean canTransfer(Connection bgConnection) {

        if (bgConnection == null) {
            log("canTransfer: connection is null");
            return false;
        }

        if (!(bgConnection instanceof TelephonyConnection)) {
            // the connection may be ConferenceParticipantConnection.
            log("canTransfer: the connection isn't telephonyConnection");
            return false;
        }

        TelephonyConnection bConnection = (TelephonyConnection) bgConnection;

        Phone activePhone = null;
        Phone heldPhone = null;

        TelephonyConnection fConnection = getFgConnection();
        if (fConnection != null) {
            activePhone = fConnection.getPhone();
        }

        if (bgConnection != null) {
            heldPhone = bConnection.getPhone();
        }

        return (heldPhone == activePhone && activePhone.canTransfer());
    }

    /// M: CC: ECC retry. @{
    // Used for destroy the old connection when ECC phone type is not default phone type.
    private void resetTreatAsEmergencyCall(Connection connection) {
        if (connection instanceof MtkTelephonyConnection) {
            ((MtkTelephonyConnection) connection).resetTreatAsEmergencyCall();
        }
    }

    private void startTurnOnRadio(final Connection connection,
            final ConnectionRequest request, final Uri emergencyHandle, String number) {
        // Get the right phone object from the account data passed in.
        final Phone defaultPhone = getPhoneForAccount(request.getAccountHandle(), true);
        Phone phone = MtkTelephonyConnectionServiceUtil.getInstance()
                .selectPhoneBySpecialEccRule(request.getAccountHandle(), number, defaultPhone);

        /// M: TDD data only @{
        if (MtkTelephonyConnectionServiceUtil.getInstance().isDataOnlyMode(phone)) {
            Log.i(this, "startTurnOnRadio, 4G data only");
            /// M: CC: ECC retry @{
            // Assume only one ECC exists. Don't trigger retry
            // since Modem fails to power on should be a bug
            if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                Log.i(this, "ECC retry: clear ECC param");
                MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
            }
            /// @}
            connection.setDisconnected(
                    DisconnectCauseUtil.toTelecomDisconnectCause(
                    android.telephony.DisconnectCause.OUTGOING_CANCELED,
                    null));
            /// M: CC: to check whether the device has on-going ECC
            RadioManager.getInstance().forceSetEccState(false);
            connection.destroy();
            return;
        }
        /// @}

        /// M: CC: ECC retry @{
        if (!MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
            Log.i(this, "ECC retry: set param with Intial ECC.");
            MtkTelephonyConnectionServiceUtil.getInstance().setEccRetryParams(
                    request,
                    phone.getPhoneId());
        }
        /// @}

        final Phone eccPhone = phone;
        final int defaultPhoneType = PhoneFactory.getDefaultPhone().getPhoneType();
        if (mEmergencyCallHelper == null) {
            mEmergencyCallHelper = new MtkEmergencyCallHelper(this);
        }

        mEmergencyCallHelper.enableEmergencyCalling(new EmergencyCallStateListener.Callback() {
            @Override
            public void onComplete(EmergencyCallStateListener listener, boolean isRadioReady) {
                if (connection.getState() == Connection.STATE_DISCONNECTED) {
                    Log.i(this, "startTurnOnRadio, connection disconnect");
                    /// M: CC: to check whether the device has on-going ECC
                    RadioManager.getInstance().forceSetEccState(false);
                    return;
                }
                if (isRadioReady) {
                    // If the PhoneType of the Phone being used is different than the Default
                    // Phone, then we need create a new Connection using that PhoneType and
                    // replace it in Telecom.
                    if (phone.getPhoneType() != defaultPhoneType) {
                        Connection repConnection = getTelephonyConnection(request, number,
                                true, emergencyHandle, phone);
                        /// Modify the follow to handle the no sound issue. @{
                        // 1. Add the new connection into Telecom;
                        // 2. Disconnect the old connection;
                        // 3. Place the new connection.
                        if (repConnection instanceof TelephonyConnection) {
                            addExistingConnection(PhoneUtils.makePstnPhoneAccountHandle(phone),
                                    repConnection);
                            // Reset the emergency call flag for destroying old connection.
                            resetTreatAsEmergencyCall(connection);
                            connection.setDisconnected(
                                    DisconnectCauseUtil.toTelecomDisconnectCause(
                                            android.telephony.DisconnectCause.OUTGOING_CANCELED,
                                            "Reconnecting outgoing Emergency Call."));
                        } else {
                            /// M: CC: ECC retry @{
                            // Assume only one ECC exists
                            if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                                Log.i(this, "ECC retry: clear ECC param");
                                MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
                            }
                            /// @}
                            /// M: CC: to check whether the device has on-going ECC
                            RadioManager.getInstance().forceSetEccState(false);
                            connection.setDisconnected(repConnection.getDisconnectCause());
                        }
                        connection.destroy();
                        /// @}

                        // If there was a failure, the resulting connection will not be a
                        // TelephonyConnection, so don't place the call, just return!
                        if (repConnection instanceof TelephonyConnection) {
                            placeOutgoingConnection((TelephonyConnection) repConnection,
                                    phone, request);
                        }
                    } else {
                        placeOutgoingConnection((TelephonyConnection) connection,
                                phone, request);
                    }
                } else {
                    /// M: CC: ECC retry @{
                    // Assume only one ECC exists. Don't trigger retry
                    // since Modem fails to power on should be a bug
                    if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                        Log.i(this, "ECC retry: clear ECC param");
                        MtkTelephonyConnectionServiceUtil.getInstance()
                                .clearEccRetryParams();
                    }
                    /// @}
                    Log.i(this, "startTurnOnRadio, failed to turn on radio");
                    connection.setDisconnected(
                            DisconnectCauseUtil.toTelecomDisconnectCause(
                            android.telephony.DisconnectCause.POWER_OFF,
                            "Failed to turn on radio."));
                    /// M: CC: to check whether the device has on-going ECC
                    RadioManager.getInstance().forceSetEccState(false);
                    connection.destroy();
                }
            }
        });
    }
    /// @}

}
