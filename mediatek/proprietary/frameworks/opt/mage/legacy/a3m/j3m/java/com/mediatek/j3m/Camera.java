/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M Camera interface.
 */

package com.mediatek.j3m;

/* Extend JNI Overview */ /*! \addtogroup a3mJusr

<h2>%Camera</h2>

The Camera SceneNode simply defines the point in world space to be used as the
viewpoint for the scene.  The part of the world to be rendered is a volume that
is defined by the position and orientation of the camera, the projection and the
distance clipping ranges. For a perspective view the volume is a "view frustum".

\image html view_frustum_01.png "View Frustum for a Perspective View"

The shape of the frustum is set by using setNear(z), setFar(z), and
setFov(radians).

The distance clipping ranges (Z-Near and Z-Far, in the <i>camera's</i> Z
direction) exclude objects that are too near or too far from the camera.  This
improves performance.

<div class="learn">Remember to minimise the ZFar/ZNear ratio to avoid Z-fighting.</div>

The projection matrix of the camera can be obtained using getProjection() which
returns a Matrix4.  This is mainly for use when the Java user needs to cast "hit
rays" to detect what object has been selected by a touch on the screen.

<h3>Stereo Cameras</h3> Where hardware supports stereo displays as stereo camera
can be specified. This uses only one SceneNode but generates two slightly
separated viewpoints in the camera's X axis.  More details are given in the
setStereo() function.

*/

/*!
 * \ingroup a3mJusrScenenodes
 * @{
 */
/**
 * The Camera is a scene node that defines the world-space viewpoint for
 * rendering. As a scene node it can be positioned, rotated, etc. The Camera
 * specialism can also be pointed at another scene node, and has camera-specific
 * settings such as field of view (FOV), and Z-clipping distances.
 */
public interface Camera extends SceneNode {
    /** Identifier for this node type */
    char NODE_TYPE = 'C';

    /** Projection type.
     * In most 3D scenarios the PERSPECTIVE mode will be used, but for 1:1
     * mapping between a flat world and the screen the ORTHOGRAPHIC mode may be
     * useful.
     */
    public static class ProjectionType {
        /** Orthographic projection */
        public static final int ORTHOGRAPHIC = 0;
        /** Perspective projection */
        public static final int PERSPECTIVE = 1;
    }

    /**
     * Sets the type of projection used by the camera.
     *
     * @param type Projection type
     */
    void setProjectionType(int type);

    /**
     * Returns the type of projection used by the camera.
     *
     * @return Projection type
     */
    int getProjectionType();

    /**
     * Sets the Field-of-View angle when using a PERSPECTIVE projection.
     * In A3M the FOV is used for whichever is the smaller of the window
     * dimensions, ensuring that all objects within the specified angular range
     * will be visible of the screen.
     *
     * When using an ORTHOGRAPHIC projection, this parameter has no visible
     * effect, and setting the FOV while in ORTHOGRAPHIC mode will raise a
     * warning (though the value will still be used when in PERSPECTIVE mode).
     *
     * @param units The AngularUnits to use
     * @param fov The new FOV angle
     */
    void setFov(int units, float fov);

    /**
     * Returns the Field-of-View angle.
     *
     * @param units The AngularUnits in which to get the angle
     * @return FOV angle
     */
    float getFov(int units);

    /**
     * Sets the width of the viewing frustum in world-units when using an
     * ORTHOGRAPHIC projection.
     * When using an orthographic projection, objects will always be drawn the
     * same size on-screen as they are in the world (assuming no scaling has
     * been applied to the camera).  The frustum width determines the scale of
     * the screen coordinates (i.e how many world units correspond to an entire
     * width of the rendering viewport in pixels).  Often, a per-pixel (one
     * world-space unit corresponds to one pixel) orthographic projection is
     * desired, in which case the width should be equal to the width of the
     * rendering viewport in pixels (if the viewport size changes, this value
     * should be updated).  The default frustum width is 2 world units (giving
     * a range of -1 to 1 when the camera has no additional transformations
     * applied).
     *
     * When using an PERSPECTIVE projection, this parameter has no visible
     * effect, and setting the width while in PERSPECTIVE mode will raise a
     * warning (though the value will still be used when in ORTHOGRAPHIC mode).
     *
     * @param width Width of viewing frustum
     */
    void setWidth(float width);

    /**
     * Returns the width of the viewing frustum.
     *
     * @return Frustum width
     */
    float getWidth();

    /**
     * Sets the near clipping distance.
     *
     * @param near the new near clipping distance
     */
    void setNear(float near);

    /**
     * Returns the near clipping distance.
     *
     * @return Clipping distance
     */
    float getNear();

    /**
     * Sets the far clipping distance.
     *
     * @param far the new far clipping distance
     */
    void setFar(float far);

    /**
     * Returns the far clipping distance.
     *
     * @return Clipping distance
     */
    float getFar();

    /**
     * Set the data used in stereoscopic displays.
     * When creating the pair of renderings for a stereoscopic display
     * the camera is moved off the centre point to two positions either side,
     * representing the positions of the two eyes.  Simple approximations
     * to this process simply keep the camera looking at the same point in
     * world space but this introduces small rotations in the camera view
     * and the two cameras end up having non-parallel screen planes. This can
     * cause odd artifacts.
     *
     * The preferred approach is to shift both the camera and the viewpoint
     * so that the screen planes align. This means each camera has an
     * asymmetric view frustum.
     *
     * In A3M this is handled internally (as a 'stereo camera'). This
     * avoids unnecessary exposure of low-level data (frustum settings) at
     * the application level, and enables the rendering to be optimised
     * rather than crudely duplicated at the top level.
     *
     * The additional data required by the renderer is the 'focal length'
     * and the 'eye separation'.
     *
     * The 'focal length' is the distance from the camera to the plane
     * in world space where no stereo parallax is required - effectively
     * zScreen, and will normally be between zNear and zFar.
     *
     * The \b focal \b length is the more important factor as
     * this is the measurement that relates directly to the
     * model being generated by the application.  Eye separation is secondary
     * and is typically recommended to be approximately 1/30th of the focal
     * length.  Therefore a typical call to this would be:
     *
     * \code
     setStereo( focalLength, focalLength / 30.0f );
       \endcode
     *
     * Setting the eye separation to exactly zero disables the stereo camera
     * and rendering reverts to conventional mono rendering. There is no
     * separate 'enable' switch, to prevent it being accidently enabled with a
     * meaningless zero eye separation.
     *
     * @param focalDistance  the focal distance in world units
     * @param eyeSeparation  eye separation in world units, set zero to disable
     */
    void setStereo(float focalDistance, float eyeSeparation);

    /**
     * Returns whether this camera is using stereoscopic projections.
     *
     * @return True if the camera is stereoscopic
     */
    boolean isStereo();
}

/*! @} */
