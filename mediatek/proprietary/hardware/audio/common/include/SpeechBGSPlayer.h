#ifndef ANDROID_SPEECH_BACKGROUND_SOUND_PLAYER_H
#define ANDROID_SPEECH_BACKGROUND_SOUND_PLAYER_H

#include <pthread.h>
#include "AudioType.h"
#include "AudioUtility.h"
#include "MtkAudioComponent.h"
#include <AudioLock.h>

namespace android {
// for debug
//#define DUMP_BGS_DATA
//#define DUMP_BGS_BLI_BUF
//#define BGS_USE_SINE_WAVE
#if defined(SPH_SR32K)||defined(SPH_SR48K)
#define BGS_TARGET_SAMPLE_RATE  (32000)
#define BGS_PLAY_BUFFER_LEN     (6044)  // AudioMTKStreamOut write max 16384 bytes * (32000Hz / 44100Hz) * (1ch / 2ch) = 5944 bytes
#else
#define BGS_TARGET_SAMPLE_RATE  (16000)
#define BGS_PLAY_BUFFER_LEN     (3072)  // AudioMTKStreamOut write max 16384 bytes * (16000Hz / 44100Hz) * (1ch / 2ch) = 2972 bytes
#endif

/*=============================================================================
 *                              Class definition
 *===========================================================================*/

class BGSPlayer;
class SpeechDriverInterface;

class BGSPlayBuffer {
private:
    BGSPlayBuffer();
    virtual ~BGSPlayBuffer(); // only destroied by friend class BGSPlayer
    friend          class BGSPlayer;
    status_t        InitBGSPlayBuffer(BGSPlayer *playPointer, uint32_t sampleRate, uint32_t chNum, int32_t mFormat);
    uint32_t        Write(char *buf, uint32_t num);
    bool        IsBGSBlisrcDumpEnable();

    int32_t         mFormat;
    RingBuf         mRingBuf;
    MtkAudioSrcBase     *mBliSrc;
    char           *mBliOutputLinearBuffer;

    AudioLock       mBGSPlayBufferRuningMutex;
    AudioLock       mBGSPlayBufferMutex;

    bool            mExitRequest;

    bool        mIsBGSBlisrcDumpEnable;
    FILE        *pDumpFile;
};

class BGSPlayer {
public:
    virtual ~BGSPlayer();

    static BGSPlayer        *GetInstance();
    BGSPlayBuffer        *CreateBGSPlayBuffer(uint32_t sampleRate, uint32_t chNum, int32_t format);
    void        DestroyBGSPlayBuffer(BGSPlayBuffer *pBGSPlayBuffer);
    bool        Open(SpeechDriverInterface *pSpeechDriver, uint8_t uplink_gain, uint8_t downlink_gain);
    bool        Close();
    bool        IsBGSDumpEnable();
    uint32_t        Write(BGSPlayBuffer *pBGSPlayBuffer, void *buf, uint32_t num);
    uint32_t        PutData(BGSPlayBuffer *pBGSPlayBuffer, char *target_ptr, uint16_t num_data_request);
    uint16_t        PutDataToSpeaker(char *target_ptr, uint16_t num_data_request);

    AudioLock   mBGSMutex; // use for create/destroy bgs buffer & ccci bgs data request

private:
    BGSPlayer();

    static BGSPlayer        *mBGSPlayer; // singleton
    SpeechDriverInterface        *mSpeechDriver;
    SortedVector<BGSPlayBuffer *> mBGSPlayBufferVector;
    char        *mBufBaseTemp;

    AudioLock    mBGSPlayBufferVectorLock;
    uint16_t     mCount;

    bool        mIsBGSDumpEnable;
    FILE        *pDumpFile;
};


} // end namespace android

#endif //ANDROID_SPEECH_BACKGROUND_SOUND_PLAYER_H
