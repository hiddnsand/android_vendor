/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony.dataconnection;

import android.os.AsyncResult;

import com.android.internal.telephony.dataconnection.ApnSetting;
import com.android.internal.telephony.dataconnection.DcFailCause;
import com.android.internal.telephony.Phone;

import java.util.ArrayList;

/** IDataConnectionExt is a Plugin prototype for DataConnectionExt. */
public interface IDataConnectionExt {

    /**
    *Check if domestic roaming is enable or not.
    *@return true if domestic roaming is enable, else return false.
    */
    boolean isDomesticRoamingEnabled();

    /**
    *Check if data allow when data enable is turned off.
    *@param apnType Type of request APN.
    *@return true if data is allowed when data enable is turned off, else return false.
    */
    boolean isDataAllowedAsOff(String apnType);

    /**
    *Check if FDN support or not.
    *@return true if FDN is supported.
    */
    boolean isFdnEnableSupport();

    /**
    *IPV6RA feature, check address is valid.
    *@param address The address.
    *@return The address valid.
    */
    long getIPv6Valid(Object address);

    /**
     * For OP01 IOT test, when data connection activated, it will enable
     * firewall for non-ims pdn, which will drop unexpected dns package.
     *
     * @param apnTypes The data connection apnTypes.
     * @param ifc The interface name.
     */
    void onDcActivated(String[] apnTypes, String ifc);

    /**
     * For OP01 IOT test, when data connection deactivated, it will
     * disable firewall for non-ims pdn.
     *
     * @param apnTypes The data connection apnTypes.
     * @param ifc The interface name.
     */
    void onDcDeactivated(String[] apnTypes, String ifc);

    /**
    *Get retry timer.
    *@param reason Disconnect done reason.
    *@param defaultTimer The default time.
    *@return Retry time.
    */
    long getDisconnectDoneRetryTimer(String reason, long defaultTimer);

    /**
    *Check if only support single pdn.
    *@return true if only single pdn is supported.
    */
    public boolean isOnlySingleDcAllowed();

    /**
    *Ignore default data unselected.
    *@param apnType The type of apn to be established.
    *
    *@return true if input apntype is ims, emergency, and mms, else return false.
    */
    boolean ignoreDefaultDataUnselected(String apnType);

    /**
    *Ignore data roaming setting for not.
    *@param apnType The type of apn to be established.
    *
    *@return true if OP09 and if input apntype is ims in OP06,08,16, else return false.
    */
    boolean ignoreDataRoaming(String apnType);

    /**
     * To create data roaming customization instance.
     * @param phone phone proxy
     */
    void startDataRoamingStrategy(Phone phone);

    /**
     * To dispose data roaming customization instance.
     */
    void stopDataRoamingStrategy();

    /**
     * To get mcc&mnc from IMPI, see TelephonyManagerEx.getIsimImpi().
     * @param defaultValue default value.
     * @param phoneId phoneId used to get IMPI.
     * @return operator numeric
     */
    public String getOperatorNumericFromImpi(String defaultValue, int phoneId);

    /**
     * To check metered apn type is decided by load type or not.
     *
     * @return true if metered apn type is decided by load type.
     */
    public boolean isMeteredApnTypeByLoad();

    /**
     * To check apn type is metered or not.
     *
     * @param type APN type.
     * @param isRoaming true if network in roaming state.
     * @return true if this APN type is metered.
     */
    public boolean isMeteredApnType(String type, boolean isRoaming);

    /**
     * isIgnoredCause for nw reject
     */
    public boolean isIgnoredCause(DcFailCause cause);

    /**
     * handle PCO data after PS attached
     */
    public void handlePcoDataAfterAttached(final AsyncResult ar, final Phone phone,
            final ArrayList<ApnSetting> settings);
}
