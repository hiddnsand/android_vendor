package com.mediatek.media.mediascanner;

import android.media.MediaFile;
import android.mtp.MtpConstants;
import android.os.SystemProperties;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * The class implement for MediaFileEx.
 */
public class MediaFileExImpl extends MediaFileEx {
    private static final String TAG = "MediaFileExImpl";

    /// M: more audio file types
    private static final int FILE_TYPE_APE     = 111;
    private static final int FILE_TYPE_CAF     = 112;
    private static final int FILE_TYPE_ADPCM   = 113;
    private static final int FILE_TYPE_3GA     = 193;
    private static final int FILE_TYPE_QUICKTIME_AUDIO = 194;
    private static final int FILE_TYPE_MP2     = 197;
    private static final int FILE_TYPE_RA      = 198;
    private static final int FILE_TYPE_3GPP3   = 199;

    //M:add more video type
    private static final int FILE_TYPE_FLA     = 196;
    private static final int FILE_TYPE_MP2TS   = 308;
    private static final int FILE_TYPE_MP2PS   = 393;
    private static final int FILE_TYPE_OGM     = 394;
    private static final int FILE_TYPE_QUICKTIME_VIDEO = 397;
    private static final int FILE_TYPE_FLV     = 398;
    private static final String ADD_FILE_TYPE = "addFileType";
    private static Method sAddFileType;
    private static Method sAddFileTypeMoreDetail;

    static {
            sAddFileType = getMethod(MediaFile.class, ADD_FILE_TYPE,
                String.class, int.class, String.class);
            sAddFileTypeMoreDetail = getMethod(MediaFile.class, ADD_FILE_TYPE,
                String.class, int.class, String.class, int.class, boolean.class);
        }

    private static Method getMethod(Class<?> clazz, String methodName,
                                   Class<?>... parameterTypes) {
        try {
            Method method = clazz.getDeclaredMethod(methodName, parameterTypes);
            method.setAccessible(true);
            return method;
        } catch (NoSuchMethodException e) {
            Log.e(TAG, "[getMethod]", e);
        }
        return null;
    }

    private static Object callMethodOnObject(Object receiver, Method method, Object... args) {
        try {
            return method.invoke(receiver, args);
        } catch (InvocationTargetException e2) {
            Log.e(TAG, "[callMethodOnObject]", e2);
        } catch (IllegalAccessException e3) {
            Log.e(TAG, "[callMethodOnObject]", e3);
        }
        return null;
    }

    /**
     * Add more video file type.
     */
    public void addMoreVideoFileType() {
        /// M: Add more video file types to maps. {@
        callMethodOnObject(null, sAddFileType, "MTS", FILE_TYPE_MP2TS, "video/mp2ts");
        callMethodOnObject(null, sAddFileType, "M2TS", FILE_TYPE_MP2TS, "video/mp2ts");
        callMethodOnObject(null, sAddFileType, "MOV", FILE_TYPE_QUICKTIME_VIDEO, "video/quicktime");
        callMethodOnObject(null, sAddFileType, "QT", FILE_TYPE_QUICKTIME_VIDEO, "video/quicktime");
        callMethodOnObject(null, sAddFileType,  "OGV", FILE_TYPE_OGM, "video/ogm");
        callMethodOnObject(null, sAddFileType, "OGM", FILE_TYPE_OGM, "video/ogm");
        if (SystemProperties.getBoolean("ro.mtk_flv_playback_support", false)) {
            callMethodOnObject(null, sAddFileType, "FLV", FILE_TYPE_FLV, "video/x-flv");
            callMethodOnObject(null, sAddFileType, "F4V", FILE_TYPE_FLV, "video/x-flv");
            callMethodOnObject(null, sAddFileType, "PFV", FILE_TYPE_FLV, "video/x-flv");
            callMethodOnObject(null, sAddFileType, "FLA", FILE_TYPE_FLA, "audio/x-flv");
        }
        if (SystemProperties.getBoolean("ro.mtk_mtkps_playback_support", false)) {
            callMethodOnObject(null, sAddFileType, "PS", FILE_TYPE_MP2PS, "video/mp2p");
            /// Only support VOB when mtkps feature option is enabled
            callMethodOnObject(null, sAddFileType, "VOB", FILE_TYPE_MP2PS, "video/mp2p");
            /// DAT files should not be scanned as mpeg2 PS if PS is not supported.
            callMethodOnObject(null, sAddFileType, "DAT", FILE_TYPE_MP2PS, "video/mp2p");
        }
        /// @}
    }

   /**
     * Add more audio file type.
     */
    public void addMoreAudioFileType() {
       /// M: Add more audio file types to maps. {@
       callMethodOnObject(null, sAddFileType, "3GP", FILE_TYPE_3GPP3, "audio/3gpp");
       callMethodOnObject(null, sAddFileType, "3GA", FILE_TYPE_3GA, "audio/3gpp");
       callMethodOnObject(null, sAddFileType, "MOV", FILE_TYPE_QUICKTIME_AUDIO, "audio/quicktime");
       callMethodOnObject(null, sAddFileType, "QT", FILE_TYPE_QUICKTIME_AUDIO, "audio/quicktime");
       /// Add to support Apple Lossless Codec(audio/alac)
       if (SystemProperties.getBoolean("ro.mtk_audio_alac_support", false)) {
           callMethodOnObject(null, sAddFileType, "CAF", FILE_TYPE_CAF, "audio/alac");
       }
       if (SystemProperties.getBoolean("ro.mtk_audio_alac_support", false)) {
           callMethodOnObject(null, sAddFileType, "WAV", FILE_TYPE_ADPCM, "audio/adpcm");
       }
       /// Add to support PCM(audio/wav)
       callMethodOnObject(null, sAddFileTypeMoreDetail,
                "WAV", MediaFile.FILE_TYPE_WAV, "audio/wav", MtpConstants.FORMAT_WAV, true);
       callMethodOnObject(null, sAddFileTypeMoreDetail,
                "OGG", MediaFile.FILE_TYPE_OGG, "audio/vorbis", MtpConstants.FORMAT_OGG, true);
       callMethodOnObject(null, sAddFileTypeMoreDetail,
                "OGG", MediaFile.FILE_TYPE_OGG, "audio/webm", MtpConstants.FORMAT_OGG, true);
       /// Add to support MP2, first add video/mp2p, so that use MP2 can return as audio type
       if (SystemProperties.getBoolean("ro.mtk_support_mp2_playback", false)) {
           callMethodOnObject(null, sAddFileType, "MP2", FILE_TYPE_MP2, "audio/mpeg");
       }
       /// Add to support Monkey's Audio APE(audio/ape)
       if (SystemProperties.getBoolean("ro.mtk_audio_ape_support", false)) {
           callMethodOnObject(null, sAddFileType, "APE", FILE_TYPE_APE, "audio/ape");
       }
       /// Add to support OMA DRM audio type DCF
       if (SystemProperties.getBoolean("ro.mtk_oma_drm_support", false)) {
           callMethodOnObject(null, sAddFileType, "DCF", MediaFile.FILE_TYPE_MP3, "audio/mpeg");
       }
       /// @}

    }
}

