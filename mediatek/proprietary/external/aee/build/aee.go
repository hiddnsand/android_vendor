package mtkAeeFeature

import (
	"android/soong/android"
	"android/soong/cc"
	"android/soong/android/mediatek"
)

func mtkAeeDefaults(ctx android.LoadHookContext) {
	type props struct {
		Cflags []string
		Shared_libs []string
	}
	p := &props{}
	enabled := envTrue(ctx, "HAVE_AEE_FEATURE")
	if enabled {
		p.Cflags = append(p.Cflags, "-DHAVE_AEE_FEATURE")
		p.Shared_libs = append(p.Shared_libs, "libaed")
	}
	ctx.AppendProperties(p)
}

func init() {
	android.RegisterModuleType("mtk_aee_defaults", mtkAeeDefaultsFactory)
}

func mtkAeeDefaultsFactory() android.Module {
	module := cc.DefaultsFactory()
	android.AddLoadHook(module, mtkAeeDefaults)
	return module
}

func envTrue(ctx android.BaseContext, key string) bool {
	//if proptools.Bool(ctx.AConfig().ProductVariables.Eng) {
	//}
	//return ctx.AConfig().Getenv(key) == "yes"
	return mediatek.GetFeature("HAVE_AEE_FEATURE") == "yes"
}
