/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M main interface.
 */

package com.mediatek.j3m;

/** \ingroup a3mJref
 * @{
 */
/**
 * J3M Graphics Engine interface.
 * This interface defines functions for creating and loading top-level objects.
 * Since all objects are handled via interfaces, it is necessary to instantiate
 * a single implementation of this interface, through which all other objects
 * will then be instantiated.
 */
public interface J3m {
    /**
     * Returns the current version of the J3M implementation.
     */
    Version getVersion();

    /**
     * Returns a description of how the J3M implementation was built.
     */
    String getBuildInfo();

    /**
     * Creates a RenderContext.
     *
     * @return RenderContext
     */
    RenderContext createRenderContext();

    /**
     * Creates an AssetPool.
     * Asset pools are used to create and load engine resources.
     *
     * @return AssetPool
     */
    AssetPool createAssetPool();

    /**
     * Creates a default Appearance.
     * Appearances are assigned to Solid objects in order to determine how they
     * are rendered to screen.  Appearances can also be loaded from file using
     * an AssetPool.
     *
     * @return Appearance
     */
    Appearance createAppearance();

    /**
     * Creates an empty FlagMask.
     * This flag mask has no flags set.
     *
     * @return FlagMask
     */
    FlagMask createFlagMask();

    /**
     * Creates a FlagMask.
     *
     * @param index Index of the flag to create
     * @param state Default state of the flag
     * @return FlagMask
     */
    FlagMask createFlagMask(int index, boolean state);

    /**
     * Creates an empty SceneNode.
     * Scene nodes are used to create hierarchical structures called "scene
     * graphs", which are used to arrange objects in a scene for rendering.
     * A plain scene node has no visible properties, but contain other child
     * nodes, allowing for flexible scene arrangement.
     *
     * @return SceneNode
     */
    SceneNode createSceneNode();

    /**
     * Creates an empty Solid.
     * Solids are visible objects in a scene.  Solids must be assigned both a
     * Mesh and an Appearance in order to be rendered.
     *
     * @return Solid
     */
    Solid createSolid();

    /**
     * Creates a camera.
     *
     * @return Camera
     */
    Camera createCamera();

    /**
     * Creates a light.
     *
     * @return Light
     */
    Light createLight();

    /**
     * Creates a Renderer.
     * A renderer is what actually renders the scene from the perspective of a
     * camera.  Different kinds of renderer may exist in the future of different
     * purposes.
     *
     * @param context Global rendering context
     * @param pool Asset pool to use to create the renderer
     * @return Renderer
     */
    Renderer createRenderer(RenderContext context, AssetPool pool);

    /**
     * Creates a MotionBlurRenderer.
     * A special renderer used to create an image of screen-space velocities.
     * This image can then be used to blur an ordinary copy of the scene using
     * the scale and direction of the velocity at each pixel.
     *
     * @param context Global rendering context
     * @param pool Asset pool to use to create the renderer
     * @return a MotionBlurRenderer
     */
    Renderer createMotionBlurRenderer(RenderContext context, AssetPool pool);

    /**
     * Creates a RenderBlock.
     * Render blocks are the basic building block of the rendering pipeline.  A
     * render block simply takes a scene and uses a renderer to render it from
     * a camera's perspective.  Different kinds of render block may exist in
     * the future for different purposes.
     *
     * @param renderer Renderer to use to render the scene
     * @param sceneNode Root node of the scene to render
     * @param camera Camera to use to render the scene
     * @return RenderBlock
     */
    RenderBlock createRenderBlock(
        Renderer renderer,
        SceneNode sceneNode,
        Camera camera);

    /**
     * Creates a RenderBlockGroup.
     * Render block groups are a special kind of render block which can contain
     * other render blocks.  Blocks in a group are rendered sequentially,
     * allowing different rendering pipelines to be constructed.
     *
     * @return RenderBlockGroup
     */
    RenderBlockGroup createRenderBlockGroup();

    /**
     * Creates a RenderTarget.
     * A RenderTarget can be rendered as an alternative to rendering directly to
     * a device's screen buffer. The result of rendering is then available as a
     * texture which can then be used as a source for a subsequent render stage.
     *
     * RenderBlocks may be given a RenderTarget to use.
     *
     * @param colourTexture Colour texture target
     * @param depthTexture Depth texture target
     * @param depthBuffer True to use depth buffer if there's no depth texture
     * @param stencilBuffer True to use stencil buffer
     * @return RenderBlock
     */
    RenderTarget createRenderTarget(
        Texture2D colourTexture,
        Texture2D depthTexture,
        boolean depthBuffer,
        boolean stencilBuffer);

    /**
     * Creates a raycast Ray.
     * Rays comprise a position plus a direction, and can be tested for
     * intersection with collision shapes.
     *
     * @return Ray
     */
    Ray createRay();

    /**
     * Creates a plane collision shape.
     * Planes are a flat collision shape which extend infinitely along both
     * dimensions.
     *
     * @return Plane
     */
    Plane createPlane();

    /**
     * Creates a square collision shape.
     * Squares are 1x1 bounded collision planes.
     *
     * @return Square
     */
    Square createSquare();

    /**
     * Creates a sphere collision shape.
     * The collision sphere has a radius of 1.
     *
     * @return Sphere
     */
    Sphere createSphere();

    /**
     * Creates a default Version, (0, 0, 0, "")
     *
     * @return Version
     */
    Version createVersion();

    /**
     * Creates a Version in the format (major, 0, 0, "")
     *
     * @param major Major version number
     * @return Version
     */
    Version createVersion(int major);

    /**
     * Creates a Version in the format (major, minor, 0, "")
     *
     * @param major Major version number
     * @param minor Minor version number
     * @return Version
     */
    Version createVersion(int major, int minor);

    /**
     * Creates a Version in the format (major, minor, patch, "")
     *
     * @param major Major version number
     * @param minor Minor version number
     * @param patch Patch version number
     * @return Version
     */
    Version createVersion(int major, int minor, int patch);

    /**
     * Creates a Version in the format (major, minor, patch, extra)
     *
     * @param major Major version number
     * @param minor Minor version number
     * @param patch Patch version number
     * @param extra Extra information string
     * @return Version
     */
    Version createVersion(int major, int minor, int patch, String extra);

    /**
     * Returns a block of pixels copied from an area of the backbuffer.
     * The pixels will be in RGBA format.
     *
     * @param x Left edge of area
     * @param y Top edge of area
     * @param width Width of area
     * @param height Height of area
     * @return Array of RGBA pixel values of size width * height * 4
     */
    byte[] getPixels(int x, int y, int width, int height);

    /**
     * Get Z order of lights in experimental renderer.
     *
     * @return Z light order
     */
    int getLightZOrder();
}

/*! @} */

/**************************************************************************
 * JAVA User Documentation (Doxygen)
 ***************************************************************************/

/* AUTHORS:

Wherever possible the User description should be with the corresponding classes
and use \addtogroup to add it to this section.  For example SceneNode.java
contains the "Scene Construction" part of this introduction as well as the
'Reference' section describing the class per se.

*/

/** \defgroup a3mJusr A3M Java API Users Guide

A tour of the A3M Java API and links to detailed class descriptions.

<div class="learn">Please read the \ref a3mPrimer first, even if you are
familiar with 3D graphics.</div>

<h2>A3M Types</h2>

The Java garbage collector can cause the rendering thread to pause for
fractions of a section, and can have a negative effect on the smoothness of a
rendered sequence.  In order to reduce the workload for the garbage collector,
the A3M Java interface does not use individual objects for basic data types and
mathematical entities (e.g. vectors, angles, matrices).  Instead, functions
take and return raw numerical values.  If the application programmer requires
these kinds of small objects, they can store these values in their own
intermediate objects for convenience.  However, the user should prefer to
create these objects outside of the rendering loop or at a time when rendering
will not be affected, and reuse them as much as possible.

Angular values are positive and work in a Right Handed fashion.

\image html right_hand_rotation.png "Right-Handed Rotation"

*/



/* Java API Reference Material Groupings */
/**
 * \defgroup a3mJusrRender Graphics renderer (render loop)
 * \ingroup a3mJusr
 * Contains the Renderer class that creates the scene on the display.
 */

/**
 * \defgroup a3mJusrScenenodes Scene-object classes
 * \ingroup a3mJusr
 *
 * SceneNode, Camera, etc.
 *
 * Classes related to scene nodes - the base generic SceneNode and
 * derivatives such as Camera and Solid objects - also describes the Model
 * object, which, while not a scene node object <i> per se </i> it results
 * in one.
 */

/**
 * \defgroup a3mJusrAppear Appearance-related classes
 * \ingroup a3mJusr
 * Textures, Blending, Clipping, Shaders, Asset management (for textures), etc.
 */

/**
 * \defgroup a3mJusrMaths Maths-related classes
 * \ingroup a3mJusr
 * AngularUnits
 */

/**
 * \defgroup a3mJusrCollision Collision test-related classes
 * \ingroup a3mJusr
 * Shape, Plane, Square, Sphere, etc.
 */

/**
 * \defgroup a3mJref Other A3M JNI reference material
 * Classes and types that are for JNI-internal use only.
 *
 * \warning These do not form part of the public API and may change
 * without notice.
 */
