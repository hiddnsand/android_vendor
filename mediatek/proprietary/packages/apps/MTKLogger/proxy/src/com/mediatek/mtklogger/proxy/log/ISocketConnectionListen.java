package com.mediatek.mtklogger.proxy.log;

/**
 * @author MTK81255
 *
 */
public interface ISocketConnectionListen {

    /**
     * @param response String
     */
    public void socketResponse(String response);

}
