LOCAL_DIR := $(GET_LOCAL_DIR)
TARGET := k70hd_bsp_fwv_512m
MODULES += app/mt_boot \
           dev/lcm
MTK_EMMC_SUPPORT = yes
DEFINES += MTK_NEW_COMBO_EMMC_SUPPORT
#DEFINES += MTK_KERNEL_POWER_OFF_CHARGING
#MTK_KERNEL_POWER_OFF_CHARGING = yes
DEFINES += MTK_GPT_SCHEME_SUPPORT
MTK_KERNEL_POWER_OFF_CHARGING = yes
MTK_LCM_PHYSICAL_ROTATION = 0
CUSTOM_LK_LCM = "hx8392a_dsi_cmd_fwvga"
MTK_SECURITY_SW_SUPPORT = yes
MTK_SEC_FASTBOOT_UNLOCK_SUPPORT = yes
MTK_VERIFIED_BOOT_SUPPORT = yes
BOOT_LOGO:=fwvga
DEBUG := 2
#DEFINES += WITH_DEBUG_DCC=1
DEFINES += WITH_DEBUG_UART=1
#DEFINES += WITH_DEBUG_FBCON=1
#DEFINES += MACH_FPGA=y
#DEFINES += MACH_FPGA_NO_DISPLAY=y
#DEFINES += NO_POWER_OFF=y
MTK_PROTOCOL1_RAT_CONFIG = W/G
MTK_MD1_SUPPORT = 3
MTK_SECURITY_ANTI_ROLLBACK = no
MTK_GOOGLE_TRUSTY_SUPPORT=no
MTK_DM_VERITY_OFF = yes
