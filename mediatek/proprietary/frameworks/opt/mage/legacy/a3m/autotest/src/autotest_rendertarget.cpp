/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M RenderTarget unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <GLES2/gl2.h>                  /* for glGetError() etc.              */
#include <a3m/renderdevice.h>           /* for RenderDevice                   */
#include <a3m/rendertarget.h>           /* for RenderTarget                   */
#include <a3m/texture2d.h>              /* for Texture2D                      */
#include <texture2dloader.h>            /* for Texture2DLoader                */
#include <autotest_common.h>            /* for AutotestTest                   */
#include <memfilesource_autotest.h>     /* for create_memfilesource_autotest  */

using namespace a3m;

namespace
{

  /*
   * Test fixture.
   */
  struct A3mRenderTargetTest : public AutotestTest
  {
    Texture2DCache::Ptr cache;

    A3mRenderTargetTest()
    {
      cache.reset(new Texture2DCache());
      cache->registerLoader(Texture2DLoader::Ptr(new Texture2DLoader()));
      cache->registerSource(create_memfilesource_autotest());
    }

    ~A3mRenderTargetTest()
    {
      cache->release();
    }

    Texture2D::Ptr createTexture(A3M_INT32 size)
    {
      return cache->create(
               size,
               size,
               Texture::RGBA,
               Texture::UNSIGNED_BYTE,
               0);
    }
  };

} // namespace

/*
 * Test RenderTarget constructors.
 */
TEST_F(A3mRenderTargetTest, Constructors)
{
  Texture2D::Ptr nullTexture;
  Texture2D::Ptr texture = createTexture(0);

  RenderTarget::Ptr renderTarget1(new RenderTarget(nullTexture, nullTexture));

  EXPECT_FALSE(renderTarget1->getColourTexture());
  EXPECT_FALSE(renderTarget1->getDepthTexture());
  EXPECT_FALSE(renderTarget1->isValid());

  RenderTarget::Ptr renderTarget2(new RenderTarget(texture, nullTexture));

  EXPECT_TRUE(renderTarget2->getColourTexture());
  EXPECT_FALSE(renderTarget2->getDepthTexture());
  EXPECT_FALSE(renderTarget2->isValid());

  RenderTarget::Ptr renderTarget3(new RenderTarget(nullTexture, texture));

  EXPECT_FALSE(renderTarget3->getColourTexture());
  EXPECT_TRUE(renderTarget3->getDepthTexture());
  EXPECT_FALSE(renderTarget3->isValid());
  AutotestTest::clearError();

  Texture2D::Ptr depthTexture = createTexture(2048);
  RenderTarget::Ptr renderTarget4(new RenderTarget(depthTexture, nullTexture));

  EXPECT_TRUE(renderTarget4->getColourTexture());
  EXPECT_FALSE(renderTarget4->getDepthTexture());
  EXPECT_TRUE(renderTarget4->isValid());
  EXPECT_EQ(renderTarget4->getColourTexture(), depthTexture);
}

/*
 * Test RenderTarget enabling and disabling.
 */
TEST_F(A3mRenderTargetTest, EnableDisable)
{
  Texture2D::Ptr nullTexture;
  Texture2D::Ptr depthTexture = createTexture(2048);

  RenderTarget::Ptr renderTarget(new RenderTarget(depthTexture, nullTexture));
  renderTarget->enable();
  EXPECT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());

  renderTarget->disable();
  EXPECT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());
}
