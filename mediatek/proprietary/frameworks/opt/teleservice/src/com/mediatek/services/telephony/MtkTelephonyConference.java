/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.services.telephony;

import com.android.internal.telephony.Call;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.Phone;
import com.android.services.telephony.Log;
import com.android.services.telephony.TelephonyConference;
import com.android.services.telephony.TelephonyConnection;
import com.mediatek.internal.telephony.MtkGsmCdmaPhone;

import android.telecom.Connection;
import android.telecom.PhoneAccountHandle;

/**
 * MTK add-on extension for TelephonyConference
 */
public class MtkTelephonyConference extends TelephonyConference {

    private static final String TAG = "TelephonyConf";

    public MtkTelephonyConference(PhoneAccountHandle phoneAccount) {
        super(phoneAccount);
    }

    /// M: CC: HangupAll for FTA 31.4.4.2 @{
    /**
     * Invoked when the Conference and all it's {@link Connection}s should be disconnected.
     * @hide
     */
    public void onHangupAll() {
        final TelephonyConnection connection = getFirstConnection();
        if (connection != null) {
            try {
                Phone phone = connection.getPhone();
                if (phone != null && (phone instanceof MtkGsmCdmaPhone)) {
                    ((MtkGsmCdmaPhone)phone).hangupAll();
                } else {
                    Log.w(TAG, "Attempting to hangupAll a connection without backing phone.");
                }
            } catch (CallStateException e) {
                Log.e(TAG, e, "Exception thrown trying to hangupAll a conference");
            }
        }
    }
    /// @}

    /// M: CC: For DSDS/DSDA Two-action operation @{
    /**
     * Invoked when the Conference and all it's {@link Connection}s should be disconnected,
     * with pending call action, answer?
     * @param pendingCallAction The pending call action.
     */
    public void onDisconnect(String pendingCallAction) {
        for (Connection connection : getConnections()) {
            if (disconnectCall(connection, pendingCallAction)) {
                break;
            }
        }
    }

    /**
     * Disconnect the underlying Telephony Call for a connection.
     * with pending call action, answer?
     *
     * @param connection The connection.
     * @param pendingCallAction The pending call action.
     * @return {@code True} if the call was disconnected.
     */
    private boolean disconnectCall(Connection connection, String pendingCallAction) {
        Call call = getMultipartyCallForConnection(connection, "onDisconnect");
        if (call != null) {
            log("Found multiparty call to hangup for conference, with pending action");
            /// M: CC: Hangup Conference special handling @{
            // To avoid hangupForegroundResumeBackground() is invoked by hangup(GsmCall),
            // hangup all connections in a conference one by one via conn.hangup()
            // [ALPS02408504] [ALPS01814074]
            //
            // To avoid hangup a disconnected conn by checking conn state first [ALPS01941738]
            // FIXME: mConnections in GsmCall is NOT removed when a conn in a conf is disconnected.
            log("pendingCallAction = " + pendingCallAction);
            if ("answer".equals(pendingCallAction) || "unhold".equals(pendingCallAction)) {
                for (com.android.internal.telephony.Connection conn: call.getConnections()) {
                    if (conn != null && conn.isAlive()) {
                        try {
                            conn.hangup();
                        } catch (CallStateException e) {
                            Log.e(TAG, e, "Exception thrown trying to hangup conference member");
                            return false;
                        }
                    }
                }
                return true;
            } else {
                try {
                    call.hangup();
                    return true;
                } catch (CallStateException e) {
                    Log.e(TAG, e, "Exception thrown trying to hangup conference");
                }
            }
            /// @}
        }
        return false;
    }
    /// @}

    private void log(String s) {
        Log.d(TAG, s);
    }

}
