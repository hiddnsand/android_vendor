package com.mediatek.op01.dialer;

import android.content.Context;

import com.mediatek.dialer.ext.ICallDetailExtension;
import com.mediatek.dialer.ext.ICallLogExtension;
import com.mediatek.dialer.ext.IDialPadExtension;
import com.mediatek.dialer.ext.IDialerUtilsExtension;
import com.mediatek.dialer.ext.OpDialerCustomizationFactoryBase;

import com.mediatek.op01.dialer.calllog.Op01CallDetailExtension;
import com.mediatek.op01.dialer.calllog.Op01CallLogExtension;
import com.mediatek.op01.dialer.speeddial.Op01DialPadExtension;

public class Op01DialerCustomizationFactory extends OpDialerCustomizationFactoryBase {
    public Context mContext;
    public Op01DialerCustomizationFactory (Context context){
        mContext = context;
    }

    @Override
    public ICallDetailExtension makeCallDetailExt() {
        return new Op01CallDetailExtension(mContext);
    }

    @Override
    public ICallLogExtension makeCallLogExt() {
        return new Op01CallLogExtension(mContext);
    }

    @Override
    public IDialPadExtension makeDialPadExt() {
        return new Op01DialPadExtension(mContext);
    }

    @Override
    public IDialerUtilsExtension makeDialerUtilsExt() {
        return new Op01DialerUtilsExtension();
    }
}
