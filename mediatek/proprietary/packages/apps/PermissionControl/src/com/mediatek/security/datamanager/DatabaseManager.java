package com.mediatek.security.datamanager;

import android.Manifest;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.mediatek.security.R;
import com.mediatek.security.service.PermControlUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class DatabaseManager {
    private static final String TAG = "DatabaseManager";

    public static enum InitLevel {UNINIT, CELLULAR_DATA_OFF, CELLULAR_DATA_ON};
    private static InitLevel mInitLevel = InitLevel.UNINIT;

    private static DatabaseHelper sDataBaseHelper;
    private static HashMap<String, CheckedPermRecord> sPkgPermissionsCache =
      new HashMap<String, CheckedPermRecord>();

    /**
     * Init database and cache must be called at first and in async process
     * @param context Context of process
     */
    public static void initDataBase(Context context, InitLevel level) {
        Log.d("@M_" + TAG, "initDataBase");
        if (level.ordinal() > mInitLevel.ordinal()) {
            mInitLevel = level;
            createDataBase(context);
            updateDataBase(context);
            setDataIntoCache();
        }
    }

    public static void deInitDataBase() {
        mInitLevel = InitLevel.UNINIT;
    }

    private static void createDataBase(Context context) {
        if (sDataBaseHelper == null) {
            Log.d("@M_" + TAG, "new DatabaseHelper");
            sDataBaseHelper = new DatabaseHelper(context);
        }
    }

    public static HashSet<String> getInstalledPkgSet(Context context) {
        List<PackageInfo> infos = PermControlUtils.getInstalledPackages(
                context, PermControlUtils.GET_3TH_PACKAGE);
        HashSet<String> sets = new HashSet<String>();
        for (PackageInfo info : infos) {
            sets.add(info.packageName);
        }
        return sets;
    }

    private static HashMap<String, List<CheckedPermRecord>> getPkg(Context context,
                                                                    int type) {
        List<PackageInfo> packageInfoList = null;
        packageInfoList = PermControlUtils.getInstalledPackages(context, type);
        if (packageInfoList == null) {
            //in some case no app install /data/app
            return null;
        }
        //Get current install package info and set in permission record list into a map
        HashMap<String, List<CheckedPermRecord>> currentMap = null;
        for (PackageInfo info : packageInfoList) {
            List<CheckedPermRecord> permRecordList =
                    PermControlUtils.getPermRecordListByPkg(info.packageName, info);
            if (permRecordList != null) {
                if (currentMap == null) {
                    currentMap = new HashMap<String, List<CheckedPermRecord>>();
                }
                currentMap.put(info.packageName, permRecordList);
            }
        }
        return currentMap;
    }

    private static void updateDataBase(Context context) {
        updateCellularData(context);
    }

    private static void updateCellularData(Context context) {
        HashMap<String, List<CheckedPermRecord>> map =
            getPkg(context, PermControlUtils.GET_3TH_PACKAGE);
        if (map != null) {
            HashSet<String> sets = sDataBaseHelper.getCellularDataPkgNameFromDB();
            Log.d("@M_" + TAG, "getInstalledPackages = " + map.size() + ", sets = " + sets.size());
            Iterator<String> interator = map.keySet().iterator();
            while (interator.hasNext()) {
                String pkgName = interator.next();
                if (sets.contains(pkgName)) {
                    interator.remove();
                    sets.remove(pkgName);
                }
            }

            Log.d("@M_" + TAG, "new = " + map.size() + ", old = " + sets.size());
            Set<String> names = map.keySet();
            for (String pkgName : names) {
                List<CheckedPermRecord> records = map.get(pkgName);
                if (isUseInternet(records)) {
                    sDataBaseHelper.addCellularDatePerm(new CheckedPermRecord(
                            pkgName, R.string.cellular_data_message));
                }
            }

            for (String pkgName : sets) {
                sDataBaseHelper.deleteCellularDatePerm(pkgName);
            }
        }

        if (PermControlUtils.isCellularDataControlOn(context)) {
            List<CheckedPermRecord> cache = sDataBaseHelper
                    .getCellularDataPermFromDB();
            PackageManager pm = context.getPackageManager();
            for (CheckedPermRecord record : cache) {
                ApplicationInfo info = null;
                try {
                    info = pm.getApplicationInfo(
                      record.mPackageName, 0);
                    } catch (NameNotFoundException e) {
                        e.printStackTrace();
                    }
                if (record.getStatus() != CheckedPermRecord.STATUS_GRANTED) {
                    PermControlUtils.setFirewallPolicy(
                                context, info.uid, 0, true);
                }
                if (record.getStatus() == CheckedPermRecord.STATUS_FIRST_CHECK) {
                    PermControlUtils.updateCtaAppStatus(
                        info.uid, true);
                } else {
                    PermControlUtils.updateCtaAppStatus(
                        info.uid, false);
                }
            }
        }
    }

    private static void testPrint(String type, Map<String, List<CheckedPermRecord>> map) {
        if (map != null) {
            Set<String> key = map.keySet();
            for (String pkgName : key) {
                Log.d("@M_" + TAG, type + " = " + pkgName);
            }
        }
    }

    public static void setDataIntoCache() {
        sDataBaseHelper.initCacheData();
        sPkgPermissionsCache = sDataBaseHelper.getCellularDataPermCache();
    }

    /**
     * Add a new install pkg permission into database. Since it will write database,
     * call it in Asynchronous.
     * @param pkgName package name
     * @param permList package related Permission List
     * @return the package corresponding permission record list, if null the package no
     * permission under monitor or no permission registered
     */
    public static List<CheckedPermRecord> add(Context context, String pkgName) {
        List<CheckedPermRecord> newPkgPermRecordList =
                PermControlUtils.getPackagePermList(context, pkgName);
        if (newPkgPermRecordList != null) {
            if (isUseInternet(newPkgPermRecordList)) {
                if (PermControlUtils.isCellularDataControlOn(context)) {
                    PackageManager pm = context.getPackageManager();
                    try {
                        ApplicationInfo info = pm
                                .getApplicationInfo(pkgName, 0);
                        PermControlUtils.setFirewallPolicy(
                                context, info.uid, 0, true);
                        PermControlUtils.updateCtaAppStatus(
                                info.uid, true);
                    } catch (NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                sDataBaseHelper.addCellularDatePerm(new CheckedPermRecord(
                            pkgName, R.string.cellular_data_message));
            }
        }
        return newPkgPermRecordList;
    }

    /**
     * Delete the specific package from database. Since it will write database,
     * call it in Asynchronous.
     * @param pkgName package name of apk
     */
    public static void delete(String pkgName) {
        if (pkgName != null) {
            sDataBaseHelper.deleteCellularDatePerm(pkgName);
        }
    }

    enum KEY_TYPE {
        Pkg_Key,
        Perm_Key
    }

    public static List<CheckedPermRecord> getCellularPerm() {
        List<CheckedPermRecord> list = new ArrayList<CheckedPermRecord>();
        Iterator<CheckedPermRecord> iterator = sPkgPermissionsCache.values().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }

    public static CheckedPermRecord getCellularDataRecord(String packageName) {
        CheckedPermRecord record = sPkgPermissionsCache.get(packageName);
        if (record != null) {
            return new CheckedPermRecord(record);
        } else {
            Log.e(TAG, "getCellularDataRecord can't get pgk = " + packageName);
        }
        return null;
    }

    public static boolean modifyCellularDatePerm(CheckedPermRecord info) {
        return sDataBaseHelper.modifyCellularDatePerm(info);
    }

    public static boolean isUseInternet(List<CheckedPermRecord> records) {
        for (CheckedPermRecord record : records) {
            String permission = record.mPermission;
            if (  permission.equals(Manifest.permission.CHANGE_NETWORK_STATE)
                ||permission.equals(Manifest.permission.INTERNET)
                ||permission.equals("android.permission.SEND_MMS")) {
                return true;
            }
        }
        return false;
    }
}
