/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.configurecheck2;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.nfc.NfcAdapter;
import android.os.AsyncResult;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.SystemService;
//IMEI
import android.telephony.TelephonyManager;

//DM
import android.os.ServiceManager;
//import com.mediatek.common.dm.DmAgent;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;

import com.mediatek.custom.CustomProperties;


/**
 *
 * @author mtk80202
 *      this is a demo for developer how to add new configure check item.
 *      please write your owner configure checker here by extends CheckItemBase
 */

class CheckIMEI extends CheckItemBase {

    private String mImei = null;
    CheckIMEI(Context c, String key) {
        super(c, key);
        /* No check + No auto configration */

        setTitle(R.string.imei_title);
        setNote(R.string.imei_note);
    }
    public String convertIMEI(String imei) {
        if (imei != null && !imei.equals("")){
            long temp = Long.parseLong(imei.trim());
            temp = temp + 111;
            String IMEI = Long.toString(temp);
            return IMEI;
        } else {
          return null;
        }
    }
    public boolean onCheck() {
        TelephonyManager tm = (TelephonyManager) getContext().getSystemService(
                Context.TELEPHONY_SERVICE);
        TelephonyManager tmEx = TelephonyManager.from(getContext());
        if (TelephonyManager.from(getContext()).getPhoneCount() > 1) {
            if (!getKey().equals(CheckItemKeySet.CI_IMEI_IN_DM)) {
                //if is Gemini phone, need to query two IMEI, except
                //CI_IMEI_IN_DM which only need to query the default IMEI
                String nextLine = System.getProperty("line.separator");
                String sim1IMEI = tmEx.getDeviceId(PhoneConstants.SIM_ID_1);
                String sim2IMEI = tmEx.getDeviceId(PhoneConstants.SIM_ID_2);
                StringBuilder sb = new StringBuilder(getContext().getString(R.string.imei_slot1));
                if (null == sim1IMEI) {
                    sb.append("null");
                } else {
                    sb.append(sim1IMEI);
                }
                sb.append(nextLine).append(getContext().getString(R.string.imei_slot2));
                if (null == sim2IMEI) {
                    sb.append("null");
                } else {
                    sb.append(sim2IMEI);
                }
                mImei = sb.toString();
                setValue(mImei);
                CTSCLog.e("CTSCIME", "sim1IME: " + convertIMEI(sim1IMEI));
                CTSCLog.e("CTSCIME", "sim2IME: " + convertIMEI(sim2IMEI));
                return true;
            }
        }
        mImei = tm.getDeviceId();
        CTSCLog.e("CTSCIME", "IME: " + convertIMEI(mImei));
        if (mImei == null) {
            setValue("null");
        } else {
            setValue(mImei);
        }
        return true;
    }
    public check_result getCheckResult() {
        
        mResult = super.getCheckResult();
        if (mImei == null && (check_result.UNKNOWN == mResult)) {
            mResult = check_result.WRONG;
        }
        return mResult;
    }
}

class CheckRoot extends CheckItemBase {

    private final String PROPERTY_RO_SECURE = "ro.secure";
    private String isUser;

    CheckRoot(Context c, String key) {
        super(c, key);

        setTitle(R.string.root_title);
        setNote(R.string.root_note);
        setProperty(PROPERTY_AUTO_CHECK);
    }

    public boolean onCheck() {
        isUser = SystemProperties.get(PROPERTY_RO_SECURE);
        CTSCLog.e("RootCheck", "Current state is:" + isUser);
        if (isUser.equals("1")) {
            SystemProperties.set(PROPERTY_RO_SECURE, "0");
            if (SystemProperties.get(PROPERTY_RO_SECURE).equals("0")) {
                CTSCLog.e("RootCheck", "Try to change state:" + SystemProperties.get(PROPERTY_RO_SECURE));
                SystemProperties.set(PROPERTY_RO_SECURE, "1");
                mResult = check_result.RIGHT;
                setValue(R.string.ctsc_yes);
            } else {
                mResult = check_result.WRONG;
                setValue(R.string.ctsc_no);
            }

        } else {
            mResult = check_result.WRONG;
            setValue(R.string.root_already);
        }
        return true;
    }

    public check_result getCheckResult() {

        return mResult;
    }

}
class CheckWIFISleepPolicy extends CheckItemBase {

    CheckWIFISleepPolicy(Context c, String key) {
        super(c, key);

        if (key.equals(CheckItemKeySet.CI_WIFI_NEVERKEEP_ONLYCHECK)) {
            setTitle(R.string.wifi_sleep_title);
            setNote(R.string.wifi_no_sleep_note);
            setProperty(PROPERTY_AUTO_CHECK);
        } else if (key.equals(CheckItemKeySet.CI_WIFI_NEVERKEEP)) {
            setTitle(R.string.wifi_sleep_title);
            setNote(R.string.wifi_no_sleep_note);
            setProperty(PROPERTY_AUTO_CHECK | PROPERTY_AUTO_CONFG);
        } else {
        setTitle(R.string.wifi_sleep_title);
        setNote(R.string.wifi_sleep_note);
        setProperty(PROPERTY_AUTO_CHECK | PROPERTY_AUTO_CONFG);
    }
    }

    public boolean onCheck() {

        try {
            int sleep = Settings.Global.getInt(getContext()
                    .getContentResolver(), Settings.Global.WIFI_SLEEP_POLICY);

            if (Settings.Global.WIFI_SLEEP_POLICY_NEVER == sleep) {
                setValue(R.string.wifi_sleep_no);
                mResult = getKey().equals(CheckItemKeySet.CI_WIFI_ALWAYKEEP) ?
                        check_result.RIGHT : check_result.WRONG;
            } else if (Settings.Global.WIFI_SLEEP_POLICY_DEFAULT == sleep) {
                setValue(R.string.wifi_sleep_yes);
                mResult = getKey().equals(CheckItemKeySet.CI_WIFI_ALWAYKEEP) ?
                        check_result.WRONG : check_result.RIGHT;
            } else {
                setValue(R.string.wifi_sleep_charge);
                mResult = check_result.WRONG;
            }

        } catch (SettingNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }

    public check_result getCheckResult() {

        return mResult;
    }

    @Override
    public boolean onReset() {

        if (getKey().equals(CheckItemKeySet.CI_WIFI_ALWAYKEEP)) {
        return Settings.Global.putInt(getContext().getContentResolver(),
                Settings.Global.WIFI_SLEEP_POLICY,
                Settings.Global.WIFI_SLEEP_POLICY_NEVER);
        } else {
            return Settings.Global.putInt(getContext().getContentResolver(),
                    Settings.Global.WIFI_SLEEP_POLICY,
                    Settings.Global.WIFI_SLEEP_POLICY_DEFAULT);
        }
    }
}


class CheckUA extends CheckItemBase {

    CheckUA(Context c, String key) {
        super(c, key);

        setNote(R.string.ua_note);
        setProperty(PROPERTY_CLEAR);
    }

    private String getUAString(String name, String type) {
        String str = null;
        try {
            str = CustomProperties.getString(name, type);
        } catch (java.lang.NoClassDefFoundError e) {
            e.printStackTrace();
        }

        CTSCLog.s("CheckUA", "name:" + name + "type:" + type + "str:" + str);

        return str;
    }

    public check_result getCheckResult() {

        check_result result = super.getCheckResult();
        String Value;

        if (getKey().equals(CheckItemKeySet.CI_UA_BROWSER)) {
            setValue(getUAString("browser", "UserAgent"));
            setTitle(R.string.browser_ua_title);
        } else if (getKey().equals(CheckItemKeySet.CI_UA_BROWSERURL)) {
            setValue(getUAString("browser", "UAProfileURL"));
            setTitle(R.string.browser_ua_url_title);
        } else if (getKey().equals(CheckItemKeySet.CI_UA_MMS)) {
            setValue(getUAString("mms", "UserAgent"));
            setTitle(R.string.mms_ua_title);
        } else if (getKey().equals(CheckItemKeySet.CI_UA_MMSURL)) {
            setValue(getUAString("mms", "UAProfileURL"));
            setTitle(R.string.mms_ua_url_title);
        } else if (getKey().equals(CheckItemKeySet.CI_UA_HTTP)) {
            setValue(getUAString("http_streaming", "UserAgent"));
            setTitle(R.string.http_ua_title);
        } else if (getKey().equals(CheckItemKeySet.CI_UA_HTTPURL)) {
            setValue(getUAString("http_streaming", "UAProfileURL"));
            setTitle(R.string.http_ua_url_title);
        } else if (getKey().equals(CheckItemKeySet.CI_UA_RTSP)) {
            setValue(getUAString("rtsp_streaming", "UserAgent"));
            setTitle(R.string.rtsp_ua_title);
        } else if (getKey().equals(CheckItemKeySet.CI_UA_RTSPURL)) {
            setValue(getUAString("rtsp_streaming", "UAProfileURL"));
            setTitle(R.string.rtsp_ua_url_title);
        } else if (getKey().equals(CheckItemKeySet.CI_UA_CMMB)) {
            setValue(getUAString("cmmb", "UserAgent"));
            setTitle(R.string.cmmb_ua_title);
        } else {
            setValue(R.string.ctsc_error);
            setTitle(R.string.ctsc_unknown);
        }

        return result;
    }

}

class CheckGprsMode extends CheckItemBase {
    private static String TAG = "CheckGprsMode";
    private static final int DATA_PREFER = 0;
    private static final int CALL_PREFER = 1;

    boolean mCallPref;

    CheckGprsMode(Context c, String key) {
        super(c, key);

        setTitle(R.string.gprs_mode_title);
        if (key.equals(CheckItemKeySet.CI_GPRS_CALL_PREF)) {
            setNote(R.string.gprs_call_pref_note);
            setProperty(PROPERTY_AUTO_CHECK);
        } else {
            setNote(R.string.gprs_data_pref_note);
        setProperty(PROPERTY_AUTO_CHECK | PROPERTY_AUTO_CONFG);
    }
    }

    public boolean onCheck() {
        int mode = SystemProperties.getInt("persist.radio.gprs.prefer", 0);
        if (0 == mode) {
            mCallPref = true;
            setValue(R.string.gprs_call_preferred);
        } else {
            mCallPref = false;
            setValue(R.string.gprs_data_preferred);
        }

        if (getKey().equals(CheckItemKeySet.CI_GPRS_CALL_PREF)) {
            mResult = mCallPref ? check_result.RIGHT : check_result.WRONG;
        } else {
            mResult = mCallPref ? check_result.WRONG : check_result.RIGHT;
        }
        return true;
    }

    public check_result getCheckResult() {

        return mResult;
    }
    private void setGprsTransferType(int type) {
        String property = (type == DATA_PREFER? "1" : "0");
        CTSCLog.i(TAG, "Change persist.radio.gprs.prefer to " + property);
        SystemProperties.set("persist.radio.gprs.prefer", property);
        for (int i = 0 ; i < TelephonyManager.getDefault().getPhoneCount(); i++) {
            Phone phone = PhoneFactory.getPhone(i);

            if (phone != null) {
                phone.invokeOemRilRequestStrings(new String[] {"AT+EGTP=" + type, ""}, null);
                phone.invokeOemRilRequestStrings(new String[] {"AT+EMPPCH=" + type, ""}, null);
            }
        }
    }

    @Override
    public boolean onReset() {
        if (getKey().equals(CheckItemKeySet.CI_GPRS_CALL_PREF)) {
            setGprsTransferType(CALL_PREFER);
        } else {
            setGprsTransferType(DATA_PREFER);
        }
        return true;

    }
}


class CheckMTKLogger extends CheckItemBase {

    CheckMTKLogger(Context c, String key) {
        super(c, key);

        setProperty(PROPERTY_AUTO_FWD);

        if (key.equals(CheckItemKeySet.CI_LOGGER_ON)) {
            setTitle(R.string.logger_on_title);
            setNote(R.string.logger_on_note);
        } else if (key.equals(CheckItemKeySet.CI_LOGGER_OFF)) {
            setTitle(R.string.logger_off_title);
            setNote(R.string.logger_off_note);
        } else if (key.equals(CheckItemKeySet.CI_LOGGER_OFF_LTE)) {
            setTitle(R.string.logger_off_title);
            setNote(getContext().getString(R.string.mtklog_off_note));
        } else if (key.equals(CheckItemKeySet.CI_TAGLOG_OFF)) {
            setTitle(R.string.taglog_off_title);
            setNote(R.string.taglog_off_note);
        } else {
            throw new IllegalArgumentException("Error key = " + key);
        }
    }

    public Intent getForwardIntent() {

        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.mediatek.mtklogger", "com.mediatek.mtklogger.MainActivity"));
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        if (getContext().getPackageManager().resolveActivity(intent, 0) != null) {
            return intent;
        } else {
            throw new RuntimeException(
                    "Can't find such activity: com.mediatek.mtklogger, " +
                    "com.mediatek.mtklogger.MainActivity");
        }
    }
}

class CheckCTIA extends CheckItemBase {

    CheckCTIA(Context c, String key) {
        super(c, key);

        setProperty(PROPERTY_AUTO_FWD);
        setTitle(R.string.ctia_title);
        setNote(R.string.ctia_note);
    }


    public Intent getForwardIntent() {

        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.mediatek.engineermode",
                "com.mediatek.engineermode.wifi.WifiTestSetting"));

        if (getContext().getPackageManager().resolveActivity(intent, 0) != null) {
            return intent;
        } else {
            throw new RuntimeException(
                    "Can't find such activity: com.mediatek.engineermode, " +
                    "com.mediatek.engineermode.wifi.WifiTestSetting");
        }
    }
}

class CheckSN extends CheckItemBase {

    CheckSN(Context c, String key) {
        super(c, key);
        setTitle(R.string.sn_title);
        setNote(R.string.sn_note);
    }

    public boolean onCheck() {

        StringBuilder sResult = new StringBuilder("");
        InputStream inputstream = null;
        BufferedReader bufferedreader = null;

        try {
            Process proc = Runtime.getRuntime().exec("cat /sys/class/android_usb/android0/iSerial");
            inputstream = proc.getInputStream();
            InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
            bufferedreader = new BufferedReader(inputstreamreader);

            if (proc.waitFor() == 0) {
                String line;
                while ((line = bufferedreader.readLine()) != null) {
                    CTSCLog.s("SN", "read line " + line);
                    sResult.append(line);
                }
            } else {
                CTSCLog.s("SN", "exit value = " + proc.exitValue() + "|| get:sb-- " + sResult);
            }

        } catch (InterruptedException e) {
            CTSCLog.s("SN", "exe fail " + e.toString() + "|| get:sb-- " + sResult);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
             if (null != bufferedreader) {
                 try {
                     bufferedreader.close();
                 } catch (IOException e) {
                    CTSCLog.w("SN", "close IOException: " + e.getMessage());
                }
            }
        }

        setValue(sResult.toString());
        return true;
    }

    public check_result getCheckResult() {
        return super.getCheckResult();
    }
}

class CheckATCI extends CheckItemBase {
    private static final String TAG = "CheckATCI";
    CheckATCI(Context c, String key) {
        super(c, key);
        setTitle(R.string.atci_title);
        setNote(R.string.atci_note);
        setProperty(PROPERTY_CLEAR);
    }
}

class CheckAtciInEM extends CheckItemBase {
    private static final String TAG = "CheckAtciInEM";
    private static final String ATCI_USERMODE = "persist.service.atci.usermode";
    private static final String RADIO_PORT_INDEX = "persist.radio.port_index";
    private static final String ATCI_AUTO_START = "persist.service.atci.autostart";
    private boolean mIsreseted = false;
    CheckAtciInEM(Context c, String key) {
        super(c, key);
        setProperty(PROPERTY_AUTO_CHECK | PROPERTY_AUTO_CONFG);
        setTitle(R.string.atci_in_EM_title);
        setNote(R.string.atci_in_EM_note);
    }

    public boolean onCheck() {
        boolean is_atci_running = SystemService.isRunning("atci_service");
        //boolean is_atcid_running = SystemService.isRunning("atcid-daemon-u");
        String is_port_on = SystemProperties.get(RADIO_PORT_INDEX, "0");
        CTSCLog.i(TAG, "onCheck(),is_atci_running = " + is_atci_running);
        if (is_atci_running && is_port_on.equals("1")) {
            setValue(R.string.ctsc_enabled);
            mResult = check_result.RIGHT;
        } else {
            setValue(R.string.ctsc_disabled);
            mResult = check_result.WRONG;
        }
        CTSCLog.i(TAG, "onCheck(),mResult = " + mResult);
        return true;
    }

    public boolean onReset() {
        CTSCLog.i(TAG, "onReset");
        /*
         * ATCI need turn on port and start two native atci service in user load
         * but only need turn on port in eng load.
         * ATCI service in native on eng load is : "atcid-daemon-e" and
         *  "atci_service"
         * on user load is:"atcid-daemon-u" and
         *  "atci_service"
         */
        SystemProperties.set(ATCI_USERMODE, "1");
        SystemProperties.set(RADIO_PORT_INDEX, "1");
        String type = SystemProperties.get(Utils.RO_BUILD_TYPE, "unknown");
        if(!type.equals("eng")) {
            SystemProperties.set(ATCI_AUTO_START, "1");
            CTSCLog.v(TAG, "start atcid-daemon-u");
            SystemProperties.set("ctl.start", "atcid-daemon-u");
            CTSCLog.v(TAG, "start atci_service");
            SystemProperties.set("ctl.start", "atci_service");
            CTSCLog.i(TAG, "com.mediatek.atci.service.startup");
        }
        Intent intent = new Intent("com.mediatek.atci.service.startup");
        getContext().sendBroadcast(intent);
        mResult = check_result.RIGHT;
        return true;
    }
}

class CheckBattery extends CheckItemBase {

    IntentFilter mIF;
    int mLevel = -1;

    private BroadcastReceiver mIR = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action  = intent.getAction();
            if (action != null && action.equals(Intent.ACTION_BATTERY_CHANGED)) {
                int newlevel = intent.getIntExtra("level", 200);
                if (mLevel != newlevel) {
                    mLevel = newlevel;
                    sendBroadcast();
                } else {
                    CTSCLog.i("checkBattery", "no change, return");
                    return;
                }
                CTSCLog.i("checkBattery", "level = " + mLevel);
                setValue(String.valueOf(mLevel) + "%");
            }
        }

    };

    //public void finalize() {
    //    getContext().unregisterReceiver(mIR);
    //}

    CheckBattery(Context c, String key) {
        super(c, key);

        mIF = new IntentFilter();
        mIF.addAction(Intent.ACTION_BATTERY_CHANGED);
        getContext().registerReceiver(mIR, mIF);

        setProperty(PROPERTY_AUTO_CHECK);
        setTitle(R.string.battery_title);
        setNote(R.string.battery_note);
    }

    public check_result getCheckResult() {

         if (mLevel < 0) {
            setValue(R.string.ctsc_querying);
            return check_result.UNKNOWN;
         } else if (mLevel < 90) {
             return check_result.WRONG;
         } else {
             return check_result.RIGHT;
         }
    }

}

class CheckBuildType extends CheckItemBase {

    private final String PROPERTY_RO_BUILD_TYPE = "ro.build.type";

    CheckBuildType(Context c, String key) {
        super(c, key);
        setTitle(R.string.buildtype_title);
        setNote(R.string.buildtype_note);
        setProperty(PROPERTY_AUTO_CHECK);
    }

    public boolean onCheck() {
        String type = SystemProperties.get(PROPERTY_RO_BUILD_TYPE);
        setValue(type + " mode");
        mResult = type.equals("user") ? check_result.RIGHT : check_result.WRONG;
        return true;
    }

    public check_result getCheckResult() {
        return mResult;
    }
}

class CheckTargetMode extends CheckItemBase {
    CheckTargetMode(Context c, String key) {
        super(c, key);
        setTitle(R.string.targetmode_title);
        setNote(R.string.targetmode_note);
    }

    public boolean onCheck() {
        Uri customerServiceUri = Uri.parse("content://com.mediatek.cmcc.provider/phoneinfo");
        StringBuilder sb = new StringBuilder("");
        try {
            Cursor cursor = getContext().getContentResolver().query(customerServiceUri, null, null, null, null);
            while (cursor.moveToNext()) {
                sb.append(cursor.getString(cursor.getColumnIndex("phone_model")));
            }
            cursor.close();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        setValue(sb.toString());
        return true;
    }
}

class CheckTargetVersion extends CheckItemBase {

    private final String PROPERTY_RO_BUILD_VERSION = "ro.build.version.release";

    CheckTargetVersion(Context c, String key) {
        super(c, key);
        setTitle(R.string.targetversion_title);
        setNote(R.string.targetversion_note);
    }

    public boolean onCheck() {
        setValue(SystemProperties.get(PROPERTY_RO_BUILD_VERSION));
        return true;
    }
}

class CheckVendorApk extends CheckItemBase {

    CheckVendorApk(Context c, String key) {
        super(c, key);
        setTitle(R.string.vendor_title);
        setNote(R.string.vendor_apk_note);
    }

//    final private String[] mApkArray = {
//        "MobileMarket",
//        "MobileVideo",
//        "CMCCMobileMusic",
//        "PIMClient",
//        "GameHall",
//        "WLANClient"
//    };    
//    
//    CheckVendorApk(Context c, String key) {
//        super(c, key);
//        setTitle(R.string.vendor_title);
//        setNote(R.string.vendor_note);
//        setProperty(PROPERTY_AUTO_CHECK);
//    }
//    
//    public boolean onCheck() {
//        int size = mApkArray.length;
//        
//        setValue(R.string.vendor_value_normal);
//        mResult = check_result.RIGHT;
//        CTSCLog.i("CheckVendor", String.valueOf(size));
//        for (int i = 0; i < size; i++) {
//            if (!apkExist(mApkArray[i])) {
//                mResult = check_result.WRONG;
//                setValue(R.string.vendor_value_abnormal);
//                break;
//            }
//        }
//                    
//        return true;
//    }
//    
//    private boolean apkExist(String apkName) {
//        StringBuilder apkDir = new StringBuilder("system/vendor/operator/app/");
//        apkDir.append(apkName);
//        apkDir.append(".apk");
//        
//        CTSCLog.i("CheckVendor", "apkExist:" + apkDir.toString());
//        File apkFile = new File(apkDir.toString());
//        if (!apkFile.exists()) {
//            return false;
//        }        
//        return true;
//    }
}

class CheckFR extends CheckItemBase {

    CheckFR(Context c, String key) {
        super(c, key);
        setTitle(R.string.rf_title);
        setNote(R.string.rf_note);
    }
}

class CheckBaseFunc extends CheckItemBase {

    CheckBaseFunc(Context c, String key) {
        super(c, key);
        setTitle(R.string.basefunc_title);
        setNote(R.string.basefunc_note);
    }
}

class CheckRestore extends CheckItemBase {
    CheckRestore(Context c, String key) {
        super(c, key);
        setTitle(R.string.restore_title);
        setNote(R.string.restore_note);
    }
}

//Yongmao
class CheckCalData extends CheckItemBase {

    CheckCalData(Context c, String key) {
        super(c, key);
        setTitle(R.string.title_calibration_data);
        setNote(R.string.note_calibration_data);
        setProperty(PROPERTY_CLEAR);
    }
}

class CheckApkExist extends CheckItemBase {

    CheckApkExist(Context c, String key) {
        super(c, key);
        setProperty(PROPERTY_CLEAR);

        if (key.equals(CheckItemKeySet.CI_APK_CALCULATOR)) {
            setTitle(R.string.title_apk_calculator);
        } else if (key.equals(CheckItemKeySet.CI_APK_STOPWATCH)) {
            setTitle(R.string.title_apk_stopwatch);
        } else if (key.equals(CheckItemKeySet.CI_APK_NOTEBOOK)) {
            setTitle(R.string.title_apk_notebook);
        } else if (key.equals(CheckItemKeySet.CI_APK_BACKUP)) {
            setTitle(R.string.title_apk_backup);
        } else if (key.equals(CheckItemKeySet.CI_APK_CALENDAR)) {
            setTitle(R.string.title_apk_calendar);
        } else if (key.equals(CheckItemKeySet.CI_APK_OFFICE)) {
            setTitle(R.string.title_apk_office);
            setNote(R.string.note_apk_office);
        } else {
            throw new IllegalArgumentException("Error key = " + key);
        }
    }

    public check_result getCheckResult() {
        CTSCLog.i("CheckApkExist", "CheckApkExist");

        check_result result = super.getCheckResult();
        if (getKey().equals(CheckItemKeySet.CI_APK_OFFICE)) {
            return result;
        }
        if (check_result.UNKNOWN == result) {
            if (getKey().equals(CheckItemKeySet.CI_APK_CALCULATOR)) {
                if (apkExist(R.string.apk_calculator_name)) {
                    setProperty(PROPERTY_AUTO_CHECK);
                    setNote(R.string.note_apk_calculator_exist);
                    setValue(R.string.value_apk_calculator_exist);
                    result = check_result.RIGHT;
                } else {
                    setNote(R.string.note_mtk_calculator_not_exist);
                }
            } else if (getKey().equals(CheckItemKeySet.CI_APK_STOPWATCH)) {
                boolean isJB2 = (Build.VERSION.SDK_INT >= 17);
                if (isJB2) {
                    if (apkExist(R.string.apk_deskclock_name)) {
                        setProperty(PROPERTY_AUTO_CHECK);
                        setNote(R.string.note_apk_stopwatch_exist);
                        setValue(R.string.value_apk_deskclock_exist);
                        result = check_result.RIGHT;
                    } else {
                        setNote(R.string.note_deskclock_not_exist);
                    }
                } else {
                    if (apkExist(R.string.apk_stopwatch_name)) {
                        setProperty(PROPERTY_AUTO_CHECK);
                        setNote(R.string.note_apk_stopwatch_exist);
                        setValue(R.string.value_apk_stopwatch_exist);
                        result = check_result.RIGHT;
                    } else {
                        setNote(R.string.note_mtk_stopwatch_not_exist);
                    }
                }
            } else if (getKey().equals(CheckItemKeySet.CI_APK_NOTEBOOK)) {
                if (apkExist(R.string.apk_notebook_name)) {
                    setProperty(PROPERTY_AUTO_CHECK);
                    setNote(R.string.note_apk_notebook_exist);
                    setValue(R.string.value_apk_notebook_exist);
                    result = check_result.RIGHT;
                } else {
                    setNote(R.string.note_mtk_notebook_not_exist);
                }
            } else if (getKey().equals(CheckItemKeySet.CI_APK_BACKUP)) {
                if (apkExist(R.string.apk_backup_name)) {
                    setProperty(PROPERTY_AUTO_CHECK);
                    setNote(R.string.note_apk_backup_exist);
                    setValue(R.string.value_apk_backup_exist);
                    result = check_result.RIGHT;
                } else {
                    setNote(R.string.note_mtk_backup_not_exist);
                }
            } else if (getKey().equals(CheckItemKeySet.CI_APK_CALENDAR)) {
                if (apkExist(R.string.apk_calendar_name)) {
                    setProperty(PROPERTY_AUTO_CHECK);
                    setNote(R.string.note_apk_calendar_exist);
                    setValue(R.string.value_apk_calendar_exist);
                    result = check_result.RIGHT;
                } else {
                    setNote(R.string.note_calendar_not_exist);
                }
            } else {
                throw new IllegalArgumentException("Error key = " + getKey());
            }
        } else {
            if (getKey().equals(CheckItemKeySet.CI_APK_CALCULATOR)) {
                setNote(R.string.note_apk_calculator_exist);
            } else if (getKey().equals(CheckItemKeySet.CI_APK_STOPWATCH)) {
                setNote(R.string.note_apk_stopwatch_exist);
            } else if (getKey().equals(CheckItemKeySet.CI_APK_NOTEBOOK)) {
                setNote(R.string.note_apk_notebook_exist);
            } else if (getKey().equals(CheckItemKeySet.CI_APK_BACKUP)) {
                setNote(R.string.note_apk_backup_exist);
            } else if (getKey().equals(CheckItemKeySet.CI_APK_CALENDAR)) {
                setNote(R.string.note_apk_calendar_exist);
            } else {
                throw new IllegalArgumentException("Error key = " + getKey());
            }
        }

        return result;
    }

    private boolean apkExist(int apkNameId) {
        StringBuilder apkDir = new StringBuilder("system/app/");
        apkDir.append(getContext().getString(apkNameId));
        apkDir.append(".apk");

        File apkFile = new File(apkDir.toString());
        if (!apkFile.exists()) {
            return false;
        }

        return true;
    }
}

class Check24hChargeProtect extends CheckItemBase {

    Check24hChargeProtect(Context c, String key) {
        super(c, key);
        setTitle(R.string.title_24h_protect);
        setNote(R.string.note_24h_disable);
        setProperty(PROPERTY_CLEAR);
    }
}

class CheckHSUPA extends CheckItemBase {
    private static final String TAG = "CheckHSUPA";
    private boolean mAsyncDone = true;
    private boolean mNeedNofity = false;
    
    private static final int EVENT_HSPA_INFO = 1;
    private static final String QUERY_CMD = "AT+EHSM?";
    private static final String RESPONSE_CMD = "+EHSM: ";

    private Phone mPhone = null;
    
    private static final int MODE_HSUPA_OFF_1 = 0;
    private static final int MODE_HSUPA_OFF_2 = 1;
    private static final int MODE_HSUPA_ON_1 = 2;
    private static final int MODE_HSUPA_ON_2 = 3;

    private Handler mATCmdHander = new Handler() {
        public void handleMessage(Message msg) {
            CTSCLog.d(TAG, "Receive msg from HSUPA info query");
            if (msg.what == EVENT_HSPA_INFO) {
                mAsyncDone = true;
                AsyncResult ar = (AsyncResult) msg.obj;
                if (ar != null && ar.exception == null) {
                    handleQuery((String[]) ar.result);
                } else {
                    setValue("Query failed");
                }
            }
            
            if (mNeedNofity) {
                sendBroadcast();
            }
        }
    };
    
    private void handleQuery(String[] result) {
        if (null == result || result.length <= 0) {
            setValue("Query failed");
            return;
        }
        
        CTSCLog.d(TAG, "Modem return: " + result[0]);
        String[] mode = result[0].substring(RESPONSE_CMD.length(), result[0].length()).split(",");
        if (null == mode || mode.length <= 0) {
            setValue("Query failed");
            return;
        }
        
        int modeInt = -1;
        try {
            modeInt = Integer.parseInt(mode[0]);
        } catch (NumberFormatException e) {
            CTSCLog.d(TAG, "Modem return invalid mode: " + mode[0]);
            return;
        }
        switch (modeInt) {
        case MODE_HSUPA_OFF_1:
        case MODE_HSUPA_OFF_2:
            setValue(R.string.value_hsupa_off);
            mResult = check_result.WRONG;
            break;
            
        case MODE_HSUPA_ON_1:
        case MODE_HSUPA_ON_2:
            setValue(R.string.value_hsupa_on);
            mResult = check_result.RIGHT;
            break;

        default:
            setValue("Query failed");
            break;
        }

    }

    CheckHSUPA(Context c, String key) {
        super(c, key);
        setTitle(R.string.hsupa_title);
        setNote(R.string.hsupa_note);
        setProperty(PROPERTY_AUTO_CHECK); 
    }

    public boolean onCheck() {
        getHSUPAInfo();
        return true;
    }
   
    public check_result getCheckResult() {
        if (!mAsyncDone) {
            mResult = check_result.UNKNOWN;
            mNeedNofity = true;
            setValue(R.string.ctsc_querying);
        } else {
            mNeedNofity = false;
        }
        CTSCLog.d(TAG, "getCheckResult mResult = " + mResult);       
        return mResult;       
    }
   
    private void getHSUPAInfo() {
        CTSCLog.i(TAG, "getHSUPAInfo");
        mAsyncDone = false;
        String[] cmd = new String[2];
        cmd[0] = QUERY_CMD;
        cmd[1] = RESPONSE_CMD;
        if (TelephonyManager.getDefault().getPhoneCount() > 1) {
            mPhone = PhoneFactory.getPhone(PhoneConstants.SIM_ID_1);
            mPhone.invokeOemRilRequestStrings(cmd,
                    mATCmdHander.obtainMessage(EVENT_HSPA_INFO));
        } else {
            mPhone = (Phone) PhoneFactory.getDefaultPhone();
            mPhone.invokeOemRilRequestStrings(cmd, mATCmdHander.obtainMessage(EVENT_HSPA_INFO));
        }
    }
//
//    public boolean onCheck() {
//        getHSUPAInfo();
//        return true;
//    }
//
//    public check_result getCheckResult() {
//        if (!mAsyncDone) {
//            mResult = check_result.UNKNOWN;
//            mNeedNofity = true;
//            setValue(R.string.ctsc_querying);
//        } else {
//            mNeedNofity = false;
//        }
//        CTSCLog.d(TAG, "getCheckResult mResult = " + mResult);
//        return mResult;
//    }
//
//    private void getHSUPAInfo() {
//        CTSCLog.i(TAG, "getHSUPAInfo");
//        mAsyncDone = false;
//        String[] cmd = new String[2];
//        cmd[0] = QUERY_CMD;
//        cmd[1] = RESPONSE_CMD;
//        if (PhoneConstants.GEMINI_SIM_NUM > 1) {
//            mGeminiPhone = (GeminiPhone) PhoneFactory.getDefaultPhone();
//            mGeminiPhone.invokeOemRilRequestStringsGemini(cmd,
//                    mATCmdHander.obtainMessage(EVENT_HSPA_INFO), PhoneConstants.GEMINI_SIM_1);
//        } else {
//            mPhone = (Phone) PhoneFactory.getDefaultPhone();
//            mPhone.invokeOemRilRequestStrings(cmd, mATCmdHander.obtainMessage(EVENT_HSPA_INFO));
//        }
//    }
}

class CheckWlanSSID extends CheckItemBase {

    CheckWlanSSID(Context c, String key) {
        super(c, key);
        setTitle(R.string.title_wlan_ssid);
        setNote(R.string.note_wlan_ssid);
        setProperty(PROPERTY_CLEAR);
    }

    public check_result getCheckResult() {
        check_result result = super.getCheckResult();

        WifiManager wifi = (WifiManager) getContext().getSystemService(Context.WIFI_SERVICE);
        WifiConfiguration config = wifi.getWifiApConfiguration();
        String ssid = config == null ? null : config.SSID;

        if (ssid == null && !wifi.isWifiEnabled()) {
            setValue(R.string.value_please_open_wlan);
        } else {
            setValue(ssid);
        }

        return result;
    }
}

class CheckWlanMacAddr extends CheckItemBase {

    CheckWlanMacAddr(Context c, String key) {
        super(c, key);
        setTitle(R.string.title_wlan_mac_addr);
        setNote(R.string.note_wlan_mac_addr);
        setProperty(PROPERTY_CLEAR);
    }

    public check_result getCheckResult() {
        check_result result = super.getCheckResult();

        WifiManager wifi = (WifiManager) getContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifi.getConnectionInfo();
        String macAddress = wifiInfo == null ? null : wifiInfo.getMacAddress();

        if (macAddress == null && !wifi.isWifiEnabled()) {
            setValue(R.string.value_please_open_wlan);
        } else {
            setValue(macAddress);
        }

        return result;
    }
}

class CheckScreen extends CheckItemBase {
    private static final String TAG = "CheckScreen";
    private PowerManager mPm = null;
    private AlertDialog mAlertDialog = null;
    
    CheckScreen(Context c, String key) {
        super(c, key);
        
        setProperty(PROPERTY_AUTO_CHECK | PROPERTY_AUTO_CONFG);
        setTitle(R.string.Screen_title);
        if (getKey().equals(CheckItemKeySet.CI_SCREEN_OFF)) {
            setNote(getContext().getString(R.string.Screen_off_note));
        } else {
            throw new IllegalArgumentException("Error key = " + key);
        }
        mPm = (PowerManager)getContext().getSystemService(Context.POWER_SERVICE);
    }

    public boolean onCheck() {
        CTSCLog.d(TAG, "oncheck");
        mResult = check_result.WRONG;
        if (mPm.isScreenOn()) {
            setValue(R.string.Screen_on);
        } else {
            setValue(R.string.Screen_off);
            if (getKey().equals(CheckItemKeySet.CI_SCREEN_OFF)) {
                mResult = check_result.RIGHT;
            }
        }
        return true;
    }
    
    public boolean onReset() {
        CTSCLog.i(TAG, "onReset");
        screenOffAlert(5000, 1000);
        
        new Handler().postDelayed(new Runnable() {
            public void run() {
                CTSCLog.d(TAG, "Turn screen off");
                mPm.goToSleep(SystemClock.uptimeMillis());
            }
        }, 5000);
        
        return true;
    }
    
    private void screenOffAlert(long millisUntilFinished, long countDownInterval) {
        if (null == mAlertDialog) {
            mAlertDialog = new AlertDialog.Builder(getContext()).create();
        }
        mAlertDialog.setTitle("Screen off");
        mAlertDialog.setMessage("Wait");
        mAlertDialog.setCanceledOnTouchOutside(false);
        mAlertDialog.setCancelable(false);
        mAlertDialog.show();

        new CountDownTimer(millisUntilFinished, countDownInterval) {
            @Override
            public void onTick(long millisUntilFinished) {
               mAlertDialog.setMessage("Wait " + (millisUntilFinished/1000) + " seconds");
            }

            @Override
            public void onFinish() {
                mAlertDialog.cancel();
            }
        }.start();
    }
}

class CheckNFC extends CheckItemBase {
    
    private static final String TAG = "CheckNFC";
    private NfcAdapter mNfcAdapter;
    private final IntentFilter mIntentFilter;
    
    /**
     * The broadcast receiver is used to handle the nfc adapter state changed
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (NfcAdapter.ACTION_ADAPTER_STATE_CHANGED.equals(action)) {
                sendBroadcast();
            }
        }
    };
    
    CheckNFC(Context c, String key) {
        super(c, key);
        
        setProperty(PROPERTY_AUTO_CHECK | PROPERTY_AUTO_CONFG);
        setTitle(R.string.NFC_title);
        if (getKey().equals(CheckItemKeySet.CI_NFC_OFF)) {
            setNote(getContext().getString(R.string.NFC_off_note));
        } else {
            throw new IllegalArgumentException("Error key = " + key);
        }
        
        mNfcAdapter = NfcAdapter.getDefaultAdapter(getContext());
        mIntentFilter = new IntentFilter(NfcAdapter.ACTION_ADAPTER_STATE_CHANGED);
        getContext().registerReceiver(mReceiver, mIntentFilter);
    }
    
    public check_result getCheckResult() {
        if (mNfcAdapter == null) {
            CTSCLog.d(TAG, "Nfc Adapter is null");
            setValue(R.string.NFC_not_support);
            setProperty(PROPERTY_AUTO_CHECK);
            return check_result.UNKNOWN;
        }
        
        switch (mNfcAdapter.getAdapterState()) {
        case NfcAdapter.STATE_OFF:
            setValue(R.string.ctsc_disabled);
            if (getKey().equals(CheckItemKeySet.CI_NFC_OFF)) {
                mResult = check_result.RIGHT;
            }
            break;
        case NfcAdapter.STATE_ON:
            setValue(R.string.ctsc_enabled);
            if (getKey().equals(CheckItemKeySet.CI_NFC_OFF)) {
                mResult = check_result.WRONG;
            }
            break;
        case NfcAdapter.STATE_TURNING_ON:
        case NfcAdapter.STATE_TURNING_OFF:
            setValue(R.string.ctsc_querying);
            mResult = check_result.UNKNOWN;
            break;
        default:
            setValue(R.string.ctsc_error);
            mResult = check_result.UNKNOWN;
            break;
        }
        
        return mResult;
    }
    
    public boolean onReset() {
        CTSCLog.i(TAG, "onReset");
        if (mNfcAdapter != null) {
            mNfcAdapter.disable();
        }
        return true;
    }
}
