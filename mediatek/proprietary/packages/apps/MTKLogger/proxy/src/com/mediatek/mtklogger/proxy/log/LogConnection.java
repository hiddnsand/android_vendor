package com.mediatek.mtklogger.proxy.log;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;

import com.mediatek.mtklogger.proxy.utils.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Class which will communicate with native layer. Send command to native by
 * local socket, then monitor response code, and feed that back to user
 */
public class LogConnection {
    private static final String TAG = Utils.TAG + "/LogConnection";

    private ISocketConnectionListen mSocketConnectionListen;
    private Thread mListenThread = null;

    LocalSocket mSocket;
    LocalSocketAddress mAddress;

    private OutputStream mOutputStream;
    private InputStream mInputStream;
    /**
     * Get at most this number of byte from socket. Since modem log may return a
     * log folder path through socket, this response maybe a long string
     */
    private static final int BUFFER_SIZE = 1024;

    /**
     * Flag for whether local monitor thread should keep running.
     */
    private boolean mShouldStop = false;

    /**
     * Constructor with default socket name space.
     *
     * @param sockname
     *            String
     * @param socketConnectionListen ISocketConnectionListen
     */
    public LogConnection(String sockname, ISocketConnectionListen socketConnectionListen) {
        this(sockname, LocalSocketAddress.Namespace.ABSTRACT, socketConnectionListen);
    }

    /**
     * @param sockname
     *            String
     * @param nameSpace
     *            Namespace
     * @param socketConnectionListen ISocketConnectionListen
     */
    public LogConnection(String sockname, LocalSocketAddress.Namespace nameSpace,
            ISocketConnectionListen socketConnectionListen) {
        this.mSocketConnectionListen = socketConnectionListen;
        mSocket = new LocalSocket();
        mAddress = new LocalSocketAddress(sockname, nameSpace);
    }

    /**
     * @return boolean
     */
    public boolean connect() {
        Utils.logd(TAG, "-->connect(), socketName=" + mAddress.getName());
        if (mSocket == null) {
            Utils.logw(TAG, "-->connect(), mSocket = null, just return.");
            return false;
        }
        try {
            mSocket.connect(mAddress);
            mOutputStream = mSocket.getOutputStream();
            mInputStream = mSocket.getInputStream();
        } catch (IOException ex) {
            Utils.logw(TAG, "Communications error,"
                    + " Exception happens when connect to socket server");
            stop();
            return false;
        }
        mListenThread = new Thread() {
            public void run() {
                listen();
            }
        };
        mListenThread.start();

        Utils.logd(TAG, "Connect to native socket OK. And start local monitor thread now");
        return true;
    }

    /**
     * @return boolean
     */
    public boolean isConnected() {
        boolean isConnectedNow = (mSocket != null && mSocket.isConnected());
        return isConnectedNow;
    }

    /**
     * @param cmd String
     * @return boolean
     */
    public boolean sendCmd(String cmd) {
        Utils.logd(TAG, "-->send cmd: [" + cmd + "] to [" + mAddress.getName() + "]");
        boolean success = false;
        synchronized (this) {
            if (mOutputStream == null) {
                Utils.loge(TAG, "No connection to daemon, outputstream is null.");
                stop();
            } else {
                StringBuilder builder = new StringBuilder(cmd);
                builder.append('\0');
                Utils.logd(TAG, "Command builder success");
                try {
                    mOutputStream.write(builder.toString().getBytes());
                    mOutputStream.flush();
                    success = true;
                } catch (IOException ex) {
                    Utils.loge(TAG, "IOException while sending command to native.", ex);
                    mOutputStream = null;
                    stop();
                }
            }
        }
        Utils.logi(TAG, "<--send cmd done : [" + cmd + "] to [" + mAddress.getName() + "]");
        return success;
    }

    /**
     * void return.
     */
    public void listen() {
        int count;
        byte[] buffer = new byte[BUFFER_SIZE];

        Utils.logd(TAG, "Monitor thread running");
        while (!mShouldStop/* true */) {
            try {
                count = mInputStream.read(buffer, 0, BUFFER_SIZE);
                if (count < 0) {
                    Utils.loge(TAG, "Get a empty response from native layer, stop listen.");
                    break;
                }
                Utils.logv(TAG, "Response from native byte size=" + count);
                byte[] resp = new byte[count];
                System.arraycopy(buffer, 0, resp, 0, count);
                mSocketConnectionListen.socketResponse(new String(resp));
            } catch (IOException ex) {
                Utils.loge(TAG, "read failed", ex);
                break;
            }
        }
        return;
    }

    /**
     * return void.
     */
    public void stop() {
        Utils.logi(TAG, "-->stop()");
        mShouldStop = true;

        if (mSocket == null) {
            return;
        }

        try {
            mSocket.shutdownInput();
            mSocket.shutdownOutput();
            mSocket.close();
        } catch (IOException e) {
            Utils.loge(TAG, "Exception happended while closing socket: " + e);
        }
        mListenThread = null;
        mSocket = null;
    }

    public LocalSocket getSocket() {
        return this.mSocket;
    }
}
