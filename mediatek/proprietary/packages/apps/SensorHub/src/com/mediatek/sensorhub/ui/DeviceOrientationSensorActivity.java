package com.mediatek.sensorhub.ui;

import java.util.Arrays;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class DeviceOrientationSensorActivity extends CustomerSensorBaseActivity {

    public DeviceOrientationSensorActivity() {
        super(Utils.KEY_DEVICE_ORIENTATION_STATUS, Sensor.TYPE_DEVICE_ORIENTATION);
    }

    @Override
    public void onSensorChanged(float[] value) {
        mReceiveDataTimes++;
        int[] values = new int[value.length];
        for (int i = 0; i < value.length; i++) {
            values[i] = (int) value[i];
        }
        mSensorSwitch.setSummary(Arrays.toString(values) + getString(R.string.receive_string)
                + mReceiveDataTimes);
    }
}
