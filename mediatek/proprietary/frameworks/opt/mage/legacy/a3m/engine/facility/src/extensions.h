/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Obtains available OpenGL extensions.
 *
 */

#pragma once
#ifndef A3M_EXTENSIONS_H
#define A3M_EXTENSIONS_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <GLES2/gl2.h>                /* required by gl2ext.h                */
#include <GLES2/gl2ext.h>             /* for function signitures             */
#include <a3m/base_types.h>           /* for A3M_BOOL                        */

#define glGetProgramBinaryOES ::a3m::get_glGetProgramBinaryOES()
#define glProgramBinaryOES ::a3m::get_glProgramBinaryOES()

#define GL_EXTENSION_NOT_SUPPORTED 0

// Some extensions used in A3M may not be supported on the target
// platform (e.g. PC emulators from different vendors) so they need
// to be defined here as undefined and also dealt with at runtine
#ifndef GL_TEXTURE_EXTERNAL_OES
#define GL_TEXTURE_EXTERNAL_OES GL_EXTENSION_NOT_SUPPORTED
#endif
#ifndef GL_SAMPLER_EXTERNAL_OES
#define GL_SAMPLER_EXTERNAL_OES GL_EXTENSION_NOT_SUPPORTED
#endif

namespace a3m
{
  /**
   * Checks if GL_OES_EGL_image_external extension is supported
   * \return A3M_TRUE if extension is supported
   */
  A3M_BOOL GL_OES_EGL_image_external_supported();

  /**
   * Returns glGetProgramBinaryOES function pointer.
   * \return Function pointer or NULL if extension is not supported
   */
  A3M_BOOL GL_OES_get_program_binary_supported();

  /**
   * Returns glGetProgramBinaryOES function pointer.
   * \return Function pointer or NULL if extension is not supported
   */
  PFNGLGETPROGRAMBINARYOESPROC get_glGetProgramBinaryOES();

  /**
   * Returns glProgramBinaryOES function pointer.
   * \return Function pointer or NULL if extension is not supported
   */
  PFNGLPROGRAMBINARYOESPROC get_glProgramBinaryOES();

} // namespace a3m

#endif /* A3M_EXTENSIONS_H */
