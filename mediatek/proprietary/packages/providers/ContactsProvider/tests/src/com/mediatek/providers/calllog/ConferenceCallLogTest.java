package com.mediatek.providers.calllog;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog.Calls;

import com.android.providers.contacts.CallLogProvider;
import com.android.providers.contacts.TestUtils;
import com.mediatek.provider.MtkCallLog;
import com.mediatek.provider.MtkCallLog.ConferenceCalls;

/**
 * Unit tests for {@link CallLogProvider}.
 *
 * Run the test like this:
 * <code>
 * adb shell am instrument -e class com.mediatek.providers.calllog.ConferenceCallLogTest -w \
 *         com.android.providers.calllog.tests/android.test.InstrumentationTestRunner
 * </code>
 */
public class ConferenceCallLogTest extends CallLogProviderTest {

    public void testInsert_ConferenceCallRecord() {
        ContentValues values = new ContentValues();
        values.put(ConferenceCalls.CONFERENCE_DATE, 10000);
        Uri uri = mResolver.insert(ConferenceCalls.CONTENT_URI, values);
        long conferenceId = ContentUris.parseId(uri);
        assertTrue("Failed to insert conference record.", conferenceId > 0);
    }

    public void testInsert_ConferenceCallLogRecord() {
        String number1 = "18002637643";
        createRawContact(mResolver, "Lucy", number1);

        // insert conference call
        ContentValues values = new ContentValues();
        values.put(ConferenceCalls.CONFERENCE_DATE, 100000);
        Uri uri = mResolver.insert(ConferenceCalls.CONTENT_URI, values);

        //insert participant call
        long confCallId = ContentUris.parseId(uri);
        ContentValues values1 = getCallLogValues(Calls.OUTGOING_TYPE, number1);
        values1.put(MtkCallLog.Calls.CONFERENCE_CALL_ID, confCallId);
        Uri uri1 =  mResolver.insert(Calls.CONTENT_URI, values1);
        assertStoredValues(uri1, values1);
        assertSelection(uri1, values1, ConferenceCalls._ID, ContentUris.parseId(uri1));

        ContentValues values2 = getDefaultValues(Calls.OUTGOING_TYPE);
        values2.put(MtkCallLog.Calls.CONFERENCE_CALL_ID, confCallId);
        Uri uri2 =  mResolver.insert(Calls.CONTENT_URI, values2);
        assertStoredValues(uri2, values2);
        assertSelection(uri2, values2, ConferenceCalls._ID, ContentUris.parseId(uri2));

        // Check callLog result
        ContentValues[] expectedValues = new ContentValues[2];
        expectedValues[0] = values1;
        expectedValues[1] = values2;
        assertStoredValuesOrderly(uri, null, null, expectedValues);
    }

    /**
     * Query conference info of conference table.
     */
    public void testQueryConference() {
        // clear the table first
        clearConferenceCalls(mResolver);

        // insert 2 entry into conference table
        long ccId = insertConferenceCall(mResolver);
        insertConferenceCall(mResolver);

        // get the data in Conference Calls table
        Cursor conference = mResolver.query(ConferenceCalls.CONTENT_URI, null, null, null, null);
        // get the first entry data
        Cursor first = mResolver.query(ConferenceCalls.CONTENT_URI, null, ConferenceCalls._ID + "="
                + ccId, null, null);
        try {
            assertEquals(2, conference.getCount());
            assertEquals(1, first.getCount());
        } finally {
            if (conference != null) {
                conference.close();
            }
            if (first != null) {
                first.close();
            }
        }
    }

    /**
     * Delete conference info of conference table.
     */
    public void testDeleteConference() {
        // clear table first
        clearConferenceCalls(mResolver);

        // insert entry into conference table
        long conId = insertConferenceCall(mResolver);
        Uri queryUri = ContentUris.withAppendedId(ConferenceCalls.CONTENT_URI, conId);
        // insert 2 conference call log.
        ContentValues value;
        value = getCallLogValues(Calls.INCOMING_TYPE, "1-800-263-0000");
        value.put(MtkCallLog.Calls.CONFERENCE_CALL_ID, conId);
        insertCallRecord(mResolver, value);
        value = getCallLogValues(Calls.INCOMING_TYPE, "1-800-263-1111");
        value.put(MtkCallLog.Calls.CONFERENCE_CALL_ID, conId);
        insertCallRecord(mResolver, value);

        // get the data in Conference Calls table
        Cursor conference = mResolver.query(ConferenceCalls.CONTENT_URI, null, null, null, null);
        // get conference call log by conference uri, there are 2 call log
        // associated with conference id.
        Cursor callLog = mResolver.query(queryUri, null, null, null, null);
        try {
            assertEquals(1, conference.getCount());
            assertEquals(2, callLog.getCount());
            long deletedCon = clearConferenceCalls(mResolver);
            assertEquals(1, deletedCon);
            long deletedCalls = clearCallLogs(mResolver);
            assertEquals(0, deletedCalls);
        } finally {
            if (conference != null) {
                conference.close();
            }
            if (callLog != null) {
                callLog.close();
            }
        }
    }

    /**
     * Delete conference info of conference table.
     */
    public void testDeleteConferenceById() {
        // clear table first
        clearConferenceCalls(mResolver);

        // insert 2 entries into conference table
        long conId = insertConferenceCall(mResolver);
        insertConferenceCall(mResolver);
        Uri ccUri = ContentUris.withAppendedId(ConferenceCalls.CONTENT_URI, conId);

        int deleted = mResolver.delete(ccUri, null, null);
        assertEquals(1, deleted);
    }

    /**
     * Delete Conference CallLog.
     */
    public void testDeleteConferenceCallLog() {
        // insert entry into conference table
        long conId = insertConferenceCall(mResolver);
        // insert 2 conference call log.
        ContentValues value;
        value = getCallLogValues(Calls.INCOMING_TYPE, "1-800-263-2222");
        value.put(MtkCallLog.Calls.CONFERENCE_CALL_ID, conId);
        insertCallRecord(mResolver, value);
        value = getCallLogValues(Calls.INCOMING_TYPE, "1-800-263-3333");
        value.put(MtkCallLog.Calls.CONFERENCE_CALL_ID, conId);
        insertCallRecord(mResolver, value);

        // ensure the call log inserted in table
        Cursor conferenceCallLog = mResolver.query(Calls.CONTENT_URI, null,
                MtkCallLog.Calls.CONFERENCE_CALL_ID + "=" + conId, null, null);
        try {
            assertEquals(2, conferenceCallLog.getCount());
            // delete the related call log.
            int deleted = mResolver.delete(Calls.CONTENT_URI, MtkCallLog.Calls.CONFERENCE_CALL_ID + "="
                    + conId, null);
            assertEquals(2, deleted);
        } finally {
            if (conferenceCallLog != null) {
                conferenceCallLog.close();
            }
        }
    }

    public void testConferenceTableUpdate() {
        ContentValues values = new ContentValues();
        values.put(ConferenceCalls.CONFERENCE_DATE, System.currentTimeMillis());
        Uri uri = mResolver.insert(ConferenceCalls.CONTENT_URI, values);

        long conferenceId = ContentUris.parseId(uri);
        ContentValues callLogValues = getDefaultCallValues();
        callLogValues.put(MtkCallLog.Calls.CONFERENCE_CALL_ID, conferenceId);
        Uri newCallLogUri = addCallLog(callLogValues);
        long callLogId = ContentUris.parseId(newCallLogUri);
        assertTrue("CallLog did not save successfully!!!", callLogId > 0);

        ContentValues updatedValues = new ContentValues();
        updatedValues.put(ConferenceCalls.CONFERENCE_DATE, System.currentTimeMillis());
        updatedValues.put(ConferenceCalls.GROUP_ID, 2);
        int updateCount = mResolver.update(uri, values, null, null);
        assertEquals(1, updateCount);
    }

    public void testConferenceCallLogQuery() {
        // Create contacts
        String phoneNumber1 = "123456001";
        String phoneNumber2 = "123456002";
        String phoneNumber3 = "123456003";
        String phoneNumber4 = "123456004";
        String phoneNumber5 = "123456005";

        // Insert a conference
        ContentValues ConferenceValues = new ContentValues();
        ConferenceValues.put(ConferenceCalls.CONFERENCE_DATE, 3);
        Uri uri = mResolver.insert(ConferenceCalls.CONTENT_URI, ConferenceValues);
        long conferenceId = ContentUris.parseId(uri);
        assertTrue("Conference not saved successfully!!!", conferenceId > 0);

        // Insert 5 calllogs contains 3 conference calllogs
        ContentValues callLogValues1 = insertCallLog(Calls.MISSED_TYPE, phoneNumber1,
                1, -1);
        long callLogId1 = callLogValues1.getAsLong(Calls._ID);

        ContentValues callLogValues2 = insertCallLog(Calls.OUTGOING_TYPE, phoneNumber2,
                2, conferenceId);
        long callLogId2 = callLogValues2.getAsLong(Calls._ID);

        ContentValues callLogValues3 = insertCallLog(Calls.OUTGOING_TYPE, phoneNumber3,
                3, conferenceId);
        long callLogId3 = callLogValues3.getAsLong(Calls._ID);

        ContentValues callLogValues4 = insertCallLog(Calls.INCOMING_TYPE, phoneNumber4,
                4, -1);
        long callLogId4 = callLogValues4.getAsLong(Calls._ID);

        ContentValues callLogValues5 = insertCallLog(Calls.OUTGOING_TYPE, phoneNumber5,
                5, conferenceId);
        long callLogId5 = callLogValues5.getAsLong(Calls._ID);

        // Union query and check the result
        Uri queryUri = Calls.CONTENT_URI;
        Uri ueryUri1 = ContentUris.withAppendedId(queryUri, callLogId1);
        // The contact info
        assertStoredValues(ueryUri1, callLogValues1);

        // Union query and check the result
        Uri ueryUri12 = ContentUris.withAppendedId(queryUri, callLogId2);
        // The contact info
        assertStoredValues(ueryUri12, callLogValues2);

        // Query and check the calllog order
        String orderby = MtkCallLog.Calls.SORT_DATE + " DESC";
        Cursor c = mResolver.query(queryUri, null, null, null, orderby);
        try {
            assertEquals(5, c.getCount());
            c.moveToFirst();
            assertEquals(callLogId4, c.getLong(c.getColumnIndex(Calls._ID)));
            c.moveToNext();
            assertEquals(callLogId2, c.getLong(c.getColumnIndex(Calls._ID)));
            c.moveToNext();
            assertEquals(callLogId3, c.getLong(c.getColumnIndex(Calls._ID)));
            c.moveToNext();
            assertEquals(callLogId5, c.getLong(c.getColumnIndex(Calls._ID)));
            c.moveToNext();
            assertEquals(callLogId1, c.getLong(c.getColumnIndex(Calls._ID)));
        } catch (Error e) {
            TestUtils.dumpCursor(c);
            throw e;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }
}
