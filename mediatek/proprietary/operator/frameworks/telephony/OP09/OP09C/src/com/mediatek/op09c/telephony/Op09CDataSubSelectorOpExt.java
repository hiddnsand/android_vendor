package com.mediatek.op09c.telephony;

import java.util.Arrays;

import android.content.Context;
import android.content.Intent;

import android.os.Build;
import android.os.SystemProperties;

import android.telephony.Rlog;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.SubscriptionController;
import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.internal.telephony.datasub.DataSubConstants;
import com.mediatek.internal.telephony.datasub.IDataSubSelectorOPExt;
import com.mediatek.internal.telephony.datasub.DataSubSelector;
import com.mediatek.internal.telephony.datasub.DataSubSelectorUtil;
import com.mediatek.internal.telephony.datasub.ISimSwitchForDSSExt;
import com.mediatek.internal.telephony.datasub.CapabilitySwitch;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;
import com.mediatek.internal.telephony.datasub.DataSubSelectorUtil;

public class Op09CDataSubSelectorOpExt implements IDataSubSelectorOPExt {
    private static final boolean USER_BUILD = TextUtils.equals(Build.TYPE, "user");
    private static boolean DBG = true;

    private static String LOG_TAG = "Op09CDSSelector";

    private static Context mContext = null;

    private static DataSubSelector mDataSubSelector = null;

    private static ISimSwitchForDSSExt mSimSwitchForDSS = null;

    private static CapabilitySwitch mCapabilitySwitch = null;

    private int mPhoneNum = 0;

    public Op09CDataSubSelectorOpExt(Context context) {
        mContext = context;
    }

    public void init(DataSubSelector dataSubSelector, ISimSwitchForDSSExt simSwitchForDSS) {
        mDataSubSelector = dataSubSelector;
        mSimSwitchForDSS = simSwitchForDSS;
        mCapabilitySwitch = CapabilitySwitch.getInstance(mContext, dataSubSelector);
        mPhoneNum = mDataSubSelector.getPhoneNum();
    }

    @Override
    public void handleSimStateChanged(Intent intent) {
        String simStatus = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
        int slotId = intent.getIntExtra(PhoneConstants.SLOT_KEY, PhoneConstants.SIM_ID_1);

        log("handleSimStateChanged, slotId = " + slotId + ", " + simStatus);
        if (simStatus.equals(IccCardConstants.INTENT_VALUE_ICC_IMSI)) {
            subSelector(intent);
        }
    }

    @Override
    public void handleSubinfoRecordUpdated(Intent intent) {
        int detectedType = intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE);
        if (detectedType != MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE) {
            subSelector(intent);
        }
    }

    @Override
    public void subSelector(Intent intent) {
        log("DataSubSelector for op09C");

        int phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        int insertedSimCount = 0;
        int insertedStatus = 0;
        String[] currIccId = new String[mPhoneNum];

        for (int i = 0; i < mPhoneNum; i++) {
            currIccId[i] = DataSubSelectorUtil.getIccidFromProp(i);
            if (currIccId[i] == null || "".equals(currIccId[i])) {
                log("op09C error: iccid not found, wait for next sub ready");
                return;
            }
            if (!USER_BUILD) {
                log("op09C currIccId[" + i + "]:"
                    + SubscriptionInfo.givePrintableIccid(currIccId[i]));
            }
            if (!DataSubConstants.NO_SIM_VALUE.equals(currIccId[i])) {
                ++insertedSimCount;
                insertedStatus = insertedStatus | (1 << i);
            }
        }
        log("op09C: Inserted SIM count: " + insertedSimCount +
                    ", insertedStatus: " + insertedStatus);

        if (mCapabilitySwitch.isSimUnLocked() == false) {
            log("DataSubSelector for op09C: do not switch because of sim locking");
            return;
        }

        if (insertedSimCount == 1) {
            for (int i = 0; i < mPhoneNum; i++) {
                if ((insertedStatus & (1 << i)) != 0) {
                    phoneId = i;
                    break;
                }
            }
            log("op09C: Single SIM: Set Default data to phone:" + phoneId);
        } else {
            String defaultIccid = "";
            int defDataSubId = SubscriptionController.getInstance().getDefaultDataSubId();
            int defDataPhoneId = SubscriptionManager.getPhoneId(defDataSubId);
            if (defDataPhoneId >= 0) {
                if (defDataPhoneId >= mPhoneNum) {
                   log("phoneId out of boundary :" + defDataPhoneId);
                } else {
                   defaultIccid = DataSubSelectorUtil.getIccidFromProp(defDataPhoneId);
                }
            }

            if (!USER_BUILD) {
                log("Default data Iccid = " + defaultIccid);
            }
            if (("N/A".equals(defaultIccid)) || ("".equals(defaultIccid))) {
                return;
            }

            for (int i = 0; i < mPhoneNum; i++) {
                if (defaultIccid.equals(currIccId[i])) {
                    phoneId = i;
                    break;
                }
            }
            log("op09C: Dual SIM: Set Default data to phone:" + phoneId);
        }
        if (SubscriptionManager.INVALID_PHONE_INDEX != phoneId) {
            mCapabilitySwitch.setCapability(phoneId);
            mDataSubSelector.setDefaultData(phoneId);
            mDataSubSelector.syncDefaultDataToMd(phoneId);
        }
    }

    @Override
    public void handleAirPlaneModeOff(Intent intent) {}

    @Override
    public void handlePlmnChanged(Intent intent) {}

    @Override
    public void handleDataEnable(int status) {}

    @Override
    public void handleDefaultDataChanged(Intent intent) {}

    private void log(String txt) {
        if (DBG) {
            Rlog.d(LOG_TAG, txt);
        }
    }
}
