/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mmsdk/CallbackClientMgr"

#include <log/log.h>
#include <utils/Errors.h>
#include <mtkcam/feature/effectHalBase/ICallbackClientMgr.h>
#include "common.h"

//use property_get
#include <cutils/properties.h>

#include <utils/List.h>
#include <mtkcam/def/PriorityDefs.h>
#include <sys/prctl.h>

#include <mutex>
#include <list>
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF(LOG_TAG "(%d)[%s] " fmt "\n", ::gettid(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

#define FUNCTION_LOG_START          MY_LOGD_IF(1<=1, " +");
#define FUNCTION_LOG_END            MY_LOGD_IF(1<=1, " -");

using namespace std;
using namespace NSCam;
using namespace android;

/******************************************************************************
 *
 ******************************************************************************/
class CallbackClientMgr : public ICallbackClientMgr
{

public:
                             CallbackClientMgr() {}
    virtual                 ~CallbackClientMgr() {mlTimestamp.clear(); mlCbFunc.clear();}

    virtual MINT32           size();
    virtual MVOID            createCallbackClient(MINT64 timestamp);
    virtual MVOID            removeCallbackClient(MINT64 timestamp);
    virtual MBOOL            canSupportBGService();
    virtual MBOOL            registerCB(CallbackFunc func);
    virtual MBOOL            registerBufferCB(BufferCBFunc func);


// Calback API
    virtual bool              onCB_CompressedImage_packed(
                                  int64_t const   i8Timestamp                   __attribute__((unused)),
                                  uint32_t const  u4BitstreamSize               __attribute__((unused)),
                                  uint8_t const*  puBitstreamBuf                __attribute__((unused)),
                                  uint32_t const  u4CallbackIndex = 0,
                                  bool            fgIsFinalImage = true,
                                  uint32_t const  msgType = MTK_CAMERA_MSG_EXT_DATA_COMPRESSED_IMAGE,
                                  bool            bNormal = false
                              );
private:
    std::list<MINT64>                 mlTimestamp;        // timestamp to setCallbackClient
    std::mutex                        mMutex;
    std::list<CallbackFunc>           mlCbFunc;
    std::mutex                        mCBMutex;
    BufferCBFunc                      mBufferCbFunc = nullptr;
};

/******************************************************************************
 *
 ******************************************************************************/
ICallbackClientMgr *
ICallbackClientMgr::
getInstance()
{
    static CallbackClientMgr gCbClientMgr;
    return &gCbClientMgr;
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
CallbackClientMgr::
createCallbackClient(MINT64 timestamp)
{
    FUNCTION_LOG_START
    std::unique_lock <std::mutex> l(mMutex);
    mlTimestamp.push_front(timestamp);
    MY_LOGD("TimeStamp %" PRId64 " size %zd", timestamp, mlTimestamp.size());
    FUNCTION_LOG_END
    return;
}
/******************************************************************************
 *
 ******************************************************************************/
MINT32
CallbackClientMgr::
size()
{
    FUNCTION_LOG_START
    std::unique_lock <std::mutex> l(mMutex);
    MY_LOGD("CallbackClientList size %zd", mlTimestamp.size());
    FUNCTION_LOG_END
    return mlTimestamp.size();
}
/******************************************************************************
 *
 ******************************************************************************/
MBOOL
CallbackClientMgr::
canSupportBGService()
{
#if 1
    return size() > 0 ;
#else
    char prop[PROPERTY_VALUE_MAX];
    int enable;
    ::property_get("camera.callback.enable", prop, "0");
    enable = ::atoi(prop);
    return (enable > 0);
#endif
}
/******************************************************************************
 *
 ******************************************************************************/
MBOOL
CallbackClientMgr::
registerCB(CallbackFunc func)
{
    FUNCTION_LOG_START
    std::unique_lock <std::mutex> l(mCBMutex);
    if (!mlCbFunc.empty())
    {
        std::list<CallbackFunc>::iterator iterCbFunc = mlCbFunc.begin();
        for(;iterCbFunc != mlCbFunc.end();++iterCbFunc)
        {
            if (*iterCbFunc == func)
            {
                MY_LOGD("func existed");
                return MTRUE;
            }
        }
    }

    mlCbFunc.push_front(func);
    MY_LOGD("mlCbFunc.size() %zd", mlCbFunc.size());
    FUNCTION_LOG_END
    return MTRUE;
}
/******************************************************************************
 *
 ******************************************************************************/
MBOOL
CallbackClientMgr::
registerBufferCB(BufferCBFunc func)
{
    FUNCTION_LOG_START
//    std::unique_lock <std::mutex> l(mCBMutex);
    mBufferCbFunc = func;
    FUNCTION_LOG_END
    return MTRUE;
}
/******************************************************************************
 *
 ******************************************************************************/
MVOID
CallbackClientMgr::
removeCallbackClient(MINT64 timestamp)
{
    FUNCTION_LOG_START
    MBOOL bCB = MFALSE;
    {
        std::unique_lock <std::mutex> l(mMutex);
        if (!mlTimestamp.empty())
        {
            // erase elapsed CallbackClient
            std::list<MINT64>::iterator               iter   = mlTimestamp.begin();
            for(;iter != mlTimestamp.end();)
            {
                if (*iter == timestamp)
                {
                    iter   = mlTimestamp.erase(iter);
                    MY_LOGD("remove CallbackClient with timestamp = %" PRId64 ", size %zd", timestamp, mlTimestamp.size());
                    if (mlTimestamp.size() == 0)
                    {
                        bCB = MTRUE;
                    }
                    break;
                }
                else
                {
                    ++iter;
                }
            }
        }
    }
    if (bCB)
    {
        // destroy PostProc after CameraAP be destroyed
        std::unique_lock <std::mutex> l(mCBMutex);
        MY_LOGD("mlCbFunc.size() %zd", mlCbFunc.size());
        if (!mlCbFunc.empty())
        {
            std::list<CallbackFunc>::iterator iterCbFunc = mlCbFunc.begin();
            for(;iterCbFunc != mlCbFunc.end();++iterCbFunc)
            {
                MY_LOGD("Callback to remove PostProc");
                (*iterCbFunc)();
            }
        }
    }
    FUNCTION_LOG_END
}
/******************************************************************************
 *
 ******************************************************************************/
bool
CallbackClientMgr::
onCB_CompressedImage_packed(
                            int64_t const   i8Timestamp,
                            uint32_t const  u4BitstreamSize,
                            uint8_t const*  puBitstreamBuf,
                            uint32_t const  u4CallbackIndex,
                            bool            fgIsFinalImage,
                            uint32_t const  msgType,
                            bool            bNormal
                           )
{
    FUNCTION_LOG_START
    bool ret = false;
    std::unique_lock <std::mutex> l(mMutex);
    std::list<MINT64>::iterator iter;

    MY_LOGD(
        "timestamp(%" PRId64 "), bitstream:size/buf=%d/%p, index(%d), IsFinalImage(%d)",
        i8Timestamp, u4BitstreamSize, puBitstreamBuf, u4CallbackIndex, fgIsFinalImage
    );

    if (mlTimestamp.empty() || mBufferCbFunc == nullptr)
    {
        MY_LOGE("CallbackClient list is empty!!!");
    }
    else
    {
        ret = mBufferCbFunc(
                              i8Timestamp,
                              u4BitstreamSize,
                              puBitstreamBuf,
                              u4CallbackIndex,
                              fgIsFinalImage,
                              msgType == MTK_CAMERA_MSG_EXT_DATA_COMPRESSED_IMAGE ? CAMERA_MSG_COMPRESSED_IMAGE : msgType,
                              bNormal
                           );
    }

    FUNCTION_LOG_END
    return ret;
}
