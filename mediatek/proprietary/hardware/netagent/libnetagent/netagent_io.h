#ifndef __LIB_NETAGENT_IO_H__
#define __LIB_NETAGENT_IO_H__

typedef enum {
    NETAGENT_IO_RET_SUCCESS     = 0,
    NETAGENT_IO_RET_GENERIC_FAILURE = 1,
    NETAGENT_IO_RET_AT_FAIL     = 2,
} netagent_io_ret_e;

typedef enum {
    NETAGENT_IO_CMD_IFST        = 1,
    NETAGENT_IO_CMD_RA          = 2,
    NETAGENT_IO_CMD_IPUPDATE    = 3,

    NETAGENT_IO_CMD_IFUP        = 101,
    NETAGENT_IO_CMD_IFDOWN      = 102,
    NETAGENT_IO_CMD_IFCHG       = 103,
    NETAGENT_IO_CMD_IFSTATE     = 104,
    NETAGENT_IO_CMD_MAX         = 0xFFFFFFFF
} netagent_io_cmd_e;

typedef enum {
    NETAGENT_IO_IFST_DOWN       = 0,
    NETAGENT_IO_IFST_UP         = 1,
    NETAGENT_IO_IFST_MAX        = 0xFFFFFFFF
} netagent_io_ifst_e;

typedef enum {
    NETAGENT_IO_ADDR_TYPE_UNKNOWN = 0,
    NETAGENT_IO_ADDR_TYPE_IPv4  = 0x1,
    NETAGENT_IO_ADDR_TYPE_IPv6  = 0x2,
    NETAGENT_IO_ADDR_TYPE_IPv4v6= 0x3,
    NETAGENT_IO_ADDR_TYPE_Any   = 0x4,
    NETAGENT_IO_ADDR_TYPE_MAX   = 0xFFFFFFFF
} netagent_io_addr_type_e;

typedef enum {
    NETAGENT_IO_NO_RA_REFRESH       = 0,
    NETAGENT_IO_NO_RA_INITIAL       = 1,
    NETAGENT_IO_RA_MAX              = 0xFFFFFFFF
} netagent_io_ra_e;

void *netagent_io_init();
int netagent_io_deinit(void *o);

void *netagent_io_cmd_ifst_alloc(unsigned int if_id, netagent_io_ifst_e state, netagent_io_addr_type_e addr_type);
void *netagent_io_cmd_ra_alloc(unsigned int if_id, netagent_io_ra_e flag);
void *netagent_io_cmd_ipupdate_alloc(unsigned int if_id, netagent_io_addr_type_e addr_type, unsigned int *addr, int ipv6PrefixLength);
int netagent_io_cmd_free(void *co);

int netagent_io_get_if_id(void *co, unsigned int *if_id);
int netagent_io_get_ip_change_reason(void *co, char **reason);
int netagent_io_get_cmd_type(void *co, netagent_io_cmd_e *cmd);
int netagent_io_get_addr_type(void *co, netagent_io_addr_type_e *addr_type);
int netagent_io_get_addr_v4(void *co, unsigned int *addr);
int netagent_io_get_addr_v6(void *co, unsigned int *addr);


void *netagent_io_recv(void *o) ;
int netagent_io_send(void *o, void *co) ;
int netagent_io_test(void *o, char *cmd);


/* ----

Example 1 :  for init

    void    *netagent_io_obj = 0;

    netagent_io_obj = netagent_io_init();
    if (!netagent_io_obj) {
        Err("init fail");
    } else {
        Dbg("init success");
    }

Example 2 : for deinit

    if (netagent_io_deinit(netagent_io_obj) == NETAGENT_IO_RET_SUCCESS) {
        Dbg("deinit success");
    } else {
        Err("deinit fail");
    }

Example 3 : Receive the URC

    void    *netagent_io_cmd_obj = 0;

    if (netagent_io_recv(netagent_io_obj, &netagent_io_cmd_obj) == NETAGENT_IO_RET_SUCCESS) {
        netagent_io_cmd_e cmd;
        netagent_io_get_addr_type type;
        unsigned int addr_v4;
        unsigned int addr_v6[4];

        if (netagent_io_get_cmd_type(netagent_io_cmd_obj, &cmd) == NETAGENT_IO_RET_SUCCESS) {
            switch (cmd) {
                case NETAGENT_IO_CMD_IFUP :
                    if (netagent_io_get_cmd_type(netagent_io_cmd_obj, &type) == NETAGENT_IO_RET_SUCCESS) {
                        switch (type) {
                            case NETAGENT_IO_ADDR_TYPE_IPv4 :
                                if (netagent_io_get_addr_v4(netagent_io_cmd_obj, &addr_v4) == NETAGENT_IO_RET_SUCCESS) {
                                    Dbg("get v4 address success");
                                } else {
                                    Err("get v4 address fail");
                                }
                                break;
                            case NETAGENT_IO_ADDR_TYPE_IPv6 :
                                if (netagent_io_get_addr_v6(netagent_io_cmd_obj, &addr_v6) == NETAGENT_IO_RET_SUCCESS) {
                                    Dbg("get v6 address success");
                                } else {
                                    Err("get v6 address fail");
                                }
                                break;
                            case NETAGENT_IO_ADDR_TYPE_IPv4v6;
                                if (netagent_io_get_addr_v4(netagent_io_cmd_obj, &addr_v4) == NETAGENT_IO_RET_SUCCESS) {
                                    Dbg("get v4 address success");
                                } else {
                                    Err("get v4 address fail");
                                }
                                if (netagent_io_get_addr_v6(netagent_io_cmd_obj, &addr_v6) == NETAGENT_IO_RET_SUCCESS) {
                                    Dbg("get v6 address success");
                                } else {
                                    Err("get v6 address fail");
                                }
                                break;
                        }

                    } else {
                        Err("get type fail");
                    }
                    break;
                case NETAGENT_IO_CMD_IFDOWN :
                case NETAGENT_IO_CMD_IFCHG :
            }
        } else {
            Err("get cmd fail");
        }

        netagent_io_cmd_free(netagent_io_cmd_obj);

    } else {
        Err("recv fail");
    }

Example 4 : send ifst (up with ipv4 address);

    void *netagent_io_cmd_obj = 0;
    unsigned int interface_id = 1;

    netagent_io_cmd_obj = netagent_io_cmd_ifst_alloc(interface_id, NETAGENT_IO_IFST_UP, NETAGENT_IO_ADDR_TYPE_IPv4);

    if (netagent_io_send(netagent_io_obj, &netagent_io_cmd_obj) == NETAGENT_IO_RET_SUCCESS) {
        Dbg("send cmd success");
    } else {
        Err("send cmd fail");
    }

    netagent_io_cmd_free(netagent_io_cmd_obj);

---- */


#endif /* __LIB_NETAGENT_IO_H__ */
