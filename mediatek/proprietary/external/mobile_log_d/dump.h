#ifndef _DUMP_H
#define _DUMP_H

struct dump_util {
	char src_file[255];
	char des_file[255];
	void (*dump)(struct dump_util *du_p);
	struct dump_util *next;
};

// all kinds of mobile log files node
struct file_node {
	char name[128];
	long int index;
	struct file_node *next;
};
typedef struct file_node file_node_t;

struct dump_util *add_dump_file(char *src,char *des,char *path, void* func);
void add_to_dump_list(struct dump_util **du_list,struct dump_util *item);
void copy_and_dump(void);
void copy_dir(char *srcpath, char *despath, char *postfix);
void copy_logs(char *srcpath, char *despath, char *filename, char *postfix);
void copy_to_sdcard(void);
struct dump_util *default_init_dump_list(char *des_path);
void do_copy_and_dump(void);
void dump_file(struct dump_util *du_p);
void dump_sys_prop(struct dump_util *du_p);
void dump_system_info(void);
static void get_sys_prop(const char *key, const char *name, void *cookies);
struct dump_util *init_dump_list(char *des_path);
struct dump_util *init_dump_list_from_file(char *file,char *path);
void set_func(struct dump_util *du_p, char *key);
#endif
