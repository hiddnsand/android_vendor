#
# create loop device bind on IPO-H file for nand project

LOCAL_PATH:=$(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= ipo_swap.c

LOCAL_MODULE:= ipo_swap
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_C_INCLUDES += vendor/mediatek/proprietary/external/mtd-utils/include

LOCAL_SHARED_LIBRARIES += liblog

include $(MTK_EXECUTABLE)
