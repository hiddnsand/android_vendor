LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/Android.mk
LOCAL_CLANG := true
LOCAL_SRC_FILES := storagemanagerd.cpp
LOCAL_MODULE := storagemanagerd
LOCAL_SHARED_LIBRARIES := libcutils libbase

LOCAL_INIT_RC := storagemanagerd.rc

include $(MTK_EXECUTABLE)
