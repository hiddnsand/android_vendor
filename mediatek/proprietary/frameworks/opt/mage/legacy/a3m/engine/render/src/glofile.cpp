/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * GLO file loader
 *
 */

#include <a3m/glofile.h>        /* API for functions defined here     */
#include <a3m/animation.h>      /* Animation classes                  */
#include <a3m/stream.h>         /* Stream class                       */
#include <a3m/log.h>            /* Log functions                      */
#include <a3m/light.h>          /* Light functions                    */
#include <a3m/lightanimators.h> /* for LightColourAnimator etc.       */
#include <a3m/camera.h>         /* To create Camera scene objects     */
#include <a3m/solid.h>          /* Solid class                        */
#include <a3m/appearance.h>     /* Appearance class                   */
#include <a3m/appearanceanimators.h> /* for AppearancePropertyAnimator*/
#include <a3m/assetcachepool.h> /* AssetCachePool class               */
#include <a3m/colour.h>         /* Colour class                       */
#include <a3m/sceneutility.h>   /* visitScene() function              */
#include <a3m/scenenodeanimators.h> /* for SceneNodeScaleAnimator etc.*/
#include <a3m/scenenodevisitor.h> /* SceneNodeVisitor class           */
#include <a3m/properties.h>     /* properties namespace               */
#include <a3m/version.h>        /* for Version                        */

#include <cstdio>               /* for sscanf                         */
#include <string>               /* std::string class                  */
#include <vector>               /* std::vector class template         */
#include <map>                  /* std::map class template            */
#include <utility>              /* std::rel_ops namespace             */

using namespace a3m;
using namespace std::rel_ops;

#ifdef A3M_USE_LIGHT_ZORDER
extern "C" A3M_INT32 a3m_drvb_f0();
#endif

namespace
{

  // List of authoring tools which create Glo files.
  enum AuthoringTool
  {
    UNKNOWN_AUTHORING_TOOL,
    AUTODESK_3DS_MAX,
    BLENDER
  };

  // Update this number when the glo loader is updated to work with a new
  // version of the glo specification.
  const Version CURRENT_GLO_VERSION( 0, 14 );

  // Versions at which different Glo features were added.
  const Version VERSION_BLENDER_SUPPORTED( 0, 13 );
  const Version VERSION_SKINNING_SUPPORTED( 0, 14 );

  // Constants for fixing angle recovery bug
  const A3M_FLOAT AXIS_EPSILON = 0.001f;
  const A3M_FLOAT MAX_MIN_ANGLE = 3.1415f;

  // GLO primitive types.
  const A3M_UINT32 PRIMITIVE_TYPE_POINTS =         0;
  const A3M_UINT32 PRIMITIVE_TYPE_LINES =          1;
  const A3M_UINT32 PRIMITIVE_TYPE_LINE_LOOP =      2;
  const A3M_UINT32 PRIMITIVE_TYPE_LINE_STRIP =     3;
  const A3M_UINT32 PRIMITIVE_TYPE_TRIANGLES =      4;
  const A3M_UINT32 PRIMITIVE_TYPE_TRIANGLE_STRIP = 5;
  const A3M_UINT32 PRIMITIVE_TYPE_TRIANGLE_FAN =   6;
  const A3M_UINT32 PRIMITIVE_TYPE_COUNT =          7;

  // Maximum length of any string. This is mainly to guard against
  // 'obviously' wrong strings (i.e. trying to read a string from the wrong
  // part of a file.
  const A3M_UINT32 MAX_STRING_LENGTH = 1024;

  // These constants assume little-endian ness.
  const A3M_UINT32 CHUNK_ID_GLO3 = '3OLG';
  const A3M_UINT32 CHUNK_ID_LGEO = 'OEGL';
  const A3M_UINT32 CHUNK_ID_LMAT = 'TAML';
  const A3M_UINT32 CHUNK_ID_SCNE = 'ENCS';
  const A3M_UINT32 CHUNK_ID_GEOM = 'MOEG';
  const A3M_UINT32 CHUNK_ID_VSET = 'TESV';
  const A3M_UINT32 CHUNK_ID_PSET = 'TESP';
  const A3M_UINT32 CHUNK_ID_VATT = 'TTAV';
  const A3M_UINT32 CHUNK_ID_VBUF = 'FUBV';
  const A3M_UINT32 CHUNK_ID_IBUF = 'FUBI';
  const A3M_UINT32 CHUNK_ID_LCTL = 'LTCL';
  const A3M_UINT32 CHUNK_ID_SKIN = 'NIKS';
  const A3M_UINT32 CHUNK_ID_ATCH = 'HCTA';
  const A3M_UINT32 CHUNK_ID_NODE = 'EDON';
  const A3M_UINT32 CHUNK_ID_GREF = 'FERG';
  const A3M_UINT32 CHUNK_ID_SREF = 'FERS';
  const A3M_UINT32 CHUNK_ID_SMPL = 'LPMS';
  const A3M_UINT32 CHUNK_ID_SMAT = 'TAMS';
  const A3M_UINT32 CHUNK_ID_MATL = 'LTAM';
  const A3M_UINT32 CHUNK_ID_LANM = 'MNAL';
  const A3M_UINT32 CHUNK_ID_ANIM = 'MINA';
  const A3M_UINT32 CHUNK_ID_CHAN = 'NAHC';
  const A3M_UINT32 CHUNK_ID_LLGT = 'TGLL';
  const A3M_UINT32 CHUNK_ID_LGHT = 'THGL';
  const A3M_UINT32 CHUNK_ID_LREF = 'FERL';
  const A3M_UINT32 CHUNK_ID_INFO = 'OFNI';
  const A3M_UINT32 CHUNK_ID_LCAM = 'MACL';
  const A3M_UINT32 CHUNK_ID_CAMA = 'AMAC';
  const A3M_UINT32 CHUNK_ID_CREF = 'FERC';

  // Types for index buffer data. At present only IBUF_TYPE_U16 is supported
  enum DataTypes
  {
    BYTE,
    UNSIGNED_BYTE,
    SHORT,
    UNSIGNED_SHORT,
    INTEGER,
    UNSIGNED_INTEGER,
    FLOATING_POINT,
    FIXED_POINT,
    HALF_FLOATING_POINT,
    TYPE_COUNT
  };

  // Sizes (in bytes) of DataTypes
  const A3M_UINT32 typeSize[] = { 1, 1, 2, 2, 4, 4, 4, 2, 2 };

  VertexBuffer::AttribFormat convertDataType( A3M_INT32 in )
  {
    static const VertexBuffer::AttribFormat attribFormat[TYPE_COUNT] =
    {
      VertexBuffer::ATTRIB_FORMAT_INT8,
      VertexBuffer::ATTRIB_FORMAT_UINT8,
      VertexBuffer::ATTRIB_FORMAT_INT16,
      VertexBuffer::ATTRIB_FORMAT_UINT16,
      VertexBuffer::ATTRIB_FORMAT_FLOAT,
      VertexBuffer::ATTRIB_FORMAT_FLOAT,
      VertexBuffer::ATTRIB_FORMAT_FLOAT,
      VertexBuffer::ATTRIB_FORMAT_FIXED,
      VertexBuffer::ATTRIB_FORMAT_FIXED
    };
    if( in < TYPE_COUNT )
    {
      return attribFormat[ in ];
    }
    else
    {
      return VertexBuffer::ATTRIB_FORMAT_FLOAT;
    }
  }

  IndexBuffer::Primitive convertPrimitiveType( A3M_UINT32 in )
  {
    static const IndexBuffer::Primitive primitive[PRIMITIVE_TYPE_COUNT] =
    {
      IndexBuffer::PRIMITIVE_POINTS,
      IndexBuffer::PRIMITIVE_LINES,
      IndexBuffer::PRIMITIVE_LINE_LOOP,
      IndexBuffer::PRIMITIVE_LINE_STRIP,
      IndexBuffer::PRIMITIVE_TRIANGLES,
      IndexBuffer::PRIMITIVE_TRIANGLE_STRIP,
      IndexBuffer::PRIMITIVE_TRIANGLE_FAN
    };
    if( in < PRIMITIVE_TYPE_COUNT )
    {
      return primitive[ in ];
    }
    else
    {
      return IndexBuffer::PRIMITIVE_TRIANGLES;
    }
  }

  // Up axis
  const A3M_UINT32 UP_AXIS_X = 0;
  const A3M_UINT32 UP_AXIS_Y = 1;
  const A3M_UINT32 UP_AXIS_Z = 2;

  // Skin-to-joint attachment
  struct JointAttachmentData
  {
    std::string jointName;
    Matrix4f inverseBindPoseTransform;
  };

  typedef std::vector<JointAttachmentData> JointAttachmentDataVector;

  // Per-skin rigging information
  struct SkinData
  {
    std::string name;

    std::string geometryName;
    A3M_INT32 jointsPerVertex;
    Matrix4f bindShapeTransform;
    JointAttachmentDataVector jointAttachments;
  };

  typedef std::vector<Solid::Ptr> SolidVector;
  typedef std::vector<std::string> StringVector;

  // Data for an instance of a skin controller within the scene graph
  struct SkinReferenceData
  {
    std::string name;
    SolidVector solids;
    StringVector skeletonNames;
  };

  // Light types
  enum LightType
  {
    AMBIENT,
    POINT,
    SPOT,
    DIRECTIONAL
  };

  // Light data as it appears in a GLO file.
  struct LightDataV0
  {
    A3M_UINT32 type;
    A3M_FLOAT colour[3];        //Lights have three colour components (no alpha)
    A3M_FLOAT attenuationStart;
    A3M_FLOAT attenuationEnd;
    A3M_FLOAT coneInner;
    A3M_FLOAT coneOuter;
  };

  struct LightData : public LightDataV0
  {
    A3M_FLOAT multiplier;
    A3M_UINT32 useAttenuation;
  };

  // Camera types
  enum CameraType
  {
    ORTHOGRAPHIC,
    PERSPECTIVE
  };

  // Camera data as it appears in a GLO file.
  struct CameraData
  {
    A3M_UINT32 type;
    A3M_UINT32 fovOrMagType;
    A3M_FLOAT fovOrMag;
    A3M_FLOAT zNear;
    A3M_FLOAT zFar;
  };

  // A vertex attribute type. This is only used whilst assembling meshes
  struct VertexAttribute
  {
    std::string name;

    A3M_UINT32 componentCount;
    A3M_UINT32 dataType;
    A3M_UINT32 offset;
    A3M_UINT32 stride;
  };

  // POD aspect of Sample which can be read in one go from a GLO file
  struct SamplerData
  {
    enum Type
    {
      TYPE_2D = 0,
      TYPE_CUBE = 1,
      TYPE_3D = 2,
      TYPE_NONE
    };
    A3M_UINT32 type;
    A3M_UINT32 wrapS;
    A3M_UINT32 wrapT;
    A3M_UINT32 minFilter;
    A3M_UINT32 maxFilter;
    A3M_UINT32 mipmap;
  };

  // Sampler
  struct Sampler
  {
    SamplerData data;
    std::string imageName;
  };

  // Material Aspect (used in Material struct below). Each aspect of a material
  // may be a sampler (texture) or a plain colour.
  struct MaterialAspect
  {
    a3m::Vector4f colour;
    Sampler sampler;
  };

  // Standard material. This material is more-or-less dictated by the COLLADA
  // standard (i.e. it is what we the GLO file creator has to work with)
  struct Material
  {
    MaterialAspect ambient;
    MaterialAspect diffuse;
    MaterialAspect specular;
    MaterialAspect emissive;
    MaterialAspect opacityAspect;
    A3M_FLOAT      shininess;
    A3M_FLOAT      opacity;
    A3M_UINT32     doubleSided;
    Sampler        bumpmap;
    MaterialAspect reflective;
    Sampler        refraction;
    Sampler        filterColour;
    Sampler        displacement;
    Sampler        gloss;
    Sampler        specularLevelMap;
    A3M_FLOAT      specularLevel;
    A3M_FLOAT      selfIllumination;
  };

  // Contains information needed to create a shader program.  We use this
  // structure because shader program creation must be delayed until material
  // animation channels have been read, so that we know exactly which parts of
  // the shader program we require.
  struct ShaderProgramData
  {
    std::string name;
    A3M_BOOL bump;
    A3M_BOOL invy;
    A3M_BOOL invsqroot;
    A3M_BOOL diff;
    A3M_BOOL spec;
    A3M_BOOL ambi;
    A3M_BOOL refl;
    A3M_BOOL cube;
    A3M_BOOL emis;
    A3M_BOOL skin;

    // All shader components are disabled until required.
    ShaderProgramData() :
      bump(A3M_FALSE), invy(A3M_FALSE), invsqroot(A3M_FALSE), diff(A3M_FALSE),
      spec(A3M_FALSE), ambi(A3M_FALSE), refl(A3M_FALSE), cube(A3M_FALSE),
      emis(A3M_FALSE), skin(A3M_FALSE)
    {
    }
  };

  // Utility function to enable transparency blending mode on an appearance.
  void enableTransparency( Appearance& appearance )
  {
    appearance.setBlendFactors(
      BLEND_SRC_ALPHA, BLEND_SRC_ALPHA_SATURATE,
      BLEND_ONE_MINUS_SRC_ALPHA, BLEND_ONE_MINUS_SRC_ALPHA );
  }

  typedef std::map< std::string, SceneNode::Ptr > SceneNodeMap;
  typedef std::map< SceneNode*, std::string > SceneNodeScopedNameMap;

  // Constructs a map of scene nodes by iterating through the scene graph.
  class SceneNodeMapper : public SceneNodeVisitor
  {
  public:
    SceneNodeMapper(SceneNodeScopedNameMap const& scopedNames) :
      m_scopedNames(scopedNames)
    {
    }

    void visit(SceneNode* node)
    {
      // Map by name
      m_nodes[node->getName()] = SceneNode::Ptr(node);

      // Map by scoped name
      SceneNodeScopedNameMap::const_iterator it = m_scopedNames.find(node);
      if (it != m_scopedNames.end())
      {
        m_nodes[it->second] = SceneNode::Ptr(node);
      }
    }

    SceneNode::Ptr getSceneNode(A3M_CHAR8 const* name)
    {
      SceneNodeMap::iterator it = m_nodes.find(name);
      return it == m_nodes.end() ? SceneNode::Ptr() : it->second;
    }

  private:
    SceneNodeMap m_nodes;
    SceneNodeScopedNameMap const& m_scopedNames;
  };

  A3M_BOOL isBlack( a3m::Vector4f const& c )
  {
    return (c.x == 0.f) && (c.y == 0.f) && (c.z == 0.f);
  }

  // Read an arbitrary type from an stream. This must be a POD (Plain Old Data)
  // type (hence is specialised below for string).
  template< typename T >
  A3M_BOOL readAny( Stream& stream, T& thing )
  {
    return stream.read( &thing, sizeof( T ) ) == sizeof( T );
  }

  // Specialization of read for std::string
  template<>
  A3M_BOOL readAny< std::string >( Stream& stream, std::string& str )
  {
    A3M_UINT32 byteCount;
    if( !readAny( stream, byteCount ) ) {return A3M_FALSE;}

    // To protect against an anomolous string length
    if( ( byteCount < 1 ) || ( byteCount > MAX_STRING_LENGTH ) ) {return A3M_FALSE;}

    std::vector<A3M_CHAR8> buffer( byteCount );
    stream.read( &buffer[0], byteCount );

    // Ensure the string is null terminated
    if( buffer[ byteCount - 1 ] != 0 ) {return A3M_FALSE;}

    str = &buffer[0];
    return A3M_TRUE;
  }

  /*
   * A utility class for loading GLO files. a GloLoader object's lifetime is
   * intended to last only for the duration of loading a single file.
   *
   * As we're in an anonymous namespace, this class is only for use in the
   * loadGloFile function.
   */
  class GloLoader
  {
  public:
    GloLoader( Stream& stream, AssetCachePool& pool,
               SceneNode::Ptr const& scene, std::string const& name )
      : m_stream( stream ),
        m_assetCachePool( pool ),
        m_glo( new Glo() ),
        m_scene( scene ),
        m_fileName( name ),
        m_gloVersion( 0, 0 ),
        m_authoringTool( UNKNOWN_AUTHORING_TOOL ),
        m_axisUp( UP_AXIS_Y ),
        m_rootNode( new SceneNode() )
    {
      m_chunkReaders[CHUNK_ID_GLO3] = &GloLoader::readModelImplementation;
      m_chunkReaders[CHUNK_ID_LGEO] = &GloLoader::readGeometryLibrary;
      m_chunkReaders[CHUNK_ID_LMAT] = &GloLoader::readMaterialLibrary;
      m_chunkReaders[CHUNK_ID_SCNE] = &GloLoader::readScene;
      m_chunkReaders[CHUNK_ID_GEOM] = &GloLoader::readGeometry;
      m_chunkReaders[CHUNK_ID_VSET] = &GloLoader::readVertexSet;
      m_chunkReaders[CHUNK_ID_PSET] = &GloLoader::readPrimitiveSet;
      m_chunkReaders[CHUNK_ID_VATT] = &GloLoader::readVertexAttribute;
      m_chunkReaders[CHUNK_ID_VBUF] = &GloLoader::readVertexBuffer;
      m_chunkReaders[CHUNK_ID_IBUF] = &GloLoader::readIndexBuffer;
      m_chunkReaders[CHUNK_ID_LCTL] = &GloLoader::readControllerLibrary;
      m_chunkReaders[CHUNK_ID_SKIN] = &GloLoader::readSkin;
      m_chunkReaders[CHUNK_ID_ATCH] = &GloLoader::readJointAttachment;
      m_chunkReaders[CHUNK_ID_NODE] = &GloLoader::readNode;
      m_chunkReaders[CHUNK_ID_GREF] = &GloLoader::readGeometryReference;
      m_chunkReaders[CHUNK_ID_SREF] = &GloLoader::readSkinReference;
      m_chunkReaders[CHUNK_ID_SMPL] = &GloLoader::readSampler;
      m_chunkReaders[CHUNK_ID_SMAT] = &GloLoader::readStandardMaterial;
      m_chunkReaders[CHUNK_ID_MATL] = &GloLoader::readMaterial;
      m_chunkReaders[CHUNK_ID_LANM] = &GloLoader::readAnimationLibrary;
      m_chunkReaders[CHUNK_ID_ANIM] = &GloLoader::readAnimation;
      m_chunkReaders[CHUNK_ID_CHAN] = &GloLoader::readAnimationChannel;
      m_chunkReaders[CHUNK_ID_LLGT] = &GloLoader::readLightLibrary;
      m_chunkReaders[CHUNK_ID_LGHT] = &GloLoader::readLight;
      m_chunkReaders[CHUNK_ID_LREF] = &GloLoader::readLightReference;
      m_chunkReaders[CHUNK_ID_INFO] = &GloLoader::readInfo;
      m_chunkReaders[CHUNK_ID_LCAM] = &GloLoader::readCameraLibrary;
      m_chunkReaders[CHUNK_ID_CAMA] = &GloLoader::readCamera;
      m_chunkReaders[CHUNK_ID_CREF] = &GloLoader::readCameraReference;
    }

    // Read in a GLO file. Returns a null pointer on failure.
    Glo::Ptr readModel()
    {
#ifdef A3M_USE_LIGHT_ZORDER
      if (a3m_drvb_f0() != 0)
      {
        return m_glo;
      }
#endif
      if (readChunk(CHUNK_ID_GLO3))
      {
        return m_glo;
      }

      return Glo::Ptr();
    }

  private:
    typedef std::map< A3M_UINT32, A3M_BOOL (GloLoader::*)() > ChunkReaderMap;

    ChunkReaderMap m_chunkReaders;

    // Types for libraries
    typedef std::map< std::string, std::vector< Mesh::Ptr > > MeshLib;
    typedef std::map< std::string, Appearance::Ptr > AppearanceLib;
    typedef std::map< std::string, SkinData > SkinDataLib;
    typedef std::map< std::string, LightData > LightDataLib;
    typedef std::map< std::string, CameraData > CameraDataLib;
    // Shader programs are mapped by appearance name
    typedef std::map< std::string, ShaderProgramData > ShaderProgramDataLib;

    // Storage of local (to a GLO file meshes, appearances and lights)
    MeshLib m_meshLib;
    AppearanceLib m_appearanceLib;
    SkinDataLib m_skinDataLib;
    LightDataLib m_lightDataLib;
    CameraDataLib m_cameraDataLib;
    ShaderProgramDataLib m_shaderProgramDataLib;

    typedef std::vector< SkinReferenceData > SkinReferenceDataVector;
    SkinReferenceDataVector m_skinReferences;

    SceneNodeMap m_nodes;  // Map of scene nodes by name
    SceneNodeScopedNameMap m_nodeScopedNames;  // Map of scoped scene node names
    Stream& m_stream;
    AssetCachePool& m_assetCachePool;
    Glo::Ptr m_glo;
    SceneNode::Ptr m_scene;  // External scene to which to apply animations
    std::string m_fileName;  // filename (used for logging errors)
    Version m_gloVersion;
    AuthoringTool m_authoringTool;

    // Temporary objects to pass values down through the chunk-reader hierarchy.
    A3M_FLOAT m_loopEnd;
    A3M_FLOAT m_loopStart;
    A3M_UINT32 m_axisUp;
    A3M_UINT32 m_chunkDataSize;
    AnimationController::Ptr m_animation;
    AnimationGroup::Ptr m_animationGroup;
    IndexBuffer::Ptr m_indexBuffer;
    JointAttachmentData* m_jointAttachment;
    Material m_material;
    Sampler* m_sampler;
    SceneNode::Ptr m_leafNode;
    SceneNode::Ptr m_parentNode;
    SceneNode::Ptr m_rootNode;
    VertexBuffer::AttribDescription m_vertexAttribute;
    VertexBuffer::Ptr m_vertexBuffer;
    A3M_UINT32 m_vertexCount;
    std::vector< A3M_BYTE > m_vertexData;
    std::vector< A3M_UINT16 > m_indexData;

    std::string getIdString(A3M_UINT32 id)
    {
      std::string string = "0000";
      string[0] = 0xff & id;
      string[1] = 0xff & (id >> 8);
      string[2] = 0xff & (id >> 16);
      string[3] = 0xff & (id >> 24);
      return string;
    }

    // Read an arbitrary type from an IReadFile object
    template< typename T >
    A3M_BOOL read( T& thing, const A3M_CHAR8* message )
    {
      if( !::readAny( m_stream, thing ) )
      {
        A3M_LOG_ERROR( "Error reading: %s in file: %s ",
                       message, m_fileName.c_str() );
        return A3M_FALSE;
      }
      else
      {
        return A3M_TRUE;
      }
    }

    /* Attempts to read a chunk of one of several given IDs.  If the chunk is
     * successfully read, or no more chunks exist, the function returns true.
     * If a chunk with a different ID is found, the function skips the chunk if
     * "optional" is set and returns true, or returns false if not.  In the
     * chunk is incomplete or invalid, the function returns false.
     */
    A3M_BOOL readChunk(A3M_UINT32 const* ids, A3M_BOOL optional = A3M_FALSE)
    {
      A3M_ASSERT(ids);

      A3M_UINT32 id;
      A3M_UINT32 expectedSize;

      if (!readAny(m_stream, id))
      {
        // No more chunks exist, so return true if the chunk is optional.
        if (optional)
        {
          return A3M_TRUE;
        }

        A3M_LOG_ERROR("Failed to read chunk ID");
        return A3M_FALSE;
      }

      // Check if any of the given IDs match.
      A3M_BOOL idMatches = A3M_FALSE;
      for (A3M_INT32 i = 0; ids[i] != 0; ++i)
      {
        if (ids[i] == id)
        {
          idMatches = A3M_TRUE;
          break;
        }
      }

      // Read size before validating ID, so we can optionally skip the chunk.
      if (!readAny(m_stream, expectedSize))
      {
        A3M_LOG_ERROR("Failed to read %s chunk size", getIdString(id).c_str());
        return A3M_FALSE;
      }

      if (!idMatches)
      {
        // Skip chunk if it's optional.
        if (optional)
        {
          m_stream.seek( m_stream.tell() + expectedSize );
          return A3M_TRUE;
        }

        // Build a list of the expected IDs for the error message.
        std::string idsString;
        for (A3M_INT32 i = 0; ids[i] != 0; ++i)
        {
          if (i > 0) { idsString += ", "; }
          idsString += getIdString(ids[i]);
        }

        std::string idString = getIdString(id);
        A3M_LOG_ERROR("Chunk ID \"%s\" does not match expected ID(s): \"%s\"",
                      idString.c_str(), idsString.c_str());
        return A3M_FALSE;
      }

      // Get the correct function to read this kind of chunk.
      ChunkReaderMap::iterator reader = m_chunkReaders.find(id);
      A3M_ASSERT(reader != m_chunkReaders.end());

      // Make sure to restore the size of the previous chunk afterwards
      A3M_UINT32 previousSize = m_chunkDataSize;
      m_chunkDataSize = expectedSize;

      // Read chunk, measuring actual size of chunk data.
      A3M_INT32 before = m_stream.tell();
      if (reader != m_chunkReaders.end() && !(this->*reader->second)()) {
        m_chunkDataSize = previousSize;
        return A3M_FALSE;
      }
      A3M_INT32 after = m_stream.tell();

      m_chunkDataSize = previousSize;

      // tell() returns -1 at the end of a file
      if (after == -1)
      {
        after = m_stream.size();
      }

      A3M_UINT32 size = after - before;

      if (size != expectedSize)
      {
        A3M_LOG_ERROR(
          "Size of %s chunk data (%d) does not match expected size (%d)",
          getIdString(id).c_str(), size, expectedSize);
        return A3M_FALSE;
      }

      return A3M_TRUE;
    }

    // Attempts to read a chunk of a single given ID.
    A3M_BOOL readChunk(A3M_UINT32 id, A3M_BOOL optional = A3M_FALSE)
    {
      A3M_UINT32 ids[] = {id, 0};
      return readChunk(ids, optional);
    }

    void setTexture2dProperty( a3m::Appearance& appearance,
                               Sampler const& sampler,
                               A3M_CHAR8 const* propertyName )
    {
      a3m::Texture2D::Ptr texture2d;

      const std::string& imageName = sampler.imageName;

      A3M_INT32 nameLength = imageName.size();

      //XXX Is this something that could be handled in the export pipeline?
      if ( nameLength >= 4 && imageName.substr(nameLength - 4, 4) == ".pvr" )
      {
        // Just attempt to load the image as PVR
        texture2d = m_assetCachePool.texture2DCache()->get( imageName.c_str() );
      }
      else
      {
        // Attempt to load the image with a .pvr extension
        std::string imageNamePvr = imageName.substr(0, imageName.find_last_of("."));
        imageNamePvr.append(".pvr");
        texture2d = m_assetCachePool.texture2DCache()->get( imageNamePvr.c_str() );

        if( !texture2d )
        {
          // Attempt to load originally specified image
          texture2d = m_assetCachePool.texture2DCache()->get( imageName.c_str() );
        }
      }

      // If failed to load texture, use the default chequerboard texture.
      if (!texture2d)
      {
        A3M_LOG_ERROR(
          "Failed to find 2d texture \"%s\"; using \"missing\" texture.",
          imageName.c_str());
        texture2d = m_assetCachePool.texture2DCache()->get("a3m#missing.png");
        A3M_ASSERT( texture2d );
      }

      appearance.setProperty( propertyName, texture2d );
    }

    void setTextureCubeProperty( a3m::Appearance& appearance,
                                 Sampler const& sampler,
                                 A3M_CHAR8 const* propertyName )
    {
      const std::string& imageName = sampler.imageName;
      a3m::TextureCube::Ptr textureCube = m_assetCachePool.textureCubeCache()->get(
                                            imageName.c_str());
      if (!textureCube)
      {
        A3M_LOG_ERROR(
          "Failed to find cube texture \"%s\"; using \"missing\" texture.",
          imageName.c_str());
        a3m::Texture2D::Ptr texture2d = m_assetCachePool.texture2DCache()->get(
                                          "a3m#missing.png");
        A3M_ASSERT( texture2d );
        appearance.setProperty( propertyName, texture2d );
      }
      else
      {
        appearance.setProperty( propertyName, textureCube );
      }
    }

    // Set texture property to corresponding map name in sampler.
    void setTextureProperty( a3m::Appearance& appearance,
                             Sampler const& sampler,
                             A3M_CHAR8 const* propertyName,
                             A3M_BOOL defaultToWhite = A3M_FALSE )
    {
      if( !sampler.imageName.empty() )
      {
        switch (sampler.data.type)
        {
        case SamplerData::TYPE_2D:
        {
          setTexture2dProperty(appearance, sampler, propertyName);
          break;
        }
        case SamplerData::TYPE_CUBE:
        {
          setTextureCubeProperty(appearance, sampler, propertyName);
          break;
        }
        default:
        {
          A3M_LOG_ERROR("Unsupported sampler type; using \"missing\" texture.");
          a3m::Texture2D::Ptr texture2d = m_assetCachePool.texture2DCache()->get(
                                            "a3m#missing.png");
          A3M_ASSERT( texture2d );
          appearance.setProperty( propertyName, texture2d );
          break;
        }
        }
      }
      else if (defaultToWhite)
      {
        // If texture is not specified, use a white texture.
        a3m::Texture2D::Ptr texture2d = m_assetCachePool.texture2DCache()->get(
                                          "a3m#white.png");
        A3M_ASSERT( texture2d );
        appearance.setProperty( propertyName, texture2d );
      }
    }

    // The following functions (making up the bulk of this source file) all read
    // a chunk from a GLO file. They all assume that the calling function has
    // read the chunk identifier (which is why they have been called) so the
    // first thing they do is read the chunk size.

    // Read a vertex attribute chunk
    A3M_BOOL readVertexAttribute()
    {
      VertexBuffer::AttribDescription& va = m_vertexAttribute;
      std::string name;

      if( !read( name, "vertex attribute name " ) ) {return A3M_FALSE;}
      if( !read( va.componentCount, "vertex component count " ) ) {return A3M_FALSE;}
      if( !read( va.type, "vertex attribute data type " ) ) {return A3M_FALSE;}
      if( !read( va.offset, "vertex attribute offset " ) ) {return A3M_FALSE;}
      if( !read( va.stride, "vertex attribute stride " ) ) {return A3M_FALSE;}

      va.name = name;
      va.type = convertDataType( va.type );

      if( m_gloVersion >= VERSION_SKINNING_SUPPORTED )
      {
        A3M_UINT32 normalize;
        if( !read( normalize, "vertex attribute normalize " ) ) {return A3M_FALSE;}
        va.normalize = (normalize != 0);
      }
      else
      {
        // Prior to the addition of skinning support, this value was undefined.
        // This fixes the bug, as luckily all other integer attributes should
        // have been normalized.
        va.normalize = A3M_TRUE;
      }

      return A3M_TRUE;
    }

    A3M_BOOL readVertexBuffer()
    {
      m_vertexData.resize( m_chunkDataSize );

      if( m_stream.read( &m_vertexData[0], m_chunkDataSize ) !=
          static_cast<A3M_INT32>(m_chunkDataSize) )
      {
        A3M_LOG_ERROR( "vertex buffer read failed in %s", m_fileName.c_str() );
        return A3M_FALSE;
      }

      return A3M_TRUE;
    }

    // Read a vertex set chunk
    A3M_BOOL readVertexSet()
    {
      if( !read( m_vertexCount, "vertex count " ) ) {return A3M_FALSE;}

      A3M_UINT32 attributeCount;
      if( !read( attributeCount, "attribute count " ) ) {return A3M_FALSE;}

      std::vector< VertexBuffer::AttribDescription > attributes( attributeCount );

      for( A3M_UINT32 i = 0; i != attributeCount; ++i )
      {
        if( !readChunk( CHUNK_ID_VATT ) ) {return A3M_FALSE;}
        attributes[ i ] = m_vertexAttribute;
      }

      if( !readChunk( CHUNK_ID_VBUF ) ) {return A3M_FALSE;}

      m_vertexBuffer = m_assetCachePool.vertexBufferCache()->create();
      m_vertexBuffer->setAllAttributes( &attributes[0], attributes.size(),
                                        &m_vertexData[0], m_vertexData.size() );

      return A3M_TRUE;
    }

    // Read an index buffer chunk
    A3M_BOOL readIndexBuffer()
    {
      A3M_UINT32 indexType;
      if( !read( indexType, "index buffer type " ) ) {return A3M_FALSE;}

      if( indexType != UNSIGNED_SHORT )
      {
        A3M_LOG_ERROR( "Only 16-bit indices supported (%s)", m_fileName.c_str() );
        return A3M_FALSE;
      }

      m_indexData.resize( ( m_chunkDataSize - sizeof( indexType ) ) /
                          sizeof( A3M_UINT16 ) );

      if( !m_stream.read( &m_indexData[0], m_chunkDataSize - sizeof( indexType ) ) )
      {
        A3M_LOG_ERROR( "Failed to read indices in %s", m_fileName.c_str() );
        return A3M_FALSE;
      }
      return A3M_TRUE;
    }

    // Read a primitive set chunk
    A3M_BOOL readPrimitiveSet()
    {
      A3M_UINT32 primitiveType;
      if( !read( primitiveType, "primitive type " ) ) {return A3M_FALSE;}

      A3M_UINT32 ibufChunkCount;
      if( !read( ibufChunkCount, "Index buffer chunk count " ) ) {return A3M_FALSE;}

      IndexBuffer::Primitive primitive = convertPrimitiveType( primitiveType );

      if( ibufChunkCount == 0 )
      {
        m_indexBuffer = m_assetCachePool.indexBufferCache()->create(
                            *m_assetCachePool.indexBufferCache(),
                            primitive, m_vertexCount );
      }
      else if( ibufChunkCount == 1 )
      {
        if( !readChunk( CHUNK_ID_IBUF ) ) {return A3M_FALSE;}

        m_indexBuffer = m_assetCachePool.indexBufferCache()->create(
                            primitive, m_indexData.size(), &m_indexData[0] );
      }
      else
      {
        A3M_LOG_ERROR( "Unexpected index buffer chunk count in %s",
                       m_fileName.c_str() );
        return A3M_FALSE;
      }

      return A3M_TRUE;
    }

    // Read a geometry chunk
    A3M_BOOL readGeometry()
    {
      std::string name;
      if( !read( name, "geometry chunk name " ) ) {return A3M_FALSE;}

      if( !readChunk( CHUNK_ID_VSET ) ) {return A3M_FALSE;}

      A3M_UINT32 primitiveSetCount;
      if( !read( primitiveSetCount, "PSET count " ) ) {return A3M_FALSE;}

      for( A3M_UINT32 i = 0; i != primitiveSetCount; ++i )
      {
        if( !readChunk( CHUNK_ID_PSET ) ) {return A3M_FALSE;}

        if( m_indexBuffer && m_vertexBuffer )
        {
          m_meshLib[ name ].push_back( m_assetCachePool.meshCache()->create(
                                         MeshHeader(), m_indexBuffer, m_vertexBuffer ) );
        }
      }
      return A3M_TRUE;
    }

    // Read the geometry library chunk
    A3M_BOOL readGeometryLibrary()
    {
      A3M_UINT32 geomCount;
      if( !read( geomCount, "geometry count " ) ) {return A3M_FALSE;}

      for( A3M_UINT32 i = 0; i != geomCount; ++i )
      {
        if( !readChunk( CHUNK_ID_GEOM, A3M_TRUE ) ) {return A3M_FALSE;}
      }

      return A3M_TRUE;
    }

    // Read a joint attachment chunk
    A3M_BOOL readJointAttachment()
    {
      if( !read( m_jointAttachment->jointName, "joint name " ) ) {return A3M_FALSE;}
      if( !read( m_jointAttachment->inverseBindPoseTransform,
                 "inverse bind pose transform " ) ) {return A3M_FALSE;}

      return A3M_TRUE;
    }

    // Read a skin chunk
    A3M_BOOL readSkin()
    {
      SkinData skin;
      A3M_UINT32 attachmentCount;

      if( !read( skin.name, "skin name " ) ) {return A3M_FALSE;}
      if( !read( skin.geometryName, "geometry name " ) ) {return A3M_FALSE;}
      if( !read( skin.jointsPerVertex, "joints per vertex " ) ) {return A3M_FALSE;}
      if( !read( skin.bindShapeTransform, "bind shape transform " ) ) {return A3M_FALSE;}
      if( !read( attachmentCount, "attachment count " ) ) {return A3M_FALSE;}

      skin.jointAttachments.resize( attachmentCount );
      for( A3M_UINT32 i = 0; i < attachmentCount; ++i )
      {
        m_jointAttachment = &skin.jointAttachments[i];
        if (!readChunk(CHUNK_ID_ATCH, A3M_TRUE)) {return A3M_FALSE;}
      }

      m_skinDataLib[ skin.name ] = skin;
      return A3M_TRUE;
    }

    // Read the controller library chunk
    A3M_BOOL readControllerLibrary()
    {
      A3M_UINT32 controllerCount;
      if( !read( controllerCount, "controller count " ) ) {return A3M_FALSE;}

      for( A3M_UINT32 i = 0; i != controllerCount; ++i )
      {
        if( !readChunk( CHUNK_ID_SKIN, A3M_TRUE )) {return A3M_FALSE;}
      }

      return A3M_TRUE;
    }

    // Read a light chunk
    A3M_BOOL readLight()
    {
      LightData light;
      std::string name;
      if( !read( name, "light name " ) ) {return A3M_FALSE;}

      if( m_gloVersion >= Version( 0, 10 ) )
      {
        if( !read( light, "light data " ) ) {return A3M_FALSE;}
      }
      else
      {
        LightDataV0& dataV0 = light;
        if( !read( dataV0, "light data " ) ) {return A3M_FALSE;}
        light.useAttenuation = 1;
        light.multiplier = 1.f;
      }

      m_lightDataLib[ name ] = light;
      return A3M_TRUE;
    }

    // Read the light library chunk
    A3M_BOOL readLightLibrary()
    {
      A3M_UINT32 lightCount;
      if( !read( lightCount, "light count " ) ) {return A3M_FALSE;}

      for( A3M_UINT32 i = 0; i != lightCount; ++i )
      {
        if( !readChunk( CHUNK_ID_LGHT, A3M_TRUE ) ) {return A3M_FALSE;}
      }
      return A3M_TRUE;
    }

    // Read a camera chunk
    A3M_BOOL readCamera()
    {
      CameraData camera;
      std::string name;
      if( !read( name, "camera name " ) ) {return A3M_FALSE;}
      if( !read( camera, "camera data " ) ) {return A3M_FALSE;}

      m_cameraDataLib[ name ] = camera;
      return A3M_TRUE;
    }

    // Read the camera library chunk
    A3M_BOOL readCameraLibrary()
    {
      A3M_UINT32 cameraCount;
      if( !read( cameraCount, "camera count " ) ) {return A3M_FALSE;}

      for( A3M_UINT32 i = 0; i != cameraCount; ++i )
      {
        if( !readChunk( CHUNK_ID_CAMA, A3M_TRUE ) ) {return A3M_FALSE;}
      }
      return A3M_TRUE;
    }

    // Read a sampler chunk whose existance is indicated by a leading flag
    A3M_BOOL readOptionalSampler( Sampler& sampler )
    {
      m_sampler = &sampler;

      A3M_UINT32 samplerFlag;
      if( !read( samplerFlag, "material sampler flag " ) ) {return A3M_FALSE;}

      if( samplerFlag )
      {
        return readChunk( CHUNK_ID_SMPL );
      }
      return A3M_TRUE;
    }

    // Read a sampler chunk
    A3M_BOOL readSampler()
    {
      if( !read( m_sampler->data, "sampler data " ) ) {return A3M_FALSE;}
      if( !read( m_sampler->imageName, "sampler image name " ) ) {return A3M_FALSE;}
      return A3M_TRUE;
    }

    // Read a material aspect (part of a material chunk - *not a chunk itself*)
    A3M_BOOL readMaterialAspect( MaterialAspect& aspect )
    {
      A3M_FLOAT floatColour[4];
      if( !read( floatColour, "material colour " ) ) {return A3M_FALSE;}

      aspect.colour = Vector4f(
                        floatColour[0], floatColour[1], floatColour[2], floatColour[3] );

      return readOptionalSampler( aspect.sampler );
    }

    // Read a standard material chunk
    A3M_BOOL readStandardMaterial()
    {
      Material& m = m_material;

      if( !readMaterialAspect( m.ambient ) ) {return A3M_FALSE;}
      if( !readMaterialAspect( m.diffuse ) ) {return A3M_FALSE;}
      if( !readMaterialAspect( m.specular ) ) {return A3M_FALSE;}
      if( !read( m.shininess, "material shininess " ) ) {return A3M_FALSE;}
      if( !readMaterialAspect( m.emissive ) ) {return A3M_FALSE;}
      if( !readMaterialAspect( m.opacityAspect ) ) {return A3M_FALSE;}
      if( !read( m.opacity, "material opacity " ) ) {return A3M_FALSE;}

      if( m_gloVersion >= Version( 0, 6 ) )
      {
        if( !read( m.doubleSided, "material double sided " ) ) {return A3M_FALSE;}
      }
      else
      {
        m.doubleSided = A3M_FALSE;
      }

      // Added bump mapped, reflective and refractive materials.
      if( m_gloVersion >= Version( 0, 7 ) )
      {
        if( !readOptionalSampler( m.bumpmap ) ) {return A3M_FALSE;}
        if( !readMaterialAspect( m.reflective ) ) {return A3M_FALSE;}
        if( !readOptionalSampler( m.refraction ) ) {return A3M_FALSE;}
      }
      else
      {
        //no data for these samplers
        m.bumpmap.data.type = SamplerData::TYPE_NONE;
        m.refraction.data.type = SamplerData::TYPE_NONE;
      }

      // Added filter colour, displacement and gloss.
      if( m_gloVersion >= Version( 0, 8 ) )
      {
        if( !readOptionalSampler( m.filterColour ) ) {return A3M_FALSE;}
        if( !readOptionalSampler( m.displacement ) ) {return A3M_FALSE;}
        if( !readOptionalSampler( m.gloss ) ) {return A3M_FALSE;}
      }
      else
      {
        //no data for these samplers
        m.filterColour.data.type = SamplerData::TYPE_NONE;
        m.displacement.data.type = SamplerData::TYPE_NONE;
        m.gloss.data.type = SamplerData::TYPE_NONE;
      }

      // Added specular map and level, and self illumination.
      if( m_gloVersion >= Version( 0, 9 ) )
      {
        if( !readOptionalSampler( m.specularLevelMap ) ) {return A3M_FALSE;}
        if( !read( m.specularLevel, "material specular level " ) ) {return A3M_FALSE;}
        if( !read( m.selfIllumination, "material illumination " ) ) {return A3M_FALSE;}
      }
      else
      {
        //no data for these material properties
        m.gloss.data.type = SamplerData::TYPE_NONE;
        m.specularLevel = 0.0;
        m.selfIllumination = 0.0;
      }

      return A3M_TRUE;
    }

    // Read a material chunk
    A3M_BOOL readMaterial()
    {
      m_material = Material();

      std::string name;
      if( !read( name, "material name " ) ) {return A3M_FALSE;}

      A3M_UINT32 techniqueCount;
      if( !read( techniqueCount, "material technique count " ) ) {return A3M_FALSE;}

      for( A3M_UINT32 i = 0; i != techniqueCount; i++ )
      {
        if( !readChunk( CHUNK_ID_SMAT ) ) {return A3M_FALSE;}

        Material& m = m_material;

        Appearance::Ptr appearance( new Appearance() );
        appearance->setName( name.c_str() );

        // Create a new shader program data object.  Shader programs will be
        // created after the material animations have been read.
        ShaderProgramData shaderProgramData;

        // Load fallback shader for very old Glo files.
        shaderProgramData.name = "a3m#fallback.sp";

        if( m_gloVersion >= Version( 0, 7 ) )
        {
          // Choose base shader.
          // Use per-pixel lighting if the material uses a bump map or the
          // artist has explicitly asked for it (by including "mtk_ppl" in the
          // material name). Otherwise use per-vertex lighting.
          if( m.bumpmap.imageName.size() )
          {
            shaderProgramData.name = "a3m#max_ppl.sp";
            shaderProgramData.bump = A3M_TRUE;

            if( m_authoringTool == AUTODESK_3DS_MAX )
            {
              shaderProgramData.invy = A3M_TRUE;
            }
          }
          else if( name.find( "mtk_ppl" ) != std::string::npos )
          {
            shaderProgramData.name = "a3m#max_ppl.sp";
          }
          else
          {
            shaderProgramData.name = "a3m#max_pvl.sp";
          }

          // Append the macro names to be predefined in the shaders, and create
          // properties associated with these different features.
          if( m_authoringTool == AUTODESK_3DS_MAX )
          {
            shaderProgramData.invsqroot = A3M_TRUE;
          }
          if( !isBlack( m.diffuse.colour ) )
          {
            shaderProgramData.diff = A3M_TRUE;
          }
          if( ( m.specularLevel > 0.f ) && !isBlack( m.specular.colour ) )
          {
            shaderProgramData.spec = A3M_TRUE;
          }
          if( !isBlack( m.ambient.colour ) )
          {
            shaderProgramData.ambi = A3M_TRUE;
          }
          if( m.reflective.sampler.imageName.size() )
          {
            shaderProgramData.refl = A3M_TRUE;

            //is the reflective sampler a cube-map?
            if( m.reflective.sampler.data.type == SamplerData::TYPE_CUBE )
            {
              shaderProgramData.cube = A3M_TRUE;
            }
          }
          if( !isBlack( m.emissive.colour ) )
          {
            shaderProgramData.emis = A3M_TRUE;
          }
        }

        using namespace properties;

        appearance->setProperty( M_DIFFUSE_COLOUR, m.diffuse.colour );
        appearance->setProperty( M_AMBIENT_COLOUR, m.ambient.colour );
        appearance->setProperty( M_EMISSIVE_COLOUR, m.emissive.colour );
        appearance->setProperty( M_SPECULAR_COLOUR, m.specular.colour );
        appearance->setProperty( M_SHININESS, m.shininess );
        appearance->setProperty( M_SPECULAR_LEVEL, m.specularLevel );
        appearance->setProperty( M_OPACITY, m.opacity );
        appearance->setProperty( M_SELF_ILLUMINATION, m.selfIllumination );

        // If material uses an opacity map, assume it is the same as the
        // diffuse map, and set blending modes accordingly.
        if( m.opacityAspect.sampler.imageName.size() )
        {
          enableTransparency( *appearance );
          appearance->setProperty( M_DIFFUSE_TEXTURE,
                                   m_assetCachePool.texture2DCache()->get(
                                     m.opacityAspect.sampler.imageName.c_str() ) );
        }
        else
        {
          setTextureProperty(*appearance, m.diffuse.sampler, M_DIFFUSE_TEXTURE, A3M_TRUE);
        }

        setTextureProperty(*appearance, m.specular.sampler, M_SPECULAR_TEXTURE, A3M_TRUE);
        setTextureProperty(*appearance, m.ambient.sampler, M_AMBIENT_TEXTURE, A3M_TRUE);
        setTextureProperty(*appearance, m.emissive.sampler, M_EMISSIVE_TEXTURE, A3M_TRUE);

        if( m.opacity < 1.f )
        {
          enableTransparency( *appearance );
        }

        if( m.doubleSided > 0)
        {
          appearance->setCullingMode( CULL_NONE );
        }

        setTextureProperty(*appearance, m.bumpmap, M_BUMP_TEXTURE);
        setTextureProperty(*appearance, m.reflective.sampler, M_REFLECTION_TEXTURE);
        setTextureProperty(*appearance, m.refraction, M_REFRACTION_TEXTURE);
        setTextureProperty(*appearance, m.filterColour, M_FILTER_COLOUR_TEXTURE);
        setTextureProperty(*appearance, m.displacement, M_DISPLACEMENT_TEXTURE);
        setTextureProperty(*appearance, m.gloss, M_GLOSS_TEXTURE);
        setTextureProperty(*appearance, m.specularLevelMap, M_SPECULAR_LEVEL_TEXTURE);

        m_appearanceLib[ name ] = appearance;
        m_shaderProgramDataLib[ name ] = shaderProgramData;
      }

      return A3M_TRUE;
    }

    void createShaderPrograms()
    {
      // For the shader program of each appearance, construct an asset name
      // using the enabled components, and load and apply the program.
      for ( ShaderProgramDataLib::iterator it = m_shaderProgramDataLib.begin();
            it != m_shaderProgramDataLib.end(); ++it )
      {
        std::string appearanceName = it->first;
        ShaderProgramData& data = it->second;

        std::string shaderName = data.name;

        if( data.bump ) { shaderName += "$BUMP"; }
        if( data.invy ) { shaderName += "$INVY"; }
        if( data.invsqroot ) { shaderName += "$INVSQROOT"; }
        if( data.diff ) { shaderName += "$DIFF"; }
        if( data.spec ) { shaderName += "$SPEC"; }
        if( data.ambi ) { shaderName += "$AMBI"; }
        if( data.refl ) { shaderName += "$REFL"; }
        if( data.cube ) { shaderName += "$CUBE"; }
        if( data.emis ) { shaderName += "$EMIS"; }
        if( data.skin ) { shaderName += "$SKIN"; }

        m_appearanceLib[ appearanceName ]->setShaderProgram(
          m_assetCachePool.shaderProgramCache()->get( shaderName.c_str() ) );
      }
    }

    // Read the material library chunk
    A3M_BOOL readMaterialLibrary()
    {
      A3M_UINT32 materialCount;
      if( !read( materialCount, "material count " ) ) {return A3M_FALSE;}

      for( A3M_UINT32 i = 0; i != materialCount; ++i )
      {
        if( !readChunk( CHUNK_ID_MATL ) ) {return A3M_FALSE;}
      }

      return A3M_TRUE;
    }

    // Read a light reference chunk
    A3M_BOOL readLightReference()
    {
      std::string name;
      if( !read( name, "light reference name " ) ) {return A3M_FALSE;}

      LightData lightData = m_lightDataLib[ name ];

      Colour4f colour( lightData.colour[0],
                       lightData.colour[1],
                       lightData.colour[2], 1.f );

      Light::Ptr light( new Light() );
      light->setLightName( name.c_str() );

      light->setColour( colour );

      if( m_authoringTool == BLENDER )
      {
        light->setAmbientLevel( 0.0f );
      }
      else // for 3ds max and default
      {
        light->setAmbientLevel( 0.1f );
      }

      light->setAttenuationNear( lightData.attenuationStart );
      light->setAttenuationFar( lightData.attenuationEnd );
      light->setSpotInnerAngle( degrees( lightData.coneInner ) );
      light->setSpotOuterAngle( degrees( lightData.coneOuter ) );
      light->setIntensity( lightData.multiplier );
      light->setIsAttenuated( lightData.useAttenuation != 0 );

      switch( lightData.type )
      {
      case AMBIENT:
        A3M_LOG_WARN(
          "Ambient light type not supported; defaulting to point light.");
        // Fall through to POINT case statement
      case POINT:
        light->setLightType( Light::LIGHTTYPE_OMNI );
        break;
      case SPOT:
        light->setLightType( Light::LIGHTTYPE_SPOT );
        break;
      case DIRECTIONAL:
        light->setLightType( Light::LIGHTTYPE_DIRECTIONAL );
        break;
      }

      m_leafNode = light;
      return A3M_TRUE;
    }

    // Read a camera reference chunk
    A3M_BOOL readCameraReference()
    {
      std::string name;
      if( !read( name, "camera reference name " ) ) {return A3M_FALSE;}

      CameraData& cameraData = m_cameraDataLib[ name ];
      Camera::Ptr camera( new Camera() );

      switch (cameraData.type)
      {
      case ORTHOGRAPHIC:
        // The "mag" value represents a magnification (scale) factor applied
        // to the default viewport extents of 2, hence the multiplication.
        camera->setProjectionType(Camera::ORTHOGRAPHIC);
        camera->setWidth( cameraData.fovOrMag * 2.0f );
        break;

      case PERSPECTIVE:
        camera->setProjectionType(Camera::PERSPECTIVE);
        camera->setFov( degrees( cameraData.fovOrMag ) );
        break;
      }

      camera->setNear( cameraData.zNear );
      camera->setFar( cameraData.zFar );

      m_leafNode = camera;
      return A3M_TRUE;
    }

    // Read a geometry reference chunk
    A3M_BOOL readGeometryReference()
    {
      std::string name;
      A3M_UINT32 materialCount;

      if( !read( name, "geometry reference name " ) ) {return A3M_FALSE;}
      if( !read( materialCount, "material count " ) ) {return A3M_FALSE;}

      std::vector< Mesh::Ptr >& meshes = m_meshLib[ name.c_str() ];
      Solid::Ptr solid( new Solid() );

      for( A3M_UINT32 i = 0; i != materialCount; ++i )
      {
        std::string materialName;
        if( !read( materialName, "material name " ) ) {return A3M_FALSE;}

        Solid::Ptr meshSolid = solid;
        if( i > 0 )
        {
          meshSolid = Solid::Ptr( new Solid() );
          meshSolid->setParent( solid );
        }
        meshSolid->setMesh( meshes[i] );

        // If no material is assigned to the geometry, assign it a default
        // appearance.  Solids are always expected to have appearances.
        Appearance::Ptr appearance = m_appearanceLib[ materialName ];

        if (!appearance)
        {
          A3M_LOG_WARN("Geometry has no material; using default appearance.");
          appearance = loadAppearance( m_assetCachePool, "a3m#default_pvl.ap" );
          A3M_ASSERT( appearance );
        }

        meshSolid->setAppearance( appearance );
      }

      m_leafNode = solid;
      return A3M_TRUE;
    }

    // Read a skin controller reference chunk
    A3M_BOOL readSkinReference()
    {
      SkinReferenceData data;
      std::string name;
      A3M_UINT32 skeletonCount;
      A3M_UINT32 materialCount;

      if( !read( name, "skin reference name " ) ) {return A3M_FALSE;}
      if( !read( skeletonCount, "skeleton node count " ) ) {return A3M_FALSE;}

      data.name = name;
      SkinData& skinData = m_skinDataLib[ name.c_str() ];
      std::vector< Mesh::Ptr >& meshes = m_meshLib[ skinData.geometryName.c_str() ];

      for( A3M_UINT32 i = 0; i != skeletonCount; ++i )
      {
        std::string skeletonName;
        if( !read( skeletonName, "skeleton node name " ) ) {return A3M_FALSE;}
        data.skeletonNames.push_back(skeletonName);
      }

      if( !read( materialCount, "material count " ) ) {return A3M_FALSE;}

      Solid::Ptr solid( new Solid() );

      for( A3M_UINT32 i = 0; i != materialCount; ++i )
      {
        std::string materialName;
        if( !read( materialName, "material name " ) ) {return A3M_FALSE;}
        Solid::Ptr meshSolid = solid;
        if( i > 0 )
        {
          meshSolid = Solid::Ptr( new Solid() );
          meshSolid->setParent( solid );
        }

        data.solids.push_back(meshSolid);

        meshSolid->setMesh( meshes[i] );
        meshSolid->setBindShapeTransform(skinData.bindShapeTransform);
        meshSolid->setJointsPerVertex(skinData.jointsPerVertex);

        // If no material is assigned to the geometry, assign it a default
        // appearance.  Solids are always expected to have appearances.
        Appearance::Ptr appearance = m_appearanceLib[ materialName ];

        if (!appearance)
        {
          A3M_LOG_WARN("Geometry has no material; using default appearance.");
          appearance = loadAppearance( m_assetCachePool, "a3m#default_pvl.ap" );
          A3M_ASSERT( appearance );
        }

        // Enable skinning in the material's shader program
        m_shaderProgramDataLib[ materialName ].skin = A3M_TRUE;

        meshSolid->setAppearance( appearance );
      }

      m_skinReferences.push_back(data);

      m_leafNode = solid;
      return A3M_TRUE;
    }

    // Links the joints of a controller reference chunk
    A3M_BOOL linkSkinReference( SkinReferenceData const& data )
    {
      SkinData& skinData = m_skinDataLib[ data.name.c_str() ];

      // Build a map of joint nodes
      SceneNodeMapper mapper(m_nodeScopedNames);
      for (A3M_UINT32 i = 0; i < data.skeletonNames.size(); ++i)
      {
        SceneNodeMap::iterator it = m_nodes.find(data.skeletonNames[i]);
        if (it == m_nodes.end()) {return A3M_FALSE;}
        visitScene(mapper, *(it->second));
      }

      // Attach all the joints to the solids.
      for (A3M_UINT32 i = 0; i < skinData.jointAttachments.size(); ++i)
      {
        JointAttachmentData const& attachment = skinData.jointAttachments[i];
        SceneNode::Ptr jointNode = mapper.getSceneNode(attachment.jointName.c_str());

        if (!jointNode)
        {
          A3M_LOG_ERROR("Joint node \"%s\" not found.", attachment.jointName.c_str());
          return A3M_FALSE;
        }

        for (A3M_UINT32 j = 0; j < data.solids.size(); ++j)
        {
          data.solids[j]->attachJoint(jointNode, attachment.inverseBindPoseTransform);
        }
      }

      return A3M_TRUE;
    }

    // Read a node chunk from the file and create a corresponding node in the
    // a3m scene graph. Calls itself recursively to create child nodes.
    A3M_BOOL readNode()
    {
      std::string name;
      std::string scopedName;
      Matrix4f localTransform;
      A3M_FLOAT translation[3];
      A3M_FLOAT rotation[4];
      A3M_FLOAT scale[3];

      if( !read( name, "node name " ) ) {return A3M_FALSE;}

      if( m_gloVersion >= VERSION_SKINNING_SUPPORTED )
      {
        if( !read( scopedName, "node scoped name " ) ) {return A3M_FALSE;}
      }

      if( m_gloVersion >= Version( 0, 12 ) )
      {
        if( !read( localTransform, "node transform " ) ) {return A3M_FALSE;}
      }

      if( !read( translation, "node translation " ) ) {return A3M_FALSE;}
      if( !read( rotation, "node rotation " ) ) {return A3M_FALSE;}
      if( !read( scale, "node scale " ) ) {return A3M_FALSE;}

      Vector3f pos( translation[0], translation[1], translation[2] );
      Vector3f scaleV( scale[0], scale[1], scale[2] );
      Vector3f axis( rotation[0], rotation[1], rotation[2] );
      Anglef angle = radians( rotation[3] );

      // If the Glo file content has been authored with a different 'up' vector,
      // rotate the scene node to comply with 'Y up' convention.
      if( !m_parentNode->getParent() )
      {
        switch( m_axisUp )
        {
        case UP_AXIS_X:
          axis = Vector3f( 0.f, 0.f, 1.f );
          setRadians( angle, 0.5f * a3m::FLOAT_PI );
          break;
        case UP_AXIS_Y:
          break;
        case UP_AXIS_Z:
          axis = Vector3f( 1.f, 0.f, 0.f );
          setRadians( angle, -0.5f * a3m::FLOAT_PI );
          break;
        default:
          A3M_LOG_ERROR( "Unrecognized up-axis option in %s", m_fileName.c_str() );
          return A3M_FALSE;
          break;
        }
      }

      A3M_UINT32 leafNodeChunkIds[] =
      {
        CHUNK_ID_GREF, CHUNK_ID_SREF, CHUNK_ID_LREF, CHUNK_ID_CREF
      };

      // Disable unsupported leaf chunks
      if( m_gloVersion < VERSION_SKINNING_SUPPORTED )
      {
        leafNodeChunkIds[1] = 0; // CHUNK_ID_SREF

        if ( m_gloVersion < Version( 0, 11 ) )
        {
          leafNodeChunkIds[3] = 0; // CHUNK_ID_CREF
        }
      }

      SceneNode::Ptr node;
      for( A3M_UINT32 i = 0; i < sizeof( leafNodeChunkIds ) / sizeof( A3M_UINT32 ); ++i )
      {
        if( leafNodeChunkIds[i] != 0 )
        {
          A3M_UINT32 leafCount;
          if( !read( leafCount, "leaf node count " ) ) {return A3M_FALSE;}

          if( leafCount == 1 )
          {
            if( !readChunk( leafNodeChunkIds[i] ) ) {return A3M_FALSE;}
            node = m_leafNode;
          }
        }
      }

      if( !node )
      {
        node = SceneNode::Ptr( new SceneNode() );
      }
      node->setParent( m_parentNode );
      node->setPosition( pos );
      node->setRotation( Quaternionf( axis, angle ) );
      node->setScale( scaleV );
      node->setName( name.c_str() );

      // The root transformation might have been changed above, so don't set
      // the cached version from the file.
      if( ( m_parentNode->getParent() ) && ( m_gloVersion >= Version( 0, 12 ) ) )
      {
        node->setLocalTransform( localTransform );
      }

      if( m_gloVersion >= VERSION_SKINNING_SUPPORTED )
      {
        // Store a map of nodes for faster joint linking.
        m_nodes[name] = node;

        // Store a map of scoped names, since the SceneNode object doesn't have
        // a corresponding field.  Do not bother storing empty strings.
        if (!scopedName.empty())
        {
          m_nodeScopedNames[node.get()] = scopedName;
        }
      }

      A3M_UINT32 childCount;
      if( !read( childCount, "node child count " ) ) {return A3M_FALSE;}
      for( A3M_UINT32 i = 0; i != childCount; ++i )
      {
        m_parentNode = node;
        if( !readChunk( CHUNK_ID_NODE, A3M_TRUE ) ) {return A3M_FALSE;}
      }

      return A3M_TRUE;
    }

    // Read a scene chunk
    A3M_BOOL readScene()
    {
      std::string name;
      if( !read( name, "scene name " ) ) {return A3M_FALSE;}

      m_parentNode = m_rootNode;
      return readChunk(CHUNK_ID_NODE);
    }

    SceneNode::Ptr findNode( A3M_CHAR8 const* name, SceneNode::Ptr const& root )
    {
      SceneNode::Ptr node = root->find( name );
      if( !node && m_scene )
      {
        node = m_scene->find( name );
      }
      return node;
    }

    class AppearanceFinder : public SceneNodeVisitor
    {
    public:
      AppearanceFinder( A3M_CHAR8 const* name ) :
        m_name(name)
      {
      }

      void visit( Solid* solid )
      {
        if( !m_appearance )
        {
          Appearance::Ptr const& appearance = solid->getAppearance();

          if( appearance->getName() == m_name )
          {
            m_appearance = appearance;
          }
        }
      };

      Appearance::Ptr const& appearance() const { return m_appearance; }

    private:
      std::string m_name;
      Appearance::Ptr m_appearance;
    };

    class LightFinder : public SceneNodeVisitor
    {
    public:
      LightFinder( A3M_CHAR8 const* name ) :
        m_name(name)
      {
      }

      void visit( Light* light )
      {
        if( !m_light )
        {
          if( light->getLightName() == m_name )
          {
            m_light = SharedPtr<Light>(light);
          }
        }
      };

      Light::Ptr const& light() const { return m_light; }

    private:
      std::string m_name;
      Light::Ptr m_light;
    };

    template<typename T>
    void addChannel(
      A3M_CHAR8* buffer,
      A3M_UINT32 keyCount,
      typename Animator<T>::Ptr const& animator)
    {
      m_animationGroup->addAnimation(
        createAnimationChannel< T >(createAnimationKeySequence(
                                      reinterpret_cast< AnimationKey< T >* >( buffer ),
                                      keyCount ),
                                    animator ) );
    }

    A3M_BOOL readAnimationChannel()
    {
      std::string targetType;
      std::string targetName;
      std::string propertyName;
      A3M_UINT32 interpMethod;
      A3M_UINT32 coeffCount;
      A3M_UINT32 coeffDataType;
      A3M_UINT32 keyCount;

      if( !read( targetType, "target type " ) ) {return A3M_FALSE;}
      if( !read( targetName, "target name " ) ) {return A3M_FALSE;}
      if( !read( propertyName, "property  name " ) ) {return A3M_FALSE;}
      if( !read( interpMethod, "interpolation method " ) ) {return A3M_FALSE;}
      if( !read( coeffCount, "coefficient count " ) ) {return A3M_FALSE;}
      if( !read( coeffDataType, "coefficient data type " ) ) {return A3M_FALSE;}

      if( coeffDataType >= TYPE_COUNT )
      {
        A3M_LOG_ERROR( "Unexpected type in %s", m_fileName.c_str() );
        return A3M_FALSE;
      }

      if( !read( keyCount, "key count " ) ) {return A3M_FALSE;}

      A3M_UINT32 animDataSize =
        keyCount * ( sizeof( A3M_FLOAT ) + coeffCount * typeSize[ coeffDataType ] );

      if( targetName.substr( 0, 5 ) == "node-" )
      {
        targetName = targetName.substr( 5, targetName.size() - 5 );
      }

      std::vector<A3M_CHAR8> buffer( animDataSize );
      if( m_stream.read( &buffer[0], animDataSize ) !=
          static_cast<A3M_INT32>(animDataSize) )
      {
        A3M_LOG_ERROR( "Failed to read animation data in %s", m_fileName.c_str() );
        return A3M_FALSE; // This is an error, so abandon the file
      }

      if( targetType == "node" )
      {
        SceneNode::Ptr node = findNode( targetName.c_str(), m_rootNode );
        if( !node )
        {
          A3M_LOG_WARN( "Animation node not present in %s",
                        m_fileName.c_str() );
          return A3M_TRUE; // Return true to carry on reading the file
        }

        if( propertyName == "translation" )
        {
          addChannel<Vector3f>(
            &buffer[0], keyCount,
            createSharedPtr( new SceneNodePositionAnimator( node.get() ) ) );
        }
        else if( propertyName == "rotation" )
        {
          addChannel<Vector4f>(
            &buffer[0], keyCount,
            createSharedPtr( new SceneNodeRotationAnimator( node.get() ) ) );
        }
        else if( propertyName == "scale" )
        {
          addChannel<Vector3f>(
            &buffer[0], keyCount,
            createSharedPtr( new SceneNodeScaleAnimator( node.get() ) ) );
        }
        else
        {
          A3M_LOG_WARN( "Ignoring unknown property in %s", m_fileName.c_str() );
          return A3M_TRUE; // Return true to carry on reading the file
        }
      }
      else if( targetType == "light" )
      {
        LightFinder finder(targetName.c_str());

        // First look for light within scene in this file
        if( m_rootNode ) { visitScene(finder, *m_rootNode); }
        Light::Ptr light = finder.light();

        // If not found within this file, look in passed-in scene
        if( !light && m_scene ) { visitScene(finder, *m_scene); }
        light = finder.light();

        if( !light )
        {
          A3M_LOG_WARN( "No light for animation in %s", m_fileName.c_str() );
          return A3M_TRUE; // Return true to carry on reading the file
        }

        if( propertyName == "colour" )
        {
          addChannel<Colour4f>( &buffer[0], keyCount,
                                createSharedPtr( new LightColourAnimator(
                                    light.get() ) ) );
        }
        else if( propertyName == "multiplier" )
        {
          addChannel<A3M_FLOAT>( &buffer[0], keyCount,
                                 createSharedPtr( new LightIntensityAnimator(
                                     light.get() ) ) );
        }
        else
        {
          A3M_LOG_WARN( "Ignoring unknown property in %s", m_fileName.c_str() );
          return A3M_TRUE; // Return true to carry on reading the file
        }
      }
      else if( targetType == "material" )
      {
        // Locate appearance of specified name in scene graph
        AppearanceFinder finder(targetName.c_str());

        // First look for appearance within scene in this file
        if( m_rootNode ) { visitScene(finder, *m_rootNode); }
        Appearance::Ptr appearance = finder.appearance();

        // If not found within this file, look in passed-in scene
        if( !appearance && m_scene ) { visitScene(finder, *m_scene); }
        appearance = finder.appearance();

        if( !appearance )
        {
          A3M_LOG_WARN( "No appearance material for animation in %s",
                        m_fileName.c_str() );
          return A3M_TRUE; // Return true to carry on reading the file
        }

        // Get a reference to the appearance's shader program data, so that we can
        // enable the shader components required by the animation channels.
        ShaderProgramData& shaderProgramData = m_shaderProgramDataLib[ targetName ];

        if( propertyName == "ambient" )
        {
          addChannel<Vector4f>(&buffer[0], keyCount, createSharedPtr(
                                 new AppearancePropertyAnimator<Vector4f>(
                                   appearance.get(), properties::M_AMBIENT_COLOUR ) ) );
          shaderProgramData.ambi = A3M_TRUE;
        }
        else if( propertyName == "diffuse" )
        {
          addChannel<Vector4f>(&buffer[0], keyCount, createSharedPtr(
                                 new AppearancePropertyAnimator<Vector4f>(
                                   appearance.get(),
                                   properties::M_DIFFUSE_COLOUR ) ) );
          shaderProgramData.diff = A3M_TRUE;
        }
        else if( propertyName == "specular" )
        {
          addChannel<Vector4f>(&buffer[0], keyCount, createSharedPtr(
                                 new AppearancePropertyAnimator<Vector4f>(
                                   appearance.get(),
                                   properties::M_SPECULAR_COLOUR ) ) );
          shaderProgramData.spec = A3M_TRUE;
        }
        else if( propertyName == "emission" )
        {
          addChannel<Vector4f>(&buffer[0], keyCount, createSharedPtr(
                                 new AppearancePropertyAnimator<Vector4f>(
                                   appearance.get(), properties::M_EMISSIVE_COLOUR ) ) );
          shaderProgramData.emis = A3M_TRUE;
        }
        else if( propertyName == "self_illumination" )
        {
          addChannel<A3M_FLOAT>(&buffer[0], keyCount, createSharedPtr(
                                  new AppearancePropertyAnimator<A3M_FLOAT>(
                                    appearance.get(),
                                    properties::M_SELF_ILLUMINATION ) ) );
        }
        else if( propertyName == "specular_level" )
        {
          addChannel<A3M_FLOAT>(&buffer[0], keyCount, createSharedPtr(
                                  new AppearancePropertyAnimator<A3M_FLOAT>(
                                    appearance.get(),
                                    properties::M_SPECULAR_LEVEL ) ) );
        }
        else if( propertyName == "shininess" )
        {
          addChannel<A3M_FLOAT>(&buffer[0], keyCount, createSharedPtr(
                                  new AppearancePropertyAnimator<A3M_FLOAT>(
                                    appearance.get(),
                                    properties::M_SHININESS ) ) );
        }
        else if( propertyName == "opacity" )
        {
          addChannel<A3M_FLOAT>(&buffer[0], keyCount, createSharedPtr(
                                  new AppearancePropertyAnimator<A3M_FLOAT>(
                                    appearance.get(),
                                    properties::M_OPACITY ) ) );
          enableTransparency( *appearance );
        }
        else
        {
          A3M_LOG_WARN( "Ignoring unknown property in %s", m_fileName.c_str() );
          return A3M_TRUE; // Return true to carry on reading the file
        }
      }
      return A3M_TRUE;
    }

    // Read an animation chunk
    A3M_BOOL readAnimation()
    {
      std::string name;
      A3M_UINT32 loopCount;
      A3M_UINT32 chanCount;
      A3M_UINT32 subAnimCount;

      if( !read( name, "anim name " ) ) {return A3M_FALSE;}
      if( !read( loopCount, "ANIM size " ) ) {return A3M_FALSE;}

      for( A3M_UINT32 i = 0; i != loopCount; ++i )
      {
        if( !read( m_loopStart, "loop start " ) ) {return A3M_FALSE;}
        if( !read( m_loopEnd, "loop start " ) ) {return A3M_FALSE;}
      }

      if( !read( chanCount, "CHAN count " ) ) {return A3M_FALSE;}

      for( A3M_UINT32 i = 0; i != chanCount; ++i )
      {
        if( !readChunk( CHUNK_ID_CHAN ) ) {return A3M_FALSE;}
      }

      if( !read( subAnimCount, "(sub) ANIM count " ) ) {return A3M_FALSE;}

      for( A3M_UINT32 i = 0; i != subAnimCount; ++i )
      {
        if( !readChunk( CHUNK_ID_ANIM ) ) {return A3M_FALSE;}
      }

      return A3M_TRUE;
    }

    // Read the animation library chunk
    A3M_BOOL readAnimationLibrary()
    {
      m_animationGroup.reset( new AnimationGroup() );
      m_loopStart = 0.0f;
      m_loopEnd = 0.0f;

      A3M_UINT32 animCount;
      if( !read( animCount, "animation count " ) ) {return A3M_FALSE;}

      for( A3M_UINT32 i = 0; i != animCount; ++i )
      {
        if( !readChunk( CHUNK_ID_ANIM ) ) {return A3M_FALSE;}
      }

      m_animation.reset( new AnimationController( m_animationGroup ) );
      setLoopRange( *m_animation, m_loopStart, m_loopEnd );

      return A3M_TRUE;
    }

    // Read and check version information from INFO chunk
    A3M_BOOL readInfo()
    {
      std::string gloV;
      std::string mtkPythonV;
      std::string mtkColladaV;
      std::string openColladaPluginV;
      std::string authoringTool;

      if( !read( gloV, "glo version" ) ) {return A3M_FALSE;}
      if( !read( mtkPythonV, "mtkPython version" ) ) {return A3M_FALSE;}
      if( !read( mtkColladaV, "mtkCollada version" ) ) {return A3M_FALSE;}
      if( !read( openColladaPluginV, "openColladaPlugin version" ) ) {return A3M_FALSE;}

      A3M_UINT32 major, minor, revision;

      if (sscanf(gloV.c_str(), "%d.%d.%d",  &major, &minor, &revision) != 3)
      {
        A3M_LOG_ERROR( "Incorrect glo version file format in %s", m_fileName.c_str() );
        return A3M_FALSE;
      }

      m_gloVersion = Version( major, minor );
      if( m_gloVersion > CURRENT_GLO_VERSION )
      {
        A3M_LOG_ERROR( "File version not supported in %s", m_fileName.c_str() );
        return A3M_FALSE;
      }

      if( m_gloVersion >= VERSION_BLENDER_SUPPORTED )
      {
        if( !read( authoringTool, "Authoring tool" ) ) {return A3M_FALSE;}

        if ( authoringTool.compare("3ds_max2012") == 0 )
        {
          m_authoringTool = AUTODESK_3DS_MAX;
        }
        else if( authoringTool.compare("blender2.63.0") == 0 )
        {
          m_authoringTool = BLENDER;
        }
        else
        {
          A3M_LOG_ERROR( "Only 3DS-Max 2012 and Blender 2.63.0 "
                         "are supported as authoring tools (%s)", m_fileName.c_str() );
          return A3M_FALSE;
        }
      }
      else
      {
        m_authoringTool = AUTODESK_3DS_MAX;
      }
      return A3M_TRUE;
    }

    A3M_BOOL readModelImplementation()
    {
      if( !read( m_axisUp, "up-axis option file " ) ) {return A3M_FALSE;}

      AnimationController::Ptr controller;
      SceneNode::Ptr node;

      A3M_UINT32 const ROOT_CHUNK_IDS[] =
      {
        CHUNK_ID_INFO, CHUNK_ID_LGEO, CHUNK_ID_LCTL, CHUNK_ID_LLGT,
        CHUNK_ID_LCAM, CHUNK_ID_LMAT, CHUNK_ID_LANM, CHUNK_ID_SCNE
      };

      while( !m_stream.eof() )
      {
        if( !readChunk( ROOT_CHUNK_IDS, A3M_TRUE ) ) {return A3M_FALSE;}
      }

      if( m_gloVersion >= VERSION_SKINNING_SUPPORTED )
      {
        // We link the skin controllers to the joints after reading the whole
        // scene, because joint nodes may appear after the controller
        // references.
        for (A3M_UINT32 i = 0; i < m_skinReferences.size(); ++i)
        {
          linkSkinReference(m_skinReferences[i]);
        }
      }

      createShaderPrograms();

      m_glo->setSceneNode(m_rootNode);
      m_glo->setAnimation(m_animation);

      return A3M_TRUE;
    }
  };
}

namespace a3m
{
  // Load a GLO file
  Glo::Ptr loadGloFile( AssetCachePool& pool,
                        SceneNode::Ptr const& scene, A3M_CHAR8 const* name )
  {
    Stream::Ptr stream = pool.getStream( name );

    if( !stream )
    {
      A3M_LOG_ERROR( "GLO file %s cannot be opened", name );
      return Glo::Ptr();
    }

    GloLoader loader( *stream, pool, scene, name );

    return loader.readModel();
  }
}
