/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.ratconfig;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.R;
import android.os.SystemProperties;


/**
 * Set world mode.
 */
public class RatConfigPs2Activity extends Activity implements View.OnClickListener {
    private final static String TAG = "EM/RatConfig";

    private TextView mCurrentText;
    private RadioButton mRadioCWG;
    private RadioButton mRadioCG;
    private RadioButton mRadioWG;
    private RadioButton mRadioG;
    private RadioGroup mRadioGroup;
    private Button mButtonSet;
    private String mCurrentRATConfig;
    private TextView mHintText;

    private static final int FAIL = 0;
    private static final int REBOOT = 1;
    private static final int NON_REBOOT = 2;

    private static final String RAT_CONFIG_PROPERTY = "persist.radio.mtk_ps2_rat";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rat_config_ps2);
        mRadioCWG = (RadioButton) findViewById(R.id.rat_mode_c_w_g);
        mRadioCG = (RadioButton) findViewById(R.id.rat_mode_c_g);
        mRadioWG = (RadioButton) findViewById(R.id.rat_mode_w_g);
        mRadioG = (RadioButton) findViewById(R.id.rat_mode_g);
        mRadioGroup = (RadioGroup) findViewById(R.id.rat_mode_radio_group);
        mButtonSet = (Button) findViewById(R.id.rat_mode_set);
        mCurrentText = (TextView) findViewById(R.id.Current_rat_config);

    }

    @Override
    protected void onResume() {
        super.onResume();

        mCurrentRATConfig = SystemProperties.get(RAT_CONFIG_PROPERTY);
        Elog.d(TAG, "mCurrentRATConfig = " + mCurrentRATConfig);
        mCurrentText.setText(getString(R.string.Current_RAT_Config) + mCurrentRATConfig);
        setRadioConfig(mCurrentRATConfig);
    }

    private void updateUI() {
        Elog.d(TAG, "mCurrentRATConfig = " + mCurrentRATConfig);
        mCurrentText.setText(getString(R.string.Current_RAT_Config) + mCurrentRATConfig);
    }

    private void setRadioConfig(String currentRATConfig) {
        if (currentRATConfig.equals(getString(R.string.rat_mode_c_g))) {
            mRadioCG.setChecked(true);
        } else if (currentRATConfig.equals(getString(R.string.rat_mode_w_g))) {
            mRadioWG.setChecked(true);
        } else if (currentRATConfig.equals(getString(R.string.rat_mode_g))) {
            mRadioG.setChecked(true);
        } else if (currentRATConfig.equals(getString(R.string.rat_mode_c_w_g))) {
            mRadioCWG.setChecked(true);
        } else {
            Elog.e(TAG, "The rat config value is not right");
        }
    }

    @Override
    protected void onDestroy() {
        Elog.d(TAG, "onDestroy()");
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSet) {
            RadioButton checkedButton =
                (RadioButton)findViewById(mRadioGroup.getCheckedRadioButtonId());
            mCurrentRATConfig = checkedButton.getText().toString();
            SystemProperties.set(RAT_CONFIG_PROPERTY, mCurrentRATConfig);
            updateUI();
        }
    }
}
