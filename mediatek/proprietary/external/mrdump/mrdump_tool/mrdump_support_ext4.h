#if !defined(__MRDUMP_SUPPORT_EXT4_H__)
#define __MRDUMP_SUPPORT_EXT4_H__

// C99
#include <inttypes.h>
#include <stdbool.h>

// FIGETBSZ
#include <linux/fs.h>

// ioctl (FIBMAP)
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/syscall.h>
#include <linux/version.h> 

/*
 * statfs() and statvfs()
 * Note: bionic header didn't define EXT4_SUPER_MAGIC
 *       use EXT3_SUPER_MAGIC instead (the same)
 */
#include <sys/statfs.h>
#include <sys/statvfs.h>

// FIEMAP
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,0,0) 
    #include <linux/fiemap.h> 
#else 
    struct fiemap_extent { 
        __u64 fe_logical; 
        __u64 fe_physical; 
        __u64 fe_length; 
        __u64 fe_reserved64[2]; 
        __u32 fe_flags; 
        __u32 fe_reserved[3]; 
    }; 
                                                                                                                                                                                                          
    struct fiemap { 
        __u64 fm_start; 
        __u64 fm_length; 
        __u32 fm_flags; 
        __u32 fm_mapped_extents; 
        __u32 fm_extent_count; 
        __u32 fm_reserved; 
        struct fiemap_extent fm_extents[0]; 
    }; 
#endif
struct fiemap_info {
    __u32 lba;
    __u32 tot;
};

// fstat
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

// MISC
#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <zlib.h>

#if 0
// CUSTOMISED
#include "aed/aed.h"
#include "libaee/aee_internal.h"
#include "libaee/dirop.h"
#include "mrdump/mrdump_support.h"

#endif

/* LOG macro support */
#include <android/log.h> 
#define MD_LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__); printf(__VA_ARGS__) 
#define MD_LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG,  __VA_ARGS__);  printf(__VA_ARGS__)
#define MD_LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);    printf(__VA_ARGS__)
#define MD_LOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__);    printf(__VA_ARGS__)
#define MD_LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__);   printf(__VA_ARGS__)

// Variables defined
#define AE_DUMPSYS_DATA_PATH        "/data/vendor/dumpsys"
#define MRDUMP_EXT4_NEW_FILE        true
#define MRDUMP_EXT4_OLD_FILE        false
#define MRDUMP_EXT4_PARA_ENABLE     "/sys/module/mrdump/parameters/enable"
#define MRDUMP_EXT4_PARA_LBAOOO     "/sys/module/mrdump/parameters/lbaooo"
#define MRDUMP_EXT4_PARA_FSTYPE     "/sys/module/mrdump/parameters/fstype"
#define MRDUMP_EXT4_PROC_MOUNTS     "/proc/self/mounts"
#define MRDUMP_EXT4_MOUNT_POINT     "/data"
#define MRDUMP_EXT4_ALLOCATE_FILE   AE_DUMPSYS_DATA_PATH"/mrdump_coredump.zip"
#define MRDUMP_EXT4_MIN_ALLOCATE    256
#define MRDUMP_EXT4_1MB_SIZE        (1024*1024)
#define MRDUMP_EXT4_REST_SIZE       500
#define MRDUMP_EXT4_REST_SPACE      (MRDUMP_EXT4_REST_SIZE * MRDUMP_EXT4_1MB_SIZE)

// const.
#define MRDUMP_EXT4_BLKSIZE         4096
#define MRDUMP_EXT4_MAX_CONTINUE    64
#define MRDUMP_EXT4_EXSPACE         (MRDUMP_EXT4_BLKSIZE*MRDUMP_EXT4_MAX_CONTINUE)      // Expect continue space
#define MRDUMP_EXT4_LBA_PER_BLOCK   1022

typedef enum {
    EXT4_1ST_LBA,
    EXT4_2ND_LBA,
    EXT4_CORE_DUMP_SIZE,
    EXT4_USER_FILESIZE,
    EXT4_INFOBLOCK_CRC,
    EXT4_LBA_INFO_NUM
} MRDUMP_LBA_INFO;

typedef enum {
    DEFAULT_DISABLE,
    DEFAULT_HALFMEM,
    DEFAULT_FULLMEM,
    USERDEFINED_MEM
} MRDUMP_DEFAULT_SIZE;

// for chattr
#define FS_IOC_GETFLAGS                 _IOR('f', 1, long)
#define FS_IOC_SETFLAGS                 _IOW('f', 2, long)
#define FS_SECRM_FL                     0x00000001 /* Secure deletion */
#define FS_IMMUTABLE_FL                 0x00000010 /* Immutable file */

struct mrdump_file_info {
    uint32_t info_lba;
    uint32_t addr_lba;
    int64_t filesize;
    int64_t coredump_size;
};

// Function Prototypes
//
// mrdump
unsigned int mrdump_ext4_get_lba_from_file(char *allocfile, int block);

//
//fiemap
int mrdump_fiemap_get_entry_lba(int fd, unsigned int blksize, unsigned int rows);
int mrdump_fiemap_get_entry_tot(int fd, unsigned int blksize, unsigned int rows);
unsigned int mrdump_fiemap_get_lba_of_block(struct fiemap_info *myinfo, unsigned int num, unsigned int block);
unsigned int mrdump_fiemap_total_entries(int fd, unsigned int blksize);
bool mrdump_fiemap_get_entries(int fd, unsigned int blksize, struct fiemap_info *mapinfo, unsigned int rows);

// Public api
void mrdump_file_set_maxsize(int mrdump_size);
/*
 * Setup mrdump pre-allocated file
 * reset : true to reset coredump header
 */
void mrdump_file_setup(bool reset);
bool mrdump_file_get_info(const char *allocfile, struct mrdump_file_info *info);
bool mrdump_file_fetch_zip_coredump(const char *outfile);

#endif /* __MRDUMP_SUPPORT_EXT4_H__ */
