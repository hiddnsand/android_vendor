/*
 * Copyright (C) 2013-2016, Shenzhen Huiding Technology Co., Ltd.
 * All Rights Reserved.
 */

#define LOG_TAG "GoodixFingerprintDaemonCallbackProxy"

#include <stdlib.h>
#include <utils/String16.h>
#include <utils/Log.h>
#include <hardware/hardware.h>
#include "IGoodixFingerprintDaemonCallback.h"
#include "GoodixFingerprintDaemonCallbackProxy.h"

using namespace android;

gf_fingerprint_device_t* GoodixFingerprintDaemonCallbackProxy::mDevice = NULL;

GoodixFingerprintDaemonCallbackProxy::GoodixFingerprintDaemonCallbackProxy() {
}
GoodixFingerprintDaemonCallbackProxy::~GoodixFingerprintDaemonCallbackProxy() {
}

status_t GoodixFingerprintDaemonCallbackProxy::onEnrollResult(int64_t __unused devId, int32_t fpId, int32_t gpId,
        int32_t rem) {
    ALOGD("onEnrollResult, fpId = %u, gpId = %u, rem = %u", fpId, gpId, rem);
    gf_fingerprint_msg_t message;
    message.type = GF_FINGERPRINT_TEMPLATE_ENROLLING;
    message.data.enroll.finger.fid = fpId;
    message.data.enroll.finger.gid = gpId;
    message.data.enroll.samples_remaining = rem;

    if(mDevice != NULL) {
        mDevice->notify(&message);
    } else {
        ALOGD("onEnrollResult mDevice is NULL");
    }

    return NO_ERROR;
}

status_t GoodixFingerprintDaemonCallbackProxy::onAcquired(int64_t __unused devId, int32_t acquiredInfo) {
    ALOGD("onAcquired, acquiredInfo = %u", acquiredInfo);
    gf_fingerprint_msg_t message;
    message.type = GF_FINGERPRINT_ACQUIRED;
    message.data.acquired.acquired_info = (gf_fingerprint_acquired_info_t)acquiredInfo;

    if(mDevice != NULL) {
        mDevice->notify(&message);
    } else {
        ALOGD("onAcquired mDevice is NULL");
    }

    return NO_ERROR;
}

status_t GoodixFingerprintDaemonCallbackProxy::onAuthenticated(int64_t __unused devId, int32_t fingerId,
        int32_t groupId, const uint8_t* hat, int32_t hatSize) {
    ALOGD("onAuthenticated, fpId = %u, gpId = %u", fingerId, groupId);
    gf_fingerprint_msg_t message;
    message.type = GF_FINGERPRINT_AUTHENTICATED;
    message.data.authenticated.finger.fid = fingerId;
    message.data.authenticated.finger.gid = groupId;
    memcpy(&(message.data.authenticated.hat), hat, hatSize);

    if(mDevice != NULL) {
        mDevice->notify(&message);
    } else {
        ALOGD("onAuthenticated mDevice is NULL");
    }

    return NO_ERROR;
}

status_t GoodixFingerprintDaemonCallbackProxy::onError(int64_t __unused devId, int32_t error) {
    ALOGD("onError, error = %u", error);
    gf_fingerprint_msg_t message;
    message.type = GF_FINGERPRINT_ERROR;
    message.data.error = (gf_fingerprint_error_t)error;

    if(mDevice != NULL) {
        mDevice->notify(&message);
    } else {
        ALOGD("onError mDevice is NULL");
    }

    return NO_ERROR;
}

status_t GoodixFingerprintDaemonCallbackProxy::onRemoved(int64_t __unused devId,
        int32_t fingerId, int32_t groupId) {
    ALOGD("onRemoved, fpId = %u, gpId = %u", fingerId, groupId);
    gf_fingerprint_msg_t message;
    message.type = GF_FINGERPRINT_TEMPLATE_REMOVED;
    message.data.removed.finger.fid = fingerId;
    message.data.removed.finger.gid = groupId;

    if(mDevice != NULL) {
        mDevice->notify(&message);
    } else {
        ALOGD("onRemoved mDevice is NULL");
    }

    return NO_ERROR;
}

status_t GoodixFingerprintDaemonCallbackProxy::onEnumerate(int64_t __unused devId, int32_t fpId, int32_t gpId, int32_t rem) {
    ALOGD("onEnumerate, fpId = %u, gpId = %u, remain = %u", fpId, gpId, rem);
    gf_fingerprint_msg_t message;
    message.type = GF_FINGERPRINT_ENUMERATED;
    message.data.enumerated.finger.fid = fpId;
    message.data.enumerated.finger.gid = gpId;
    message.data.enumerated.remaining_templates = rem;

    if(mDevice != NULL) {
        mDevice->notify(&message);
    } else {
        ALOGD("onRemoved mDevice is NULL");
    }

    return NO_ERROR;
}

status_t GoodixFingerprintDaemonCallbackProxy::onAuthenticatedFido(int64_t __unused devId, int32_t fpId, const uint8_t *uvtData, int32_t uvtDataLen) {
    ALOGD("onAuthenticatedFido, uvtDataLen = %d.", uvtDataLen);
    gf_fingerprint_msg_t message;
    message.type = GF_FINGERPRINT_AUTHENTICATED_FIDO;
    message.data.authenticated_fido.finger_id = fpId;
    message.data.authenticated_fido.uvt_len = uvtDataLen;
    memcpy(message.data.authenticated_fido.uvt_data, uvtData, uvtDataLen);

    if(mDevice != NULL) {
        mDevice->notify(&message);
    } else {
        ALOGD("onAuthenticatedFido mDevice is NULL");
    }

    return NO_ERROR;
}


status_t GoodixFingerprintDaemonCallbackProxy::onTestCmd(int64_t  __unused devId,
            int32_t  cmdId, const int8_t  *result, int32_t  resultLen) {
    ALOGD("onTestCmd, cmdId = %u, result[0] = %u, resultLen = %u", cmdId, result[0], resultLen);
    return NO_ERROR;
}

status_t GoodixFingerprintDaemonCallbackProxy::onDump(int64_t __unused devId,
        int32_t __unused cmdId, const int8_t __unused *data, int32_t __unused dataLen) {
    return NO_ERROR;
}

status_t GoodixFingerprintDaemonCallbackProxy::onHbdData(int64_t __unused devId, int32_t heartBeatRate,
        int32_t status, const int32_t* displayData, int32_t dataLen, const int32_t* rawData, int32_t rawDataLen) {
    ALOGD("onHbdData, heartBeatRate = %d, status = %d, displayData = %p, dataLen = %d, rawData = %p, rawDataLen = %d.",
            heartBeatRate, status,displayData,dataLen,rawData,rawDataLen);
    return NO_ERROR;
}
