package com.mediatek.security.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.os.RemoteException;
import android.os.INetworkManagementService;
import com.mediatek.net.connectivity.IMtkIpConnectivityMetrics;
import android.os.ServiceManager;
import android.os.Process;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.Log;

import com.mediatek.security.R;
import com.mediatek.security.datamanager.CheckedPermRecord;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.mediatek.cta.CtaManager;
import vendor.mediatek.hardware.netdagent.V1_0.INetdagent;


public class PermControlUtils {
    private static final String TAG = "PmUtils";

    public static final String PACKAGE_NAME = "exta_package_name";
    public static final String PERMISSION_NAME = "extra_permission_name";
    public static final String UID = "uid";
    public static final String ACTION_PACKAGE_UPDATE =
            "com.mediatek.security.action.ACTION_PACKAGE_UPDATE";
    public static final String PERM_CONTROL_AUTOBOOT_UPDATE =
            "com.mediatek.security.action.AUTOBOOT_UPDATE";
    public static final String CELLULAR_DATA_UPDATE =
            "com.mediatek.security.action.CELLULAR_DATA_UPDATE";
    public static final String EXTRA_STATE = "state";
    public static final String CELLULAR_CONTROL_STATE = "cellulardata_control_state";
    public static final String IS_HANDLE_CHECK_PERM = "IS_HANDLE_CHECK_PERM";

    public static final int MAX_WATI_TIME = 20000; //in mill-seconds
    public static final int NOTIFY_FOREGROUND_ID = 1201;

    public static int GET_ALL_PACKAGE = 1;
    public static int GET_3TH_PACKAGE = 2;
    public static int GET_MAIN_LAUNCH_PACKAGE = 3;

    public static int RESULT_START_ACTIVITY = 0;
    public static int RESULT_FINISH_ITSELF = 1;

    public static final String PKG_NAME = "package_name";

    /**
     * Get application name by passing the  package name
     * @param context Context
     * @param pkgName package name
     * @return the application name
     */
    public static String getApplicationName(Context context, String pkgName) {
        if (pkgName == null) {
            return null;
        }
        String appName = null;
        try {
            PackageManager pkgManager = context.getPackageManager();
            ApplicationInfo info = pkgManager.getApplicationInfo(pkgName, 0);
            appName = pkgManager.getApplicationLabel(info).toString();
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return appName;
    }

    /**
     * Get application icon by passing the  package name
     * @param context Context
     * @param pkgName package name
     * @return the application icon
     */
    public static Drawable getApplicationIcon(Context context, String pkgName) {
        Drawable appIcon = null;
        try {
            PackageManager pkgManager = context.getPackageManager();
            ApplicationInfo info = pkgManager.getApplicationInfo(pkgName, 0);
            appIcon = pkgManager.getApplicationIcon(info);
        } catch (NameNotFoundException ex) {
            Log.w("@M_" + TAG, "get icon is null");
        }
        return appIcon;
    }

    public static Notification getNotification(Context context) {
        Log.d("@M_" + TAG, "getNotification");
        /*
        String titleStr = context
                .getString(R.string.foreground_notification_title);
        String summaryStr = context
                .getString(R.string.foreground_notification_summary);
        Notification notification = new Notification();
        notification.icon = R.drawable.ic_settings_security;
        notification.tickerText = titleStr;
        notification.when = 0;
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        Intent intent = new Intent();
        intent.setAction(PermControlUtils.REQUEST_UI_ACTION);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                intent, 0);
        notification.setLatestEventInfo(context, titleStr, summaryStr,
                pendingIntent);*/
        Notification notification = new Notification.Builder(context)
            .setSmallIcon(R.drawable.ic_settings_security).build();
        //notification.flags |= Notification.FLAG_HIDE_NOTIFICATION;
        return notification;
    }

    public static List<CheckedPermRecord> getPackagePermList(Context context, String pkg) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo packageInfo = pm.getPackageInfo(
                    pkg,
                    PackageManager.GET_PERMISSIONS);
            return getPermRecordListByPkg(pkg, packageInfo);
        } catch (NameNotFoundException e) {
            Log.e(TAG, "getPackageInfo() failed!", e);
        }
        return new ArrayList<CheckedPermRecord>();
    }

    public static List<PackageInfo> getInstalledPackages(Context context, int type) {
        Log.d(TAG, "getInstalledPackages()");
        List<PackageInfo> installedPackages = new ArrayList<PackageInfo>();
        Collection<ApplicationInfo> applicationInfos = null;
        PackageManager pm = context.getPackageManager();
        try {
            if (type == GET_MAIN_LAUNCH_PACKAGE) {
                Intent query = new Intent(Intent.ACTION_MAIN);
                query.addCategory(Intent.CATEGORY_LAUNCHER);
                List<ResolveInfo> infolist1 = pm
                        .queryIntentActivities(query, 0);

                query.removeCategory(Intent.CATEGORY_LAUNCHER);
                query.addCategory(Intent.CATEGORY_HOME);
                query.addCategory(Intent.CATEGORY_DEFAULT);
                List<ResolveInfo> infolist2 = pm
                        .queryIntentActivities(query, 0);

                List<ResolveInfo> infolist = new ArrayList<ResolveInfo>();
                if (infolist1 != null) {
                    infolist.addAll(infolist1);
                }
                if (infolist2 != null) {
                    infolist.addAll(infolist2);
                }

                Map<String, ApplicationInfo> temp = new HashMap<String, ApplicationInfo>();
                for (ResolveInfo info : infolist) {
                    if (info.activityInfo != null) {
                        if (info.activityInfo.applicationInfo != null) {
                            String name = info.activityInfo.applicationInfo.packageName;
                            if (!temp.containsKey(name)) {
                                temp.put(name,
                                        info.activityInfo.applicationInfo);
                            }
                        }
                    }
                }
                applicationInfos = temp.values();
            } else {
                applicationInfos = pm
                    .getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES
                            | PackageManager.GET_DISABLED_COMPONENTS);
            }

            if (applicationInfos != null) {
                Iterator<ApplicationInfo> iterator = applicationInfos
                        .iterator();
                while (iterator.hasNext()) {
                    ApplicationInfo applicationInfo = (ApplicationInfo) iterator
                            .next();
                    if (!(type == GET_3TH_PACKAGE && isSystemApp(applicationInfo))) {
                        PackageInfo packageInfo = pm.getPackageInfo(
                                applicationInfo.packageName,
                                PackageManager.GET_PERMISSIONS);
                        if (packageInfo != null) {
                            installedPackages.add(packageInfo);
                        }
                    }
                }
            }
        } catch (NameNotFoundException e) {
            Log.e(TAG, "getPackageInfo() failed!", e);
        }
        Log.d(TAG, "getInstalledPackages(), size = " + installedPackages.size());
        return installedPackages;
    }

    public static boolean isSystemApp(ApplicationInfo applicationInfo) {
        if (applicationInfo != null) {
            int appId = UserHandle.getAppId(applicationInfo.uid);
            return ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ||
                    (appId == Process.SYSTEM_UID);
        } else {
            Log.d(TAG, "isSystemApp() return false with null packageName");
            return false;
        }
    }

    /**
     * Get whether the pkg is under installed, false not installed.
     * @param context
     * @param pkgName
     * @return
     */
    public static boolean isPkgInstalled(Context context, String pkgName) {
        boolean isPkgInstalled = true;
        try {
            PackageManager pkgManager = context.getPackageManager();
            ApplicationInfo info = pkgManager.getApplicationInfo(pkgName, 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            Log.e("@M_" + TAG, "Package is not installed");
            isPkgInstalled = false;
        }
        return isPkgInstalled;
    }

    /**
     * show a toast with deny hint msg body
     * @param context the context
     * @param pkgName the package name
     * @param permissionName the related permissin name
     */
    public static void showDenyToast(Context context, String pkgName, String permissionName) {
        String label = getApplicationName(context, pkgName);
        Log.d("@M_" + TAG, "showDenyToast() pkgName = " + pkgName + " label = " + label);
        if (label != null) {
            String msg = context.getString(R.string.toast_deny_msg_body, label, permissionName);
            CustomizedToast.toast(context, msg);
        }
    }

    public static void setFirewallPolicy(Context context, int appUid, int networkType,
                                         boolean enabled) {
        String appName = "";
        Log.d(TAG, "setFirewallPolicy:" + appUid + ":" + networkType + ":" + enabled);

        PackageManager pm = context.getPackageManager();
        String[] packages = pm.getPackagesForUid(appUid);

        if (packages == null) {
            Log.e(TAG, "null packages");
            return;
        }
        setFirewallUidChainRule(appUid, enabled);
    }

    private static void setFirewallUidChainRule(int appUid,boolean isEnable) {
        Thread thread = new Thread("setFirewallUidChainRule") {
            public void run() {
                try {
                    INetdagent agent = INetdagent.getService();
                    if (agent == null) {
                        Log.e(TAG, "agnet is null");
                        return;
                    }
                    final String rule = isEnable ? "allow" : "deny";
                    String cmd = String.format(
                            "netdagent firewall set_uid_fw_rule %s mobile %s",
                            appUid, rule);
                    Log.d(TAG, "cmd:" + cmd);
                    agent.dispatchNetdagentCmd(cmd);
                    } catch (Exception e) {
                        Log.e(TAG, "set_uid_fw_rule" + e);
                    }
            }
        };
        thread.start();
    }

    public static void updateCtaAppStatus(int uid,boolean notify){
        IMtkIpConnectivityMetrics IpConnectivityMetrics = getIMtkIpConnectivityMetrics();
        try {
            IpConnectivityMetrics.updateCtaAppStatus(uid,notify);
         } catch (RemoteException e){
            e.printStackTrace();
         }
    }

    private static IMtkIpConnectivityMetrics getIMtkIpConnectivityMetrics() {
        return IMtkIpConnectivityMetrics.Stub.asInterface(
        ServiceManager.getService("mtkconnmetrics"));
    }

    public static List<CheckedPermRecord> getPermRecordListByPkg(String packageName,
                                                                  PackageInfo info) {
        String[] permissions = info.requestedPermissions;
        List<CheckedPermRecord> list = new ArrayList<CheckedPermRecord>();
        if (permissions != null) {
            for (String permission : permissions) {
                list.add(new CheckedPermRecord(packageName, permission));
            }
        }
        return list;
    }

    public static boolean isCellularDataControlOn(Context context) {
        return Settings.System.getInt(context.getContentResolver(),
                PermControlUtils.CELLULAR_CONTROL_STATE,
                context.getResources().getInteger(R.integer.default_enable_state)) > 0;
    }

    public static void enableCellularDataControl(boolean state, Context context) {
        Log.d("@M_" + TAG, "enableCellularDataControl state = " + state);
        Settings.System.putInt(context.getContentResolver(),
                CELLULAR_CONTROL_STATE, state ? 1 : 0);
    }
}
