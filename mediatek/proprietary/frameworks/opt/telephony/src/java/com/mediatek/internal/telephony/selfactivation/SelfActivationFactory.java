package com.mediatek.internal.telephony.selfactivation;

import android.content.Context;

import com.android.internal.telephony.CommandsInterface;

public class SelfActivationFactory {

    public static ISelfActivation getInstance(int subId) {
        ISelfActivation instance = null;
        instance = SelfActivationDefault.getInstance();

        return instance;
    }

    // Initialize the instance in phone process internally
    public static void initInstance(Context context, int numPhones, CommandsInterface[] ci) {
        SelfActivationDefault.init();
    }
}
