/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.android.music;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

/**
 * Dialog helper class.
 */
class MusicDialog extends AlertDialog {
    private final DialogInterface.OnClickListener mListener;
    private View mView;
    private Activity mActivity;
    private final DialogInterface.OnCancelListener mCancelListener =
     new DialogInterface.OnCancelListener() {
        public void onCancel(DialogInterface dialog) {
            mActivity.finish();
        }
    };

    /**
     * M: Listen to search key to avoid respond to quick search request when dialog is showing.
     */
    private final DialogInterface.OnKeyListener mSearchKeyListener =
     new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
            if (KeyEvent.KEYCODE_SEARCH == keyCode) {
                return true;
            }
            return false;
        }
    };

    /**
     * M: Create MusicDialog with given parameter.
     *
     * @param context context.
     * @param listener dialog interface click listener.
     * @param view dialog view.
     */
    public MusicDialog(Context context, DialogInterface.OnClickListener listener, View view) {
        super(context);
        mActivity = (Activity) context;
        mListener = listener;
        mView = view;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (mView != null) {
            setView(mView);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setCancelable(boolean flag) {
        if (flag) {
            setOnCancelListener(mCancelListener);
        }
        super.setCancelable(flag);
    }

    public void setSearchKeyListener() {
        setOnKeyListener(mSearchKeyListener);
    }
    /**
     * positive button response.
     * @param text
     */
    public void setPositiveButton(CharSequence text) {
        setButton(DialogInterface.BUTTON_POSITIVE, text, mListener);
    }

    /**
     * neutral button response.
     * @param text
     */
    public void setNeutralButton(CharSequence text) {
        setButton(DialogInterface.BUTTON_NEUTRAL, text, mListener);
    }

    /**
     * get positive button.
     * @return positive button
     */
    public Button getPositiveButton() {
        return getButton(DialogInterface.BUTTON_POSITIVE);
    }

    /**
     * get neutral button.
     * @return neutral button
     */
    public Button getNeutralButton() {
        return getButton(DialogInterface.BUTTON_NEUTRAL);
    }
}
