#define LOG_TAG "CamCalDrv"
#define MTK_LOG_ENABLE 1

#include <utils/Errors.h>
#include <cutils/properties.h>
#include <cutils/log.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>
#include <mtkcam/drv/mem/cam_cal_drv.h>
#include "cam_cal_drv_imp.h"

#ifdef CAM_CAL_SUPPORT
#include "camera_custom_msdk.h"
#endif

#include "camera_custom_cam_cal.h"

#include <mtkcam/drv/IHalSensor.h>
using namespace NSCam;

/*******************************************************************************
*
********************************************************************************/

#define CAM_CAL_VER "camcal_ver5800~"	//99 mean that chip  00 is our sw ver


#ifdef CAM_CAL_SHOW_LOG
#define CAM_CAL_DRV_LOG(fmt, arg...)    ALOGI(CAM_CAL_VER " " fmt, ##arg)
#define CAM_CAL_DRV_ERR(fmt, arg...)    ALOGE(CAM_CAL_VER "Err: %5d: " fmt, __LINE__, ##arg)
#else
#define CAM_CAL_DRV_LOG(fmt, arg...)    void(0)
#define CAM_CAL_DRV_ERR(fmt, arg...)    void(0)
#endif

#define CAM_CAL_DRV_LOG_IF(cond, ...)      do { if ( (cond) ) { CAM_CAL_DRV_LOG(__VA_ARGS__); } }while(0)

/*******************************************************************************
* Mutex
********************************************************************************/
static pthread_mutex_t mCam_Cal_Mutex = PTHREAD_MUTEX_INITIALIZER;

/*******************************************************************************
*
********************************************************************************/
  CAM_CAL_DATA_STRUCT CamCalDrv::StCamCalCaldata;
MINT32 CamCalDrv::m32CamCalDataValidation=CAM_CAL_ERR_NO_DEVICE;
/*******************************************************************************
*
********************************************************************************/
CamCalDrvBase*
CamCalDrvBase::createInstance()
{
    return CamCalDrv::getInstance();
}

/*******************************************************************************
*
********************************************************************************/
CamCalDrvBase*
CamCalDrv::getInstance()
{
    static CamCalDrv singleton;
    return &singleton;
}

/*******************************************************************************
*
********************************************************************************/
void
CamCalDrv::destroyInstance()
{
}

/*******************************************************************************
*
********************************************************************************/
CamCalDrv::CamCalDrv()
    : CamCalDrvBase()
{
}

/*******************************************************************************
*
********************************************************************************/
CamCalDrv::~CamCalDrv()
{

}

int CamCalDrv::GetCamCalCalData(unsigned long i4SensorDevId,
                          CAMERA_CAM_CAL_TYPE_ENUM a_eCamCalDataType,
	                      void *a_pCamCalData)
{
    int err = 0;

	MUINT32 locali4SensorDevId = i4SensorDevId;
    MUINT32 mulIdx;
    PCAM_CAL_DATA_STRUCT la_pCamCalData = (PCAM_CAL_DATA_STRUCT)a_pCamCalData;
    PCAM_CAL_DATA_STRUCT pCamcalData = &StCamCalCaldata;
    MINT32 i4CurrSensorId = 0xFF;

    //====== Get Property ======
    char value[PROPERTY_VALUE_MAX] = {'\0'};
    property_get("camcaldrv.log", value, "0");
    MINT32 dumpEnable = atoi(value);
    //====== Get Property ======

    CAM_CAL_DRV_LOG_IF(dumpEnable,"CamCalDrv::GetCamCalCalData(). \n");
    CAM_CAL_DRV_LOG_IF(dumpEnable, "CAMERA_CAM_CAL_TYPE_ENUM: %d\n", a_eCamCalDataType);

    if ((a_eCamCalDataType >= CAMERA_CAM_CAL_DATA_LIST)||
        (a_pCamCalData == NULL) )
    {
        m32CamCalDataValidation =CAM_CAL_ERR_NO_DEVICE;
        CAM_CAL_DRV_LOG_IF(dumpEnable,"[CAM_CAL_ERR_NO_DEVICE]0x%x \n",m32CamCalDataValidation);
        CAM_CAL_DRV_LOG_IF(dumpEnable,"i4SensorDevId(0x%8x),a_eCamCalDataType(0x%8x),a_pCamCalData(0x%8x) \n", i4SensorDevId, a_eCamCalDataType, a_pCamCalData);

        return m32CamCalDataValidation;
    }
    //finish data extraction from CamCal
#if 0
    if(!bfFirstCalData)
    {
        Init(u4SensorID,a_pCamCalData);
        bfFirstCalData = TRUE;
    }
#endif

    CAM_CAL_DRV_LOG_IF(dumpEnable, "Get the sensor id of sensor dev: %d \n", i4SensorDevId);



    // Get the sensor id via IHalSenor
    MUINT32 sensorArray[1] = {0};
    IHalSensorList* const pSensorList = MAKE_HalSensorList();
    SensorStaticInfo senStaticInfo;
#if 1
    switch (i4SensorDevId)
    {
        case SENSOR_DEV_MAIN:
        case SENSOR_DEV_SUB:
        case SENSOR_DEV_MAIN_2:
        case SENSOR_DEV_SUB_2:
            pSensorList->querySensorStaticInfo(i4SensorDevId, &senStaticInfo);
            i4CurrSensorId = senStaticInfo.sensorDevID;
            CAM_CAL_DRV_LOG_IF(dumpEnable, "i4CurrSensorId 0x%x...", i4CurrSensorId);
            break;
        case SENSOR_DEV_MAIN_3D:
            pSensorList->querySensorStaticInfo(SENSOR_DEV_MAIN, &senStaticInfo);
            i4CurrSensorId = senStaticInfo.sensorDevID;
            CAM_CAL_DRV_LOG_IF(dumpEnable,"i4CurrSensorId 0x%x...",i4CurrSensorId);
        break;
        default:
            m32CamCalDataValidation = CAM_CAL_ERR_NO_DEVICE;
            CAM_CAL_DRV_LOG_IF(dumpEnable,"[CAM_CAL_ERR_NO_DEVICE] i4SensorDevId = 0x%x\n", i4SensorDevId);
            return m32CamCalDataValidation;
    }
    CAM_CAL_DRV_LOG_IF(dumpEnable, "Start to get CamCal data!! CamCalDrv::GetCamCalCalData()..... \n");
    switch(a_eCamCalDataType) {
        case CAMERA_CAM_CAL_DATA_MODULE_VERSION:            //seanlin 121016 it's for user to get info. of single module or N3D module
        case CAMERA_CAM_CAL_DATA_PART_NUMBER:                      //seanlin 121016 return 5x4 byes gulPartNumberRegCamCal[5]
        case CAMERA_CAM_CAL_DATA_SHADING_TABLE:                  //seanlin 121016 return SingleLsc or N3DLsc
        case CAMERA_CAM_CAL_DATA_3A_GAIN:                              //seanlin 121016 return Single2A or N3D3A
        case CAMERA_CAM_CAL_DATA_STEREO_DATA:
        case CAMERA_CAM_CAL_DATA_PDAF:
            pthread_mutex_lock(&mCam_Cal_Mutex);
            pCamcalData->deviceID = i4SensorDevId;
            pCamcalData->Command = a_eCamCalDataType;
            #ifdef CAM_CAL_SUPPORT  //20170616
            pCamcalData->sensorID= i4CurrSensorId;
            m32CamCalDataValidation= GetCameraCalData(i4CurrSensorId,(MUINT32*)pCamcalData);
            #else
            m32CamCalDataValidation = CamCalReturnErr[a_eCamCalDataType];
            #endif
            pthread_mutex_unlock(&mCam_Cal_Mutex);
        break;
        default:
            m32CamCalDataValidation = CAM_CAL_ERR_NO_CMD;
            CAM_CAL_DRV_ERR("Unknown cam_cal command 0x%x\n",a_eCamCalDataType);
        break;
    }

    // Dump error messages
    CAM_CAL_DRV_LOG_IF(dumpEnable,"All CAM_CAL ERROR if any .\n");
    for(mulIdx = 0; mulIdx < CAMERA_CAM_CAL_DATA_LIST; mulIdx++)
    {
        if(m32CamCalDataValidation & CamCalReturnErr[mulIdx])
            CAM_CAL_DRV_LOG_IF(dumpEnable,"Return ERROR %s\n",CamCalErrString[mulIdx]);
    }

    memcpy((unsigned char*)la_pCamCalData,(unsigned char*)pCamcalData,sizeof(CAM_CAL_DATA_STRUCT));//UINT8 is not exist
    CAM_CAL_DRV_LOG_IF(dumpEnable, "Done get CamCal data!! CamCalDrv::GetCamCalCalData()..... \n");

    return m32CamCalDataValidation;
#else
	return CAM_CAL_ERR_NO_DEVICE;
#endif
}

/*******************************************************************************
*
********************************************************************************/

int
CamCalDrv::Init(unsigned long u4SensorID,void *a_pCamCalData)
{
	u4SensorID = 0;
	a_pCamCalData = NULL;

    return m32CamCalDataValidation;
}



