/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/StereoCam1Device"
//
#include "MyUtils.h"
#include "StereoCam1Device.h"
//
using namespace android;
//
#include <mtkcam/utils/std/common.h>
#include <mtkcam/utils/hw/CamManager.h>
#include <mtkcam/feature/stereo/hal/stereo_setting_provider.h>
using namespace NSCam;
using namespace StereoHAL;
using namespace NSCam::v1::Stereo;
//
#include <sys/prctl.h>

#define THERMAL_DENOISE_POLICY_NAME "thermal_policy_04"
#define THERMAL_VSDOF_POLICY_NAME "thermal_policy_03"
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("(%d)(%s:%d)[%s] " fmt, ::gettid(), getDevName(), getOpenId(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)


/******************************************************************************
 *
 ******************************************************************************/
extern "C"
NSCam::Cam1Device*
createCam1Device_MtkStereo(
    String8 const&          rDevName,
    int32_t const           i4OpenId
)
{
    return new StereoCam1Device(rDevName, i4OpenId);
}


/******************************************************************************
 *
 ******************************************************************************/
StereoCam1Device::
StereoCam1Device(
    String8 const&          rDevName,
    int32_t const           i4OpenId
)
    : Cam1DeviceBase(rDevName, i4OpenId)
    //
    , mpHalSensor(NULL)
    , mpHalSensor_Main2(NULL)
    //
#if '1'==MTKCAM_HAVE_3A_HAL
    , mpHal3a_Main(NULL)
    , mpHal3a_Main2(NULL)
    , mpSync3AMgr(NULL)
#endif
    , mbThreadRunning(MFALSE)
    //
    , mSensorId_Main(-1)
    , mSensorId_Main2(-1)
{
}


/******************************************************************************
 *
 ******************************************************************************/
StereoCam1Device::
~StereoCam1Device()
{
}


/******************************************************************************
 *
 ******************************************************************************/
bool
StereoCam1Device::
onInit()
{
    MY_LOGD("+");
    {
        CAM_TRACE_NAME("init(profile)");
    }
    Utils::CamProfile  profile(__FUNCTION__, "StereoCam1Device");
    //
    bool    ret = false;
    int     err = 0, i4DeviceNum = 0;
    //
    if(NSCam::Utils::CamManager::getInstance()->getDeviceCount() > 0)
    {
        MY_LOGE("CamManager.getDeviceCount() > 0, close them before open stereo mode.");
        return ret;
    }

    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    if(!pHalSensorList)
    {
        MY_LOGE("pHalSensorList == NULL");
        return ret;
    }
    //
    i4DeviceNum = pHalSensorList->queryNumberOfSensors();
    if(i4DeviceNum < 2)
    {
        MY_LOGE("i4DeviceNum(%d) < 2", i4DeviceNum);
        return ret;
    }
    else if(i4DeviceNum > 4)
    {
        MY_LOGW("i4DeviceNum(%d) > 4, it is weired to turn on stereo cam in such situation!", i4DeviceNum);
    }else{
        MY_LOGD("i4DeviceNum(%d)", i4DeviceNum);
    }

    uint32_t curDevIdx = pHalSensorList->querySensorDevIdx(mi4OpenId);
    switch(curDevIdx){
        case SENSOR_DEV_MAIN:
            MY_LOGD("setup rear+rear sensor pair to StereoSettingProvider");
            StereoSettingProvider::setStereoProfile(STEREO_SENSOR_PROFILE_REAR_REAR);
            break;
        case SENSOR_DEV_SUB:
            MY_LOGD("setup front+front sensor pair to StereoSettingProvider");
            StereoSettingProvider::setStereoProfile(STEREO_SENSOR_PROFILE_FRONT_FRONT);
            break;
        default:
            MY_LOGE("Unrecognized opendId(%d)", mi4OpenId);
            return 0;
    }
    // force set stereo feature mode to VSDOF.
    // Because use this device just for VSDOF.
    StereoSettingProvider::setStereoFeatureMode(E_STEREO_FEATURE_CAPTURE | E_STEREO_FEATURE_VSDOF);

    if(i4DeviceNum == 2){
        MY_LOGW("There is only 2 sensors, use rear+front setting!");
        StereoSettingProvider::setStereoProfile(STEREO_SENSOR_PROFILE_REAR_FRONT);
    }

    if(!StereoSettingProvider::getStereoSensorIndex(mSensorId_Main, mSensorId_Main2)){
        MY_LOGE("Cannot get sensor ids from StereoSettingProvider! (%d,%d)", mSensorId_Main, mSensorId_Main2);
        goto lbExit;
    }else{
        MY_LOGD("main1 openId:%d, main2 openId:%d", getOpenId_Main(), getOpenId_Main2());
    }
    //--------------------------------------------------------------------------
    {
        CAM_TRACE_NAME("init(sensor)");
        //  (0) power on sensor
        pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, ANDROID_PRIORITY_FOREGROUND};
        if( pthread_create(&mThreadHandle, &attr, doThreadInit, this) != 0 )
        {
            MY_LOGE("pthread create failed");
            goto lbExit;
        }
        mbThreadRunning = MTRUE;
        //
        // workaround: yuv sensor, 3A depends on sensor power-on
        if( pHalSensorList->queryType( getOpenId_Main() ) == NSCam::NSSensorType::eYUV ||
            pHalSensorList->queryType( getOpenId_Main2() ) == NSCam::NSSensorType::eYUV )
        {
            if( !waitThreadInitDone() )
            {
                MY_LOGE("init in thread failed");
                goto lbExit;
            }
        }
    }
    //--------------------------------------------------------------------------
        //  (1) Open 3A
#if '1'==MTKCAM_HAVE_3A_HAL
    {
        CAM_TRACE_NAME("init(3A)");

        mpSync3AMgr = MAKE_Sync3AMgr();
        if  ( ! mpSync3AMgr ) {
            MY_LOGE("bad mpSync3AMgr");
            goto lbExit;
        }
#if 0
        if  ( ! mpSync3AMgr->init(0, getOpenId_Main(), getOpenId_Main2()) ) {
            MY_LOGE("mpSync3AMgr->init fail");
            goto lbExit;
        }
#endif
        mpHal3a_Main = MAKE_Hal3A(
                getOpenId_Main(),
                LOG_TAG);
        if  ( ! mpHal3a_Main ) {
            MY_LOGE("bad mpHal3a_Main");
            goto lbExit;
        }
        mpHal3a_Main2 = MAKE_Hal3A(
                getOpenId_Main2(),
                LOG_TAG);
        if  ( ! mpHal3a_Main2 ) {
            MY_LOGE("bad mpHal3a_Main2");
            goto lbExit;
        }

        NS3Av3::ISync3AMgr::Stereo_Param_T rStereoParam; //! mpSync3AMgr->init(0, getOpenId_Main(), getOpenId_Main2())
        rStereoParam.i4Sync2AMode = 2;
        rStereoParam.i4SyncAFMode = 1;
        rStereoParam.i4HwSyncMode = 1;
        rStereoParam.i4MasterIdx = getOpenId_Main();
        rStereoParam.i4SlaveIdx = getOpenId_Main2();
        mpHal3a_Main->send3ACtrl(NS3Av3::E3ACtrl_SetStereoParams, (MINTPTR)&rStereoParam, 0);
        mpHal3a_Main2->send3ACtrl(NS3Av3::E3ACtrl_SetStereoParams, (MINTPTR)&rStereoParam, 0);

        MY_LOGD("notifyPwrOn");
        if (mpHal3a_Main != NULL)
        {
            mpHal3a_Main->notifyPwrOn();
        }
        if (mpHal3a_Main2 != NULL)
        {
            mpHal3a_Main2->notifyPwrOn();
        }

        profile.print("3A Hal -");
    }
#endif  //MTKCAM_HAVE_3A_HAL
    //--------------------------------------------------------------------------
    {
        CAM_TRACE_NAME("init(base)");
        //  (2) Init Base.
        if  ( ! Cam1DeviceBase::onInit() )
        {
            goto lbExit;
        }
    }
    //
    //--------------------------------------------------------------------------
    ret = true;
    mbIsUninit = false;
lbExit:
    profile.print("");
    MY_LOGD("- ret(%d) sensorId(%d,%d)", ret, getOpenId_Main(), getOpenId_Main2());
    return  ret;
}


/******************************************************************************
 *
 ******************************************************************************/
bool
StereoCam1Device::
onUninit()
{
    MY_LOGD("+");
    if(mbIsUninit)
    {
        MY_LOGE("already uninit. skip");
        return true;
    }
    Utils::CamProfile  profile(__FUNCTION__, "StereoCam1Device");
    //
    if( !waitThreadInitDone() )
    {
        MY_LOGE("init in thread failed");
    }
    // reset stereo feature mode
    StereoSettingProvider::setStereoFeatureMode(0);
    StereoSettingProvider::setStereoShotMode(0);
    //--------------------------------------------------------------------------
    //  (1) Uninit Base
    {
        CAM_TRACE_NAME("uninit(base)");
        Cam1DeviceBase::onUninit();
        profile.print("Cam1DeviceBase::onUninit() -");
    }
    //--------------------------------------------------------------------------
    //  (2) Uninit3A and power off sensor.
    std::vector<std::future<MBOOL> > vThread;
#define UNINIT_PIPELINE(JOB_NAME, EXECTUE_FUNC_NAME, PARAM1, PARAM2)\
    do{\
        struct JOB_NAME\
        {\
            static\
                MBOOL      execute(StereoCam1Device* pSelf) {\
                                return pSelf->EXECTUE_FUNC_NAME(pSelf->PARAM1, pSelf->PARAM2);\
                            }\
        };\
        vThread.push_back(std::async(std::launch::async, &JOB_NAME::execute, this));\
    }while(0);
#if '1'==MTKCAM_HAVE_3A_HAL
    UNINIT_PIPELINE(job_uninit3a_main, uninit3A, getOpenId_Main(), mpHal3a_Main);
    UNINIT_PIPELINE(job_uninit3a_main2, uninit3A, getOpenId_Main2(), mpHal3a_Main2);
#endif  //MTKCAM_HAVE_3A_HAL
#if '1'==MTKCAM_HAVE_SENSOR_HAL
    UNINIT_PIPELINE(job_powerOff_main, powerOffSensor, getOpenId_Main(), mpHalSensor);
    UNINIT_PIPELINE(job_powerOff_main2, powerOffSensor, getOpenId_Main2(), mpHalSensor_Main2);
#endif  //MTKCAM_HAVE_SENSOR_HAL
#undef UNINIT_PIPELINE
    //
    for(int i = 0 ;i<vThread.size();++i)
    {
        vThread[i].wait();
    }
#if '1'==MTKCAM_HAVE_CAMDRV
    {
        CAM_TRACE_NAME("uninit(resource)");
        // CPTLogStr(Event_Hal_DefaultCamDevice_init, CPTFlagSeparator, "Resource +");
        IResManager* pResManager = IResManager::getInstance();
        //
        if  ( pResManager != NULL )
        {
                if(!(pResManager->close(USER_NAME)))
            {
                MY_LOGE("pResManager->close fail");
            }
        }
        profile.print("Resource -");
        // CPTLogStr(Event_Hal_DefaultCamDevice_init, CPTFlagSeparator, "Resource -");
    }
#endif  //MTKCAM_HAVE_CAMDRV
    //--------------------------------------------------------------------------
    profile.print("");
    mbIsUninit = true;
    MY_LOGD("-");
    return  true;
}


/******************************************************************************
 * [Template method] Called by startPreview().
 ******************************************************************************/
bool
StereoCam1Device::
onStartPreview()
{
    bool ret = false;
    Utils::CamManager* pCamMgr = Utils::CamManager::getInstance();
    //
    //  (0) wait for thread
    if( !waitThreadInitDone() )
    {
        MY_LOGE("init in thread failed");
        goto lbExit;
    }
    //
    //  (1) Check Permission.
    if ( ! pCamMgr->getPermission() )
    {
        MY_LOGE("Cannot start preview ... Permission denied");
        goto lbExit;
    }

    //  (3) Initialize Camera Adapter.
    if  ( ! initCameraAdapter() )
    {
        MY_LOGE("NULL Camera Adapter");
        goto lbExit;
    }

    // (4) Enter thermal policy.
    struct job
    {
        static
            MBOOL      execute(StereoCam1Device* pSelf) {
                            pSelf->enterThermalPolicy();
                            return MTRUE;
                        }
    };

    mThread_ThermalPolicy = std::async(std::launch::async, &job::execute, this);

    ret = true;

lbExit:
    return ret;
}


/******************************************************************************
 *  [Template method] Called by stopPreview().
 ******************************************************************************/
void
StereoCam1Device::
onStopPreview()
{
    MY_LOGD("+");
    if  ( mpCamAdapter != 0 )
    {
        mpCamAdapter->cancelPicture();
        mpCamAdapter->uninit();
        mpCamAdapter.clear();
    }
    thermalExitThread.wait();
    MY_LOGD("-");
}


/******************************************************************************
 * Create a thread to hide some initial steps to speed up launch time
 ******************************************************************************/
bool
StereoCam1Device::
powerOnSensor()
{
    MY_LOGD("+");
    bool    ret = false;
    //  (1) Open Sensor
#if '1'==MTKCAM_HAVE_SENSOR_HAL
    CAM_TRACE_CALL();
    Utils::CamProfile  profile(__FUNCTION__, "StereoCam1Device");
    //
    NSCam::Utils::CamManager::getInstance()->incSensorCount(LOG_TAG);
    NSCam::Utils::CamManager::getInstance()->incSensorCount(LOG_TAG);
    //
    NSCam::IHalSensorList* pHalSensorList = MAKE_HalSensorList();
    MUINT pIndex_Main1[1] = { (MUINT)getOpenId_Main()};
    MUINT pIndex_Main2[1] = { (MUINT)getOpenId_Main2()};
    MUINT const main1Index = getOpenId_Main();
    MUINT const main2Index = getOpenId_Main2();
    if(!pHalSensorList)
    {
        MY_LOGE("pHalSensorList == NULL");
        goto lbExit;
    }
    //
    mpHalSensor = pHalSensorList->createSensor(
                                        USER_NAME,
                                        1,
                                        pIndex_Main1);
    if(mpHalSensor == NULL)
    {
       MY_LOGE("mpHalSensor is NULL");
       goto lbExit;
    }

    //
    mpHalSensor_Main2 = pHalSensorList->createSensor(
                                        USER_NAME,
                                        1,
                                        pIndex_Main2);
    if(mpHalSensor_Main2 == NULL)
    {
       MY_LOGE("mpHalSensor_Main2 is NULL");
       goto lbExit;
    }

    MY_LOGD("powerOn dual sensors 1-by-1");
    if( !mpHalSensor->powerOn(USER_NAME, 1, &main1Index) )
    {
        MY_LOGE("sensor power on failed: %d", pIndex_Main1[0]);
        NSCam::Utils::CamManager::getInstance()->decSensorCount(LOG_TAG);
        goto lbExit;
    }
    if( !mpHalSensor_Main2->powerOn(USER_NAME, 1, &main2Index) )
    {
        MY_LOGE("sensor power on failed: %d", pIndex_Main2[0]);
        NSCam::Utils::CamManager::getInstance()->decSensorCount(LOG_TAG);
        goto lbExit;
    }
    //
    profile.print("Sensor Hal -");
#endif  //MTKCAM_HAVE_SENSOR_HAL
    ret = true;
lbExit:
    MY_LOGD("-");
    return ret;
}


/******************************************************************************
 * the init function to be called in the thread
 ******************************************************************************/
void*
StereoCam1Device::
doThreadInit(void* arg)
{
    ::prctl(PR_SET_NAME,"initCamdevice", 0, 0, 0);
    StereoCam1Device* pSelf = reinterpret_cast<StereoCam1Device*>(arg);
    pSelf->mRet = pSelf->powerOnSensor();
    pthread_exit(NULL);
    return NULL;
}


/******************************************************************************
 * Wait for initializations by thread are done.
 ******************************************************************************/
bool
StereoCam1Device::
waitThreadInitDone()
{
    bool ret = false;
    if( mbThreadRunning )
    {
        MY_LOGD("wait init done +");
        int s = pthread_join(mThreadHandle, NULL);
        MY_LOGD("wait init done -");
        mbThreadRunning = MFALSE;
        if( s != 0 )
        {
            MY_LOGE("pthread join error: %d", s);
            goto lbExit;
        }

        if( !mRet )
        {
            MY_LOGE("init in thread failed");
            goto lbExit;
        }
    }

    ret = true;
lbExit:
    return ret;
}


/******************************************************************************
 * stereo has it own stop flow.
 * because it needs to hide exit thermal policy to improve performance.
 ******************************************************************************/
void
StereoCam1Device::
stopPreview()
{
    struct job
    {
        static
            MBOOL      execute(StereoCam1Device* pSelf) {
                            pSelf->exitThermalPolicy();
                            return MTRUE;
                        }
    };

    thermalExitThread = std::async(std::launch::async, &job::execute, this);
    Cam1DeviceBase::stopPreview();
}


/******************************************************************************
 *  Set the camera parameters. This returns BAD_VALUE if any parameter is
 *  invalid or not supported.
 ******************************************************************************/
status_t
StereoCam1Device::
setParameters(const char* params)
{
    CAM_TRACE_CALL();
    status_t status = OK;
    //
    //  (1) Update params to mpParamsMgr.
    status = mpParamsMgr->setParameters(String8(params));
    if  ( OK != status ) {
        goto lbExit;
    }

    //  Here (1) succeeded.
    //  (2) If CamAdapter exists, apply mpParamsMgr to CamAdapter;
    //      otherwise it will be applied when CamAdapter is created.
    {
        sp<ICamAdapter> pCamAdapter = mpCamAdapter;
        if  ( pCamAdapter != 0 ) {
            status = pCamAdapter->setParameters();
        }
    }

lbExit:
    return  status;
}


/******************************************************************************
 *  get stereo mode
 *  currently used for thermal policy decision
 ******************************************************************************/
int32_t
StereoCam1Device::
getStereoMode(
)
{
    int32_t value = 0;
    if(::strcmp(mpParamsMgr->getStr(
            MtkCameraParameters::KEY_STEREO_REFOCUS_MODE), MtkCameraParameters::ON) == 0)
    {
        MY_LOGD("enable stereo capture");
        value |= E_STEREO_FEATURE_CAPTURE;
    }
    if(::strcmp(mpParamsMgr->getStr(
            MtkCameraParameters::KEY_STEREO_VSDOF_MODE), MtkCameraParameters::ON) == 0)
    {
        MY_LOGD("enable vsdof");
        value |= E_STEREO_FEATURE_VSDOF;
    }
    if(::strcmp(mpParamsMgr->getStr(
            MtkCameraParameters::KEY_STEREO_DENOISE_MODE), MtkCameraParameters::ON) == 0)
    {
        MY_LOGD("enable denoise");
        value |= E_STEREO_FEATURE_DENOISE;
    }
    if(::strcmp(mpParamsMgr->getStr(
            MtkCameraParameters::KEY_STEREO_3RDPARTY_MODE), MtkCameraParameters::ON) == 0)
    {
        MY_LOGD("enable 3party");
        value |= E_STEREO_FEATURE_THIRD_PARTY;
    }
    MY_LOGD("Stereo mode(%d)", value);
    return value;
}

/******************************************************************************
 *  enter thermal policy
 *
 ******************************************************************************/
status_t
StereoCam1Device::
enterThermalPolicy(
)
{
    switch(getStereoMode()){
        case E_STEREO_FEATURE_CAPTURE | E_STEREO_FEATURE_VSDOF:
        case E_STEREO_FEATURE_THIRD_PARTY:
            MY_LOGD("enable thermal policy 03");
            Utils::CamManager::getInstance()->setThermalPolicy(THERMAL_VSDOF_POLICY_NAME, 1);
            break;
        case E_STEREO_FEATURE_DENOISE:
            MY_LOGD("enable thermal policy 04");
            Utils::CamManager::getInstance()->setThermalPolicy(THERMAL_DENOISE_POLICY_NAME, 1);
            break;
        default:
            MY_LOGW("feature combination not supported:%d, use default policy", getStereoMode());
    }
    return OK;
}

/******************************************************************************
 *  exit thermal policy
 *
 ******************************************************************************/
status_t
StereoCam1Device::
exitThermalPolicy(
)
{
    MY_LOGD("disable thermal policies");
    Utils::CamManager::getInstance()->setThermalPolicy(THERMAL_VSDOF_POLICY_NAME, 0);
    Utils::CamManager::getInstance()->setThermalPolicy(THERMAL_DENOISE_POLICY_NAME, 0);
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
StereoCam1Device::
powerOffSensor(
    int32_t openId,
    NSCam::IHalSensor*& halSensor
)
{
    MY_LOGD("(%d) +", openId);
    if(halSensor == nullptr)
    {
        MY_LOGW("(%d) halSensor is nullptr", openId);
        return OK;
    }
    if(openId < 0)
    {
        MY_LOGW("openId(%d) < 0, skip.", openId);
        return OK;
    }
    MUINT pIndex_main[1] = { (MUINT)openId};
    halSensor->powerOff(USER_NAME, 1, pIndex_main);
    NSCam::Utils::CamManager::getInstance()->decSensorCount(LOG_TAG);

    halSensor->destroyInstance(USER_NAME);
    halSensor = NULL;
    MY_LOGD("(%d) -", openId);
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
StereoCam1Device::
uninit3A(
    int32_t openId,
    NS3Av3::IHal3A*& hal3A
)
{
    MY_LOGD("(%d) +", openId);
    if(hal3A == nullptr)
    {
        MY_LOGW("(%d) hal3a is nullptr", openId);
        return OK;
    }
    NS3Av3::ISync3AMgr::Stereo_Param_T rStereoParam; //! mpSync3AMgr->init(0, getOpenId_Main(), getOpenId_Main2())
    rStereoParam.i4Sync2AMode = 1;
    rStereoParam.i4SyncAFMode = 2;
    rStereoParam.i4HwSyncMode = 2;
    rStereoParam.i4MasterIdx = getOpenId_Main(); // for stereo cam1 device, this value is fixed.
    rStereoParam.i4SlaveIdx = getOpenId_Main2(); // for stereo cam1 device, this value is fixed.
    hal3A->send3ACtrl(NS3Av3::E3ACtrl_SetStereoParams, (MINTPTR)&rStereoParam, 0);
    hal3A->notifyPwrOff();
    hal3A->destroyInstance(LOG_TAG);
    hal3A = NULL;
    MY_LOGD("(%d) -", openId);
    return OK;
}
