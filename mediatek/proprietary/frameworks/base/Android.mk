# Copyright Statement:
#
# This software/firmware and related documentation ("MediaTek Software") are
# protected under relevant copyright laws. The information contained herein
# is confidential and proprietary to MediaTek Inc. and/or its licensors.
# Without the prior written permission of MediaTek inc. and/or its licensors,
# any reproduction, modification, use or disclosure of MediaTek Software,
# and information contained herein, in whole or in part, shall be strictly prohibited.
#
# MediaTek Inc. (C) 2011. All rights reserved.
#
# BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
# THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
# RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
# AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
# NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
# SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
# SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
# THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
# THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
# CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
# SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
# STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
# CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
# AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
# OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
# MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
#
# The following software/firmware and/or related documentation ("MediaTek Software")
# have been modified by MediaTek Inc. All revisions are subject to any receiver's
# applicable license agreements with MediaTek Inc.

#
# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This makefile shows how to build your own shared library that can be
# shipped on the system of a phone, and included additional examples of
# including JNI code with the library and writing client applications against it.

ifneq ($(TARGET_BUILD_PDK),true)

LOCAL_PATH := $(call my-dir)

include $(MTK_PATH_SOURCE)/frameworks/base/mpathmap.mk

# MediaTek resource dependency definition.
#mediatek-res-source-path := APPS/mediatek-res_intermediates/src
#mediatek_res_R_stamp := \
#	$(call intermediates-dir-for,APPS,mediatek-res,,COMMON)/src/R.stamp

# MediaTek framework library.
# ============================================================
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE := mediatek-framework

ifeq ($(strip $(MTK_SDK_EMMA_COVERAGE)), yes)
EMMA_INSTRUMENT := true
LOCAL_NO_EMMA_INSTRUMENT := false
LOCAL_NO_EMMA_COMPILE := false
LOCAL_EMMA_COVERAGE_FILTER := +com.mediatek.camcorder.* +com.mediatek.hardware.CameraEx* +com.mediatek.media.* +com.mediatek.build.* +com.mediatek.content.*
endif

LOCAL_JAVA_LIBRARIES := voip-common mediatek-common \
    android.hardware.power-V1.0-java \
    vendor.mediatek.hardware.power-V1.1-java

LOCAL_STATIC_JAVA_LIBRARIES := \
    vendor.mediatek.hardware.power-V1.1-java-static

LOCAL_SRC_FILES := $(call all-java-files-under,$(MTK_FRAMEWORKS_BASE_JAVA_SRC_DIRS))
LOCAL_SRC_FILES += $(call all-java-files-under,camera/mmsdk/java/com/mediatek/mmsdk/)
LOCAL_SRC_FILES += $(call all-java-files-under,core/java/)
LOCAL_SRC_FILES += $(call all-java-files-under,services/core/java/com/mediatek/location/)


LOCAL_AIDL_INCLUDES += $(LOCAL_PATH)/bluetooth/java

#LOCAL_INTERMEDIATE_SOURCES := \
#	$(mediatek-res-source-path)/com/mediatek/R.java \
#	$(mediatek-res-source-path)/com/mediatek/Manifest.java

#Disable it for Basic Telephony branch
#LOCAL_SRC_FILES += \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothMap.aidl \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothBipi.aidl \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothBipr.aidl \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothBpp.aidl \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothDun.aidl \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothFtpCtrl.aidl \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothFtpServerCallback.aidl \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothFtpServer.aidl \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothProfileManager.aidl \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothSimapCallback.aidl \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothSimap.aidl \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothPrxm.aidl \
#    bluetooth/java/com/mediatek/bluetooth/IBluetoothPrxr.aidl \
#Basic Telephony end


# Include AIDL files from mediatek-common.
LOCAL_AIDL_INCLUDES += vendor/mediatek/proprietary/frameworks/common/src

# MMSDK
LOCAL_AIDL_INCLUDES += frameworks/native/aidl/gui

LOCAL_SRC_FILES += \
    camera/mmsdk/java/com/mediatek/mmsdk/IMMSdkService.aidl \
    camera/mmsdk/java/com/mediatek/mmsdk/IEffectUser.aidl \
    camera/mmsdk/java/com/mediatek/mmsdk/IEffectUpdateListener.aidl \
    camera/mmsdk/java/com/mediatek/mmsdk/ImageInfo.java \
    camera/mmsdk/java/com/mediatek/mmsdk/IMemory.aidl \
    camera/mmsdk/java/com/mediatek/mmsdk/IMemoryHeap.aidl \
    camera/mmsdk/java/com/mediatek/mmsdk/IImageTransformUser.aidl \
    camera/mmsdk/java/com/mediatek/mmsdk/IFeatureManager.aidl \
    camera/mmsdk/java/com/mediatek/mmsdk/IEffectFactory.aidl \
    camera/mmsdk/java/com/mediatek/mmsdk/IEffectHalClient.aidl \
    camera/mmsdk/java/com/mediatek/mmsdk/IEffectHal.aidl \
    camera/mmsdk/java/com/mediatek/mmsdk/IEffectListener.aidl \
    camera/mmsdk/java/com/mediatek/mmsdk/TrasformOptions.java \
    camera/mmsdk/java/com/mediatek/mmsdk/Rect.java \
    camera/mmsdk/java/com/mediatek/mmsdk/EffectHalVersion.java \
    camera/mmsdk/java/com/mediatek/mmsdk/BaseParameters.java \
    camera/mmsdk/java/com/mediatek/mmsdk/BinderHolder.java \
    core/java/com/mediatek/search/ISearchEngineManagerService.aidl\
    core/java/com/mediatek/datashaping/IDataShapingManager.aidl \

LOCAL_AIDL_INCLUDES += $(LOCAL_PATH)/camera/mmsdk/java


# AdvCam
LOCAL_SRC_FILES += camera/advcam/java/com/mediatek/advcam/IAdvCamService.aidl
LOCAL_AIDL_INCLUDES += frameworks/av/camera/aidl # for CaptureRequest

LOCAL_AIDL_INCLUDES += $(LOCAL_PATH)/bluetooth/java

ANDROID_BT_JB_MR1 := yes

ifeq (yes,$(ANDROID_BT_JB_MR1))
LOCAL_SRC_FILES := $(filter-out bluetooth/java/com/mediatek/bluetooth/4.1/BluetoothAdapterEx.java, $(LOCAL_SRC_FILES))
else
LOCAL_SRC_FILES := $(filter-out bluetooth/java/com/mediatek/bluetooth/4.2/BluetoothAdapterEx.java, $(LOCAL_SRC_FILES))
endif

LOCAL_SHARED_LIBRARIES := libcustom_jni
LOCAL_REQUIRED_MODULES := libmpojni libjni_pq libgifEncoder_jni libcustom_jni

include $(BUILD_JAVA_LIBRARY)

$(stub_full_target): MEDIATEK_FRAMEWORK_RES_PACKAGE := $(mediatek_framework_res_package)

# For now, must specify package names whose sources will be built into the android framework id stub library.
stub_package_framework_res_id_list := android:com.android.internal

mediatek-res-source-path := APPS/mediatek-res_intermediates/src
framework-res-source-path := APPS/framework-res_intermediates/src

#$(full_target): mediatek-res-package-target
# Avoid shell command in Android.mk, and it is redundant.
#$(shell mkdir -p $(OUT_DOCS))

# The target needs mediatek-res files.


# Stub library source.
# Generate stub source for making Android framework-res R.java library.
# ============================================================
include $(CLEAR_VARS)

FRAMEWORK_RES_ID_API_FILE := $(TARGET_OUT_COMMON_INTERMEDIATES)/PACKAGING/framework_res_id_api.txt

LOCAL_SRC_FILES :=
LOCAL_INTERMEDIATE_SOURCES := \
	$(framework-res-source-path)/android/R.java \
    $(framework-res-source-path)/com/android/internal/R.java

LOCAL_MODULE := android-framework-res-id-stubs
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_MODULE_CLASS := JAVA_LIBRARIES

LOCAL_DROIDDOC_OPTIONS:= \
        -stubpackages $(stub_package_framework_res_id_list) \
		-api $(FRAMEWORK_RES_ID_API_FILE) \
		-nodocs

include $(BUILD_DROIDDOC)

$(FRAMEWORK_RES_ID_API_FILE): $(full_target)

# ============================================================
ifeq (,$(ONE_SHOT_MAKEFILE))
include $(call first-makefiles-under,$(LOCAL_PATH))
$(mediatek_api_stubs_src):$(ALL_MODULES.mediatek-res.BUILT)
endif

endif
