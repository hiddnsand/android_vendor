/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Logging facility for the A3M middleware framework.
 */


/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <stdio.h>     /* vsnprintf */
#include <stdarg.h>    /* va_list etc data type */
#include <stdlib.h>    /* strtol etc functions */
#include <string.h>    /* strncpy */
#include <a3m/log.h>   /* a3m log functions */

/*****************************************************************************
 * Constants
 *****************************************************************************/
/* Muximum number of module can be logged */
#define PSS_MAX_MODULE_CACHE_ENTRIES    50
/* Muximum number of filters used in log types */
#define PSS_MAX_MODULE_FILTER_ENTRIES   10
/* Maximum length of logging content includes the terminating zero */
#define PSS_MAX_LOGGING_TEXT_LENGTH     1024

/*****************************************************************************
 * Local Function Prototypes
 *****************************************************************************/
static void pssLogAddToCache(const A3M_CHAR8* pszModule, A3M_INT32 i);
static A3M_INT32 pssLogPrintStdio(const A3M_CHAR8* pszString);
static void pssLogFlushStdio(void);

/*****************************************************************************
 * Local variables
 *****************************************************************************/
static const A3M_BOOL bModuleFiltering = A3M_FALSE;

static struct
{
  const A3M_CHAR8* pszName;
  const A3M_CHAR8* pszModule;
} moduleCache[PSS_MAX_MODULE_CACHE_ENTRIES + 1];

static const A3M_CHAR8* const pszModuleFilters[PSS_MAX_MODULE_FILTER_ENTRIES + 1] =
{
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  ""
};

static const PfPrint pfPrintStdio  = pssLogPrintStdio;
static const PfFlush pfFlushStdio  = pssLogFlushStdio;

/*****************************************************************************
 * Local Functions
 *****************************************************************************/
/**
 * Pss print string
 * \return length of the string.
 */
static A3M_INT32 pssLogPrintStdio(const A3M_CHAR8* pszString)
{
  printf("%s", pszString);
  return static_cast<A3M_INT32>(strlen(pszString));
}

/**
 * Pss log flush the standard out io connection.
 * \return none.
 */
static void pssLogFlushStdio(void)
{
  fflush( stdout );
}

/**
 * Pss log: store log information into cache
 * \return none
 */
static void pssLogAddToCache(const A3M_CHAR8* pszModule, A3M_INT32 i)
{
  A3M_UINT32 j, k;
  const A3M_CHAR8* pszFilename;

  if (i >= PSS_MAX_MODULE_CACHE_ENTRIES)
  {
    fprintf(stderr, "no space left in module cache\n");
    return;
  }

  moduleCache[i].pszModule = pszModule;

  /* Find pszFilename from module name */
  j = strlen(moduleCache[i].pszModule);
  for( ; (j > 0) && (moduleCache[i].pszModule[j - 1] != '/') &&
       (moduleCache[i].pszModule[j - 1] != '\\'); j-- ) {};

  pszFilename = moduleCache[i].pszModule + j;

  /* Try to find a match with the filter list */
  for( j = 0; pszModuleFilters[j]; j++ )
  {
    for( k = 0; (pszModuleFilters[j][k] && pszFilename[k] &&
                 (pszModuleFilters[j][k]) == pszFilename[k]); k++ ) {};

    if (pszModuleFilters[j][k] != '*')
    {
      /* Not a match */
      continue;
    }

    if ((pszFilename[k] != '.') &&
        (pszModuleFilters[j][k] != '*'))
    {
      /* Not a match */
      continue;
    }

    /* We found it */
    moduleCache[i].pszName = pszModuleFilters[j];
  }
}

/** Log a specified message using a format string and format argument list.
 */
static void pssLog(const pssLogType type, const A3M_CHAR8* pszModule,
                   const A3M_CHAR8* pszFunction, A3M_INT32 line,
                   const A3M_CHAR8* pszFormat, va_list ap)
{
  static const A3M_CHAR8* const pszFormats[] =
  {
    "LOG_ERROR ", "LOG_WARN  ", "LOG_INFO  ", "LOG_DEBUG "
  };

  A3M_CHAR8 text[PSS_MAX_LOGGING_TEXT_LENGTH];
  A3M_UINT32 textLength;

  /* Check if filtering is enabled on a per pszModule basis */
  if ( bModuleFiltering != A3M_FALSE )
  {
    A3M_INT32 i;

    /* Look for the pszModule in the cache */
    for(i = 0;
        moduleCache[i].pszModule && moduleCache[i].pszModule != pszModule;
        i++) {};

    /* Add pszModule to cache if it wasn't found */
    if (!moduleCache[i].pszModule)
    {
      pssLogAddToCache(pszModule, i);
    }

    /* Check if it is selected */
    if (!moduleCache[i].pszName)
    {
      return;
    }
  }

  /* Strip path from pszModule name */
  if (pszModule)
  {
    const A3M_CHAR8* pszName = pszModule;
    while (*pszName)
    {
      pszName++;
    }
    while ((pszName[-1] != '\\') && (pszName[-1] != '/') &&
           (pszName > pszModule))
    {
      pszName--;
    }
    pszModule = pszName;
  }

  A3M_INT32 result = snprintf( text, (PSS_MAX_LOGGING_TEXT_LENGTH - 1),
                               " %s (%s, %s, %i) ",
                               pszFormats[type], pszModule,
                               pszFunction, line );
  A3M_PARAM_NOT_USED(result);
  text[PSS_MAX_LOGGING_TEXT_LENGTH - 1] = '\0';
  textLength = strlen(text);
  result = vsnprintf(&text[textLength], (PSS_MAX_LOGGING_TEXT_LENGTH - 1) - textLength,
                     pszFormat, ap);
  text[PSS_MAX_LOGGING_TEXT_LENGTH - 1] = '\0';
  textLength = strlen(text);
  strncat(&text[textLength], "\n",
          (PSS_MAX_LOGGING_TEXT_LENGTH - 1) - textLength);
  text[PSS_MAX_LOGGING_TEXT_LENGTH - 1] = '\0';

  result = pfPrintStdio(text);
  pfFlushStdio();

  A3M_PARAM_NOT_USED(result);
}

/*****************************************************************************
 * Global Functions
 *****************************************************************************/
/*
 * Initialise the a3m logging interface.
 */
void pssLogInit()
{
  // Nothing to do currently.
}

/*
 * Deinitialise the a3m logging interface.
 * It is currently a place holder for any resource needs to be released in the
 * future.
 */
void pssLogDeInit()
{
  /* there is nothing needed to be deinited at present */
}


/*
 * Error log messages to be used to log critical errors using a format string
 * and a variable number of format arguments.
 */
void pssLogError(const A3M_CHAR8* pszModule, const A3M_CHAR8* pszFunction,
                 A3M_INT32 line, const A3M_CHAR8* pszFormat, ...)
{
  va_list ap;
  va_start( ap, pszFormat );
  pssLog(PSS_LOG_ERROR, pszModule, pszFunction, line, pszFormat, ap);
  va_end(ap);

  A3M_PARAM_NOT_USED(ap);
}

/* Log a Warning message using a format string and a variable number
 * of format arguments.
 */
void pssLogWarn(const A3M_CHAR8* pszModule, const A3M_CHAR8* pszFunction,
                A3M_INT32 line, const A3M_CHAR8* pszFormat, ...)
{
  va_list ap;
  va_start( ap, pszFormat );
  pssLog(PSS_LOG_WARN, pszModule, pszFunction, line, pszFormat, ap);
  va_end(ap);

  A3M_PARAM_NOT_USED(ap);
}

/* Log a specified message using a format string and a variable number of
 * format arguments.
 */
void pssLogInfo(const A3M_CHAR8* pszModule, const A3M_CHAR8* pszFunction,
                A3M_INT32 line, const A3M_CHAR8* pszFormat, ...)
{
  va_list ap;
  va_start( ap, pszFormat );
  pssLog(PSS_LOG_INFO, pszModule, pszFunction, line, pszFormat, ap);
  va_end(ap);

  A3M_PARAM_NOT_USED(ap);
}


/* Log a "debug" message using a format string and a variable number
 * of format arguments.
 */
void pssLogDebug(const A3M_CHAR8* pszModule, const A3M_CHAR8* pszFunction,
                 A3M_INT32 line, const A3M_CHAR8* pszFormat, ...)
{
  va_list ap;
  va_start( ap, pszFormat );
  pssLog(PSS_LOG_DEBUG, pszModule, pszFunction, line, pszFormat, ap);
  va_end(ap);

  A3M_PARAM_NOT_USED(ap);
}

