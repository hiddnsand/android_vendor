/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.server;
import com.android.server.*;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AppOpsManager;
import android.app.BroadcastOptions;
import android.app.IAlarmCompleteListener;
import android.app.IAlarmListener;
import android.app.IAlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PermissionInfo;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.WorkSource;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.ArrayMap;
import android.util.KeyValueListParser;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseLongArray;
import android.util.TimeUtils;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.TreeSet;

import static android.app.AlarmManager.RTC_WAKEUP;
import static android.app.AlarmManager.RTC;
import static android.app.AlarmManager.ELAPSED_REALTIME_WAKEUP;
import static android.app.AlarmManager.ELAPSED_REALTIME;
/// M: added for powerOffAlarm feature @{
import static android.app.AlarmManager.PRE_SCHEDULE_POWER_OFF_ALARM;
///@}
/// M: For handling non-wakeup alarms while WFD is connected
import android.hardware.display.DisplayManager;
import android.hardware.display.WifiDisplayStatus;
///@}
/// M: added for BG powerSaving feature @{
import com.mediatek.amplus.AlarmManagerPlus;
///@}

public class MtkAlarmManagerService extends AlarmManagerService{

    /// M: added for BG powerSaving feature @{
    static final String ClockReceiver_TAG = "ClockReceiver";
    private static int mAlarmMode = 2; //M: use AlarmGrouping v2 default
    private static boolean mSupportAlarmGrouping = false;
    boolean mNeedRebatchForRepeatingAlarm = false;
    private AlarmManagerPlus mAmPlus;
    private boolean mNeedGrouping = true;
    ///@}
    /// M: added for powerOffAlarm feature @{
    private Object mWaitThreadlock = new Object();
    private Object mPowerOffAlarmLock = new Object();
    private final ArrayList<Alarm> mPoweroffAlarms = new ArrayList<Alarm>();
    //PowerOff alarm will be preponed by these many seconds.
    static final long POWER_OFF_ALARM_BUFFER_TIME = 150*1000; // 120 seconds
    ///@}
    /// M:add for PPL feature ,@{
    private PPLReceiver mPPLReceiver = null;
    private boolean mPPLEnable = true;
    private Object mPPLLock = new Object();
    private ArrayList<PendingIntent> mPPLFreeList = null;
    private ArrayList<String> mAlarmIconPackageList = null;
    private ArrayList<Alarm> mPPLResendList = null;
    ///@}
    /// M: For handling non-wakeup alarms while WFD is connected
    WFDStatusChangedReceiver mWFDStatusChangedReceiver;
    boolean mIsWFDConnected = false;
    ///@}
    public MtkAlarmManagerService(Context context) {
        super(context);
        if(Build.TYPE.equals("eng")||Build.TYPE.equals("userdebug")){
            localLOGV=true;
        }
    }
    /// M: For handling non-wakeup alarms while WFD is connected
    @Override
    protected void registerWFDStatusChangeReciever(){
        mWFDStatusChangedReceiver = new WFDStatusChangedReceiver();
    }
    @Override
    protected boolean isWFDConnected(){
        /// M: Extra Logging @{
        if(DEBUG_WAKELOCK) {
            Slog.v(TAG, "checkAllowNonWakeupDelayLocked isWFDConnected :"+mIsWFDConnected);
        }
        ///@}
        return mIsWFDConnected;
    }
    ///@}
    /// M: added for BG powerSaving feature @{
    @Override
    protected boolean needAlarmGrouping(){
        return mNeedGrouping;
    }
    @Override
    protected void resetneedRebatchForRepeatingAlarm(){
          mNeedRebatchForRepeatingAlarm=false;
    }
    @Override
    protected boolean needRebatchForRepeatingAlarm(){
        return  mNeedRebatchForRepeatingAlarm;
    }
    @Override
    protected boolean supportAlarmGrouping(){
        return (mSupportAlarmGrouping && (mAmPlus != null));
    }
    @Override
    protected void initAlarmGrouping(){
        if (SystemProperties.get("ro.mtk_bg_power_saving_support").equals("1")) {
        //M:enable PowerSaving
            mSupportAlarmGrouping = true;
        }

        if (mSupportAlarmGrouping && (mAmPlus == null)) {
            try {
                    mAmPlus = new AlarmManagerPlus(getContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }
    @Override
    protected long getMaxTriggerTimeforAlarmGrouping(int type,long whenElapsed,
    long windowLength,long interval,PendingIntent operation,Alarm a)
    {
        long maxElapsed;

        maxElapsed = mAmPlus.getMaxTriggerTime(type, whenElapsed, windowLength,
                            interval,operation, mAlarmMode, true);
        /// M: [ALPS03910104] [CTS fail] Past alarm, need to trigger immediatly(min_futurity) @{
        if (whenElapsed < 0){
            Slog.v(TAG,"Past alarm, need to trigger immediatly (min_futurity)");
            maxElapsed = whenElapsed;
            if(a!=null)
                a.needGrouping = false;
            else{
                mNeedGrouping=false;
            }
        ///@}
        } else if (maxElapsed < 0){
            maxElapsed = 0 - maxElapsed;
            if(a!=null)
                a.needGrouping = false;
            else{
                mNeedGrouping=false;
            }
        }
        /// M: [ALPS03910104] handled default case in the end instead of beginning @{
        else {
            if (a!=null) {
                a.needGrouping = true;
            }
            else {
                mNeedGrouping=true;
            }
        }
        ///@}
        return maxElapsed;
    }
    @Override
    protected boolean removeInvalidAlarmLocked(PendingIntent operation, IAlarmListener listener) {
        boolean didRemove = false;
        for (int i = mAlarmBatches.size() - 1; i >= 0; i--) {
            Batch b = mAlarmBatches.get(i);
            didRemove |= b.remove(operation, listener);
            if (b.size() == 0) {
                mAlarmBatches.remove(i);
            }
        }
        if(didRemove)
            mNeedRebatchForRepeatingAlarm=true;
        return didRemove;
    }
    ///@}
    /// M: For handling non-wakeup alarms while WFD is connected
    class WFDStatusChangedReceiver extends BroadcastReceiver {
        public WFDStatusChangedReceiver() {
            IntentFilter filter = new IntentFilter();
            // mIsWFDConnected registerReceiver
            filter.addAction(DisplayManager.ACTION_WIFI_DISPLAY_STATUS_CHANGED);
            getContext().registerReceiver(this, filter);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (DisplayManager.ACTION_WIFI_DISPLAY_STATUS_CHANGED.equals(intent.getAction())) {

                WifiDisplayStatus wfdStatus = (WifiDisplayStatus)
                intent.getParcelableExtra(DisplayManager.EXTRA_WIFI_DISPLAY_STATUS);
                mIsWFDConnected =
                (WifiDisplayStatus.DISPLAY_STATE_CONNECTED == wfdStatus.getActiveDisplayState());
            }
        }
    }
    ///@}
    /// M:add for PPL feature ,@{
    @Override
    protected void initPpl()
    {
        mPPLReceiver = new PPLReceiver();
        mPPLFreeList = new ArrayList<PendingIntent>();
        mPPLFreeList.add(mTimeTickSender);
        mPPLFreeList.add(mDateChangeSender);
        mPPLResendList = new ArrayList<Alarm>();
    }
    @Override
    protected boolean freePplCheck(ArrayList<Alarm> triggerList, long nowELAPSED)
    {
        boolean ret=false;
        synchronized (mPPLLock) {
            if (mPPLEnable == false) {
                FreePPLIntent(triggerList, mPPLFreeList, nowELAPSED, mPPLResendList);
                ret=true;
            }
        }
        return ret;
    }
    class PPLReceiver extends BroadcastReceiver {
        public PPLReceiver() {
            IntentFilter filter = new IntentFilter();
            filter.addAction("com.mediatek.ppl.NOTIFY_LOCK");
            filter.addAction("com.mediatek.ppl.NOTIFY_UNLOCK");
            getContext().registerReceiver(this, filter);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals("com.mediatek.ppl.NOTIFY_LOCK")) {
                mPPLEnable = false;
            } else if (action.equals("com.mediatek.ppl.NOTIFY_UNLOCK")) {
                mPPLEnable = true;
                enablePPL();
            }
        }
    }

    public int enablePPL() {

        synchronized (mPPLLock) {
            if (mPPLEnable) {
                resendPPLPendingList(mPPLResendList);
                mPPLResendList = null;
                mPPLResendList = new ArrayList<Alarm>();
            }
        }
        return -1;
    }

    /**
     *For PPL feature, to Free PPLIntent
     */
    private void FreePPLIntent(ArrayList<Alarm> triggerList, ArrayList<PendingIntent> mPPLFreeList,
                              long nowELAPSED, ArrayList<Alarm> resendList) {
        Iterator<Alarm> it = triggerList.iterator();
        boolean isFreeIntent = false;
        while (it.hasNext()) {
            isFreeIntent = false;
            Alarm alarm = it.next();
            // if with null operation, skip this alarm
            if (alarm.operation == null) {
                /// M: Extra Logging @{
                if (DEBUG_WAKELOCK) {
                    Slog.v(TAG, "FreePPLIntent skip with null operation APP listener("
                        + alarm.listenerTag + ") : type = " + alarm.type
                        + " triggerAtTime = " + alarm.when);
                }
                ///@}
                continue;
            }
            try {
                for (int i = 0; i < mPPLFreeList.size(); i++) {
                    if (alarm.operation.equals(mPPLFreeList.get(i))) {
                        /// M: Extra Logging @{
                        if (DEBUG_WAKELOCK) {
                            Slog.v(TAG, "sending alarm " + alarm);
                        }
                        ///@}
                        alarm.operation.send(getContext(), 0,
                                mBackgroundIntent.putExtra(
                                        Intent.EXTRA_ALARM_COUNT, alarm.count),
                                this.mDeliveryTracker, mHandler);
                        // we have an active broadcast so stay awake.
                        if (mBroadcastRefCount == 0) {
                setWakelockWorkSource(alarm.operation, alarm.workSource,
                    alarm.type, alarm.statsTag, alarm.uid,true);
                            mWakeLock.acquire();
                        }


                final InFlight inflight = new InFlight(MtkAlarmManagerService.this,
                        alarm.operation, alarm.listener, alarm.workSource, alarm.uid,
                        alarm.packageName, alarm.type, alarm.statsTag, 0); //ALPS02190343
                        mInFlight.add(inflight);
                        mBroadcastRefCount++;
                        final BroadcastStats bs = inflight.mBroadcastStats;
                        bs.count++;
                        if (bs.nesting == 0) {
                            bs.nesting = 1;
                            bs.startTime = nowELAPSED;
                        } else {
                            bs.nesting++;
                        }
                        final FilterStats fs = inflight.mFilterStats;
                        fs.count++;
                        if (fs.nesting == 0) {
                            fs.nesting = 1;
                            fs.startTime = nowELAPSED;
                        } else {
                            fs.nesting++;
                        }
                        if (alarm.type == ELAPSED_REALTIME_WAKEUP
                                || alarm.type == RTC_WAKEUP) {
                            bs.numWakeup++;
                            fs.numWakeup++;
                        }
                        isFreeIntent = true;
                        break;
                    }

                }
                if (!isFreeIntent) {
                    resendList.add(alarm);
                    isFreeIntent = false;
                }
            } catch (PendingIntent.CanceledException e) {
                if (alarm.repeatInterval > 0) {
                    // This IntentSender is no longer valid, but this
                    // is a repeating alarm, so toss the hoser.
                    //remove(alarm.operation);
                }
            }
        }
    }

    /**
     *For PPL feature, to resend PPLPendingList
     */
    private void resendPPLPendingList(ArrayList<Alarm> PPLResendList) {
        Iterator<Alarm> it = PPLResendList.iterator();
        while (it.hasNext()) {
            Alarm alarm = it.next();
            // if with null operation, skip this alarm
            if (alarm.operation == null) {
                /// M: Extra Logging @{
                if (localLOGV) {
                    Slog.v(TAG, "resendPPLPendingList skip with null operation, APP listener("
                        + alarm.listenerTag + ") : type = " + alarm.type
                        + " triggerAtTime = " + alarm.when);
                }
                ///@}
                continue;
            }
            try {
                /// M: Extra Logging @{
                if (DEBUG_WAKELOCK) {
                    Slog.v(TAG, "sending alarm " + alarm);
                }
                ///@}
                alarm.operation.send(getContext(), 0,
                        mBackgroundIntent.putExtra(
                                Intent.EXTRA_ALARM_COUNT, alarm.count),
                               this.mDeliveryTracker, mHandler);

                // we have an active broadcast so stay awake.
                if (mBroadcastRefCount == 0) {
                    setWakelockWorkSource(alarm.operation, alarm.workSource,
                            alarm.type, alarm.statsTag, alarm.uid,true);
                    mWakeLock.acquire();
                }
                final InFlight inflight = new InFlight(MtkAlarmManagerService.this,
                alarm.operation, alarm.listener, alarm.workSource, alarm.uid,
                alarm.packageName, alarm.type, alarm.statsTag, 0); //ALPS02190343

                mInFlight.add(inflight);
                mBroadcastRefCount++;
                final BroadcastStats bs = inflight.mBroadcastStats;
                bs.count++;
                if (bs.nesting == 0) {
                    bs.nesting = 1;
                    bs.startTime = SystemClock.elapsedRealtime();
                } else {
                    bs.nesting++;
                }
                final FilterStats fs = inflight.mFilterStats;
                fs.count++;
                if (fs.nesting == 0) {
                    fs.nesting = 1;
                    fs.startTime = SystemClock.elapsedRealtime();
                } else {
                    fs.nesting++;
                }
                if (alarm.type == ELAPSED_REALTIME_WAKEUP
                        || alarm.type == RTC_WAKEUP) {
                    bs.numWakeup++;
                    fs.numWakeup++;
                    //ActivityManagerNative.noteWakeupAlarm(
                    //alarm.operation);
                }
            } catch (PendingIntent.CanceledException e) {
                if (alarm.repeatInterval > 0) {
                    // This IntentSender is no longer valid, but this
                    // is a repeating alarm, so toss the hoser.
                    //remove(alarm.operation);
                }
            }
        }
    }
    ///@}
    /// M: added for powerOffAlarm feature @{
    @Override
    protected boolean isPowerOffAlarmType(int type){
        if(type != PRE_SCHEDULE_POWER_OFF_ALARM)
            return false;
        else
            return true;
    }

    @Override
    protected boolean schedulePoweroffAlarm(int type,
        long triggerAtTime,long interval,PendingIntent operation,IAlarmListener directReceiver,
        String listenerTag,WorkSource workSource,AlarmManager.AlarmClockInfo alarmClock,
        String callingPackage)
    {
        /// M:add for PowerOffAlarm feature(type 7) for booting phone before actual alarm@{
        if (type == PRE_SCHEDULE_POWER_OFF_ALARM) {
            if (mNativeData == -1) {
                Slog.w(TAG, "alarm driver not open ,return!");
                return false;
            }
            /// M: Extra Logging @{
            if (DEBUG_ALARM_CLOCK) {
                Slog.d(TAG, "alarm set type 7 , package name " + operation.getTargetPackage());
            }
            ///@}
            String packageName = operation.getTargetPackage();

            String setPackageName = null;
            long nowTime = System.currentTimeMillis();
            triggerAtTime = triggerAtTime - POWER_OFF_ALARM_BUFFER_TIME;

            if (triggerAtTime < nowTime) {
                /// M: Extra Logging @{
                if (DEBUG_ALARM_CLOCK) {
                    Slog.w(TAG, "PowerOff alarm set time is wrong! nowTime = " + nowTime
                       + " ; triggerAtTime = " + triggerAtTime);
                }
                ///@}
                return false;
            }
            /// M: Extra Logging @{
            if (DEBUG_ALARM_CLOCK) {
                Slog.d(TAG, "PowerOff alarm TriggerTime = " + triggerAtTime +" now = " + nowTime);
            }
            ///@}
            synchronized (mPowerOffAlarmLock) {
                removePoweroffAlarmLocked(operation.getTargetPackage());
                final int poweroffAlarmUserId = UserHandle.getCallingUserId();
                Alarm alarm = new Alarm(type, triggerAtTime, 0, 0, 0,
                        interval, operation, directReceiver, listenerTag,
                        workSource, 0, alarmClock,
                        poweroffAlarmUserId, callingPackage,false);
                addPoweroffAlarmLocked(alarm);
                if (mPoweroffAlarms.size() > 0) {
                    resetPoweroffAlarm(mPoweroffAlarms.get(0));
                }
            }
            type = RTC_WAKEUP;

        }
        return true;
        ///@}
    }

    @Override
    protected void updatePoweroffAlarmtoNowRtc(){
        final long nowRTC = System.currentTimeMillis();
        updatePoweroffAlarm(nowRTC);
    }

    /**
    *For PowerOffalarm feature, to update Poweroff Alarm
    */
    private void updatePoweroffAlarm(long nowRTC) {
        synchronized (mPowerOffAlarmLock) {
            if (mPoweroffAlarms.size() == 0) {
                return;
            }

            if (mPoweroffAlarms.get(0).when > nowRTC) {
                return;
            }

            Iterator<Alarm> it = mPoweroffAlarms.iterator();

            while (it.hasNext())
            {
                Alarm alarm = it.next();

                if (alarm.when > nowRTC) {
                    // don't fire alarms in the future
                    break;
                }
                /// M: Extra Logging @{
                if (DEBUG_ALARM_CLOCK) {
                    Slog.w(TAG, "power off alarm update deleted");
                }
                ///@}
                // remove the alarm from the list
                it.remove();
            }

            if (mPoweroffAlarms.size() > 0) {
                resetPoweroffAlarm(mPoweroffAlarms.get(0));
            }
        }
    }

    private int addPoweroffAlarmLocked(Alarm alarm) {
        ArrayList<Alarm> alarmList = mPoweroffAlarms;

        int index = Collections.binarySearch(alarmList, alarm, sIncreasingTimeOrder);
        if (index < 0) {
            index = 0 - index - 1;
        }
        if (localLOGV) Slog.v(TAG, "Adding alarm " + alarm + " at " + index);
        alarmList.add(index, alarm);
        /// M: Extra Logging @{
        if (localLOGV) {
            // Display the list of alarms for this alarm type
            Slog.v(TAG, "alarms: " + alarmList.size() + " type: " + alarm.type);
            int position = 0;
            for (Alarm a : alarmList) {
                Time time = new Time();
                time.set(a.when);
                String timeStr = time.format("%b %d %I:%M:%S %p");
                Slog.v(TAG, position + ": " + timeStr
                        + " " + a.operation.getTargetPackage());
                position += 1;
            }
        }
        ///@}

        return index;
    }

    private void removePoweroffAlarmLocked(String packageName) {
        ArrayList<Alarm> alarmList = mPoweroffAlarms;
        if (alarmList.size() <= 0) {
            return;
        }

        // iterator over the list removing any it where the intent match
        Iterator<Alarm> it = alarmList.iterator();

        while (it.hasNext()) {
            Alarm alarm = it.next();
            if (alarm.operation.getTargetPackage().equals(packageName)) {
                it.remove();
            }
        }
    }

    /**
     *For PowerOffalarm feature, this function is used for AlarmManagerService
     * to set the latest alarm registered
     */
    private void resetPoweroffAlarm(Alarm alarm) {

        String setPackageName = alarm.operation.getTargetPackage();
        long latestTime = alarm.when;

        // Only DeskClock is allowed to set this alarm
        if (mNativeData != 0 && mNativeData != -1) {
            if (setPackageName.equals("com.android.deskclock")) {
                /// M: Extra Logging @{
                if (DEBUG_ALARM_CLOCK) {
                    Slog.i(TAG, "mBootPackage = " + setPackageName + " set Prop 2");
                }
                ///@}
                SystemProperties.set("persist.sys.bootpackage", "2");
                set(mNativeData, PRE_SCHEDULE_POWER_OFF_ALARM,
                    latestTime / 1000, (latestTime % 1000) * 1000 * 1000);
                SystemProperties.set("sys.power_off_alarm", Long.toString(latestTime / 1000));
            }   else if(setPackageName.equals("com.mediatek.sqa8.aging")) {
                /// M: Extra Logging @{
                if (DEBUG_ALARM_CLOCK) {
                    Slog.i(TAG, "mBootPackage = " + setPackageName + " set Prop 2");
                }
                ///@}
                SystemProperties.set("persist.sys.bootpackage", "2");
                set(mNativeData, 7,
                    latestTime / 1000, (latestTime % 1000) * 1000 * 1000);
                SystemProperties.set("sys.power_off_alarm", Long.toString(latestTime / 1000));
            } else {
                /// M: Extra Logging @{
                if (DEBUG_ALARM_CLOCK) {
                    Slog.w(TAG, "unknown package (" + setPackageName + ") to set power off alarm");
                }
                ///@}
            }
            /// M: Extra Logging @{
            if (DEBUG_ALARM_CLOCK) {
                Slog.i(TAG, "reset power off alarm is " + setPackageName);
            }
            ///@}
       } else {
            /// M: Extra Logging @{
            if (DEBUG_ALARM_CLOCK) {
                Slog.i(TAG, " do not set alarm to RTC when fd close ");
            }
            ///@}
       }
    }
    /**
     * For PowerOffalarm feature, this function is used for APP to
     * cancelPoweroffAlarm
     */
    @Override
    public void cancelPoweroffAlarmImpl(String name) {
        /// M: Extra Logging @{
        if (DEBUG_ALARM_CLOCK) {
            Slog.i(TAG, "remove power off alarm pacakge name " + name);
        }
        ///@}
        // not need synchronized
        synchronized (mPowerOffAlarmLock) {
            removePoweroffAlarmLocked(name);
            // AlarmPair tempAlarmPair = mPoweroffAlarms.remove(name);
            // it will always to cancel the alarm in alarm driver
            String bootReason = SystemProperties.get("persist.sys.bootpackage");
            if (bootReason != null && mNativeData != 0 && mNativeData != -1) {
                if (bootReason.equals("2") && name.equals("com.android.deskclock")) {
                    set(mNativeData, PRE_SCHEDULE_POWER_OFF_ALARM, 0, 0);
                    SystemProperties.set("sys.power_off_alarm", Long.toString(0));
                }
            }
            if (mPoweroffAlarms.size() > 0) {
                resetPoweroffAlarm(mPoweroffAlarms.get(0));
            }
        }
    }
    ///@}
    /// M:Add dynamic enable alarmManager log @{
    protected void configLogTag(PrintWriter pw, String[] args, int opti) {

        if (opti >= args.length) {
            pw.println("  Invalid argument!");
        } else {
            if ("on".equals(args[opti])) {
                localLOGV = true;
                DEBUG_BATCH = true;
                DEBUG_VALIDATE = true;
            } else if ("off".equals(args[opti])) {
                localLOGV = false;
                DEBUG_BATCH = false;
                DEBUG_VALIDATE = false;
            } else if ("0".equals(args[opti])) {
                mAlarmMode = 0;
                Slog.v(TAG, "mAlarmMode = " + mAlarmMode);
            } else if ("1".equals(args[opti])) {
                mAlarmMode = 1;
                Slog.v(TAG, "mAlarmMode = " + mAlarmMode);
            } else if ("2".equals(args[opti])) {
                mAlarmMode = 2;
                Slog.v(TAG, "mAlarmMode = " + mAlarmMode);
            } else {
                pw.println("  Invalid argument!");
            }
        }
    }
    @Override
    protected void dumpWithargs(PrintWriter pw, String[] args){
        /// M: Dynamically enable alarmManager logs @{
        int opti = 0;
        while (opti < args.length) {
            String opt = args[opti];
            if (opt == null || opt.length() <= 0 || opt.charAt(0) != '-') {
                break;
            }
            opti++;
            if ("-h".equals(opt)) {
                pw.println("alarm manager dump options:");
                pw.println("  log  [on/off]");
                pw.println("  Example:");
                pw.println("  $adb shell dumpsys alarm log on");
                pw.println("  $adb shell dumpsys alarm log off");
                return;
            } else {
                pw.println("Unknown argument: " + opt + "; use -h for help");
            }
        }

        if (opti < args.length) {
            String cmd = args[opti];
            opti++;
            if ("log".equals(cmd)) {
                configLogTag(pw, args, opti);
                return;
            }
        }
        dumpImpl(pw, args);
    }
    ///@}
    /// M: added for wakeupAlarm logging @{
    @Override
    protected void updateWakeupAlarmLog(Alarm alarm){
        if ((DEBUG_BATCH) &&(alarm.type == RTC_WAKEUP || alarm.type == ELAPSED_REALTIME_WAKEUP)) {
            if (alarm.operation == null) {
                Slog.d(TAG, "wakeup alarm = " + alarm
                            + "; listener package = " + alarm.listenerTag
                            + "needGrouping = " + alarm.needGrouping);
            } else {
                Slog.d(TAG, "wakeup alarm = " + alarm
                            + "; package = " + alarm.operation.getTargetPackage()
                            + "needGrouping = " + alarm.needGrouping);
            }
        }
    }
    ///@}
}
