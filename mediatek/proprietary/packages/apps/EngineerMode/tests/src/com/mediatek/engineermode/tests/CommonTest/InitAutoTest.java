package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.net.wifi.WifiManager;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.nonsleep.NonSleepMode;
import android.bluetooth.BluetoothAdapter;
import android.app.KeyguardManager;
import android.os.PowerManager;

public class InitAutoTest extends ActivityInstrumentationTestCase2<NonSleepMode> {

    private Solo mSolo;
    private Context mContext;
    private Instrumentation mIns;
    private Activity mActivity;
    private WifiManager mWifiManager = null;
    private BluetoothAdapter adapter = null;

    public InitAutoTest() {
        super("com.mediatek.engineermode", NonSleepMode.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mIns = getInstrumentation();
        mContext = mIns.getTargetContext();
        mActivity = getActivity();
        mSolo = new Solo(mIns, mActivity);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    private void disableWiFi() {
        mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        assertNotNull(mWifiManager);
        EmOperate.waitSomeTime(EmOperate.TIME_MID);
        if (WifiManager.WIFI_STATE_DISABLED != mWifiManager.getWifiState()) {
            mWifiManager.setWifiEnabled(false);
            int jumpCount = 0;
            while (WifiManager.WIFI_STATE_DISABLED != mWifiManager.getWifiState()
                    && (jumpCount++ < 15)) {
                EmOperate.waitSomeTime(EmOperate.TIME_MID);
            }
            assertTrue("disableWiFi have issues",
                    WifiManager.WIFI_STATE_DISABLED == mWifiManager.getWifiState());
        }
    }

    private void disableBTAdaptor() {
        adapter = BluetoothAdapter.getDefaultAdapter();
        if (BluetoothAdapter.STATE_OFF != adapter.getState()) {
            adapter.disable();
            int jumpCount = 0;
            while ((BluetoothAdapter.STATE_OFF != adapter.getState()) && (jumpCount++ < 15)) {
                EmOperate.waitSomeTime(EmOperate.TIME_SUPER_LONG);
            }
        }
    }

    public static void wakeUpAndUnlock(Context context) {
        KeyguardManager km = (KeyguardManager) context
                .getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock kl = km.newKeyguardLock("unLock");

        kl.disableKeyguard();

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);

        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.SCREEN_DIM_WAKE_LOCK, "bright");

        wl.acquire();

        wl.release();
    }

    public void verifyPreconditions() {
        assertTrue(mIns != null);
        assertTrue(mActivity != null);
        assertTrue(mContext != null);
        assertTrue(mSolo != null);
    }

    @UiAnnotation
    public void testCase02_CheckBtn() {

        // mSolo.unlockScreen();
        verifyPreconditions();
        wakeUpAndUnlock(mContext);

        mSolo.assertCurrentActivity("Not NonSleepMode Class", NonSleepMode.class);

        mSolo.clickOnButton("Enable Non-sleep mode");

        mSolo.sleep(EmOperate.TIME_MID);

        disableWiFi();

        disableBTAdaptor();

        mSolo.sleep(EmOperate.TIME_MID);

        mSolo.goBack();
    }

}
