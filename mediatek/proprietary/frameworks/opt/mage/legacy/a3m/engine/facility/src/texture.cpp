/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Texture Implementation
 *
 */

/**
 * \todo Parameterise the selection of GL_TEXTURE_2D vs GL_TEXTURE_EXTERNAL_OES
 * vs GL_TEXTURE_CUBE_MAP; and thus minimise the code duplication here.
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <extensions.h> /* for OES extensions */
#include <error.h> /* for CHECK_GL_ERROR */
#include <cmath> /* std::ceil() */
#include <sstream> /* std::ostringstream */
#include <string> /* std::string */

#include <a3m/PVRTTextureAPI.h> /* For LoadPVR */
#include <a3m/log.h> /* A3M log API */
#include <a3m/rendercontext.h> /* for RenderContext */
#include <a3m/texture2d.h> /* Declaration of Texture2D Class */
#include <a3m/texturecube.h> /* Declaration of TextureCube Class */

#include <stdlib.h>
#include <stdio.h>

using namespace android;

namespace
{
  /***************************************************************************
   * Local functions
   ***************************************************************************/
  /*
   * Convert our format enum to OpenGL. Return GL_INVALID_ENUM for invalid
   * format.
   */
  GLenum glFormatFromFormat( a3m::Texture::Format format )
  {
    switch( format )
    {
    case a3m::Texture2D::RGBA:
      return GL_RGBA;
    case a3m::Texture2D::RGB:
      return GL_RGB;
    case a3m::Texture2D::LUMINANCE_ALPHA:
      return GL_LUMINANCE_ALPHA;
    case a3m::Texture2D::LUMINANCE:
      return GL_LUMINANCE;
    case a3m::Texture2D::ALPHA:
      return GL_ALPHA;
    case a3m::Texture2D::DEPTH:
      return GL_DEPTH_COMPONENT;
    default:
      return GL_INVALID_ENUM;
    }
  }

  /*
   * Convert OpenGL enum to out format enum. Return GL_RGB for invalid format.
   */
  a3m::Texture::Format formatFromGlFormat( GLenum glFormat )
  {
    switch( glFormat )
    {
    case GL_RGBA:
      return a3m::Texture2D::RGBA;
    case GL_RGB:
      return a3m::Texture2D::RGB;
    case GL_LUMINANCE_ALPHA:
      return a3m::Texture2D::LUMINANCE_ALPHA;
    case GL_LUMINANCE:
      return a3m::Texture2D::LUMINANCE;
    case GL_ALPHA:
      return a3m::Texture2D::ALPHA;
    case GL_DEPTH_COMPONENT:
      return a3m::Texture2D::DEPTH;
    default:
      return a3m::Texture2D::RGB;
    }
  }

  /*
   * Convert our type enum to OpenGL. Return GL_INVALID_ENUM for invalid type
   */
  GLenum glTypeFromType( a3m::Texture::Type type )
  {
    switch( type )
    {
    case a3m::Texture2D::UNSIGNED_BYTE:
      return GL_UNSIGNED_BYTE;
    case a3m::Texture2D::UNSIGNED_SHORT:
      return GL_UNSIGNED_SHORT;
    case a3m::Texture2D::UNSIGNED_SHORT_4_4_4_4:
      return GL_UNSIGNED_SHORT_4_4_4_4;
    case a3m::Texture2D::UNSIGNED_SHORT_5_5_5_1:
      return GL_UNSIGNED_SHORT_5_5_5_1;
    case a3m::Texture2D::UNSIGNED_SHORT_5_6_5:
      return GL_UNSIGNED_SHORT_5_6_5;
    default:
      return GL_INVALID_ENUM;
    }
  }

  /*
   * Convert our OpenGL type to our enum. Return UNSIGNED_BYTE for invalid type
   */
  a3m::Texture::Type typeFromGlType( GLenum glType )
  {
    switch( glType )
    {
    case GL_UNSIGNED_BYTE:
      return a3m::Texture2D::UNSIGNED_BYTE;
    case GL_UNSIGNED_SHORT:
      return a3m::Texture2D::UNSIGNED_SHORT;
    case GL_UNSIGNED_SHORT_4_4_4_4:
      return a3m::Texture2D::UNSIGNED_SHORT_4_4_4_4;
    case GL_UNSIGNED_SHORT_5_5_5_1:
      return a3m::Texture2D::UNSIGNED_SHORT_5_5_5_1;
    case GL_UNSIGNED_SHORT_5_6_5:
      return a3m::Texture2D::UNSIGNED_SHORT_5_6_5;
    default:
      return a3m::Texture2D::UNSIGNED_BYTE;
    }
  }

  /*
   * Convert FilterMode to OpenGL filter mode. Return -1 for invalid mode.
   */
  GLint glFilterFromFilterMode( a3m::TextureParameters::FilterMode mode )
  {
    switch( mode )
    {
    case a3m::TextureParameters::NEAREST:
      return GL_NEAREST;
    case a3m::TextureParameters::LINEAR:
      return GL_LINEAR;
    case a3m::TextureParameters::NEAREST_MIPMAP_NEAREST:
      return GL_NEAREST_MIPMAP_NEAREST;
    case a3m::TextureParameters::NEAREST_MIPMAP_LINEAR:
      return GL_NEAREST_MIPMAP_LINEAR;
    case a3m::TextureParameters::LINEAR_MIPMAP_NEAREST:
      return GL_LINEAR_MIPMAP_NEAREST;
    case a3m::TextureParameters::LINEAR_MIPMAP_LINEAR:
      return GL_LINEAR_MIPMAP_LINEAR;
    default:
      return -1;
    }
  }

  a3m::TextureParameters::FilterMode filterModeFromGlFilter( GLint mode )
  {
    switch( mode )
    {
    case GL_NEAREST:
      return a3m::TextureParameters::NEAREST;
    case GL_LINEAR:
      return a3m::TextureParameters::LINEAR;
    case GL_NEAREST_MIPMAP_NEAREST:
      return a3m::TextureParameters::NEAREST_MIPMAP_NEAREST;
    case GL_NEAREST_MIPMAP_LINEAR:
      return a3m::TextureParameters::NEAREST_MIPMAP_LINEAR;
    case GL_LINEAR_MIPMAP_NEAREST:
      return a3m::TextureParameters::LINEAR_MIPMAP_NEAREST;
    case GL_LINEAR_MIPMAP_LINEAR:
      return a3m::TextureParameters::LINEAR_MIPMAP_LINEAR;
    default:
      return a3m::TextureParameters::NEAREST;
    }
  }

  /*
   * Convert WrapMode to OpenGL wrap mode. Return -1 for invalid mode.
   */
  GLint glWrapFromWrapMode( a3m::TextureParameters::WrapMode wrap )
  {
    switch( wrap )
    {
    case a3m::TextureParameters::REPEAT:
      return GL_REPEAT;
    case a3m::TextureParameters::CLAMP:
      return GL_CLAMP_TO_EDGE;
    case a3m::TextureParameters::MIRROR:
      return GL_MIRRORED_REPEAT;
    default:
      return -1;
    }
  }

  /*
   * Convert OpenGL wrap mode to WrapMode. Return REPEAT for invalid mode.
   */
  a3m::TextureParameters::WrapMode wrapModeFromGlWrap( GLint glWrap )
  {
    switch( glWrap )
    {
    case GL_REPEAT:
      return a3m::TextureParameters::REPEAT;
    case GL_CLAMP_TO_EDGE:
      return a3m::TextureParameters::CLAMP;
    case GL_MIRRORED_REPEAT:
      return a3m::TextureParameters::MIRROR;
    default:
      return a3m::TextureParameters::REPEAT;
    }
  }

  /*
   * Convert Face to OpenGL face enum. Return GL_INVALID_ENUM for invalid face.
   */
  GLenum glFaceFromFace( a3m::TextureCube::Face face )
  {
    switch( face )
    {
    case a3m::TextureCube::POSITIVE_X:
      return GL_TEXTURE_CUBE_MAP_POSITIVE_X;
    case a3m::TextureCube::NEGATIVE_X:
      return GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
    case a3m::TextureCube::POSITIVE_Y:
      return GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
    case a3m::TextureCube::NEGATIVE_Y:
      return GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
    case a3m::TextureCube::POSITIVE_Z:
      return GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
    case a3m::TextureCube::NEGATIVE_Z:
      return GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
    default:
      return GL_INVALID_ENUM;
    }
  }

  /*
   * Get number of channels for colour from specified OpenGL format
   */
  A3M_INT32 getNumChannels(GLint glFormat)
  {
    A3M_INT32 numChannels = 0;

    switch (glFormat)
    {
    case GL_RGBA:
      numChannels = 4;
      break;

    case GL_RGB:
      numChannels = 3;
      break;

    case GL_LUMINANCE_ALPHA:
      numChannels = 2;
      break;

    case GL_LUMINANCE:
    case GL_ALPHA:
    case GL_DEPTH_COMPONENT:
      numChannels = 1;
      break;
    }

    return numChannels;
  }

  /*
   * Calculate pixel size in bytes from specified OpenGL type and format
   */
  A3M_INT32 getBytesPerPixel( GLenum glType,
                              GLenum glFormat )
  {
    A3M_INT32 bytesPerPixel = 0;
    A3M_INT32 numChannels = getNumChannels(glFormat);

    switch (glType)
    {
    case GL_UNSIGNED_BYTE:
      bytesPerPixel = numChannels;
      break;

    case GL_UNSIGNED_SHORT:
      bytesPerPixel = numChannels * 2;
      break;

    case GL_UNSIGNED_SHORT_4_4_4_4:
    case GL_UNSIGNED_SHORT_5_5_5_1:
    case GL_UNSIGNED_SHORT_5_6_5:
      bytesPerPixel = 2;
      break;
    }

    return bytesPerPixel;
  }

  A3M_BOOL isPowerOfTwo( A3M_UINT32 i )
  {
    return ( i & ( i - 1 ) ) == 0;
  }

  /*
   * Concatenates the names of the faces of a texture cube, separating each name
   * with a semicolon.
   */
  std::string getTextureCubeName(
    A3M_CHAR8 const* positiveX, A3M_CHAR8 const* negativeX,
    A3M_CHAR8 const* positiveY, A3M_CHAR8 const* negativeY,
    A3M_CHAR8 const* positiveZ, A3M_CHAR8 const* negativeZ)
  {
    std::ostringstream stream;
    stream << positiveX << ';';
    stream << negativeX << ';';
    stream << positiveY << ';';
    stream << negativeY << ';';
    stream << positiveZ << ';';
    stream << negativeZ;
    return stream.str();
  }

} // namespace

/*****************************************************************************
 * a3m Namespace
 *****************************************************************************/
namespace a3m
{
  /*
   * TextureParameters Constructor
   */
  TextureParameters::TextureParameters( FilterMode magFilterIn,
                                        FilterMode minFilterIn,
                                        WrapMode horizontalWrapIn,
                                        WrapMode verticalWrapIn )
    : magFilter( magFilterIn ),
      minFilter( minFilterIn ),
      horizontalWrap( horizontalWrapIn ),
      verticalWrap( verticalWrapIn )
  {
  }

  /******************
   * Texture2DCache *
   ******************/

  Texture2D::Ptr Texture2DCache::create(A3M_UINT32 width,
                                        A3M_UINT32 height,
                                        Texture::Format format,
                                        Texture::Type type,
                                        void const* pixels,
                                        A3M_CHAR8 const* name)
  {
    if( (width == 0) || (height == 0) )
    {
      // we've been asked to create a texture hole rather than a texture
      return createForExternalSource();
    }

    // Use default texture parameters
    TextureParameters params;

    /* Convert pixel format and parameters */
    GLenum glFormat = glFormatFromFormat( format );
    GLenum glType = glTypeFromType( type );
    GLint glMinFilter = glFilterFromFilterMode( params.minFilter );
    GLint glMagFilter = glFilterFromFilterMode( params.magFilter );
    GLint glWrapHorizontal = glWrapFromWrapMode( params.horizontalWrap );
    GLint glWrapVertical = glWrapFromWrapMode( params.verticalWrap );

    /* Check pixel format and parameters are valid */
    if( ( glFormat == GL_INVALID_ENUM ) ||
        ( glType == GL_INVALID_ENUM ) ||
        ( glMinFilter == -1 ) ||
        ( glMagFilter == -1 ) ||
        ( glWrapHorizontal == -1 ) ||
        ( glWrapVertical == -1 ) )
    {
      return Texture2D::Ptr();
    }

    // Special case for depth and non-power-of-two textures
    if( ( format == Texture::DEPTH ) ||
        !( isPowerOfTwo( width ) && isPowerOfTwo( height ) ) )
    {
      glWrapHorizontal = GL_CLAMP_TO_EDGE;
      glWrapVertical = GL_CLAMP_TO_EDGE;
      if( glMinFilter != GL_NEAREST ) { glMinFilter = GL_LINEAR; }
    }

    /* Specify no gaps between scanline data */
    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
    CHECK_GL_ERROR;

    // Allocate underlying OpenGL texture and track with resource cache
    detail::TextureResource::Ptr resource(new detail::TextureResource());
    resource->allocate();
    getResourceCache()->add(resource);

    glBindTexture( GL_TEXTURE_2D, resource->getId() );
    CHECK_GL_ERROR;

    glTexImage2D( GL_TEXTURE_2D, 0 /*level*/, glFormat,
                  static_cast< GLsizei >( width ),
                  static_cast< GLsizei >( height ),
                  0 /*border*/, glFormat, glType, pixels );
    CHECK_GL_ERROR;

    A3M_FLOAT sizeInBytes = static_cast<A3M_FLOAT>(
                              width * height * getBytesPerPixel(glType, glFormat));

    /* If minification parameter specifies mipmap usage, then generate them
     * now
     */
    A3M_BOOL hasMipMaps = A3M_FALSE;

    if( ( glMinFilter != GL_LINEAR ) && ( glMinFilter != GL_NEAREST ) )
    {
      glGenerateMipmap( GL_TEXTURE_2D );
      hasMipMaps = A3M_TRUE;
    }

    /* Set texture filter modes */
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glMinFilter );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, glMagFilter );

    /* Set texture wrapping modes */
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, glWrapHorizontal );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, glWrapVertical );
    CHECK_GL_ERROR;

    // Create new Texture2D asset and add to cache
    Texture2D::Ptr texture(new Texture2D(
                             width, height, sizeInBytes, hasMipMaps, resource));
    add(texture, name);

    return texture;
  }


  Texture2D::Ptr Texture2DCache::create(const SkBitmap& bm,
                                        A3M_CHAR8 const* name)
  {
    // Use default texture parameters
    TextureParameters params;
    GLenum glFormat;
    GLenum glType;

    /* Convert pixel format*/
    SkColorType type = bm.colorType();
    switch( type )
    {
    case kRGBA_8888_SkColorType:
      glFormat = GL_RGBA;
      glType = GL_UNSIGNED_BYTE;
      break;
    case kRGB_565_SkColorType:
      glFormat = GL_RGB;
      glType = GL_UNSIGNED_SHORT_5_6_5;
      break;
    case kAlpha_8_SkColorType:
      glFormat = GL_ALPHA;
      glType = GL_UNSIGNED_BYTE;
      break;
    case kARGB_4444_SkColorType:
      glFormat = GL_RGBA;
      glType = GL_UNSIGNED_SHORT_4_4_4_4;
      break;
    default:
      glFormat = GL_INVALID_ENUM;
    }

    A3M_UINT32 width = bm.width();
    A3M_UINT32 height = bm.height();

    /* Convert parameters */
    GLint glMinFilter = glFilterFromFilterMode( params.minFilter );
    GLint glMagFilter = glFilterFromFilterMode( params.magFilter );
    GLint glWrapHorizontal = glWrapFromWrapMode( params.horizontalWrap );
    GLint glWrapVertical = glWrapFromWrapMode( params.verticalWrap );

    /* Check pixel format and parameters are valid */
    if( ( glFormat == GL_INVALID_ENUM ) ||
        ( glType == GL_INVALID_ENUM ) ||
        ( glMinFilter == -1 ) ||
        ( glMagFilter == -1 ) ||
        ( glWrapHorizontal == -1 ) ||
        ( glWrapVertical == -1 ) )
    {
      return Texture2D::Ptr();
    }

    /* Specify no gaps between scanline data */
    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
    CHECK_GL_ERROR;

    // Allocate underlying OpenGL texture and track with resource cache
    detail::TextureResource::Ptr resource(new detail::TextureResource());
    resource->allocate();
    getResourceCache()->add(resource);

    glBindTexture( GL_TEXTURE_2D, resource->getId() );
    CHECK_GL_ERROR;

    const char* p = (const char*)bm.getPixels();

    glTexImage2D( GL_TEXTURE_2D, 0 /*level*/, glFormat,
                  static_cast< GLsizei >( width ),
                  static_cast< GLsizei >( height ),
                  0 /*border*/, glFormat, glType, p );
    CHECK_GL_ERROR;

    A3M_FLOAT sizeInBytes = static_cast<A3M_FLOAT>(
                              width * height * getBytesPerPixel(glType, glFormat));

    /* If minification parameter specifies mipmap usage, then generate them
     * now
     */
    A3M_BOOL hasMipMaps = A3M_FALSE;

    if( ( glMinFilter != GL_LINEAR ) && ( glMinFilter != GL_NEAREST ) )
    {
      glGenerateMipmap( GL_TEXTURE_2D );
      hasMipMaps = A3M_TRUE;
    }

    /* Set texture filter modes */
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glMinFilter );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, glMagFilter );

    /* Set texture wrapping modes */
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, glWrapHorizontal );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, glWrapVertical );
    CHECK_GL_ERROR;

    // Create new Texture2D asset and add to cache
    Texture2D::Ptr texture(new Texture2D(
                             width, height, sizeInBytes, hasMipMaps, resource));
    add(texture, name);

    return texture;
  }

  Texture2D::Ptr Texture2DCache::create(sp<GraphicBuffer> gb)
  {
    EGLDisplay dpy = eglGetCurrentDisplay();
    EGLContext context = eglGetCurrentContext();

    EGLClientBuffer clientBuffer = (EGLClientBuffer)gb->getNativeBuffer();
    EGLImageKHR img = eglCreateImageKHR(dpy, EGL_NO_CONTEXT, EGL_NATIVE_BUFFER_ANDROID,
            clientBuffer, 0);

    if (img == EGL_NO_IMAGE_KHR) {
        A3M_LOG_ERROR("EGL_NO_IMAGE_KHR");
        //return NULL;
    }

    // Create a new 'resource'
    detail::TextureResource::Ptr resource(new detail::TextureResource());
    // Allocate underlying OpenGL texture [i.e. glGenTextures( 1, &Name );]
    resource->allocate();
    // add this resource to the cache

    getResourceCache()->add(resource);

    glBindTexture(GL_TEXTURE_EXTERNAL_OES, resource->getId());
    glEGLImageTargetTexture2DOES(GL_TEXTURE_EXTERNAL_OES, (GLeglImageOES)img);
    CHECK_GL_ERROR;
    if (img != EGL_NO_IMAGE_KHR) {
        eglDestroyImageKHR(dpy, img);
    }

    Texture2D::Ptr texture(new Texture2D( 0, 0, 1, A3M_FALSE, resource,
                    /* is external? */ A3M_TRUE));

    // The texture->set methods assume GL_TEXTURE_2D, so use 'raw' OpenGL
    // instead
    glTexParameteri( GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MIN_FILTER,
                    GL_NEAREST);
    glTexParameteri( GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR);
    glTexParameteri( GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_S,
                    GL_CLAMP_TO_EDGE);
    glTexParameteri( GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_T,
                    GL_CLAMP_TO_EDGE);

    return texture;
  }

  Texture2D::Ptr Texture2DCache::create(A3M_UINT32 width,
                                        A3M_UINT32 height,
                                        A3M_FLOAT bytesPerPixel,
                                        A3M_BOOL hasMipMaps,
                                        A3M_UINT32 glId,
                                        A3M_CHAR8 const* name)
  {
    // Track existing OpenGL resource with resource cache
    detail::TextureResource::Ptr resource(new detail::TextureResource(glId));
    getResourceCache()->add(resource);

    // Create new asset and add to cache
    Texture2D::Ptr texture(new Texture2D(
                             width, height, bytesPerPixel, hasMipMaps, resource));
    add(texture, name);

    return texture;
  }

  Texture2D::Ptr Texture2DCache::createFromBackbuffer(
    RenderContext const& context,
    Texture::Format format,
    A3M_CHAR8 const* name)
  {
    if (format == Texture::DEPTH)
    {
      A3M_LOG_ERROR("Creating depth texture from backbuffer"
                    " is not permitted.", 0);
      return Texture2D::Ptr();
    }

    // Get GL format and type for bytes per pixel calculation
    GLint glFormat = glFormatFromFormat(format);
    GLint glType;
    glGetIntegerv(GL_IMPLEMENTATION_COLOR_READ_TYPE, &glType);

    // Get parameters to construct texture from backbuffer
    A3M_UINT32 width = static_cast<A3M_UINT32>(context.getViewportWidth());
    A3M_UINT32 height = static_cast<A3M_UINT32>(context.getViewportHeight());
    A3M_BOOL hasMipMaps = A3M_FALSE;
    A3M_FLOAT bytesPerPixel = static_cast<A3M_FLOAT>(
                                getBytesPerPixel(glFormat, glType));

    // Allocate underlying OpenGL texture and track with resource cache
    detail::TextureResource::Ptr resource(new detail::TextureResource());
    resource->allocate();
    getResourceCache()->add(resource);

    glBindTexture(GL_TEXTURE_2D, resource->getId());
    CHECK_GL_ERROR;

    // Get backbuffer pixel data
    glCopyTexImage2D(GL_TEXTURE_2D, 0, glFormat, 0, 0, width, height, 0 );
    CHECK_GL_ERROR;

    // Create new asset and add to cache
    Texture2D::Ptr texture(new Texture2D(
                             width, height, bytesPerPixel, hasMipMaps, resource));
    add(texture, name);

    // Set the texture parameters
    texture->setMagFilter(TextureParameters::LINEAR);
    texture->setMinFilter(TextureParameters::NEAREST);
    texture->setHorizontalWrap(TextureParameters::CLAMP);
    texture->setVerticalWrap(TextureParameters::CLAMP);

    return texture;
  }

  Texture2D::Ptr Texture2DCache::createForExternalSource()
  {
    // Check if platform support external image source
    if (!GL_OES_EGL_image_external_supported())
    {
      A3M_LOG_ERROR("Extension \"GL_OES_EGL_image_external\""
                    " not supported.");
      return Texture2D::Ptr();
    }

    // Create a new 'resource'
    detail::TextureResource::Ptr resource(new detail::TextureResource());
    // Allocate underlying OpenGL texture [i.e. glGenTextures( 1, &Name );]
    resource->allocate();
    // add this resource to the cache
    getResourceCache()->add(resource);

    // Bind using special 'EXTERNAL' target rather than GL_TEXTURE_2D
    glBindTexture(GL_TEXTURE_EXTERNAL_OES, resource->getId());
    CHECK_GL_ERROR;

    Texture2D::Ptr texture(new Texture2D( 0, 0, 1, A3M_FALSE, resource,
                                          /* is external? */ A3M_TRUE));

    /* Khronos spec:

            "When <target> is TEXTURE_EXTERNAL_OES only NEAREST and LINEAR are
        accepted as TEXTURE_MIN_FILTER and only CLAMP_TO_EDGE is accepted as
        TEXTURE_WRAP_S and TEXTURE_WRAP_T.  Attempting to set other values for
        TEXTURE_MIN_FILTER, TEXTURE_WRAP_S, or TEXTURE_WRAP_T will result in
        an INVALID_ENUM error.
    */

    // The texture->set methods assume GL_TEXTURE_2D, so use 'raw' OpenGL
    // instead
    glTexParameteri( GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MIN_FILTER,
                     GL_NEAREST);
    glTexParameteri( GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MAG_FILTER,
                     GL_LINEAR);
    glTexParameteri( GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_S,
                     GL_CLAMP_TO_EDGE);
    glTexParameteri( GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_T,
                     GL_CLAMP_TO_EDGE);

    return texture;
  }

  A3M_UINT32 getTotalAssetSizeInBytes(Texture2DCache const& cache)
  {
    A3M_UINT32 totalBytes = 0;

    for (A3M_UINT32 i = 0; i < cache.getCount(); ++i)
    {
      Texture2D::Ptr texture = cache.get(i);
      A3M_ASSERT(texture);
      totalBytes += texture->getSizeInBytes();
    }

    return totalBytes;
  }

  /*************
   * Texture2D *
   *************/

  Texture2D::Texture2D(
    A3M_UINT32 width,
    A3M_UINT32 height,
    A3M_FLOAT bytesPerPixel,
    A3M_BOOL hasMipMaps,
    detail::TextureResource::Ptr const& resource,
    A3M_BOOL isExternal ) :
    m_resource(resource),
    m_width(width),
    m_height(height),
    m_hasMipMaps(hasMipMaps),
    m_sizeInBytes(static_cast<A3M_UINT32>(bytesPerPixel* m_width* m_height)),
    m_isExternal(isExternal)
  {
  }

  void Texture2D::setHorizontalWrap( TextureParameters::WrapMode mode )
  {
    if( m_isExternal ) {
	  return;
    }
    GLint glWrapHorizontal = glWrapFromWrapMode( mode );
    glBindTexture( GL_TEXTURE_2D, m_resource->getId() );
    CHECK_GL_ERROR;
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, glWrapHorizontal );
    CHECK_GL_ERROR;
  }

  TextureParameters::WrapMode Texture2D::getHorizontalWrap() const
  {
    if( m_isExternal ) {
	  return a3m::TextureParameters::REPEAT;
    }
    GLint glWrapHorizontal;
    glBindTexture( GL_TEXTURE_2D, m_resource->getId() );
    CHECK_GL_ERROR;
    glGetTexParameteriv( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, &glWrapHorizontal );
    CHECK_GL_ERROR;
    return wrapModeFromGlWrap(glWrapHorizontal);
  }

  void Texture2D::setVerticalWrap( TextureParameters::WrapMode mode )
  {
    if( m_isExternal ) {
	  return;
    }
    GLint glWrapVertical = glWrapFromWrapMode( mode );
    glBindTexture( GL_TEXTURE_2D, m_resource->getId() );
    CHECK_GL_ERROR;
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, glWrapVertical );
    CHECK_GL_ERROR;
  }

  TextureParameters::WrapMode Texture2D::getVerticalWrap() const
  {
    if( m_isExternal ) {
	  return a3m::TextureParameters::REPEAT;
    }
    GLint glWrapVertical;
    glBindTexture( GL_TEXTURE_2D, m_resource->getId() );
    CHECK_GL_ERROR;
    glGetTexParameteriv( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, &glWrapVertical );
    CHECK_GL_ERROR;
    return wrapModeFromGlWrap(glWrapVertical);
  }

  /*
   * Enable texture
   */
  void Texture2D::enable()
  {
    if( m_isExternal )
    {
      glBindTexture( GL_TEXTURE_EXTERNAL_OES, m_resource->getId() );
    }
    else
    {
      glBindTexture( GL_TEXTURE_2D, m_resource->getId() );
    }
    CHECK_GL_ERROR;
  }

  /*
   * Set minification filter
   */
  void Texture2D::setMinFilter( TextureParameters::FilterMode filterIn )
  {
    if( m_isExternal ) {
	  return;
    }

    GLint glMinFilter = glFilterFromFilterMode( filterIn );
    glBindTexture( GL_TEXTURE_2D, m_resource->getId() );
    CHECK_GL_ERROR;

    if( !m_hasMipMaps &&
        ( glMinFilter != GL_LINEAR ) &&
        ( glMinFilter != GL_NEAREST ) )
    {
      glGenerateMipmap( GL_TEXTURE_2D );
      m_hasMipMaps = A3M_TRUE;
      CHECK_GL_ERROR;
    }

    /* Set texture filter modes */
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glMinFilter );
    CHECK_GL_ERROR;
  }

  void Texture2D::release()
  {
    m_resource->doDeallocate(m_resource->getId());
  }

  /*
   * Get minification filter
   */
  TextureParameters::FilterMode Texture2D::getMinFilter() const
  {

    if( m_isExternal ) {
	  return a3m::TextureParameters::NEAREST;
    }

    glBindTexture( GL_TEXTURE_2D, m_resource->getId() );
    CHECK_GL_ERROR;

    GLint glMinFilter;
    glGetTexParameteriv( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, &glMinFilter );
    CHECK_GL_ERROR;

    return filterModeFromGlFilter( glMinFilter );
  }

  /*
   * Set magnification filter
   */
  void Texture2D::setMagFilter( TextureParameters::FilterMode filterIn )
  {

    if( m_isExternal ) {
	  return;
    }

    GLint glMagFilter = glFilterFromFilterMode( filterIn );
    glBindTexture( GL_TEXTURE_2D, m_resource->getId() );
    CHECK_GL_ERROR;

    if (filterIn == TextureParameters::LINEAR ||
        filterIn == TextureParameters::NEAREST)
    {
      /* Set texture filter modes */
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, glMagFilter );
      CHECK_GL_ERROR;
    }
    else
    {
      A3M_LOG_ERROR( "Only LINEAR and NEAREST supported for Mag Filter.", 0);
    }
  }

  /*
   * Get magnification filter
   */
  TextureParameters::FilterMode Texture2D::getMagFilter() const
  {

    if( m_isExternal ) {
	  return a3m::TextureParameters::NEAREST;
    }

    glBindTexture( GL_TEXTURE_2D, m_resource->getId() );
    CHECK_GL_ERROR;

    GLint glMagFilter;
    glGetTexParameteriv( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, &glMagFilter );
    CHECK_GL_ERROR;

    return filterModeFromGlFilter( glMagFilter );
  }

  /********************
   * TextureCubeCache *
   ********************/

  TextureCube::Ptr TextureCubeCache::create(A3M_UINT32 width, A3M_UINT32 height,
      Texture::Format format, Texture::Type type, A3M_CHAR8 const* name)
  {
    // Allocate underlying OpenGL texture and track with resource cache
    detail::TextureResource::Ptr resource(new detail::TextureResource());
    resource->allocate();
    getResourceCache()->add(resource);

    // Create new asset and add to cache
    TextureCube::Ptr texture(new TextureCube(
                               width, height, format, type, resource));
    add(texture, name);

    return texture;
  }

  TextureCube::Ptr get(
    TextureCubeCache& cache,
    A3M_CHAR8 const* positiveX, A3M_CHAR8 const* negativeX,
    A3M_CHAR8 const* positiveY, A3M_CHAR8 const* negativeY,
    A3M_CHAR8 const* positiveZ, A3M_CHAR8 const* negativeZ)
  {
    return cache.get(getTextureCubeName(
                       positiveX, negativeX,
                       positiveY, negativeY,
                       positiveZ, negativeZ).c_str());
  }

  A3M_BOOL exists(
    TextureCubeCache& cache,
    A3M_CHAR8 const* positiveX, A3M_CHAR8 const* negativeX,
    A3M_CHAR8 const* positiveY, A3M_CHAR8 const* negativeY,
    A3M_CHAR8 const* positiveZ, A3M_CHAR8 const* negativeZ)
  {
    return cache.exists(getTextureCubeName(
                          positiveX, negativeX,
                          positiveY, negativeY,
                          positiveZ, negativeZ).c_str());
  }

  /***************
   * TextureCube *
   ***************/

  /*
   * TextureCube Constructor
   */
  TextureCube::TextureCube(A3M_UINT32 width, A3M_UINT32 height, Format format,
                           Type type, detail::TextureResource::Ptr const& resource) :
    m_resource( resource ),
    m_width( width ),
    m_height( height ),
    m_format( format ),
    m_type( type ),
    m_nFacesSet( 0 )
  {
    // Clamp cube textures to avoid visible seams between faces
    m_parameters.horizontalWrap = TextureParameters::CLAMP;
    m_parameters.verticalWrap = TextureParameters::CLAMP;
  }

  /*
   * Set data for cube map face
   */
  void TextureCube::setFace( Face face, const void* pixels )
  {
    if( !m_resource->getId() ) { return; }

    /* Convert parameters */
    GLenum glFormat = glFormatFromFormat( m_format );
    GLenum glType = glTypeFromType( m_type );
    GLenum glFace = glFaceFromFace( face );
    GLint glMinFilter = glFilterFromFilterMode( m_parameters.minFilter );
    GLint glMagFilter = glFilterFromFilterMode( m_parameters.magFilter );
    GLint glWrapHorizontal = glWrapFromWrapMode( m_parameters.horizontalWrap );
    GLint glWrapVertical = glWrapFromWrapMode( m_parameters.verticalWrap );

    /* Check parameters are valid */
    if( ( glFormat == GL_INVALID_ENUM ) ||
        ( glType == GL_INVALID_ENUM ) ||
        ( glFace == GL_INVALID_ENUM ) ||
        ( glMinFilter == -1 ) ||
        ( glMagFilter == -1 ) )
    {
      return;
    }

    if ( m_width != m_height )
    {
      A3M_LOG_ERROR(
        "Error: Cube map texture is not square.  Width and height "
        "parameter of faces of the cube map texture are not equal. If "
        "used glTexImage2D after this then, it will result into GL "
        "error : \'Invalid Value\' " );
      return;
    }

    // Special case for non-power-of-two textures
    if( !isPowerOfTwo( m_width ) || !isPowerOfTwo( m_height ) )
    {
      glWrapHorizontal = GL_CLAMP_TO_EDGE;
      glWrapVertical   = GL_CLAMP_TO_EDGE;
      if( glMinFilter != GL_NEAREST ) { glMinFilter = GL_LINEAR; }
    }

    glBindTexture( GL_TEXTURE_CUBE_MAP, m_resource->getId() );
    CHECK_GL_ERROR;

    glTexImage2D( glFace, 0 /*level*/, glFormat,
                  static_cast< GLsizei >( m_width ),
                  static_cast< GLsizei >( m_height ),
                  0 /*border*/, glFormat, glType, pixels );
    CHECK_GL_ERROR;

    ++m_nFacesSet;
    if( m_nFacesSet == 6 )
    {
      /* If minification parameter specifies mipmap usage, then generate them
       * now
       */
      if( ( glMinFilter != GL_LINEAR ) &&
          ( glMinFilter != GL_NEAREST ) )
      {
        glGenerateMipmap( GL_TEXTURE_CUBE_MAP );
        CHECK_GL_ERROR;
      }

      glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER,
                       glMinFilter );
      CHECK_GL_ERROR;
      glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER,
                       glMagFilter );
      CHECK_GL_ERROR;

      /* Set texture wrapping modes */
      glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S,
                       glWrapHorizontal );
      CHECK_GL_ERROR;
      glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T,
                       glWrapVertical );
      CHECK_GL_ERROR;
    }
  }

  /*
   * Enable texture
   */
  void TextureCube::enable()
  {
    glBindTexture( GL_TEXTURE_CUBE_MAP, m_resource->getId() );
    CHECK_GL_ERROR;
  }

} /* namespace a3m */
