/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.mms.plugin;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.PduHeaders;
import com.mediatek.mms.plugin.R;

import com.mediatek.mms.ext.DefaultOpSendTransactionExt;
import com.mediatek.android.mms.pdu.MtkSendConf;

/**
 * M: mms failed notify ext for op09.
 */
public class Op09SendTransactionExt extends DefaultOpSendTransactionExt {
    private static final String TAG = "Mms/Op09SendTransactionExt";
    private Context mContext = null;
    private Resources mResources = null;
    private static final int REQUEST_RESPONSE_TEXT = 0;


    private Handler mToastHandler = new Handler() {

        public void handleMessage(Message msg) {
            String str = null;

            switch(msg.what) {
            case REQUEST_RESPONSE_TEXT:
                str = (String) msg.obj;
                break;

            default:
                break;
            }

            if (str != null) {
                Toast.makeText(mContext, str, Toast.LENGTH_LONG).show();
            }
        }
    };

    /**
     * M: Constructor.
     * @param context the Context.
     */
    public Op09SendTransactionExt(Context context) {
        super(context);
        mContext = context;
        mResources = mContext.getResources();
    }

    public void checkSendResult(int responseStatus, EncodedStringValue responseText) {

        if (responseStatus != PduHeaders.RESPONSE_STATUS_OK) {
            if (responseText != null) {
                String txt = responseText.getString();
                popupToast(REQUEST_RESPONSE_TEXT, txt);
            }
        }
    }

    private void popupToast(int reason, String statusText) {
        Log.d("@M_" + TAG, "MmsFailedNotifyExt.popupToast()");

        switch(reason) {
        case REQUEST_RESPONSE_TEXT:
            if (statusText == null) {
                Log.d("@M_" + TAG, "popupToast() statusText == null!");
                return;
            }
            Log.d("@M_" + TAG, "popupToast():REQUEST_RESPONSE_TEXT");
            Message msg = mToastHandler.obtainMessage(REQUEST_RESPONSE_TEXT);
            msg.obj = statusText;
            mToastHandler.sendMessage(msg);
            break;

        default:
            Log.d("@M_" + TAG, "popupToast():default");
            break;
        }
    }

    public void checkSendResult(MtkSendConf sendConf) {
        checkSendResult(sendConf.getResponseStatus(), sendConf.getResponseText());
    }
}
