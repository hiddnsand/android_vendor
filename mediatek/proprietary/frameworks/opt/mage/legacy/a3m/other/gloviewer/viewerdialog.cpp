/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#include "viewerdialog.h"
#define NOMINMAX
#include <commctrl.h>
#include <sstream>
#include <algorithm>

namespace
{
  BOOL CALLBACK dlgProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam)
  {
    // Only windows which have been created in ViewerDialog will use this event
    // handler, so the following cast should be safe.
    ViewerDialog *dlg = reinterpret_cast< ViewerDialog *>(
      GetWindowLongPtr( hwndDlg, GWL_USERDATA ) );
    if( !dlg )
      return FALSE;

    switch (message)
    {
    case WM_COMMAND:
      dlg->onCommand( LOWORD(wParam), HIWORD(wParam), lParam );
      return FALSE;

    case WM_NOTIFY:
      dlg->onNotify( wParam, lParam );
      return FALSE;

    case WM_HSCROLL:
      if( lParam )
      {
        dlg->onHScroll( SendMessage( (HWND)lParam, TBM_GETPOS, 0, 0 ) );

        if( LOWORD(wParam) == TB_ENDTRACK )
        {
          dlg->onHScrollEnd();
        }
      }
      break;

    case WM_DRAWITEM:
    {
      LPDRAWITEMSTRUCT pDIS = (LPDRAWITEMSTRUCT)lParam;
      dlg->onDrawCtl( pDIS->CtlID, pDIS->hDC, pDIS->rcItem );
      return TRUE;
    }

    case WM_CLOSE:
      dlg->visible( !dlg->visible() );
      return FALSE;
    }
    return FALSE;
  }

  BYTE toByte( A3M_FLOAT v )
  {
    return static_cast<BYTE>(std::min( std::max( v, 0.f), 1.f ) * 255);
  }
}


ViewerDialog::ViewerDialog( HWND parentWindowHandle, A3M_UINT32 resource )
{
  m_wnd = CreateDialog( GetModuleHandle(0),
                MAKEINTRESOURCE(resource),
                (HWND)parentWindowHandle,
                (DLGPROC)dlgProc );
  SetWindowLongPtr( m_wnd, GWLP_USERDATA, (LONG_PTR)this );
}

A3M_BOOL ViewerDialog::visible() const
{
  return ( GetWindowLongPtr( m_wnd, GWL_STYLE ) & WS_VISIBLE ) != 0;
}

void ViewerDialog::visible( A3M_BOOL makeVisible )
{
  ShowWindow( m_wnd, makeVisible ? SW_RESTORE : SW_HIDE );
}

HWND ViewerDialog::hwnd( A3M_UINT32 control ) const
{
  return GetDlgItem( m_wnd, control );
}

A3M_UINT32 ViewerDialog::control( HWND hwnd )
{
  return GetWindowLong( hwnd, GWL_ID );
}

void ViewerDialog::setChecked( A3M_UINT32 control, A3M_BOOL checked ) const
{
  if (checked)
  {
      SendMessage( hwnd( control ), BM_SETCHECK, BST_CHECKED, 0);
  }
  else
  {
      SendMessage( hwnd( control ), BM_SETCHECK, BST_UNCHECKED, 0);
  }
}

A3M_BOOL ViewerDialog::isChecked( A3M_UINT32 control ) const
{
   return SendMessage( hwnd( control ), BM_GETCHECK, 0, 0) == BST_CHECKED;
}

void ViewerDialog::setEditText( A3M_UINT32 control, A3M_CHAR8 const *text )
{
  A3M_CHAR8 buffer[256];
  GetDlgItemText( hwnd(), control, buffer, 256 );
  // Only set if text has changed to reduce flicker
  if( strcmp( buffer, text ) != 0 )
  {
      SetDlgItemText( hwnd(), control, text );
  }
}

void ViewerDialog::setEditText( A3M_UINT32 control, A3M_FLOAT v )
{
  std::ostringstream stream;
  stream << v;
  setEditText( control, stream.str().c_str() );
}

void ViewerDialog::setEditText( A3M_UINT32 control, A3M_INT32 v )
{
  std::ostringstream stream;
  stream << v;
  setEditText( control, stream.str().c_str() );
}

void ViewerDialog::setEditText( A3M_UINT32 control1, A3M_UINT32 control2,
                                A3M_UINT32 control3, a3m::Vector3f const &v )
{
  setEditText( control1, v.x );
  setEditText( control2, v.y );
  setEditText( control3, v.z );
}

void ViewerDialog::setEditText( A3M_UINT32 control1, A3M_UINT32 control2,
                                A3M_UINT32 control3, A3M_UINT32 control4,
                                a3m::Vector4f const &v )
{
  setEditText( control1, v.x );
  setEditText( control2, v.y );
  setEditText( control3, v.z );
  setEditText( control4, v.w );
}

void ViewerDialog::setEditText( A3M_UINT32 control1, A3M_UINT32 control2,
                                A3M_UINT32 control3, a3m::Colour4f const &c )
{
  setEditText( control1, c.r );
  setEditText( control2, c.g );
  setEditText( control3, c.b );
}

void ViewerDialog::setEditText( A3M_UINT32 control1, A3M_UINT32 control2,
                                A3M_UINT32 control3, A3M_UINT32 control4,
                                a3m::Colour4f const &c )
{
  setEditText( control1, c.r );
  setEditText( control2, c.g );
  setEditText( control3, c.b );
  setEditText( control4, c.a );
}

void ViewerDialog::setEditColour( A3M_UINT32 control, a3m::Colour4f const &c )
{
  SetWindowLongPtr( hwnd( control ), GWLP_USERDATA,
    (LONG_PTR)RGB( toByte( c.r ), toByte( c.g ),toByte( c.b ) ) );
  InvalidateRect( hwnd( control ), 0, A3M_FALSE );
}

void ViewerDialog::setEditColour( A3M_UINT32 control, a3m::Vector4f const &c )
{
  SetWindowLongPtr( hwnd( control ), GWLP_USERDATA,
    (LONG_PTR)RGB( toByte( c.x ), toByte( c.y ),toByte( c.z ) ) );
  InvalidateRect( hwnd( control ), 0, A3M_FALSE );
}

void ViewerDialog::setCtlVisible( A3M_UINT32 control, A3M_BOOL visible )
{
  ShowWindow( hwnd( control ), visible ? SW_RESTORE : SW_HIDE );
}

void ViewerDialog::setCtlEnabled( A3M_UINT32 control, A3M_BOOL enabled )
{
  EnableWindow( hwnd( control ), enabled );
}

void ViewerDialog::setScrollPos( A3M_UINT32 control, A3M_INT32 position )
{
  SendMessage( hwnd( control ), TBM_SETPOS, (WPARAM) TRUE, (LPARAM) position );
}

void ViewerDialog::setScrollPageSize( A3M_UINT32 control, A3M_INT32 size )
{
  SendMessage( hwnd( control ), TBM_SETPAGESIZE, 0, (LPARAM) size );
}

void ViewerDialog::setScrollRange( A3M_UINT32 control,
  A3M_INT32 min, A3M_INT32 max )
{
  SendMessage( hwnd( control ), TBM_SETRANGE,
    (WPARAM) TRUE, (LPARAM) MAKELONG( min, max ) );
}

void ViewerDialog::onDrawCtl( A3M_INT32 control, HDC dc, RECT const &rect )
{
  COLORREF c = (COLORREF) GetWindowLongPtr( hwnd( control ), GWLP_USERDATA );
  SelectObject( dc, CreateSolidBrush( c ) );
  Rectangle( dc, rect.left, rect.top, rect.right, rect.bottom);
}
