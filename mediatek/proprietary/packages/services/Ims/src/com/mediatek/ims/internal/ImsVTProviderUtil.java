/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims.internal;

import android.content.Context;
import android.os.PatternMatcher;
import android.os.SystemProperties;
import android.telecom.VideoProfile.CameraCapabilities;
import android.telecom.Connection;
import android.telecom.VideoProfile;
import android.view.Surface;

// for sync operation of Util to a individual thread (not in call)
import android.os.HandlerThread;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.android.internal.os.SomeArgs;

// for VilTE switch change
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.TelephonyManager;
import com.android.ims.ImsConfig;
import com.android.ims.ImsConfigListener;
import com.android.ims.ImsManager;

import android.hardware.camera2.CameraCharacteristics;
import android.telecom.VideoProfile;
import java.util.StringTokenizer;
import java.util.Map;
import android.util.Log;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.lang.Integer;
import java.lang.Long;

import com.mediatek.ims.internal.ImsVTProvider;
import com.mediatek.ims.internal.VTSource;

public class ImsVTProviderUtil {

    private static final String TAG = "ImsVT Util";
    public static final String MULTI_IMS_SUPPORT = "ro.mtk_multiple_ims_support";
    private static final String VILTE_SUPPORT = "persist.mtk_vilte_support";
    private static final String VIWIFI_SUPPORT = "persist.mtk_viwifi_support";

    private static final int MSG_INIT_REFVTP            = 1;
    private static final int MSG_BIND                   = 2;
    private static final int MSG_SETUIMODE              = 3;
    private static final int MSG_INIT_SWITCH_FEATURE    = 4;

    public static final int HIDE_ME_TYPE_NONE           = 0;
    public static final int HIDE_ME_TYPE_DISABLE        = 1;
    public static final int HIDE_ME_TYPE_FREEZE         = 2;
    public static final int HIDE_ME_TYPE_PICTURE        = 3;

    public static final int HIDE_YOU_TYPE_DISABLE       = 0;
    public static final int HIDE_YOU_TYPE_ENABLE        = 1;

    public static final int TURN_OFF_CAMERA             = -1;

    public static final int UI_MODE_FG                  = 0;
    public static final int UI_MODE_BG                  = 1;
    public static final int UI_MODE_FULL_SCREEN         = 2;
    public static final int UI_MODE_NORMAL_SCREEN       = 3;
    public static final int UI_MODE_RESET               = 4;

    // for notify call is end
    // IMCB sometimes won't notify (in some error case)
    public static final int UI_MODE_DESTROY             = 65536;

    public static final int TAG_VILTE_MOBILE            = 0xFF000000;
    public static final int TAG_VILTE_WIFI              = 0xFF100000;

    public static boolean VILTE_MOBILE_ON  = true;
    public static boolean VILTE_WIFI_ON    = true;

    // for ImsConfig
    private final static String EXTRA_PHONE_ID = "phone_id";

    // Singleton instance
    private static ImsVTProviderUtil mInstance = getInstance();

    public static class Size {
        /**
         * Sets the dimensions for pictures.
         *
         * @param w the photo width (pixels)
         * @param h the photo height (pixels)
         */
        public Size(int w, int h) {
            width = w;
            height = h;
        }

        /**
         * Compares {@code obj} to this size.
         *
         * @param obj the object to compare this size with.
         * @return {@code true} if the width and height of {@code obj} is the
         *         same as those of this size. {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Size)) {
                return false;
            }
            Size s = (Size) obj;
            return width == s.width && height == s.height;
        }

        @Override
        public int hashCode() {
            return width * 32713 + height;
        }

        /** width of the picture */
        public int width;
        /** height of the picture */
        public int height;
    };

    public class FeatureValueReceiver extends BroadcastReceiver {

        private ImsVTProviderUtil   mOwner;
        private boolean             mViLTEValue;
        private boolean             mViWifiValue;

        public void setOwner(ImsVTProviderUtil owner) {
            mOwner = owner;
        }

        public void setInitViLTEValue(boolean value) {
            mViLTEValue = value;
            VILTE_MOBILE_ON = value;
        }

        public void setInitViWifiValue(boolean value) {
            mViWifiValue = value;
            VILTE_WIFI_ON = value;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive");

            if (intent == null || intent.getAction() == null) {
                return;
            }

            if (intent.getAction().equals(ImsConfig.ACTION_IMS_FEATURE_CHANGED)) {

                int feature = intent.getIntExtra(ImsConfig.EXTRA_CHANGED_ITEM, -1);
                int phoneId = intent.getIntExtra(EXTRA_PHONE_ID, -1);
                int status  = intent.getIntExtra(ImsConfig.EXTRA_NEW_VALUE, -1);

                Log.d(TAG, "onRecevied feature changed phoneId: " + phoneId +
                        ", feature: " + feature + ", status: " + status);

                if (phoneId < 0) {
                    Log.d(TAG, "ignore it for invalid SIM id");
                    return;
                }

                if (feature == ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE) {

                    if(status == ImsConfig.FeatureValueConstants.OFF) {
                        mViLTEValue = false;
                    } else if (status == ImsConfig.FeatureValueConstants.ON) {
                        mViLTEValue = true;
                    }
                    mOwner.switchFeature(TAG_VILTE_MOBILE, mViLTEValue);

                } else if (feature == ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_WIFI) {

                    if(status == ImsConfig.FeatureValueConstants.OFF) {
                        mViWifiValue = false;
                    } else if (status == ImsConfig.FeatureValueConstants.ON) {
                        mViWifiValue = true;
                    }
                    mOwner.switchFeature(TAG_VILTE_WIFI, mViWifiValue);
                }
            }
        }
    };

    public static class ImsVTMessagePacker {

        public String packFromVdoProfile(VideoProfile videoProfile) {

            StringBuilder flattened = new StringBuilder();

            flattened.append("mVideoState");
            flattened.append("=");
            flattened.append("" + videoProfile.getVideoState());
            flattened.append(";");
            flattened.append("mQuality");
            flattened.append("=");
            flattened.append("" + videoProfile.getQuality());
            flattened.append(";");

            // chop off the extra semicolon at the end
            flattened.deleteCharAt(flattened.length() - 1);

            Log.d(TAG, "[packFromVdoProfile] profile = " + flattened.toString());

            return flattened.toString();
        }

        public VideoProfile unPackToVdoProfile(String flattened) {

            Log.d(TAG, "[unPackToVdoProfile] flattened = " + flattened);

            StringTokenizer tokenizer = new StringTokenizer(flattened, ";");
            int state = VideoProfile.STATE_BIDIRECTIONAL;
            int qty = VideoProfile.QUALITY_DEFAULT;

            while (tokenizer.hasMoreElements()) {
                String kv = tokenizer.nextToken();
                int pos = kv.indexOf('=');
                if (pos == -1) {
                    continue;
                }
                String k = kv.substring(0, pos);
                String v = kv.substring(pos + 1);

                Log.d(TAG, "[unPackToVdoProfile] k = " + k + ", v = " + v);

                if (k.equals("mVideoState")) {
                    state = Integer.valueOf(v).intValue();
                } else if (k.equals("mQuality")) {
                    qty = Integer.valueOf(v).intValue();
                }
            }
            Log.d(TAG, "[unPackToVdoProfile] state = " + state + ", qty = " + qty);
            return new VideoProfile(state, qty);
        }

    }

    private static ImsVTMessagePacker               mPacker = new ImsVTMessagePacker();
    private static FeatureValueReceiver             mFeatureValueReceiver;
    private static Map<String, Object>              mProviderById = new ConcurrentHashMap<>();
    private static Map<String, Object>              mSurfaceStatusById = new HashMap<>();
    private static Map<String, Object>              mDataUsageById = new HashMap<>();
    public  static Context                          mContext;

    protected static HandlerThread                  mProviderHandlerThread;
    private static Handler                          mProviderHandler;

    private ImsVTProviderUtil() {

        mProviderHandlerThread = new HandlerThread("ProviderHandlerThread");
        mProviderHandlerThread.start();

        mProviderHandler = new Handler(mProviderHandlerThread.getLooper()) {

            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MSG_INIT_REFVTP: {
                        setContextAndInitRefVTPInternal((Context) msg.obj);
                        break;
                    }
                    case MSG_BIND: {
                        SomeArgs args = (SomeArgs) msg.obj;
                        try {
                            ImsVTProvider vtp = (ImsVTProvider) args.arg1;
                            int cid = (int) args.arg2;
                            int pid = (int) args.arg3;

                            bindInternal(vtp, cid, pid);
                        } finally {
                            args.recycle();
                        }
                        break;
                    }
                    case MSG_SETUIMODE: {
                        SomeArgs args = (SomeArgs) msg.obj;
                        try {
                            ImsVTProvider vtp = (ImsVTProvider) args.arg1;
                            int mode = (int) args.arg2;

                            setUIModeInternal(vtp, mode);
                        } finally {
                            args.recycle();
                        }
                        break;
                    }
                    case MSG_INIT_SWITCH_FEATURE: {
                        SomeArgs args = (SomeArgs) msg.obj;
                        try {
                            int feature = (int) args.arg1;
                            boolean isOn = (boolean) args.arg2;

                            switchFeatureInternal(feature, isOn);
                        } finally {
                            args.recycle();
                        }
                        break;
                    }
                    default:
                        break;
                }
            }
        };

        //because use in constructor, we need create it here firstly
        mFeatureValueReceiver = new FeatureValueReceiver();
        mFeatureValueReceiver.setOwner(this);

        if (SystemProperties.getInt(VILTE_SUPPORT, 0) > 0) {
            mFeatureValueReceiver.setInitViLTEValue(true);
        } else {
            mFeatureValueReceiver.setInitViLTEValue(false);
        }

        if (SystemProperties.getInt(VIWIFI_SUPPORT, 0) > 0) {
            mFeatureValueReceiver.setInitViWifiValue(true);
        } else {
            mFeatureValueReceiver.setInitViWifiValue(false);
        }
    }

    public static ImsVTProviderUtil getInstance() {
        if (mInstance == null) {
            mInstance = new ImsVTProviderUtil();
        }
        return mInstance;
    }

    public static String packFromVdoProfile(VideoProfile VideoProfile) {
        return mPacker.packFromVdoProfile(VideoProfile);
    }

    public static VideoProfile unPackToVdoProfile(String flattened) {
        return mPacker.unPackToVdoProfile(flattened);
    }

    public static void surfaceSet(int Id, boolean isLocal, boolean isSet) {
        Integer status = (Integer) mSurfaceStatusById.get("" + Id);
        int statusInt;

        Log.d(TAG, "[surfaceSet] isLocal = " + isLocal + ", isSet = " + isSet);

        if (status != null) {
            statusInt = status.intValue();
            Log.d(TAG, "[surfaceSet] state (before) = " + statusInt);
            if (isLocal) {
                if (!isSet) {
                    statusInt &= ~0x1;
                } else {
                    statusInt |= 0x1;
                }
            } else {
                if (!isSet) {
                    statusInt &= ~0x2;
                } else {
                    statusInt |= 0x2;
                }
            }
        } else {
            Log.d(TAG, "[surfaceSet] state (before) = null");
            if (isLocal) {
                if (!isSet) {
                    statusInt = 0x0;
                } else {
                    statusInt = 0x1;
                }
            } else {
                if (!isSet) {
                    statusInt = 0x0;
                } else {
                    statusInt = 0x2;
                }
            }
        }
        Log.d(TAG, "[surfaceSet] state (after) = " + statusInt + ", Id = " + Id);

        mSurfaceStatusById.put("" + Id, new Integer(statusInt));
        return;
    }

    public static int surfaceGet(int Id) {
        Integer status = (Integer) mSurfaceStatusById.get("" + Id);
        if (status != null) {
            Log.d(TAG, "[surfaceGet] state = " + status.intValue() + ", Id = " + Id);
            return status.intValue();
        } else {
            Log.d(TAG, "[surfaceGet] state = 0" + ", Id = " + Id);
            return 0;
        }
    }

    public static void usageSet(int Id, long usage) {
        Log.d(TAG, "[usageSet] id = " + Id + ", usage = " + usage);
        mDataUsageById.put("" + Id, new Long(usage));
        return;
    }

    public static long usageGet(int Id) {
        Long usage = (Long) mDataUsageById.get("" + Id);
        if (usage != null) {
            Log.d(TAG, "[usageGet] usage = " + usage.longValue() + ", Id = " + Id);
            return usage.longValue();
        } else {
            Log.d(TAG, "[usageGet] usage = 0" + ", Id = " + Id);
            return 0;
        }
    }

    public static void recordAdd(int Id, ImsVTProvider p) {
        Log.d(TAG, "recordAdd id = " + Id + ", size = " + recordSize());
        mProviderById.put("" + Id, p);
        return;
    }

    public static void recordRemove(int Id) {
        Log.d(TAG, "recordRemove id = " + Id + ", size = " + recordSize());
        mProviderById.remove("" + Id);
        return;
    }

    public static void recordRemoveAll() {
        Log.d(TAG, "recordRemoveAll, size = " + recordSize());
        mProviderById.clear();
        return;
    }

    public static ImsVTProvider recordGet(int Id) {
        Log.d(TAG, "recordGet id = " + Id + ", size = " + recordSize());
        return (ImsVTProvider) mProviderById.get("" + Id);
    }

    public static int recordPopId() {

        if (mProviderById.size() != 0) {
            for (Object p : mProviderById.values()) {
                return ((ImsVTProvider) p).getId();
            }
        }
        return ImsVTProvider.VT_PROVIDER_INVALIDE_ID;
    }

    public static boolean recordContain(int Id) {
        return mProviderById.containsKey(Id);
    }

    public static int recordSize() {
        return mProviderById.size();
    }

    public static void quitAllThread() {
        if (mProviderById.size() != 0) {
            for (Object p : mProviderById.values()) {
                Log.d(TAG, "quitThread, id = " + ((ImsVTProvider) p).getId());
                ((ImsVTProvider) p).quitThread();
            }
        }
    }

    public static void updateCameraUsage(int Id) {
        Log.d(TAG, "updateCameraUsage");
        if (mProviderById.size() != 0) {
            for (Object p : mProviderById.values()) {
                //Only release other call's camera
                if(((ImsVTProvider) p).getId() != Id) {
                    ((ImsVTProvider) p).onSetCamera(null);
                }
            }
        }
    }

    public static boolean isVideoCallOn() {
        return VILTE_MOBILE_ON || VILTE_WIFI_ON;
    }

    public static void setContextAndInitRefVTP(Context context) {
        mProviderHandler.obtainMessage(MSG_INIT_REFVTP, context).sendToTarget();
    }

    public static void bind(ImsVTProvider p, int CallId, int PhoneId) {
        SomeArgs args = SomeArgs.obtain();
        args.arg1 = p;
        args.arg2 = CallId;
        args.arg3 = PhoneId;
        mProviderHandler.obtainMessage(MSG_BIND, args).sendToTarget();
    }

    public static void setUIMode(ImsVTProvider p, int mode) {
        SomeArgs args = SomeArgs.obtain();
        args.arg1 = p;
        args.arg2 = mode;
        mProviderHandler.obtainMessage(MSG_SETUIMODE, args).sendToTarget();
    }

    public static void switchFeature(int feature, boolean isOn) {
        SomeArgs args = SomeArgs.obtain();
        args.arg1 = feature;
        args.arg2 = isOn;
        mProviderHandler.obtainMessage(MSG_INIT_SWITCH_FEATURE, args).sendToTarget();
    }

    public void setContextAndInitRefVTPInternal(Context context) {
        Log.d(TAG, "setContextAndInitRefVTPInternal(), context =" + context);

        if (SystemProperties.get("ro.mtk_ims_video_call_support", "none").equals("none")) {
            Log.d(TAG, "setContextAndInitRefVTPInternal(), ViLTE off, ignore setContext");
            return;
        }

        mContext = context;
        VTSource.setContext(context);

        if (mFeatureValueReceiver != null) {
            Log.d(TAG, "setContextAndInitRefVTP, register broadcast receiver");
            IntentFilter filter = new IntentFilter();

            // Path: /tb_feature/phoneID/feature/networks
            String ViltePath = "/tb_feature/.*/" +
                    ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE + "/" +
                    TelephonyManager.NETWORK_TYPE_LTE;

            String ViWiFiPath = "/tb_feature/.*/" +
                    ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_WIFI + "/" +
                    TelephonyManager.NETWORK_TYPE_IWLAN;

            try {
                filter.addDataPath(ViltePath, PatternMatcher.PATTERN_SIMPLE_GLOB);
                filter.addDataPath(ViWiFiPath, PatternMatcher.PATTERN_SIMPLE_GLOB);

                filter.addAction(ImsConfig.ACTION_IMS_FEATURE_CHANGED);

                filter.addDataScheme("content");
                filter.addDataAuthority("com.mediatek.ims.config.provider", null);
                filter.addDataType("vnd.android.cursor.item/imsconfig");
            } catch (IntentFilter.MalformedMimeTypeException e) {
                Log.d(TAG, "setContextAndInitRefVTP: exception=" + e);
            }
            mContext.registerReceiver(mFeatureValueReceiver, filter);
        }

        ImsVTProvider.nInitRefVTP();
    }

    public void bindInternal(ImsVTProvider p, int CallId, int PhoneId) {

        Log.d(TAG, "bindInternal(), vtp = " + p + ", id = " + CallId + ", phone id = " + PhoneId);

        // In some case setUIMode destroy will come first, no need to bind and add record
        if (ImsVTProviderUtil.UI_MODE_DESTROY == p.mMode) {
            Log.d(TAG, "Ignore bind ImsVTProvider because UI_MODE_DESTROY");
            return;
        }

        // Note that "PhoneId" means the call from which sim slot (0/1).
        // If not support dual IMS (IMS = 1), we can just assume PhoneId as 0 (same as IMCB simslot ID)
        // For the construct id, the first 2 bytes are phone Id, the last 2 bytes are call id
        int id = CallId;
        if (SystemProperties.getInt(MULTI_IMS_SUPPORT, 1) > 1) {
            id = (PhoneId << 16) | CallId;
        }

        if (p.getId() == ImsVTProvider.VT_PROVIDER_INVALIDE_ID) {

            // Check id if exist in map
            // The same id exist mean the last call with the same id
            // does not disconnected yet at native layer.
            int wait_time = 0;
            Log.d(TAG, "bind ImsVTProvider check if exist the same id");
            while (null != recordGet(id)) {
                Log.d(TAG, "bind ImsVTProvider the same id exist, wait ...");

                try {
                    Thread.sleep(400);
                } catch(InterruptedException ex) {
                }

                wait_time += 1;
                if (wait_time > 10) {
                    Log.d(TAG, "bind ImsVTProvider the same id exist, break!");
                    break;
                }
            }

            p.setId(id);
            p.mInitialUsage = usageGet(id);
            recordAdd(id, p);
            p.mSource = new VTSource(VTSource.VT_SRV_CALL_4G, p.getId(), p);
            p.mSource.setReplacePicture(ImsVTProvider.REPLACE_PICTURE_PATH);
            p.nInitialization(id);
            p.mInitComplete = true;
        }
    }

    public static void setUIModeInternal(ImsVTProvider p, int mode) {
        p.onSetUIMode(mode);
    }

    public static void switchFeatureInternal(int feature, boolean isOn) {
        if (false == isOn) {
            if (mProviderById.size() != 0) {
                for (Object p : mProviderById.values()) {
                    Log.d(TAG, "switchFeature, id = " + ((ImsVTProvider) p).getId());
                    ((ImsVTProvider) p).onSwitchFeature(feature, isOn);
                }
            }
        }

        if (TAG_VILTE_MOBILE == feature) {
            VILTE_MOBILE_ON = isOn;
        } else if (TAG_VILTE_WIFI == feature) {
            VILTE_WIFI_ON = isOn;
        }
    }
}
