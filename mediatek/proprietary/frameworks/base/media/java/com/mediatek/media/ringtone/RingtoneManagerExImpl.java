package com.mediatek.media.ringtone;

import android.annotation.NonNull;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.mediatek.media.MtkMediaStore;

import libcore.io.IoUtils;

/**
 * The class implement for RingtoneManagerEx.
 */
public class RingtoneManagerExImpl extends RingtoneManagerEx {
    private static final String TAG = "RingtoneManagerExImpl";

    private static final String[] DRM_PROJECTION = new String[] {
            MediaStore.MediaColumns._ID, // 0
            MediaStore.MediaColumns.IS_DRM, // 1
            MtkMediaStore.MediaColumns.DRM_METHOD //2
    };

    /// M: for OMA DRM v1 implementation:
    /// currently we only allow those "Forward Lock" content to be set as ringtone.@{
    private static final String EXTRA_DRM_LEVEL = "android.intent.extra.drm_level";
    private static final int DRM_LEVEL_FL = 1;
    private static final int DRM_LEVEL_SD = 2;
    private static final int DRM_LEVEL_ALL = 4;
    /// @}

    /**
     * Pre-filter drm files for ringtone add.
     * All drm files cannot be added into ringtone.
     * @param context the context of ringtoneManager.
     * @param fileUri the added file's uri.
     */
    public void preFilterDrmFilesForFlType(final Context context,
            @NonNull final Uri fileUri) {
        Cursor c = null;
        String is_drm = null;
        String drm_method = null;
        try {
            c = context.getContentResolver().query(fileUri, DRM_PROJECTION,
                    null, null, null);
            if (c != null && c.getCount() == 1) {
                c.moveToFirst();
                is_drm = c.getString(1);
                drm_method = c.getString(2);
                c.deactivate();
            }
        } finally {
            IoUtils.closeQuietly(c);
        }
        Log.d(TAG, "is_drm = " + is_drm);
        if ("1".equals(is_drm)) {
            throw new IllegalArgumentException("Ringtone DRM file must have is_drm = 1 &"
                    + " drm_method = 1 , But given file has is_drm = \"" + is_drm);
        }
    }

    /**
     *  For OMA DRM v1 implementation.
     *  use this to append extra drm conditions will be something like
     *  "and (is_drm!=1 or drm_method=1)" or something like "and (is_drm!=1)"
     *  @param activity The activity init RingtoneManager.
     *  @return the string for query database.
     */
    public String appendDrmToWhereClause(final Activity activity) {
        Log.d(TAG, "[appendDrmToWhereClause] activity = " + activity);
        if (activity == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(" and ");
        sb.append("(");

        // check the extra information to judge that if the FL ringstones can be shown in the list
        sb.append(MediaStore.Audio.Media.IS_DRM).append("!=1");
        Intent it = activity.getIntent();
        if (it != null) {
            // by default the extra value is METHOD_FL (1)
            int extraValue = it.getIntExtra(EXTRA_DRM_LEVEL, DRM_LEVEL_FL);
            if (extraValue == DRM_LEVEL_FL) {
                sb.append(" or ")
                  .append(MtkMediaStore.MediaColumns.DRM_METHOD)
                  .append("=" + DRM_LEVEL_FL);
            }
        }

        sb.append(")");
        Log.d(TAG, "[appendDrmToWhereClause] return:" + sb.toString());
        return sb.toString();
    }

    public String[] getMtkMediaColumns() {
        return new String[] {
                MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE,
                "\"" + MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "\"",
                MediaStore.Audio.Media.TITLE_KEY,
                MediaStore.Audio.Media.IS_DRM,    /// M: add for handling OMA DRM v1 content
                MtkMediaStore.MediaColumns.DRM_METHOD /// M: add for handling OMA DRM v1 content
            };
    }
}

