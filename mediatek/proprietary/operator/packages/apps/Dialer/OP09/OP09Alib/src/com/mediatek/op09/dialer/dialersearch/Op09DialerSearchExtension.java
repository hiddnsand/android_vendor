package com.mediatek.op09.dialer.dialersearch;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mediatek.dialer.ext.DefaultDialerSearchExtension;
import com.mediatek.op09.dialer.Op09DialerPluginUtil;
import com.mediatek.op09.dialer.R;

public class Op09DialerSearchExtension extends DefaultDialerSearchExtension {
    private static final String TAG = "Op09DialerSearchExtension";
    private static final String ACCOUNT_LABEL = "call_account_label";
    private static final String CALL_INFO = "call_info";
    private static final String ID = "id";
    private static final String SIM_INDICATOR = "simIndicator";
    public static final int ICON_ID = 100;

    /**
     * remove call account info if it exists in contact item for OP09
     * @param Context context
     * @param View view
     */
    public void removeCallAccountForDialerSearch(Context context, View view) {
        Op09DialerPluginUtil dialerPlugin = new Op09DialerPluginUtil(context);
        Context pluginContext = dialerPlugin.getPluginContext();
        Resources resource = pluginContext.getResources();
        String packageName = pluginContext.getPackageName();

        View simIndicator =
                view.findViewById(
                        resource.getIdentifier(SIM_INDICATOR, ID, packageName));

        if (simIndicator != null) {
            log("Got simIndicator in layout");
            ViewGroup indicatorParent = (ViewGroup) simIndicator.getParent();
            if (indicatorParent == null) {
                log("indicatorParent is null");
                return;
            }

            indicatorParent.setVisibility(View.GONE);
        }
    }



    /**
     * for OP09
     * @param Context context
     * @param View view
     * @param PhoneAccountHandle phoneAccountHandle
     */
    public void setCallAccountForDialerSearch(Context context, View view,
                                              PhoneAccountHandle phoneAccountHandle) {
        TelephonyManager telephonyManager = (TelephonyManager) context.
                getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.getDefault().getPhoneCount() < 2) {
            return;
        }
        log("setCallAccountForDialerSearch view = " + view +
            " phoneAccountHandle = " + phoneAccountHandle);
        Op09DialerPluginUtil dialerPlugin = new Op09DialerPluginUtil(context);
        Context pluginContext = dialerPlugin.getPluginContext();
        Resources resource = context.getResources();
        String packageName = context.getPackageName();
        TelecomManager telecomManager = (TelecomManager) context.getSystemService(
                Context.TELECOM_SERVICE);
        PhoneAccount account = telecomManager.getPhoneAccount(phoneAccountHandle);
        if (null == view || null == context) {
            log("view or context is null.");
            return;
        }

        View callinfo =
                view.findViewById(
                        resource.getIdentifier(CALL_INFO, ID, packageName));

        View accountLable =
                view.findViewById(
                        resource.getIdentifier(ACCOUNT_LABEL, ID, packageName));
        ViewGroup callLogItem = null;
        if (callinfo != null) {
            callLogItem = (ViewGroup) callinfo.getParent();
        }

        ViewGroup viewChild = (ViewGroup) callLogItem.findViewById(ICON_ID);

        /// This is a contact item or no account info.
        if (phoneAccountHandle == null || account == null) {
            if (viewChild != null) {
                viewChild.setVisibility(View.GONE);
            }
            if (accountLable != null) {
                accountLable.setVisibility(View.GONE);
            }
            return;
        }

        if (accountLable != null) {
            callLogItem.removeView(accountLable);
            accountLable.setVisibility(View.VISIBLE);
        } else {
            log("Call log items, but no account lable.");
            return;
        }

        Bitmap iconBitmap = account.getIcon().getBitmap();
        if (null == viewChild && null != iconBitmap && null != callLogItem) {
            LayoutInflater mInflater;
            mInflater = LayoutInflater.from(pluginContext);
            ViewGroup iconLayout =
                    (ViewGroup) mInflater.inflate(R.layout.ct_sim_indicator, callLogItem, false);
            iconLayout.setId(ICON_ID);
            ImageView simIndicator = (ImageView)iconLayout.findViewById(R.id.simIndicator);
            simIndicator.setVisibility(View.VISIBLE);
            simIndicator.setImageDrawable(new BitmapDrawable(iconBitmap));
            iconLayout.addView(accountLable, 1);
            callLogItem.addView(iconLayout, 3);
        } else if (null != iconBitmap && null != callLogItem) {
            viewChild.setVisibility(View.VISIBLE);
            View simIndicator = viewChild.getChildAt(0);
            if (null != simIndicator && (simIndicator instanceof ImageView)) {
                simIndicator.setVisibility(View.VISIBLE);
                ((ImageView)simIndicator).setImageDrawable(new BitmapDrawable(iconBitmap));
            }
        }
    }

    private void log(String msg) {
        Log.d(TAG, msg);
    }
}
