
package com.mediatek.ims.internal;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.telephony.Rlog;

import com.android.ims.ImsExternalCallState;
import com.android.ims.internal.IImsExternalCallStateListener;
import com.android.ims.internal.IImsMultiEndpoint;
import com.mediatek.ims.internal.DialogEventPackageReceiver.Listener;
import com.mediatek.ims.internal.DialogEventPackageReceiver.ListenerBase;

import java.util.List;

/**
 * OEM implementation of IImsMultiEndpoint aidl.
 */
public class ImsMultiEndpointImpl extends IImsMultiEndpoint.Stub {
    private static final String TAG = "ImsMultiEndpointImpl";

    private DialogEventPackageReceiver mReceiver;
    private IImsExternalCallStateListener mListener = null;
    private Handler mHandler;
    private List<ImsExternalCallState> mExternalCallStateList;

    private static final int MSG_UPDATE = 1;

    /**
     * Creates the instance of ImsMultiEndpointImpl.
     * @param context application context
     */
    public ImsMultiEndpointImpl(Context context) {
        mReceiver = new DialogEventPackageReceiver(context, mDepListener);
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MSG_UPDATE:
                        update(mExternalCallStateList);
                        break;
                    default:
                        break;
                }
            }
        };
        Rlog.d(TAG, "ImsMultiEndpointImpl");
    }

    private final Listener mDepListener = new ListenerBase() {
        @Override
        public void onStateChanged(List<ImsExternalCallState> list) {
            update(list);
        }
    };

    public void setListener(IImsExternalCallStateListener listener) {
        mListener = listener;
    }

    /**
     * Query api to get the latest Dialog Event Package information.
     * Should be invoked only after setListener is done.
     */
    public void requestImsExternalCallStateInfo() {
        mHandler.sendEmptyMessage(MSG_UPDATE);
    }

    private void update(List<ImsExternalCallState> externalCallDialogs) {
        mExternalCallStateList = externalCallDialogs;
        if (mListener == null) {
            return;
        }
        try {
            mListener.onImsExternalCallStateUpdate(externalCallDialogs);
        } catch (RemoteException e) {
            Rlog.e(TAG, "RemoteException when update: " + e);
        }
    }
}
