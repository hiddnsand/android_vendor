/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "JNI_PQ"

#define MTK_LOG_ENABLE 1
#include <jni_pq.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <errno.h>
#include <utils/Log.h>
#include <utils/threads.h>
#include <cutils/log.h>
#include <cutils/properties.h>

#include "cust_color.h"
#include "cust_tdshp.h"
#include "cust_gamma.h"
#include <PQServiceCommon.h>
#ifdef MTK_PQ_SERVICE
#include <vendor/mediatek/hardware/pq/2.0/IPictureQuality.h>

using vendor::mediatek::hardware::pq::V2_0::IPictureQuality;
using vendor::mediatek::hardware::pq::V2_0::PQFeatureID;
using vendor::mediatek::hardware::pq::V2_0::Result;
#endif

#ifdef MTK_AAL_SUPPORT
#include "AALClient.h"
#endif

#ifdef MTK_MIRAVISION_IMAGE_DC_SUPPORT
#include <PQDCHistogram.h>
#endif

using namespace android;

#define MIN_COLOR_WIN_SIZE (0x0)
#define MAX_COLOR_WIN_SIZE (0xFFFF)

#define UNUSED(expr) do { (void)(expr); } while (0)
#define JNI_PQ_CLASS_NAME "com/mediatek/pq/PictureQuality"

Mutex mLock;

static int CAPABILITY_MASK_COLOR       = 0x00000001;
static int CAPABILITY_MASK_SHARPNESS   = 0x00000002;
static int CAPABILITY_MASK_GAMMA       = 0x00000004;
static int CAPABILITY_MASK_DC          = 0x00000008;
static int CAPABILITY_MASK_OD          = 0x00000010;
/////////////////////////////////////////////////////////////////////////////////
static jint getCapability(JNIEnv *env, jobject thiz)
{
    Mutex::Autolock autoLock(mLock);

    UNUSED(env);
    UNUSED(thiz);

    int cap = (CAPABILITY_MASK_COLOR |
               CAPABILITY_MASK_SHARPNESS |
               CAPABILITY_MASK_GAMMA |
               CAPABILITY_MASK_DC);

#ifdef MTK_OD_SUPPORT
    cap |= CAPABILITY_MASK_OD;
#endif

    return cap;
}

static void setCameraPreviewMode(JNIEnv* env, jobject thiz, jint step)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return;
    }

    service->setDISPScenario(SCENARIO_ISP_PREVIEW, step);
#else
    ALOGE("[JNI_PQ] setCameraPreviewMode(), not supported!");

    UNUSED(step);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return;
}

static void setGalleryNormalMode(JNIEnv* env, jobject thiz, jint step)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return;
    }

    service->setDISPScenario(SCENARIO_PICTURE, step);
#else
    ALOGE("[JNI_PQ] setGalleryNormalMode(), not supported!");

    UNUSED(step);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return;
}

static void setVideoPlaybackMode(JNIEnv* env, jobject thiz, jint step)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return;
    }

    service->setDISPScenario(SCENARIO_VIDEO, step);
#else
    ALOGE("[JNI_PQ] setVideoPlaybackMode(), not supported!");

    UNUSED(step);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return;
}

#ifdef MTK_MIRAVISION_IMAGE_DC_SUPPORT
static void Hist_set(JNIEnv* env, jobject obj, jint index, jint value)
{
    jclass clazz = env->FindClass(JNI_PQ_CLASS_NAME "$Hist");
    jmethodID setMethod = env->GetMethodID(clazz, "set", "(II)V");
    env->CallVoidMethod(obj, setMethod, index, value);

    env->DeleteLocalRef(clazz);
}
#endif

static void getDynamicContrastHistogram(JNIEnv* env, jclass clz, jbyteArray srcBuffer, jint srcWidth, jint srcHeight, jobject hist)
{
#ifdef MTK_MIRAVISION_IMAGE_DC_SUPPORT
    Mutex::Autolock autoLock(mLock);
    CPQDCHistogram *pDCHist = new CPQDCHistogram;
    DynCInput   input;
    DynCOutput  output;
    int i;

    input.pSrcFB = (unsigned char*)env->GetByteArrayElements(srcBuffer, 0);
    input.iWidth = srcWidth;
    input.iHeight = srcHeight;

    pDCHist->Main(input, &output);
    for (i = 0; i < DCHIST_INFO_NUM; i++)
    {
        Hist_set(env, hist, i, output.Info[i]);
    }

    env->ReleaseByteArrayElements(srcBuffer, (jbyte*)input.pSrcFB, 0);
    delete pDCHist;
#else
    ALOGE("[JNI_PQ] getDynamicContrastHistogram(), not supported!");

    UNUSED(env);
    UNUSED(srcBuffer);
    UNUSED(srcWidth);
    UNUSED(srcHeight);
    UNUSED(hist);
#endif
    UNUSED(clz);
}

static void Range_set(JNIEnv* env, jobject obj, jint min, jint max, jint defaultValue)
{
    jclass clazz = env->FindClass(JNI_PQ_CLASS_NAME "$Range");
    jmethodID setMethod = env->GetMethodID(clazz, "set", "(III)V");
    env->CallVoidMethod(obj, setMethod, min, max, defaultValue);
}

static jboolean enableColor(JNIEnv *env, jobject thiz, int isEnable);
static jboolean enableSharpness(JNIEnv *env, jobject thiz, int isEnable);
static jboolean enableDynamicContrast(JNIEnv *env, jobject thiz, int isEnable);
static jboolean enableGamma(JNIEnv *env, jobject thiz, int isEnable);
static jboolean enableOD(JNIEnv *env, jobject thiz, int isEnable);

/////////////////////////////////////////////////////////////////////////////////
static jboolean enablePQ(JNIEnv *env, jobject thiz, int isEnable)
{
    if (isEnable)
    {
        enableColor(env, thiz, 1);
        enableSharpness(env, thiz, 1);
        enableDynamicContrast(env, thiz, 1);
        enableOD(env, thiz, 1);
    }
    else
    {
        enableColor(env, thiz, 0);
        enableSharpness(env, thiz, 0);
        enableDynamicContrast(env, thiz, 0);
        enableOD(env, thiz, 0);
    }

    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
static jboolean enableColor(JNIEnv *env, jobject thiz, int isEnable)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setFeatureSwitch(PQFeatureID::DISPLAY_COLOR, isEnable);
#else
    ALOGE("[JNI_PQ] enableColor(), not supported!");

    UNUSED(isEnable);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
static jboolean enableSharpness(JNIEnv *env, jobject thiz, int isEnable)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setFeatureSwitch(PQFeatureID::SHARPNESS, isEnable);
#else
    ALOGE("[JNI_PQ] enableSharpness(), not supported!");

    UNUSED(isEnable);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
static jboolean enableDynamicContrast(JNIEnv *env, jobject thiz, int isEnable)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setFeatureSwitch(PQFeatureID::DYNAMIC_CONTRAST, isEnable);
#else
    ALOGE("[JNI_PQ] enableDynamicContrast(), not supported!");

    UNUSED(isEnable);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

static jboolean enableColorEffect(JNIEnv *env, jobject thiz, int isEnable)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setFeatureSwitch(PQFeatureID::CONTENT_COLOR_VIDEO, isEnable);
#else
    ALOGE("[JNI_PQ] enableColorEffect(), not supported!");

    UNUSED(isEnable);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

static jboolean enableDynamicSharpness(JNIEnv *env, jobject thiz, int isEnable)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setFeatureSwitch(PQFeatureID::DYNAMIC_SHARPNESS, isEnable);
#else
    ALOGE("[JNI_PQ] enableDynamicSharpness(), not supported!");

    UNUSED(isEnable);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

static jboolean enableUltraResolution(JNIEnv *env, jobject thiz, int isEnable)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setFeatureSwitch(PQFeatureID::ULTRA_RESOLUTION, isEnable);
#else
    ALOGE("[JNI_PQ] enableUltraResolution(), not supported!");

    UNUSED(isEnable);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

static jboolean enableISOAdaptiveSharpness(JNIEnv *env, jobject thiz, int isEnable)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setFeatureSwitch(PQFeatureID::ISO_ADAPTIVE_SHARPNESS, isEnable);
#else
    ALOGE("[JNI_PQ] enableISOAdaptiveSharpness(), not supported!");

    UNUSED(isEnable);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

static jboolean enableContentColor(JNIEnv *env, jobject thiz, int isEnable)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setFeatureSwitch(PQFeatureID::CONTENT_COLOR, isEnable);
#else
    ALOGE("[JNI_PQ] enableContentColor(), not supported!");

    UNUSED(isEnable);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
static jboolean enableGamma(JNIEnv *env, jobject thiz, int isEnable)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setFeatureSwitch(PQFeatureID::DISPLAY_GAMMA, isEnable);
#else
    ALOGE("[JNI_PQ] enableGamma(), not supported!");

    UNUSED(isEnable);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
static jint getPictureMode(JNIEnv *env, jobject thiz)
{
    Mutex::Autolock autoLock(mLock);
    char value[PROPERTY_VALUE_MAX];
    int mode = -1;

    property_get(PQ_PIC_MODE_PROPERTY_STR, value, PQ_PIC_MODE_DEFAULT);
    mode = atoi(value);
    ALOGD("[JNI_PQ] getPictureMode(), property get [%d]", mode);

    UNUSED(env);
    UNUSED(thiz);

    return mode;
}

/////////////////////////////////////////////////////////////////////////////////

static jboolean setPictureMode(JNIEnv *env, jobject thiz, int mode, int step)
{
#ifdef MTK_PQ_SERVICE
    Mutex::Autolock autoLock(mLock);
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setPQMode(mode, step);
#else
    ALOGE("[JNI_PQ] setPictureMode(), not supported!");

    UNUSED(mode);
    UNUSED(step);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

/////////////////////////////////////////////////////////////////////////////////

static jboolean setColorRegion(JNIEnv *env, jobject thiz, int isEnable, int startX, int startY, int endX, int endY)
{
#ifdef MTK_PQ_SERVICE
    DISP_PQ_WIN_PARAM win_param;
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    ALOGD("[JNI_PQ] setColorRegion(), en[%d], sX[%d], sY[%d], eX[%d], eY[%d]", isEnable, startX, startY, endX, endY);

    if (isEnable)
    {
        win_param.split_en = 1;
        win_param.start_x = startX;
        win_param.start_y = startY;
        win_param.end_x = endX;
        win_param.end_y = endY;
    }
    else
    {
        win_param.split_en = 0;
        win_param.start_x = MIN_COLOR_WIN_SIZE;
        win_param.start_y = MIN_COLOR_WIN_SIZE;
        win_param.end_x = MAX_COLOR_WIN_SIZE;
        win_param.end_y = MAX_COLOR_WIN_SIZE;
    }

    if (service->setColorRegion(win_param.split_en,win_param.start_x,win_param.start_y,win_param.end_x,win_param.end_y) != Result::OK) {
        ALOGE("[JNI_PQ] setColorRegion() fail!!!");
    }

#else
    ALOGE("[JNI_PQ] setColorRegion(), not supported!");

    UNUSED(isEnable);
    UNUSED(startX);
    UNUSED(startY);
    UNUSED(endX);
    UNUSED(endY);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

/////////////////////////////////////////////////////////////////////////////////
static void getContrastIndexRange(JNIEnv* env, jclass clz, jobject range)
{
    Mutex::Autolock autoLock(mLock);

    Range_set(env, range, 0, PQ_CONTRAST_INDEX_RANGE_NUM - 1, atoi(PQ_CONTRAST_INDEX_DEFAULT));

    UNUSED(clz);
}

static jint getContrastIndex(JNIEnv *env, jobject thiz)
{
    Mutex::Autolock autoLock(mLock);
    char value[PROPERTY_VALUE_MAX];
    int index = -1;

    property_get(PQ_CONTRAST_PROPERTY_STR, value, PQ_CONTRAST_INDEX_DEFAULT);
    index = atoi(value);
    ALOGD("[JNI_PQ] getContrastIndex(), property get [%d]", index);

    UNUSED(env);
    UNUSED(thiz);

    return index;
}

static void setContrastIndex(JNIEnv *env, jobject thiz, int index, int step)
{
#ifdef MTK_PQ_SERVICE
    Mutex::Autolock autoLock(mLock);
    char value[PROPERTY_VALUE_MAX];
    int ret;
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return;
    }

    service->setPQIndex(index, SCENARIO_PICTURE, TDSHP_FLAG_NORMAL, SET_PQ_CONTRAST, step);

    snprintf(value, PROPERTY_VALUE_MAX, "%d\n", index);
    ret = property_set(PQ_CONTRAST_PROPERTY_STR, value);
#else
    ALOGE("[JNI_PQ] setContrastIndex(), not supported!");

    UNUSED(index);
    UNUSED(step);
#endif
    UNUSED(env);
    UNUSED(thiz);

}

/////////////////////////////////////////////////////////////////////////////////
static void getSaturationIndexRange(JNIEnv* env, jclass clz, jobject range)
{
    Mutex::Autolock autoLock(mLock);

    Range_set(env, range, 0, PQ_GSAT_INDEX_RANGE_NUM - 1, atoi(PQ_GSAT_INDEX_DEFAULT));

    UNUSED(clz);
}

static jint getSaturationIndex(JNIEnv *env, jobject thiz)
{
    Mutex::Autolock autoLock(mLock);
    char value[PROPERTY_VALUE_MAX];
    int index = -1;

    property_get(PQ_GSAT_PROPERTY_STR, value, PQ_GSAT_INDEX_DEFAULT);
    index = atoi(value);
    ALOGD("[JNI_PQ] getSaturationIndex(), property get [%d]", index);

    UNUSED(env);
    UNUSED(thiz);

    return index;
}

static void setSaturationIndex(JNIEnv *env, jobject thiz, int index, int step)
{
#ifdef MTK_PQ_SERVICE
    Mutex::Autolock autoLock(mLock);
    char value[PROPERTY_VALUE_MAX];
    int ret;
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return;
    }

    ALOGD("[JNI_PQ] setSaturationIndex...index[%d]", index);

    service->setPQIndex(index, SCENARIO_PICTURE, TDSHP_FLAG_NORMAL, SET_PQ_SAT_GAIN, step);

    snprintf(value, PROPERTY_VALUE_MAX, "%d\n", index);
    ret = property_set(PQ_GSAT_PROPERTY_STR, value);
#else
    ALOGE("[JNI_PQ] setSaturationIndex(), not supported!");

    UNUSED(index);
    UNUSED(step);
#endif
    UNUSED(env);
    UNUSED(thiz);
}

/////////////////////////////////////////////////////////////////////////////////
static void getPicBrightnessIndexRange(JNIEnv* env, jclass clz, jobject range)
{
    Mutex::Autolock autoLock(mLock);

    Range_set(env, range, 0, PQ_PIC_BRIGHT_INDEX_RANGE_NUM - 1, atoi(PQ_PIC_BRIGHT_INDEX_DEFAULT));

    UNUSED(clz);
}

static jint getPicBrightnessIndex(JNIEnv *env, jobject thiz)
{
    Mutex::Autolock autoLock(mLock);
    char value[PROPERTY_VALUE_MAX];
    int index = -1;

    property_get(PQ_PIC_BRIGHT_PROPERTY_STR, value, PQ_PIC_BRIGHT_INDEX_DEFAULT);
    index = atoi(value);
    ALOGD("[JNI_PQ] getPicBrightnessIndex(), property get [%d]", index);

    UNUSED(env);
    UNUSED(thiz);

    return index;
}

static void setPicBrightnessIndex(JNIEnv *env, jobject thiz, int index, int step)
{
#ifdef MTK_PQ_SERVICE
    Mutex::Autolock autoLock(mLock);
    char value[PROPERTY_VALUE_MAX];
    int ret;
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return;
    }

    ALOGD("[JNI_PQ] setPicBrightnessIndex...index[%d]", index);

    service->setPQIndex(index, SCENARIO_PICTURE, TDSHP_FLAG_NORMAL, SET_PQ_BRIGHTNESS, step);

    snprintf(value, PROPERTY_VALUE_MAX, "%d\n", index);
    ret = property_set(PQ_PIC_BRIGHT_PROPERTY_STR, value);
#else
    ALOGE("[JNI_PQ] setPicBrightnessIndex(), not supported!");

    UNUSED(index);
    UNUSED(step);
#endif
    UNUSED(env);
    UNUSED(thiz);

}

/////////////////////////////////////////////////////////////////////////////////
static void getSharpnessIndexRange(JNIEnv* env, jclass clz, jobject range)
{
    Mutex::Autolock autoLock(mLock);

    Range_set(env, range, 0, PQ_TDSHP_INDEX_RANGE_NUM - 1, atoi(PQ_TDSHP_INDEX_DEFAULT));

    UNUSED(clz);
}

static jint getSharpnessIndex(JNIEnv *env, jobject thiz)
{
    Mutex::Autolock autoLock(mLock);
    char value[PROPERTY_VALUE_MAX];
    int index = -1;

    property_get(PQ_TDSHP_PROPERTY_STR, value, PQ_TDSHP_STANDARD_DEFAULT);
    index = atoi(value);
    ALOGD("[JNI_PQ] getSharpnessIndex(), property get [%d]", index);

    UNUSED(env);
    UNUSED(thiz);

    return index;
}

static void setSharpnessIndex(JNIEnv *env, jobject thiz , int index)
{
#ifdef MTK_PQ_SERVICE
    Mutex::Autolock autoLock(mLock);
    char value[PROPERTY_VALUE_MAX];
    int ret;
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return;
    }

    ALOGD("[JNI_PQ] setSharpnessIndex...index[%d]", index);

    service->setPQIndex(index, SCENARIO_PICTURE, TDSHP_FLAG_NORMAL, SET_PQ_SHP_GAIN, PQ_DEFAULT_TRANSITION_OFF_STEP);

    snprintf(value, PROPERTY_VALUE_MAX, "%d\n", index);
    ret = property_set(PQ_TDSHP_PROPERTY_STR, value);
#else
    ALOGE("[JNI_PQ] setSharpnessIndex(), not supported!");

    UNUSED(index);
#endif
    UNUSED(env);
    UNUSED(thiz);
}

/////////////////////////////////////////////////////////////////////////////////
static void getDynamicContrastIndexRange(JNIEnv* env, jclass clz, jobject range)
{
    Mutex::Autolock autoLock(mLock);

    Range_set(env, range, 0, PQ_ADL_INDEX_RANGE_NUM, atoi(PQ_ADL_INDEX_DEFAULT));

    UNUSED(clz);
}

static jint getDynamicContrastIndex(JNIEnv *env, jobject thiz)
{
    Mutex::Autolock autoLock(mLock);
    char value[PROPERTY_VALUE_MAX];
    int index = -1;

    property_get(PQ_ADL_PROPERTY_STR, value, PQ_ADL_INDEX_DEFAULT);
    index = atoi(value);
    ALOGD("[JNI_PQ] getDynamicContrastIndex(), property get [%d]", index);

    UNUSED(env);
    UNUSED(thiz);

    return index;
}

static void setDynamicContrastIndex(JNIEnv *env, jobject thiz, int index)
{
#ifdef MTK_PQ_SERVICE
    Mutex::Autolock autoLock(mLock);
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return;
    }

    service->setFeatureSwitch(PQFeatureID::DYNAMIC_CONTRAST, index);
#else
    ALOGE("[JNI_PQ] setDynamicContrastIndex(), not supported!");

    UNUSED(index);
#endif
    UNUSED(env);
    UNUSED(thiz);
}

static void getColorEffectIndexRange(JNIEnv* env, jclass clz, jobject range)
{
    Mutex::Autolock autoLock(mLock);

    Range_set(env, range, 0, PQ_MDP_COLOR_EN_INDEX_RANGE_NUM, atoi(PQ_MDP_COLOR_EN_DEFAULT));

    UNUSED(clz);
}

static jint getColorEffectIndex(JNIEnv *env, jobject thiz)
{
    Mutex::Autolock autoLock(mLock);
    char value[PROPERTY_VALUE_MAX];
    int index = -1;

    property_get(PQ_MDP_COLOR_EN_STR, value, PQ_MDP_COLOR_EN_DEFAULT);
    index = atoi(value);
    ALOGD("[JNI_PQ] getColorEffectIndex(), property get [%d]", index);

    UNUSED(env);
    UNUSED(thiz);

    return index;
}

static void setColorEffectIndex(JNIEnv *env, jobject thiz, int index)
{
#ifdef MTK_PQ_SERVICE
    Mutex::Autolock autoLock(mLock);
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return;
    }

    service->setFeatureSwitch(PQFeatureID::CONTENT_COLOR_VIDEO, index);
#else
    ALOGE("[JNI_PQ] setColorEffectIndex(), not supported!");

    UNUSED(index);
#endif
    UNUSED(env);
    UNUSED(thiz);
}
static void getGammaIndexRange(JNIEnv* env, jclass clz, jobject range)
{
    Mutex::Autolock autoLock(mLock);

    Range_set(env, range, 0, GAMMA_INDEX_MAX - 1, GAMMA_INDEX_DEFAULT);

    UNUSED(clz);
}


static void setGammaIndex(JNIEnv* env, jclass clz, jint index, jint step)
{
#ifdef MTK_PQ_SERVICE
    Mutex::Autolock autoLock(mLock);
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return;
    }

    service->setGammaIndex(index, step);
#else
    ALOGE("[JNI_PQ] setGammaIndex(), not supported!");

    UNUSED(index);
    UNUSED(step);
#endif
    UNUSED(env);
    UNUSED(clz);
}

// OD
static jboolean enableOD(JNIEnv *env, jobject thiz, int isEnable)
{
#if defined(MTK_PQ_SERVICE) && defined(MTK_OD_SUPPORT)
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setFeatureSwitch(PQFeatureID::DISPLAY_OVER_DRIVE, isEnable);
#else
    ALOGE("[JNI_PQ] enableOD(), not supported!");

    UNUSED(isEnable);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

static jboolean nativeEnableVideoHDR(JNIEnv* env, jclass clz, jboolean isEnable)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setFeatureSwitch(PQFeatureID::VIDEO_HDR, isEnable);
#else
    ALOGE("[JNI_PQ] nativeEnableVideoHDR(), not supported!");

    UNUSED(isEnable);
#endif
    UNUSED(env);
    UNUSED(clz);

    return JNI_TRUE;
}


static jboolean nativeIsVideoHDREnabled(JNIEnv* env, jclass clz)
{
    bool isEnabled = false;
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->getFeatureSwitch(PQFeatureID::VIDEO_HDR,
        [&] (Result retval, uint32_t value) {
        if (retval == Result::OK) {
            isEnabled = value;
        } else {
            ALOGE("[JNI_PQ] nativeIsVideoHDREnabled() failed!");
        }
    });
#else
    ALOGE("[JNI_PQ] nativeIsVideoHDREnabled(), not supported!");
#endif
    UNUSED(env);
    UNUSED(clz);

    return (isEnabled ? JNI_TRUE : JNI_FALSE);
}

static jboolean nativeSetBlueLightStrength(JNIEnv* env, jclass clz, jint strength, jint step)
{
#if defined(MTK_PQ_SERVICE) && defined(MTK_BLULIGHT_DEFENDER_SUPPORT)
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    if (service->setBlueLightStrength(strength, step) != Result::OK) {
        ALOGE("[JNI_PQ] nativeSetBlueLightStrength() failed!");
        return JNI_FALSE;
    }
#else
    ALOGE("[JNI_PQ] nativeSetBlueLightStrength(), not supported!");

    UNUSED(strength);
    UNUSED(step);
#endif
    UNUSED(env);
    UNUSED(clz);

    return JNI_TRUE;
}


static jint nativeGetBlueLightStrength(JNIEnv* env, jclass clz)
{
    int32_t getStrength = 0;
#if defined(MTK_PQ_SERVICE) && defined(MTK_BLULIGHT_DEFENDER_SUPPORT)
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return 0;
    }

    service->getBlueLightStrength(
        [&] (Result retval, int32_t strength) {
        if (retval == Result::OK) {
            getStrength = strength;
        } else {
            ALOGE("[JNI_PQ] nativeGetBlueLightStrength() failed!");
        }
    });
#else
    ALOGE("[JNI_PQ] nativeGetBlueLightStrength(), not supported!");
#endif
    UNUSED(env);
    UNUSED(clz);

    return getStrength;
}


static jboolean nativeEnableBlueLight(JNIEnv* env, jclass clz, jboolean isEnable, jint step)
{
#if defined(MTK_PQ_SERVICE) && defined(MTK_BLULIGHT_DEFENDER_SUPPORT)
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    if (service->enableBlueLight(isEnable ? true : false, step) != Result::OK) {
        ALOGE("[JNI_PQ] enableBlueLight() failed!");
        return JNI_FALSE;
    }
#else
    ALOGE("[JNI_PQ] nativeSetBlueLightStrength(), not supported!");

    UNUSED(isEnable);
    UNUSED(step);
#endif

    UNUSED(env);
    UNUSED(clz);

    return JNI_TRUE;
}


static jboolean nativeIsBlueLightEnabled(JNIEnv* env, jclass clz)
{
    bool getEnabled = false;
#if defined(MTK_PQ_SERVICE) && defined(MTK_BLULIGHT_DEFENDER_SUPPORT)
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->getBlueLightEnabled(
        [&] (Result retval, bool isEnabled) {
        if (retval == Result::OK) {
            getEnabled = isEnabled;
        } else {
            ALOGE("[JNI_PQ] enableBlueLight() failed!");
        }
    });
#else
    ALOGE("[JNI_PQ] nativeIsBlueLightEnabled(), not supported!");
#endif

    UNUSED(env);
    UNUSED(clz);

    return (getEnabled ? JNI_TRUE : JNI_FALSE);
}

static jboolean nativeSetChameleonStrength(JNIEnv* env, jclass clz, jint strength, jint step)
{
    UNUSED(env);
    UNUSED(clz);

#if defined(MTK_PQ_SERVICE) && defined(MTK_CHAMELEON_DISPLAY_SUPPORT)
    sp<IPictureQuality> service = IPictureQuality::tryGetService();
    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    if (service->setChameleonStrength(strength, step) != Result::OK) {
        ALOGE("[JNI_PQ] nativeSetChameleonStrength() failed!");
        return JNI_FALSE;
    }

    return JNI_TRUE;

#else

    UNUSED(strength);
    UNUSED(step);
    ALOGE("[JNI_PQ] nativeSetChameleonStrength() not supported.");
    return JNI_FALSE;

#endif
}

static jint nativeGetChameleonStrength(JNIEnv* env, jclass clz)
{
    UNUSED(env);
    UNUSED(clz);

#if defined(MTK_PQ_SERVICE) && defined(MTK_CHAMELEON_DISPLAY_SUPPORT)
    int32_t getStrength = 0;

    sp<IPictureQuality> service = IPictureQuality::tryGetService();
    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return 0;
    }

    service->getChameleonStrength(
        [&] (Result retval, int32_t strength) {
        if (retval == Result::OK) {
            getStrength = strength;
        } else {
            ALOGE("[JNI_PQ] nativeGetChameleonStrength() failed!");
        }
    });

    return getStrength;

#else

    ALOGE("[JNI_PQ] nativeGetChameleonStrength() not supported.");
    return 0;

#endif
}


static jboolean nativeEnableChameleon(JNIEnv* env, jclass clz, jboolean isEnable, jint step)
{
    UNUSED(env);
    UNUSED(clz);

#if defined(MTK_PQ_SERVICE) && defined(MTK_CHAMELEON_DISPLAY_SUPPORT)
    sp<IPictureQuality> service = IPictureQuality::tryGetService();
    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    if (service->enableChameleon(isEnable ? true : false, step) != Result::OK) {
        ALOGE("[JNI_PQ] nativeEnableChameleon() failed!");
        return JNI_FALSE;
    }

    return JNI_TRUE;

#else

    UNUSED(isEnable);
    UNUSED(step);
    ALOGE("[JNI_PQ] nativeEnableChameleon() not supported.");
    return JNI_FALSE;

#endif
}


static jboolean nativeIsChameleonEnabled(JNIEnv* env, jclass clz)
{
    UNUSED(env);
    UNUSED(clz);

#if defined(MTK_PQ_SERVICE) && defined(MTK_CHAMELEON_DISPLAY_SUPPORT)
    bool getEnabled = false;

    sp<IPictureQuality> service = IPictureQuality::tryGetService();
    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->getChameleonEnabled(
        [&] (Result retval, bool isEnabled) {
        if (retval == Result::OK) {
            getEnabled = isEnabled;
        } else {
            ALOGE("[JNI_PQ] nativeIsChameleonEnabled() failed!");
        }
    });

    return (getEnabled ? JNI_TRUE : JNI_FALSE);

#else

    ALOGE("[JNI_PQ] nativeIsChameleonEnabled() not supported.");
    return JNI_FALSE;

#endif
}

static jint nativeGetDefaultOffTransitionStep(JNIEnv* env, jclass clz)
{
    UNUSED(env);
    UNUSED(clz);

    return PQ_DEFAULT_TRANSITION_OFF_STEP;
}

static jint nativeGetDefaultOnTransitionStep(JNIEnv* env, jclass clz)
{
    UNUSED(env);
    UNUSED(clz);

    return PQ_DEFAULT_TRANSITION_ON_STEP;
}

static jboolean nativeSetGlobalPQSwitch(JNIEnv* env, jclass clz, jint globalPQSwitch)
{
    UNUSED(env);
    UNUSED(clz);

#if defined(MTK_PQ_SERVICE) && defined(MTK_GLOBAL_PQ_SUPPORT)

    Mutex::Autolock autoLock(mLock);
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    if (service->setGlobalPQSwitch(globalPQSwitch) != Result::OK) {
        ALOGE("[JNI_PQ] nativeSetGlobalPQSwitch() failed!");
        return JNI_FALSE;
    }

    return JNI_TRUE;

#else

    UNUSED(globalPQSwitch);
    ALOGE("[JNI_PQ] nativeSetGlobalPQSwitch() not supported.");
    return JNI_FALSE;

#endif
}


static jint nativeGetGlobalPQSwitch(JNIEnv* env, jclass clz)
{
    UNUSED(env);
    UNUSED(clz);

#if defined(MTK_PQ_SERVICE) && defined(MTK_GLOBAL_PQ_SUPPORT)

    Mutex::Autolock autoLock(mLock);
    int32_t globalPQSwitch;
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->getGlobalPQSwitch(
        [&] (Result retval, int32_t switch_value) {
        if (retval == Result::OK) {
            globalPQSwitch = switch_value;
        } else {
            ALOGE("[JNI_PQ] nativeGetGlobalPQSwitch() failed!");
        }
    });

    ALOGD("[JNI_PQ] globalPQSwitch = %d\n", globalPQSwitch);

    return globalPQSwitch;

#else

    ALOGE("[JNI_PQ] nativeGetGlobalPQSwitch() not supported.");
    return 0;

#endif
}

static jboolean nativeSetGlobalPQStrength(JNIEnv* env, jclass clz, jint globalPQStrength)
{
    UNUSED(env);
    UNUSED(clz);

#if defined(MTK_PQ_SERVICE) && defined(MTK_GLOBAL_PQ_SUPPORT)

    Mutex::Autolock autoLock(mLock);
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    if (service->setGlobalPQStrength(globalPQStrength) != Result::OK) {
        ALOGE("[JNI_PQ] nativeSetGlobalPQStrength() failed!");
        return JNI_FALSE;
    }

    return JNI_TRUE;

#else

    UNUSED(globalPQStrength);
    ALOGE("[JNI_PQ] nativeSetGlobalPQStrength() not supported.");
    return JNI_FALSE;

#endif
}


static jint nativeGetGlobalPQStrength(JNIEnv* env, jclass clz)
{
    UNUSED(env);
    UNUSED(clz);

#if defined(MTK_PQ_SERVICE) && defined(MTK_GLOBAL_PQ_SUPPORT)

    Mutex::Autolock autoLock(mLock);
    uint32_t globalPQStrength;
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->getGlobalPQStrength(
        [&] (Result retval, int32_t strength) {
        if (retval == Result::OK) {
            globalPQStrength = strength;
        } else {
            ALOGE("[JNI_PQ] nativeGetGlobalPQStrength() failed!");
        }
    });

    ALOGD("[JNI_PQ] globalPQStrength = %d\n", globalPQStrength);

    return globalPQStrength;

#else

    ALOGE("[JNI_PQ] nativeGetGlobalPQStrength() not supported.");
    return 0;

#endif
}


static jint nativeGetGlobalPQStrengthRange(JNIEnv* env, jclass clz)
{
    UNUSED(env);
    UNUSED(clz);

#if defined(MTK_PQ_SERVICE) && defined(MTK_GLOBAL_PQ_SUPPORT)

    Mutex::Autolock autoLock(mLock);
    uint32_t globalPQStrengthRange;
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->getGlobalPQStrengthRange(
        [&] (Result retval, int32_t strength_range) {
        if (retval == Result::OK) {
            globalPQStrengthRange = strength_range;
        } else {
            ALOGE("[JNI_PQ] nativeGetGlobalPQStrength() failed!");
        }
    });

    ALOGD("[JNI_PQ] globalPQStrengthRange = %d\n", globalPQStrengthRange);

    return globalPQStrengthRange;

#else

    ALOGE("[JNI_PQ] nativeGetGlobalPQStrengthRange() not supported.");
    return 0;

#endif
}

static void nativeSetAALFunction(JNIEnv* env, jclass clz, jint func)
{
    UNUSED(env);
    UNUSED(clz);

#if defined(MTK_AAL_SUPPORT)
    AALClient::getInstance().setFunction(func);
#else
    UNUSED(func);
    ALOGE("nativeSetAALFunction(): MTK_AAL_SUPPORT disabled");
#endif
}

static jboolean nativeSetExternalPanelNits(JNIEnv* env, jclass clz, jint externalPanelNits)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    if (service->setExternalPanelNits(externalPanelNits) != Result::OK) {
        ALOGE("[JNI_PQ] nativeSetExternalPanelNits() failed!");
        return JNI_FALSE;
    }
#else
    ALOGE("[JNI_PQ] nativeSetExternalPanelNits() not supported.");

    UNUSED(externalPanelNits);
#endif
    UNUSED(env);
    UNUSED(clz);

    return JNI_TRUE;
}


static jint nativeGetExternalPanelNits(JNIEnv* env, jclass clz)
{
    uint32_t getExternalPanelNits = 0;
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return 0;
    }

    service->getExternalPanelNits(
        [&] (Result retval, uint32_t externalPanelNits) {
        if (retval == Result::OK) {
            getExternalPanelNits = externalPanelNits;
        } else {
            ALOGE("[JNI_PQ] nativeGetExternalPanelNits() failed!");
        }
    });
#else
    ALOGE("[JNI_PQ] nativeGetExternalPanelNits() not supported.");
#endif
    UNUSED(env);
    UNUSED(clz);

    return getExternalPanelNits;
}


static jboolean nativeSetRGBGain(JNIEnv *env, jobject thiz, jint r_gain, jint g_gain, jint b_gain, jint step)
{
#ifdef MTK_PQ_SERVICE
    sp<IPictureQuality> service = IPictureQuality::tryGetService();

    if (service == nullptr) {
        ALOGE("[JNI_PQ] failed to get HW service");
        return JNI_FALSE;
    }

    service->setRGBGain(r_gain, g_gain, b_gain, step);
#else
    ALOGE("[JNI_PQ] setRGBGain(), not supported!");

    UNUSED(r_gain);
    UNUSED(g_gain);
    UNUSED(b_gain);
    UNUSED(step);
#endif
    UNUSED(env);
    UNUSED(thiz);

    return JNI_TRUE;
}

/////////////////////////////////////////////////////////////////////////////////

//JNI register
////////////////////////////////////////////////////////////////
static const char *classPathName = JNI_PQ_CLASS_NAME;

static JNINativeMethod g_methods[] = {

    // query features
    {"nativeGetCapability", "()I", (void*)getCapability},

    // Camera PQ switch
    {"nativeSetCameraPreviewMode", "(I)V", (void*)setCameraPreviewMode},
    {"nativeSetGalleryNormalMode", "(I)V", (void*)setGalleryNormalMode},
    {"nativeSetVideoPlaybackMode", "(I)V", (void*)setVideoPlaybackMode},
    // Image DC
    {"nativeGetDynamicContrastHistogram", "([BIIL" JNI_PQ_CLASS_NAME "$Hist;)V", (void*)getDynamicContrastHistogram},

    // MiraVision setting
    {"nativeEnablePQ", "(I)Z", (void*)enablePQ},
    {"nativeEnableColor", "(I)Z", (void*)enableColor},
    {"nativeEnableContentColor", "(I)Z", (void*)enableContentColor},
    {"nativeEnableSharpness", "(I)Z", (void*)enableSharpness},
    {"nativeEnableDynamicContrast", "(I)Z", (void*)enableDynamicContrast},
    {"nativeEnableDynamicSharpness", "(I)Z", (void*)enableDynamicSharpness},
    {"nativeEnableColorEffect", "(I)Z", (void*)enableColorEffect},
    {"nativeEnableGamma", "(I)Z", (void*)enableGamma},
    {"nativeEnableUltraResolution", "(I)Z", (void*)enableUltraResolution},
    {"nativeEnableISOAdaptiveSharpness", "(I)Z", (void*)enableISOAdaptiveSharpness},
    {"nativeGetPictureMode", "()I", (void*)getPictureMode},
    {"nativeSetPictureMode", "(II)Z", (void*)setPictureMode},
    {"nativeSetColorRegion", "(IIIII)Z", (void*)setColorRegion},
    {"nativeGetContrastIndexRange", "(L" JNI_PQ_CLASS_NAME "$Range;)V", (void*)getContrastIndexRange},
    {"nativeGetContrastIndex", "()I", (void*)getContrastIndex},
    {"nativeSetContrastIndex", "(II)V", (void*)setContrastIndex},
    {"nativeGetSaturationIndexRange", "(L" JNI_PQ_CLASS_NAME "$Range;)V", (void*)getSaturationIndexRange},
    {"nativeGetSaturationIndex", "()I", (void*)getSaturationIndex},
    {"nativeSetSaturationIndex", "(II)V", (void*)setSaturationIndex},
    {"nativeGetPicBrightnessIndexRange", "(L" JNI_PQ_CLASS_NAME "$Range;)V", (void*)getPicBrightnessIndexRange},
    {"nativeGetPicBrightnessIndex", "()I", (void*)getPicBrightnessIndex},
    {"nativeSetPicBrightnessIndex", "(II)V", (void*)setPicBrightnessIndex},
    {"nativeGetSharpnessIndexRange", "(L" JNI_PQ_CLASS_NAME "$Range;)V", (void*)getSharpnessIndexRange},
    {"nativeGetSharpnessIndex", "()I", (void*)getSharpnessIndex},
    {"nativeSetSharpnessIndex", "(I)V", (void*)setSharpnessIndex},
    {"nativeGetDynamicContrastIndexRange", "(L" JNI_PQ_CLASS_NAME "$Range;)V", (void*)getDynamicContrastIndexRange},
    {"nativeGetDynamicContrastIndex", "()I", (void*)getDynamicContrastIndex},
    {"nativeSetDynamicContrastIndex", "(I)V", (void*)setDynamicContrastIndex},
    {"nativeGetColorEffectIndexRange", "(L" JNI_PQ_CLASS_NAME "$Range;)V", (void*)getColorEffectIndexRange},
    {"nativeGetColorEffectIndex", "()I", (void*)getColorEffectIndex},
    {"nativeSetColorEffectIndex", "(I)V", (void*)setColorEffectIndex},
    {"nativeGetGammaIndexRange", "(L" JNI_PQ_CLASS_NAME "$Range;)V", (void*)getGammaIndexRange},
    {"nativeSetGammaIndex", "(II)V", (void*)setGammaIndex},
    {"nativeEnableOD", "(I)Z", (void*)enableOD},
    {"nativeSetBlueLightStrength", "(II)Z", (void*)nativeSetBlueLightStrength},
    {"nativeGetBlueLightStrength", "()I", (void*)nativeGetBlueLightStrength},
    {"nativeEnableBlueLight", "(ZI)Z", (void*)nativeEnableBlueLight},
    {"nativeIsBlueLightEnabled", "()Z", (void*)nativeIsBlueLightEnabled},
    {"nativeSetChameleonStrength", "(II)Z", (void*)nativeSetChameleonStrength},
    {"nativeGetChameleonStrength", "()I", (void*)nativeGetChameleonStrength},
    {"nativeEnableChameleon", "(ZI)Z", (void*)nativeEnableChameleon},
    {"nativeIsChameleonEnabled", "()Z", (void*)nativeIsChameleonEnabled},
    {"nativeGetDefaultOffTransitionStep", "()I", (void*)nativeGetDefaultOffTransitionStep},
    {"nativeGetDefaultOnTransitionStep", "()I", (void*)nativeGetDefaultOnTransitionStep},
    {"nativeSetGlobalPQSwitch", "(I)Z", (void*)nativeSetGlobalPQSwitch},
    {"nativeGetGlobalPQSwitch", "()I", (void*)nativeGetGlobalPQSwitch},
    {"nativeSetGlobalPQStrength", "(I)Z", (void*)nativeSetGlobalPQStrength},
    {"nativeGetGlobalPQStrength", "()I", (void*)nativeGetGlobalPQStrength},
    {"nativeGetGlobalPQStrengthRange", "()I", (void*)nativeGetGlobalPQStrengthRange},
    {"nativeEnableVideoHDR", "(Z)Z", (void*)nativeEnableVideoHDR},
    {"nativeIsVideoHDREnabled", "()Z", (void*)nativeIsVideoHDREnabled},
    {"nativeSetAALFunction", "(I)V", (void*)nativeSetAALFunction},
    {"nativeSetExternalPanelNits", "(I)Z", (void*)nativeSetExternalPanelNits},
    {"nativeGetExternalPanelNits", "()I", (void*)nativeGetExternalPanelNits},
    {"nativeSetRGBGain", "(IIII)Z", (void*)nativeSetRGBGain},
};

/*
 * Register several native methods for one class.
 */
static int registerNativeMethods(JNIEnv* env, const char* className,
    JNINativeMethod* gMethods, int numMethods)
{
    jclass clazz;

    clazz = env->FindClass(className);
    if (clazz == NULL) {
        ALOGE("Native registration unable to find class '%s'", className);
        return JNI_FALSE;
    }
    if (env->RegisterNatives(clazz, gMethods, numMethods) < 0) {
        ALOGE("RegisterNatives failed for '%s'", className);
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

// ----------------------------------------------------------------------------

/*
 * This is called by the VM when the shared library is first loaded.
 */

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    JNIEnv* env = NULL;
    jint result = -1;

    UNUSED(reserved);

    ALOGI("JNI_OnLoad");

    if (JNI_OK != vm->GetEnv((void **)&env, JNI_VERSION_1_4)) {
        ALOGE("ERROR: GetEnv failed");
        goto bail;
    }

    if (!registerNativeMethods(env, classPathName, g_methods, sizeof(g_methods) / sizeof(g_methods[0]))) {
        ALOGE("ERROR: registerNatives failed");
        goto bail;
    }

    result = JNI_VERSION_1_4;

bail:
    return result;
}
