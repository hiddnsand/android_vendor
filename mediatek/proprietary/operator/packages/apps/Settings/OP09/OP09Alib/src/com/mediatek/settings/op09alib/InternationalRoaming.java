/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.UserHandle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.PhoneConstants;

/**
 * The class <code>InternationalRoaming</code> is main class of International
 * Roaming about China Telecom.
 */
public class InternationalRoaming extends Activity implements TabListener {

    private static final String TAG = "InternationalRoaming";

    private static final String KEY_CURRENT_SLOT = "current_slot";
    private int mCurSlot = 0;

    private SlotSettingsFragment mSlot1Fragement = null;
    private SlotSettingsFragment mSlot2Fragement = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (UserHandle.myUserId() == UserHandle.USER_OWNER) {
            mSlot1Fragement = new SlotSettingsFragment();
            mSlot2Fragement = new SlotSettingsFragment();
            mSlot1Fragement.setSlotId(PhoneConstants.SIM_ID_1);
            mSlot2Fragement.setSlotId(PhoneConstants.SIM_ID_2);

            setContentView(R.layout.international_roaming_settings);
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(
                Context.TELEPHONY_SERVICE);
            if (telephonyManager != null && telephonyManager.getSimCount() == 1) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.add(R.id.tab, mSlot1Fragement);
                transaction.commitAllowingStateLoss();
                return;
            }
            ActionBar actionBar = getActionBar();
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            Tab simSlot1 = actionBar.newTab();
            simSlot1.setText(R.string.sim_slot_1);
            simSlot1.setTabListener(this);
            actionBar.addTab(simSlot1);
            Tab simSlot2 = actionBar.newTab();
            simSlot2.setText(R.string.sim_slot_2);
            simSlot2.setTabListener(this);
            actionBar.addTab(simSlot2);
            // Get current slot and select the corresponding tab if created from
            // relaunching.
            if (savedInstanceState != null) {
                mCurSlot = savedInstanceState.getInt(KEY_CURRENT_SLOT);
            }
            Log.d("@M_" + TAG, "onCreate: savedInstanceState = "
                    + savedInstanceState + ",mSlot1Fragement = "
                    + mSlot1Fragement + ", mSlot2Fragement = "
                    + mSlot2Fragement + ",mCurSlot = "
                    + mCurSlot);
            if (mCurSlot == PhoneConstants.SIM_ID_1) {
                actionBar.selectTab(simSlot1);
            } else if (mCurSlot == PhoneConstants.SIM_ID_2) {
                actionBar.selectTab(simSlot2);
            }
        } else {
            setContentView(R.layout.international_roaming_settings_guest);
        }
        setTaskDescription(new ActivityManager.TaskDescription(
                getResources().getString(R.string.international_roaming_label),
                BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_launcher_international_roaming_set)
                ));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (UserHandle.myUserId() == UserHandle.USER_OWNER) {
            outState.putInt(KEY_CURRENT_SLOT, mCurSlot);
            Log.d("@M_" + TAG, "onSaveInstanceState: outState = "
                    + outState + ", mCurSlot = " + mCurSlot);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if (UserHandle.myUserId() == UserHandle.USER_OWNER) {
            mCurSlot = state.getInt(KEY_CURRENT_SLOT);
            Log.d("@M_" + TAG, "onRestoreInstanceState: state = "
                    + state + ", mCurSlot = " + mCurSlot);
        }
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        if (UserHandle.myUserId() == UserHandle.USER_OWNER) {
            mCurSlot = tab.getPosition();
            Log.d("@M_" + TAG, "onTabSelected: tab.getPosition() = "
                    + tab.getPosition() + ", mCurSlot = "
                    + mCurSlot);
            if (mCurSlot == PhoneConstants.SIM_ID_1) {
                ft.replace(R.id.tab, mSlot1Fragement);
            } else if (mCurSlot == PhoneConstants.SIM_ID_2) {
                ft.replace(R.id.tab, mSlot2Fragement);
            }
        }
    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {
    }

}
