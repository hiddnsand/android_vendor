package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class StationarySensorActivity extends BaseTriggerSensorActivity {

    public StationarySensorActivity() {
        super(Utils.KEY_STATIONARY_STATUS, Utils.KEY_STATIONARY_NOTIFY_STATUS,
                Utils.KEY_STATIONARY_AUTO_STATUS, Sensor.TYPE_STATIONARY_DETECT);
    }
}
