/*
 * Copyright (C) 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Contributed by: Giesecke & Devrient GmbH.
 */

package org.simalliance.openmobileapi.uicc1terminal;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.IccOpenLogicalChannelResponse;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.MissingResourceException;
import java.util.NoSuchElementException;

import org.simalliance.openmobileapi.internal.ByteArrayConverter;
import org.simalliance.openmobileapi.service.ITerminalService;
import org.simalliance.openmobileapi.service.SmartcardError;
import org.simalliance.openmobileapi.service.OpenLogicalChannelResponse;
import org.simalliance.openmobileapi.internal.Util;
/// M: @ {
import android.os.ServiceManager;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.mediatek.internal.telephony.IMtkTelephonyEx;
import android.os.SystemProperties;
import android.telephony.SubscriptionManager;
/// }

public final class Uicc1Terminal extends Service {

    private static final String TAG = "Uicc1Terminal";

    /// M: @ {
    // Card state is ready (RIL state String)
    private static final String RIL_CARD_READY = "READY";
    private static final int mUiccNumber = 0; // UICC Number
    private static final int MAX_LOGICAL_CHANNEL_NUMBER = 20;
    private static final int GET_SUBID_NULL_ERROR = -99;
    /// }

    public static final String ACTION_SIM_STATE_CHANGED =
        "org.simalliance.openmobileapi.action.SIM1_STATE_CHANGED";

    /// M: @ {
    private ITelephony manager = null;
    private IBinder mBinder = null;
    private IMtkTelephonyEx managerEx = null;
    private IBinder mBinderEx = null;
    /// }

    private List<Integer> mChannelIds;

    private BroadcastReceiver mSimReceiver;

    private String mCurrentSelectedFilePath;

    private byte[] mAtr;

    @Override
    public IBinder onBind(Intent intent) {
        return new TerminalServiceImplementation();
    }

    @Override
    public void onCreate() {
        /// M: @ {
        // Get telephony manager
        try {
            mBinder = ServiceManager.getService("phone");
            manager = ITelephony.Stub.asInterface(mBinder);

            mBinderEx = ServiceManager.getService("phoneEx");
            managerEx = IMtkTelephonyEx.Stub.asInterface(mBinderEx);
        } catch (Exception ex) {
            Log.d(TAG, "Exception for getService(phone) ");
            ex.printStackTrace();
        }
        Log.d(TAG, "Uicc1Terminal(), telephony manager = " + manager);
        Log.d(TAG, "Uicc1Terminal(), telephony manager ex = " + managerEx);

        /// }

        // Constructor
        /// M: Error handling for hot-plug SIM card @{
        resetChannelIds();
        /// M: Error handling for hot-plug SIM card @}

        registerSimStateChangedEvent();
        mCurrentSelectedFilePath = "";
        mAtr = null;
    }

    @Override
    public void onDestroy() {
        unregisterSimStateChangedEvent();
        super.onDestroy();
    }

    /// M: Error handling for hot-plug SIM card @{
    private void resetChannelIds() {
        mChannelIds = new ArrayList<>();
        // Occupy mChannelIds[0] to avoid return channel number = 0 on openLogicalChannel
        mChannelIds.add(0xFFFFFFFF);
        for (int i = 1; i < MAX_LOGICAL_CHANNEL_NUMBER; i++) {
            mChannelIds.add(0x00);
        }
    }
    /// M: Error handling for hot-plug SIM card @}

    /**
     * Performs all the logic for opening a logical channel.
     *
     * @param aid The AID to which the channel shall be opened, empty string to
     * specify "no AID".
     *
     * @return The index of the opened channel ID in the mChannelIds list.
     */
    private OpenLogicalChannelResponse iccOpenLogicalChannel(String aid, byte p2)
            throws NoSuchElementException, MissingResourceException, IOException {
        Log.d(TAG, "iccOpenLogicalChannel > " + aid
            + " p2(byte)=" + p2 + " p2(int)=" + ((int) p2 & 0xFF));
        // Remove any previously stored selection response
        IccOpenLogicalChannelResponse response;
        String selectAIDResponse = null;

        /// M: @ {
        Log.d(TAG, "internalOpenLogicalChannel --> iccOpenLogicalChannel");
        try {
            if (manager == null || (mBinder != null && !mBinder.isBinderAlive())) {
                mBinder = ServiceManager.getService("phone");
                manager = ITelephony.Stub.asInterface(mBinder);
                Log.d(TAG, "iccOpenLogicalChannel(), re-get telephony manager = " + manager);
            }
            response = manager.iccOpenLogicalChannel(
                getSubIdBySlot(mUiccNumber), getOpPackageName(),
                aid, (int) p2 & 0xFF);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new IOException("iccOpenLogicalChannel failed");
        }

        int status = response.getStatus();
        if (status != IccOpenLogicalChannelResponse.STATUS_NO_ERROR) {
            Log.d(TAG, "iccOpenLogicalChannel failed.");
            // An error occured.
            if (status == IccOpenLogicalChannelResponse.STATUS_MISSING_RESOURCE) {
                Log.d(TAG, "all channels are used");
                return null;
            }
            if (status == IccOpenLogicalChannelResponse.STATUS_NO_SUCH_ELEMENT) {
                throw new NoSuchElementException("Applet not found");
            }
            throw new IOException("iccOpenLogicalChannel failed");
        }

        // Operation succeeded
        // Set the select response
        // Save channel ID. First check if there is any channelID which is empty
        // to reuse it.
        for (int i = 1; i < MAX_LOGICAL_CHANNEL_NUMBER; i++) {
            if (mChannelIds.get(i) == 0) {
                mChannelIds.set(i, response.getChannel());
                if ("".equals(aid)) {
                    Log.d(TAG, "aid is not provided, just executes the manage" +
                        " channel operation without SELECT command!");
                    Log.d(TAG, "Manage Channel Response:" +
                        ByteArrayConverter.byteArrayToHexString(response.getSelectResponse()));
                    return new OpenLogicalChannelResponse(i, null);
                }
                try {
                    Log.d(TAG, "aid:" + aid + " p2:"+String.format("%02X", p2));
                    selectAIDResponse = manager.iccTransmitApduLogicalChannel(
                        getSubIdBySlot(mUiccNumber), mChannelIds.get(i), mChannelIds.get(i), 0xA4,
                        0x04, p2 & 0xFF, aid.length()/2, aid);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    throw new IOException("iccTransmitApduLogicalChannel failed");
                }
                if (selectAIDResponse == null) {
                    Log.d(TAG, "DATA+SW1SW2 = null");
                }else {
                    Log.d(TAG, "DATA+SW1SW2 = " + selectAIDResponse + " len = " +
                        selectAIDResponse.length());
                }

                return new OpenLogicalChannelResponse(i,
                    ByteArrayConverter.hexStringToByteArray(selectAIDResponse));
            }
        }

        // If no channel ID is empty, append one at the end of the list.
        throw new IOException("out of channels");
        /// }
    }

    private void registerSimStateChangedEvent() {
        Log.v(TAG, "register to android.intent.action.SIM_STATE_CHANGED event");

        IntentFilter intentFilter = new IntentFilter("android.intent.action.SIM_STATE_CHANGED");
        mSimReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if ("android.intent.action.SIM_STATE_CHANGED".equals(intent.getAction())) {
                    /// M: @ {
                    /*Bundle extras = intent.getExtras();
                    boolean simReady = (extras != null)
                            && "READY".equals(extras.getString("ss"));
                    boolean simLoaded = (extras != null)
                            && "LOADED".equals(extras.getString("ss"));
                    if (simReady || simLoaded) {
                        Log.i(TAG, "SIM is ready or loaded. Checking access rules for updates.");
                        Intent i = new Intent(ACTION_SIM_STATE_CHANGED);
                        sendBroadcast(i);
                    }*/
                    String iccState;
                    int simId;
                    iccState = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
                    simId = intent.getIntExtra(PhoneConstants.SLOT_KEY, -1);
                    if(simId != mUiccNumber)
                        return;
                    if (iccState == null) {
                        iccState = "NULL";
                    }
                    Log.d(TAG, "ACTION_SIM_STATE_CHANGED receiver with iccState = " + iccState +
                        ", simId = " + simId);
                    if (iccState.equals(IccCardConstants.INTENT_VALUE_ICC_LOADED)) {
                        if (simId == mUiccNumber) {
                            Log.i(TAG, "SIM"+ simId +
                                " is ready. Checking access rules for updates.");
                            Intent i = new Intent(ACTION_SIM_STATE_CHANGED);
                            sendBroadcast(i);
                        }
                    /// M: Error handling for hot-plug SIM card @{
                    } else if (iccState.equals(IccCardConstants.INTENT_VALUE_ICC_ABSENT)) {
                        if (simId == mUiccNumber) {
                            resetChannelIds();
                        }
                    }
                    /// M: Error handling for hot-plug SIM card @}
                    /// }
                }
            }
        };
        registerReceiver(mSimReceiver, intentFilter);
    }

    private void unregisterSimStateChangedEvent() {
        if (mSimReceiver != null) {
            Log.v(TAG, "unregister SIM_STATE_CHANGED event");
            unregisterReceiver(mSimReceiver);
            mSimReceiver = null;
        }
    }

    /**
     * The Terminal service interface implementation.
     */
    final class TerminalServiceImplementation extends ITerminalService.Stub {

        @Override
        public OpenLogicalChannelResponse internalOpenLogicalChannel(
                byte[] aid,
                byte p2,
                SmartcardError error) throws RemoteException {
            if (aid == null && SystemProperties.get("ro.mtk_nfc_gsma_support", "0").equals("0")) {
                throw new NullPointerException("aid must not be null");
            }
            try {
                return iccOpenLogicalChannel(ByteArrayConverter.byteArrayToHexString(aid), p2);
            } catch (Exception e) {
                Log.e(TAG, "Exception at internalOpenLogicalChannel", e);
                error.set(e);
                return null;
            }

        }

        @Override
        public void internalCloseLogicalChannel(int channelNumber, SmartcardError error)
                throws RemoteException {
            try {
                if (channelNumber == 0) {
                    return;
                }
                if (mChannelIds.get(channelNumber) == 0) {
                    throw new IllegalStateException("Channel not open");
                }

                /// M: @ {
                Log.d(TAG,
                    "internalCloseLogicalChannel() --> ITelephony.iccCloseLogicalChannel()");
                if (manager.iccCloseLogicalChannel(getSubIdBySlot(mUiccNumber),
                    mChannelIds.get(channelNumber)) == false) {
                    throw new IOException("close channel failed");
                }
                /// }
                mChannelIds.set(channelNumber, 0);
            } catch (Exception e) {
                Log.e(TAG, "Exception at internalCloseLogicalChannel", e);
                error.set(e);
            }
        }

        @Override
        public byte[] internalTransmit(byte[] command, SmartcardError error)
            throws RemoteException {
            try {
                Log.d(TAG, "internalTransmit > " +
                    ByteArrayConverter.byteArrayToHexString(command));
                int cla = Util.clearChannelNumber(command[0]) & 0xFF;
                int ins = command[1] & 0xff;
                int p1 = command[2] & 0xff;
                int p2 = command[3] & 0xff;
                int p3 = -1;
                /*--- Fixed for APDU command with header(4 bytes - CLA, INS, P1 & P2) only---*/
                if (command.length == 4) {
                    p3 = 0;
                }
                if (command.length > 4) {
                    p3 = command[4] & 0xff;
                }
                String data = null;
                if (command.length > 5) {
                    data = ByteArrayConverter.byteArrayToHexString(command, 5, command.length - 5);
                }

                int channelNumber = Util.parseChannelNumber(command[0]);

                String response;
                if (channelNumber == 0) {
                    Log.d(TAG, "internalTransmit() --> ITelephony.iccTransmitApduBasicChannel()");
                    try {
                        response = manager.iccTransmitApduBasicChannel(
                            getSubIdBySlot(mUiccNumber), cla, ins, p1, p2, p3, data);
                    } catch (Exception ex) {
                        throw new IOException("transmit command failed");
                    }
                } else {
                    if ((channelNumber > 0) && (mChannelIds.get(channelNumber) == 0)) {
                        throw new IOException("Channel not open");
                    }

                    try {
                        Log.d(TAG,
                                "APDU = "
                                + String.format("[%02x]%02x%02x%02x%02x%02x:",
                                mChannelIds.get(channelNumber), cla & 0xFC, ins, p1, p2, p3) +
                                data);

                        Log.d(TAG,
                            "internalTransmit() --> ITelephony.iccTransmitApduLogicalChannel()");
                        response = manager.iccTransmitApduLogicalChannel(
                            getSubIdBySlot(mUiccNumber), mChannelIds.get(channelNumber), cla,
                            ins, p1, p2, p3, data);
                        Log.d(TAG, "RAPDU = " + response);
                    } catch (Exception ex) {
                        throw new IOException("transmit command failed");
                    }
                }
                Log.d(TAG, "internalTransmit < " + response);
                return ByteArrayConverter.hexStringToByteArray(response);
            } catch (Exception e) {
                Log.e(TAG, "Exception at internalTransmit", e);
                error.set(e);
                return null;
            }
        }

        @Override
        public byte[] getAtr() {
            /// M: @ {
            Log.d(TAG, "Uicc1Terminal:getATR()");
            if (mAtr == null) {
                try {
                    if (managerEx == null || (mBinderEx != null && !mBinderEx.isBinderAlive())) {
                        mBinderEx = ServiceManager.getService("phoneEx");
                        managerEx = IMtkTelephonyEx.Stub.asInterface(mBinderEx);

                        Log.d(TAG, "getAtr(), re-get telephony manager ex = " + managerEx
                            + ", " + mBinderEx);
                    }

                    String atr;
                    Log.d(TAG, "getATR() --> ITelephony.getIccAtr() ex");
                    atr = managerEx.getIccAtr(getSubIdBySlot(mUiccNumber));
                    Log.d(TAG, "atr = " + (atr == null ? "" : atr));
                    if (atr != null && !"".equals(atr)) {
                        mAtr = ByteArrayConverter.hexStringToByteArray(atr);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            return mAtr;
            /// }
        }

        @Override
        public boolean isCardPresent() throws RemoteException {
            /// M: @ {
            int sim_State = -1;
            try {
                sim_State = TelephonyManager.getDefault().getSimState(mUiccNumber);
            } catch (Exception ex) {
                ex.printStackTrace();
                Log.d(TAG,"Cannot getSimState from Uicc" + mUiccNumber + " TelephonyManager");
                return false;
                //throw new IOException("Cannot getSimState from Uicc" +
                //    mUiccNumber + " TelephonyManager");
            }

            Log.d(TAG, "isCardPresent(), SIM" + mUiccNumber + " sim_State:" + sim_State);

            if (sim_State == android.telephony.TelephonyManager.SIM_STATE_READY) {
                Log.d(TAG, "SIM " + mUiccNumber + " is ready");
                return true;
            }

            if (manager == null || (mBinder != null && !mBinder.isBinderAlive())) {
                try {
                    mBinder = ServiceManager.getService("phone");
                    manager = ITelephony.Stub.asInterface(mBinder);
                    /*manager = ITelephony.Stub.asInterface(ServiceManager
                            .getService("phone"));*/
                } catch (Exception ex) {
                    Log.d(TAG, "Exception for getService(phone) ");
                    ex.printStackTrace();
                }
                Log.d(TAG, "isCardPresent(), re-get telephony manager = " + manager);
            }

            if (managerEx == null || (mBinderEx != null && !mBinderEx.isBinderAlive())) {
                try {
                    mBinderEx = ServiceManager.getService("phoneEx");
                    managerEx = IMtkTelephonyEx.Stub.asInterface(mBinderEx);
                } catch (Exception ex) {
                    Log.d(TAG, "Exception for getService(phone) ");
                    ex.printStackTrace();
                }
                Log.d(TAG, "isCardPresent(), re-get telephony manager ex = " + managerEx
                    + ", " + mBinderEx);
            }

            return false;
            /// }
        }

        @Override
        public byte[] simIOExchange(int fileID, String filePath, byte[] cmd, SmartcardError error)
                throws RemoteException {
            try {
                int ins;
                int p1 = cmd[2] & 0xff;
                int p2 = cmd[3] & 0xff;
                int p3 = cmd[4] & 0xff;
                switch (cmd[1]) {
                    case (byte) 0xB0:
                        ins = 176;
                        break;
                    case (byte) 0xB2:
                        ins = 178;
                        break;
                    case (byte) 0xA4:
                        ins = 192;
                        p1 = 0;
                        p2 = 0;
                        p3 = 15;
                        break;
                    default:
                        throw new IOException("Unknown SIM_IO command");
                }

                if (filePath != null && filePath.length() > 0) {
                    mCurrentSelectedFilePath = filePath;
                }

                return manager.iccExchangeSimIO(getSubIdBySlot(mUiccNumber), fileID, ins, p1,
                    p2, p3, mCurrentSelectedFilePath);
            } catch (Exception e) {
                Log.e(TAG, "Exception at simIOExchange", e);
                error.set(e);
                return null;
            }
        }

        @Override
        public String getSeStateChangedAction() {
            return ACTION_SIM_STATE_CHANGED;
        }
    }

    private int getSubIdBySlot(int slot) {
        int [] subId = SubscriptionManager.getSubId(slot);
        if (subId == null || subId.length == 0) {
            return  GET_SUBID_NULL_ERROR;
        }
        Log.d(TAG, "MTK getSubIdBySlot, simId " + slot + " subId " + subId[0]);
        return subId[0];
    }
}
