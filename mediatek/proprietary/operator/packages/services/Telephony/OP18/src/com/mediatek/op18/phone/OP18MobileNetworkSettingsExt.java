/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op18.phone;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.telephony.RadioAccessFamily;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.ProxyController;

import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.op18.phone.R;
import com.mediatek.phone.ext.DefaultMobileNetworkSettingsExt;


import java.util.List;
/**
 * Plugin implementation for WFC Settings plugin
 */
public class OP18MobileNetworkSettingsExt extends DefaultMobileNetworkSettingsExt {
    private static final String TAG = "OP18MobileNetworkSettingsExt";

    private static final String NETWORKINIT = "NETWORKINIT";
    private static final String BUTTON_4G_LTE_KEY = "enhanced_4g_lte";
    private Context mContext = null;
    public static final String BUTTON_ENABLED_NETWORKS_KEY = "enabled_networks_key";

    private boolean mIsWfcReceiverRegistered = false;
    private PreferenceActivity mCallFeatureSettingsActivity = null;
    private Preference mButton4gLte = null;

    private IntentFilter mIntentFilter;
    private MyHandler mHandler;
    private static final int MESSAGE_SET_PREFERRED_NETWORK_TYPE = 0;
    private ListPreference mButtonEnabledNetworks = null;
    private int mNetworkMode = -1;
    private int mCurrentMode = -1;
    ContentResolver mContentResolver;
    int mPhoneSubId;
    private Phone mPhone;
    private boolean mVolteConnected;
    private int mPrimaryPhoneSubId;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "onReceive action = " + action);
            if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                final NetworkInfo networkInfo = (NetworkInfo)
                        intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (networkInfo != null) {
                    Log.d(TAG, "onReceive: CONNECTIVITY_ACTION");
                    int nwType = networkInfo.getType();
                    String typename = networkInfo.getTypeName();
                    String subtypename =  networkInfo.getSubtypeName();
                    Log.d(TAG, "nwType:" + nwType + " typename = "
                            + typename + " subtypename = " + subtypename);
                    if ("MOBILEIMS".equals(typename) &&
                         nwType == ConnectivityManager.TYPE_MOBILE_IMS) {
                        mVolteConnected = networkInfo.isConnected();
                    }
                }
            }
        }
    };

    /** Constructor.
     * @param context context
     */
    public OP18MobileNetworkSettingsExt(Context context) {
        super();
        mContext = context;
        mHandler = new MyHandler();
    }
    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        mContext.registerReceiver(mReceiver, filter);
    }

    /**
     * Updating entry and entry values of list preference.
     * @param activity preference activity
     * @param subId phone id
     */
    @Override
    public void initOtherMobileNetworkSettings(PreferenceActivity activity, int subId) {
        Log.d(TAG, "update entry and entry values of list preference");
        PreferenceScreen prefSet = activity.getPreferenceScreen();
        ListPreference buttonEnabledNetworks = (ListPreference) prefSet.findPreference(
                BUTTON_ENABLED_NETWORKS_KEY);
        if (buttonEnabledNetworks == null) {
            Log.d(TAG, "buttonEnabledNetworks is null");
            return;
        }
        int phoneId = SubscriptionManager.getPhoneId(subId);
        CharSequence entries[] = mContext.getResources().getTextArray(R.array.network_mode_options);
        CharSequence entryValues[] = mContext.getResources().getTextArray(
                R.array.network_mode_options_values);
        buttonEnabledNetworks.setEntries(entries);
        buttonEnabledNetworks.setEntryValues(entryValues);
        Phone phone =
                PhoneFactory.getPhone(SubscriptionManager.getPhoneId(subId));
        int currentNwMode =
                android.provider.Settings.Global.getInt(phone.getContext().getContentResolver(),
                android.provider.Settings.Global.PREFERRED_NETWORK_MODE + subId, 0);
        Log.d(TAG, "Current network mode for subId " + subId + " : " + currentNwMode);
        int currRat = phone.getRadioAccessFamily();
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        /*if (SystemProperties.getInt("ro.mtk_multiple_ims_support", 1) != 1) {
            boolean initNetwok = sharedPref.getBoolean(NETWORKINIT, false);
            if (!((currRat & RadioAccessFamily.RAF_LTE) == RadioAccessFamily.RAF_LTE) &&
                    initNetwok == false) {
                if ((currRat & RadioAccessFamily.RAF_UMTS) == RadioAccessFamily.RAF_UMTS) {
                    updateNetworkMode(subId, Phone.NT_MODE_WCDMA_PREF);
                } else {
                    updateNetworkMode(subId, Phone.NT_MODE_GSM_ONLY);
                }
                editor.putBoolean(NETWORKINIT, true);
                editor.commit();
            }
        }*/

    }

    /**
     * Updating network mode and summary.
     * @param buttonEnabledNetworks list preference
     * @param networkMode network mode
     */
    @Override
    public void updatePreferredNetworkValueAndSummary(ListPreference buttonEnabledNetworks,
            int networkMode) {
        Log.d(TAG, "Updating network mode and summary. networkMode:" + networkMode);
        if (buttonEnabledNetworks == null) {
            Log.d(TAG, "buttonEnabledNetworks is null");
            return;
        }
        switch (networkMode) {
            case Phone.NT_MODE_LTE_GSM_WCDMA :
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_LTE_GSM_WCDMA));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.gsm_wcdma_lte_mode));
                break;
            case Phone.NT_MODE_WCDMA_PREF:
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_WCDMA_PREF));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.gsm_wcdma_mode));
                break;
            case Phone.NT_MODE_WCDMA_ONLY:
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_WCDMA_ONLY));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.wcdma_only_mode));
                break;
            case Phone.NT_MODE_GSM_ONLY :
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_GSM_ONLY));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.gsm_only_mode));
                break;
            case Phone.NT_MODE_LTE_ONLY:
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_LTE_ONLY));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_only_mode));
                break;
            default:
        }
    }

    /**
     * Updating network mode and summary.
     * @param buttonEnabledNetworks list preference
     * @param networkMode network mode
     * @param currentMode current network mode
     * @param phone Phone on which UE will handle
     * @param cr Content resolver of Settings
     * @param phoneSubId SubID on which change occurred
     * @param handler Handler to take care of NEtwork update
     * @return false if operator handle
     */
    @Override
    public boolean isNetworkUpdateNeeded(ListPreference buttonEnabledNetworks,
            int networkMode, int currentMode, Phone phone, ContentResolver cr,
            int phoneSubId, Handler handler) {
        Log.d(TAG, "isNetworkUpdateNeeded:" + networkMode);
        mContentResolver = cr;
        mButtonEnabledNetworks = buttonEnabledNetworks;
        mCurrentMode = currentMode;
        mNetworkMode = networkMode;
        mPhoneSubId = phoneSubId;
        mPhone = phone;
        mPrimaryPhoneSubId = android.provider.Settings.Global.getInt(mContentResolver,
                            com.mediatek.provider.MtkSettingsExt.Global.PRIMARY_SIM, 0);
        int primaryPhoneId = SubscriptionManager.getPhoneId(mPrimaryPhoneSubId);
        List<SubscriptionInfo> simList = SubscriptionManager.from(mContext).
                getActiveSubscriptionInfoList();
        if (buttonEnabledNetworks == null) {
            Log.d(TAG, "buttonEnabledNetworks is null");
            return true;
        }
        ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        AlertDialog.Builder builder;
        switch (networkMode) {
            case Phone.NT_MODE_GSM_ONLY :
            case Phone.NT_MODE_WCDMA_ONLY:
            case Phone.NT_MODE_WCDMA_PREF:
                if (!(currentMode == Phone.NT_MODE_LTE_ONLY ||
                    currentMode == Phone.NT_MODE_LTE_GSM_WCDMA)) {
                    Log.d(TAG, "Current mode is non LTE" + currentMode);
                    return true;
                }
                builder =
                        new AlertDialog.Builder(buttonEnabledNetworks.getContext())
                        .setTitle(mContext.getText(R.string.network_mode_update_warning_title))
                        .setMessage(getWarningText(networkMode, phoneSubId))
                        .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (simList != null) {
                                    int simCount = simList.size();
                                    for (int index = 0; index < simCount; index ++) {
                                        int subId = simList.get(index).getSubscriptionId();
                                        if (subId == mPhoneSubId) {
                                            //Change the network mode to GSM or 3G
                                            updateNetworkMode(mPhoneSubId, mNetworkMode);
                                            setSimIdCapabilityforSubId(mPhoneSubId, mNetworkMode);
                                            Log.d(TAG, "For yes change mode to Gsm");
                                        } else {
                                            //Set the network mode of other to Auto mode
                                            Phone phone =
                                                    PhoneFactory.getPhone(
                                                            SubscriptionManager.getPhoneId(subId));
                                            int currentNwMode =
                                                android.provider.Settings.Global.getInt(
                                                phone.getContext().getContentResolver(),
                                                android.provider.Settings.Global.
                                                PREFERRED_NETWORK_MODE + subId, 0);
                                            if (currentNwMode == Phone.NT_MODE_GSM_ONLY ||
                                                    currentNwMode == Phone.NT_MODE_WCDMA_ONLY ||
                                                    currentNwMode == Phone.NT_MODE_WCDMA_PREF) {
                                                updateNetworkMode(subId,
                                                        Phone.NT_MODE_LTE_GSM_WCDMA);
                                                Log.d(TAG, "yes set mode of other to auto");
                                            }
                                        }
                                    }
                                }
                            }
                         })
                        .setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                updatePreferredNetworkValueAndSummary(mButtonEnabledNetworks,
                                        currentMode);
                                Log.d(TAG, "For no don't change mode");
                            }
                         });
                builder.show();
                return false;
            case Phone.NT_MODE_LTE_ONLY :
            case Phone.NT_MODE_LTE_GSM_WCDMA :
                if ((currentMode == Phone.NT_MODE_LTE_ONLY ||
                    currentMode == Phone.NT_MODE_LTE_GSM_WCDMA) || isDeviceSingleSim()) {
                    Log.d(TAG, "Current mode is of LTE" + currentMode);
                    return true;
                }
                int othersMode;
                int smCount = simList.size();
                for (int index = 0; index < smCount; index ++) {
                    int subId = simList.get(index).getSubscriptionId();
                    if (subId != mPhoneSubId) {
                        Phone othersPhone =
                                PhoneFactory.getPhone(
                                        SubscriptionManager.getPhoneId(subId));
                        othersMode =
                            android.provider.Settings.Global.getInt(
                            othersPhone.getContext().getContentResolver(),
                            android.provider.Settings.Global.
                            PREFERRED_NETWORK_MODE + subId, 0);
                        if (!(othersMode == Phone.NT_MODE_LTE_ONLY ||
                                othersMode == Phone.NT_MODE_LTE_GSM_WCDMA)) {
                            Log.d(TAG, "No mode is currently LTE , so no warning or change" +
                                    "of mode needed: Others" + othersMode + "current: " +
                                    currentMode);
                            return true;
                        }
                    }
                }
                Log.d(TAG, "Show alert dialog for MODE_LTE");
                builder =
                        new AlertDialog.Builder(buttonEnabledNetworks.getContext())
                        .setTitle(mContext.getText(R.string.network_mode_update_warning_title))
                        .setMessage(getWarningText(networkMode, phoneSubId))
                        .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (simList != null) {
                                    int simCount = simList.size();
                                    for (int index = 0; index < simCount; index ++) {
                                        int subId = simList.get(index).getSubscriptionId();
                                        if (subId == mPhoneSubId) {
                                            //Set the network mode to Auto mode
                                            updateNetworkMode(mPhoneSubId, mNetworkMode);
                                            setSimIdCapabilityforSubId(mPhoneSubId, mNetworkMode);
                                            Log.d(TAG, "For yes change mode to LTE Only or Auto");
                                        } else {
                                            Phone phone =
                                                PhoneFactory.getPhone(
                                                        SubscriptionManager.getPhoneId(subId));
                                            int currentNwMode =
                                                android.provider.Settings.Global.getInt(
                                                phone.getContext().getContentResolver(),
                                                android.provider.Settings.Global.
                                                PREFERRED_NETWORK_MODE + subId, 0);
                                            if (currentNwMode == Phone.NT_MODE_LTE_GSM_WCDMA ||
                                                    currentNwMode == Phone.NT_MODE_LTE_ONLY) {
                                                //Set the network mode of other to GSM only mode
                                                updateNetworkMode(subId, Phone.NT_MODE_GSM_ONLY);
                                                Log.d(TAG, "For yes set mode of other to gsm");
                                            }
                                        }
                                    }
                                }
                            }
                         })
                        .setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                updatePreferredNetworkValueAndSummary(mButtonEnabledNetworks,
                                        currentMode);
                                Log.d(TAG, "For no don't change mode");
                            }
                         });
                builder.show();
                return false;
            default:
                updateNetworkMode(mPhoneSubId, mNetworkMode);
                setSimIdCapabilityforSubId(mPhoneSubId, mNetworkMode);
                Log.d(TAG, "For yes change mode by default");
        }
        return false;
    }

    @Override
    public boolean isNetworkModeSettingNeeded() {
        return false;
    }

    @Override
    public boolean isEnhancedLTENeedToAdd(boolean defaultValue, int phoneId) {
        if (SystemProperties.getInt("ro.mtk_multiple_ims_support", 1) != 1) {
            int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(phoneId);
            Phone phone = PhoneFactory.getPhone(SubscriptionManager.getPhoneId(subId));
            int primaryPhoneSubId = android.provider.Settings.Global.getInt(
                                phone.getContext().getContentResolver(),
                                com.mediatek.provider.MtkSettingsExt.Global.PRIMARY_SIM, 0);
            if (subId == primaryPhoneSubId) {
                return true;
            }
            return false;
        } else {
            return defaultValue;
        }
    }

    private void updateNetworkMode(int subId, int mode) {
        Log.d(TAG, "Updating network mode for subId " + subId + "mode " + mode);
        Phone phone = PhoneFactory.getPhone(SubscriptionManager.getPhoneId(subId));

        updatePreferredNetworkValueAndSummary(mButtonEnabledNetworks, mode);
        android.provider.Settings.Global.putInt(phone.getContext().getContentResolver(),
                android.provider.Settings.Global.PREFERRED_NETWORK_MODE + subId, mode);
        phone.setPreferredNetworkType(mode,
                mHandler.obtainMessage(MESSAGE_SET_PREFERRED_NETWORK_TYPE, subId, mode));
    }

    /**
     * MyHandler class to update network type.
     */
    private class MyHandler extends Handler {

        static final int MESSAGE_SET_PREFERRED_NETWORK_TYPE = 0;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_SET_PREFERRED_NETWORK_TYPE:
                    handleSetPreferredNetworkTypeResponse(msg, msg.arg1, msg.arg2);
                    break;
                default:
                    break;
            }
        }

        private void handleSetPreferredNetworkTypeResponse(Message msg, int subId, int mode) {

            AsyncResult ar = (AsyncResult) msg.obj;
            Phone phone = PhoneFactory.getPhone(SubscriptionManager.getPhoneId(subId));
            if (ar.exception == null) {
                Log.d(TAG, "handleSetPreferredNetwrokTypeResponse2: networkMode:" + mode);
                android.provider.Settings.Global.putInt(phone.getContext().getContentResolver(),
                        android.provider.Settings.Global.PREFERRED_NETWORK_MODE + subId,
                        mode);
            } else {
                Log.d(TAG, "handleSetPreferredNetworkTypeResponse:exception in setting network.");
            }
        }
    }

    private boolean setSimIdCapabilityforSubId(int phoneSubId, int mode) {
        ProxyController proxyController = ProxyController.getInstance();
        List<SubscriptionInfo> simList = SubscriptionManager.from(mContext).
                getActiveSubscriptionInfoList();
        int simCount = simList.size();
        RadioAccessFamily[] rafs = new RadioAccessFamily[simCount];
        int raf;
        int count = SubscriptionManager.from(mContext).getActiveSubscriptionInfoCount();
        Log.d(TAG, "Active Sub Info Count " + count);
        if (count == 1) {
            return true;
        }
        if (mode == Phone.NT_MODE_GSM_ONLY) {
            for (int index = 0; index < simCount; index ++) {
                int subId = simList.get(index).getSubscriptionId();
                if (subId == phoneSubId) {
                    raf = proxyController.getMinRafSupported();
                    Log.d(TAG, "Set proxyController for GSM Only to GSM");
                } else {
                    raf = proxyController.getMaxRafSupported();
                    Log.d(TAG, "Set proxyController for GSM Only to LTE");
                }
                rafs[index] = new RadioAccessFamily(index, raf);
            }
        } else if (mode == Phone.NT_MODE_WCDMA_ONLY ||
                        mode == Phone.NT_MODE_WCDMA_PREF) {
            for (int index = 0; index < simCount; index ++) {
                int subId = simList.get(index).getSubscriptionId();
                if (subId == phoneSubId) {
                    raf = RadioAccessFamily.RAF_UMTS;
                    Log.d(TAG, "Set proxyController to WCDMA");
                } else {
                    raf = proxyController.getMaxRafSupported();
                    Log.d(TAG, "Set proxyController to LTE");
                }
                rafs[index] = new RadioAccessFamily(index, raf);
            }
        } else {
            for (int index = 0; index < simCount; index ++) {
                int subId = simList.get(index).getSubscriptionId();
                if (subId == phoneSubId) {
                    raf = proxyController.getMaxRafSupported();
                    Log.d(TAG, "Set proxyController for LTE to LTE");
                } else {
                    raf = proxyController.getMinRafSupported();
                    Log.d(TAG, "Set proxyController for LTE to GSM");
                }
                rafs[index] = new RadioAccessFamily(index, raf);
            }
        }
        boolean result = proxyController.setRadioCapability(rafs);
        Log.d(TAG, "Result after setting ProxyController: " + result);
        return result;
    }

    private boolean isDeviceSingleSim() {
        int count = SubscriptionManager.from(mContext).getActiveSubscriptionInfoCount();
        Log.d(TAG, "Active Sub Info Count " + count);
        if (count == 1) {
            return true;
        }
        return false;
    }

    private CharSequence getWarningText(int mode, int subId) {
        if (isDeviceSingleSim()) {
            return mContext.getText(R.string.network_mode_update_warning_text);
        }
        int phoneId = SubscriptionManager.getPhoneId(subId);
        Log.d(TAG, "Show warning when mode change to: " + mode +
                " for phone id: " + phoneId);
        if (mode == Phone.NT_MODE_LTE_ONLY || mode == Phone.NT_MODE_LTE_GSM_WCDMA) {
            if (phoneId == 0) {
                return mContext.getText(R.string.network_mode_update_warning_text_sub2);
            } else {
                return mContext.getText(R.string.network_mode_update_warning_text_sub1);
            }
        } else {
            if (phoneId == 0) {
                return mContext.getText(R.string.network_mode_update_warning_text_sub1);
            } else {
                return mContext.getText(R.string.network_mode_update_warning_text_sub2);
            }
        }
    }
}
