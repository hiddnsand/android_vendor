/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/MetadataProvider.constructStatic"
//
#include "MyUtils.h"
//
#include <dlfcn.h>
//
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/aaa/IDngInfo.h>
#include <mtkcam/utils/metadata/IMetadataConverter.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <cutils/properties.h>


/******************************************************************************
 *
 ******************************************************************************/

//#define MY_LOGI(fmt, arg...)    do {XLOGI(LOG_TAG fmt, ##arg);printf(LOG_TAG fmt "\n", ##arg);} while (0)
//#define MY_LOGD(fmt, arg...)    do {XLOGD(LOG_TAG fmt, ##arg);printf(LOG_TAG fmt "\n", ##arg);} while (0)
//#define MY_LOGE(fmt, arg...)    do {XLOGE("error" LOG_TAG fmt, ##arg);printf(LOG_TAG fmt "\n", ##arg);} while (0)
#define FUNCTION_LOG_START      MY_LOGD("[%s] - E.", __FUNCTION__)
#define FUNCTION_LOG_END        do {if(!ret) MY_LOGE("[%s] fail", __FUNCTION__); MY_LOGD("[%s] - X. ret=%d", __FUNCTION__, ret);} while(0)
#define FUNCTION_LOG_END_MUM    MY_LOGD("[%s] - X.", __FUNCTION__)

#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)

#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)

//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

/******************************************************************************
 *
 ******************************************************************************/

status_t
MetadataProvider::
impConstructStaticMetadata_by_SymbolName(
    String8 const&      s8Symbol,
    IMetadata &metadata
)
{
typedef status_t (*PFN_T)(
        IMetadata &         metadata,
        Info const&         info
    );
    //
    PFN_T pfn = (PFN_T)::dlsym(RTLD_DEFAULT, s8Symbol.string());
    if  ( ! pfn ) {
        MY_LOGW("%s not found", s8Symbol.string());
        return  NAME_NOT_FOUND;
    }
    //
    status_t const status = pfn(metadata, mInfo);
    MY_LOGW_IF(OK != status, "%s returns status[%s(%d)]", s8Symbol.string(), ::strerror(-status), -status);
    //
    return  status;
}


/******************************************************************************
 *
 ******************************************************************************/
status_t
MetadataProvider::
impConstructStaticMetadata(
    IMetadata &metadata
)
{
    for (int i = 0; NULL != kStaticMetadataTypeNames[i]; i++)
    {
        char const*const pTypeName = kStaticMetadataTypeNames[i];

        status_t status = OK;
        //
        String8 const s8Symbol_Sensor = String8::format("%s_DEVICE_%s_%s", PREFIX_FUNCTION_STATIC_METADATA, pTypeName, mInfo.getSensorDrvName());
        status = impConstructStaticMetadata_by_SymbolName(s8Symbol_Sensor, metadata);
        if  ( OK == status ) {
            continue;
        }
        //
        String8 const s8Symbol_Common = String8::format("%s_DEVICE_%s_%s", PREFIX_FUNCTION_STATIC_METADATA, pTypeName, "COMMON");
        status = impConstructStaticMetadata_by_SymbolName(s8Symbol_Common, metadata);
        if  ( OK == status ) {
            continue;
        }
        //
        MY_LOGE_IF(0, "Fail for both %s & %s", s8Symbol_Sensor.string(), s8Symbol_Common.string());
    }
    //
    for (int i = 0; NULL != kStaticMetadataTypeNames[i]; i++)
    {
        char const*const pTypeName = kStaticMetadataTypeNames[i];

        status_t status = OK;
        //
        String8 const s8Symbol_Sensor = String8::format("%s_PROJECT_%s_%s", PREFIX_FUNCTION_STATIC_METADATA, pTypeName, mInfo.getSensorDrvName());
        status = impConstructStaticMetadata_by_SymbolName(s8Symbol_Sensor, metadata);
        if  ( OK == status ) {
            continue;
        }
        //
        String8 const s8Symbol_Common = String8::format("%s_PROJECT_%s_%s", PREFIX_FUNCTION_STATIC_METADATA, pTypeName, "COMMON");
        status = impConstructStaticMetadata_by_SymbolName(s8Symbol_Common, metadata);
        if  ( OK == status ) {
            continue;
        }
        //
        MY_LOGE_IF(0, "Fail for both %s & %s", s8Symbol_Sensor.string(), s8Symbol_Common.string());
    }
    //
    return  OK;
}


/******************************************************************************
 *
 ******************************************************************************/
status_t
MetadataProvider::
constructStaticMetadata(sp<IMetadataConverter> pConverter, camera_metadata*& rpDstMetadata, IMetadata& mtkMetadata)
{
    MY_LOGD("construct static metadata\n");

    status_t status = OK;

    //-----(1)-----//
    //get static informtation from customization (with camera_metadata format)
    //calculate its entry count and data count
    if  ( OK != (status = impConstructStaticMetadata(mtkMetadata)) ) {
        MY_LOGE("Unable evaluate the size for camera static info - status[%s(%d)]\n", ::strerror(-status), -status);
        return  status;
    }
    MY_LOGD("Allocating %d entries from customization", mtkMetadata.count());


    //-----(2.1)------//
    //get static informtation from sensor hal moduls (with IMetadata format)
    IMetadata sensorMetadata = MAKE_HalSensorList()->queryStaticInfo(mInfo.getDeviceId());
    MY_LOGD("Allocating %d entries from sensor HAL", sensorMetadata.count());

    //
#if 1
    IMetadata rDngMeta = MAKE_DngInfo(LOG_TAG, mInfo.getDeviceId())->getStaticMetadata();
#else
    IMetadata rDngMeta;
#endif
    MY_LOGD("Allocating %d entries from Dng Info", rDngMeta.count());

    //--- (2.1.1) --- //
    //merge.
    sensorMetadata += rDngMeta;
    for (size_t i = 0; i < sensorMetadata.count(); i++)
    {
        IMetadata::Tag_t mTag = sensorMetadata.entryAt(i).tag();
        mtkMetadata.update(mTag, sensorMetadata.entryAt(i));
    }
    MY_LOGD("Allocating %d entries from customization + sensor HAL + Dng Info", mtkMetadata.count());

#if 0
    //-----(2.2)------//
    //get static informtation from other hal moduls (with IMetadata format)
    IMetadata halmetadata = MAKE_HalSensorList()->queryStaticInfo(mInfo.getDeviceId());

    //calculate its entry count and data count
    entryCount = 0;
    dataCount = 0;


    status = AndroidMetadata::getIMetadata_dataCount(halmetadata, entryCount, dataCount);
    if (status != OK)
    {
        MY_LOGE("get Imetadata count error - status[%s(%d)", ::strerror(-status), -status);
        return status;
    }

    MY_LOGD(
        "Allocating %d entries, %d extra bytes from HAL modules",
        entryCount, dataCount
    );

    addOrSizeInfo.mEntryCount += entryCount;
    addOrSizeInfo.mDataCount += dataCount;

#endif

    //overwrite
    updateData(mtkMetadata);
    //
    #if (PLATFORM_SDK_VERSION >= 21)
    pConverter->convert(mtkMetadata, rpDstMetadata);
    //
    ::sort_camera_metadata(rpDstMetadata);
    #endif

    return  status;
}


/******************************************************************************
 *
 ******************************************************************************/
template<class T>
struct converter {
    converter( T const& tag, T const& srcFormat, T const& dstFormat, IMetadata& data) {
        IMetadata::IEntry entry = data.entryFor(tag);
        copy( srcFormat, dstFormat, entry);
        data.update(tag, entry);
    }

    void copy( T const& srcFormat, T const& dstFormat, IMetadata::IEntry& entry) {
        T input = MTK_SCALER_AVAILABLE_STREAM_CONFIGURATIONS_INPUT;
        for(size_t i = 0; i < entry.count(); i+=4) {
            if (entry.itemAt(i, Type2Type<T>())!= srcFormat
                || entry.itemAt(i+3, Type2Type< T >()) == input) {
                continue;
            }
            entry.push_back(dstFormat, Type2Type< T >());
            entry.push_back(entry.itemAt(i+1, Type2Type< T >()), Type2Type< T >());
            entry.push_back(entry.itemAt(i+2, Type2Type< T >()), Type2Type< T >());
            entry.push_back(entry.itemAt(i+3, Type2Type< T >()), Type2Type< T >());
        }
    };
};

void
MetadataProvider::
updateData(IMetadata &rMetadata)
{
    {
        MINT32 maxJpegsize = 0;
        IMetadata::IEntry blobEntry = rMetadata.entryFor(MTK_SCALER_AVAILABLE_STREAM_CONFIGURATIONS);
        for(size_t i = 0; i < blobEntry.count(); i+=4) {
            if (blobEntry.itemAt(i, Type2Type<MINT32>())!= HAL_PIXEL_FORMAT_BLOB) {
                continue;
            }
            //avaiblable blob size list should order in descedning.
            MSize maxBlob = MSize(blobEntry.itemAt(i+1, Type2Type<MINT32>()),
                            blobEntry.itemAt(i+2, Type2Type<MINT32>()));
            MINT32 jpegsize = maxBlob.size()*1.2; //*2*0.6
            if (jpegsize > maxJpegsize) {
                maxJpegsize = jpegsize;
            }
            IMetadata::IEntry entry(MTK_JPEG_MAX_SIZE);
            entry.push_back(maxJpegsize, Type2Type< MINT32 >());
            rMetadata.update(MTK_JPEG_MAX_SIZE, entry);
         }
    }

    // update implementation defined
    {
        converter<MINT32>(
            MTK_SCALER_AVAILABLE_STREAM_CONFIGURATIONS,
            HAL_PIXEL_FORMAT_YCbCr_420_888, HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED,
            rMetadata
        );
        //
        converter<MINT64>(
            MTK_SCALER_AVAILABLE_MIN_FRAME_DURATIONS,
            HAL_PIXEL_FORMAT_YCbCr_420_888, HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED,
            rMetadata
        );
        //
        converter<MINT64>(
            MTK_SCALER_AVAILABLE_STALL_DURATIONS,
            HAL_PIXEL_FORMAT_YCbCr_420_888, HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED,
            rMetadata
        );
    }

    // update yv12
    {
        converter<MINT32>(
            MTK_SCALER_AVAILABLE_STREAM_CONFIGURATIONS,
            HAL_PIXEL_FORMAT_YCbCr_420_888, HAL_PIXEL_FORMAT_YV12,
            rMetadata
        );
        //
        converter<MINT64>(
            MTK_SCALER_AVAILABLE_MIN_FRAME_DURATIONS,
            HAL_PIXEL_FORMAT_YCbCr_420_888, HAL_PIXEL_FORMAT_YV12,
            rMetadata
        );
        //
        converter<MINT64>(
            MTK_SCALER_AVAILABLE_STALL_DURATIONS,
            HAL_PIXEL_FORMAT_YCbCr_420_888, HAL_PIXEL_FORMAT_YV12,
            rMetadata
        );
    }

    // update HDR Request Common Type
    {
        IMetadata::IEntry availReqEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
        availReqEntry.push_back(MTK_HDR_FEATURE_HDR_MODE , Type2Type< MINT32 >());
        rMetadata.update(availReqEntry.tag(), availReqEntry);

        IMetadata::IEntry availResultEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_RESULT_KEYS);
        availResultEntry.push_back(MTK_HDR_FEATURE_HDR_DETECTION_RESULT , Type2Type< MINT32 >());
        rMetadata.update(availResultEntry.tag(), availResultEntry);

        IMetadata::IEntry availCharactsEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_CHARACTERISTICS_KEYS);
        availCharactsEntry.push_back(MTK_HDR_FEATURE_AVAILABLE_HDR_MODES_PHOTO , Type2Type< MINT32 >());
        availCharactsEntry.push_back(MTK_HDR_FEATURE_AVAILABLE_HDR_MODES_VIDEO , Type2Type< MINT32 >());
        rMetadata.update(availCharactsEntry.tag(), availCharactsEntry);
    }

    // update Available vHDR Mode & HDR Modes
    {

        // -----------------
        // -- isHDRSensor --
        // -----------------
        // Available vHDR mode
        IMetadata::IEntry availVhdrEntry = rMetadata.entryFor(MTK_HDR_FEATURE_AVAILABLE_VHDR_MODES);
        if(availVhdrEntry.isEmpty()){
            IMetadata::IEntry entry(MTK_HDR_FEATURE_AVAILABLE_VHDR_MODES);
            entry.push_back(MTK_HDR_FEATURE_VHDR_MODE_OFF, Type2Type< MINT32 >());
            rMetadata.update(entry.tag(), entry);
            availVhdrEntry = entry;
        }
        MBOOL isHDRSensor = isHdrSensor(availVhdrEntry.count());

        // --------------------------
        // -- isSingleFrameSupport --
        // --------------------------
        IMetadata::IEntry singleFrameHdrEntry = rMetadata.entryFor(MTK_HDR_FEATURE_AVAILABLE_SINGLE_FRAME_HDR);
        MBOOL isSingleFrameSupport = (singleFrameHdrEntry.count() > 0)
                    && (singleFrameHdrEntry.itemAt(0, Type2Type<MUINT8>()) == MTK_HDR_FEATURE_SINGLE_FRAME_HDR_SUPPORTED);
        MINT32 singleFrameProp = property_get_int32("debug.camera.hal3.singleFrame", -1);
        isSingleFrameSupport = (singleFrameProp != -1) ? (singleFrameProp > 0) : isSingleFrameSupport;

        // ----------------------
        // -- hdrDetectionMode --
        // ----------------------
        MINT32 hdrDetectionMode = MTKCAM_HDR_DETECTION_MODE; /* 1 : hdr sensor,  2 : generic sensor, 3 : all sensors*/
        // HDR Detection support force switch
        MINT32 hdrDetectProp = property_get_int32("debug.camera.hal3.hdrDetection", 0);
        hdrDetectionMode = (hdrDetectProp != -1) ? hdrDetectProp : hdrDetectionMode;


        // update availHdrPhoto & availHdrVideo Metadata
        updateHdrData(isHDRSensor, isSingleFrameSupport, hdrDetectionMode, rMetadata);

    }

    // update Available 3DNR Mode
    {
        IMetadata::IEntry avail3DNREntry = rMetadata.entryFor(MTK_NR_FEATURE_AVAILABLE_3DNR_MODES);
#ifndef NR3D_SUPPORTED
        avail3DNREntry.clear();
        avail3DNREntry.push_back(MTK_NR_FEATURE_3DNR_MODE_OFF, Type2Type< MINT32 >());
        rMetadata.update(avail3DNREntry.tag(), avail3DNREntry);
#endif
    }

#if MTKCAM_HAVE_MFB_SUPPORT
    // update AIS Request Common Type
    IMetadata::IEntry availAisModeEntry = rMetadata.entryFor(MTK_MFNR_FEATURE_AVAILABLE_AIS_MODES);
    if(availAisModeEntry.isEmpty()){
        IMetadata::IEntry availReqEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
        availReqEntry.push_back(MTK_MFNR_FEATURE_AIS_MODE , Type2Type< MINT32 >());
        rMetadata.update(availReqEntry.tag(), availReqEntry);

        IMetadata::IEntry availResultEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_RESULT_KEYS);
        availResultEntry.push_back(MTK_MFNR_FEATURE_AIS_RESULT , Type2Type< MINT32 >());
        rMetadata.update(availResultEntry.tag(), availResultEntry);

        IMetadata::IEntry entry(MTK_MFNR_FEATURE_AVAILABLE_AIS_MODES);
        entry.push_back(MTK_MFNR_FEATURE_AIS_OFF , Type2Type< MINT32 >());
        rMetadata.update(entry.tag(), entry);
    }
    // update MFB Request Common Type
    IMetadata::IEntry availMfbModeEntry = rMetadata.entryFor(MTK_MFNR_FEATURE_AVAILABLE_MFB_MODES);
    if(availAisModeEntry.isEmpty()){
        IMetadata::IEntry availReqEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
        availReqEntry.push_back(MTK_MFNR_FEATURE_MFB_MODE , Type2Type< MINT32 >());
        rMetadata.update(availReqEntry.tag(), availReqEntry);

        IMetadata::IEntry availResultEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_RESULT_KEYS);
        availResultEntry.push_back(MTK_MFNR_FEATURE_MFB_RESULT , Type2Type< MINT32 >());
        rMetadata.update(availResultEntry.tag(), availResultEntry);

        IMetadata::IEntry entry(MTK_MFNR_FEATURE_AVAILABLE_MFB_MODES);
        entry.push_back(MTK_MFNR_FEATURE_MFB_OFF , Type2Type< MINT32 >());
        rMetadata.update(entry.tag(), entry);
    }
#endif // MTKCAM_HAVE_MFB_SUPPORT

    // update Streaming Request Common Type
    {
        IMetadata::IEntry availReqEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
        availReqEntry.push_back(MTK_STREAMING_FEATURE_RECORD_STATE , Type2Type< MINT32 >());
        rMetadata.update(availReqEntry.tag(), availReqEntry);

        IMetadata::IEntry availCharactsEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_CHARACTERISTICS_KEYS);
        availCharactsEntry.push_back(MTK_STREAMING_FEATURE_AVAILABLE_RECORD_STATES , Type2Type< MINT32 >());
        rMetadata.update(availCharactsEntry.tag(), availCharactsEntry);
    }
    // update Streaming Available EIS ControlFlow
    {
        IMetadata::IEntry availRecordEntry = rMetadata.entryFor(MTK_STREAMING_FEATURE_AVAILABLE_RECORD_STATES);
        if(availRecordEntry.isEmpty()){
            IMetadata::IEntry entry(MTK_STREAMING_FEATURE_AVAILABLE_RECORD_STATES);
            entry.push_back(MTK_STREAMING_FEATURE_RECORD_STATE_PREVIEW, Type2Type< MINT32 >());
            entry.push_back(MTK_STREAMING_FEATURE_RECORD_STATE_RECORD, Type2Type< MINT32 >());
            rMetadata.update(entry.tag(), entry);
        }
    }

}

MBOOL
MetadataProvider::
isHdrSensor(MUINT const availVhdrEntryCount)
{
    MBOOL isHDRSensor = (availVhdrEntryCount > 1);
    char strVhdrLog[100];
    memset(strVhdrLog, '\0', sizeof(strVhdrLog));

    //query sensor static info from sensor driver to decide Hal3 vHDR support
    NSCam::IHalSensorList *pSensorHalList = NULL;
    pSensorHalList = MAKE_HalSensorList();
    if(pSensorHalList == NULL)
    {
        MY_LOGE("pSensorHalList::get fail");
    } else {
        MUINT32 sensorDev = (MUINT32)pSensorHalList->querySensorDevIdx(mInfo.getDeviceId());
        NSCam::SensorStaticInfo sensorStaticInfo;
        pSensorHalList->querySensorStaticInfo(sensorDev, &sensorStaticInfo);
        isHDRSensor = (sensorStaticInfo.HDR_Support > 0) ? isHDRSensor : false;
        sprintf(strVhdrLog, " sensorDev:%d, sensorStaticInfo.HDR_Support:%d,",
            sensorDev, sensorStaticInfo.HDR_Support);
    }

    //force set ON/OFF Hal3 vHDR support
    MINT32 vhdrHal3Prop = property_get_int32("debug.camera.hal3.vhdrSupport", -1);
    isHDRSensor = (vhdrHal3Prop != -1) ? (vhdrHal3Prop > 0) : isHDRSensor;
    MY_LOGD("isHDRSensor:%d, vhdrHal3Prop:%d,%s availVhdrEntry.count():%d",
        isHDRSensor, vhdrHal3Prop, strVhdrLog, availVhdrEntryCount);

    return isHDRSensor;
}

MVOID
MetadataProvider::
updateHdrData(MBOOL const isHDRSensor, MBOOL const isSingleFrameSupport, MINT32 const hdrDetectionMode,
    IMetadata &rMetadata)
{
    // Available HDR modes for Photo & Video
    IMetadata::IEntry availHdrPhotoEntry(MTK_HDR_FEATURE_AVAILABLE_HDR_MODES_PHOTO);
    IMetadata::IEntry availHdrVideoEntry(MTK_HDR_FEATURE_AVAILABLE_HDR_MODES_VIDEO);

    // --- MODE_OFF ----
    availHdrPhotoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_OFF, Type2Type< MINT32 >());
    availHdrVideoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_OFF, Type2Type< MINT32 >());

#if (MTKCAM_HAVE_HDR_SUPPORT == 1)

    // --- MODE_ON ----
    availHdrPhotoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_ON, Type2Type< MINT32 >());
    /* Video mode not support MODE_ON*/

    // --- MODE_AUTO ----
    if (hdrDetectionMode == 3
        || (hdrDetectionMode == 2 && !isHDRSensor)
        || (hdrDetectionMode == 1 && isHDRSensor))
    {
        availHdrPhotoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_AUTO, Type2Type< MINT32 >());
    }
    /* Video mode not support MODE_AUTO*/

    // --- MODE_VIDEO_ON ----
    if(isHDRSensor)
    {
        availHdrVideoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_VIDEO_ON, Type2Type< MINT32 >());
        if(isSingleFrameSupport)
            availHdrPhotoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_VIDEO_ON, Type2Type< MINT32 >());
    }

     // --- MODE_VIDEO_AUTO ----
    if(isHDRSensor && (hdrDetectionMode == 1 || hdrDetectionMode == 3))
    {
        availHdrVideoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_VIDEO_AUTO, Type2Type< MINT32 >());
        if(isSingleFrameSupport)
            availHdrPhotoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_VIDEO_AUTO, Type2Type< MINT32 >());
    }

#endif// MTKCAM_HAVE_HDR_SUPPORT endif

    rMetadata.update(availHdrPhotoEntry.tag(), availHdrPhotoEntry);
    rMetadata.update(availHdrVideoEntry.tag(), availHdrVideoEntry);
}

