package com.mediatek.mms.ipmessage;

import android.content.Context;

import com.mediatek.common.util.OperatorCustomizationFactoryLoader;
import com.mediatek.common.util.OperatorCustomizationFactoryLoader.OperatorFactoryInfo;

import java.util.ArrayList;
import java.util.List;

public class IpMmsCustomizationUtils {

    // list every operator's factory path and name.
    private static final List<OperatorFactoryInfo> sOperatorFactoryInfoList
            = new ArrayList<OperatorFactoryInfo>();

    static IpMmsCustomizationFactoryBase sFactory = null;

    static {
        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP03Mms.apk",
                         "com.mediatek.op03.mms.OP03MmsCustomizationFactory",
                         "com.mediatek.op03.mms",
                         "OP03",
                         "SEGDEFAULT"
                        ));
        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP18Mms.apk",
                         "com.mediatek.op18.mms.OP03MmsCustomizationFactory",
                         "com.mediatek.op18.mms",
                         "OP03",
                         "SEGDEFAULT"
                        ));
    }

    public static synchronized IpMmsCustomizationFactoryBase getOpFactory(Context context) {
        if (sFactory == null) {
            sFactory = (IpMmsCustomizationFactoryBase) OperatorCustomizationFactoryLoader
                        .loadFactory(context, sOperatorFactoryInfoList);
            if (sFactory == null) {
                sFactory = new IpMmsCustomizationFactoryBase(context);
            }
        }
        return sFactory;
    }
}
