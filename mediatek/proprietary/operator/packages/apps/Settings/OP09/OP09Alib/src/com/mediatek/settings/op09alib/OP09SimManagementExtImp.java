/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.content.Context;
import android.os.SystemProperties;
import android.support.v7.preference.Preference;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.PhoneConstants;

import com.mediatek.internal.telephony.MtkDefaultSmsSimSettings;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.settings.ext.DefaultSimManagementExt;

import java.util.List;

/**
 * For settings SIM management feature.
 */
public class OP09SimManagementExtImp extends DefaultSimManagementExt {
    private static final String TAG = "OP09SimManagementExt";

    private static final String KEY_CELLULAR_DATA = "sim_cellular_data";
    private static final String KEY_SMS = "sim_sms";
    private static final String KEY_CALLS = "sim_calls";
    private Context mContext;
    private int mToCloseSlot = -1;
    private TelephonyManager mTelephonyManager;
    private ITelephony mITelephony;
    public static final int DATA_PICK = 0;
    public static final int CALLS_PICK = 1;
    public static final int SMS_PICK = 2;
    /**
     * update the preference screen of sim management.
     * @param context The context
     */
    public OP09SimManagementExtImp(Context context) {
        super();
        mContext = context;
    }

    /**
     * Called when set radio power state for a specific sub.
     * @param subId  the slot to set radio power state
     * @param turnOn  on or off
     */
    @Override
    public void setRadioPowerState(int subId, boolean turnOn) {
        int slotId = SubscriptionManager.getSlotIndex(subId);
        TelephonyManager telephonyManager = TelephonyManager.from(mContext);
        Log.d(TAG, "setRadioPowerState, slotId = " + slotId + " subId = " + subId +
              " turnOn = " + turnOn);
        if (telephonyManager.getPhoneCount() <= PhoneConstants.MAX_PHONE_COUNT_DUAL_SIM) {
            if (Utils.isAllSlotRadioOn(mContext) && (!turnOn)) {
                Utils.setToClosedSimSlot(mContext, slotId);
            } else if (Utils.isAllSlotRadioOff(mContext) && turnOn) {
                Utils.setToOpenSimSlot(mContext, slotId);
            }
        }
    }

    /**
     * CT A feature.
     * For delete "always ask" item in sim setting.
     * @param strings which is list include:
     * "always ask", "sim1", "sim2"
     */
    @Override
    public void customizeListArray(List<String> strings) {
        Log.i(TAG, "op09 customizeListArray");
        if (strings != null && strings.size() > 1) {
            strings.remove(0);
            Log.i(TAG, "op09 customizeListArray dothings");
        }
    }

    /**
     * CT A feature.
     * For delete "always ask" item in sim setting.
     * @param subscriptionInfo which is list include all sim subscriptionInfo
     */
    @Override
    public void customizeSubscriptionInfoArray(List<SubscriptionInfo> subscriptionInfo) {
        if (subscriptionInfo != null && subscriptionInfo.size() > 1) {
            subscriptionInfo.remove(0);
        }
    }

    /**
     * For CTA case, CT dismiss the google default sim choose dialog.
     * So CTA need call or send message , need QA can choose the card.
     * @param value value
     * @return value
     */
//    @Override
//    public int customizeValue(int value) {
//        if ((value == 0) && (SubscriptionManager.from(mContext)
//                .getActiveSubscriptionInfoCount() == 1) &&
//                TelephonyManager.getDefault().getPhoneCount() > 1) {
//            /// For single card, click item.
//            value = 1;
//            Log.d(TAG, "customizeValue, value = " + value);
//        } else {
//            /// For mulit card, call and sms value must be add 1 ---> for common flow
//            SubscriptionManager subscriptionManager = SubscriptionManager.from(mContext);
//            final List<SubscriptionInfo> subInfoList =
//                subscriptionManager.getActiveSubscriptionInfoList();
//            Log.i(TAG, "op09 customizeValue first value = " + value);
//            if (subInfoList != null && subInfoList.size() > 1) {
//                Log.i(TAG, "op09 customizeValue value add = " + (value + 1));
//                return value + 1;
//            }
//            Log.i(TAG, "op09 customizeValue value = " + value);
//        }
//        return value;
//
//    }

    @Override
    public void setDataState(int subid) {
        TelephonyManager mTelephonyManager;
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(mContext);
        mTelephonyManager = TelephonyManager.from(mContext);
        boolean enableBefore = mTelephonyManager.getDataEnabled();
        int mResetSubId = subscriptionManager.getDefaultDataSubscriptionId();
        Log.d(TAG, "setDataState: " + enableBefore + " , " + subid + " , " + mResetSubId);
        if (subscriptionManager.isValidSubscriptionId(subid) &&
            subid != mResetSubId) {
            subscriptionManager.setDefaultDataSubId(subid);
            if (enableBefore) {
                mTelephonyManager.setDataEnabled(subid, true);
                mTelephonyManager.setDataEnabled(mResetSubId, false);
            }
        }
    }


    @Override
    public SubscriptionInfo setDefaultSubId(Context context, SubscriptionInfo sir, String value) {
        //fragment is detach.
        if (context == null) {
            return sir;
        }
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        SubscriptionInfo sirTmp = sir;
        int type = 0;

        if (value.equals(KEY_CELLULAR_DATA)) {
            type = 2;
        } else if (value.equals(KEY_SMS)) {
            type = 1;
        }

        if (sir == null) {
            List<SubscriptionInfo> subList = subscriptionManager.getActiveSubscriptionInfoList();
            int subCount;
            if (subList == null) {
                subCount = 0;
            } else {
                subCount = subList.size();
            }
            int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
            if (subCount == 1) {
                subId = subList.get(0).getSubscriptionId();
                if (type == 2) {
                    //subscriptionManager.setDefaultDataSubId(subId);
                    //TelephonyManager.getDefault().setDataEnabled(subId, true);
                    Log.d(TAG, "setDefaultSubId subCount = 1, type = 2, data sub set to " + subId);
                } else if (type == 1) {
                    //subscriptionManager.setDefaultSmsSubId(subId);
                    Log.d(TAG, "setDefaultSubId subCount = 1, type = 1, sms sub set to " + subId);
                }
                sirTmp = Utils.findRecordBySubId(context, subId);
            } else if (subCount >= 2) {
                if (type == 2 && SubscriptionManager.getDefaultDataSubscriptionId() ==
                        SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
                    //subscriptionManager.setDefaultDataSubId(subId);
                    //TelephonyManager.getDefault().setDataEnabled(subId, true);
                    Log.d(TAG, "setDefaultSubId subCount = 2, type = 2, data sub set to " + subId);
                } else if (type == 1 && SubscriptionManager.getDefaultSmsSubscriptionId() ==
                        SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
                    subscriptionManager.setDefaultSmsSubId(subId);
                    Log.d(TAG, "setDefaultSubId subCount = 2, type = 1, sms sub set to " + subId);
                }
                sirTmp = Utils.findRecordBySubId(context, subId);
            }
        }
        return sirTmp;
    }

    @Override
    public PhoneAccountHandle setDefaultCallValue(PhoneAccountHandle phoneAccount) {
        final TelecomManager telecomManager = TelecomManager.from(mContext);
        PhoneAccountHandle result = phoneAccount;
        Log.d(TAG, "setDefaultCallValue phoneAccount=" + phoneAccount);
        if (phoneAccount == null) {
            List<PhoneAccountHandle> phoneAccountlist
                = telecomManager.getCallCapablePhoneAccounts();
            int accoutSum = phoneAccountlist.size();

            Log.d(TAG, "setDefaultCallValue accoutSum=" + accoutSum);
            if (accoutSum > 0) {
                 result = phoneAccountlist.get(0);
            }
        }
        Log.d(TAG, "setDefaultCallValue result=" + result);
        return result;
    }

    /**
     * Called when SIM dialog is about to show for SIM info changed.
     * @return false if plug-in do not need SIM dialog
     */
    @Override
    public boolean isSimDialogNeeded() {
        return false;
    }

    /**
     * Judge if it is CT card.
     * @return true if it is CT card, otherwise false.
     */
    @Override
    public boolean useCtTestcard() {
        return Utils.isCTLteTddTestSupport();
    }

    /**
     * For CTA case, CT dismiss the google default sim choose dialog.
     * So CTA need call or send message , need QA can choose the card.
     * @param simPref simPref
     * @param type type
     * @param size size
     */
    public void configSimPreferenceScreen(Preference simPref, String type, int size) {
        Log.d(TAG, "configSimPreferenceScreen type: " + type + ", size: " + size);
        int count = TelephonyManager.getDefault().getPhoneCount();
        Log.i(TAG, "isGeminiSupport phone count = " + count);
        if (count <= 1) {
            return;
        }
        switch (type) {
            case KEY_CALLS:
            case KEY_SMS:
                simPref.setEnabled(size >= 1);
                break;
            default:
                break;
        }
    }

    /**
     * simDialogOnClick.
     * @param id type of sim prefrence
     * @param value value of position selected
     * @param context context
     * @return handled by plugin or not
     */
    @Override
    public boolean simDialogOnClick(int id, int value, Context context) {
        boolean isNeedId = false;
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        final List<SubscriptionInfo> subInfoList =
            subscriptionManager.getActiveSubscriptionInfoList();
        switch (id) {
            case DATA_PICK:
                break;
            case CALLS_PICK:
                if ((value == 0) &&
                        (SubscriptionManager.from(mContext).getActiveSubscriptionInfoCount() == 1)
                            && TelephonyManager.getDefault().getPhoneCount() > 1) {
                    /// For single card, click item.
                    value = 1;
                    Log.d(TAG, "customizeValue, value = " + value);
                } else {
                    /// For mulit card, call and sms value must be add 1 ---> for common flow
                    Log.i(TAG, "op09 customizeValue first value = " + value);
                    if (subInfoList != null && subInfoList.size() > 1) {
                        Log.i(TAG, "op09 customizeValue value add = " + (value + 1));
                        value = value + 1;
                    }
                    Log.i(TAG, "op09 customizeValue value = " + value);
                }
                final TelecomManager telecomManager =
                        TelecomManager.from(context);
                final List<PhoneAccountHandle> phoneAccountsList =
                        telecomManager.getCallCapablePhoneAccounts();
                Log.d(TAG, "phoneAccountsList = " + phoneAccountsList.toString());
                /// M: for ALPS02320816 @{
                // phone account may changed in background
                if (value > phoneAccountsList.size()) {
                    Log.w(TAG, "phone account changed, do noting! value = " +
                            value + ", phone account size = " +
                            phoneAccountsList.size());
                    break;
                }
                /// @}
                Log.d(TAG, "value = " + value);
                setUserSelectedOutgoingPhoneAccount(value < 1 ?
                        null : phoneAccountsList.get(value - 1));
                isNeedId = true;
                break;
            case SMS_PICK:
                if ((value == 0) &&
                        (SubscriptionManager.from(mContext).getActiveSubscriptionInfoCount() == 1)
                            && TelephonyManager.getDefault().getPhoneCount() > 1) {
                    /// For single card, click item.
                    value = 1;
                    Log.d(TAG, "customizeValue, value = " + value);
                } else {
                    /// For mulit card, call and sms value must be add 1 ---> for common flow
                    Log.i(TAG, "op09 customizeValue first value = " + value);
                    if (subInfoList != null && subInfoList.size() > 1) {
                        Log.i(TAG, "op09 customizeValue value add = " + (value + 1));
                        value = value + 1;
                    }
                    Log.i(TAG, "op09 customizeValue value = " + value);
                }
                int subId = getPickSmsDefaultSub(subInfoList, value);
                setDefaultSmsSubId(context, subId);
                isNeedId = true;
                break;
            default:
                throw new IllegalArgumentException("Invalid dialog type "
                        + id + " in SIM dialog.");
        }
        return isNeedId;
    }

    private void setUserSelectedOutgoingPhoneAccount(PhoneAccountHandle phoneAccount) {
        Log.d(TAG, "setUserSelectedOutgoingPhoneAccount phoneAccount = " + phoneAccount);
        final TelecomManager telecomManager = TelecomManager.from(mContext);
        telecomManager.setUserSelectedOutgoingPhoneAccount(phoneAccount);
    }

    private int getPickSmsDefaultSub(final List<SubscriptionInfo> subInfoList,
            int value) {
        int subId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;

        if (value < 1) {
            int length = subInfoList == null ? 0 : subInfoList.size();
            if (length == 1) {
                subId = subInfoList.get(value).getSubscriptionId();
            } else {
                subId = MtkDefaultSmsSimSettings.ASK_USER_SUB_ID;
            }
        } else if (value >= 1 && value < subInfoList.size() + 1) {
            subId = subInfoList.get(value - 1).getSubscriptionId();
        }
        Log.d(TAG, "getPickSmsDefaultSub, value: " + value + ", subId: " + subId);
        return subId;
    }

    private static void setDefaultSmsSubId(final Context context, final int subId) {
        Log.d(TAG, "setDefaultSmsSubId, sub = " + subId);
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        subscriptionManager.setDefaultSmsSubId(subId);
    }

}
