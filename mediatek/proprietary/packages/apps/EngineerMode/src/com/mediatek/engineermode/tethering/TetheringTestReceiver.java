package com.mediatek.engineermode.tethering;

import static android.provider.Settings.Global.NETSTATS_GLOBAL_ALERT_BYTES;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.util.Log;


/**
 * Broadcast receiver to receive command to enable telephony log.
 *
 */
public class TetheringTestReceiver extends BroadcastReceiver {
    private static final String TAG = "TetheringTestReceiver";
    private static final String ACTION_ENABLE_NSIOT =
                        "com.mediatek.intent.action.ACTION_ENABLE_NSIOT_TESTING";
    private static final String ACTION_DISABLE_DATA_ALERT =
                        "com.mediatek.intent.action.IGNORE_DATA_USAGE_ALERT";
    private static final String ACTION_ENABLE_TETHERING =
                        "com.mediatek.intent.action.TETHERING_CHANGED";

    // Configure 1 TB
    private static final long DISABLE_DATA_ALERT_BYTES = 1024 * 1024 * 1024 * 1024 * 1024L;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        if (intent == null) {
            Log.e(TAG, "Intent is null");
            return;
        }
        boolean enabled = false;
        Log.i(TAG, "TetheringTestReceiver:" + intent.getAction());

        if (ACTION_ENABLE_NSIOT.equals(intent.getAction())) {
            enabled = intent.getBooleanExtra("nsiot_enabled", true);
            Log.i(TAG, "enabled:" + enabled);
            Intent i = new Intent(context, TetheringTestService.class);
            if (enabled) {
                context.startService(i);
            } else {
                context.stopService(i);
            }
        } else if (ACTION_DISABLE_DATA_ALERT.equals(intent.getAction())) {
            Thread thread = new Thread("IGNORE_DATA_USAGE_ALERT") {
                public void run() {
                    Log.i(TAG, "Disable data usage alert");
                    Settings.Global.putLong(context.getContentResolver(),
                            NETSTATS_GLOBAL_ALERT_BYTES,
                            DISABLE_DATA_ALERT_BYTES);
                }
            };
            thread.start();
        } else if (ACTION_ENABLE_TETHERING.equals(intent.getAction())) {
            enabled = intent.getBooleanExtra("tethering_isconnected", true);
            enableTethering(context, enabled);
        }
    }

    private void enableTethering(Context context, boolean enabled) {
        try {
            Log.i(TAG, "enableTethering:" + enabled);
            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
            if (enabled) {
                OnStartTetheringCallback callback = new OnStartTetheringCallback();
                connMgr.startTethering(
                    ConnectivityManager.TETHERING_USB, false, callback);
            } else {
                connMgr.stopTethering(ConnectivityManager.TETHERING_USB);
            }
        } catch (Exception e) {
            Log.e(TAG, "enableTethering:" + e);
        }
    }

    private final class OnStartTetheringCallback extends
            ConnectivityManager.OnStartTetheringCallback {
        @Override
        public void onTetheringStarted() {
        }

        @Override
        public void onTetheringFailed() {
        }
    }
}
