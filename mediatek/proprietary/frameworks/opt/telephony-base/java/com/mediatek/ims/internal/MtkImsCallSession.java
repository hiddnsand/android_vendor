/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims.internal;

import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.android.ims.ImsCallProfile;
import com.android.ims.ImsReasonInfo;
import com.android.ims.internal.IImsCallSession;
import com.android.ims.internal.IImsCallSessionListener;
import com.android.ims.internal.ImsCallSession;

import java.util.Objects;

import com.mediatek.ims.internal.op.OpImsCallSessionBase;
import com.mediatek.ims.internal.op.OpImsFwkBaseFactoryBase;

public class MtkImsCallSession extends ImsCallSession {
    private static final String TAG = "MtkImsCallSession";
    private final IMtkImsCallSession miMtkSession;
    private OpImsCallSessionBase mOpExt;

    public MtkImsCallSession(IMtkImsCallSession iSession) {
        miMtkSession = iSession;

        if (iSession == null) {
            mClosed = true;
            return;
        }

        try {
            miMtkSession.setListener(new IMtkImsCallSessionListenerProxy());
        } catch (RemoteException e) {
        }
        try {
            miSession = miMtkSession.getIImsCallSession();
        } catch (RemoteException e) {
        }

        if (miSession != null) {
            try {
                miSession.setListener(new IImsCallSessionListenerProxy());
            } catch (RemoteException e) {
            }
        } else {
            mClosed = true;
        }

        mOpExt = OpImsFwkBaseFactoryBase.getInstance().makeOpImsCallSession();
    }

    /**
     * Closes this object. This object is not usable after being closed.
     */
    public synchronized void close() {
        if (mClosed) {
            return;
        }

        try {
            miMtkSession.close();
            mClosed = true;
        } catch (RemoteException e) {
        }
    }

    /**
     * Sends a DTMF code. According to <a href="http://tools.ietf.org/html/rfc2833">RFC 2833</a>,
     * event 0 ~ 9 maps to decimal value 0 ~ 9, '*' to 10, '#' to 11, event 'A' ~ 'D' to 12 ~ 15,
     * and event flash to 16. Currently, event flash is not supported.
     *
     * @param c the DTMF to send. '0' ~ '9', 'A' ~ 'D', '*', '#' are valid inputs.
     */
    public void sendDtmf(char c, Message result) {
        if (mClosed) {
            return;
        }

        try {
            /// M: ALPS02321477 @{
            /// Google issue. Original sendDtmf could not pass Message.target to another process,
            /// because Message.writeToParcel didn't write target. Workaround this issue by adding
            /// a new API which passes target by Messenger.
            if (result != null && result.getTarget() != null) {
                Messenger target = new Messenger(result.getTarget());
                miMtkSession.sendDtmfbyTarget(c, result, target);
            } else {
                miSession.sendDtmf(c, result);
            }
            /// @}
        } catch (RemoteException e) {
        }
    }

    // For one-key conference MT displayed as incoming conference call.
    /**
     * Determines if the incoming session is multiparty.
     *
     * @return {@code True} if the incoming session is multiparty.
     * @hide
     */
    public boolean isIncomingCallMultiparty() {
        if (mClosed) {
            return false;
        }

        try {
            return miMtkSession.isIncomingCallMultiparty();
        } catch (RemoteException e) {
            return false;
        }
    }

    /**
     * Transfers the active & hold call.
     */
    public void explicitCallTransfer() {
        if (mClosed) {
            return;
        }

        try {
            miMtkSession.explicitCallTransfer();
        } catch (RemoteException e) {
            Log.e(TAG, "explicitCallTransfer: RemoteException!");
        }
    }

    /**
     * Transfers the active to specific number.
     *
     * @param number The transfer target number.
     * @param type ECT type
     */
    public void unattendedCallTransfer(String number, int type) {
        if (mClosed) {
            return;
        }

        try {
            miMtkSession.unattendedCallTransfer(number, type);
        } catch (RemoteException e) {
            Log.e(TAG, "explicitCallTransfer: RemoteException!");
        }
    }

    /**
     * A listener type for receiving notification on IMS call session events.
     * When an event is generated for an {@link IImsCallSession},
     * the application is notified by having one of the methods called on
     * the {@link IImsCallSessionListener}.
     */
    public class IMtkImsCallSessionListenerProxy extends IMtkImsCallSessionListener.Stub {
        @Override
        public void callSessionTransferred(IMtkImsCallSession session) {
            mListener.callSessionTransferred(MtkImsCallSession.this);
        }

        @Override
        public void callSessionTransferFailed(IMtkImsCallSession session,
                ImsReasonInfo reasonInfo) {
            mListener.callSessionTransferFailed(MtkImsCallSession.this, reasonInfo);
        }

        @Override
        public void callSessionTextCapabilityChanged(IMtkImsCallSession session,
                int localCapability, int remoteCapability,
                int localTextStatus, int realRemoteCapability) {
            mOpExt.callSessionTextCapabilityChanged(mListener, MtkImsCallSession.this,
                    localCapability, remoteCapability, localTextStatus, realRemoteCapability);
        }

        /**
         * Notifies the start of a call merge operation.
         *
         * @param session The call session.
         * @param newSession The merged call session.
         * @param profile The call profile.
         */
        @Override
        public void callSessionMergeStarted(IMtkImsCallSession session,
                                            IMtkImsCallSession newSession, ImsCallProfile profile) {
            // This callback can be used for future use to add additional
            // functionality that may be needed between conference start and complete
            Log.d(TAG, "callSessionMergeStarted");
        }

        /**
         * Notifies the successful completion of a call merge operation.
         *
         * @param newSession The call session.
         */
        @Override
        public void callSessionMergeComplete(IMtkImsCallSession newSession) {
            if (mListener != null) {
                if (newSession != null) {
                    // Check if the active session is the same session that was
                    // active before the merge request was sent.
                    ImsCallSession validActiveSession = MtkImsCallSession.this;
                    try {
                        if (!Objects.equals(miSession.getCallId(), newSession.getCallId())) {
                            // New session created after conference
                            validActiveSession = new MtkImsCallSession(newSession);
                        }
                    } catch (RemoteException rex) {
                        Log.e(TAG, "callSessionMergeComplete: exception for getCallId!");
                    }
                    mListener.callSessionMergeComplete(validActiveSession);
                } else {
                    // Session already exists. Hence no need to pass
                    mListener.callSessionMergeComplete(null);
                }
            }
        }
    }
}
