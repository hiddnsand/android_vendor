LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

# Only build program binaries when USE_OPENGL_RENDERER is
# defined in the current device/board configuration
ifeq ($(USE_OPENGL_RENDERER),true)
  LOCAL_SRC_FILES:= \
    main_programbuilder.cpp \
    BuildProgram.cpp

  intermediates := $(call intermediates-dir-for,STATIC_LIBRARIES,TARGET,)

  LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../core \
    system/core/include \
    frameworks/base/libs/hwui \
    external/skia/include/core \
    external/skia/src/core \

  LOCAL_CFLAGS := -DUSE_OPENGL_RENDERER -DEGL_EGLEXT_PROTOTYPES -DGL_GLEXT_PROTOTYPES
  LOCAL_SHARED_LIBRARIES := libutils libbinder liblog libprogrambinary libEGL libGLESv2 libcutils libskia libhwui
  LOCAL_MODULE := program_binary_builder
LOCAL_MODULE_OWNER := mtk
  LOCAL_MODULE_TAGS := optional

  # Defaults for ATRACE_TAG and LOG_TAG for programbinary
  LOCAL_CFLAGS += -DLOG_TAG=\"ProgramBinary\/Builder\"

  include $(BUILD_EXECUTABLE)
endif
