#include <inttypes.h>

#include <cutils/memory.h>

#include <utils/Log.h>

#include <binder/IPCThreadState.h>
#include <binder/ProcessState.h>
#include <binder/IServiceManager.h>

#include <dfps/FpsPolicy.h>
#include <dfps/FpsInfo.h>

#define __STDC_FORMAT_MACROS 1

#define ISW_MIN_FPS 20
#define ISW_MAX_FPS 60

using namespace android;

int main(int /*argc*/, char** /*argv*/)
{
    sp<ProcessState> ps(ProcessState::self());
    ps->startThreadPool();

    String8 string("");
    string.appendFormat("isw-switch-test");
    sp<FpsPolicy> policy = new FpsPolicy(FpsPolicy::API_THERMAL, string);
    int number;
    FpsRange range;
    policy->getPanelInfoNumber(&number);
    printf("total fps: %d\n", number);
    for (int i = 0; i < number; i++) {
        policy->getFpsRange(i, &range);
        printf("+ panel fps: [%d] %d~%d\n", i, range.min, range.max);
    }
    srand(0);

    int fps_mode = 0;
    printf("[ISW] fps switch mode:\n");
    printf("0: random fps\n");
    printf("1: constant fps(%d,%d)\n", ISW_MIN_FPS, ISW_MAX_FPS);
    scanf("%d", &fps_mode);

    int sleep_time = 0;
    printf("interval between switch fps?(ms)\n");
    scanf("%d", &sleep_time);

    while (true) {
        switch (fps_mode) {
            case 0:
            {
                int diff = ISW_MAX_FPS - ISW_MIN_FPS + 1;
                int fps = (rand() % diff) + ISW_MIN_FPS;
                policy->setFps(fps, FpsPolicy::MODE_INTERNAL_SW);
                break;
            }

            case 1:
            {
                static int count = 0;
                if (count % 2) {
                    policy->setFps(ISW_MAX_FPS, FpsPolicy::MODE_INTERNAL_SW);
                } else {
                    policy->setFps(ISW_MIN_FPS, FpsPolicy::MODE_INTERNAL_SW);
                }
                count++;
                break;
            }

            default:
                break;
        }
        for (int i = 0; i < sleep_time; i++) {
            usleep(1000);
        }
    }

    int i = 0;
    printf("press any key to release client\n");
    scanf("%d", &i);

    policy->cancelFpsRequestListener();
    policy = NULL;

    printf("press any key to exit\n");
    scanf("%d", &i);

    return 0;
}
