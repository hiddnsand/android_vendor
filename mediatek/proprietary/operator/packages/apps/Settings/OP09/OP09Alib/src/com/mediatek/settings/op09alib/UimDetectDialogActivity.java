/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

/**
 * CT feature: Show UIM detect dialog:
 * 1) Insert 4G card suggestion 2) No UIM card suggestion.
 */
public class UimDetectDialogActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "UimDetectDialogActivity";
    private String mTextMsg;
    private TextView mMessageView;
    private String mSharePreferencesName = "SimDialogServiceSharePreferences";
    private SharedPreferences mSharedPreferences;
    private String mSimtypeKey = "SIMTYPEKEY";
    private String mUserpresentKey = "USERPRESENTKEY";
    private String mTextKey = "TEXTKEY";
    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        mSharedPreferences = this.createDeviceProtectedStorageContext()
            .getSharedPreferences(mSharePreferencesName, Context.MODE_PRIVATE);
        mSharedPreferences.edit().putBoolean(mSimtypeKey, false).commit();
        mSharedPreferences.edit().putString(mTextKey, null).commit();
        initFromIntent(getIntent());
        if (mTextMsg == null) {
            finish();
            return;
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        setContentView(R.layout.uim_detect_dlg);
        mMessageView = (TextView) window.findViewById(R.id.dialog_message);

        Button okButton = (Button) findViewById(R.id.button_ok);
        okButton.setOnClickListener(this);
        setFinishOnTouchOutside(false);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initFromIntent(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMessageView.setText(mTextMsg);
        Log.d(TAG, "UimDetectDialogActivity showed onResume");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(SimDialogService.TEXT, mTextMsg);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mTextMsg = savedInstanceState.getString(SimDialogService.TEXT);
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "User onClick");
        Intent intent = new Intent(SimDialogService.ACTION_START_SELF);
        this.sendBroadcast(intent);
        Log.d(TAG, "send ACTION_START_SELF broadcast");
        finish();
    }

    private void initFromIntent(Intent intent) {

        if (intent != null) {
            mTextMsg = intent.getStringExtra(SimDialogService.TEXT);
            Log.d(TAG, "initFromIntent mTextMsg=" + mTextMsg);
        } else {
            finish();
        }
    }

}
