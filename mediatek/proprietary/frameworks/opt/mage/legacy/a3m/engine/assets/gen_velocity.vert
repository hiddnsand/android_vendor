/**************************************************************************
 *
 * Copyright (c) 2013 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Vertex shader used for generation of velocity map
 */


precision mediump float;

/* These should match the maximum values in the renderer */
#define MAX_JOINTS 20


/* Transformation uniforms */
uniform highp mat4 u_t_viewProjection;      // World to projection space transform
uniform highp mat4 u_t_modelViewProjection; // Model to projection space transform
uniform highp mat4 u_t_oldModelViewProjection; // Old model to projection space transform

uniform float u_blurFactor;
uniform float u_deltaTime;

/* Joint uniforms */
uniform int u_j_count;                      // Number of joints per vertex
#ifdef SKIN
uniform highp mat4 u_j_world[MAX_JOINTS];   // Joint world space transforms
#endif


/* Vertex attributes */
attribute vec4 a_position;    // Position in model space

attribute float a_jointIndex0;   // Index of attached joint
attribute float a_jointWeight0;  // Attachment weight of joint
attribute float a_jointIndex1;   // Index of attached joint
attribute float a_jointWeight1;  // Attachment weight of joint
attribute float a_jointIndex2;   // Index of attached joint
attribute float a_jointWeight2;  // Attachment weight of joint
attribute float a_jointIndex3;   // Index of attached joint
attribute float a_jointWeight3;  // Attachment weight of joint


varying vec2 v_velocity;

// This value determines the maximum fraction of the screen the motion blur can
// cover. A value of 4.0 means that a single pixel will not be smeared over
// more than 1/4 of the screen (a value of 8.0 would mean 1/8). To prevent
// obvious artifacts due to quantization inaccuracies for small velocities, it
// should NOT be set to a value smaller than 4.0.
//
// IF YOU CHANGE THIS VALUE YOU MUST CHANGE THE CORRESPONDING VALUE IN
// motionblur.frag
#define GEN_VELOCITY_SCALE 4.0

void main()
{

  /* The skinning calculations performed here are duplicated in
   * max_pvl.vert, max_ppl.vert and gen_velocity.vert
   * Any changes made to one file should be made to all files.
   */

#ifdef SKIN
  // Perform a weighted sum of each joint transformation
  if (u_j_count > 0)
  {
    highp mat4 worldTransform = a_jointWeight0 * u_j_world[int(a_jointIndex0)];

    if (u_j_count > 1 && a_jointWeight1 > 0.0)
    {
      worldTransform += a_jointWeight1 * u_j_world[int(a_jointIndex1)];

      if (u_j_count > 2 && a_jointWeight2 > 0.0)
      {
        worldTransform += a_jointWeight2 * u_j_world[int(a_jointIndex2)];

        if (u_j_count > 3 && a_jointWeight3 > 0.0)
        {
          worldTransform += a_jointWeight3 * u_j_world[int(a_jointIndex3)];
        }
      }
    }

    // Apply skinning transformation to vertex data
    vec4 vertexPosition = worldTransform * a_position;
    gl_Position = u_t_viewProjection * vertexPosition;
  }
  else
#endif
  {
    // Final position is model position multiplied by model view projection matrix
    gl_Position = u_t_modelViewProjection * a_position;

  }
  highp vec4 lastPos = u_t_oldModelViewProjection * a_position;

  v_velocity = gl_Position.xy / gl_Position.w - lastPos.xy / lastPos.w ;

  v_velocity = v_velocity * ( u_deltaTime * u_blurFactor * GEN_VELOCITY_SCALE ) + 0.5;

}
