#define DEBUG_LOG_TAG "DBQ"

#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>

#include <cutils/properties.h>

#include "utils/debug.h"
#include "utils/tools.h"
#include "overlay.h"
#include "queue.h"
#include "hwc.h"
#include "hwdev.h"
#include "sync.h"
#include "platform.h"

// Macros for including the BufferQueue name in log messages
#define QLOGV(x, ...) HWC_LOGV("(%s:%p) " x, m_client_name.string(), this, ##__VA_ARGS__)
#define QLOGD(x, ...) HWC_LOGD("(%s:%p) " x, m_client_name.string(), this, ##__VA_ARGS__)
#define QLOGI(x, ...) HWC_LOGI("(%s:%p) " x, m_client_name.string(), this, ##__VA_ARGS__)
#define QLOGW(x, ...) HWC_LOGW("(%s:%p) " x, m_client_name.string(), this, ##__VA_ARGS__)
#define QLOGE(x, ...) HWC_LOGE("(%s:%p) " x, m_client_name.string(), this, ##__VA_ARGS__)

//#define QUEUE_DEBUG
#ifdef QUEUE_DEBUG
#define DBG_LOGD(x, ...) HWC_LOGD("(%s:%p) " x, m_client_name.string(), this, ##__VA_ARGS__)
#define DBG_LOGW(x, ...) HWC_LOGW("(%s:%p) " x, m_client_name.string(), this, ##__VA_ARGS__)
#else
#define DBG_LOGD(x, ...)
#define DBG_LOGW(x, ...)
#endif

#define HWC_ATRACE_BUFFER_INDEX(string, index)                                \
    if (ATRACE_ENABLED()) {                                                   \
        char ___traceBuf[1024];                                               \
        snprintf(___traceBuf, 1024, "%s: %d", (string), (index));             \
        android::ScopedTrace ___bufTracer(ATRACE_TAG, ___traceBuf);           \
    }

// ---------------------------------------------------------------------------

DisplayBufferQueue::DisplayBufferQueue(int type)
    : m_queue_type(type)
    , m_is_synchronous(true)
    , m_frame_counter(0)
    , m_last_acquire_idx(INVALID_BUFFER_SLOT)
    , m_listener(NULL)
{
    // 20120818: read board prop to check if need to disable triple buffer
    char value[PROPERTY_VALUE_MAX];
    property_get("ro.sf.triplebuf.disable", value, "0");
    m_buffer_count = (atoi(value)) ? (NUM_BUFFER_SLOTS - 1) : NUM_BUFFER_SLOTS;

    if (m_queue_type <= QUEUE_TYPE_NONE || m_queue_type > QUEUE_TYPE_OVL)
    {
        QLOGE("Initialzie with invalied Type (%d)", m_queue_type);
        m_queue_type = QUEUE_TYPE_NONE;

        m_client_name = String8::format("noinit");
    }
    else
    {
#ifdef MTK_USER_BUILD
        m_client_name = String8::format("%s", (m_queue_type == QUEUE_TYPE_BLT) ?
                                                "q2" : "q1");
#else
        m_client_name = String8::format("%s", (m_queue_type == QUEUE_TYPE_BLT) ?
                                                "blt" : "ovl");
#endif
        QLOGI("Buffer queue is created with size(%d)", m_buffer_count);
    }
}

DisplayBufferQueue::~DisplayBufferQueue()
{
    QLOGI("Buffer queue is destroyed");

    int size = m_buffer_param.size;
    for (int i = 0; i < m_buffer_count; i++)
    {
        BufferSlot* slot = &m_slots[i];

        if (NULL == slot->data) continue;

        if (slot->release_fence != -1)
        {
            sp<SyncFence> fence(new SyncFence(slot->disp_id));
            fence->wait(slot->release_fence, 1000, DEBUG_LOG_TAG);
        }

        if ((1 == HWCMediator::getInstance().m_features.svp) && m_buffer_param.secure)
        {
            TEEMemDevice::getInstance().free((unsigned int)slot->data);
        }
        else
        {
            QLOGI("Free Slot(%d), %u -> 0", i, slot->data_size);

            const int extra_bytes = (DevicePlatform::m_config.platform & OVL_GEN_6582) ?
                128 + DevicePlatform::m_config.shift_bytes : 0;
            MMUDevice::getInstance().unmap(DevicePlatform::m_config.client_id,
                (unsigned int)slot->data, slot->data_size + extra_bytes, slot->mva);

            free(slot->data);
            slot->data = NULL;
            slot->mva = 0;
            slot->data_size = 0;
        }
    }

    m_listener = NULL;
}

status_t DisplayBufferQueue::setBufferParam(BufferParam& param)
{
    AutoMutex l(m_mutex);
    m_buffer_param = param;
    return NO_ERROR;
}

status_t DisplayBufferQueue::reallocate(int idx)
{
    HWC_ATRACE_CALL();

    BufferSlot* slot = &m_slots[idx];

    slot->data_pitch = m_buffer_param.pitch;
    slot->data_format = m_buffer_param.format;

    if (slot->data_size == m_buffer_param.size) return NO_ERROR;

    QLOGI("Reallocate Slot(%d), %d -> %d",
          idx, slot->data_size, m_buffer_param.size);

    // release old buffer
    const bool is_svp_support = (1 == HWCMediator::getInstance().m_features.svp);
    if (slot->data)
    {
        if (is_svp_support && m_buffer_param.secure)
        {
            TEEMemDevice::getInstance().free((unsigned int)slot->data);
        }
        else
        {
            int size = slot->data_size + 128 + DevicePlatform::m_config.shift_bytes;
            if (DevicePlatform::m_config.platform & OVL_GEN_6582)
                size += 128 + DevicePlatform::m_config.shift_bytes;

            MMUDevice::getInstance().unmap(DevicePlatform::m_config.client_id,
                (unsigned int)slot->data, size, slot->mva);
            free(slot->data);
        }
    }

    if (is_svp_support && m_buffer_param.secure)
    {
        unsigned int sec_handle = TEEMemDevice::getInstance().alloc(m_buffer_param.size);

        if (sec_handle == 0)
        {
            HWC_LOGE("Failed to allocate secure memory size(%d)", m_buffer_param.size);

            slot->data = NULL;
            slot->mva = 0;
            slot->data_size = 0;
            return -EINVAL;
        }

        slot->data = (void *)sec_handle;
        slot->mva = (int)sec_handle;
        slot->data_size = m_buffer_param.size;
    }
    else
    {
        // allocate buffer
        // [NOTE] we must allocate extra 128 + buffer_shift_bytes bytes
        //        128 bytes to fit 128 byte alignement requirements
        //        m_shift_bytes protects invalid memory access when buffers need to shift
        const int extra_bytes = (DevicePlatform::m_config.platform & OVL_GEN_6582) ?
            128 + DevicePlatform::m_config.shift_bytes : 0;

        slot->data_size = m_buffer_param.size;
        slot->data = malloc(m_buffer_param.size + extra_bytes);
        if (slot->data == NULL)
        {
            HWC_LOGE("Failed to allocate memory size(%d)", m_buffer_param.size);
            slot->mva = 0;
            slot->data_size = 0;
            return -EINVAL;
        }
        slot->offset = (DevicePlatform::m_config.platform & OVL_GEN_6582) ?
            128 - ((uintptr_t)(slot->data) & 0x7f) : 0;

        // allocate mva
        int err = MMUDevice::getInstance().map(DevicePlatform::m_config.client_id,
                (unsigned int)slot->data, slot->data_size + extra_bytes, &slot->mva);
        if (err < 0)
        {
            HWC_LOGE("Failed to map MVA");

            free(slot->data);
            slot->data = NULL;
            slot->mva = 0;
            slot->data_size = 0;

            return -EINVAL;
        }
    }

    return NO_ERROR;
}

status_t DisplayBufferQueue::drainQueueLocked()
{
    while (m_is_synchronous && !m_queue.isEmpty())
    {
        m_dequeue_condition.wait(m_mutex);
    }

    return NO_ERROR;
}

status_t DisplayBufferQueue::dequeueBuffer(
    DisplayBuffer* buffer, bool async)
{
    HWC_ATRACE_CALL();

    AutoMutex l(m_mutex);

    int found = INVALID_BUFFER_SLOT;
    bool tryAgain = true;
    while (tryAgain)
    {
        found = INVALID_BUFFER_SLOT;
        for (int i = 0; i < m_buffer_count; i++)
        {
            const int state = m_slots[i].state;
            if (state == BufferSlot::FREE)
            {
                // return the oldest of the free buffers to avoid
                // stalling the producer if possible.
                if (found < 0)
                {
                    found = i;
                }
                else if (m_slots[i].frame_num < m_slots[found].frame_num)
                {
                    found = i;
                }
            }
        }

        // if no buffer is found, wait for a buffer to be released
        tryAgain = (found == INVALID_BUFFER_SLOT);
        if (tryAgain)
        {
            QLOGW("dequeueBuffer: cannot find available buffer, wait...");
            status_t res = m_dequeue_condition.waitRelative(m_mutex, ms2ns(16));
            QLOGW("dequeueBuffer: wake up to find available buffer (%s)",
                    (res == TIMED_OUT) ? "TIME OUT" : "WAKE");
        }
    }

    const int idx = found;

    HWC_ATRACE_BUFFER_INDEX("dequeue", idx);

    reallocate(idx);

    // buffer is now in DEQUEUED state
    m_slots[idx].state = BufferSlot::DEQUEUED;

    buffer->data                 = m_slots[idx].data;
    buffer->mva                  = m_slots[idx].mva;

    // [NOTE] for 82 hw limitation
    // should be removed after mt6582 phasing out
    const bool is_secure = (1 == HWCMediator::getInstance().m_features.svp) && m_buffer_param.secure;
    if ((DevicePlatform::m_config.platform & OVL_GEN_6582) && !is_secure)
    {
        buffer->data += m_slots[idx].offset;
        buffer->mva += m_slots[idx].offset;
        if (m_buffer_param.need_shift)
        {
            buffer->data += DevicePlatform::m_config.shift_bytes;
            buffer->mva += DevicePlatform::m_config.shift_bytes;
        }
    }

    buffer->data_size            = m_slots[idx].data_size;
    buffer->data_pitch           = m_slots[idx].data_pitch;
    buffer->data_format          = m_slots[idx].data_format;
    buffer->data_info.src_crop.makeInvalid();
    buffer->data_info.dst_crop.makeInvalid();
    buffer->data_info.is_sharpen = false;
    buffer->timestamp            = m_slots[idx].timestamp;
    buffer->frame_num            = m_slots[idx].frame_num;
    buffer->need_shift           = m_buffer_param.need_shift;
    buffer->release_fence        = m_slots[idx].release_fence;
    buffer->index                = idx;

    m_slots[idx].release_fence = -1;

    DBG_LOGD("dequeueBuffer (idx=%d, fence=%d)",
        idx, buffer->release_fence);

    // wait release fence
    if (!async)
    {
        sp<SyncFence> fence(new SyncFence(m_slots[idx].disp_id));
        fence->wait(buffer->release_fence, 1000, DEBUG_LOG_TAG);
        buffer->release_fence = -1;
    }

    return NO_ERROR;
}

status_t DisplayBufferQueue::queueBuffer(DisplayBuffer* buffer)
{
    HWC_ATRACE_CALL();

    sp<ConsumerListener> listener;

    {
        AutoMutex l(m_mutex);

        const int idx = buffer->index;

        HWC_ATRACE_BUFFER_INDEX("queue", idx);

        if (idx < 0 || idx >= m_buffer_count)
        {
            QLOGE("queueBuffer: slot index out of range [0, %d]: %d",
                     m_buffer_count, idx);
            return -EINVAL;
        }
        else if (m_slots[idx].state != BufferSlot::DEQUEUED)
        {
            QLOGE("queueBuffer: slot %d is not owned by the client "
                     "(state=%d)", idx, m_slots[idx].state);
            return -EINVAL;
        }

        // if queue not empty, means consumer is slower than producer
        // * in sync mode, may cause lag (but size 1 should be OK for triple buffer)
        // * in async mode, frame drop
        //bool dump_fifo = false;
        bool dump_fifo = false;
        if (true == m_is_synchronous)
        {
            // fifo depth 1 is ok for multiple buffer, but 2 would cause lag
            if (1 < m_queue.size())
            {
                QLOGW("queued:%d (lag), type:%d", m_queue.size(), m_queue_type);
                dump_fifo = true;
            }
        }
        else
        {
            // frame drop is fifo is not empty
            if (0 < m_queue.size())
            {
                QLOGW("queued:%d (drop frame), type:%d", m_queue.size(), m_queue_type);
                dump_fifo = true;
            }
        }

        // dump current fifo data, and the new coming one
        if (true == dump_fifo)
        {
            const BufferSlot *slot = &(m_slots[idx]);
            QLOGD("NEW [idx:%d] data:%p, mva:%p", idx, slot->data, slot->mva);

            Fifo::const_iterator i(m_queue.begin());
            while (i != m_queue.end())
            {
                slot = &(m_slots[*i]);
                QLOGD("    [idx:%d] data:%p, mva:%p", *i++, slot->data, slot->mva);
            }
        }

        if (m_is_synchronous)
        {
            // In synchronous mode we queue all buffers in a FIFO
            m_queue.push_back(idx);

            listener = m_listener;
        }
        else
        {
            // In asynchronous mode we only keep the most recent buffer.
            if (m_queue.empty())
            {
                m_queue.push_back(idx);
            }
            else
            {
                Fifo::iterator front(m_queue.begin());
                // buffer currently queued is freed
                m_slots[*front].state = BufferSlot::FREE;
                // and we record the new buffer index in the queued list
                *front = idx;
            }

            listener = m_listener;
        }

        m_slots[idx].handle               = buffer->handle;
        m_slots[idx].data_info.src_crop   = buffer->data_info.src_crop;
        m_slots[idx].data_info.dst_crop   = buffer->data_info.dst_crop;
        m_slots[idx].data_info.is_sharpen = buffer->data_info.is_sharpen;
        m_slots[idx].timestamp            = buffer->timestamp;
        m_slots[idx].state                = BufferSlot::QUEUED;
        m_slots[idx].frame_num            = (++m_frame_counter);
        m_slots[idx].alpha_enable         = buffer->alpha_enable;
        m_slots[idx].alpha                = buffer->alpha;
        m_slots[idx].sequence             = buffer->sequence;
        m_slots[idx].acquire_fence        = buffer->acquire_fence;
        m_slots[idx].release_fence        = buffer->release_fence;
        m_slots[idx].rel_fence_idx        = buffer->rel_fence_idx;
        m_slots[idx].disp_id              = buffer->disp_id;

        DBG_LOGD("(%d) queueBuffer (idx=%d, acquire_fence=%d, release_fence=%d)",
            m_slots[idx].disp_id, idx, m_slots[idx].acquire_fence,
            m_slots[idx].release_fence);

        m_dequeue_condition.broadcast();
    }

    if (listener != NULL) listener->onBufferQueued();

    return NO_ERROR;
}

status_t DisplayBufferQueue::cancelBuffer(int index)
{
    HWC_ATRACE_CALL();
    HWC_ATRACE_BUFFER_INDEX("cancel", index);

    AutoMutex l(m_mutex);

    if (index == INVALID_BUFFER_SLOT) return -EINVAL;

    if (index < 0 || index >= m_buffer_count)
    {
        QLOGE("cancelBuffer: slot index out of range [0, %d]: %d",
                m_buffer_count, index);
        return -EINVAL;
    }
    else if (m_slots[index].state != BufferSlot::DEQUEUED)
    {
        QLOGE("cancelBuffer: slot %d is not owned by the client (state=%d)",
                index, m_slots[index].state);
        return -EINVAL;
    }

    QLOGD("cancelBuffer (%d)", index);

    m_slots[index].state = BufferSlot::FREE;
    m_slots[index].frame_num = 0;
    m_slots[index].acquire_fence = -1;
    m_slots[index].release_fence = -1;
    m_slots[index].rel_fence_idx = 0;

    m_dequeue_condition.broadcast();
    return NO_ERROR;
}

status_t DisplayBufferQueue::setSynchronousMode(bool enabled)
{
    AutoMutex l(m_mutex);

    if (m_is_synchronous != enabled)
    {
        // drain the queue when changing to asynchronous mode
        if (!enabled) drainQueueLocked();

        m_is_synchronous = enabled;
        m_dequeue_condition.broadcast();
    }

    return NO_ERROR;
}

void DisplayBufferQueue::dumpLocked(int idx)
{
    if (idx < 0 || idx >= m_buffer_count)
    {
        HWC_LOGE("Failed to dump buffer with invalid index(%d)", idx);
        return;
    }

    if (m_slots[idx].handle == NULL)
    {
        HWC_LOGE("Failed to dump buffer(%d) with invalid source", idx);
        return;
    }

    String8 file_name = String8::format("/data/dp_%p_%d_%d",
                                m_slots[idx].handle,
                                m_buffer_param.width,
                                m_buffer_param.height);

    switch (m_slots[idx].data_format)
    {
        case HAL_PIXEL_FORMAT_RGBA_8888:
        case HAL_PIXEL_FORMAT_RGBX_8888:
        case HAL_PIXEL_FORMAT_BGRA_8888:
        case HAL_PIXEL_FORMAT_IMG1_BGRX_8888:
            file_name.append(".abgr");
            break;

        case HAL_PIXEL_FORMAT_RGB_565:
            file_name.append(".rgb565");
            break;

        case HAL_PIXEL_FORMAT_NV12_BLK_FCM:
        case HAL_PIXEL_FORMAT_NV12_BLK:
        case HAL_PIXEL_FORMAT_I420:
        case HAL_PIXEL_FORMAT_YV12:
        case HAL_PIXEL_FORMAT_YUV_PRIVATE:
            file_name.append(".yuyv");
            break;
    }

    FILE* fp = fopen(file_name.string(), "wb");
    if (fp != NULL)
    {
        size_t dst_size = m_buffer_param.pitch * m_buffer_param.height;

#ifdef BUFFER_DEBUG
        fwrite((char*)m_slots[idx].data - (EXTRA_BYTES / 2),
            dst_size + EXTRA_BYTES, 1, fp);
#else
        fwrite(m_slots[idx].data, dst_size, 1, fp);
#endif

        fclose(fp);
    }
}

void DisplayBufferQueue::dump(QUEUE_DUMP_CONDITION cond)
{
    AutoMutex l(m_mutex);

    switch(cond)
    {
        case QUEUE_DUMP_LAST_ACQUIRED:
            dumpLocked(m_last_acquire_idx);
            break;

        case QUEUE_DUMP_ALL_QUEUED:
            break;

        default:
            break;
    };
}

status_t DisplayBufferQueue::acquireBuffer(
    DisplayBuffer* buffer, bool async)
{
    HWC_ATRACE_CALL();

    AutoMutex l(m_mutex);

    // check if queue is empty
    // In asynchronous mode the list is guaranteed to be one buffer deep.
    // In synchronous mode we use the oldest buffer.
    if (!m_queue.empty())
    {
        Fifo::iterator front(m_queue.begin());
        int idx = *front;

        HWC_ATRACE_BUFFER_INDEX("acquire", idx);

        // buffer is now in ACQUIRED state
        m_slots[idx].state = BufferSlot::ACQUIRED;

        buffer->data        = m_slots[idx].data;
        buffer->mva         = m_slots[idx].mva;

        // [NOTE] for 82 hw limitation
        // should be removed after mt6582 phasing out
        const bool is_secure = (1 == HWCMediator::getInstance().m_features.svp) && m_buffer_param.secure;
        if ((DevicePlatform::m_config.platform & OVL_GEN_6582) && !is_secure)
        {
            buffer->data += m_slots[idx].offset;
            buffer->mva += m_slots[idx].offset;
            if (m_buffer_param.need_shift)
            {
                buffer->data += DevicePlatform::m_config.shift_bytes;
                buffer->mva += DevicePlatform::m_config.shift_bytes;
            }
        }

        buffer->data_size   = m_slots[idx].data_size;
        buffer->data_pitch  = m_slots[idx].data_pitch;
        buffer->data_format = m_slots[idx].data_format;
        buffer->data_info.src_crop   = m_slots[idx].data_info.src_crop;
        buffer->data_info.dst_crop   = m_slots[idx].data_info.dst_crop;
        buffer->data_info.is_sharpen = m_slots[idx].data_info.is_sharpen;
        buffer->timestamp            = m_slots[idx].timestamp;
        buffer->frame_num            = m_slots[idx].frame_num;
        buffer->protect              = m_buffer_param.protect;
        buffer->secure               = m_buffer_param.secure;
        buffer->need_shift           = m_buffer_param.need_shift;
        buffer->alpha_enable         = m_slots[idx].alpha_enable;
        buffer->alpha                = m_slots[idx].alpha;
        buffer->sequence             = m_slots[idx].sequence;
        buffer->acquire_fence        = m_slots[idx].acquire_fence;
        buffer->rel_fence_idx        = m_slots[idx].rel_fence_idx;
        buffer->index                = idx;

        m_slots[idx].acquire_fence = -1;

        DBG_LOGD("acquireBuffer (idx=%d, fence=%d)",
            idx, buffer->acquire_fence);

        // remember last acquire buffer's index
        m_last_acquire_idx = idx;

        m_queue.erase(front);
        m_dequeue_condition.broadcast();
    }
    else
    {
        DBG_LOGW("acquireBuffer fail");

        buffer->index = INVALID_BUFFER_SLOT;
        return NO_BUFFER_AVAILABLE;
    }

    // wait acquire fence
    if (!async)
    {
        sp<SyncFence> fence(new SyncFence(m_slots[m_last_acquire_idx].disp_id));
        fence->wait(buffer->acquire_fence, 1000, DEBUG_LOG_TAG);
        buffer->acquire_fence = -1;
    }

    return NO_ERROR;
}

status_t DisplayBufferQueue::releaseBuffer(int index)
{
    HWC_ATRACE_CALL();
    HWC_ATRACE_BUFFER_INDEX("release", index);

    AutoMutex l(m_mutex);

    if (index == INVALID_BUFFER_SLOT) return -EINVAL;

    if (index < 0 || index >= m_buffer_count)
    {
        QLOGE("releaseBuffer: slot index out of range [0, %d]: %d",
                m_buffer_count, index);
        return -EINVAL;
    }

    if (m_slots[index].state != BufferSlot::ACQUIRED)
    {
        QLOGE("attempted to release buffer(%d) with state(%d)",
                 index, m_slots[index].state);
        return -EINVAL;
    }

    m_slots[index].state = BufferSlot::FREE;

    DBG_LOGD("releaseBuffer (idx=%d)", index);

    m_dequeue_condition.broadcast();
    return NO_ERROR;
}

void DisplayBufferQueue::setConsumerListener(
    const sp<ConsumerListener>& listener)
{
    QLOGI("setConsumerListener");
    AutoMutex l(m_mutex);
    m_listener = listener;
}
