/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.anttunerdebug;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.os.AsyncResult;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;

import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.R;

public class AntTunerDebugBPI extends Activity implements OnClickListener {
    private static final String TAG = "anttunerdebugBPI";

    public static final int OP_BPI_READ = 0;
    public static final int OP_BPI_WRITE = 1;
    private static final String RESPONSE_CMD = "+ERFTX: ";
    private Spinner mSpRWType;

    private EditText mEdBpiData1;
    private EditText mEdBpiData2;

    private Button mBtnBpiRead;
    private Button mBtnBpiWrite;

    private String mBpiData1 = null;
    private String mBpiData2 = null;
    private String mBpiMode = null;
    private Phone mPhone = null;
    private Toast mToast = null;

    private void handleQuery(String[] result) {
        if (result != null && result.length > 0) {
            Elog.v(TAG, "Modem return: " + result[0]);
            String[] values = result[0].substring(RESPONSE_CMD.length(), result[0].length())
                    .trim().split(",");
            if (values != null && values.length > 0) {
                if (values[2] != null) {
                    values[2]  = Long.toHexString(Long.parseLong(values[2]));
                    mEdBpiData1.setText(values[2]);
                }
                if (values[3] != null) {
                    values[3]  = Long.toHexString(Long.parseLong(values[3]));
                    mEdBpiData2.setText(values[3]);
                }

            }
        } else {
            Elog.e(TAG, "Modem return null");
        }
    }

    private final Handler mATHandler = new Handler() {
        private String[] mReturnData = new String[2];

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == OP_BPI_READ) {
                AsyncResult ar = (AsyncResult) msg.obj;
                if (ar.exception == null) {
                    Log.d(TAG, "BPI read successful.");
                    mReturnData = (String[]) ar.result;
                    Elog.d(TAG, "mReturnData = " + mReturnData[0]);
                    showToast(mReturnData[0]);
                    handleQuery(mReturnData);
                } else {
                    showToast("BPI read failed.");
                }
            } else if (msg.what == OP_BPI_WRITE) {
                AsyncResult ar = (AsyncResult) msg.obj;
                if (ar.exception == null) {
                    Log.d(TAG, "MIPI write successful.");
                } else {
                    showToast("MIPI write failed.");
                }
            }

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ant_tuner_debug_bpi);

        mEdBpiData1 = (EditText) findViewById(R.id.ant_tuner_debug_bpi_data1);
        mEdBpiData2 = (EditText) findViewById(R.id.ant_tuner_debug_bpi_data2);

        mBtnBpiRead = (Button) findViewById(R.id.ant_tuner_debug_bpi_read);
        mBtnBpiWrite = (Button) findViewById(R.id.ant_tuner_debug_bpi_write);
        mBtnBpiRead.setOnClickListener(this);
        mBtnBpiWrite.setOnClickListener(this);

        int subId = SubscriptionManager.getDefaultDataSubscriptionId();
        Log.i("@M_" + TAG, "sub id " + subId);
        int phoneId = SubscriptionManager.getPhoneId(subId);
        Log.i("@M_" + TAG, "phone id " + phoneId);
        int phoneCount = TelephonyManager.getDefault().getPhoneCount();
        Log.i("@M_" + TAG, "phone count " + phoneCount);
        mPhone = PhoneFactory.getPhone(phoneId >= 0 && phoneId < phoneCount ? phoneId : 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void onClick(View arg0) {
        switch (arg0.getId()) {
        case R.id.ant_tuner_debug_bpi_read:
            if (valueGetAndCheck(0) == true) {
                String[] cmd = new String[2];
                cmd[0] = "AT+ERFTX=12,1,0";
                cmd[1] = "+ERFTX:";
                sendAtCommand(cmd, OP_BPI_READ);
            }
            break;
        case R.id.ant_tuner_debug_bpi_write:
            if (valueGetAndCheck(1) == true) {
                String[] cmd = new String[2];
                mBpiData1 = Long.toString(Long.parseLong(mBpiData1, 16));
                mBpiData2 = Long.toString(Long.parseLong(mBpiData2, 16));
                cmd[0] = "AT+ERFTX=12,1,1," + mBpiData1 + "," + mBpiData2;
                cmd[1] = "";
                sendAtCommand(cmd, OP_BPI_WRITE);
            }
            break;
        }
    }

    private void sendAtCommand(String[] command, int msg) {
        Elog.d(TAG, "sendAtCommand() " + command[0]);
        // showToast("sendAtCommand: " + command[0]);
        if (mPhone != null) {
            mPhone.invokeOemRilRequestStrings(command, mATHandler.obtainMessage(msg));
        }
    }

    private boolean valueGetAndCheck(int flag) {
        mBpiData1 = mEdBpiData1.getText().toString();
        mBpiData2 = mEdBpiData2.getText().toString();

        mBpiMode = (flag == 0) ? "0" : "1"; // 0:read 1:write

        if (mBpiMode.equals("1")) {
            if (mBpiData1.equals("")) {
                showToast("Data1 should not be empty");
                return false;
            }

            try {
                Long.parseLong(mBpiData1, 16);
            } catch (NumberFormatException e) {
                showToast("Data1 should be 16 HEX");
                return false;
            }

            if (!mBpiData2.equals("")) {
                try {
                    Long.parseLong(mBpiData2, 16);
                } catch (NumberFormatException e) {
                    showToast("Data2 should be 16 HEX");
                    return false;
                }
            } else {
                mBpiData2 = "0";
            }
        }

        return true;
    }

    private void showToast(String msg) {
        mToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        mToast.show();
    }

}
