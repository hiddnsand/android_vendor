/*
* Copyright (c) 2014 - 2016 MediaTek Inc.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files
* (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include "secmem_tlc.h"

int main(int argc, char *argv[])
{
	(void) argc;
	(void) argv;
	
	uint32_t ret = UREE_SUCCESS;

	printf("Secure memory usage\n");
	printf("###################\n");
	printf("SECMEM TLC called\n");

	ret = tlcMemOpen();

	if (UREE_SUCCESS != ret) {
		printf("open TL session failed!\n");
		return ret;
	}

	printf("Start to call the Dump Usage Secure Memory\n");
	ret = UREE_DumpSecurememInfo();
	if (UREE_SUCCESS == ret) {
		printf("Secure memory dump success\n");
	} else {
		printf("Secure memory dump fail, ret=0x%x\n", ret);
	}

	tlcMemClose();
	return ret;
}
