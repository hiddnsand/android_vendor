##
# Here properties overriden
##
CIP_PROPERTY_OVERRIDES :=

ifeq ($(strip $(MTK_CLR_CODE_SUPPORT)), yes)
  CIP_PROPERTY_OVERRIDES += persist.mtk_clr_code_support=1
else ifeq ($(strip $(MTK_CLR_CODE_SUPPORT)), no)
  CIP_PROPERTY_OVERRIDES += persist.mtk_clr_code_support=0
endif

#WiFi Offloading support
ifeq ($(strip $(MTK_EPDG_CIP_SUPPORT)), yes)
  CIP_PROPERTY_OVERRIDES += persist.mtk_epdg_support=1
else ifeq ($(strip $(MTK_WFC_SUPPORT)), yes)
  CIP_PROPERTY_OVERRIDES += persist.mtk_epdg_support=1
else
  CIP_PROPERTY_OVERRIDES += persist.mtk_epdg_support=0
endif

#VoLTE/IMS support
ifeq ($(strip $(MTK_VOLTE_SUPPORT)), yes)
  CIP_PROPERTY_OVERRIDES += persist.mtk_volte_support=1
  CIP_PROPERTY_OVERRIDES += persist.mtk.volte.enable=1
else ifeq ($(strip $(MTK_VOLTE_SUPPORT)), no)
  CIP_PROPERTY_OVERRIDES += persist.mtk_volte_support=0
  CIP_PROPERTY_OVERRIDES += persist.mtk.volte.enable=0
endif

ifeq ($(strip $(MTK_IMS_SUPPORT)), yes)
  CIP_PROPERTY_OVERRIDES += persist.mtk_ims_support=1
else ifeq ($(strip $(MTK_IMS_SUPPORT)), no)
  CIP_PROPERTY_OVERRIDES += persist.mtk_ims_support=0
endif

ifeq ($(strip $(MTK_WFC_SUPPORT)), yes)
  CIP_PROPERTY_OVERRIDES += persist.mtk_wfc_support=1
  CIP_PROPERTY_OVERRIDES += persist.mtk.wfc.enable=1
else ifeq ($(strip $(MTK_WFC_SUPPORT)), no)
  CIP_PROPERTY_OVERRIDES += persist.mtk_wfc_support=0
  CIP_PROPERTY_OVERRIDES += persist.mtk.wfc.enable=0
endif

ifeq ($(strip $(MTK_VILTE_SUPPORT)),yes)
  CIP_PROPERTY_OVERRIDES += persist.mtk_vilte_support=1
  CIP_PROPERTY_OVERRIDES += persist.mtk.ims.video.enable=1
else ifeq ($(strip $(MTK_VILTE_SUPPORT)), no)
  CIP_PROPERTY_OVERRIDES += persist.mtk_vilte_support=0
  CIP_PROPERTY_OVERRIDES += persist.mtk.ims.video.enable=0
endif

#Power on/off md in flight mode
ifeq ($(strip $(MTK_FLIGHT_MODE_POWER_OFF_MD)),yes)
  CIP_PROPERTY_OVERRIDES += persist.flight_mode_md_off=1
else ifeq ($(strip $(MTK_FLIGHT_MODE_POWER_OFF_MD)),no)
  CIP_PROPERTY_OVERRIDES += persist.flight_mode_md_off=0
endif

ifdef MTK_MULTI_SIM_SUPPORT
  CIP_PROPERTY_OVERRIDES += persist.radio.multisim.config=$(strip $(MTK_MULTI_SIM_SUPPORT))
endif

ifdef MTK_SHARE_MODEM_CURRENT
  CIP_PROPERTY_OVERRIDES += ril.current.share_modem=$(strip $(MTK_SHARE_MODEM_CURRENT))
endif

ifeq ($(strip $(MTK_RCS_UA_SUPPORT)),yes)
  CIP_PROPERTY_OVERRIDES += persist.mtk_rcs_ua_support=1
else ifeq ($(strip $(MTK_RCS_UA_SUPPORT)),no)
  CIP_PROPERTY_OVERRIDES += persist.mtk_rcs_ua_support=0
endif

#Sim switch policy support
ifdef MTK_SIM_SWITCH_POLICY
  CIP_PROPERTY_OVERRIDES += persist.mtk_sim_switch_policy=$(strip $(MTK_SIM_SWITCH_POLICY))
endif

#Sim switch capability support
ifeq ($(strip $(MTK_DISABLE_CAPABILITY_SWITCH)),yes)
  CIP_PROPERTY_OVERRIDES += persist.mtk_disable_cap_switch=1
else ifeq($(strip $(MTK_DISABLE_CAPABILITY_SWITCH)),no)
  CIP_PROPERTY_OVERRIDES += persist.mtk_disable_cap_switch=0
endif

ifeq ($(strip $(MTK_CC33_SUPPORT)),yes)
  CIP_PROPERTY_OVERRIDES += persist.data.cc33.support=1
else ifeq ($(strip $(MTK_CC33_SUPPORT)),no)
  CIP_PROPERTY_OVERRIDES += persist.data.cc33.support=0
endif

# SOS quick dial support
ifeq ($(strip $(MTK_SOS_QUICK_DIAL_SUPPORT)),yes)
  CIP_PROPERTY_OVERRIDES += persist.mtk_sos_quick_dial=1
else
  CIP_PROPERTY_OVERRIDES += persist.mtk_sos_quick_dial=0
endif
