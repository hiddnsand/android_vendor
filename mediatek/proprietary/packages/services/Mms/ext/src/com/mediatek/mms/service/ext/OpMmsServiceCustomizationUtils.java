package com.mediatek.mms.service.ext;

import android.content.Context;

import com.mediatek.common.util.OperatorCustomizationFactoryLoader;
import com.mediatek.common.util.OperatorCustomizationFactoryLoader.OperatorFactoryInfo;

import java.util.ArrayList;
import java.util.List;

public class OpMmsServiceCustomizationUtils {

    // list every operator's factory path and name.
    private static final List<OperatorFactoryInfo> sOperatorFactoryInfoList
            = new ArrayList<OperatorFactoryInfo>();

    static OpMmsServiceCustomizationFactoryBase sFactory = null;

    static {
        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP01Mms.apk",
                         "com.mediatek.mms.plugin.Op01MmsCustomizationFactory",
                         "com.mediatek.mms.plugin",
                         "OP01"
                        ));

        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP09Mms.apk",
                         "com.mediatek.mms.plugin.Op09MmsCustomizationFactory",
                         "com.mediatek.mms.plugin",
                         "OP09",
                         "SEGDEFAULT"
                        ));
    }

    public static synchronized OpMmsServiceCustomizationFactoryBase getOpFactory(Context context) {
        if (sFactory == null) {
            sFactory = (OpMmsServiceCustomizationFactoryBase) OperatorCustomizationFactoryLoader
                        .loadFactory(context, sOperatorFactoryInfoList);
            if (sFactory == null) {
                sFactory = new OpMmsServiceCustomizationFactoryBase(context);
            }
        }
        return sFactory;
    }
}
