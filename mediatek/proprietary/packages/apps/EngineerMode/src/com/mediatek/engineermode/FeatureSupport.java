package com.mediatek.engineermode;

import java.util.List;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.SystemProperties;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

import com.mediatek.engineermode.Elog;

public class FeatureSupport {

    private static final String SUPPORTED = "1";
    private static final String TAG = "EM_ModemCategory";

    public static final String FK_MATV_FEATURE = "ro.have_matv_feature";
    public static final String FK_FD_SUPPORT = "ro.mtk_fd_support";
    public static final String FK_LOG2SERVER_APP = "ro.mtk_log2server_app";
    public static final String FK_SMSREG_APP = "ro.mtk_smsreg_app";
    public static final String FK_BSP_PACKAGE = "ro.mtk_bsp_package";
    public static final String FK_EVDO_DT_SUPPORT = "ro.evdo_dt_support";
    public static final String FK_MTK_C2K_SUPPORT = "ro.boot.opt_c2k_support";
    public static final String FK_WFD_SUPPORT = "ro.mtk_wfd_support";
    public static final String FK_DEVREG_APP = "ro.mtk_devreg_app";
    public static final String FK_LTE_DC_SUPPORT = "ro.mtk_lte_dc_support";
    public static final String FK_LTE_SUPPORT = "ro.boot.opt_lte_support";
    public static final String FK_VOLTE_SUPPORT = "persist.mtk_volte_support";
    public static final String FK_TC1_FEATURE = "ro.mtk_tc1_feature";
    public static final String FK_WORLD_PHONE = "ro.mtk_world_phone";
    public static final String FK_WCDMA_PREFERRED = "ro.mtk_rat_wcdma_preferred";
    public static final String FK_VOW_SUPPORT = "ro.mtk_vow_support";
    public static final String FK_VOICE_UNLOCK_SUPPORT = "ro.mtk_voice_unlock_support";
    public static final String FK_AAL_SUPPORT = "ro.mtk_aal_support";
    public static final String FK_CT4GREG_APP = "ro.mtk_ct4greg_app";
    public static final String FK_VILTE_SUPPORT = "persist.mtk_vilte_support";
    public static final String FK_MTK_WEARABLE_PLATFORM = "ro.mtk_wearable_platform";
    public static final String FK_MD_WM_SUPPORT = "ro.mtk_md_world_mode_support";
    public static final String FK_MTK_TDD_DATA_ONLY_SUPPORT = "ro.mtk_tdd_data_only_support";
    public static final String FK_MTK_C2K_LTE_MODE = "ro.boot.opt_c2k_lte_mode";
    public static final String FK_MTK_RAT_CONFIG_USING_DEFAULT = "ro.boot.opt_using_default";
    public static final String FK_MTK_TELEPHONY_SWITCH = "ro.mtk_telephony_switch";
    public static final String FK_MTK_DSDA_SUPPORT = "persist.radio.multisim.config";
    public static final String FK_MTK_TEL_LOG_SUPPORT = "persist.log.tag.tel_log_ctrl";
    public static final String FK_MTK_93_SUPPORT = "ro.mtk_ril_mode";
    public static final String FK_APC_SUPPORT = "ril.apc.support";
    public static final String FK_MTK_EPDG_CONFIG = "persist.mtk_epdg_support";
    public static final String FK_MTK_WFC_SUPPORT = "persist.mtk_wfc_support";
    public static final String FK_BUILD_VERSION_RELEASE = "ro.build.version.release";

    public static final String PK_CDS_EM = "com.mediatek.connectivity";
    public static final String ENG_LOAD = "eng";
    public static final String USER_LOAD = "user";
    public static final String USERDEBUG_LOAD = "userdebug";

    public static boolean is_support_3GOnly_md = false;
    public static boolean is_support_90_md = false;
    public static boolean is_support_91_md = false;
    public static boolean is_support_92_md = false;
    public static boolean is_support_93_md = false;

    public static boolean is3GOnlyModem() {
        if( ChipSupport.getChip() >= ChipSupport.MTK_6570_SUPPORT &&
                ChipSupport.getChip() <= ChipSupport.MTK_6580_SUPPORT ){
               is_support_3GOnly_md = true;
           }else{
               is_support_3GOnly_md = false;
           }
            Elog.d(TAG, "This modem is is_support_3GOnly_md: " + is_support_3GOnly_md);
            return is_support_3GOnly_md;
     }

    public static boolean is90Modem() {
        if(ChipSupport.getChip() == ChipSupport.MTK_6735_SUPPORT){
            is_support_90_md = true;
        }else{
            is_support_90_md = false;
        }
         Elog.d(TAG, "This modem is 90 modem: " + is_support_90_md);
         return is_support_90_md;
     }

    public static boolean is91Modem() {
        if( ChipSupport.getChip() >= ChipSupport.MTK_6755_SUPPORT &&
             ChipSupport.getChip() <= ChipSupport.MTK_6757CH_SUPPORT ){
            is_support_91_md = true;
        }else{
            is_support_91_md = false;
        }
         Elog.d(TAG, "This modem is 91 modem: " + is_support_91_md);
         return is_support_91_md;
     }

    public static boolean is92Modem() {
        if( ChipSupport.getChip() > ChipSupport.MTK_6757CH_SUPPORT &&
                !is93Modem() ){
               is_support_92_md = true;
           }else{
               is_support_92_md = false;
           }
            Elog.d(TAG, "This modem is 92 modem: " + is_support_92_md);
         return is_support_92_md;
     }

    public static boolean is93Modem() {
        is_support_93_md = "c6m_1rild".equals(SystemProperties
                .get(FK_MTK_93_SUPPORT)) ? true : false;
        Elog.d(TAG, "This modem is 93 modem: " + is_support_93_md);
        return is_support_93_md;
    }
    public static boolean isO0Branch() {
        String curVersion = SystemProperties
                .get(FK_BUILD_VERSION_RELEASE, "").replace(" ", "");
        boolean is_o0_branch = "8.0.0".equals(curVersion) ? true : false;
        Elog.d(TAG, "Current version: " + curVersion + ", isO0Branch: " + is_o0_branch);
        return is_o0_branch;
    }

    public static boolean isO1Branch() {
        String curVersion = SystemProperties
                .get(FK_BUILD_VERSION_RELEASE, "").replace(" ", "");
        boolean is_o1_branch = "8.1.0".equals(curVersion) ? true : false;
        Elog.d(TAG, "Current version: " + curVersion + ", isO1Branch: " + is_o1_branch);
        return is_o1_branch;
    }

    public static boolean isSupported(String featureKey) {
        return SUPPORTED.equals(SystemProperties.get(featureKey));
    }

    public static String isSupportC2kLte() {
        return SystemProperties.get(FK_MTK_C2K_LTE_MODE);
    }

    public static String getProperty (String propertyName) {
        return SystemProperties.get(propertyName);
    }

    /**
     * Get em svr support or not.
     *
     * @return true or false.
     */
    public static boolean isSupportedEmSrv() {
        boolean isSupported = true;
        if (SUPPORTED.equals(SystemProperties.get("ro.mtk_gmo_ram_optimize"))) {
            if (!(ENG_LOAD.equals(SystemProperties.get("ro.build.type")))) {
                isSupported = false;
            }
        }
        return isSupported;
    }

    public static boolean isPackageExisted(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(0);
        for (ApplicationInfo ai : packages) {
            if (ai.packageName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isEngLoad() {
        return ENG_LOAD.equals(SystemProperties.get("ro.build.type"));
    }

    public static boolean isUserLoad() {
        return USER_LOAD.equals(SystemProperties.get("ro.build.type"));
    }
   public static boolean isUserDebugLoad() {
        return USERDEBUG_LOAD.equals(SystemProperties.get("ro.build.type"));
    }

    public static boolean isSupportWfc() {
        return SystemProperties.get(FK_MTK_WFC_SUPPORT, "0").equals("1");
    }
}