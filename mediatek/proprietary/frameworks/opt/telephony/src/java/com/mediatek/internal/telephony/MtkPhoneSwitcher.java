/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.internal.telephony;

import android.content.Context;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Looper;
import android.telephony.Rlog;

import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.DctConstants;
import com.android.internal.telephony.ITelephonyRegistry;
import com.android.internal.telephony.dataconnection.ApnContext;
import com.android.internal.telephony.dataconnection.DcRequest;
import com.android.internal.telephony.SubscriptionController;
import com.android.internal.telephony.PhoneSwitcher;
import com.android.internal.telephony.Phone;

import com.mediatek.internal.telephony.dataconnection.MtkDcHelper;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;

import java.util.List;

public final class MtkPhoneSwitcher extends PhoneSwitcher {
    private final static String LOG_TAG = "MtkPhoneSwitcher";
    private final static boolean VDBG = true;
    private static MtkPhoneSwitcher sInstance = null;

    public MtkPhoneSwitcher(int maxActivePhones, int numPhones, Context context,
            SubscriptionController subscriptionController, Looper looper, ITelephonyRegistry tr,
            CommandsInterface[] cis, Phone[] phones) {
        super(maxActivePhones, numPhones, context, subscriptionController, looper, tr, cis, phones);
        sInstance = this;
    }

    public static MtkPhoneSwitcher getInstance() {
        return sInstance;
    }

    @Override
    protected NetworkCapabilities makeNetworkFilter() {
        NetworkCapabilities netCap = super.makeNetworkFilter();
        netCap.addCapability(NetworkCapabilities.NET_CAPABILITY_BIP);
        return netCap;
    }

    // used when the modem may have been rebooted and we want to resend
    // setDataAllowed
    @Override
    public void resendDataAllowed(int phoneId) {
        log("resendDataAllowed " + phoneId);
        super.resendDataAllowed(phoneId);
    }

    /// M: ECC w/o SIM  @{
    private boolean isEimsAllowed(NetworkRequest networkRequest) {
        if (networkRequest.networkCapabilities.hasCapability(
                NetworkCapabilities.NET_CAPABILITY_EIMS)) {
            //Check if sim inserted
            for (int i = 0; i < mNumPhones; i++) {
                if (MtkDcHelper.isSimInserted(i)) {
                    loge("isAllowEims, sim is not null");
                    return false;
                }
            }
            return true;
        }
        loge("isAllowEims, NetworkRequest not include EIMS capability");
        return false;
    }
    /// @}

    @Override
    protected void suggestDefaultActivePhone(List<Integer> newActivePhones) {
        // Scenario:
        //    1. Default data unset and camp on 4G at SIM1.
        //    2. IMS request and release after at SIM1.
        //    3. 4G been detached and not recover ever.
        //  The reason for that is because default data unset and after detach,
        //  there's no more active phone to attach.
        // Solution:
        //    In the case of no active phone, add one with following condition
        //    1. Get main capability (4G) phone id
        //    2. Add it into activie phone if SIM inserted.
        MtkDcHelper dcHelper = MtkDcHelper.getInstance();
        int mainCapPhoneId = RadioCapabilitySwitchUtil.getMainCapabilityPhoneId();
        if (newActivePhones.isEmpty()) {
            log("newActivePhones is empty");
            if (MtkDcHelper.isSimInserted(mainCapPhoneId)) {
                log("newActivePhones mainCapPhoneId=" + mainCapPhoneId);
                newActivePhones.add(mainCapPhoneId);
            }
        }

        /// M: ECC w/o SIM  @{
        if (newActivePhones.isEmpty() && !dcHelper.hasMdAutoSetupImsCapability()) {
            log("ECC w/o SIM");
            for (DcRequest dcRequest : mPrioritizedDcRequests) {
                if (isEimsAllowed(dcRequest.networkRequest) == true) {
                    log("newActivePhones mainCapPhoneId=" + mainCapPhoneId);
                    newActivePhones.add(mainCapPhoneId);
                }
            }
        }
        /// @}
    }

    /**
     * Return if PhoneSwitcher accepts this network request or not.
     * If the apnId is invalid, this request will not be processed.
     *
     * @param request the NetworkRequest to evaluate.
     * @param score the score of this network factory.
     * @return a boolean indicates whether to process this request or not.
     */
    public static boolean acceptRequest(NetworkRequest request, int score) {
        if (ApnContext.apnIdForNetworkRequest(request) == DctConstants.APN_INVALID_ID) {
            log("[acceptRequest] Invalid APN ID request: " + request);
            return false;
        }
        return true;
    }

    private static void log(String l) {
        Rlog.d(LOG_TAG, l);
    }

    private static void loge(String l) {
        Rlog.e(LOG_TAG, l);
    }
}

