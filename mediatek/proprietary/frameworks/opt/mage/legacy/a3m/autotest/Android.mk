#
# Makes Java-side of A3M Autotest
#

#################################################
# Build package.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := tests
LOCAL_SDK_VERSION := current

LOCAL_SRC_FILES := $(call all-java-files-under, java)
LOCAL_PACKAGE_NAME := A3mAutotest
LOCAL_JNI_SHARED_LIBRARIES := liba3m liba3mautotest

include $(BUILD_PACKAGE)

##################################################

include $(call all-makefiles-under,$(LOCAL_PATH))
