#define DEBUG_LOG_TAG "DPY"

#include <stdint.h>

#include <hardware/hwcomposer.h>
#include <cutils/properties.h>

#include <linux/mtkfb_info.h>

#include "utils/debug.h"
#include "utils/tools.h"

#include "display.h"
#include "hwdev.h"
#include "overlay.h"
#include "event.h"
#include "hwc_priv.h"

// ---------------------------------------------------------------------------

ANDROID_SINGLETON_STATIC_INSTANCE(DisplayManager);

sp<VSyncThread> g_vsync_thread = NULL;
sp<UEventThread> g_uevent_thread = NULL;

#ifdef MTK_USER_BUILD
int DisplayManager::m_profile_level = PROFILE_NONE;
#else
int DisplayManager::m_profile_level = PROFILE_COMP | PROFILE_BLT | PROFILE_TRIG;
#endif

DisplayManager::DisplayManager()
    : m_curr_disp_num(1)
    , m_fake_disp_num(0)
    , m_listener(NULL)
{
    m_data = (DisplayData*) calloc(1, MAX_DISPLAYS * sizeof(DisplayData));

    HWC_LOGI("Display Information:");
    HWC_LOGI("# fo current devices : %d", m_curr_disp_num);

    char value[PROPERTY_VALUE_MAX];
    property_get("ro.sf.lcd_density", value, "160");
    float density = atof(value);

    // query primary display info
    {
        disp_session_info info;
        DispDevice::getInstance().getOverlaySessionInfo(HWC_DISPLAY_PRIMARY, &info);

        DisplayData* disp_data = &m_data[HWC_DISPLAY_PRIMARY];
        disp_data->width     = info.displayWidth;
        disp_data->height    = info.displayHeight;
        disp_data->format    = info.displayFormat;
        disp_data->xdpi      = info.physicalWidth == 0 ? density :
                                (info.displayWidth * 25.4f / info.physicalWidth);
        disp_data->ydpi      = info.physicalHeight == 0 ? density :
                                (info.displayHeight * 25.4f / info.physicalHeight);
        disp_data->has_vsync = info.isHwVsyncAvailable;
        disp_data->connected = info.isConnected;

        // TODO: ask from display driver
        disp_data->secure    = true;
        disp_data->subtype   = HWC_DISPLAY_LCM;

        float refreshRate = info.vsyncFPS / 100.0;
        if (0 >= refreshRate) refreshRate = 60.0;
        m_data[HWC_DISPLAY_PRIMARY].refresh   = nsecs_t(1e9 / refreshRate);

        printDisplayInfo(HWC_DISPLAY_PRIMARY);
    }

    g_vsync_thread = new VSyncThread();
    if (g_vsync_thread == NULL)
    {
        HWC_LOGE("Failed to initialize VSYNC thread!!");
        abort();
    }
    g_vsync_thread->initialize(!m_data[0].has_vsync, m_data[0].refresh);

    g_uevent_thread = new UEventThread();
    if (g_uevent_thread == NULL)
    {
        HWC_LOGE("Failed to initialize UEvent thread!!");
        abort();
    }
    g_uevent_thread->initialize();
}

DisplayManager::~DisplayManager()
{
    m_listener = NULL;
    free(m_data);
}

void DisplayManager::printDisplayInfo(int dpy)
{
    if (dpy < 0 || dpy >= MAX_DISPLAYS) return;

    DisplayData* disp_data = &m_data[dpy];

    HWC_LOGI("------------------------------------");
    HWC_LOGI("Device id   : %d",   dpy);
    HWC_LOGI("Width       : %d",   disp_data->width);
    HWC_LOGI("Height      : %d",   disp_data->height);
    HWC_LOGI("xdpi        : %f",   disp_data->xdpi);
    HWC_LOGI("ydpi        : %f",   disp_data->ydpi);
    HWC_LOGI("vsync       : %d",   disp_data->has_vsync);
    HWC_LOGI("refresh     : %lld", disp_data->refresh);
    HWC_LOGI("connected   : %d",   disp_data->connected);
    HWC_LOGI("subtype     : %d",   disp_data->subtype);
}

int DisplayManager::query(int what, int* value)
{
    switch (what)
    {
        case DISP_CURRENT_NUM:
            *value = m_curr_disp_num;
            break;

        default:
            return -EINVAL;
    }

    return 0;
}

void DisplayManager::dump(struct dump_buff* log)
{
    if (g_vsync_thread != NULL) g_vsync_thread->setProperty();

    if (g_uevent_thread != NULL) g_uevent_thread->setProperty();
}

void DisplayManager::setListener(const sp<EventListener>& listener)
{
    m_listener = listener;
}

void DisplayManager::requestVSync(int dpy, bool enabled)
{
    g_vsync_thread->setEnabled(enabled);
}

void DisplayManager::requestNextVSync()
{
    g_vsync_thread->setLoopAgain();
}

void DisplayManager::vsync(int dpy, nsecs_t timestamp, bool enabled)
{
    if (m_listener != NULL)
        m_listener->onVSync(dpy, timestamp, enabled);
}

void DisplayManager::hotplugExt(int dpy, bool connected, bool fake)
{
    if (dpy != HWC_DISPLAY_EXTERNAL)
    {
        HWC_LOGW("Failed to hotplug external disp(%d) connect(%d) !", dpy, connected);
        return;
    }

    HWC_LOGI("Hotplug external disp(%d) connect(%d) fake(%d)", dpy, connected, fake);

    DisplayData* disp_data = &m_data[HWC_DISPLAY_EXTERNAL];

    if (connected && !disp_data->connected)
    {
        if (m_listener != NULL) m_listener->onPlugIn(dpy);

        if (fake == true)
        {
            static int _s_shrink_size = 4;
            _s_shrink_size = (_s_shrink_size == 2) ? 4 : 2;
            memcpy(disp_data, &m_data[0], sizeof(DisplayData));
            disp_data->width   = m_data[0].width / _s_shrink_size;
            disp_data->height  = m_data[0].height / _s_shrink_size;
            disp_data->subtype = FAKE_DISPLAY;

            m_fake_disp_num++;
        }
        else
        {
            mtk_dispif_info_t info;
            HdmiDevice::getInstance().getDisplayInfo(dpy, &info);
            if (!info.isConnected)
            {
                HWC_LOGE("Failed to add display, hdmi is not connected!");
                return;
            }

            char value[PROPERTY_VALUE_MAX];
            property_get("ro.sf.lcd_density", value, "160");
            float density = atof(value);

            disp_data->width     = info.displayWidth;
            disp_data->height    = info.displayHeight;
            disp_data->format    = info.displayFormat;
            disp_data->xdpi      = info.physicalWidth == 0 ? density :
                                    (info.displayWidth * 25.4f / info.physicalWidth);
            disp_data->ydpi      = info.physicalHeight == 0 ? density :
                                    (info.displayHeight * 25.4f / info.physicalHeight);
            disp_data->has_vsync = info.isHwVsyncAvailable;
            disp_data->connected = info.isConnected;
            //disp_data->secure    = ;
            disp_data->subtype   = (info.displayType == HDMI_SMARTBOOK) ?
                                    HWC_DISPLAY_SMARTBOOK : HWC_DISPLAY_HDMI_MHL;

            float refreshRate = info.vsyncFPS / 100.0;
            if (0 >= refreshRate) refreshRate = 60.0;
            disp_data->refresh   = nsecs_t(1e9 / refreshRate);
        }

        hotplugPost(dpy, 1, DISP_PLUG_CONNECT);

        if (m_listener != NULL) m_listener->onHotPlugExt(dpy, 1);
    }
    else if (!connected && disp_data->connected)
    {
        if (fake == true) m_fake_disp_num--;

        hotplugPost(dpy, 0, DISP_PLUG_DISCONNECT);

        if (m_listener != NULL)
        {
            m_listener->onHotPlugExt(dpy, 0);
            m_listener->onPlugOut(dpy);
        }
    }
}

void DisplayManager::hotplugVir(int dpy, hwc_display_contents_1_t* list)
{
    if (dpy != HWC_DISPLAY_VIRTUAL)
    {
        HWC_LOGW("Failed to hotplug virtual disp(%d) !", dpy);
        return;
    }

    DisplayData* disp_data = &m_data[HWC_DISPLAY_VIRTUAL];
    bool connected = (list != NULL);

    if (connected == disp_data->connected) return;

    HWC_LOGW("Hotplug virtual disp(%d) connect(%d)", dpy, connected);

    if (connected)
    {
        if (list == NULL)
        {
            HWC_LOGE("Failed to add display, virtual list is NULL");
            return;
        }

        if (list->outbuf == NULL)
        {
            HWC_LOGE("Failed to add display, virtual outbuf is NULL");
            return;
        }

        hwc_buffer_info_t info;
        getBufferInfo(list->outbuf, &info);

        disp_data->width     = info.width;
        disp_data->height    = info.height;
        disp_data->format    = info.format;
        disp_data->xdpi      = m_data[HWC_DISPLAY_PRIMARY].xdpi;
        disp_data->ydpi      = m_data[HWC_DISPLAY_PRIMARY].ydpi;
        disp_data->has_vsync = false;
        disp_data->connected = true;
        disp_data->secure    = (info.usage & GRALLOC_USAGE_SECURE);
        disp_data->subtype   = HWC_DISPLAY_MEMORY;

        hotplugPost(dpy, 1, DISP_PLUG_CONNECT);

        if (m_listener != NULL) m_listener->onPlugIn(dpy);
    }
    else
    {
        hotplugPost(dpy, 0, DISP_PLUG_DISCONNECT);

        if (m_listener != NULL) m_listener->onPlugOut(dpy);
    }
}

void DisplayManager::hotplugPost(int dpy, bool connected, int state)
{
    DisplayData* disp_data = &m_data[dpy];

    switch (state)
    {
        case DISP_PLUG_CONNECT:
            HWC_LOGI("Added Display Information:");
            printDisplayInfo(dpy);
            m_curr_disp_num++;
            break;

        case DISP_PLUG_DISCONNECT:
            HWC_LOGI("Removed Display Information:");
            printDisplayInfo(dpy);
            memset((void*)disp_data, 0, sizeof(DisplayData));
            m_curr_disp_num--;
            break;

        case DISP_PLUG_NONE:
            HWC_LOGW("Unexpected hotplug: disp(%d:%d) connect(%d)",
                dpy, disp_data->connected, connected);
            return;
    };
}

void DisplayManager::setPowerMode(int dpy, int mode)
{
    g_vsync_thread->setPowerMode(dpy, mode);
}

