package com.mediatek.op07.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
//import android.support.v7.preference.PreferenceViewHolder;
import android.support.v7.preference.Preference;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Switch;

import com.android.ims.ImsManager;

import com.mediatek.op07.settings.R;
import com.mediatek.settings.ext.DefaultDataUsageSummaryExt;

public class OP07DataUsageSummaryExt extends DefaultDataUsageSummaryExt {

    private static final String TAG = "OP07DataUsageSummaryExt";
    private Context mContext;
    private Context mActivity;
    private DialogInterface mDialog;
    private View.OnClickListener mDialogListener;
    private TelephonyManager mTelephonyManager;
    private View mView;

    public OP07DataUsageSummaryExt(Context context) {
        super(context);
        mContext = context;
        mTelephonyManager = (TelephonyManager) mContext.
                getSystemService(Context.TELEPHONY_SERVICE);
    }

    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        boolean isEnable = mTelephonyManager.getDataEnabled();
            if (isNeedtoShowRoamingMsg() && !isEnable) {
                Log.d("@M_" + TAG, "create new dialog");
                int message = R.string.data_conn_under_roaming_hint;
                AlertDialog.Builder dialogBuild = new AlertDialog.Builder(mActivity);
                dialogBuild.setMessage(mContext.getText(message))
                .setTitle(android.R.string.dialog_alert_title)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setNegativeButton(android.R.string.cancel, null)
                .create();
                dialogBuild.setPositiveButton(mContext.getString(R.string.data_roaming_settings),
                        new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        mDialogListener.onClick(mView);
                        Intent i = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mActivity.startActivity(i);
                        }
                    });
                dialogBuild.show();
            } else {
                mDialogListener.onClick(v);
            }
         }
    };

    /**
     * app use to judge LTE open.
     * @param context Host activity context
     * @param dataEnabledView view to enable/disable mobile data
     * @param dataEnabledDialogListerner listener of dialog action
     */
    public void onBindViewHolder(Context context, View dataEnabledView,
            View.OnClickListener dataEnabledDialogListerner) {
        Log.d("@M_" + TAG, "setDataEnableClickListener");
        mView = dataEnabledView;
        mDialogListener = dataEnabledDialogListerner;
        dataEnabledView.setOnClickListener(mClickListener);
        mActivity = context;
    }

    private boolean isNeedtoShowRoamingMsg() {

        boolean isInRoaming = mTelephonyManager.isNetworkRoaming();
        boolean isRoamingEnabled = getDataRoaming();
        Log.d("@M_" + TAG, "isInRoaming=" + isInRoaming + " isRoamingEnabled=" + isRoamingEnabled);

        return (isInRoaming && !isRoamingEnabled);
    }

    private boolean getDataRoaming() {
        final ContentResolver resolver = mContext.getContentResolver();
        return Settings.Global.getInt(resolver, Settings.Global.DATA_ROAMING, 0) != 0;
    }

    /**
     * Customize when OP07
     * Set summary for mobile data switch.
     * @param dataPref : preference of mobile data item.
     */
    @Override
    public void setPreferenceSummary(Preference dataPref) {
        Log.d("@M_" + TAG, "setPreferenceSummary");
        String summaryText = "";
        boolean isDeviceVtCapable = ImsManager.isVtEnabledByPlatform(mContext);
        Log.d("@M_" + TAG, "isDeviceVtCapable :" + isDeviceVtCapable);
        if (isDeviceVtCapable) {
            summaryText = mContext.getString(R.string.mobile_data_summary_when_vilte_on);
        } else {
            summaryText = mContext.getString(R.string.mobile_data_summary_default);
        }
        Log.d("@M_" + TAG, "summaryText => " + summaryText);

        dataPref.setTitle(mContext.getString(R.string.mobile_data));
        dataPref.setSummary(summaryText);
    }
}
