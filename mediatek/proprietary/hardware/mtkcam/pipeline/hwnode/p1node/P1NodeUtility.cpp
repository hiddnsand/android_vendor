/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/P1NodeUtility"
//
#include "P1NodeCommon.h"
#include "P1NodeUtility.h"
//
#if 1
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  StuffBufferPool Implementation
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
StuffBufferPool::compareLayout(
    MINT32 format, MSize size,
    MUINT32 stride0, MUINT32 stride1, MUINT32 stride2)
{
    return ((format == mFormat) &&
            (stride0 == mStride0) &&
            (stride1 == mStride1) &&
            (stride2 == mStride2) &&
            (size == mSize));
};

/******************************************************************************
 *
 ******************************************************************************/
MERROR
StuffBufferPool::
acquireBuffer(sp<IImageBuffer> & imageBuffer)
{
    FUNCTION_IN;
    //
    MERROR ret = OK;
    sp<IImageBuffer> pImgBuf = NULL;
    BufNote bufNote;
    size_t i = 0;
    imageBuffer = NULL;
    //
    for (i = 0; i < mvInfoMap.size(); i++) {
        bufNote = mvInfoMap.editValueAt(i);
        if (BUF_STATE_RELEASED == bufNote.mState) {
            sp<IImageBuffer> const pImageBuffer = mvInfoMap.keyAt(i);
            bufNote.mState = BUF_STATE_ACQUIRED;
            mvInfoMap.replaceValueAt(i, bufNote);
            pImgBuf = pImageBuffer;
            break;
        }
    }
    //
    if (pImgBuf != NULL) {
        MY_LOGD_IF(mLogLevel > 1,
            "Acquire Stuff Buffer (%s) index(%zu) (%zu/%d)",
            bufNote.msName.string(), i, mvInfoMap.size(), mWaterMark);
        if (!(pImgBuf->lockBuf(bufNote.msName.string(), mUsage))) {
            MY_LOGE("[%s] Stuff ImgBuf lock fail", bufNote.msName.string());
            return BAD_VALUE;
        }
        imageBuffer = pImgBuf;
        return OK;
    }
    //
    MY_LOGD_IF(mLogLevel > 1, "StuffBuffer-Acquire (NoAvailable) (%zu/%d)",
            mvInfoMap.size(), mWaterMark);
    //
    ret = createBuffer(imageBuffer);
    FUNCTION_OUT;
    return ret;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
StuffBufferPool::
releaseBuffer(sp<IImageBuffer> & imageBuffer)
{
    FUNCTION_IN;
    //
    MERROR ret = OK;
    if (imageBuffer == NULL) {
        MY_LOGW("Stuff ImageBuffer not exist");
        return BAD_VALUE;
    }
    ssize_t index = mvInfoMap.indexOfKey(imageBuffer);
    if (index < 0 || (size_t)index >= mvInfoMap.size()) {
        MY_LOGW("ImageBuffer(%p) not found (%zd/%zu)",
            imageBuffer.get(), index, mvInfoMap.size());
        return BAD_VALUE;
    }
    imageBuffer->unlockBuf(mvInfoMap.valueAt(index).msName.string());
    BufNote bufNote = mvInfoMap.editValueAt(index);
    bufNote.mState = BUF_STATE_RELEASED;
    mvInfoMap.replaceValueAt(index, bufNote);
    //
    if (mvInfoMap.size() > mWaterMark) {
        ret = destroyBuffer(index);
    }
    //
    MY_LOGD_IF(mLogLevel > 1, "StuffBuffer-Release (%s) index(%zu) (%zu/%d)",
        bufNote.msName.string(), index, mvInfoMap.size(), mWaterMark);
    //
    FUNCTION_OUT;
    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
StuffBufferPool::
createBuffer(sp<IImageBuffer> & imageBuffer)
{
    FUNCTION_IN;
    //
    imageBuffer = NULL;
    // add information to buffer name
    android::String8 imgBufName = android::String8(msName);
    char str[256] = {0};
    snprintf(str, sizeof(str), ":Size%dx%d:Stride%d.%d.%d:Sn%d",
        mSize.w, mSize.h, mStride0, mStride1, mStride2, ++mSerialNum);
    imgBufName += str;
    //
    if (mvInfoMap.size() >= mMaxAmount) {
        MY_LOGW("[%s] the pool size is over max amount, "
            "please check the buffer usage and situation (%zu/%d)",
            imgBufName.string(), mvInfoMap.size(), mMaxAmount);
        return NO_MEMORY;
    }
    // create buffer
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {mStride0, mStride1, mStride2};
    if (mPlaneCnt == 0) { // ref. StuffBufferPool::CTR mStride0/1/2 checking
        MY_LOGE("[%s] Stuff ImageBufferHeap stride invalid (%d.%d.%d)",
            imgBufName.string(), mStride0, mStride1, mStride2);
        return BAD_VALUE;
    }
    IImageBufferAllocator::ImgParam imgParam =
        IImageBufferAllocator::ImgParam((EImageFormat)mFormat,
            mSize, bufStridesInBytes, bufBoundaryInBytes, (size_t)mPlaneCnt);
    sp<IIonImageBufferHeap> pHeap =
        IIonImageBufferHeap::create(imgBufName.string(), imgParam);
    if (pHeap == NULL) {
        MY_LOGE("[%s] Stuff ImageBufferHeap create fail", imgBufName.string());
        return BAD_VALUE;
    }
    sp<IImageBuffer> pImgBuf = pHeap->createImageBuffer();
    if (pImgBuf == NULL) {
        MY_LOGE("[%s] Stuff ImageBuffer create fail", imgBufName.string());
        return BAD_VALUE;
    }
    // lock buffer
    if (!(pImgBuf->lockBuf(imgBufName.string(), mUsage))) {
        MY_LOGE("[%s] Stuff ImageBuffer lock fail", imgBufName.string());
        return BAD_VALUE;
    }
    BufNote bufNote(imgBufName, BUF_STATE_ACQUIRED);
    mvInfoMap.add(pImgBuf, bufNote);
    imageBuffer = pImgBuf;
    //
    MY_LOGD_IF(mLogLevel > 1, "StuffBuffer-Create (%s) (%zu/%d) "
        "ImgBuf(%p)(0x%X)(%dx%d,%zu,%zu)(P:0x%zx)(V:0x%zx)",
        imgBufName.string(), mvInfoMap.size(), mWaterMark,
        imageBuffer.get(), imageBuffer->getImgFormat(),
        imageBuffer->getImgSize().w, imageBuffer->getImgSize().h,
        imageBuffer->getBufStridesInBytes(0), imageBuffer->getBufSizeInBytes(0),
        imageBuffer->getBufPA(0), imageBuffer->getBufVA(0));
    //
    FUNCTION_OUT;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
StuffBufferPool::
destroyBuffer(sp<IImageBuffer> & imageBuffer)
{
    FUNCTION_IN;
    //
    MERROR ret = OK;
    if (imageBuffer == NULL) {
        MY_LOGW("Stuff ImageBuffer not exist");
        return BAD_VALUE;
    }
    //
    ssize_t index = mvInfoMap.indexOfKey(imageBuffer);
    if (index < 0 || (size_t)index >= mvInfoMap.size()) {
        MY_LOGW("ImageBuffer(%p) not found (%zd/%zu)",
            imageBuffer.get(), index, mvInfoMap.size());
        return BAD_VALUE;
    }
    ret = destroyBuffer(index);
    FUNCTION_OUT;
    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
StuffBufferPool::
destroyBuffer(size_t index)
{
    FUNCTION_IN;
    //
    if (index >= mvInfoMap.size()) {
        MY_LOGW("index(%zu) not exist, size(%zu)", index, mvInfoMap.size());
        return BAD_VALUE;
    }
    BufNote bufNote = mvInfoMap.valueAt(index);
    sp<IImageBuffer> const pImageBuffer = mvInfoMap.keyAt(index);
    MY_LOGD_IF(mLogLevel > 1, "StuffBuffer-Destroy (%s) index(%zu) state(%d) "
        "(%zu/%d)", bufNote.msName.string(), index, bufNote.mState,
        mvInfoMap.size(), mWaterMark);
    if (bufNote.mState == BUF_STATE_ACQUIRED) {
        sp<IImageBuffer> pImgBuf = pImageBuffer;
        pImgBuf->unlockBuf(bufNote.msName.string());
    }
    // destroy buffer
    mvInfoMap.removeItemsAt(index);
    //pImgBuf = NULL;
    //
    FUNCTION_OUT;
    return OK;
}

#endif


#if 1
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  StuffBufferManager Implementation
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
 *
 ******************************************************************************/
MERROR
StuffBufferManager::
acquireStoreBuffer(sp<IImageBuffer> & imageBuffer,char const* szName,
    MINT32 format, MSize size, Vector<MUINT32> vStride,
    MUINT8 multiple, MBOOL writable)
{
    FUNCTION_IN;
    //
    //CAM_TRACE_FMT_BEGIN("Stuff(%dx%d)%s", size.w, size.h, szName);
    //
    Mutex::Autolock _l(mLock);
    MERROR ret = OK;
    //
    StuffBufferPool* bufPool = NULL;
    imageBuffer = NULL;
    //
    MUINT32 stride[3] = {0, 0, 0};
    size_t count = vStride.size();
    if (count > 3) {
        MY_LOGW("Fmt:0x%x (%dx%d) Cnt(%zu)", format, size.w, size.h, count);
        count = 3;
    }
    for (size_t i = 0; i < count; i++) {
        stride[i] = vStride[i];
    }
    //
    Vector<StuffBufferPool>::iterator it = mvPoolSet.begin();
    for(; it != mvPoolSet.end(); it++) {
        if (it->compareLayout(format, size, stride[0], stride[1], stride[2])) {
            bufPool = it;
            break;
        }
    }
    //
    if (bufPool == NULL) {
        StuffBufferPool newPool(szName, format, size,
            stride[0], stride[1], stride[2],
            multiple, writable, mLogLevel);
        mvPoolSet.push_back(newPool);
        it = mvPoolSet.end();
        bufPool = (it - 1);
        MY_LOGD_IF(mLogLevel > 0, "PoolSet.size(%zu)", mvPoolSet.size());
    }
    //
    if (bufPool == NULL) {
        MY_LOGE("Cannot create stuff buffer pool");
        return BAD_VALUE;
    } else {
        ret = bufPool->acquireBuffer(imageBuffer);
    }
    //
    //CAM_TRACE_FMT_END();
    //
    FUNCTION_OUT;
    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
StuffBufferManager::
releaseStoreBuffer(sp<IImageBuffer> & imageBuffer)
{
    FUNCTION_IN;
    //
    Mutex::Autolock _l(mLock);
    //
    if (imageBuffer == NULL) {
        MY_LOGW("Stuff ImageBuffer not exist");
        return BAD_VALUE;
    }
    //
    MINT const format = imageBuffer->getImgFormat();
    MSize const size = imageBuffer->getImgSize();
    MUINT32 stride[3] = {0, 0, 0};
    size_t count = imageBuffer->getPlaneCount();
    if (count > 3) {
        MY_LOGW("ImageBuffer Fmt:0x%x (%dx%d) PlaneCount(%zu)",
            imageBuffer->getImgFormat(), imageBuffer->getImgSize().w,
            imageBuffer->getImgSize().h, count);
        count = 3;
    }
    for (size_t i = 0; i < count; i++) {
        stride[i] = imageBuffer->getBufStridesInBytes(i);
    }
    //
    StuffBufferPool* bufPool = NULL;
    Vector<StuffBufferPool>::iterator it = mvPoolSet.begin();
    for(; it != mvPoolSet.end(); it++) {
        if (it->compareLayout(format, size, stride[0], stride[1], stride[2])) {
            bufPool = it;
            break;
        }
    }
    //
    if (bufPool == NULL) {
        MY_LOGE("Cannot find stuff buffer pool");
        return BAD_VALUE;
    } else {
        bufPool->releaseBuffer(imageBuffer);
    }
    //
    FUNCTION_OUT;
    return OK;
}

#endif


#if 1
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  TimingChecker Implementation
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
 *
 ******************************************************************************/
void
TimingChecker::Client::action(void)
{
    Mutex::Autolock _l(mLock);
    switch (mType) {
        case EVENT_TYPE_WARNING:
            MY_LOGW("[TimingChecker-W] [%s] (%dms) "
                "= ( %" PRId64 " - %" PRId64 " ns)",
                mStr.string(), mTimeInvMs,
                mEndTsNs, mBeginTsNs);
            break;
        case EVENT_TYPE_ERROR:
            MY_LOGE("[TimingChecker-E] [%s] (%dms) "
                "= ( %" PRId64 " - %" PRId64 " ns)",
                mStr.string(), mTimeInvMs,
                mEndTsNs, mBeginTsNs);
            break;
        case EVENT_TYPE_FATAL:
            MY_LOGF("[TimingChecker-F] [%s] (%dms) "
                "= ( %" PRId64 " - %" PRId64 " ns)",
                mStr.string(), mTimeInvMs,
                mEndTsNs, mBeginTsNs);
            // AEE trigger
            break;
        default: // EVENT_TYPE_NONE:
            // do nothing
            break;
    }
};

/******************************************************************************
 *
 ******************************************************************************/
void
TimingChecker::Client::onLastStrongRef(const void* /*id*/)
{
    dump("TC_Client::onLastStrongRef");
};

/******************************************************************************
 *
 ******************************************************************************/
void
TimingChecker::Client::dump(char const * tag)
{
    Mutex::Autolock _l(mLock);
    if (mLog) {
        char const * str =
            (tag != NULL) ? tag : "NULL";
        MY_LOGI("[%s][%s] (%dms) = "
            "( %" PRId64 " - %" PRId64 " ns)",
            str, mStr.string(), mTimeInvMs,
            mEndTsNs, mBeginTsNs);
    }
};

/******************************************************************************
 *
 ******************************************************************************/
size_t
TimingChecker::RecStore::size(void)
{
    return mHeap.size();
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
TimingChecker::RecStore::isEmpty(void)
{
    return mHeap.empty();
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
TimingChecker::RecStore::addRec(RecPtr rp)
{
    if (rp == NULL) {
        return MFALSE;
    }
    //P1_CAM_TRACE_FUNC;
    mHeap.push(rp);
    //dump("AddRec");
    return MTRUE;
};

/******************************************************************************
 *
 ******************************************************************************/
TimingChecker::RecPtr const &
TimingChecker::RecStore::getMin(void)
{
    //P1_CAM_TRACE_FUNC;
    return mHeap.top();
};

/******************************************************************************
 *
 ******************************************************************************/
void
TimingChecker::RecStore::delMin(void)
{
    //P1_CAM_TRACE_FUNC;
    mHeap.pop();
    //dump("DelMin");
};

/******************************************************************************
 *
 ******************************************************************************/
void
TimingChecker::RecStore::dump(char const * tag)
{
    RecPtr* pTop = (RecPtr*)(&(mHeap.top()));
    MY_LOGI("RecPtrHeap @ %s",
        (tag != NULL) ? tag : "NULL");
    #if 1
    MY_LOGI("RecPtrHeap[0/%zu]@(%p) = (%p) "
            "( %" PRId64 " ns)",
            mHeap.size(), (void*)(pTop),
            (void*)(*pTop), (*pTop)->mTimeMarkNs);
    #else
    RecPtr* pCur = pTop;
    for (size_t i = 0; i < mHeap.size(); i++) {
        pCur = pTop + i;
        MY_LOGI("RecPtrHeap[%zu/%zu]@(%p) = (%p) "
            "( %" PRId64 " ns)",
            i, mHeap.size(), (void*)(pCur),
            (void*)(*pCur), (*pCur)->mTimeMarkNs);
    }
    #endif
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
TimingChecker::doThreadLoop(void)
{
    Mutex::Autolock _l(mLock);
    //mData.clear();
    mWakeTiming = 0;
    mExitPending = MFALSE;
    mRunning = MTRUE;
    mEnterCond.broadcast();
    /*
        For less affecting, the TimingChecker caller
        might not wait for this thread loop ready.
        Hence, it checks the current time with the
        registered client's timing mark directly.
    */
    while (!mExitPending) {
        //MY_LOGD("TC_Loop" " (%" PRId64 ") ", mWakeTiming);
        nsecs_t current = ::systemTime();
        if (mWakeTiming <= current) {
            mWakeTiming = checkList(current);
            //
            if (mWakeTiming == 0) {
                //CAM_TRACE_NAME("TC_wait()");
                //MY_LOGD("TC_Loop" " (%" PRId64 ") " " wait +", mWakeTiming);
                mClientCond.wait(mLock);
                //MY_LOGD("TC_Loop" " (%" PRId64 ") " " wait -", mWakeTiming);
            }
            //MY_LOGD("TC_Loop" " (%" PRId64 ") " " continue", mWakeTiming);
            continue;
        }
        //
        nsecs_t sleep = mWakeTiming - current;
        //CAM_TRACE_FMT_BEGIN("TC_wait(%dms)", (MUINT32)(sleep / ONE_MS_TO_NS));
        //MY_LOGD("TC_Loop" " (%" PRId64 ") " " waitRelative + sleep" " (%" PRId64 ") ", mWakeTiming, sleep);
        mClientCond.waitRelative(mLock, sleep);
        //MY_LOGD("TC_Loop" " (%" PRId64 ") " " waitRelative - sleep" " (%" PRId64 ") ", mWakeTiming, sleep);
        //CAM_TRACE_FMT_END();
    }
    mRunning = MFALSE;
    mExitedCond.broadcast();
    return MFALSE;
};

/******************************************************************************
 *
 ******************************************************************************/
void
TimingChecker::doRequestExit(void)
{
    Mutex::Autolock _l(mLock);
    mWakeTiming = 0;
    mExitPending = MTRUE;
    //CAM_TRACE_NAME("TC_exit()");
    mEnterCond.broadcast();
    mClientCond.broadcast();
    // join loop
    while (mRunning) {
        //CAM_TRACE_NAME("TC_join()");
        mExitedCond.waitRelative(mLock, ONE_MS_TO_NS);
    };
    // clear data
    while (!mData.isEmpty()) {
        //CAM_TRACE_NAME("TC_clean()");
    #if 0
        MY_LOGI("RecordStore NOT empty, size(%zu),"
            " (%" PRId64 ") ", mData.size(),
            mData.getMin()->mTimeMarkNs);
    #endif
        sp<TimingChecker::Client> c =
            mData.getMin()->mpClient.promote();
        if (c != NULL) {
            c->setLog(MTRUE);
            c->dump("RecordStoreCleaning");
        } else {
            //MY_LOGI("Client was already released");
        }
        delete (mData.getMin());
        mData.delMin();
    }
};

/******************************************************************************
 *
 ******************************************************************************/
void
TimingChecker::doWaitReady(void)
{
    Mutex::Autolock _l(mLock);
    while (!mRunning && !mExitPending) {
        //CAM_TRACE_NAME("TC_ready()");
        mEnterCond.waitRelative(mLock, ONE_MS_TO_NS);
    };
};

/******************************************************************************
 *
 ******************************************************************************/
sp<TimingChecker::Client>
TimingChecker::createClient(
    char const * str,
    MUINT32 uTimeoutMs,
    EVENT_TYPE eType)
{
    //P1_CAM_TRACE_FUNC;
    sp<TimingChecker::Client> client = new
        TimingChecker::Client(str, uTimeoutMs, eType);
    //MY_LOGD("new sp client: s(%d) w(%d)", client->getStrongCount(), client->getWeakRefs()->getWeakCount());
    if (client == NULL) {
        MY_LOGE("CANNOT create TimingCheckerClient "
            "[%s]", str);
        return NULL;
    }
    {
        Mutex::Autolock _l(mLock);
        nsecs_t ts = client->getTimeStamp();
        RecPtr pRec = new Record(ts, client);
        if (pRec == NULL || !(mData.addRec(pRec))) {
            MY_LOGE("CANNOT new Record");
            client = NULL;
            return NULL;
        }
        if (mWakeTiming == 0 || ts < mWakeTiming) {
            //CAM_TRACE_NAME("TC_broadcast()");
            mClientCond.broadcast();
        }
    }
    return client;
};

/******************************************************************************
 *
 ******************************************************************************/
nsecs_t
TimingChecker::checkList(nsecs_t time)
{
    nsecs_t ts = 0;
    RecPtr pRec = NULL;
    //MY_LOGD("checkList +++ [%zu] @" " (%" PRId64 ") ", mData.size(), time);
    while (!mData.isEmpty()) {
        pRec = mData.getMin();
        if (pRec == NULL) {
            MY_LOGE("Error Record in Store");
            ts = 0;
            break;
        }
        ts = pRec->mTimeMarkNs;
        if (ts <= time) {
            sp<TimingChecker::Client> c =
                pRec->mpClient.promote();
            if (c != NULL) {
                c->action();
            }
            delete pRec;
            mData.delMin();
            ts = 0;
        } else {
            //MY_LOGD("checkList : NO Record needs to Pop-up");
            break;
        }
    }
    //MY_LOGD("checkList --- (%" PRId64 ")", ts);
    return ts;
};

#endif


#if 1
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  TimingCheckerMgr Implementation
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
 *
 ******************************************************************************/
void
TimingCheckerMgr::setEnable(MBOOL en)
{
    if (mpTimingChecker == NULL) {
        Mutex::Autolock _l(mLock);
        mIsEn = MFALSE;
        return;
    }
    MY_LOGD("TimingChecker enable(%d)", en);
    {
        Mutex::Autolock _l(mLock);
        mIsEn = en;
        if (mIsEn) {
            return;
        }
    }
    // as (mIsEn == false)
    mpTimingChecker->doRequestExit();
};

/******************************************************************************
 *
 ******************************************************************************/
void
TimingCheckerMgr::waitReady(void)
{
    if (mpTimingChecker == NULL) {
        return;
    }
    // no waiting for less affecting
    //mpTimingChecker->doWaitReady();
};

/******************************************************************************
 *
 ******************************************************************************/
void
TimingCheckerMgr::onCheck(void)
{
    if (mpTimingChecker == NULL) {
        return;
    }
    {
        Mutex::Autolock _l(mLock);
        if (!mIsEn) {
            return;
        }
    }
    // as (mIsEn == true)
    if (mpTimingChecker->doThreadLoop()) {
        MY_LOGD("TimingChecker next loop");
    }
};

/******************************************************************************
 *
 ******************************************************************************/
sp<TimingChecker::Client>
TimingCheckerMgr::createClient(
    char const * str, MUINT32 uTimeoutMs,
    TimingChecker::EVENT_TYPE eType)
{
    if (mpTimingChecker == NULL) {
        return NULL;
    }
    return mpTimingChecker->createClient
        (str, uTimeoutMs * mFactor, eType);
};

#endif


#if 1
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  LongExposureStatus Implementation
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
LongExposureStatus::reset(MINT num)
{
    Mutex::Autolock _l(mLock);
    if (mvSet.empty()) {
        return MFALSE;
    }
    Vector<MINT32>::iterator it = mvSet.begin();
    for (; it != mvSet.end(); it++) {
        if (num == *it) {
            mvSet.erase(it);
            break;
        }
    }
    if (mvSet.empty()) {
        mRunning = MFALSE;
    }
    MY_LOGI("(%d/%zu) LongExposure[%d]",
            num, mvSet.size(), mRunning);
    return MTRUE;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
LongExposureStatus::set(MINT num, MINT64 exp_ns)
{
    Mutex::Autolock _l(mLock);
    if (exp_ns >= mThreshold && num > 0) {
        MBOOL isFound = MFALSE;
        Vector<MINT32>::iterator it = mvSet.begin();
        for (; it != mvSet.end(); it++) {
            if (num == *it) {
                isFound = MTRUE;
                break;
            }
        }
        if (!isFound) {
            mvSet.push_back(num);
            mRunning = MTRUE;
        }
        MY_LOGI("(%d/%zu) LongExposure[%d]",
                num, mvSet.size(), mRunning);
        return MTRUE;
    }
    return MFALSE;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
LongExposureStatus::get(void)
{
    Mutex::Autolock _l(mLock);
    MBOOL isRunning = mRunning;
    #if 0 //(!SUPPORT_LONG_EXPOSURE_ABORT)
    isRunning = MFALSE;
    #endif
    return isRunning;
};

#endif


#if 1
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  ProcedureStageControl Implementation
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
ProcedureStageControl::reset(void)
{
    for (MUINT32 i = 0; i < mvpStage.size(); i++) {
        sp<StageNote> p = mvpStage.editItemAt(i);
        Mutex::Autolock _l(p->mLock);
        if (p->mWait) {
            p->mCond.broadcast();
        }
        p->mWait = MFALSE;
        p->mDone = MFALSE;
        p->mSuccess = MFALSE;
    }
    MY_LOGI("StageCtrl reset OK");
    return MTRUE;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
ProcedureStageControl::wait(MUINT32 eStage, MBOOL& rSuccess)
{
    if (eStage >= mvpStage.size()) {
        MY_LOGW("wait - illegal (%d >= %zu)",
            eStage, mvpStage.size());
        return MFALSE;
    }
    //
    {
        sp<StageNote> p = mvpStage.editItemAt(eStage);
        Mutex::Autolock _l(p->mLock);
        if (!p->mDone) {
            CAM_TRACE_FMT_BEGIN("S_Wait(%d)", p->mId);
            MY_LOGI("StageCtrl waiting(%d)", eStage);
            p->mWait = MTRUE;
            p->mCond.wait(p->mLock);
            CAM_TRACE_FMT_END();
        }
        p->mWait = MFALSE;
        rSuccess = p->mSuccess;
    }
    MY_LOGI("StageCtrl wait(%d) OK", eStage);
    return MTRUE;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
ProcedureStageControl::done(MUINT32 eStage, MBOOL bSuccess)
{
    if (eStage >= mvpStage.size()) {
        MY_LOGW("done - illegal (%d >= %zu)",
            eStage, mvpStage.size());
        return MFALSE;
    }
    //
    {
        sp<StageNote> p = mvpStage.editItemAt(eStage);
        Mutex::Autolock _l(p->mLock);
        p->mDone = MTRUE;
        p->mSuccess = bSuccess;
        if (p->mWait) {
            MY_LOGI("StageCtrl signal(%d)", eStage);
            p->mCond.broadcast();
        }
    }
    MY_LOGI("StageCtrl done(%d) OK", eStage);
    return MTRUE;
};

#endif


#if 1
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  ConcurrenceControl Implementation
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
ConcurrenceControl::initBufInfo_clean(void)
{
    Mutex::Autolock _l(mLock);
    if (mpBufInfo != NULL) {
        delete mpBufInfo;
        mpBufInfo = NULL;
        return MTRUE;
    }
    return MFALSE;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
ConcurrenceControl::initBufInfo_get(QBufInfo** ppBufInfo)
{
    Mutex::Autolock _l(mLock);
    if (mpBufInfo == NULL) {
        *ppBufInfo = NULL;
        return MFALSE;
    }
    *ppBufInfo = mpBufInfo;
    return MTRUE;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
ConcurrenceControl::initBufInfo_create(QBufInfo** ppBufInfo)
{
    Mutex::Autolock _l(mLock);
    if (mpBufInfo != NULL) {
        delete mpBufInfo;
        mpBufInfo = NULL;
    }
    //
    mpBufInfo = new QBufInfo();
    //
    if (mpBufInfo == NULL) {
        *ppBufInfo = NULL;
        return MFALSE;
    }
    *ppBufInfo = mpBufInfo;
    return MTRUE;
};

/******************************************************************************
 *
 ******************************************************************************/
void
ConcurrenceControl::setAidUsage(MBOOL enable)
{
    Mutex::Autolock _l(mLock);
    mIsAssistUsing = enable;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
ConcurrenceControl::getAidUsage(void)
{
    Mutex::Autolock _l(mLock);
    return mIsAssistUsing;
};

/******************************************************************************
 *
 ******************************************************************************/
void
ConcurrenceControl::cleanAidStage(void)
{
    setAidUsage(MFALSE);
    if (getStageCtrl() != NULL) {
        getStageCtrl()->reset();
    }
};

/******************************************************************************
 *
 ******************************************************************************/
sp<ProcedureStageControl>
ConcurrenceControl::getStageCtrl(void)
{
    return mpStageCtrl;
};

#endif


#if 1
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  HardwareStateControl Implementation
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
 *
 ******************************************************************************/
void
HardwareStateControl::config(
    MINT32 nLogLevel,
    MINT32 nOpenId,
    MUINT8 nBurstNum,
    INormalPipe* pCamIO,
    IHal3A_T* p3A
)
{
    Mutex::Autolock _l(mLock);
    mLogLevel = nLogLevel;
    mOpenId = nOpenId;
    mBurstNum = nBurstNum;
    mpCamIO = pCamIO;
    mp3A = p3A;
    mState = STATE_NORMAL;
    mStandbySetNum = 0;
    mStreamingSetNum = 0;
    mShutterTimeUs = 0;
    mRequestPass = MFALSE;
    mvStoreNum.clear();
};


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HardwareStateControl::isActive(void)
{
    #if 0 // force to disable HardwareStateControl
    return MFALSE;
    #endif
    // by DRV comment, SUSPEND is not supported in burst mode
    return ((mBurstNum == 1) && (mpCamIO != NULL) && (mp3A != NULL));
};


/******************************************************************************
 *
 ******************************************************************************/
void
HardwareStateControl::checkReceiveFrame(IMetadata* pMeta)
{
    if (!isActive()) {
        return;
    }
    //
    Mutex::Autolock _l(mLock);
    MINT32 ctrl = MTK_P1_SENSOR_STATUS_NONE;
    MBOOL tag = MFALSE;
    //
    if (tryGetMetadata<MINT32>(pMeta, MTK_P1NODE_SENSOR_STATUS, ctrl)) {
        tag = MTRUE;
        if (ctrl == MTK_P1_SENSOR_STATUS_SW_STANDBY ||
            ctrl == MTK_P1_SENSOR_STATUS_HW_STANDBY) {
            switch (mState) {
                case STATE_NORMAL:
                    mState = STATE_SUS_WAIT_NUM;
                    break;
                default:
                    break;
            };
            MY_LOGI("[Cam::%d][SUS-RES] meta-sus(%d) @(%d)",
                mOpenId, ctrl, mState);
        } else if (ctrl == MTK_P1_SENSOR_STATUS_STREAMING) {
            switch (mState) {
                case STATE_SUS_DONE:
                    mState = STATE_RES_WAIT_NUM;
                    break;
                default:
                    break;
            };
            MY_LOGI("[Cam::%d][SUS-RES] meta-res(%d) @(%d)",
                mOpenId, ctrl, mState);
        }
    }
    MY_LOGD_IF(mLogLevel > 2,
        "tag(%d) : sensor(%d) - state(%d)", tag, ctrl, mState);
    if (mState == STATE_RES_WAIT_NUM) {
        mShutterTimeUs = (MINT32)0;
        if (tryGetMetadata<MINT32>(pMeta, MTK_P1NODE_RESUME_SHUTTER_TIME_US,
            mShutterTimeUs)) {
            MY_LOGI("[Cam::%d][SUS-RES] re-streaming with (%d)us",
                mOpenId, mShutterTimeUs);
        } else {
            MY_LOGI("[Cam::%d][SUS-RES] re-streaming without time-set",
                mOpenId);
        }
    }
    return;
};


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HardwareStateControl::checkReceiveRestreaming(void)
{
    if (!isActive()) {
        return MFALSE;
    }
    return (mState == STATE_RES_WAIT_NUM) ? MTRUE : MFALSE;
}


/******************************************************************************
 *
 ******************************************************************************/
void
HardwareStateControl::checkReceiveNode(MINT32 num, MBOOL bSkipEnQ,
    MBOOL & rIsNeedEnQ, MINT32 & rShutterTimeUs)
{
    if (!isActive()) {
        return;
    }
    //
    Mutex::Autolock _l(mLock);
    rIsNeedEnQ = MFALSE;
    if (mState == STATE_SUS_WAIT_NUM) {
        mStandbySetNum = num;
        mRequestPass = MFALSE;
        mState = STATE_SUS_WAIT_SYNC;
        MY_LOGI("[Cam::%d][SUS-RES] StandbySet(%d) @(%d)",
            mOpenId, mStandbySetNum, mState);
    } else if (mState == STATE_RES_WAIT_NUM) {
        mStreamingSetNum = num;
        mRequestPass = MFALSE;
        //
        rIsNeedEnQ = MFALSE;
        rShutterTimeUs = mShutterTimeUs;
        if (!bSkipEnQ) {
            #if P1_SUPPORT_DIR_RESTREAM
            // need check DRV support
            rIsNeedEnQ = MTRUE;
            #endif
        } // else force to skip the directly EnQ flow
        //
        if (!rIsNeedEnQ) { // NO Directly EnQ flow
            MBOOL ret = MFALSE;
            CAM_TRACE_FMT_BEGIN("P1:DRV-resume(%d)", mShutterTimeUs);
            ret = mpCamIO->resume(mShutterTimeUs);
            CAM_TRACE_FMT_END();
            if (!ret) {
                MY_LOGE("[Cam::%d][SUS-RES] FAIL : num-res(%d) EnQ(%d:%d) "
                    "@(%d)", mOpenId, num, bSkipEnQ, rIsNeedEnQ, mState);
                mState = STATE_NORMAL;
                mStandbySetNum = 0;
                mStreamingSetNum = 0;
                mShutterTimeUs = 0;
                mRequestPass = MFALSE;
                mvStoreNum.clear();
                return;
            }
            //
            CAM_TRACE_FMT_BEGIN("P1:3A-resume");
            mp3A->resume();
            CAM_TRACE_FMT_END();
            //
            MY_LOGI("[Cam::%d][SUS-RES] Recover-Loop-N", mOpenId);
            mThreadCond.broadcast();
        } else { // Directly EnQ flow
            CAM_TRACE_FMT_BEGIN("P1:3A-resume(%d)", mStreamingSetNum);
            mp3A->resume(mStreamingSetNum);
            CAM_TRACE_FMT_END();
        }
        //
        mState = STATE_RES_WAIT_SYNC;
        MY_LOGI("[Cam::%d][SUS-RES] SkipEnQ(%d) NeedEnQ(%d) StreamingSet(%d) "
            "@(%d)", mOpenId, bSkipEnQ, rIsNeedEnQ, mStreamingSetNum, mState);
    }
    return;
};


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HardwareStateControl::checkSetNum(MINT32 num)
{
    if (!isActive()) {
        return MFALSE;
    }
    //
    Mutex::Autolock _l(mLock);

    if (mState == STATE_SUS_WAIT_SYNC && mStandbySetNum == num) {
        CAM_TRACE_FMT_BEGIN("P1:3A-pause");
        mp3A->pause();
        CAM_TRACE_FMT_END();
        //
        MBOOL ret = MFALSE;
        CAM_TRACE_FMT_BEGIN("P1:DRV-suspend");
        ret = mpCamIO->suspend();
        CAM_TRACE_FMT_END();
        if (!ret) {
            MY_LOGE("[Cam::%d][SUS-RES] FAIL : num-sus(%d) @(%d)",
                mOpenId, num, mState);
            mp3A->resume();
            mState = STATE_NORMAL;
            mStandbySetNum = 0;
            mStreamingSetNum = 0;
            mShutterTimeUs = 0;
            mRequestPass = MFALSE;
            mvStoreNum.clear();
            mRequestCond.broadcast();
            mThreadCond.broadcast();
            return MTRUE;
        }
        //
        mState = STATE_SUS_READY;
        mRequestCond.broadcast();
        MY_LOGI("[Cam::%d][SUS-RES] CurNum(%d) (%d/%d) @(%d)", mOpenId, num,
            mStandbySetNum, mStreamingSetNum, mState);
        return MTRUE;
    }
    return MFALSE;
};


/******************************************************************************
 *
 ******************************************************************************/
void
HardwareStateControl::checkRequest(void)
{
    if (!isActive()) {
        return;
    }
    //
    Mutex::Autolock _l(mLock);
    if (mState == STATE_SUS_WAIT_SYNC) {
        MY_LOGI("[Cam::%d][SUS-RES] Suspend-Request @(%d)", mOpenId, mState);
        CAM_TRACE_NAME("P1:pause");
        MY_LOGD("[Cam::%d][SUS-RES] wait pause +", mOpenId);
        mRequestCond.wait(mLock);
        MY_LOGD("[Cam::%d][SUS-RES] wait pause -", mOpenId);
    }
    return;
};


/******************************************************************************
 *
 ******************************************************************************/
void
HardwareStateControl::checkThreadStandby(void)
{
    if (!isActive()) {
        return;
    }
    //
    Mutex::Autolock _l(mLock);
    if (mState == STATE_SUS_READY) {
        mState = STATE_SUS_DONE;
        MY_LOGI("[Cam::%d][SUS-RES] Suspend-Loop @(%d)", mOpenId, mState);
        CAM_TRACE_NAME("P1:suspend");
        MY_LOGD("[Cam::%d][SUS-RES] wait re-streaming +", mOpenId);
        mThreadCond.wait(mLock);
        MY_LOGD("[Cam::%d][SUS-RES] wait re-streaming -", mOpenId);
    }
    return;
};


/******************************************************************************
 *
 ******************************************************************************/
void
HardwareStateControl::checkThreadWeakup(void)
{
    if (!isActive()) {
        return;
    }
    //
    Mutex::Autolock _l(mLock);
    if (mState == STATE_RES_WAIT_SYNC) {
        MY_LOGI("[Cam::%d][SUS-RES] Recover-Loop-W", mOpenId);
        mThreadCond.broadcast();
    }
    return;
};


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HardwareStateControl::checkFirstSync(void)
{
    if (!isActive()) {
        return MFALSE;
    }
    //
    Mutex::Autolock _l(mLock);
    if (mState == STATE_RES_WAIT_SYNC) {
        mState = STATE_RES_WAIT_DONE;
        MY_LOGI("[Cam::%d][SUS-RES] FirstSync (%d/%d) @(%d)",
            mOpenId, mStandbySetNum, mStreamingSetNum, mState);
        return MTRUE;
    }
    return MFALSE;
};


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HardwareStateControl::checkSkipSync(void)
{
    if (!isActive()) {
        return MFALSE;
    }
    //
    Mutex::Autolock _l(mLock);
    switch (mState) {
        case STATE_NORMAL:
        case STATE_SUS_WAIT_NUM:
        case STATE_SUS_WAIT_SYNC:
        case STATE_RES_WAIT_SYNC:
        case STATE_RES_WAIT_DONE:
            return MFALSE;
        //case STATE_RES_WAIT_NUM:
        //case STATE_SUS_READY:
        //case STATE_SUS_DONE:
        default:
            break;
    };
    MY_LOGI("[Cam::%d][SUS-RES] SkipSync (%d/%d) @(%d)",
        mOpenId, mStandbySetNum, mStreamingSetNum, mState);
    return MTRUE;
};


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HardwareStateControl::checkSkipWait(void)
{
    if (!isActive()) {
        return MFALSE;
    }
    //
    Mutex::Autolock _l(mLock);
    if (mRequestPass) {
        MY_LOGI("[Cam::%d][SUS-RES] SkipWait pass (%d/%d) @(%d)",
            mOpenId, mStandbySetNum, mStreamingSetNum, mState);
        mRequestPass = MFALSE;
        return MTRUE;
    }
    switch (mState) {
        case STATE_NORMAL:
        case STATE_SUS_WAIT_NUM:
        case STATE_SUS_WAIT_SYNC:
        case STATE_SUS_READY:
        case STATE_SUS_DONE:
        case STATE_RES_WAIT_SYNC:
        case STATE_RES_WAIT_DONE:
            return MFALSE;
        //case STATE_RES_WAIT_NUM:
        default:
            break;
    };
    MY_LOGI("[Cam::%d][SUS-RES] SkipWait (%d/%d) @(%d)",
        mOpenId, mStandbySetNum, mStreamingSetNum, mState);
    return MTRUE;
};


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HardwareStateControl::checkSkipBlock(void)
{
    if (!isActive()) {
        return MFALSE;
    }
    //
    Mutex::Autolock _l(mLock);
    if (mRequestPass) {
        MY_LOGI("[Cam::%d][SUS-RES] SkipBlock pass (%d/%d) @(%d)",
            mOpenId, mStandbySetNum, mStreamingSetNum, mState);
        mRequestPass = MFALSE;
        return MTRUE;
    }
    switch (mState) {
        case STATE_NORMAL:
        case STATE_SUS_WAIT_NUM:
        case STATE_RES_WAIT_NUM:
        case STATE_RES_WAIT_SYNC:
        case STATE_RES_WAIT_DONE:
            return MFALSE;
        //case STATE_SUS_WAIT_SYNC:
        //case STATE_SUS_READY:
        //case STATE_SUS_DONE:
        default:
            break;
    };
    MY_LOGI("[Cam::%d][SUS-RES] SkipBlock (%d/%d) @(%d)",
        mOpenId, mStandbySetNum, mStreamingSetNum, mState);
    return MTRUE;
};


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HardwareStateControl::checkBufferState(void)
{
    if (!isActive()) {
        // zero buffer count is abnormal
        return MFALSE;
    }
    //
    Mutex::Autolock _l(mLock);
    switch (mState) {
        case STATE_NORMAL:
        case STATE_SUS_WAIT_NUM:
        case STATE_SUS_WAIT_SYNC:
        case STATE_RES_WAIT_DONE:
            // zero buffer count is abnormal
            return MFALSE;
        //case STATE_SUS_READY:
        //case STATE_SUS_DONE:
        //case STATE_RES_WAIT_NUM:
        //case STATE_RES_WAIT_SYNC:
        default:
            break;
    };
    MY_LOGI("[Cam::%d][SUS-RES] NormalCase (%d/%d) @(%d)",
        mOpenId, mStandbySetNum, mStreamingSetNum, mState);
    // zero buffer count is normal
    return MTRUE;
};


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HardwareStateControl::checkDoneNum(MINT32 num)
{
    if (!isActive()) {
        return MFALSE;
    }
    //
    Mutex::Autolock _l(mLock);
    switch (mState) {
        case STATE_NORMAL:
        case STATE_SUS_WAIT_NUM:
        case STATE_SUS_WAIT_SYNC:
        case STATE_SUS_READY:
            // do nothing
            return MFALSE;
        //case STATE_SUS_DONE:
        //case STATE_RES_WAIT_NUM:
        //case STATE_RES_WAIT_SYNC:
        //case STATE_RES_WAIT_DONE:
        default:
            break;
    };
    mvStoreNum.clear();
    if (mState == STATE_RES_WAIT_DONE && mStreamingSetNum == num) {
        mStandbySetNum = 0;
        mStreamingSetNum = 0;
        mShutterTimeUs = 0;
        mRequestPass = MFALSE;
        mState = STATE_NORMAL;
    }
    MY_LOGI("[Cam::%d][SUS-RES] CurNum(%d) SetNum(%d/%d) @(%d)", mOpenId, num,
        mStandbySetNum, mStreamingSetNum, mState);
    return MTRUE; // need to drop previous frame
};


/******************************************************************************
 *
 ******************************************************************************/
void
HardwareStateControl::checkNotePass(MBOOL pass)
{
    if (!isActive()) {
        return;
    }
    //
    Mutex::Autolock _l(mLock);
    mRequestPass = pass;
    MY_LOGI("[Cam::%d][SUS-RES] NoteNextRequestPass(%d) (%d/%d) @(%d)",
        mOpenId, mRequestPass, mStandbySetNum, mStreamingSetNum, mState);
    return;
};


/******************************************************************************
 *
 ******************************************************************************/
void
HardwareStateControl::setDropNum(MINT32 num)
{
    if (!isActive()) {
        return;
    }
    //
    Mutex::Autolock _l(mLock);
    mvStoreNum.push_back(num);
    MY_LOGI("[Cam::%d][SUS-RES] CurNum(%d) (%d/%d) @(%d)", mOpenId, num,
        mStandbySetNum, mStreamingSetNum, mState);
};


/******************************************************************************
 *
 ******************************************************************************/
MINT32
HardwareStateControl::getDropNum(void)
{
    if (!isActive()) {
        return 0;
    }
    //
    Mutex::Autolock _l(mLock);
    MINT32 num = 0;
    if (!mvStoreNum.isEmpty()) {
        Vector<MINT32>::iterator it = mvStoreNum.begin();
        num = *it;
        mvStoreNum.erase(it);
    }
    return num;
};


/******************************************************************************
 *
 ******************************************************************************/
void
HardwareStateControl::reset(void)
{
    if (!isActive()) {
        return;
    }
    //
    Mutex::Autolock _l(mLock);
    if (mState != STATE_NORMAL) {
        MY_LOGI("[Cam::%d][SUS-RES] reset (%d/%d) @(%d ===>>> %d)",
            mOpenId, mStandbySetNum, mStreamingSetNum, mState, STATE_NORMAL);
    }
    mp3A = NULL;
    mpCamIO = NULL;
    mvStoreNum.clear();
    mStandbySetNum = 0;
    mStreamingSetNum = 0;
    mShutterTimeUs = 0;
    mRequestPass = MFALSE;
    mState = STATE_NORMAL;
    MY_LOGD_IF(mLogLevel > 1, "HardwareStateControl RESET");
    mRequestCond.broadcast();
    mThreadCond.broadcast();
    return;
};

#endif

