/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemProperties;

import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.ims.ImsReasonInfo;
import com.android.ims.internal.IImsUt;

import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandsInterface;

import com.mediatek.ims.internal.IMtkImsUt;
import com.mediatek.ims.internal.IMtkImsUtListener;
import com.mediatek.ims.MtkImsCallForwardInfo;
import com.mediatek.ims.MtkImsReasonInfo;
import com.mediatek.ims.ril.ImsCommandsInterface;
import com.mediatek.internal.telephony.MtkCallForwardInfo;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.MtkRIL;
import com.mediatek.simservs.xcap.XcapException;

import java.net.UnknownHostException;

/**
 * ImsUT class for handle the IMS UT interface.
 *
 * The implementation is based on IR.92
 *
 *  @hide
 */
public class MtkImsUtImpl extends IMtkImsUt.Stub {
    private static final String TAG = "MtkImsUtImpl";
    private static final boolean DBG = true;

    private static final Object mLock = new Object();
    private Context mContext;
    private IMtkImsUtListener mListener = null;

    private ResultHandler mHandler;
    private ImsUtImpl mImsUtImpl = null;
    private ImsCommandsInterface mImsRILAdapter;
    private ImsService mImsService = null;
    private int mPhoneId = 0;

    static final int IMS_UT_EVENT_GET_CF_TIME_SLOT = 1200;
    static final int IMS_UT_EVENT_SET_CF_TIME_SLOT = 1201;


    /**
     *
     * Construction function for ImsConfigStub.
     *
     * @param context the application context
     *
     */
    public MtkImsUtImpl(Context context, ImsService imsService, int phoneId,
                        ImsCommandsInterface imsRILAdapter, ImsUtImpl imsUtImpl) {
        mContext = context;
        mImsUtImpl = imsUtImpl;

        HandlerThread thread = new HandlerThread("MtkImsUtImplResult");
        thread.start();
        Looper looper = thread.getLooper();
        mHandler = new ResultHandler(looper);

        mImsService = imsService;
        mImsRILAdapter = imsRILAdapter;
        mPhoneId = phoneId;
    }

    /**
     * Sets the listener.
     */
    public void setListener(IMtkImsUtListener listener) {
        mListener = listener;
    }

    /// For OP01 UT @{
    /**
     * Retrieves the configuration of the call forward in a time slot.
     */
    public int queryCallForwardInTimeSlot(int condition) {
        int requestId;

        synchronized (mLock) {
            requestId = mImsUtImpl.getAndIncreaseRequestId();
        }
        if (DBG) {
            Log.d(TAG, "queryCallForwardInTimeSlot(): requestId = " + requestId);
        }

        Message msg = mHandler.obtainMessage(IMS_UT_EVENT_GET_CF_TIME_SLOT,
                requestId, 0, null);
        mImsRILAdapter.queryCallForwardInTimeSlotStatus(
                mImsUtImpl.getCFReasonFromCondition(condition),
                CommandsInterface.SERVICE_CLASS_VOICE, msg);

        return requestId;
    }

    /**
     * Updates the configuration of the call forward in a time slot.
     */
    public int updateCallForwardInTimeSlot(int action, int condition,
            String number, int timeSeconds, long[] timeSlot) {
        int requestId;

        synchronized (mLock) {
            requestId = mImsUtImpl.getAndIncreaseRequestId();
        }
        if (DBG) {
            Log.d(TAG, "updateCallForwardInTimeSlot(): requestId = " + requestId);
        }

        Message msg = mHandler.obtainMessage(IMS_UT_EVENT_SET_CF_TIME_SLOT, requestId, 0, null);

        mImsRILAdapter.setCallForwardInTimeSlot(mImsUtImpl.getCFActionFromAction(action),
                mImsUtImpl.getCFReasonFromCondition(condition),
                CommandsInterface.SERVICE_CLASS_VOICE,
                number, timeSeconds, timeSlot, msg);

        return requestId;
    }
    /// @}

    private class ResultHandler extends Handler {
        public ResultHandler(Looper looper) {
             super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            if (DBG) {
                Log.d(TAG, "handleMessage(): event = " + msg.what + ", requestId = " + msg.arg1
                         + ", mListener=" + mListener);
            }
            SuppSrvConfig ssConfig = SuppSrvConfig.getInstance(mContext);

            switch (msg.what) {
                case IMS_UT_EVENT_SET_CF_TIME_SLOT: // For OP01 UT
                    if (null != mListener) {
                        AsyncResult ar = (AsyncResult) msg.obj;
                        if (null == ar.exception) {
                            if (DBG) {
                                Log.d(TAG, "utConfigurationUpdated(): "
                                        + "event = " + msg.what);
                            }
                            mImsUtImpl.notifyUtConfigurationUpdated(msg);
                        } else {

                            ImsReasonInfo reason;
                            if (ar.exception instanceof CommandException) {
                                reason = mImsUtImpl.commandExceptionToReason((CommandException)(ar.exception));
                            } else {
                                reason = new ImsReasonInfo(ImsReasonInfo.CODE_UT_NETWORK_ERROR, 0);
                            }
                            mImsUtImpl.notifyUtConfigurationUpdateFailed(msg, reason);
                        }
                    }
                    break;
                 case IMS_UT_EVENT_GET_CF_TIME_SLOT:
                     if (null != mListener) {
                         AsyncResult ar = (AsyncResult) msg.obj;
                         if (null == ar.exception) {

                             MtkCallForwardInfo[] cfInfo = (MtkCallForwardInfo[]) ar.result;
                             MtkImsCallForwardInfo[] imsCfInfo = null;

                             if (cfInfo != null) {
                                 imsCfInfo = new MtkImsCallForwardInfo[cfInfo.length];
                                 for (int i = 0; i < cfInfo.length; i++) {
                                     MtkImsCallForwardInfo info = new MtkImsCallForwardInfo();
                                     info.mCondition =
                                             mImsUtImpl.getConditionFromCFReason(cfInfo[i].reason);
                                     info.mStatus = cfInfo[i].status;
                                     info.mServiceClass = cfInfo[i].serviceClass;
                                     info.mToA = cfInfo[i].toa;
                                     info.mNumber = cfInfo[i].number;
                                     info.mTimeSeconds = cfInfo[i].timeSeconds;
                                     info.mTimeSlot = cfInfo[i].timeSlot;
                                     imsCfInfo[i] = info;
                                 }
                             }

                             try {
                                 mListener.utConfigurationCallForwardInTimeSlotQueried(
                                         MtkImsUtImpl.this, msg.arg1, imsCfInfo);
                             } catch (RemoteException e) {
                                 Log.e(TAG, "RemoteException in IMS_UT_EVENT_GET_CF_TIME_SLOT"
                                         + " utConfigurationCallForwardInTimeSlotQueried");
                                 e.printStackTrace();
                             }
                         } else {

                            ImsReasonInfo reason;
                            if (ar.exception instanceof CommandException) {
                                reason = mImsUtImpl.commandExceptionToReason((CommandException)(ar.exception));
                            } else {
                                reason = new ImsReasonInfo(ImsReasonInfo.CODE_UT_NETWORK_ERROR, 0);
                            }

                            mImsUtImpl.notifyUtConfigurationQueryFailed(msg, reason);
                         }
                     }
                     break;
                default:
                    Log.d(TAG, "Unknown Event: " + msg.what);
                    break;
            }
        }
    };
}
