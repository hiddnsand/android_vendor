package com.mediatek.op15.phone;

import android.content.Context;
import android.util.Log;

import com.mediatek.phone.ext.DefaultCallFeaturesSettingExt;


public class Op15CallFeaturesSettingExt extends DefaultCallFeaturesSettingExt {
    private static final String TAG = "Op15CallFeaturesSettingExt";
    private Context mContext;

    public Op15CallFeaturesSettingExt(Context context) {
        mContext = context;
    }

    @Override
    public boolean needShowOpenMobileDataDialog(Context context, int subId) {
        Log.d(TAG, "needShowOpenMobileDataDialog false");
        return false;
    }
}
