package com.mediatek.entitlement.ext;

import android.content.Context;
import android.util.Log;

import com.mediatek.common.MPlugin;

/** Class to get Corresponding OP Plugin or Default instance otherwise.
 */
public class ExtensionManager {

    public static final String LOG_TAG = "Entitlement-ExtensionManager";

    private static IEntitlementModificationExt sEntitlementModificationExt;

    /**
     * To get one implementation of IEntitlementModificationExt.
     * @param context Calling Application context
     * @return Plugin instance or Default instance
     */
    public static IEntitlementModificationExt getEntitlementModificationExt(Context context) {
        if (sEntitlementModificationExt == null) {
            synchronized (IEntitlementModificationExt.class) {
                sEntitlementModificationExt = (IEntitlementModificationExt) MPlugin.createInstance(
                        IEntitlementModificationExt.class.getName(), context);
                log("getEntitlementModificationExt(). sEntitlementModificationExt = "
                         + sEntitlementModificationExt);
                if (sEntitlementModificationExt == null) {
                    sEntitlementModificationExt = new DefaultEntitlementModificationExt(context);
                }
            }
        }
        return sEntitlementModificationExt;
    }

    private static void log(String msg) {
        Log.d(LOG_TAG, msg);
    }
}
