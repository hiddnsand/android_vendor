#define DEBUG_LOG_TAG "JOB"

#include <hardware/hwcomposer.h>

#include "utils/debug.h"
#include "utils/tools.h"

#include "hwc.h"
#include "dispatcher.h"
#include "display.h"
#include "overlay.h"
#include "sync.h"
#include "hwdev.h"

#define HWC_ATRACE_JOB(string, n1, n2, n3, n4)                                \
    if (ATRACE_ENABLED()) {                                                   \
        char ___traceBuf[1024];                                               \
        snprintf(___traceBuf, 1024, "%s(%d): %d %d %d", (string),             \
            (n1), (n2), (n3), (n4));                                          \
        android::ScopedTrace ___bufTracer(ATRACE_TAG, ___traceBuf);           \
    }

// ---------------------------------------------------------------------------

HWCDispatcher::HWCDispatcher()
    : m_sequence(1)
    , m_disable_skip_redundant(false)
{
}

HWCDispatcher::~HWCDispatcher()
{
    for (uint32_t i = 0; i < DisplayManager::MAX_DISPLAYS; i++)
    {
        if (m_alloc_disp_ids.hasBit(i))
        {
            onPlugOut(i);
        }
    }
}

void HWCDispatcher::onFirstRef()
{
    m_vsync_signaled = false;

    // init for primary display
    onPlugIn(HWC_DISPLAY_PRIMARY);

    sprintf(m_thread_name, "HWCDispatchThread");
    run(m_thread_name, PRIORITY_URGENT_DISPLAY);
}

void HWCDispatcher::waitNextVSync()
{
    // request next vsync
    DisplayManager::getInstance().requestNextVSync();

    // wait for vsync notification
    {
        AutoMutex l(m_vsync_lock);
        if (m_vsync_cond.waitRelative(m_vsync_lock, ms2ns(16)) == TIMED_OUT)
        {
            HWC_LOGW("Timed out waiting for vsync...");
        }
    }
}

bool HWCDispatcher::threadLoop()
{
    sem_wait(&m_event);

    bool need_check_next_period = false;

    while (1)
    {
        DispatcherJobGroup* group = NULL;

        {
            AutoMutex l(m_lock);
            jobDepth = m_group_queue.size();
            if (m_group_queue.empty())
            {
                HWC_LOGD("Job is empty...");
                break;
            }

            Fifo::iterator front(m_group_queue.begin());
            group = *front;

            m_group_queue.erase(front);
        }

#ifndef MTK_USER_BUILD
        HWC_ATRACE_NAME("dispatcher_loop");
#endif

        // need to wait vsync since get another job from job queue directly
        //
        // TODO:
        // if ovl is decoupled, need to make sure if ovl could have internal queue
        // if yes, there is no need to wait for next vsync for handling next composition
        if (need_check_next_period || !m_vsync_signaled)
        {
            HWC_LOGD("Wait to handle next job...");

#ifndef MTK_USER_BUILD
            HWC_ATRACE_NAME("dispatcher_wait");
#endif

            waitNextVSync();
        }

        {
            AutoMutex l(m_vsync_lock);
            m_vsync_signaled = false;
        }

        // handle jobs
        {
            AutoMutex l(m_plug_lock_loop);

            HWC_LOGD("Handle Job...");

            for (uint32_t i = 0; i < DisplayManager::MAX_DISPLAYS; i++)
            {
                DispatcherJob* job = group->jobs[i];

                sp<UILayerComposer> ui_thread = m_workers[i].ui_thread;
                sp<MMLayerComposer> mm_thread = m_workers[i].mm_thread;
                sp<SyncControl> sync_ctrl = m_workers[i].sync_ctrl;
                if (m_workers[i].enable && job != NULL && job->enable &&
                    ui_thread != NULL && mm_thread != NULL && sync_ctrl != NULL)
                {
#ifndef MTK_USER_BUILD
                    HWC_ATRACE_JOB("trigger", i, job->fbt_exist, job->num_ui_layers, job->num_mm_layers);
#endif

                    // set sync control
                    sync_ctrl->setSync(job);

                    // trigger mm thread if there are mm layers
                    if (job->num_mm_layers)
                        mm_thread->trigger(job);

                    // trigger ui thread if there are ui layers or fbt
                    if (job->num_ui_layers || job->fbt_exist)
                        ui_thread->trigger(job);
                }
            }

            for (uint32_t i = 0; i < DisplayManager::MAX_DISPLAYS; i++)
            {
                DispatcherJob* job = group->jobs[i];

                sp<UILayerComposer> ui_thread = m_workers[i].ui_thread;
                sp<MMLayerComposer> mm_thread = m_workers[i].mm_thread;
                sp<SyncControl> sync_ctrl = m_workers[i].sync_ctrl;
                if (m_workers[i].enable && job != NULL && job->enable &&
                    ui_thread != NULL && mm_thread != NULL && sync_ctrl != NULL)
                {
#ifndef MTK_USER_BUILD
                    HWC_ATRACE_JOB("wait", i, job->fbt_exist, job->num_ui_layers, job->num_mm_layers);
#endif

                    // wait until composition is finished
                    if (job->num_mm_layers)
                        mm_thread->wait();

                    if (job->num_ui_layers || job->fbt_exist)
                        ui_thread->wait();
                }

                // clear used job
                if (job != NULL)
                {
                    free(job->hw_layers);
                    delete job;
                }
            }

            // clear used job group
            free(group);
        }

        need_check_next_period = true;
    }

    {
        AutoMutex l(m_lock);
        m_state = HWC_THREAD_IDLE;
        m_condition.signal();
    }

    return true;
}

void HWCDispatcher::prepare()
{
    m_curr_group = (DispatcherJobGroup*)calloc(1, sizeof(DispatcherJobGroup));
}

void HWCDispatcher::set()
{
#ifndef MTK_USER_BUILD
    HWC_ATRACE_NAME("dispatcher_set");
#endif

    AutoMutex l(m_lock);

    DispatcherJob* job = m_curr_group->jobs[0];

    // to avoid too many MDP jobs when Camera
    if (job != NULL && job->hasCameraLayer)
    {
        waitLocked();
    }

    m_group_queue.push_back(m_curr_group);

    m_state = HWC_THREAD_TRIGGER;

    sem_post(&m_event);

    // increment sequence number by 1 after triggering dispatcher thread
    if (DisplayManager::m_profile_level & PROFILE_TRIG)
    {
        m_sequence = m_sequence + 1;
    }
}

DispatcherJob* HWCDispatcher::getJob(int dpy)
{
    if (dpy >= DisplayManager::MAX_DISPLAYS)
        return NULL;

    DispatcherJob* job = NULL;
    {
        AutoMutex l(m_plug_lock_main);

        if (!m_workers[dpy].enable)
        {
            HWC_LOGE("Failed to get job: dpy(%d) is not enable", dpy);
            return NULL;
        }

        job = new DispatcherJob();
        job->display_id = dpy;
        job->num_layers = m_workers[dpy].ovl_engine->getAvailableInputNum();
        job->hw_layers  = (HWLayer*)calloc(1, sizeof(HWLayer) * job->num_layers);

        m_curr_group->jobs[dpy] = job;
    }

    return job;
}

void HWCDispatcher::setJob(
    int dpy, struct hwc_display_contents_1* list)
{
    if (dpy >= DisplayManager::MAX_DISPLAYS) return;

    {
        AutoMutex l(m_plug_lock_main);

        if (!m_workers[dpy].enable)
        {
            HWC_LOGE("Failed to set job: dpy(%d) is not enable", dpy);
            clearListFbt(list);
            return;
        }

        if (!m_workers[dpy].ovl_engine->isEnable())
        {
            HWC_LOGD("SET/bypass/dpy=%d/blank", dpy);
            clearListAll(list);
            return;
        }

        DispatcherJob* job = m_curr_group->jobs[dpy];
        if (NULL == job)
        {
            HWC_LOGW("SET/bypass/dpy=%d/nulljob", dpy);
            clearListFbt(list);
            return;
        }

        if (dpy == HWC_DISPLAY_VIRTUAL && m_curr_group->jobs[HWC_DISPLAY_PRIMARY] != NULL)
        {
            // get video timestamp from primary display
            job->timestamp = m_curr_group->jobs[HWC_DISPLAY_PRIMARY]->timestamp;
        }

        if (DisplayManager::m_profile_level & PROFILE_TRIG)
        {
            job->sequence = m_sequence;
        }

        // check if fbt handle is valid
        if (job->fbt_exist)
        {
            hwc_layer_1_t* fbt_layer = &list->hwLayers[list->numHwLayers - 1];
            if (fbt_layer->handle == NULL)
            {
                int idx = job->num_ui_layers + job->num_mm_layers;
                HWLayer* fbt_hw_layer = &job->hw_layers[idx];
                fbt_hw_layer->enable = false;
#ifdef MTK_HWC_PROFILING
                fbt_hw_layer->fbt_input_layers = 0;
                fbt_hw_layer->fbt_input_bytes  = 0;
#endif

                job->fbt_exist = false;

                if (0 == (job->num_mm_layers + job->num_ui_layers))
                {
                    clearListAll(list);
                    job->enable = false;
                    HWC_LOGD("(%d) SET/bypass/no layers", dpy);
                    return;
                }
            }
        }

#ifndef MTK_USER_BUILD
        HWC_ATRACE_JOB("set", dpy, job->fbt_exist, job->num_ui_layers, job->num_mm_layers);
#endif

        HWC_LOGD("SET/dpy=%d/fbt=%d/ui=%d/mm=%d",
            dpy, job->fbt_exist, job->num_ui_layers, job->num_mm_layers);

        // verify
        // 1. if outbuf should be used for virtual display
        // 2. if any input buffer is dirty for physical display
        m_workers[dpy].post_handler->set(list, job);
        if ((job->post_state & HWC_POST_CONTINUE_MASK) == 0)
        {
            job->enable = false;
            return;
        }

        bool job_enabled = false;

        if (job->num_ui_layers || job->fbt_exist)
        {
            m_workers[dpy].ui_thread->set(list, job);
            job_enabled = true;
        }

        if (job->num_mm_layers)
        {
            m_workers[dpy].mm_thread->set(list, job);
            job_enabled = true;
        }

        // marked job that should be processed
        job->enable = job_enabled;
    }
}

void HWCDispatcher::releaseResource(int dpy)
{
    // wait until all thread is idle
    if (m_workers[dpy].ui_thread != NULL)
    {
        m_workers[dpy].ui_thread->wait();
        m_workers[dpy].ui_thread->requestExit();
        m_workers[dpy].ui_thread->trigger(NULL);
        m_workers[dpy].ui_thread->join();
        m_workers[dpy].ui_thread = NULL;
    }

    if (m_workers[dpy].mm_thread != NULL)
    {
        m_workers[dpy].mm_thread->wait();
        m_workers[dpy].mm_thread->requestExit();
        m_workers[dpy].mm_thread->trigger(NULL);
        m_workers[dpy].mm_thread->join();
        m_workers[dpy].mm_thread = NULL;
    }

    m_workers[dpy].post_handler = NULL;
    m_workers[dpy].sync_ctrl = NULL;

    m_workers[dpy].ovl_engine->blank(1);
    // flip again to make sure OutdatedQueue is cleared for WFD and MHL
    m_workers[dpy].ovl_engine->flip();
    m_workers[dpy].ovl_engine = NULL;

    HWC_LOGD("Release resource (dpy=%d)", dpy);

    disp_session_info info;

    if (DispDevice::getInstance().getOverlaySessionInfo(dpy, &info) != INVALID_OPERATION)
    {
        // session still exists after destroying overlay engine
        // something goes wrong in display driver?
        HWC_LOGW("Session is not destroyed (dpy=%d)", dpy);
    }

}

void HWCDispatcher::onPlugIn(int dpy)
{
    if (dpy >= DisplayManager::MAX_DISPLAYS)
    {
        HWC_LOGE("Invalid display(%d) is plugged(%d) !!", dpy);
        return;
    }

    {
        AutoMutex ll(m_plug_lock_loop);
        AutoMutex lm(m_plug_lock_main);

        if (m_alloc_disp_ids.hasBit(dpy))
        {
            HWC_LOGE("Display(%d) is already connected !!", dpy);
            return;
        }

        m_workers[dpy].enable = false;

        m_workers[dpy].ovl_engine = new OverlayEngine(dpy);
        if (m_workers[dpy].ovl_engine == NULL ||
            !m_workers[dpy].ovl_engine->isEnable())
        {
            m_workers[dpy].ovl_engine = NULL;
            HWC_LOGE("Failed to create OverlayEngine (dpy=%d) !!", dpy);
            return;
        }

        if (dpy < HWC_DISPLAY_VIRTUAL)
            m_workers[dpy].post_handler = new PhyPostHandler(dpy, m_workers[dpy].ovl_engine);
        else
            m_workers[dpy].post_handler = new VirPostHandler(dpy, m_workers[dpy].ovl_engine);

        if (m_workers[dpy].post_handler == NULL)
        {
            HWC_LOGE("Failed to create PostHandler (dpy=%d) !!", dpy);
            releaseResource(dpy);
            return;
        }

        // create post listener to know when need to set and trigger overlay engine
        struct PostListener : public SyncControl::SyncListener
        {
            PostListener(sp<PostHandler> handler) : m_handler(handler) { }
            ~PostListener() { m_handler = NULL; }
        private:
            sp<PostHandler> m_handler;
            virtual void onTrigger(DispatcherJob* job)
            {
                m_handler->process(job);
            }
        };

        m_workers[dpy].sync_ctrl =
            new SyncControl(new PostListener(m_workers[dpy].post_handler));
        if (m_workers[dpy].sync_ctrl == NULL)
        {
            HWC_LOGE("Failed to create SyncControl (dpy=%d) !!", dpy);
            releaseResource(dpy);
            return;
        }

        m_workers[dpy].ui_thread =
            new UILayerComposer(dpy, m_workers[dpy].sync_ctrl, m_workers[dpy].ovl_engine);
        if (m_workers[dpy].ui_thread == NULL)
        {
            HWC_LOGE("Failed to create UILayerComposer (dpy=%d) !!", dpy);
            releaseResource(dpy);
            return;
        }

        m_workers[dpy].mm_thread =
            new MMLayerComposer(dpy, m_workers[dpy].sync_ctrl, m_workers[dpy].ovl_engine);
        if (m_workers[dpy].mm_thread == NULL)
        {
            HWC_LOGE("Failed to create MMLayerComposer (dpy=%d) !!", dpy);
            releaseResource(dpy);
            return;
        }

        m_alloc_disp_ids.markBit(dpy);
        m_workers[dpy].enable = true;
    }
}

void HWCDispatcher::onPlugOut(int dpy)
{
    if (dpy >= DisplayManager::MAX_DISPLAYS)
    {
        HWC_LOGE("Invalid display(%d) is unplugged !!", dpy);
    }

    if (dpy == HWC_DISPLAY_PRIMARY)
    {
        HWC_LOGE("Should not disconnect primary display !!");
        return;
    }

    {
        AutoMutex ll(m_plug_lock_loop);
        AutoMutex lm(m_plug_lock_main);

        if (m_workers[dpy].enable)
        {
            releaseResource(dpy);
        }
        else
        {
            HWC_LOGE("Failed to disconnect invalid display(%d) !!", dpy);
        }

        m_alloc_disp_ids.clearBit(dpy);
        m_workers[dpy].enable = false;
    }
}

void HWCDispatcher::onBlank(int dpy, int blank)
{
    if (HWC_DISPLAY_VIRTUAL > dpy)
    {
        if (blank) wait();

        AutoMutex l(m_plug_lock_main);

        if (m_workers[dpy].enable)
        {
            m_workers[dpy].ovl_engine->blank(blank);
        }
    }
}

void HWCDispatcher::onVSync(int dpy)
{
    {
        AutoMutex l(m_vsync_lock);
        m_vsync_signaled = true;
        m_vsync_cond.signal();
    }

    {
        AutoMutex l(m_plug_lock_main);

        if (m_workers[dpy].enable)
        {
            m_workers[dpy].ovl_engine->flip();
        }
    }
}

void HWCDispatcher::dump(struct dump_buff* log, int dump_level)
{
    AutoMutex l(m_plug_lock_main);

    for (int dpy = 0; dpy < DisplayManager::MAX_DISPLAYS; dpy++)
    {
        if (m_workers[dpy].enable)
        {
            m_workers[dpy].ovl_engine->dump(log, dump_level);
        }
    }
    dump_printf(log, "%s\n", Debugger::getInstance().m_prepare_info.string());
    dump_printf(log, "\n");
}

int HWCDispatcher::getJobDepth()
{
    int result = -1;
    result = jobDepth;
    return jobDepth;
}

bool HWCDispatcher::saveFbtHandle(int dpy, buffer_handle_t handle)
{
    bool res = m_prev_fbt[dpy] != handle;
    m_prev_fbt[dpy] = handle;
    return res;
}

// ---------------------------------------------------------------------------

#define PLOGD(x, ...) HWC_LOGD("(%d) " x, m_disp_id, ##__VA_ARGS__)
#define PLOGI(x, ...) HWC_LOGI("(%d) " x, m_disp_id, ##__VA_ARGS__)
#define PLOGW(x, ...) HWC_LOGW("(%d) " x, m_disp_id, ##__VA_ARGS__)
#define PLOGE(x, ...) HWC_LOGE("(%d) " x, m_disp_id, ##__VA_ARGS__)

HWCDispatcher::PostHandler::PostHandler(int dpy, const sp<OverlayEngine>& ovl_engine)
    : m_disp_id(dpy)
    , m_ovl_engine(ovl_engine)
    , m_sync_fence(new SyncFence(dpy))
{ }

HWCDispatcher::PostHandler::~PostHandler()
{
    m_ovl_engine = NULL;
    m_sync_fence = NULL;
}

// ---------------------------------------------------------------------------

void HWCDispatcher::PhyPostHandler::set(
    struct hwc_display_contents_1* list, DispatcherJob* job)
{
    if (list->flags & HWC_SKIP_DISPLAY)
    {
        PLOGD("skip composition: display has skip flag");
        job->post_state = HWC_POST_INPUT_NOTDIRTY;
        clearListAll(list);
        return;
    }

    uint32_t total_num = job->num_layers;

    bool is_dirty = false;
    {
        // TODO: checking dirty after src/dst corp checking method is ready
        is_dirty = true;
        /*
        for (uint32_t i = 0; i < total_num; i++)
        {
            HWLayer* hw_layer = &job->hw_layers[i];

            if (!hw_layer->enable) continue;

            hwc_layer_1_t* layer = &list->hwLayers[hw_layer->index];
            // TODO: FBT could query info after ION supports this feature
            //
            // check if fbt is dirty
            //if (HWC_LAYER_TYPE_FBT == hw_layer->type)
            //{
            //    gralloc_extra_getBufInfo(layer->handle, &hw_layer->buffer_info);
            //
            //    if (hw_layer->buffer_info.status & GRALLOC_EXTRA_MASK_DIRTY)
            //        is_dirty = true;
            //}

            // check if any layer is dirty
            is_dirty |= hw_layer->dirty;

            if (HWC_LAYER_TYPE_FBT != hw_layer->type && layer->handle) {
                gralloc_extra_sf_set_status(
                        &hw_layer->ext_info, GRALLOC_EXTRA_MASK_DIRTY, GRALLOC_EXTRA_BIT_UNDIRTY);

                gralloc_extra_perform(
                        layer->handle, GRALLOC_EXTRA_SET_IOCTL_ION_SF_INFO, &hw_layer->ext_info);
            }
        }
        */
    }

    if (is_dirty)
    {
        job->post_state = HWC_POST_INPUT_DIRTY;

        // TODO: enable after driver implements present fence
    }
    else
    {
        // set as nodirty since could not find any dirty layers
        job->post_state = HWC_POST_INPUT_NOTDIRTY;

        PLOGD("No need to compose since no dirty layers");

        // clear all layers' acquire fences
        for (uint32_t i = 0; i < total_num; i++)
        {
            HWLayer* hw_layer = &job->hw_layers[i];

            if (!hw_layer->enable) continue;

            hwc_layer_1_t* layer = &list->hwLayers[hw_layer->index];
            layer->releaseFenceFd = -1;
            if (layer->acquireFenceFd != -1) ::close(layer->acquireFenceFd);
        }
    }
}

void HWCDispatcher::PhyPostHandler::process(DispatcherJob* job)
{
    // disable unused input layer
    {
        int idx_begin = job->num_ui_layers + job->num_mm_layers + (int)job->fbt_exist;
        int idx_bound = job->num_layers;
        for (int i = idx_begin; i < idx_bound; i++)
        {
            m_ovl_engine->disableInput(i);
        }
    }

    // update all input configs to driver
    m_ovl_engine->setInputs();

    // trigger overlay engine
    m_ovl_engine->trigger();
}

// ---------------------------------------------------------------------------

void HWCDispatcher::VirPostHandler::setError(DispatcherJob* job)
{
    for (int i = 0; i < job->num_layers; i++)
    {
        m_ovl_engine->disableInput(i);
    }
}

void HWCDispatcher::VirPostHandler::set(
    struct hwc_display_contents_1* list, DispatcherJob* job)
{
    if (list->flags & HWC_SKIP_DISPLAY)
    {
        PLOGD("skip composition: display has skip flag");
        job->post_state = HWC_POST_INPUT_NOTDIRTY;
        clearListAll(list);
        return;
    }

    // If FORCE_HWC_COPY is disabled,
    // let GPU to queue buffer to encoder directly in WFD extension mode.
    if ((!HWCMediator::getInstance().m_features.copyvds) &&
        0 == (job->num_ui_layers + job->num_mm_layers))
    {
        PLOGD("No need to handle outbuf with GLES mode");
        job->post_state = HWC_POST_OUTBUF_DISABLE;
        clearListFbt(list);
        setError(job);
        return;
    }

    if (list->outbuf == NULL)
    {
        PLOGE("Fail to get outbuf");
        job->post_state = HWC_POST_INVALID;
        clearListAll(list);
        setError(job);
        return;
    }

    HWOutbuf* hw_outbuf = &job->hw_outbuf;

    PrivateHandle* priv_handle = &hw_outbuf->priv_handle;
    status_t err = NO_ERROR;
    int ion_fd = DISP_NO_ION_FD;

    getBufferInfo(list->outbuf, &hw_outbuf->buffer_info);

    getPrivateHandleInfo(list->outbuf, priv_handle);

    if ((1 == HWCMediator::getInstance().m_features.svp) &&
        (hw_outbuf->buffer_info.usage & GRALLOC_USAGE_SECURE))
    {
        PLOGD("Set secure outbuf (handle=%p)", list->outbuf);
    }
    else
    {
        int fd = getIonFd(list->outbuf);
        if (fd < 0)
        {
            PLOGE("Failed to get ION fd !! (outbuf=%p) !!", list->outbuf);
            job->post_state = HWC_POST_INVALID;
            clearListAll(list);
            setError(job);
            return;
        }

        ion_fd = fd;
    }

    job->post_state = HWC_POST_OUTBUF_ENABLE;

    // get retire fence from display driver
    OverlayPrepareParam prepare_param;
    {
        prepare_param.ion_fd        = ion_fd;
        prepare_param.is_need_flush = 0;

        err = m_ovl_engine->prepareOutput(prepare_param);
        if (NO_ERROR != err)
        {
            prepare_param.fence_index = 0;
            prepare_param.fence_fd = -1;
        }

        if (prepare_param.fence_fd <= 0)
        {
            PLOGE("Failed to get retireFence !!");
        }
    }

    hw_outbuf->ion_fd               = ion_fd;
    hw_outbuf->out_acquire_fence_fd = list->outbufAcquireFenceFd;
    hw_outbuf->out_retire_fence_fd  = prepare_param.fence_fd;
    hw_outbuf->out_retire_fence_idx = prepare_param.fence_index;
    hw_outbuf->handle               = list->outbuf;

    list->retireFenceFd = prepare_param.fence_fd;

    PLOGD("Outbuf retireFence (fd=%d, idx=%d)"
        " acquireFence (fd=%d) (handle=%p, ion=%d)",
        hw_outbuf->out_retire_fence_fd, hw_outbuf->out_retire_fence_idx,
        hw_outbuf->out_acquire_fence_fd, hw_outbuf->handle, hw_outbuf->ion_fd);

    if (!job->fbt_exist)
    {
        hwc_layer_1_t* layer = &list->hwLayers[list->numHwLayers - 1];
        layer->releaseFenceFd = -1;
        if (layer->acquireFenceFd != -1) ::close(layer->acquireFenceFd);
    }

    // set video usage and timestamp into output buffer handle
    gralloc_extra_ion_sf_info_t* ext_info = &hw_outbuf->priv_handle.ext_info;
    ext_info->timestamp = job->timestamp;
    if (DisplayManager::m_profile_level & PROFILE_TRIG)
    {
        // set token to buffer handle for profiling latency purpose
        ext_info->sequence = job->sequence;
    }
    grallocExtraPerform(
            hw_outbuf->handle, GRALLOC_EXTRA_SET_IOCTL_ION_SF_INFO, ext_info);
}

void HWCDispatcher::VirPostHandler::process(DispatcherJob* job)
{
    // disable unused input layer
    {
        int idx_begin = job->num_ui_layers + job->num_mm_layers + (int)job->fbt_exist;
        int idx_bound = job->num_layers;
        for (int i = idx_begin; i < idx_bound; i++)
        {
            m_ovl_engine->disableInput(i);
        }
    }

    // update all input configs to driver
    m_ovl_engine->setInputs();

    // set output buffer for virtual display
    {
        HWOutbuf* hw_outbuf = &job->hw_outbuf;
        hwc_buffer_info_t* buffer_info = &hw_outbuf->buffer_info;

        OverlayPortParam param;

        const bool is_secure = (1 == HWCMediator::getInstance().m_features.svp) &&
                    (buffer_info->usage & GRALLOC_USAGE_SECURE);
        if (is_secure)
        {
            int sec_handle = 0;
            status_t err = grallocExtraQuery(hw_outbuf->handle, GRALLOC_EXTRA_GET_SECURE_HANDLE, &sec_handle);
            if (NO_ERROR != err || sec_handle == 0)
            {
                PLOGE("Failed to get secure outbuf !! (handle=%p, err=%d)",
                    hw_outbuf->handle, err);
                return;
            }

            param.va      = (void*)sec_handle;
            param.mva     = (void*)sec_handle;
        }
        else
        {
            param.va      = NULL;
            param.mva     = NULL;
        }
        param.pitch       = buffer_info->y_stride;
        param.format      = buffer_info->format;
        param.dst_crop    = Rect(buffer_info->width, buffer_info->height);
        param.fence_index = hw_outbuf->out_retire_fence_idx;
        param.secure      = is_secure;
        param.sequence    = job->sequence;
        param.ion_fd      = hw_outbuf->ion_fd;

        m_sync_fence->wait(hw_outbuf->out_acquire_fence_fd, 1000, DEBUG_LOG_TAG);

        m_ovl_engine->setOutput(&param);
    }

    // trigger overlay engine
    m_ovl_engine->trigger();
}
