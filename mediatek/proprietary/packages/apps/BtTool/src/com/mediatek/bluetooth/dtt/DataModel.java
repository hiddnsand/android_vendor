package com.mediatek.bluetooth.dtt;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;

/**
 * Created by MTK01635 on 2015/4/23.
 */
public class DataModel {
    private static final boolean DBG = true;
    private static final String TAG = "MtkBtLogDataModel";
//  Need to archive - SharedPrefernces
    public static final String F_HOST_TEST_SITUATION = "host_pref_test_situation";
    public static final String F_HOST_BTSC_CUST_PATH = "host_pref_btsc_cust_path";
    public static final String F_FW_TEST_SITUATION = "fw_pref_test_situation";
    public static final String F_SYS_TOOL_OPEN_MTKLOGGER = "pref_systool_open_mtklogger";
    public static final String F_SYS_TOOL_OPEN_ENGMODE = "pref_systool_open_engmode";

    public String mBtHostTestSituation;
    public String mBtHostScCustPath = Const.DEFAULT_CONFIG_DIR + Const.DEFAULT_STACK_CONF; // 當 Test Situation 為 Cust 時對應的 path
    public String mBtFwTestSituation;

//  Don't need to archive
    public int mState = Const.STATE_INIT;

    /**
     * load preferences from storage
     *
     * @param context
     */

    DataModel(){
        mBtHostTestSituation = Const.TS_DEFAULT;
        mBtFwTestSituation = Const.TS_DEFAULT;
        String build = SystemProperties.get("ro.build.type").toLowerCase();
        if(build.equals("eng") || build.equals("userdebug"))
            mBtFwTestSituation = Const.TS_SQC;
    }

    public boolean load(Context context){
        boolean change = false;
        // from SharedPreferences
        SharedPreferences preferences = Util.getSettingPreference(context);
        String host_sc = preferences.getString(F_HOST_TEST_SITUATION, mBtHostTestSituation);
        String fw_sc = preferences.getString(F_FW_TEST_SITUATION, mBtFwTestSituation);

        if (!mBtHostTestSituation.equals(host_sc)) {
            change = true;
        }
        //this.mBtHostScCustPath = preferences.getString(F_HOST_BTSC_CUST_PATH, mBtHostScCustPath);
        mBtHostTestSituation = host_sc;
        mBtFwTestSituation = fw_sc;
        return change;
    }

    /**
     * save preferences into storage
     * @param context
     */
    public void save(Context context){

        SharedPreferences preferences = Util.getSettingPreference(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(F_HOST_TEST_SITUATION, mBtHostTestSituation);
       // editor.putString(F_HOST_BTSC_CUST_PATH, mBtHostScCustPath);
        editor.putString(F_FW_TEST_SITUATION, mBtFwTestSituation);
        editor.commit();
    }

    public void refresh(Context context){
        this.load(context);

        //check the property
        String hostScProp = SystemProperties.get(Const.HOST_LOG_CONF_PROPERTY).toLowerCase();
        String fwScProp = SystemProperties.get(Const.FW_LOG_CONF_PROPERTY).toLowerCase();
        SharedPreferences preferences = Util.getSettingPreference(context);
        String[] host_values = context.getResources().getStringArray(R.array.act_set_host_pref_test_situation_entries_values);
        String[] fw_values = context.getResources().getStringArray(R.array.act_set_fw_pref_test_situation_entries_values);

        for(String host_value:host_values)
        {
            if(hostScProp.equals(host_value) && !hostScProp.equals(mBtHostTestSituation))
                this.mBtHostTestSituation = hostScProp;
        }

        for(String fw_value:fw_values)
        {
            if( fwScProp.equals(fw_value) && !fwScProp.equals(mBtFwTestSituation))
                this.mBtFwTestSituation = fwScProp;
        }

        this.save(context);
    }

    public void pushTestSituation(){
        SystemProperties.set(Const.HOST_LOG_CONF_PROPERTY, this.mBtHostTestSituation);
        SystemProperties.set(Const.FW_LOG_CONF_PROPERTY, this.mBtFwTestSituation);
        pokeSystemProperties();

        if (this.mBtHostTestSituation.equals(Const.TS_CUST))
            SystemProperties.set(Const.FW_LOG_CONF_PROPERTY, this.mBtHostScCustPath);
/*
             if (!this.mBtHostTestSituation.equals(Const.TS_DEFAULT))
                 SystemProperties.set(Const.LOG_TOO_MUCH_PROPERTY, "false");
            else
                SystemProperties.set(Const.LOG_TOO_MUCH_PROPERTY, "true");
*/
    }

    private void pokeSystemProperties() {
        if (DBG) {
            Log.i(TAG, "pokeSystemProperties for "
                + SystemProperties.get(Const.FW_LOG_CONF_PROPERTY));
        }
        // Since system property without notification callback,
        // to take advantage of binder debug proposed SYSPROPS to achieve to notify
        (new SystemPropPoker()).execute();
    }

    /**
     * System property poker.
     */
    public static class SystemPropPoker extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            IBinder obj = ServiceManager.checkService(Context.BLUETOOTH_SERVICE);
            if (obj != null) {
                Parcel data = Parcel.obtain();
                try {
                    obj.transact(IBinder.SYSPROPS_TRANSACTION, data, null, 0);
                } catch (RemoteException e) {
                    Log.i(TAG, "A bad remote service to be poked: " + e);
                } catch (Exception e) {
                    Log.i(TAG, "A bad service to be poked: " + e);
                }
                data.recycle();
            } else {
                Log.e(TAG, "Could not get Bluetooth service");
            }
            return null;
        }
    }
}
