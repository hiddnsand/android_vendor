#ifndef _LIBFUNC_H
#define _LIBFUNC_H

int create_dir(char *whole_path);
int notify_client(int clientfd, char *msg);
int is_mblog_running();
char *set_cur_logpath(char *logpath);
void copy_file(char *srcfile, char *desfile);

#endif
