#ifndef VENDOR_MEDIATEK_HARDWARE_NVRAM_V1_1_MTKNVRAM_H
#define VENDOR_MEDIATEK_HARDWARE_NVRAM_V1_1_MTKNVRAM_H

#include <vendor/mediatek/hardware/nvram/1.1/IMtkNvram.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>

namespace vendor {
namespace mediatek {
namespace hardware {
namespace nvram {
namespace V1_1 {
namespace implementation {

using ::android::hidl::base::V1_0::DebugInfo;
using ::android::hidl::base::V1_0::IBase;
using ::vendor::mediatek::hardware::nvram::V1_1::IMtkNvram;
using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

struct MtkNvram : public IMtkNvram {
    MtkNvram();
    ~MtkNvram();	
    // Methods from ::vendor::mediatek::hardware::nvram::V1_1::IMtkNvram follow.
    Return<void> setParametersCallback(uint32_t parameters) override;

    // Methods from ::android::hidl::base::V1_0::IBase follow.

};

extern "C" IMtkNvram* HIDL_FETCH_IMtkNvram(const char* name);

}  // namespace implementation
}  // namespace V1_1
}  // namespace nvram
}  // namespace hardware
}  // namespace mediatek
}  // namespace vendor

#endif  // VENDOR_MEDIATEK_HARDWARE_NVRAM_V1_1_MTKNVRAM_H
