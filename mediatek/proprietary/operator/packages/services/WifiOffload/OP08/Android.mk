LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_JAVA_LIBRARIES += mediatek-common
LOCAL_STATIC_JAVA_LIBRARIES += com.mediatek.wfo.op

LOCAL_MODULE_TAGS := optional
LOCAL_PACKAGE_NAME := OP08Wfo
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true

# Put operator customization apk together to specific folder
# Specify install path for MTK CIP solution
ifeq ($(strip $(MTK_CIP_SUPPORT)),yes)
    LOCAL_MODULE_PATH := $(TARGET_CUSTOM_OUT)/app
else
    LOCAL_MODULE_PATH := $(TARGET_OUT)/app
endif

LOCAL_PROGUARD_ENABLED := disabled

include $(BUILD_PACKAGE)
