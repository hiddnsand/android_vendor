LOCAL_PATH:= $(call my-dir)
mediatek-res-source-path := APPS/mediatek-res_intermediates/src

include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, java)
LOCAL_SRC_FILES += \
    java/com/mediatek/cta/IPermRecordsController.aidl \

LOCAL_AIDL_INCLUDES += $(LOCAL_PATH)/java

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE := mediatek-cta

#LOCAL_PROGUARD_FLAG_FILES := proguard.flags
LOCAL_PROGUARD_ENABLED := disabled

LOCAL_JAVA_LIBRARIES := services
LOCAL_JAVA_LIBRARIES += framework
LOCAL_JAVA_LIBRARIES += org.apache.http.legacy.boot

LOCAL_REQUIRED_MODULES := services
LOCAL_REQUIRED_MODULES += framework

include $(BUILD_JAVA_LIBRARY)
