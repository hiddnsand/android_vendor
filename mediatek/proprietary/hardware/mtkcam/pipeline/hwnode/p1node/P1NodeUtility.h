/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_INCLUDE_MTKCAM_PIPELINE_HWNODE_P1NODE_UTILITY_H_
#define _MTK_HARDWARE_INCLUDE_MTKCAM_PIPELINE_HWNODE_P1NODE_UTILITY_H_

#include "P1NodeCommon.h"
//
#ifdef P1_TIMING_CHECK
#undef P1_TIMING_CHECK
#endif
#if (IS_P1_LOGD) // for system use LOGD
#define P1_TIMING_CHECK(str, timeout_ms, type)\
    sp<TimingChecker::Client> TimingCheckerClient =\
    (mpTimingCheckerMgr != NULL) ?\
    (mpTimingCheckerMgr->createClient(str, timeout_ms, type)) :\
    (NULL);
#else
#define P1_TIMING_CHECK(str, timeout_ms, type)  ;
#endif

#ifdef TC_W
#undef TC_W
#endif
#define TC_W TimingChecker::EVENT_TYPE_WARNING
#ifdef TC_E
#undef TC_E
#endif
#define TC_E TimingChecker::EVENT_TYPE_ERROR
#ifdef TC_F
#undef TC_F
#endif
#define TC_F TimingChecker::EVENT_TYPE_FATAL

/******************************************************************************
 *
 ******************************************************************************/

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#if 0
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class AAAResult {
    protected:
        struct info{
            sp<IPipelineFrame> spFrame;
            IMetadata          resultVal;
            MUINT32            mFlag;
            info()
                : spFrame(0)
                , resultVal()
                , mFlag(0)
                {}
        };

    protected:
        enum KeyType {
            KeyType_StrobeState = 1,
            KeyType_Rest        = 2, //1,2,4,8,...
        };

    protected:
        mutable Mutex              mLock;
        KeyedVector<MUINT32, info> mData; //key: magicnum, val: info
        MUINT32                    mAllKey;

    public:

        AAAResult()
            : mLock()
            , mData()
            , mAllKey(KeyType_Rest)
            //, mAllKey(KeyType_StrobeState|KeyType_Rest)
            {}

        void add(MUINT32 magicNum, MUINT32 key, MUINT32 val)
        {
             Mutex::Autolock lock(mLock);
             if(key != MTK_FLASH_STATE) {
                 //unSupported
                 return;
             }

             IMetadata::IEntry entry(MTK_FLASH_STATE);
             entry.push_back(val, Type2Type< MUINT8 >()); //{MTK_FLASH_STATE, MUINT8}
             ssize_t i = mData.indexOfKey(magicNum);
             if(i < 0) {
                 info data;
                 data.resultVal.update(MTK_FLASH_STATE, entry);

    data.mFlag |= KeyType_StrobeState;
                 mData.add(magicNum, data);
             } else {
                 info& data = mData.editValueFor(magicNum);
                 data.resultVal.update(MTK_FLASH_STATE, entry);

    data.mFlag |= KeyType_StrobeState;
             }
        }

        void add(MUINT32 magicNum, sp<IPipelineFrame> pframe, IMetadata &rVal)
        {
             Mutex::Autolock lock(mLock);
             ssize_t i = mData.indexOfKey(magicNum);
             if(i < 0) {
                 info data;
                 data.spFrame = pframe;
                 data.resultVal = rVal;

data.mFlag |= KeyType_Rest;
                 mData.add(magicNum, data);
             } else {
                 info& data = mData.editValueFor(magicNum);
                 data.spFrame = pframe;
                 data.resultVal += rVal;
                 data.mFlag |= KeyType_Rest;
             }
        }

        const info& valueFor(const MUINT32& magicNum) const {
            return mData.valueFor(magicNum);
        }

        bool isCompleted(MUINT32 magicNum) {
            Mutex::Autolock lock(mLock);
            return (mData.valueFor(magicNum).mFlag & mAllKey) == mAllKey;
        }

        void removeItem(MUINT32 key) {
            Mutex::Autolock lock(mLock);
            mData.removeItem(key);
        }

        void clear() {
            debug();
            Mutex::Autolock lock(mLock);
            mData.clear();
        }

        void debug() {
            Mutex::Autolock lock(mLock);
            for(size_t i = 0; i < mData.size(); i++) {
                MY_LOGW_IF((mData.valueAt(i).mFlag & KeyType_StrobeState) == 0,
                           "No strobe result: (%d)", mData.keyAt(i));
                MY_LOGW_IF((mData.valueAt(i).mFlag & KeyType_Rest) == 0,
                           "No rest result: (%d)", mData.keyAt(i));
            }
        }
};
#endif
#if 0
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class Storage {

    protected:
        typedef DefaultKeyedVector<MINTPTR, sp<IImageBuffer> >  MapType;
        MapType                    mvStorageQ;
        mutable Mutex              mStorageLock;
        MINT32                     mLogEnable;
    public:
                                   Storage()
                                       : mvStorageQ()
                                       , mStorageLock()
                                       , mLogEnable(0)
                                       {}

        virtual                   ~Storage(){};

        void                       init(MINT32 logEnable)
                                   {
                                       mvStorageQ.clear();
                                       mLogEnable = logEnable;
                                   }

        void                       uninit()
                                   {
                                       mvStorageQ.clear();
                                   }

        void                       enque(sp<IImageStreamBuffer> const& key, sp<IImageBuffer> &value) {
                                       Mutex::Autolock lock(mStorageLock);
                                       MY_LOGD_IF(mLogEnable, "Storage-enque::(key)0x%x/(val)0x%x",
                                           key.get(), value.get());
                                       MY_LOGD_IF(mLogEnable, "Info::(val-pa)0x%x/%d/%d/%d/%d/%d",
                                        value->getBufPA(0),value->getImgSize().w, value->getImgSize().h,
                                        value->getBufStridesInBytes(0), value->getBufSizeInBytes(0), value->getPlaneCount());

                                       mvStorageQ.add(reinterpret_cast<MINTPTR>(key.get()), value);
                                   };


        sp<IImageBuffer>           deque(MINTPTR key) {
                                       Mutex::Autolock lock(mStorageLock);
                                       sp<IImageBuffer> pframe = mvStorageQ.valueFor(key);
                                       if (pframe != NULL)
                                       {
                                           mvStorageQ.removeItem(key); //should un-mark
                                           MY_LOGD_IF(mLogEnable, "Storage-deque::(key)0x%x/(val)0x%x",
                                            key, pframe.get());
                                           MY_LOGD_IF(mLogEnable, "(val-pa)0x%x",
                                            pframe->getBufPA(0));
                                           return pframe;
                                       }
                                       return NULL;
                                   }
        sp<IImageBuffer>           query(MINTPTR key) {
                                       Mutex::Autolock lock(mStorageLock);
                                       sp<IImageBuffer> pframe = mvStorageQ.valueFor(key);
                                       if (pframe != NULL)
                                       {
                                           MY_LOGD_IF(mLogEnable, "Storage-deque::(key)0x%x/(val)0x%x",
                                            key, pframe.get());
                                           MY_LOGD_IF(mLogEnable, "Info::(val-pa)0x%x",
                                            pframe->getBufPA(0));
                                           return pframe;
                                       }
                                       return NULL;
                                   }
};
#endif

#if 1
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class StuffBufferPool {

    #define STUFF_BUFFER_WATER_MARK 8   // "0" the pool will not store buffer
    #define STUFF_BUFFER_MAX_AMOUNT 16  // the max amount for general use case

    enum BUF_STATE
    {
        BUF_STATE_RELEASED  = 0,
        BUF_STATE_ACQUIRED
    };

    struct BufNote {
        public:
                            BufNote()
                                : msName("")
                                , mState(BUF_STATE_RELEASED)
                            {};
                            BufNote(android::String8 name, BUF_STATE state)
                                : msName(name)
                                , mState(state)
                            {};
            virtual         ~BufNote()
                            {};

            android::String8    msName;
            BUF_STATE           mState;
    };

public:
                        StuffBufferPool()
                            : mLogLevel(0)
                            , msName("")
                            , mFormat(0)
                            , mSize(0, 0)
                            , mStride0(0)
                            , mStride1(0)
                            , mStride2(0)
                            , mPlaneCnt(0)
                            , mUsage(0)
                            , mSerialNum(0)
                            , mWaterMark(STUFF_BUFFER_WATER_MARK)
                            , mMaxAmount(STUFF_BUFFER_MAX_AMOUNT)
                        {
                            MY_LOGD_IF(mLogLevel > 1, "+++");
                            mUsage = (GRALLOC_USAGE_SW_READ_OFTEN |
                                        GRALLOC_USAGE_HW_CAMERA_READ |
                                        GRALLOC_USAGE_HW_CAMERA_WRITE);
                            mvInfoMap.clear();
                            MY_LOGD_IF(mLogLevel > 1, "---");
                        };

                        StuffBufferPool(
                            char const * szName,
                            MINT32 format,
                            MSize size,
                            MUINT32 stride0,
                            MUINT32 stride1,
                            MUINT32 stride2,
                            MUINT8 multiple,    // for burst mode
                            MBOOL writable,     // for SW write
                            MINT32  log
                        )
                            : mLogLevel(log)
                            , msName(szName)
                            , mFormat(format)
                            , mSize(size)
                            , mStride0(stride0)
                            , mStride1(stride1)
                            , mStride2(stride2)
                            , mPlaneCnt(0)
                            , mUsage(0)
                            , mSerialNum(0)
                            , mWaterMark(STUFF_BUFFER_WATER_MARK * multiple)
                            , mMaxAmount(STUFF_BUFFER_MAX_AMOUNT * multiple)
                        {
                            //MY_LOGD_IF(mLogLevel > 1, "+++");
                            MY_LOGD_IF(mLogLevel > 0, "[%s]"
                                " 0x%x-%dx%d-%d.%d.%d *%d +%d", szName, format,
                                size.w, size.h, stride0, stride1, stride2,
                                multiple, writable);
                            //
                            if (mStride2 > 0) {
                                if (mStride1 > 0 && mStride0 > 0) {
                                    mPlaneCnt = 3;
                                }
                            } else if (mStride1 > 0) {
                                if (mStride0 > 0) {
                                    mPlaneCnt = 2;
                                }
                            } else if (mStride0 > 0) {
                                mPlaneCnt = 1;
                            }
                            if (mPlaneCnt == 0) {
                                MY_LOGW("[%s] stride invalid (%d.%d.%d)",
                                    msName.string(),
                                    mStride0, mStride1, mStride2);
                            }
                            //
                            mUsage = (GRALLOC_USAGE_SW_READ_OFTEN |
                                        GRALLOC_USAGE_HW_CAMERA_READ |
                                        GRALLOC_USAGE_HW_CAMERA_WRITE);
                            if(writable) {
                                mUsage |= GRALLOC_USAGE_SW_WRITE_OFTEN;
                            }
                            mvInfoMap.clear();
                            //MY_LOGD_IF(mLogLevel > 1, "---");
                        };

    virtual             ~StuffBufferPool()
                        {
                            //MY_LOGD_IF(mLogLevel > 1, "+++");
                            while (mvInfoMap.size() > 0) {
                                destroyBuffer(0); // it remove one of mvInfoMap
                            }
                            mvInfoMap.clear();
                            MY_LOGD_IF(mLogLevel > 1,
                                "[%s] 0x%x-%dx%d-%d.%d.%d", msName.string(),
                                mFormat, mSize.w, mSize.h,
                                mStride0, mStride1, mStride2);
                            //MY_LOGD_IF(mLogLevel > 1, "---");
                        };

    MBOOL               compareLayout(
                            MINT32 format,
                            MSize size,
                            MUINT32 stride0,
                            MUINT32 stride1,
                            MUINT32 stride2
                        );

    MERROR              acquireBuffer(
                            sp<IImageBuffer> & imageBuffer
                        );

    MERROR              releaseBuffer(
                            sp<IImageBuffer> & imageBuffer
                        );

    MERROR              createBuffer(
                            sp<IImageBuffer> & imageBuffer
                        );

    MERROR              destroyBuffer(
                            sp<IImageBuffer> & imageBuffer
                        );

    MERROR              destroyBuffer(
                            size_t index
                        );

private:
    MINT32              mLogLevel;
    android::String8    msName;
    MINT32              mFormat;
    MSize               mSize;
    MUINT32             mStride0;
    MUINT32             mStride1;
    MUINT32             mStride2;
    MUINT8              mPlaneCnt;
    MUINT               mUsage;
    MUINT32             mSerialNum;
    // it will destroy buffer while releasing, if pool_size > WaterMark
    MUINT32             mWaterMark;
    // it will not create buffer while acquiring, if pool_size >= MaxAmount
    MUINT32             mMaxAmount;
    DefaultKeyedVector< sp<IImageBuffer>, BufNote >
                        mvInfoMap;
};

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class StuffBufferManager {

public:
                        StuffBufferManager()
                            : mLogLevel(0)
                            , mLock()
                        {
                            MY_LOGD_IF(mLogLevel > 1, "+++");
                            mvPoolSet.clear();
                            MY_LOGD_IF(mLogLevel > 1, "---");
                        };

                        StuffBufferManager(MINT32 log)
                            : mLogLevel(log)
                            , mLock()
                        {
                            MY_LOGD_IF(mLogLevel > 1, "+++");
                            mvPoolSet.clear();
                            MY_LOGD_IF(mLogLevel > 1, "---");
                        };

    virtual             ~StuffBufferManager()
                        {
                            MY_LOGD_IF(mLogLevel > 1, "+++");
                            mvPoolSet.clear();
                            MY_LOGD_IF(mLogLevel > 1, "---");
                        };

    void                setLog(MINT32 log)
                        {
                            MY_LOGD_IF(mLogLevel > 1,
                                "StuffBufferManager log(%d)", log);
                            Mutex::Autolock _l(mLock);
                            mLogLevel = log;
                        }

    MERROR              acquireStoreBuffer(
                            sp<IImageBuffer> & imageBuffer,
                            char const * szName,
                            MINT32 format,
                            MSize size,
                            Vector<MUINT32> vStride,
                            MUINT8 multiple = 1,    // for burst mode
                            MBOOL writable = MFALSE // for SW write
                        );

    MERROR              releaseStoreBuffer(
                            sp<IImageBuffer> & imageBuffer
                        );

private:
    MINT32              mLogLevel;
    mutable Mutex       mLock;
    Vector<StuffBufferPool>
                        mvPoolSet;
};
#endif

#if 1
class TimingChecker
    : public virtual android::RefBase
{
    public:
    enum EVENT_TYPE
    {
        EVENT_TYPE_NONE         = 0,
        EVENT_TYPE_WARNING,
        EVENT_TYPE_ERROR,
        EVENT_TYPE_FATAL
    };
    //
    public:
    class Client
        : public virtual android::RefBase
    {
        public:
                            Client(char const * str, MUINT32 uTimeoutMs,
                                EVENT_TYPE eType)
                                : mLock()
                                , mLog(MFALSE)
                                , mStr(str)
                                , mTimeInvMs(uTimeoutMs)
                                , mBeginTsNs(0)
                                , mEndTsNs(0)
                                , mType(eType)
                            {
                                mBeginTsNs = ::systemTime();
                                mEndTsNs = mBeginTsNs +
                                    (ONE_MS_TO_NS * uTimeoutMs);
                                //setLog(MTRUE);
                                dump("TC_Client::CTR");
                            };
            virtual         ~Client()
                            {
                                dump("TC_Client::DTR");
                            };
        //
        public:
            MUINT32         getTimeInterval(void)
                            {
                                Mutex::Autolock _l(mLock);
                                return mTimeInvMs;
                            };
            nsecs_t         getTimeStamp(void)
                            {
                                Mutex::Autolock _l(mLock);
                                return mEndTsNs;
                            };
            void            setLog(MBOOL en)
                            {
                                Mutex::Autolock _l(mLock);
                                mLog = en;
                                return;
                            };
        //
        public:
            void            dump(char const * tag = NULL);
            void            action(void);
        //
        private:
            void            onLastStrongRef(const void* /*id*/);
        //
        private:
            mutable Mutex   mLock;
            MBOOL           mLog;
            android::String8
                            mStr;
            MUINT32         mTimeInvMs; // TimeInterval in msec
            nsecs_t         mBeginTsNs; // Begin TimeStamp in nsec
            nsecs_t         mEndTsNs; // End TimeStamp in nsec
            EVENT_TYPE      mType;
    };
    //
    private:
    class Record {
        public:
                            Record(nsecs_t ns, wp<TimingChecker::Client> pc)
                            : mTimeMarkNs(ns)
                            , mpClient(pc)
                            {
                            };
            virtual         ~Record()
                            {
                            };
            nsecs_t         mTimeMarkNs;
            wp<TimingChecker::Client>
                            mpClient;
    };
    //
    typedef TimingChecker::Record*  RecPtr;
    //
    private:
    class RecCmp {
        public:
                            RecCmp()
                            {
                            };
            virtual         ~RecCmp()
                            {
                            };
            MBOOL           operator()
                            (const RecPtr & rpL, const RecPtr & rpR)
                            {
                                return (rpL->mTimeMarkNs > rpR->mTimeMarkNs);
                            };
    };
    //
    private:
    class RecStore {
        public:
                            RecStore()
                            : mHeap()
                            {
                            };
            virtual         ~RecStore()
                            {
                            };
            size_t          size(void);

            MBOOL           isEmpty(void);

            MBOOL           addRec(RecPtr rp);

            RecPtr const &  getMin(void);

            void            delMin(void);

        //
        private:
            void            dump(char const * tag = NULL);
        //
        private:
            std::priority_queue< RecPtr, std::vector<RecPtr>, RecCmp >
                            mHeap;
    };
    //
    public:
                        TimingChecker()
                        : mLock()
                        , mClientCond()
                        , mWakeTiming(0)
                        , mExitPending(MFALSE)
                        , mRunning(MFALSE)
                        , mExitedCond()
                        , mEnterCond()
                        , mData()
                        {
                            //MY_LOGD("+++");
                            //MY_LOGD("---");
                        };

        virtual         ~TimingChecker()
                        {
                            //MY_LOGD("+++");
                            doRequestExit();
                            //MY_LOGD("---");
                        };
    //
    public:
        MBOOL           doThreadLoop(void);

        void            doRequestExit(void);

        void            doWaitReady(void);
    //
    public:
        sp<TimingChecker::Client>
                        createClient(char const * str, MUINT32 uTimeoutMs,
                            EVENT_TYPE eType);
    //
    private:
        nsecs_t         checkList(nsecs_t time);
    //
    private:
        mutable Mutex   mLock;
        Condition       mClientCond;
        nsecs_t         mWakeTiming;
        // for loop control
        MBOOL           mExitPending;
        MBOOL           mRunning;
        Condition       mExitedCond;
        Condition       mEnterCond;
        // for client record stroage
        RecStore        mData;
};
#endif


class TimingCheckerMgr
    : public virtual android::RefBase
{
    public:
                        TimingCheckerMgr(MUINT32 factor)
                            : mLock()
                            , mIsEn(MFALSE)
                            , mFactor(factor)
                            , mpTimingChecker(NULL)
                        {
                            //MY_LOGD("+++");
                            #if (IS_P1_LOGD)
                            mpTimingChecker = new TimingChecker();
                            #endif
                            //MY_LOGD("---");
                        };
        virtual         ~TimingCheckerMgr()
                        {
                            //MY_LOGD("+++");
                            //MY_LOGD("---");
                        };
    public:
        void            setEnable(MBOOL en);

        void            waitReady(void);

        void            onCheck(void);

        sp<TimingChecker::Client>
                        createClient(char const * str, MUINT32 uTimeoutMs,
                            TimingChecker::EVENT_TYPE eType =
                            TimingChecker::EVENT_TYPE_WARNING);
        //
    private:
        mutable Mutex   mLock;
        MBOOL           mIsEn;
        MUINT32         mFactor;
        sp<TimingChecker>
                        mpTimingChecker;
};


class LongExposureStatus
{
    #define P1_LONG_EXP_TIME_TH (500 * 1000000) //(1ms = 1000000ns)
    public:
                        LongExposureStatus()
                            : mLock()
                            , mThreshold(P1_LONG_EXP_TIME_TH)
                            , mRunning(MFALSE)
                        {
                            //MY_LOGD("+++");

                            #if 0 // for Long Expousre IT
                            #warning "[FIXME] force to change P1 LE threshold"
                            {
                                MUINT32 thd_ms =
                                    ::property_get_int32("debug.camera.p1exp",
                                    0);
                                if (thd_ms > 0) {
                                    mThreshold = thd_ms * 1000000;
                                }
                                MY_LOGI("debug.camera.p1exp = %d - "
                                    "Threshold = %lld", thd_ms, mThreshold);
                            }
                            #endif

                            //MY_LOGD("---");
                        };

        virtual         ~LongExposureStatus()
                        {
                            //MY_LOGD("+++");

                            //MY_LOGD("---");
                        };

        MBOOL           reset(MINT num);

        MBOOL           set(MINT num, MINT64 exp_ns);

        MBOOL           get(void);

    private:
        mutable Mutex   mLock;
        MINT64          mThreshold;
        MBOOL           mRunning;
        Vector<MINT32>  mvSet;
};


enum STAGE_DONE
{
    STAGE_DONE_START                = 0,
    STAGE_DONE_INIT_ITEM,
    STAGE_DONE_TOTAL
};

class ProcedureStageControl
    : public virtual android::RefBase
{
    class StageNote
        : public virtual android::RefBase
    {
        public:
                            StageNote(MUINT32 uId)
                                : mId(uId)
                                , mLock()
                                , mCond()
                                , mWait(MFALSE)
                                , mDone(MFALSE)
                                , mSuccess(MFALSE)
                            {
                                //MY_LOGD("(%d)", mId);
                            };
            virtual         ~StageNote()
                            {
                                Mutex::Autolock _l(mLock);
                                mDone = MTRUE;
                                if (mWait) {
                                    mCond.broadcast();
                                }
                                mWait = MFALSE;
                                //MY_LOGD("(%d)", mId);
                            };
            //
            MUINT32         mId;
            mutable Mutex   mLock;
            Condition       mCond;
            MBOOL           mWait;
            MBOOL           mDone;
            MBOOL           mSuccess;
    };

    public:
                        ProcedureStageControl(MUINT32 nStageAmount)
                        {
                            //MY_LOGD("+++");
                            mvpStage.clear();
                            for (MUINT32 i = 0; i < nStageAmount; i++) {
                                sp<StageNote> p = new StageNote(i);
                                mvpStage.push_back(p);
                            }
                            MY_LOGI("StageNum(%zu)", mvpStage.size());
                            //MY_LOGD("---");
                        };

        virtual         ~ProcedureStageControl()
                        {
                            //MY_LOGD("+++");
                            for (MUINT32 i = 0; i < mvpStage.size(); i++) {
                                sp<StageNote> p = mvpStage.editItemAt(i);
                                p = NULL;
                            }
                            mvpStage.clear();
                            //MY_LOGI("StageNum(%zu)", mvpStage.size());
                            //MY_LOGD("---");
                        };

        MBOOL           reset(void);

        MBOOL           wait(MUINT32 eStage, MBOOL& rSuccess);

        MBOOL           done(MUINT32 eStage, MBOOL bSuccess);

    private:
        Vector< sp<StageNote> >
                        mvpStage;
};

class ConcurrenceControl
    : public virtual android::RefBase
{
    public:
                        ConcurrenceControl()
                        : mLock()
                        , mIsAssistUsing(MFALSE)
                        , mpBufInfo(NULL)
                        , mpStageCtrl(NULL)
                        {
                            //MY_LOGD("+++");
                            mpStageCtrl =
                                new ProcedureStageControl(STAGE_DONE_TOTAL);
                            if (mpStageCtrl == NULL) {
                                MY_LOGE("ProcedureStageControl create fail");
                            }
                            //MY_LOGD("---");
                        };

        virtual         ~ConcurrenceControl()
                        {
                            //MY_LOGD("+++");
                            initBufInfo_clean();
                            if (mpStageCtrl != NULL) {
                                mpStageCtrl = NULL;
                            }
                            //MY_LOGD("---");
                        };

        MBOOL           initBufInfo_clean(void);

        MBOOL           initBufInfo_get(QBufInfo** ppBufInfo);

        MBOOL           initBufInfo_create(QBufInfo** ppBufInfo);

        void            setAidUsage(MBOOL enable);

        MBOOL           getAidUsage(void);

        void            cleanAidStage(void);

        sp<ProcedureStageControl>
                        getStageCtrl(void);

    private:
        mutable Mutex   mLock;
        MBOOL           mIsAssistUsing;
        QBufInfo*       mpBufInfo;
        sp<ProcedureStageControl>
                        mpStageCtrl;

};

class HardwareStateControl
    : public virtual android::RefBase
{
    public:
    enum STATE
    {
        STATE_NORMAL         = 0,
        STATE_SUS_WAIT_NUM,     // received the suspend meta and wait the node magic number assign
        STATE_SUS_WAIT_SYNC,    // received the suspend meta and wait the 3A CB to set frame
        STATE_SUS_READY,        // already called 3A/DRV suspend function
        STATE_SUS_DONE,         // thread was blocked and wait resume
        STATE_RES_WAIT_NUM,     // received the resume meta and wait the node magic number assign
        STATE_RES_WAIT_SYNC,    // called 3A/DRV resume function and wait the 3A CB for EnQ
        STATE_RES_WAIT_DONE,    // received the 3A CB after 3A/DRV resume function and wait the previous frames done
        STATE_MAX
    };

    public:
                        HardwareStateControl()
                        : mLock()
                        , mLogLevel(0)
                        , mOpenId(-1)
                        , mBurstNum(1)
                        , mState(STATE_NORMAL)
                        , mStandbySetNum(0)
                        , mStreamingSetNum(0)
                        , mShutterTimeUs(0)
                        , mRequestPass(MFALSE)
                        , mRequestCond()
                        , mThreadCond()
                        , mpCamIO(NULL)
                        , mp3A(NULL)
                        {
                            //MY_LOGD("+++");
                            mvStoreNum.clear();
                            //MY_LOGD("---");
                        };

        virtual         ~HardwareStateControl()
                        {
                            //MY_LOGD("+++");
                            mvStoreNum.clear();
                            //MY_LOGD("---");
                        };

        void            config(
                            MINT32 nLogLevel,
                            MINT32 nOpenId,
                            MUINT8 nBurstNum,
                            INormalPipe* pCamIO,
                            IHal3A_T* p3A
                        );

        MBOOL           isActive(void);

        void            checkReceiveFrame(IMetadata* pMeta);

        MBOOL           checkReceiveRestreaming(void);

        void            checkReceiveNode(MINT32 num, MBOOL bSkipEnQ,
                            MBOOL & rIsNeedEnQ, MINT32 & rShutterTimeUs);

        MBOOL           checkSetNum(MINT32 num);

        void            checkRequest(void);

        void            checkThreadStandby(void);

        void            checkThreadWeakup(void);

        MBOOL           checkFirstSync(void);

        MBOOL           checkSkipSync(void);

        MBOOL           checkSkipWait(void);

        MBOOL           checkSkipBlock(void);

        MBOOL           checkBufferState(void);

        MBOOL           checkDoneNum(MINT32 num);

        void            checkNotePass(MBOOL pass = MTRUE);

        void            setDropNum(MINT32 num);

        MINT32          getDropNum(void);

        void            reset(void);

    private:
        mutable Mutex   mLock;
        MINT32          mLogLevel;
        MINT32          mOpenId;
        MUINT8          mBurstNum;
        STATE           mState;
        Vector<MINT32>  mvStoreNum;
        MINT32          mStandbySetNum;
        MINT32          mStreamingSetNum;
        MINT32          mShutterTimeUs;
        MBOOL           mRequestPass;
        Condition       mRequestCond;
        Condition       mThreadCond;
        INormalPipe*    mpCamIO;
        IHal3A_T*       mp3A;

};


#endif //_MTK_HARDWARE_INCLUDE_MTKCAM_PIPELINE_HWNODE_P1NODE_UTILITY_H_

