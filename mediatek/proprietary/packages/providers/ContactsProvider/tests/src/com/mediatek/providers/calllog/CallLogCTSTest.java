package com.mediatek.providers.calllog;

import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.CallLog;
import android.provider.CallLog.Calls;
import android.provider.Contacts.Phones;
import android.test.InstrumentationTestCase;

import java.util.Date;

public class CallLogCTSTest extends InstrumentationTestCase {
    private ContentResolver mContentResolver;
    private ContentProviderClient mCallLogProvider;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mContentResolver = getInstrumentation().getTargetContext().getContentResolver();
        mCallLogProvider = mContentResolver.acquireContentProviderClient(CallLog.AUTHORITY);
    }

    /**
     * Test case for the behavior of the ContactsProvider's calls table
     * It does not test any APIs in android.provider.Contacts.java
     */
    public void testCallsTable() {
        final String[] CALLS_PROJECTION = new String[] {
                Calls._ID, Calls.NUMBER, Calls.DATE, Calls.DURATION, Calls.TYPE,
                Calls.NEW, Calls.CACHED_NAME, Calls.CACHED_NUMBER_TYPE,
                Calls.CACHED_NUMBER_LABEL, Calls.CACHED_FORMATTED_NUMBER,
                Calls.CACHED_MATCHED_NUMBER, Calls.CACHED_NORMALIZED_NUMBER,
                Calls.CACHED_LOOKUP_URI, Calls.CACHED_PHOTO_ID, Calls.COUNTRY_ISO,
                Calls.GEOCODED_LOCATION, Calls.CACHED_PHOTO_URI};
        final int ID_INDEX = 0;
        final int NUMBER_INDEX = 1;
        final int DATE_INDEX = 2;
        final int DURATION_INDEX = 3;
        final int TYPE_INDEX = 4;
        final int NEW_INDEX = 5;
        final int CACHED_NAME_INDEX = 6;
        final int CACHED_NUMBER_TYPE_INDEX = 7;
        final int CACHED_NUMBER_LABEL_INDEX = 8;
        final int CACHED_FORMATTED_NUMBER_INDEX = 9;
        final int CACHED_MATCHED_NUMBER_INDEX = 10;
        final int CACHED_NORMALIZED_NUMBER_INDEX = 11;
        final int CACHED_LOOKUP_URI_INDEX = 12;
        final int CACHED_PHOTO_ID_INDEX = 13;
        final int COUNTRY_ISO_INDEX = 14;
        final int GEOCODED_LOCATION_INDEX = 15;
        final int CACHED_PHOTO_URI_INDEX = 16;

        String insertCallsNumber = "0123456789";
        int insertCallsDuration = 120;
        String insertCallsName = "cached_name_insert";
        String insertCallsNumberLabel = "cached_label_insert";

        String updateCallsNumber = "987654321";
        int updateCallsDuration = 310;
        String updateCallsName = "cached_name_update";
        String updateCallsNumberLabel = "cached_label_update";
        String updateCachedFormattedNumber = "987-654-4321";
        String updateCachedMatchedNumber = "987-654-4321";
        String updateCachedNormalizedNumber = "+1987654321";
        String updateCachedLookupUri = "cached_lookup_uri_update";
        long updateCachedPhotoId = 100;
        String updateCachedPhotoUri = "content://com.android.contacts/display_photo/1";
        String updateCountryIso = "hk";
        String updateGeocodedLocation = "Hong Kong";

        try {
            // Test: insert
            int insertDate = (int) new Date().getTime();
            ContentValues value = new ContentValues();
            value.put(Calls.NUMBER, insertCallsNumber);
            value.put(Calls.DATE, insertDate);
            value.put(Calls.DURATION, insertCallsDuration);
            value.put(Calls.TYPE, Calls.INCOMING_TYPE);
            value.put(Calls.NEW, 0);
            value.put(Calls.CACHED_NAME, insertCallsName);
            value.put(Calls.CACHED_NUMBER_TYPE, Phones.TYPE_HOME);
            value.put(Calls.CACHED_NUMBER_LABEL, insertCallsNumberLabel);

            Uri uri = mCallLogProvider.insert(Calls.CONTENT_URI, value);
            Cursor cursor = mCallLogProvider.query(
                    Calls.CONTENT_URI, CALLS_PROJECTION,
                    Calls.NUMBER + " = ?",
                    new String[] {insertCallsNumber}, null, null);
            assertTrue(cursor.moveToNext());
            assertEquals(insertCallsNumber, cursor.getString(NUMBER_INDEX));
            assertEquals(insertDate, cursor.getInt(DATE_INDEX));
            assertEquals(insertCallsDuration, cursor.getInt(DURATION_INDEX));
            assertEquals(Calls.INCOMING_TYPE, cursor.getInt(TYPE_INDEX));
            assertEquals(0, cursor.getInt(NEW_INDEX));
            assertEquals(insertCallsName, cursor.getString(CACHED_NAME_INDEX));
            assertEquals(Phones.TYPE_HOME, cursor.getInt(CACHED_NUMBER_TYPE_INDEX));
            assertEquals(insertCallsNumberLabel, cursor.getString(CACHED_NUMBER_LABEL_INDEX));
            int id = cursor.getInt(ID_INDEX);
            cursor.close();

            // Test: update. Also add new cached fields to simulate extra cached fields being
            // inserted into the call log after the initial lookup.
            int now = (int) new Date().getTime();
            value.clear();
            value.put(Calls.NUMBER, updateCallsNumber);
            value.put(Calls.DATE, now);
            value.put(Calls.DURATION, updateCallsDuration);
            value.put(Calls.TYPE, Calls.MISSED_TYPE);
            value.put(Calls.NEW, 1);
            value.put(Calls.CACHED_NAME, updateCallsName);
            value.put(Calls.CACHED_NUMBER_TYPE, Phones.TYPE_CUSTOM);
            value.put(Calls.CACHED_NUMBER_LABEL, updateCallsNumberLabel);
            value.put(Calls.CACHED_FORMATTED_NUMBER, updateCachedFormattedNumber);
            value.put(Calls.CACHED_MATCHED_NUMBER, updateCachedMatchedNumber);
            value.put(Calls.CACHED_NORMALIZED_NUMBER, updateCachedNormalizedNumber);
            value.put(Calls.CACHED_PHOTO_ID, updateCachedPhotoId);
            value.put(Calls.CACHED_PHOTO_URI, updateCachedPhotoUri);
            value.put(Calls.COUNTRY_ISO, updateCountryIso);
            value.put(Calls.GEOCODED_LOCATION, updateGeocodedLocation);
            value.put(Calls.CACHED_LOOKUP_URI, updateCachedLookupUri);

            mCallLogProvider.update(uri, value, null, null);
            cursor = mCallLogProvider.query(Calls.CONTENT_URI, CALLS_PROJECTION,
                    Calls._ID + " = " + id, null, null, null);
            assertTrue(cursor.moveToNext());
            assertEquals(updateCallsNumber, cursor.getString(NUMBER_INDEX));
            assertEquals(now, cursor.getInt(DATE_INDEX));
            assertEquals(updateCallsDuration, cursor.getInt(DURATION_INDEX));
            assertEquals(Calls.MISSED_TYPE, cursor.getInt(TYPE_INDEX));
            assertEquals(1, cursor.getInt(NEW_INDEX));
            assertEquals(updateCallsName, cursor.getString(CACHED_NAME_INDEX));
            assertEquals(Phones.TYPE_CUSTOM, cursor.getInt(CACHED_NUMBER_TYPE_INDEX));
            assertEquals(updateCallsNumberLabel, cursor.getString(CACHED_NUMBER_LABEL_INDEX));
            assertEquals(updateCachedFormattedNumber,
                    cursor.getString(CACHED_FORMATTED_NUMBER_INDEX));
            assertEquals(updateCachedMatchedNumber, cursor.getString(CACHED_MATCHED_NUMBER_INDEX));
            assertEquals(updateCachedNormalizedNumber,
                    cursor.getString(CACHED_NORMALIZED_NUMBER_INDEX));
            assertEquals(updateCachedPhotoId, cursor.getLong(CACHED_PHOTO_ID_INDEX));
            assertEquals(updateCachedPhotoUri, cursor.getString(CACHED_PHOTO_URI_INDEX));
            assertEquals(updateCountryIso, cursor.getString(COUNTRY_ISO_INDEX));
            assertEquals(updateGeocodedLocation, cursor.getString(GEOCODED_LOCATION_INDEX));
            assertEquals(updateCachedLookupUri, cursor.getString(CACHED_LOOKUP_URI_INDEX));
            cursor.close();

            // Test: delete
            mCallLogProvider.delete(Calls.CONTENT_URI, Calls._ID + " = " + id, null);
            cursor = mCallLogProvider.query(Calls.CONTENT_URI, CALLS_PROJECTION,
                    Calls._ID + " = " + id, null, null, null);
            assertEquals(0, cursor.getCount());
            cursor.close();
        } catch (RemoteException e) {
            fail("Unexpected RemoteException");
        }
    }

}
