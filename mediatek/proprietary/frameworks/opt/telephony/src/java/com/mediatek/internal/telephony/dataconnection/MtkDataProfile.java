/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mediatek.internal.telephony.dataconnection;

import android.telephony.ServiceState;
import android.text.TextUtils;

import com.android.internal.telephony.dataconnection.ApnSetting;
import com.android.internal.telephony.dataconnection.DataProfile;
import com.android.internal.telephony.RILConstants;

public class MtkDataProfile extends DataProfile {

    //inactive timer
    public final int inactiveTimer;

    public MtkDataProfile(int profileId, String apn, String protocol, int authType,
                String user, String password, int type, int maxConnsTime, int maxConns,
                int waitTime, boolean enabled, int supportedApnTypesBitmap, String roamingProtocol,
                int bearerBitmap, int mtu, String mvnoType, String mvnoMatchData,
                boolean modemCognitive, int inactiveTimer) {

        super(profileId, apn, protocol, authType, user, password,
                type, maxConnsTime, maxConns, waitTime,
                enabled, supportedApnTypesBitmap,
                roamingProtocol, bearerBitmap, mtu,
                mvnoType, mvnoMatchData, modemCognitive);

        this.inactiveTimer = inactiveTimer;
    }

    public MtkDataProfile(ApnSetting apn, int inactiveTimer) {
        this(apn, apn.profileId, inactiveTimer);
    }

    public MtkDataProfile(ApnSetting apn, int profileId, int inactiveTimer) {
        this(profileId, apn.apn, apn.protocol,
                apn.authType, apn.user, apn.password, apn.bearerBitmask == 0
                        ? TYPE_COMMON : (ServiceState.bearerBitmapHasCdma(apn.bearerBitmask)
                        ? TYPE_3GPP2 : TYPE_3GPP),
                apn.maxConnsTime, apn.maxConns, apn.waitTime, apn.carrierEnabled, apn.typesBitmap,
                apn.roamingProtocol, apn.bearerBitmask, apn.mtu, apn.mvnoType, apn.mvnoMatchData,
                apn.modemCognitive, inactiveTimer);
    }

    @Override
    public String toString() {
        return "MtkDataProfile=" + profileId + "/" + apn + "/" + protocol + "/" + authType
                + "/" + user + "/" + password + "/" + type + "/" + maxConnsTime
                + "/" + maxConns + "/" + waitTime + "/" + enabled + "/" + supportedApnTypesBitmap
                + "/" + roamingProtocol + "/" + bearerBitmap + "/" + mtu + "/" + mvnoType + "/"
                + mvnoMatchData + "/" + modemCognitive + "/" + inactiveTimer;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof MtkDataProfile == false) return false;
        return toString().equals(o.toString());
    }
}
