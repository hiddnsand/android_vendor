/*
 * jpegint.h
 *
 * Copyright (C) 1991-1997, Thomas G. Lane.
 * This file is part of the Independent JPEG Group's software.
 * For conditions of distribution and use, see the accompanying README file.
 *
 * This file provides common declarations for the various JPEG modules.
 * These declarations are considered internal to the JPEG library; most
 * applications using the library shouldn't need to include this file.
 */


/* Declarations for both compression & decompression */

typedef enum {			/* Operating modes for buffer controllers */
	JBUF_PASS_THRU_MTK,		/* Plain stripwise operation */
	/* Remaining modes require a full-image buffer to have been created */
	JBUF_SAVE_SOURCE_MTK,	/* Run source subobject only, save output */
	JBUF_CRANK_DEST_MTK,	/* Run dest subobject only, using saved data */
	JBUF_SAVE_AND_PASS_MTK	/* Run both subobjects, save output */
} J_BUF_MODE_MTK;

/* Values of global_state field (jdapi.c has some dependencies on ordering!) */
#define CSTATE_START_MTK	100	/* after create_compress */
#define CSTATE_SCANNING_MTK	101	/* start_compress done, write_scanlines OK */
#define CSTATE_RAW_OK_MTK	102	/* start_compress done, write_raw_data OK */
#define CSTATE_WRCOEFS_MTK	103	/* jpeg_write_coefficients done */
#define DSTATE_START_MTK	200	/* after create_decompress */
#define DSTATE_INHEADER_MTK	201	/* reading header markers, no SOS yet */
#define DSTATE_READY_MTK	202	/* found SOS, ready for start_decompress */
#define DSTATE_PRELOAD_MTK	203	/* reading multiscan file in start_decompress */
#define DSTATE_PRESCAN_MTK	204	/* performing dummy pass for 2-pass quant */
#define DSTATE_SCANNING_MTK	205	/* start_decompress done, read_scanlines OK */
#define DSTATE_RAW_OK_MTK	206	/* start_decompress done, read_raw_data OK */
#define DSTATE_BUFIMAGE_MTK	207	/* expecting jpeg_start_output */
#define DSTATE_BUFPOST_MTK	208	/* looking for SOS/EOI in jpeg_finish_output */
#define DSTATE_RDCOEFS_MTK	209	/* reading file in jpeg_read_coefficients */
#define DSTATE_STOPPING_MTK	210	/* looking for EOI in jpeg_finish_decompress */


/* Declarations for compression modules */

/* Master control module */
struct jpeg_comp_master_MTK {
	JMETHOD_MTK(void, prepare_for_pass, (j_compress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, pass_startup, (j_compress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, finish_pass, (j_compress_ptr_MTK cinfo));

	/* State variables made visible to other modules */
	boolean_MTK call_pass_startup;	/* True if pass_startup must be called */
	boolean_MTK is_last_pass;	/* True during last pass */
};

/* Main buffer control (downsampled-data buffer) */
struct jpeg_c_main_controller_MTK {
	JMETHOD_MTK(void, start_pass, (j_compress_ptr_MTK cinfo, J_BUF_MODE_MTK pass_mode));
	 JMETHOD_MTK(void, process_data, (j_compress_ptr_MTK cinfo,
				      JSAMPARRAY_MTK input_buf, JDIMENSION_MTK * in_row_ctr,
				      JDIMENSION_MTK in_rows_avail));
};

/* Compression preprocessing (downsampling input buffer control) */
struct jpeg_c_prep_controller_MTK {
	JMETHOD_MTK(void, start_pass, (j_compress_ptr_MTK cinfo, J_BUF_MODE_MTK pass_mode));
	 JMETHOD_MTK(void, pre_process_data, (j_compress_ptr_MTK cinfo,
					  JSAMPARRAY_MTK input_buf,
					  JDIMENSION_MTK * in_row_ctr,
					  JDIMENSION_MTK in_rows_avail,
					  JSAMPIMAGE_MTK output_buf,
					  JDIMENSION_MTK * out_row_group_ctr,
					  JDIMENSION_MTK out_row_groups_avail));
};

/* Coefficient buffer control */
struct jpeg_c_coef_controller_MTK {
	JMETHOD_MTK(void, start_pass, (j_compress_ptr_MTK cinfo, J_BUF_MODE_MTK pass_mode));
	 JMETHOD_MTK(boolean_MTK, compress_data, (j_compress_ptr_MTK cinfo, JSAMPIMAGE_MTK input_buf));
#if 1				/* def JPEG_THREAD_SUPPORT */
	 JMETHOD_MTK(void, compress_data_thread_with_output, (void *arg));
	 JMETHOD_MTK(void, compress_data_thread_no_output, (void *arg));
#endif
};

/* Colorspace conversion */
struct jpeg_color_converter_MTK {
	JMETHOD_MTK(void, start_pass, (j_compress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, color_convert, (j_compress_ptr_MTK cinfo,
				       JSAMPARRAY_MTK input_buf, JSAMPIMAGE_MTK output_buf,
				       JDIMENSION_MTK output_row, int num_rows));
};

/* Downsampling */
struct jpeg_downsampler_MTK {
	JMETHOD_MTK(void, start_pass, (j_compress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, downsample, (j_compress_ptr_MTK cinfo,
				    JSAMPIMAGE_MTK input_buf, JDIMENSION_MTK in_row_index,
				    JSAMPIMAGE_MTK output_buf, JDIMENSION_MTK out_row_group_index));

	boolean_MTK need_context_rows;	/* TRUE if need rows above & below */
};

/* Forward DCT (also controls coefficient quantization) */
struct jpeg_forward_dct_MTK {
	JMETHOD_MTK(void, start_pass, (j_compress_ptr_MTK cinfo));
	/* perhaps this should be an array??? */
	 JMETHOD_MTK(void, forward_DCT, (j_compress_ptr_MTK cinfo,
				     jpeg_component_info_MTK * compptr,
				     JSAMPARRAY_MTK sample_data, JBLOCKROW_MTK coef_blocks,
				     JDIMENSION_MTK start_row, JDIMENSION_MTK start_col,
				     JDIMENSION_MTK num_blocks, int *last_coffs_zero));
};

/* Entropy encoding */
struct jpeg_entropy_encoder_MTK {
	JMETHOD_MTK(void, start_pass, (j_compress_ptr_MTK cinfo, boolean_MTK gather_statistics));
	 JMETHOD_MTK(boolean_MTK, encode_mcu, (j_compress_ptr_MTK cinfo, JBLOCKROW_MTK * MCU_data));
	 JMETHOD_MTK(void, finish_pass, (j_compress_ptr_MTK cinfo));
#if 1				/* def JPEG_THREAD_SUPPORT */
	 JMETHOD_MTK(boolean_MTK, encode_mcu_thread,
		 (j_compress_ptr_MTK cinfo, JBLOCKROW_MTK * MCU_data, int index));
	 JMETHOD_MTK(boolean_MTK, emit_restart_thread,
		 (j_compress_ptr_MTK cinfo, int index, int restart_flag, int restart));
#endif
};

/* Marker writing */
struct jpeg_marker_writer_MTK {
	JMETHOD_MTK(void, write_file_header, (j_compress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, write_frame_header, (j_compress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, write_scan_header, (j_compress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, write_file_trailer, (j_compress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, write_tables_only, (j_compress_ptr_MTK cinfo));
	/* These routines are exported to allow insertion of extra markers */
	/* Probably only COM and APPn markers should be written this way */
	 JMETHOD_MTK(void, write_marker_header, (j_compress_ptr_MTK cinfo, int marker,
					     unsigned int datalen));
	 JMETHOD_MTK(void, write_marker_byte, (j_compress_ptr_MTK cinfo, int val));
};


/* Declarations for decompression modules */

/* Master control module */
struct jpeg_decomp_master_MTK {
	JMETHOD_MTK(void, prepare_for_output_pass, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, finish_output_pass, (j_decompress_ptr_MTK cinfo));

	/* State variables made visible to other modules */
	boolean_MTK is_dummy_pass;	/* True during 1st pass for 2-pass quant */
};

/* Input control module */
struct jpeg_input_controller_MTK {
	JMETHOD_MTK(int, consume_input, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(int, consume_input_build_huffman_index, (j_decompress_ptr_MTK cinfo,
							  huffman_index_MTK * index, int scan_count));
	 JMETHOD_MTK(int, consume_markers, (j_decompress_ptr_MTK cinfo,
					huffman_index_MTK * index, int scan_count));
	 JMETHOD_MTK(void, reset_input_controller, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, start_input_pass, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, finish_input_pass, (j_decompress_ptr_MTK cinfo));

	/* State variables made visible to other modules */
	boolean_MTK has_multiple_scans;	/* True if file has multiple scans */
	boolean_MTK eoi_reached;	/* True when EOI has been consumed */
};

/* Main buffer control (downsampled-data buffer) */
struct jpeg_d_main_controller_MTK {
	JMETHOD_MTK(void, start_pass, (j_decompress_ptr_MTK cinfo, J_BUF_MODE_MTK pass_mode));
	 JMETHOD_MTK(void, process_data, (j_decompress_ptr_MTK cinfo,
				      JSAMPARRAY_MTK output_buf, JDIMENSION_MTK * out_row_ctr,
				      JDIMENSION_MTK out_rows_avail));
};

/* Coefficient buffer control */
struct jpeg_d_coef_controller_MTK {
	JMETHOD_MTK(void, start_input_pass, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(int, consume_data, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(int, consume_data_build_huffman_index, (j_decompress_ptr_MTK cinfo,
							 huffman_index_MTK * index, int scan_count));
	 JMETHOD_MTK(void, start_output_pass, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(int, decompress_data, (j_decompress_ptr_MTK cinfo, JSAMPIMAGE_MTK output_buf));
	/* Pointer to array of coefficient virtual arrays, or NULL if none */
	jvirt_barray_ptr_MTK *coef_arrays;

	/* column number of the first and last tile, respectively */
	int column_left_boundary;
	int column_right_boundary;

	/* column number of the first and last MCU, respectively */
	int MCU_column_left_boundary;
	int MCU_column_right_boundary;

	/* the number of MCU columns to skip from the indexed MCU, iM,
	 * to the requested MCU boundary, rM, where iM is the MCU that we sample
	 * into our index and is the nearest one to the left of rM.
	 */
	int MCU_columns_to_skip;
};

/* Decompression postprocessing (color quantization buffer control) */
struct jpeg_d_post_controller_MTK {
	JMETHOD_MTK(void, start_pass, (j_decompress_ptr_MTK cinfo, J_BUF_MODE_MTK pass_mode));
	 JMETHOD_MTK(void, post_process_data, (j_decompress_ptr_MTK cinfo,
					   JSAMPIMAGE_MTK input_buf,
					   JDIMENSION_MTK * in_row_group_ctr,
					   JDIMENSION_MTK in_row_groups_avail,
					   JSAMPARRAY_MTK output_buf,
					   JDIMENSION_MTK * out_row_ctr, JDIMENSION_MTK out_rows_avail));
};

/* Marker reading & parsing */
struct jpeg_marker_reader_MTK {
	JMETHOD_MTK(void, reset_marker_reader, (j_decompress_ptr_MTK cinfo));
	/* Read markers until SOS or EOI.
	 * Returns same codes as are defined for jpeg_consume_input:
	 * JPEG_SUSPENDED, JPEG_REACHED_SOS, or JPEG_REACHED_EOI.
	 */
	 JMETHOD_MTK(int, read_markers, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, get_sos_marker_position, (j_decompress_ptr_MTK cinfo, huffman_index_MTK * index));
	/* Read a restart marker --- exported for use by entropy decoder only */
	jpeg_marker_parser_method_MTK read_restart_marker;

	/* State of marker reader --- nominally internal, but applications
	 * supplying COM or APPn handlers might like to know the state.
	 */
	boolean_MTK saw_SOI;	/* found SOI? */
	boolean_MTK saw_SOF;	/* found SOF? */
	int next_restart_num;	/* next restart number expected (0-7) */
	int current_sos_marker_position;
	unsigned int discarded_bytes;	/* # of bytes skipped looking for a marker */
};

/* Entropy decoding */
struct jpeg_entropy_decoder_MTK {
	JMETHOD_MTK(void, start_pass, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(boolean_MTK, decode_mcu, (j_decompress_ptr_MTK cinfo, JBLOCKROW_MTK * MCU_data));
	 JMETHOD_MTK(boolean_MTK, decode_mcu_discard_coef, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, configure_huffman_decoder, (j_decompress_ptr_MTK cinfo,
						   huffman_offset_data_MTK offset));
	 JMETHOD_MTK(void, get_huffman_decoder_configuration, (j_decompress_ptr_MTK cinfo,
							   huffman_offset_data_MTK * offset));

	/* This is here to share code between baseline and progressive decoders; */
	/* other modules probably should not use it */
	boolean_MTK insufficient_data;	/* set TRUE after emitting warning */

	huffman_index_MTK *index;
};

/* Inverse DCT (also performs dequantization) */
typedef JMETHOD_MTK(void, inverse_DCT_method_ptr_MTK,
		(j_decompress_ptr_MTK cinfo, jpeg_component_info_MTK * compptr,
		 JCOEFPTR_MTK coef_block, JSAMPARRAY_MTK output_buf, JDIMENSION_MTK output_col));

struct jpeg_inverse_dct_MTK {
	JMETHOD_MTK(void, start_pass, (j_decompress_ptr_MTK cinfo));
	/* It is useful to allow each component to have a separate IDCT method. */
	inverse_DCT_method_ptr_MTK inverse_DCT[MAX_COMPONENTS_MTK];
};

/* Upsampling (note that upsampler must also call color converter) */
struct jpeg_upsampler_MTK {
	JMETHOD_MTK(void, start_pass, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, upsample, (j_decompress_ptr_MTK cinfo,
				  JSAMPIMAGE_MTK input_buf,
				  JDIMENSION_MTK * in_row_group_ctr,
				  JDIMENSION_MTK in_row_groups_avail,
				  JSAMPARRAY_MTK output_buf,
				  JDIMENSION_MTK * out_row_ctr, JDIMENSION_MTK out_rows_avail));

	boolean_MTK need_context_rows;	/* TRUE if need rows above & below */
};

/* Colorspace conversion */
struct jpeg_color_deconverter_MTK {
	JMETHOD_MTK(void, start_pass, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, color_convert, (j_decompress_ptr_MTK cinfo,
				       JSAMPIMAGE_MTK input_buf, JDIMENSION_MTK input_row,
				       JSAMPARRAY_MTK output_buf, int num_rows));
};

/* Color quantization or color precision reduction */
struct jpeg_color_quantizer_MTK {
	JMETHOD_MTK(void, start_pass, (j_decompress_ptr_MTK cinfo, boolean_MTK is_pre_scan));
	 JMETHOD_MTK(void, color_quantize, (j_decompress_ptr_MTK cinfo,
					JSAMPARRAY_MTK input_buf, JSAMPARRAY_MTK output_buf, int num_rows));
	 JMETHOD_MTK(void, finish_pass, (j_decompress_ptr_MTK cinfo));
	 JMETHOD_MTK(void, new_color_map, (j_decompress_ptr_MTK cinfo));
};


/* Miscellaneous useful macros */

#undef MAX
#define MAX(a, b)	((a) > (b) ? (a) : (b))
#undef MIN
#define MIN(a, b)	((a) < (b) ? (a) : (b))


/* We assume that right shift corresponds to signed division by 2 with
 * rounding towards minus infinity.  This is correct for typical "arithmetic
 * shift" instructions that shift in copies of the sign bit.  But some
 * C compilers implement >> with an unsigned shift.  For these machines you
 * must define RIGHT_SHIFT_IS_UNSIGNED.
 * RIGHT_SHIFT provides a proper signed right shift of an INT32 quantity.
 * It is only applied with constant shift counts.  SHIFT_TEMPS must be
 * included in the variables of any routine using RIGHT_SHIFT.
 */

#ifdef RIGHT_SHIFT_IS_UNSIGNED_MTK
#define SHIFT_TEMPS_MTK	INT32_MTK shift_temp;
#define RIGHT_SHIFT_MTK(x, shft)  \
	((shift_temp = (x)) < 0 ? \
	 (shift_temp >> (shft)) | ((~((INT32_MTK) 0)) << (32-(shft))) : \
	 (shift_temp >> (shft)))
#else
#define SHIFT_TEMPS_MTK
#define RIGHT_SHIFT_MTK(x, shft)	((x) >> (shft))
#endif


/* Short forms of external names for systems with brain-damaged linkers. */

#ifdef NEED_SHORT_EXTERNAL_NAMES_MTK
#define jinit_compress_master_MTK	jICompress
#define jinit_c_master_control_MTK	jICMaster
#define jinit_c_main_controller_MTK	jICMainC
#define jinit_c_prep_controller_MTK	jICPrepC
#define jinit_c_coef_controller_MTK	jICCoefC
#define jinit_color_converter_MTK	jICColor
#define jinit_downsampler_MTK	jIDownsampler
#define jinit_forward_dct_MTK	jIFDCT
#define jinit_huff_encoder_MTK	jIHEncoder
#define jinit_phuff_encoder_MTK	jIPHEncoder
#define jinit_marker_writer_MTK	jIMWriter
#define jinit_master_decompress_MTK	jIDMaster
#define jinit_d_main_controller_MTK	jIDMainC
#define jinit_d_coef_controller_MTK	jIDCoefC
#define jinit_d_post_controller_MTK	jIDPostC
#define jinit_input_controller_MTK	jIInCtlr
#define jinit_marker_reader_MTK	jIMReader
#define jinit_huff_decoder_MTK	jIHDecoder
#define jinit_phuff_decoder_MTK	jIPHDecoder
#define jinit_inverse_dct_MTK	jIIDCT
#define jinit_upsampler_MTK		jIUpsampler
#define jinit_color_deconverter_MTK	jIDColor
#define jinit_1pass_quantizer_MTK	jI1Quant
#define jinit_2pass_quantizer_MTK	jI2Quant
#define jinit_merged_upsampler_MTK	jIMUpsampler
#define jinit_memory_mgr_MTK	jIMemMgr
#define jdiv_round_up_MTK		jDivRound
#define jround_up_MTK		jRound
#define jcopy_sample_rows_MTK	jCopySamples
#define jcopy_block_row_MTK		jCopyBlocks
#define jzero_far_MTK		jZeroFar
#define jpeg_zigzag_order_MTK	jZIGTable
#define jpeg_natural_order_MTK	jZAGTable
#endif				/* NEED_SHORT_EXTERNAL_NAMES */

#define jinit_compress_master_MTK(cinfo) \
    jinit_compress_master_MTK(cinfo)
#define jinit_c_master_control_MTK(cinfo, transcode_only) \
    jinit_c_master_control_MTK(cinfo, transcode_only)
#define jinit_c_main_controller_MTK(cinfo, need_full_buffer) \
    jinit_c_main_controller_MTK(cinfo, need_full_buffer)
#define jinit_c_prep_controller_MTK(cinfo, need_full_buffer) \
    jinit_c_prep_controller_MTK(cinfo, need_full_buffer)
#define jinit_c_coef_controller_MTK(cinfo, need_full_buffer) \
    jinit_c_coef_controller_MTK(cinfo, need_full_buffer)
#define jinit_color_converter_MTK(cinfo) \
    jinit_color_converter_MTK(cinfo)
#define jinit_downsampler_MTK(cinfo) \
    jinit_downsampler_MTK(cinfo)
#define jinit_forward_dct_MTK(cinfo) \
    jinit_forward_dct_MTK(cinfo)
#define jinit_huff_encoder_MTK(cinfo) \
    jinit_huff_encoder_MTK(cinfo)
#define jinit_phuff_encoder_MTK(cinfo) \
    jinit_phuff_encoder_MTK(cinfo)
#define jinit_marker_writer_MTK(cinfo) \
    jinit_marker_writer_MTK(cinfo)
#define jinit_master_decompress_MTK(cinfo) \
    jinit_master_decompress_MTK(cinfo)
#define jinit_d_main_controller_MTK(cinfo, need_full_buffer) \
    jinit_d_main_controller_MTK(cinfo, need_full_buffer)
#define jinit_d_coef_controller_MTK(cinfo, need_full_buffer) \
    jinit_d_coef_controller_MTK(cinfo, need_full_buffer)
#define jinit_d_post_controller_MTK(cinfo, need_full_buffer) \
    jinit_d_post_controller_MTK(cinfo, need_full_buffer)
#define jinit_input_controller_MTK(cinfo) \
    jinit_input_controller_MTK(cinfo)
#define jinit_marker_reader_MTK(cinfo) \
    jinit_marker_reader_MTK(cinfo)
#define jinit_huff_decoder_MTK(cinfo) \
    jinit_huff_decoder_MTK(cinfo)
#define jinit_phuff_decoder_MTK(cinfo) \
    jinit_phuff_decoder_MTK(cinfo)
#define jinit_inverse_dct_MTK(cinfo) \
    jinit_inverse_dct_MTK(cinfo)
#define jinit_upsampler_MTK(cinfo) \
    jinit_upsampler_MTK(cinfo)
#define jinit_color_deconverter_MTK(cinfo) \
    jinit_color_deconverter_MTK(cinfo)
#define jinit_1pass_quantizer_MTK(cinfo) \
    jinit_1pass_quantizer_MTK(cinfo)
#define jinit_2pass_quantizer_MTK(cinfo) \
    jinit_2pass_quantizer_MTK(cinfo)
#define jinit_merged_upsampler_MTK(cinfo) \
    jinit_merged_upsampler_MTK(cinfo)
#define jinit_memory_mgr_MTK(cinfo) \
    jinit_memory_mgr_MTK(cinfo)
#define jcopy_block_row_MTK(cinfo, output_row, num_blocks) \
    jcopy_block_row_MTK(cinfo, output_row, num_blocks)
#define jset_input_stream_position_MTK(cinfo, offset) \
    jset_input_stream_position_MTK(cinfo, offset)
#define jset_input_stream_position_bit_MTK(cinfo, byte_offset, bit_left, buf) \
    jset_input_stream_position_bit_MTK(cinfo, byte_offset, bit_left, buf)
#define jget_input_stream_position_MTK(cinfo) \
    jget_input_stream_position_MTK(cinfo)

/* Compression module initialization routines */
EXTERN_MTK(void)
jinit_compress_master_MTK JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_c_master_control_MTK JPP_MTK((j_compress_ptr_MTK cinfo, boolean_MTK transcode_only));
EXTERN_MTK(void)
jinit_c_main_controller_MTK JPP_MTK((j_compress_ptr_MTK cinfo, boolean_MTK need_full_buffer));
EXTERN_MTK(void)
jinit_c_prep_controller_MTK JPP_MTK((j_compress_ptr_MTK cinfo, boolean_MTK need_full_buffer));
EXTERN_MTK(void)
jinit_c_coef_controller_MTK JPP_MTK((j_compress_ptr_MTK cinfo, boolean_MTK need_full_buffer));
EXTERN_MTK(void)
jinit_color_converter_MTK JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_downsampler_MTK JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_forward_dct_MTK JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_huff_encoder_MTK JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_phuff_encoder_MTK JPP_MTK((j_compress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_marker_writer_MTK JPP_MTK((j_compress_ptr_MTK cinfo));
/* Decompression module initialization routines */
EXTERN_MTK(void)
jinit_master_decompress_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_d_main_controller_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, boolean_MTK need_full_buffer));
EXTERN_MTK(void)
jinit_d_coef_controller_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, boolean_MTK need_full_buffer));
EXTERN_MTK(void)
jinit_d_post_controller_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, boolean_MTK need_full_buffer));
EXTERN_MTK(void)
jinit_input_controller_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_marker_reader_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_huff_decoder_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_huff_decoder_no_data_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_phuff_decoder_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_inverse_dct_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_upsampler_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_color_deconverter_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_1pass_quantizer_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_2pass_quantizer_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void)
jinit_merged_upsampler_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
EXTERN_MTK(void) jpeg_decompress_per_scan_setup_MTK(j_decompress_ptr_MTK cinfo);
/* Memory manager initialization */
EXTERN_MTK(void)
jinit_memory_mgr_MTK JPP_MTK((j_common_ptr_MTK cinfo));

/* Utility routines in jutils.c */
EXTERN_MTK(long)
jdiv_round_up_MTK JPP_MTK((long a, long b));
EXTERN_MTK(long)
jround_up_MTK JPP_MTK((long a, long b));
EXTERN_MTK(long)
jmin_MTK JPP_MTK((long a, long b));
EXTERN_MTK(void)
jcopy_sample_rows_MTK JPP_MTK((JSAMPARRAY_MTK input_array, int source_row,
		       JSAMPARRAY_MTK output_array, int dest_row, int num_rows, JDIMENSION_MTK num_cols));
EXTERN_MTK(void)
jcopy_block_row_MTK JPP_MTK((JBLOCKROW_MTK input_row, JBLOCKROW_MTK output_row, JDIMENSION_MTK num_blocks));
EXTERN_MTK(void)
jzero_far_MTK JPP_MTK((void FAR_MTK * target, size_t bytestozero));

EXTERN_MTK(void)
jset_input_stream_position_MTK JPP_MTK((j_decompress_ptr_MTK cinfo, int offset));
EXTERN_MTK(void)
jset_input_stream_position_bit_MTK JPP_MTK((j_decompress_ptr_MTK cinfo,
				    int byte_offset, int bit_left, INT32_MTK buf));

EXTERN_MTK(int)
jget_input_stream_position_MTK JPP_MTK((j_decompress_ptr_MTK cinfo));
/* Constant tables in jutils.c */
#if 0				/* This table is not actually needed in v6a */
extern const int jpeg_zigzag_order_MTK[];	/* natural coef order to zigzag order */
#endif
extern const int jpeg_natural_order_MTK[];	/* zigzag coef order to natural order */

/* Suppress undefined-structure complaints if necessary. */

#ifdef INCOMPLETE_TYPES_BROKEN_MTK
#ifndef AM_MEMORY_MANAGER_MTK	/* only jmemmgr.c defines these */
struct jvirt_sarray_control_MTK {
	long dummy;
};
struct jvirt_barray_control_MTK {
	long dummy;
};
#endif
#endif				/* INCOMPLETE_TYPES_BROKEN */
