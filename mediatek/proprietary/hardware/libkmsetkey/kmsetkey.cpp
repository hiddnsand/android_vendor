#define LOG_TAG "KMSetkey"

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include <log/log.h>
#include <cutils/log.h>
#include <kmsetkey.h>

#include <vendor/mediatek/hardware/keymaster_attestation/1.0/IKeymasterDevice.h>
using ::vendor::mediatek::hardware::keymaster_attestation::V1_0::IKeymasterDevice;
using ::vendor::mediatek::hardware::keymaster_attestation::V1_0::StatusCode;
using android::hardware::Return;

static uint32_t kb_offset = 0, kb_len = 0;
static uint8_t *kb_buf = NULL;

void clean(void)
{
	free(kb_buf);
	kb_len = 0;
	kb_offset = 0;
	kb_buf = NULL;
}

int32_t ree_import_attest_keybox(const uint8_t *peakb, const uint32_t peakb_len, const uint32_t finish)
{
	uint32_t payload_offset, payload_len;

	if ((peakb == NULL || peakb_len == 0) && finish == 0) {
		ALOGE("attest keybox is NULL\n");
		return -1;
	}

	payload_offset = kb_offset + peakb_len;
	if (payload_offset > kb_len) {
		payload_len = (finish == 1 ? payload_offset : 2 * payload_offset);
		kb_buf = (uint8_t *)realloc(kb_buf, payload_len);
		kb_len = payload_len;
	}
	memcpy(kb_buf + kb_offset, peakb, peakb_len);
	kb_offset += peakb_len;

	if (finish == 0)
		return 0;

	ALOGI("%s: get HIDL service!\n", __func__);
	android::sp<IKeymasterDevice> mDevice = IKeymasterDevice::getService();
	android::hardware::hidl_vec<uint8_t> _hidl_data;
	_hidl_data.setToExternal(const_cast<unsigned char*>(kb_buf), kb_offset);

	if (mDevice == nullptr) {
		ALOGE("%s: unable to getService.\n", __func__);
		clean();
		return -2;
	}

	ALOGI("%s: start to enter attestKeyInstall!\n", __func__);
	Return<StatusCode> mDevrep = mDevice->attestKeyInstall(_hidl_data);
	ALOGI("%s: exit attestKeyInstall!\n", __func__);

	if (mDevrep == StatusCode::UNIMPLEMENTED) {
		ALOGE("%s: attest key install failed, HAL not exists!\n", __func__);
		clean();
		return -1;
	}
	else if (mDevrep == StatusCode::KEYINSTALLFAILED) {
		ALOGE("%s: attest key install failed!\n", __func__);
		clean();
		return -2;
	}
	else if (mDevrep == StatusCode::OK) {
		ALOGI("%s: attest key install success!\n", __func__);
		clean();
		return 0;
	}

	ALOGE("%s: attest key install failed, unknown error!\n", __func__);
	clean();
	return -1000;
}

