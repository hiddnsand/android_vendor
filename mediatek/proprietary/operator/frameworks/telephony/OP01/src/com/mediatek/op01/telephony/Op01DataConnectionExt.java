/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op01.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.net.NetworkRequest.Builder;
import android.net.wifi.IWifiManager;
import android.os.IBinder;
import android.os.INetworkManagementService;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telephony.Rlog;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.PhoneConstants;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.dataconnection.DataConnectionExt;


/**
 * Implement methods to support China Mobile requirements.
 *
 * @return
 */
public class Op01DataConnectionExt extends DataConnectionExt {
    static final String TAG = "Op01DataConnectionExt";
    private Context mContext;
    private ConnectivityManager mConnectivityManager;
    private boolean mIsRebooting = false;
    private static final String ACTION_SHUTDOWN_IPO = "mediatek.intent.action.ACTION_SHUTDOWN_IPO";
    //private static final String ACTION_PREBOOT_IPO = "android.intent.action.ACTION_PREBOOT_IPO";
    private static final String ACTION_WIFI_FAILOVER_GPRS_DIALOG =
        "com.mediatek.intent.action_WIFI_FAILOVER_GPRS_DIALOG";
    private static final int WIFI_CONNECT_REMINDER_ALWAYS = 0;
    //Settings.System.WIFI_CONNECT_REMINDER
    private static final String WIFI_CONNECT_REMINDER = "wifi_connect_reminder";
    // Settings.System.LAST_SIMID_BEFORE_WIFI_DISCONNECTED,
    private static final String LAST_SIMID_BEFORE_WIFI_DISCONNECTED =
        "last_simid_before_wifi_disconnected";
    private NetworkCallback mLostListener;
    private BroadcastReceiver mReceiver;

    public Op01DataConnectionExt(Context context) {
        super(context);

        // For Data Dialog begin
        boolean bspPackage = SystemProperties.getBoolean("ro.mtk_bsp_package", false);
        if (!bspPackage) {
            mContext = context;
            mConnectivityManager = (ConnectivityManager)
                mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            mLostListener = new NetworkLostCallback();
            Builder builder = new NetworkRequest.Builder();
            builder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI);
            NetworkRequest nwRequest = builder.build();
            mConnectivityManager.registerNetworkCallback(nwRequest, mLostListener);

            IntentFilter filter = new IntentFilter();
            filter.addAction(ACTION_SHUTDOWN_IPO);
            //filter.addAction(ACTION_PREBOOT_IPO);
            mReceiver = new ConnectivityServiceReceiver();
            mContext.registerReceiver(mReceiver, filter);
            // For Data Dialog end
        }
    }

    @Override
    public boolean isDataAllowedAsOff(String apnType) {
        if (TextUtils.equals(apnType, PhoneConstants.APN_TYPE_MMS)
                || TextUtils.equals(apnType, PhoneConstants.APN_TYPE_SUPL)
                || TextUtils.equals(apnType, PhoneConstants.APN_TYPE_IMS)
                || TextUtils.equals(apnType, PhoneConstants.APN_TYPE_EMERGENCY)
                || TextUtils.equals(apnType, MtkPhoneConstants.APN_TYPE_XCAP)
                || TextUtils.equals(apnType, MtkPhoneConstants.APN_TYPE_RCS)) {
            return true;
        }

        return false;
    }

    /**
     * For OP01 IOT test, when data connection activated, it will enable
     * firewall for non-ims pdn, which will drop unexpected dns package.
     *
     * @param apnTypes The data connection apnTypes.
     * @param ifc The interface name.
     */
    @Override
    public void onDcActivated(String[] apnTypes, String ifc) {
        if (apnTypes == null || ifc == null) {
            return;
        }

        log("onDcActivated. ifc: " + ifc);
        if (ifc.length() == 0) {
            return;
        }

        if (!hasImsApnType(apnTypes)) {
            return;
        }

        enableVolteIotFirewall(true, ifc);
    }

    /**
     * For OP01 IOT test, when data connection deactivated, it will
     * disable firewall for non-ims pdn.
     *
     * @param apnTypes The data connection apnTypes.
     * @param ifc The interface name.
     */
    @Override
    public void onDcDeactivated(String[] apnTypes, String ifc) {
        if (apnTypes == null || ifc == null) {
            return;
        }

        log("onDcDeactivated. ifc: " + ifc);
        if (ifc.length() == 0) {
            return;
        }

        if (!hasImsApnType(apnTypes)) {
            return;
        }

        enableVolteIotFirewall(false, ifc);
    }


    /**
     * To check metered apn type is decided by load type or not.
     *
     * @return true if metered apn type is decided by load type.
     */
    @Override
    public boolean isMeteredApnTypeByLoad() {
        return true;
    }


    /**
     * To check apn type is metered or not.
     *
     * @param type APN type.
     * @param isRoaming true if network in roaming state.
     * @return true if this APN type is metered.
     */
    @Override
    public boolean isMeteredApnType(String type, boolean isRoaming) {
        log("apnType=" + type + ",isRoaming=" + isRoaming);
        // Default metered apn type: [default, supl, dun, mms]
        if (TextUtils.equals(type, PhoneConstants.APN_TYPE_DEFAULT)
            || TextUtils.equals(type, PhoneConstants.APN_TYPE_DUN)
            || (isRoaming && TextUtils.equals(type, PhoneConstants.APN_TYPE_MMS))) {
            return true;
        }

        return false;
    }

    /**
     *Print radio log.
     *@param text The context needs to be printed.
     */
    @Override
    public void log(String text) {
        Rlog.d(TAG, text);
    }

    private boolean hasImsApnType(String[] apnTypes) {
        boolean ret = false;
        if (apnTypes != null) {
            for (String apnType : apnTypes) {
                if (TextUtils.equals(apnType, PhoneConstants.APN_TYPE_IMS)) {
                    log("apnType = " + apnType);
                    ret = true;
                    break;
                }
            }
        }

        return ret;
    }

    private void enableVolteIotFirewall(boolean enable, String ifc) {
        log("enableVolteIotFirewall, ifc: "
            + (ifc == null ? "null" : ifc) + ", " + enable);

        String simType = SystemProperties.get("gsm.sim.ril.testsim");
        if (simType == null || !simType.equals("1")) {
            simType = SystemProperties.get("gsm.sim.ril.testsim.2");
            if (simType == null || !simType.equals("1")) {
                log("enableVolteIotFirewall, not TEST SIM");
                return;
            }
        }

        ///  Bypass USB check.
        //   UsbManager usbMgr = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
        //   int usbState = usbMgr.getCurrentState();
        //   if (usbState != 1  && enable) {
        //   log("enableVolteIotFirewall, USB not connected");
        //   return;
        //   }
        //   if (!usbMgr.isFunctionEnabled("acm") && enable) {
        //   log("enableVolteIotFirewall, not contains acm");
        //   return;
        //   }

        INetworkManagementService netd = INetworkManagementService.Stub.asInterface(
            ServiceManager.getService(Context.NETWORKMANAGEMENT_SERVICE));
        if (netd == null) {
            log("enableVolteIotFirewall, netd == null");
            return;
        }

        try {
            if (enable) {
                netd.setVolteIotFirewall(ifc);
                log("enableVolteIotFirewall, setVolteIotFirewall()");
            } else {
                netd.clearVolteIotFirewall(ifc);
                log("enableVolteIotFirewall, clearVolteIotFirewall()");
            }
        } catch (RemoteException e) {
            log("enableVolteIotFirewall, exception: " + e);
        }
    }

    // For Data Dialog
    private class NetworkLostCallback extends NetworkCallback {
        public NetworkLostCallback() {
        }

        @Override
        public void onLost(Network network) {
            NetworkInfo netInfo = mConnectivityManager.getNetworkInfo(network);
            log("onLost: networInfo:::" + netInfo);
            if ((netInfo != null) && (netInfo.getType() == ConnectivityManager.TYPE_WIFI)) {
                Thread thread = new Thread() {
                        @Override
                        public void run() {
                            log("receive WIFI onlost, userPrompt");
                            userPrompt();
                        }
                    };
                thread.start();
            }
        }
    }

    public void userPrompt() {
        boolean skipDataDialog = SystemProperties.get("ro.op01_compatible").equals("1");
        if (skipDataDialog) {
            log("skip DataDialog, no datadialog");
            return;
        }

        int isAsking = Settings.System.getInt(mContext.getContentResolver(),
                                              WIFI_CONNECT_REMINDER,
                                              WIFI_CONNECT_REMINDER_ALWAYS);
        if (isAsking != WIFI_CONNECT_REMINDER_ALWAYS) {
            // not asking mode
            log("Not ask mode");
            return;
        }

        if (mIsRebooting) {
            log("IPO rebooting, skip datadialog");
            return;
        }

        boolean dataAvailable = isPsDataAvailable();
        log("dataAvailable:" + dataAvailable);
        if (!dataAvailable) {
            return;
        }

        // Close data connection switch here.
        turnOffDataConnection();

        IBinder binder = ServiceManager.getService(Context.WIFI_SERVICE);
        final IWifiManager wifiService = IWifiManager.Stub.asInterface(binder);
        boolean hasConnectableAP = false;
        // TODO:waiting for WIFI ready
        // try {
        //     if (wifiService != null) {
        //         hasConnectableAP = wifiService.hasConnectableAp();
        //     }
        // } catch (RemoteException e) {
        //     log("hasConnectableAp failed!");
        // }
        log("hasConnectableAP: " + hasConnectableAP);

        if (!hasConnectableAP) {
            // Show the Data Dialog here
            Intent i = new Intent(ACTION_WIFI_FAILOVER_GPRS_DIALOG);
            //i.putExtra("simId", 1);
            i.setClassName("com.mediatek.op.wifi", "com.mediatek.op.wifi.DataConnectionReceiver");
            mContext.sendBroadcast(i);
            log("Send ACTION_WIFI_FAILOVER_GPRS_DIALOG intent");
        }

        // WIFI module will setup DIALOG later
    }

    private class ConnectivityServiceReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                return;
            }

            String action = intent.getAction();
            log("received intent ==> " + action);

            if (ACTION_SHUTDOWN_IPO.equals(action)) {
                mIsRebooting = true;
            }
            //  else if (ACTION_PREBOOT_IPO.equals(action)) {
            //     mIsRebooting = false;
            // }

        }
    }

    private boolean isPsDataAvailable() {
        // Check SIM ready
        TelephonyManager telMgr =
            (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        if (telMgr == null) {
            log("TelephonyManager is null");
            return false;
        }

        boolean isSIMReady = false;
        int i = 0;
        int n = telMgr.getSimCount();
        for (i = 0; i < n; i++) {
            if (telMgr.getSimState(i) == TelephonyManager.SIM_STATE_READY) {
                isSIMReady = true;
                break;
            }
        }

        log("isSIMReady: " + isSIMReady);
        if (!isSIMReady) {
            return false;
        }

        // check radio on
        ITelephony iTel =
            ITelephony.Stub.asInterface(ServiceManager.getService(Context.TELEPHONY_SERVICE));
        if (iTel == null) {
            log("ITelephony is null");
            return false;
        }

        SubscriptionManager subMgr = SubscriptionManager.from(mContext);
        if (subMgr == null) {
            log("SubscriptionManager is null");
            return false;
        }

        int[] subIdList = subMgr.getActiveSubscriptionIdList();
        n = 0;
        if (subIdList != null) {
            n = subIdList.length;
        }

        boolean isRadioOn = false;
        for (i = 0; i < n; i++) {
            try {
                isRadioOn = iTel.isRadioOnForSubscriber(subIdList[i], mContext.getPackageName());
                if (isRadioOn) {
                    break;
                }
            } catch (RemoteException e) {
                log("isRadioOnForSubscriber RemoteException");
                isRadioOn = false;
            }
        }
        if (!isRadioOn) {
            log("All sub Radio OFF");
            return false;
        }

        // Check flight mode
        int airplanMode = Settings.System.getInt(mContext.getContentResolver(),
                                                 Settings.System.AIRPLANE_MODE_ON,
                                                 0);
        log("airplanMode:" + airplanMode);
        if (airplanMode == 1) {
            return false;
        }

        return true;
    }


    private void turnOffDataConnection() {
        TelephonyManager telMgr =
            (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        if (telMgr == null) {
            return;
        }

        // Remember last status on(1) or off(-1)
        Settings.System.putLong(mContext.getContentResolver(),
                                LAST_SIMID_BEFORE_WIFI_DISCONNECTED,
                                telMgr.getDataEnabled() ? 1 : -1);

        telMgr.setDataEnabled(false);
    }
}
