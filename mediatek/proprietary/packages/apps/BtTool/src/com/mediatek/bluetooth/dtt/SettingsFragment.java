package com.mediatek.bluetooth.dtt;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.*;
import android.util.Log;

public class SettingsFragment extends PreferenceFragment {

    // preference
    protected ListPreference mHostPrefTestSituation;
    protected Preference mHostPrefBtScCustPath;
    protected ListPreference mFwPrefTestSituation;
    protected Preference mPrefSysToolOpenMtkLogger;
    protected Preference mPrefSysToolOpenEngMode;

    // parent activity
    private MainActivity mParentActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // config parent activity
        this.mParentActivity = (MainActivity)this.getActivity();

        // 使用自動的 shared preference name
        PreferenceManager pm = this.getPreferenceManager();
        pm.setSharedPreferencesName(Const.PREF_NAME);
        pm.setSharedPreferencesMode(Context.MODE_PRIVATE);
        // Load the preferences from an XML resource
        this.addPreferencesFromResource(R.xml.preferences);

        // 設定 ui component
        this.mHostPrefTestSituation = (ListPreference)this.findPreference(DataModel.F_HOST_TEST_SITUATION);
        this.mHostPrefBtScCustPath = this.findPreference(DataModel.F_HOST_BTSC_CUST_PATH);
        this.mFwPrefTestSituation = (ListPreference)this.findPreference(DataModel.F_FW_TEST_SITUATION);
        this.mPrefSysToolOpenMtkLogger = this.findPreference(DataModel.F_SYS_TOOL_OPEN_MTKLOGGER);
        this.mPrefSysToolOpenEngMode = this.findPreference(DataModel.F_SYS_TOOL_OPEN_ENGMODE);
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener){
        this.getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(listener);
    }

    /**
     * called by parent activity (MainActivity)
     * @param dataModel
     */
    protected void updateUI(DataModel dataModel){

        Log.i(Const.TAG, "SettingsFragment.updateUI()[+]");

        if (this.getActivity() == null || !this.isAdded()){
            Log.w(Const.TAG, "SettingsFragment.updateUI(): wrong state, isAdded[" + this.isAdded() + "], activity:" + this.getActivity());
            return;
        }

        // update preference summary
        String host_labels[] = this.getResources().getStringArray(R.array.act_set_host_pref_test_situation_entries_labels);
        String[] host_summaries = this.getResources().getStringArray(R.array.act_set_host_pref_test_situation_entries_summaries);
        int index=this.mHostPrefTestSituation.findIndexOfValue(dataModel.mBtHostTestSituation);
        this.mHostPrefTestSituation.setTitle(this.getString(R.string.act_set_pref_test_situation_title, host_labels[index]));
        this.mHostPrefTestSituation.setSummary(host_summaries[index]);
        if(!dataModel.mBtHostTestSituation.equals(this.mHostPrefTestSituation.getValue()))
            this.mHostPrefTestSituation.setValue(dataModel.mBtHostTestSituation);

        String fw_labels[] = this.getResources().getStringArray(R.array.act_set_fw_pref_test_situation_entries_labels);
        String[] fw_summaries = this.getResources().getStringArray(R.array.act_set_fw_pref_test_situation_entries_summaries);
        index=this.mFwPrefTestSituation.findIndexOfValue(dataModel.mBtFwTestSituation);
        this.mFwPrefTestSituation.setTitle(this.getString(R.string.act_set_pref_test_situation_title, fw_labels[index]));
        this.mFwPrefTestSituation.setSummary(fw_summaries[index]);
        if(!dataModel.mBtFwTestSituation.equals(this.mFwPrefTestSituation.getValue()))
            this.mFwPrefTestSituation.setValue(dataModel.mBtFwTestSituation);

        // update cust config path
        boolean isCust = dataModel.mBtHostTestSituation.equals(Const.TS_CUST);

        String HostScCustPath = this.getString(R.string.act_set_host_pref_btsc_cust_path_summary, dataModel.mBtHostScCustPath);

        this.mHostPrefBtScCustPath.setEnabled(isCust);
        this.mHostPrefBtScCustPath.setSummary(HostScCustPath);
    }

    /**
     * 1. 處理 Click 事件 ( 在 onSharedPreferenceChanged 之後執行 ) <br>
     * 2. 針對 persistent=false 者, 只能在此處理
     */
    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference)
    {
        Log.i(Const.TAG, "onPreferenceTreeClick: " + preference.getKey());

        String key = preference.getKey();
        if (DataModel.F_HOST_BTSC_CUST_PATH.equals(key))
        {
            this.mParentActivity.onSelectBtScCustPath();
            return true;
        }
        if (DataModel.F_SYS_TOOL_OPEN_MTKLOGGER.equals(key)){
            Util.openMtkLogger(this.getActivity());
            return true;
        }
        if (DataModel.F_SYS_TOOL_OPEN_ENGMODE.equals(key)){
            Util.openEngineerMode(this.getActivity());
            return true;
        }
        return false;
    }
}