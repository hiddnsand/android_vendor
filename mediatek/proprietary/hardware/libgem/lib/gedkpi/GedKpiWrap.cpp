#include <ged/ged_kpi.h>
#include <cutils/log.h>
#include <gedkpi/GedKpiWrap.h>

GED_KPI_HANDLE ged_kpi_create_wrap(uint64_t BBQ_ID)
{
	return ged_kpi_create(BBQ_ID);
}

void ged_kpi_destroy_wrap(GED_KPI_HANDLE hKPI)
{
	ged_kpi_destroy(hKPI);
}

GED_ERROR ged_kpi_dequeue_buffer_tag_wrap(GED_KPI_HANDLE hKPI, int32_t BBQ_api_type, int32_t fence, int32_t pid, intptr_t buffer_addr)
{
	return ged_kpi_dequeue_buffer_tag(hKPI, BBQ_api_type, fence, pid, buffer_addr);
}

GED_ERROR ged_kpi_queue_buffer_tag_wrap(GED_KPI_HANDLE hKPI, int32_t BBQ_api_type, int32_t fence, int32_t pid, int32_t QedBuffer_length, intptr_t buffer_addr)
{
	return ged_kpi_queue_buffer_tag(hKPI, BBQ_api_type, fence, pid, QedBuffer_length, buffer_addr);
}

GED_ERROR ged_kpi_acquire_buffer_tag_wrap(GED_KPI_HANDLE hKPI, int pid, intptr_t buffer_addr)
{
	return ged_kpi_acquire_buffer_tag(hKPI, pid, buffer_addr);
}
