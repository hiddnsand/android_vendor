package com.mediatek.op09.telecom;

import android.os.SystemProperties;

import android.util.Log;

import com.mediatek.server.telecom.ext.DefaultCallMgrExt;


public class Op09CallMgrExt extends DefaultCallMgrExt {
    private static final String LOG_TAG = "Op09CallMgrExt";

    /**
     * should resume hold call.
     *
     * @return whether resume hold call or not.
     *
     */
    @Override
    public boolean shouldResumeHoldCall() {
        //always resume hold call in denali svlte project
        return isSvlte();
    }

    private boolean isSvlte() {
        boolean isSvlte = false;
        isSvlte = (SystemProperties.get("ro.boot.opt_c2k_lte_mode").equals("1"));
        Log.d(LOG_TAG,"isSvlte=" + isSvlte);
        return isSvlte;
    }

    /**
     * Should disconnect call calls when dial an ECC out.
     * Only for CMCC Volte PCT test
     *
     * @return whether to disconnect or not
     *
     */
    @Override
    public boolean shouldDisconnectCallsWhenEcc() {
        return !isTestSim();
    }

    private boolean isTestSim() {
        boolean isTestSim = false;
        isTestSim = SystemProperties.get("gsm.sim.ril.testsim").equals("1") ||
                   SystemProperties.get("gsm.sim.ril.testsim.2").equals("1") ||
                   SystemProperties.get("gsm.sim.ril.testsim.3").equals("1") ||
                   SystemProperties.get("gsm.sim.ril.testsim.4").equals("1");
        Log.d(LOG_TAG,"isTestSim " + isTestSim);
        return isTestSim;
    }
}
