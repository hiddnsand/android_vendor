/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.wfo.impl;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.IBinder;
import android.os.INetworkManagementService;
import android.os.Message;
import android.os.ServiceManager;
import android.os.SystemProperties;

import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.telephony.Rlog;
import android.text.TextUtils;

import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.MtkTelephonyIntents;
import com.mediatek.telephony.MtkTelephonyManagerEx;
import com.mediatek.wfo.util.RssiMonitoringProcessor;

/**
 * Top-level Application class for the WifiOffload app.
 */
public class WifiPdnHandler {
    private static final String TAG = "WifiPdnHandler";
    private static final String WIFI_IF_NAME = "wlan0";

    private Context mContext;
    private int mSimCount;
    private boolean mHasWiFiDisabledPending;

    private WifiManager mWifiManager;
    private int mRatType[];
    private boolean mIsWifiEnabled;
    private boolean[] mWifiPdnExisted;
    private WifiManager.WifiLock mWifiLock;

    private ConnectivityManager mConnectivityManager;
    private INetworkManagementService mNetworkManager;
    private TelephonyManager mTelephonyManager;
    private RssiMonitoringProcessor mRssiMonitoringProcessor;

    // wifi state
    private boolean mIsWifiConnected = false;
    private String mWifiApMac = "";
    private String mWifiIpv4Address = "";
    private String mWifiIpv6Address = "";
    private String mIfName = "";
    private String mSsid = "";
    private int mLastRssi;

    private static final String PROPERTY_WFC_ENABLE = "persist.mtk.wfc.enable";
    private static final String PROPERTY_MIMS_SUPPORT = "persist.mtk_mims_support";

    // for Wos -> Wi-Fi fwk to notify defer Wi-Fi disabled action for WFC de-initialization.
    private static final String WFC_STATUS_CHANGED =
            "com.mediatek.intent.action.WFC_STATUS_CHANGED";
    private static final String EXTRA_WFC_STATUS_KEY = "wfc_status";

    public static final int SNR_UNKNOWN = 60;

    // Message codes. See mHandler below.
    private static final int BASE = 1000;
    private static final int EVENT_WIFI_NETWORK_STATE_CHANGE            = BASE + 0;
    private static final int EVENT_SET_WIFI_SIGNAL_STRENGTH             = BASE + 1;
    private static final int EVENT_SET_WIFI_ENABLED                     = BASE + 2;
    private static final int EVENT_SET_WIFI_ASSOC                       = BASE + 3;
    private static final int EVENT_SET_WIFI_IP_ADDR                     = BASE + 4;
    private static final int EVENT_CONNECTED_TO_RIL                     = BASE + 5;
    private static final int EVENT_WIFI_SCAN                            = BASE + 6;

    public static final int EVENT_ON_WIFI_MONITORING_THRESHOLD_CHANGED  = BASE + 101;
    public static final int EVENT_ON_WIFI_PDN_ACTIVATE                  = BASE + 102;

    private static final int RESPONSE_SET_WIFI_ENABLED                  = BASE + 200;
    private static final int RESPONSE_SET_WIFI_SIGNAL_LEVEL             = BASE + 201;
    private static final int RESPONSE_SET_WIFI_ASSOC                    = BASE + 202;
    private static final int RESPONSE_SET_WIFI_IP_ADDR                  = BASE + 203;

    private static final int RETRY_TIMEOUT = 3000;
    private static final int WIFI_SCAN_DELAY = 3000; // 3s

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Rlog.d(TAG, "handleMessage: " + messageToString(msg));
            AsyncResult ar;
            switch (msg.what) {
                /* handle internal event start */
                case EVENT_WIFI_NETWORK_STATE_CHANGE:
                    updateWifiConnectedInfo(msg.arg1);
                    break;
                case EVENT_SET_WIFI_SIGNAL_STRENGTH:
                    setWifiSignalLevel();
                    break;
                case EVENT_SET_WIFI_ENABLED:
                    setWifiEnabled();
                    break;
                case EVENT_SET_WIFI_ASSOC:
                    setWifiAssoc();
                    break;
                case EVENT_SET_WIFI_IP_ADDR:
                    setWifiIpAddress();
                    break;
                case EVENT_CONNECTED_TO_RIL:
                    // TRM happened, sync information to RIL
                    setWifiSignalLevel();
                    setWifiEnabled();
                    setWifiAssoc();
                    setWifiIpAddress();
                    break;
                case EVENT_WIFI_SCAN:
                    // Check if start wifi scan to improve MOBIKE
                    checkIfstartWifiScan(true);
                    break;
                /* handle internal event end */
                /* handle UNSOL start */
               case EVENT_ON_WIFI_MONITORING_THRESHOLD_CHANGED:
                    onWifiMonitoringThreshouldChanged(msg);
                    break;
                case EVENT_ON_WIFI_PDN_ACTIVATE:
                    onWifiPdnActivate(msg);
                    break;
                /* handle UNSOL end */
                /* handle request response start */
                case RESPONSE_SET_WIFI_ENABLED:
                    ar = (AsyncResult) msg.obj;
                    handleRetry(EVENT_SET_WIFI_ENABLED, ar);
                    break;
                case RESPONSE_SET_WIFI_SIGNAL_LEVEL:
                    ar = (AsyncResult) msg.obj;
                    handleRetry(EVENT_SET_WIFI_SIGNAL_STRENGTH, ar);
                    break;
                case RESPONSE_SET_WIFI_ASSOC:
                    ar = (AsyncResult) msg.obj;
                    handleRetry(EVENT_SET_WIFI_ASSOC, ar);
                    break;
                case RESPONSE_SET_WIFI_IP_ADDR:
                    ar = (AsyncResult) msg.obj;
                    handleRetry(EVENT_SET_WIFI_IP_ADDR, ar);
                    break;
                /* handle request response end */
                default:
                    break;
            }
        }

        private void handleRetry(int msgId, AsyncResult result) {
            if (mHandler.hasMessages(msgId)) {
                return;
            }

            //if (result != null && result.exception != null) {
                sendEmptyMessageDelayed(msgId, RETRY_TIMEOUT);
            //}
        }

        private String messageToString(Message msg) {
            switch (msg.what) {

                case EVENT_WIFI_NETWORK_STATE_CHANGE:
                    return "EVENT_WIFI_NETWORK_STATE_CHANGE";
                case EVENT_SET_WIFI_SIGNAL_STRENGTH:
                    return "EVENT_SET_WIFI_SIGNAL_STRENGTH";
                case EVENT_SET_WIFI_ENABLED:
                    return "EVENT_SET_WIFI_ENABLED";
                case EVENT_SET_WIFI_ASSOC:
                    return "EVENT_SET_WIFI_ASSOC";
                case EVENT_SET_WIFI_IP_ADDR:
                    return "EVENT_SET_WIFI_IP_ADDR";
                case EVENT_CONNECTED_TO_RIL:
                    return "EVENT_CONNECTED_TO_RIL";
                case EVENT_ON_WIFI_MONITORING_THRESHOLD_CHANGED:
                    return "EVENT_ON_WIFI_MONITORING_THRESHOLD_CHANGED";
                case EVENT_ON_WIFI_PDN_ACTIVATE:
                    return "EVENT_ON_WIFI_PDN_ACTIVATE";
                case EVENT_WIFI_SCAN:
                    return "EVENT_WIFI_SCAN";
                case RESPONSE_SET_WIFI_ENABLED:
                    return "RESPONSE_SET_WIFI_ENABLED";
                case RESPONSE_SET_WIFI_SIGNAL_LEVEL:
                    return "RESPONSE_SET_WIFI_SIGNAL_LEVEL";
                case RESPONSE_SET_WIFI_ASSOC:
                    return "RESPONSE_SET_WIFI_ASSOC";
                case RESPONSE_SET_WIFI_IP_ADDR:
                    return "RESPONSE_SET_WIFI_IP_ADDR";

                default:
                    return "UNKNOWN";
            }
        }
    };

    public Handler getHandler() {
        return mHandler;
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || intent.getAction() == null) {
                return;
            }
            Rlog.d(TAG, "onReceive action:" + intent.getAction());
            if (intent.getAction().equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                if (mWifiManager == null) {
                    Rlog.d(TAG, "Unexpected error, mWifiManager is null!");
                    return;
                }

                int wifiState = intent.getIntExtra(
                        WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);
                if (wifiState == WifiManager.WIFI_STATE_DISABLING) {
                    mIsWifiEnabled = false;
                    if (!isWifiPdnExisted()) {
                        Rlog.d(TAG, "no WiFi pdn, turn off WiFi directly...");
                        Intent sendIntent = new Intent(WFC_STATUS_CHANGED);
                        sendIntent.putExtra(EXTRA_WFC_STATUS_KEY, 2);
                        mContext.sendBroadcast(sendIntent);
                    } else {
                        // wait for RDS's onRequestSetWifiDisabled callback.
                        mHasWiFiDisabledPending = true;
                    }
                    setWifiEnabled();
                } else {
                    boolean isWifiEnabled = mWifiManager.isWifiEnabled();
                    if (isWifiEnabled != mIsWifiEnabled) {
                        mIsWifiEnabled = isWifiEnabled;
                        setWifiEnabled();
                    }
                }
            }
        }
    };

    public WifiPdnHandler(Context context, int simCount) {
        mContext = context;
        mSimCount = simCount;

        mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        mConnectivityManager = ConnectivityManager.from(mContext);
        IBinder b = ServiceManager.getService(Context.NETWORKMANAGEMENT_SERVICE);
        mNetworkManager = INetworkManagementService.Stub.asInterface(b);
        mRssiMonitoringProcessor = new RssiMonitoringProcessor(mConnectivityManager);

        mIsWifiEnabled = mWifiManager.isWifiEnabled();
        mWifiLock = mWifiManager.createWifiLock("WifiOffloadService-Wifi Lock");
        if (mWifiLock != null) {
            mWifiLock.setReferenceCounted(false);
        }

        mWifiPdnExisted = new boolean[mSimCount];
        mRatType = new int[mSimCount];

        mRssiMonitoringProcessor.initialize(mSimCount);

        registerForBroadcast();

        setupCallbacksForWifiStatus();

        // sync status at the first time
        setWifiEnabled();
    }

    public boolean isWifiConnected() {
        return mIsWifiConnected;
    }

    private void registerForBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        mContext.registerReceiver(mReceiver, filter);
    }

    private int[] getIntArrayFromMsg(Message msg) {
        AsyncResult ar = (AsyncResult) msg.obj;
        Intent intent = (Intent) ar.result;
        return getIntArrayFromIntent(intent);
    }

    private int[] getIntArrayFromIntent(Intent intent) {
        String extra = null;
        switch(intent.getAction()) {
            case MtkTelephonyIntents.ACTION_RSSI_THRESHOLD_CHANGED:
                extra = MtkTelephonyIntents.EXTRA_RSSI_THRESHOLDS;
                break;
            case MtkTelephonyIntents.ACTION_WIFI_PDN_ACTIVATED:
                extra = MtkTelephonyIntents.EXTRA_WIFI_PDN_ACTIVATED;
                break;
            default:
                Rlog.d(TAG, intent.getAction() + " no implements");
        }
        if (null == extra) {
            return null;
        }
        return intent.getExtras().getIntArray(extra);
    }

    private void onWifiMonitoringThreshouldChanged(Message msg) {
        int[] result = getIntArrayFromMsg(msg);
        if (null == result) {
            Rlog.e(TAG, "onWifiMonitoringThreshouldChanged(): result is null");
            return;
        }

        boolean enable = (result[0] == 1);
        int length = result.length;
        int simIdx = result[length - 1]; // last params

        if (!enable) {
            Rlog.d(TAG, "Turn off RSSI monitoring");
            mRssiMonitoringProcessor.unregisterAllRssiMonitoring(simIdx);
            return;
        }
        int count = result[1];
        if ((2 + count + 1) < length) { // <enable>,<count>,<rssi_1>,<rssi_2>,...,<simIdx>
            Rlog.e(TAG, "onWifiMonitoringThreshouldChanged(): Bad params");
            return;
        }
        if (checkInvalidSimIdx(simIdx, "onWifiMonitoringThreshouldChanged: invalid SIM id")) {
            return;
        }

        int[] rssi = new int[count];
        int offset = 2;
        for (int i = 0; i < count; ++i) {
            rssi[i] = result[i + offset];
            Rlog.d(TAG, "onWifiMonitoringThreshouldChanged(): rssi = " + rssi[i]);
        }

        onRssiMonitorRequest(simIdx, count, rssi);
    }

    private void onWifiPdnActivate(Message msg) {
        int[] result = getIntArrayFromMsg(msg);
        if (null == result) {
            Rlog.e(TAG, "onWifiPdnActivate(): result is null");
            return;
        }
        if (result.length < 2) {
            Rlog.e(TAG, "onWifiPdnActivate(): Bad params");
            return;
        }
        int pdnCount = result[0];
        int simIdx = result[1];

        if (checkInvalidSimIdx(simIdx, "onWifiPdnActivate(): invalid SIM id")) {
            return;
        }

        if (SystemProperties.getInt(PROPERTY_MIMS_SUPPORT, 0) < 2) {
            Rlog.d(TAG, "MIMS does not support, sync up pdn status to all slots.");
            for (int i = 0; i < mSimCount; i++) {
                mWifiPdnExisted[i] = (pdnCount > 0);
            }
        } else {
            Rlog.d(TAG, "MIMS supported, update pdn status to specific slot[" + simIdx + "].");
            mWifiPdnExisted[simIdx] = (pdnCount > 0);
        }

        if (mWifiPdnExisted[simIdx]) {
            Rlog.d(TAG, "Notify Wi-Fi fwk to enable defer disable Wi-Fi process");
            mWifiLock.acquire();
            Intent intent = new Intent(WFC_STATUS_CHANGED);
            intent.putExtra(EXTRA_WFC_STATUS_KEY, 1);
            mContext.sendBroadcast(intent);
        }

        // Check if start wifi scan to improve MOBIKE
        checkIfstartWifiScan(false);

        Rlog.d(TAG, "isWifiPdnExist: " + isWifiPdnExisted() +
            " hasPendingWifiDisabled: " + mHasWiFiDisabledPending);
        if (!isWifiPdnExisted() && mHasWiFiDisabledPending) {
            mHasWiFiDisabledPending = false;
            Intent intent = new Intent(WFC_STATUS_CHANGED);
            intent.putExtra(EXTRA_WFC_STATUS_KEY, 2);
            mContext.sendBroadcast(intent);
        }

        if (!mWifiPdnExisted[simIdx]) {
            Rlog.d(TAG, "Notify Wi-Fi fwk to disable defer disable Wi-Fi process");
            Intent intent = new Intent(WFC_STATUS_CHANGED);
            intent.putExtra(EXTRA_WFC_STATUS_KEY, 0);
            mContext.sendBroadcast(intent);
            mWifiLock.release();
        }
    }

    private void updateWifiConnectedInfo(int isConnected) {
        boolean changed = false;
        boolean ipAddrChanged = false;

        if (isConnected == 0) {
            if (mIsWifiConnected) {
                mIsWifiConnected = false;
                mWifiApMac = "";
                mWifiIpv4Address = "";
                mWifiIpv6Address = "";
                mIfName = "";
                mSsid = "";
                changed = true;
                ipAddrChanged = true;
            }
        } else {
            String wifiApMac = "", ipv4Address = "", ipv6Address = "", ifName = "", ssid = "";
            mIsWifiConnected = true;

            // get MAC address of the current access point
            WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
            if (wifiInfo != null) {
                wifiApMac = wifiInfo.getBSSID();
                ssid = updateSsidToHexString(wifiInfo.getSSID());
                if (!mWifiApMac.equals(wifiApMac)) {
                    mWifiApMac = (wifiApMac == null ? "" : wifiApMac);
                    changed = true;
                }
            }

            // get ip
            for (Network nw : mConnectivityManager.getAllNetworks()) {
                LinkProperties prop = mConnectivityManager.getLinkProperties(nw);
                // MAL only care about wlan
                if (prop == null || prop.getInterfaceName() == null
                        || !prop.getInterfaceName().startsWith("wlan")) {
                    continue;
                }
                for (InetAddress address : prop.getAddresses()) {
                    if (address instanceof Inet4Address && !address.isLoopbackAddress()) {
                        ipv4Address = address.getHostAddress();
                    } else if (address instanceof Inet6Address && !address.isLinkLocalAddress()
                        && !address.isLoopbackAddress()) {
                        // Filters out link-local address. If cannot find non-link-local address,
                        // pass empty string to MAL.
                        ipv6Address = address.getHostAddress();
                    } else {
                        Rlog.d(TAG, "ignore address= " + address +
                            " isLoopbackAddr: " + address.isLinkLocalAddress());
                    }
                }
                // get interface name
                ifName = prop.getInterfaceName();
            }
            if (!mWifiIpv4Address.equals(ipv4Address)) {
                mWifiIpv4Address = (TextUtils.isEmpty(ipv4Address)) ? "" : ipv4Address;
                ipAddrChanged = true;
            }
            if (!mWifiIpv6Address.equals(ipv6Address)) {
                mWifiIpv6Address = (TextUtils.isEmpty(ipv6Address)) ? "" : ipv6Address;
                ipAddrChanged = true;
            }
            if (!mIfName.equals(ifName)) {
                mIfName = (ifName == null ? "" : ifName);
                changed = true;
            }
            if (!mSsid.equals(ssid)) {
                mSsid = (ssid == null ? "" : ssid);
                changed = true;
            }
        }

        // Check if start wifi scan to improve MOBIKE
        checkIfstartWifiScan(false);

        if (changed) {
            setWifiAssoc();
        }

        if (ipAddrChanged) {
            setWifiIpAddress();
        }
    }

    private void setWifiEnabled() {
        Message result = mHandler.obtainMessage(RESPONSE_SET_WIFI_ENABLED);
        Rlog.d(TAG, "setWifiEnabled(): " + mIsWifiEnabled);
        boolean ret = MtkTelephonyManagerEx.getDefault().setWifiEnabled(
                getMainCapabilityPhoneId(), WIFI_IF_NAME, (mIsWifiEnabled) ? 1 : 0);
        if (!ret) {
            result.sendToTarget();
        }
    }

    private void setWifiSignalLevel() {
        Message result = mHandler.obtainMessage(RESPONSE_SET_WIFI_SIGNAL_LEVEL);
        Rlog.d(TAG, "setWifiSignalLevel(): " + mLastRssi);
        boolean ret = MtkTelephonyManagerEx.getDefault().setWifiSignalLevel(
                getMainCapabilityPhoneId(), mLastRssi, SNR_UNKNOWN);
        if (!ret) {
            result.sendToTarget();
        }
    }

    private void setWifiAssoc() {
        Message result = mHandler.obtainMessage(RESPONSE_SET_WIFI_ASSOC);
        Rlog.d(TAG, "setWifiAssoc() ifName: " + mIfName + " associated: " + mIsWifiConnected +
            " ssid: " + mSsid + " apMac: " + mWifiApMac);
        boolean ret = MtkTelephonyManagerEx.getDefault().setWifiAssociated(getMainCapabilityPhoneId(),
                WIFI_IF_NAME, mIsWifiConnected, mSsid, mWifiApMac);
        if (!ret) {
            result.sendToTarget();
        }
    }

    private void setWifiIpAddress() {
        Message result = mHandler.obtainMessage(RESPONSE_SET_WIFI_ASSOC);
        Rlog.d(TAG, "setWifiIpAddr() ifName: " + mIfName + " ipv4Addr: " + mWifiIpv4Address +
            " ipv6Addr: " + mWifiIpv6Address);
        boolean ret = MtkTelephonyManagerEx.getDefault().setWifiIpAddress(getMainCapabilityPhoneId(),
                WIFI_IF_NAME, mWifiIpv4Address, mWifiIpv6Address);
        if (!ret) {
            result.sendToTarget();
        }
    }

    private void checkIfstartWifiScan(boolean scanImmediately) {

        boolean wifiPdnExisted = isWifiPdnExisted();

        if (!mIsWifiConnected && wifiPdnExisted) {

            if (scanImmediately) {
                Rlog.d(TAG, "call WifiManager.startScan()");
                mWifiManager.startScan();
            }

            if (!mHandler.hasMessages(EVENT_WIFI_SCAN)) {
                Rlog.d(TAG, "start 3s delay to trigger wifi scan");
                mHandler.sendMessageDelayed(
                    mHandler.obtainMessage(EVENT_WIFI_SCAN), WIFI_SCAN_DELAY);
            }
        } else {
            mHandler.removeMessages(EVENT_WIFI_SCAN);
        }
    }

    /**
     * callback from MD to configure RSSI monitor thresholds.
     */
    private void onRssiMonitorRequest(int simId, int size, int[] rssiThresholds) {
        mRssiMonitoringProcessor.registerRssiMonitoring(simId, size, rssiThresholds);
    }

    /**
     * setup callbacks from ConnectivityService when WiFi is changed.
     */
    private void setupCallbacksForWifiStatus() {
        if (mConnectivityManager == null) {
            Rlog.d(TAG, "Unexpected error, mConnectivityManager = null");
            return;
        }

        if (mNetworkManager == null) {
            Rlog.d(TAG, "Unexpected error, mNetworkManager = null");
            return;
        }

        NetworkRequest request = new NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .addCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)
                .build();
        mConnectivityManager.registerNetworkCallback(request,
            new ConnectivityManager.NetworkCallback() {
            /**
             * @param network
             */
            @Override
            public void onAvailable(Network network) {
                Message msg = mHandler.obtainMessage(
                        EVENT_WIFI_NETWORK_STATE_CHANGE,
                        1,  // isConnected
                        0, null);
                mHandler.sendMessage(msg);
            }

            /**
             * @param network
             */
            @Override
            public void onLost(Network network) {
                Message msg = mHandler.obtainMessage(
                        EVENT_WIFI_NETWORK_STATE_CHANGE,
                        0,  // isConnected
                        0, null);
                mHandler.sendMessage(msg);
            }

            @Override
            public void onCapabilitiesChanged(final Network network,
                        final NetworkCapabilities networkCapabilities) {
                if (networkCapabilities == null) {
                    Rlog.d(TAG, "NetworkCallback.onCapabilitiesChanged, Capabilities=null");
                    return;
                }

                int rssi = networkCapabilities.getSignalStrength();

                if (mLastRssi == rssi) {
                    return;
                }
                mLastRssi = rssi;
                mHandler.sendEmptyMessage(EVENT_SET_WIFI_SIGNAL_STRENGTH);
            }

            /**
             * @param network
             */
            @Override
            public void onLinkPropertiesChanged(Network network,
                LinkProperties linkProperties) {
                if (mIsWifiConnected) {
                    // At this timing, goes to check if IP address is updated.
                    Message msg = mHandler.obtainMessage(
                            EVENT_WIFI_NETWORK_STATE_CHANGE,
                            1,  // isConnected
                            0, null);
                    mHandler.sendMessage(msg);
                }
            }
        });
    }

    private boolean checkInvalidSimIdx(int simIdx, String dbgMsg) {
        if (simIdx < 0 || simIdx >= mSimCount) {
            Rlog.d(TAG, dbgMsg);
            return true;
        }
        return false;
    }

    private boolean isWifiPdnExisted() {
        for (int i = 0; i < mSimCount; i++) {
            if (mWifiPdnExisted[i]) {
                Rlog.d(TAG, "isWifiPdnExisted: found WiFi PDN on SIM: " + i);
                return true;
            }
        }
        return false;
    }

    private void updateLastRssi() {
        WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
        if (wifiInfo != null) {
            mLastRssi = wifiInfo.getRssi();
        }
    }

    private String updateSsidToHexString(String ssid) {
        if (TextUtils.isEmpty(ssid)) {
            return "";
        }
        // take away the double quote added by android fwk
        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
            ssid = ssid.substring(1, ssid.length() - 1);
        }

        byte[] bytes = ssid.getBytes();
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < bytes.length; ++i) {
            str.append(String.format("%02x", bytes[i]));
        }

        Rlog.d(TAG, "updateSsidToHexString orig: " + ssid + " after: " + str.toString());
        return str.toString();
    }

    public int getLastRssi() {
        return mLastRssi;
    }

    /**
     * to get main capability phone id.
     *
     * @return The phone id with highest capability.
     */
    private int getMainCapabilityPhoneId() {
       int phoneId = SystemProperties.getInt(MtkPhoneConstants.PROPERTY_CAPABILITY_SWITCH, 1) - 1;
       if (phoneId < 0 || phoneId >= TelephonyManager.getDefault().getPhoneCount()) {
           phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
       }
       Rlog.d(TAG, "getMainCapabilityPhoneId = " + phoneId);
       return phoneId;
    }
}
