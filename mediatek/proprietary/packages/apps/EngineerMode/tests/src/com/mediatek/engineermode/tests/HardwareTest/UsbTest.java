/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.ContentResolver;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.usb.UsbList;
import com.mediatek.engineermode.usb.UsbPhyTuning;
import com.mediatek.engineermode.usb.UsbOtgSwitch;

public class UsbTest extends ActivityInstrumentationTestCase2<UsbList> {

    private static final String TAG = "USBList";
    private static final String EM_BAKC_FAIL_MSG = "Back to USBList fail";
    private Solo mSolo = null;
    private Activity mActivity = null;
    private Context mContext = null;
    private Instrumentation mInst = null;
    private ContentResolver mCr = null;

    private boolean textFound = false;

    public UsbTest() {
        super(UsbList.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        mInst = getInstrumentation();
        mContext = mInst.getTargetContext();
        mActivity = getActivity();
        mSolo = new Solo(mInst, mActivity);

    }

    @BasicFuncAnnotation
    public void test01_Precondition() {
        assertNotNull(mInst);
        assertNotNull(mContext);
        assertNotNull(mActivity);
        assertNotNull(mSolo);
    }

    public void test02_ItemUsbPhyTuning() {

        if (!EmOperate.enterItem(mSolo, "USB PHY Tuning", "UsbPhyTuning"))
            return;

        mSolo.clickOnText("Submit", 1);
        textFound = mSolo.waitForText("USB_DRIVING_CAPABILITY execute success");
        assertTrue("USB_DRIVING_CAPABILITY execute failed", textFound);

        mSolo.clickOnText("Submit", 2);
        textFound = mSolo.waitForText("RG_USB20_HSTX_SRCTRL execute success");
        assertTrue("RG_USB20_HSTX_SRCTRL execute failed", textFound);

        mSolo.clickOnText("Submit", 3);
        textFound = mSolo.waitForText("RG_USB20_INTR_EN execute success");
        assertTrue("RG_USB20_INTR_EN execute failed", textFound);

        mSolo.goBack();
    }

    public static boolean isUsbOtgSupported() {
        String[] supportedChips = { "mt6757", "mt6755" };
        String chipName = EmOperate.getChipName();
        Log.d("@M_" + TAG, "chipName:" + chipName);
        for (int i = 0; i < supportedChips.length; i++) {
            if (supportedChips[i].equals(chipName)) {
                return true;
            }
        }
        return false;
    }

    public void test03_ItemUsbOTGSwitch() {

        if (!EmOperate.enterItem(mSolo, "OTG Switch", "UsbOtgSwitch"))
            return;

        if (!isUsbOtgSupported())
            return;

        mSolo.clickOnText("Disabled");
        textFound = mSolo.waitForText("Enabled");
        assertTrue("Enabled OTG Switch failed", textFound);

        mSolo.sleep(EmOperate.TIME_MID);

        mSolo.clickOnText("Enabled");
        textFound = mSolo.waitForText("Disabled");
        assertTrue("Disabled OTG Switch failed", textFound);

        mSolo.sleep(EmOperate.TIME_MID);

        mSolo.clickOnText("Disabled");
        textFound = mSolo.waitForText("Enabled");
        assertTrue("Enabled OTG Switch failed", textFound);

        mSolo.goBack();
        mSolo.clickOnText("OTG Switch");
        mSolo.sleep(EmOperate.TIME_MID);

        textFound = mSolo.waitForText("Enabled");
        assertTrue("Enabled OTG Switch failed", textFound);

        mSolo.sleep(EmOperate.TIME_MID);

        mSolo.clickOnText("Enabled");
        textFound = mSolo.waitForText("Disabled");
        assertTrue("Disabled OTG Switch failed", textFound);
        mSolo.goBack();
    }

}
