/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
package com.mediatek.emcamera;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.hardware.Camera.Parameters;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.mediatek.emcamera.config.AssemblyVerifyConfig;
import com.mediatek.emcamera.R;

public class AssemblyVerify {
    private static final String TAG = AssemblyVerify.class.getSimpleName();

    private static final int STATE_NEAR_MAIN1 = 0;
    private static final int STATE_NEAR_MAIN2 = 1;
    private static final int STATE_FAR_MAIN1 = 2;
    private static final int STATE_FAR_MAIN2 = 3;

    public static final int TYPE_NEAR = 0; //Near
    public static final int TYPE_FAR = 1; //Far
    public static final int TYPE_NEAR_AND_FAR = 2; //Far

    private static final String PATTERN = "[A-Za-z0-9]+__([0-9]+)x([0-9]+)_[A-Za-z0-9]+";

    private String mKey;
    private Activity mActivity;
    private boolean mVerifyOn;
    private Listener mListener;
    private int mLocationNum;
    private int mLocationType;
    private int mState;
    // private String mCameraImagePath;
    private String[] mRawFileNames;
    private String mResultFileName;
    private int[] mSizes;
    private boolean mIsPaused;

    public interface Listener {
        void onReloadAll(String preferenceKey);

        void onSwitchCamera();

        void onAutoCapture();
    }

    AssemblyVerify(Activity activity) {
        mActivity = activity;
        mKey = AssemblyVerifyConfig.PREFERENCE_KEY[0];

        final SharedPreferences preferences = mActivity.getSharedPreferences(mKey,
                android.content.Context.MODE_PRIVATE);
        mVerifyOn = preferences.getInt(mActivity.getString(R.string.auto_clibr_assembly_verify),
                AssemblyVerifyConfig.VERIFY_OFF_INDEX) == AssemblyVerifyConfig.VERIFY_ON_INDEX;
        if (mVerifyOn) {
            mLocationNum = preferences.getInt(
                    mActivity.getString(R.string.auto_clibr_location_num), 2);
            mLocationType = preferences.getInt(
                    mActivity.getString(R.string.auto_clibr_location_type),
                    TYPE_NEAR_AND_FAR);
            resetState();
        }
    }

    public void registerListener(Listener listener) {
        mListener = listener;
    }

    private void resetState() {
        if(mLocationType == TYPE_FAR) {
            mState = STATE_FAR_MAIN1;
        } else {
            mState = STATE_NEAR_MAIN1;
        }
    }

    private boolean needMoveToNextLocation() {
        return 2 == mLocationNum && STATE_NEAR_MAIN2 == mState;
    }

    public void setFileName(String rawFileName) {
        Elog.v(TAG, "onCaptureCompleted mVerifyOn = " + mVerifyOn + ",mState = " + mState
                + ",rawFileName = " + rawFileName);
        if (mVerifyOn) {
            if(mRawFileNames == null) {
                mRawFileNames = new String[4];
                mSizes = new int[4];
            }
            mRawFileNames[mState] = rawFileName;
        }
    }

    public void onCaptureCompleted() {
        if (mVerifyOn) {
            if (STATE_NEAR_MAIN1 == mState || STATE_FAR_MAIN1 == mState) {
                ++mState;
                mListener.onSwitchCamera();
                mListener.onAutoCapture();
            } else if (needMoveToNextLocation()) {
                Util.showConfirmDialog(mActivity, R.string.auto_clibr_assembly_verify,
                        R.string.auto_clibr_switch_location, mClickListener);
            } else {
                resetState();
                mListener.onReloadAll(AssemblyVerifyConfig
                        .PREFERENCE_KEY[AssemblyVerifyConfig.LOCATION_NEAR]);
                Util.toastOnUiThread(mActivity, R.string.auto_clibr_assembly_verify_start);
                assemblyVerify();
            }
        }
    }

    private DialogInterface.OnClickListener mClickListener = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface arg0, int arg1) {
            Elog.v(TAG, "DialogInterface.OnClickListener mState = " + mState);

            if (DialogInterface.BUTTON_POSITIVE == arg1) {
                if (needMoveToNextLocation()) {
                    ++mState;
                    mListener.onReloadAll(AssemblyVerifyConfig
                            .PREFERENCE_KEY[AssemblyVerifyConfig.LOCATION_FAR]);
                    mListener.onAutoCapture();
                }
            } else if (DialogInterface.BUTTON_NEGATIVE == arg1) {
                --mState;
                mListener.onSwitchCamera();
            }
        }
    };

    public void assemblyVerify() {
        (new AsyncTask<Void, Void, Void>() {
            private AssemblyVerifyResult mResult = null;
            @Override
            protected Void doInBackground(Void ... arg) {
                Elog.i(TAG, "==== assemblyVerify ====");
                if(mRawFileNames == null) {
                    Elog.e(TAG, "get file name is wrong!!!");
                } else {
                    // need reset state first
                    mResultFileName = mRawFileNames[mState];
                    if(updateRawName() == false) {
                        return null;
                    }
                    Elog.v(TAG, "mRawFileNames[0] = " + mRawFileNames[0]
                            + "\nmRawFileNames[1] = " + mRawFileNames[1]
                            + "\nmRawFileNames[2] = " + mRawFileNames[2]
                            + "\nmRawFileNames[3] = " + mRawFileNames[3]);
                    Elog.v(TAG, "mSizes[0] = " + mSizes[0] + ", mSizes[1] = " + mSizes[1]
                            + ", mSizes[2] = " + mSizes[2] + ", mSizes[3] = " + mSizes[3]);
                    mResult = CameraJni.verify(mRawFileNames[0], mRawFileNames[1], mRawFileNames[2]
                            , mRawFileNames[3], mResultFileName, mSizes, mLocationType);
                    mSizes = null;
                    mRawFileNames = null;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void arg) {
                Elog.v(TAG, "==== Show W+T Verify Result onPostExecute====");
                if(mIsPaused == false) {
                    String title = mActivity.getString(R.string.auto_clibr_assembly_verify);
                    boolean result = false;
                    if(mResult != null &&  mResult.status == 0) {
                        result = true;
                    }
                    showDrawableDialog(title, mResult == null ? "" : mResult.toString(), result);
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void showDrawableDialog(String title, String msg, boolean result) {
        View drawableView = mActivity.getLayoutInflater().inflate(R.layout.em_img_dlg_layout, null);

        final AlertDialog dialog = new AlertDialog.Builder(mActivity).setCancelable(true)
                .setTitle(title).setView(drawableView).create();

        ImageView imgView = (ImageView) drawableView.findViewById(R.id.image_dlg_result_img);
        imgView.setImageResource(result ? R.drawable.pass : R.drawable.fail);
        TextView msgText = (TextView) drawableView.findViewById(R.id.image_dlg_msg_text);
        msgText.setText(msg);

        Button okBtn = (Button) drawableView.findViewById(R.id.image_dlg_ok_btn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private boolean updateRawName() {
        if(mRawFileNames[mState] == null) {
            Elog.w(TAG, "get jpegfile name is null!!");
            return false;
        }
        File jpegfile = new File(mRawFileNames[mState]+".jpeg");
        if(jpegfile == null) {
            Elog.w(TAG, "jpegfile: " + jpegfile + "is null!!");
            return false;
        }

        File parentDir = jpegfile.getParentFile();
        if(parentDir == null) {
            Elog.w(TAG, "parentDir: " + parentDir + "is null!!");
            return false;
        }

        File[] files = parentDir.listFiles();
        int updatedNum = 0;
        for(File file:files) {
            if(updatedNum == mLocationNum*2) {
                break;
            }
            Pattern pattern=Pattern.compile(PATTERN);
            if(!file.isDirectory()) {
                String fileName = file.getAbsolutePath();
                if(fileName.endsWith(".raw")) {
                    for (int i = 0; i < 4; ++ i) {
                        if(mRawFileNames[i] != null && fileName.contains(mRawFileNames[i])) {
                            mRawFileNames[i] = fileName;
                            Matcher matcher = pattern.matcher(fileName); 
                            while (matcher.find()) {
                                try {
                                    mSizes[(i%2)*2] = Integer.parseInt(matcher.group(1));
                                    mSizes[(i%2)*2+1] = Integer.parseInt(matcher.group(2));
                                } catch( NumberFormatException e) {
                                    Elog.w(TAG, "getSizes Error!");
                                    return false;
                                }
                            }
                            ++updatedNum;
                            break;
                        }
                    }
                }
            }
        }

        if(updatedNum !=  mLocationNum*2) {
            Elog.w(TAG, "raw files number is not enough!! updatedNum = " + updatedNum);
            return false;
        }

        return true;
    }

    public void onResume() {
        mIsPaused = false;
    }

    public void onPause() {
        mIsPaused = true;
    }

}
