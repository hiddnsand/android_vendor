package com.mediatek.op09.telecom;

import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.util.Log;

import com.mediatek.server.telecom.ext.DefaultPhoneAccountExt;

import java.util.List;
import java.util.Objects;

/**
 * PhoneAccount extension plugin for op09.
*/
public class Op09PhoneAccountExt extends DefaultPhoneAccountExt {

    private static final String TAG = "Op09PhoneAccountExt";

    /**
     * should remove the default MO phone account.
     *
     * @param accountHandleList
     *            capable account list
     * @return true if need to remove.
     */
    @Override
    public boolean shouldRemoveDefaultPhoneAccount(List<PhoneAccountHandle> accountHandleList) {
        if (null != accountHandleList && accountHandleList.size() == 2) {
            return false;
        }
        return true;
    }

    /**
     * simple log info.
     *
     * @param msg need print out string.
     * @return void.
     */
    private static void log(String msg) {
        Log.d(TAG, msg);
    }
}

