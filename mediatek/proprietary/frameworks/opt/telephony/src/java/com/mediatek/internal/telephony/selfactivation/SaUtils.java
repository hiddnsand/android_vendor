package com.mediatek.internal.telephony.selfactivation;

import android.os.SystemProperties;

public class SaUtils {
    // Public APIs
    public static final int ACTION_ADD_DATA_SERVICE = 0;
    public static final int ACTION_MO_CALL = 1;
    // Extra key for call type
    public static final String EXTRA_KEY_MO_CALL_TYPE = "key_mo_call_type";
    public static final int CALL_TYPE_NORMAL = 0;
    public static final int CALL_TYPE_EMERGENCY = 1;
    // Extra key for add data operation
    public static final String EXTRA_KEY_ADD_DATA_OP = "key_add_data_operation";
    public static final int ADD_DATA_DECLINE = 0;
    public static final int ADD_DATA_AGREE = 1;

    public static final int PCO_VALUE_DEFAULT = 0;
    public static final int PCO_VALUE_SELF_ACT = 5;
    public static final int PCO_VALUE_INVALID = -1;

    // PCO FF00H 0
    public static final int STATE_NONE = 0;
    // PCO FF00H 0?
    public static final int STATE_ACTIVATED = 1;
    // PCO FF00H 5
    public static final int STATE_NOT_ACTIVATED = 2;
    public static final int STATE_UNKNOWN = -1;

    // Project configuration constants
    private static final String PROPERTY_OP_OPTR = "persist.operator.optr";
    private static final String PROPERTY_OP_SEGMENT = "persist.operator.seg";
    private static final String PROPERTY_OP12_DEVICE_MODEL = "persist.op12.model";
    private static final String OP12 = "OP12";
    private static final String OP12_TYPE3_DEVICE = "SEGTYPE3";
    private static final String OP12_BRANDED_DEVICE = "0";

    // Internal debug usage
    public static final String ACTION_DEBUG_SELFACTIVATION =
            "mediatek.intent.action.DEBUG_SELFACTIVATION";
    public static final String KEY_DBG_ACTION = "dbg_action";
    public static final int DEBUG_ACTION_RESET = 1;
    public static final int DEBUG_ACTION_DUMP = 2;
    public static final int DEBUG_ACTION_NONE = 0;

    public static boolean isOp12Project() {
        return (OP12.equalsIgnoreCase(
                SystemProperties.get(PROPERTY_OP_OPTR))) ? true : false;
    }

    public static boolean isOp12BrandedDevice() {
        return (OP12_BRANDED_DEVICE.equalsIgnoreCase(
                SystemProperties.get(PROPERTY_OP12_DEVICE_MODEL))) ? true : false;
    }

    public static boolean isOp12Type3Device() {
         return (OP12_TYPE3_DEVICE.equalsIgnoreCase(
                SystemProperties.get(PROPERTY_OP_SEGMENT))) ? true : false;
    }
}
