/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.opcommon.telephony;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.HwBinder;
import android.os.Message;
import android.os.Parcel;
import android.os.Registrant;
import android.os.RegistrantList;
import android.content.Context;
import android.content.res.Resources;
import android.os.SystemProperties;
import android.os.WorkSource;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

import com.mediatek.internal.telephony.IMtkRilOp;
import android.telephony.Rlog;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import android.os.RemoteException;
import com.android.internal.telephony.CommandException;
import static com.android.internal.telephony.RILConstants.*;
import static com.mediatek.internal.telephony.MtkRILConstants.*;

import vendor.mediatek.hardware.radio_op.V1_1.IRadioOp;
import vendor.mediatek.hardware.radio_op.V1_1.IRadioResponseOp;
import vendor.mediatek.hardware.radio_op.V1_1.IRadioIndicationOp;

import com.android.internal.telephony.RIL;
import com.android.internal.telephony.RILConstants;
import com.android.internal.telephony.RILRequest;
import com.mediatek.internal.telephony.MtkRILConstants;

import android.hardware.radio.V1_0.IRadio;

/**
 * Implement methods to support operator feature
 *
 * @return
 */
public class MtkRilOp extends RIL implements IMtkRilOp {
    static final String TAG = "MtkRilOp";

    //***** Constants
    volatile IRadioOp mRadioProxyOp = null;
    IRadioResponseOp mRadioResponseOp = null;
    IRadioIndicationOp mRadioIndicationOp = null;

    protected final AtomicLong mRadioProxyCookie = new AtomicLong(0);
    protected final RadioOpProxyDeathRecipient mRadioOpProxyDeathRecipient;
    protected final RilHandlerOp mRilHandlerOp;

    protected class RilHandlerOp extends Handler {
        //***** Handler implementation
        @Override public void
        handleMessage(Message msg) {
            switch (msg.what) {
                case EVENT_RADIO_PROXY_DEAD:
                    log("handleMessage: EVENT_RADIO_PROXY_DEAD cookie = " + msg.obj +
                            " mRadioProxyCookie = " + mRadioProxyCookie.get());
                    if ((long) msg.obj == mRadioProxyCookie.get()) {
                        resetProxyAndRequestList();

                        // todo: rild should be back up since message was sent with a delay. this is
                        // a hack.
                        getRadioOpProxy(null);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    final class RadioOpProxyDeathRecipient implements HwBinder.DeathRecipient {
        @Override
        public void serviceDied(long cookie) {
            // Deal with service going away
            log("serviceDied");
            // todo: temp hack to send delayed message so that rild is back up by then
            //mRilHandlerOp.sendMessage(mRilHandlerOp.obtainMessage(EVENT_RADIO_PROXY_DEAD, cookie));
            mRilHandlerOp.sendMessageDelayed(
                    mRilHandlerOp.obtainMessage(EVENT_RADIO_PROXY_DEAD, cookie),
                    IRADIO_GET_SERVICE_DELAY_MILLIS);
        }
    }

    public MtkRilOp(Context context, int preferredNetworkType,
            int cdmaSubscription, Integer instanceId) {
        super(context, preferredNetworkType, cdmaSubscription, instanceId);
        log("MtkRilOp constructor ");
        mRadioResponseOp = new MtkRadioResponseOp(this);
        mRadioIndicationOp = new MtkRadioIndicationOp(this);
        mRilHandlerOp = new RilHandlerOp();
        mRadioOpProxyDeathRecipient = new RadioOpProxyDeathRecipient();
        getRadioOpProxy(null);
    }

    protected IRadio getRadioProxy(Message result) {
        log("MtkRilOp getRadioProxy");
        return null;
    }

    protected IRadioOp getRadioOpProxy(Message result) {
        if (mRadioProxyOp != null) {
            return mRadioProxyOp;
        }

        try {
            mRadioProxyOp = IRadioOp.getService(HIDL_SERVICE_NAME[mPhoneId == null ? 0 : mPhoneId]);
            if (mRadioProxyOp != null) {
                mRadioProxyOp.linkToDeath(mRadioOpProxyDeathRecipient,
                        mRadioProxyCookie.incrementAndGet());
                mRadioProxyOp.setResponseFunctions(mRadioResponseOp, mRadioIndicationOp);
            } else {
                log("getRadioOpProxy: mRadioProxy == null");
            }
        } catch (RemoteException | RuntimeException e) {
            mRadioProxyOp = null;
            log("RadioProxy getService/setResponseFunctions: " + e);
        }

        if (mRadioProxyOp == null) {
            if (result != null) {
                AsyncResult.forMessage(result, null,
                        CommandException.fromRilErrno(RADIO_NOT_AVAILABLE));
                result.sendToTarget();
            }

            // if service is not up, treat it like death notification to try to get service again
            mRilHandlerOp.sendMessageDelayed(
                    mRilHandlerOp.obtainMessage(EVENT_RADIO_PROXY_DEAD, mRadioProxyCookie.get()),
                    IRADIO_GET_SERVICE_DELAY_MILLIS);
        }

        return mRadioProxyOp;
    }

    protected void handleRadioOpProxyExceptionForRR(RILRequest rr, String caller, Exception e) {
        log(caller + ": " + e);
        resetProxyAndRequestList();

        // service most likely died, handle exception like death notification to try to get service
        // again
        mRilHandlerOp.sendMessageDelayed(
                mRilHandlerOp.obtainMessage(EVENT_RADIO_PROXY_DEAD,
                        mRadioProxyCookie.incrementAndGet()),
                IRADIO_GET_SERVICE_DELAY_MILLIS);
    }

    protected void resetProxyAndRequestList() {
        super.resetProxyAndRequestList();
        mRadioProxyOp = null;
    }

    public void log(String text) {
        Rlog.d(TAG, text);
    }
}

