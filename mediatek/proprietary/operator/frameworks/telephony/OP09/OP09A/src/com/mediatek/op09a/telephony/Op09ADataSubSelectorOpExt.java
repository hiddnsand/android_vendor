package com.mediatek.op09A.telephony;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

import android.os.Build;
import android.os.IBinder;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.RemoteException;

import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;

import android.telephony.Rlog;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.SubscriptionController;

import com.mediatek.internal.telephony.datasub.IDataSubSelectorOPExt;
import com.mediatek.internal.telephony.datasub.DataSubConstants;
import com.mediatek.internal.telephony.datasub.DataSubSelector;
import com.mediatek.internal.telephony.datasub.DataSubSelectorUtil;
import com.mediatek.internal.telephony.datasub.ISimSwitchForDSSExt;
import com.mediatek.internal.telephony.datasub.CapabilitySwitch;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.internal.telephony.ppl.IPplAgent;

public class Op09ADataSubSelectorOpExt implements IDataSubSelectorOPExt {
    private static final boolean USER_BUILD = TextUtils.equals(Build.TYPE, "user");
    private static boolean DBG = true;

    private static String LOG_TAG = "Op09ADSSExt";

    private static DataSubSelector mDataSubSelector = null;

    private static Context mContext = null;

    private static ISimSwitchForDSSExt mSimSwitchForDSS = null;

    private static CapabilitySwitch mCapabilitySwitch = null;

    private static final String PREF_CATEGORY_RADIO_STATUS = "RADIO_STATUS";

    private Intent mIntent = null;

    protected SharedPreferences mSimSwitchPreference;

    private PreferenceListener mPrefListener;

    private class PreferenceListener implements OnSharedPreferenceChangeListener {
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            log("onSharedPreferenceChanged, key = " + key);

            // When radio on sim, reselect default data sim.
            if (sharedPreferences.contains(key) == false) {
                subSelector(mIntent);
            }
        }
    }

    public Op09ADataSubSelectorOpExt(Context context) {
        mContext = context;

    }

    public void init(DataSubSelector dataSubSelector, ISimSwitchForDSSExt simSwitchForDSS) {
        mDataSubSelector = dataSubSelector;
        mCapabilitySwitch = CapabilitySwitch.getInstance(mContext, dataSubSelector);
        mSimSwitchForDSS = simSwitchForDSS;

        // Register radio on/off event,
        // when user enable/disable radio status in settings, reselect default data.
        mSimSwitchPreference = mContext.getSharedPreferences(PREF_CATEGORY_RADIO_STATUS, 0);
        mPrefListener = new PreferenceListener();
        mSimSwitchPreference.registerOnSharedPreferenceChangeListener(mPrefListener);
    }

    @Override
    public void handleSimStateChanged(Intent intent) {
        String simStatus = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
        int slotId = intent.getIntExtra(PhoneConstants.SLOT_KEY, PhoneConstants.SIM_ID_1);
        if (simStatus.equals(IccCardConstants.INTENT_VALUE_ICC_IMSI)) {
            mCapabilitySwitch.handleSimImsiStatus(intent);

            //handleNeedWaitImsi and handleNeedWaitUnlock
        } else if (simStatus.equals(IccCardConstants.INTENT_VALUE_ICC_ABSENT)) {
            mCapabilitySwitch.handleSimImsiStatus(intent);
        } else if (simStatus.equals(IccCardConstants.INTENT_VALUE_ICC_NOT_READY)) {
            mCapabilitySwitch.handleSimImsiStatus(intent);
        }
    }

    @Override
    public void handleSubinfoRecordUpdated(Intent intent) {
        int detectedType = intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                    MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE);
        if (detectedType != MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE) {
            subSelector(intent);
        }
    }

    protected void handleNeedWaitUnlock(Intent intent) {
        if (CapabilitySwitch.isNeedWaitUnlock()) {
            CapabilitySwitch.setNeedWaitUnlock("false");
            subSelector(intent);
        }
        if (CapabilitySwitch.isNeedWaitUnlockRoaming()) {
            CapabilitySwitch.setNeedWaitUnlockRoaming("false");
        }
    }

    protected void handleNeedWaitImsi(Intent intent) {
        if (CapabilitySwitch.isNeedWaitImsi()) {
            CapabilitySwitch.setNeedWaitImsi(Boolean.toString(false));
        }
        if (CapabilitySwitch.isNeedWaitImsiRoaming() == true) {
            CapabilitySwitch.setNeedWaitImsiRoaming(Boolean.toString(false));
        }
    }

    @Override
    public void subSelector(Intent intent) {
        if (intent == null) {
            log("OP09: intent is null, ignore!");
            return;
        }

        int detectedType = intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS, -1);
        int detectedCount = intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_SIM_COUNT, -1);
        SubscriptionController subController = SubscriptionController.getInstance();
        int phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        int[] subList = subController.getActiveSubIdList();
        int defaultSub = subController.getDefaultDataSubId();
        int subCount = subList.length;

        log("OP09: Sub count: " + subCount + " Intent count: " + detectedCount +
                " detectedType = " + detectedType + " defaultSub = " + defaultSub);

        if (detectedCount > -1 && detectedCount != subCount) {
            log("OP09: Intent count and latest sub count not match, ignore and wait next.");
            return;
        }

        int insertedSimCount = 0;
        for (int i = 0; i < subCount; i++) {
            int subId = subList[i];
            int tempPhoneId = subController.getPhoneId(subId);
            if (isSimRadioOn(tempPhoneId)) {
                // The phoneId is only used when insertedSimCount == 1.
                phoneId = tempPhoneId;
                insertedSimCount++;
            }
        }

        mIntent = intent;
        log("OP09: Inserted and radio on sim count: " + insertedSimCount + " phoneId = " + phoneId);

        if (insertedSimCount == 0) {
            log("OP09 C0: No SIM inserted, do nothing.");
        } else if (insertedSimCount == 1) {
            switch (detectedType) {
            case MtkSubscriptionManager.EXTRA_VALUE_NEW_SIM:
                // only one new sim card is detected, make it as default and data on.
                log("OP09 C1: a new sim detected, Set Default slot: " + phoneId);
                mDataSubSelector.setDefaultData(phoneId);
                setDataStatus(phoneId, true);
                break;
            case MtkSubscriptionManager.EXTRA_VALUE_REMOVE_SIM:
                // remove one sim and left one.
                if (subList[0] != defaultSub) {
                    // the left sim is not default, make it as default
                    log("OP09 C2.1: left a sim not default, Set Default: " + phoneId);
                    // Before change default data, get the switch status and set to new default.
                    setDataStatus(phoneId, getDataEnabledFromSetting(defaultSub));
                    mDataSubSelector.setDefaultData(phoneId);
                    mDataSubSelector.syncDefaultDataToMd(phoneId);
                } else {
                    log("OP09 C2.2: a sim left and it's default sub, do nothing.");
                }
                break;
            case MtkSubscriptionManager.EXTRA_VALUE_REPOSITION_SIM:
                if (subList[0] != defaultSub) {
                    log("OP09 C3.1: a sim left and is not default data sim,"
                            + " set it as default data sim.");
                    setDataStatus(phoneId, getDataEnabledFromSetting(defaultSub));
                    mDataSubSelector.setDefaultData(phoneId);
                    mDataSubSelector.syncDefaultDataToMd(phoneId);
                } else {
                    log("OP09 C3.2: a sim left with default data on it, do nothing.");
                }
                // TODO: sim switch need do something. But we plan seperate it from data logic.
                break;
            case MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE:
                // boot up and detect an old sim, do nothing.
                log("OP09 C4: a sim exist and is old, do nothing.");
                break;
            default:
                log("OP09 C5: ignore unknown detectedType: " + detectedType);
                break;
            }
        } else if (insertedSimCount == 2) {
            switch (detectedType) {
            case MtkSubscriptionManager.EXTRA_VALUE_NEW_SIM:
                int newSimStatus =
                        intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_NEW_SIM_SLOT, 0);
                log("OP09 C6.0: newSimStatus = " + newSimStatus + " subList[0] = " +
                        subList[0] + " subList[1] = " + subList[1]);
                // 1: slot 0 insert a new sim, 2: slot 1 insert a new sim.
                // NOTE: Sub module report new sim cards after hot plug.
                if (defaultSub == subList[0]) {
                    // default sub still on sim1, keep default data.
                    log("OP09 C6.1: data on old sim1, turn off SIM2, set capability to SIM1.");
                    if (newSimStatus == 3) {
                        setDataStatus(PhoneConstants.SIM_ID_1, true);
                    }
                    mDataSubSelector.setDataEnabled(PhoneConstants.SIM_ID_2, false);
                } else if (defaultSub == subList[1]) {
                    // default sub still on sim2, keep default data.
                    log("OP09 C6.2: data on old sim2, turn off SIM1, set capability to SIM2.");
                    if (newSimStatus == 3) {
                        setDataStatus(PhoneConstants.SIM_ID_2, true);
                    }
                    mDataSubSelector.setDataEnabled(PhoneConstants.SIM_ID_1, false);
                } else {
                    log("OP09 C6.3: new + new or new + old, no default, set sim1 as default.");
                    if (newSimStatus == 1 || newSimStatus == 2) {
                        setDataStatus(PhoneConstants.SIM_ID_1,
                                getDataEnabledFromSetting(defaultSub));
                    } else {
                        setDataStatus(PhoneConstants.SIM_ID_1, true);
                    }
                    // force set sim2 data off to ensure OP09 only one switch is on,
                    // the record maybe on if it is inserted before.
                    mDataSubSelector.setDataEnabled(PhoneConstants.SIM_ID_2, false);
                    mDataSubSelector.setDefaultData(PhoneConstants.SIM_ID_1);
                    mDataSubSelector.syncDefaultDataToMd(PhoneConstants.SIM_ID_1);
                }
                break;
            case MtkSubscriptionManager.EXTRA_VALUE_REMOVE_SIM:
                // remove one sim and left two? impossible.
                log("OP09 C7: a sim removed and two sim left, not support yet!");
                break;
            case MtkSubscriptionManager.EXTRA_VALUE_REPOSITION_SIM:
                // two sims swap slot don't change settings.
                log("OP09 C8: two sims swap slot location, do nothing.");
                // TODO: sim switch need do something. But we plan seperate it from data logic.
                break;
            case MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE:
                // boot up and detect two old sims, do nothing.
                log("OP09 C9: two sims exist and are old, do nothing.");
                break;
            default:
                log("OP09 C10: ignore unknown detectedType: " + detectedType);
                break;
            }
        } else {
            log("OP09 C11: sim count bigger than 2, not support yet!");
        }

        // Sync default data to system property.
        mDataSubSelector.updateDefaultDataProperty();
    }

    protected void setDataStatus(int phoneId, boolean status) {
        // Ppl means mobile anti-theft is enabled and phone entered in lock mode,
        // When Ppl enabled, disable data.
        if (isPplEnabled()) {
            mDataSubSelector.setDataEnabled(phoneId, false);
        } else {
            mDataSubSelector.setDataEnabled(phoneId, status);
        }
    }

    // Check whether sim is radio on in settings,
    // return true when sim inserted and radio on in settings.
    protected boolean isSimRadioOn(int phoneId) {
        String iccid = SystemProperties.get(DataSubSelectorUtil.PROPERTY_ICCID[phoneId], "");

        // If sim is absent, return false.
        if (TextUtils.isEmpty(iccid) || (DataSubConstants.NO_SIM_VALUE).equals(iccid)) {
            return false;
        }

        // if sim radio off, return false
        if (mSimSwitchPreference != null &&
                mSimSwitchPreference.contains(iccid) == true) {
            log("isSimRadioOn, iccid = " + iccid + " is radio off");
            return false;
        }

        return true;
    }

    protected boolean isPplEnabled() {
        boolean isPplEnabled = false;
        try {
            IBinder binder = ServiceManager.getService("PPLAgent");
            if (binder != null) {
                IPplAgent agent = IPplAgent.Stub.asInterface(binder);
                if (agent.needLock() == 1) {
                    isPplEnabled = true;
                }
            }
        } catch (RemoteException e) {
            log("DataSubselector, error in get PPLAgent service.");
        }

        return isPplEnabled;
    }

    protected boolean getDataEnabledFromSetting(int subId) {
        if (mContext == null || mContext.getContentResolver() == null) {
            log("getDataEnabledFromSetting, context or resolver is null, return");
            return false;
        }

        boolean retVal = false;
        try {
            retVal = Settings.Global.getInt(mContext.getContentResolver(),
                    Settings.Global.MOBILE_DATA + subId) != 0;
        } catch (SettingNotFoundException snfe) {
            retVal = false;
        }

        log("getDataEnabledFromSetting, subId = " + subId + " retVal = " + retVal);
        return retVal;
    }

    @Override
    public void handleAirPlaneModeOff(Intent intent) {

    }

    public void handlePlmnChanged(Intent intent) {}

    public void handleDataEnable(int status) {}

    public void handleDefaultDataChanged(Intent intent) {}

    private void log(String txt) {
        if (DBG) {
            Rlog.d(LOG_TAG, txt);
        }
    }

    private void loge(String txt) {
        if (DBG) {
            Rlog.e(LOG_TAG, txt);
        }
    }
}