#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <utils/Log.h>
//#include <utils/String8.h>
#include <assert.h>

#include <arsi_api.h> // implement MTK AURISYS API
#include <wrapped_audio.h>


#include "audio_custom_exp.h"


#include "AudioCompFltCustParamc.h"
#include "MtkAudioLoudc.h"




static debug_log_fp_t print_log;

static int32_t mMusicCount = 0;
static int32_t mMaxMusicCount = 10;

static audioloud_param_uthashlist_t *mAudioLoudParamList;

static audio_devices_t mIputDeviceOld;
static audio_devices_t mOutputDeviceOld;
static AUDIO_ACF_CUSTOM_PARAM_STRUCT mXmlParam[6];

static int mNoiseFilter;

//-----------------------------------------------------------
// private function
//-----------------------------------------------------------

static audioloud_lib_handle_t *lib_param_init(const int32_t enhancement_mode);

static int copy_param(audioloud_lib_handle_t *user);

status_t audioloud_arsi_set_key_value_pair(
    const string_buf_t *key_value_pair,
    void               *p_handler);
//-----------------------------------------------------------



audioloud_lib_handle_t *lib_param_init(const int32_t enhancement_mode) {

    audioloud_lib_handle_t *pUser = (audioloud_lib_handle_t *)
                                    malloc(sizeof(audioloud_lib_handle_t));
    memset(&pUser->mBloudHandle, 0, sizeof(BS_HD_Handle));
    memset(&pUser->mInitParam, 0, sizeof(BLOUD_HD_InitParam));
    memset(&pUser->mCachedAudioParam, 0,
           sizeof(AUDIO_ACF_CUSTOM_PARAM_STRUCT));
    memset(&pUser->mParamFormatUse, 0,
           sizeof(BLOUD_HD_IIR_Design_Param));

    BLOUD_HD_InitParam *mInitParam = &pUser->mInitParam;

    mInitParam->pMode_Param = malloc(sizeof(BLOUD_HD_ModeParam));
    memset(mInitParam->pMode_Param, 0, sizeof(BLOUD_HD_ModeParam));

    mInitParam->pMode_Param->pFilter_Coef_L =  malloc(sizeof(
                                                          BLOUD_HD_FilterCoef));
    memset(mInitParam->pMode_Param->pFilter_Coef_L, 0, sizeof(BLOUD_HD_FilterCoef));

    mInitParam->pMode_Param->pFilter_Coef_R = malloc(sizeof(BLOUD_HD_FilterCoef));
    memset(mInitParam->pMode_Param->pFilter_Coef_R, 0, sizeof(BLOUD_HD_FilterCoef));

    mInitParam->pMode_Param->pCustom_Param = malloc(sizeof(
                                                        BLOUD_HD_CustomParam));
    memset(mInitParam->pMode_Param->pCustom_Param, 0, sizeof(BLOUD_HD_CustomParam));

    BLOUD_HD_SetHandle(&pUser->mBloudHandle);
    pUser->id = (uint32_t)pUser;                  /* SHOULD BE uniq ID */
    pUser->mFilterType = enhancement_mode;
    pUser->mOriFilterType = enhancement_mode;
    pUser->bIsEnhaceOn = true;
    pUser->mState = BLOUD_STATE_INIT;
    //ALOGD("[lib]MtkAudioLoud Constructor id = %u\n", pUser->id);
    return pUser;
}

void lib_load_xmlparam(void) {
    int enhancemode;
    int xmlnum = 0;
    for (enhancemode = 0 ; enhancemode < AUDIO_COMP_FLT_NUM ; enhancemode++) {
#if defined(MTK_BESLOUDNESS_RUN_WITH_HAL)
        if (enhancemode == AUDIO_COMP_FLT_AUDIO ||
            enhancemode == AUDIO_COMP_FLT_HEADPHONE ||
            enhancemode == AUDIO_COMP_FLT_DRC_FOR_MUSIC ||
            enhancemode == AUDIO_COMP_FLT_DRC_FOR_RINGTONE ||
            enhancemode == AUDIO_COMP_FLT_VIBSPK)
#else
        if (enhancemode == AUDIO_COMP_FLT_AUDIO ||
            enhancemode == AUDIO_COMP_FLT_HEADPHONE ||
            enhancemode == AUDIO_COMP_FLT_HCF_FOR_USB ||
            enhancemode == AUDIO_COMP_FLT_VIBSPK)
#endif
        {
            getAudioCompFltCustParamFromStorage((AudioCompFltType_t)enhancemode,
                                                (AUDIO_ACF_CUSTOM_PARAM_STRUCT *)&mXmlParam[xmlnum]);

            audioloud_param_uthashlist_t  *pUser = (audioloud_param_uthashlist_t *)
                                                   malloc(sizeof(audioloud_param_uthashlist_t));
            pUser->mFilterId = enhancemode;
            pUser->mParamSturct = &mXmlParam[xmlnum];
            HASH_ADD_INT(mAudioLoudParamList, mFilterId, pUser); /* id: name of key field */
            // ALOGD("MtkAudioLoud load  xml content mode: %d\n",
            //      pUser->mFilterId = enhancemode);
            xmlnum++;
        }
    }
}

void lib_release_xmlparam(void) {
    unsigned int num_users;
    num_users = HASH_COUNT(mAudioLoudParamList);
    if (num_users == 0) {
        return ;
    }

    audioloud_param_uthashlist_t *user;
    audioloud_param_uthashlist_t *temp;

    HASH_ITER(hh, mAudioLoudParamList, user, temp) {
        if (user != NULL) {
            /* free mComponent*/
            user->mParamSturct = NULL;
            /* free component container*/
            HASH_DEL(mAudioLoudParamList, user); /* delete; users advances to next */
            free(user);
        }
    }
}

void lib_release_varlist(audioloud_lib_handle_t *p_handler) {

    //constructor init
    BLOUD_HD_InitParam *mInitParam = &p_handler->mInitParam;
    // check to free notch worksace;
    free(mInitParam->pMode_Param->pFilter_Coef_L);
    free(mInitParam->pMode_Param->pFilter_Coef_R);
    free(mInitParam->pMode_Param->pCustom_Param);
    free(mInitParam->pMode_Param);
    /* free component container*/
    free(p_handler);
}

status_t lib_update_filterparam(audioloud_lib_handle_t *pUser,
                                AUDIO_ACF_CUSTOM_PARAM_STRUCT *mArsiParamBuf, uint32_t mFilterType ,
                                bool bUpdateXml) {
    if (pUser == NULL) {
        return BAD_VALUE;
    }
    audioloud_param_uthashlist_t *param_user;
    audioloud_param_uthashlist_t *param_temp;
    AUDIO_ACF_V5F_PARAM acf_temp;
    int mCheckStatus = 0;

    if (mFilterType != pUser->mFilterType) {
        pUser->mOriFilterType = pUser->mFilterType;
        pUser->mFilterType = mFilterType;
    }

    memset(&acf_temp, 0, sizeof(AUDIO_ACF_V5F_PARAM));

    HASH_ITER(hh, mAudioLoudParamList, param_user, param_temp) {
        if (mFilterType == AUDIO_COMP_FLT_DRC_FOR_MUSIC  ||
            mFilterType == AUDIO_COMP_FLT_DRC_FOR_RINGTONE) {
            if (param_user->mFilterId == AUDIO_COMP_FLT_AUDIO) {
                memcpy((void *)&acf_temp,
                       (void *)&param_user->mParamSturct->bes_loudness_f_param.V5F,
                       sizeof(AUDIO_ACF_V5F_PARAM));
            }
        }
        if ((uint32_t)param_user->mFilterId == mFilterType) {
            if (bUpdateXml && mArsiParamBuf != NULL) {
                memcpy((void *)param_user->mParamSturct, (void *)mArsiParamBuf,
                       sizeof(AUDIO_ACF_CUSTOM_PARAM_STRUCT));
            } else if (mArsiParamBuf != NULL) {
                memcpy((void *)mArsiParamBuf, (void *)param_user->mParamSturct,
                       sizeof(AUDIO_ACF_CUSTOM_PARAM_STRUCT));
            }

            memcpy((void *)&pUser->mCachedAudioParam, (void *)param_user->mParamSturct,
                   sizeof(AUDIO_ACF_CUSTOM_PARAM_STRUCT));
            break;
        }
    }

    // combine DRC + ACF param
    if (pUser->mFilterType == AUDIO_COMP_FLT_DRC_FOR_MUSIC  ||
        pUser->mFilterType == AUDIO_COMP_FLT_DRC_FOR_RINGTONE) {
        if (mArsiParamBuf != NULL) {
            memcpy((void *)&mArsiParamBuf->bes_loudness_f_param.V5F, (void *)&acf_temp,
                   sizeof(AUDIO_ACF_V5F_PARAM));
        }
        memcpy((void *)&pUser->mCachedAudioParam.bes_loudness_f_param.V5F,
               (void *)&acf_temp,
               sizeof(AUDIO_ACF_V5F_PARAM));
        //ALOGD("Liang : DRC type:%d combine ACF param!", pUser->mFilterType);
    }
    mCheckStatus = copy_param(pUser);

    if (mCheckStatus != 0) {
        return BAD_VALUE;
    } else {
        return NO_ERROR;
    }
}


void use_default_fullband(BLOUD_HD_InitParam *pInitParam) {
    if (pInitParam) {
        unsigned int Att_Time_Default[6] = {64, 64, 64, 64, 64, 64};
        unsigned int Rel_Time_Default[6] = {6400, 6400, 6400, 6400, 6400, 6400};
        int          Hyst_Th_Default[6]  = {256, 256, 256, 256, 256, 256};
        int          DRC_Th_Default[5]   = { -15360, -12800, -10240, -7680, 0,};
        int          DRC_Gn_Default[5]   = {6144, 6144, 6144, 6144, 0};

        pInitParam->pMode_Param->pCustom_Param->Num_Bands = 1;
        pInitParam->pMode_Param->pCustom_Param->SB_Mode[0] = 0;
        pInitParam->pMode_Param->pCustom_Param->SB_Gn[0] = 0;
        pInitParam->pMode_Param->pCustom_Param->Lim_Th = 0x7FFF;
        pInitParam->pMode_Param->pCustom_Param->Lim_Gn = 0x7FFF;
        pInitParam->pMode_Param->pCustom_Param->Lim_Const = 4;
        pInitParam->pMode_Param->pCustom_Param->Sep_LR_Filter = 0;

        memcpy((void *)pInitParam->pMode_Param->pCustom_Param->Att_Time,
               (void *)Att_Time_Default, 6 * sizeof(unsigned int));
        memcpy((void *)pInitParam->pMode_Param->pCustom_Param->Rel_Time,
               (void *)Rel_Time_Default, 6 * sizeof(unsigned int));
        memcpy((void *)pInitParam->pMode_Param->pCustom_Param->Hyst_Th,
               (void *)Hyst_Th_Default,  6 * sizeof(int));
        memcpy((void *)pInitParam->pMode_Param->pCustom_Param->DRC_Th,
               (void *)DRC_Th_Default,   5 * sizeof(int));
        memcpy((void *)pInitParam->pMode_Param->pCustom_Param->DRC_Gn,
               (void *)DRC_Gn_Default,   5 * sizeof(int));
    }
}

void use_noise_filter(BLOUD_HD_InitParam *pInitParam) {
    if (pInitParam) {
        int NoiseGap1 = NOISE_FILTER_BASE * mNoiseFilter;
        int NoiseGap2 = NOISE_FILTER_BASE * (mNoiseFilter - NOISE_FILTER_STEP);

        int DRC_Th_Default[2]   = {NoiseGap1, NoiseGap2};
        int DRC_Gn_Default[1]   = {0};
        unsigned int i;
        for (i = 0; i < pInitParam->pMode_Param->pCustom_Param->Num_Bands; i++) {
            if (pInitParam->pMode_Param->pCustom_Param->DRC_Th[i][0] > DRC_Th_Default[0]) {
                pInitParam->pMode_Param->pCustom_Param->DRC_Th[i][0] = DRC_Th_Default[0];
            }
            if (pInitParam->pMode_Param->pCustom_Param->DRC_Th[i][1] > DRC_Th_Default[1]) {
                pInitParam->pMode_Param->pCustom_Param->DRC_Th[i][1] = DRC_Th_Default[1];
            }

            pInitParam->pMode_Param->pCustom_Param->DRC_Gn[i][0] = DRC_Gn_Default[0];
        }
    }
}

AudioCompFltType_t get_filter_bydevice(const arsi_task_config_t
                                       *p_arsi_task_config) {
    AudioCompFltType_t filtertype = AUDIO_COMP_FLT_AUDIO;
    audio_devices_t output_devices = p_arsi_task_config->output_device_info.devices;
    uint32_t hw_info_mask = p_arsi_task_config->output_device_info.hw_info_mask;
    AUDIO_AUDENH_CONTROL_OPTION_STRUCT drcParam;

    //ALOGD("Liang: get_filter_bydevice : output_devices = %x mAudioMode = %d",output_devices,mAudioMode);
    if ((output_devices & AUDIO_DEVICE_OUT_WIRED_HEADSET) ||
        (output_devices & AUDIO_DEVICE_OUT_WIRED_HEADPHONE)) {
        filtertype = AUDIO_COMP_FLT_HEADPHONE;
    } else if (output_devices & AUDIO_DEVICE_OUT_USB_DEVICE) {
        filtertype = AUDIO_COMP_FLT_HCF_FOR_USB;
    }

    if (output_devices & AUDIO_DEVICE_OUT_SPEAKER) {
#if defined(MTK_BESLOUDNESS_RUN_WITH_HAL)
        if (getDRCControlOptionParamFromStorage(&drcParam)) {
            if (drcParam.u32EnableFlg) {   // DRC enable
                if (p_arsi_task_config->audio_mode == AUDIO_MODE_RINGTONE) {
                    filtertype = AUDIO_COMP_FLT_DRC_FOR_RINGTONE;
                } else {
                    filtertype = AUDIO_COMP_FLT_DRC_FOR_MUSIC;
                }
            } else {
                filtertype = AUDIO_COMP_FLT_AUDIO;
            }
        }
#else
        filtertype = AUDIO_COMP_FLT_AUDIO;
#endif
    } else if (output_devices & AUDIO_DEVICE_OUT_EARPIECE) {
        if ((hw_info_mask & OUTPUT_DEVICE_HW_INFO_VIBRATION_SPEAKER) &&
            (hw_info_mask & OUTPUT_DEVICE_HW_INFO_2IN1_SPEAKER)) {
            filtertype = AUDIO_COMP_FLT_VIBSPK;
        }
    }
    return filtertype;
}


status_t set_lib_parameter(uint32_t paramID, void *param,
                           audioloud_lib_handle_t *pUser) {
    uint32_t Curparam = (uint32_t)((long)param);
    int32_t ret = 0;
    //print_log("+%s(), paramID %d, param %d\n", __FUNCTION__, paramID, Curparam);

    //Add constraint to limit the use after open.
    switch (paramID) {
    case BLOUD_PAR_SET_FILTER_TYPE: {
        pUser->mFilterType = Curparam;
        break;
    }
    case BLOUD_PAR_SET_WORK_MODE: {
        switch (Curparam) {
        case AUDIO_CMP_FLT_LOUDNESS_BASIC:     // basic Loudness mode
            pUser->mInitParam.pMode_Param->Filter_Mode   = HD_FILT_MODE_LOUD_FLT;
            pUser->mInitParam.pMode_Param->Loudness_Mode = HD_LOUD_MODE_BASIC;
            break;
        case AUDIO_CMP_FLT_LOUDNESS_ENHANCED:     // enhancement(1) Loudness mode
            pUser->mInitParam.pMode_Param->Filter_Mode   = HD_FILT_MODE_LOUD_FLT;
            pUser->mInitParam.pMode_Param->Loudness_Mode = HD_LOUD_MODE_ENHANCED;
            break;
        case AUDIO_CMP_FLT_LOUDNESS_AGGRESSIVE:     // enhancement(2) Loudness mode
            pUser->mInitParam.pMode_Param->Filter_Mode   = HD_FILT_MODE_LOUD_FLT;
            pUser->mInitParam.pMode_Param->Loudness_Mode = HD_LOUD_MODE_AGGRESSIVE;
            break;
        case AUDIO_CMP_FLT_LOUDNESS_LITE:     // Only DRC, no filtering
            pUser->mInitParam.pMode_Param->Filter_Mode   = HD_FILT_MODE_NONE;
            pUser->mInitParam.pMode_Param->Loudness_Mode = HD_LOUD_MODE_BASIC;
            break;
        case AUDIO_CMP_FLT_LOUDNESS_COMP:     // Audio Compensation Filter mode (No DRC)
            pUser->mInitParam.pMode_Param->Filter_Mode   = HD_FILT_MODE_COMP_FLT;
            pUser->mInitParam.pMode_Param->Loudness_Mode = HD_LOUD_MODE_NONE;
            break;
        case AUDIO_CMP_FLT_LOUDNESS_COMP_BASIC:     // Audio Compensation Filter mode + DRC
            pUser->mInitParam.pMode_Param->Filter_Mode   = HD_FILT_MODE_COMP_FLT;
            pUser->mInitParam.pMode_Param->Loudness_Mode = HD_LOUD_MODE_BASIC;
            break;
        case AUDIO_CMP_FLT_LOUDNESS_COMP_HEADPHONE:     //HCF
            pUser->mInitParam.pMode_Param->Filter_Mode   = HD_FILT_MODE_COMP_HDP;
            pUser->mInitParam.pMode_Param->Loudness_Mode = HD_LOUD_MODE_NONE;
            break;
        case AUDIO_CMP_FLT_LOUDNESS_COMP_AUDENH:
            pUser->mInitParam.pMode_Param->Filter_Mode   = HD_FILT_MODE_AUD_ENH;
            pUser->mInitParam.pMode_Param->Loudness_Mode = HD_LOUD_MODE_NONE;
            break;
        case AUDIO_CMP_FLT_LOUDNESS_COMP_LOW_LATENCY:
            pUser->mInitParam.pMode_Param->Filter_Mode   = HD_FILT_MODE_LOW_LATENCY_ACF;
            pUser->mInitParam.pMode_Param->Loudness_Mode = HD_LOUD_MODE_NONE;
            pUser->mInitParam.pMode_Param->pCustom_Param->Disable_Limiter = 1;
            break;
        default:
            ALOGW("%s() invalide workmode %d\n", __FUNCTION__, Curparam);
            break;
        }
        break;
    }
    case BLOUD_PAR_SET_SAMPLE_RATE: {
        pUser->mInitParam.Sampling_Rate = Curparam;
        break;
    }
    case BLOUD_PAR_SET_PCM_FORMAT: {
        pUser->mInitParam.PCM_Format = Curparam;
        break;
    }
    case BLOUD_PAR_SET_CHANNEL_NUMBER: {
        uint32_t chNum = Curparam;
        if (chNum > 0 && chNum < 3) {
            // chnum should be 1 or 2
            pUser->mInitParam.Channel = chNum;
        } else {
            ret = -1;
        }
        break;
    }
    case BLOUD_PAR_SET_STEREO_TO_MONO_MODE: {
        pUser->mInitParam.pMode_Param->S2M_Mode = Curparam;
        if (pUser->mState == BLOUD_STATE_OPEN) {
            BLOUD_HD_RuntimeParam runtime_param;
            runtime_param.Command = BLOUD_HD_CHANGE_MODE;
            runtime_param.pMode_Param = pUser->mInitParam.pMode_Param;
            ret = pUser->mBloudHandle.SetParameters(&pUser->mBloudHandle,
                                                    &runtime_param);
        }
    }
    case BLOUD_PAR_SET_UPDATE_PARAM_TO_SWIP: {
        if (pUser->mState != BLOUD_STATE_OPEN) {
            ret = -1;
        }
        BLOUD_HD_RuntimeParam runtime_param;
        runtime_param.Command = BLOUD_HD_CHANGE_MODE;
        runtime_param.pMode_Param = (BLOUD_HD_ModeParam *)
                                    pUser->mInitParam.pMode_Param;
        ret = pUser->mBloudHandle.SetParameters(&pUser->mBloudHandle,
                                                &runtime_param);
        // print_log("%s runtime BLOUD_HD_CHANGE_MODE, FLT Mode = %d Loudness Mode = %d", __FUNCTION__,
        //       pUser->mInitParam.pMode_Param->Filter_Mode, pUser->mInitParam.pMode_Param->Loudness_Mode);
        break;
    }
    case BLOUD_PAR_SET_RAMP_UP: {
        if (Curparam == 0) {
            pUser->mInitParam.Initial_State = BLOUD_HD_NORMAL_STATE;
        } else {
            pUser->mInitParam.Initial_State = BLOUD_HD_BYPASS_STATE;
        }

        // print_log("%s %d mInitParam.Initial_State %d", __FUNCTION__, __LINE__,
        //       pUser->mInitParam.Initial_State);
        break;
    }
    case BLOUD_PAR_SET_NOISE_FILTER: {
        if (Curparam == 1) {
            mNoiseFilter = DRC_NOISEFILTER_MIN;
        } else {
            mNoiseFilter = 0;
        }
        break;
    }
    case BLOUD_PAR_SET_CHANGE_TO_BYPASS: {

        BLOUD_HD_RuntimeStatus runtime_status;

        BLOUD_HD_RuntimeParam runtime_param;
        runtime_param.Command = BLOUD_HD_TO_BYPASS_STATE;
        ret = pUser->mBloudHandle.SetParameters(&pUser->mBloudHandle,
                                                &runtime_param);
        // print_log("Change to Bypass! ret = %d", ret);
        break;
    }
    case BLOUD_PAR_SET_CHANGE_TO_NORMAL: {
        BLOUD_HD_RuntimeStatus runtime_status;

        BLOUD_HD_RuntimeParam runtime_param;
        runtime_param.Command = BLOUD_HD_TO_NORMAL_STATE;
        ret = pUser->mBloudHandle.SetParameters(&pUser->mBloudHandle,
                                                &runtime_param);
        // print_log("Change to Normal! ret = %d", ret);
        break;
    }
    default:
        //print_log("-%s() Error\n", __FUNCTION__);
        ret = -1;
    }

    if (ret != 0) {
        ret = BAD_VALUE;
    } else {
        ret = NO_ERROR;
    }

    return ret;
}

int convert_filter_type(AudioCompFltType_t filtertype, uint32_t task_flag) {
    AudioComFltMode_t AudioFltMode = AUDIO_CMP_FLT_LOUDNESS_COMP;

    switch (filtertype) {
    case AUDIO_COMP_FLT_AUDIO :
    {
        if(task_flag == AUDIO_OUTPUT_FLAG_FAST)
            AudioFltMode = AUDIO_CMP_FLT_LOUDNESS_COMP_LOW_LATENCY;
        else
            AudioFltMode = AUDIO_CMP_FLT_LOUDNESS_COMP;
    }
        break;
    case AUDIO_COMP_FLT_HEADPHONE :
    case AUDIO_COMP_FLT_HCF_FOR_USB :
        AudioFltMode = AUDIO_CMP_FLT_LOUDNESS_COMP_HEADPHONE;
        break;
    case AUDIO_COMP_FLT_AUDENH :
        AudioFltMode = AUDIO_CMP_FLT_LOUDNESS_COMP_AUDENH;
        break;
    case AUDIO_COMP_FLT_VIBSPK :
        AudioFltMode = AUDIO_CMP_FLT_LOUDNESS_COMP;
        break;
    case AUDIO_COMP_FLT_DRC_FOR_MUSIC :
    case AUDIO_COMP_FLT_DRC_FOR_RINGTONE:
        AudioFltMode = AUDIO_CMP_FLT_LOUDNESS_COMP_BASIC;
        break;
    default:
        break;
    }
    return AudioFltMode;
}

uint32_t convert_audiomode_to_drcflt(audio_mode_t audiomode) {
    AudioCompFltType_t AudioFltType = AUDIO_COMP_FLT_AUDIO;
    switch (audiomode) {
    case AUDIO_MODE_NORMAL :
        AudioFltType = AUDIO_COMP_FLT_DRC_FOR_MUSIC;
        break;
    case AUDIO_MODE_RINGTONE:
        AudioFltType = AUDIO_COMP_FLT_DRC_FOR_RINGTONE;
        break;
    default:
        print_log("%s() unsupport audiomode to drc %d\n", __FUNCTION__, audiomode);
        break;
    }
    return AudioFltType;
}

int get_samplingrate_index(unsigned int sampling_rate) {
    int sr_idx;

    switch (sampling_rate) {
    case 48000:
        sr_idx = 0;
        break;
    case 44100:
        sr_idx = 1;
        break;
    case 32000:
        sr_idx = 2;
        break;
    case 24000:
        sr_idx = 3;
        break;
    case 22050:
        sr_idx = 4;
        break;
    case 16000:
        sr_idx = 5;
        break;
    case 12000:
        sr_idx = 6;
        break;
    case 11025:
        sr_idx = 7;
        break;
    case  8000:
        sr_idx = 8;
        break;
    default:
        sr_idx = -1;
        break;
    }

    return sr_idx;
}


static int copy_param(audioloud_lib_handle_t *user) { //ony use for v5
    bool ZeroFlag = true;
    bool bIsZeroCoeff;
    bool mIsSepLR_Filter;
    int mCheckStatus = 0, i;

    BLOUD_HD_InitParam mInitParam = user->mInitParam;
    AUDIO_ACF_CUSTOM_PARAM_STRUCT mCachedAudioParam = user->mCachedAudioParam;
    uint32_t mFilterType = user->mFilterType;
    BLOUD_HD_IIR_Design_Param mParamFormatUse = user->mParamFormatUse;

    mInitParam.pMode_Param->pCustom_Param->WS_Gain_Max =
        mCachedAudioParam.bes_loudness_WS_Gain_Max;
    mInitParam.pMode_Param->pCustom_Param->WS_Gain_Min =
        mCachedAudioParam.bes_loudness_WS_Gain_Min;
    mInitParam.pMode_Param->pCustom_Param->Filter_First =
        mCachedAudioParam.bes_loudness_Filter_First;
    mInitParam.pMode_Param->pCustom_Param->Num_Bands =
        mCachedAudioParam.bes_loudness_Num_Bands;
    mInitParam.pMode_Param->pCustom_Param->Flt_Bank_Order =
        mCachedAudioParam.bes_loudness_Flt_Bank_Order;
    mInitParam.pMode_Param->pCustom_Param->DRC_Delay = mCachedAudioParam.DRC_Delay;
    mInitParam.pMode_Param->pCustom_Param->Lim_Th = mCachedAudioParam.Lim_Th;
    mInitParam.pMode_Param->pCustom_Param->Lim_Gn = mCachedAudioParam.Lim_Gn;
    mInitParam.pMode_Param->pCustom_Param->Lim_Const = mCachedAudioParam.Lim_Const;
    mInitParam.pMode_Param->pCustom_Param->Lim_Delay = mCachedAudioParam.Lim_Delay;
    mInitParam.pMode_Param->pCustom_Param->Sep_LR_Filter = mIsSepLR_Filter =
                                                               mCachedAudioParam.bes_loudness_Sep_LR_Filter;
    memcpy((void *)mInitParam.pMode_Param->pCustom_Param->Att_Time,
           (void *)mCachedAudioParam.Att_Time, 48 * sizeof(unsigned int));
    memcpy((void *)mInitParam.pMode_Param->pCustom_Param->Rel_Time,
           (void *)mCachedAudioParam.Rel_Time, 48 * sizeof(unsigned int));
    memcpy((void *)mInitParam.pMode_Param->pCustom_Param->Cross_Freq,
           (void *)mCachedAudioParam.bes_loudness_Cross_Freq, 7 * sizeof(unsigned int));
    memcpy((void *)mInitParam.pMode_Param->pCustom_Param->SB_Mode,
           (void *)mCachedAudioParam.SB_Mode, 8 * sizeof(unsigned int));
    memcpy((void *)mInitParam.pMode_Param->pCustom_Param->SB_Gn,
           (void *)mCachedAudioParam.SB_Gn, 8 * sizeof(unsigned int));
    memcpy((void *)mInitParam.pMode_Param->pCustom_Param->Hyst_Th,
           (void *)mCachedAudioParam.Hyst_Th, 48 * sizeof(int));
    memcpy((void *)mInitParam.pMode_Param->pCustom_Param->DRC_Th,
           (void *)mCachedAudioParam.DRC_Th, 40 * sizeof(int));
    memcpy((void *)mInitParam.pMode_Param->pCustom_Param->DRC_Gn,
           (void *)mCachedAudioParam.DRC_Gn, 40 * sizeof(int));

    if (mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_hpf_fc != 0
        || mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_hpf_order != 0
        || mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_lpf_fc != 0
        || mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_lpf_order != 0
        || mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_hpf_fc != 0
        || mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_hpf_order != 0
        || mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_lpf_fc != 0
        || mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_lpf_order != 0
       ) {
        ZeroFlag = false;
    }

    if (ZeroFlag) {
        for (i = 0; i < 8; i++) {
            if (mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_bpf_fc[i] != 0
                || mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_bpf_bw[i] != 0
                || mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_bpf_gain[i] != 0
                || mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_bpf_fc[i] != 0
                || mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_bpf_bw[i] != 0
                || mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_bpf_gain[i] != 0
               ) {
                ZeroFlag = false;
                break;
            }
        }
    }

    bIsZeroCoeff = ZeroFlag;

    if (mFilterType == AUDIO_COMP_FLT_VIBSPK) {
        unsigned int bes_loudness_bpf_coeff[8][6][3] = {{{0}}};

        memset(mInitParam.pMode_Param->pFilter_Coef_R->BPF_COEF, 0,
               8 * 6 * sizeof(unsigned int));
        memcpy((void *)bes_loudness_bpf_coeff,
               (void *)mCachedAudioParam.bes_loudness_f_param.V5ViVSPK.bes_loudness_bpf_coeff,
               36 * sizeof(unsigned int));

        int sr_idx = get_samplingrate_index(mInitParam.Sampling_Rate);

        if (sr_idx >= 0 && sr_idx < 6) {
            int flt_idx;
            int i = 0;
            for (flt_idx = 0; flt_idx < 8; flt_idx++) {
                memset(mInitParam.pMode_Param->pFilter_Coef_L->BPF_COEF[flt_idx], 0,
                       sizeof(unsigned int) * 6);
                memcpy(mInitParam.pMode_Param->pFilter_Coef_L->BPF_COEF[flt_idx],
                       bes_loudness_bpf_coeff[flt_idx][sr_idx], sizeof(unsigned int) * 3);
                memset(mInitParam.pMode_Param->pFilter_Coef_R->BPF_COEF[flt_idx], 0,
                       sizeof(unsigned int) * 6);
                memcpy(mInitParam.pMode_Param->pFilter_Coef_R->BPF_COEF[flt_idx],
                       bes_loudness_bpf_coeff[flt_idx][sr_idx], sizeof(unsigned int) * 3);
                i++;
            }
        }
    } else {
        mParamFormatUse.hpf_fc =
            mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_hpf_fc;
        mParamFormatUse.hpf_order =
            mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_hpf_order;
        mParamFormatUse.lpf_fc =
            mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_lpf_fc;
        mParamFormatUse.lpf_order =
            mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_lpf_order;
        memcpy((void *)mParamFormatUse.bpf_fc,
               (void *)mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_bpf_fc,
               8 * sizeof(unsigned int));
#ifdef DEBUG_PARAMDUMP
        ALOGD("%s Filter_Mode %d, Loudness_Mode %d", __FUNCTION__,
              mInitParam.pMode_Param->Filter_Mode, mInitParam.pMode_Param->Loudness_Mode);
        int i = 0;
        for (i = 0; i < 8; i++) {
            ALOGD("mParamFormatUse.bpf_fc : 0x%08X, ", mParamFormatUse.bpf_fc[i]);
        }
#endif
        memcpy((void *)mParamFormatUse.bpf_bw,
               (void *)mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_bpf_bw,
               8 * sizeof(unsigned int));
        memcpy((void *)mParamFormatUse.bpf_gain,
               (void *)mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_L_bpf_gain,
               8 * sizeof(int));
        mCheckStatus = BLOUD_HD_Filter_Design(mInitParam.pMode_Param->Filter_Mode,
                                              mInitParam.Sampling_Rate, &mParamFormatUse,
                                              mInitParam.pMode_Param->pFilter_Coef_L);

        mParamFormatUse.hpf_fc =
            mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_hpf_fc;
        mParamFormatUse.hpf_order =
            mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_hpf_order;
        mParamFormatUse.lpf_fc =
            mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_lpf_fc;
        mParamFormatUse.lpf_order =
            mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_lpf_order;
        memcpy((void *)mParamFormatUse.bpf_fc,
               (void *)mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_bpf_fc,
               8 * sizeof(unsigned int));
        memcpy((void *)mParamFormatUse.bpf_bw,
               (void *)mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_bpf_bw,
               8 * sizeof(unsigned int));
        memcpy((void *)mParamFormatUse.bpf_gain,
               (void *)mCachedAudioParam.bes_loudness_f_param.V5F.bes_loudness_R_bpf_gain,
               8 * sizeof(int));

        mCheckStatus = BLOUD_HD_Filter_Design(mInitParam.pMode_Param->Filter_Mode,
                                              mInitParam.Sampling_Rate, &mParamFormatUse,
                                              mInitParam.pMode_Param->pFilter_Coef_R);
    }

    if (mIsSepLR_Filter == 0) {
        memset((void *)mInitParam.pMode_Param->pFilter_Coef_R->HPF_COEF, 0,
               10 * sizeof(unsigned int));
        memset((void *)mInitParam.pMode_Param->pFilter_Coef_R->BPF_COEF, 0,
               48 * sizeof(unsigned int));
        memset((void *)mInitParam.pMode_Param->pFilter_Coef_R->LPF_COEF, 0,
               3 * sizeof(unsigned int));
    }
#ifdef DEBUG_PARAMDUMP
    ALOGD("Liang :bIsZeroCoeff %d mFilterType %d", bIsZeroCoeff, mFilterType);
    ALOGD("Liang :Channel %d", mInitParam.Channel);
    ALOGD("Liang :Sampling_Rate %d", mInitParam.Sampling_Rate);
    ALOGD("Liang :PCM_Format %d", mInitParam.PCM_Format);

    ALOGD("Liang :copy_param mIsSepLR_Filter [%d]", mIsSepLR_Filter);

    int stage_idx;
    int coef_idx;
    ALOGD("HPF[2][5] = ");
    for (stage_idx = 0; stage_idx < 2; stage_idx++) {
        for (coef_idx = 0; coef_idx < 5; coef_idx++) {
            ALOGD("0x%08X, ",
                  mInitParam.pMode_Param->pFilter_Coef_L->HPF_COEF[stage_idx][coef_idx]);
        }
    }
    ALOGD("LPF[3] = \n");
    for (coef_idx = 0; coef_idx < 3; coef_idx++) {
        ALOGD("0x%08X ", mInitParam.pMode_Param->pFilter_Coef_L->LPF_COEF[coef_idx]);
    }
    for (i = 0; i < 5; i++) {
        ALOGD("DRC_Th[0][%d] = %d", i,
              mInitParam.pMode_Param->pCustom_Param->DRC_Th[0][i]);
        ALOGD("DRC_Gn[0][%d] = %d", i,
              mInitParam.pMode_Param->pCustom_Param->DRC_Gn[0][i]);
    }
#endif
    return mCheckStatus;
}


status_t audioloud_arsi_query_working_buf_size(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    uint32_t                 *p_working_buf_size,
    const debug_log_fp_t      debug_log_fp) {
    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        p_working_buf_size == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }
    uint32_t mPcmFormat;
    uint32_t mTempBufSize;
    uint32_t mInternalBufSize;

    /* for callback query buffer API */
    BS_HD_Handle mBloudHandle;
    BLOUD_HD_SetHandle(&mBloudHandle);

    if (p_arsi_lib_config->audio_format ==
        AUDIO_FORMAT_PCM_32_BIT) {
        mPcmFormat = BLOUDHD_IN_Q1P31_OUT_Q1P31;
    } else {
        mPcmFormat = BLOUDHD_IN_Q1P15_OUT_Q1P15;
    }

    /* query buffer size */
    mBloudHandle.GetBufferSize(&mInternalBufSize, &mTempBufSize, mPcmFormat);

    *p_working_buf_size = mInternalBufSize + mTempBufSize;

    debug_log_fp("%s(), working_buf_size(%lu) = mInternalBufSize (%u) + mTempBufSize (%u),mPcmFormat(%d)\n",
                 __func__, *p_working_buf_size, mInternalBufSize, mTempBufSize, mPcmFormat);
    return NO_ERROR;
}

status_t audioloud_arsi_create_handler(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const data_buf_t         *p_param_buf,
    data_buf_t               *p_working_buf,
    void                    **pp_handler,
    const debug_log_fp_t      debug_log_fp) {
    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        p_param_buf == NULL ||
        p_working_buf == NULL ||
        pp_handler == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

    int32_t result;
    uint32_t mPcmFormat;
    AudioComFltMode_t AudioMode;
    AudioCompFltType_t filtertype;
    audioloud_lib_handle_t *pUser = NULL;
    char *pworking_buf = (char *)p_working_buf->p_buffer;

    if (p_arsi_lib_config->audio_format ==
        AUDIO_FORMAT_PCM_32_BIT) {
        mPcmFormat = BLOUDHD_IN_Q1P31_OUT_Q1P31;
    } else {
        mPcmFormat = BLOUDHD_IN_Q1P15_OUT_Q1P15;
    }

    filtertype = get_filter_bydevice(p_arsi_task_config);

    /*init lib handle*/
    pUser = lib_param_init(filtertype);

    if (pUser == NULL) {
        return BAD_VALUE;
    }

    pUser->mScenario = p_arsi_task_config->output_flags;
    /*set lib params*/
    set_lib_parameter(BLOUD_PAR_SET_CHANNEL_NUMBER,
                      (void *)(long)p_arsi_lib_config->p_dl_buf_in->num_channels, pUser);
    set_lib_parameter(BLOUD_PAR_SET_SAMPLE_RATE,
                      (void *)(long)p_arsi_lib_config->p_dl_buf_in->sample_rate_content, pUser);
    set_lib_parameter(BLOUD_PAR_SET_PCM_FORMAT, (void *)(long)mPcmFormat, pUser);
    set_lib_parameter(BLOUD_PAR_SET_RAMP_UP, (void *)(long)p_arsi_lib_config->reserve2, pUser);
    AudioMode = convert_filter_type(pUser->mFilterType, pUser->mScenario);
    set_lib_parameter(BLOUD_PAR_SET_WORK_MODE, (void *)(long)AudioMode, pUser);
    debug_log_fp("mScenario = %d AudioMode = %d", pUser->mScenario, AudioMode);
#if !defined(MTK_ENABLE_STEREO_SPEAKER)
    audio_devices_t output_devices = p_arsi_task_config->output_device_info.devices;

    if (output_devices & AUDIO_DEVICE_OUT_SPEAKER) {
        set_lib_parameter(BLOUD_PAR_SET_STEREO_TO_MONO_MODE, (void *)(long)BLOUD_S2M_MODE_ST2MO2ST, pUser);
    }
#endif

    lib_update_filterparam(pUser, NULL, pUser->mFilterType, false);
    /* To get size info*/
    pUser->mBloudHandle.GetBufferSize(&pUser->mInternalBufSize, &pUser->mTempBufSize, mPcmFormat);


    pUser->mpInternalBuf = pworking_buf;
    pUser->mpTempBuf = pworking_buf + pUser->mInternalBufSize;

    debug_log_fp("%s(), mpInternalBuf = %p, mpTempBuf = %p, p_param_buf = %p\n",
                 __func__, pUser->mpInternalBuf, pUser->mpTempBuf, p_param_buf->p_buffer);

    result = pUser->mBloudHandle.Open(&pUser->mBloudHandle,
                                      pUser->mpInternalBuf, (const void *)&pUser->mInitParam);
    pUser->mState = BLOUD_STATE_OPEN;

    *pp_handler = pUser;

    return NO_ERROR;
}


status_t audioloud_arsi_process_ul_buf(
    audio_buf_t *p_ul_buf_in,
    audio_buf_t *p_ul_buf_out,
    audio_buf_t *p_ul_ref_bufs __unused,
    data_buf_t  *p_debug_dump_buf __unused,
    void        *p_handler) {

    if (p_ul_buf_in == NULL ||
        p_ul_buf_out == NULL ||
        p_handler == NULL) {
        return NO_INIT;
    }

    return NO_ERROR;
}


status_t audioloud_arsi_process_dl_buf(
    audio_buf_t *p_dl_buf_in,
    audio_buf_t *p_dl_buf_out,
    audio_buf_t *p_dl_ref_bufs __unused,
    data_buf_t  *p_debug_dump_buf __unused,
    void        *p_handler) {
    int32_t result = 0;
    audioloud_lib_handle_t *lib_handler = NULL;

    if (p_dl_buf_in == NULL ||
        p_dl_buf_out == NULL ||
        p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (audioloud_lib_handle_t *)p_handler;
#if !defined(ENABLE_AUDIO_COMPENSATION_FILTER) || defined(BYPASS_ACF)
    if ((convert_filter_type(lib_handler->mFilterType, lib_handler->mScenario))
        == AUDIO_CMP_FLT_LOUDNESS_COMP ||
        (convert_filter_type(lib_handler->mFilterType, lib_handler->mScenario))
        == AUDIO_CMP_FLT_LOUDNESS_COMP_LOW_LATENCY) {
        // bypass the process
        memcpy(p_dl_buf_out->data_buf.p_buffer, p_dl_buf_in->data_buf.p_buffer,
               p_dl_buf_in->data_buf.data_size);
        p_dl_buf_out->data_buf.data_size = p_dl_buf_in->data_buf.data_size;
        p_dl_buf_in->data_buf.data_size = 0;
        print_log("Bypass ACF process\n");
        return NO_ERROR;
    }
#endif

#if !defined(ENABLE_HEADPHONE_COMPENSATION_FILTER)
    if ((convert_filter_type(lib_handler->mFilterType, lib_handler->mScenario))
        == AUDIO_CMP_FLT_LOUDNESS_COMP_HEADPHONE) {
        // bypass the process
        memcpy(p_dl_buf_out->data_buf.p_buffer, p_dl_buf_in->data_buf.p_buffer,
               p_dl_buf_in->data_buf.data_size);
        p_dl_buf_out->data_buf.data_size = p_dl_buf_in->data_buf.data_size;
        p_dl_buf_in->data_buf.data_size = 0;
        print_log("Bypass HCF process\n");
        return NO_ERROR;
    }
#endif


    uint32_t block_size, block_size_byte, offset_bit, loop_cnt, i;
    uint32_t ConsumedSampleCount = 0, TotalConsumedSample = 0, TotalOuputSample = 0;


    //Simplify handle (BLOCK_SIZE x N) Samples
    if (lib_handler->mInitParam.PCM_Format == BLOUDHD_IN_Q1P15_OUT_Q1P15) { // 16 bits
        // 2-byte, mono
        if (lib_handler->mInitParam.Channel == 1) {
            offset_bit = 1;
        }// 2-byte, L/R
        else {
            offset_bit = 2;
        }
    } else { //32 bits
        if (lib_handler->mInitParam.Channel == 1) {
            // 4-byte, Mono
            offset_bit = 2;
        } else {
            // 4-byte, L/R
            offset_bit = 3;
        }
    }
    if(lib_handler->mScenario == AUDIO_OUTPUT_FLAG_FAST) {
        block_size = BLOCK_SIZE_LOW_LATENCY;
    }
    else {
        block_size = BLOCK_SIZE;
    }
    block_size_byte = block_size * (1 << offset_bit);
    loop_cnt = (p_dl_buf_in->data_buf.data_size) / block_size_byte;

    print_log("usr id = %u block_size_byte = %d,PCM = %d, CH = %d filtertype = %d,in data_size = %d",
              lib_handler->id, block_size_byte,
              lib_handler->mInitParam.PCM_Format, lib_handler->mInitParam.Channel, lib_handler->mFilterType, p_dl_buf_in->data_buf.data_size);

    for (i = 0; i < loop_cnt; i++) {
        ConsumedSampleCount = block_size_byte;
        p_dl_buf_out->data_buf.data_size = block_size_byte;

        if ((p_dl_buf_in->data_buf.data_size - TotalConsumedSample) <
            ConsumedSampleCount) {
            print_log("Warning for remained input [%d] < output [%d], and skip process",
                      p_dl_buf_in->data_buf.data_size, ConsumedSampleCount);
            break;
        }


        result = lib_handler->mBloudHandle.Process(&lib_handler->mBloudHandle,
                                                   (char *)lib_handler->mpTempBuf,
                                                   (int *)((char *)p_dl_buf_in->data_buf.p_buffer + TotalConsumedSample),
                                                   (int *)&ConsumedSampleCount,
                                                   (int *)((char *)p_dl_buf_out->data_buf.p_buffer + TotalOuputSample),
                                                   (int *)&p_dl_buf_out->data_buf.data_size);

        TotalConsumedSample += ConsumedSampleCount;
        TotalOuputSample += p_dl_buf_out->data_buf.data_size;
        print_log("[loop: %d] id = %u ConsumedSampleCount = %d", i,
                  lib_handler->id,
                  ConsumedSampleCount);
    }
    p_dl_buf_out->data_buf.data_size = TotalOuputSample;
    p_dl_buf_in->data_buf.data_size -= TotalConsumedSample;  //remained input

    /* Liang: check xml param change here */
    if (getCallbackFilter(lib_handler->mFilterType)) {
        string_buf_t keyValue;
        char updatestr[50];
        keyValue.p_string = updatestr;
        snprintf(keyValue.p_string, sizeof(updatestr), "UpdateACFHCFParameters=%u", lib_handler->mFilterType);
        print_log("%s Param change! %s", __FUNCTION__, keyValue.p_string);
        audioloud_arsi_set_key_value_pair(&keyValue, lib_handler);
        resetCallbackFilter();
    }

    print_log("- DL raw: result = %d in->data_buf.p_buffer = %p in->data_buf.data_size = %d, out->data_buf.p_buffer = %p, out->data_buf.data_size = %d\n",
              result,
              p_dl_buf_in->data_buf.p_buffer,
              p_dl_buf_in->data_buf.data_size,
              p_dl_buf_out->data_buf.p_buffer,
              p_dl_buf_out->data_buf.data_size
             );
    return result;
}


status_t audioloud_arsi_reset_handler(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const data_buf_t         *p_param_buf,
    void                     *p_handler) {

    audioloud_lib_handle_t *lib_handler = NULL;

    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        p_param_buf == NULL ||
        p_handler == NULL) {
        return BAD_VALUE;
    }


    lib_handler = (audioloud_lib_handle_t *)p_handler;

    BLOUD_HD_RuntimeParam runtime_param;
    runtime_param.Command = BLOUD_HD_RESET;
    lib_handler->mBloudHandle.SetParameters(&lib_handler->mBloudHandle,
                                            &runtime_param);

    print_log("%s(), done\n", __func__);

    return NO_ERROR;
}


status_t audioloud_arsi_destroy_handler(void *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    print_log("%s(), p_handler = %p\n", __func__, p_handler);

    lib_handler = (audioloud_lib_handle_t *)p_handler;
    lib_release_varlist(lib_handler);

    return NO_ERROR;
}


status_t audioloud_arsi_update_device(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const data_buf_t         *p_param_buf,
    void                     *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;

    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        p_param_buf == NULL ||
        p_handler == NULL) {
        return NO_INIT;
    }
    // should not happened? should reset buffer and re-open again?
    bool needupdate  = false;
    AudioComFltMode_t AudioMode;
    AudioCompFltType_t filtertype;
    uint32_t pcm_format;
    uint32_t num_users;

    lib_handler = (audioloud_lib_handle_t *)p_handler;

    audio_devices_t input_device_new =
        p_arsi_task_config->input_device_info.devices;

    audio_devices_t output_devices_new =
        p_arsi_task_config->output_device_info.devices;

    if (p_arsi_lib_config->p_dl_buf_in->audio_format == AUDIO_FORMAT_PCM_8_24_BIT ||
        p_arsi_lib_config->p_dl_buf_in->audio_format == AUDIO_FORMAT_PCM_32_BIT) {
        pcm_format = BLOUDHD_IN_Q1P31_OUT_Q1P31;
    } else {
        pcm_format = BLOUDHD_IN_Q1P15_OUT_Q1P15;
    }

    print_log("[arsi_update_device] input device: new => 0x%x, old => 0x%x\n",
              input_device_new, mIputDeviceOld);
    print_log("[arsi_update_device] output device: new => 0x%x, old => 0x%x\n",
              output_devices_new, mOutputDeviceOld);

    if (mOutputDeviceOld != output_devices_new) {
        mOutputDeviceOld = output_devices_new;
        needupdate = true;
    }

    return NO_ERROR;
}


status_t audioloud_arsi_update_param(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const data_buf_t         *p_param_buf,
    void                     *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;
    acfhcf_buffer_t *lib_param_buf = NULL;

    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        p_param_buf == NULL  ||
        p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (audioloud_lib_handle_t *)p_handler;
    lib_param_buf = (acfhcf_buffer_t *)p_param_buf->p_buffer;
    lib_update_filterparam(lib_handler, &lib_param_buf->sACFHCFParam,
                           lib_param_buf->enhancemode, true);
    return NO_ERROR;
}


status_t audioloud_arsi_query_param_buf_size(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const string_buf_t       *product_name,
    const string_buf_t       *param_file_path,
    const int32_t             enhancement_mode,
    uint32_t                 *p_param_buf_size,
    const debug_log_fp_t      debug_log_fp) {
    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        product_name == NULL ||
        param_file_path == NULL ||
        p_param_buf_size == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

    AudioComFltMode_t AudioMode;
    AudioCompFltType_t filtertype;

    filtertype = get_filter_bydevice(p_arsi_task_config);

    lib_param_init(filtertype);

    *p_param_buf_size = sizeof(AUDIO_ACF_CUSTOM_PARAM_STRUCT);

    debug_log_fp("%s(), get param buf size %u filtertype = %d enhancement_mode = %d\n",
                 __func__, *p_param_buf_size, filtertype, enhancement_mode);

    return NO_ERROR;
}


status_t audioloud_arsi_parsing_param_file(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const string_buf_t       *product_name,
    const string_buf_t       *param_file_path,
    const int32_t             enhancement_mode,
    data_buf_t               *p_param_buf,
    const debug_log_fp_t      debug_log_fp) {

    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        product_name == NULL ||
        param_file_path == NULL ||
        p_param_buf == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

    //call xml flow
    if (enhancement_mode >= AUDIO_COMP_FLT_NUM) {
        return BAD_VALUE;
    }

    return NO_ERROR;
}


status_t audioloud_arsi_set_addr_value(
    const uint32_t addr,
    const uint32_t value,
    void          *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (audioloud_lib_handle_t *)p_handler;

    print_log("%s(), set value 0x%x at addr 0x%x\n",
              __func__, value, addr);


    return NO_ERROR;
}


status_t audioloud_arsi_get_addr_value(
    const uint32_t addr,
    uint32_t      *p_value,
    void          *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;

    if (p_value == NULL || p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (audioloud_lib_handle_t *)p_handler;

    if (addr == 0x1234) {
        //  *p_value = lib_handler->value_at_addr_0x1234;
    }

    // print_log("%s(), value 0x%x at addr 0x%x\n",
    //                        __func__, *p_value, addr);
    return NO_ERROR;
}


status_t audioloud_arsi_set_key_value_pair(
    const string_buf_t *key_value_pair,
    void               *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;

    if (key_value_pair == NULL || p_handler == NULL) {
        return NO_INIT;
    }

    int ret = NO_INIT;
    int num_users = 0;
    char *strval = NULL;

    lib_handler = (audioloud_lib_handle_t *)p_handler;

    if (strchr(key_value_pair->p_string, '=')) {  // this line contain '=' character
        strval = strtok(key_value_pair->p_string, "=");
        strval = strtok(NULL, "=");
        print_log(" %s(), key value pair = %s value = %d\n",
                  __func__, key_value_pair->p_string, atoi(strval));
    } else {
        return BAD_VALUE;
    }

#if defined(MTK_BESLOUDNESS_RUN_WITH_HAL)
    if (strstr(key_value_pair->p_string, "SetBesLoudnessStatus") != NULL &&
        lib_handler->mFilterType != AUDIO_COMP_FLT_HEADPHONE) {
        AudioComFltMode_t AudioFltMode;
        if (atoi(strval) == 0) {   //set process to bypass
            AudioFltMode = convert_filter_type(AUDIO_COMP_FLT_AUDIO, lib_handler->mScenario);
            set_lib_parameter(BLOUD_PAR_SET_WORK_MODE, (void *)(long)AudioFltMode, lib_handler);
            lib_update_filterparam(lib_handler, NULL, AUDIO_COMP_FLT_AUDIO, false);
        } else if (atoi(strval) == 1) {
            AudioFltMode = convert_filter_type(lib_handler->mOriFilterType, lib_handler->mScenario);
            set_lib_parameter(BLOUD_PAR_SET_WORK_MODE, (void *)(long)AudioFltMode, lib_handler);
#if defined(ENABLE_SWIP_ST2MO_SPEAKER)
            set_lib_parameter(BLOUD_PAR_SET_STEREO_TO_MONO_MODE, (void *)(long)BLOUD_S2M_MODE_ST2MO2ST, lib_handler);
#endif
            lib_update_filterparam(lib_handler, NULL, lib_handler->mOriFilterType, false);

        }
        ret = set_lib_parameter(BLOUD_PAR_SET_UPDATE_PARAM_TO_SWIP, (void *)(long)strval, lib_handler);
    }
#endif
    if (strstr(key_value_pair->p_string,
               "UpdateACFHCFParameters") != NULL) {
        AUDIO_ACF_CUSTOM_PARAM_STRUCT *sACFHCFParam = (AUDIO_ACF_CUSTOM_PARAM_STRUCT *)
                                                      malloc(sizeof(AUDIO_ACF_CUSTOM_PARAM_STRUCT));
        if (atoi(strval) == AUDIO_COMP_FLT_AUDIO && lib_handler->mFilterType == AUDIO_COMP_FLT_AUDIO) {
            getAudioCompFltCustParamFromStorage(AUDIO_COMP_FLT_AUDIO, sACFHCFParam);
        } else if (atoi(strval) == AUDIO_COMP_FLT_HEADPHONE &&
                   lib_handler->mFilterType == AUDIO_COMP_FLT_HEADPHONE) {
            getAudioCompFltCustParamFromStorage(AUDIO_COMP_FLT_HEADPHONE, sACFHCFParam);
        } else if (atoi(strval) == AUDIO_COMP_FLT_HCF_FOR_USB &&
                   lib_handler->mFilterType == AUDIO_COMP_FLT_HCF_FOR_USB) {
            getAudioCompFltCustParamFromStorage(AUDIO_COMP_FLT_HCF_FOR_USB, sACFHCFParam);
        } else if (atoi(strval) == AUDIO_COMP_FLT_DRC_FOR_MUSIC &&
                   lib_handler->mFilterType == AUDIO_COMP_FLT_DRC_FOR_MUSIC) {
            getAudioCompFltCustParamFromStorage(AUDIO_COMP_FLT_DRC_FOR_MUSIC, sACFHCFParam);
        } else {
            print_log("UpdateACFHCFParameters not match with current Filter\n");
            free(sACFHCFParam);
            return BAD_VALUE;
        }
        /* Update Stored xml param*/
        ret = lib_update_filterparam(lib_handler, sACFHCFParam, lib_handler->mFilterType, true);
        if (ret == NO_ERROR) {
            ret = set_lib_parameter(BLOUD_PAR_SET_UPDATE_PARAM_TO_SWIP, (void *)(long)strval, lib_handler);
        }
        free(sACFHCFParam);
    } else if (strstr(key_value_pair->p_string,
                      "BLOUD_PAR_SET_STEREO_TO_MONO_MODE") != NULL) {
        ret = set_lib_parameter(BLOUD_PAR_SET_STEREO_TO_MONO_MODE, (void *)(long)strval, lib_handler);
    } else if (strstr(key_value_pair->p_string,
                      "BLOUD_PAR_SET_UPDATE_PARAM_TO_SWIP") != NULL) {
        ret = set_lib_parameter(BLOUD_PAR_SET_UPDATE_PARAM_TO_SWIP, (void *)(long)strval, lib_handler);
    } else if (strstr(key_value_pair->p_string,
                      "BLOUD_PAR_SET_CHANGE_TO_BYPASS") != NULL) {
        ret = set_lib_parameter(BLOUD_PAR_SET_CHANGE_TO_BYPASS, (void *)(long)atoi(strval), lib_handler);
    } else if (strstr(key_value_pair->p_string,
                      "BLOUD_PAR_SET_CHANGE_TO_NORMAL") != NULL) {
        ret = set_lib_parameter(BLOUD_PAR_SET_CHANGE_TO_NORMAL, (void *)(long)atoi(strval), lib_handler);
    }
    if (ret != 0) {
        ret = UNKNOWN_ERROR;
    } else {
        ret = NO_ERROR;
    }

    print_log("- %s(), ret = %d\n", __func__, ret);

    return ret;
}

status_t audioloud_arsi_get_key_value_pair(
    string_buf_t *key_value_pair,
    void         *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;

    if (key_value_pair == NULL || p_handler == NULL) {
        return NO_INIT;
    }
    lib_handler = (audioloud_lib_handle_t *)p_handler;
    print_log("+%s(), key = %s\n",
              __func__, key_value_pair->p_string);

    if (strcmp(key_value_pair->p_string, "GetStatus") == 0) {
        BLOUD_HD_RuntimeStatus run_stat;
        memset(&run_stat, 0, sizeof(BLOUD_HD_RuntimeStatus));
        lib_handler->mBloudHandle.GetStatus(&lib_handler->mBloudHandle, &run_stat);
        if (run_stat.State == BLOUD_HD_SWITCHING_STATE) {
            sprintf(key_value_pair->p_string, "%s", "BLOUD_HD_SWITCHING_STATE");
        } else if (run_stat.State == BLOUD_HD_NORMAL_STATE) {
            sprintf(key_value_pair->p_string, "%s", "BLOUD_HD_NORMAL_STATE");
        }
    }
    return NO_ERROR;
}


status_t audioloud_arsi_set_ul_digital_gain(
    const int16_t ul_analog_gain_ref_only __unused,
    const int16_t ul_digital_gain __unused,
    void         *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (audioloud_lib_handle_t *)p_handler;
    return NO_ERROR;
}


status_t audioloud_arsi_set_dl_digital_gain(
    const int16_t dl_analog_gain_ref_only __unused,
    const int16_t dl_digital_gain __unused,
    void         *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (audioloud_lib_handle_t *)p_handler;
    return NO_ERROR;
}


status_t audioloud_arsi_set_ul_mute(const uint8_t b_mute_on __unused, void *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (audioloud_lib_handle_t *)p_handler;
    return NO_ERROR;
}


status_t audioloud_arsi_set_dl_mute(const uint8_t b_mute_on, void *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (audioloud_lib_handle_t *)p_handler;

    print_log("%s b_mute_on = %d\n", __func__, b_mute_on);
    return NO_ERROR;
}


status_t audioloud_arsi_set_ul_enhance(const uint8_t b_enhance_on __unused,
                                       void *p_handler) {
    if (p_handler == NULL) {
        return NO_INIT;
    }
    return NO_ERROR;
}


status_t audioloud_arsi_set_dl_enhance(const uint8_t b_enhance_on,
                                       void *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;
    int ret = 0, num_users;
    bool change = false;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (audioloud_lib_handle_t *)p_handler;

    print_log("pUser->id = %u b_enhance_on = %d\n", lib_handler->id ,
              b_enhance_on);

    if (lib_handler->bIsEnhaceOn != b_enhance_on) {
        lib_handler->bIsEnhaceOn  = b_enhance_on;
        change = true;
        print_log("Change! pUser->id = %u\n", lib_handler->id);
    }
    if (change) {
        if (!b_enhance_on) {
            ret = set_lib_parameter(BLOUD_PAR_SET_CHANGE_TO_BYPASS, (void *)(long)b_enhance_on, lib_handler);
        } else {
            ret = set_lib_parameter(BLOUD_PAR_SET_CHANGE_TO_NORMAL, (void *)(long)b_enhance_on, lib_handler);
        }

        if (ret != 0) {
            ret = UNKNOWN_ERROR;
        } else {
            ret = NO_ERROR;
        }
    }
    return ret;
}


status_t audioloud_arsi_set_debug_log_fp(const debug_log_fp_t debug_log,
                                         void *p_handler) {
    audioloud_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (audioloud_lib_handle_t *)p_handler;
    print_log = debug_log;

    return NO_ERROR;
}

status_t audioloud_arsi_get_lib_version(string_buf_t *version_buf) {

    /* for callback query buffer API */
    BS_HD_Handle mBloudHandle;
    BLOUD_HD_SetHandle(&mBloudHandle);
    BS_HD_EngineInfo Eng_Info;
    mBloudHandle.GetEngineInfo(&Eng_Info);
    sprintf(version_buf->p_string, "%d", Eng_Info.Version);

    return NO_ERROR;

}


void dynamic_link_arsi_assign_lib_fp(AurisysLibInterface *lib) {
    lib->arsi_get_lib_version = audioloud_arsi_get_lib_version;
    lib->arsi_query_working_buf_size = audioloud_arsi_query_working_buf_size;
    lib->arsi_create_handler = audioloud_arsi_create_handler;
    lib->arsi_process_ul_buf = audioloud_arsi_process_ul_buf;
    lib->arsi_process_dl_buf = audioloud_arsi_process_dl_buf;
    lib->arsi_reset_handler = audioloud_arsi_reset_handler;
    lib->arsi_destroy_handler = audioloud_arsi_destroy_handler;
    lib->arsi_update_device = audioloud_arsi_update_device;
    lib->arsi_update_param = audioloud_arsi_update_param;
    lib->arsi_query_param_buf_size = audioloud_arsi_query_param_buf_size;
    lib->arsi_parsing_param_file = audioloud_arsi_parsing_param_file;
    lib->arsi_set_addr_value = audioloud_arsi_set_addr_value;
    lib->arsi_get_addr_value = audioloud_arsi_get_addr_value;
    lib->arsi_set_key_value_pair = audioloud_arsi_set_key_value_pair;
    lib->arsi_get_key_value_pair = audioloud_arsi_get_key_value_pair;
    lib->arsi_set_ul_digital_gain = audioloud_arsi_set_ul_digital_gain;
    lib->arsi_set_dl_digital_gain = audioloud_arsi_set_dl_digital_gain;
    lib->arsi_set_ul_mute = audioloud_arsi_set_ul_mute;
    lib->arsi_set_dl_mute = audioloud_arsi_set_dl_mute;
    lib->arsi_set_ul_enhance = audioloud_arsi_set_ul_enhance;
    lib->arsi_set_dl_enhance = audioloud_arsi_set_dl_enhance;
    lib->arsi_set_debug_log_fp = audioloud_arsi_set_debug_log_fp;
    lib_load_xmlparam(); //load all enhancement param files
    audioComFltCustParamInit(); /* register xml change callback*/
}

