package com.mediatek.sensorhub.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEventListener;
import android.hardware.TriggerEvent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;

import com.mediatek.sensorhub.ui.R;
import com.mediatek.sensorhub.settings.Utils;
import com.mediatek.sensorhub.stresstest.SensorStressTestActivity;

import java.util.Arrays;
import java.util.HashMap;

public class SensorEventListenerService extends Service implements SensorEventListener {
    private static final String TAG = "SensorEventListenerService";
    private SensorManager mSensorManager;
    private TriggerListener mTriggerListener = new TriggerListener();
    private HashMap<Integer, Sensor> mSensorKeyMap = new HashMap<Integer, Sensor>();
    // record log
    private String mRecordStr;

    // Play notify @{
    private SoundPool mSounds;
    private int mSoundIdF;
    private int mSoundIdT;
    private int mSoundStreamId;
    private AudioManager mAudioManager;
    private int mUiSoundsStreamType;
    // @}
    // for UI update
    private MainHandler mMainHandler;
    // for background sensor change data handle
    private BackgroundHandler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    private static final String BUNDLE_KEY_LISTENER = "listener_key";
    private static final String BUNDLE_KEY_SENSOR_VALUE = "sensor_value";
    private static final String BUNDLE_KEY_SENSOR_TYPE = "sensor_type";

    // Add for event record string OOM
    private int mCounter;

    private class BackgroundHandler extends Handler {
        static final int MSG_ON_SENSOR_CHANGE = 1;
        static final int MSG_ON_ACCURACY_CHANGE = 2;

        BackgroundHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_ON_SENSOR_CHANGE: {
                SensorEvent sensorEvent = (SensorEvent) msg.obj;
                int sensorType = sensorEvent.sensor.getType();
                OnSensorChangedListener listener = null;
                String sensorTypeKeyStr = null;
                switch (sensorType) {
                case Sensor.TYPE_PEDOMETER:
                    sensorTypeKeyStr = Utils.KEY_PEDOMETER_STATUS;
                    break;
                case Sensor.TYPE_ACTIVITY:
                    sensorTypeKeyStr = Utils.KEY_ACTIVITY_STATUS;
                    break;
                case Sensor.TYPE_PDR:
                    sensorTypeKeyStr = Utils.KEY_PDR_STATUS;
                    break;
                case Sensor.TYPE_ACCELEROMETER:
                    sensorTypeKeyStr = Utils.KEY_ACCELEROMETER_STATUS;
                    break;
                case Sensor.TYPE_LIGHT:
                    sensorTypeKeyStr = Utils.KEY_LIGHT_STATUS;
                    break;
                case Sensor.TYPE_PROXIMITY:
                    sensorTypeKeyStr = Utils.KEY_PROXIMITY_STATUS;
                    break;
                case Sensor.TYPE_PRESSURE:
                    sensorTypeKeyStr = Utils.KEY_PRESSURE_STATUS;
                    break;
                case Sensor.TYPE_STEP_COUNTER:
                    sensorTypeKeyStr = Utils.KEY_STEP_COUNTER_STATUS;
                    Utils.setSensorValues(Utils.KEY_TOTAL_STEP_COUNTER, sensorEvent.values[0]);
                    break;
                case Sensor.TYPE_STEP_DETECTOR:
                    sensorTypeKeyStr = Utils.KEY_STEP_DETECTOR_STATUS;
                    break;
                case Sensor.TYPE_GYROSCOPE:
                    sensorTypeKeyStr = Utils.KEY_GYROSCOPE_STATUS;
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    sensorTypeKeyStr = Utils.KEY_MAGNETIC_FIELD_STATUS;
                    break;
                case Sensor.TYPE_ORIENTATION:
                    sensorTypeKeyStr = Utils.KEY_ORIENTATION_STATUS;
                    break;
                case Sensor.TYPE_GAME_ROTATION_VECTOR:
                    sensorTypeKeyStr = Utils.KEY_GAME_ROTATION_VECTOR_STATUS;
                    break;
                case Sensor.TYPE_DEVICE_ORIENTATION:
                    sensorTypeKeyStr = Utils.KEY_DEVICE_ORIENTATION_STATUS;
                    break;
                case Sensor.TYPE_FLOOR_COUNT:
                    sensorTypeKeyStr = Utils.KEY_FLOOR_COUNTER_STATUS;
                    Utils.setSensorValues(Utils.KEY_TOTAL_FLOOR_COUNT, sensorEvent.values[0]);
                    break;
                default:
                    break;
                }
                if (sensorTypeKeyStr != null) {
                    listener = onSensorChangedListenerMap.get(sensorTypeKeyStr);
                    if (listener != null) {
                        Bundle bundle = new Bundle();
                        bundle.putFloatArray(BUNDLE_KEY_SENSOR_VALUE, sensorEvent.values);
                        bundle.putString(BUNDLE_KEY_LISTENER, sensorTypeKeyStr);
                        //if (!mMainHandler.hasMessages(MainHandler.MSG_COMMON_SENSOR_CHANGE,
                          //      bundle)) {
                            Message message = mMainHandler.obtainMessage(
                                    MainHandler.MSG_COMMON_SENSOR_CHANGE, bundle);
                            mMainHandler.sendMessage(message);
                        //}
                    }
                }
                // Composite sensors
                if (Utils.isCompositeSesnor(sensorType)) {
                    listener = onSensorChangedListenerMap.get(Utils.KEY_COMPOSITE_STATUS);
                    if (listener != null) {
                        Bundle bundle = new Bundle();
                        bundle.putFloatArray(BUNDLE_KEY_SENSOR_VALUE, sensorEvent.values);
                        bundle.putString(BUNDLE_KEY_LISTENER, Utils.KEY_COMPOSITE_STATUS);
                        bundle.putInt(BUNDLE_KEY_SENSOR_TYPE, sensorType);
                        //if (!mMainHandler.hasMessages(MainHandler.MSG_COMPOSITE_CHANGE, bundle)) {
                            Message message = mMainHandler.obtainMessage(
                                    MainHandler.MSG_COMPOSITE_CHANGE, bundle);
                            mMainHandler.sendMessage(message);
                        //}
                    }
                }
            }
                break;
            case MSG_ON_ACCURACY_CHANGE:
                Log.d(TAG, "MSG_ON_ACCURACY_CHANGE");
                Bundle bundle = (Bundle) msg.obj;
                int sensorType = bundle.getInt(BUNDLE_KEY_SENSOR_TYPE);
                String sensorTypeKeyStr = null;
                OnSensorChangedListener listener = null;
                switch (sensorType) {
                case Sensor.TYPE_MAGNETIC_FIELD:
                    sensorTypeKeyStr = Utils.KEY_MAGNETIC_FIELD_STATUS;
                    break;
                case Sensor.TYPE_ORIENTATION:
                    sensorTypeKeyStr = Utils.KEY_ORIENTATION_STATUS;
                    break;
                }
                Log.d(TAG, "MSG_ON_ACCURACY_CHANGE : " + sensorTypeKeyStr);

                if (sensorTypeKeyStr != null) {
                    listener = onSensorChangedListenerMap.get(sensorTypeKeyStr);
                    if (listener != null) {
                        Bundle bundleSent = new Bundle();
                        bundleSent.putInt(BUNDLE_KEY_SENSOR_VALUE, bundle
                                .getInt(BUNDLE_KEY_SENSOR_VALUE));
                        bundleSent.putString(BUNDLE_KEY_LISTENER, sensorTypeKeyStr);
//                        if (!mMainHandler.hasMessages(MainHandler.MSG_ACCURACY_CHANGE
//                                , bundleSent)) {
                            Message message = mMainHandler.obtainMessage(
                                    MainHandler.MSG_ACCURACY_CHANGE, bundleSent);
                            mMainHandler.sendMessage(message);
                            Log.d(TAG, "MSG_ON_ACCURACY_CHANGE : sendMessage");
//                        }
                    }
                }
                break;
            }
        }
    }

    class MainHandler extends Handler {
        static final int MSG_COMPOSITE_CHANGE = 1;
        static final int MSG_COMMON_SENSOR_CHANGE = 2;
        static final int MSG_ACCURACY_CHANGE = 3;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_COMMON_SENSOR_CHANGE: {
                Bundle bundle = (Bundle) msg.obj;
                onSensorChangedListenerMap.get(bundle.getString(BUNDLE_KEY_LISTENER))
                        .onSensorChanged(bundle.getFloatArray(BUNDLE_KEY_SENSOR_VALUE));
            }
                break;
            case MSG_COMPOSITE_CHANGE: {
                Bundle bundle = (Bundle) msg.obj;
                onSensorChangedListenerMap.get(bundle.getString(BUNDLE_KEY_LISTENER))
                        .onSensorChanged(bundle.getInt(BUNDLE_KEY_SENSOR_TYPE),
                                bundle.getFloatArray(BUNDLE_KEY_SENSOR_VALUE));
            }
                break;
            case MSG_ACCURACY_CHANGE: {
                Log.d(TAG, "MSG_ON_ACCURACY_CHANGE : sendMessage");
                Bundle bundle = (Bundle) msg.obj;
                onSensorChangedListenerMap.get(bundle.getString(BUNDLE_KEY_LISTENER))
                        .onAccuracyChanged(bundle.getInt(BUNDLE_KEY_SENSOR_VALUE));
            }
                break;
            }
        }
    }

    private final IBinder mBinder = new LocalBinder();

    public SensorEventListenerService() {

    }

    public interface OnSensorChangedListener {
        public void onSensorChanged(float[] value);

        public void onSensorChanged(int sensorType, float[] value);

        public void onSensorChanged(int sensorType, float[] value, long time);

        public void onAccuracyChanged(int accuracy);

        public void onAccuracyChanged(int sensorType, int accuracy);
    }

    private HashMap<String, OnSensorChangedListener> onSensorChangedListenerMap =
        new HashMap<String, OnSensorChangedListener>();

    public void putOnSensorChangedListener(String key, OnSensorChangedListener listener) {
        onSensorChangedListenerMap.put(key, listener);
    }

    public void removeOnSensorChangedListener(String key) {
        onSensorChangedListenerMap.remove(key);
    }

    /**
     * Class used for the client Binder. Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public SensorEventListenerService getService(String key, OnSensorChangedListener listener) {
            putOnSensorChangedListener(key, listener);
            // Return this instance of SensorEventListenerService so clients can
            // call public methods
            return SensorEventListenerService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "onReceive: " + action);
            if (Intent.ACTION_SHUTDOWN.equals(action)) {
                stopSelf();
            } else if (Intent.ACTION_SCREEN_ON.equals(action)) {
                // when screen on, flush data
                flush();
            }
        }
    };

    private void flush() {
        if (Utils.getSensorStatus(Utils.KEY_ACTIVITY_STATUS)) {
            Log.d(TAG, "when screen on, register activity use normal mode");
            mSensorManager.flush(this);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        mSensorManager = Utils.getSensorManager();
        mSensorKeyMap = Utils.getSensorKeyMap();

        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mSounds = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 0);
        mSoundIdF = mSounds.load(this, R.raw.in_pocket, 0);
        mSoundIdT = mSounds.load(this, R.raw.non_in_pocket, 0);

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SHUTDOWN);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mReceiver, filter);

        mBackgroundThread = new HandlerThread("SensorEventListenerService.changed",
                Process.THREAD_PRIORITY_BACKGROUND);
        mBackgroundThread.start();
        mMainHandler = new MainHandler();
        mBackgroundHandler = new BackgroundHandler(mBackgroundThread.getLooper());
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        unRegisterAllSensors();
        Utils.restoreStatusToDefult();
        recordLogs(false);
        unregisterReceiver(mReceiver);
        mBackgroundHandler.removeMessages(BackgroundHandler.MSG_ON_SENSOR_CHANGE);
        mBackgroundHandler.removeMessages(BackgroundHandler.MSG_ON_ACCURACY_CHANGE);
        mMainHandler.removeMessages(MainHandler.MSG_COMMON_SENSOR_CHANGE);
        mMainHandler.removeMessages(MainHandler.MSG_COMPOSITE_CHANGE);
        mMainHandler.removeMessages(MainHandler.MSG_ACCURACY_CHANGE);
        if (mBackgroundThread != null) {
            mBackgroundThread.quit();
        }
        super.onDestroy();
    }

    // Register or unRegister sensor
    public void registerSensor(String prefKey, boolean isRegister) {
        Log.d(TAG, "registerSensor " + prefKey + " status " + isRegister);
        // Preference key is the sensor type
        int sensorType = Integer.valueOf(prefKey);
        Sensor sensor = mSensorKeyMap.get(sensorType);
        if (Utils.containsSensor(Utils.triggerSensorType, sensorType)) {
            if (isRegister) {
                mSensorManager.requestTriggerSensor(mTriggerListener, sensor);
            } else {
                mSensorManager.cancelTriggerSensor(mTriggerListener, sensor);
            }
        } else {
            if (isRegister) {
                if (sensorType == Sensor.TYPE_ACTIVITY) {
                    mSensorManager.registerListener(this, sensor, 1000000, 600000000); // 1s 10min
                } else {
                    mSensorManager
                            .registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
                }
            } else {
                mSensorManager.unregisterListener(this, sensor);
            }
        }
    }

    public void registerSensorWithCustomeTime(String prefKey, boolean isRegister, int rate,
            int latency) {
        Log.d(TAG, "registerSensorWithCustomeTime " + prefKey + " status " + isRegister + " rate "
                + rate + " latency " + latency);
        // Preference key is the sensor type
        int sensorType = Integer.valueOf(prefKey);
        Sensor sensor = mSensorKeyMap.get(sensorType);
        if (isRegister) {
            mSensorManager.registerListener(this, sensor, rate, latency);
        } else {
            mSensorManager.unregisterListener(this, sensor);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d(TAG, "onAccuracyChanged " + sensor.getName());
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_KEY_SENSOR_VALUE, accuracy);
        bundle.putInt(BUNDLE_KEY_SENSOR_TYPE, sensor.getType());
//        if (!mBackgroundHandler.hasMessages(BackgroundHandler.MSG_ON_ACCURACY_CHANGE, bundle)) {
            Message msg = mBackgroundHandler.obtainMessage(
                    BackgroundHandler.MSG_ON_ACCURACY_CHANGE, bundle);
            mBackgroundHandler.sendMessage(msg);
//        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // For record log
        if (Utils.getSensorStatus(Utils.LOG_STATUS)) {
            mCounter++;
            if (mCounter == 600) { // 1s a event, record once every ten minutes
                mCounter = 0;
                recordLogs(false);
            }
            String pRecordStr = event.sensor.getName() + "@" + event.timestamp
                    + Arrays.toString(event.values);
            mRecordStr = mRecordStr + "\n" + pRecordStr;
        }

//        if (!mBackgroundHandler.hasMessages(BackgroundHandler.MSG_ON_SENSOR_CHANGE, event)) {
            Message msg = mBackgroundHandler.obtainMessage(BackgroundHandler.MSG_ON_SENSOR_CHANGE,
                    event);
            mBackgroundHandler.sendMessage(msg);
//        }
    }

    class TriggerListener extends TriggerEventListener {
        public void onTrigger(TriggerEvent event) {
            String pRecordStr = event.sensor.getName() + "@" + event.timestamp
                    + Arrays.toString(event.values);
            mRecordStr = mRecordStr + "\n" + pRecordStr;
            int sensorType = event.sensor.getType();
            OnSensorChangedListener listener = null;
            String sensorTypeKeyStr = null;
            String sensorAutoEnabledKeyStr = null;
            switch (sensorType) {
            case Sensor.TYPE_SIGNIFICANT_MOTION:
                sensorTypeKeyStr = Utils.KEY_SMD_STATUS;
                sensorAutoEnabledKeyStr = Utils.KEY_SMD_AUTO_STATUS;
                notifyUser(Utils.KEY_SMD_NOTIFY_STATUS, event.values[0]);
                break;
            case Sensor.TYPE_ANSWER_CALL:
                sensorTypeKeyStr = Utils.KEY_ANSWER_CALL_STATUS;
                sensorAutoEnabledKeyStr = Utils.KEY_ANSWER_CALL_AUTO_STATUS;
                notifyUser(Utils.KEY_ANSWER_CALL_NOTIFY_STATUS, event.values[0]);
                break;
            case Sensor.TYPE_IN_POCKET:
                sensorTypeKeyStr = Utils.KEY_INPOCKET_STATUS;
                sensorAutoEnabledKeyStr = Utils.KEY_INPOCKET_AUTO_STATUS;
                notifyUser(Utils.KEY_INPOCKET_NOTIFY_STATUS, event.values[0]);
                break;
            case Sensor.TYPE_STATIONARY_DETECT:
                sensorTypeKeyStr = Utils.KEY_STATIONARY_STATUS;
                sensorAutoEnabledKeyStr = Utils.KEY_STATIONARY_AUTO_STATUS;
                notifyUser(Utils.KEY_STATIONARY_NOTIFY_STATUS, event.values[0]);
                break;
            case Sensor.TYPE_MOTION_DETECT:
                sensorTypeKeyStr = Utils.KEY_MOTION_DETECT_STATUS;
                sensorAutoEnabledKeyStr = Utils.KEY_MOTION_DETECT_AUTO_STATUS;
                notifyUser(Utils.KEY_MOTION_DETECT_NOTIFY_STATUS, event.values[0]);
                break;
            default:
                break;
            }
            // For others on-shot sensors
            if (sensorTypeKeyStr != null) {
                listener = onSensorChangedListenerMap.get(sensorTypeKeyStr);
                if (listener != null) {
                    listener.onSensorChanged(event.values);
                }
                if (sensorAutoEnabledKeyStr != null
                        && !Utils.getSensorStatus(sensorAutoEnabledKeyStr)) {
                    Utils.setSensorStatus(sensorTypeKeyStr, false);
                } else {
                    // auto enabled
                    if (Sensor.TYPE_SIGNIFICANT_MOTION == sensorType) {
                        try {
                            Thread.sleep(1000); // SMD sleep 1s
                        } catch (InterruptedException e) {
                            Log.d(TAG, "interrupt");
                        }
                    }
                    mSensorManager.requestTriggerSensor(mTriggerListener, event.sensor);
                }
            }
            // For all gesture sensors
            if (Utils.isOrginalGesture(sensorType)) {
                if (event.values[0] == 1) {
                    playSound(mSoundIdF);
                }
                String autoEnabled = getString(R.string.auto_enabled_title);
                if (Utils.getSensorStatus(sensorType + "_" + autoEnabled)) {
                    mSensorManager.requestTriggerSensor(mTriggerListener, event.sensor);
                } else {
                    Utils.setSensorStatus(String.valueOf(sensorType), false);
                }
            }
        }
    }

    // notify user when sensor event change
    private void notifyUser(String key, float value) {
        if (Utils.getSensorStatus(key)) {
            if (value == 1) {
                playSound(mSoundIdF);
            }
        }
    }

    private void playSound(int soundId) {
        mSounds.stop(mSoundStreamId);
        if (mAudioManager != null) {
            mUiSoundsStreamType = mAudioManager.getUiSoundsStreamType();
        }
        // If the stream is muted, don't play the sound
        if (mAudioManager.isStreamMute(mUiSoundsStreamType))
            return;
        mSoundStreamId = mSounds.play(soundId, 1, 1, 1/* priortiy */, 0/* loop */, 1.0f/* rate */);
    }

    private void unRegisterAllSensors() {
        mSensorManager.unregisterListener(this);
        mSensorManager.unregisterListener(mStressTestEventListener);
    }

    public void recordLogs(boolean isRecord) {
        if (isRecord) {
            Log.d(TAG, "sensor algorithm test start");
            Utils.initRecordFileName();
        } else {
            Log.d(TAG, "sensor algorithm test end");
            if (!TextUtils.isEmpty(mRecordStr)) {
                Utils.recordToSdcard(mRecordStr.getBytes());
            }
        }
        mRecordStr = "";
        return;
    }

    // Add for Stress test @{
    private SensorEventListener mStressTestEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            int sensorType = event.sensor.getType();
            onSensorChangedListenerMap.get(SensorStressTestActivity.TAG).onSensorChanged(sensorType,
                    event.values, event.timestamp);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            int sensorType = sensor.getType();
            onSensorChangedListenerMap.get(SensorStressTestActivity.TAG)
                .onAccuracyChanged(sensorType, accuracy);
        }
    };

    private TriggerEventListener mStressTestTriggerListener = new TriggerEventListener() {

        public void onTrigger(TriggerEvent event) {
            int sensorType = event.sensor.getType();
            onSensorChangedListenerMap.get(SensorStressTestActivity.TAG).onSensorChanged(sensorType,
                    event.values, event.timestamp);
        }
    };

    private final int[] SENSOR_DELAY_ARRAY = { SensorManager.SENSOR_DELAY_NORMAL,
            SensorManager.SENSOR_DELAY_GAME, SensorManager.SENSOR_DELAY_FASTEST,
            SensorManager.SENSOR_DELAY_UI };

    public void stressTestRegisterSensor(Sensor sensor, boolean isRegister, int rateIndex) {
        boolean isTriggerSensor = (sensor.getReportingMode() == Sensor.REPORTING_MODE_ONE_SHOT);
        if (isTriggerSensor) {
            if (isRegister) {
                mSensorManager.requestTriggerSensor(mStressTestTriggerListener, sensor);
            } else {
                mSensorManager.cancelTriggerSensor(mStressTestTriggerListener, sensor);
            }
        } else {
            if (isRegister) {
                mSensorManager.registerListener(mStressTestEventListener, sensor,
                        SENSOR_DELAY_ARRAY[rateIndex]);
            } else {
                mSensorManager.unregisterListener(mStressTestEventListener, sensor);
            }
        }
    }

    public void unRegisterStressTestSensor() {
        Log.d("SensorStressTestLog", "unRegisterStressTestSensor ");
        mSensorManager.unregisterListener(mStressTestEventListener);
    }
    // @}
}
