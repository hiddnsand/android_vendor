/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.power;

import android.app.Activity;
import android.app.TabActivity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TabHost.OnTabChangeListener;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.ShellExe;
import com.mediatek.engineermode.power.PMU6575.RunThread;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Wakelock Debug Log Activity.
 */
@SuppressWarnings("deprecation")
public class DebugLog extends TabActivity implements OnClickListener{
    private static final int TAB_DEBUGINFO = 1;
    private static final int TAB_DEBUGSEETING = 2;
    private static final String TAG = "EM/DebugLog";
    private final String mDebugInfo = "Debug info";
    private final String mDebugSetting = "Debug setting";
    private final String mFileName = "/sys/devices/platform/spm/debug_log";
    private boolean mDebugInfoRun = false;
    private static final int EVENT_UPDATE = 1;
    private static final int UPDATE_INTERVAL = 1500;
    private Toast mToast;
    private int mWhichTab = TAB_DEBUGINFO;
    private TabHost tabHost = null;
    private TextView mTextDebugInfo = null;
    private TextView mTextDebugSettingValue = null;
    private Button mButtonDebugSettingSet = null;
    private int mDebugNum = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Elog.d(TAG, "onCreate");

        tabHost = getTabHost();
        LayoutInflater.from(this).inflate(R.layout.debug_log,
                tabHost.getTabContentView(), true);
        tabHost.addTab(tabHost.newTabSpec(mDebugInfo).setIndicator(mDebugInfo)
                .setContent(R.id.Debug_info));
        tabHost.addTab(tabHost.newTabSpec(mDebugSetting).setIndicator(mDebugSetting)
                .setContent(R.id.Debug_setting));

        mTextDebugInfo = (TextView)findViewById(R.id.text_debug_log);
        mButtonDebugSettingSet = (Button)findViewById(R.id.Debug_setting_set);
        mTextDebugSettingValue = (TextView)findViewById(R.id.Debug_setting_value);
        mButtonDebugSettingSet.setOnClickListener(this);
        mDebugNum = 0;

        tabHost.setOnTabChangedListener(new OnTabChangeListener(){
            @Override
            public void onTabChanged(String tabId) {
                if (tabId.equals(mDebugInfo)) {
                    mWhichTab = TAB_DEBUGINFO;
                    if(mDebugInfoRun == false) {
                        mDebugInfoRun = true;
                        new RunThread().start();
                    }
                }
                if (tabId.equals(mDebugSetting)) {
                    mWhichTab = TAB_DEBUGSEETING;
                    if(mDebugInfoRun == true) {
                        mDebugInfoRun = false;
                    }
                }
            }
        });
    }

   private String getInfo(String cmd) {
       String result = null;
       try {
           int ret = ShellExe.execCommand(cmd);
           if (0 == ret) {
               result = ShellExe.getOutput();
           } else {
               result = "getInfo error";
           }
       } catch (IOException e) {
           Log.i("@M_" + TAG, e.toString());
           result = e.toString();
       }
       return result;
   }
    private void showToast(String msg) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        mToast.show();
    }
    private boolean checkInfo(String s) {
        if (s == null || s.length() < 1 || s.length() > 256 ) {
            return false;
        }
        return true;
    }
    public void onClick(View arg0) {
        if (arg0.getId() == mButtonDebugSettingSet.getId()) {
            String info = mTextDebugSettingValue.getText().toString();
            String cmd = "echo " + info + " > " + mFileName ;
            showToast(cmd);
            if (checkInfo(info)) {
                String result = getInfo(cmd);
                if (null != result && result.equals("getInfo error")) {
                    showToast("set value failed!");
                }else{
                    showToast("set value succeed!");
                }
            } else {
                showToast("Please check the info!");
            }
        }
    }

    private String getDebugInfo() {
        String cmd = "cat " + mFileName;
        return getInfo(cmd);
    }

    private Handler mUpdateHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case EVENT_UPDATE:
                Bundle b = msg.getData();
                mTextDebugInfo.setText(b.getString("INFO"));
//                showToast("mDebugNum = " + mDebugNum);
                break;
            default:
                break;
            }
        }
    };
    class RunThread extends Thread {

        public void run() {
            while (mDebugInfoRun) {
                Bundle b = new Bundle();
                b.putString("INFO", getDebugInfo());
                Message msg = new Message();
                msg.what = EVENT_UPDATE;
                msg.setData(b);
                mUpdateHandler.sendMessage(msg);
                try {
                    sleep(UPDATE_INTERVAL);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mDebugNum++;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mDebugInfoRun == true)
            mDebugInfoRun = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mDebugInfoRun == false){
            mDebugInfoRun = true;
            new RunThread().start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mDebugInfoRun == true)
            mDebugInfoRun = false;
    }
}
