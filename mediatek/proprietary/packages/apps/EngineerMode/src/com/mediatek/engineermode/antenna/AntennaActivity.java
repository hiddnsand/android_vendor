/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.antenna;

import android.app.Activity;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.ModemCategory;
import com.mediatek.engineermode.R;
import java.util.HashSet;

public class AntennaActivity extends Activity {
    private static final String TAG = "AntennaTest";

    private static final int MSG_QUERY_ANTENNA_MODE = 1;
    private static final int MSG_SET_ANTENNA_MODE = 2;
    private static final int MSG_QUERY_ANTENNA_MODE_C2K = 4;
    private static final int MODE_INDEX_BASE_3G = 10;

    private HashSet<Integer> mSupportModes = new HashSet<Integer>();
    private Phone mPhone = null;
    private Phone mCdmaPhone = null;
    private Spinner mSpinner4G = null;
    private Spinner mSpinner3G = null;
    private Spinner mSpinnerC2kMode = null;


    private Toast mToast = null;
    private int mCurrentPos = 0, mCurrent3GPos = 0, mCurrentPosCdma = 0;

    private final OnItemSelectedListener mItemSelectedListener = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {
            if (arg0 == mSpinner4G) {
                if (mCurrentPos == arg0.getSelectedItemPosition()) {
                    return;
                }
                mCurrentPos = arg0.getSelectedItemPosition();
                setMode(pos);
            } else if (arg0 == mSpinner3G) {
                if (mCurrent3GPos == arg0.getSelectedItemPosition()) {
                    return;
                }
                mCurrent3GPos = arg0.getSelectedItemPosition();
                setMode(MODE_INDEX_BASE_3G + pos - 1);
            } else if (arg0 == mSpinnerC2kMode) {
                if (mCurrentPosCdma == arg0.getSelectedItemPosition()) {
                    return;
                }
                mCurrentPosCdma = arg0.getSelectedItemPosition();
                setCdmaMode(pos);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // Do nothing
        }
    };

    private final Handler mCommandHander = new Handler() {
        @Override
        public void handleMessage(final Message msg) {
            AsyncResult asyncResult;
            switch (msg.what) {
            case MSG_QUERY_ANTENNA_MODE:
                asyncResult = (AsyncResult) msg.obj;
                if (asyncResult != null && asyncResult.exception == null
                        && asyncResult.result != null) {
                    final String[] result = (String[]) asyncResult.result;
                    for (int i = 0; i < result.length; i++) {
                        parseCurrentMode(result[i]);
                    }
                } else {
                    showToast("Query 3G/4G antenna currect mode failed.");
                    Log.e(TAG, "Query 3G/4G antenna currect mode failed.");
                }
                break;

            case MSG_QUERY_ANTENNA_MODE_C2K:
                asyncResult = (AsyncResult) msg.obj;
                if (asyncResult != null && asyncResult.exception == null
                        && asyncResult.result != null) {
                    final String[] result = (String[]) asyncResult.result;
                    for (int i = 0; i < result.length; i++) {
                        parseCurrentCdmaMode(result[i]);
                    }
                } else {
                    showToast("Query cdma antenna currect mode failed.");
                    Log.e(TAG, "Query cdma antenna currect mode failed.");
                }
                break;
            case MSG_SET_ANTENNA_MODE:
                asyncResult = (AsyncResult) msg.obj;
                if (asyncResult.exception == null) {
                    showToast("Set successful.");
                    Log.d(TAG, "set antenna mode succeed.");
                } else {
                    showToast("Set failed.");
                    Log.e(TAG, "set antenna mode failed.");
                }
                break;
            default:
                break;
            }
        }
    };

    private int parseCurrentMode(String data) {
        // query result is like: +ERXPATH: 1
        int mode = -1;
        Log.d("@M_" + TAG, "parseCurrentMode data= " + data);
        try {
            mode = Integer.valueOf(data.substring("+ERXPATH:".length()).trim());
        } catch (Exception e) {
            Log.e("@M_" + TAG, "Wrong current mode format: " + data);
        }

        if (mode < 0 ||
           (mode >= mSpinner4G.getCount() && mode >= MODE_INDEX_BASE_3G + mSpinner4G.getCount())) {
            showToast("Modem returned invalid mode: " + data);
            return -1;
        } else {
            if (mode >= MODE_INDEX_BASE_3G) {
                mCurrent3GPos = mode - MODE_INDEX_BASE_3G + 1;
                Log.d("@M_" + TAG, "parseCurrent3GMode is: " + mCurrent3GPos);
                mSpinner3G.setSelection(mode - MODE_INDEX_BASE_3G + 1);
            } else {
                Log.d("@M_" + TAG, "parseCurrentLteMode is: " + mode);
                mCurrentPos = mode;
                mSpinner4G.setSelection(mode);
                mSpinner4G.setEnabled(true);
            }
        }
        return mode;
    }

    private int parseCurrentCdmaMode(String data) {
        // query result is like: +ERXTESTMODE: 1
        int mode = -1;
        Log.d("@M_" + TAG, "parseCurrentCdmaMode data= " + data);
        try {
            mode = Integer.valueOf(data.substring("+ERXTESTMODE:".length()).trim());
        } catch (Exception e) {
            Log.e("@M_" + TAG, "Wrong current mode format: " + data);
        }

        if (mode < 0 ||
           (mode >= mSpinnerC2kMode.getCount())) {
            showToast("Modem returned invalid mode: " + data);
            return -1;
        } else {
            mCurrentPosCdma = mode;
            Log.d("@M_" + TAG, "parseCurrentCDMAMode is: " + mCurrentPosCdma);
            mSpinnerC2kMode.setSelection(mode);
            mSpinnerC2kMode.setEnabled(true);
        }
        return mode;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.antena_test);

        mSpinner4G = (Spinner) findViewById(R.id.spinner_antenna_4g);
        mSpinner4G.setOnItemSelectedListener(mItemSelectedListener);
        mSpinner4G.setEnabled(false);

        mSpinner3G = (Spinner) findViewById(R.id.spinner_antenna_3g);
        mSpinner3G.setOnItemSelectedListener(mItemSelectedListener);

        mSpinnerC2kMode = (Spinner) findViewById(R.id.spinner_antenna_c2k_mode);
        mSpinnerC2kMode.setOnItemSelectedListener(mItemSelectedListener);

        if (!FeatureSupport.isSupported(FeatureSupport.FK_LTE_SUPPORT)) {
            findViewById(R.id.antenna_title_4g).setVisibility(View.GONE);
            findViewById(R.id.antenna_hint).setVisibility(View.GONE);
            mSpinner4G.setVisibility(View.GONE);
        }

        if (ModemCategory.getModemType() == ModemCategory.MODEM_TD ) {
            findViewById(R.id.antenna_title_3g).setVisibility(View.GONE);
            mSpinner3G.setVisibility(View.GONE);
        }

        if (!FeatureSupport.isSupported(FeatureSupport.FK_MTK_C2K_SUPPORT)
                || FeatureSupport.is90Modem() ) {
            findViewById(R.id.antenna_title_c2k).setVisibility(View.GONE);
            mSpinnerC2kMode.setVisibility(View.GONE);
        }
        else {
            mCdmaPhone = ModemCategory.getCdmaPhone();
        }
        mPhone = PhoneFactory.getPhone(ModemCategory.getCapabilitySim());
    }

    @Override
    protected void onResume() {
        super.onResume();
        queryCurrentMode();
        if (FeatureSupport.isSupported(FeatureSupport.FK_MTK_C2K_SUPPORT)
                && !FeatureSupport.is90Modem()) {
            queryCurrentCdmaMode();
        }
    }

    private void queryCurrentMode() {
        sendCommand(new String[] {"AT+ERXPATH?", "+ERXPATH:"}, MSG_QUERY_ANTENNA_MODE);
    }

    private void queryCurrentCdmaMode() {
        String[] cmd = new String[3];
        cmd[0] = "AT+ERXTESTMODE?";
        cmd[1] = "+ERXTESTMODE:";
        cmd[2] = "DESTRILD:C2K";
        String[] cmd_s = ModemCategory.getCdmaCmdArr(cmd);
        sendCdmaCommand(cmd_s,MSG_QUERY_ANTENNA_MODE_C2K);
    }

    private void setMode(int mode) {
        Log.i("@M_" + TAG, "Set mode " + mode);
        sendCommand(new String[] {"AT+ERXPATH=" + mode, ""}, MSG_SET_ANTENNA_MODE);
    }

    private void setCdmaMode(int mode) {
        String[] cmd = new String[3];
        cmd[0] = "AT+ERXTESTMODE=" + mode;
        cmd[1] = "+ERXTESTMODE:";
        cmd[2] = "DESTRILD:C2K";
        String[] cmd_s = ModemCategory.getCdmaCmdArr(cmd);
        sendCdmaCommand(cmd_s,MSG_SET_ANTENNA_MODE);
    }

    private void sendCommand(String[] command, int msg) {
        Log.d("@M_" + TAG, "sendCommand " + command[0]);
        if (mPhone != null) {
            mPhone.invokeOemRilRequestStrings(command, mCommandHander.obtainMessage(msg));
        }
    }

    private void sendCdmaCommand(String[] command, int msg) {
        Log.d(TAG, "send cdma cmd: " + command[0] + ",command.length = " + command.length);
        if (mCdmaPhone!= null) {
            mCdmaPhone.invokeOemRilRequestStrings(command,
                          mCommandHander.obtainMessage(msg));
        }
    }

    private void showToast(String msg) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        mToast.show();
    }
}
