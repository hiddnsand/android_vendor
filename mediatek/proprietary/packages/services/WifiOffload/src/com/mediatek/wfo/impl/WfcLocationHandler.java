/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.wfo.impl;

import android.content.Context;
import android.content.Intent;

import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.Geocoder;

import android.net.Uri;

import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;

import android.provider.Settings;
import android.telephony.Rlog;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.MtkTelephonyIntents;
import com.mediatek.telephony.MtkTelephonyManagerEx;

public class WfcLocationHandler {
    private static final String TAG = "WfcLocationHandler";

    private static final String AID_SETTING_URI_STR = "";
    private static final Uri AID_SETTING_URI = Settings.Global.getUriFor(AID_SETTING_URI_STR);

    private static final String UT_AID_BROADCAST_STRING = "com.mediatek.internal.wfcdispatcher.aid";
    private static final String UT_AID_INTENT_KEY = "aid";

    // Geo Location: the payload defined as LocationInfo and handled in handleLocationInfo().
    private static final int MSG_REG_IMSA_REQUEST_GEO_LOCATION_INFO = 96009;
    private static final int MSG_REG_IMSA_RESPONSE_GETO_LOCATION_INFO = 91030;
    private static int MAX_NUM_OF_GET_LOCATION_TASKS_THREAD = 3;

    /* Event Message definition for hanlder */
    private static final int BASE = 3000;
    public static final int EVENT_GET_LOCATION_REQUEST              = BASE + 0;
    private static final int EVENT_HANDLE_NETWORK_LOCATION_RESPONSE = BASE + 1;
    private static final int EVENT_SET_LOCATION_INFO                = BASE + 2;

    private static int NETWORK_LOCATION_UPDATE_TIME = 1000;

    private Context mContext;

    private ArrayList<LocationInfo> mLocationInfoQueue = new ArrayList<LocationInfo>();
    private int mExecutingLocationInfoNum = 0;

    private ArrayList<LocationInfo> mNetworkLocationTasks = new ArrayList<LocationInfo>();
    LocationManager mLocationManager;
    Geocoder mGeoCoder;
    LocationListenerImp mLocationListener = new LocationListenerImp();

    private class LocationListenerImp implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            log("onLocationChanged: " + location);
            cancelNetworkGeoLocationRequest();
            mHandler.obtainMessage(EVENT_HANDLE_NETWORK_LOCATION_RESPONSE, 0, 0, location)
                    .sendToTarget();
        }

        @Override
        public void onProviderDisabled(String provider) {
            log("onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            log("onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            log("onStatusChanged: " + provider + ", status=" + status);
        }
    }

    public class LocationInfo {
        public int mSimIdx;
        public int mAccountId;
        public int mBroadcastFlag;

        public double mLatitude;
        public double mLongitude;
        public double mAccuracy;
        public String mMethod = "";
        public String mCity = "";
        public String mState = "";
        public String mZip = "";
        public String mCountryCode = "";

        LocationInfo(int simIdx, int accountId,
            int broadcastFlag, double latitude, double longitude, double accuracy) {
            mSimIdx = simIdx;
            mAccountId = accountId;
            mBroadcastFlag = broadcastFlag;
            mLatitude = latitude;
            mLongitude = longitude;
            mAccuracy = mAccuracy;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[LocationInfo objId: ");
            sb.append(System.identityHashCode(this));
            sb.append(", phoneId: " + mSimIdx);
            sb.append(", transationId: " + mAccountId);
            sb.append(", latitude: " + mLatitude);
            sb.append(", longitude: " + mLongitude);
            sb.append(", accuracy: " + mAccuracy);
            sb.append(", method: " + mMethod);
            sb.append(", city: " + mCity);
            sb.append(", state: " + mState);
            sb.append(", zip: " + mZip);
            sb.append(", countryCode: " + mCountryCode);
            return sb.toString();
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            log("handleMessage: msg= " + messageToString(msg));
            switch (msg.what) {
                case EVENT_GET_LOCATION_REQUEST:
                    handleLocationRequest(msg);
                    break;
                case EVENT_HANDLE_NETWORK_LOCATION_RESPONSE: {
                    Location location = (Location) msg.obj;
                    handleNetworkLocationUpdate(location);
                    break;
                }
                case EVENT_SET_LOCATION_INFO: {
                    LocationInfo locationInfo = (LocationInfo) msg.obj;
                    setLocationInfo(locationInfo);
                    break;
                }
                default:
                    break;
            }
        }

        private String messageToString(Message msg) {
            switch (msg.what) {

                case EVENT_GET_LOCATION_REQUEST:
                    return "EVENT_GET_LOCATION_REQUEST";
                case EVENT_HANDLE_NETWORK_LOCATION_RESPONSE:
                    return "EVENT_HANDLE_NETWORK_LOCATION_RESPONSE";
                case EVENT_SET_LOCATION_INFO:
                    return "EVENT_SET_LOCATION_INFO";

                default:
                    return "UNKNOWN";
            }
        }
    };

    public Handler getHandler() {
        return mHandler;
    }

    public WfcLocationHandler(Context context) {
        mContext = context;

        mGeoCoder = new Geocoder(mContext, Locale.US);
        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
    }

    protected void log(String s) {
        Rlog.d(TAG, s);
    }

    private String[] getStringArrayFromMsg(Message msg) {
        AsyncResult ar = (AsyncResult) msg.obj;
        Intent intent = (Intent) ar.result;
        return getStringArrayFromIntent(intent);
    }

    private String[] getStringArrayFromIntent(Intent intent) {
        String extra = null;
        switch(intent.getAction()) {
            case MtkTelephonyIntents.ACTION_REQUEST_GEO_LOCATION:
                extra = MtkTelephonyIntents.EXTRA_LOCATION_REQ;
                break;
            default:
                Rlog.d(TAG, intent.getAction() + " no implements");
        }
        if (null == extra) {
            return null;
        }
        return intent.getExtras().getStringArray(extra);
    }

    /** Geo Location **/
    private void handleLocationRequest(Message msg) {
        String[] result = getStringArrayFromMsg(msg);
        if (null == result) {
            Rlog.e(TAG, "handleLocationInfo(): result is null");
            return;
        }

        try {
            int accId = Integer.parseInt(result[0]);
            int broadcastFlag = Integer.parseInt(result[1]);
            double latitude = Double.parseDouble(result[2]);
            double longitude = Double.parseDouble(result[3]);
            double accuracy = Double.parseDouble(result[4]);
            int simIdx = Integer.parseInt(result[5]);


            LocationInfo locationInfo = new LocationInfo(
                simIdx, accId, broadcastFlag, latitude, longitude, accuracy);

            log("handleGeoLocationRequest(): " + locationInfo);
            dispatchLocationRequest(locationInfo);
        } catch (Exception e) {
            log("handleGeoLocationRequest()[" + result.length + "]" + result[0] + " " +
                result[1] + " " +result[2] + " " + result[3] + " " + result[4] + " " + result[5]);
        }

    }

    private void dispatchLocationRequest(LocationInfo info) {
        double latitude = info.mLatitude;
        double longitude = info.mLongitude;
        double accuracy = info.mAccuracy;
        // if no gps signal, use Wifi location
        if (latitude == 0 && longitude == 0 && accuracy == 0) {
            proccessLocationFromNetwork(info);
        } else {
            info.mMethod = "GPS";
            mLocationInfoQueue.add(info);
            pollLocationInfo();
        }
        log("dispatchLocationRequest(): " + info.mMethod);
    }

    private void handleNetworkLocationUpdate(Location location) {
        if (location == null) {
            log("network location get null, unexpected result");
            return;
        }

        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        log("update all LocationInfo with (" + latitude + ", " + longitude + ")");

        for (LocationInfo locationInfo : mNetworkLocationTasks) {
            locationInfo.mLatitude = latitude;
            locationInfo.mLongitude = longitude;
            locationInfo.mAccuracy = 1.0;
            log("Get network location, send EVENT_MSG_REQUEST_GEO_LOCATION for"
                    + " transationId-" + locationInfo.mAccountId);
            mLocationInfoQueue.add(locationInfo);
        }
        pollLocationInfo();
        mNetworkLocationTasks.clear();
    }

    private void proccessLocationFromNetwork(LocationInfo info) {
        info.mMethod = "Network";
        mNetworkLocationTasks.add(info);
        if (!requestGeoLocationFromNetworkLocation()) {
            mNetworkLocationTasks.remove(info);
            log("requestGeoLocationFromNetworkLocation failed");
            setLocationInfo(info);
        }
    }

    private void pollLocationInfo() {
        if (mLocationInfoQueue.isEmpty()) {
            log("No GeoLocation task");
            return;
        }

        if (mExecutingLocationInfoNum >= MAX_NUM_OF_GET_LOCATION_TASKS_THREAD) {
            log("current number of GeoLocation task is limited, wait for previous task finish");
            return;
        }
        ++mExecutingLocationInfoNum;
        // process 1st request by thread and post result to main thread
        final LocationInfo locationInfo = mLocationInfoQueue.remove(0);
        new Thread(new Runnable() {
            @Override
            public void run() {
                LocationInfo res = getGeoLocationFromLatLong(locationInfo);
                mHandler.obtainMessage(EVENT_SET_LOCATION_INFO, 0, 0, res).sendToTarget();
                --mExecutingLocationInfoNum;
            }
        }).start();

    }

    private void setLocationInfo(LocationInfo info) {
        MtkTelephonyManagerEx.getDefault().setLocationInfo(
                getMainCapabilityPhoneId(),
                Integer.toString(info.mAccountId),
                Integer.toString(info.mBroadcastFlag),
                String.valueOf(info.mLatitude),
                String.valueOf(info.mLongitude),
                String.valueOf(info.mAccuracy),
                info.mMethod,
                info.mCity,
                info.mState,
                info.mZip,
                info.mCountryCode);
        pollLocationInfo();
    }
    /**
     * This function will be executed in worker thread.
     */
    private LocationInfo getGeoLocationFromLatLong(LocationInfo location) {
        if (mGeoCoder == null) {
            log("getGeoLocationFromLatLong: empty geoCoder, return an empty location");
            return location;
        }

        if (!mGeoCoder.isPresent()) {
            log("getGeoLocationFromLatLong: this system has no GeoCoder implementation!!");
            return location;
        }

        double lat = location.mLatitude;
        double lng = location.mLongitude;

        List<Address> lstAddress = null;
        try {
            lstAddress = mGeoCoder.getFromLocation(lat, lng, 1);
        } catch (IOException e) {
            log("mGeoCoder.getFromLocation throw exception:" + e);
        } catch (Exception e2) {
            log("mGeoCoder.getFromLocation throw exception:" + e2);
        }

        if (lstAddress == null || lstAddress.isEmpty()) {
            log("getGeoLocationFromLatLong: get empty address");
            return location;
        }

        location.mCity = lstAddress.get(0).getAdminArea();  // Taipei
        location.mState = lstAddress.get(0).getCountryName();  // Taiwan;
        location.mZip = lstAddress.get(0).getPostalCode();
        location.mCountryCode = lstAddress.get(0).getCountryCode();

        log("getGeoLocationFromLatLong: location=" + location);
        return location;
    }

    private boolean requestGeoLocationFromNetworkLocation() {
        log("requestGeoLocationFromNetworkLocation");
        if (mLocationManager == null) {
            log("getGeoLocationFromNetworkLocation: empty locationManager, return");
            return false;
        }

        if (!mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            log("requestGeoLocationFromNetworkLocation:"
                    + "this system has no networkProvider implementation!");
            return false;
        }

        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                NETWORK_LOCATION_UPDATE_TIME, 0, mLocationListener);
        log("requestGeoLocationFromNetworkLocation: request networkLocation update");

        return true;
    }

    private void cancelNetworkGeoLocationRequest() {
        log("cancelNetworkGeoLocationRequest");
        if (mLocationManager == null) {
            log("cancelNetworkGeoLocationRequest: empty locationManager, return");
            return;
        }
        mLocationManager.removeUpdates(mLocationListener);
    }

    private void utGeoLocationRequest() {
        LocationInfo locationInfo = new LocationInfo(
                0, 8, 0, 212.0, 147.0, 1);
        dispatchLocationRequest(locationInfo);
    }

    private void utNetworkLocationRequest() {
        LocationInfo locationInfo = new LocationInfo(
                0, 8, 0, 0.0, 0.0, 0.0);
        dispatchLocationRequest(locationInfo);
    }

    /**
     * to get main capability phone id.
     *
     * @return The phone id with highest capability.
     */
    private int getMainCapabilityPhoneId() {
       int phoneId = SystemProperties.getInt(MtkPhoneConstants.PROPERTY_CAPABILITY_SWITCH, 1) - 1;
       if (phoneId < 0 || phoneId >= TelephonyManager.getDefault().getPhoneCount()) {
           phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
       }
       Rlog.d(TAG, "getMainCapabilityPhoneId = " + phoneId);
       return phoneId;
    }
}
