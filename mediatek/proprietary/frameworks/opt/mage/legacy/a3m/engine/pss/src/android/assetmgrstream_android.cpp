/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/**
 * PSS AssetMgrStreamSource API
 *
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/log.h>                   /* for logging              */
#include <assetmgrstream_android.h>    /* AssetMgrStreamSource API */
#include <cstdio>                      /* for sprintf */

#include <android/log.h>                /* for __android_log_print() */
#include <android/asset_manager.h>      /* Android NDK asset manager API */
/**
 * Given a Dalvik AssetManager object, obtain the corresponding native AAssetManager
 * object.
 */
#include <android/asset_manager_jni.h>  /* for AAsset */

/** \todo
 *  Remove all log messages when the function is tested.
 *
 */
#define  LOG_TAG    "A3M Asset Mgr"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

namespace a3m
{
  /****************************************************************************
   * AssetMgrStreamSource private types and definitions
   ****************************************************************************/
  namespace
  {
    /*
     * Concrete Stream class for reading data from the Android Asset Manager
     */
    class AssetMgrStream : public Stream
    {
    public:
      /*
       * Constructor
       * Constructs a Android asset stream with the given name and initialises
       * the size of the stream.
       */
      AssetMgrStream(AAsset* asset, const A3M_CHAR8* name) :
        m_size(0), m_asset(asset), m_pos(0)
      {
        //LOGI("AssetMgrStream Constructor File name %s", name);

        // get the total size of the asset data
        m_size = AAsset_getLength(m_asset);
      }

      /*
       * Destructor
       * Release Asset Manager
       */
      virtual ~AssetMgrStream()
      {
        //LOGI("AssetMgrStream destructor");
        AAsset_close(m_asset);
      }

      /*
       * Returns A3M_TRUE if the stream is in a working state.
       */
      virtual A3M_BOOL valid()
      {
        return A3M_TRUE;
      }

      /*
       * Returns A3M_TRUE if the end of the stream has been reached.
       */
      virtual A3M_BOOL eof()
      {
        return m_pos >= m_size;
      }

      /*
       * Returns the size of the stream in bytes.
       */
      virtual A3M_INT32 size()
      {
        //LOGI("AssetMgrStream stream size:  %d", m_size);
        return m_size;
      }

      /*
       * Modifies the internal read pointer of the underlying resource
       * object.  Seek operation is referenced from the start of the resource
       * file stream.
       * Returns the new position on success, or -1 on error.
       */
      virtual A3M_INT32 seek(A3M_UINT32 offset)
      {
        m_pos = offset;
        //LOGI("AssetMgrStream seek:  %d", offset);
        return AAsset_seek(m_asset, offset, SEEK_SET);
      }

      /*
       * Return current position from the source file.
       */
      virtual A3M_INT32 tell()
      {
        //LOGI("AssetMgrStream tell");
        return m_pos;
      }

      /*
       * Read byteLength number of bytes from the file stream and copied
       * the data to the specified destination.
       */
      virtual A3M_INT32 read(void* dest, A3M_UINT32 byteLength)
      {
        if (dest == 0) { return 0; }
        //LOGI("AssetMgrStream read   :  %d", byteLength);
        m_pos += byteLength;
        return (A3M_INT32)AAsset_read(m_asset, dest, byteLength);
      }

      /*
       * TODO: write
       */
      virtual A3M_INT32 write(const void* source, A3M_UINT32 byteLength)
      {
        // ZIP stream is not writable
        A3M_PARAM_NOT_USED(source);
        A3M_PARAM_NOT_USED(byteLength);
        return 0;
      }

    private:
      A3M_INT32 m_size;      // size of the stream
      AAsset* m_asset;       // asset pointer
      A3M_INT32 m_pos;      // hold current position from the file
    };
  } /* anonymous namespace */

  /****************************************************************************
   * AssetMgrStreamSource class definition
   ****************************************************************************/
  /*
   * Constructor
   */
  AssetMgrStreamSource::AssetMgrStreamSource(AAssetManager* mgr): m_manager(mgr)
  {
    //LOGI("AssetMgrStreamSource constructor");
  }

  /*
   * Destructor
   */
  AssetMgrStreamSource::~AssetMgrStreamSource()
  {
  }

  /*
   * Check for the existence of a file
   */
  A3M_BOOL AssetMgrStreamSource::exists(const A3M_CHAR8* stream)
  {
    if ((stream == NULL) || (stream[0] == '\0'))
    {
      //LOGE("AssetMgrStreamSource doesn't exist");
      return A3M_FALSE;
    }

    AAsset* asset = AAssetManager_open(m_manager, stream, AASSET_MODE_UNKNOWN);

    if (asset == NULL)
    {
      // This is duplicated error report with relevant material loaders, and also
      // misleading message if the asset exists in other asset path. So change
      // to informative and re-word the message.
      LOGI("AssetMgrStreamSource %s  could not load from apk/assets folder", stream);
      return A3M_FALSE;
    }

    //LOGI("AssetMgrStreamSource exist");
    AAsset_close(asset);
    return A3M_TRUE;
  }

  /*
   * Open a file stream for reading or writing.
   */
  Stream::Ptr AssetMgrStreamSource::open(const A3M_CHAR8* stream, A3M_BOOL writable)
  {
    if (writable)
    {
      A3M_LOG_ERROR( "Cannot open a ZIP stream for writing", 0);
      return Stream::Ptr();
    }

    AAsset* asset = AAssetManager_open(m_manager, stream, AASSET_MODE_UNKNOWN);

    if (asset == NULL)
    {
      LOGE("AssetMgrStreamSource %s  not found", stream);
      return Stream::Ptr();
    }

    //LOGI("createAndOpenAsset succeed %s writable %d", stream, writable);
    return Stream::Ptr(new AssetMgrStream(asset, stream));
  }

} /* namespace a3m */
