/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony.uicc;

import android.content.Context;
import android.os.AsyncResult;
import android.os.SystemProperties;
import android.os.Bundle;
import android.telephony.Rlog;
import com.android.internal.telephony.uicc.UiccController;
import com.android.internal.telephony.uicc.IccCardStatus;
import com.android.internal.telephony.uicc.UiccCard;
import com.android.internal.telephony.CommandsInterface;
import com.mediatek.internal.telephony.uicc.MtkUiccCard;
import android.app.ActivityManagerNative;
import static android.Manifest.permission.READ_PHONE_STATE;
import android.os.UserHandle;
import android.provider.Settings;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.TelephonyIntents;

import android.content.SharedPreferences;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.Intent;

import android.app.PendingIntent;
import android.app.Notification;
import android.app.NotificationManager;

import com.android.internal.telephony.uicc.IccCardApplicationStatus.AppState;
import com.android.internal.telephony.uicc.IccCardApplicationStatus.PersoSubState;
import com.android.internal.telephony.uicc.IccRefreshResponse;
import com.android.internal.telephony.uicc.UiccCardApplication;
import com.android.internal.telephony.SubscriptionController;
import android.os.Handler;
import android.os.Bundle;
import android.os.Registrant;
import android.os.RegistrantList;
import android.os.Message;
import android.telephony.TelephonyManager;
import com.mediatek.internal.telephony.MtkRIL;
import com.mediatek.internal.telephony.MtkTelephonyIntents;
import com.mediatek.internal.telephony.MtkIccCardConstants;
// External SIM [START]
import com.mediatek.internal.telephony.MtkTelephonyProperties;
import com.mediatek.internal.telephony.RadioManager;
import com.mediatek.telephony.internal.telephony.vsim.ExternalSimManager;
// External SIM [END]

public class MtkUiccController extends UiccController {
    private static final String LOG_TAG_EX = "MtkUiccCtrl";
    protected static final int EVENT_RADIO_AVAILABLE = 100;
    protected static final int EVENT_VIRTUAL_SIM_ON = 101;
    protected static final int EVENT_VIRTUAL_SIM_OFF = 102;
    // MTK-START: SIM MISSING/RECOVERY
    protected static final int EVENT_SIM_MISSING = 103;
    protected static final int EVENT_SIM_RECOVERY = 105;
    protected static final int EVENT_GET_ICC_STATUS_DONE_FOR_SIM_MISSING = 106;
    protected static final int EVENT_GET_ICC_STATUS_DONE_FOR_SIM_RECOVERY = 107;
    // MTK-END
    // MTK-START: SIM HOT SWAP
    protected static final int EVENT_SIM_PLUG_OUT = 109;
    protected static final int EVENT_SIM_PLUG_IN = 110;
    // MTK-END

    protected static final int EVENT_INVALID_SIM_DETECTED = 114;
    // MTK-START: CMCC DUAL SIM DEPENDENCY LOCK
    protected static final int EVENT_REPOLL_SML_STATE = 115;
    // MTK-END
    // MTK-START: SIM COMMON SLOT
    protected static final int EVENT_COMMON_SLOT_NO_CHANGED = 116;
    // MTK-END
    // MTK-START: CMCC DUAL SIM DEPENDENCY LOCK
    private static final int SML_FEATURE_NO_NEED_BROADCAST_INTENT = 0;
    private static final int SML_FEATURE_NEED_BROADCAST_INTENT = 1;
    // MTK-END
    // MTK-START: SIM MISSING/RECOVERY
    private RegistrantList mRecoveryRegistrants = new RegistrantList();
    // MTK-END
    //Multi-application
    private int[] mIsimSessionId = new int[TelephonyManager.getDefault().getPhoneCount()];
    private RegistrantList mApplicationChangedRegistrants = new RegistrantList();
    private int[] UICCCONTROLLER_STRING_NOTIFICATION_VIRTUAL_SIM_ON = {
        com.mediatek.internal.R.string.virtual_sim_on_slot1,
        com.mediatek.internal.R.string.virtual_sim_on_slot2,
        com.mediatek.internal.R.string.virtual_sim_on_slot3,
        com.mediatek.internal.R.string.virtual_sim_on_slot4
    };
    // MTK-START: SIM COMMON SLOT
    protected static final String COMMON_SLOT_PROPERTY = "ro.mtk_sim_hot_swap_common_slot";
    // MTK-END

    public MtkUiccController(Context c, CommandsInterface []ci) {
        super(c, ci);
        if (DBG) Rlog.d(LOG_TAG_EX, "Creating MtkUiccController");

        for (int i = 0; i < mCis.length; i++) {
            Integer index = new Integer(i);
            mCis[i].unregisterForAvailable(this);
            mCis[i].unregisterForOn(this);
            // MTK-START
            if (SystemProperties.get("ro.crypto.state").equals("unencrypted")
                    || SystemProperties.get("ro.crypto.state").equals("unsupported")
                    || SystemProperties.get("ro.crypto.type").equals("file")
                    || DECRYPT_STATE.equals(SystemProperties.get("vold.decrypt"))) {
            //if (DECRYPT_STATE.equals(SystemProperties.get("vold.decrypt"))) {
            // MTK-END
                mCis[i].registerForAvailable(this, EVENT_ICC_STATUS_CHANGED, index);
            } else {
                mCis[i].registerForOn(this, EVENT_ICC_STATUS_CHANGED, index);
            }
            //TODO: HIDL extention impl.
            ((MtkRIL)mCis[i]).registerForVirtualSimOn(this, EVENT_VIRTUAL_SIM_ON, index);
            ((MtkRIL)mCis[i]).registerForVirtualSimOff(this, EVENT_VIRTUAL_SIM_OFF, index);

            // MTK-START: SIM MISSING/RECOVERY
            ((MtkRIL)mCis[i]).registerForSimMissing(this, EVENT_SIM_MISSING, index);
            ((MtkRIL)mCis[i]).registerForSimRecovery(this, EVENT_SIM_RECOVERY, index);
            // MTK-END
            // MTK-START: SIM HOT SWAP
            ((MtkRIL)mCis[i]).registerForSimPlugOut(this, EVENT_SIM_PLUG_OUT, index);
            ((MtkRIL)mCis[i]).registerForSimPlugIn(this, EVENT_SIM_PLUG_IN, index);
            // MTK-END
            // MTK-START: SIM COMMON SLOT
            ((MtkRIL)mCis[i]).registerForCommonSlotNoChanged(this, EVENT_COMMON_SLOT_NO_CHANGED,
                    index);
            // MTK-END
        }

        // External SIM [Start]
        ExternalSimManager.make(c, ci);
        // Exteranl SIM [End]

        mMdStateReceiver = new ModemStateChangedReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(RadioManager.ACTION_MODEM_POWER_NO_CHANGE);
        mContext.registerReceiver(mMdStateReceiver, filter);
    }

    // Easy to use API
    // MTK-START
    public UiccCardApplication getUiccCardApplication(int family) {
        return getUiccCardApplication(SubscriptionController.getInstance().getPhoneId(
                SubscriptionController.getInstance().getDefaultSubId()), family);
    }
    // MTK-END

    // MTK-START
    public int getIccApplicationChannel(int slotId, int family) {
        synchronized (mLock) {
            int index = 0;
            switch (family) {
                case UiccController.APP_FAM_IMS:
                    // FIXME: error handling for invaild slotId?
                    index = mIsimSessionId[slotId];
                    // Workaround: to avoid get sim status has open isim channel but java layer
                    // haven't update channel id
                    if (index == 0) {
                        index = (getUiccCardApplication(slotId, family) != null) ? 1 : 0;
                    }
                    break;
                default:
                    if (DBG) mtkLog("unknown application");
                    break;
            }
            return index;
        }
    }
    // MTK-END

    // MTK-START: SIM MISSING/RECOVERY
    //Notifies when card status changes
    public void registerForIccRecovery(Handler h, int what, Object obj) {
        synchronized (mLock) {
            Registrant r = new Registrant(h, what, obj);
            mRecoveryRegistrants.add(r);
            //Notify registrant right after registering, so that it will get the latest ICC status,
            //otherwise which may not happen until there is an actual change in ICC status.
            r.notifyRegistrant();
        }
    }

    public void unregisterForIccRecovery(Handler h) {
        synchronized (mLock) {
            mRecoveryRegistrants.remove(h);
        }
    }
    // MTK-END

    @Override
    public void handleMessage (Message msg) {
        synchronized (mLock) {
            Integer index = getCiIndex(msg);

            if (index < 0 || index >= mCis.length) {
                Rlog.e(LOG_TAG_EX, "Invalid index : " + index + " received with event " + msg.what);
                return;
            }

            // The msg.obj may be Integer if it is from ModemStateChangedReceiver
            AsyncResult ar = null;
            if(msg.obj != null && msg.obj instanceof AsyncResult) {
                ar = (AsyncResult)msg.obj;
            }

            switch (msg.what) {
                case EVENT_ICC_STATUS_CHANGED:
                    // MTK-START
                    if (DBG) {
                        mtkLog("Received EVENT_ICC_STATUS_CHANGED, calling getIccCardStatus,index: "
                            + index);
                    }

                    //mCis[index].getIccCardStatus(obtainMessage(EVENT_GET_ICC_STATUS_DONE, index));
                    if (ignoreGetSimStatus()) {
                        if (DBG) mtkLog("FlightMode ON, Modem OFF: ignore get sim status");
                    } else {
                    // MTK-END
                        mCis[index].getIccCardStatus(obtainMessage(
                                EVENT_GET_ICC_STATUS_DONE, index));
                    // MTK-START
                    }
                    // MTK-END
                    break;
                // MTK-START: CMCC DUAL SIM DEPENDENCY LOCK
                case EVENT_REPOLL_SML_STATE:
                    if (DBG) mtkLog("Received EVENT_REPOLL_SML_STATE");
                    ar = (AsyncResult) msg.obj;
                    boolean needIntent =
                            ((msg.arg1 == SML_FEATURE_NEED_BROADCAST_INTENT) ? true : false);

                    //Update Uicc Card status.
                    onGetIccCardStatusDone(ar, index, false);

                    // If we still in Network lock, broadcast intent if caller need this intent.
                    if (mUiccCards[index] != null && needIntent == true) {
                        UiccCardApplication app = mUiccCards[index].getApplication(APP_FAM_3GPP);
                        if (app == null) {
                            if (DBG) mtkLog("UiccCardApplication = null");
                            break;
                        }
                        if (app.getState() == AppState.APPSTATE_SUBSCRIPTION_PERSO) {
                            Intent lockIntent = new Intent();
                            if (DBG) mtkLog("Broadcast ACTION_UNLOCK_SIM_LOCK");
                            lockIntent.setAction(MtkTelephonyIntents.ACTION_UNLOCK_SIM_LOCK);
                            lockIntent.putExtra(IccCardConstants.INTENT_KEY_ICC_STATE,
                                    IccCardConstants.INTENT_VALUE_ICC_LOCKED);
                            lockIntent.putExtra(IccCardConstants.INTENT_KEY_LOCKED_REASON,
                                    parsePersoType(app.getPersoSubState()));
                            SubscriptionManager.putPhoneIdAndSubIdExtra(lockIntent, index);
                            mContext.sendBroadcast(lockIntent);
                        }
                    }
                    break;
                // MTK-END
                case EVENT_VIRTUAL_SIM_ON:
                    if (DBG) {
                        mtkLog("handleMessage (EVENT_VIRTUAL_SIM_ON)");
                    }
                    setNotificationVirtual(index, EVENT_VIRTUAL_SIM_ON);
                    SharedPreferences shOn = mContext.getSharedPreferences("AutoAnswer", 1);
                    SharedPreferences.Editor editorOn = shOn.edit();
                    editorOn.putBoolean("flag", true);
                    editorOn.commit();
                    break;
               case EVENT_VIRTUAL_SIM_OFF:
                    if (DBG) mtkLog("handleMessage (EVENT_VIRTUAL_SIM_OFF)");
                    removeNotificationVirtual(index, EVENT_VIRTUAL_SIM_ON);
                    //setNotification(index, EVENT_SIM_MISSING);
                    SharedPreferences shOff = mContext.getSharedPreferences("AutoAnswer", 1);
                    SharedPreferences.Editor editorOff = shOff.edit();
                    editorOff.putBoolean("flag", false);
                    editorOff.commit();
                    break;
                // MTK-START: SIM MISSING/RECOVERY
                case EVENT_SIM_RECOVERY:
                    if (DBG) mtkLog("handleMessage (EVENT_SIM_RECOVERY)");
                    mCis[index].getIccCardStatus(
                            obtainMessage(EVENT_GET_ICC_STATUS_DONE_FOR_SIM_RECOVERY, index));
                    mRecoveryRegistrants.notifyRegistrants(new AsyncResult(null, index, null));

                    //ALPS01209124
                    Intent intent = new Intent();
                    intent.setAction(MtkTelephonyIntents.ACTION_SIM_RECOVERY_DONE);
                    mContext.sendBroadcast(intent);
                    break;
                case EVENT_SIM_MISSING:
                    if (DBG) mtkLog("handleMessage (EVENT_SIM_MISSING)");
                    //setNotification(index, EVENT_SIM_MISSING);
                    mCis[index].getIccCardStatus(
                            obtainMessage(EVENT_GET_ICC_STATUS_DONE_FOR_SIM_MISSING, index));
                    break;
                case EVENT_GET_ICC_STATUS_DONE_FOR_SIM_MISSING:
                    if (DBG) mtkLog("Received EVENT_GET_ICC_STATUS_DONE_FOR_SIM_MISSING");
                    ar = (AsyncResult) msg.obj;
                    onGetIccCardStatusDone(ar, index, false);
                    break;
                case EVENT_GET_ICC_STATUS_DONE_FOR_SIM_RECOVERY:
                    if (DBG) mtkLog("Received EVENT_GET_ICC_STATUS_DONE_FOR_SIM_RECOVERY");
                    ar = (AsyncResult) msg.obj;
                    onGetIccCardStatusDone(ar, index, false);
                    break;
                // MTK-END
                // MTK-START: SIM COMMON SLOT
                case EVENT_COMMON_SLOT_NO_CHANGED:
                    if (DBG) mtkLog("handleMessage (EVENT_COMMON_SLOT_NO_CHANGED)");
                    Intent intentNoChanged = new Intent(
                            MtkTelephonyIntents.ACTION_COMMON_SLOT_NO_CHANGED);
                    int slotId = index.intValue();
                    SubscriptionManager.putPhoneIdAndSubIdExtra(intentNoChanged, slotId);
                    mtkLog("Broadcasting intent ACTION_COMMON_SLOT_NO_CHANGED for mSlotId : " +
                            slotId);
                    mContext.sendBroadcast(intentNoChanged);
                    break;
                // MTK-END
                default:
                    super.handleMessage(msg);
            }
        }
    }

    protected synchronized void onGetIccCardStatusDone(AsyncResult ar, Integer index) {
        if (ar.exception != null) {
            Rlog.e(LOG_TAG_EX,"Error getting ICC status. "
                    + "RIL_REQUEST_GET_ICC_STATUS should "
                    + "never return an error", ar.exception);
            return;
        }
        if (!isValidCardIndex(index)) {
            Rlog.e(LOG_TAG_EX,"onGetIccCardStatusDone: invalid index : " + index);
            return;
        }
        // MTK-START
        if (DBG) Rlog.d(LOG_TAG_EX, "onGetIccCardStatusDone, index " + index);
        // MTK-END

        IccCardStatus status = (IccCardStatus)ar.result;

        if (mUiccCards[index] == null) {
            //Create new card
            mUiccCards[index] = new MtkUiccCard(mContext, mCis[index], status, index);
        } else {
            //Update already existing card
            mUiccCards[index].update(mContext, mCis[index] , status);
        }

        if (DBG) Rlog.d(LOG_TAG_EX, "Notifying IccChangedRegistrants");
        mIccChangedRegistrants.notifyRegistrants(new AsyncResult(null, index, null));

    }

    // TK only
    protected synchronized void onGetIccCardStatusDone(
            AsyncResult ar, Integer index, boolean isUpdate) {
        if (ar.exception != null) {
            Rlog.e(LOG_TAG_EX, "Error getting ICC status. "
                    + "RIL_REQUEST_GET_ICC_STATUS should "
                    + "never return an error", ar.exception);
            return;
        }
        if (!isValidCardIndex(index)) {
            Rlog.e(LOG_TAG_EX, "onGetIccCardStatusDone: invalid index : " + index);
            return;
        }
        if (DBG) mtkLog("onGetIccCardStatusDone, index " + index + "isUpdateSiminfo " + isUpdate);

        IccCardStatus status = (IccCardStatus) ar.result;

        if (mUiccCards[index] == null) {
            //Create new card
            // MTK START
            mUiccCards[index] = new MtkUiccCard(mContext, mCis[index], status, index, isUpdate);
            // MTK END
        } else {
            //Update already existing card
            ((MtkUiccCard)mUiccCards[index]).update(mContext, mCis[index] , status, isUpdate);
        }

        if (DBG) mtkLog("Notifying IccChangedRegistrants");
        // TODO: Think if it is possible to pass isUpdate
        // MTK-START: SIM COMMON SLOT
        if (!SystemProperties.get(COMMON_SLOT_PROPERTY).equals("1")) {
        // MTK-END
            mIccChangedRegistrants.notifyRegistrants(new AsyncResult(null, index, null));
        // MTK-START: SIM COMMON SLOT
        } else {
            Bundle result = new Bundle();
            result.putInt("Index", index.intValue());
            result.putBoolean("ForceUpdate", isUpdate);

            mIccChangedRegistrants.notifyRegistrants(new AsyncResult(null, result, null));
        }
        // MTK-END
    }

    private void setNotificationVirtual(int slot, int notifyType) {
        if (DBG) mtkLog("setNotificationVirtual(): notifyType = " + notifyType);
        Notification notification = new Notification();
        notification.when = System.currentTimeMillis();
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        notification.icon = com.android.internal.R.drawable.stat_sys_warning;
        Intent intent = new Intent();
        notification.contentIntent = PendingIntent.getActivity(mContext, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        String title = null;

        if (TelephonyManager.getDefault().getSimCount() > 1) {
            title = Resources.getSystem().getText(
                    UICCCONTROLLER_STRING_NOTIFICATION_VIRTUAL_SIM_ON[slot]).toString();
        } else {
            title = Resources.getSystem().getText(
                    com.mediatek.internal.R.string.virtual_sim_on).toString();
        }
        CharSequence detail = mContext.getText(
                com.mediatek.internal.R.string.virtual_sim_on).toString();
        notification.tickerText = mContext.getText(
                com.mediatek.internal.R.string.virtual_sim_on).toString();

        notification.setLatestEventInfo(mContext, title, detail, notification.contentIntent);
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(
                Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notifyType + slot, notification);
    }

    private void removeNotificationVirtual(int slot, int notifyType) {
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(
                Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notifyType + slot);
    }

    private int mBtSlotId = -1;

    /**
     * Get BT connected sim id.
     *
     * @internal
     */
    public int getBtConnectedSimId() {
        if (DBG) mtkLog("getBtConnectedSimId, slot " + mBtSlotId);
        return mBtSlotId;
    }

    /**
     * Set BT connected sim id.
     *
     * @internal
     */
    public void setBtConnectedSimId(int simId) {
        mBtSlotId = simId;
        if (DBG) mtkLog("setBtConnectedSimId, slot " + mBtSlotId);
    }

    // MTK-START: CMCC DUAL SIM DEPENDENCY LOCK
    /**
     * Parse network lock reason string.
     *
     * @param state network lock type
     * @return network lock string
     *
     */
    private String parsePersoType(PersoSubState state) {
        if (DBG) mtkLog("parsePersoType, state = " + state);
        switch (state) {
            case PERSOSUBSTATE_SIM_NETWORK:
                return IccCardConstants.INTENT_VALUE_LOCKED_NETWORK;
            case PERSOSUBSTATE_SIM_NETWORK_SUBSET:
                return MtkIccCardConstants.INTENT_VALUE_LOCKED_NETWORK_SUBSET;
            case PERSOSUBSTATE_SIM_CORPORATE:
                return MtkIccCardConstants.INTENT_VALUE_LOCKED_CORPORATE;
            case PERSOSUBSTATE_SIM_SERVICE_PROVIDER:
                return MtkIccCardConstants.INTENT_VALUE_LOCKED_SERVICE_PROVIDER;
            case PERSOSUBSTATE_SIM_SIM:
                return MtkIccCardConstants.INTENT_VALUE_LOCKED_SIM;
            default:
                break;
        }
        return IccCardConstants.INTENT_VALUE_ICC_UNKNOWN;
    }

    //Modem SML change feature.
    public void repollIccStateForModemSmlChangeFeatrue(int slotId, boolean needIntent) {
        if (DBG) mtkLog("repollIccStateForModemSmlChangeFeatrue, needIntent = " + needIntent);
        int arg1 = ((needIntent == true) ?
                SML_FEATURE_NEED_BROADCAST_INTENT : SML_FEATURE_NO_NEED_BROADCAST_INTENT);
        //Use arg1 to determine the intent is needed or not
        //Use object to indicated slotId
        mCis[slotId].getIccCardStatus(obtainMessage(EVENT_REPOLL_SML_STATE, arg1, 0, slotId));
    }
    // MTK-END

    //Notifies when application status changes
    public void registerForApplicationChanged(Handler h, int what, Object obj) {
        synchronized (mLock) {
            Registrant r = new Registrant(h, what, obj);
            mApplicationChangedRegistrants.add(r);
            // Notify registrant right after registering, so that it will get the
            // latest application status, otherwise which may not happen until there
            // is an actual change in application status.
            r.notifyRegistrant();
        }
    }

    public void unregisterForApplicationChanged(Handler h) {
        synchronized (mLock) {
            mApplicationChangedRegistrants.remove(h);
        }
    }

    // Check if modem will be power off quickly after boot up device
    public boolean ignoreGetSimStatus() {
        int airplaneMode = Settings.Global.getInt(mContext.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0);

        if (DBG) mtkLog("ignoreGetSimStatus(): airplaneMode - " + airplaneMode);
        if (RadioManager.isFlightModePowerOffModemEnabled() && (airplaneMode == 1)) {
            if (DBG) mtkLog("ignoreGetSimStatus(): return true");
            return true;
        }

        return false;
    }


    public void onSimRefresh(AsyncResult ar, Integer index) {
        if (ar.exception != null) {
            Rlog.e(LOG_TAG_EX, "Sim REFRESH with exception: " + ar.exception);
            return;
        }

        if (!isValidCardIndex(index)) {
            Rlog.e(LOG_TAG_EX, "onSimRefresh: invalid index : " + index);
            return;
        }

        IccRefreshResponse resp = (IccRefreshResponse) ar.result;
        Rlog.d(LOG_TAG_EX, "onSimRefresh: " + resp);

        if (mUiccCards[index] == null) {
            Rlog.e(LOG_TAG_EX, "onSimRefresh: refresh on null card : " + index);
            return;
        }

        if (resp.refreshResult != IccRefreshResponse.REFRESH_RESULT_RESET ||
                (resp.aid == null || resp.aid.equals(""))) {
            Rlog.d(LOG_TAG_EX, "Ignoring reset: " + resp);
            return;
        }

        Rlog.d(LOG_TAG_EX, "Handling refresh reset: " + resp);

        boolean changed = mUiccCards[index].resetAppWithAid(resp.aid);
        if (changed) {
            boolean requirePowerOffOnSimRefreshReset = mContext.getResources().getBoolean(
                    com.android.internal.R.bool.config_requireRadioPowerOffOnSimRefreshReset);
            /*
             * if (requirePowerOffOnSimRefreshReset) {
             * mCis[index].setRadioPower(false, null);
             */
            if (SystemProperties.get("ro.sim_refresh_reset_by_modem").equals("1") != true) {
                // reset modem directly
                mCis[index].resetRadio(null);
            } else {
                mCis[index].getIccCardStatus(obtainMessage(EVENT_GET_ICC_STATUS_DONE));
            }
            mIccChangedRegistrants.notifyRegistrants(new AsyncResult(null, index, null));
        }
    }

    protected void mtkLog(String string) {
        Rlog.d(LOG_TAG_EX, string);
    }

    // External SIM [START]
    public boolean isAllRadioAvailable() {
        boolean isRadioReady = true;
        for (int i = 0; i < TelephonyManager.getDefault().getPhoneCount(); i++) {
            if (CommandsInterface.RadioState.RADIO_UNAVAILABLE == mCis[i].getRadioState()) {
                isRadioReady = false;
            }
        }

        mtkLog("isAllRadioAvailable = " + isRadioReady);

        return isRadioReady;
    }

    public void resetRadioForVsim() {
        int allPhoneIdBitMask = (1 << TelephonyManager.getDefault().getPhoneCount()) - 1;

        mtkLog("resetRadioForVsim...false");
        RadioManager.getInstance().setModemPower(false, allPhoneIdBitMask);
        mtkLog("resetRadioForVsim...true");
        RadioManager.getInstance().setModemPower(true, allPhoneIdBitMask);
    }

    /**
     *  Check if the SIM card is VSIM card.
     *  @param slot id.
     */
    public static MtkIccCardConstants.VsimType getVsimCardType(int slotId) {
        int rSim = SystemProperties.getInt(MtkTelephonyProperties.PROPERTY_PREFERED_REMOTE_SIM, -1);
        int akaSim = SystemProperties.getInt(MtkTelephonyProperties.PROPERTY_PREFERED_AKA_SIM, -1);
        boolean isVsim = false;

        String inserted = TelephonyManager.getDefault().getTelephonyProperty(
                slotId , MtkTelephonyProperties.PROPERTY_EXTERNAL_SIM_INSERTED, "0");
        if (inserted != null && inserted.length() > 0 && !"0".equals(inserted)) {
            isVsim = true;
        }

        if ((slotId == rSim) && isVsim) {
            return MtkIccCardConstants.VsimType.REMOTE_SIM;
        }

        if (slotId == akaSim) {
            if (isVsim == true) {
                return MtkIccCardConstants.VsimType.SOFT_AKA_SIM;
            } else {
                return MtkIccCardConstants.VsimType.PHYSICAL_AKA_SIM;
            }
        }

        if (rSim == -1 && akaSim == -1 && isVsim) {
            return MtkIccCardConstants.VsimType.LOCAL_SIM;
        }

        return MtkIccCardConstants.VsimType.PHYSICAL_SIM;
    }
    // External SIM [END]

    /**
     * Some feature will turn on modem when flight mode is on and the modem is off
     * so we have to listen the intent sent by RadioManager to trigger GET_SIM_STATUS.
     */
    private BroadcastReceiver mMdStateReceiver;

    private class ModemStateChangedReceiver extends BroadcastReceiver {
        public void onReceive(Context content, Intent intent) {
            String action = intent.getAction();
            if (action.equals(RadioManager.ACTION_MODEM_POWER_NO_CHANGE)) {
                for (int i = 0; i < mCis.length; i++) {
                    Integer index = new Integer(i);
                    Message msg = obtainMessage(EVENT_ICC_STATUS_CHANGED, index);
                    sendMessage(msg);
                    log("Trigger GET_SIM_STATUS due to modem state changed for slot " + i);
                }
            }
        }
    }
}
