/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/**
 * Android AssetMgrStreamSource
 *
 */

#ifndef PSS_ASSETMGRSTREAM_ANDROID_H
#define PSS_ASSETMGRSTREAM_ANDROID_H

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/stream.h>                 /* StreamSource API */

class AAssetManager;

namespace a3m
{
  /*
   * Concrete StreamSource class for obtaining streams residing in the "assets"
   * folder on Android
   */
  class AssetMgrStreamSource : public StreamSource
  {
  public:
    /*
     * Constructor
     */
    AssetMgrStreamSource(AAssetManager* mgr);

    /*
     * Destructor
     */
    virtual ~AssetMgrStreamSource();

    /*
     * Checks if a stream exists within this source.
     * Returns A3M_TRUE if it does, else A3M_FALSE.
     */
    virtual A3M_BOOL exists(const A3M_CHAR8* stream);

    /*
     * Opens a file stream for reading or writing.
     * Returns a smart pointer to a new archive stream.
     */
    virtual Stream::Ptr open(const A3M_CHAR8* stream
                             /* name of the stream to open */,
                             A3M_BOOL writable = A3M_FALSE
                                 /* A3M_TRUE = open a stream for writing
                                    A3M_FALSE = open stream for reading */);

    /*
     * Get the name of this StreamSource.
     *
     * Returns the name/path of the StreamSource as a NULL-terminated string
     */
    virtual A3M_CHAR8 const* getName() const { return m_name; }

  private:
    /* Maximum length for an absolute path + archive name */
    static const A3M_UINT32 MAX_NAME_LENGTH = 128;

    /* Local copy of the path+name of this ZIP archive */
    A3M_CHAR8 m_name[MAX_NAME_LENGTH];

    /* hold AssetManager information */
    AAssetManager* m_manager;   // asset manager from Android
  };
};

#endif
