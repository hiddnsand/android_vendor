/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.channellock;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.Intent;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import android.telephony.TelephonyManager;

import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.EditText;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.R;



public class ChannelLock extends Activity implements OnClickListener {

    private static final String TAG = "EM-ChannelLock";

    private static final String CMD_TURN_ON_RF = "AT+CFUN=1";
    private static final String CMD_TURN_OFF_RF = "AT+CFUN=4";
    private static final String CMD_TURN_OFF_RF_SIM = "AT+CFUN=0";
    private static final String CMD_RESET_MODEM = "AT+CFUN=1,1";
    private static final String CMD_PLMN_SEARCH = "AT+COPS=0";

    private static final int MSG_TURN_ON_RF = 1;
    private static final int MSG_TURN_OFF_RF = 2;
    private static final int MSG_TURN_OFF_RF_SIM = 3;
    private static final int MSG_RESET_MODEM = 4;
    private static final int MSG_PLMN_SEARCH = 5;
    private static final int MSG_CHANNEL_LOCK = 6;
    private static final int MSG_QUERY_CHANNEL_LOCK = 7;

    private Button mSetButton;
    private Button mSetButtonPLMN;
    private Button mSetButtonSoftReset;
    private Button mSetButtonReset;
    private Button mSetButtonApply;

    private RadioButton mRadioLockEnableButton;
    private RadioButton mRadioLockDisableButton;
    private RadioButton mRadioRatGSMButton;
    private RadioButton mRadioRatUMTSButton;
    private RadioButton mRadioRatLTEButton;
    private RadioButton mRadioGSM1900YesButton;
    private RadioButton mRadioGSM1900NoButton;

    private EditText mTextARFCNnumber;
    private EditText mTextCELLIDNnumber;

    private String mEMMCHLCKcommand = new String();

    private Phone mPhone = null;
    private int mSimType;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.channel_lock);
        mSetButton = (Button) findViewById(R.id.channellock_switch);

        mSetButton.setOnClickListener(this);

        mSetButtonPLMN = (Button) findViewById(R.id.channellock_cops_button);
        mSetButtonPLMN.setOnClickListener(this);

        mSetButtonApply = (Button) findViewById(R.id.channel_lock_apply_button );
        mSetButtonApply.setOnClickListener(this);

        mSetButtonSoftReset = (Button) findViewById(R.id.channellock_soft_reset_button );
        mSetButtonSoftReset.setOnClickListener(this);

        mSetButtonReset = (Button) findViewById(R.id.channellock_reset_button );
        mSetButtonReset.setOnClickListener(this);

        mRadioLockEnableButton = (RadioButton) findViewById(R.id.channel_lock_lock_enable_radio);
        mRadioLockDisableButton = (RadioButton) findViewById(R.id.channel_lock_lock_disable_radio);

        mRadioRatGSMButton = (RadioButton) findViewById(R.id.rat_gsm_radio);
        mRadioRatUMTSButton = (RadioButton) findViewById(R.id.rat_umts_radio);
        mRadioRatLTEButton = (RadioButton) findViewById(R.id.rat_lte_radio);

        mRadioGSM1900YesButton = (RadioButton) findViewById(R.id.gsm1900_yes_radio);
        mRadioGSM1900NoButton = (RadioButton) findViewById(R.id.gsm1900_no_radio);

        mTextARFCNnumber = (EditText) findViewById(R.id.channel_lock_ARCFN_number_text);
        mTextCELLIDNnumber = (EditText) findViewById(R.id.channel_lock_cellid_number_text);

        Intent intent = getIntent();
        mSimType = intent.getIntExtra("mSimType", PhoneConstants.SIM_ID_1);
        Elog.d(TAG, "mSimType: " + mSimType);
        mPhone = PhoneFactory.getPhone(mSimType);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String[] cmd = new String[2];
        cmd[0] = "AT+EMMCHLCK?";
        cmd[1] = "+EMMCHLCK:";
        sendATCommand(cmd,MSG_QUERY_CHANNEL_LOCK);
    }

    @Override
    public void onClick(final View arg0) {

        if (arg0 == mSetButton) {
            sendATCommand(new String[] {CMD_TURN_OFF_RF, ""}, MSG_TURN_OFF_RF);
        } else if (arg0 == mSetButtonSoftReset) {
            sendATCommand(new String[] {CMD_TURN_OFF_RF_SIM, ""}, MSG_TURN_OFF_RF_SIM);
        } else if (arg0 == mSetButtonReset) {
            sendATCommand(new String[] {CMD_RESET_MODEM, ""}, MSG_RESET_MODEM);
        } else if (arg0 == mSetButtonPLMN) {
            sendATCommand(new String[] {CMD_PLMN_SEARCH, ""}, MSG_PLMN_SEARCH);
        } else if (arg0 == mSetButtonApply) {
            mEMMCHLCKcommand = "AT+EMMCHLCK=";
            if ( mRadioLockEnableButton.isChecked()){
                // Start building the AT+EMMCHLCK= command
                // e.g. AT+EMMCHLCK=1,
                mEMMCHLCKcommand = mEMMCHLCKcommand + "1,";
                // Second parameter is the RAT
                // e.g. AT+EMMCHLCK=1,7,
                if ( mRadioRatGSMButton.isChecked() ){
                    mEMMCHLCKcommand = mEMMCHLCKcommand + "0,";
                } else if (mRadioRatUMTSButton.isChecked() ){
                    mEMMCHLCKcommand = mEMMCHLCKcommand + "2,";
                } else if (mRadioRatLTEButton.isChecked() ){
                    mEMMCHLCKcommand = mEMMCHLCKcommand + "7,";
                } else {
                    Toast.makeText(ChannelLock.this, "ERROR in RAT", Toast.LENGTH_SHORT).show();
                }

                // e.g. AT+EMMCHLCK=1,7,0,
                if ( mRadioGSM1900YesButton.isChecked() ){
                    mEMMCHLCKcommand = mEMMCHLCKcommand + "1,";
                } else if (mRadioGSM1900NoButton.isChecked() ){
                    mEMMCHLCKcommand = mEMMCHLCKcommand + "0,";
                } else {
                    Toast.makeText(ChannelLock.this, "ERROR in GSM1900", Toast.LENGTH_SHORT).show();
                }

                // e.g. AT+EMMCHLCK=1,7,0,10608
                mEMMCHLCKcommand = mEMMCHLCKcommand + mTextARFCNnumber.getText();

                // If CELLID is given, add parameter, if not do not add
                // e.g. AT+EMMCHLCK=1,7,0,10608,105
                if ( isNumeric( mTextCELLIDNnumber.getText().toString() ) ) {
                    mEMMCHLCKcommand = mEMMCHLCKcommand + ","
                                  + mTextCELLIDNnumber.getText().toString();
                }
            }else if ( mRadioLockDisableButton.isChecked() ){
                mEMMCHLCKcommand = mEMMCHLCKcommand + "0";
            }else {
                Toast.makeText(ChannelLock.this, "ERROR in Lock", Toast.LENGTH_SHORT).show();
            }
            sendATCommand(new String[] {mEMMCHLCKcommand, ""}, MSG_CHANNEL_LOCK);
        }

    }

    private void sendATCommand(String[] atCommand, int msg) {
         if (mPhone != null) {
             Elog.d(TAG, "sendATCommand " + atCommand[0]);
             mPhone.invokeOemRilRequestStrings(atCommand, mATCmdHander.obtainMessage(msg));
         }
     }

    private void handleQuery(String[] data) {
        if (null == data) {
            Toast.makeText(ChannelLock.this,
                    "The returned data is wrong.", Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        if (data[0].length() > "+EMMCHLCK:".length()) {
            String str = data[0].substring("+EMMCHLCK:".length()).trim();
            Elog.d(TAG, "the AT command response " + str);
            String[] arrayval = str.split(",");
            if (arrayval == null || arrayval.length < 1) {
                Elog.e(TAG, "Wrong AT command response");
                return;
            }
            if ((arrayval[0] != null ) && (arrayval[0].length() > 0)) {
                if (arrayval[0].equals("0")) {
                    mRadioLockDisableButton.setChecked(true);
                } else if (arrayval[0].equals("1")) {
                    mRadioLockEnableButton.setChecked(true);

                    if ((arrayval.length >= 2) && (arrayval[1] != null )
                        && (arrayval[1].length() > 0)) {
                       if (arrayval[1].equals("0")) {
                           mRadioRatGSMButton.setChecked(true);
                       } else if (arrayval[1].equals("2")) {
                           mRadioRatUMTSButton.setChecked(true);
                       } else if(arrayval[1].equals("7")) {
                           mRadioRatLTEButton.setChecked(true);
                       } else {
                           //Toast.makeText(ChannelLock.this, "Invalid Channel Lock RAT value",
                           //    Toast.LENGTH_SHORT).show();
                           Elog.e(TAG, "Invalid Channel Lock RAT value");
                       }
                    }

                    if((arrayval.length >= 3) && (arrayval[2] != null )
                        && (arrayval[2].length() > 0)) {
                        if (arrayval[2].equals("1")) {
                            mRadioGSM1900YesButton.setChecked(true);
                        } else if(arrayval[2].equals("0")) {
                            mRadioGSM1900NoButton.setChecked(true);
                        } else {
                            //Toast.makeText(ChannelLock.this, "Invalid Channel Lock GSM1900 value",
                            //    Toast.LENGTH_SHORT).show();
                            Elog.e(TAG, "Invalid Channel Lock GSM1900 value");
                        }
                    }

                    if((arrayval.length >= 4) && (arrayval[3] != null )
                        && (arrayval[3].length() > 0)) {
                        mTextARFCNnumber.setText(arrayval[3]);
                    }

                    if((arrayval.length >= 5) && (arrayval[4] != null )
                        && (arrayval[4].length() > 0)) {
                        int cellid = -1;
                        try {
                            cellid  = Integer.parseInt(arrayval[4]);
                        } catch (NumberFormatException e) {
                            Toast.makeText(ChannelLock.this,
                              "Query Cell ID value error", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if ( cellid < 0 || cellid > 511) {
                            Elog.e(TAG, "Invalid Cell ID value");
                        } else {
                            mTextCELLIDNnumber.setText(arrayval[4]);
                        }
                    }
                } else {
                  //Toast.makeText(ChannelLock.this, "Invalid Channel Lock value",
                  //    Toast.LENGTH_SHORT).show();
                  Elog.e(TAG, "Invalid Channel Lock value");
                }
            }
        } else {
            Elog.e(TAG, "The data returned is not right.");
        }
    }

    private final Handler mATCmdHander = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Elog.d(TAG, "handleMessage() " + msg.what);
            AsyncResult ar = (AsyncResult) msg.obj;
            switch (msg.what) {
            case MSG_TURN_OFF_RF:
                if (ar.exception != null) {
                    Elog.e(TAG, ar.exception.getMessage());
                    Toast.makeText(ChannelLock.this, "Failed to Turn OFF RF",
                        Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChannelLock.this, "Turn OFF RF success",
                        Toast.LENGTH_SHORT).show();
                    sendATCommand(new String[] {CMD_TURN_ON_RF, ""}, MSG_TURN_ON_RF);
                }
                break;
            case MSG_TURN_ON_RF:
                if (ar.exception != null) {
                    Elog.e(TAG, ar.exception.getMessage());
                    Toast.makeText(ChannelLock.this, "Failed to Turn ON RF",
                        Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChannelLock.this, "Turn ON RF success",
                        Toast.LENGTH_SHORT).show();
                }
                break;
            case MSG_TURN_OFF_RF_SIM:
                if (ar.exception != null) {
                    Elog.e(TAG, ar.exception.getMessage());
                    Toast.makeText(ChannelLock.this, "Failed to Turn OFF RF and SIM",
                        Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChannelLock.this, "Turn OFF RF and SIM success",
                        Toast.LENGTH_SHORT).show();
                    sendATCommand(new String[] {CMD_TURN_ON_RF, ""}, MSG_TURN_ON_RF);
                }
                break;
            case MSG_RESET_MODEM:
                if (ar.exception != null) {
                    Elog.e(TAG, ar.exception.getMessage());
                    Toast.makeText(ChannelLock.this, "Failed to Reset Modem",
                        Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChannelLock.this, "Reset Modem success",
                        Toast.LENGTH_SHORT).show();
                }
                break;
            case MSG_PLMN_SEARCH:
                if (ar.exception != null) {
                    Elog.e(TAG, ar.exception.getMessage());
                    Toast.makeText(ChannelLock.this, "Failed to send AT+COPS=0",
                        Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChannelLock.this, "send AT+COPS=0 success",
                        Toast.LENGTH_SHORT).show();
                }
                break;
            case MSG_CHANNEL_LOCK:
                if (ar.exception != null) {
                    Elog.e(TAG, ar.exception.getMessage());
                    Toast.makeText(ChannelLock.this, "Failed to set Channel Lock",
                        Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ChannelLock.this, "set Channel Lock success",
                        Toast.LENGTH_SHORT).show();
                }
                break;
            case MSG_QUERY_CHANNEL_LOCK:
                ar = (AsyncResult) msg.obj;
                if (ar.exception == null) {
                    Elog.i(TAG, "Query success.");
                    String[] data = (String[]) ar.result;
                    handleQuery(data);
                } else {
                    Toast.makeText(ChannelLock.this, "Query failed.",
                            Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
            }
        }
    };

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch(NumberFormatException nfe) {
            return false;
        }
        return true;
    }

}
