/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
#include "soliddialog.h"        /* Class header */
#include <a3m/solid.h>          /* inspect objects of class Solid */
#include <a3m/texture2d.h>      /* To find dimensions of texture uniforms */

#include "resource.h"  /* Dialog resource identifier */

SolidDialog::SolidDialog( HWND parentWindowHandle )
  : ViewerDialog( parentWindowHandle, IDD_SOLID ),
    m_solid( 0 ),
    m_uniform( 0 )
{
}

void SolidDialog::init( a3m::Solid *solid )
{
  m_solid = solid;
  m_uniform = 0;

  // Clear uniform list
  HWND hwndList = GetDlgItem( hwnd(), IDC_UNIFORMS );
  SendMessage( hwndList, LB_RESETCONTENT, 0, 0 );

  // Populate uniform list
  solid->getAppearance()->collectProperties( this );

  // Hide all the Uniform controls until one is selected
  static const A3M_UINT32 controls[] =
    { IDC_UNI1, IDC_UNI2, IDC_UNI3, IDC_UNI4, IDC_COLOUR, IDC_COLOUR_LABEL };

  for( A3M_UINT32 i = 0; i != 6; ++i )
  {
    setCtlVisible( controls[i], A3M_FALSE );
  }
}

A3M_BOOL SolidDialog::collect(
    a3m::ShaderUniformBase::Ptr const& uniform,
    A3M_CHAR8 const* property,
    A3M_INT32 index )
{
  HWND hwndList = GetDlgItem( hwnd(), IDC_UNIFORMS );

  A3M_INT32 pos = (A3M_INT32)SendMessage( hwndList, LB_ADDSTRING, 0,
      (LPARAM) property );
  SendMessage(hwndList, LB_SETITEMDATA, pos, (LPARAM) uniform.get() );

  return A3M_TRUE;
}

void SolidDialog::update()
{
  static const A3M_UINT32 controls[] =
    { IDC_UNI1, IDC_UNI2, IDC_UNI3, IDC_UNI4 };

  if( !m_uniform )
  {
    return;
  }

  A3M_UINT32 fields_used = 0;

  a3m::Vector4f colour;
  if( m_uniform->getValue( colour ) )
  {
    setEditText( IDC_UNI1, IDC_UNI2, IDC_UNI3, IDC_UNI4, colour );
    fields_used = 4;
    setEditColour( IDC_COLOUR, colour );
    setCtlVisible( IDC_COLOUR_LABEL, A3M_TRUE );
    setCtlVisible( IDC_COLOUR, A3M_TRUE );
  }
  else
  {
    setCtlVisible( IDC_COLOUR_LABEL, A3M_FALSE );
    setCtlVisible( IDC_COLOUR, A3M_FALSE );
  }

  A3M_FLOAT val;
  if( m_uniform->getValue( val ) )
  {
    setEditText( IDC_UNI1, val );
    fields_used = 1;
  }

  a3m::Texture2D::Ptr tex;

  if( m_uniform->getValue( tex ) )
  {
    if ( !tex )
    {
      setEditText( IDC_UNI1, " Missing! " );
      fields_used = 1;
    }
    else
    {
      setEditText( IDC_UNI1, static_cast< A3M_INT32 >( tex->getWidth() ) );
      setEditText( IDC_UNI2, static_cast< A3M_INT32 >( tex->getHeight() ) );
      fields_used = 2;
    }
  }

  // Show controls being used and hide the rest
  for( A3M_UINT32 i = 0; i != fields_used; ++i )
  {
    setCtlVisible( controls[i], A3M_TRUE );
  }

  for( A3M_UINT32 i = fields_used; i != 4; ++i )
  {
    setCtlVisible( controls[i], A3M_FALSE );
  }
}

void SolidDialog::onCommand( A3M_INT32 control,
                             A3M_INT32 event,
                             A3M_INT32 lparam )
{
  if( ( control == IDC_UNIFORMS ) &&
      ( event == LBN_SELCHANGE ) &&
      m_solid )
  {
    HWND hwndList = GetDlgItem( hwnd(), IDC_UNIFORMS );
    A3M_INT32 lbItem = (A3M_INT32)SendMessage( hwndList, LB_GETCURSEL, 0, 0 );
    m_uniform = reinterpret_cast< a3m::ShaderUniformBase *> (
      SendMessage(hwndList, LB_GETITEMDATA, lbItem, 0) );

    update();
  }
}
