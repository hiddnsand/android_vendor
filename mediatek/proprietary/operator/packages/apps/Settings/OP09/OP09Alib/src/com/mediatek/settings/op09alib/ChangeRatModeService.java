/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;

import com.mediatek.internal.telephony.MtkGsmCdmaPhone;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.telephony.MtkTelephonyManagerEx;

/**
* Switch rat mode.
*/
public class ChangeRatModeService extends Service {

    private static final String TAG = "ChangeRatModeService";
    private static final String MODE_STATUS = "mode_status";
    private static final String LTETDD_CDMA = "ltetdd_cdma";

    private final IBinder mSetRatMode = new SetRatMode();
    private MtkTelephonyManagerEx mTelephonyManagerEx = null;

    @Override
    public void onCreate() {
        Log.i(TAG, "ChangeRatModeService onCreate");
        mTelephonyManagerEx = MtkTelephonyManagerEx.getDefault();
    }
    @Override
    public void onDestroy() {
        Log.i(TAG, "ChangeRatModeService onDestroy");
    }

    @Override
    public int onStartCommand(Intent intent, int flag, int startId) {
        Log.i(TAG, "ChangeRatModeService start");
        try {
            saveData(true);
            switchSvlte(true);
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            stopSelf(startId);
        }
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mSetRatMode;
    }
    /**
    *Binder for client.
    */
    public class SetRatMode extends Binder {
        public ChangeRatModeService getService() {
            return ChangeRatModeService.this;
        }
    }
    /**
    * Save RAT mode.
    * @param isChecked open or close the 4G switch.
    * @throws RemoteException remote exception
    */
    public void saveData(boolean isChecked) throws RemoteException {
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
        if (!isChecked) {
            int lastMode = Settings.Global.getInt(this.getContentResolver(),
                                    Settings.Global.PREFERRED_NETWORK_MODE + subId,
                                    Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
            Log.i(TAG, "saveData isChecked = false lastMode = " + lastMode);
            this.getSharedPreferences(MODE_STATUS, Context.MODE_PRIVATE)
                .edit().putInt(LTETDD_CDMA, lastMode).commit();
            Settings.Global.putInt(this.getContentResolver(),
                        Settings.Global.PREFERRED_NETWORK_MODE + subId,
                        Phone.NT_MODE_GLOBAL);
        } else {
            int lteCdma = this.getSharedPreferences(MODE_STATUS, Context.MODE_PRIVATE)
                .getInt(LTETDD_CDMA, -1);
            Log.i(TAG, "saveData isChecked = true lteCdma = " + lteCdma);
            if (lteCdma != -1) {
                Settings.Global.putInt(this.getContentResolver(),
                        Settings.Global.PREFERRED_NETWORK_MODE + subId,
                        lteCdma);
            } else {
                Settings.Global.putInt(this.getContentResolver(),
                        Settings.Global.PREFERRED_NETWORK_MODE + subId,
                        Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
            }
        }
    }
    /**
    * Switch RAT mode.
    * @param subId sub id
    * @param networkType network type
    */
    private void setPreferredNetworkType(int subId, int networkType) {
        Thread changeNetworkType = new Thread(new ChangeNetworkType(subId,
                                                                   networkType,
                                                                   this));
        changeNetworkType.start();
    }
    /**
    * Switch RAT mode.
    * @param isChecked open or close the 4G switch.
    * @throws RemoteException remote exception
    */
    public void switchSvlte(boolean isChecked) {//throws RemoteException {
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
        ITelephony telephony = ITelephony.Stub.asInterface(ServiceManager
                .getService(Context.TELEPHONY_SERVICE));

        if (null != telephony) {
            if (!isChecked) {
                Log.i(TAG, "switchSvlte isChecked = false switchSvlteRatMode 3g");
                setPreferredNetworkType(subId, Phone.NT_MODE_GLOBAL);
                Log.i(TAG, "switchSvlte setPreferredNetworkType 1, value= " +
                        Phone.NT_MODE_GLOBAL);
            } else {
                int lteCdma = this.getSharedPreferences(MODE_STATUS,
                        Context.MODE_PRIVATE).getInt(LTETDD_CDMA, -1);

                Log.i(TAG, "switchSvlte isChecked = true last mode lte_cdma = " + lteCdma);
                if (lteCdma != -1) {
                    Log.i(TAG, "switchSvlte saveData isChecked = true put LTETDD_CDMA = -1");
                    this.getSharedPreferences(MODE_STATUS, Context.MODE_PRIVATE)
                        .edit().putInt(LTETDD_CDMA, -1).commit();

                    if (lteCdma == MtkGsmCdmaPhone.NT_MODE_LTE_TDD_ONLY) {
                        Log.i(TAG, "switchSvlte last mode SVLTE_RAT_MODE_4G_DATA_ONLY");
                        setPreferredNetworkType(subId, MtkGsmCdmaPhone.NT_MODE_LTE_TDD_ONLY);
                        Log.i(TAG, "switchSvlte 4G_DATA_ONLY, value= " +
                            MtkGsmCdmaPhone.NT_MODE_LTE_TDD_ONLY);
                    } else if (lteCdma == Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA) {
                        Log.i(TAG, "switchSvlte last mode SVLTE_RAT_MODE_4G");
                        setPreferredNetworkType(subId, Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
                        Log.i(TAG, "switchSvlte RAT_MODE_4G, value= " +
                                                Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
                    } else {
                        Log.i(TAG, "switchSvlte last mode error, do open 4G mode");
                        setPreferredNetworkType(subId, Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
                        Log.i(TAG, "switchSvlte mode error, value= " +
                                Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
                    }
                } else {
                    setPreferredNetworkType(subId, Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
                    Log.i(TAG, "switchSvlte setPreferredNetworkType 2, value= " +
                            Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
                }
            }
        }
    }

}
