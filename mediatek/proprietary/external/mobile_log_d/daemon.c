#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <time.h>
#include <dirent.h>
#include <fcntl.h>
#include <termios.h>
#include <log/log.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/eventfd.h>
#include <cutils/properties.h>
#include <cutils/sockets.h>

#include "global_var.h"
#include "config.h"
#include "mlog.h"
#include "daemon.h"
#include "bootmode.h"
#include "libfunc.h"
#include "size_control.h"
#include "config.h"
#include "logging.h"
#include "dump.h"

extern void* packlog_to_com(void*);

uint64_t STORAGE_FULL_EVENT = 1001;
uint64_t FOLDER_REMOVED_EVENT = 1002;
uint64_t SIG_STOPLOG_EVENT = 1003;
int event_fd;

struct client {
	int fd;
	struct sockaddr addr;
};

struct client_msg {
	int fd;
	char msg[64];
};

int g_redirect_flag = 0;

int make_full_path()
{
	int len;
	char path[255],curTime[255], curFolderName[255];
	struct tm *tmp;
	time_t lt;

	FILE *fp;
	char fileTreePath[255];
	unsigned long index; // folder index
	sd_type_t logSd; // sdcard type used to store log

	// get mobilelog path
	memset(path, 0x00, sizeof(path));
	snprintf(path, sizeof(path), "%s", sdContext.sdpath);

	len = strlen(path);
	if (path[len-1] != '/') {
		path[len] = '/';
		path[len+1] = '\0';
	}
	strncat(path, PATH_SUFFIX, sizeof(path) - 1 - len); // ../mtklog/mobilelog/
	snprintf(fileTreePath, sizeof(fileTreePath), "%s", path); // copy to file tree path

	// create current APlog folder, Name Format: APLog_index__timestamp
	lt = time(NULL);
	tmp = localtime(&lt);
	memset(curTime, 0x0 ,sizeof(curTime));
	strftime(curTime, sizeof(curTime), "%Y_%m%d_%H%M%S", tmp);

	logSd = sdContext.location;
	index = read_folder_index(logSd);
	if (index < 10000)
		index++;
	else {
		MLOGE_BOTH("index is %lu, reset to 0", index);
		index = 0;
	}
	snprintf(curFolderName, sizeof(curFolderName), "APLog_%s__%lu/", curTime, index);
	len = strlen(path);
	strncat(path, curFolderName, sizeof(path) - 1 - len);

	if (!create_dir(path))
		return 0;

	/* Record the current logging folder 'path' to file_tree.txt */
	strncat(fileTreePath, "file_tree.txt", sizeof(fileTreePath) - 1 - strlen(fileTreePath));
	if ((fp = fopen(fileTreePath, "a+"))) {
		fputs(path, fp);
		fputs("\n", fp);
		fclose(fp);
	}

	MLOGI_BOTH("log dir: %s", path);

	// update folder list
	curFolderName[strlen(curFolderName)-1] = '\0';
	update_folder_list(curFolderName, index, logSd);

	set_cur_logpath(path);
	return 1;
}


void maybe_config_msg(char *msg)
{
	char *p = NULL;
	char key[128];
	char value[128];

	if (strlen(msg) > sizeof(key)) {
		MLOGE_BOTH("msg too long!");
		abort();
	}
	snprintf(key, sizeof(key), "%s", msg);
	if ((p = strchr(key, '=')) != NULL) {
		*p = '\0';
		snprintf(value, sizeof(value), "%s", p + 1);
		update_config_value(key, value);
	} else if ((p = strchr(key, ',')) != NULL) {
		snprintf(value, sizeof(value), "%s", p + 1);
		if (strstr(value, "storage") != NULL) {
			update_sd_context(value);
			property_set(PROP_OLDPATH, value);
		}
	} else {
		MLOGE_BOTH("%s not a config msg", msg);
		return;
	}
}

int set_redirect_flag()
{
	g_redirect_flag = 1 - g_redirect_flag;

	return 1;
}

void handle_client_msg(void *cli_msg)
{
	prctl(PR_SET_NAME,"mobile_log_d.mg");
	int i,ret,fd;
	char msg[128];
	char ans[128];
	struct client_msg climsg;

	MLOGI_BOTH("thread %d handle msg", gettid());
	climsg = *(struct client_msg*)cli_msg;
	free(cli_msg);

	fd = climsg.fd;
	memset(msg, 0x0, sizeof(msg));
	snprintf(msg, sizeof(msg), "%s", climsg.msg);

	if (!strncmp(msg, "deep_start",10)) {
		if (is_mblog_running()) {
			if (g_mblog_status == SAVE_TO_BOOTUP) { /* running and saving to /data/log_temp */
				set_status(SAVE_TO_SDCARD);
				ret = make_full_path();
				if (ret == 0) {
					MLOGE_DATA("make full path failed");
					notify_client(fd, "die");
					return;
				}
				set_redirect_flag();
				MLOGI_BOTH("redirect to sdcard");
				copy_and_dump();
			} else
				MLOGE_BOTH("mblog already running ,why send deep_start?");
			notify_client(fd,"deep_start,1");
			return;
		}

		MLOGI_BOTH("'deepstart' mblog");
		set_status(SAVE_TO_SDCARD);
		ret = make_full_path();
		if (ret == 0) {
			MLOGE_DATA("make full path failed");
			notify_client(fd, "die");
			return;
		}

		copy_and_dump();
		prepare_logging();

		update_config_value(KEY_BOOT,ENABLE);
	} else if (!strncmp(msg, "start", 5)) {
		if (is_mblog_running()) {
			if (g_mblog_status == SAVE_TO_BOOTUP) { /* running and saving to /data/log_temp */
				set_status(SAVE_TO_SDCARD);
				ret = make_full_path();
				if (ret == 0) {
					MLOGE_DATA("make full path failed");
					notify_client(fd, "die");
					return;
				}
				set_redirect_flag();
				MLOGI_BOTH("redirect to sdcard");
				copy_and_dump();
			} else
				MLOGE_BOTH("mblog already running ,why send start?");
			notify_client(fd,"start,1");
			return;
		}

		MLOGI_BOTH("'start' mblog");
		set_status(SAVE_TO_SDCARD);
		ret = make_full_path();
		if (ret == 0) {
			MLOGE_DATA("make full path failed");
			notify_client(fd, "die");
			return;
		}

		copy_and_dump();
		prepare_logging();
	} else if (!strncmp(msg, "deep_stop", 9)) {
		if (!is_mblog_running()) {
			MLOGE_BOTH("mblog already stop ,why send deep_stop?");
			notify_client(fd,"deep_stop,1");
			return;
		}

		set_status(STOPPED);
		g_redirect_flag = 0;
		update_config_value(KEY_BOOT, DISABLE);
		for (i=0; i<15; i++) {
			if (is_mblog_running()) //mobilelog is still running
				sleep(1);
			else
				break;
		}
		if (i == 15)
			return;
	} else if (!strncmp(msg, "stop", 4)) {
		if (!is_mblog_running()) {
			MLOGE_BOTH("mblog already stop ,why send stop?");
			notify_client(fd,"stop,1");
			return;
		}

		set_status(STOPPED);
		g_redirect_flag = 0;
		for (i=0; i<15; i++) {
			if (is_mblog_running()) //mobilelog is still running
				sleep(1);
			else
				break;
		}
		if (i == 15)
			return;
	} else if (!strncmp(msg, "copy", 4)) {
		if (g_mblog_status == SAVE_TO_SDCARD) {
			MLOGE_BOTH("alreay save to sdcard, why send copy again?");
			notify_client(fd,"copy,1");
			return;
		}
		set_status(SAVE_TO_SDCARD);
		ret = make_full_path();
		if (ret == 0) {
			MLOGE_DATA("make full path failed");
			notify_client(fd, "die");
			set_status(STOPPED);
			return;
		}
		if (!is_mblog_running()) {
			MLOGI_BOTH("start log when copy");
			prepare_logging();
		} else
			set_redirect_flag();

		copy_and_dump();
	} else if (!strncmp(msg, "IPO_SHUTDOWN", 12)) {
		if (g_mblog_status == STOPPED) {
			MLOGE_BOTH("mobilelog should stopped!");
			return;
		}
		set_status(SAVE_TO_BOOTUP);
		create_dir(PATH_IPO);
		set_cur_logpath(PATH_IPO);
		set_redirect_flag();

	} else if (!strncmp(msg, "ext_autopull", 12)) {
	//} else if (strstr(msg, "stop")) {
		if (is_mblog_running()) {
			MLOGI_BOTH("mobileLog is running, stop logging first...");
			set_status(STOPPED);
			g_redirect_flag = 0;
			for (i=0; i<15; i++) {
				if (is_mblog_running()) //mobilelog is still running
					sleep(1);
				else
					break;
			}
			if (i == 15) {
				MLOGE_BOTH("Can not stop, failed to 'ext_autopull'");
				return;
			}
		}
		// pack log and send to com port
		pthread_t thread_id;
		pthread_attr_t attr;

		if (!pthread_attr_init(&attr)) {
			if (!pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED)) {
				if (!pthread_create(&thread_id, &attr, packlog_to_com, NULL))
					pthread_attr_destroy(&attr);
				else
					MLOGE_BOTH("create pach_log thread fail(%s)",strerror(errno));
			}
		}

	} else {
		maybe_config_msg(msg);
	}
	memset(ans,0x0,sizeof(ans));
	snprintf(ans, sizeof(ans), "%s,1", msg);
	notify_client(fd,ans);
}

/*
 * When recv a msg from client, create a new thread to handle it
 */
pthread_mutex_t client_lock = PTHREAD_MUTEX_INITIALIZER;
void wait_client(void *socket_id)
{
	prctl(PR_SET_NAME,"mobile_log_d.wc");
#define MAX_CLIENT_NUM          10

	struct client clients[MAX_CLIENT_NUM];
	int sock_id,max_fd = -1,ret,i,e;
	int should_timeout;
	fd_set read_fds;
	struct stat fd_state;
	char msg[255];
	struct timeval timeout = {600,0};

	for (i=0; i<MAX_CLIENT_NUM; i++) {
		clients[i].fd = -1;
	}

	event_fd = eventfd(0, EFD_NONBLOCK);

	sock_id = *(int*)socket_id;

	should_timeout = timeout_in_this_boot();
	while (1) {
		FD_ZERO(&read_fds);
		if (sock_id > 0) {
			FD_SET(sock_id, &read_fds);
			max_fd = sock_id;
		}

		if (event_fd > 0) {
			FD_SET(event_fd, &read_fds);
			if (event_fd > max_fd)
				max_fd = event_fd;
		}

		pthread_mutex_lock(&client_lock);
		for (i=0; i<MAX_CLIENT_NUM; i++) {
			if (clients[i].fd > 0) {
				FD_SET(clients[i].fd, &read_fds);
				if (max_fd < clients[i].fd)
					max_fd = clients[i].fd;
			}
		}
		pthread_mutex_unlock(&client_lock);

		ret = select(max_fd+1, &read_fds, NULL, NULL, should_timeout? &timeout: NULL);
		e = errno;
		if (ret < 0) {
			MLOGE_BOTH("select fail(%s)",strerror(e));
			for (i=0; i<MAX_CLIENT_NUM; i++) {
				if (clients[i].fd > 0) {
					if (fstat(clients[i].fd, &fd_state) < 0) {
						e = errno;
						clients[i].fd = -1;
						MLOGE_BOTH("reset client %d to -1: %s", clients[i].fd, strerror(e));
					}
				}
			}
			continue;
		} else if (!ret) {
			MLOGE_DATA("select timeout,try copy to sd");
			set_status(SAVE_TO_SDCARD);
			update_sd_context(PATH_DEFAULT); // default path is /sdcard/ (internal sd path)
			ret = make_full_path();
			if (ret == 0) {
				MLOGE_DATA("make full path failed,stop logging");
				set_status(STOPPED);
			} else {
				set_redirect_flag();
				copy_and_dump();
			}
			should_timeout = 0;
			continue;
		}

		if (FD_ISSET(sock_id, &read_fds)) {
			struct sockaddr addr;
			socklen_t alen = sizeof(addr);
			int client_fd;

			client_fd = accept(sock_id, &addr, &alen);
			if (client_fd < 0) {
				MLOGE_BOTH("accept from %d failed(%s)", sock_id, strerror(errno));
				continue;
			}

			MLOGI_BOTH("new client(%d) conected!", client_fd);
			pthread_mutex_lock(&client_lock);
			for (i=0; i<MAX_CLIENT_NUM; i++) {
				if (clients[i].fd < 0) {
					clients[i].fd = client_fd;
					clients[i].addr = addr;
					break;
				}
			}
			pthread_mutex_unlock(&client_lock);

			should_timeout = 0;

			if (i==MAX_CLIENT_NUM) {
				MLOGE_BOTH("clients full ,cant accept a new one.");
			}
		} else {
			for (i=0; i<MAX_CLIENT_NUM; i++) {
				int fd = clients[i].fd;
				int e;

				if (fd < 0 || !FD_ISSET(fd,&read_fds))
					continue;

				char temp[255] = {0};
				ret = TEMP_FAILURE_RETRY(recv(fd, temp, sizeof(temp)-1, 0));
				e = errno;
				if (ret <= 0) {
					if (ret == 0)
						MLOGE_BOTH("recv returned 0 from client %d(%s)", fd, strerror(e));
					if (ret < 0)
						MLOGE_BOTH("read error from client %d(%s)", fd, strerror(e));
					MLOGI_BOTH("close client %d",fd);
					close(clients[i].fd);
					clients[i].fd = -1;
					memset((void *)&(clients[i].addr), 0x0, sizeof(struct sockaddr));
					continue;
				}

				MLOGI_BOTH("recv %s from %d",temp,fd);

				struct client_msg *cli_msg = (struct client_msg*)malloc(sizeof(struct client_msg));
				if (cli_msg == NULL) {
					MLOGE_BOTH("create cli_msg fail, %s", strerror(errno));
					continue;
				}
				memset(cli_msg, 0x0, sizeof(struct client_msg));
				cli_msg->fd = fd;
				strncpy(cli_msg->msg, temp, sizeof(cli_msg->msg)-1);
				pthread_t handle_client_t;
				pthread_create(&handle_client_t, NULL, (void *)handle_client_msg, cli_msg);
				pthread_join(handle_client_t, NULL);
			}
		}

		if (FD_ISSET(event_fd, &read_fds)) {
			int e, ret;
			uint64_t u = 0;
			ret = TEMP_FAILURE_RETRY(read(event_fd, &u, sizeof(u)));
			e = errno;
			if (ret > 0) {
				MLOGI_BOTH("read %llu from event_fd", (unsigned long long)u);
				if (u == STORAGE_FULL_EVENT) {
					for (i=0; i<MAX_CLIENT_NUM; i++) {
						int fd = clients[i].fd;
						if (fd>0) {
							notify_client(fd, "storage_full");
							close(clients[i].fd);
							clients[i].fd = -1;
							memset((void *)&(clients[i].addr), 0x0, sizeof(struct sockaddr));
						}
					}
				} else if (u == FOLDER_REMOVED_EVENT) {
					while (is_mblog_running()) usleep(100000);
					MLOGI_BOTH("restoring mobilelog...");
					set_status(SAVE_TO_SDCARD);
					if (!make_full_path()) {
						MLOGE_DATA("make full path failed");
						for (i=0; i<MAX_CLIENT_NUM; i++) {
							int fd = clients[i].fd;
							if (fd > 0)
								notify_client(fd, "die");
						}
						continue;
					}
					copy_and_dump();
					prepare_logging();
				} else if (u == SIG_STOPLOG_EVENT) { // stop logging
					MLOGI_BOTH("SIG_STOPLOG_EVENT handler...");
					if (!is_mblog_running()) {
						MLOGI_BOTH("MobiLog already stop, ignore SIG_EVENT.");
						continue;
					}
					set_status(STOPPED);
					g_redirect_flag = 0;
					for (i=0; i<15; i++) {
						if (is_mblog_running()) //mobilelog is still running
							sleep(1);
						else
							break;
					}
					if (i == 15)
						MLOGE_BOTH("Can not stop, handle SIG_EVENT Fail.");
					MLOGI_BOTH("SIG_STOPLOG_EVENT handler...end...");
				}
			} else
				MLOGE_BOTH("read error from event_fd(%s)", strerror(e));
		}
	}
}

/*
 * Create server socket and start a thread to listen
 */
int setup_socket()
{
	int ret;
	static int socket_id = 0;
	pthread_t thread_id;
	pthread_attr_t attr;

	socket_id = socket_local_server("mobilelogd", ANDROID_SOCKET_NAMESPACE_ABSTRACT, SOCK_STREAM);
	if (socket_id < 0) {
		MLOGE_DATA("create server fail(%s).",strerror(errno));
		return 0;
	}

	if ((ret = listen(socket_id,MAX_CLIENT_NUM)) < 0) {
		MLOGE_DATA("listen socket fail(%s).",strerror(errno));
		close(socket_id);
		return 0;
	}

	if (!pthread_attr_init(&attr)) {
		if (!pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED)) {
			if (!pthread_create(&thread_id, &attr, (void *)wait_client,(void *)&socket_id))
				pthread_attr_destroy(&attr);
			else {
				MLOGE_BOTH("create thread fail(%s)",strerror(errno));
				close(socket_id);
			}
		}
	}
	return 1;
}
