package com.mediatek.browser.plugin;

import android.content.Context;

import com.mediatek.browser.ext.IBrowserBookmarkExt;
import com.mediatek.browser.ext.IBrowserDownloadExt;
import com.mediatek.browser.ext.IBrowserHistoryExt;
import com.mediatek.browser.ext.IBrowserMiscExt;
import com.mediatek.browser.ext.IBrowserSettingExt;
import com.mediatek.browser.ext.IBrowserSiteNavigationExt;
import com.mediatek.browser.ext.IBrowserUrlExt;
import com.mediatek.browser.ext.OpBrowserCustomizationFactoryBase;

public class Op01BrowserCustomizationFactory extends OpBrowserCustomizationFactoryBase {
    private Context mContext;

    public Op01BrowserCustomizationFactory(Context context) {
        mContext = context;
	}
	
    @Override
    public IBrowserBookmarkExt makeBrowserBookmarkExt() {
        android.util.Log.d("YifanTest", "new plugin framework!");
        return new Op01BrowserBookmarkExt(mContext);
    }
    @Override
    public IBrowserDownloadExt makeBrowserDownloadExt() {
        return new Op01BrowserDownloadExt(mContext);
    }
    @Override
    public IBrowserHistoryExt makeBrowserHistoryExt() {
        return new Op01BrowserHistoryExt(mContext);
    }
    @Override
    public IBrowserMiscExt makeBrowserMiscExt() {
        return new Op01BrowserMiscExt();
    }
    @Override
    public IBrowserSettingExt makeBrowserSettingExt() {
        return new Op01BrowserSettingExt(mContext);
    }
    @Override
    public IBrowserSiteNavigationExt makeBrowserSiteNavigationExt() {
        return new Op01BrowserSiteNavigationExt(mContext);
    }
    @Override
    public IBrowserUrlExt makeBrowserUrlExt() {
        return new Op01BrowserUrlExt(mContext);
    }   
}