/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * A3M Render block group
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/renderblockgroup.h>  /* Class declaration */

namespace a3m
{
  /*
   * Default constructor
   */
  RenderBlockGroup::RenderBlockGroup() : m_zFocal(0.f), m_eyeSep(0.f)
  {
  }

  /*
   * Add block to group
   */
  void RenderBlockGroup::addBlock( SharedPtr< RenderBlockBase > const& block )
  {
    block->setStereo( m_zFocal, m_eyeSep );
    m_blocks.push_back( block );
  }

  /*
   * Remove block from group
   */
  void RenderBlockGroup::removeBlock( SharedPtr< RenderBlockBase > const& block )
  {
    m_blocks.erase( std::remove( m_blocks.begin(), m_blocks.end(), block ),
                    m_blocks.end() );
  }

  /*
   * Render this block
   */
  void RenderBlockGroup::render()
  {
    std::vector< SharedPtr< RenderBlockBase > >::iterator block;
    for( block = m_blocks.begin(); block != m_blocks.end(); ++block )
    {
      (*block)->render();
    }
  }

  /*
   * Update this block
   */
  void RenderBlockGroup::update( A3M_FLOAT timeInSeconds )
  {
    std::vector< SharedPtr< RenderBlockBase > >::iterator block;
    for( block = m_blocks.begin(); block != m_blocks.end(); ++block )
    {
      (*block)->update(timeInSeconds);
    }
  }

  /*
   * Set stereoscopic parameters for camera
   */
  void RenderBlockGroup::setStereo( A3M_FLOAT zFocal, A3M_FLOAT eyeSep )
  {
    m_zFocal = zFocal;
    m_eyeSep = eyeSep;
    std::vector< SharedPtr< RenderBlockBase > >::iterator block;
    for( block = m_blocks.begin(); block != m_blocks.end(); ++block )
    {
      (*block)->setStereo(zFocal, eyeSep);
    }
  }
} /* namespace a3m */
