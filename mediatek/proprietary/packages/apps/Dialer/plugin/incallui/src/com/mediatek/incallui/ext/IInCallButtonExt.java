package com.mediatek.incallui.ext;

import android.os.Bundle;

public interface IInCallButtonExt {

    /**
     * Checks if contact is video call capable through presence
     * @param number number to get video capability.
     * @return true if contact is video call capable.
     */
    boolean isVideoCallCapable(String number);

    /**
     * Show toast for GTT feature.
     * @param extra extra.
     */
    void showToastForGTT(Bundle extra);
}
