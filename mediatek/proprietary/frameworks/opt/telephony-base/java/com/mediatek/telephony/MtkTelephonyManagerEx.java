/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.telephony;

import android.app.ActivityThread;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;

import android.telephony.CellLocation;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.IPhoneSubInfo;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.ITelephonyRegistry;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyProperties;

import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.telephony.Rlog;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import java.util.ArrayList;
import java.util.List;

import com.mediatek.internal.telephony.IMtkTelephonyEx;
import com.mediatek.internal.telephony.IMtkPhoneSubInfoEx;
import com.mediatek.internal.telephony.MtkIccCardConstants;

// [OMH] @{
import com.mediatek.internal.telephony.MmsConfigInfo;
import com.mediatek.internal.telephony.MmsIcpInfo;
import com.mediatek.internal.telephony.MtkPhoneNumberUtils.EccEntry;
// [OMH] @}
import com.mediatek.internal.telephony.PseudoCellInfo;

/**
 * Provides access to information about the telephony services on
 * the device, especially for multiple SIM cards device.
 *
 * Applications can use the methods in this class to
 * determine telephony services and states, as well as to access some
 * types of subscriber information. Applications can also register
 * a listener to receive notification of telephony state changes.
 *
 * Note that access to some telephony information is
 * permission-protected. Your application cannot access the protected
 * information unless it has the appropriate permissions declared in
 * its manifest file. Where permissions apply, they are noted in the
 * the methods through which you access the protected information.
 */
public class MtkTelephonyManagerEx {
    private static final String TAG = "MtkTelephonyManagerEx";

    private Context mContext = null;
    private ITelephonyRegistry mRegistry;
    private static final String PRLVERSION = "cdma.prl.version";

    //MTK-START: SIM
    /** @hide */
    public static final int APP_FAM_NONE = 0x00;
    /** @hide */
    public static final int APP_FAM_3GPP = 0x01;
    /** @hide */
    public static final int APP_FAM_3GPP2 = 0x02;
    /** @hide */
    public static final int CARD_TYPE_NONE = 0x00;
    /** @hide */
    public static final int CARD_TYPE_SIM = 0x01;
    /** @hide */
    public static final int CARD_TYPE_USIM = 0x02;
    /** @hide */
    public static final int CARD_TYPE_CSIM = 0x04;
    /** @hide */
    public static final int CARD_TYPE_RUIM = 0x08;

    // [SIM-C2K] @{
    /**
     * The property is used to get supported card type of each SIM card in the slot.
     */
    private static final String[] PROPERTY_RIL_FULL_UICC_TYPE  = {
        "gsm.ril.fulluicctype",
        "gsm.ril.fulluicctype.2",
        "gsm.ril.fulluicctype.3",
        "gsm.ril.fulluicctype.4",
    };

    /**
     * The property is used to check if the card is cdma 3G dual mode card in the slot.
     */
    private static final String[] PROPERTY_RIL_CT3G = {
        "gsm.ril.ct3g",
        "gsm.ril.ct3g.2",
        "gsm.ril.ct3g.3",
        "gsm.ril.ct3g.4",
    };

    /**
     * The property shows cdma card type that is only for cdma card.
     */
    private static final String[] PROPERTY_RIL_CDMA_CARD_TYPE = {
        "ril.cdma.card.type.1",
        "ril.cdma.card.type.2",
        "ril.cdma.card.type.3",
        "ril.cdma.card.type.4",
    };
    // [SIM-C2K] @}

    /// M: CC: ECC is in progress @{
    /**
     * Intent to notify other modules ECC is in progress.
     * @hide
     */
    public static final String ACTION_ECC_IN_PROGRESS = "android.intent.action.ECC_IN_PROGRESS";

    /**
     * Extra value to indicate the ECC state.
     * @hide
     */
    public static final String EXTRA_IN_PROGRESS = "in_progress";
    /// @}

    /**
     * Construction function for TelephonyManager
     * @param context a context
     */
    public MtkTelephonyManagerEx(Context context) {
        mContext = context;
        mRegistry = ITelephonyRegistry.Stub.asInterface(ServiceManager.getService(
                    "telephony.registry"));

    }

    /*  Construction function for TelephonyManager */
    private MtkTelephonyManagerEx() {
        mRegistry = ITelephonyRegistry.Stub.asInterface(ServiceManager.getService(
                   "telephony.registry"));
    }

    private  static MtkTelephonyManagerEx sInstance = new MtkTelephonyManagerEx();

    /**
     * Return the static instance of TelephonyManagerEx
     * @return return the static instance of TelephonyManagerEx
     * @hide
     */
    public static MtkTelephonyManagerEx getDefault() {
        return sInstance;
    }

    /**
     * Returns a constant indicating the device phone type.  This
     * indicates the type of radio used to transmit voice calls.
     *
     * @param simId Indicates which SIM(slot) to query
     * @return  a constant indicating the device phone type
     *
     * @see #PHONE_TYPE_NONE
     * @see #PHONE_TYPE_GSM
     * @see #PHONE_TYPE_CDMA
     */
    public int getPhoneType(int simId) {
        int subIds[] = SubscriptionManager.getSubId(simId);
        if (subIds == null) {
            return TelephonyManager.getDefault()
                    .getCurrentPhoneType(SubscriptionManager.INVALID_SUBSCRIPTION_ID);
        }
        Rlog.e(TAG, "Deprecated! getPhoneType with simId " + simId + ", subId " + subIds[0]);
        return TelephonyManager.getDefault().getCurrentPhoneType(subIds[0]);
    }

    private int getSubIdBySlot(int slot) {
        int [] subId = SubscriptionManager.getSubId(slot);
        Rlog.d(TAG, "getSubIdBySlot, simId " + slot +
                "subId " + ((subId != null) ? subId[0] : "invalid!"));
        return (subId != null) ? subId[0] : SubscriptionManager.getDefaultSubscriptionId();
    }

    private ITelephony getITelephony() {
        return ITelephony.Stub.asInterface(ServiceManager.getService(Context.TELEPHONY_SERVICE));
    }

    private IMtkTelephonyEx getIMtkTelephonyEx() {
        return IMtkTelephonyEx.Stub.asInterface(ServiceManager.getService("phoneEx"));
    }

    private IPhoneSubInfo getSubscriberInfo() {
        // get it each time because that process crashes a lot
        return IPhoneSubInfo.Stub.asInterface(ServiceManager.getService("iphonesubinfo"));
    }

    /**
     * It used to get whether the device is in dual standby dual connection.
     * For example, call application will be able to support dual connection
     * if the device mode is in DSDA.
     *
     * @return true if the device is in DSDA mode, false for others
     * @hide
     */
    public boolean isInDsdaMode() {
        if (SystemProperties.get("ro.mtk_switch_antenna", "0").equals("1")) {
            return false;
        }
        if (SystemProperties.getInt("ro.boot.opt_c2k_lte_mode", 0) == 1) {
            // check if contains CDMAPhone
            TelephonyManager tm = TelephonyManager.getDefault();
            int simCount = tm.getSimCount();
            for (int i = 0; i < simCount; i++) {
                int[] allSubId = SubscriptionManager.getSubId(i);
                if (allSubId == null) {
                    Rlog.d(TAG, "isInDsdaMode, allSubId is null for slot" + i);
                    continue;
                }
                int phoneType = tm.getCurrentPhoneType(allSubId[0]);
                Rlog.d(TAG, "isInDsdaMode, allSubId[0]:" + allSubId[0]
                        + ", phoneType:" + phoneType);
                if (phoneType == PhoneConstants.PHONE_TYPE_CDMA) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Enable or disable Telephony and connectivity debug log
     * @param enable true: enable log, false: disable log
     */
    public void setTelLog(boolean enable) {
        try {
            getIMtkTelephonyEx().setTelLog(enable);
        } catch (RemoteException ex) {
            return;
        } catch (NullPointerException ex) {
            return;
        }
    }

    /// M: [Network][C2K] Add isInHomeNetwork interface. @{
    /**
     * Return whether in home area for the specific subscription id.
     *
     * @param subId the id of the subscription to be queried.
     * @return true if in home network.
     */
    public boolean isInHomeNetwork(int subId) {
        try {
            IMtkTelephonyEx telephonyEx = getIMtkTelephonyEx();
            if (telephonyEx == null) {
                return false;
            } else {
                return telephonyEx.isInHomeNetwork(subId);
            }
        } catch (RemoteException ex) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        }
    }
    /// @}

    // MTK-START: SIM
    private IMtkPhoneSubInfoEx getMtkSubscriberInfoEx() {
        // get it each time because that process crashes a lot
        return IMtkPhoneSubInfoEx.Stub.asInterface(ServiceManager.getService("iphonesubinfoEx"));
    }

    /**
     * Returns the USIM Service Table (UST) that was loaded from the USIM.
     * @param service service index on UST
     * @return the indicated service is supported or not
     * @hide
     */
    public boolean getUsimService(int service) {
        return getUsimService(SubscriptionManager.getDefaultSubscriptionId(), service);
    }

    /**
     * Returns the USIM Service Table (UST) that was loaded from the USIM.
     * @param subId subscription ID to be queried
     * @param service service index on UST
     * @return the indicated service is supported or not
     * @hide
     */
    public boolean getUsimService(int subId, int service) {
        try {
            return getMtkSubscriberInfoEx().getUsimServiceForSubscriber(subId,
                    service, getOpPackageName());
        } catch (RemoteException ex) {
            return false;
        } catch (NullPointerException ex) {
            // This could happen before phone restarts due to crashing
            return false;
        }
    }

    /**
     * Get icc app family by slot id.
     * @param slotId slot id
     * @return the family type
     */
    public int getIccAppFamily(int slotId) {
        try {
            return getIMtkTelephonyEx().getIccAppFamily(slotId);
        } catch (RemoteException ex) {
            return APP_FAM_NONE;
        } catch (NullPointerException ex) {
            return APP_FAM_NONE;
        }
    }

    /**
     * Get Icc Card Type
     * @param subId which subId to query
     * @return "SIM" for SIM card or "USIM" for USIM card.
     * @hide
     * @internal
     */
    public String getIccCardType(int subId) {
        String type = null;
        try {
            type = getIMtkTelephonyEx().getIccCardType(subId);
        } catch (RemoteException ex) {
            ex.printStackTrace();
        } catch (NullPointerException ex) {
            // This could happen before phone restarts due to crashing
            ex.printStackTrace();
        }
        Rlog.d(TAG, "getIccCardType sub " + subId + " ,icc type " +
                ((type != null) ? type : "null"));
        return type;
    }

      private String getOpPackageName() {
        // For legacy reasons the TelephonyManager has API for getting
        // a static instance with no context set preventing us from
        // getting the op package name. As a workaround we do a best
        // effort and get the context from the current activity thread.
        if (mContext != null) {
            return mContext.getOpPackageName();
        }
        return ActivityThread.currentOpPackageName();
    }

    /**
     * Returns the response APDU for a command APDU sent through SIM_IO.
     *
     * <p>Requires Permission:
     *   {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}
     * Or the calling app has carrier privileges. @see #hasCarrierPrivileges
     *
     * @param slot
     * @param family
     * @param fileID
     * @param filePath
     * @return The APDU response
     * @hide
     */
    public byte[] loadEFTransparent(int slotId, int family, int fileID, String filePath) {
        try {
            IMtkTelephonyEx telephony = getIMtkTelephonyEx();
            if (telephony != null)
                return telephony.loadEFTransparent(slotId, family, fileID, filePath);
        } catch (RemoteException ex) {
        } catch (NullPointerException ex) {
        }
        return null;
    }

    /**
     * Returns the response APDU for a command APDU sent through SIM_IO.
     *
     * <p>Requires Permission:
     *   {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}
     * Or the calling app has carrier privileges. @see #hasCarrierPrivileges
     *
     * @param slot
     * @param family
     * @param fileID
     * @param filePath
     * @return The APDU response
     * @hide
     */
    public List<String> loadEFLinearFixedAll(int slotId, int family, int fileID,
            String filePath) {
        try {
            IMtkTelephonyEx telephony = getIMtkTelephonyEx();
            if (telephony != null)
                return telephony.loadEFLinearFixedAll(slotId, family, fileID, filePath);
        } catch (RemoteException ex) {
        } catch (NullPointerException ex) {
        }
        return null;
    }
    // MTK-END

    // [SIM-C2K] @{
    /**
     * Get uim imsi by given sub id.
     * @param subId subscriber id
     *
     * Requires Permission:
     *   {@link android.Manifest.permission#READ_PHONE_STATE}
     *
     * @return uim imsi
     */
    public String getUimSubscriberId(int subId) {
        String  uimImsi = "";

        try {
            uimImsi = getIMtkTelephonyEx().getUimSubscriberId(getOpPackageName(), subId);
        } catch (RemoteException ex) {
            ex.printStackTrace();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        return uimImsi;
    }

    /**
     * Return the supported card type of the SIM card in the slot.
     *
     * @param slotId the given slot id.
     * @return supported card type array.
     */
    public String[] getSupportCardType(int slotId) {
        String property = null;
        String prop = null;
        String values[] = null;

        if (slotId < 0 || slotId >= PROPERTY_RIL_FULL_UICC_TYPE.length) {
            Rlog.e(TAG, "getSupportCardType: invalid slotId " + slotId);
            return null;
        }
        prop = SystemProperties.get(PROPERTY_RIL_FULL_UICC_TYPE[slotId], "");
        if ((!prop.equals("")) && (prop.length() > 0)) {
            values = prop.split(",");
        }
        Rlog.d(TAG, "getSupportCardType slotId " + slotId + ", prop value= " + prop +
                ", size= " + ((values != null) ? values.length : 0));
        return values;
    }

    /**
     * Check if the specified slot is CT 3G dual mode card.
     * @param slotId slot ID
     * @return if it's CT 3G dual mode card
     */
    public boolean isCt3gDualMode(int slotId) {
        if (slotId < 0 || slotId >= PROPERTY_RIL_CT3G.length) {
            Rlog.e(TAG, "isCt3gDualMode: invalid slotId " + slotId);
            return false;
        }
        String result = SystemProperties.get(PROPERTY_RIL_CT3G[slotId], "");
        Rlog.d(TAG, "isCt3gDualMode:  " + result);
        return ("1".equals(result));
    }

    /**
     * Get CDMA card type by given slot id.
     * @param slotId slot ID
     * @return the SIM type
     */
    public MtkIccCardConstants.CardType getCdmaCardType(int slotId) {
        if (slotId < 0 || slotId >= PROPERTY_RIL_CT3G.length) {
            Rlog.e(TAG, "getCdmaCardType: invalid slotId " + slotId);
            return null;
        }
        MtkIccCardConstants.CardType mCdmaCardType = MtkIccCardConstants.CardType.UNKNOW_CARD;
        String result = SystemProperties.get(PROPERTY_RIL_CDMA_CARD_TYPE[slotId], "");
        if (!result.equals("")) {
            int cardtype = Integer.parseInt(result);
            mCdmaCardType = MtkIccCardConstants.CardType.getCardTypeFromInt(cardtype);
        }
        Rlog.d(TAG, "getCdmaCardType result: " + result + "  mCdmaCardType: " + mCdmaCardType);
        return mCdmaCardType;
    }

    /**
     * Returns the serial number for the given subscription, if applicable. Return null if it is
     * unavailable.
     * @param simId  Indicates which SIM to query.
     *               Value of simId:
     *                 0 for SIM1
     *                 1 for SIM2
     * <p>
     * @return       serial number of the SIM, if applicable. Null is returned if it is unavailable.
     *
     */
    public String getSimSerialNumber(int simId) {
        if (simId < 0 || simId >= TelephonyManager.getDefault().getSimCount()) {
            Rlog.e(TAG, "getSimSerialNumber with invalid simId " + simId);
            return null;
        }

        String iccId = null;
        try {
            iccId = getIMtkTelephonyEx().getSimSerialNumber(getOpPackageName(), simId);
        } catch (RemoteException ex) {
            ex.printStackTrace();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        if (iccId != null && (iccId.equals("N/A") || iccId.equals(""))) {
            iccId = null;
        }

        return iccId;
    }
    // [SIM-C2K] @}

    /**
     * Returns the current cell location of the device.
     * <p>
     * Required Permission:
     *  android.Manifest.permission#ACCESS_COARSE_LOCATION ACCESS_COARSE_LOCATION or
     *  android.Manifest.permission#ACCESS_COARSE_LOCATION ACCESS_FINE_LOCATION.
     *
     * @param simId  Indicates which SIM to query.
     *               Value of simId:
     *                 0 for SIM1
     *                 1 for SIM2
     * @return current cell location of the device. A CellLocation object
     * returns null if the current location is not available.
     *
     */
    public CellLocation getCellLocation(int simId) {
        try {
            IMtkTelephonyEx telephony = getIMtkTelephonyEx();
            if (telephony == null) {
                Rlog.d(TAG, "getCellLocation returning null because telephony is null");
                return null;
            }
            Bundle bundle = telephony.getCellLocationUsingSlotId(simId);
            if (bundle == null) {
                Rlog.d(TAG, "getCellLocation returning null because bundle is null");
                return null;
            } else if (bundle.isEmpty()) {
                Rlog.d(TAG, "getCellLocation returning null because bundle is empty");
                return null;
            }
            int phoneType = getPhoneType(simId);
            CellLocation cl = null;
            switch (phoneType) {
                case PhoneConstants.PHONE_TYPE_CDMA:
                    cl = new CdmaCellLocation(bundle);
                    break;
                case PhoneConstants.PHONE_TYPE_GSM:
                    cl = new GsmCellLocation(bundle);
                    break;
                default:
                    cl = null;
                    break;
            }
            Rlog.d(TAG, "getCellLocation is" + cl);
            if (cl == null) {
                Rlog.d(TAG, "getCellLocation returning null because cl is null");
                return null;
            } else if (cl.isEmpty()) {
                Rlog.d(TAG, "getCellLocation returning null because CellLocation is empty");
                return null;
            }
            return cl;
        } catch (RemoteException ex) {
            Rlog.d(TAG, "getCellLocation returning null due to RemoteException " + ex);
            return null;
        } catch (NullPointerException ex) {
            Rlog.d(TAG, "getCellLocation returning null due to NullPointerException " + ex);
            return null;
        }
    }

    // [OMH] @{
    /**
     * The Open Market Handsets (OMH) initiative is a strategic effort
     * to benefit the CDMA ecosystem by enabling open distribution of devices
     * across networks and regions by expanding Removable User Identity Module
     * (R-UIM) capabilities to support a full set of competitive features and
     * standardizing a uniform device implementation for each feature.
     * [refer to 167 spec]
     * This API is used to check if OMH feature is enabled or not for this SIM.
     * @param subId the subId will be checked.
     * @return return true if OMH enable.
     */
    public boolean isOmhEnable(int subId) {
        try {
            return getIMtkTelephonyEx().isOmhEnable(subId);
        } catch (RemoteException ex) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    /**
     * The Open Market Handsets (OMH) initiative is a strategic effort
     * to benefit the CDMA ecosystem by enabling open distribution of devices
     * across networks and regions by expanding Removable User Identity Module
     * (R-UIM) capabilities to support a full set of competitive features and
     * standardizing a uniform device implementation for each feature.
     * [refer to 167 spec]
     * This API is used to check if the card is OMH card or not.
     * @param subId the subId for the card.
     * @return return if the card is OMH card.
     */
    public boolean isOmhCard(int subId) {
        try {
            return getIMtkTelephonyEx().isOmhCard(subId);
        } catch (RemoteException ex) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    /**
     * Get the configuration for mms.
     * @param subId for the card.
     * @return return the mms configuration.
     */
    public MmsConfigInfo getMmsConfigInfo(int subId) {
        try {
            return getIMtkTelephonyEx().getMmsConfigInfo(subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            return null;
        }
    }

    /**
     * Get the MMS ICP info.
     * @param subId The subId for the card to get.
     * @return return the mms icp info.
     */
    public MmsIcpInfo getMmsIcpInfo(int subId) {
        try {
            return getIMtkTelephonyEx().getMmsIcpInfo(subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            return null;
        }
    }

    /**
     * Return the user customized ecc list.
     *
     * @return Return the user customized ecc list
     */
    public ArrayList<EccEntry> getUserCustomizedEccList() {
        ArrayList<EccEntry> result = new ArrayList<EccEntry>();
        Bundle bundle = null;
        try {
            bundle = getIMtkTelephonyEx().getUserCustomizedEccList();
        } catch (RemoteException ex) {
            return result;
        } catch (NullPointerException ex) {
            return result;
        }
        if (bundle != null) {
            ArrayList<String> names = bundle.getStringArrayList("names");
            ArrayList<String> numbers = bundle.getStringArrayList("numbers");
            if (names == null || numbers == null || names.size() != numbers.size()) {
                return result;
            }
            for (int i = 0; i < names.size(); i++) {
                EccEntry entry = new EccEntry(names.get(i), numbers.get(i));
                result.add(entry);
            }
        }
        Rlog.d(TAG, "getUserCustomizedEccList, result:" + result);
        return result;
    }

    /**
     * Update the user customized ecc list.
     * @param eccList The user customized ECC list
     * @return true if succeed, or false
     */
    public boolean updateUserCustomizedEccList(ArrayList<EccEntry> eccList) {
        Rlog.d(TAG, "updateUserCustomizedEccList, eccList:" + eccList);
        Bundle bundle = null;
        if (eccList != null) {
            bundle = new Bundle();
            ArrayList<String> names = new ArrayList<String>();
            ArrayList<String> numbers = new ArrayList<String>();
            for (EccEntry entry : eccList) {
                names.add(entry.getName());
                numbers.add(entry.getEcc());
            }
            bundle.putStringArrayList("names", names);
            bundle.putStringArrayList("numbers", numbers);
        }
        try {
            return getIMtkTelephonyEx().updateUserCustomizedEccList(bundle);
        } catch (RemoteException ex) {
            Rlog.d(TAG, "updateUserCustomizedEccList, RemoteException:" + ex);
            return false;
        } catch (NullPointerException ex) {
            Rlog.d(TAG, "updateUserCustomizedEccList, NullPointerException:" + ex);
            return false;
        }
    }

    /**
     * Check if the number is user customized ecc.
     * @param number The number need to check
     * @return true if yes, or false
     */
    public boolean isUserCustomizedEcc(String number) {
        if (number == null) {
            return false;
        }
        try {
            return getIMtkTelephonyEx().isUserCustomizedEcc(number);
        } catch (RemoteException ex) {
            Rlog.d(TAG, "isUserCustomizedEcc, RemoteException:" + ex);
            return false;
        } catch (NullPointerException ex) {
            Rlog.d(TAG, "isUserCustomizedEcc, NullPointerException:" + ex);
            return false;
        }
    }

    /**
     * Get the feature code of call forwarding.
     * The feature code is stored in the OMH(Open Market Handsets) card.
     * See 3gpp2 spec C.S0023-D_v1.0_R-UIM.
     *
     * @param type call forwarding type
     *              1 - All call forwarding type
     *              2 - Busy to voice mail
     *              3 - default to voice mail
     *              4 - No answer forward-to number
     *              5 - Unconditional forward-to number
     * @param subId the subscription ID
     * @return the feature code of call forwarding.
     */
    public int[] getCallForwardingFc(int type, int subId) {
        try {
            return getIMtkTelephonyEx().getCallForwardingFc(type, subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            return null;
        }
    }

    /**
     * Get the feature code of call waiting.
     * The feature code is stored in the OMH(Open Market Handsets) card.
     * See 3gpp2 spec C.S0023-D_v1.0_R-UIM.
     *
     * @param subId the subscription ID
     * @return the feature code of call waiting
     */
    public int[] getCallWaitingFc(int subId) {
        try {
            return getIMtkTelephonyEx().getCallWaitingFc(subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            return null;
        }
    }

    /**
     * Get the feature code of do not disturb.
     * The feature code is stored in the OMH(Open Market Handsets) card.
     * See 3gpp2 spec C.S0023-D_v1.0_R-UIM.
     *
     * @param subId the subscription ID
     * @return the feature code of do not disturb
     */
    public int[] getDonotDisturbFc(int subId) {
        try {
            return getIMtkTelephonyEx().getDonotDisturbFc(subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            return null;
        }
    }

    /**
     * Get the feature code of voice mail retrieve.
     * The feature code is stored in the OMH(Open Market Handsets) card.
     * See 3gpp2 spec C.S0023-D_v1.0_R-UIM.
     *
     * @param subId the subscription ID
     * @return the feature code of voice message retrieve
     */
    public int[] getVoiceMailRetrieveFc(int subId) {
        try {
            return getIMtkTelephonyEx().getVMRetrieveFc(subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            return null;
        }
    }
    // [OMH] @}

    // MTK-START: ISIM
    /**
     * Returns the IMS private user identity (IMPI) that was loaded from the ISIM.
     * @param subId subscription ID to be queried
     * @return the IMPI, or null if not present or not loaded
     * @hide
     */
    public String getIsimImpi(int subId) {
        try {
            return getMtkSubscriberInfoEx().getIsimImpiForSubscriber(subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            // This could happen before phone restarts due to crashing
            return null;
        }
    }

    /**
     * Returns the IMS home network domain name that was loaded from the ISIM.
     * @param subId subscription ID to be queried
     * @return the IMS domain name, or null if not present or not loaded
     * @hide
     */
    public String getIsimDomain(int subId) {
        try {
            return getMtkSubscriberInfoEx().getIsimDomainForSubscriber(subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            // This could happen before phone restarts due to crashing
            return null;
        }
    }

    /**
     * Returns the IMS public user identities (IMPU) that were loaded from the ISIM.
     * @param subId subscription ID to be queried
     * @return an array of IMPU strings, with one IMPU per string, or null if
     *      not present or not loaded
     * @hide
     */
    public String[] getIsimImpu(int subId) {
        try {
            return getMtkSubscriberInfoEx().getIsimImpuForSubscriber(subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            // This could happen before phone restarts due to crashing
            return null;
        }
    }

    /**
     * Returns the IMS Service Table (IST) that was loaded from the ISIM.
     * @param subId subscription ID to be queried
     * @return IMS Service Table or null if not present or not loaded
     * @hide
     */
    public String getIsimIst(int subId) {
        try {
            return getMtkSubscriberInfoEx().getIsimIstForSubscriber(subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            // This could happen before phone restarts due to crashing
            return null;
        }
    }

    /**
     * Returns the IMS Proxy Call Session Control Function(PCSCF) that were loaded from the ISIM.
     * @param subId subscription ID to be queried
     * @return an array of PCSCF strings with one PCSCF per string, or null if
     *         not present or not loaded
     * @hide
     */
    public String[] getIsimPcscf(int subId) {
        try {
            return getMtkSubscriberInfoEx().getIsimPcscfForSubscriber(subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            // This could happen before phone restarts due to crashing
            return null;
        }
    }
    // MTK-END

    /**
     * Get IMS registration state by given sub-id.
     * @param subId The subId for query
     * @return true if IMS is registered, or false
     * @hide
     */
    public boolean isImsRegistered(int subId) {
        try {
            return getIMtkTelephonyEx().isImsRegistered(subId);
        } catch (RemoteException ex) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    /**
     * Get Volte registration state by given sub-id.
     * @param subId The subId for query
     * @return true if volte is registered, or false
     * @hide
     */
    public boolean isVolteEnabled(int subId) {
        try {
            return getIMtkTelephonyEx().isVolteEnabled(subId);
        } catch (RemoteException ex) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    /**
     * Get WFC registration state by given sub-id.
     * @param subId The subId for query
     * @return true if wfc is registered, or false
     * @hide
     */
    public boolean isWifiCallingEnabled(int subId) {
        try {
            return getIMtkTelephonyEx().isWifiCallingEnabled(subId);
        } catch (RemoteException ex) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    // MTK-START: SIM GBA
    /**
     * Returns the GBA bootstrapping parameters (GBABP) that was loaded from the ISIM.
     * @return GBA bootstrapping parameters or null if not present or not loaded
     * @hide
     */
    public String getIsimGbabp() {
        return getIsimGbabp(SubscriptionManager.getDefaultSubscriptionId());
    }

    /**
     * Returns the GBA bootstrapping parameters (GBABP) that was loaded from the ISIM.
     * @param subId subscription ID to be queried
     * @return GBA bootstrapping parameters or null if not present or not loaded
     * @hide
     */
    public String getIsimGbabp(int subId) {
        try {
            return getMtkSubscriberInfoEx().getIsimGbabpForSubscriber(subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            // This could happen before phone restarts due to crashing
            return null;
        }
    }

    /**
     * Set the GBA bootstrapping parameters (GBABP) value into the ISIM.
     * @param gbabp a GBA bootstrapping parameters value in String type
     * @param onComplete
     *        onComplete.obj will be an AsyncResult
     *        ((AsyncResult)onComplete.obj).exception == null on success
     *        ((AsyncResult)onComplete.obj).exception != null on fail
     * @hide
     */
    public void setIsimGbabp(String gbabp, Message onComplete) {
        setIsimGbabp(SubscriptionManager.getDefaultSubscriptionId(), gbabp, onComplete);
    }

    /**
     * Set the GBA bootstrapping parameters (GBABP) value into the ISIM.
     * @param subId subscription ID to be queried
     * @param gbabp a GBA bootstrapping parameters value in String type
     * @param onComplete
     *        onComplete.obj will be an AsyncResult
     *        ((AsyncResult)onComplete.obj).exception == null on success
     *        ((AsyncResult)onComplete.obj).exception != null on fail
     * @hide
     */
    public void setIsimGbabp(int subId, String gbabp, Message onComplete) {
        try {
            getMtkSubscriberInfoEx().setIsimGbabpForSubscriber(subId, gbabp, onComplete);
        } catch (RemoteException ex) {
            return;
        } catch (NullPointerException ex) {
            // This could happen before phone restarts due to crashing
            return;
        }
    }
    // MTK-END

    // MTK-START: SIM GBA
    /**
     * Returns the GBA bootstrapping parameters (GBABP) that was loaded from the USIM.
     * @return GBA bootstrapping parameters or null if not present or not loaded
     * @hide
     */
    public String getUsimGbabp() {
        return getUsimGbabp(SubscriptionManager.getDefaultSubscriptionId());
    }

    /**
     * Returns the GBA bootstrapping parameters (GBABP) that was loaded from the USIM.
     * @param subId subscription ID to be queried
     * @return GBA bootstrapping parameters or null if not present or not loaded
     * @hide
     */
    public String getUsimGbabp(int subId) {
        try {
            return getMtkSubscriberInfoEx().getUsimGbabpForSubscriber(subId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            // This could happen before phone restarts due to crashing
            return null;
        }
    }

    /**
     * Set the GBA bootstrapping parameters (GBABP) value into the USIM.
     * @param gbabp a GBA bootstrapping parameters value in String type
     * @param onComplete
     *        onComplete.obj will be an AsyncResult
     *        ((AsyncResult)onComplete.obj).exception == null on success
     *        ((AsyncResult)onComplete.obj).exception != null on fail
     * @hide
     */
    public void setUsimGbabp(String gbabp, Message onComplete) {
        setUsimGbabp(SubscriptionManager.getDefaultSubscriptionId(), gbabp, onComplete);
    }

    /**
     * Set the GBA bootstrapping parameters (GBABP) value into the USIM.
     * @param subId subscription ID to be queried
     * @param gbabp a GBA bootstrapping parameters value in String type
     * @param onComplete
     *        onComplete.obj will be an AsyncResult
     *        ((AsyncResult)onComplete.obj).exception == null on success
     *        ((AsyncResult)onComplete.obj).exception != null on fail
     * @hide
     */
    public void setUsimGbabp(int subId, String gbabp, Message onComplete) {
        try {
            getMtkSubscriberInfoEx().setUsimGbabpForSubscriber(subId, gbabp, onComplete);
        } catch (RemoteException ex) {
            return;
        } catch (NullPointerException ex) {
            // This could happen before phone restarts due to crashing
            return;
        }
    }
    // MTK-END

    /**
     * Get PrlVersion by subId.
     * @param subId subId
     * @return prl version
     * @hide
     */
    ///M: [Network][C2K] provide API to get Prl version by SubId. @{
    public String getPrlVersion(int subId) {
        int slotId = SubscriptionManager.getSlotIndex(subId);
        String prlVersion = SystemProperties.get(PRLVERSION + slotId, "");
        Rlog.d(TAG, "getPrlversion PRLVERSION subId = " + subId
                + " key = " + PRLVERSION + slotId
                + " value = " + prlVersion);
        return prlVersion;
    }
    /// @}

    /**
     * Set RF test Configuration to defautl phone
     * @param config The configuration
     *  0: signal information is not available on all Rx chains
     *  1: Rx diversity bitmask for chain 0(primary antenna)
     *  2: Rx diversity bitmask for chain 1(secondary antenna) is available
     *  3: Signal information on both Rx chains is available
     */
    public int [] setRxTestConfig(int config) {
        int defaultPhoneId =
                SubscriptionManager.getPhoneId(SubscriptionManager.getDefaultSubscriptionId());
        try {
            return getIMtkTelephonyEx().setRxTestConfig(defaultPhoneId, config);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            return null;
        }
    }

    /**
     * Query RF Test Result
     */
    public int [] getRxTestResult() {
        int defaultPhoneId =
                SubscriptionManager.getPhoneId(SubscriptionManager.getDefaultSubscriptionId());
        try {
            return getIMtkTelephonyEx().getRxTestResult(defaultPhoneId);
        } catch (RemoteException ex) {
            return null;
        } catch (NullPointerException ex) {
            return null;
        }
    }

    /// M: CC: ECC is in progress @{
    /**
     * Set ECC in progress.
     *
     * @param state ECC in pregress or not.
     */
    public void setEccInProgress(boolean state) {
        try {
            getIMtkTelephonyEx().setEccInProgress(state);
        } catch (RemoteException ex) {
            return;
        } catch (NullPointerException ex) {
            return;
        }
    }

    /**
     * Check if ECC is in progress or not.
     *
     * @return true if ECC is in progress, otherwise false.
     */
    public boolean isEccInProgress() {
        try {
            return getIMtkTelephonyEx().isEccInProgress();
        } catch (RemoteException ex) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        }
    }
    /// @}

    /**
     * Request to exit emergency call back mode.
     *
     * @param subId the subscription ID
     * @return true if exist the emeregency call back mode sucessfully
     */
    public boolean exitEmergencyCallbackMode(int subId) {
        try {
            return getIMtkTelephonyEx().exitEmergencyCallbackMode(subId);
        } catch (RemoteException ex) {
            return false;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    public void setApcMode(int slotId, int mode, boolean reportOn,
            int reportInterval) {
        if (slotId < 0 || slotId >= TelephonyManager.getDefault().getSimCount()) {
            Rlog.e(TAG, "setApcMode error with invalid slotId " + slotId);
            return;
        }
        if (mode < 0 || mode > 2) {
            Rlog.e(TAG, "setApcMode error with invalid mode " + mode);
            return;
        }

        try {
            IMtkTelephonyEx telephony = getIMtkTelephonyEx();
            if (telephony == null) {
                Rlog.e(TAG, "setApcMode error because telephony is null");
                return;
            }
            telephony.setApcModeUsingSlotId(slotId, mode,
                    reportOn, reportInterval);
        } catch (RemoteException ex) {
            Rlog.e(TAG, "setApcMode error due to RemoteException " + ex);
            return;
        } catch (NullPointerException ex) {
            Rlog.e(TAG, "setApcMode error due to NullPointerException " + ex);
            return;
        }
    }

    public PseudoCellInfo getApcInfo(int slotId) {
        if (slotId < 0 || slotId >= TelephonyManager.getDefault().getSimCount()) {
            Rlog.e(TAG, "getApcInfo with invalid slotId " + slotId);
            return null;
        }

        try {
            IMtkTelephonyEx telephony = getIMtkTelephonyEx();
            if (telephony == null) {
                Rlog.e(TAG, "getApcInfo return null because telephony is null");
                return null;
            }
            return telephony.getApcInfoUsingSlotId(slotId);
        } catch (RemoteException ex) {
            Rlog.e(TAG, "getApcInfo returning null due to RemoteException " + ex);
            return null;
        } catch (NullPointerException ex) {
            Rlog.e(TAG, "getApcInfo returning null due to NullPointerException " + ex);
            return null;
        }
    }

    /**
     * Get CDMA subscription active status  by subId.
     * @param subId subId
     * @return active status. 1 is active, 0 is deactive
     */
    public int getCdmaSubscriptionActStatus(int subId) {
        int actStatus = 0;

        try {
            actStatus = getIMtkTelephonyEx().getCdmaSubscriptionActStatus(subId);
        } catch (RemoteException ex) {
            Rlog.d(TAG, "fail to getCdmaSubscriptionActStatus due to RemoteException");
        } catch (NullPointerException ex) {
            Rlog.d(TAG, "fail to getCdmaSubscriptionActStatus due to NullPointerException");
        }
        return actStatus;
    }

    /// M: CC: Vzw ECC/hVoLTE redial @{
    public boolean useVzwLogic() {
        boolean ret = false;

        // Via DSBP and SBP id
        String dsbp;
        String optr;
        dsbp = SystemProperties.get("persist.radio.mtk_dsbp_support", "0");
        if (dsbp.equals("1")) {
            optr = SystemProperties.get("persist.radio.sim.sbp", "0");
        } else {
            optr = SystemProperties.get("ro.mtk_md_sbp_custom_value", "0");
        }

        if (optr.equals("12") || optr.equals("20")) {
            ret = true;
        }
        Rlog.d(TAG, "optr = " + optr + ", useVzwLogic = " + ret);

        return ret;
    }
    ///@}

    public boolean setWifiEnabled(int pheonId, String ifName, int isEnabled) {
        try {
            IMtkTelephonyEx telephony = getIMtkTelephonyEx();
            if (telephony == null) {
                Rlog.e(TAG, "[setWifiEnabled] telephony = null");
                return false;
            }
            telephony.setWifiEnabled(pheonId, ifName, isEnabled);
        } catch (RemoteException ex) {
            Rlog.e(TAG, "[setWifiEnabled] RemoteException " + ex);
            return false;
        } catch (NullPointerException ex) {
            Rlog.e(TAG, "[setWifiEnabled] NullPointerException " + ex);
            return false;
        }
        return true;
    }

    public boolean setWifiAssociated(int pheonId, String ifName,
            boolean associated, String ssid, String apMac) {
        try {
            IMtkTelephonyEx telephony = getIMtkTelephonyEx();
            if (telephony == null) {
                Rlog.e(TAG, "[setWifiAssociated] telephony = null");
                return false;
            }
            telephony.setWifiAssociated(pheonId, ifName, associated, ssid, apMac);
        } catch (RemoteException ex) {
            Rlog.e(TAG, "[setWifiAssociated] RemoteException " + ex);
            return false;
        } catch (NullPointerException ex) {
            Rlog.e(TAG, "[setWifiAssociated] NullPointerException " + ex);
            return false;
        }
        return true;
    }

    public boolean setWifiSignalLevel(int pheonId, int rssi, int snr) {
        try {
            IMtkTelephonyEx telephony = getIMtkTelephonyEx();
            if (telephony == null) {
                Rlog.e(TAG, "[setWifiSignalLevel] telephony = null");
                return false;
            }
            telephony.setWifiSignalLevel(pheonId, rssi, snr);
        } catch (RemoteException ex) {
            Rlog.e(TAG, "[setWifiSignalLevel] RemoteException " + ex);
            return false;
        } catch (NullPointerException ex) {
            Rlog.e(TAG, "[setWifiSignalLevel] NullPointerException " + ex);
            return false;
        }
        return true;
    }

    public boolean setWifiIpAddress(int pheonId, String ifName, String ipv4Addr,
            String ipv6Addr) {
        try {
            IMtkTelephonyEx telephony = getIMtkTelephonyEx();
            if (telephony == null) {
                Rlog.e(TAG, "[setWifiIpAddress] telephony = null");
                return false;
            }
            telephony.setWifiIpAddress(pheonId, ifName, ipv4Addr, ipv6Addr);
        } catch (RemoteException ex) {
            Rlog.e(TAG, "[setWifiIpAddress] RemoteException " + ex);
            return false;
        } catch (NullPointerException ex) {
            Rlog.e(TAG, "[setWifiIpAddress] NullPointerException " + ex);
            return false;
        }
        return true;
    }

    public boolean setLocationInfo(int pheonId, String accountId, String broadcastFlag,
            String latitude, String longitude, String accuracy, String method, String city,
            String state, String zip, String countryCode) {
        try {
            IMtkTelephonyEx telephony = getIMtkTelephonyEx();
            if (telephony == null) {
                Rlog.e(TAG, "[setLocationInfo] telephony = null");
                return false;
            }
            telephony.setLocationInfo(pheonId, accountId, broadcastFlag, latitude,
                    longitude, accuracy, method, city, state, zip, countryCode);
        } catch (RemoteException ex) {
            Rlog.e(TAG, "[setLocationInfo] RemoteException " + ex);
            return false;
        } catch (NullPointerException ex) {
            Rlog.e(TAG, "[setLocationInfo] NullPointerException " + ex);
            return false;
        }
        return true;
    }

    public boolean setEmergencyAddressId(int pheonId, String aid) {
        try {
            IMtkTelephonyEx telephony = getIMtkTelephonyEx();
            if (telephony == null) {
                Rlog.e(TAG, "[setEmergencyAddressId] telephony = null");
                return false;
            }
            telephony.setEmergencyAddressId(pheonId, aid);
        } catch (RemoteException ex) {
            Rlog.e(TAG, "[setEmergencyAddressId] RemoteException " + ex);
            return false;
        } catch (NullPointerException ex) {
            Rlog.e(TAG, "[setEmergencyAddressId] NullPointerException " + ex);
            return false;
        }
        return true;
    }

    /**
     * Returns the result and response from RIL for oem request
     *
     * @param oemReq the data is sent to ril.
     * @param oemResp the respose data from RIL.
     * @return negative value request was not handled or get error
     *         0 request was handled succesfully, but no response data
     *         positive value success, data length of response
     */
    public int invokeOemRilRequestRaw(byte[] oemReq, byte[] oemResp) {
        try {
            IMtkTelephonyEx telephony = getIMtkTelephonyEx();
            if (telephony != null)
                return telephony.invokeOemRilRequestRaw(oemReq, oemResp);
        } catch (RemoteException ex) {
        } catch (NullPointerException ex) {
        }
        return -1;
    }
}
