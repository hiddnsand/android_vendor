#include <mtkcam/def/common.h>
#include <mtkcam/drv/iopipe/PostProc/IEgnStream.h>
#include <mtkcam/drv/def/owecommon.h>
#include <drv/imem_drv.h>
#include "owe/bittrue.h"
#include "owe_tc00.h"

/*
 * Golden WMFE DMAI/O
 */
extern "C" {
#include "WMFE_TC00/frame_0/frame_0.h"
}
/*
 * Golden OCC DMAI/O
 */
static char tc00_mp[] = {
#include "owe_occ_test_case_00/occ_maj_pxl_frame_00.dhex"
};

static char tc00_mv[] = {
#include "owe_occ_test_case_00/occ_maj_vec_frame_00.dhex"
};

static char tc00_rp[] = {
#include "owe_occ_test_case_00/occ_ref_pxl_frame_00.dhex"
};

static char tc00_rv[] = {
#include "owe_occ_test_case_00/occ_ref_vec_frame_00.dhex"
};

static char wdma[] = {
#include "owe_occ_test_case_00/occ_wdma_frame_00.vhex"
};

/* Golden Comparision : OCC */
unsigned int OCC_WDMA_bittrue(char* gold, IMEM_BUF_INFO &buf , OCCConfig &cfg)
{
    MUINT32 wdma_w;
    MUINT32 wdma_stride;
    MUINT32 err_cnt_dma;
    MUINT32 x_end;
    MUINT32 y_end;

    wdma_w = -1;
    wdma_stride = cfg.OCC_WDMA.u4Stride;
    /*
     * DAMO XSIZE/YSIZE calculation from DMAI: x_end, y_end
     */
    x_end = WDMA_XSIZE(cfg.occ_hsize, cfg.occ_h_crop_e, cfg.occ_h_crop_s, cfg.occ_h_skip_mode) - 1;
    y_end = WDMA_YSIZE(cfg.occ_vsize, cfg.occ_v_crop_e, cfg.occ_v_crop_s) - 1;
    printf("wdma xsize(%d)(%x)\n", x_end, x_end);
    printf("wdma ysize(%d)(%x)\n", y_end, y_end);
    printf("comparing MUT(%p) with golden(%p)\n", (void *)buf.phyAddr, (void *)gold);

    err_cnt_dma = comp_roi_mem_with_file(gold, 
							0, 
							(MUINTPTR)buf.virtAddr,
							wdma_w,
							(y_end + 1),
							wdma_stride,
							0,
							0,
							x_end,
							y_end,
							0,
							1,
							0
							);	
    if (err_cnt_dma)
        printf("owe wmda bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    else
        printf("owe wdma bit true pass!!!\n");

    return err_cnt_dma;
}

/* Golden Comparison : WMFE */
unsigned int WMFE_bittrue(unsigned long golden_addr, WMFECtrl &cfg)
{
    int err_cnt_dma;
    int int_data_dma_0;
    int int_data_dma_1;
    int int_data_dma_2;
    int int_data_dma_3;

    int_data_dma_0 = 0;
    int_data_dma_1 = 0;
    int_data_dma_2 = cfg.Wmfe_Width - 1;
    int_data_dma_3 = cfg.Wmfe_Height - 1;

    err_cnt_dma = comp_roi_mem_with_file((char *)golden_addr,
                                                   0,
                                cfg.Wmfe_Dpo.u4BufVA,
                                cfg.Wmfe_Width,
                                cfg.Wmfe_Height,
                                cfg.Wmfe_Dpo.u4Stride,
                                int_data_dma_0,
                                int_data_dma_1,
                                int_data_dma_2,
                                int_data_dma_3,
                                0,
                                0,
                                0
                                );
    if (err_cnt_dma)
        printf("dpe WMFE DPO Frame0 bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    else
        printf("dpe WMFE DPO Frame0 bit true pass!!!\n");

    return err_cnt_dma;
}
/*
 * OweStream User Call Back
 */
MBOOL g_bOCCCallback;
MVOID OWE_OCCCallback(EGNParams<OCCConfig>& rParams)
{

    printf("--- [OCC callback func]\n");

    vector<OCCConfig>::iterator iter = rParams.mEGNConfigVec.begin();
    for (;iter!= rParams.mEGNConfigVec.end();iter++)
    {
    }
    g_bOCCCallback = MTRUE;

}

MBOOL g_bWMFECallback;
MVOID OWE_WMFECallback(EGNParams<WMFEConfig>& rParams)
{

    printf("--- [WMFE callback func]\n");

    vector<WMFEConfig>::iterator iter = rParams.mEGNConfigVec.begin();
    for (;iter!= rParams.mEGNConfigVec.end();iter++)
    {
    }
    g_bWMFECallback = MTRUE;

}

#define USER_NAME "OWE_DEFAULT"
int owe_default()
{
    int ret = 0;
    printf("[%s] +\n", __func__);
    
     /* Objects + */
    NSCam::NSIoPipe::NSEgn::IEgnStream<OCCConfig>* pOccStream;
    NSCam::NSIoPipe::NSEgn::IEgnStream<WMFEConfig>* pWmfStream;

    pOccStream = NSCam::NSIoPipe::NSEgn::IEgnStream<OCCConfig>::createInstance(USER_NAME);
    pOccStream->init();   

    pWmfStream = NSCam::NSIoPipe::NSEgn::IEgnStream<WMFEConfig>::createInstance(USER_NAME);
    pWmfStream->init();   

    printf("[%s] OweStream init done\n", __func__);

    IMemDrv* mpImemDrv = NULL;
    mpImemDrv = IMemDrv::createInstance();
    mpImemDrv->init();

    /*
     * OCC Memory Allocation Using Imem
     */

    MEM_PREPARE_COPY(buf_mp, tc00_mp, ARRAY_SIZE(tc00_mp));
    MEM_PREPARE_COPY(buf_mv, tc00_mv, ARRAY_SIZE(tc00_mv));
    MEM_PREPARE_COPY(buf_rp, tc00_rp, ARRAY_SIZE(tc00_rp));
    MEM_PREPARE_COPY(buf_rv, tc00_rv, ARRAY_SIZE(tc00_rv));

    MEM_PREPARE_SET(wdma_occ, 0x00000000, ARRAY_SIZE(wdma));

    /*
     * WMFE Golden Import and Memory Alocation
     */
    unsigned long golden_dpe_dvo_l_frame;
    unsigned long golden_dpe_dvo_r_frame;
    unsigned long golden_dpe_confo_l_frame;
    unsigned long golden_dpe_confo_r_frame;
    unsigned long golden_dpe_respo_l_frame;
    unsigned long golden_dpe_respo_r_frame;
    unsigned long golden_dpe_wmf_dpo_frame_0;
    unsigned long golden_dpe_wmf_dpo_frame_1;
    unsigned long golden_dpe_wmf_dpo_frame_2;
    
    getframe_0GoldPointer(
    &golden_dpe_dvo_l_frame,
    &golden_dpe_dvo_r_frame,
    &golden_dpe_confo_l_frame,
    &golden_dpe_confo_r_frame,
    &golden_dpe_respo_l_frame,
    &golden_dpe_respo_r_frame,
    &golden_dpe_wmf_dpo_frame_0,
    &golden_dpe_wmf_dpo_frame_1,
    &golden_dpe_wmf_dpo_frame_2
    );
    
    char* dpe_wmf_imgi_frame_0;
    char* dpe_wmf_dpi_frame_0;
    char* dpe_wmf_tbli_frame_0;
    char* dpe_wmf_maski_frame_0;
    
    char* dpe_wmf_imgi_frame_1;
    char* dpe_wmf_dpi_frame_1;
    char* dpe_wmf_tbli_frame_1;
    char* dpe_wmf_maski_frame_1;
    
    char* dpe_wmf_imgi_frame_2;
    char* dpe_wmf_dpi_frame_2;
    char* dpe_wmf_tbli_frame_2;
    char* dpe_wmf_maski_frame_2;

#if WMFE_ENABLE_0
    dpe_wmf_imgi_frame_0 = &frame_0_in_dpe_wmf_imgi_frame_0[0];
    dpe_wmf_dpi_frame_0 = &frame_0_in_dpe_wmf_dpi_frame_0[0];
    dpe_wmf_tbli_frame_0 = &frame_0_in_dpe_wmf_tbli_frame_0[0];
#if WMFE_MASK_EN_0
    dpe_wmf_maski_frame_0 = &frame_0_in_dpe_wmf_maski_frame_0[0];
#endif
#endif

#if WMFE_ENABLE_1
    dpe_wmf_imgi_frame_1 = &frame_0_in_dpe_wmf_imgi_frame_1[0];
    dpe_wmf_dpi_frame_1 = &frame_0_in_dpe_wmf_dpi_frame_1[0];
    dpe_wmf_tbli_frame_1 = &frame_0_in_dpe_wmf_tbli_frame_1[0];
#if WMFE_MASK_EN_1
    dpe_wmf_maski_frame_1 = &frame_0_in_dpe_wmf_maski_frame_1[0];
#endif
#endif

#if WMFE_ENABLE_2
    dpe_wmf_imgi_frame_2 = &frame_0_in_dpe_wmf_imgi_frame_2[0];
    dpe_wmf_dpi_frame_2 = &frame_0_in_dpe_wmf_dpi_frame_2[0];
    dpe_wmf_tbli_frame_2 = &frame_0_in_dpe_wmf_tbli_frame_2[0];
#if WMFE_MASK_EN_2
    dpe_wmf_maski_frame_2 = &frame_0_in_dpe_wmf_maski_frame_2[0];
#endif
#endif
    /*
     * Memory Allocation Using Imem; Additional DMAI for Android
     */
    MEM_PREPARE_COPY(wmf_imgi_f0, dpe_wmf_imgi_frame_0, frame_0_dpe_wmf_imgi_frame_00_00_0_size);
    MEM_PREPARE_COPY(wmf_dpi_f0, dpe_wmf_dpi_frame_0, frame_0_dpe_wmf_dpi_frame_00_00_0_size);
    MEM_PREPARE_COPY(wmf_tbli_f0, dpe_wmf_tbli_frame_0, frame_0_dpe_wmf_tbli_frame_00_00_0_size);
 
    MEM_PREPARE_COPY(wmf_imgi_f1, dpe_wmf_imgi_frame_1, frame_0_dpe_wmf_imgi_frame_00_00_1_size);
    MEM_PREPARE_COPY(wmf_dpi_f1, dpe_wmf_dpi_frame_1, frame_0_dpe_wmf_dpi_frame_00_00_1_size);
    MEM_PREPARE_COPY(wmf_tbli_f1, dpe_wmf_tbli_frame_1, frame_0_dpe_wmf_tbli_frame_00_00_1_size);
 
    MEM_PREPARE_COPY(wmf_imgi_f2, dpe_wmf_imgi_frame_2, frame_0_dpe_wmf_imgi_frame_00_00_2_size);
    MEM_PREPARE_COPY(wmf_dpi_f2, dpe_wmf_dpi_frame_2, frame_0_dpe_wmf_dpi_frame_00_00_2_size);
    MEM_PREPARE_COPY(wmf_tbli_f2, dpe_wmf_tbli_frame_2, frame_0_dpe_wmf_tbli_frame_00_00_2_size);
 
    MEM_PREPARE_SET(wmf_dpo_frame_0, 0x00000000, frame_0_golden_dpe_wmf_dpo_0_size);
    MEM_PREPARE_SET(wmf_dpo_frame_1, 0x00000000, frame_0_golden_dpe_wmf_dpo_1_size);
    MEM_PREPARE_SET(wmf_dpo_frame_2, 0x00000000, frame_0_golden_dpe_wmf_dpo_2_size);

    /*
     * OweStream Flow
     */
    printf("###########OweStream Flow###########\n");

    EGNParams<OCCConfig> rOccParams;
    OCCConfig occconfig;

    rOccParams.mpEngineID = eOCC;  
    rOccParams.mpfnCallback = OWE_OCCCallback;  
    /*
     * Golden Config : OCC
     */
    occconfig.occ_scan_r2l = FIELD(0x400, 0x00000001, 0);
    occconfig.occ_horz_ds4 = FIELD(0x400, 0x00000002, 1);
    occconfig.occ_vert_ds4 = FIELD(0x400, 0x00000004, 2);
    occconfig.occ_h_skip_mode  = FIELD(0x400, 0x00000008, 3);
    occconfig.occ_imgi_maj_fmt = FIELD(0x400, 0x00000300, 8);
    occconfig.occ_imgi_ref_fmt = FIELD(0x400, 0x00000C00, 10);
    occconfig.occ_hsize    = FIELD(0x1f001a8, 0x000003FF, 0); 
    occconfig.occ_vsize    = FIELD(0x1f001a8, 0x0FFF0000, 16); 
    occconfig.occ_v_crop_s = FIELD(0x1e70008, 0x00000FFF, 0); 
    occconfig.occ_v_crop_e = FIELD(0x1e70008, 0x0FFF0000, 16); 
    occconfig.occ_h_crop_s = FIELD(0x1870020, 0x000003FF, 0);
    occconfig.occ_h_crop_e = FIELD(0x1870020, 0x03FF0000, 16);
    occconfig.occ_th_luma  = FIELD(0x80180840, 0x000000FF, 0);
    occconfig.occ_th_h     = FIELD(0x80180840, 0x0000FF00, 8);
    occconfig.occ_th_v     = FIELD(0x80180840, 0x000F0000, 16);
    occconfig.occ_vec_shift  = FIELD(0x80180840, 0x00700000, 20);
    occconfig.occ_vec_offset = FIELD(0x80180840, 0xFF000000, 24);
    occconfig.occ_invalid_value = FIELD(0x306ff, 0x000000FF, 0);
    occconfig.occ_owc_th        = FIELD(0x306ff, 0x0000FF00, 8);
    occconfig.occ_owc_en        = FIELD(0x306ff, 0x00010000, 16);
    occconfig.occ_depth_clip_en = FIELD(0x306ff, 0x00020000, 17);
    occconfig.occ_spare         = 0;
    occconfig.OCC_REF_VEC.u4BufPA   = buf_rv.phyAddr;
    occconfig.OCC_REF_VEC.u4BufSize = FIELD(0x34f0350,0xFFFF0000, 16);
    occconfig.OCC_REF_VEC.u4Stride  = FIELD(0x34f0350,0x0000FFFF, 0);
    occconfig.OCC_REF_PXL.u4BufPA   = buf_rp.phyAddr;
    occconfig.OCC_REF_PXL.u4BufSize = FIELD(0x34f0350,0xFFFF0000, 16);
    occconfig.OCC_REF_PXL.u4Stride  = FIELD(0x34f0350,0x0000FFFF, 0);
    occconfig.OCC_MAJ_VEC.u4BufPA  = buf_mv.phyAddr; 
    occconfig.OCC_MAJ_VEC.u4BufSize= FIELD(0x34f0350,0xFFFF0000, 16);
    occconfig.OCC_MAJ_VEC.u4Stride = FIELD(0x34f0350,0x0000FFFF, 0);
    occconfig.OCC_MAJ_PXL.u4BufPA  = buf_mp.phyAddr;
    occconfig.OCC_MAJ_PXL.u4BufSize= FIELD(0x1a701a8,0xFFFF0000, 16); 
    occconfig.OCC_MAJ_PXL.u4Stride = FIELD(0x1a701a8,0x0000FFFF, 0);  
    occconfig.OCC_WDMA.u4BufPA   = wdma_occ.phyAddr;
    occconfig.OCC_WDMA.u4BufSize = FIELD(0x1670168,0xFFFF0000, 16); 
    occconfig.OCC_WDMA.u4Stride  = FIELD(0x1670168,0x0000FFFF, 0); 
    rOccParams.mEGNConfigVec.push_back(occconfig);

    g_bOCCCallback = MFALSE;

    /* enque */
    ret = pOccStream->EGNenque(rOccParams);
    if(!ret)
        printf("---ERRRRRRRRR [owe_default..occ enque fail\n]");
    else
        printf("---[owe_default..occ enque done\n]");

    /*
     * Golden Config : WMFE
     */
    EGNParams<WMFEConfig> rWmfeParams;
    WMFEConfig wmfeconfig;
    WMFECtrl ctrl0, ctrl1, ctrl2;
    memset((MUINT8*)&ctrl0, 0x0, sizeof(WMFECtrl));
    memset((MUINT8*)&ctrl1, 0x0, sizeof(WMFECtrl));
    memset((MUINT8*)&ctrl2, 0x0, sizeof(WMFECtrl));

    rWmfeParams.mpEngineID = eWMFE;
    rWmfeParams.mpfnCallback = OWE_WMFECallback;  

#if WMFE_ENABLE_0
    ctrl0.Wmfe_Enable = true;
    ctrl0.WmfeFilterSize = WMFE_FILTER_SIZE_7x7;
    ctrl0.Wmfe_Width = 0xF0;
    ctrl0.Wmfe_Height = 0x87;
    ctrl0.WmfeImgiFmt = OWE_IMGI_YC_FMT;
    ctrl0.WmfeDpiFmt = WMFE_DPI_D_FMT;

    ctrl0.Wmfe_Imgi.u4BufVA = wmf_imgi_f0.virtAddr;
    ctrl0.Wmfe_Imgi.u4BufPA = wmf_imgi_f0.phyAddr;
    ctrl0.Wmfe_Imgi.u4Stride = 0x1E0;
    ctrl0.Wmfe_Imgi.u4BufSize = wmf_imgi_f0.size;

    ctrl0.Wmfe_Dpi.u4BufVA = wmf_dpi_f0.virtAddr;
    ctrl0.Wmfe_Dpi.u4BufPA = wmf_dpi_f0.phyAddr;
    ctrl0.Wmfe_Dpi.u4Stride = 0xF0;
    ctrl0.Wmfe_Dpi.u4BufSize = wmf_dpi_f0.size;

    ctrl0.Wmfe_Tbli.u4BufVA = wmf_tbli_f0.virtAddr;
    ctrl0.Wmfe_Tbli.u4BufPA = wmf_tbli_f0.phyAddr;
    ctrl0.Wmfe_Tbli.u4Stride = 0x100;
    ctrl0.Wmfe_Tbli.u4BufSize = wmf_tbli_f0.size;

    ctrl0.Wmfe_Dpo.u4BufVA = wmf_dpo_frame_0.virtAddr;
    ctrl0.Wmfe_Dpo.u4BufPA = wmf_dpo_frame_0.phyAddr;
    ctrl0.Wmfe_Dpo.u4Stride = 0xF0;
    ctrl0.Wmfe_Dpo.u4BufSize = wmf_dpo_frame_0.size;

    ctrl0.Wmfe_Maski.u4BufVA = wmf_dpi_f0.virtAddr;
    ctrl0.Wmfe_Maski.u4BufPA = wmf_dpi_f0.phyAddr;
    ctrl0.Wmfe_Maski.u4Stride = 0xF0;
    ctrl0.Wmfe_Maski.u4BufSize = wmf_dpi_f0.size;

    ctrl0.Wmfe_Dpnd_En = true;      //V1.1
    ctrl0.Wmfe_Mask_En = true;      //V1.1
    ctrl0.WmfeHorzScOrd = WMFE_HORZ_RIGHT2LEFT; //V1.1
    ctrl0.WmfeVertScOrd = WMFE_VERT_TOP2BOTTOM; //V1.1
    ctrl0.Wmfe_Mask_Value = 0xff;//V1.2
    ctrl0.Wmfe_Mask_Mode = 0x1; //V1.2
    ctrl0.Wmfe_Chroma_En = 0x1; //V1.3

#endif

#if WMFE_ENABLE_1
    ctrl1.Wmfe_Enable = true;
    ctrl1.WmfeFilterSize = WMFE_FILTER_SIZE_7x7;
    ctrl1.Wmfe_Width = 0xF0;
    ctrl1.Wmfe_Height = 0x87;
    ctrl1.WmfeImgiFmt = OWE_IMGI_YC_FMT;
    ctrl1.WmfeDpiFmt= WMFE_DPI_D_FMT;

    ctrl1.Wmfe_Imgi.u4BufVA = wmf_imgi_f1.virtAddr;
    ctrl1.Wmfe_Imgi.u4BufPA = wmf_imgi_f1.phyAddr;
    ctrl1.Wmfe_Imgi.u4Stride = 0x1E0;
    ctrl1.Wmfe_Imgi.u4BufSize = wmf_imgi_f1.size;

    ctrl1.Wmfe_Dpi.u4BufVA = wmf_dpi_f1.virtAddr;
    ctrl1.Wmfe_Dpi.u4BufPA = wmf_dpi_f1.phyAddr;
    ctrl1.Wmfe_Dpi.u4Stride = 0xF0;
    ctrl1.Wmfe_Dpi.u4BufSize = wmf_dpi_f1.size;

    ctrl1.Wmfe_Tbli.u4BufVA = wmf_tbli_f1.virtAddr;
    ctrl1.Wmfe_Tbli.u4BufPA = wmf_tbli_f1.phyAddr;
    ctrl1.Wmfe_Tbli.u4Stride = 0x100;
    ctrl1.Wmfe_Tbli.u4BufSize = wmf_tbli_f1.size;

    ctrl1.Wmfe_Dpo.u4BufVA = wmf_dpo_frame_1.virtAddr;
    ctrl1.Wmfe_Dpo.u4BufPA = wmf_dpo_frame_1.phyAddr;
    ctrl1.Wmfe_Dpo.u4Stride = 0xF0;
    ctrl1.Wmfe_Dpo.u4BufSize = wmf_dpo_frame_1.size;

    ctrl1.Wmfe_Maski.u4BufVA = wmf_dpi_f1.virtAddr;
    ctrl1.Wmfe_Maski.u4BufPA = wmf_dpi_f1.phyAddr;
    ctrl1.Wmfe_Maski.u4Stride = 0xF0;
    ctrl1.Wmfe_Maski.u4BufSize = wmf_dpi_f1.size;

    ctrl1.Wmfe_Dpnd_En = true;      //V1.1
    ctrl1.Wmfe_Mask_En = true;      //V1.1
    ctrl1.WmfeHorzScOrd = WMFE_HORZ_LEFT2RIGHT; //V1.1
    ctrl1.WmfeVertScOrd = WMFE_VERT_BOTTOM2TOP; //V1.1
    ctrl1.Wmfe_Mask_Value = 0xff;//V1.2
    ctrl1.Wmfe_Mask_Mode = 0x1; //V1.2
    ctrl1.Wmfe_Chroma_En = 0x1; //V1.3
#endif

#if WMFE_ENABLE_2
    ctrl2.Wmfe_Enable = true;
    ctrl2.WmfeFilterSize = WMFE_FILTER_SIZE_7x7;
    ctrl2.Wmfe_Width = 0xF0;
    ctrl2.Wmfe_Height = 0x87;
    ctrl2.WmfeImgiFmt = OWE_IMGI_YC_FMT;
    ctrl2.WmfeDpiFmt= WMFE_DPI_D_FMT;

    ctrl2.Wmfe_Imgi.u4BufVA = wmf_imgi_f2.virtAddr;
    ctrl2.Wmfe_Imgi.u4BufPA = wmf_imgi_f2.phyAddr;
    ctrl2.Wmfe_Imgi.u4Stride = 0x1E0;
    ctrl2.Wmfe_Imgi.u4BufSize = wmf_imgi_f2.size;

    ctrl2.Wmfe_Dpi.u4BufVA = wmf_dpi_f2.virtAddr;
    ctrl2.Wmfe_Dpi.u4BufPA = wmf_dpi_f2.phyAddr;
    ctrl2.Wmfe_Dpi.u4Stride = 0xF0;
    ctrl2.Wmfe_Dpi.u4BufSize = wmf_dpi_f2.size;

    ctrl2.Wmfe_Tbli.u4BufVA = wmf_tbli_f2.virtAddr;
    ctrl2.Wmfe_Tbli.u4BufPA = wmf_tbli_f2.phyAddr;
    ctrl2.Wmfe_Tbli.u4Stride = 0x100;
    ctrl2.Wmfe_Tbli.u4BufSize = wmf_tbli_f2.size;

    ctrl2.Wmfe_Dpo.u4BufVA = wmf_dpo_frame_2.virtAddr;
    ctrl2.Wmfe_Dpo.u4BufPA = wmf_dpo_frame_2.phyAddr;
    ctrl2.Wmfe_Dpo.u4Stride = 0xF0;
    ctrl2.Wmfe_Dpo.u4BufSize = wmf_dpo_frame_2.size;

    ctrl2.Wmfe_Maski.u4BufVA = wmf_dpi_f2.virtAddr;
    ctrl2.Wmfe_Maski.u4BufPA = wmf_dpi_f2.phyAddr;
    ctrl2.Wmfe_Maski.u4Stride = 0xF0;
    ctrl2.Wmfe_Maski.u4BufSize = wmf_dpi_f2.size;

    ctrl2.Wmfe_Dpnd_En = false;      //V1.1
    ctrl2.Wmfe_Mask_En = false;      //V1.1
    ctrl2.WmfeHorzScOrd = WMFE_HORZ_LEFT2RIGHT; //V1.1
    ctrl2.WmfeVertScOrd = WMFE_VERT_TOP2BOTTOM; //V1.1
    ctrl2.Wmfe_Mask_Value = 0xff;//V1.2
    ctrl2.Wmfe_Mask_Mode = 0x0; //V1.2
    ctrl2.Wmfe_Chroma_En = 0x1; //V1.3
#endif

    wmfeconfig.mWMFECtrlVec.push_back(ctrl0);
    wmfeconfig.mWMFECtrlVec.push_back(ctrl1);
    wmfeconfig.mWMFECtrlVec.push_back(ctrl2);
    rWmfeParams.mEGNConfigVec.push_back(wmfeconfig);
    g_bWMFECallback = MFALSE;

    ret=pWmfStream->EGNenque(rWmfeParams);
    if(!ret)
        printf("---ERRRRRRRRR [dpe_default..wmfe enque fail\n]");
    else
        printf("---[dpe_default..wmfe enque done\n]");


    /* Checking for Callback : OCC */
    do{
        usleep(100000);
        if (MTRUE == g_bOCCCallback)
        {
            break;
        }
    }while(1);

#if 0
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_mp); 
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_mv);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_rp); 
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_rv);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &wdma_occ); 
#endif
    /* Golden Comparision : OCC*/
    OCC_WDMA_bittrue(wdma, wdma_occ, occconfig);


    /* Checking for Callback : WMFE */
    do{
        usleep(100000);
        if (MTRUE == g_bWMFECallback)
        {
            break;
        }
    }while(1);

    /* TODO: Golden Comparision : OCC*/
    WMFE_bittrue(golden_dpe_wmf_dpo_frame_0, ctrl0);
    WMFE_bittrue(golden_dpe_wmf_dpo_frame_1, ctrl1);
    WMFE_bittrue(golden_dpe_wmf_dpo_frame_2, ctrl2);


    /* Memory Free Using IMem */
    mpImemDrv->freeVirtBuf(&buf_mp);
    mpImemDrv->freeVirtBuf(&buf_mv);
    mpImemDrv->freeVirtBuf(&buf_rp);
    mpImemDrv->freeVirtBuf(&buf_rv);
    mpImemDrv->freeVirtBuf(&wdma_occ);

#if WMFE_ENABLE_0
    mpImemDrv->freeVirtBuf(&wmf_dpo_frame_0);
    mpImemDrv->freeVirtBuf(&wmf_imgi_f0);
    mpImemDrv->freeVirtBuf(&wmf_dpi_f0);
    mpImemDrv->freeVirtBuf(&wmf_tbli_f0);
#endif
#if WMFE_ENABLE_1
    mpImemDrv->freeVirtBuf(&wmf_dpo_frame_1);
    mpImemDrv->freeVirtBuf(&wmf_imgi_f1);
    mpImemDrv->freeVirtBuf(&wmf_dpi_f1);
    mpImemDrv->freeVirtBuf(&wmf_tbli_f1);
#endif
#if WMFE_ENABLE_2
    mpImemDrv->freeVirtBuf(&wmf_dpo_frame_2);
    mpImemDrv->freeVirtBuf(&wmf_imgi_f2);
    mpImemDrv->freeVirtBuf(&wmf_dpi_f2);
    mpImemDrv->freeVirtBuf(&wmf_tbli_f2);
#endif

    /* Objects - */
    pOccStream->uninit();   
    printf("[%s] OccStream uninit done\n", __func__);
    pOccStream->destroyInstance(USER_NAME);

    pWmfStream->uninit();   
    printf("[%s] WmfStream uninit done\n", __func__);
    pWmfStream->destroyInstance(USER_NAME);

    mpImemDrv->uninit();
    printf("[%s] Imem uninit done\n", __func__);

    return ret;
}
