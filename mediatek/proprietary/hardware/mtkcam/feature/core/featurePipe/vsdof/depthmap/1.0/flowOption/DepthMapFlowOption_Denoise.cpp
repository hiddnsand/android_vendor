/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**
 * @file DepthMapFlowOption_Denoise.cpp
 * @brief DepthMapFlowOption for VSDOF
 */

// Standard C header file

// Android system/core header file

// mtkcam custom header file
#include <camera_custom_stereo.h>
// mtkcam global header file
#include <mtkcam/def/common.h>
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>
#include <mtkcam/drv/iopipe/PostProc/IHalPostProcPipe.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/feature/stereo/hal/stereo_setting_provider.h>
#include <isp_tuning.h>
// Module header file
#include <stereo_tuning_provider.h>
// Local header file
#include "DepthMapFlowOption_Denoise.h"
#include "../DepthMapPipe.h"
#include "../DepthMapPipeNode.h"
#include "./bufferPoolMgr/BaseBufferHandler.h"

#define PIPE_MODULE_TAG "DepthMapPipe"
#define PIPE_CLASS_TAG "DepthMapFlowOption_Denoise"
#include <featurePipe/core/include/PipeLog.h>

/*******************************************************************************
* Namespace start.
********************************************************************************/
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe_DepthMap {

typedef NodeBufferSizeMgr::DPEBufferSize DPEBufferSize;
typedef NodeBufferSizeMgr::P2ABufferSize P2ABufferSize;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Instantiation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DepthMapFlowOption_Denoise::
DepthMapFlowOption_Denoise(
    sp<DepthMapPipeSetting> pSetting,
    sp<DepthMapPipeOption> pOption
)
: BayerMonoQTemplateProvider(pSetting, pOption, this)
{
    // flow option config
    mConfig.mbCaptureFDEnable = MFALSE;
    mConfig.mFDSize = MSize(0,0); // not support FD
}

DepthMapFlowOption_Denoise::
~DepthMapFlowOption_Denoise()
{
    MY_LOGD("[Destructor] +");
    MY_LOGD("[Destructor] -");
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  P2AFlowOption Public Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

MBOOL
DepthMapFlowOption_Denoise::
buildQParam(
    DepthMapRequestPtr pReq,
    const Stereo3ATuningRes& tuningResult,
    QParams& rOutParam
)
{
    MBOOL bRet;
    DepthMapPipeOpState opState = pReq->getRequestAttr().opState;
    if(opState == eSTATE_CAPTURE)
        bRet = TemplateProvider::buildQParams_CAPTURE(pReq, tuningResult, rOutParam);
    else
    {
        MY_LOGE("Denoise cannot support NORMAL mode.");
        return MFALSE;
    }

    return bRet;
}

INPUT_RAW_TYPE
DepthMapFlowOption_Denoise::
getInputRawType(
    sp<DepthMapEffectRequest> pReq,
    StereoP2Path path
)
{
    return BayerMonoQTemplateProvider::getInputRawType(pReq, path);
}


MBOOL
DepthMapFlowOption_Denoise::
onP2ProcessDone(
    P2AFMNode* pNode,
    sp<DepthMapEffectRequest> pRequest
)
{
    sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();
    // config output buffers for N3D input
    // FEO
    pBufferHandler->configOutBuffer(eDPETHMAP_PIPE_NODEID_P2AFM, BID_P2A_OUT_FE1BO, eDPETHMAP_PIPE_NODEID_N3D);
    pBufferHandler->configOutBuffer(eDPETHMAP_PIPE_NODEID_P2AFM, BID_P2A_OUT_FE2BO, eDPETHMAP_PIPE_NODEID_N3D);
    pBufferHandler->configOutBuffer(eDPETHMAP_PIPE_NODEID_P2AFM, BID_P2A_OUT_FE1CO, eDPETHMAP_PIPE_NODEID_N3D);
    pBufferHandler->configOutBuffer(eDPETHMAP_PIPE_NODEID_P2AFM, BID_P2A_OUT_FE2CO, eDPETHMAP_PIPE_NODEID_N3D);
    // FMO
    pBufferHandler->configOutBuffer(eDPETHMAP_PIPE_NODEID_P2AFM, BID_P2A_OUT_FMBO_LR, eDPETHMAP_PIPE_NODEID_N3D);
    pBufferHandler->configOutBuffer(eDPETHMAP_PIPE_NODEID_P2AFM, BID_P2A_OUT_FMBO_RL, eDPETHMAP_PIPE_NODEID_N3D);
    pBufferHandler->configOutBuffer(eDPETHMAP_PIPE_NODEID_P2AFM, BID_P2A_OUT_FMCO_LR, eDPETHMAP_PIPE_NODEID_N3D);
    pBufferHandler->configOutBuffer(eDPETHMAP_PIPE_NODEID_P2AFM, BID_P2A_OUT_FMCO_RL, eDPETHMAP_PIPE_NODEID_N3D);
    // CC_in/Rect_in
    pBufferHandler->configOutBuffer(eDPETHMAP_PIPE_NODEID_P2AFM, BID_P2A_OUT_CC_IN1, eDPETHMAP_PIPE_NODEID_N3D);
    pBufferHandler->configOutBuffer(eDPETHMAP_PIPE_NODEID_P2AFM, BID_P2A_OUT_CC_IN2, eDPETHMAP_PIPE_NODEID_N3D);
    pBufferHandler->configOutBuffer(eDPETHMAP_PIPE_NODEID_P2AFM, BID_P2A_OUT_RECT_IN1, eDPETHMAP_PIPE_NODEID_N3D);
    pBufferHandler->configOutBuffer(eDPETHMAP_PIPE_NODEID_P2AFM, BID_P2A_OUT_RECT_IN2, eDPETHMAP_PIPE_NODEID_N3D);
    // pass to N3D
    pNode->handleDataAndDump(P2A_TO_N3D_FEFM_CCin, pRequest);

    return MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  DepthMapFlowOption Public Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

MBOOL
DepthMapFlowOption_Denoise::
init()
{
    // prepare template
    MBOOL bRet = BayerMonoQTemplateProvider::init(&mSizeMgr);
    return bRet;
}

MBOOL
DepthMapFlowOption_Denoise::
queryReqAttrs(
    sp<DepthMapEffectRequest> pRequest,
    EffectRequestAttrs& rReqAttrs
)
{
    // Denoise only has capture
    rReqAttrs.isEISOn = MFALSE;
    rReqAttrs.opState = eSTATE_CAPTURE;
    rReqAttrs.bufferScenario = eBUFFER_POOL_SCENARIO_CAPTURE;

    return MTRUE;
}


MBOOL
DepthMapFlowOption_Denoise::
queryPipeNodeBitSet(PipeNodeBitSet& nodeBitSet)
{
    nodeBitSet.reset();
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_P2AFM, 1);
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_N3D, 1);
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_DPE, 1);
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_WMF, 0);
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_OCC, 0);
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_FD, 0);
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_GF, 0);
    nodeBitSet.set(eDPETHMAP_PIPE_NODEID_P2ABAYER, 0);
    return MTRUE;
}

MBOOL
DepthMapFlowOption_Denoise::
buildPipeGraph(DepthMapPipe* pPipe, const DepthMapPipeNodeMap& nodeMap)
{
    DepthMapPipeNode* pP2AFMNode = nodeMap.valueFor(eDPETHMAP_PIPE_NODEID_P2AFM);
    DepthMapPipeNode* pN3DNode = nodeMap.valueFor(eDPETHMAP_PIPE_NODEID_N3D);
    DepthMapPipeNode* pDPENode = nodeMap.valueFor(eDPETHMAP_PIPE_NODEID_DPE);

    #define CONNECT_DATA(DataID, src, dst)\
        pPipe->connectData(DataID, DataID, src, dst);\
        mvAllowDataIDMap.add(DataID, MTRUE);

    // P2AFM to N3D + P2A_OUTput
    CONNECT_DATA(P2A_TO_N3D_FEFM_CCin, *pP2AFMNode, *pN3DNode);
    CONNECT_DATA(P2A_OUT_MV_F, *pP2AFMNode, pPipe);
    CONNECT_DATA(P2A_OUT_FD, *pP2AFMNode, pPipe);
    CONNECT_DATA(P2A_OUT_MV_F_CAP, *pP2AFMNode, pPipe);
    CONNECT_DATA(TO_DUMP_BUFFERS, *pP2AFMNode, pPipe);
    // P2AFM buffer dump
    CONNECT_DATA(TO_DUMP_RAWS, *pP2AFMNode, pPipe);
    // Hal Meta frame output
    CONNECT_DATA(P2A_TO_N3D_FEFM_CCin, *pP2AFMNode, *pN3DNode);
    // N3D to DPE/OCC
    CONNECT_DATA(N3D_TO_DPE_MVSV_MASK, *pN3DNode, *pDPENode);
    CONNECT_DATA(N3D_OUT_JPS_WARPMTX, *pN3DNode, pPipe);
    // DPE output
    CONNECT_DATA(DPE_OUT_DISPARITY, *pDPENode, pPipe);
    // Hal Meta frame output
    CONNECT_DATA(DEPTHMAP_META_OUT, *pN3DNode, pPipe);

    // default node graph - Error handling
    for(size_t index=0;index<nodeMap.size();++index)
    {
        CONNECT_DATA(ERROR_OCCUR_NOTIFY, *nodeMap.valueAt(index), pPipe);
    }

    pPipe->setRootNode(pP2AFMNode);

    return MTRUE;
}

MBOOL
DepthMapFlowOption_Denoise::
checkConnected(
    DepthMapDataID dataID
)
{
    // check data id is allowed to handledata or not.
    if(mvAllowDataIDMap.indexOfKey(dataID)>=0)
    {
        return MTRUE;
    }
    return MFALSE;
}

MBOOL
DepthMapFlowOption_Denoise::
config3ATuningMeta(
    StereoP2Path path,
    MetaSet_T& rMetaSet
)
{
    // main1 path need to-W profile
    if(path == eP2APATH_MAIN1)
    {
        #warning "[FIXME][XXXX] EIspProfile_N3D_Capture_toW is removed"
        // trySetMetadata<MUINT8>(&rMetaSet.halMeta, MTK_3A_ISP_PROFILE, NSIspTuning::EIspProfile_N3D_Capture_toW);
    }
    else
    {
        trySetMetadata<MUINT8>(&rMetaSet.halMeta, MTK_3A_ISP_PROFILE, NSIspTuning::EIspProfile_N3D_Capture);
    }

    return MTRUE;
}

DepthMapBufferID
DepthMapFlowOption_Denoise::
reMapBufferID(
    const EffectRequestAttrs&,
    DepthMapBufferID bufferID
)
{   // no remapped
    return bufferID;
}

}; // NSFeaturePipe_DepthMap
}; // NSCamFeature
}; // NSCam
