package com.mediatek.entitlement;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;
import com.android.ims.ImsManager;
import com.android.internal.telephony.TelephonyIntents;
import com.mediatek.internal.telephony.MtkSubscriptionManager;

import java.util.HashMap;
import java.util.Map;

interface Consumer<T> {
    void accept(T t);
}

public class EntitlementService extends Service {
    private static final String TAG = "EntitlementService";
    private static final boolean DEBUG = true;

    private final Map<String, EntitlementHandling> mServices = new HashMap<>();
    private final Map<String, Provisioning> mProvisionings = new HashMap<>();
    private final RemoteCallbackList<ISesServiceListener> mListeners = new RemoteCallbackList<>();

    private static final String BROADCAST_REGID_UPDATE = "com.mediatek.entitlement.gcm.RegistrationIdUpdate";
    private static final String BROADCAST_PUSH_NOTIFICATION = "com.mediatek.entitlement.gcm.PushNotification";

    static final String ACTION_PROVISIONING_PENDING = "provisioning-pending";

    // Periodically check whether a service is still pending.
    private class Provisioning {
        private final String mService;
        private final int mAlarmInterval;
        private int mRetryTimes;

        Provisioning(String service, int alarmInterval, int retryTimesMax) {
            mService = service;
            mAlarmInterval = alarmInterval;
            mRetryTimes = retryTimesMax;

            setAlarm();
        }

        synchronized boolean shouldRetry() {
            return mRetryTimes > 0;
        }

        synchronized boolean retry() {
            if (!shouldRetry()) {
                return false;
            }

            mRetryTimes--;
            getHandling(mService).startEntitlementCheck();
            return true;
        }

        void setAlarm() {
            Intent i = new Intent(EntitlementService.this, EntitlementService.class);
            i.setAction(ACTION_PROVISIONING_PENDING);
            i.putExtra("service", mService);
            PendingIntent pi = PendingIntent.getService(EntitlementService.this, 1, i, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * 60 * mAlarmInterval, pi);
        }

        void cancelAlarm() {
            Intent i = new Intent(EntitlementService.this, EntitlementService.class);
            i.setAction(ACTION_PROVISIONING_PENDING);
            i.putExtra("service", mService);
            PendingIntent pi = PendingIntent.getService(EntitlementService.this, 1, i, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pi);
        }
    }

    static final String ACTION_ENTITLEMENT_CHECK = "entitlement-check";

    // Once a service is entitled, check the entitlement daily
    private class EntitlementCheck {
        void setAlarm(String service) {
            BootCompletedReceiver.enable(EntitlementService.this);

            Intent i = new Intent(EntitlementService.this, EntitlementService.class);
            i.setAction(ACTION_ENTITLEMENT_CHECK);
            i.putExtra("service", service);
            PendingIntent pi = PendingIntent.getService(EntitlementService.this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
        }

        void cancelAlarm(String service) {
            BootCompletedReceiver.disable(EntitlementService.this);

            Intent i = new Intent(EntitlementService.this, EntitlementService.class);
            i.setAction(ACTION_ENTITLEMENT_CHECK);
            i.putExtra("service", service);
            PendingIntent pi = PendingIntent.getService(EntitlementService.this, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pi);
        }
    }

    private EntitlementCheck mEntitlementCheck = new EntitlementCheck();

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            log("Received Broadcast with action = " + action);

            if (TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED.equals(action)) {
                if (intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                        MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE)
                        == MtkSubscriptionManager.EXTRA_VALUE_REMOVE_SIM) {
                    log("REMOVE_SIM, reset all state and set wfc disabled");

                    synchronized (mServices) {
                        // Clear all services. Can recreate them if necessary.
                        for (final String service : mServices.keySet()) {
                            notifyListeners(new Consumer<ISesServiceListener>() {
                                @Override
                                public void accept(ISesServiceListener l) {
                                    try {
                                        l.onEntitlementEvent(service, "remove_sim", null);
                                    } catch (RemoteException ignored) {
                                    }
                                }
                            });

                            mServices.get(service).clearListeners();
                        }
                        mServices.clear();
                    }

                    // in case of reset state, turn off vowifi
                    ImsManager.setWfcSetting(getApplicationContext(), false);
                } else if (intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                        MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE)
                        == MtkSubscriptionManager.EXTRA_VALUE_NEW_SIM) {
                    log("NEW_SIM, notify UI");

                    synchronized (mServices) {
                        // Notify new sim.
                        for (String service : mServices.keySet()) {
                            notifyListeners(new Consumer<ISesServiceListener>() {
                                @Override
                                public void accept(ISesServiceListener l) {
                                    try {
                                        l.onEntitlementEvent(service, "new_sim", null);
                                    } catch (RemoteException ignored) {
                                    }
                                }
                            });
                        }
                    }
                }
            } else if (BROADCAST_REGID_UPDATE.equals(action)) {
                String regId = intent.getStringExtra("RegId");
                synchronized (mServices) {
                    for (String service : mServices.keySet()) {
                        mServices.get(service).updateGcmToken(regId);
                    }
                }
            } else if (BROADCAST_PUSH_NOTIFICATION.equals(action)) {
                synchronized (mServices) {
                    for (String service : mServices.keySet()) {
                        mServices.get(service).handleGcmNotification(intent.getExtras());
                    }
                }
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        final IntentFilter filter = new IntentFilter();
        /// M: Dynamic SIM Switch
        filter.addAction(TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED);
        filter.addAction(BROADCAST_REGID_UPDATE);
        filter.addAction(BROADCAST_PUSH_NOTIFICATION);
        registerReceiver(mBroadcastReceiver, filter);
    }

    private EntitlementHandling getHandling(final String service) {
        synchronized (mServices) {
            EntitlementHandling handling = mServices.get(service);
            if (handling == null) {
                handling = new EntitlementHandling(service, getApplicationContext(), new EntitlementHandling.Listener() {
                    @Override
                    public void onStateChange(EntitlementHandling handling, int state) {
                        //  Check state and set alarm
                        boolean notify = true;

                        if (handling.getState() == EntitlementHandling.STATE_ENTITLED) {
                            mEntitlementCheck.setAlarm(service);

                            synchronized (mProvisionings) {
                                Provisioning provisioning = mProvisionings.get(service);
                                if (provisioning != null) {
                                    provisioning.cancelAlarm();
                                    mProvisionings.remove(service);
                                }
                            }
                        } else {
                            mEntitlementCheck.cancelAlarm(service);

                            if (handling.getState() == EntitlementHandling.STATE_NOT_ENTITLED) {
                                synchronized (mProvisionings) {
                                    Provisioning provisioning = mProvisionings.get(service);
                                    if (provisioning != null) {
                                        if (provisioning.shouldRetry()) {
                                            provisioning.setAlarm();
                                            notify = false;
                                        } else {
                                            // provisioning failed after 30/60min retry.
                                            mProvisionings.remove(service);
                                        }
                                    }
                                    ImsManager.setWfcSetting(getApplicationContext(), false);
                                }
                            }
                        }

                        if (notify) {
                            notifyListeners(new Consumer<ISesServiceListener>() {
                                @Override
                                public void accept(ISesServiceListener l) {
                                    try {
                                        l.onEntitlementEvent(service, translateToExternalState(state), null);
                                    } catch (RemoteException ignored) {
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onWebsheetPost(String url, String serverData) {
                        notifyListeners(new Consumer<ISesServiceListener>() {
                            @Override
                            public void accept(ISesServiceListener l) {
                                try {
                                    l.onWebsheetPost(url, serverData);
                                } catch (RemoteException ignored) {
                                }
                            }
                        });
                    }

                    @Override
                    public void onInfo(Bundle info) {
                        Log.d(TAG, "Info discovered: " + info);
                        notifyListeners(new Consumer<ISesServiceListener>() {
                            @Override
                            public void accept(ISesServiceListener l) {
                                try {
                                    l.onEntitlementEvent(service, "info", info);
                                } catch (RemoteException ignored) {
                                }
                            }
                        });
                    }
                });
                mServices.put(service, handling);
            }
            return handling;

        }
    }

    private void notifyListeners(Consumer<ISesServiceListener> consumer) {
        synchronized (mListeners) {
            int i = mListeners.beginBroadcast();
            while (i > 0) {
                i--;
                consumer.accept(mListeners.getBroadcastItem(i));
            }
            mListeners.finishBroadcast();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        log("onStartCommand: " + intent + " " + startId);

        if (ACTION_ENTITLEMENT_CHECK.equals(intent.getAction())) {
            String service = intent.getStringExtra("service");
            getHandling(service).startEntitlementCheck();
        } else if (ACTION_PROVISIONING_PENDING.equals(intent.getAction())) {
            String service = intent.getStringExtra("service");
            synchronized (mProvisionings) {
                Provisioning provisioning = mProvisionings.get(service);
                if (provisioning != null) {
                    provisioning.retry();
                }
            }
        }
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        log("onBind: " + intent);
        return new ISesService.Stub() {
            @Override
            public void startEntitlementCheck(String service, int retryTimer, int retryTimes) {
                log("startEntitlementCheckService " + service);

                if (retryTimer > 0) {
                    synchronized (mProvisionings) {
                        mProvisionings.put(service, new Provisioning(service, retryTimer, retryTimes));
                    }
                } else {
                    getHandling(service).startEntitlementCheck();
                }
            }

            @Override
            public void stopEntitlementCheck(String service) {
                log("stopEntitlementCheck");
                EntitlementHandling handling = getHandling(service);
                handling.stopEntitlementCheck();
            }

            @Override
            public void updateLocationAndTc(String service) {
                log("updateLocationAndTc");
                EntitlementHandling handling = getHandling(service);
                handling.updateLocationAndTc();
            }

            @Override
            public String getCurrentEntitlementState(String service) {
                log("getCurrentEntitlementState");
                EntitlementHandling handling = getHandling(service);
                return translateToExternalState(handling.getState());
            }

            @Override
            public void registerListener(ISesServiceListener listener) {
                log("registerStateListener");
                if (listener != null) {
                    synchronized (mListeners) {
                        log("registerStateListener " + listener.getClass().toString());
                        mListeners.register(listener);
                    }
                }
            }

            @Override
            public void unregisterListener(ISesServiceListener listener) {
                if (listener != null) {
                    synchronized (mListeners) {
                        log("unregisterStateListener " + listener.getClass().toString());
                        mListeners.unregister(listener);
                    }
                }
            }

            @Override
            public int getLastErrorCode() {
                int errCode = ErrorCodes.getErrorCode();
                log("getErrorCode = " + errCode);
                return errCode;
            }

            @Override
            public void deactivateService(String service) {
                log("deactivateService");
                EntitlementHandling handling = getHandling(service);
                handling.deactivateService();
            }
        };
    }

    private String translateToExternalState(int internalState) {
        switch (internalState) {
            case EntitlementHandling.STATE_NOT_ENTITLED:
                return "not-entitled";
            case EntitlementHandling.STATE_ACTIVATING:
            case EntitlementHandling.STATE_PENDING_WITH_NO_TC:
            case EntitlementHandling.STATE_PENDING_WITH_NO_ADDRESS:
            case EntitlementHandling.STATE_PENDING_WITH_ADDRESS:
                return "pending";
            case EntitlementHandling.STATE_ENTITLED:
                return "entitled";
            default:
                Log.e(TAG, "Unhandled state: " + internalState);
                return "Unhandled";
        }
    }

    private static void log(String s) {
        if (DEBUG) Log.d(TAG, s);
    }
}
