package com.mediatek.mms.plugin;

import com.google.android.mms.pdu.PduHeaders;
import com.mediatek.mms.ext.DefaultOpDefaultRetrySchemeExt;

public class Op09DefaultRetrySchemeExt extends DefaultOpDefaultRetrySchemeExt {

    @Override
    public int[] init(int... messageType) {
        if (messageType != null && messageType.length == 1) {
            return new Op09MmsConfigExt().getMmsRetryScheme(messageType[0]);
        } else {
            int msgType = PduHeaders.MESSAGE_TYPE_RETRIEVE_CONF;
            return new Op09MmsConfigExt().getMmsRetryScheme(msgType);
        }
    }
}
