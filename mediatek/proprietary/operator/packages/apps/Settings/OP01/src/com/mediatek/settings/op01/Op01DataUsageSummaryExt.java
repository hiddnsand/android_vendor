/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op01;

import android.content.Context;
import android.content.IntentFilter;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.internal.telephony.MtkTelephonyIntents;
import com.mediatek.settings.ext.DefaultDataUsageSummaryExt;

import java.util.List;

/**
 * Data Usage summary info plugin.
 */
public class Op01DataUsageSummaryExt extends DefaultDataUsageSummaryExt {
    private static final String TAG = "Op01DataUsageSummaryExt";
    private static final String[] MCCMNC_TABLE_TYPE_CMCC = {
        "46000", "46002", "46004", "46007", "46008"};
    private static final String[] MCCMNC_TABLE_TYPE_CT = {
        "45502", "46003", "46011", "46012", "46013"};

    private Context mContext;

    /**
     * Init context.
     * @param context The Context
     */
    public Op01DataUsageSummaryExt(Context context) {
        super(context);
        mContext = context;
    }

    /**
     * app use to judge the Card is CMCC
     * @param slotId
     * @return true is CMCC
     */
    private boolean isCMCCCard(int subId) {
        Log.d("@M_" + TAG, "isCMCCCard, subId = " + subId);
        String simOperator = null;
        simOperator = getSimOperator(subId);
        if (simOperator != null) {
            Log.d("@M_" + TAG, "isCMCCCard, simOperator =" + simOperator);
            for (String mccmnc : MCCMNC_TABLE_TYPE_CMCC) {
                if (simOperator.equals(mccmnc)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * app use to judge the Card is CT.
     * @param subId
     * @return true is CT
     */
    private boolean isCTCard(int subId) {
        Log.d("@M_" + TAG, "isCTCard, subId = " + subId);
        String simOperator = null;
        simOperator = getSimOperator(subId);
        if (simOperator != null) {
            Log.d("@M_" + TAG, "isCTCard, simOperator =" + simOperator);
            for (String mccmnc : MCCMNC_TABLE_TYPE_CT) {
                if (simOperator.equals(mccmnc)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Gets the MCC+MNC (mobile country code + mobile network code)
     * of the provider of the SIM. 5 or 6 decimal digits.
     * Availability: The result of calling getSimState()
     * must be android.telephony.TelephonyManager.SIM_STATE_READY.
     * @param slotId  Indicates which SIM to query.
     * @return MCC+MNC (mobile country code + mobile network code)
     * of the provider of the SIM. 5 or 6 decimal digits.
     */
    private String getSimOperator(int subId) {
        if (subId < 0) {
            return null;
        }
        String simOperator = null;
        int status = TelephonyManager.SIM_STATE_UNKNOWN;
        int slotId = SubscriptionManager.getSlotIndex(subId);
        if (slotId != SubscriptionManager.INVALID_SIM_SLOT_INDEX) {
             status = TelephonyManager.getDefault().getSimState(slotId);
        }
        if (status == TelephonyManager.SIM_STATE_READY) {
            simOperator = TelephonyManager.getDefault().getSimOperator(subId);
        }
        Log.d("@M_" + TAG, "getSimOperator, simOperator = " + simOperator + " subId = " + subId);
        return simOperator;
    }

    @Override
    public boolean isAllowDataEnable(int subId) {
        Log.d("@M_" + TAG, "isAllowDataEnable, cursubId = " + subId);
        if (subId < 0) {
            return true;
        }
        List<SubscriptionInfo> si = SubscriptionManager.from(mContext)
            .getActiveSubscriptionInfoList();
        if (si != null && si.size() > 1) {
            int otherId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
            for (int i = 0; i < si.size(); i++) {
                SubscriptionInfo subInfo = si.get(i);
                int curId = subInfo.getSubscriptionId();
                if (curId != subId) {
                    otherId = curId;
                    break;
                }
            }
            if (!Op01SimManagementExt.isTargetSubRadioOn(subId)) {
                Log.d("@M_" + TAG, "isAllowDataEnable, disable");
                return false;
            }

            if (isCMCCCard(otherId) && !isCMCCCard(subId)) {
                if (Op01SimManagementExt.isTargetSubRadioOn(otherId)) {
                    Log.d("@M_" + TAG, "isAllowDataEnable, close switch");
                    //TelephonyManager.from(mContext).setDataEnabled(subId, false);
                    return false;
                }
            }
            String plmn = getSimOperator(otherId);
            if ((plmn == null || plmn.equals("")) && !isCMCCCard(subId)) {
                Log.d("@M_" + TAG, "isAllowDataEnable, disable");
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean customDualReceiver(String action) {
        Log.d("@M_" + TAG, "action = "+  action);

        if (null != action
                && (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(action)
                       || MtkTelephonyIntents.ACTION_RADIO_STATE_CHANGED.equals(action))) {
            return true;
        }
        return false;
    }

    @Override
    public void customReceiver(IntentFilter intentFilter) {
        Log.d("@M_" + TAG, "intentFilter add sim_state_changed ");
        intentFilter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        intentFilter.addAction(MtkTelephonyIntents.ACTION_RADIO_STATE_CHANGED);
    }

    /**
     * Allow to disable data for other subscriptions.
     */
    @Override
    public boolean isAllowDataDisableForOtherSubscription() {
        Log.i(TAG, "isAllowDataDisableForOtherSubscription true");
        return true;
    }
}