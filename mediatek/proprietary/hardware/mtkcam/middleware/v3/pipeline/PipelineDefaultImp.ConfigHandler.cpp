/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/HwPipeline/ConfigHandler"
//
#include "PipelineDefaultImp.h"
#include "PipelineUtility.h"
//
#include <mtkcam/drv/IHalSensor.h>
//
#include <mtkcam/pipeline/hwnode/P1Node.h>
#include <mtkcam/pipeline/hwnode/P2Node.h>
#include <mtkcam/pipeline/hwnode/FDNode.h>
#include <mtkcam/pipeline/hwnode/JpegNode.h>
#include <mtkcam/pipeline/hwnode/RAW16Node.h>
#include <mtkcam/pipeline/extension/MFNR.h>
//
#include <mtkcam/utils/metastore/IMetadataProvider.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
//
#include <mtkcam/utils/hw/IScenarioControl.h>
#include <mtkcam/utils/hw/HwTransform.h>
#include <mtkcam/aaa/IIspMgr.h>
//
#include <cutils/compiler.h>

#include <camera_custom_eis.h>

using namespace android;
using namespace NSCam;
using namespace NSCam::EIS;
using namespace NSCam::v3;
using namespace NSCam::Utils;
using namespace NSCam::v3::Utils;
using namespace NSCam::v3::NSPipelineContext;


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
configureLocked(
    PipeConfigParams const& rConfigParams,
    android::sp<IPipelineModel> pOldPipeline
)
{
    CAM_TRACE_NAME("PipelineDefaultImp::configure");
    FUNC_START;
    // RWLock::AutoWLock _l(mRWLock);
    //
    CHECK_ERROR(checkPermission());
    //
#if MTKCAM_HAVE_ADV_SETTING
    if (mCommonInfo->mpAdvSettingMgr != NULL)
    {
        PipelineDefaultImp::MyRecordParams recordParam = MyProcessedParams::genRecordParam(rConfigParams);
        NSCam::AdvCamSettingMgr::AdvCamInputParam input{
            recordParam.hasVRConsumer,
            recordParam.videoSize,
            recordParam.has4KVR,
            NULL,
            rConfigParams.mOperation_mode
        };
        mParams->mStreamingPipeParams.mpAdvSetting = mCommonInfo->mpAdvSettingMgr->generateAdvSetting(input);
    }
    mCommonInfo->mbFirstReqReceived = MFALSE;
#endif
    //
    mParams->update(rConfigParams, MFALSE);
    //
    MY_LOGD_IF(mCommonInfo->mpPipelineContext.get(), "strong count %d", mCommonInfo->mpPipelineContext->getStrongCount());
    mCommonInfo->mpPipelineContext = PipelineContext::create("DefaultPipeline");
    //
    if ( pOldPipeline.get() )
        mpOldCtx = pOldPipeline->getContext();
    MY_LOGD_IF(  mpOldCtx.get(), "old strong count %d", mpOldCtx->getStrongCount());
    //
    if ( mpOldCtx.get() )
    {
#if 1
        CHECK_ERROR(mpOldCtx->waitUntilNodeDrained(0x01));
#else
        CHECK_ERROR(mpOldCtx->waitUntilDrained());
#endif
    }
    //
    CHECK_ERROR(mCommonInfo->mpPipelineContext->beginConfigure(mpOldCtx));
    //
    CHECK_ERROR(configScenarioCtrlLocked());
    // create IStreamInfos
    CHECK_ERROR(setupAppStreamsLocked(rConfigParams));
    CHECK_ERROR(setupHalStreamsLocked(rConfigParams));
    //
    // config stream
    CHECK_ERROR(configContextLocked_Streams(mCommonInfo->mpPipelineContext));
    // config node
    CHECK_ERROR(configContextLocked_Nodes(mCommonInfo->mpPipelineContext, MFALSE));
    // config pipeline
    CHECK_ERROR(configContextLocked_Pipeline(mCommonInfo->mpPipelineContext));
    //
    CHECK_ERROR(mCommonInfo->mpPipelineContext->endConfigure(true));
    mpOldCtx = NULL;
    //
    CHECK_ERROR(configRequestRulesLocked());
    //
    mpDeviceHelper->configDone();
    //
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
P2Common::UsageHint
PipelineDefaultImp::ConfigHandler::
prepareP2Usage(
    P2Node::ePass2Type type
) const
{
    P2Common::UsageHint usage;
#if MTKCAM_HAVE_ADV_SETTING
#ifdef MTKCAM_SUPPORT_COMMON_P2NODE
    if( type == P2Node::PASS2_STREAM && mParams->isSupportAdvP2())
    {
        usage.mP2NodeType = P2Common::P2_NODE_COMMON;
        usage.m3DNRMode = mParams->mStreamingPipeParams.mpAdvSetting->nr3dMode;
        usage.mUseTSQ = mParams->mStreamingPipeParams.mpAdvSetting->useTSQ;
    }
#endif // MTKCAM_SUPPORT_COMMON_P2NODE
#endif
    usage.mPackedEisInfo = EisInfo::packEISInfo(mParams->mStreamingPipeParams.mEisInfo);
    if( mParams->mPipelineConfigParams.mbHasRecording )
    {
        usage.mAppMode = P2Common::APP_MODE_VIDEO;
    }
    if( mParams->mStreamSet.mpHalImage_P1_ResizerRaw != NULL )
    {
        usage.mStreamingSize = mParams->mStreamSet.mpHalImage_P1_ResizerRaw->getImgSize();
    }
    MY_LOGI("operation_mode=%d p2_type=%d p2_node_type=%d app_mode=%d 3dnr_mode=0x%x eis_mode=0x%x eis_factor=%d stream_size=%dx%d",
            mParams->mPipelineConfigParams.mOperation_mode, type, usage.mP2NodeType, usage.mAppMode,
            usage.m3DNRMode, mParams->mStreamingPipeParams.mEisInfo.mode,
            mParams->mStreamingPipeParams.mEisInfo.factor, usage.mStreamingSize.w, usage.mStreamingSize.h);
    return usage;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
PipelineDefaultImp::ConfigHandler::
evaluatePreviewSize(
    PipeConfigParams const& rConfigParams,
    MSize &rSize
)
{
    sp<IImageStreamInfo> pStreamInfo;
    int consumer_usage = 0;
    int allocate_usage = 0;
    int maxheight = rSize.h;
    int prevwidth = 0;
    int prevheight = 0;
    for (size_t i = 0; i < rConfigParams.vImage_Yuv_NonStall.size(); i++) {
        if  ( (pStreamInfo = rConfigParams.vImage_Yuv_NonStall[i]) != 0 ) {
            consumer_usage = pStreamInfo->getUsageForConsumer();
            allocate_usage = pStreamInfo->getUsageForAllocator();
            MY_LOGD("consumer : %X, allocate : %X", consumer_usage, allocate_usage);
            if(consumer_usage & GRALLOC_USAGE_HW_TEXTURE) {
                prevwidth = pStreamInfo->getImgSize().w;
                prevheight = pStreamInfo->getImgSize().h;
                break;
            }
            if(consumer_usage & GRALLOC_USAGE_HW_VIDEO_ENCODER) {
                continue;
            }
            prevwidth = pStreamInfo->getImgSize().w;
            prevheight = pStreamInfo->getImgSize().h;
        }
    }
    if(prevwidth == 0 || prevheight == 0)
        return ;
    rSize.h = prevheight * rSize.w / prevwidth;
    if(maxheight < rSize.h) {
        MY_LOGW("Warning!!,  scaled preview height(%d) is larger than max height(%d)", rSize.h, maxheight);
        rSize.h = maxheight;
    }
    MY_LOGD("evaluate preview size : %dx%d", prevwidth, prevheight);
    MY_LOGD("FD buffer size : %dx%d", rSize.w, rSize.h);
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
setupAppStreamsLocked(
    PipeConfigParams const& rConfigParams
)
{
    mParams->mStreamSet.mvStreamDurations.clear();

    MINT32 iBurstNum = mParams->mCHSvrParams.mDefaultBusrstNum;
    //App:Meta:Control
    {
        sp<IMetaStreamInfo> pStreamInfo;
        if  ( (pStreamInfo = rConfigParams.pMeta_Control) != 0 )
        {
            mParams->mStreamSet.mpAppMeta_Control = pStreamInfo;
            pStreamInfo->setMaxBufNum(10*iBurstNum);
        }
    }

    //App:dynamic
    if( mParams->mPipelineConfigParams.mbUseP1Node )
    {
        //App:Meta:01
        //   pass1 result meta
        sp<IMetaStreamInfo> pStreamInfo =
            new MetaStreamInfo(
                    "App:Meta:DynamicP1",
                    eSTREAMID_META_APP_DYNAMIC_01,
                    eSTREAMTYPE_META_OUT,
                    10*iBurstNum, 1
                    );
        mParams->mStreamSet.mpAppMeta_DynamicP1 = pStreamInfo;
    }

    if( mParams->mPipelineConfigParams.mbUseP2Node )
    {
        //App:Meta:P2
        //   pass2 result meta
        sp<IMetaStreamInfo> pStreamInfo =
            new MetaStreamInfo(
                    "App:Meta:DynamicP2",
                    eSTREAMID_META_APP_DYNAMIC_02,
                    eSTREAMTYPE_META_OUT,
                    10*iBurstNum, 1
                    );
        mParams->mStreamSet.mpAppMeta_DynamicP2 = pStreamInfo;
    }


    if( mParams->mPipelineConfigParams.mbUseFDNode )
    {
        //App:Meta:FD
        //   FD result meta
        sp<IMetaStreamInfo> pStreamInfo =
            new MetaStreamInfo(
                    "App:Meta:FD",
                    eSTREAMID_META_APP_DYNAMIC_FD,
                    eSTREAMTYPE_META_OUT,
                    10, 1
                    );
        mParams->mStreamSet.mpAppMeta_DynamicFD = pStreamInfo;
    }

    if( mParams->mPipelineConfigParams.mbUseJpegNode )
    {
        //App:Meta:Jpeg
        //   Jpeg result meta
        sp<IMetaStreamInfo> pStreamInfo =
            new MetaStreamInfo(
                    "App:Meta:Jpeg",
                    eSTREAMID_META_APP_DYNAMIC_JPEG,
                    eSTREAMTYPE_META_OUT,
                    10, 1
                    );
        mParams->mStreamSet.mpAppMeta_DynamicJpeg = pStreamInfo;
    }


    //App:Image
    {
        sp<IImageStreamInfo> pStreamInfo;
        //
        //App:Image:Raw
        if  ( (pStreamInfo = rConfigParams.pImage_Raw) != 0
                 &&  eImgFmt_RAW16 == pStreamInfo->getImgFormat()
        ) {
            // RAW16 = rConfigParams.pImage_Raw set this stream
            mParams->mStreamSet.mpAppImage_RAW16 = pStreamInfo;
            pStreamInfo->setMaxBufNum(1);
            //
            mParams->mStreamSet.mvStreamDurations.add(
                    pStreamInfo->getStreamId(),
                    rConfigParams.mImage_Raw_Duration.minDuration
                    );
        }
        //
        //App:Image:Jpeg:Stall
        if  ( (pStreamInfo = rConfigParams.pImage_Jpeg_Stall) != 0 ) {
            mParams->mStreamSet.mpAppImage_Jpeg = pStreamInfo;
            pStreamInfo->setMaxBufNum(1);
            //
            mParams->mStreamSet.mvStreamDurations.add(
                    pStreamInfo->getStreamId(),
                    rConfigParams.mImage_Jpeg_Duration.minDuration
                    );
        }
        //App:Image:Yuv:In
        if  ( (pStreamInfo = rConfigParams.pImage_Yuv_In) != 0 ) {
            mParams->mStreamSet.mpAppImage_Yuv_In = pStreamInfo;
            pStreamInfo->setMaxBufNum(2);
            //
            mParams->mStreamSet.mvStreamDurations.add(
                    pStreamInfo->getStreamId(),
                    rConfigParams.mImage_Yuv_In_Duration.minDuration
                    );
        }
        //App:Image:Opaque:In
        if  ( (pStreamInfo = rConfigParams.pImage_Opaque_In) != 0 ) {
            mParams->mStreamSet.mpAppImage_Opaque_In = pStreamInfo;
            pStreamInfo->setMaxBufNum(2);
            //
            mParams->mStreamSet.mvStreamDurations.add(
                    pStreamInfo->getStreamId(),
                    rConfigParams.mImage_Opaque_In_Duration.minDuration
                    );
        }
        //App:Image:Opaque:Out
        if  ( (pStreamInfo = rConfigParams.pImage_Opaque_Out) != 0 ) {
            mParams->mStreamSet.mpAppImage_Opaque_Out = pStreamInfo;
            pStreamInfo->setMaxBufNum(6);
            //
            mParams->mStreamSet.mvStreamDurations.add(
                    pStreamInfo->getStreamId(),
                    rConfigParams.mImage_Opaque_Out_Duration.minDuration
                    );
        }
        //
        //App:Image:Yuv:NotStall

        for (size_t i = 0; i < rConfigParams.vImage_Yuv_NonStall.size(); i++) {
            if  ( (pStreamInfo = rConfigParams.vImage_Yuv_NonStall[i]) != 0 ) {
                mParams->mStreamSet.mvAppYuvImage.add(pStreamInfo->getStreamId(), pStreamInfo);
                if ( mParams->mPipelineConfigParams.mOperation_mode )
                    if ( pStreamInfo->getUsageForConsumer() & GRALLOC_USAGE_HW_VIDEO_ENCODER )
                        pStreamInfo->setMaxBufNum(54);
                    else
                        pStreamInfo->setMaxBufNum(12);
                else{
#if MTKCAM_HAVE_ADV_SETTING
                    if ( (pStreamInfo->getUsageForConsumer() & GRALLOC_USAGE_HW_VIDEO_ENCODER)
                            && mParams->mStreamingPipeParams.mpAdvSetting != NULL){
                        pStreamInfo->setMaxBufNum(8 + mParams->mStreamingPipeParams.mpAdvSetting->eisExtraBufNum);
                    }else{
                        pStreamInfo->setMaxBufNum(8);
                    }
#else
                    pStreamInfo->setMaxBufNum(8);
#endif
                }
                //
                if( i >= rConfigParams.vImage_Yuv_Duration.size() ) {
                    MY_LOGE("not enough yuv duration for streams");
                    continue;
                }
                mParams->mStreamSet.mvStreamDurations.add(
                        pStreamInfo->getStreamId(),
                        rConfigParams.vImage_Yuv_Duration[i].minDuration
                        );
            }
        }
        //
        // dump durations
        String8 durations = String8("durations:");
        for( size_t i = 0; i < mParams->mStreamSet.mvStreamDurations.size(); i++) {
            durations += String8::format("(stream %#" PRIx64 ": %lld) ",
                    mParams->mStreamSet.mvStreamDurations.keyAt(i), (long long int)mParams->mStreamSet.mvStreamDurations.valueAt(i));
        }
        MY_LOGD("%s", durations.string());
    }

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
setupHalStreamsLocked(
    PipeConfigParams const& rConfigParams
)
{
    MINT32 iBurstNum = mParams->mCHSvrParams.mDefaultBusrstNum;
    //Hal:Meta
    //
    if( 1 )
    {
        //Hal:Meta:Control
        sp<IMetaStreamInfo> pStreamInfo =
            new MetaStreamInfo(
                    "Hal:Meta:Control",
                    eSTREAMID_META_PIPE_CONTROL,
                    eSTREAMTYPE_META_IN,
                    10*iBurstNum, 1
                    );
        mParams->mStreamSet.mpHalMeta_Control = pStreamInfo;
    }
    //
    if( mParams->mPipelineConfigParams.mbUseP1Node )
    {
        //Hal:Meta:01
        sp<IMetaStreamInfo> pStreamInfo =
            new MetaStreamInfo(
                    "Hal:Meta:P1:Dynamic",
                    eSTREAMID_META_PIPE_DYNAMIC_01,
                    eSTREAMTYPE_META_INOUT,
                    10*iBurstNum, 1
                    );
        mParams->mStreamSet.mpHalMeta_DynamicP1 = pStreamInfo;
    }
    //
    if( mParams->mPipelineConfigParams.mbUseP2Node )
    {
        //Hal:Meta:01
        sp<IMetaStreamInfo> pStreamInfo =
            new MetaStreamInfo(
                    "Hal:Meta:P2:Dynamic",
                    eSTREAMID_META_PIPE_DYNAMIC_02,
                    eSTREAMTYPE_META_INOUT,
                    10*iBurstNum, 1
                    );
        mParams->mStreamSet.mpHalMeta_DynamicP2 = pStreamInfo;
    }

    // p1node image port: imgo/rrzo/lcso/rsso
    mPass1Resource.setCapacity(4);
    //Hal:Image
    if ( mParams->mPipelineConfigParams.mbUseP1Node &&
            !! mParams->mHwParams.mFullrawSize.size() )
    {
        sp<IImageStreamInfo> pStreamInfo;
        // p1: fullsize
        MSize const& size = mParams->mHwParams.mFullrawSize;
        MINT const format = mParams->mHwParams.mFullrawFormat;
        size_t const stride = mParams->mHwParams.mFullrawStride;
        MUINT const usage = 0;//eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE;
        //
        MBOOL bDone = MFALSE;
        if ( mpOldCtx.get() &&
             OK == mpOldCtx->queryStream(eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00, pStreamInfo) &&
             pStreamInfo.get() )
        {
            if ( pStreamInfo->getImgSize().w == size.w &&
                 pStreamInfo->getImgSize().h == size.h &&
                 pStreamInfo->getImgFormat() == format &&
                 pStreamInfo->getBufPlanes().itemAt(0).rowStrideInBytes == stride &&
                 pStreamInfo->getUsageForAllocator() == usage )
            {
                MY_LOGD_IF( 1, "stream could be reused:%#" PRIx64 , eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00);
                mPass1Resource.updateStreamStatus( eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00,
                                                   pass1Resource::StreamStatus::eStatus_Reuse);
                bDone = MTRUE;
            }
        }
        //
        if ( !bDone )
        {
            pStreamInfo = createRawImageStreamInfo(
                    "Hal:Image:P1:Fullraw",
                    eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00,
                    eSTREAMTYPE_IMAGE_INOUT,
                    (6+2)*iBurstNum, 0,
                    usage, format, size, stride/*, mParams->mHwInfoHelper*/
            );
            //
            if( pStreamInfo == NULL ) {
                return BAD_VALUE;
            }
            mPass1Resource.updateStreamStatus( eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00,
                                               pass1Resource::StreamStatus::eStatus_Inited);
        }
        //
        MY_LOGD_IF( 1, "setup streamid(%#" PRIx64 "):%p",
                    eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00, pStreamInfo.get() );
        mParams->mStreamSet.mpHalImage_P1_Raw = pStreamInfo;
    } else if ( mParams->mPipelineConfigParams.mbUseP1Node &&
                ! mParams->mHwParams.mFullrawSize.size() )
    {
        sp<IImageStreamInfo> pStreamInfo;
        if ( mpOldCtx.get() &&
             OK == mpOldCtx->queryStream(eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00, pStreamInfo) &&
             ! pStreamInfo.get() )
        {
            MY_LOGD_IF( 1, "case of previous and current pipeline have no streamid:%#" PRIx64 ,
                        eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00);
            mPass1Resource.updateStreamStatus( eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00,
                                               pass1Resource::StreamStatus::eStatus_NoNeed);
            mParams->mStreamSet.mpHalImage_P1_Raw = pStreamInfo;
        }
    }

    //if (
    //        mConfigProfile.mbConfigP1 &&
    //        !! mConfigProfile.mResizedSize
    if ( mParams->mPipelineConfigParams.mbUseP1Node &&
            !! mParams->mHwParams.mResizedrawSize.size() )
    {
        sp<IImageStreamInfo> pStreamInfo;
        // p1: resize
        MSize const& size = mParams->mHwParams.mResizedrawSize;
        MINT const format = mParams->mHwParams.mResizedrawFormat;
        size_t const stride = mParams->mHwParams.mResizedrawStride;
        MUINT const usage = 0;
        //
        MBOOL bDone = MFALSE;
        if ( mpOldCtx.get() &&
             OK == mpOldCtx->queryStream(eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00, pStreamInfo) &&
             pStreamInfo.get() )
        {
            if ( pStreamInfo->getImgSize().w == size.w &&
                 pStreamInfo->getImgSize().h == size.h &&
                 pStreamInfo->getImgFormat() == format &&
                 pStreamInfo->getBufPlanes().itemAt(0).rowStrideInBytes == stride &&
                 pStreamInfo->getUsageForAllocator() == usage )
            {
                MY_LOGD_IF( 1, "stream could be reused:%#" PRIx64 , eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00);
                mPass1Resource.updateStreamStatus( eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00,
                                                   pass1Resource::StreamStatus::eStatus_Reuse);
                bDone = MTRUE;
            }
        }
        //
        if ( !bDone )
        {
            pStreamInfo = createRawImageStreamInfo(
                    "Hal:Image:P1:ResizerawI",
                    eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00,
                    eSTREAMTYPE_IMAGE_INOUT,
                    (6+2)*iBurstNum, 0,
                    usage, format, size, stride/*, mParams->mHwInfoHelper*/
            );
            //
            if( pStreamInfo == NULL ) {
                return BAD_VALUE;
            }
            mPass1Resource.updateStreamStatus( eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00,
                                               pass1Resource::StreamStatus::eStatus_Inited);
        }
        //
        MY_LOGD_IF( 1, "setup streamid(%#" PRIx64 "):%p",
                    eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00, pStreamInfo.get() );
        mParams->mStreamSet.mpHalImage_P1_ResizerRaw = pStreamInfo;
    } else if ( mParams->mPipelineConfigParams.mbUseP1Node &&
                ! mParams->mHwParams.mResizedrawSize.size() )
    {
        sp<IImageStreamInfo> pStreamInfo;
        if ( mpOldCtx.get() &&
             OK == mpOldCtx->queryStream(eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00, pStreamInfo) &&
             ! pStreamInfo.get() )
        {
            MY_LOGD_IF( 1, "case of previous and current pipeline have no streamid:%#" PRIx64 ,
                        eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00);
            mPass1Resource.updateStreamStatus( eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00,
                                               pass1Resource::StreamStatus::eStatus_NoNeed);
            mParams->mStreamSet.mpHalImage_P1_ResizerRaw = pStreamInfo;
        }
    }

    //Hal:Image Lcso
    if ( mParams->mPipelineConfigParams.mbUseP1Node && mParams->mPipelineConfigParams.mbHasLcso)
    {
        sp<IImageStreamInfo> pStreamInfo;
        // p1: lcso size
        MUINT const usage = 0;
        NS3Av3::LCSO_Param lcsoParam;
        NS3Av3::IIspMgr* ispMgr = MAKE_IspMgr();
        if (ispMgr)
            ispMgr->queryLCSOParams(lcsoParam);
        else
            MY_LOGE("Query IIspMgr FAILED!");
        //
        MBOOL bDone = MFALSE;
        if ( mpOldCtx.get() &&
             OK == mpOldCtx->queryStream(eSTREAMID_IMAGE_PIPE_RAW_LCSO_00, pStreamInfo) &&
             pStreamInfo.get() )
        {

            MY_LOGD_IF( 1, "stream could be reused:%#" PRIx64 , eSTREAMID_IMAGE_PIPE_RAW_LCSO_00);
            mPass1Resource.updateStreamStatus( eSTREAMID_IMAGE_PIPE_RAW_LCSO_00,
                                                   pass1Resource::StreamStatus::eStatus_Reuse);
            bDone = MTRUE;
        }
        //
        if ( !bDone )
        {
            pStreamInfo = createRawImageStreamInfo(
                "Hal:Image:LCSraw",
                eSTREAMID_IMAGE_PIPE_RAW_LCSO_00,
                eSTREAMTYPE_IMAGE_INOUT,
                (6+2)*iBurstNum, 1,
                usage, lcsoParam.format, lcsoParam.size, lcsoParam.stride/*, mParams->mHwInfoHelper*/
            );
            //
            if( pStreamInfo == NULL ) {
                return BAD_VALUE;
            }
            mPass1Resource.updateStreamStatus( eSTREAMID_IMAGE_PIPE_RAW_LCSO_00,
                                               pass1Resource::StreamStatus::eStatus_Inited);
        }
        //
        MY_LOGD_IF( 1, "setup streamid(%#" PRIx64 "):%p",
                    eSTREAMID_IMAGE_PIPE_RAW_LCSO_00, pStreamInfo.get() );
        mParams->mStreamSet.mpHalImage_P1_Lcso = pStreamInfo;
    } else if ( mParams->mPipelineConfigParams.mbUseP1Node && ! mParams->mPipelineConfigParams.mbHasLcso )
    {
        sp<IImageStreamInfo> pStreamInfo;
        if ( mpOldCtx.get() &&
             OK == mpOldCtx->queryStream(eSTREAMID_IMAGE_PIPE_RAW_LCSO_00, pStreamInfo) &&
             ! pStreamInfo.get() )
        {
            MY_LOGD_IF( 1, "case of previous and current pipeline have no streamid:%#" PRIx64 ,
                        eSTREAMID_IMAGE_PIPE_RAW_LCSO_00);
            mPass1Resource.updateStreamStatus( eSTREAMID_IMAGE_PIPE_RAW_LCSO_00,
                                               pass1Resource::StreamStatus::eStatus_NoNeed);
            mParams->mStreamSet.mpHalImage_P1_Lcso = pStreamInfo;
        }
    }

    //Hal:Image Rsso
    if ( mParams->mPipelineConfigParams.mbUseP1Node && mParams->mStreamingPipeParams.mbHasRsso)
    {
        sp<IImageStreamInfo> pStreamInfo;
        // p1: rsso size
        MUINT const usage = 0;
        NS3Av3::LCSO_Param lcsoParam;
        NS3Av3::IIspMgr* ispMgr = MAKE_IspMgr();
        MBOOL bDone = MFALSE;
        if ( mpOldCtx.get() &&
             OK == mpOldCtx->queryStream(eSTREAMID_IMAGE_PIPE_RAW_RSSO_00, pStreamInfo) &&
             pStreamInfo.get() )
        {

            MY_LOGD_IF( 1, "stream could be reused:%#" PRIx64 , eSTREAMID_IMAGE_PIPE_RAW_RSSO_00);
            mPass1Resource.updateStreamStatus(eSTREAMID_IMAGE_PIPE_RAW_RSSO_00,
                                              pass1Resource::StreamStatus::eStatus_Reuse);
        }
        else
        {
            MUINT usage = eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE;
            MINT format = eImgFmt_STA_BYTE;
            MSize size(288, 512);
            size_t stride = size.w;
            pStreamInfo = createRawImageStreamInfo(
                "Hal:Image:RSSO",
                eSTREAMID_IMAGE_PIPE_RAW_RSSO_00,
                eSTREAMTYPE_IMAGE_INOUT,
                (6+2)*iBurstNum, 1,
                usage, format, size, stride/*, mParams->mHwInfoHelper*/
            );
            //
            if( pStreamInfo == NULL ) {
                return BAD_VALUE;
            }
            mPass1Resource.updateStreamStatus( eSTREAMID_IMAGE_PIPE_RAW_RSSO_00,
                                               pass1Resource::StreamStatus::eStatus_Inited);
        }
        //
        MY_LOGD_IF( 1, "setup streamid(%#" PRIx64 "):%p",
                    eSTREAMID_IMAGE_PIPE_RAW_RSSO_00, pStreamInfo.get() );
        mParams->mStreamSet.mpHalImage_P1_Rsso = pStreamInfo;
    }
    else if ( mParams->mPipelineConfigParams.mbUseP1Node && ! mParams->mStreamingPipeParams.mbHasRsso )
    {
        sp<IImageStreamInfo> pStreamInfo;
        if ( mpOldCtx.get() &&
             OK == mpOldCtx->queryStream(eSTREAMID_IMAGE_PIPE_RAW_RSSO_00, pStreamInfo) &&
             ! pStreamInfo.get() )
        {
            MY_LOGD_IF( 1, "case of previous and current pipeline have no streamid:%#" PRIx64 ,
                        eSTREAMID_IMAGE_PIPE_RAW_RSSO_00);
            mPass1Resource.updateStreamStatus( eSTREAMID_IMAGE_PIPE_RAW_RSSO_00,
                                               pass1Resource::StreamStatus::eStatus_NoNeed);
            mParams->mStreamSet.mpHalImage_P1_Rsso = pStreamInfo;
        }
    }

    //Hal:Image:FD
    if ( mParams->mPipelineConfigParams.mbUseFDNode )
    {
        //MSize const size(640, 480); //FIXME: hard-code here?
        MSize size(640, 480);
        // evaluate preview size
        evaluatePreviewSize(rConfigParams, size);

        // mPrevFDEn = MFALSE;

        MY_LOGD("evaluate FD buffer size : %dx%d", size.w, size.h);

        MINT const format = eImgFmt_YUY2;//eImgFmt_YV12;
        MUINT const usage = 0;

        sp<ImageStreamInfo>
            pStreamInfo = createImageStreamInfo(
                "Hal:Image:FD",
                eSTREAMID_IMAGE_FD,
                eSTREAMTYPE_IMAGE_INOUT,
                5, 1,
                usage, format, size
            );
        if( pStreamInfo == NULL ) {
            return BAD_VALUE;
        }
        //
        mParams->mStreamSet.mpHalImage_FD_YUV = pStreamInfo;
    }

    //Hal:Image:YUY2 for jpeg & thumbnail
    if ( mParams->mPipelineConfigParams.mbUseJpegNode )
    {
        //Hal:Image:YUY2 for jpeg
        {
            MSize const& size = rConfigParams.pImage_Jpeg_Stall->getImgSize();
            MINT const format = eImgFmt_YUY2;
            MUINT const usage = 0;
            sp<ImageStreamInfo>
                pStreamInfo = createImageStreamInfo(
                    "Hal:Image:YuvJpeg",
                    eSTREAMID_IMAGE_PIPE_YUV_JPEG_00,
                    eSTREAMTYPE_IMAGE_INOUT,
                    1, 0,
                    usage, format, size
                );
            if( pStreamInfo == NULL ) {
                return BAD_VALUE;
            }
            //
            mParams->mStreamSet.mpHalImage_Jpeg_YUV = pStreamInfo;
        }
        //
        //Hal:Image:YUY2 for thumbnail
        {
            MSize const size(-1L, -1L); //unknown now
            MINT const format = eImgFmt_YUY2;
            MUINT const usage = 0;
            sp<ImageStreamInfo>
                pStreamInfo = createImageStreamInfo(
                    "Hal:Image:YuvThumbnail",
                    eSTREAMID_IMAGE_PIPE_YUV_THUMBNAIL_00,
                    eSTREAMTYPE_IMAGE_INOUT,
                    1, 0,
                    usage, format, size
                );
            if( pStreamInfo == NULL ) {
                return BAD_VALUE;
            }
            //
            mParams->mStreamSet.mpHalImage_Thumbnail_YUV = pStreamInfo;
            MY_LOGD("streamId:%#" PRIx64 " %s %p", pStreamInfo->getStreamId(), pStreamInfo->getStreamName(), pStreamInfo.get());
        }
    }
    return OK;

}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
configContextLocked_Streams(sp<PipelineContext> pContext)
{
    CAM_TRACE_CALL();
#define BuildStream(_type_, _IStreamInfo_)                                     \
    do {                                                                       \
        if( _IStreamInfo_.get() ) {                                            \
            MERROR err;                                                        \
            if ( OK != (err = StreamBuilder(_type_, _IStreamInfo_)             \
                    .build(pContext)) )                                        \
            {                                                                  \
                MY_LOGE("StreamBuilder fail stream %#" PRIx64 " of type %d",  \
                    _IStreamInfo_->getStreamId(), _type_);                     \
                return err;                                                    \
            }                                                                  \
        }                                                                      \
    } while(0)
    BuildStream(eStreamType_META_HAL, mParams->mStreamSet.mpHalMeta_Control);
    BuildStream(eStreamType_META_HAL, mParams->mStreamSet.mpHalMeta_DynamicP1);
    BuildStream(eStreamType_META_HAL, mParams->mStreamSet.mpHalMeta_DynamicP2);
    //
    BuildStream(eStreamType_META_APP, mParams->mStreamSet.mpAppMeta_DynamicP1);
    BuildStream(eStreamType_META_APP, mParams->mStreamSet.mpAppMeta_DynamicP2);
    BuildStream(eStreamType_META_APP, mParams->mStreamSet.mpAppMeta_DynamicFD);
    BuildStream(eStreamType_META_APP, mParams->mStreamSet.mpAppMeta_DynamicJpeg);
    BuildStream(eStreamType_META_APP, mParams->mStreamSet.mpAppMeta_Control);
    //
    BuildStream(eStreamType_IMG_HAL_POOL   , mParams->mStreamSet.mpHalImage_FD_YUV);
    //
#if 1
    if ( mPass1Resource.checkStreamsReusable() )
    {
        if ( mParams->mStreamSet.mpHalImage_P1_Raw.get() )
            CHECK_ERROR( pContext->reuseStream(mParams->mStreamSet.mpHalImage_P1_Raw) );
        if ( mParams->mStreamSet.mpHalImage_P1_ResizerRaw.get() )
            CHECK_ERROR( pContext->reuseStream(mParams->mStreamSet.mpHalImage_P1_ResizerRaw) );
        if ( mParams->mStreamSet.mpHalImage_P1_Lcso.get() )
            CHECK_ERROR( pContext->reuseStream(mParams->mStreamSet.mpHalImage_P1_Lcso) );
        if ( mParams->mStreamSet.mpHalImage_P1_Rsso.get() )
            CHECK_ERROR( pContext->reuseStream(mParams->mStreamSet.mpHalImage_P1_Rsso) );
        //
        MY_LOGD_IF( 1, "Reuse:  p1 full raw(%p); resized raw(%p)",
                    mParams->mStreamSet.mpHalImage_P1_Raw.get(), mParams->mStreamSet.mpHalImage_P1_ResizerRaw.get());
        mPass1Resource.setReuseFlag(MTRUE);
    }
    else
    {
        BuildStream(eStreamType_IMG_HAL_POOL   , mParams->mStreamSet.mpHalImage_P1_Raw);
        BuildStream(eStreamType_IMG_HAL_POOL   , mParams->mStreamSet.mpHalImage_P1_ResizerRaw);
        BuildStream(eStreamType_IMG_HAL_POOL   , mParams->mStreamSet.mpHalImage_P1_Lcso);
        BuildStream(eStreamType_IMG_HAL_POOL   , mParams->mStreamSet.mpHalImage_P1_Rsso);
        MY_LOGD_IF( 1, "New: p1 full raw(%p); resized raw(%p)",
                    mParams->mStreamSet.mpHalImage_P1_Raw.get(), mParams->mStreamSet.mpHalImage_P1_ResizerRaw.get());
        mPass1Resource.setReuseFlag(MFALSE);
    }
#else
        BuildStream(eStreamType_IMG_HAL_POOL   , mpHalImage_P1_Raw);
        BuildStream(eStreamType_IMG_HAL_POOL   , mpHalImage_P1_ResizerRaw);
        BuildStream(eStreamType_IMG_HAL_POOL   , mpHalImage_P1_Lcso);
        BuildStream(eStreamType_IMG_HAL_POOL   , mpHalImage_P1_Rsso);
#endif
    if ( mParams->mStreamSet.mpAppImage_Yuv_In.get() )
        BuildStream(eStreamType_IMG_APP, mParams->mStreamSet.mpAppImage_Yuv_In);
    if ( mParams->mStreamSet.mpAppImage_Opaque_In.get() )
        BuildStream(eStreamType_IMG_APP, mParams->mStreamSet.mpAppImage_Opaque_In);
    if ( mParams->mStreamSet.mpAppImage_Opaque_Out.get() )
        BuildStream(eStreamType_IMG_APP, mParams->mStreamSet.mpAppImage_Opaque_Out);
    //
    if ( ! mCommonInfo->mJpegRotationEnable )
        BuildStream(eStreamType_IMG_HAL_POOL   , mParams->mStreamSet.mpHalImage_Jpeg_YUV);
    else
        BuildStream(eStreamType_IMG_HAL_RUNTIME   , mParams->mStreamSet.mpHalImage_Jpeg_YUV);
    BuildStream(eStreamType_IMG_HAL_RUNTIME, mParams->mStreamSet.mpHalImage_Thumbnail_YUV);
    //
    for (size_t i = 0; i < mParams->mStreamSet.mvAppYuvImage.size(); i++ )
    {
        BuildStream(eStreamType_IMG_APP, mParams->mStreamSet.mvAppYuvImage[i]);
    }
    BuildStream(eStreamType_IMG_APP, mParams->mStreamSet.mpAppImage_Jpeg);
    BuildStream(eStreamType_IMG_APP, mParams->mStreamSet.mpAppImage_RAW16);
#undef BuildStream
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
configContextLocked_Nodes(sp<PipelineContext> pContext, MBOOL isReConfig)
{
    CAM_TRACE_CALL();
    if( mParams->mPipelineConfigParams.mbUseP1Node )
        CHECK_ERROR( configContextLocked_P1Node(pContext, isReConfig) );
    if( mParams->mPipelineConfigParams.mbUseP2Node )
        CHECK_ERROR( configContextLocked_P2Node(pContext, isReConfig) );
    if( mParams->mPipelineConfigParams.mbUseP2TSNode )
        CHECK_ERROR( configContextLocked_P2TSNode(pContext, isReConfig) );
    if( mParams->mPipelineConfigParams.mbUseFDNode )
        CHECK_ERROR( configContextLocked_FdNode(pContext, isReConfig) );
    if( mParams->mPipelineConfigParams.mbUseJpegNode )
        CHECK_ERROR( configContextLocked_JpegNode(pContext, isReConfig) );
    if( mParams->mPipelineConfigParams.mbUseRaw16Node )
        CHECK_ERROR( configContextLocked_Raw16Node(pContext, isReConfig) );
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
configContextLocked_Pipeline(sp<PipelineContext> pContext)
{
    CAM_TRACE_CALL();
    NodeSet roots;
    {
        roots.add(eNODEID_P1Node);
    }
    NodeEdgeSet edges;
    {
        // in:p1 -> out:
        if( mParams->mPipelineConfigParams.mbUseP1Node )
        {
            if ( mParams->mPipelineConfigParams.mbUseP2Node )
                edges.addEdge(eNODEID_P1Node, eNODEID_P2Node);
            if ( mParams->mPipelineConfigParams.mbUseP2TSNode )
                edges.addEdge(eNODEID_P1Node, eNODEID_P2Node_VSS);
            if ( mParams->mPipelineConfigParams.mbUseRaw16Node )
                edges.addEdge(eNODEID_P1Node, eNODEID_RAW16Out);
        }
        // in:p2 -> out:
        if ( mParams->mPipelineConfigParams.mbUseP2Node )
        {
            if ( mParams->mPipelineConfigParams.mbUseFDNode )
                edges.addEdge(eNODEID_P2Node, eNODEID_FDNode);
            if ( mParams->mPipelineConfigParams.mbUseJpegNode )
                edges.addEdge(eNODEID_P2Node, eNODEID_JpegNode);
        }
        // in:p2ts -> out:
        if ( mParams->mPipelineConfigParams.mbUseP2TSNode )
        {
            if ( mParams->mPipelineConfigParams.mbUseJpegNode )
                edges.addEdge(eNODEID_P2Node_VSS, eNODEID_JpegNode);
        }
    }
    //
    CHECK_ERROR(
            PipelineBuilder()
            .setRootNode(roots)
            .setNodeEdges(edges)
            .build(pContext)
            );
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
#define add_stream_to_set(_set_, _IStreamInfo_)                                \
    do {                                                                       \
        if( _IStreamInfo_.get() ) { _set_.add(_IStreamInfo_->getStreamId()); } \
    } while(0)
//
#define setImageUsage( _IStreamInfo_, _usg_ )                                   \
    do {                                                                        \
        if( _IStreamInfo_.get() ) {                                             \
            builder.setImageStreamUsage( _IStreamInfo_->getStreamId(), _usg_ ); \
        }                                                                       \
    } while(0)
/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
configContextLocked_P1Node(sp<PipelineContext> pContext, MBOOL isReConfig)
{
    typedef P1Node                  NodeT;
    typedef NodeActor< NodeT >      NodeActorT;
    //
    NodeId_T const nodeId = eNODEID_P1Node;
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = mCommonInfo->mOpenId;
        initParam.nodeId = nodeId;
        initParam.nodeName = "P1Node";
    }

    NodeT::ConfigParams cfgParam;
    {
        NodeT::SensorParams sensorParam(
        /*mode     : */mParams->mHwParams.mSensorMode,
        /*size     : */mParams->mHwParams.mSensorSize,
        /*fps      : */mParams->mHwParams.mSensorFps,
        /*pixelMode: */mParams->mHwParams.mPixelMode,
        /*vhdrMode : */mParams->mStreamingPipeParams.mVhdrMode
        );
        //
        cfgParam.pInAppMeta        = mParams->mStreamSet.mpAppMeta_Control;
        cfgParam.pInHalMeta        = mParams->mStreamSet.mpHalMeta_Control;
        cfgParam.pOutAppMeta       = mParams->mStreamSet.mpAppMeta_DynamicP1;
        cfgParam.pOutHalMeta       = mParams->mStreamSet.mpHalMeta_DynamicP1;
        cfgParam.pOutImage_resizer = mParams->mStreamSet.mpHalImage_P1_ResizerRaw;
        cfgParam.pOutImage_lcso    = mParams->mStreamSet.mpHalImage_P1_Lcso;
        cfgParam.pOutImage_rsso    = mParams->mStreamSet.mpHalImage_P1_Rsso;
        if( mParams->mStreamSet.mpHalImage_P1_Raw.get() )
            cfgParam.pvOutImage_full.push_back(mParams->mStreamSet.mpHalImage_P1_Raw);
        cfgParam.enableLCS          = mParams->mPipelineConfigParams.mbHasLcso;
        cfgParam.enableRSS          = mParams->mStreamingPipeParams.mbHasRsso;
        cfgParam.sensorParams        = sensorParam;
        cfgParam.pStreamPool_resizer = NULL;
        cfgParam.pStreamPool_full    = NULL;
        // cfgParam.pStreamPool_resizer = mpHalImage_P1_ResizerRaw.get() ?
        //    pContext->queryImageStreamPool(eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00) : NULL;
        // cfgParam.pStreamPool_full = mpHalImage_P1_Raw.get() ?
        //    pContext->queryImageStreamPool(eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00) : NULL;
        if( mParams->mStreamSet.mpAppImage_Yuv_In.get() )
            cfgParam.pInImage_yuv = mParams->mStreamSet.mpAppImage_Yuv_In;
        if( mParams->mStreamSet.mpAppImage_Opaque_In.get() )
            cfgParam.pInImage_opaque = mParams->mStreamSet.mpAppImage_Opaque_In;
        if( mParams->mStreamSet.mpAppImage_Opaque_Out.get() )
            cfgParam.pOutImage_opaque = mParams->mStreamSet.mpAppImage_Opaque_Out;
        cfgParam.sensorParams      = sensorParam;
        cfgParam.pStreamPool_resizer = mParams->mStreamSet.mpHalImage_P1_ResizerRaw.get() ?
            pContext->queryImageStreamPool(eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00) : NULL;
        cfgParam.pStreamPool_lcso = mParams->mStreamSet.mpHalImage_P1_Lcso.get() ?
            pContext->queryImageStreamPool(eSTREAMID_IMAGE_PIPE_RAW_LCSO_00) : NULL;
        cfgParam.pStreamPool_rsso = mParams->mStreamSet.mpHalImage_P1_Rsso.get() ?
            pContext->queryImageStreamPool(eSTREAMID_IMAGE_PIPE_RAW_RSSO_00) : NULL;
        cfgParam.pStreamPool_full = mParams->mStreamSet.mpHalImage_P1_Raw.get() ?
            pContext->queryImageStreamPool(eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00) : NULL;
        MBOOL needLMV = (mParams->mPipelineConfigParams.mbHasRecording && !mParams->mPipelineConfigParams.mOperation_mode);
#if MTKCAM_HAVE_ADV_SETTING
        needLMV = needLMV || (mParams->mStreamingPipeParams.mpAdvSetting != NULL && mParams->mStreamingPipeParams.mpAdvSetting->needLMV);
#endif
        cfgParam.enableEIS = needLMV ? mpDeviceHelper->isFirstUsingDevice() : MFALSE;
        cfgParam.packedEisInfo = EisInfo::packEISInfo(mParams->mStreamingPipeParams.mEisInfo);
        // cfgParam.eisMode = mParams->mEISMode;
        //
        if ( mParams->mPipelineConfigParams.mb4KRecording )
            cfgParam.receiveMode = NodeT::REV_MODE::REV_MODE_CONSERVATIVE;
        //
        if ( mParams->mPipelineConfigParams.mOperation_mode == IPipelineModelMgr::OperationMode::C_HIGH_SPEED_VIDEO_MODE )
        {
            MINT32 batch = mParams->mCHSvrParams.mAeTargetFpsMin/30;
            cfgParam.burstNum     = (mParams->mCHSvrParams.mDefaultBusrstNum==batch)?
                                    mParams->mCHSvrParams.mDefaultBusrstNum : batch;
        }
        //
        if ( mParams->mHwInfoHelper.getDualPDAFSupported(mParams->mHwParams.mSensorMode) ) {
            cfgParam.enableDualPD = MTRUE;
            if ( ! mpCamMgr->isMultiDevice() )
                cfgParam.disableFrontalBinning = MTRUE;
            MY_LOGD("PDAF supported for sensor mode:%d - enableDualPD:%d disableFrontalBinning:%d",
                    mParams->mHwParams.mSensorMode, cfgParam.enableDualPD, cfgParam.disableFrontalBinning);
        }
    }
    //
    sp<NodeActorT> pNode;

    if (isReConfig) {
        mCommonInfo->mpPipelineContext->queryNodeActor(nodeId, pNode);
        pNode->getNodeImpl()->config(cfgParam);
    }else{
        sp<NodeActorT> pOldNode;
        MBOOL bHasOldNode = (mpOldCtx.get() && OK == mpOldCtx->queryNodeActor(nodeId, pOldNode));
        if ( MTRUE == mPass1Resource.getReuseFlag() && bHasOldNode )
        {
            NodeT::InitParams oldInitParam;
            pOldNode->getInitParam(oldInitParam);
            NodeT::ConfigParams oldCfgParam;
            pOldNode->getConfigParam(oldCfgParam);
            //
            if ( compareParamsLocked_P1Node(initParam, oldInitParam, cfgParam, oldCfgParam) )
            {
                MERROR err = pContext->reuseNode(nodeId);
                MY_LOGD_IF( 1, "check p1 state Old[%d] New[%d]",
                            mpOldCtx->queryINodeActor(nodeId)->getStatus(),
                            pContext->queryINodeActor(nodeId)->getStatus() );
                return err;
            }
        }
        if(bHasOldNode)
            pOldNode->getNodeImpl()->uninit(); // must uninit old P1 before call new P1 config
        //
        pNode = new NodeActorT( NodeT::createInstance() );

    }
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    StreamSet inStreamSet;
    StreamSet outStreamSet;
    //
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpAppMeta_Control);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalMeta_Control);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpAppImage_Yuv_In);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpAppImage_Opaque_In);
    //
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpHalImage_P1_Raw);
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpHalImage_P1_ResizerRaw);
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpHalImage_P1_Lcso);
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpHalImage_P1_Rsso);
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpAppMeta_DynamicP1);
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpHalMeta_DynamicP1);
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpAppImage_Opaque_Out);
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    setImageUsage(mParams->mStreamSet.mpAppImage_Yuv_In        , eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(mParams->mStreamSet.mpAppImage_Opaque_In     , eBUFFER_USAGE_SW_READ_OFTEN);
    //
    setImageUsage(mParams->mStreamSet.mpHalImage_P1_Raw        , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    setImageUsage(mParams->mStreamSet.mpHalImage_P1_ResizerRaw , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    setImageUsage(mParams->mStreamSet.mpHalImage_P1_Lcso       , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    setImageUsage(mParams->mStreamSet.mpHalImage_P1_Rsso       , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    setImageUsage(mParams->mStreamSet.mpAppImage_Opaque_Out    , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    //
    MERROR err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;
}

/******************************************************************************
 *
 ******************************************************************************/
template <class INITPARAM_T, class CONFIGPARAM_T>
MERROR
PipelineDefaultImp::ConfigHandler::
compareParamsLocked_P1Node(
    INITPARAM_T const& initParam1,  INITPARAM_T const& initParam2,
    CONFIGPARAM_T const& cfgParam1, CONFIGPARAM_T const& cfgParam2
) const
{
    FUNC_START;
    if ( initParam1.openId != initParam2.openId ||
         initParam1.nodeId != initParam2.nodeId ||
         strcmp(initParam1.nodeName, initParam2.nodeName) )
        return MFALSE;
    //
    if ( cfgParam1.sensorParams.mode         != cfgParam2.sensorParams.mode ||
         cfgParam1.sensorParams.size         != cfgParam2.sensorParams.size ||
         cfgParam1.sensorParams.fps          != cfgParam2.sensorParams.fps ||
         cfgParam1.sensorParams.pixelMode    != cfgParam2.sensorParams.pixelMode ||
         cfgParam1.sensorParams.vhdrMode    != cfgParam2.sensorParams.vhdrMode )
        return MFALSE;
    //
    if ( ! cfgParam1.pInAppMeta.get()  || ! cfgParam2.pInAppMeta.get() ||
         ! cfgParam1.pOutAppMeta.get() || ! cfgParam2.pOutAppMeta.get() ||
         ! cfgParam1.pOutHalMeta.get() || ! cfgParam2.pOutHalMeta.get() ||
         cfgParam1.pInAppMeta->getStreamId()  != cfgParam2.pInAppMeta->getStreamId() ||
         cfgParam1.pOutAppMeta->getStreamId() != cfgParam2.pOutAppMeta->getStreamId() ||
         cfgParam1.pOutHalMeta->getStreamId() != cfgParam2.pOutHalMeta->getStreamId() )
        return MFALSE;
    //
    if ( ! cfgParam1.pOutImage_resizer.get() || ! cfgParam2.pOutImage_resizer.get() ||
        cfgParam1.pOutImage_resizer->getStreamId() != cfgParam2.pOutImage_resizer->getStreamId() )
        return MFALSE;
    //
    if ( cfgParam1.pOutImage_lcso.get() != cfgParam2.pOutImage_lcso.get()
        || cfgParam1.enableLCS != cfgParam2.enableLCS){
        return MFALSE;
    }
    //
    if ( cfgParam1.pOutImage_rsso.get() != cfgParam2.pOutImage_rsso.get()
        || cfgParam1.enableRSS != cfgParam2.enableRSS){
        return MFALSE;
    }
    //
    if ( cfgParam1.pvOutImage_full.size() != cfgParam2.pvOutImage_full.size() )
        return MFALSE;
    //
    for ( size_t i=0; i<cfgParam1.pvOutImage_full.size(); i++ ) {
        MBOOL bMatch = MFALSE;
        for ( size_t j=0; j<cfgParam2.pvOutImage_full.size(); j++ ) {
            if ( cfgParam1.pvOutImage_full.itemAt(i)->getStreamId() == cfgParam2.pvOutImage_full.itemAt(i)->getStreamId() )
            {
                bMatch = MTRUE;
                break;
            }
        }
        if ( !bMatch )
            return MFALSE;
    }
    //
    FUNC_END;
    return MTRUE;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
configContextLocked_P2Node(sp<PipelineContext> pContext, MBOOL isReConfig)
{
    typedef P2Node                  NodeT;
    typedef NodeActor< NodeT >      NodeActorT;
    //
    NodeId_T const nodeId = eNODEID_P2Node;
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = mCommonInfo->mOpenId;
        initParam.nodeId = nodeId;
        initParam.nodeName = "P2Node";
    }
    NodeT::ConfigParams cfgParam;
    {
        cfgParam.pInAppMeta    = mParams->mStreamSet.mpAppMeta_Control;
        cfgParam.pInAppRetMeta = mParams->mStreamSet.mpAppMeta_DynamicP1;
        cfgParam.pInHalMeta    = mParams->mStreamSet.mpHalMeta_DynamicP1;
        cfgParam.pOutAppMeta   = mParams->mStreamSet.mpAppMeta_DynamicP2;
        cfgParam.pOutHalMeta   = mParams->mStreamSet.mpHalMeta_DynamicP2;
        //
        if( mParams->mStreamSet.mpHalImage_P1_Raw.get() )
            cfgParam.pvInFullRaw.push_back(mParams->mStreamSet.mpHalImage_P1_Raw);
        //
        cfgParam.pInResizedRaw = mParams->mStreamSet.mpHalImage_P1_ResizerRaw;
        //
        cfgParam.pInLcsoRaw = mParams->mStreamSet.mpHalImage_P1_Lcso;
        //
        cfgParam.pInRssoRaw = mParams->mStreamSet.mpHalImage_P1_Rsso;
        //
        if( mParams->mStreamSet.mpAppImage_Yuv_In.get() )
            cfgParam.pInYuvImage = mParams->mStreamSet.mpAppImage_Yuv_In;
        //
        if( mParams->mStreamSet.mpAppImage_Opaque_In.get() )
            cfgParam.pvInOpaque.push_back(mParams->mStreamSet.mpAppImage_Opaque_In);
        //
        if( mParams->mStreamSet.mpAppImage_Opaque_Out.get() )
            cfgParam.pvInOpaque.push_back(mParams->mStreamSet.mpAppImage_Opaque_Out);
        //
        for (size_t i = 0; i < mParams->mStreamSet.mvAppYuvImage.size(); i++)
            cfgParam.vOutImage.push_back(mParams->mStreamSet.mvAppYuvImage[i]);
        //
        if( mParams->mStreamSet.mpHalImage_Jpeg_YUV.get() )
            cfgParam.vOutImage.push_back(mParams->mStreamSet.mpHalImage_Jpeg_YUV);
        if( mParams->mStreamSet.mpHalImage_Thumbnail_YUV.get() )
            cfgParam.vOutImage.push_back(mParams->mStreamSet.mpHalImage_Thumbnail_YUV);
        //
        cfgParam.pOutFDImage = mParams->mStreamSet.mpHalImage_FD_YUV;
        //
        if ( mParams->mPipelineConfigParams.mOperation_mode == IPipelineModelMgr::OperationMode::C_HIGH_SPEED_VIDEO_MODE )
        {
            MINT32 batch = mParams->mCHSvrParams.mAeTargetFpsMin/30;
            cfgParam.burstNum     = (mParams->mCHSvrParams.mDefaultBusrstNum==batch)?
                                    mParams->mCHSvrParams.mDefaultBusrstNum : batch;
        }
#if MTKCAM_HAVE_IVENDOR_SUPPORT
        mVendorHandler->getVendorCfg(cfgParam.pVendor, cfgParam.uUserId);
        MY_LOGD("vendor instance : %p", cfgParam.pVendor.get());
#endif
    }
    P2Common::UsageHint p2Usage = prepareP2Usage(P2Node::PASS2_STREAM);
    cfgParam.mUsageHint = p2Usage;

    //
    sp<NodeActorT> pNode;
    if (isReConfig) {
        mCommonInfo->mpPipelineContext->queryNodeActor(nodeId, pNode);
        pNode->getNodeImpl()->config(cfgParam);
    } else {
        pNode = new NodeActorT( NodeT::createInstance(P2Node::PASS2_STREAM, p2Usage) );
    }
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    StreamSet inStreamSet;
    StreamSet outStreamSet;
    //
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpAppMeta_Control);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpAppMeta_DynamicP1);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalMeta_DynamicP1);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalImage_P1_Raw);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalImage_P1_ResizerRaw);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalImage_P1_Lcso);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalImage_P1_Rsso);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpAppImage_Yuv_In);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpAppImage_Opaque_In);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpAppImage_Opaque_Out);
    //
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpAppMeta_DynamicP2);
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpHalMeta_DynamicP2);
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpHalImage_Jpeg_YUV);
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpHalImage_Thumbnail_YUV);
    //
    for (size_t i = 0; i < mParams->mStreamSet.mvAppYuvImage.size(); i++)
        add_stream_to_set(outStreamSet, mParams->mStreamSet.mvAppYuvImage[i]);
    //
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpHalImage_FD_YUV);
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    setImageUsage(mParams->mStreamSet.mpAppImage_Yuv_In        , eBUFFER_USAGE_HW_CAMERA_READ | eBUFFER_USAGE_SW_READ_OFTEN);
    setImageUsage(mParams->mStreamSet.mpAppImage_Opaque_In     , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(mParams->mStreamSet.mpAppImage_Opaque_Out    , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(mParams->mStreamSet.mpHalImage_P1_Raw        , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(mParams->mStreamSet.mpHalImage_P1_ResizerRaw , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(mParams->mStreamSet.mpHalImage_P1_Lcso       , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(mParams->mStreamSet.mpHalImage_P1_Rsso       , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    //
    for (size_t i = 0; i < mParams->mStreamSet.mvAppYuvImage.size(); i++)
        setImageUsage(mParams->mStreamSet.mvAppYuvImage[i], eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    //
    setImageUsage(mParams->mStreamSet.mpHalImage_Jpeg_YUV      , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    setImageUsage(mParams->mStreamSet.mpHalImage_Thumbnail_YUV , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    setImageUsage(mParams->mStreamSet.mpHalImage_FD_YUV        , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    //
    MERROR err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
configContextLocked_P2TSNode(sp<PipelineContext> pContext, MBOOL isReConfig)
{
    typedef P2Node                  NodeT;
    typedef NodeActor< NodeT >      NodeActorT;
    //
    NodeId_T const nodeId = eNODEID_P2Node_VSS;
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = mCommonInfo->mOpenId;
        initParam.nodeId = nodeId;
        initParam.nodeName = "P2Node_VSS";
    }
    NodeT::ConfigParams cfgParam;
    {
        cfgParam.pInAppMeta    = mParams->mStreamSet.mpAppMeta_Control;
        cfgParam.pInHalMeta    = mParams->mStreamSet.mpHalMeta_DynamicP1;
        cfgParam.pOutAppMeta   = NULL;
        cfgParam.pOutHalMeta   = mParams->mStreamSet.mpHalMeta_DynamicP2;
        //
        if( mParams->mStreamSet.mpHalImage_P1_Raw.get() ) {
            cfgParam.pvInFullRaw.push_back(mParams->mStreamSet.mpHalImage_P1_Raw);
            cfgParam.pInResizedRaw = NULL;
        }
        else
            cfgParam.pInResizedRaw = mParams->mStreamSet.mpHalImage_P1_ResizerRaw;
        //
        cfgParam.pInLcsoRaw = mParams->mStreamSet.mpHalImage_P1_Lcso;
        cfgParam.pInRssoRaw = mParams->mStreamSet.mpHalImage_P1_Rsso;
        //
        if( mParams->mStreamSet.mpHalImage_Jpeg_YUV.get() )
            cfgParam.vOutImage.push_back(mParams->mStreamSet.mpHalImage_Jpeg_YUV);
        if( mParams->mStreamSet.mpHalImage_Thumbnail_YUV.get() )
            cfgParam.vOutImage.push_back(mParams->mStreamSet.mpHalImage_Thumbnail_YUV);
#if MTKCAM_HAVE_IVENDOR_SUPPORT
        mVendorHandler->getVendorCfg(cfgParam.pVendor, cfgParam.uUserId);
        MY_LOGD("vendor instance : %p", cfgParam.pVendor.get());
#endif
    }
    P2Common::UsageHint p2Usage = prepareP2Usage(P2Node::PASS2_TIMESHARING);
    cfgParam.mUsageHint = p2Usage;

    //
    sp<NodeActorT> pNode;
    if (isReConfig) {
        mCommonInfo->mpPipelineContext->queryNodeActor(nodeId, pNode);
        pNode->getNodeImpl()->config(cfgParam);
    } else {
        pNode = new NodeActorT( NodeT::createInstance(P2Node::PASS2_TIMESHARING, p2Usage) );
    }
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    StreamSet inStreamSet;
    StreamSet outStreamSet;
    //
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpAppMeta_Control);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalMeta_DynamicP1);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalImage_P1_Raw);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalImage_P1_ResizerRaw);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalImage_P1_Lcso);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalImage_P1_Rsso);
    //
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpHalMeta_DynamicP2);
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpHalImage_Jpeg_YUV);
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpHalImage_Thumbnail_YUV);
    //
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    setImageUsage(mParams->mStreamSet.mpHalImage_P1_Raw        , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(mParams->mStreamSet.mpHalImage_P1_ResizerRaw , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(mParams->mStreamSet.mpHalImage_P1_Lcso       , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    //
    setImageUsage(mParams->mStreamSet.mpHalImage_Jpeg_YUV      , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    setImageUsage(mParams->mStreamSet.mpHalImage_Thumbnail_YUV , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    //
    MERROR err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
configContextLocked_FdNode(sp<PipelineContext> pContext, MBOOL isReConfig)
{
    typedef FdNode                  NodeT;
    typedef NodeActor< NodeT >      NodeActorT;
    //
    NodeId_T const nodeId = eNODEID_FDNode;
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = mCommonInfo->mOpenId;
        initParam.nodeId = nodeId;
        initParam.nodeName = "FDNode";
    }
    NodeT::ConfigParams cfgParam;
    {
        cfgParam.pInAppMeta    = mParams->mStreamSet.mpAppMeta_Control;
        cfgParam.pOutAppMeta   = mParams->mStreamSet.mpAppMeta_DynamicFD;
        cfgParam.vInImage      = mParams->mStreamSet.mpHalImage_FD_YUV;
    }
    //
    sp<NodeActorT> pNode;
    if (isReConfig) {
        mCommonInfo->mpPipelineContext->queryNodeActor(nodeId, pNode);
        pNode->getNodeImpl()->config(cfgParam);
    } else {
        pNode = new NodeActorT( NodeT::createInstance() );
    }
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    StreamSet inStreamSet;
    StreamSet outStreamSet;
    //
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpAppMeta_Control);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalImage_FD_YUV);
    //
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpAppMeta_DynamicFD);
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    setImageUsage(mParams->mStreamSet.mpHalImage_FD_YUV , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    //
    MERROR err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
configContextLocked_JpegNode(sp<PipelineContext> pContext, MBOOL isReConfig)
{
    typedef JpegNode                NodeT;
    typedef NodeActor< NodeT >      NodeActorT;
    //
    NodeId_T const nodeId = eNODEID_JpegNode;
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = mCommonInfo->mOpenId;
        initParam.nodeId = nodeId;
        initParam.nodeName = "JpegNode";
    }
    NodeT::ConfigParams cfgParam;
    {
        cfgParam.pInAppMeta        = mParams->mStreamSet.mpAppMeta_Control;
        cfgParam.pInHalMeta        = mParams->mStreamSet.mpHalMeta_DynamicP2;
        cfgParam.pOutAppMeta       = mParams->mStreamSet.mpAppMeta_DynamicJpeg;
        cfgParam.pInYuv_Main       = mParams->mStreamSet.mpHalImage_Jpeg_YUV;
        cfgParam.pInYuv_Thumbnail  = mParams->mStreamSet.mpHalImage_Thumbnail_YUV;
        cfgParam.pOutJpeg          = mParams->mStreamSet.mpAppImage_Jpeg;
    }
    //
    sp<NodeActorT> pNode;
    if (isReConfig) {
        mCommonInfo->mpPipelineContext->queryNodeActor(nodeId, pNode);
        pNode->getNodeImpl()->config(cfgParam);
    } else {
        pNode = new NodeActorT( NodeT::createInstance() );
    }
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    StreamSet inStreamSet;
    StreamSet outStreamSet;
    //
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpAppMeta_Control);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalMeta_DynamicP2);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalImage_Jpeg_YUV);
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalImage_Thumbnail_YUV);
    //
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpAppMeta_DynamicJpeg);
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpAppImage_Jpeg);
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    setImageUsage(mParams->mStreamSet.mpHalImage_Jpeg_YUV, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(mParams->mStreamSet.mpHalImage_Thumbnail_YUV, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(mParams->mStreamSet.mpAppImage_Jpeg, eBUFFER_USAGE_SW_WRITE_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    //
    MERROR err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
configContextLocked_Raw16Node(sp<PipelineContext> pContext, MBOOL isReConfig)
{
    typedef RAW16Node               NodeT;
    typedef NodeActor< NodeT >      NodeActorT;
    //
    NodeId_T const nodeId = eNODEID_RAW16Out;
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = mCommonInfo->mOpenId;
        initParam.nodeId = nodeId;
        initParam.nodeName = "Raw16Node";
    }
    NodeT::ConfigParams cfgParam;
    {
    }
    //
    sp<NodeActorT> pNode;
    if (isReConfig) {
        mCommonInfo->mpPipelineContext->queryNodeActor(nodeId, pNode);
        pNode->getNodeImpl()->config(cfgParam);
    } else {
        pNode = new NodeActorT( NodeT::createInstance() );
    }
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    StreamSet inStreamSet;
    StreamSet outStreamSet;
    //
    add_stream_to_set(inStreamSet, mParams->mStreamSet.mpHalImage_P1_Raw);
    //
    add_stream_to_set(outStreamSet, mParams->mStreamSet.mpAppImage_RAW16);
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    setImageUsage(mParams->mStreamSet.mpHalImage_P1_Raw, eBUFFER_USAGE_SW_READ_OFTEN);
    setImageUsage(mParams->mStreamSet.mpAppImage_RAW16, eBUFFER_USAGE_SW_WRITE_OFTEN);
    //
    MERROR err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;

}
/******************************************************************************
 *
 ******************************************************************************/
#undef add_stream_to_set
#undef setImageUsage

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
configScenarioCtrlLocked()
{
    CAM_TRACE_NAME("PipelineDefaultImp::BWC");
    sp<IScenarioControl> pScenarioCtrl = IScenarioControl::create(mCommonInfo->mOpenId);
    IScenarioControl::ControlParam param;

    // invoke setScenarioCtrl in component list
#if 1
    // basic component
    param.scenario   =
        (mParams->mPipelineConfigParams.mOperation_mode!=0) ?                  IScenarioControl::Scenario_HighSpeedVideo :
        (mParams->mPipelineConfigParams.mbHasRecording && mParams->mPipelineConfigParams.mbHasJpeg) ? IScenarioControl::Scenario_VSS :
        (mParams->mPipelineConfigParams.mbHasRecording) ?                      IScenarioControl::Scenario_VideoRecord:
        (mParams->mPipelineConfigParams.mbHasJpeg) ?                           IScenarioControl::Scenario_Capture :
        IScenarioControl::Scenario_NormalPreivew;
    param.sensorSize = mParams->mHwParams.mSensorSize;
    param.sensorFps  = mParams->mHwParams.mSensorFps;

    // streamingpipe component -> eis
    if( mpDeviceHelper->isFirstUsingDevice() )
    {
        MBOOL advEIS = EIS_MODE_IS_EIS_22_ENABLED(mParams->mStreamingPipeParams.mEisInfo.mode) ||
                       EIS_MODE_IS_EIS_25_ENABLED(mParams->mStreamingPipeParams.mEisInfo.mode) ||
                       EIS_MODE_IS_EIS_30_ENABLED(mParams->mStreamingPipeParams.mEisInfo.mode);
        if( advEIS && mParams->mPipelineConfigParams.mb4KRecording )
        {
            FEATURE_CFG_ENABLE_MASK(param.featureFlag, IScenarioControl::FEATURE_ADV_EIS_4K);
        }
        else if( advEIS || mParams->mPipelineConfigParams.mb4KRecording )
        {
            FEATURE_CFG_ENABLE_MASK(param.featureFlag, IScenarioControl::FEATURE_ADV_EIS);
        }
    }

    // streamingpipe component -> vhdr
    if( mParams->mStreamingPipeParams.mVhdrMode == SENSOR_VHDR_MODE_IVHDR )
        FEATURE_CFG_ENABLE_MASK(param.featureFlag, IScenarioControl::FEATURE_IVHDR);
    if( mParams->mStreamingPipeParams.mVhdrMode == SENSOR_VHDR_MODE_MVHDR )
        FEATURE_CFG_ENABLE_MASK(param.featureFlag, IScenarioControl::FEATURE_MVHDR);
    if( mParams->mStreamingPipeParams.mVhdrMode == SENSOR_VHDR_MODE_ZVHDR )
        FEATURE_CFG_ENABLE_MASK(param.featureFlag, IScenarioControl::FEATURE_ZVHDR);
#endif

    CHECK_ERROR(pScenarioCtrl->exitScenario());
    CHECK_ERROR(pScenarioCtrl->enterScenario(param));
    CHECK_ERROR(mCommonInfo->mpPipelineContext->setScenarioControl(pScenarioCtrl));
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
checkPermission()
{
    if ( ! mpCamMgr->getPermission() )
    {
        MY_LOGD("cannot config pipeline ... Permission denied");
        return PERMISSION_DENIED;
    }
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
configRequestRulesLocked()
{
    struct categorize_yuv_stream
    {
        MVOID   operator() (
                    sp<const IImageStreamInfo> const pInfo,
                    MSize const& thres,
                    StreamSet& vLarge, StreamSet& vSmall
                )
                {
                    if( ! pInfo.get() ) return;
                    //
                    MSize const size = pInfo->getImgSize();
                    if ( size.w > thres.w || size.h > thres.h )
                        vLarge.add(pInfo->getStreamId());
                    else
                        vSmall.add(pInfo->getStreamId());
                }
    };
    //
    mParams->mStreamSet.mvYuvStreams_Fullraw.clear();
    mParams->mStreamSet.mvYuvStreams_Resizedraw.clear();
    //
    if( ! mParams->mStreamSet.mpHalImage_P1_ResizerRaw.get() && ! mParams->mStreamSet.mpHalImage_P1_Raw.get() &&
        ! mParams->mStreamSet.mpAppImage_Opaque_In.get() && ! mParams->mStreamSet.mpAppImage_Yuv_In.get() ) {
        MY_LOGE("no available raw stream");
        return UNKNOWN_ERROR;
    }
    //
    MSize const threshold =
       mParams->mStreamSet.mpHalImage_P1_ResizerRaw.get() ? mParams->mStreamSet.mpHalImage_P1_ResizerRaw->getImgSize() : MSize(0,0);
    //
    StreamSet& vLarge =
        mParams->mStreamSet.mpHalImage_P1_Raw.get() || mParams->mStreamSet.mpAppImage_Opaque_Out.get() ? mParams->mStreamSet.mvYuvStreams_Fullraw : mParams->mStreamSet.mvYuvStreams_Resizedraw;
    StreamSet& vSmall =
        mParams->mStreamSet.mpHalImage_P1_ResizerRaw.get() ? mParams->mStreamSet.mvYuvStreams_Resizedraw : mParams->mStreamSet.mvYuvStreams_Fullraw;
    //
    //bool haveFullraw = mpHalImage_P1_Raw.get();
    //
    for( size_t i = 0; i < mParams->mStreamSet.mvAppYuvImage.size(); i++ ) {
        sp<const IImageStreamInfo> pStreamInfo = mParams->mStreamSet.mvAppYuvImage.valueAt(i);
        if( (pStreamInfo->getUsageForConsumer() & GRALLOC_USAGE_HW_VIDEO_ENCODER) &&
             mParams->mPipelineConfigParams.mb4KRecording )
        {
            // 4K recording only
            // Avoid to use IMGO & RRZO in the same time
            // for reaching 30 fps performance
            categorize_yuv_stream()(pStreamInfo, threshold, vSmall, vSmall);
        }
        else
        {
            categorize_yuv_stream()(pStreamInfo, threshold, vLarge, vSmall);
        }
    }
    categorize_yuv_stream()(mParams->mStreamSet.mpHalImage_FD_YUV, threshold, vLarge, vSmall);
    //
    categorize_yuv_stream()(mParams->mStreamSet.mpHalImage_Jpeg_YUV, MSize(0,0), vLarge, vSmall);
    categorize_yuv_stream()(mParams->mStreamSet.mpHalImage_Thumbnail_YUV, MSize(0,0), vLarge, vSmall);
    //
#if 1
    // dump raw stream dispatch rule
    StreamId_T fullStream = mParams->mStreamSet.mpHalImage_P1_Raw.get() ? mParams->mStreamSet.mpHalImage_P1_Raw->getStreamId() :
                            mParams->mStreamSet.mpAppImage_Opaque_Out.get() ? mParams->mStreamSet.mpAppImage_Opaque_Out->getStreamId() :
                            -1;
    for( size_t i = 0; i < mParams->mStreamSet.mvYuvStreams_Fullraw.size(); i++ ) {
        MY_LOGD("full raw streamId:%#" PRIx64 " -> yuv streamId:%#" PRIx64,
                fullStream, mParams->mStreamSet.mvYuvStreams_Fullraw[i]);
    }
    for( size_t i = 0; i < mParams->mStreamSet.mvYuvStreams_Resizedraw.size(); i++ ) {
        MY_LOGD("resized raw streamId:%#" PRIx64 " -> yuv streamId:%#" PRIx64,
                mParams->mStreamSet.mpHalImage_P1_ResizerRaw->getStreamId(), mParams->mStreamSet.mvYuvStreams_Resizedraw[i]);
    }
#endif
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
sp<ImageStreamInfo>
PipelineDefaultImp::ConfigHandler::
createRawImageStreamInfo(
    char const*         streamName,
    StreamId_T          streamId,
    MUINT32             streamType,
    size_t              maxBufNum,
    size_t              minInitBufNum,
    MUINT               usageForAllocator,
    MINT                imgFormat,
    MSize const&        imgSize,
    size_t const        stride
) const
{
    IImageStreamInfo::BufPlanes_t bufPlanes;
    //
#define addBufPlane(planes, height, stride)                                      \
        do{                                                                      \
            size_t _height = (size_t)(height);                                   \
            size_t _stride = (size_t)(stride);                                   \
            IImageStreamInfo::BufPlane bufPlane= { _height * _stride, _stride }; \
            planes.push_back(bufPlane);                                          \
        }while(0)
    switch( imgFormat ) {
        case eImgFmt_BAYER10:
        case eImgFmt_FG_BAYER10:
        case eImgFmt_BAYER8: // LCSO
        case eImgFmt_STA_BYTE:
        case eImgFmt_STA_2BYTE: // LCSO with LCE3.0
            addBufPlane(bufPlanes , imgSize.h, stride);
            break;
        case eImgFmt_UFEO_BAYER8:
        case eImgFmt_UFEO_BAYER10:
        case eImgFmt_UFEO_BAYER12:
        case eImgFmt_UFEO_BAYER14:
            {
                size_t ufoStride = 0;
                addBufPlane(bufPlanes , imgSize.h, stride);
                mParams->mHwInfoHelper.queryUFOStride(imgFormat, imgSize, ufoStride);
                addBufPlane(bufPlanes , imgSize.h, ufoStride);
                break;
            }
        default:
            MY_LOGE("format not support yet %d", imgFormat);
            break;
    }
#undef  addBufPlane

    sp<ImageStreamInfo>
        pStreamInfo = new ImageStreamInfo(
                streamName,
                streamId,
                streamType,
                maxBufNum, minInitBufNum,
                usageForAllocator, imgFormat, imgSize, bufPlanes
                );

    if( pStreamInfo == NULL ) {
        MY_LOGE("create ImageStream failed, %s, %#" PRIx64,
                streamName, streamId);
    }

    return pStreamInfo;
}


/*******************************************************************************
 *
 ********************************************************************************/
PipelineDefaultImp::ConfigHandler::
ConfigHandler(
    std::shared_ptr<CommonInfo> pCommonInfo,
    std::shared_ptr<MyProcessedParams> pParams,
    sp<PipelineDefaultImp::VendorHandler> pVendorHandler
)
    : mCommonInfo(pCommonInfo)
    , mParams(pParams)
    , mPass1Resource()
    , mpCamMgr(CamManager::getInstance())
    , mpDeviceHelper(new CamManager::UsingDeviceHelper(mCommonInfo->mOpenId))
    , mVendorHandler(pVendorHandler)
{
    FUNC_START;
    FUNC_END;
}


/*******************************************************************************
 *
 ********************************************************************************/
PipelineDefaultImp::ConfigHandler::
~ConfigHandler()
{
    FUNC_START;
    FUNC_END;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::ConfigHandler::
reconfigPipelineLocked(parsedAppRequest const& request)
{
    CAM_TRACE_NAME(__FUNCTION__);
    //
    IMetadata* pAppMetaControl = request.pRequest->vIMetaBuffers[0]->tryReadLock(__FUNCTION__);
    if( ! pAppMetaControl ) {
        MY_LOGE("cannot get control meta");
        return UNKNOWN_ERROR;
    }
#if MTKCAM_HAVE_ADV_SETTING
    // Notify to clear app's config meta
    if( ! mCommonInfo->mbFirstReqReceived )
    {
        mCommonInfo->mbFirstReqReceived = MTRUE;
        if(mCommonInfo->mpAdvSettingMgr != NULL)
            mCommonInfo->mpAdvSettingMgr->notifyFirstReq();
    }

    // === Reconfig partial nodes if needed =======
    NSCam::AdvCamSettingMgr::PipelineParam pipelineParam;
    pipelineParam.mResizedRawSize = mParams->mHwParams.mResizedrawSize;
    pipelineParam.mSensorMode = mParams->mHwParams.mSensorMode;
    pipelineParam.mSensorSize = mParams->mHwParams.mSensorSize;
    pipelineParam.mVideoSize = mParams->mPipelineConfigParams.mVideoSize;
    pipelineParam.mMaxStreamSize = mParams->mPipelineConfigParams.mMaxStreamSize;
    pipelineParam.currentAdvSetting = mParams->mStreamingPipeParams.mpAdvSetting;
    NSCam::AdvCamSettingMgr::AdvCamInputParam advCamInput{
            mParams->mPipelineConfigParams.mbHasRecording,
            mParams->mPipelineConfigParams.mVideoSize,
            mParams->mPipelineConfigParams.mb4KRecording,
            mParams->mStreamingPipeParams.mpAdvSetting,
            mParams->mConfigParams.mOperation_mode
        };
    if(mCommonInfo->mpAdvSettingMgr != NULL && mCommonInfo->mpAdvSettingMgr->needReconfigPipeline(pAppMetaControl, pipelineParam, advCamInput) )
    {
        MSize oldFullSize = mParams->mHwParams.mFullrawSize;
        MSize oldResizedSize = mParams->mHwParams.mResizedrawSize;
        MINT32 oldEisMode = mParams->mStreamingPipeParams.mEisInfo.mode;
        MINT32 oldEisFactor = mParams->mStreamingPipeParams.mEisInfo.factor;

        mParams->mStreamingPipeParams.mpAdvSetting = mCommonInfo->mpAdvSettingMgr->regenAdvSetting(pAppMetaControl, advCamInput);
        mParams->update(mParams->mConfigParams, MFALSE);

        MBOOL bStreamChange = (mParams->mPipelineConfigParams.mbHasLcso && ! mParams->mStreamSet.mpHalImage_P1_Lcso.get())
                           || (mParams->mStreamingPipeParams.mbHasRsso && ! mParams->mStreamSet.mpHalImage_P1_Rsso.get())
                           || (oldFullSize != mParams->mHwParams.mFullrawSize)
                           || (oldResizedSize != mParams->mHwParams.mResizedrawSize)
                           || (oldEisMode != mParams->mStreamingPipeParams.mEisInfo.mode)
                           || (oldEisFactor != mParams->mStreamingPipeParams.mEisInfo.factor);

        MY_LOGI("do re-configure pipeline. streamChange(%d), Full(%d,%d)->(%d,%d), Resize(%d,%d)->(%d,%d),LcsoStream(%p),\
                    eisMode(0x%x->0x%x), eisFac(%u->%u)",
                    bStreamChange, oldFullSize.w, oldFullSize.h, mParams->mHwParams.mFullrawSize.w, mParams->mHwParams.mFullrawSize.h,
                                   oldResizedSize.w, oldResizedSize.h, mParams->mHwParams.mResizedrawSize.w, mParams->mHwParams.mResizedrawSize.h,
                    mParams->mStreamSet.mpHalImage_P1_Lcso.get(), oldEisMode, mParams->mStreamingPipeParams.mEisInfo.mode, oldEisFactor, mParams->mStreamingPipeParams.mEisInfo.factor);

        if(bStreamChange){
            mCommonInfo->mpPipelineContext->waitUntilDrained();
            CHECK_ERROR(configScenarioCtrlLocked());
            CHECK_ERROR(setupHalStreamsLocked(mParams->mConfigParams));
            CHECK_ERROR(configContextLocked_Streams(mCommonInfo->mpPipelineContext));
            CHECK_ERROR(configContextLocked_Nodes(mCommonInfo->mpPipelineContext, MTRUE));
        }else{
            CHECK_ERROR(mCommonInfo->mpPipelineContext->waitUntilNodeDrained(eNODEID_P1Node));
            CHECK_ERROR(configScenarioCtrlLocked());
            configContextLocked_P1Node(mCommonInfo->mpPipelineContext, MTRUE);
        }
    }
#endif
    if ( mParams->mPipelineConfigParams.mOperation_mode )
    {
        // smvr
        IMetadata::IEntry const& entry = pAppMetaControl->entryFor(MTK_CONTROL_AE_TARGET_FPS_RANGE);
        MINT32 i4MinFps = entry.itemAt(0, Type2Type< MINT32 >());
        MINT32 i4MaxFps = entry.itemAt(1, Type2Type< MINT32 >());
        if ( mParams->mCHSvrParams.mAeTargetFpsMin != i4MinFps )
        {
            MY_LOGI("do re-configure pipeline. min fps: %d -> %d ", mParams->mCHSvrParams.mAeTargetFpsMin, i4MinFps);
            mParams->mCHSvrParams.mAeTargetFpsMin_Req = i4MinFps;
            mParams->mCHSvrParams.mResetAeTargetFps_Req = MTRUE;
            CAM_TRACE_BEGIN("reconfigure: waitUntilDrained");
            mCommonInfo->mpPipelineContext->waitUntilDrained();
            CAM_TRACE_END();
            MY_LOGD("wait drained done");
            //
            mParams->update(mParams->mConfigParams, MFALSE);
            CHECK_ERROR(configScenarioCtrlLocked());
            CHECK_ERROR(setupHalStreamsLocked(mParams->mConfigParams));
            CHECK_ERROR(configContextLocked_Streams(mCommonInfo->mpPipelineContext));
            CHECK_ERROR(configContextLocked_Nodes(mCommonInfo->mpPipelineContext, MTRUE));
            MY_LOGI("end re-configure pipeline");
        }
    }
    //
    if( pAppMetaControl )
        request.pRequest->vIMetaBuffers[0]->unlock(
                __FUNCTION__, pAppMetaControl
            );
    return OK;
}
