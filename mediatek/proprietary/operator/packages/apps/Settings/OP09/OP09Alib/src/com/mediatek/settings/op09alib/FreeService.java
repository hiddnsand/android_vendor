/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.app.IntentService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.android.internal.telephony.PhoneConstants;

/**
 * When user press <code>FreeService</code>,
 * the number "+86 18918910000" will be dialed directly.
 * Note:
 * Even the default SIM for "Voice call" is another non-CDMA SIM,
 * this free service number is always dialed out by CDMA SIM card.
 */
public class FreeService extends IntentService {

    private static final String TAG = "FreeService";

    public static final int NO_SIM_ERROR = 0;
    public static final int ONE_CDMA = 1;
    public static final int ONE_GSM = 2;
    public static final int TWO_SIM = 3;
    public static final String SIM_INFO = "SIM_INFO";
    public static final String EXTRA_SLOT_ID = "com.android.phone.extra.slot";

    private static final String FREE_SERVICE_URI = "tel:+8618918910000";

    /**
     * Creates a new <code>FreeService</code> instance.
     *
     */
    public FreeService() {
        super("FreeService");
        Log.v("@M_" + TAG, "FreeService Constructor is called.");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onHandleIntent(Intent intent) {
        Log.v("@M_" + TAG, "onHandleIntent method is called.");
        Intent newIntent = new Intent(Intent.ACTION_CALL, Uri.parse(FREE_SERVICE_URI));
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        int simStatus = intent.getIntExtra(SIM_INFO, NO_SIM_ERROR);
        Log.v("@M_" + TAG, "dialUsingAccount, simStatus=" + simStatus);
        if (simStatus == TWO_SIM) {
            // When two SIM inserted, using the account to dial to neglect the sim choice dialog.
            dialUsingAccount(PhoneConstants.SIM_ID_1, newIntent);
        } else if (simStatus == ONE_CDMA) {
            // It's normal status, calling by SIM1 CDMA
            newIntent.putExtra(EXTRA_SLOT_ID, PhoneConstants.SIM_ID_1);
        } else if (simStatus == ONE_GSM) {
            // Only GSM Sim inserted into Slot2, calling by SIM2 GSM
            newIntent.putExtra(EXTRA_SLOT_ID, PhoneConstants.SIM_ID_2);
        } else { // NO_SIM_ERROR
            Log.v("@M_" + TAG, "ERROR! No sim detected!");
        }

        startActivity(newIntent);
        Log.v("@M_" + TAG, "Dialing Successfully!");
    }

    /**
     * Using the account to fill the intent for neglecting the sim choice dialog when dialing.
     * @param slotId the slot id.
     * @param intent the Intent.
     */
    private void dialUsingAccount(int slotId, Intent intent) {
        final Context context = getApplicationContext();
        final int [] subIds = SubscriptionManager.getSubId(slotId);
        Log.v("@M_" + TAG, "dialUsingAccount, slotId = " + slotId + ",subIds = " + subIds);
        if (subIds != null && subIds.length != 0) {
            int subId = subIds[0];
            Log.v("@M_" + TAG, "dialUsingAccount, slotId = " + slotId + ",subId = " + subId);
            if (subId != -1) {
                ComponentName pstnConnectionServiceName = new ComponentName("com.android.phone",
                        "com.mediatek.services.telephony.TelephonyConnectionService");
                //String id = "" + String.valueOf(subId);
                SubscriptionInfo subInfo = SubscriptionManager.from(context)
                        .getActiveSubscriptionInfo(subId);
                String id = subInfo.getIccId();
                PhoneAccountHandle phoneAccountHandle =
                    new PhoneAccountHandle(pstnConnectionServiceName, id);
                intent.putExtra(TelecomManager.EXTRA_PHONE_ACCOUNT_HANDLE, phoneAccountHandle);
            }
        }
    }

}
