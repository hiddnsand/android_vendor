/*
 * Copyright (C) 2017 MICROTRUST Incorporated
 * All rights reserved
 *
 * This file and software is confidential and proprietary to MICROTRUST Inc.
 * Unauthorized copying of this file and software is strictly prohibited.
 * You MUST NOT disclose this file and software unless you get a license
 * agreement from MICROTRUST Incorporated.
 */

#define IMSG_TAG "ut_kmsetkey"

#include <errno.h>
#include <hardware/hardware.h>
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <imsg_log.h>

#include "ut_km_def.h"
#include "ut_km_ioctl.h"
#include "ut_kmsetkey.h"

static int ut_kmsetkey_open(const hw_module_t* module, const char* name, hw_device_t** device);

static kmsetkey_error_t ut_ree_import_attest_keybox(const unsigned char* peakb,
    const unsigned int peakb_len)
{
    IMSG_ENTER();
    int32_t retVal;
    uint8_t* p_data_buff = 0;
    uint32_t in_params_size = 0;
    ut_message_t kmstr_msg;

	if (peakb == NULL) {
		IMSG_ERROR("null pointer in import attest key");
		return KMSK_ERROR_UNEXPECTED_NULL_POINTER;
	}

    if (TEE_Alloc_KM_Buffer_And_Reset() != 0) {
        IMSG_ERROR("%s: malloc km buffer failed", __FUNCTION__);
        return KMSK_ERROR_MEMORY_ALLOCATION_FAILED;
    }

    memset(&kmstr_msg, 0, sizeof(ut_message_t));
    p_data_buff = keymaster_buffer + sizeof(ut_message_t);
    kmstr_msg.command.commandId = CMD_ID_TEE_REE_IMPORT_KEYBOX;
    kmstr_msg.import_attest_keybox.attest_keybox_len = peakb_len;
    memcpy(keymaster_buffer, &kmstr_msg, sizeof(ut_message_t));
	memcpy(p_data_buff, peakb, peakb_len);
    retVal = TEE_InvokeCommand();

    if (retVal != 0) {
        IMSG_ERROR("TEE invoke command err %d", retVal);
        return KMSK_ERROR_INVOKE_COMMAND_FAILED;
    }

    memcpy(&kmstr_msg, keymaster_buffer, sizeof(ut_message_t));

    if (kmstr_msg.response.error != KMSK_ERROR_OK) {
        IMSG_ERROR("keymaster configure failed %d", kmstr_msg.response.error);
        return KMSK_ERROR_RESPONSE_ERROR;
    }

    //retVal = kmstr_msg.response.error;
    return KMSK_ERROR_OK;

}

static int ut_kmsetkey_close(hw_device_t* dev) {
    IMSG_ENTER();

    TEE_CLOSE();
    free(dev);

    return 0;
}

static struct hw_module_methods_t kmsetkey_module_methods = {
	.open = ut_kmsetkey_open,
};

__attribute__((visibility("default")))
struct kmsetkey_module HAL_MODULE_INFO_SYM = {
	.common =
	{
		.tag = HARDWARE_MODULE_TAG,
		.module_api_version = KMSETKEY_MODULE_API_VERSION_0_1,
		.hal_api_version = HARDWARE_HAL_API_VERSION,
		.id = KMSETKEY_HARDWARE_MODULE_ID,
		.name = KMSETKEY_HARDWARE_MODULE_NAME,
		.author = "Microtrust",
		.methods = &kmsetkey_module_methods,
		.dso = 0,
		.reserved = {},
	},
};

static int ut_kmsetkey_open(const hw_module_t* module, const char* name, hw_device_t** device) {
    IMSG_ENTER();

    if (name == NULL)
        return -EINVAL;

    // Make sure we initialize only if module provided is known
    if ((module->tag != HAL_MODULE_INFO_SYM.common.tag) ||
        (module->module_api_version != HAL_MODULE_INFO_SYM.common.module_api_version) ||
        (module->hal_api_version != HAL_MODULE_INFO_SYM.common.hal_api_version) ||
        (0 != memcmp(module->name, HAL_MODULE_INFO_SYM.common.name,
                    sizeof(KMSETKEY_HARDWARE_MODULE_NAME)-1)) )
	{
        IMSG_ERROR("invalide kmsetkey module information");
        return -EINVAL;
    }

    int ret = TEE_OPEN();

    if (ret != 0) {
        IMSG_ERROR("kmsetkey open device node failed, errno=%d", errno);
        return -ENOMEM;
    }

    kmsetkey_device_t* dev = NULL;
    dev = (kmsetkey_device_t*)malloc(sizeof(kmsetkey_device_t));

    dev->common.tag = HARDWARE_MODULE_TAG;
    dev->common.version = 1;
    dev->common.module = (struct hw_module_t*) module;
    dev->common.close = ut_kmsetkey_close;

    dev->attest_key_install = ut_ree_import_attest_keybox;

    *device = (hw_device_t*)dev;
    return 0;
}

