/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mediatek.protect.emailcommon.utility;

import android.util.Base64DataException;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * M: An extend Base64 InputStream that can decode the extra data behind the
 * standard end flag '='. An InputStream that does Base64 decoding on the data
 * read through it.
 */
public class ExtendBase64InputStream extends FilterInputStream {
    private final ExtendBase64.Coder mCoder;

    private static final byte[] EMPTY = new byte[0];

    private static final int BUFFER_SIZE = 2048;
    private boolean mEof;
    private byte[] mInputBuffer;
    private int mOutputStart;
    private int mOutputEnd;

    /**
     * An InputStream that performs Base64 decoding on the data read from the
     * wrapped stream.
     *
     * @param in
     *            the InputStream to read the source data from
     * @param flags
     *            bit flags for controlling the decoder; see the constants in
     *            {@link Base64}
     */
    public ExtendBase64InputStream(InputStream in, int flags) {
        this(in, flags, false);
    }

    /**
     * Performs Base64 encoding or decoding on the data read from the wrapped
     * InputStream.
     *
     * @param in
     *            the InputStream to read the source data from
     * @param flags
     *            bit flags for controlling the decoder; see the constants in
     *            {@link Base64}
     * @param encode
     *            true to encode, false to decode
     *
     * @hide
     */
    public ExtendBase64InputStream(InputStream in, int flags, boolean encode) {
        super(in);
        mEof = false;
        mInputBuffer = new byte[BUFFER_SIZE];
        if (encode) {
            mCoder = new ExtendBase64.Encoder(flags, null);
        } else {
            mCoder = new ExtendBase64.Decoder(flags, null);
        }
        mCoder.output = new byte[mCoder.maxOutputSize(BUFFER_SIZE)];
        mOutputStart = 0;
        mOutputEnd = 0;
    }

    public boolean markSupported() {
        return false;
    }

    public void mark(int readlimit) {
        throw new UnsupportedOperationException();
    }

    public void reset() {
        throw new UnsupportedOperationException();
    }

    public void close() throws IOException {
        in.close();
        mInputBuffer = null;
    }

    public int available() {
        return mOutputEnd - mOutputStart;
    }

    public long skip(long n) throws IOException {
        if (mOutputStart >= mOutputEnd) {
            refill();
        }
        if (mOutputStart >= mOutputEnd) {
            return 0;
        }
        long bytes = Math.min(n, mOutputEnd - mOutputStart);
        mOutputStart += bytes;
        return bytes;
    }

    public int read() throws IOException {
        if (mOutputStart >= mOutputEnd) {
            refill();
        }
        if (mOutputStart >= mOutputEnd) {
            return -1;
        } else {
            return mCoder.output[mOutputStart++] & 0xff;
        }
    }

    public int read(byte[] b, int off, int len) throws IOException {
        if (mOutputStart >= mOutputEnd) {
            refill();
        }
        if (mOutputStart >= mOutputEnd) {
            return -1;
        }
        int bytes = Math.min(len, mOutputEnd - mOutputStart);
        System.arraycopy(mCoder.output, mOutputStart, b, off, bytes);
        mOutputStart += bytes;
        return bytes;
    }

    /**
     * Read data from the input stream into inputBuffer, then decode/encode it
     * into the empty coder.output, and reset the outputStart and outputEnd
     * pointers.
     */
    private void refill() throws IOException {
        if (mEof) {
            return;
        }
        int bytesRead = in.read(mInputBuffer);
        boolean success;
        if (bytesRead == -1) {
            mEof = true;
            success = mCoder.process(EMPTY, 0, 0, true);
        } else {
            success = mCoder.process(mInputBuffer, 0, bytesRead, false);
        }
        if (!success) {
            throw new Base64DataException("bad base-64");
        }
        mOutputEnd = mCoder.op;
        mOutputStart = 0;
    }
}
