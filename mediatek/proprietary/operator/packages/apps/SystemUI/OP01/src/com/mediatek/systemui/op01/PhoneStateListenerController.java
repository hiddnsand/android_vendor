package com.mediatek.systemui.op01;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Process;
import android.os.SystemProperties;
import android.telephony.PhoneStateListener;
import android.telephony.PreciseDataConnectionState;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.SubscriptionManager.OnSubscriptionsChangedListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.List;

/**
 * M: Op01 PhoneState Listener Controller.
 */
public class PhoneStateListenerController {
    private static final String TAG = "OP01-PhoneStateListenerController";

    private TelephonyManager mTelephonyManager;
    private SubscriptionManager mSubscriptionManager;
    private OnSubscriptionsChangedListener mSubscriptionsChangedListener;
    private boolean registered;
    private Context mContext;

    private static final boolean DEBUG = !isUserLoad();
    private static HashMap<Integer, ArrayList> sUpdateActivityIconMap =
            new HashMap<Integer, ArrayList>();
    private Handler mUiHandler = new Handler(Looper.getMainLooper());

    private HashMap<Integer, StatusBarExtStateListener> mStatusBarExtStateListeners = 
            new HashMap<Integer, StatusBarExtStateListener>();

    private static PhoneStateListenerController mPhoneStateController;

    private static HashMap<Integer, ArrayList> mPreciseDataStateMap =
            new HashMap<Integer, ArrayList>();
    private HandlerThread mHandlerThread;
    // Handler that all broadcasts are received on.
    private final Handler mReceiverHandler;
    private boolean mAllDisconn;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED)) {
                handleIntent(intent);
            }
        }
    };

    public static PhoneStateListenerController getInstance(Context context) {
        if (mPhoneStateController != null) {
            return mPhoneStateController;
        } else {
            mPhoneStateController = new PhoneStateListenerController(context);
            return mPhoneStateController;
        }
    }

    public PhoneStateListenerController(Context context) {
        Log.v(TAG, "OP01 PhoneStateListenerService onCreate");
        mHandlerThread = new HandlerThread(TAG, Process.THREAD_PRIORITY_BACKGROUND);
        mHandlerThread.start();
        mReceiverHandler = new Handler(mHandlerThread.getLooper());

        mContext = context;
        mTelephonyManager = TelephonyManager.from(mContext);
        mSubscriptionManager = SubscriptionManager.from(mContext);

        IntentFilter intentFilter =  new IntentFilter(
                TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED);
        mContext.registerReceiver(mReceiver, intentFilter, null, mReceiverHandler);

        mSubscriptionsChangedListener = new OnSubscriptionsChangedListener() {
            @Override
            public void onSubscriptionsChanged() {
                updateStateListener();
            }
        };
    }

    private void handleIntent(Intent intent) {
        String state = intent.getStringExtra(PhoneConstants.STATE_KEY);
        PhoneConstants.DataState dataState;
        if (state != null) {
            dataState = Enum.valueOf(PhoneConstants.DataState.class, state);
        } else {
            dataState = PhoneConstants.DataState.DISCONNECTED;
        }
        String apnType = intent.getStringExtra(PhoneConstants.DATA_APN_TYPE_KEY);
        int subId = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY, -1);
        if (DEBUG) {
            Log.d(TAG, "handle precise data Intent, subId: " + subId + ",apnType: " + apnType +
                    ",dataState: " + dataState);
        }

        synchronized (mPreciseDataStateMap) {
            ArrayList<String> apnTypeList;
            if (mPreciseDataStateMap.containsKey(subId)) {
                apnTypeList = mPreciseDataStateMap.get(subId);
            } else {
                apnTypeList = new ArrayList<>();
            }

            if (dataState == PhoneConstants.DataState.CONNECTED) {
                if (!apnTypeList.contains(apnType)) {
                    apnTypeList.add(apnType);
                    mAllDisconn = false;
                    if (DEBUG) {
                        Log.d(TAG, "subId: " + subId + ", put apnType: " + apnType +
                                " to cache, mAllDisconn: " + mAllDisconn);
                    }
                }
            } else {
                if (apnTypeList.contains(apnType)) {
                    apnTypeList.remove(apnType);
                    if (apnTypeList.size() == 0) {
                        mAllDisconn = true;
                    }
                    if (DEBUG) {
                        Log.d(TAG, "subId: " + subId + ", remove apnType: " + apnType
                                + " from cache, mAllDisconn: " + mAllDisconn);
                    }
                }
            }
            mPreciseDataStateMap.put(subId, apnTypeList);
        }

        // update data activity icon
        mUiHandler.post(new Runnable() {
            @Override
            public void run() {
                ArrayList<ImageView> viewsList = sUpdateActivityIconMap.get(subId);
                if (viewsList != null) {
                    for (ImageView view : viewsList) {
                        if (isShowDataActyIconByIntent(subId)) {
                            view.setVisibility(View.VISIBLE);
                        } else {
                            view.setVisibility(View.GONE);
                        }
                    }
                }
            }
        });
    }

    public static final boolean isUserLoad() {
        return SystemProperties.get("ro.build.type").equals("user") ||
                    SystemProperties.get("ro.build.type").equals("userdebug");
    }

    public void addDataActivityView(int subId, ImageView view) {
        ArrayList<ImageView> viewsList;
        if (sUpdateActivityIconMap.containsKey(subId)) {
            viewsList = sUpdateActivityIconMap.get(subId);
        } else {
            viewsList = new ArrayList<>();
        }

        if (!viewsList.contains(view)) {
            viewsList.add(view);
            if (DEBUG) {
                Log.d(TAG, "addDataActivityView to list, view: " + view);
            }
        }
        sUpdateActivityIconMap.put(subId, viewsList);
        viewsList = null;
    }

    public void registerStateListener() {
        Log.d(TAG, "registerStateListener");
        if (registered) {
            Log.d(TAG, "have registered, return");
            return;
        }
        List<SubscriptionInfo> infos = mSubscriptionManager.getActiveSubscriptionInfoList();
        if (infos == null) {
            return;
        }
        synchronized (mStatusBarExtStateListeners) {
            for (SubscriptionInfo info : infos) {
                int subId = info.getSubscriptionId();
                if (!mStatusBarExtStateListeners.containsKey(subId)) {
                    StatusBarExtStateListener listener = new StatusBarExtStateListener(subId);
                    mStatusBarExtStateListeners.put(subId, listener);
                }
            }

            // Listen for phone state changed
            for (StatusBarExtStateListener listener : mStatusBarExtStateListeners.values()) {
                mTelephonyManager.listen(listener,
                        PhoneStateListener.LISTEN_PRECISE_DATA_CONNECTION_STATE);
            }
        }

        registered = true;

        // Listen for sim changed.
        //mSubscriptionManager.addOnSubscriptionsChangedListener(mSubscriptionsChangedListener);
    }

    public boolean isShowDataActyIcon(int subId) {
        synchronized (mStatusBarExtStateListeners) {
            boolean isShow = true;
            StatusBarExtStateListener listener = mStatusBarExtStateListeners.get(subId);
            if (listener != null) {
                isShow = listener.isShowDataActivityIcon();
            }
            Log.v(TAG, "isShowDataActyIcon(),subId: " + subId + ", return isShow: " + isShow);
            return isShow;
        }
    }

    public boolean isShowDataActyIconByIntent(int subId) {
        synchronized (mPreciseDataStateMap) {
            boolean isShow = true;
            ArrayList<String> apnTypeList = mPreciseDataStateMap.get(subId);
            if (apnTypeList.size() == 2) {
                if (apnTypeList.contains(PhoneConstants.APN_TYPE_IMS)
                        && apnTypeList.contains(PhoneConstants.APN_TYPE_EMERGENCY)) {
                    isShow = false;
                    if (DEBUG) {
                        Log.d(TAG, "subId: " + subId
                                + " isShowDataActyIconByIntent(), contain ims and emergency,"
                                + "return false");
                    }
                }
            } else if (apnTypeList.size() == 1) {
                if (apnTypeList.contains(PhoneConstants.APN_TYPE_IMS)
                        || apnTypeList.contains(PhoneConstants.APN_TYPE_EMERGENCY)) {
                    isShow = false;
                    if (DEBUG) {
                        Log.d(TAG, "subId: " + subId +
                                " isShowDataActyIconByIntent(), contain ims or emergency, " +
                                "return false");
                    }
                }
            } else if (mAllDisconn) {
                isShow = false;
                if (DEBUG) {
                    Log.d(TAG, "subId: " + subId +
                            " isShowDataActyIconByIntent(), mAllDisconn is ture, return false");
                }
            }
            return isShow;
        }
    }

    private void updateStateListener() {
        Log.d(TAG, "updateStateListener");
        List<SubscriptionInfo> subInfos = mSubscriptionManager.getActiveSubscriptionInfoList();
        if (subInfos == null) {
            return;
        }

        synchronized (mStatusBarExtStateListeners) {
            // We should remove all the item in mRichCallStateListener, and then add
            // the listener again.
            for (Integer id : mStatusBarExtStateListeners.keySet()) {
                int subId = id.intValue();
                StatusBarExtStateListener listener = mStatusBarExtStateListeners.get(id);
                mTelephonyManager.listen(listener, PhoneStateListener.LISTEN_NONE);
            }

            for (SubscriptionInfo info : subInfos) {
                int subId = info.getSubscriptionId();
                if (!mStatusBarExtStateListeners.containsKey(subId)) {
                    StatusBarExtStateListener listener = new StatusBarExtStateListener(subId);
                    mStatusBarExtStateListeners.put(subId, listener);
                    mTelephonyManager.listen(listener,
                            PhoneStateListener.LISTEN_PRECISE_DATA_CONNECTION_STATE);
                    Log.d(TAG, "updateStateListener, listen state for new sub: " + subId);
                } else {
                    StatusBarExtStateListener listener = mStatusBarExtStateListeners.get(subId);
                    if (listener != null) {
                        mTelephonyManager.listen(listener,
                                PhoneStateListener.LISTEN_PRECISE_DATA_CONNECTION_STATE);
                        Log.d(TAG, "updateStateListener, relisten state for old sub: " + subId);
                    }
                }
            }
        }
    }

    private class StatusBarExtStateListener extends PhoneStateListener {
        private HashSet<String> mPreciseDataConnectedState = new HashSet<String>();
        private int mSubId;
        private boolean disconn;

        public StatusBarExtStateListener(int subId) {
            super(subId);
            mSubId = subId;
        }

        @Override
        public void onPreciseDataConnectionStateChanged(PreciseDataConnectionState state) {
            String apnType = state.getDataConnectionAPNType();
            int dataState = state.getDataConnectionState();
            synchronized (mPreciseDataConnectedState) {
                Log.d(TAG, "subId: " + mSubId +
                        " onPreciseDataConnectionStateChanged: state = " + state.toString());
                if (dataState == TelephonyManager.DATA_CONNECTED) {
                    if (!mPreciseDataConnectedState.contains(apnType)) {
                        mPreciseDataConnectedState.add(apnType);
                        disconn = false;
                        Log.d(TAG, "subId: " + mSubId
                                + " onPreciseDataConnectionStateChanged: put apnType: "
                                + apnType + ", dataState: " + dataState
                                + " into mPreciseDataConnectedState, disconn: " + disconn);
                    }
                } else {
                    if (mPreciseDataConnectedState.contains(apnType)) {
                        mPreciseDataConnectedState.remove(apnType);
                        if (mPreciseDataConnectedState.size() == 0) {
                            disconn = true;
                        }
                        Log.d(TAG, "subId: " + mSubId
                                + " onPreciseDataConnectionStateChanged: remove apnType: "
                                + apnType + ", dataState: " + dataState
                                + " from mPreciseDataConnectedState, disconn: " + disconn);
                    }
                }
            }
            // update data activity icon
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    ArrayList<ImageView> viewsList = sUpdateActivityIconMap.get(mSubId);
                    if (viewsList != null) {
                        for (ImageView view : viewsList) {
                            if (isShowDataActivityIcon()) {
                                view.setVisibility(View.VISIBLE);
                            } else {
                                view.setVisibility(View.GONE);
                            }
                        }
                    }
                }
            });
        }

        /**
         * M: add for op01 Check if show data activity icon.
         * 
         * @return if show data activity icon.
         */
        public boolean isShowDataActivityIcon() {
            synchronized (mPreciseDataConnectedState) {
                if (mPreciseDataConnectedState.size() == 1) {
                    if (mPreciseDataConnectedState.contains(PhoneConstants.APN_TYPE_IMS)
                            || mPreciseDataConnectedState
                                    .contains(PhoneConstants.APN_TYPE_EMERGENCY)) {
                        if (DEBUG) {
                            Log.d(TAG, "subId: " + mSubId +
                                    " isShowDataActivityIcon(), contain ims or emergency, " +
                                    "return false");
                        }
                        return false;
                    }
                } else if (mPreciseDataConnectedState.size() == 2) {
                    if (mPreciseDataConnectedState.contains(PhoneConstants.APN_TYPE_IMS)
                            && mPreciseDataConnectedState
                                    .contains(PhoneConstants.APN_TYPE_EMERGENCY)) {
                        if (DEBUG) {
                            Log.d(TAG, "subId: " + mSubId
                                    + " isShowDataActivityIcon(), contain ims and emergency,"
                                    + "return false");
                        }
                        return false;
                    }
                } else if (disconn) {
                    if (DEBUG) {
                        Log.d(TAG, "subId: " + mSubId
                                + " isShowDataActivityIcon(), disconn is true, return false");
                    }
                    return false;
                }
                if (DEBUG) {
                    Log.d(TAG, "subId: " + mSubId + " isShowDataActivityIcon(), return true, " +
                         "mPreciseDataConnectedState.size(): " + mPreciseDataConnectedState.size());
                }
            }
            return true;
        }
    }

}
