/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.atci.utils;

/**
 * The result of execution of a single AT command.<p>
 *
 *
 * This class can represent the final response to an AT command line, and also
 * intermediate responses to a single command within a chained AT command
 * line.<p>
 *
 * The actual responses that are intended to be send in reply to the AT command
 * line are stored in a string array. The final response is stored as an
 * int enum, converted to a string when toString() is called. Only a single
 * final response is sent from multiple commands chained into a single command
 * line.<p>
 * @hide
 */
public class AtCommandResult {
    // Result code enumerations
    public static final int OK = 0;
    public static final int ERROR = 1;
    public static final int UNSOLICITED = 2;
    public static final int RILRSP = 3;

    private static final String OK_STRING = "OK";
    private static final String ERROR_STRING = "ERROR";

    private int mResultCode;  // Result code
    private StringBuilder mResponse; // Response with CRLF line breaks

    /**
     * Construct a new AtCommandResult with given result code, and an empty
     * response array.
     * @param resultCode One of OK, ERROR or UNSOLICITED.
     */
    public AtCommandResult(int resultCode) {
        mResultCode = resultCode;
        mResponse = new StringBuilder();
    }

    /**
     * Construct a new AtCommandResult with result code OK, and the specified
     * single line response.
     * @param response The single line response.
     */
    public AtCommandResult(String response) {
        this(OK);
        addResponse(response);
    }

    public int getResultCode() {
        return mResultCode;
    }

    /**
     * Add another line to the response.
     */
    public void addResponse(String response) {
        appendWithCrlf(mResponse, response);
    }

    /**
     * Add the given result into this AtCommandResult object.<p>
     * Used to combine results from multiple commands in a single command line
     * (command chaining).
     * @param result The AtCommandResult to add to this result.
     */
    public void addResult(AtCommandResult result) {
        if (result != null) {
            appendWithCrlf(mResponse, result.mResponse.toString());
            mResultCode = result.mResultCode;
        }
    }

    /**
     * Generate the string response ready to send
     */
    public String toString() {
        StringBuilder result = new StringBuilder(mResponse.toString());
        switch (mResultCode) {
        case OK:
            appendWithCrlf(result, OK_STRING);
            break;
        case ERROR:
            appendWithCrlf(result, ERROR_STRING);
            break;
        }
        return result.toString();
    }

    /** Append a string to a string builder, joining with a double
     * CRLF. Used to create multi-line AT command replies
     */
    public static void appendWithCrlf(StringBuilder str1, String str2) {
        if (str1.length() > 0 && str2.length() > 0) {
            str1.append("\r\n\r\n");
        }
        str1.append(str2);
    }
};
