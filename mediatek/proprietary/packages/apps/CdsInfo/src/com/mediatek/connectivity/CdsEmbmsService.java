package com.mediatek.connectivity;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemProperties;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
  * Service class for eMBMS enginner function.
  *
  */
public class CdsEmbmsService extends Service
                implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = "CdsEmbms";

    private static final int MAX_AT_CMD_RESPONSE = 2048;
    private static final int MAX_SESSION_NUM = 8;

    private PhoneStateListener mPhoneStateListener;
    private SharedPreferences mDataStore;
    private SharedPreferences.Editor  mDataEditor;
    private final IBinder mBinder = new EmbmsBinder();
    private boolean mIsEmbmsAuto;
    private boolean mIsEmbmsBind;
    private static String sAtCmdRsp;
    private static CountDownLatch sReceiveLatch;

    /**
     *
     * @hiden
     */
    public class EmbmsBinder extends Binder {
        CdsEmbmsService getService() {
            // Return this instance of LocalService so clients can call public methods
            return CdsEmbmsService.this;
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
                boolean isAirplaneModeOn = intent.getBooleanExtra("state", false);
                if (!isAirplaneModeOn) {
                    try {
                         Thread.sleep(1 * 1000);
                    } catch (InterruptedException e) {
                    }
                    checkEmbmsStatus();
                }
            }
        }
    };

    private void checkEmbmsStatus() {
        int count = 3;
        updateSessionVariables();
        Log.i(TAG, "Check embms status:" + mIsEmbmsAuto);
        if (mIsEmbmsAuto) {
            while (!isRadioOn() && count > 0) {
                count--;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {

                }
            }
            if (count > 0) {
                enableEmbmsSrv(true);
            }
        }
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate() {
        mDataStore = this.getSharedPreferences(CdsEmbmsConsts.EMBMS_FILE, 0);
        mDataStore.registerOnSharedPreferenceChangeListener(this);

        // IntentFilter intentFilter = new IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        // registerReceiver(mReceiver, intentFilter);
        // checkEmbmsStatus();

        TelephonyManager telephonyManager =
            (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int subId = getDefaultSubId();
        mPhoneStateListener = getPhoneStateListener(subId);
        telephonyManager.listen(mPhoneStateListener,
                            PhoneStateListener.LISTEN_SERVICE_STATE);

        updateSessionVariables();
        mIsEmbmsBind = false;
        Log.i(TAG, "onCreate in CdsEmbmsService");
    }

    private void updateSessionVariables() {
        mIsEmbmsAuto = mDataStore.getBoolean(CdsEmbmsConsts.EMBMS_AUTO_ENABLED, false);
        Log.i(TAG, "updateSessionVariables:" + mIsEmbmsAuto);
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy in CdsEmbmsService");
        mDataStore.unregisterOnSharedPreferenceChangeListener(this);
        //unregisterReceiver(mReceiver);

        TelephonyManager telephonyManager =
            (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(mPhoneStateListener,
                        PhoneStateListener.LISTEN_NONE);
    }


    @Override
    public IBinder onBind(Intent intent) {
        mIsEmbmsBind = true;
        Log.d(TAG, "onBind()" + mIsEmbmsBind);
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        checkEmbmsStatus();
        return START_NOT_STICKY;
    }

    /**
     *  Enable modem eMBMS capability.
     *
     */
    public void enableEmbmsFeature() {
        sendAtcmd("AT+ESBP=5,\"SBP_LTE_MBMS\",1");
        sendAtcmd("AT+ESBP=5,\"SBP_LTE_MBMS_COUNTING\",1");
        sendAtcmd("AT+ESBP=5,\"SBP_LTE_MBMS_SERVICE_CONTINUITY\",1");
        sendAtcmd("AT+ESBP=5,\"SBP_LTE_MBMS_SCELL\",1");
    }

    /**
     * Used for activate or deactive eMBMS session.
     *
     * @param isActivated flag for activate eMBMS session or not.
     * @param sessionInfo the information of eMBMS session.
     *
     */
    public void activateEmbmsSession(boolean isActivated, String sessionInfo) {
        int activated = (isActivated) ? 1 : 0;
        Log.i(TAG, "activateEmbmsSession:" + isActivated + ":" + sessionInfo);
        if (isActivated) {
            sendAtcmd("AT+EMSESS=1," + sessionInfo);
        } else {
            sendAtcmd("AT+EMSESS=0," + sessionInfo);
        }
    }

    /**
     * Used for enable or disable eMBMS service.
     *
     * @param isEnabled flag for enable eMBMS service.
     */
    public void enableEmbmsSrv(boolean isEnabled) {
        int enabled = (isEnabled) ? 1 : 0;
        if (isEnabled) {
            sendAtcmd("AT+EMSEN="  + enabled);
            sendAtcmd("AT+EMSRV="  + enabled);
            sendAtcmd("AT+EMSLU="  + enabled);
            sendAtcmd("AT+EMSAIL=" + enabled);
        } else {
            sendAtcmd("AT+EMSAIL=" + enabled);
            sendAtcmd("AT+EMSLU="  + enabled);
            sendAtcmd("AT+EMSRV="  + enabled);
            sendAtcmd("AT+EMSEN="  + enabled);
        }
    }

    /**
     * Used for enable or disable eMBMS session priority.
     *
     * @param isEnabled flag for enable eMBMS esseion priority.
     *
     */
    public void enableEmbmsPriority(boolean isEnabled) {
        int enabled = (isEnabled) ? 1 : 0;
        sendAtcmd("AT+EMPRI="  + enabled);
    }

    /**
     * Used for get eMBMS service status from modem.
     *
     * @return current eMBMS service status.
     *
     */
    public boolean getEmbmsSrvStatus() {
        String rawData = sendAtcmd("AT+EMSEN?");
        Log.i(TAG, "EMSEN:" + rawData);
        if (rawData.indexOf("+EMSEN: 1") != -1) {
            return true;
        }
        return false;
    }

    /**
     * Used for get eMBMS service status from modem.
     *
     * @return current eMBMS service status.
     *
     */
    public boolean isRadioOn() {
        String rawData = sendAtcmd("AT+EFUN?");
        Log.i(TAG, "EFUN:" + rawData);
        if (rawData.indexOf("+EFUN: 1") != -1) {
            return true;
        }
        return false;
    }

    /**
     * Used for get eMBMS session priority from modem.
     *
     * @return current priority of eMBMS service.
     *
     */
    public boolean getEmbmsPriority() {
        String rawData = sendAtcmd("AT+EMPRI?");
        Log.i(TAG, "EMPRI:" + rawData);
        if (rawData.indexOf("+EMPRI: 1") != -1) {
            return true;
        }
        return false;
    }

    /**
     * Used for read current session list information.
     *
     * @return current session list from network.
     */
    public String readCurrentSessionList() {
        String rawData = sendAtcmd("AT+EMSLU?");
        return parseNwSessionList(rawData);
    }

    private String parseNwSessionList(String data) {
        StringBuffer sessions = new StringBuffer();
        String dbgString = SystemProperties.get("embms.debug.string", "");
        if (dbgString.length() != 0) {
            dbgString = dbgString.replace("|", "\n");
            data = dbgString;
        }
        String[] tmpSessions = data.split("\n");

        Log.i(TAG, "Raw data:" + data);
        Log.i(TAG, "parseNwSessionList:" + tmpSessions.length);
        for (int i = 1; i < tmpSessions.length; i++) {
            if (tmpSessions[i].indexOf("EMSLU") != -1) {
                String[] tmpSessionInfo = tmpSessions[i].split(",");
                Log.i(TAG, "tmpSessionInfo:" + tmpSessionInfo.length);
                if (tmpSessionInfo.length == 5) {
                    sessions.append(tmpSessions[i] + "\n");
                }
            }
        }
        return sessions.toString();
    }

    /**
     * Utility function for send AT command.
     *
     * @param atCmd AT command string.
     * @return the response of AT command.
     *
     */
    public synchronized String sendAtcmd(String atCmd) {
        sAtCmdRsp = "";
        sReceiveLatch = new CountDownLatch(1);

        Thread atCmdThread = new Thread(new Runnable() {
            public void run() {
                try {
                    TelephonyManager telephonyManager =
                    (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    byte[] rawData = atCmd.getBytes();
                    byte[] cmdByte = new byte[rawData.length + 1];
                    byte[] cmdResp = new byte[MAX_AT_CMD_RESPONSE];
                    System.arraycopy(rawData, 0, cmdByte, 0, rawData.length);
                    cmdByte[cmdByte.length - 1] = 0;
                    Log.i(TAG, "sendAtcmd:" + atCmd);
                    int ret = MtkTelephonyManagerEx.getDefault()
                            .invokeOemRilRequestRaw(cmdByte, cmdResp);
                    if (ret != -1) {
                        sAtCmdRsp = new String(cmdResp, 0, ret);
                    }
                    sReceiveLatch.countDown();
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }
        });

        atCmdThread.start();
        try {
            if (sReceiveLatch != null) {
                sReceiveLatch.await(5, TimeUnit.SECONDS);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return sAtCmdRsp;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.i(TAG, "key:" + key);
    }

    private PhoneStateListener getPhoneStateListener(int subId) {
        PhoneStateListener phoneStateListener = new PhoneStateListener(subId) {
            @Override
            public void onServiceStateChanged(ServiceState state) {
                Log.i(TAG, "onServiceStateChanged:" + state + ":" + mIsEmbmsAuto);
                if (state.getDataRegState() == ServiceState.STATE_IN_SERVICE && mIsEmbmsAuto) {
                    if (!getEmbmsSrvStatus()) {
                        enableEmbmsSrv(true);
                    }
                    if (state.getDataNetworkType() == TelephonyManager.NETWORK_TYPE_LTE) {
                        sendInterestedEmbmsSession();
                    }
                }
            }
        };
        return phoneStateListener;
    }

    private int getDefaultSubId() {
        int subId = SubscriptionManager.getDefaultSubscriptionId();
        if (subId == SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
            SubscriptionManager.getDefaultVoiceSubscriptionId();
        }
        Log.i(TAG, "subId:" + subId);
        return subId;
    }

    /**
     * Utility function for interested sessions.
     *
     */
    public void sendInterestedEmbmsSession() {
        ArrayList<CdsEmbmsSessionEntry> sessionList =
                                new ArrayList<CdsEmbmsSessionEntry>();
        SharedPreferences dataStore = this.getSharedPreferences(
                                CdsEmbmsConsts.EMBMS_FILE, 0);
        for (int i = 0; i < CdsEmbmsConsts.MAX_SESSION_NUM; i ++) {
            String sessionName = CdsEmbmsConsts.EMBMS_SESSION_INFO + i;
            String sessionInfo = dataStore.getString(sessionName, "");
            CdsEmbmsSessionEntry entry = new CdsEmbmsSessionEntry();
            Log.i(TAG, "prepareListData:" + i + ":" + sessionInfo);
            if (sessionInfo.length() != 0) {
                String[] sessions = sessionInfo.split(CdsEmbmsConsts.SEP);
                for (int j = 0; j < sessions.length; j++) {
                    String s = sessions[j];
                        switch (j) {
                            case 0:
                                boolean v = (("true".equals(s)) ? true : false);
                                entry.setIsEnabled(v);
                                break;
                            case 1:
                                if (s != null) {
                                    entry.setTmgi(s);
                                } else {
                                    j = sessions.length; // Exit the loop
                                }
                                break;
                            case 2:
                                if (s != null) {
                                    entry.setSessionId(s);
                                } else {
                                    j = sessions.length; // Exit the loop
                                }
                                break;
                            case 3:
                                if (s != null) {
                                    entry.setSaiList(s);
                                }
                                break;
                            case 4:
                                if (s != null) {
                                    entry.setFreqList(s);
                                }
                                break;
                            default:
                                break;
                        }
                }
                // Send interested sessions.
                if (entry.getIsEnabled()) {
                    activateEmbmsSession(true, entry.toCmdString());
                }
            }
        }
    }
}
