/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims;

import android.content.Context;

import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;

import com.android.ims.ImsReasonInfo;
import com.android.ims.ImsUt;

import com.android.ims.internal.IImsUt;
import com.android.ims.internal.IImsUtListener;

import com.mediatek.ims.MtkImsCallForwardInfo;

import com.mediatek.ims.internal.IMtkImsUt;
import com.mediatek.ims.internal.IMtkImsUtListener;

import java.util.Arrays;

public class MtkImsUt extends ImsUt {
    private static final String TAG = "MtkImsUt";

    private final IMtkImsUt miMtkUt;

    public MtkImsUt(IImsUt iUt, IMtkImsUt iMtkUt) {
        super(iUt);

        miMtkUt = iMtkUt;

        if (miMtkUt != null) {
            try {
                miMtkUt.setListener(new IMtkImsUtListenerProxy());
            } catch (RemoteException e) {
            }
        }
    }

    public void close() {
        super.close();
    }
    /**
     * A listener type for the result of the supplementary service configuration.
     */
    private class IMtkImsUtListenerProxy extends IMtkImsUtListener.Stub {
        /**
         * Notifies the status of the call forwarding in a time slot supplementary service.
         */
        @Override
        public void utConfigurationCallForwardInTimeSlotQueried(IMtkImsUt iMtkUt,
                int id, MtkImsCallForwardInfo[] cfInfo) {
            Integer key = Integer.valueOf(id);

            synchronized (mLockObj) {
                sendSuccessReport(mPendingCmds.get(key), cfInfo);
                mPendingCmds.remove(key);
            }
        }
    }

    public void queryCallForwardInTimeSlot(int condition, Message result) {
        if (DBG) {
            log("queryCallForwardInTimeSlot :: Ut = " + miMtkUt + ", condition = " + condition);
        }

        synchronized (mLockObj) {
            try {
                int id = miMtkUt.queryCallForwardInTimeSlot(condition);

                if (id < 0) {
                    sendFailureReport(result,
                            new ImsReasonInfo(ImsReasonInfo.CODE_UT_SERVICE_UNAVAILABLE, 0));
                    return;
                }

                mPendingCmds.put(Integer.valueOf(id), result);
            } catch (RemoteException e) {
                sendFailureReport(result,
                        new ImsReasonInfo(ImsReasonInfo.CODE_UT_SERVICE_UNAVAILABLE, 0));
            }
        }
    }

    public void updateCallForwardInTimeSlot(int action, int condition, String number,
            int timeSeconds, long[] timeSlot, Message result) {
        if (DBG) {
            log("updateCallForwardInTimeSlot :: Ut = " + miMtkUt + ", action = " + action
                    + ", condition = " + condition + ", number = " + number
                    + ", timeSeconds = " + timeSeconds
                    + ", timeSlot = " + Arrays.toString(timeSlot));
        }

        synchronized (mLockObj) {
            try {
                int id = miMtkUt.updateCallForwardInTimeSlot(action,
                        condition, number, timeSeconds, timeSlot);

                if (id < 0) {
                    sendFailureReport(result,
                            new ImsReasonInfo(ImsReasonInfo.CODE_UT_SERVICE_UNAVAILABLE, 0));
                    return;
                }

                mPendingCmds.put(Integer.valueOf(id), result);
            } catch (RemoteException e) {
                sendFailureReport(result,
                        new ImsReasonInfo(ImsReasonInfo.CODE_UT_SERVICE_UNAVAILABLE, 0));
            }
        }
    }

}
