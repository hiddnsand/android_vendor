package com.mediatek.op01.phone.plugin;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

/**
 * Give user tips for data or roaming is not opened.
 *
 */

public class CallForwardTipsActivity extends Activity {
    private static final String LOG_TAG = "CallForwardTipsActivity";
    private static final String EXTRA_SUB_ID = "subId";
    private static final String EXTRA_DISPALY_NAME = "displayName";
    private String diaplayName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFinishOnTouchOutside(false);
        //setContentView(R.layout.activity_call_forward_tips);

        Intent intent = getIntent();
        int subId = intent.getIntExtra(EXTRA_SUB_ID, 0);
        String displayName = intent.getStringExtra(EXTRA_DISPALY_NAME);
        String message = getMessage(subId, displayName);

        Log.d(LOG_TAG, "onCreate, message = " + message);

        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setMessage(message);
        b.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                CallForwardTipsActivity.this.finish();
            }
        });
        b.setCancelable(false);

        AlertDialog dialog = b.create();
        // make the dialog more obvious by bluring the background.
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        dialog.show();
    }

    private String getMessage(int subId, String displayName) {
        String messageText = "";
        messageText = getString(R.string.volte_ss_not_available_tips_roaming, displayName);
        return messageText;
    }

}
