/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.bandselect;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.telephony.RadioAccessFamily;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.ModemCategory;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.Elog;

import java.util.ArrayList;

public class BandSelect extends Activity implements OnClickListener {
    private static final String TAG = "EM/BandModeSelect";
    private static final int WCDMA = 1;
    private static final int TDSCDMA = 2;
    private static final String GSM_BASEBAND = "gsm.baseband.capability";
    private static final String PREF_FILE = "band_select_";
    private static final String PREF_KEYS[] = { "gsm", "umts", "lte_fdd", "lte_tdd",
            "lte_96", "lte_128", "lte_160", "lte_192",
            "lte_224", "lte_256", "cdma" };

    private static final int INDEX_GSM_BAND = 0;
    private static final int INDEX_UMTS_BAND = 1;
    private static final int INDEX_LTE_FDD_BAND = 2;
    private static final int INDEX_LTE_TDD_BAND = 3;
    private static final int INDEX_LTE_BAND_96 = 4;
    private static final int INDEX_LTE_BAND_128 = 5;
    private static final int INDEX_LTE_BAND_160 = 6;
    private static final int INDEX_LTE_BAND_192 = 7;
    private static final int INDEX_LTE_BAND_224 = 8;
    private static final int INDEX_LTE_BAND_256 = 9;
    private static final int INDEX_CDMA_BAND = 10;
    private static final int INDEX_BAND_MAX = 11;
    private static final int BAND_SET_INVALID = 1000;
    private static final int DIALOG_WAIT_BANDSET = 200;

    private Phone mPhone = null;
    private Phone mCdmaPhone = null;

    private final ArrayList<BandModeMap> mModeArray = new ArrayList<BandModeMap>();
    private final ArrayList<BandModeMap> mCdmaModeArray = new ArrayList<BandModeMap>();

    private Button mBtnSet;
    private Button mBtnReset;

    private boolean mIsThisAlive = true;
    private boolean mIsSendcmd = false;
    private static int mSimType;

    private boolean mIsCdmaValid = true;
    private boolean mIsLteValid = true;
    private boolean mIsLteExtend = false;
    private String mRfStatus = "0";
    private long mCdmaSetValue = 1;

    private long mCurrentValues[] = new long[INDEX_BAND_MAX];
    private long mCurrentCdmaValues = 0;
    private final Handler mResponseHander = new Handler() {
        private String[] mReturnData = new String[2];

        public void handleMessage(final Message msg) {
            if (!mIsThisAlive) {
                return;
            }
            AsyncResult asyncResult;

            switch (msg.what) {
            case BandModeContent.EVENT_QUERY_SUPPORTED:
                asyncResult = (AsyncResult) msg.obj;
                if (asyncResult.exception == null) {
                    setMode(asyncResult, BandModeContent.EVENT_QUERY_SUPPORTED);
                } else {
                    showDialog(BandModeContent.EVENT_QUERY_SUPPORTED);
                }
                break;
            case BandModeContent.EVENT_QUERY_CURRENT:
                asyncResult = (AsyncResult) msg.obj;
                if (asyncResult.exception == null) {
                    setMode(asyncResult, BandModeContent.EVENT_QUERY_CURRENT);
                } else {
                    showDialog(BandModeContent.EVENT_QUERY_CURRENT);
                }
                break;
            case BandModeContent.EVENT_SET:
                asyncResult = (AsyncResult) msg.obj;
                if (asyncResult.exception == null) {
                    showToast("set Gsm BandMode succeed");
                    Log.v( TAG, "set Gsm BandMode succeed");
                } else {
                    showDialog(BandModeContent.EVENT_SET_FAIL);
                    showToast("set Gsm BandMode failed");
                    Log.v( TAG, "set Gsm BandMode failed");
                }
                break;
            case BandModeContent.EVENT_QUERY_CURRENT_CDMA:
                asyncResult = (AsyncResult) msg.obj;
                if (asyncResult.exception == null) {
                    setModeCdma(asyncResult, BandModeContent.EVENT_QUERY_CURRENT_CDMA);
                } else {
                    showDialog(BandModeContent.EVENT_QUERY_CURRENT_CDMA);
                }
                break;
            case BandModeContent.EVENT_SET_CDMA:
                asyncResult = (AsyncResult) msg.obj;

                if (asyncResult.exception == null) {
                    showToast("set CDMA BandMode succeed");
                    Log.v( TAG, "set CDMA BandMode succeed");
                } else {
                    showDialog(BandModeContent.EVENT_SET_FAIL);
                    showToast("set CDMA BandMode failed");
                    Log.v( TAG, "set CDMA BandMode failed");
                }

                if (FeatureSupport.is93Modem()) {
                    String[] cmd = new String[2];
                    cmd[0] = BandModeContent.SET_RF_STATUS + mRfStatus;
                    cmd[1] = "";
                    sendATCommandCdma(cmd, BandModeContent.EVENT_TURN_ON_RF);
                }
                break;
            case BandModeContent.EVENT_GET_RF_STATUS:
                asyncResult = (AsyncResult) msg.obj;
                if (asyncResult.exception == null) {
                    mReturnData = (String[]) asyncResult.result;
                    if (mReturnData != null && mReturnData.length > 0) {
                        try {
                            mRfStatus = mReturnData[0].split(": ")[1];
                            Log.v(TAG, "get RF status succeed");
                            Log.v( TAG, "RF status is: " + mRfStatus);
                        } catch (Exception e) {
                            mIsSendcmd = false;
                            Log.v(TAG, "get RF status failed 0");
                            showToast("get RF status failed 0");
                            return;
                        }
                        String[] cmd = new String[2];
                        cmd[0] = BandModeContent.SET_RF_STATUS + "0";
                        cmd[1] = "";
                        sendATCommandCdma(cmd, BandModeContent.EVENT_TURN_OFF_RF);
                    }
                    else {
                        Log.v(TAG, "get RF status failed 1");
                        showToast("get RF status failed 1");
                        mIsSendcmd = false;
                        setButtonEnableStatus(true);
                    }

                } else {
                    showToast("get RF status failed 2");
                    Log.v(TAG, "get RF status failed 2");
                    mIsSendcmd = false;
                    setButtonEnableStatus(true);
                }
                break;
            case BandModeContent.EVENT_TURN_OFF_RF:
                asyncResult = (AsyncResult) msg.obj;
                if (asyncResult.exception == null) {
                    Log.v(TAG, "turn off RF succeed");
                } else {
                    showToast("turn off RF failed");
                    Log.v(TAG, "turn off RF failed");
                }
                String[] cmd = new String[2];
                cmd[0] = BandModeContent.SET_COMMAND_CDMA + mCdmaSetValue;
                cmd[1] = "";
                sendATCommandCdma(cmd, BandModeContent.EVENT_SET_CDMA);
                break;
            case BandModeContent.EVENT_TURN_ON_RF:
                asyncResult = (AsyncResult) msg.obj;
                if (asyncResult.exception == null) {
                    Log.v(TAG, "turn on RF succeed");
                } else {
                    showToast("turn on RF failed");
                    Log.v(TAG, "turn on RF failed");
                }
                mIsSendcmd = false;
                setButtonEnableStatus(true);
                break;
            default:
                break;
            }
        }
    };

    private void showToast(String type) {
        Toast.makeText(this, type, Toast.LENGTH_SHORT).show();
    }

    private static class BandModeMap {
        public CheckBox mChkBox;
        public int mIndex;
        public int mBit;

        /**
         * @param chkbox
         *            the CheckBox from the layout
         * @param index
         *            the integer of the modem value (index)
         * @param bit
         *            the integer of the modem value (bit offset)
         */
        BandModeMap(final CheckBox chkbox, final int index, final int bit) {
            mChkBox = chkbox;
            mIndex = index;
            mBit = bit;
        }
    }

    private ArrayList<CheckBox> addCheckboxToTable(int tableResId, int arrayResId) {
        TableLayout table = (TableLayout) findViewById(tableResId);
        String[] labels = getResources().getStringArray(arrayResId);
        ArrayList<CheckBox> ret = new ArrayList<CheckBox>(labels.length);

        for (int i = 0; i < labels.length; i++) {
            TableRow row = new TableRow(this);
            CheckBox checkBox = new CheckBox(this);
            checkBox.setText(labels[i]);
            row.addView(checkBox);
            table.addView(row);
            ret.add(checkBox);
        }

        return ret;
    }

    private void initGsmArray() {
        findViewById(R.id.TableLayout_GSM).setVisibility(View.VISIBLE);
        ArrayList<CheckBox> checkBox = addCheckboxToTable(R.id.TableLayout_GSM,
                R.array.band_mode_gsm);
        for (int i = 0; i < checkBox.size(); i++) {
            mModeArray.add(new BandModeMap(checkBox.get(i), INDEX_GSM_BAND,
                    BandModeContent.GSM_BAND_BIT[i]));
        }
    }

    private void initWcdmaArray() {
        findViewById(R.id.TableLayout_UTMS).setVisibility(View.VISIBLE);
        ArrayList<CheckBox> checkBox = addCheckboxToTable(R.id.TableLayout_UTMS,
                R.array.band_mode_wcdma);
        for (int i = 0; i < checkBox.size(); i++) {
            mModeArray.add(new BandModeMap(checkBox.get(i), INDEX_UMTS_BAND, i));
        }
    }

    private void initTdscdmaArray() {
        findViewById(R.id.TableLayout_UTMS).setVisibility(View.VISIBLE);
        ArrayList<CheckBox> checkBox = addCheckboxToTable(R.id.TableLayout_UTMS,
                R.array.band_mode_tdscdma);
        for (int i = 0; i < checkBox.size(); i++) {
            mModeArray.add(new BandModeMap(checkBox.get(i), INDEX_UMTS_BAND, i));
        }
    }

    private void initCdmaArray() {
        findViewById(R.id.TableLayout_CDMA).setVisibility(View.VISIBLE);
        ArrayList<CheckBox> checkBox = addCheckboxToTable(R.id.TableLayout_CDMA,
                R.array.band_mode_cdma);
        for (int i = 0; i < checkBox.size(); i++) {
            mCdmaModeArray.add(new BandModeMap(checkBox.get(i), INDEX_CDMA_BAND, i));
        }
    }

    private void initLteArray() {
        findViewById(R.id.TableLayout_LTE).setVisibility(View.VISIBLE);
        ArrayList<CheckBox> checkBox =
                addCheckboxToTable(R.id.TableLayout_LTE, R.array.band_mode_lte_fdd);
        for (int i = 0; i < checkBox.size(); i++) {
            mModeArray.add(new BandModeMap(checkBox.get(i), INDEX_LTE_FDD_BAND, i));
        }
        checkBox = addCheckboxToTable(R.id.TableLayout_LTE, R.array.band_mode_lte_tdd);
        for (int i = 0; i < checkBox.size(); i++) {
            mModeArray.add(new BandModeMap(checkBox.get(i), INDEX_LTE_TDD_BAND, i));
        }
        checkBox = addCheckboxToTable(R.id.TableLayout_LTE, R.array.band_mode_lte_96);
        for (int i = 0; i < checkBox.size(); i++) {
            mModeArray.add(new BandModeMap(checkBox.get(i), INDEX_LTE_BAND_96, i));
        }
        checkBox = addCheckboxToTable(R.id.TableLayout_LTE, R.array.band_mode_lte_128);
        for (int i = 0; i < checkBox.size(); i++) {
            mModeArray.add(new BandModeMap(checkBox.get(i), INDEX_LTE_BAND_128, i));
        }
        checkBox = addCheckboxToTable(R.id.TableLayout_LTE, R.array.band_mode_lte_160);
        for (int i = 0; i < checkBox.size(); i++) {
            mModeArray.add(new BandModeMap(checkBox.get(i), INDEX_LTE_BAND_160, i));
        }
        checkBox = addCheckboxToTable(R.id.TableLayout_LTE, R.array.band_mode_lte_192);
        for (int i = 0; i < checkBox.size(); i++) {
            mModeArray.add(new BandModeMap(checkBox.get(i), INDEX_LTE_BAND_192, i));
        }
        checkBox = addCheckboxToTable(R.id.TableLayout_LTE, R.array.band_mode_lte_224);
        for (int i = 0; i < checkBox.size(); i++) {
            mModeArray.add(new BandModeMap(checkBox.get(i), INDEX_LTE_BAND_224, i));
        }
        checkBox = addCheckboxToTable(R.id.TableLayout_LTE, R.array.band_mode_lte_256);
        for (int i = 0; i < checkBox.size(); i++) {
            mModeArray.add(new BandModeMap(checkBox.get(i), INDEX_LTE_BAND_256, i));
        }
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        mSimType = intent.getIntExtra("mSimType", PhoneConstants.SIM_ID_1);
        int modemType = ModemCategory.getModemType();
        Log.v( TAG, "modemType = " + modemType);

        setContentView(R.layout.bandmodeselect);
        mBtnSet = (Button) findViewById(R.id.BandSel_Btn_Set);
        mBtnReset = (Button) findViewById(R.id.BandSel_Btn_Reset);
        initGsmArray();
        if (modemType == TDSCDMA && ModemCategory.isCapabilitySim(mSimType)) {
            initTdscdmaArray();
            if (FeatureSupport.isSupported(FeatureSupport.FK_LTE_SUPPORT)) {
                initLteArray();
            }
        } else if (modemType == WCDMA && ModemCategory.isCapabilitySim(mSimType)) {
            initWcdmaArray();
            if (FeatureSupport.isSupported(FeatureSupport.FK_LTE_SUPPORT)) {
                initLteArray();
            }
        } else if (!(ModemCategory.isCapabilitySim(mSimType))) {
            if (ModemCategory.checkViceSimCapability(mSimType, RadioAccessFamily.RAF_UMTS)) {
                initWcdmaArray();
            }
            if (ModemCategory.checkViceSimCapability(mSimType, RadioAccessFamily.RAF_LTE)) {
                if (FeatureSupport.isSupported(FeatureSupport.FK_LTE_SUPPORT)) {
                    initLteArray();
                }
            }
        }

        if (ModemCategory.isCdma() &&
                !FeatureSupport.is90Modem() &&
                   (mSimType == PhoneConstants.SIM_ID_1) ) {
            if (!("1".equals(FeatureSupport.isSupportC2kLte()))
                    && !("2".equals(FeatureSupport.isSupportC2kLte()))) {
                findViewById(R.id.TableLayout01).setVisibility(View.GONE);
            }
            initCdmaArray();
        }

        mBtnSet.setOnClickListener(this);
        mBtnReset.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (TelephonyManager.getDefault().getPhoneCount() > 1) {
            Log.v( TAG, "Gemini");
            mPhone = PhoneFactory.getPhone(mSimType);
        } else {
            Log.v( TAG, "Single");
            mPhone = PhoneFactory.getDefaultPhone();
        }

        if (ModemCategory.isCdma() &&
                !FeatureSupport.is90Modem() &&
                  (mSimType == PhoneConstants.SIM_ID_1) ) {
            mCdmaPhone = ModemCategory.getCdmaPhone();
            queryCurrentCdmaMode();
        }
        querySupportMode();
        queryCurrentMode();
    }

    private void setMode(AsyncResult aSyncResult, int msg) {
        final String[] result = (String[]) aSyncResult.result;

        for (final String value : result) {
            Log.v( TAG, "gsm --.>" + value);
            final String splitString = value.substring(BandModeContent.SAME_COMMAND
                    .length());
            final String[] getDigitalVal = splitString.split(",");

            if (getDigitalVal != null && getDigitalVal.length > 1) {
                long[] values = new long[INDEX_BAND_MAX];
                for (int i = 0; i < values.length; i++) {
                    if (getDigitalVal.length <= i || getDigitalVal[i] == null) {
                        values[i] = 0;
                        continue;
                    }
                    try {
                        values[i] = Long.valueOf(getDigitalVal[i].trim());
                    } catch (NumberFormatException e) {
                        values[i] = 0;
                    }
                }
                if (msg == BandModeContent.EVENT_QUERY_SUPPORTED) {
                    setSupportedMode(values);
                    if (getDigitalVal.length > 5) {
                        Log.v("@M_" + TAG, "The Modem support Lte extend band");
                        mIsLteExtend = true;
                    }
                    else {
                        Log.v( TAG, "The Modem not support Lte extend band");
                        mIsLteExtend = false;
                    }
                } else {
                    setCurrentMode(values);
                    mCurrentValues = getValFromBox(false);
                    saveDefaultValueIfNeed(values);
                }
            }
        }
    }

    /**
     * Query Modem supported band modes.
     */
    private void querySupportMode() {
        final String[] modeString = { BandModeContent.QUERY_SUPPORT_COMMAND,
                BandModeContent.SAME_COMMAND };
        Log.v( TAG, "querySupportGsmMode AT String:" + modeString[0]);
        sendATCommand(modeString, BandModeContent.EVENT_QUERY_SUPPORTED);
    }

    /**
     * Query Modem is being used band modes.
     */
    private void queryCurrentMode() {
        final String[] modeString = { BandModeContent.QUERY_CURRENT_COMMAND,
                BandModeContent.SAME_COMMAND };
        Log.v( TAG, "queryCurrentGSMMode AT String:" + modeString[0]);
        sendATCommand(modeString, BandModeContent.EVENT_QUERY_CURRENT);
    }

    /**
     * Query Modem is being used band modes.
     */
    private void sendATCommand(String[] atCommand, int msg) {
        if (mPhone != null) {
            mPhone.invokeOemRilRequestStrings(atCommand, mResponseHander.obtainMessage(msg));
        }
    }

    /**
     * Set the selected modes.
     *
     * @param values
     *            the integers of mode values
     * @return false means set failed or success
     */
    private void setBandMode(final long[] values) {
        if (values[0] > BandModeContent.GSM_MAX_VALUE
                || values[1] > BandModeContent.UMTS_MAX_VALUE
                || values[2] > BandModeContent.LTE_MAX_VALUE
                || values[3] > BandModeContent.LTE_MAX_VALUE) {
            return;
        }

        String[] modeString = {
                BandModeContent.SET_COMMAND + values[0] + "," + values[1], "" };
        if (FeatureSupport.isSupported(FeatureSupport.FK_LTE_SUPPORT)) {
            modeString[0] += "," + values[2] + "," + values[3];
            if (mIsLteExtend) {
                for (int i = 4; i < INDEX_BAND_MAX - 1; i++) {
                    modeString[0] += "," + values[i];
                }
            }
        }
        Log.v( TAG, "setGsmBandMode AT String:" + modeString[0]);
        sendATCommand(modeString, BandModeContent.EVENT_SET);

    }

    /**
     * Get the selected mode values.
     *
     * @return values from the selected boxes
     */
    private long[] getValFromBox(boolean judge) {
        long[] values = new long[INDEX_BAND_MAX];
        long[] values_temp = new long[INDEX_BAND_MAX];
        for (final BandModeMap m : mModeArray) {
            if (m.mChkBox.isChecked()) {
                values[m.mIndex] |= 1L << m.mBit;
                values_temp[m.mIndex] |= 1L << m.mBit;
            }
        }

        if (judge) {
            // band64 to band256 belongs to lte fdd, so check null together
            for (int i = INDEX_LTE_BAND_96; i <= INDEX_LTE_BAND_256; i++) {
                values_temp[INDEX_LTE_FDD_BAND] = values_temp[INDEX_LTE_FDD_BAND]
                        | values_temp[i];
            }
            // LTE only needs to check gsm, umts, lte fdd, lte tdd
            for (int i = 0; i <= INDEX_LTE_TDD_BAND; i++) {
                Log.v(TAG, "mCurrentValues[" + i + "] = " + mCurrentValues[i]
                        + ", to values[" + i + "] = " + values_temp[i]);
                if ((values_temp[i] == 0) && (mCurrentValues[i] != 0)) {
                    mIsLteValid = false;
                    break;
                } else if (!mIsLteValid) {
                    mIsLteValid = true;
                }
            }

            // null select is not allowed.
            if (values[0] == 0) {
                values[0] = BandModeContent.GSM_MAX_VALUE;
            }
            if (values[1] == 0) {
                values[1] = BandModeContent.UMTS_MAX_VALUE;
            }
            if (values_temp[2] == 0 && values_temp[3] == 0) {
                values[2] = BandModeContent.LTE_MAX_VALUE;
                values[3] = BandModeContent.LTE_MAX_VALUE;
                Log.v( TAG, "lte not to null");
            }
        }
        return values;
    }

    private long getValFromBoxCdma() {
        long value = 0;
        for (final BandModeMap m : mCdmaModeArray) {
            if (m.mChkBox.isChecked()) {
                value |= 1L << m.mBit;
            }
            if (!mIsCdmaValid) {
                mIsCdmaValid = true;
            }
        }
        if ((value == 0) && (mCurrentCdmaValues != 0)) {
            mIsCdmaValid = false;
            Log.v( TAG, "mIsCdmaValid:" + mIsCdmaValid);
        }
        return value;
    }

    /**
     * @param values
     *            the integer values from the modem
     */
    private void setCurrentMode(final long[] values) {
        for (final BandModeMap m : mModeArray) {
            if ((values[m.mIndex] & (1L << m.mBit)) == 0) {
                m.mChkBox.setChecked(false);
            } else {
                if (m.mChkBox.isEnabled()) {
                    m.mChkBox.setChecked(true);
                }
            }
        }
    }

    /**
     * @param values
     *            the integer values from the modem
     */
    private void setSupportedMode(final long[] values) {
        for (final BandModeMap m : mModeArray) {
            if ((values[m.mIndex] & (1L << m.mBit)) == 0) {
                m.mChkBox.setEnabled(false);
            } else {
                m.mChkBox.setEnabled(true);
            }
        }
    }

    private void setCurrentModeCdma(final long value) {
        for (final BandModeMap m : mCdmaModeArray) {
            if ((value & (1L << m.mBit)) == 0) {
                m.mChkBox.setChecked(false);
            } else {
                if (m.mChkBox.isEnabled()) {
                    m.mChkBox.setChecked(true);
                }
            }
        }
    }

    private void setSupportedModeCdma(final long value) {
        for (final BandModeMap m : mCdmaModeArray) {
            if ((value & (1L << m.mBit)) == 0) {
                m.mChkBox.setEnabled(false);
            } else {
                m.mChkBox.setEnabled(true);
            }
        }
    }

    /**
     * Button on click listener.
     *
     * @param arg0
     *            the view of the button event
     */
    public void onClick(final View arg0) {
        if (arg0.getId() == mBtnSet.getId()) {
            long lteValues[] = new long[INDEX_BAND_MAX];
            long cdmaValues = 0;
            if(!FeatureSupport.is90Modem()){
                cdmaValues = getValFromBoxCdma();
            }
            lteValues = getValFromBox(true);
            if (!mIsLteValid || !mIsCdmaValid) {
                Elog.d(TAG, "!mIsLteValid || !mIsCdmaValid");
                showDialog(BAND_SET_INVALID);
                return;
            }
            if (mSimType == PhoneConstants.SIM_ID_1 &&
                             ModemCategory.isCdma() &&
                    !FeatureSupport.is90Modem() ) {
                setBandModeCdma(cdmaValues);
            }
            setBandMode(lteValues);
        } else if (arg0.getId() == mBtnReset.getId()) {
            showDialog(BandModeContent.EVENT_RESET);
        }
    }

    @Override
    public void onDestroy() {
        Log.v( TAG, "onDestroy");
        mIsThisAlive = false;
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Log.v( TAG, "onBackPressed, mIsSendcmd = " + mIsSendcmd);
        if(mIsSendcmd == true){
            showDialog(DIALOG_WAIT_BANDSET);
        }
        else{
            mIsThisAlive = false;
            BandSelect.this.finish();
        }
    }


    private void queryCurrentCdmaMode() {
        String[] cmd = new String[3];
        cmd[0] = BandModeContent.QUERY_CURRENT_COMMAND_CDMA;
        cmd[1] = BandModeContent.SAME_COMMAND_CDMA;
        cmd[2] = "DESTRILD:C2K";
        String[] cmd_s = ModemCategory.getCdmaCmdArr(cmd);
        Log.v( TAG, "queryCurrentCdmaMode: ");
        sendATCommandCdma(cmd_s, BandModeContent.EVENT_QUERY_CURRENT_CDMA);
    }

    private void setModeCdma(AsyncResult aSyncResult, int msg) {
        final String[] result = (String[]) aSyncResult.result;
        for (final String string : result) {
            Log.v( TAG, "cdma --.>" + string);
            String splitString = string.substring(BandModeContent.SAME_COMMAND_CDMA.length());
            if (msg == BandModeContent.EVENT_QUERY_CURRENT_CDMA) {
                final String[] getDigitalVal = splitString.split(",");
                long[] value = new long[2];
                try {
                    for (int i = 0; i < 2; i++) {
                        if (getDigitalVal[i] != null) {
                            value[i] = Integer.parseInt(getDigitalVal[i].substring(2), 16);
                        }
                    }
                } catch (Exception e) {
                    value[0] = 0;
                }
                setSupportedModeCdma(value[0]);
                setCurrentModeCdma(value[1]);
                mCurrentCdmaValues = value[1];
                long valueCur = (value[1] == 0) ? value[0] : value[1];
                saveDefaultValueIfNeedCdma(valueCur);
            }
        }
    }

    private void sendATCommandCdma(String[] atCommand, int msg) {
        if (mCdmaPhone != null) {
            Log.v(TAG, "c2k AT String:" + atCommand[0] + ",atCommand.size = "
                    + atCommand.length);
            mCdmaPhone.invokeOemRilRequestStrings(atCommand,
                    mResponseHander.obtainMessage(msg));
        }
    }
    private void setButtonEnableStatus(boolean enabled) {
        mBtnSet.setEnabled(enabled);
        mBtnReset .setEnabled(enabled);
    }

    private void setBandModeCdma(final long value) {
        Log.v( TAG, "setCdmaBandMode: ");
        if (!mIsCdmaValid && !mIsLteValid) {
            return;
        }
        if (FeatureSupport.is93Modem()) {
            mIsSendcmd = true;
            mCdmaSetValue = value;
            String[] cmd = new String[2];
            cmd[0] = BandModeContent.GET_RF_STATUS;
            cmd[1] = BandModeContent.GET_RF_STATUS_RES;
            setButtonEnableStatus(false);
            sendATCommandCdma(cmd, BandModeContent.EVENT_GET_RF_STATUS);
        } else {
            String[] cmd = new String[3];
            cmd[0] = BandModeContent.SET_COMMAND_CDMA + value;
            cmd[1] = "";
            cmd[2] = "DESTRILD:C2K";
            sendATCommandCdma(cmd, BandModeContent.EVENT_SET_CDMA);
        }
    }

    /**
     * Get default mode values.
     *
     * @return saved default values
     */
    private long[] getDefaultValue() {
        SharedPreferences pref = getSharedPreferences(PREF_FILE + mSimType, MODE_PRIVATE);
        long[] values = new long[INDEX_BAND_MAX];
        long[] values_temp = new long[INDEX_BAND_MAX];
        Log.v( TAG, "getDefaultValueGsm: ");
        for (int i = 0; i < INDEX_BAND_MAX - 1; i++) {
            values[i] = pref.getLong(PREF_KEYS[i], 0);
            values_temp[i] = pref.getLong(PREF_KEYS[i], 0);
            Log.v( TAG, "values[" + i + "] = " + values[i]);
        }

        for (int i = INDEX_LTE_BAND_96; i <= INDEX_LTE_BAND_256; i++) {
            values_temp[INDEX_LTE_FDD_BAND] = values_temp[INDEX_LTE_FDD_BAND]
                    | values_temp[i];
        }

        // null select is not allowed.
        if (values[0] == 0) {
            values[0] = BandModeContent.GSM_MAX_VALUE;
        }
        if (values[1] == 0) {
            values[1] = BandModeContent.UMTS_MAX_VALUE;
        }
        if (values_temp[2] == 0 && values_temp[3] == 0) {
            values[2] = BandModeContent.LTE_MAX_VALUE;
            values[3] = BandModeContent.LTE_MAX_VALUE;
            Log.v( TAG, "getDefaultValue,lte not to null");
        }
        setCurrentMode(values);
        return values;
    }

    /**
     * Save default mode values to preference if not exist.
     *
     * @param values
     *            the integer values from the modem
     */
    private void saveDefaultValueIfNeed(long[] values) {
        SharedPreferences pref = getSharedPreferences(PREF_FILE + mSimType, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        for (int i = 0; i < INDEX_BAND_MAX - 1; i++) {
            if (!pref.contains(PREF_KEYS[i])) {
                editor.putLong(PREF_KEYS[i], values[i]);
                Log.v( TAG, "save gsm default values[" + i + "] = " + values[i]);
            }
        }
        editor.commit();
    }

    private long getDefaultValueCdma() {
        SharedPreferences pref = getSharedPreferences(PREF_FILE + mSimType, MODE_PRIVATE);
        long value = pref.getLong(PREF_KEYS[INDEX_CDMA_BAND], 0);
        Log.v( TAG, "getDefaultValueCdma: " + value);
        setCurrentModeCdma(value);
        return value;
    }

    private void saveDefaultValueIfNeedCdma(long value) {
        SharedPreferences pref = getSharedPreferences(PREF_FILE + mSimType, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        if (!pref.contains(PREF_KEYS[INDEX_CDMA_BAND])) {
            editor.putLong(PREF_KEYS[INDEX_CDMA_BAND], value);
            Log.v( TAG, "save cdam default value: " + value);
        }
        editor.commit();
    }

    @Override
    protected Dialog onCreateDialog(final int dialogId) {
        if (BandModeContent.EVENT_QUERY_SUPPORTED == dialogId) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    BandSelect.this);
            builder.setTitle(R.string.query_result);
            builder.setMessage(R.string.query_support_message);
            builder.setPositiveButton(R.string.query_btn_text, null);
            builder.create().show();
        } else if (BandModeContent.EVENT_QUERY_CURRENT == dialogId) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    BandSelect.this);
            builder.setTitle(R.string.query_result);
            builder.setMessage(R.string.query_current_message);
            builder.setPositiveButton(R.string.query_btn_text, null);
            builder.create().show();
        } else if (BandModeContent.EVENT_RESET == dialogId) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    BandSelect.this);
            builder.setTitle(R.string.reset_title);
            builder.setMessage(R.string.reset_message);
            builder.setPositiveButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (mSimType == PhoneConstants.SIM_ID_1 &&
                                    ModemCategory.isCdma() &&
                                    !FeatureSupport.is90Modem()) {
                                setBandModeCdma(getDefaultValueCdma());
                            }
                            setBandMode(getDefaultValue());
                            dialog.dismiss();
                        }
                    }
                    );
            builder.setNegativeButton(android.R.string.cancel, null);
            builder.create().show();
        } else if (BandModeContent.EVENT_SET_OK == dialogId) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    BandSelect.this);
            builder.setTitle(R.string.set_success_title);
            builder.setMessage(R.string.set_success_message);
            builder.setPositiveButton(R.string.set_ok_text, null);
            builder.create().show();
        } else if (BandModeContent.EVENT_QUERY_CURRENT_CDMA == dialogId) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    BandSelect.this);
            builder.setTitle(R.string.query_result);
            if (!ModemCategory.isCTcard(mSimType)) {
                builder.setMessage(R.string.query_cdma_current_message_check);
            } else {
                builder.setMessage(R.string.query_cdma_current_message);
            }
            builder.setPositiveButton(R.string.query_btn_text, null);
            builder.create().show();
        } else if (BAND_SET_INVALID == dialogId) {
            Elog.d(TAG, "band invalid dialog");
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    BandSelect.this);
            builder.setTitle(R.string.set_fail_title);
            builder.setMessage("The band is changed from not null to null, it is illegal!");
            builder.setPositiveButton(R.string.set_fail_text, null);
            builder.create().show();
        } else if(DIALOG_WAIT_BANDSET == dialogId){
            Elog.d(TAG, "band set not finish dialog");
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    BandSelect.this);
            builder.setTitle(R.string.wait_finish);
            builder.setMessage("The band set not finish,please wait for set finish and try again");
            builder.setPositiveButton(R.string.set_fail_text, null);
            builder.create().show();
        }
        else {
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    BandSelect.this);
            builder.setTitle(R.string.set_fail_title);
            builder.setMessage(R.string.set_fail_message);
            builder.setPositiveButton(R.string.set_fail_text, null);
            builder.create().show();
        }
        return super.onCreateDialog(dialogId);
    }
}
