ifneq ($(MTK_EMULATOR_SUPPORT), yes)
$(info A3M-ENGINE ANDROID.MK)
#
# Used by NDK builds and by makeMtk builds.
#
#

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE     := libja3m
LOCAL_MULTILIB   := both
LOCAL_MODULE_TAGS := optional

TARGET_PLATFORM  := android-9

LOCAL_CFLAGS     := -Wno-multichar -Wno-missing-field-initializers

LOCAL_MODULE_CLASS := SHARED_LIBRARIES


#A3M_OUT_PATH:= $(call local-intermediates-dir)
A3M_OUT_PATH:= $(local-generated-sources-dir)

# Generate source files containing built-in memory files
$(shell python $(LOCAL_PATH)/../buildtools/genmemfiles.py \
    -n a3m -i $(LOCAL_PATH)/assets -o $(A3M_OUT_PATH)/assets)
$(shell python $(LOCAL_PATH)/../buildtools/genmemfiles.py \
    -n ngin3d -i $(LOCAL_PATH)/assets_ngin3d -o $(A3M_OUT_PATH)/assets_ngin3d)

# Generate build information
$(shell python $(LOCAL_PATH)/../buildtools/genbuildinfo.py \
    -d $(LOCAL_PATH)/common/src)

A3M_ASSETS := $(A3M_OUT_PATH)/assets/src/memfilesource_a3m.cpp
NGIN3D_ASSETS := $(A3M_OUT_PATH)/assets_ngin3d/src/memfilesource_ngin3d.cpp

LOCAL_GENERATED_SOURCES += $(A3M_ASSETS)
LOCAL_GENERATED_SOURCES += $(NGIN3D_ASSETS)

LOCAL_SRC_FILES  := common/src/info.cpp \
                    common/src/version.cpp \
                    facility/src/appearance.cpp \
                    facility/src/assetpath.cpp \
                    facility/src/assetcachepool.cpp \
                    facility/src/background.cpp \
                    facility/src/colour.cpp \
                    facility/src/digestmaker.cpp \
                    facility/src/error.cpp \
                    facility/src/extensions.cpp \
                    facility/src/fileutility.cpp \
                    facility/src/font.cpp \
                    facility/src/fontloader.cpp \
                    facility/src/image.cpp \
                    facility/src/indexbuffer.cpp \
                    facility/src/memorystream.cpp \
                    facility/src/mesh.cpp \
                    facility/src/meshutility.cpp \
                    facility/src/properties.cpp \
                    facility/src/pvrtextureloader.cpp \
                    facility/src/rendercontext.cpp \
                    facility/src/renderdevice.cpp \
                    facility/src/rendertarget.cpp \
                    facility/src/resource.cpp \
                    facility/src/resourcecache.cpp \
                    facility/src/shaderprogram.cpp \
                    facility/src/shaderprogramloader.cpp \
                    facility/src/shaderuniform.cpp \
                    facility/src/stdstringutility.cpp \
                    facility/src/texture2dloader.cpp \
                    facility/src/texture.cpp \
                    facility/src/texturecubeloader.cpp \
                    facility/src/vertexbuffer.cpp \
                    render/src/animation.cpp \
                    render/src/camera.cpp \
                    render/src/glofile.cpp \
                    render/src/light.cpp \
                    render/src/motionblurrenderer.cpp \
                    render/src/renderblock.cpp \
                    render/src/renderblockgroup.cpp \
                    render/src/scenenode.cpp \
                    render/src/sceneutility.cpp \
                    render/src/simplerenderer.cpp \
                    render/src/solid.cpp \
                    maths/src/fpmaths.cpp \
                    maths/src/mathsstring.cpp \
                    collision/src/plane.cpp \
                    collision/src/ray.cpp \
                    collision/src/shape.cpp \
                    collision/src/sphere.cpp \
                    collision/src/square.cpp \
                    pss/src/android/log_android.cpp \
                    pss/src/android/stream_android.cpp \
                    pss/src/android/filestream_android.cpp \
                    pss/src/android/assetmgrstream_android.cpp \
                    external/src/md5/md5.c \
                    external/src/stb/stb_image.c \
                    external/src/stb/stb_truetype.c \
                    external/src/pvr/PVRTDecompress.cpp \
                    external/src/pvr/PVRTgles2Ext.cpp \
                    external/src/pvr/PVRTTextureAPI.cpp \
                    external/src/a3me/a3me_drvb.cpp \
                    ../jni/jni/a3m_wrap.cpp \
                    ../jni/a3mgetlightzorder.cpp \
                    ../jni/resourcestream.cpp \
                    ../jni/utility.cpp

LOCAL_C_INCLUDES := $(JNI_H_INCLUDE) \
                    $(LOCAL_PATH) \
                    $(LOCAL_PATH)/external/api \
                    $(LOCAL_PATH)/collision/api \
                    $(LOCAL_PATH)/common/api \
                    $(LOCAL_PATH)/common/src \
                    $(LOCAL_PATH)/pss/api \
                    $(LOCAL_PATH)/pss/src/android \
                    $(LOCAL_PATH)/facility/api \
                    $(LOCAL_PATH)/facility/src \
                    $(LOCAL_PATH)/maths/api \
                    $(LOCAL_PATH)/render/api \
                    $(A3M_OUT_PATH)/assets/src \
                    $(A3M_OUT_PATH)/assets_ngin3d/src \
                    external/skia/include \
                    external/skia/include/core \
                    frameworks/base/core/jni/android/graphics \
                    frameworks/base/core/jni \
                    $(LOCAL_PATH)/../jni

# This line required for NDK build.  Note no stlport
LOCAL_LDLIBS := -lEGL \
                -lGLESv2 \
                -landroid \
                -lcutils \
                -ljnigraphics \
                -llog

LOCAL_PRELINK_MODULE := false

# This line required for makeMtk build.
LOCAL_SHARED_LIBRARIES := libEGL \
                          libGLESv2 \
                          libandroid \
                          libandroid_runtime \
                          libcutils \
                          liblog \
                          libutils \
                          libui \
                          libskia

# Disable __BIONIC_FORTIFY_INLINE to maintain compatibility with JB
LOCAL_CFLAGS += -U_FORTIFY_SOURCE

LOCAL_CFLAGS += -DGL_GLEXT_PROTOTYPES -DEGL_EGLEXT_PROTOTYPES

include $(BUILD_SHARED_LIBRARY)
endif
