/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2011 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * ShaderProgram Implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/shaderprogram.h>   /* API for ShaderProgram class */
#include <a3m/rendercontext.h>   /* for RenderContext       */
#include <a3m/vertexbuffer.h>    /* for VertexBuffer */
#include <error.h>               /* for CHECK_GL_ERROR */
//lint -e(537) a3m_assert.h included indirectly, but not relying on that
#include <a3m/assert.h>          /* for A3M_COMPILE_ASSERT */
#include <a3m/log.h>             /* A3M log API */
#include <a3m/platform.h>        /* for A3M_PLATFORM */
#include <extensions.h>          /* for OES extension */
#include <algorithm>             /* std::find_if */

namespace
{
  A3M_INT32 const NO_TEX_UNIT = -1;

  /* Converts an unsigned 32-bit integer to an ASCII string.  Only
   * base-10 conversions are performed. */
  void uInt32ToAscii( A3M_UINT32 value, A3M_CHAR8* result )
  {
    const A3M_UINT32 BASE = 10;

    A3M_CHAR8* out = result;
    A3M_UINT32 quotient = value;

    do
    {
      const A3M_UINT32 tmp = quotient / BASE;
      *out++ = "0123456789"[ quotient - ( tmp * BASE ) ];
      quotient = tmp;
    }
    while ( quotient );

    std::reverse( result, out );
    *out = '\0';
  }

  /*
   * Log the cause of failure to compile a shader.
   */
  void logShaderError( GLuint shader )
  {
    GLint infoLen = 0;
    glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &infoLen );

    if( infoLen > 1 )
    {
      /* Find out the length of the log message, then allocate a buffer of that
      * length and fill it with the message */
      A3M_CHAR8* infoLog = new A3M_CHAR8[ static_cast< size_t >( infoLen ) ];
      glGetShaderInfoLog( shader, infoLen, NULL, infoLog );
      A3M_LOG_ERROR( "Failed to compile shader:\n%s\n", infoLog );
      delete [] infoLog;
    }
  }

  /*
   * Log the cause of failure to link a shader program.
   */
  void logProgramError( GLuint program )
  {
    GLint infoLen = 0;
    glGetProgramiv( program, GL_INFO_LOG_LENGTH, &infoLen );

    if( infoLen > 1 )
    {
      /* Find out the length of the log message, then allocate a buffer of that
      * length and fill it with the message */
      A3M_CHAR8* infoLog = new A3M_CHAR8[ static_cast< size_t >( infoLen ) ];
      glGetProgramInfoLog( program, infoLen, NULL, infoLog );
      A3M_LOG_ERROR( "Failed to link shader program:\n%s\n", infoLog );
      delete [] infoLog;
    }
  }

  /*
   * Compile the given shader source and return a shader object.
   * Any failures are recorded in the log.
   */
  A3M_BOOL compileShader( a3m::detail::ShaderResource& shader,
                          const A3M_CHAR8* source )
  {
    glShaderSource( shader.getId(), 1, &source, 0 );
    CHECK_GL_ERROR;

    glCompileShader( shader.getId() );
    CHECK_GL_ERROR;

    GLint compiled;
    glGetShaderiv( shader.getId(), GL_COMPILE_STATUS, &compiled );
    if( !compiled )
    {
      logShaderError( shader.getId() );
      return A3M_FALSE;
    }

    return A3M_TRUE;
  }

  a3m::ShaderUniformBase::Ptr const& getNullUniform()
  {
    static a3m::ShaderUniformBase::Ptr uniform;
    return uniform;
  }

} // namespace

namespace a3m
{
  /* Predicate class for searching for named uniforms.
   */
  struct ShaderProgram::PropertyNameEquals
  {
    std::string name;

    PropertyNameEquals(A3M_CHAR8 const* value) :
      name(value)
    {
    }

    A3M_BOOL operator()(UniformInstance const& instance)
    {
      return instance.propertyName == name;
    }
  };

  /**********************
   * ShaderProgramCache *
   **********************/

  ShaderProgramCache::ShaderProgramCache()
  {
    // Log shader binary support for user's reference.
    if (getShaderProgramBinariesSupported())
    {
      A3M_LOG_INFO("Shader program binaries are supported.");
    }
    else
    {
      A3M_LOG_INFO("Shader program binaries are not supported.");
    }
  }

  ShaderProgram::Ptr ShaderProgramCache::create(
    A3M_CHAR8 const* vsSource, A3M_CHAR8 const* fsSource,
    A3M_CHAR8 const* name)
  {
    ShaderProgram::Ptr shaderProgram;

    /* Must have both vertex and fragment source to make a valid program */
    if (!(vsSource && fsSource))
    {
      A3M_LOG_ERROR("ShaderProgram source code cannot be empty", 0);
      return shaderProgram;
    }

    /* Create program */
    detail::ProgramResource::Ptr programResource(new detail::ProgramResource());
    getResourceCache()->add(programResource);

    if (!programResource->allocate())
    {
      A3M_LOG_ERROR("Failed to allocate program resource", 0);
      return shaderProgram;
    }

    /* Create vertex shader */
    detail::ShaderResource::Ptr vsResource(new detail::ShaderResource(
        detail::ShaderResource::VERTEX));
    getResourceCache()->add(vsResource);

    if (!vsResource->allocate())
    {
      A3M_LOG_ERROR("Failed to allocate vertex shader resource", 0);
      return shaderProgram;
    }

    if (!compileShader(*vsResource, vsSource))
    {
      return shaderProgram;
    }

    /* Create fragment shader */
    detail::ShaderResource::Ptr fsResource(new detail::ShaderResource(
        detail::ShaderResource::FRAGMENT));
    getResourceCache()->add(fsResource);

    if (!fsResource->allocate())
    {
      A3M_LOG_ERROR("Failed to allocate fragment shader resource", 0);
      return shaderProgram;
    }

    if (!compileShader(*fsResource, fsSource))
    {
      return shaderProgram;
    }

    /* Attach both shaders to the program object, then link it */
    glAttachShader(programResource->getId(), vsResource->getId());
    CHECK_GL_ERROR;
    glAttachShader(programResource->getId(), fsResource->getId());
    CHECK_GL_ERROR;
    glLinkProgram(programResource->getId());
    CHECK_GL_ERROR;

    /* Test that the program linked ok */
    GLint linked;
    glGetProgramiv(programResource->getId(), GL_LINK_STATUS, &linked);

    if (!linked)
    {
      logProgramError(programResource->getId());
      return shaderProgram;
    }

    // Create the shader program object and add it to the cache
    shaderProgram.reset(new ShaderProgram(programResource));
    add(shaderProgram, name);

    return shaderProgram;
  }

  ShaderProgram::Ptr ShaderProgramCache::create(A3M_UINT32 size,
      A3M_UINT32 format, A3M_CHAR8 const* binary, A3M_CHAR8 const* name)
  {
    ShaderProgram::Ptr shaderProgram;

    if (!getShaderProgramBinariesSupported())
    {
      A3M_LOG_ERROR(
        "Shader program binaries are not supported; please check "
        "support using getShaderProgramBinariesSupported()");
      return shaderProgram;
    }

    detail::ProgramResource::Ptr resource(new detail::ProgramResource());
    getResourceCache()->add(resource);

    if (resource->allocate())
    {
      // Load the binary program
      glProgramBinaryOES(resource->getId(), format, binary, size);
      GLint success;
      glGetProgramiv(resource->getId(), GL_LINK_STATUS, &success);

      if (success)
      {
        // Create the shader program object and add it to the cache
        shaderProgram.reset(new ShaderProgram(resource));
        add(shaderProgram, name);
      }
      else
      {
        A3M_LOG_ERROR("ShaderProgram binary load failed", 0);
      }
    }
    else
    {
      A3M_LOG_ERROR("Failed to allocate program resource", 0);
    }

    return shaderProgram;
  }

  /*****************
   * ShaderProgram *
   *****************/

  ShaderProgram::ShaderProgram(detail::ProgramResource::Ptr const& resource) :
    m_context(0),
    m_resource(resource)
  {
    if (m_resource && m_resource->getState() == detail::Resource::ALLOCATED)
    {
      getUniforms();
      getAttributes();
    }
  }

  /*
   * ShaderProgram enable
   */
  void ShaderProgram::enable(RenderContext& context)
  {
    if(m_resource->getId() == 0) { return; }

    link(context);

    glUseProgram( m_resource->getId() );
    CHECK_GL_ERROR;

    enableUniforms();
  }

  void ShaderProgram::enableUniforms()
  {
    for (A3M_INT32 i = 0; i < static_cast<A3M_INT32>(m_uniformInstances.size()); ++i)
    {
      UniformInstance& instance = m_uniformInstances[i];

      // The property value overrides the linked value
      ShaderUniformBase::Ptr value;
      if (instance.propertyValue)
      {
        value = instance.propertyValue;
      }
      else
      {
        value = instance.linkedValue;
      }

      instance.currentValue->enable(
        value, instance.location, instance.texUnit, A3M_FALSE);
    }
  }

  /*
   * Check that ShaderProgram is valid
   */
  A3M_BOOL ShaderProgram::isValid()
  {
    if(m_resource->getId() == 0) { return A3M_FALSE; }

    glValidateProgram( m_resource->getId() );

    GLint valid = 0;
    glGetProgramiv( m_resource->getId(), GL_VALIDATE_STATUS, &valid );

    if( valid ) { return A3M_TRUE; }

    GLint infoLen = 0;
    glGetProgramiv( m_resource->getId(), GL_INFO_LOG_LENGTH, &infoLen );

    if( infoLen > 1 )
    {
      A3M_CHAR8* infoLog = new A3M_CHAR8[ static_cast< size_t >( infoLen ) ];
      glGetProgramInfoLog( m_resource->getId(), infoLen, NULL, infoLog );
      A3M_LOG_ERROR( "Shader program is not in valid state:\n%s\n", infoLog );
      delete [] infoLog;
    }

    return A3M_FALSE;
  }

  /*
   * Bind VertexBuffer attribures
   */
  void ShaderProgram::bind( VertexBuffer& vertexBuffer )
  {
    /* Variable to keep track of the number of attributes that were
     * bound previously. */
    static GLint prevNumAttribs(0);

    if(m_resource->getId() == 0) { return; }

    for( A3M_UINT32 i = 0; i < m_attributes.size(); ++i )
    {
      AttributeInfo const& attribute = m_attributes[i];

      if( !vertexBuffer.enableAttrib(
            static_cast< A3M_UINT32 >( attribute.location ),
            attribute.name.c_str() ) )
      {
        /* (1,1,1,1) is chosen as the default because it is appropriate for
         * missing vertex colours and no particular value is any better than
         * any other for missing texture coordinates or normals. */

        /* It is possible that constant attribute values only have to be set
         * once (as we are always using the same value). */
        GLfloat defValues[4] = {1.0, 1.0, 1.0, 1.0};
        /* We seem to have to make sure no GL vertex buffer is currently bound
         * for the constant value to work -otherwise random values seem to be
         * used. */
        glBindBuffer( GL_ARRAY_BUFFER, 0 );
        glVertexAttrib4fv( static_cast< A3M_UINT32 >( attribute.location ),
                           defValues );
        CHECK_GL_ERROR;
      }
    }

    /* Unbind all unused attributes */
    for ( A3M_INT32 i = m_attributes.size(); i < prevNumAttribs; ++i )
    {
      glDisableVertexAttribArray( static_cast< GLuint >( i ) );
    }
    prevNumAttribs = m_attributes.size();
  }

  A3M_CHAR8 const* ShaderProgram::getUniformInstanceName(A3M_INT32 i) const
  {
    if (!indexInRange(i)) { return 0; }
    return m_uniformInstances[i].instanceName.c_str();
  }

  void ShaderProgram::setUniformPropertyName(
    A3M_INT32 i, A3M_CHAR8 const* propertyName)
  {
    if (!indexInRange(i)) { return; }
    m_uniformInstances[i].propertyName = propertyName;
  }

  A3M_CHAR8 const* ShaderProgram::getUniformPropertyName(A3M_INT32 i) const
  {
    if (!indexInRange(i)) { return 0; }
    return m_uniformInstances[i].propertyName.c_str();
  }

  /*
   * Sets a ShaderUniform object
   */
  void ShaderProgram::setUniformPropertyValue(
    A3M_INT32 i, ShaderUniformBase::Ptr const& uniform )
  {
    if (!indexInRange(i)) { return; }
    m_uniformInstances[i].propertyValue = uniform;
  }

  A3M_BOOL ShaderProgram::uniformExists(A3M_CHAR8 const* name) const
  {
    UniformInstanceVector::const_iterator it = std::find_if(
          m_uniformInstances.begin(),
          m_uniformInstances.end(),
          PropertyNameEquals(name));

    return it != m_uniformInstances.end();
  }

  A3M_INT32 ShaderProgram::getUniformCount() const
  {
    return m_uniformInstances.size();
  }

  void ShaderProgram::link(RenderContext& context)
  {
    // Only link the uniforms with the context when it changes
    if (&context != m_context)
    {
      for (A3M_INT32 i = 0; i < static_cast<A3M_INT32>(m_uniformInstances.size()); ++i)
      {
        context.linkUniform(
          m_uniformInstances[i].linkedValue,
          m_uniformInstances[i].propertyName.c_str());
      }

      m_context = &context;
    }
  }

  A3M_BOOL ShaderProgram::indexInRange(A3M_INT32 index) const
  {
    if (index < 0 || index >= static_cast<A3M_INT32>(m_uniformInstances.size()))
    {
      A3M_LOG_ERROR("Uniform index (%d) out of range [0:%d).",
                    index, m_uniformInstances.size());
      return A3M_FALSE;
    }

    return A3M_TRUE;
  }

  /*
   * Create a uniform of the given (by template parameter) type and name.
   */
  template< class T >
  void ShaderProgram::createUniform( A3M_CHAR8 const* name,
                                     A3M_INT32 location,
                                     A3M_INT32 texUnit,
                                     A3M_INT32 size )
  {
    // If name has an index appended to it, then trim it off
    std::string trimmedName = name;
    trimmedName = trimmedName.substr(0, trimmedName.find_first_of('['));

    ShaderUniformBase::Ptr value = ShaderUniformBase::Ptr(
                                     new ShaderUniform< T >( size ) );

    m_uniformInstances.push_back(
      UniformInstance(trimmedName.c_str(), location, texUnit, value));
  }

  /*
   * Get list of active shader uniforms
   */
  void ShaderProgram::getUniforms()
  {
    if( m_resource->getId() == 0 ) { return; }

    GLint uniformCount;
    GLint maxNameLength;

    glGetProgramiv( m_resource->getId(),
                    GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxNameLength);
    A3M_CHAR8* name = new A3M_CHAR8[ maxNameLength ];

    glGetProgramiv( m_resource->getId(), GL_ACTIVE_UNIFORMS, &uniformCount );

    GLint texUnit = 0;

    for( GLint i = 0; i < uniformCount; ++i )
    {
      GLint  size;
      GLenum type;
      glGetActiveUniform( m_resource->getId(), static_cast<GLuint>(i),
                          maxNameLength,
                          0, &size, &type, name );
      GLint location = glGetUniformLocation( m_resource->getId(), name );

      switch( type )
      {
      case GL_FLOAT:
        createUniform< A3M_FLOAT >( name, location, NO_TEX_UNIT, size );
        break;
      case GL_FLOAT_VEC2:
        createUniform< Vector2f >( name, location, NO_TEX_UNIT, size );
        break;
      case GL_FLOAT_VEC3:
        createUniform< Vector3f >( name, location, NO_TEX_UNIT, size );
        break;
      case GL_FLOAT_VEC4:
        createUniform< Vector4f >( name, location, NO_TEX_UNIT, size );
        break;
      case GL_INT:
        createUniform< A3M_INT32 >( name, location, NO_TEX_UNIT, size );
        break;
      case GL_INT_VEC2:
        createUniform< Vector2< A3M_INT32 > >( name, location, NO_TEX_UNIT,
                                               size );
        break;
      case GL_INT_VEC3:
        createUniform< Vector3< A3M_INT32 > >( name, location, NO_TEX_UNIT,
                                               size );
        break;
      case GL_INT_VEC4:
        createUniform< Vector4< A3M_INT32 > >( name, location, NO_TEX_UNIT,
                                               size );
        break;
      case GL_BOOL:
        createUniform< A3M_BOOL >( name, location, NO_TEX_UNIT, size );
        break;
      case GL_BOOL_VEC2:
        createUniform< Vector2< A3M_BOOL > >( name, location, NO_TEX_UNIT,
                                              size );
        break;
      case GL_BOOL_VEC3:
        createUniform< Vector3< A3M_BOOL > >( name, location, NO_TEX_UNIT,
                                              size );
        break;
      case GL_BOOL_VEC4:
        createUniform< Vector4< A3M_BOOL > >( name, location, NO_TEX_UNIT,
                                              size );
        break;
      case GL_FLOAT_MAT2:
        createUniform< Matrix2f >( name, location, NO_TEX_UNIT, size );
        break;
      case GL_FLOAT_MAT3:
        createUniform< Matrix3f >( name, location, NO_TEX_UNIT, size );
        break;
      case GL_FLOAT_MAT4:
        createUniform< Matrix4f >( name, location, NO_TEX_UNIT, size );
        break;
      case GL_SAMPLER_2D:
        createUniform< Texture2D::Ptr >( name, location, texUnit, size );
        texUnit += size;
        break;
      case GL_SAMPLER_EXTERNAL_OES: // Special - for live video texture
        // Check if the platform supports external image source
        if (GL_OES_EGL_image_external_supported())
        {
          createUniform< Texture2D::Ptr >( name, location, texUnit, size );
          texUnit += size;
        }
        else
        {
          A3M_LOG_ERROR("Extension \"GL_OES_EGL_image_external\""
                        " not supported.");
        }
        break;
      case GL_SAMPLER_CUBE:
        createUniform< TextureCube::Ptr >( name, location, texUnit, size );
        texUnit += size;
        break;
      default:
        A3M_LOG_ERROR( "Unknown shader uniform type (%d)", type );
        break;
      }
    }

    delete[] name;

    /* Sanity checking:
     * Warn if number of texture samplers used by the program exceeds the
     * maximum number of texture units available on the platform. */
    GLint maxTexUnits;
    glGetIntegerv (GL_MAX_TEXTURE_IMAGE_UNITS, &maxTexUnits);
    if ((texUnit - 1) > maxTexUnits)
    {
      pssLogInfo( __FILE__, __FUNCTION__, __LINE__,
                  "Max texture units (%d) exceeded by shader program (%d).",
                  maxTexUnits, (texUnit - 1) );
    }
  }

  /*
   * Get list of active shader attributes
   */
  void ShaderProgram::getAttributes()
  {
    if(m_resource->getId() == 0) { return; }

    GLint numAttribs;
    glGetProgramiv( m_resource->getId(), GL_ACTIVE_ATTRIBUTES, &numAttribs );
    CHECK_GL_ERROR;

    for( A3M_INT32 i = 0; i < numAttribs; ++i )
    {
      GLenum type;
      GLint size;
      A3M_CHAR8 name[ a3m::VERTEX_ATTRIBUTE_MAX_NAME_LENGTH ];
      /* Get name of attribute at index i (NOT location - see below). */
      glGetActiveAttrib( m_resource->getId(), static_cast< GLuint >( i ),
                         a3m::VERTEX_ATTRIBUTE_MAX_NAME_LENGTH,
                         NULL /*length*/, &size, &type, name );
      CHECK_GL_ERROR;

      /* Although the OpenGL ES 2.0 Programming Guide and other resources
       * use the terms 'index' and 'location' interchangeably, when using
       * certain platform, they can have different values.
       * This causes vertex attributes to be bound at the wrong locations.
       * To fix this we have to look up the location of the attribute using
       * the name we have just been given by glGetActiveAttrib() and use this
       * value to bind the attribute in the VertexBuffer object.
       */
      A3M_INT32 location = glGetAttribLocation( m_resource->getId(), name );
      CHECK_GL_ERROR;

      if( location >= 0 )
      {
        m_attributes.push_back( AttributeInfo( name, location ) );
      }
    }
  }

  A3M_BOOL ShaderProgram::getBinary( A3M_UINT32& size,
                                     A3M_UINT32& format,
                                     A3M_CHAR8*& binary)
  {
    if (!getShaderProgramBinariesSupported())
    {
      A3M_LOG_ERROR(
        "Shader program binaries are not supported; please check "
        "support using getShaderProgramBinariesSupported()");
      return A3M_FALSE;
    }

    GLsizei glSize;
    glGetProgramiv( m_resource->getId(), GL_PROGRAM_BINARY_LENGTH_OES, &glSize);

    binary = new A3M_CHAR8[ glSize ];
    if (!binary)
    {
      A3M_LOG_ERROR( "Failed to allocate memory", 0 );
      return A3M_FALSE;
    }

    GLsizei outSize;
    glGetProgramBinaryOES( m_resource->getId(), glSize, &outSize, &format, binary );

    size = glSize;
    return A3M_TRUE;
  }

  A3M_BOOL getShaderProgramBinariesSupported()
  {
    return GL_OES_get_program_binary_supported();
  }

} /* namespace a3m */
