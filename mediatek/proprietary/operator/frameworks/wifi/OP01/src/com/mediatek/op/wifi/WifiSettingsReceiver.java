package com.mediatek.op.wifi;


import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * For CMCC AP afer change SIM.
 */
public class WifiSettingsReceiver extends BroadcastReceiver {

    private final static String TAG = "WifiSettingsReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        ///send implicit broadcast to "com.mediatek.op09.plugin.WifiSettingsReceiver"
        Intent explicit = new Intent(intent);
        ComponentName cn = new ComponentName("com.mediatek.settings.op01",
                "com.mediatek.settings.op01.WifiSettingsReceiver");
        explicit.setComponent(cn);
        context.sendBroadcast(explicit);
        Log.i(TAG, "OP01 WIFI FRAMEWORK SEND BROADCAST DONE");
    }
}


