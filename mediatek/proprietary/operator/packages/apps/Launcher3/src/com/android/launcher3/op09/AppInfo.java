/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.launcher3;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LauncherActivityInfo;
/// M: Add for OP customization.
import android.graphics.Bitmap;
import android.os.UserHandle;
/// M: Add for OP customization.
import android.util.Log;

import com.android.launcher3.compat.UserManagerCompat;
import com.android.launcher3.util.ComponentKey;
import com.android.launcher3.util.PackageManagerHelper;

/**
 * Represents an app in AllAppsView.
 */
public class AppInfo extends ItemInfoWithIcon {
    /// M: Add for OP customization.
    private static final String TAG = "Launcher3.AppInfo";

    /**
     * The intent used to start the application.
     */
    public Intent intent;

    public ComponentName componentName;

    /// M: Add for OP customization. @{
    public boolean isVisible = true;
    public int isForPadding = NOT_PADDING_APP;
    public static final int NOT_PADDING_APP = 0;
    public static final int PADDING_APP = 1;
    public static final int DRAGGING_PADDING_APP = 2;

    static final int DOWNLOADED_FLAG = 1;
    static final int UPDATED_SYSTEM_APP_FLAG = 2;

    int flags = 0;

    /**
     * A bitmap version of the application icon.
     */
    //public Bitmap iconBitmap;
    /// @}

    /**
     * {@see ShortcutInfo#isDisabled}
     */
    public int isDisabled = ShortcutInfo.DEFAULT;

    public AppInfo() {
        itemType = LauncherSettings.Favorites.ITEM_TYPE_APPLICATION;
    }

    @Override
    public Intent getIntent() {
        return intent;
    }

    /**
     * Must not hold the Context.
     */
    public AppInfo(Context context, LauncherActivityInfo info, UserHandle user) {
        this(info, user, UserManagerCompat.getInstance(context).isQuietModeEnabled(user));
    }

    public AppInfo(LauncherActivityInfo info, UserHandle user, boolean quietModeEnabled) {
        this.componentName = info.getComponentName();
        this.container = ItemInfo.NO_ID;
        this.user = user;
        /// M: Add for OP customization.
        flags = initFlags(info);
        if (PackageManagerHelper.isAppSuspended(info.getApplicationInfo())) {
            isDisabled |= ShortcutInfo.FLAG_DISABLED_SUSPENDED;
        }
        if (quietModeEnabled) {
            isDisabled |= ShortcutInfo.FLAG_DISABLED_QUIET_USER;
        }

        intent = makeLaunchIntent(info);
    }

    public AppInfo(AppInfo info) {
        super(info);
        componentName = info.componentName;
        title = Utilities.trim(info.title);
        intent = new Intent(info.intent);
        isDisabled = info.isDisabled;
        /// M: Add for OP customization. @{
        flags = info.flags;
        isVisible = info.isVisible;
        isForPadding = info.isForPadding;
        /// @}
    }

    @Override
    protected String dumpProperties() {
        return super.dumpProperties()
                /// M: Modify for OP customization. @{
                + " user=" + user + " isVisible=" + isVisible
                + " isForPadding=" + isForPadding + " mPos:" + mPos
                /// @}
                + " componentName=" + componentName;
    }

    public ShortcutInfo makeShortcut() {
        return new ShortcutInfo(this);
    }

    public ComponentKey toComponentKey() {
        return new ComponentKey(componentName, user);
    }

    public static Intent makeLaunchIntent(LauncherActivityInfo info) {
        return new Intent(Intent.ACTION_MAIN)
            .addCategory(Intent.CATEGORY_LAUNCHER)
            .setComponent(info.getComponentName())
            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
    }

    @Override
    public boolean isDisabled() {
        return isDisabled != 0;
    }

    /// M: Add for OP customization. @{
    public Bitmap getIcon() {
        if (iconBitmap == null) {
            iconBitmap = LauncherAppState.getInstanceNoCreate().getIconCache().getIcon(intent,
                    user);
            Log.d(TAG, "getIcon: icon is null, get it again, app=" + this);
        }
        return iconBitmap;
    }

    public static int initFlags(LauncherActivityInfo info) {
        int appFlags = info.getApplicationInfo().flags;
        int flags = 0;
        if ((appFlags & android.content.pm.ApplicationInfo.FLAG_SYSTEM) == 0) {
            flags |= DOWNLOADED_FLAG;

            if ((appFlags & android.content.pm.ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
                flags |= UPDATED_SYSTEM_APP_FLAG;
            }
        }
        return flags;
    }
    /// @}
}
