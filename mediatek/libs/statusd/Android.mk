LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/external/statusd))
include $(CLEAR_VARS)
LOCAL_MODULE = statusd
LOCAL_MODULE_CLASS = EXECUTABLES
LOCAL_MODULE_OWNER = mtk
LOCAL_PROPRIETARY_MODULE = true
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libcutils libc2kutils liblog
LOCAL_INIT_RC = statusd.rc
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/statusd
include $(BUILD_PREBUILT)
endif
