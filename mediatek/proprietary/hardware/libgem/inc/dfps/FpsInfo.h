#ifndef __FPSINFO_H__
#define __FPSINFO_H__

#include <utils/String8.h>
#include <utils/RefBase.h>
#include <utils/Mutex.h>

namespace android {

class HwcInfo {
public:
    HwcInfo();
    int32_t singleLayer;
    int32_t numDisplay;
};

class SimpleInputWindowInfo {
public:
    int32_t ownerPid;
    bool hasFocus;
};

class FpsInfo : public virtual RefBase{
public:
    class FpsInfoListener : public virtual RefBase{
    public:
        virtual ~FpsInfoListener() {};
        virtual void onFpsChanged(const int32_t fps) = 0;
    };
    FpsInfo();
    virtual ~FpsInfo();

    // updateState() is used to update the service state. Binder exception will
    // update this info, so does not called it directly.
    void updateState(int32_t flag);

    // setSingleLayer() is used to notify fps policy service that the number of layer
    // is in the screen. It should be called by HWC.
    status_t setSingleLayer(int32_t single);

    // setNumberDisplay() is used to notify fps policy service of how many displays
    // system have. It should be called by HWC.
    status_t setNumberDisplay(int32_t number);

    // setHwcInfo() is used to notify fps policy service of the status of HWC. It
    // should be called by HWC.
    status_t setHwcInfo(const HwcInfo& info);

    // setInputWindows is used to notify the fps policy service of the focused window.
    // It should be called by WMS.
    status_t setInputWindows(const String8& name, const SimpleInputWindowInfo& info);

    // regFpsInfoListener() is used to register the callback function to fps policy
    // service. When the system fps is changed, it will call this callback function.
    // Does not make free with this api. If the module which registe callback function
    // is dead, it may caused that refresh rate of system is wrong. The worst case is
    // that system will hang.
    status_t regFpsInfoListener(sp<FpsInfoListener> &cb);

    // When fps is changed, fps policy service uses this function to call registered
    // callback function. It should be called by fps policy service.
    virtual status_t cbGetFps(int32_t fps);

    // setForegroundInfo is used to set current foreground window infomation. It
    // be called by PerfService
    status_t setForegroundInfo(int32_t pid, String8& packageName);

    // setWindowFlag is used to set current status of window(like multi-window and
    // so on...). It should be called by WMS.
    status_t setWindowFlag(const int32_t flag, const int32_t mask);

    // enableTracker is used to control Tracker to dump policy info to trace. It
    // should be called by SF
    void enableTracker(const bool enable);

    // forbidAdjustingVSync is used to ban variation of VSync period. It should be
    // called by VR Hal
    status_t forbidAdjustingVsync(bool forbid);

    enum {
        FLAG_MULTI_WINDOW = 0x01,
    };

    enum {
        STATE_SERVER_DIED = 0x01,
    };

private:
    status_t connectServiceLocked();

    mutable Mutex mLock;
    status_t mState;
    uint64_t mId;
    sp<FpsInfoListener> mCb;

    int32_t mSingleLayer;
    int32_t mNumDisplay;
    HwcInfo mHwcInfo;

    int32_t mForceWindowPid;
    bool mEnableTracker;
    bool mForbidVSync;
};

};

#endif
