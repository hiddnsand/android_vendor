/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <a3m/assetcachepool.h>   /* for AssetCachePool class                */
#include <a3m/background.h>       /* for Background class                    */
#include <a3m/camera.h>           /* for Camera class                        */
#include <a3m/colour.h>           /* for Color class                         */
#include <a3m/glofile.h>          /* for for gloloader                       */
#include <a3m/renderdevice.h>     /* for Renderdevice class                  */
#include <a3m/simplerenderer.h>   /* for Simplerender clasee                 */
#include <a3m/scenenode.h>        /* for Scenenode class                     */
#include <a3m/scenenodevisitor.h> /* for SceneNodeVisitor class              */
#include <a3m/sceneutility.h>     /* for visitScene()                        */
#include <a3m/window.h>           /* for window API definition               */

namespace
{

  using namespace a3m;

  /*
   * Handler for application window events
   */
  class WindowEventHandler : public SimpleWindowListener
  {
    void onClose(Window* window);
    void onKeyDown(Window* window, KeyCode key);
    void onResize(Window* window, A3M_INT32 width, A3M_INT32 height);
  };

  class Application : public Shared, NonCopyable
  {
  private:
    A3M_BOOL m_running;
    Window::Ptr m_window;
    AssetCachePool::Ptr m_pool;
    SimpleRenderer::Ptr m_renderer;
    Background m_background;
    SceneNode::Ptr m_node;
    Camera::Ptr m_camera;
    FlagMask m_renderFlags;
    FlagMask m_recursiveFlags;

  public:
    typedef SharedPtr<Application> Ptr;

    Application(A3M_INT32 width, A3M_INT32 height);
    ~Application();

    void run();
    void close();
    void render();
    void loadGlo(A3M_CHAR8 const* filename);
  };

  class CameraFinder : public SceneNodeVisitor
  {
  private:
    Camera::Ptr m_camera;

  public:
    void visit(Camera* camera)
    {
      // Just find first camera.
      if (!m_camera)
      {
        m_camera.reset(camera);
      }
    }

    Camera::Ptr const& getCamera() const
    {
      return m_camera;
    }
  };

  Application::Ptr s_application;
  WindowEventHandler s_eventHandler;

  /***************************************************************************
   * Implementation                                                          *
   ***************************************************************************/

  void WindowEventHandler::onClose(Window* window)
  {
    s_application->close();
  }

  void WindowEventHandler::onKeyDown(Window* window, KeyCode key)
  {
    switch (key)
    {
    case KEY_ESCAPE:
      s_application->close();
      break;

    default:
      break;
    }
  }

  void WindowEventHandler::onResize(
      Window* window, A3M_INT32 width, A3M_INT32 height)
  {
    RenderDevice::setViewport(0, 0, width, height);
  }

  Application::Application(A3M_INT32 width, A3M_INT32 height) :
    m_running(A3M_FALSE),
    m_node(new SceneNode())
  {
    m_window = createWindow("A3M Example", width, height, 24);
    m_window->setListener(&s_eventHandler);

    // Must wait until after context is created to create pool.
    m_pool.reset(new AssetCachePool());
    registerSource(*m_pool, "assets");

    m_renderer.reset(new SimpleRenderer(m_pool));
    m_background.setColour(Colour4f(0.0, 0.0, 0.0, 1.0));
  }

  Application::~Application()
  {
    m_pool->release();
  }

  void Application::run()
  {
    m_running = A3M_TRUE;

    while (m_running)
    {
      render();
      m_window->refresh();
    }
  }

  void Application::close()
  {
    m_running = A3M_FALSE;
  }

  void Application::render()
  {
    RenderDevice::clear(m_background);

    if (m_camera && m_node)
    {
      m_renderer->render(*m_camera, *m_node, m_renderFlags, m_recursiveFlags);
    }
  }

  void Application::loadGlo(A3M_CHAR8 const* filename)
  {
    Glo glo = loadGloFile(*m_pool, SceneNode::Ptr(), filename);
    glo.node->setParent(m_node);

    // Find the first camera in the Glo file.
    CameraFinder finder;
    visitScene(finder, *glo.node);

    m_camera = finder.getCamera();
  }

} // namespace

int main(int argc, char* argv[])
{
  int width;
  int height;

  getScreenSize(width, height);

  s_application.reset(new Application(width, height));

  if (argc >= 2)
  {
    s_application->loadGlo(argv[1]);
  }

  s_application->run();
  s_application.reset();
}
