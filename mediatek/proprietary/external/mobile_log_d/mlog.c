#define MTK_LOG_ENABLE 1
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <log/log.h>
#include <sys/stat.h>
#include "config.h"
#include "global_var.h"
#include "mlog.h"
#include "libfunc.h"

/*
 * save msg to both 'log file' in /data/misc/mblog/
 * and current log path
 */
#define MB_LOG_FILE     "mblog_history"
#define MAX_FILE_SIZE   (10*1024*1024)

pthread_mutex_t mlog_lock = PTHREAD_MUTEX_INITIALIZER;

void __mb_log(int type, char *fmt, ...)
{
	char buf[255],ts[255],result[255];
	FILE *fp;
	struct tm *tmptr;
	time_t lt;
	int fmt_start,to_sd_fail=0;
	static int haschmod = 0, sz_overflow=0;
	static unsigned long long total_sz=0;

	memset(ts,0x0,sizeof(ts));
	lt = time(NULL);
	tmptr = localtime(&lt);
	strftime(ts,sizeof(ts),"%Y:%m:%d %H:%M:%S ",tmptr);

	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buf,sizeof(buf),fmt,ap);
	va_end(ap);

	snprintf(result,sizeof(result),"%s%s(%d)",ts,buf,getpid());

	pthread_mutex_lock(&mlog_lock);
	if (type == TO_SDCARD || type == TO_BOTH) {
		char full_path[255];

		snprintf(full_path, sizeof(full_path), "%s%s", cur_logging_path, MB_LOG_FILE);
		if (strlen(cur_logging_path) && (fp = fopen(full_path,"a+"))) {
			fputs(result, fp);
			fputs("\n", fp);
			fclose(fp);
		} else if (type == TO_SDCARD) {
			to_sd_fail=1;
		}
	}

	if (!sz_overflow && (type == TO_DATA || type == TO_BOTH || to_sd_fail)) {
		if (access(CONFIG_DIR,F_OK) != 0) {
			if (!create_dir(CONFIG_DIR)) {
				ALOGE("create dir %s failed(%s)",CONFIG_DIR,strerror(errno));
				goto err;
			}
		}

		if ((fp = fopen(MBLOG_HISTORY,"a+"))) {
			fputs(result, fp);
			fputs("\n", fp);
			fclose(fp);
		}
		total_sz += strlen(result);
		if (total_sz > MAX_FILE_SIZE)
			sz_overflow = 1;

		if (!haschmod) {
			if (chmod(MBLOG_HISTORY,0664) < 0) {
				ALOGE("chmod %s fail(%s)", MBLOG_HISTORY, strerror(errno));
				goto err;
			}
			haschmod = 1;
		}
	}

err:
	pthread_mutex_unlock(&mlog_lock);

	return;
}

