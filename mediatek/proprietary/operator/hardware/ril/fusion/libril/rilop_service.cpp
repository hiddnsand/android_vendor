/*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein
* is confidential and proprietary to MediaTek Inc. and/or its licensors.
* Without the prior written permission of MediaTek inc. and/or its licensors,
* any reproduction, modification, use or disclosure of MediaTek Software,
* and information contained herein, in whole or in part, shall be strictly prohibited.
*
* MediaTek Inc. (C) 2017. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
* AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
* NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
* SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
* SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
* THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
* THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
* CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
* SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
* CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
* AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
* OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
* MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
* The following software/firmware and/or related documentation ("MediaTek Software")
* have been modified by MediaTek Inc. All revisions are subject to any receiver's
* applicable license agreements with MediaTek Inc.
*/

#define LOG_TAG "RILS-OP"

#include <android/hardware/radio/1.0/IRadio.h>
//#include <android/hardware/radio/deprecated/1.0/IOemHook.h>
#include <vendor/mediatek/hardware/radio_op/1.1/IRadioOp.h>

#include <hwbinder/IPCThreadState.h>
#include <hwbinder/ProcessState.h>
#include <rilop_service.h>
#include <ril_service.h>
#include <hidl/HidlTransportSupport.h>
#include <utils/SystemClock.h>
#include <inttypes.h>
#include <telephony/librilutilsmtk.h>
#include <telephony/mtk_rilop.h>
#include <telephony/mtk_ril.h>

#define INVALID_HEX_CHAR 16

using ::android::hardware::configureRpcThreadpool;
using ::android::hardware::joinRpcThreadpool;
using ::android::hardware::Return;
using ::android::hardware::Status;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::hidl_array;
using ::android::hardware::Void;
using android::CommandInfo;
using android::RequestInfo;
using android::requestToString;
using android::sp;

using ::android::hardware::radio::V1_0::RadioResponseType;
using ::android::hardware::radio::V1_0::RadioIndicationType;
using ::android::hardware::radio::V1_0::RadioResponseInfo;
using ::android::hardware::radio::V1_0::CardStatus;
using ::android::hardware::radio::V1_0::RadioState;

using namespace vendor::mediatek::hardware::radio_op::V1_1;

#define BOOL_TO_INT(x) (x ? 1 : 0)
#define ATOI_NULL_HANDLED(x) (x ? atoi(x) : -1)
#define ATOI_NULL_HANDLED_DEF(x, defaultVal) (x ? atoi(x) : defaultVal)

RIL_RadioFunctions *s_vendorFunctions = NULL;
static CommandInfo *s_commands;

struct RadioImpl;
struct OemHookImpl;

#define MAX_SIM_COUNT 4
//#if (SIM_COUNT >= 2)
sp<RadioImpl> radioService[MAX_SIM_COUNT * DIVISION_COUNT];
// counter used for synchronization. It is incremented every time response callbacks are updated.
volatile int32_t mCounterRadio[MAX_SIM_COUNT * DIVISION_COUNT];

extern bool dispatchInts(int serial, int slotId, int request, int countInts, ...);
extern RadioIndicationType convertIntToRadioIndicationType(int indicationType);
extern bool dispatchStrings(int serial, int slotId, int request, int countStrings, ...);
extern void populateResponseInfo(RadioResponseInfo& responseInfo, int serial,
                                 int responseType, RIL_Errno e);
extern hidl_string convertCharPtrToHidlString(const char *ptr);

// To Compute IMS Slot Id
extern "C" int toRealSlot(int slotId);
extern "C" int isImsSlot(int slotId);
extern "C" int toImsSlot(int slotId);

struct RadioImpl : public IRadioOp {
    int32_t mSlotId;
    sp<IRadioResponseOp> mRadioResponseMtk;
    sp<IRadioIndicationOp> mRadioIndicationMtk;
    sp<IImsRadioResponseOp> mRadioResponseIms;
    sp<IImsRadioIndicationOp> mRadioIndicationIms;
    sp<IDigitsRadioResponse> mRadioResponseDigits;
    sp<IDigitsRadioIndication> mRadioIndicationDigits;

    Return<void> responseAcknowledgement();

    Return<void> setResponseFunctions(
            const ::android::sp<IRadioResponseOp>& radioResponse,
            const ::android::sp<IRadioIndicationOp>& radioIndication);

    Return<void> setResponseFunctionsIms(
           const ::android::sp<IImsRadioResponseOp>& radioResponse,
           const ::android::sp<IImsRadioIndicationOp>& radioIndication);

    Return<void> setResponseFunctionsDigits(
           const ::android::sp<IDigitsRadioResponse>& radioResponse,
           const ::android::sp<IDigitsRadioIndication>& radioIndication);

    Return<void> setRttMode(int serial, int mode);

    Return<void> sendRttModifyRequest(int serial, int callId, int newMode);

    Return<void> sendRttText(int serial, int callId, int lenOfString,
                             const hidl_string& text);

    Return<void> rttModifyRequestResponse(int serial, int callId, int result);

    Return<void> setDigitsLine(int serial, int accountId,
                               int digitsSerial, bool isLogout,
                               bool hasNext, bool isNative,
                               const ::android::hardware::hidl_string& msisdn,
                               const ::android::hardware::hidl_string& sit);

    void checkReturnStatus(Return<void>& ret);
    Return<void> testOp(int32_t serial, bool isOn);
    Return<void> testIndication();
    Return<void> testResponse();
};

void checkReturnStatus(int32_t slotId, Return<void>& ret, bool isRadioService) {
    if (ret.isOk() == false) {
        RLOGE("checkReturnStatus: unable to call response/indication callback");
        // Remote process hosting the callbacks must be dead. Reset the callback objects;
        // there's no other recovery to be done here. When the client process is back up, it will
        // call setResponseFunctions()

        // Caller should already hold rdlock, release that first
        // note the current counter to avoid overwriting updates made by another thread before
        // write lock is acquired.
        int counter = mCounterRadio[slotId];
        pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(slotId);
        int ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
        assert(ret == 0);

        // acquire wrlock
        ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
        assert(ret == 0);

        // make sure the counter value has not changed
        if (counter == mCounterRadio[slotId]) {
            radioService[slotId]->mRadioResponseMtk = NULL;
            radioService[slotId]->mRadioIndicationMtk = NULL;
            mCounterRadio[slotId]++;
        } else {
            RLOGE("checkReturnStatus: not resetting responseFunctions as they likely "
                    "got updated on another thread");
        }

        // release wrlock
        ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
        assert(ret == 0);

        // Reacquire rdlock
        ret = pthread_rwlock_rdlock(radioServiceRwlockPtr);
        assert(ret == 0);
    }
}

void RadioImpl::checkReturnStatus(Return<void>& ret) {
    ::checkReturnStatus(mSlotId, ret, true);
}

Return<void> RadioImpl::responseAcknowledgement() {
    android::releaseWakeLock();
    return Void();
}

Return<void> RadioImpl::setResponseFunctions(
        const ::android::sp<IRadioResponseOp>& radioResponseParam,
        const ::android::sp<IRadioIndicationOp>& radioIndicationParam) {
    RLOGD("setResponseFunctions");

    pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(mSlotId);
    int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
    assert(ret == 0);

    mRadioResponseMtk = radioResponseParam;
    mRadioIndicationMtk = radioIndicationParam;

    ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
    assert(ret == 0);

    return Void();
}

Return<void> RadioImpl::setResponseFunctionsIms(
        const ::android::sp<IImsRadioResponseOp>& radioResponseParam,
        const ::android::sp<IImsRadioIndicationOp>& radioIndicationParam) {
    RLOGD("setResponseFunctionsIms");

    pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(mSlotId);
    int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
    assert(ret == 0);

    mRadioResponseIms = radioResponseParam;
    mRadioIndicationIms = radioIndicationParam;

    ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
    assert(ret == 0);

    return Void();
}

Return<void> RadioImpl::setResponseFunctionsDigits(
           const ::android::sp<IDigitsRadioResponse>& radioResponseParam,
           const ::android::sp<IDigitsRadioIndication>& radioIndicationParam) {

    RLOGD("setResponseFunctionsDigits");

    pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(mSlotId);
    int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
    assert(ret == 0);

    mRadioResponseDigits = radioResponseParam;
    mRadioIndicationDigits = radioIndicationParam;

    ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
    assert(ret == 0);

    return Void();
}

/*********************************************************************************/
/*  Vendor Command                                                               */
/*********************************************************************************/
Return<void> RadioImpl::setRttMode(int serial, int mode) {
    RLOGD("setRttMode: serial %d", serial);

    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_RTT_MODE, 1, mode);
    return Void();
}

Return<void> RadioImpl::sendRttModifyRequest(int serial, int callId, int newMode) {
    RLOGD("sendRttModifyRequest: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SEND_RTT_MODIFY_REQUEST, 2,
                 callId, newMode);

    return Void();
}

Return<void> RadioImpl::sendRttText(int serial, int callId, int lenOfString,
                         const hidl_string& text) {
    RLOGD("sendRttText: serial %d", serial);
    hidl_string strCallId = std::to_string(callId);
    hidl_string strlenOfString = std::to_string(lenOfString);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SEND_RTT_TEXT, 3,
                    strCallId.c_str(), strlenOfString.c_str(), text.c_str());

    return Void();
}

Return<void> RadioImpl::rttModifyRequestResponse(int serial, int callId, int result) {
    RLOGD("rttModifyRequestResponse: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_RTT_MODIFY_REQUST_RESPONSE, 2,
                 callId, result);
    return Void();
}

Return<void> RadioImpl::setDigitsLine(int serial, int accountId,
                                      int digitsSerial, bool isLogout,
                                      bool hasNext, bool isNative,
                                      const ::android::hardware::hidl_string& msisdn,
                                      const ::android::hardware::hidl_string& sit) {
    RLOGD("setDigitsLine: serial %d", serial);

    hidl_string strAccountId = std::to_string(accountId);
    hidl_string strDigitsSerial = std::to_string(digitsSerial);
    hidl_string strIsLogout = std::to_string(isLogout);
    hidl_string strHasNext = std::to_string(hasNext);
    hidl_string strIsNative = std::to_string(isNative);

    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_DIGITS_LINE, 7,
                    strAccountId.c_str(), strDigitsSerial.c_str(), strIsLogout.c_str(),
                    strHasNext.c_str(), strIsNative.c_str(), msisdn.c_str(), sit.c_str());

    return Void();
}

/*********************************************************************************/
/*  Response of Vendor Solicated Command                                         */
/*********************************************************************************/
int radio::setRttModeResponse(int slotId,
                              int responseType, int serial, RIL_Errno e,
                              void *response, size_t responselen) {

    RLOGD("setRttModeResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setRttModeResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        RLOGE("setRttModeResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                        slotId);
    }

    return 0;
}

int radio::sendRttModifyRequestResponse(int slotId,
                                        int responseType, int serial, RIL_Errno e,
                                        void *response, size_t responselen) {

    RLOGD("sendRttModifyRequestResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 sendRttModifyRequestResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        RLOGE("sendRttModifyRequestResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                                  slotId);
    }

    return 0;
}

int radio::sendRttTextResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responselen) {

    RLOGD("sendRttTextResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 sendRttTextResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        RLOGE("sendRttTextResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                        slotId);
    }

    return 0;
}

int radio::rttModifyRequestResponseResponse(int slotId,
                                            int responseType, int serial, RIL_Errno e,
                                            void *response, size_t responselen) {

    RLOGD("rttModifyRequestResponseResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setRttModeResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        RLOGE("rttModifyRequestResponseResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                                      slotId);
    }

    return 0;
}

int radio::setDigitsLineResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responselen) {

    RLOGD("setDigitsLineResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseDigits != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseDigits->
                                 setDigitsLineResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        RLOGE("setDigitsLineResponse: radioService[%d]->mRadioResponseDigits == NULL",
                                                                              slotId);
    }

    return 0;
}


/*********************************************************************************/
/*  Vendor Unsolicated Command                                                   */
/*********************************************************************************/
int radio::rttModifyResponseInd(int slotId,
                               int indicationType, int token, RIL_Errno e,
                               void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        int *resp = (int *) response;
        int numStrings = responselen / sizeof(int);
        if(numStrings < 2) {
            RLOGE("rttModifyResponseInd: items length invalid, slotId = %d",
                                                                 imsSlotId);
            return 0;
        }

        int callId = ((int32_t *) resp)[0];
        int result = ((int32_t *) resp)[1];

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->rttModifyResponse(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, result);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        RLOGE("rttModifyResponseInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                         imsSlotId);
    }

    return 0;
}

int radio::rttTextReceiveInd(int slotId,
                             int indicationType, int token, RIL_Errno e,
                             void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        char **resp = (char **) response;
        int numStrings = responselen / sizeof(char *);
        if(numStrings < 3) {
            RLOGE("rttTextReceiveInd: items length invalid, slotId = %d",
                                                              imsSlotId);
            return 0;
        }

        int callId = atoi(resp[0]);
        int length = atoi(resp[1]);
        hidl_string text = convertCharPtrToHidlString(resp[2]);

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->rttTextReceive(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, length, text);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        RLOGE("rttTextReceiveInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                      imsSlotId);
    }

    return 0;
}

int radio::gttCapabilityIndicationInd(int slotId,
                                      int indicationType, int token, RIL_Errno e,
                                      void *response, size_t responselen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        int *resp = (int *) response;
        int numStrings = responselen / sizeof(int);
        if(numStrings < 5) {
            RLOGE("gttCapabilityIndicationInd: items length invalid, slotId = %d",
                                                                       imsSlotId);
            return 0;
        }

        int callId = ((int32_t *) resp)[0];
        int localCap = ((int32_t *) resp)[1];
        int remoteCap = ((int32_t *) resp)[2];
        int localStatus = ((int32_t *) resp)[3];
        int remoteStatus = ((int32_t *) resp)[4];

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->gttCapabilityIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, localCap, remoteCap, localStatus, remoteStatus);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        RLOGE("gttCapabilityIndicationInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                               imsSlotId);
    }

    return 0;
}

int radio::rttModifyRequestReceiveInd(int slotId,
                                      int indicationType, int token, RIL_Errno e,
                                      void *response, size_t responselen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        int *resp = (int *) response;
        int numStrings = responselen / sizeof(int);
        if(numStrings < 2) {
            RLOGE("rttModifyRequestReceiveInd: items length invalid, slotId = %d",
                                                                       imsSlotId);
            return 0;
        }

        int callId = ((int32_t *) resp)[0];
        int type = ((int32_t *) resp)[1];

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->rttModifyRequestReceive(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, type);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        RLOGE("rttModifyRequestReceiveInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                               imsSlotId);
    }

    return 0;
}

int radio::digitsLineIndicationInd(int slotId,
                                   int indicationType, int token, RIL_Errno e,
                                   void *response, size_t responselen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationDigits != NULL) {

        char **resp = (char **) response;
        int numStrings = responselen / sizeof(char *);
        if(numStrings < 5) {
            RLOGE("digitsLineIndicationInd: items length invalid, slotId = %d",
                                                              imsSlotId);
            return 0;
        }

        int accountId = atoi(resp[0]);
        int serial = atoi(resp[1]);
        int msisdnNum = atoi(resp[2]);

        if(numStrings < 3 + msisdnNum * 2) {
            RLOGE("digitsLineIndicationInd: precise items length invalid, slotId = %d",
                                                              imsSlotId);
            return 0;
        }

        hidl_vec<hidl_string> msisdns;
        msisdns.resize(msisdnNum);

        hidl_vec<bool> isActive;
        isActive.resize(msisdnNum);

        int index, respIdx = 3;
        for (index = 0; index < msisdnNum; index ++) {
            msisdns[index] = convertCharPtrToHidlString(resp[respIdx ++]);
            isActive[index] = atoi(resp[respIdx ++]);
        }

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationDigits->digitsLineIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 accountId, serial, msisdnNum, msisdns, isActive);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        RLOGE("digitsLineIndicationInd: radioService[%d]->mRadioIndicationDigits == NULL",
                                                                               imsSlotId);
    }

    return 0;
}


extern "C" void radio::registerOpService(RIL_RadioFunctions *callbacks,
                                         android::CommandInfo *commands) {

    using namespace android::hardware;
    int simCount = 1;
    const char *serviceNames[] = {
            android::RIL_getServiceName()
            , RIL2_SERVICE_NAME
            , RIL3_SERVICE_NAME
            , RIL4_SERVICE_NAME
            };
    const char *imsServiceNames[] = {
            IMS_WWOP_RIL1_SERVICE_NAME
            , IMS_WWOP_RIL2_SERVICE_NAME
            , IMS_WWOP_RIL3_SERVICE_NAME
            , IMS_WWOP_RIL4_SERVICE_NAME
            };

    simCount = getSimCount();
    configureRpcThreadpool(1, true /* callerWillJoin */);
    for (int i = 0; i < simCount; i++) {
        pthread_rwlock_t *radioServiceRwlockPtr = getRadioServiceRwlock(i);
        int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
        assert(ret == 0);

        radioService[i] = new RadioImpl;
        radioService[i]->mSlotId = i;
        RLOGI("registerOpService: starting IRadioOp %s", serviceNames[i]);
        android::status_t status = radioService[i]->registerAsService(serviceNames[i]);

        int imsSlot = toImsSlot(i);
        radioService[imsSlot] = new RadioImpl;
        radioService[imsSlot]->mSlotId = imsSlot;
        RLOGD("radio::registerService: starting IMS IRadioOp %s, slot = %d, realSlot = %d",
              imsServiceNames[i], radioService[imsSlot]->mSlotId, imsSlot);

        // Register IMS Radio Stub
        status = radioService[imsSlot]->registerAsService(imsServiceNames[i]);
        RLOGD("radio::registerService IRadio for IMS status:%d", status);
        ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
        assert(ret == 0);
    }

    s_vendorFunctions = callbacks;
    s_commands = commands;
}
