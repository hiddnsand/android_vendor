package com.mediatek.op01.phone.plugin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.android.phone.PhoneUtils;

import android.util.Log;
import com.mediatek.settings.TelephonyUtils;
import com.mediatek.internal.telephony.MtkSubscriptionManager;

//import com.mediatek.common.PluginImpl;
import com.mediatek.phone.ext.DefaultSsRoamingServiceExt;

//@PluginImpl(interfaceName = "com.mediatek.phone.ext.ISsRoamingServiceExt")
public class Op01SsRoamingServiceExt extends DefaultSsRoamingServiceExt {
    private static final String LOG_TAG = "Op01SSRoamingServiceExt";
    private Context mContext;

    private static final String ACTION_SUPPLEMENTARY_SERVICE_ROAMING_TEST
            = "android.intent.action.ACTION_SUPPLEMENTARY_SERVICE_ROAMING_TEST";

    private static final String EXTRA_PHONE_ID = "phoneId";
    private static final String EXTRA_SUB_ID = "subId";
    private static final String EXTRA_DISPALY_NAME = "displayName";

    public Op01SsRoamingServiceExt() {

    }

    /** register receiver ss roaming receiver.
     * @param context context
     */
    @Override
    public void registerSsRoamingReceiver(Context context) {
        //if (!SystemProperties.get("persist.mtk_ims_support").equals("1") ||
        //        !SystemProperties.get("persist.mtk_volte_support").equals("1")) {
        //    return;
        //}
        mContext = context;
        final IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_SUPPLEMENTARY_SERVICE_ROAMING_TEST);
        context.registerReceiver(mBroadcastReceiver, filter);
    }

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            final String action = intent.getAction();
            Log.d(LOG_TAG, "onReceive, action = " + action);
            if (action.equals(ACTION_SUPPLEMENTARY_SERVICE_ROAMING_TEST)) {
                int phoneId = intent.getIntExtra(EXTRA_PHONE_ID, -1);

                Log.d(LOG_TAG, "onReceive, phoneId = " + phoneId);

                phoneId = getValidPhoneId(phoneId);
                int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(phoneId);
                String displayName = PhoneUtils.getSubDisplayName(subId);
                boolean shouldShowDialog = TelephonyUtils.shouldShowOpenMobileDataDialog(
                    context, subId);

                Log.d(LOG_TAG, "onReceive after, phoneId = " + phoneId + " ,subId = " + subId
                    + " ,displayName = " + displayName);
                if (shouldShowDialog) {
                    Intent dialogIntent = new Intent("com.mediatek.op01.plugin.action.roamingtips");
                    dialogIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    dialogIntent.putExtra(EXTRA_SUB_ID, subId);
                    dialogIntent.putExtra(EXTRA_DISPALY_NAME, displayName);
                    context.startActivity(dialogIntent);
                }
            }
        }
    };

    private static int getValidPhoneId(int phoneId) {
        if (phoneId >= 0 && phoneId < TelephonyManager.getDefault().getPhoneCount()) {
            return phoneId;
        }
        return 0;
    }

}
