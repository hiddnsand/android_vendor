package com.mediatek.op08.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.phone.CallFeaturesSetting;
import com.android.ims.ImsManager;
import com.mediatek.ims.WfcReasonInfo;
import com.mediatek.telephony.MtkTelephonyManagerEx;
import com.mediatek.phone.ext.DefaultCallFeaturesSettingExt;


/**
 * Plugin implementation for CallfeatureSettings.
 */
public class Op08CallFeaturesSettingExt extends DefaultCallFeaturesSettingExt {
    private static final String TAG = "Op08CallFeaturesSettingExt";
    private static final String SETTINGS_CHANGED_OR_SS_COMPLETE
            = "com.mediatek.op.telephony.SETTINGS_CHANGED_OR_SS_COMPLETE";
    private static final int MESSAGE_SET_SS = 1;

    private Context mContext;
    private Preference mWfcPreference = null;
    private WfcSummary mWfcSummary;
    private PreferenceActivity mPreferenceActivity;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("@M_" + TAG, "onReceive:" + action);
            if (!ImsManager.isWfcEnabledByPlatform(mContext)) {
                Log.d("@M_" + TAG, "WFC not present in platform");
                return;
            }
            if (action.equals(WfcSummary.ACTION_WFC_SUMMARY_CHANGE)) {
                if (mWfcPreference != null) {
                    mWfcPreference.setSummary(intent.getStringExtra(WfcSummary.EXTRA_SUMMARY));
                }
            }
        }
    };

    /** Constructor.
     * @param context context
     */
    public Op08CallFeaturesSettingExt(Context context) {
        super();
        mContext = context;
        mWfcSummary = new WfcSummary(context);
    }

    @Override
    /**
    * Sends to intent to start re registration
    * @param context Context.
    * @param msg Message argument2 if is SET or GET/
    */
    public void resetImsPdnOverSSComplete(Context context, int msg) {
        Log.d(TAG, "resetImsPdnOverSSComplete");
        if (msg == MESSAGE_SET_SS) {
            Intent intent = new Intent(SETTINGS_CHANGED_OR_SS_COMPLETE);
            context.sendBroadcast(intent);
            Log.d(TAG, "Intent Broadcast " + SETTINGS_CHANGED_OR_SS_COMPLETE);
        }
    }

    @Override
    public void initOtherCallFeaturesSetting(PreferenceActivity activity) {
        Log.d(TAG, "initOtherCallFeaturesSetting");
        mPreferenceActivity = activity;
        if (ImsManager.isWfcEnabledByPlatform(mContext)) {
            mWfcPreference = ((CallFeaturesSetting) activity).getPreferenceScreen()
                                        .findPreference("button_wifi_calling_settings_key");;
        }
    }

    @Override
    /** Called from onPause
     * @param activity
     * @return
     */
    public void onCallFeatureSettingsEvent(int event) {
        Log.d("@M_" + TAG, "event:" + event);
        Log.d("@M_" + TAG, "wfcPreference:" + mWfcPreference);
        switch (event) {
            case DefaultCallFeaturesSettingExt.RESUME:
                    IntentFilter filter = new IntentFilter(WfcSummary.ACTION_WFC_SUMMARY_CHANGE);
                    mContext.registerReceiver(mReceiver, filter);
                    mWfcSummary.onResume();

                if (ImsManager.isWfcEnabledByPlatform(mContext) && mWfcPreference != null) {
                    boolean isWfcRegistered = MtkTelephonyManagerEx.getDefault()
                    .isWifiCallingEnabled(SubscriptionManager.getDefaultVoiceSubscriptionId());
                    Log.d(TAG, "[onResume]isWfcRegistered:" + isWfcRegistered);
                    int wfcState = WfcReasonInfo.CODE_WFC_DEFAULT;
                    if (isWfcRegistered) {
                        wfcState = WfcReasonInfo.CODE_WFC_SUCCESS;
                    }
                    mWfcPreference.setSummary(mWfcSummary.getWfcSummaryText(wfcState));
                }
                break;
            case DefaultCallFeaturesSettingExt.PAUSE:
                /* No need to check isWfcEnabledByPlatform in de-reg because
                 * this wil be executed in case when WFC is disabled on the fly
                 * due to Dynamic IMS Switch feature
                 */
                mContext.unregisterReceiver(mReceiver);
                mWfcSummary.onPause();
                break;
            default:
                break;
        }
    }

    @Override
    /** get operator specific customized summary for WFC button.
     * Used in CallFeatureSettings
     * @param defaultSummaryResId default summary res id
     * @return summary string to be displayed
     */
    public String getWfcSummary(Context context, int defaultSummaryResId) {
        if (!ImsManager.isWfcEnabledByPlatform(context)) {
            return context.getResources().getString(defaultSummaryResId);
        }
        boolean isWfcRegistered = MtkTelephonyManagerEx.getDefault()
              .isWifiCallingEnabled(SubscriptionManager.getDefaultVoiceSubscriptionId());
        Log.d(TAG, "[getWfcSummary]isWfcRegistered:" + isWfcRegistered);
        int wfcState = WfcReasonInfo.CODE_WFC_DEFAULT;
        if (isWfcRegistered) {
           wfcState = WfcReasonInfo.CODE_WFC_SUCCESS;
        }
        return mWfcSummary.getWfcSummaryText(wfcState);
    }
}
