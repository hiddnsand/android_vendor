/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.tests;

import junit.framework.TestSuite;

public class EmFunctionalTestRunner extends JUnitInstrumentationTestRunner {
    @Override
    public TestSuite getAllTests() {
        TestSuite tests = new TestSuite();

       tests.addTestSuite(InitAutoTest.class);
        // 0. EM main arch test
        tests.addTestSuite(EngineerModeReceiverTest.class);
        tests.addTestSuite(EngineerModeTest.class);

        // EM telephony test case
        tests.addTestSuite(CipTest.class);
        tests.addTestSuite(AutoAnswerTest.class);
        tests.addTestSuite(ModemCategoryTest.class);
        // tests.addTestSuite(CfuActivityTest.class); // Phone process
        // tests.addTestSuite(FastDormancyTest.class); // Phone process
        // tests.addTestSuite(BandModeSimSelectTest.class);
        // tests.addTestSuite(BandSelectTest.class);
        // tests.addTestSuite(RatSimSelectTest.class);
        // tests.addTestSuite(NetworkInfoTest.class);
        // tests.addTestSuite(NetworkInformationTest.class);
        // tests.addTestSuite(MobileDataPreferTest.class);

        // 1. EM hardware test case
        tests.addTestSuite(NonSleepModeTest.class);
//        tests.addTestSuite(ChargeBatteryTest.class);
        tests.addTestSuite(MemoryTest.class);
        tests.addTestSuite(UsbTest.class);
        tests.addTestSuite(PowerTest.class);
        tests.addTestSuite(AudioTest.class);
        tests.addTestSuite(TouchScreenTest.class);
        tests.addTestSuite(SensorTest.class);
        // tests.addTestSuite(DesenseTest.class);
        // tests.addTestSuite(MsdcTest.class);
        // tests.addTestSuite(IoListTest.class);

        // tests.addTestSuite(CpuStressTestTest.class);
        // tests.addTestSuite(CameraMclkTest.class);
        // tests.addTestSuite(CameraNewTest.class);
        // tests.addTestSuite(CameraTest.class);

        // 2. EM connectivity test case

        tests.addTestSuite(WiFiTest.class);
        tests.addTestSuite(BtListTest.class);
        tests.addTestSuite(HqaNfcTest.class);
        tests.addTestSuite(WifiCtiaTest.class);


        // 4. EM log and debug test case
        tests.addTestSuite(ElogTest.class);
        tests.addTestSuite(BatteryLogTest.class);
        // 5. EM location test case
        tests.addTestSuite(CWTestTest.class);

        // 6.EM others test case
//        tests.addTestSuite(SettingsFontSizeTest.class);
   tests.addTestSuite(EndAutoTest.class);
        return tests;
    }
}
