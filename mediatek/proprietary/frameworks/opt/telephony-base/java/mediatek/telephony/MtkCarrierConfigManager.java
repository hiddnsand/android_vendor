/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package mediatek.telephony;

import android.os.PersistableBundle;
import android.telephony.CarrierConfigManager;

public class MtkCarrierConfigManager {
    private final static String LOG_TAG = "MtkCarrierConfigMgr";

    /**
     * Add Mediatek proprietary key here with "MTK_KEY" leading.
     * For example:
     *   public static final String
     *       MTK_KEY_EXAMPLE_BOOL = "mtk_example_bool";
     */

     /**
     * @M: Added for config volte conference enhanced
     * Values: true if volte conference enhanced enable, else false
     * @hide
     */
    public static final String MTK_KEY_VOLTE_CONFERENCE_ENHANCED_ENABLE_BOOL =
            "mtk_volte_conference_enhanced_enable_bool";

    /**
     * @M: Added for allow one video call only
     * Values: true if allow one video call only, else false
     * @hide
     */
    public static final String MTK_KEY_ALLOW_ONE_VIDEO_CALL_ONLY_BOOL =
            "mtk_allow_one_video_call_only_bool";

    /**
     * International roaming exceptions that are defined by the carrier
     * @hide
     */
    public static final String KEY_CARRIER_INTERNATIONAL_ROAMING_EXCEPTION_LIST_STRINGS =
            "carrier_international_roaming_exception_list_strings";

    /**
     * Determine the cdma sms 7bit encoding type by carrier config.
     */
    public static final String MTK_KEY_CDMA_SMS_7_BIT_ENCODING_TYPE_INT =
            "mtk_cdma_sms_7bit_encoding_type_int";

    /**
     * Wait missing segment time in EMS handling.
     */
    public static final String MTK_KEY_EMS_WAITING_MISSING_SEGMENT_TIME_INT =
        "mtk_ems_waiting_missing_segment_time";

    /**
     * Determine whether to remove WFC preference mode or not.
     * @hide
     */
    public static final String MTK_KEY_WFC_REMOVE_PREFERENCE_MODE_BOOL =
        "mtk_wfc_remove_preference_mode_bool";

   /**
    * Added for showing dialog when user access supplymentary services in Call Settings when
    * Mobile data disabled.
    * Values: true, shows the dialog to user to open mobile data else false
    * @hide
    */
    public static final String MTK_KEY_SHOW_OPEN_MOBILE_DATA_DIALOG_BOOL
            = "mtk_show_open_mobile_data_dialog_bool";

   /** Display Caller id settings menu if true
    * @hide
    */
    public static final String MTK_KEY_SHOW_CLIR_SETTING_BOOL = "mtk_show_clir_setting_bool";

    /**
     * @M: Default enhanced_4g_mode_enabled value
     * Values: true, if the default enhanced_4g_mode_enabled value is on else false
     * @hide
     */
    public static final String
            MTK_KEY_DEFAULT_ENHANCED_4G_MODE_BOOL = "mtk_default_enhanced_4g_mode_bool";

   /**
    * @M: Added for check if need support ALFMS00872039:The device shall use the
    * ADMINISTRATIVE APN in LTE area for the UICC/OTA data connection
    * @hide
    */
    public static final String MTK_KEY_USE_ADMINISTRATIVE_APN_BOOL = "mtk_use_administrative_apn_bool";

   /**
    * @M: Added for VDF Settings UX customization on the basis of SIM present
    * If value = 0, mode to change in WFC preferecne in Wireless Settings
    * if value = 1, mode to change WFC mode
    * @hide
    */
    public static final String MTK_KEY_WFC_SETTINGS_UX_TYPE_INT = "mtk_wfc_settings_ux_type_int";

    /**
    * @M: Added for VDF Settings UX customization on the basis of SIM present
    * Values as per ImsConfig.WfcModeFeatureValueConstants.
    * @hide
    */
    public static final String MTK_KEY_DEFAULT_WFC_MODE_INT = "mtk_default_wfc_mode_int";

    /**
    * @M: Added to indicate whether WFC provisioning via OMACP supported or not
    * Values: true, if IMS provisioning via OMACP supported else false
    * @hide
    */
    public static final String MTK_KEY_WFC_PROV_SUPPORTED_BOOL =
                                    "mtk_wfc_provisioning_supported_bool";

    /**
     * Added to check for Video Call support in Supplymentary services
     * @hide
     */
    public static final String MTK_KEY_SUPPORT_VT_SS_BOOL = "mtk_support_vt_ss_bool";

   /**
    * Added to support the call pull in dialer
    * Values: true, if Call pull is supported else false
    * @hide
    */
    public static final String MTK_KEY_DIALER_CALL_PULL_BOOL = "mtk_dialer_call_pull_bool";

    /**
     * Add for support change to one way video or not, default true.
     * Values: Default true, if not support change to one way video set false.
     * @hide
     */
    public static final String MTK_KEY_ALLOW_ONE_WAY_VIDEO_BOOL =
            "mtk_allow_one_way_video_bool";

    /**
     * Add for support cancel video update during the session progress.
     * Values: true if allow cancel the upgrade request.
     * @hide
     */
    public static final String MTK_KEY_ALLOW_CANCEL_VIDEO_UPGRADE_BOOL =
            "mtk_allow_cancel_video_upgrade_bool";

    /**
    * @M: IMS data retry requirements, including PCSC and RA fail.
    * Modem requires this information to do corresponding actions.
    * Default value is false.
    * @hide
    */
    public static final String KEY_IMS_PDN_SYNC_FAIL_CAUSE_TO_MODEM_BOOL =
            "ims_pdn_sync_fail_cause_to_modem_bool";

    /**
     * @M: Operator ID
     * AP requires this information to know which operator SIM card is inserted.
     * Default value is 0(OM or no SIM inserted).
     * @hide
     */
    public static final String KEY_OPERATOR_ID_INT = "operator_id";

    /**
     * Determined if domestic roaming is enabled only by mobile data setting
     * Default value is false
     * @hide
     */
    public static final String KEY_DOMESTIC_ROAMING_ENABLED_ONLY_BY_MOBILE_DATA_SETTING =
            "mtk_domestic_roaming_enabled_only_by_mobile_data_setting";

    /**
     * If the key is defined, the domestic roaming enabled by mobile data setting needs to verify
     * if the current camped network is listed in the key's valuse.
     * Default value is null
     * @hide
     */
    public static final String KEY_DOMESTIC_ROAMING_ENABLED_ONLY_BY_MOBILE_DATA_SETTING_CHECK_NW_PLMN =
            "mtk_domestic_roaming_enabled_only_by_mobile_data_setting_check_nw_plmn";

    /**
     * Determined if international roaming is enabled only by roaming data setting
     * Default value is false
     * @hide
     */
    public static final String KEY_INTL_ROAMING_ENABLED_ONLY_BY_ROAMING_DATA_SETTING =
            "mtk_intl_roaming_enabled_only_by_roaming_data_setting";

    /**
     * Determined if there are unique domestic and international roaming settings for both
     * domestic and international roaming, respectively. And the AOSP's default roaming
     * setting is not used.
     * Default value is false
     * @hide
     */
    public static final String KEY_UNIQUE_SETTINGS_FOR_DOMESTIC_AND_INTL_ROAMING =
            "mtk_unique_settings_for_domestic_and_intl_roaming";

    /**
     * Support UT over GSM.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_GSM_UT_SUPPORT =
            "mtk_carrier_ss_gsm_ut_support";

    /**
     * Not support xcap.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_NOT_SUPPORT_XCAP =
            "mtk_carrier_ss_not_support_xcap";

    /**
     * Support terminal base CLIR.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_TB_CLIR =
            "mtk_carrier_ss_tb_clir";

    /**
     * Support IMS network call waiting.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_IMS_NW_CW =
            "mtk_carrier_ss_ims_nw_cw";

    /**
     * Support response http 409 code.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_ENABLE_XCAP_HTTP_RESPONSE_409 =
            "mtk_carrier_ss_enable_xcap_http_response_409";

    /**
     * Support response http 404 code.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_TRANSFER_XCAP_404 =
            "mtk_carrier_ss_transfer_xcap_404";

    /**
     * Not support CLIR.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_NOT_SUPPORT_CALL_IDENTITY =
            "mtk_carrier_ss_not_support_call_identity";

    /**
     * Re-register for call forwarding.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_RE_REGISTER_FOR_CF =
            "mtk_carrier_ss_re_register_for_cf";

    /**
     * Save call forwarding number.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_SUPPORT_SAVE_CF_NUMBER =
            "mtk_carrier_ss_support_save_cf_number";

    /**
     * Query CFU after set.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_QUERY_CFU_AGAIN_AFTER_SET =
            "mtk_carrier_ss_query_cfu_again_after_set";

    /**
     * Not support OCB.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_NOT_SUPPORT_OCB =
            "mtk_carrier_ss_not_support_ocb";

   /**
    * Added to support the ct volte check in cellconnMgr
    * Values: true, if ct volte is support else false
    * @hide
    */
    public static final String MTK_KEY_CT_VOLTE_STATUS_BOOL = "mtk_ct_volte_status_bool";

    /**
     * Not support WFC for supplementary.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_NOT_SUPPORT_WFC_UT =
            "mtk_carrier_ss_not_support_wfc_ut";

    /**
     * Need to check data enable for supplemtary service.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_NEED_CHECK_DATA_ENABLE =
            "mtk_carrier_ss_need_check_data_enable";


    /**
     * Need to check data roaming for supplemtary service.
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SS_NEED_CHECK_DATA_ROAMING =
            "mtk_carrier_ss_need_check_data_roaming";

    /**
     * @M: Emergency bearer management policy
     * The priority to deactivate PDN before EMERGENCY PDN activation if PDN poll is full.
     * @hide
     */
    public static final String KEY_EMERGENCY_BEARER_MANAGEMENT_POLICY =
            "emergency_bearer_management_policy";

    /**
     * Added to support settings for Sprint Roaming and bar settings.
     * @hide
     */
     public static final String MTK_KEY_ROAMING_BAR_GUARD_BOOL =
            "mtk_key_roaming_bar_guard_bool";

    /**
     * Support nourth America high priority CLIR such as *82
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_NOURTH_AMERICA_HIGH_PRIORITY_CLIR_PREFIX_SUPPORTED =
            "mtk_carrier_nouth_america_high_priority_clir_prefix_supported";

    /**
     * Need to swap conference to foreground before merge
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_SWAP_CONFERENCE_TO_FOREGROUND_BEFORE_MERGE =
            "mtk_carrier_swap_conference_to_foreground_before_merge";

    /**
     * Need to update dialing address from PAU
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_UPDATE_DIALING_ADDRESS_FROM_PAU =
            "mtk_carrier_update_dialing_address_from_pau";

    /**
     * Need to turn off wifi before E911
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_TURN_OFF_WIFI_BEFORE_E911 =
            "mtk_carrier_turn_off_wifi_before_e911";

    /**
     * IMS ECBM supported
     * Default value is false
     * @hide
     */
    public static final String MTK_KEY_CARRIER_IMS_ECBM_SUPPORTED =
            "mtk_carrier_ims_ecbm_supported";

    /**
     * @M: No need to show roaming icon.
     * Need to show roaming icon on status bar or not
     * Default value is true
     */
    public static final String MTK_KEY_CARRIER_NEED_SHOW_ROAMING_ICON =
            "mtk_key_carrier_need_show_roaming_icon";


    /**
     * Constructor function.
     */
    public MtkCarrierConfigManager() {
    }

    public static void putDefault(PersistableBundle sDefaults) {
        /* example: sDefaults.putBoolean(MTK_KEY_EXAMPLE_BOOL, true); */

        /* Set default value of volte, vilte and wfc to true as don't care in ImsManager */
        sDefaults.putBoolean(CarrierConfigManager.KEY_CARRIER_VOLTE_AVAILABLE_BOOL, true);
        sDefaults.putBoolean(CarrierConfigManager.KEY_CARRIER_VT_AVAILABLE_BOOL, true);
        sDefaults.putBoolean(CarrierConfigManager.KEY_CARRIER_WFC_IMS_AVAILABLE_BOOL, true);

        sDefaults.putBoolean(MTK_KEY_VOLTE_CONFERENCE_ENHANCED_ENABLE_BOOL, false);
        sDefaults.putBoolean(MTK_KEY_ALLOW_ONE_VIDEO_CALL_ONLY_BOOL, false);
        sDefaults.putStringArray(KEY_CARRIER_INTERNATIONAL_ROAMING_EXCEPTION_LIST_STRINGS,
                new String[]{""});
        /* set the default 7bit encoding as GSM 7bit for CDMA SMS */
        sDefaults.putInt(MTK_KEY_CDMA_SMS_7_BIT_ENCODING_TYPE_INT, 0x9);
        sDefaults.putInt(MTK_KEY_EMS_WAITING_MISSING_SEGMENT_TIME_INT, 60);
        // M: Default enhanced_4g_mode_enabled value
        sDefaults.putBoolean(MTK_KEY_DEFAULT_ENHANCED_4G_MODE_BOOL, true);
        // Used for BIP to check if need support KDDI feature
        sDefaults.putBoolean(MTK_KEY_USE_ADMINISTRATIVE_APN_BOOL, false);
        // M: WFC List Preference mode
        sDefaults.putBoolean(MTK_KEY_WFC_REMOVE_PREFERENCE_MODE_BOOL, false);
        /// M: config whether show CLIR Settings
        sDefaults.putBoolean(MTK_KEY_SHOW_CLIR_SETTING_BOOL, true);
        /// M: config whether show enable data dialog when doing SS
        sDefaults.putBoolean(MTK_KEY_SHOW_OPEN_MOBILE_DATA_DIALOG_BOOL, true);
        sDefaults.putInt(MTK_KEY_WFC_SETTINGS_UX_TYPE_INT, -1);
        /* Default value is in sync with ImsConfig.WfcModeFeatureValueConstants.WIFI_PREFERRED*/
        sDefaults.putInt(MTK_KEY_DEFAULT_WFC_MODE_INT, 2);
        sDefaults.putBoolean(MTK_KEY_WFC_PROV_SUPPORTED_BOOL, false);
        //M: VT support for SS
        sDefaults.putBoolean(MTK_KEY_SUPPORT_VT_SS_BOOL, false);
        //M: For Call pull support in Call logs
        sDefaults.putBoolean(MTK_KEY_DIALER_CALL_PULL_BOOL, false);
        // VILTE support change to one way video.
        sDefaults.putBoolean(MTK_KEY_ALLOW_ONE_WAY_VIDEO_BOOL, true);
        // VILTE support cancel the video upgrade request.
        sDefaults.putBoolean(MTK_KEY_ALLOW_CANCEL_VIDEO_UPGRADE_BOOL, false);
        // M: IMS data retry requirements
        sDefaults.putBoolean(KEY_IMS_PDN_SYNC_FAIL_CAUSE_TO_MODEM_BOOL, false);
        // M: Operator ID
        sDefaults.putInt(KEY_OPERATOR_ID_INT, 0);

        sDefaults.putBoolean(KEY_DOMESTIC_ROAMING_ENABLED_ONLY_BY_MOBILE_DATA_SETTING, false);
        sDefaults.putStringArray(
                KEY_DOMESTIC_ROAMING_ENABLED_ONLY_BY_MOBILE_DATA_SETTING_CHECK_NW_PLMN, null);
        sDefaults.putBoolean(KEY_INTL_ROAMING_ENABLED_ONLY_BY_ROAMING_DATA_SETTING, false);
        sDefaults.putBoolean(KEY_UNIQUE_SETTINGS_FOR_DOMESTIC_AND_INTL_ROAMING, false);

        // M: For Supplementary Service. @{
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_GSM_UT_SUPPORT, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_NOT_SUPPORT_XCAP, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_TB_CLIR, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_IMS_NW_CW, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_ENABLE_XCAP_HTTP_RESPONSE_409, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_TRANSFER_XCAP_404, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_NOT_SUPPORT_CALL_IDENTITY, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_RE_REGISTER_FOR_CF, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_SUPPORT_SAVE_CF_NUMBER, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_QUERY_CFU_AGAIN_AFTER_SET, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_NOT_SUPPORT_OCB, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_NOT_SUPPORT_WFC_UT, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_NEED_CHECK_DATA_ENABLE, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SS_NEED_CHECK_DATA_ROAMING, false);
        // M: For Supplementary Service. @}
        // M: Emergency bearer management policy
        sDefaults.putStringArray(KEY_EMERGENCY_BEARER_MANAGEMENT_POLICY,
                new String[]{""});
        //M: For CT VoLTE status check in CellConnMgr
        sDefaults.putBoolean(MTK_KEY_CT_VOLTE_STATUS_BOOL, false);
        //M: For Sprint roaming settings add bar settings
        sDefaults.putBoolean(MTK_KEY_ROAMING_BAR_GUARD_BOOL, false);
        // M: For IMS Call Control. @{
        sDefaults.putBoolean(
                MTK_KEY_CARRIER_NOURTH_AMERICA_HIGH_PRIORITY_CLIR_PREFIX_SUPPORTED, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_SWAP_CONFERENCE_TO_FOREGROUND_BEFORE_MERGE, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_UPDATE_DIALING_ADDRESS_FROM_PAU, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_TURN_OFF_WIFI_BEFORE_E911, false);
        sDefaults.putBoolean(MTK_KEY_CARRIER_IMS_ECBM_SUPPORTED, false);
        // @}

        // M: need to show roaming icon or not
        sDefaults.putBoolean(MTK_KEY_CARRIER_NEED_SHOW_ROAMING_ICON, true);
    }
}
