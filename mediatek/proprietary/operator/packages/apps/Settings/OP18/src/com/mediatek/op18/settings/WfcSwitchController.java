package com.mediatek.op18.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuff;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.mediatek.ims.internal.MtkImsManagerEx;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;


/**
 * Class to support operator customizations for WFC settings.
 */
public class WfcSwitchController {

    private static final String TAG = "Op18WfcSwitchController";
    private static final String AOSP_SETTING_WFC_PREFERENCE = "wifi_calling_settings";
    private static final String OP18_WFC_PREFERENCE_KEY = "op18_wfc_pref_switch";
    private static WfcSwitchController sController = null;

    private Context mPluginContext;
    private Context mAppContext;
    private android.support.v14.preference.SwitchPreference mWfcSwitchSettings = null;
    private android.support.v7.preference.PreferenceScreen mPreferenceScreenSettings = null;

    private WfcSwitchController(Context context) {
       mPluginContext = context;
    }

    /** Returns instance of Op18WfcSettings.
         * @param context context
         * @return Op18WfcSettings
         */
    public static WfcSwitchController getInstance(Context context) {

        if (sController == null) {
            sController = new WfcSwitchController(context);
        }
        return sController;
    }

    public void customizedWfcPreference(Context context, Object preferenceScreen) {
        mAppContext = context;
        android.support.v7.preference.Preference wfcPreferenceSettings = null;
        if (!init(preferenceScreen)) {
            return;
        }
       if (mPreferenceScreenSettings != null) {
            wfcPreferenceSettings =
                    (android.support.v7.preference.Preference) mPreferenceScreenSettings
                    .findPreference(AOSP_SETTING_WFC_PREFERENCE);
            Log.d(TAG, "wfcSettingsPreference: " + wfcPreferenceSettings);
        }

        CharSequence title = null;
        int order = 0;
        if (wfcPreferenceSettings != null) {
            mPreferenceScreenSettings.removePreference(wfcPreferenceSettings);
            title = wfcPreferenceSettings.getTitle();
            order = wfcPreferenceSettings.getOrder();
        }

        checkAndAddWfcSwitch(title, order);

        int callState = Settings.Global.getInt(context.getContentResolver(),
                    Op18WfcSettings.CALL_STATE, Op18WfcSettings.CALL_STATE_IDLE);
        Log.d(TAG, "call_state: " + callState);
        updateWfcSwitchState(callState);
    }

    private boolean init(Object preferenceScreen) {
        if (preferenceScreen instanceof android.support.v7.preference.PreferenceScreen) {
            mPreferenceScreenSettings =
                    (android.support.v7.preference.PreferenceScreen) preferenceScreen;
            mWfcSwitchSettings =
                    (android.support.v14.preference.SwitchPreference) mPreferenceScreenSettings
                    .findPreference(OP18_WFC_PREFERENCE_KEY);
            Log.d(TAG, "mPreferenceScreenSettings: " + mPreferenceScreenSettings);
        } else {
            Log.d(TAG, "invalid class of preferenceScreen: " + preferenceScreen);
            return false;
        }
        return true;
    }

    private void checkAndAddWfcSwitch(CharSequence title, int order) {
        Log.d(TAG, "mWfcSwitchSettings: " + mWfcSwitchSettings);
        if (mWfcSwitchSettings == null) {
            if (mPreferenceScreenSettings != null) {
                mWfcSwitchSettings =
                        new android.support.v14.preference.SwitchPreference(mAppContext);
                mWfcSwitchSettings
                        .setOnPreferenceChangeListener(new WfcSwitchListenerForSettings());
                mWfcSwitchSettings.setKey(OP18_WFC_PREFERENCE_KEY);
                mWfcSwitchSettings.setTitle(title);
                mWfcSwitchSettings.setOrder(order);
                Drawable wfcIcon = mPluginContext.getResources()
                                              .getDrawable(R.drawable.ic_wifi_calling);
                wfcIcon.setColorFilter(mPluginContext.getResources()
                     .getColor(R.color.dialtacts_secondary_text_color), PorterDuff.Mode.MULTIPLY);
                mWfcSwitchSettings.setIcon(wfcIcon);
                mPreferenceScreenSettings.addPreference(mWfcSwitchSettings);
            }
            ImsManager.setWfcMode(mAppContext,
                    ImsConfig.WfcModeFeatureValueConstants.WIFI_PREFERRED);
        }
    }

    public void updateWfcSwitchState(int callState) {
        // Disable switch if PS call ongoing
        if (mWfcSwitchSettings != null) {
            mWfcSwitchSettings.setEnabled(callState == Op18WfcSettings.CALL_STATE_PS ?
                    false : true);
            mWfcSwitchSettings.setChecked(ImsManager.isWfcEnabledByUser(mPluginContext));
            mWfcSwitchSettings.setSummary(ImsManager.isWfcEnabledByUser(mAppContext) == true ?
                mPluginContext.getResources().getString(R.string.enabled)
                : mPluginContext.getResources().getString(R.string.disabled));
        }
    }

    private boolean isInSwitchProcess() {
        int imsState = MtkPhoneConstants.IMS_STATE_DISABLED;
        try {
         imsState = MtkImsManagerEx.getInstance().getImsState(RadioCapabilitySwitchUtil
                         .getMainCapabilityPhoneId());
        } catch (ImsException e) {
           return false;
        }
        Log.d(TAG, "isInSwitchProcess , imsState = " + imsState);
        return imsState == MtkPhoneConstants.IMS_STATE_DISABLING
                || imsState == MtkPhoneConstants.IMS_STATE_ENABLING;
    }

    /** Need for the case when rjil specific wfc pref is to be added again after being removed.
     * For rest of wfc setting customization use customizedWfcPreference()
     * @return
     */
    public void addWfcPreference() {
        Log.d(TAG, "addWfcPreference,mPreferenceScreenSettings:" + mPreferenceScreenSettings);
        if (mPreferenceScreenSettings != null) {
            android.support.v7.preference.Preference aospWfcSettingsPreference =
                    (android.support.v7.preference.Preference) mPreferenceScreenSettings
                    .findPreference(AOSP_SETTING_WFC_PREFERENCE);
            android.support.v14.preference.SwitchPreference customizedWfcPreference =
                    (android.support.v14.preference.SwitchPreference) mPreferenceScreenSettings
                    .findPreference(OP18_WFC_PREFERENCE_KEY);
            if (aospWfcSettingsPreference == null && customizedWfcPreference == null) {
                mPreferenceScreenSettings.addPreference(mWfcSwitchSettings);
                ImsManager.setWfcMode(mAppContext,
                    ImsConfig.WfcModeFeatureValueConstants.WIFI_PREFERRED);
                mWfcSwitchSettings.setEnabled(Settings.Global
                        .getInt(mPluginContext.getContentResolver(),
                    Op18WfcSettings.CALL_STATE, Op18WfcSettings.CALL_STATE_IDLE) ==
                    Op18WfcSettings.CALL_STATE_PS ? false : true);
                mWfcSwitchSettings.setChecked(ImsManager.isWfcEnabledByUser(mAppContext));
            }
        }
    }

    /** Returns instance of Op18WfcSettings.
     * @return
     */
    public void removeWfcPreference() {
        if (mPreferenceScreenSettings != null && mWfcSwitchSettings != null) {
            mPreferenceScreenSettings.removePreference(mWfcSwitchSettings);
        }
    }

    private class WfcSwitchListenerForSettings implements
            android.support.v7.preference.Preference.OnPreferenceChangeListener {

        public WfcSwitchListenerForSettings() {
        }

        @Override
        public boolean onPreferenceChange(android.support.v7.preference.Preference preference,
                Object newValue) {
            boolean isChecked = ((Boolean) newValue).booleanValue();
            if (isChecked) {
                mWfcSwitchSettings.setChecked(false);
                displayConfirmationDialog();
                return false;
            } else {
               mWfcSwitchSettings.setSummary(mPluginContext.getResources()
                            .getString(R.string.disabled));
            }
            ImsManager.setWfcSetting(mPluginContext, isChecked);
            return true;
        }
    }

    private void displayConfirmationDialog() {
        new AlertDialog.Builder(mAppContext)
            .setCancelable(true)
            .setTitle(mPluginContext.getText(R.string.confirmation_dialog_title))
            .setMessage(mPluginContext.getText(R.string.confirmation_dialog_message))
            .setPositiveButton(mPluginContext.getText(R.string.enable), new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Log.d(TAG, "Enabling WFC switch");
                    if (isInSwitchProcess()) {
                       Toast.makeText(mPluginContext, R.string.Switch_not_in_use_string,
                                Toast.LENGTH_SHORT).show();
                       return;
                    }
                    if (mWfcSwitchSettings != null) {
                        mWfcSwitchSettings.setChecked(true);
                        mWfcSwitchSettings.setSummary(mPluginContext.getResources()
                                .getString(R.string.enabled));
                    }
                    ImsManager.setWfcSetting(mPluginContext, true);
                }
            })
            .setNegativeButton(mPluginContext.getText(R.string.cancel), new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                }
            })
            .show();
    }
}
