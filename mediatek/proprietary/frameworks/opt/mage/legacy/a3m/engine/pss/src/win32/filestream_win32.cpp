/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/**
 * PSS FileStreamSource implementation
 *
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <fstream>                              /* std::ifstream, ofstream    */
#include <filestream_win32.h>               /* This class's API           */

namespace a3m
{

  /****************************************************************************
   * FileStreamSource private types and definitions
   ****************************************************************************/
  namespace
  {
    /*
     * Concrete Stream class for reading data from disk
     */
    class InFileStream : public Stream
    {
    public:
      /*
       * Constructor
       *
       * Constructs a std::ifstream with the given name and initialises the
       * size of the stream.
       */
      InFileStream(const A3M_CHAR8* name) :
        m_size(0)
      {
        if (name)
        {
          m_stream.open(name, static_cast<A3M_INT32>(std::ios::binary));
        }

        // Set size of file
        if (m_stream.good())
        {
          A3M_INT32 begin, end;
          begin  = static_cast<A3M_INT32>(m_stream.tellg());
          end = static_cast<A3M_INT32>(m_stream.seekg(0,
                                       static_cast<A3M_INT32>(std::ios::end)).tellg());
          m_size = end - begin;

          // Seek back to the start
          m_stream.seekg(0, static_cast<A3M_INT32>(std::ios::beg));
        }
      }

      /*
       * Destructor
       */
      virtual ~InFileStream() {}

      /*
       * Returns A3M_TRUE if the stream is in a working state.
       */
      virtual A3M_BOOL valid()
      {
        return !m_stream.fail();
      }

      /*
       * Returns A3M_TRUE if the end-of-file has been reached.
       */
      virtual A3M_BOOL eof() { return m_stream.eof(); }

      /*
       * Returns the size of the stream in bytes.
       */
      virtual A3M_INT32 size() { return m_size; }

      /*
       * Modifies the internal read pointer of the underlying std::ifstream
       * object.  Seek operation is referenced from the start of the file
       * stream.
       */
      virtual A3M_INT32 seek(A3M_UINT32 offset)
      {
        return static_cast<A3M_INT32>(m_stream.seekg(offset,
                                      static_cast<A3M_INT32>(std::ios::beg)).tellg());
      }

      /*
       * Returns the current position (in bytes) of the internal read/write
       * pointer of the stream referenced from the start of the stream.
       */
      virtual A3M_INT32 tell() { return static_cast<A3M_INT32>(m_stream.tellg()); }

      /*
       * Read byteLength number of bytes from the file stream and copied
       * the data to the specified destination.
       */
      virtual A3M_INT32 read(void* dest, A3M_UINT32 byteLength)
      {
        if (m_stream.bad() || (dest == 0)) { return 0; }

        return static_cast<A3M_INT32>(m_stream.read(static_cast<A3M_CHAR8*>(dest),
                                      byteLength).gcount());
      }

      /*
       * Writing to an input stream is not allowed, so this just returns 0.
       */
      virtual A3M_INT32 write(const void* source, A3M_UINT32 byteLength)
      {
        A3M_PARAM_NOT_USED(source);
        A3M_PARAM_NOT_USED(byteLength);
        return 0;
      }

    private:
      A3M_INT32     m_size;
      std::ifstream m_stream;
    };

    /*
     * Concrete Stream class for writing data to disk
     */
    class OutFileStream : public Stream
    {
    public:
      /*
       * Constructor
       *
       * Constructs a std::ofstream with the given name.
       */
      OutFileStream(const A3M_CHAR8* name)
      {
        if (name)
        {
          m_stream.open(name, static_cast<A3M_INT32>(std::ios::binary));
        }
      }

      /*
       * Destructor
       */
      virtual ~OutFileStream() {}

      /*
       * Returns A3M_TRUE if the stream is in a working state.
       */
      virtual A3M_BOOL valid()
      {
        return !m_stream.fail();
      }

      /*
       * Returns A3M_TRUE if the end-of-file has been reached.
       */
      virtual A3M_BOOL eof() { return m_stream.eof(); }

      /*
       * Returns the size of the stream in bytes.
       */
      virtual A3M_INT32 size()
      {
        return static_cast<A3M_INT32>(m_stream.tellp());
      }

      /*
       * Modifies the internal write pointer of the underlying std::ofstream
       * object.  Seek operation is referenced from the start of the file
       * stream.
       */
      virtual A3M_INT32 seek(A3M_UINT32 offset)
      {
        return static_cast<A3M_INT32>(m_stream.seekp(offset,
                                      static_cast<A3M_INT32>(std::ios::beg)).tellp());
      }

      /*
       * Returns the current position (in bytes) of the internal read/write
       * pointer of the stream referenced from the start of the stream.
       */
      virtual A3M_INT32 tell() { return static_cast<A3M_INT32>(m_stream.tellp()); }

      /*
       * Output file stream is not readable, so this just returns 0.
       */
      virtual A3M_INT32 read(void* dest, A3M_UINT32 byteLength)
      {
        A3M_PARAM_NOT_USED(dest);
        A3M_PARAM_NOT_USED(byteLength);
        return 0;
      }

      /*
       * Copies byteLength number of bytes from the source buffer to the file
       * stream.
       */
      virtual A3M_INT32 write(const void* source, A3M_UINT32 byteLength)
      {
        if (m_stream.bad() || (source == 0)) { return 0; }

        A3M_INT32 beg = static_cast<A3M_INT32>(m_stream.tellp());
        A3M_INT32 end = static_cast<A3M_INT32>(
                          m_stream.write(static_cast < const A3M_CHAR8* > (source),
                                         byteLength).tellp());

        // Calculate number of bytes written
        return (end > 0) ? (end - beg) : 0;
      }

    private:
      std::ofstream m_stream;
    };


    /*
     * Platform-specific directory separator used when constructing the full
     * stream path
     */
    const A3M_CHAR8* const DIRECTORY_SEPARATOR = "\\";

    /*
     * Calculate the full path of a stream.
     */
    std::string createFullPath(std::string const& path, A3M_CHAR8 const* stream)
    {
      return path + DIRECTORY_SEPARATOR + stream;
    }

  } /* anonymous namespace */



  /****************************************************************************
   * FileStreamSource class definition
   ****************************************************************************/
  /*
   * Constructor.
   * Defaults to the current directory if an empty or null string is passed.
   */
  FileStreamSource::FileStreamSource(const A3M_CHAR8* name)
  {
    if (name && name[0] != '\0')
    {
      m_path = name;
    }
    else
    {
      m_path = ".";
    }
  }

  /*
   * Destructor
   */
  FileStreamSource::~FileStreamSource()
  {
  }

  /*
   * Check for the existence of a file
   */
  A3M_BOOL FileStreamSource::exists(const A3M_CHAR8* stream)
  {
    if ((stream == NULL) || (stream[0] == '\0'))
    {
      return A3M_FALSE;
    }

    std::string fullPath = createFullPath(m_path, stream);
    std::ifstream dummyStream( fullPath.c_str() );

    return dummyStream.good();
  }

  /*
   * Open a file stream for reading or writing.
   */
  Stream::Ptr FileStreamSource::open(const A3M_CHAR8* stream, A3M_BOOL writable)
  {
    Stream::Ptr fileStream;

    if ((stream != NULL) && (stream[0] != '\0'))
    {
      std::string fullPath = createFullPath(m_path, stream);

      if (writable)
      {
        fileStream.reset(new OutFileStream(fullPath.c_str()));
      }
      else
      {
        fileStream.reset(new InFileStream(fullPath.c_str()));
      }
    }

    // Return null pointer if filestream is not valid.
    if (!fileStream->valid())
    {
      fileStream.reset();
    }

    return fileStream;
  }

} /* namespace a3m */
