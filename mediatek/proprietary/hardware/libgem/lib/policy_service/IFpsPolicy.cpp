//#define LOG_TAG "FpsPolicy"

#include <dlfcn.h>
#include <utils/String8.h>
#include <binder/Parcel.h>
#include <cutils/log.h>

#include "dfps/IFpsPolicy.h"

namespace android {

class BpFpsPolicy : public BpInterface<IFpsPolicy> {
public:
    BpFpsPolicy(const sp<IBinder>& impl) : BpInterface<IFpsPolicy>(impl)
    {
    }

    virtual ~BpFpsPolicy() {}
};

IMPLEMENT_META_INTERFACE(FpsPolicy, "FpsPolicy");

status_t BnFpsPolicy::onTransact(uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags) {
    return BBinder::onTransact(code, data, reply, flags);
}

};
