/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.services.telephony;

import com.android.phone.ImsUtil;
import com.android.phone.common.R;
import com.android.services.telephony.DisconnectCauseUtil;

import android.content.Context;
import android.provider.Settings;
import android.telecom.DisconnectCause;

/**
 * MTK extension add-on class to DisconnectCauseUtil
 */
public class MtkDisconnectCauseUtil extends DisconnectCauseUtil {

    protected static int toTelecomDisconnectCauseCode(int telephonyDisconnectCause) {
        switch (telephonyDisconnectCause) {
        /// M: CC: Error message due to CellConnMgr checking @{
        case mediatek.telephony.MtkDisconnectCause.OUTGOING_CANCELED_BY_SERVICE:
        /// @}
            return DisconnectCause.CANCELED;

        default:
            return DisconnectCauseUtil.toTelecomDisconnectCauseCode(telephonyDisconnectCause);
        }
    }

    protected static CharSequence toTelecomDisconnectCauseDescription(
            Context context, int telephonyDisconnectCause) {
        if (context == null ) {
            return "";
        }

        Integer resourceId = null;
        switch (telephonyDisconnectCause) {
        case android.telephony.DisconnectCause.POWER_OFF:
            // Radio is explictly powered off because the device is in airplane mode.

            // TODO: Offer the option to turn the radio on, and automatically retry the call
            // once network registration is complete.

            if (ImsUtil.shouldPromoteWfc(context)) {
                resourceId = R.string.incall_error_promote_wfc;
            } else if (ImsUtil.isWfcModeWifiOnly(context)) {
                resourceId = R.string.incall_error_wfc_only_no_wireless_network;
            } else if (ImsUtil.isWfcEnabled(context)) {
                resourceId = R.string.incall_error_power_off_wfc;
            } else {
                /// M: CC: Modem reset related handling @{
                // Avoid to send power off error description (UI show turn off
                // airplane mode) to Telecom when modem reset because airplane mode
                // is off(power on) in Setting actually.
                if (Settings.Global.getInt(context.getContentResolver(),
                        Settings.Global.AIRPLANE_MODE_ON, 0) > 0) {
                    resourceId = R.string.incall_error_power_off;
                }
                //resourceId = R.string.incall_error_power_off;
                /// @}
            }
            break;

        /// M: CC: Error message due to CellConnMgr checking @{
        case mediatek.telephony.MtkDisconnectCause.OUTGOING_CANCELED_BY_SERVICE:
            break;
        /// @}

        }
        return resourceId == null ? ""
                : DisconnectCauseUtil.toTelecomDisconnectCauseDescription(context,
                        telephonyDisconnectCause);
    }

}
