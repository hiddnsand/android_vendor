package com.mediatek.op12.phone;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.IBinder;
import android.provider.Settings;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;


public class VideoSettingReceiverService extends Service {
    private static final String TAG = "Op12VideoSettingReceiverService";
    private Context mContext;
    private boolean mEnablePlatform;

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        mContext = getApplicationContext();
        mEnablePlatform = ImsManager.isVtEnabledByPlatform(mContext);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent vtIntent = new Intent("com.mediatek.intent.action.VT_CHANGED");
        final ConnectivityManager connectivityMgr = ConnectivityManager.from(mContext);

        final NetworkRequest wifiRequest = new NetworkRequest.Builder()
        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        .build();
        final NetworkRequest dataRequest = new NetworkRequest.Builder()
        .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        .build();
        connectivityMgr.registerNetworkCallback(wifiRequest,
            new ConnectivityManager.NetworkCallback() {
            /**
             * @param network
             */
            @Override
            public void onAvailable(Network network) {
                Log.d(TAG, "WifiCallback.onAvailable, network=" + network);
                if (isVilteEnabled()) {
                        boolean value = (Settings.Global.getInt(mContext.getContentResolver(),
                                "KEY_CALL_OPTIONS", 1)) == 1;
                        Log.d(TAG, " Set VT " + value);
                        vtIntent.putExtra("VT", value);
                        ImsManager.setVtSetting(mContext, value);
                        sendBroadcast(vtIntent);
                } else {
                    Log.d(TAG, " Not Set any value to VT");
                }
            }

            /**
             * @param network
             */
            @Override
            public void onLost(Network network) {
                Log.d(TAG, "WifiCallback.onLost, network=" + network);
                if (isVilteEnabled() && !isInternetConnected()) {
                    Log.d(TAG, " Set VT false");
                    vtIntent.putExtra("VT", false);
                    ImsManager.setVtSetting(mContext, false);
                    sendBroadcast(vtIntent);
                } else {
                    Log.d(TAG, " Not Set any value to VT");
                }
            }
       });
        connectivityMgr.registerNetworkCallback(dataRequest,
                 new ConnectivityManager.NetworkCallback() {
        /**
         * @param network
         */
        @Override
        public void onAvailable(Network network) {
            Log.d(TAG, "MobileCallback.onAvailable, network=" + network);
            if (Settings.Global.getInt(mContext.getContentResolver(),
                    Settings.Global.MOBILE_DATA, 1) == 1) {
            if (isVilteEnabled()) {
                    boolean value = (Settings.Global.getInt(mContext.getContentResolver(),
                            "KEY_CALL_OPTIONS", 1)) == 1;
                    Log.d(TAG, " Set VT " + value);
                    vtIntent.putExtra("VT", value);
                    ImsManager.setVtSetting(mContext, value);
                    sendBroadcast(vtIntent);
            } else {
                    Log.d(TAG, "Not Set any value to VT");
            }
          }
        }
        /**
         * @param network
         */
        @Override
        public void onLost(Network network) {
            Log.d(TAG, "MobileCallback.onLost, network=" + network);
            if (isVilteEnabled() && !isInternetConnected()) {
                Log.d(TAG, " Set VT false");
                vtIntent.putExtra("VT", false);
                ImsManager.setVtSetting(mContext, false);
                sendBroadcast(vtIntent);
            } else {
                Log.d(TAG, " Not Set any value to VT");
            }
        }
   });
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    private boolean isVilteEnabled() {
        boolean isLVCEnabled = false;
        boolean isVolteProvisioned = false;
        try {
            ImsConfig imsConfig = ImsManager.getInstance(mContext, SubscriptionManager
                        .getDefaultVoicePhoneId()).getConfigInterface();
            isVolteProvisioned = ImsManager.isVolteProvisionedOnDevice(mContext);
            isLVCEnabled = (1 == imsConfig.getProvisionedValue(
                         ImsConfig.ConfigConstants.LVC_SETTING_ENABLED));
            Log.d(TAG, "VolteProvisioned: " + isVolteProvisioned + " LVCEnabled: " +
                            isLVCEnabled);
        } catch (ImsException e) {
            Log.e(TAG, "Advanced settings not updated, ImsConfig null");
            isLVCEnabled = false;
            e.printStackTrace();
        }
        boolean enabled = mEnablePlatform && isLVCEnabled && isVolteProvisioned
                       && ImsManager.isEnhanced4gLteModeSettingEnabledByUser(mContext);
        Log.d(TAG, "isVilteEnabled:" + enabled);
        return enabled;
    }

    private boolean isInternetConnected() {
        ConnectivityManager connectivityMgr = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        // Check if wifi or mobile network is available or not. If any of them is
        // available or connected then it will return true, otherwise false;
        if (wifi != null) {
            if (wifi.isConnected()) {
                Log.d(TAG, "isInternetConnected: Wifi is connected");
                return true;
            }
        }
        if (mobile != null) {
            if (mobile.isConnected()) {
                Log.d(TAG, "isInternetConnected: Mobile data is connected");
                return true;
            }
        }
        Log.d(TAG, "isInternetConnected: Internet not Connected");
        return false;
      }
}
