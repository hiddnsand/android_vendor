package com.mediatek.phone.op09Clib.plugin;

import android.content.Context;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.mediatek.telephony.MtkTelephonyManagerEx;
import com.mediatek.phone.ext.DefaultMobileNetworkSettingsExt;
import com.mediatek.phone.op09Clib.plugin.R;

/**
 * REMOVE enable_4g_data,disable preferred_network_mode.
 */
public class OP09MobileNetworkSettingsExt extends
        DefaultMobileNetworkSettingsExt {

    private static final String TAG = "OP09ClibMobileNetworkSettingsExt";
    private static final String BUTTON_ENABLED_NETWORK_MODE = "enabled_networks_key";
    private static final String ENABLE_4G_DATA = "enable_4g_data";
    private static final String BUTTON_ROAMING_KEY = "button_roaming_key";
    private static final String[] MCCMNC_TABLE_TYPE_CT = { "45502", "46003",
            "46011", "46012", "46013" };
    // TODO:: Work around for ALPS01976671
    private boolean mIsRegister = false;
    private Context mOP09Context;
    private int mSlotId;

    /**
     * Constructor method.
     * @param context context.
     */
    public OP09MobileNetworkSettingsExt(Context context) {
        mOP09Context = context;
    }

    @Override
    public void initOtherMobileNetworkSettings(PreferenceActivity activity,
            int subId) {
        mSlotId = SubscriptionManager.getSlotIndex(subId);
        PreferenceScreen prefScreen = activity.getPreferenceScreen();
        Log.d(TAG, "initOtherMobileNetworkSettings remove enable4g"
                + "dismiss prefereNetworkmode");
        if (isCTCard(mSlotId)) {
            Preference enableNetworkMode = prefScreen
                    .findPreference(BUTTON_ENABLED_NETWORK_MODE);
            if (enableNetworkMode != null) {
                enableNetworkMode.setEnabled(false);
            } else {
                Log.d(TAG, "init, CT card add ENABLED_NETWORK_MODE");
                ListPreference ctButtonEnabledNetworks = new ListPreference(activity);
                ctButtonEnabledNetworks.setTitle(
                        mOP09Context.getString(R.string.preferred_network_mode_title));
                ctButtonEnabledNetworks.setKey(BUTTON_ENABLED_NETWORK_MODE);
                ctButtonEnabledNetworks.setSummary(
                        mOP09Context.getString(R.string.preferred_network_mode_ct_lwtcg));
                ctButtonEnabledNetworks.setEnabled(false);
                SwitchPreference buttonDataRoam = (SwitchPreference)
                        prefScreen.findPreference(BUTTON_ROAMING_KEY);
                int order = buttonDataRoam != null ? buttonDataRoam.getOrder() : 0;
                ctButtonEnabledNetworks.setOrder(order + 1);
                prefScreen.addPreference(ctButtonEnabledNetworks);
            }
        }
        Preference enableLTE = prefScreen.findPreference(ENABLE_4G_DATA);
        if (enableLTE != null) {
            prefScreen.removePreference(enableLTE);
        }
    }

    /**
     * app use to judge the Card is CT.
     *
     * @param slotId
     *            the slotId
     * @return true is CT
     */
    private boolean isCTCard(int slotId) {
        log("isCTCard, slotId = " + slotId);
        String simOperator = null;
        simOperator = getSimOperator(slotId);
        if (simOperator != null) {
            for (String mccmnc : MCCMNC_TABLE_TYPE_CT) {
                if (simOperator.equals(mccmnc)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Gets the MCC+MNC (mobile country code + mobile network code) of the
     * provider of the SIM. 5 or 6 decimal digits. Availability: The result of
     * calling getSimState() must be
     * android.telephony.TelephonyManager.SIM_STATE_READY.
     *
     * @param slotId
     *            Indicates which SIM to query.
     * @return MCC+MNC (mobile country code + mobile network code) of the
     *         provider of the SIM. 5 or 6 decimal digits.
     */
    private String getSimOperator(int slotId) {
        String simOperator = null;
        boolean isSimStateReady = false;
        isSimStateReady = TelephonyManager.SIM_STATE_READY == getSimState(slotId);
        if (isSimStateReady) {
            if (isGeminiSupport()) {
                simOperator = TelephonyManager.getDefault().getSimOperator(
                        slotId);
            } else {
                simOperator = TelephonyManager.getDefault().getSimOperator();
            }
        }
        log("getSimOperator, simOperator = " + simOperator + " slotId = "
                + slotId);
        return simOperator;
    }

    /**
     * @get SimConfig by
     *      TelephonyManager.getDefault().getMultiSimConfiguration().
     * @return true if the device has 2 or more slots
     */
    private boolean isGeminiSupport() {
        TelephonyManager.MultiSimVariants mSimConfig = TelephonyManager
                .getDefault().getMultiSimConfiguration();
        if (mSimConfig == TelephonyManager.MultiSimVariants.DSDS
                || mSimConfig == TelephonyManager.MultiSimVariants.DSDA) {
            return true;
        }
        return false;
    }

    private void log(String msg) {
        Log.d(TAG, msg);
    }

    /**
     * get the state of the device SIM card.
     *
     * @param slotId
     * @return return SIM state.
     */
    private int getSimState(int slotId) {
        int status = -100;
        if (isGeminiSupport() && isValidSlot(slotId)) {
            status = TelephonyManager.getDefault().getSimState(slotId);
        } else {
            status = TelephonyManager.getDefault().getSimState();
        }
        log("getSimState, slotId = " + slotId + "; status = " + status);
        return status;
    }

    /**
     * check the slotId value.
     * @param slotId
     * @return true or false
    */
    private boolean isValidSlot(int slotId) {
        final int[] geminiSlots = {0, 1};
        for (int i = 0; i < geminiSlots.length; i++) {
            if (geminiSlots[i] == slotId) {
                return true;
            }
        }
        return false;
     }

}
