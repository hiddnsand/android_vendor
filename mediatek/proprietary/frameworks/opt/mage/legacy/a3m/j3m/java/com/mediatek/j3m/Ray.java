/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M Ray interface.
 */

package com.mediatek.j3m;

/*!
 * \ingroup a3mJusrCollision
 * @{
 */
/**
 * An infinitely long ray used in raycast collision calculations.
 * A ray is described by an arbitrary point on the ray, plus a direction, which
 * may have an arbitrary non-zero length.  While the position and magnitude of
 * the ray may be unimportant in describing the ray, they may be used as
 * reference values when performing calculations such as raycast collisions.
 */
public interface Ray {
    /**
     * Sets the position of the ray.
     *
     * @param x X component
     * @param y Y component
     * @param z Z component
     */
    void setPosition(float x, float y, float z);

    /**
     * Returns the X component of the ray position.
     *
     * @return Vector component
     */
    float getPositionX();

    /**
     * Returns the Y component of the ray position.
     *
     * @return Vector component
     */
    float getPositionY();

    /**
     * Returns the Z component of the ray position.
     *
     * @return Vector component
     */
    float getPositionZ();

    /**
     * Sets the direction vector of the ray.
     *
     * @param x X component
     * @param y Y component
     * @param z Z component
     */
    void setDirection(float x, float y, float z);

    /**
     * Returns the X component of the ray direction vector.
     *
     * @return Vector component
     */
    float getDirectionX();

    /**
     * Returns the Y component of the ray direction vector.
     *
     * @return Vector component
     */
    float getDirectionY();

    /**
     * Returns the Z component of the ray direction vector.
     *
     * @return Vector component
     */
    float getDirectionZ();

    /**
     * Set the ray equal to a ray projected from a position on the screen.
     * Given a camera and a screen width and height, a ray may be projected
     * such that any point on the ray corresponds to the same position on the
     * screen.  This is useful for detecting objects that are rendered at that
     * point on the screen.  This function will work for all kinds of camera
     * projection.
     *
     * @param camera Camera from which to project ray
     * @param screenWidth Width of the screen in pixels
     * @param screenHeight Height of the screen in pixels
     * @param pickX X component of the point on the screen from which to project
     * @param pickY Y component of the point on the screen from which to project
     */
    void setToCameraRay(
        Camera camera,
        float screenWidth,
        float screenHeight,
        float pickX,
        float pickY);
}

/*! @} */
