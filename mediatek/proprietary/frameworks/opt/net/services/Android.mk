LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-subdir-java-files)

LOCAL_SRC_FILES += \
    java/com/mediatek/net/connectivity/IMtkIpConnectivityMetrics.aidl \
    ../../../../../../../system/netd/server/binder/android/net/metrics/INetdEventListener.aidl

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE := mediatek-framework-net

LOCAL_JAVA_LIBRARIES := services framework wifi-service mediatek-telephony-common

include $(BUILD_JAVA_LIBRARY)

# Include subdirectory makefiles
# ============================================================
include $(call all-makefiles-under,$(LOCAL_PATH))