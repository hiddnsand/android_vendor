/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/HwPipeline/RequestHandler"
//
#include "PipelineDefaultImp.h"
#include "PipelineUtility.h"
//
// #include <mtkcam/utils/metastore/IMetadataProvider.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
//

using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::Utils;
using namespace NSCam::v3::Utils;
using namespace NSCam::v3::NSPipelineContext;


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
submitRequestLocked(AppRequest& request)
{
    parsedAppRequest aParsedRequest(&request);
    CHECK_ERROR( aParsedRequest.parse() );
    //
    MY_LOGD_IF( mCommonInfo->mLogLevel>=2, "requestNo(%d) Output:raw(%zu),opaque(%zu),yuv(%zu),jpeg(%zu); Input:raw(%zu),opaque(%zu),yuv(%zu)",
            request.requestNo,
            aParsedRequest.vOImageInfos_Raw.size(), aParsedRequest.vOImageInfos_Opaque.size(),
            aParsedRequest.vOImageInfos_Yuv.size(), aParsedRequest.vOImageInfos_Jpeg.size(),
            aParsedRequest.vIImageInfos_Raw.size(), aParsedRequest.vIImageInfos_Opaque.size(),
            aParsedRequest.vIImageInfos_Yuv.size() );
    //
    // RWLock::AutoRLock _l(mRWLock);
    //
    evaluateRequestResult       evaluateResult;
    evaluateSubRequestResult    evaluateSubResult;
    //
    CHECK_ERROR( mConfigHandler->reconfigPipelineLocked(aParsedRequest) );
    //
    CHECK_ERROR( evaluateRequestLocked(aParsedRequest, evaluateResult) );
    //
    CHECK_ERROR( refineRequestMetaStreamBuffersLocked(aParsedRequest, evaluateResult) );
    //
    MBOOL runPlugin = mVendorHandler->refinePluginRequestMetaStreamBuffersLocked(evaluateResult, evaluateSubResult);
    //
    // main frame
    {
        sp<IPipelineFrame> pFrame = buildPipelineFrameLocked(request.requestNo, evaluateResult);
        if( ! pFrame.get() )
            return UNKNOWN_ERROR;
        //
        if ( runPlugin )
            CHECK_ERROR( mVendorHandler->setPluginResult(pFrame->getFrameNo(), evaluateResult, evaluateSubResult) );
        CHECK_ERROR( mCommonInfo->mpPipelineContext->queue(pFrame) );
    }
    //
    // sub frame
    for( size_t i = 0; i < evaluateSubResult.subRequetNumber; i++ ) {
        sp<IPipelineFrame> pFrame = mVendorHandler->buildSubPipelineFrameLocked(
                                        request.requestNo,
                                        evaluateSubResult.subRequestList.editItemAt(i)
                                    );
        if( ! pFrame.get() )
            return UNKNOWN_ERROR;
        //
        CHECK_ERROR( mCommonInfo->mpPipelineContext->queue(pFrame) );
    }
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
#define FUNC_ASSERT(exp, msg) \
    do{ if(!(exp)) { MY_LOGE("%s", msg); return INVALID_OPERATION; } } while(0)

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
evaluateRequestLocked(parsedAppRequest const& request, evaluateRequestResult& result)
{
    CAM_TRACE_NAME(__FUNCTION__);
    //
    IOMapColloctor aCollector;
    // NodeEdgeSet& aEdges = result.edges;
    NodeSet& aRoot      = result.roots;
    //
    FUNC_ASSERT( request.vIImageInfos_Raw.size() == 0, "[TODO] not supported yet!" );
    //FUNC_ASSERT( request.vIImageInfos_Yuv.size() == 0, "[TODO] not supported yet!" );
    FUNC_ASSERT( request.vOImageInfos_Raw.size() <= 1, "[TODO] not supported yet!" );
    FUNC_ASSERT( request.vOImageInfos_Jpeg.size() <= 1, "[TODO] not supported yet!" );
    //
    result.isTSflow = mParams->mPipelineConfigParams.mbUseP2TSNode && isTimeSharingForJpegSource(request);
    // set root node
    aRoot.add(eNODEID_P1Node);
    //
    CHECK_ERROR( evaluateRequestLocked_Img_Reproc(request, result, aCollector) );
    //
    CHECK_ERROR( evaluateRequestLocked_Img_Raw16(request, result, aCollector) );
    //
    CHECK_ERROR( evaluateRequestLocked_Img_Jpeg(request, result, aCollector) );
    //
    // CHECK_ERROR( evaluateRequestLocked_Opt(request, result, aCollector) );
    //
    CHECK_ERROR( evaluateRequestLocked_Img_AppYuvs(request, result, aCollector) );
    //
    CHECK_ERROR( evaluateRequestLocked_Img_FD(request, result, aCollector) );
    //
    CHECK_ERROR( evaluateRequestLocked_Img_Lcso(request, result, aCollector) );
    //
    CHECK_ERROR( evaluateRequestLocked_Img_Rsso(request, result, aCollector) );
    //
    CHECK_ERROR( evaluateRequestLocked_Metadata(request, result, aCollector) );
    //
    CHECK_ERROR( evaluateRequestLocked_updateIOMap(request, result, aCollector) );
    //
    CHECK_ERROR( evaluateRequestLocked_updateStreamBuffers(request, result, aCollector) );
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
evaluateRequestLocked_Img_Reproc(
    parsedAppRequest const& request,
    evaluateRequestResult& result,
    IOMapColloctor& aCollector
)
{
    CAM_TRACE_NAME(__FUNCTION__);
    //
    if ( request.vIImageInfos_Yuv.size() )
    {
        IImageStreamInfo const* pStreamInfo = request.vIImageInfos_Yuv[0].get();
        FUNC_ASSERT(
                mParams->mStreamSet.mpAppImage_Yuv_In.get(),
                "wrong yuv in config");
        //
        aCollector.addIn(eImagePathP1, mParams->mStreamSet.mpAppImage_Yuv_In);
        aCollector.addIn(eImagePathP2Full, mParams->mStreamSet.mpAppImage_Yuv_In);
        result.reprocInfos.useYuvIn = true;
    }
    //
    if ( request.vIImageInfos_Opaque.size() )
    {
        IImageStreamInfo const* pStreamInfo = request.vIImageInfos_Opaque[0].get();
        FUNC_ASSERT(
                mParams->mStreamSet.mpAppImage_Opaque_In.get(),
                "wrong opaque in config");
        //
        aCollector.addIn(eImagePathP1, mParams->mStreamSet.mpAppImage_Opaque_In);
        aCollector.addIn(eImagePathP2Full, mParams->mStreamSet.mpAppImage_Opaque_In);
        result.reprocInfos.useOpaqueIn = true;
    }
    //
    if ( request.vOImageInfos_Opaque.size() )
    {
        IImageStreamInfo const* pStreamInfo = request.vOImageInfos_Opaque[0].get();
        FUNC_ASSERT(
                mParams->mStreamSet.mpAppImage_Opaque_Out.get(),
                "wrong opaque out config");
        //
        aCollector.addOut(eImagePathP1, mParams->mStreamSet.mpAppImage_Opaque_Out);
        result.reprocInfos.useOpaqueOut = true;
    }
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
evaluateRequestLocked_Img_Raw16(
    parsedAppRequest const& request,
    evaluateRequestResult& result,
    IOMapColloctor& aCollector
)
{
    CAM_TRACE_NAME(__FUNCTION__);
    // todo: opaque reprocessing -> raw16 output
    NodeEdgeSet& aEdges = result.edges;
    //
    if ( request.vOImageInfos_Raw.size() )
    {
        IImageStreamInfo const* pStreamInfo = request.vOImageInfos_Raw[0].get();
        if ( isStream( mParams->mStreamSet.mpAppImage_RAW16, pStreamInfo->getStreamId() ) )
        {
            // Raw16: full-size raw -> raw16
            //
            FUNC_ASSERT(
                    mParams->mPipelineConfigParams.mbUseRaw16Node &&
                    mParams->mStreamSet.mpHalImage_P1_Raw.get() && mParams->mStreamSet.mpAppImage_RAW16.get(),
                    "not properly configured");
            //
            aCollector.addOut(eImagePathP1, mParams->mStreamSet.mpHalImage_P1_Raw);
            aCollector.addIn(eImagePathRaw16, mParams->mStreamSet.mpHalImage_P1_Raw);
            aCollector.addOut(eImagePathRaw16, mParams->mStreamSet.mpAppImage_RAW16);
            aEdges.addEdge(eNODEID_P1Node, eNODEID_RAW16Out);
            MY_LOGD("evaluateRequestLocked add RAW16");
        }
        else
        {
            MY_LOGE("not supported raw output stream %#" PRIx64 ,
                    pStreamInfo->getStreamId());
            return INVALID_OPERATION;
        }
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
evaluateRequestLocked_Img_Lcso(
    parsedAppRequest const& /*request*/,
    evaluateRequestResult& /*result*/,
    IOMapColloctor& aCollector
)
{
    CAM_TRACE_NAME(__FUNCTION__);
    //
    if(  mParams->mPipelineConfigParams.mbHasLcso && mParams->mStreamSet.mpHalImage_P1_Lcso.get()){
        // check LCSO
        if( aCollector.isConfigured(eImagePathP1))
            aCollector.addOut(eImagePathP1, mParams->mStreamSet.mpHalImage_P1_Lcso);
        if( aCollector.isConfigured(eImagePathP2Resized))
            aCollector.addIn(eImagePathP2Resized, mParams->mStreamSet.mpHalImage_P1_Lcso);
        if( aCollector.isConfigured(eImagePathP2Full))
            aCollector.addIn(eImagePathP2Full, mParams->mStreamSet.mpHalImage_P1_Lcso);
        if( aCollector.isConfigured(eImagePathP2TS))
            aCollector.addIn(eImagePathP2TS, mParams->mStreamSet.mpHalImage_P1_Lcso);
    }
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
evaluateRequestLocked_Img_Rsso(
    parsedAppRequest const& /*request*/,
    evaluateRequestResult& /*result*/,
    IOMapColloctor& aCollector
)
{
    CAM_TRACE_NAME(__FUNCTION__);
    //
    if(  mParams->mStreamingPipeParams.mbHasRsso && mParams->mStreamSet.mpHalImage_P1_Rsso.get()){
        // check RSSO
        if( aCollector.isConfigured(eImagePathP1))
            aCollector.addOut(eImagePathP1, mParams->mStreamSet.mpHalImage_P1_Rsso);
        if( aCollector.isConfigured(eImagePathP2Resized))
            aCollector.addIn(eImagePathP2Resized, mParams->mStreamSet.mpHalImage_P1_Rsso);
        if( aCollector.isConfigured(eImagePathP2Full))
            aCollector.addIn(eImagePathP2Full, mParams->mStreamSet.mpHalImage_P1_Rsso);
        //if( aCollector.isConfigured(eImagePathP2TS))
        //    aCollector.addIn(eImagePathP2TS, mParams->mStreamSet.mpHalImage_P1_Rsso);
    }
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
evaluateRequestLocked_Img_Jpeg(
    parsedAppRequest const& request,
    evaluateRequestResult& result,
    IOMapColloctor& aCollector
)
{
    CAM_TRACE_NAME(__FUNCTION__);
    //
    IMetadata* pAppMetaControl = request.pRequest->vIMetaBuffers[0]->tryReadLock(__FUNCTION__);
    if( ! pAppMetaControl ) {
        MY_LOGE("cannot get control meta");
        return UNKNOWN_ERROR;
    }
    NodeEdgeSet& aEdges = result.edges;
    //
    if( request.vOImageInfos_Jpeg.size() ) {
        //
        sp<IImageStreamInfo> pHalImage_Thumbnail_YUV;
        CAM_TRACE_BEGIN("createStreamInfo_Thumbnail_YUV");
        createStreamInfoLocked_Thumbnail_YUV(pAppMetaControl, pHalImage_Thumbnail_YUV);
        CAM_TRACE_END();
        //
        if( pHalImage_Thumbnail_YUV.get() )
            aCollector.updateStreamInfo(pHalImage_Thumbnail_YUV);
        //
        sp<IImageStreamInfo> pSourceRaw =
            ( 0 > mParams->mStreamSet.mvYuvStreams_Fullraw.indexOf(mParams->mStreamSet.mpHalImage_Jpeg_YUV->getStreamId())) ? mParams->mStreamSet.mpHalImage_P1_ResizerRaw :
            (result.isOpaqueReprocOut()) ? mParams->mStreamSet.mpAppImage_Opaque_Out :
            (result.isOpaqueReprocIn()) ? mParams->mStreamSet.mpAppImage_Opaque_In :
            (result.isYuvReprocIn()) ? mParams->mStreamSet.mpAppImage_Yuv_In :
            (mParams->mStreamSet.mpHalImage_P1_Raw.get()) ? mParams->mStreamSet.mpHalImage_P1_Raw : NULL;
        FUNC_ASSERT( pSourceRaw.get(), "null source raw" );

        // p2
        if( ! result.isTSflow ) {
            if( ! result.isYuvReprocIn() && ! result.isOpaqueReprocIn() )
                aCollector.addOut(eImagePathP1, pSourceRaw);
            //
            aCollector.addIn(eImagePathP2Full, pSourceRaw);
            aCollector.addOut(eImagePathP2Full, mParams->mStreamSet.mpHalImage_Jpeg_YUV);
            if( pHalImage_Thumbnail_YUV.get() )
                aCollector.addOut(eImagePathP2Full, pHalImage_Thumbnail_YUV);
            //
            aEdges.addEdge(eNODEID_P1Node, eNODEID_P2Node);
            aEdges.addEdge(eNODEID_P2Node, eNODEID_JpegNode);
        }
        else {
            if( ! result.isYuvReprocIn() && ! result.isOpaqueReprocIn() )
                aCollector.addOut(eImagePathP1, pSourceRaw);
            //
            aCollector.addIn(eImagePathP2TS, pSourceRaw);
            aCollector.addOut(eImagePathP2TS, mParams->mStreamSet.mpHalImage_Jpeg_YUV);
            if( pHalImage_Thumbnail_YUV.get() )
                aCollector.addOut(eImagePathP2TS, pHalImage_Thumbnail_YUV);
            //
            aEdges.addEdge(eNODEID_P1Node, eNODEID_P2Node_VSS);
            aEdges.addEdge(eNODEID_P2Node_VSS, eNODEID_JpegNode);
        }
        // jpeg
        sp<IImageStreamInfo> pHalImage_Jpeg_YUV;
        if ( ! mCommonInfo->mJpegRotationEnable )
        {
            aCollector.addIn(eImagePathJpeg, mParams->mStreamSet.mpHalImage_Jpeg_YUV);
        }
        else
        {
            CAM_TRACE_BEGIN("createStreamInfo_Jpeg_YUV");
            createStreamInfoLocked_Jpeg_YUV(pAppMetaControl, pHalImage_Jpeg_YUV);
            CAM_TRACE_END();
            if ( pHalImage_Jpeg_YUV.get() )
            {
                aCollector.addIn(eImagePathJpeg, pHalImage_Jpeg_YUV);
                //request.vIHalImage.add(pHalImage_Jpeg_YUV->getStreamId(), pHalImage_Jpeg_YUV);
                MY_LOGD_IF( 1, "Add new Jpeg_Yuv to map, StreamName:%s StreamId:%#" PRIx64 " ImageSize:%dx%d Transform:%d",
                            pHalImage_Jpeg_YUV->getStreamName(), pHalImage_Jpeg_YUV->getStreamId(),
                            pHalImage_Jpeg_YUV->getImgSize().w,  pHalImage_Jpeg_YUV->getImgSize().h,
                            pHalImage_Jpeg_YUV->getTransform() );
                aCollector.updateStreamInfo(pHalImage_Jpeg_YUV);
            }
            else
            {
                aCollector.addIn(eImagePathJpeg, mParams->mStreamSet.mpHalImage_Jpeg_YUV);
                MY_LOGD_IF( 1, "Add default Jpeg_Yuv to map, StreamName:%s StreamId:%#" PRIx64 " ImageSize:%dx%d Transform:%d",
                            mParams->mStreamSet.mpHalImage_Jpeg_YUV->getStreamName(), mParams->mStreamSet.mpHalImage_Jpeg_YUV->getStreamId(),
                            mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgSize().w,  mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgSize().h,
                            mParams->mStreamSet.mpHalImage_Jpeg_YUV->getTransform() );
                aCollector.updateStreamInfo( mParams->mStreamSet.mpHalImage_Jpeg_YUV);
            }
        }
        //
        if( pHalImage_Thumbnail_YUV.get() )
        {
            aCollector.addIn(eImagePathJpeg, pHalImage_Thumbnail_YUV);
            MY_LOGD_IF( 1, "Add Thumb_Yuv to map, StreamName:%s StreamId:%#" PRIx64 " ImageSize:%dx%d Transform:%d",
                    mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getStreamName(), mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getStreamId(),
                    mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getImgSize().w,  mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getImgSize().h,
                    mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getTransform() );
        }
        aCollector.addOut(eImagePathJpeg, mParams->mStreamSet.mpAppImage_Jpeg);
    }
    //
    if( pAppMetaControl )
        request.pRequest->vIMetaBuffers[0]->unlock(__FUNCTION__, pAppMetaControl);
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
evaluateRequestLocked_Img_AppYuvs(
    parsedAppRequest const& request,
    evaluateRequestResult& result,
    IOMapColloctor& aCollector
)
{
    CAM_TRACE_NAME(__FUNCTION__);
    //
    NodeEdgeSet& aEdges = result.edges;
    //
    if( request.vOImageInfos_Yuv.size() ) {
        //
        FUNC_ASSERT(
                mParams->mStreamSet.mvYuvStreams_Fullraw.size() == 0 || mParams->mStreamSet.mpHalImage_P1_Raw.get() ||
                result.isOpaqueReprocIn() || result.isYuvReprocIn(),
                "wrong fullraw config");
        FUNC_ASSERT(
                mParams->mStreamSet.mvYuvStreams_Resizedraw.size() == 0 || mParams->mStreamSet.mpHalImage_P1_ResizerRaw.get(),
                "wrong resizedraw config");
        //
        bool useFull = false;
        bool useResized = false;
#define hasUsage(flag, usage) ((flag & usage) == usage)
        for( size_t i = 0; i < request.vOImageInfos_Yuv.size(); i++ )
        {
            sp<IImageStreamInfo> pInfo = request.vOImageInfos_Yuv.valueAt(i);
            //
            result.hasEncOut |= hasUsage(pInfo->getUsageForConsumer(), GRALLOC_USAGE_HW_VIDEO_ENCODER);
            //
            StreamId_T const streamId = pInfo->getStreamId();
            if (result.forceFullRaw==true)
            {
                aCollector.addOut(eImagePathP2Full, pInfo);
                useFull = MTRUE;
            }
            else if( 0 <= mParams->mStreamSet.mvYuvStreams_Fullraw.indexOf(streamId) ||
                result.isOpaqueReprocIn() || result.isYuvReprocIn() )
            {
                aCollector.addOut(eImagePathP2Full, pInfo);
                useFull = MTRUE;
            }
            else if( 0 <= mParams->mStreamSet.mvYuvStreams_Resizedraw.indexOf(streamId) )
            {
                aCollector.addOut(eImagePathP2Resized, pInfo);
                useResized = MTRUE;
            }
            else
            {
                MY_LOGE("cannot find propery raw for stream %s(%#" PRIx64 ")",
                        pInfo->getStreamName(),streamId);
                return UNKNOWN_ERROR;
            }
        }
#undef hasUsage
        //
        if ( useFull ) {
            sp<IImageStreamInfo> pImageP2FullIn = NULL;
            if ( result.reprocInfos.useOpaqueOut ) {
                aCollector.addOut(eImagePathP1, mParams->mStreamSet.mpAppImage_Opaque_Out);
                pImageP2FullIn = mParams->mStreamSet.mpAppImage_Opaque_Out;
            } else if ( result.reprocInfos.useOpaqueIn ) {
                aCollector.addIn(eImagePathP1, mParams->mStreamSet.mpAppImage_Opaque_In);
                pImageP2FullIn = mParams->mStreamSet.mpAppImage_Opaque_In;
            } else if ( result.reprocInfos.useYuvIn ) {
                aCollector.addIn(eImagePathP1, mParams->mStreamSet.mpAppImage_Yuv_In);
                pImageP2FullIn = mParams->mStreamSet.mpAppImage_Yuv_In;
            } else {
                aCollector.addOut(eImagePathP1, mParams->mStreamSet.mpHalImage_P1_Raw);
                pImageP2FullIn = mParams->mStreamSet.mpHalImage_P1_Raw;
            }
            if( request.vOImageInfos_Yuv.size() )
                aCollector.addIn(eImagePathP2Full, pImageP2FullIn);
        }
        if ( useResized && !result.reprocInfos.useOpaqueIn && !result.reprocInfos.useYuvIn ) {
            aCollector.addOut(eImagePathP1, mParams->mStreamSet.mpHalImage_P1_ResizerRaw);
            //
            aCollector.addIn(eImagePathP2Resized, mParams->mStreamSet.mpHalImage_P1_ResizerRaw);
        }
        //
        aEdges.addEdge(eNODEID_P1Node, eNODEID_P2Node);
    }
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
evaluateRequestLocked_Opt(
    parsedAppRequest const& request,
    evaluateRequestResult& result,
    IOMapColloctor& aCollector
)
{
    CAM_TRACE_NAME(__FUNCTION__);
    //
    if ( mParams->mStreamSet.mpHalImage_P1_Raw.get() )
    {
        int testLog = 0;
        size_t occupiedFullRaw = aCollector.sizeOut(eImagePathP2Full);
        MY_LOGD_IF(testLog, "occupied full-raw: %zu", occupiedFullRaw);
        //
        size_t forceFullRaw = 0;
        for ( size_t i=0 ; i<request.vOImageInfos_Yuv.size(); i++ )
        {
            sp<IImageStreamInfo> pInfo = request.vOImageInfos_Yuv.valueAt(i);
            //
            StreamId_T const streamId = pInfo->getStreamId();
            if( 0 <= mParams->mStreamSet.mvYuvStreams_Fullraw.indexOf(streamId) ||
                result.isOpaqueReprocIn() || result.isYuvReprocIn() )
            {
                forceFullRaw++;
            }
        }
        MY_LOGD_IF(testLog, "occupied full-raw(%zu); force full-raw(%zu)", occupiedFullRaw, forceFullRaw);
        size_t needFd = (isFdEnable(request,result)==MTRUE)? 1 : 0;
        MY_LOGD_IF(testLog, "needFd: %zu", needFd);
        //
        if ( needFd && (occupiedFullRaw+forceFullRaw<=2)
             /*|| (!needFd && occupiedFullRaw+forceFullRaw<=2) */ //currently not allow because img2o->fd
            )
            result.forceFullRaw = true;
    }
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
evaluateRequestLocked_Img_FD(
    parsedAppRequest const& request,
    evaluateRequestResult& result,
    IOMapColloctor& aCollector
)
{
    CAM_TRACE_NAME(__FUNCTION__);
    NodeEdgeSet& aEdges = result.edges;
    if( isFdEnable(request, result) )
    {
        FUNC_ASSERT(
                mParams->mStreamSet.mpHalImage_FD_YUV.get(),
                "wrong fd yuv config");
// force add to imgo/rrzo
        if ( result.forceFullRaw==true )
        {
            aCollector.addOut(eImagePathP1, mParams->mStreamSet.mpHalImage_P1_Raw);
            aCollector.addIn(eImagePathP2Full, mParams->mStreamSet.mpHalImage_P1_Raw);
            aCollector.addOut(eImagePathP2Full, mParams->mStreamSet.mpHalImage_FD_YUV);
        }
        else if( 0 <= mParams->mStreamSet.mvYuvStreams_Resizedraw.indexOf(mParams->mStreamSet.mpHalImage_FD_YUV->getStreamId()) || result.reprocInfos.useOpaqueOut )
        {
            aCollector.addOut(eImagePathP1, mParams->mStreamSet.mpHalImage_P1_ResizerRaw);
            aCollector.addIn(eImagePathP2Resized, mParams->mStreamSet.mpHalImage_P1_ResizerRaw);
            aCollector.addOut(eImagePathP2Resized, mParams->mStreamSet.mpHalImage_FD_YUV);
        }
        else
        {
            aCollector.addOut(eImagePathP1, mParams->mStreamSet.mpHalImage_P1_Raw);
            aCollector.addIn(eImagePathP2Full, mParams->mStreamSet.mpHalImage_P1_Raw);
            aCollector.addOut(eImagePathP2Full, mParams->mStreamSet.mpHalImage_FD_YUV);
        }
        //
        aCollector.addIn(eImagePathFD, mParams->mStreamSet.mpHalImage_FD_YUV);
        //
        aEdges.addEdge(eNODEID_P1Node, eNODEID_P2Node);
        aEdges.addEdge(eNODEID_P2Node, eNODEID_FDNode);
    }
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
evaluateRequestLocked_Metadata(
    parsedAppRequest const& /*request*/,
    evaluateRequestResult& result,
    IOMapColloctor& aCollector
)
{
    CAM_TRACE_NAME(__FUNCTION__);
    // update meta
    if( aCollector.isConfigured(eImagePathP1) )
    {
        aCollector.addIn(eMetaPathP1, mParams->mStreamSet.mpAppMeta_Control);
        aCollector.addIn(eMetaPathP1, mParams->mStreamSet.mpHalMeta_Control);
        aCollector.addOut(eMetaPathP1, mParams->mStreamSet.mpAppMeta_DynamicP1);
        aCollector.addOut(eMetaPathP1, mParams->mStreamSet.mpHalMeta_DynamicP1);
    }
    if( aCollector.isConfigured(eImagePathP2Full) ||
        aCollector.isConfigured(eImagePathP2Resized) )
    {
        aCollector.addIn(eMetaPathP2, mParams->mStreamSet.mpAppMeta_Control);
        aCollector.addIn(eMetaPathP2, mParams->mStreamSet.mpAppMeta_DynamicP1);
        aCollector.addIn(eMetaPathP2, mParams->mStreamSet.mpHalMeta_DynamicP1);
        aCollector.addOut(eMetaPathP2, mParams->mStreamSet.mpAppMeta_DynamicP2);
        if( !result.isTSflow )
            aCollector.addOut(eMetaPathP2, mParams->mStreamSet.mpHalMeta_DynamicP2);
        //
    }
    //
    if( aCollector.isConfigured(eImagePathFD) )
    {
        aCollector.addIn(eMetaPathFD, mParams->mStreamSet.mpAppMeta_Control);
        aCollector.addOut(eMetaPathFD, mParams->mStreamSet.mpAppMeta_DynamicFD);
    }
    //
    if( result.isTSflow && aCollector.isConfigured(eImagePathP2TS) )
    {
        aCollector.addIn(eMetaPathP2TS, mParams->mStreamSet.mpAppMeta_Control);
        aCollector.addIn(eMetaPathP2TS, mParams->mStreamSet.mpHalMeta_DynamicP1);
        aCollector.addOut(eMetaPathP2TS, mParams->mStreamSet.mpHalMeta_DynamicP2);
    }
    //
    if( aCollector.isConfigured(eImagePathJpeg) )
    {
        aCollector.addIn(eMetaPathJpeg, mParams->mStreamSet.mpAppMeta_Control);
        aCollector.addIn(eMetaPathJpeg, mParams->mStreamSet.mpHalMeta_DynamicP2);
        aCollector.addOut(eMetaPathJpeg, mParams->mStreamSet.mpAppMeta_DynamicJpeg);
    }
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
evaluateRequestLocked_updateIOMap(
    parsedAppRequest const& /*request*/,
    evaluateRequestResult& result,
    IOMapColloctor& aCollector
)
{
    CAM_TRACE_NAME(__FUNCTION__);
    // updated image info to result
    for( size_t i = 0 ; i < aCollector.vUpdatedImageInfos.size(); i++ )
    {
        result.vUpdatedImageInfos.add(
                aCollector.vUpdatedImageInfos.keyAt(i),
                aCollector.vUpdatedImageInfos.valueAt(i)
                );
    }
    // update iomap
#define updateIOMap(_type_, _nodeId_, _path_ )                          \
    do{                                                                 \
        if( aCollector.isConfigured(_path_) ) {                    \
            result.nodeIOMap##_type_.add(                               \
                    _nodeId_,                                           \
                    IOMapSet().add(aCollector.editIOMap(_path_))); \
        }                                                               \
    } while(0)

    updateIOMap(Image, eNODEID_P1Node, eImagePathP1);
    {
        IOMapSet iomaps;
        if( aCollector.isConfigured(eImagePathP2Full) )
            iomaps.add(aCollector.editIOMap(eImagePathP2Full));
        if( aCollector.isConfigured(eImagePathP2Resized) )
            iomaps.add(aCollector.editIOMap(eImagePathP2Resized));
        result.nodeIOMapImage.add(eNODEID_P2Node, iomaps);
    }
    updateIOMap(Image, eNODEID_P2Node_VSS, eImagePathP2TS);
    updateIOMap(Image, eNODEID_RAW16Out  , eImagePathRaw16);
    updateIOMap(Image, eNODEID_FDNode    , eImagePathFD);
    updateIOMap(Image, eNODEID_JpegNode  , eImagePathJpeg);
    //
    updateIOMap(Meta , eNODEID_P1Node    , eMetaPathP1);
    updateIOMap(Meta , eNODEID_P2Node    , eMetaPathP2);
    updateIOMap(Meta , eNODEID_P2Node_VSS, eMetaPathP2TS);
    updateIOMap(Meta , eNODEID_RAW16Out  , eMetaPathRaw16);
    updateIOMap(Meta , eNODEID_FDNode    , eMetaPathFD);
    updateIOMap(Meta , eNODEID_JpegNode  , eMetaPathJpeg);
#undef updateIOMap
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
evaluateRequestLocked_updateStreamBuffers(
    parsedAppRequest const& request,
    evaluateRequestResult& result,
    IOMapColloctor& /*aCollector*/
)
{
    CAM_TRACE_NAME(__FUNCTION__);
    // app image
    {
        result.vAppImageBuffers.setCapacity(
                request.pRequest->vIImageBuffers.size() +
                request.pRequest->vOImageBuffers.size()
                );
        for( size_t i = 0; i < request.pRequest->vIImageBuffers.size(); i++ )
            result.vAppImageBuffers.add(
                    request.pRequest->vIImageBuffers.keyAt(i),
                    request.pRequest->vIImageBuffers.valueAt(i)
                    );
        for( size_t i = 0; i < request.pRequest->vOImageBuffers.size(); i++ )
            result.vAppImageBuffers.add(
                    request.pRequest->vOImageBuffers.keyAt(i),
                    request.pRequest->vOImageBuffers.valueAt(i)
                    );
    }
    // hal image
    {
        result.vHalImageBuffers.clear();
    }
    // app meta
    {
        result.vAppMetaBuffers.setCapacity(request.pRequest->vIMetaBuffers.size());
        for( size_t i = 0; i < request.pRequest->vIMetaBuffers.size(); i++ )
        {
            result.vAppMetaBuffers.add(
                    request.pRequest->vIMetaBuffers.keyAt(i),
                    request.pRequest->vIMetaBuffers.valueAt(i)
                    );
            if ( mParams->mStreamSet.mpAppMeta_Control.get() &&
                     mParams->mStreamSet.mpAppMeta_Control->getStreamId() ==
                     result.vAppMetaBuffers[i]->getStreamInfo()->getStreamId() )
            {
                result.isRepeating = result.vAppMetaBuffers[i]->isRepeating();
            }
        }
    }
    // hal meta
    {
        result.vHalMetaBuffers.setCapacity(1);
        sp<HalMetaStreamBuffer> pBuffer =
            HalMetaStreamBufferAllocatorT(mParams->mStreamSet.mpHalMeta_Control.get())();
        result.vHalMetaBuffers.add(mParams->mStreamSet.mpHalMeta_Control->getStreamId(), pBuffer);
    }
    //
    return OK;
}

#undef FUNC_ASSERT

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
refineRequestMetaStreamBuffersLocked(parsedAppRequest const& request, evaluateRequestResult& result)
{
    CAM_TRACE_NAME(__FUNCTION__);
    //
    if( ! mParams->mStreamSet.mpHalMeta_Control.get() ) {
        MY_LOGE("should config hal control meta");
        return UNKNOWN_ERROR;
    }
    //
    {
        sp<IMetaStreamBuffer> pBuf = result.vHalMetaBuffers.valueFor(mParams->mStreamSet.mpHalMeta_Control->getStreamId());
        if( pBuf.get() )
        {
            IMetadata* pMetadata = pBuf->tryWriteLock(LOG_TAG);

            // update sensor size
            {
                IMetadata::IEntry entry(MTK_HAL_REQUEST_SENSOR_SIZE);
                entry.push_back(mParams->mHwParams.mSensorSize, Type2Type< MSize >());
                pMetadata->update(entry.tag(), entry);
            }

            if ( (mParams->mStreamSet.mpAppImage_Jpeg.get() &&
                    0 <= result.vAppImageBuffers.indexOfKey(mParams->mStreamSet.mpAppImage_Jpeg->getStreamId())) ||
                 (mParams->mStreamSet.mpAppImage_Opaque_Out.get() &&
                    0 <= result.vAppImageBuffers.indexOfKey(mParams->mStreamSet.mpAppImage_Opaque_Out->getStreamId())) )
            {
                MY_LOGD_IF(1, "set MTK_HAL_REQUEST_REQUIRE_EXIF = 1");
                IMetadata::IEntry entry(MTK_HAL_REQUEST_REQUIRE_EXIF);
                entry.push_back(1, Type2Type<MUINT8>());
                pMetadata->update(entry.tag(), entry);
            }
            // set "the largest frame duration of streams" as "minimum frame duration"
            {
                MINT64 iMinFrmDuration = 0;
                for ( size_t i=0; i<result.vAppImageBuffers.size(); i++ ) {
                    StreamId_T const streamId = result.vAppImageBuffers.keyAt(i);
                    if( mParams->mStreamSet.mvStreamDurations.indexOfKey(streamId) < 0 ) {
                        MY_LOGE("Request App stream %#" PRIx64 "have not configured yet", streamId);
                        continue;
                    }
                    iMinFrmDuration = ( mParams->mStreamSet.mvStreamDurations.valueFor(streamId) > iMinFrmDuration)?
                        mParams->mStreamSet.mvStreamDurations.valueFor(streamId) : iMinFrmDuration;
                }
                MY_LOGD_IF( mCommonInfo->mLogLevel>=2, "The min frame duration is %" PRId64, iMinFrmDuration);
                IMetadata::IEntry entry(MTK_P1NODE_MIN_FRM_DURATION);
                entry.push_back(iMinFrmDuration, Type2Type<MINT64>());
                pMetadata->update(entry.tag(), entry);
            }
            //
            {
                MUINT8 bRepeating = (MUINT8) result.isRepeating;
                IMetadata::IEntry entry(MTK_HAL_REQUEST_REPEAT);
                entry.push_back(bRepeating, Type2Type< MUINT8 >());
                pMetadata->update(entry.tag(), entry);
                MY_LOGD_IF( mCommonInfo->mLogLevel>=2, "Control AppMetadata is repeating(%d)", bRepeating);
            }
        #if 0
            // SMVR decide pass2 direct-link venc for performance
            if ( mParams.mOperation_mode && mParams.mDefaultBusrstNum>1 )
            {
                android::sp<IMetadataProvider const>
                    pMetadataProvider = NSMetadataProviderManager::valueFor(mOpenId);
                IMetadata::IEntry const& entry = pMetadataProvider->getMtkStaticCharacteristics()
                                                 .entryFor(MTK_CONTROL_AVAILABLE_HIGH_SPEED_VIDEO_CONFIGURATIONS);
                if  ( entry.isEmpty() ) {
                    MY_LOGW("no static MTK_CONTROL_AVAILABLE_HIGH_SPEED_VIDEO_CONFIGURATIONS");
                }
                else {
                    // [width, height, fps_min, fps_max, batch_size]
                    for ( size_t i=0; i<entry.count(); i+=5 )
                    {
                        MINT32 fps = entry.itemAt(i+3, Type2Type<MINT32>());
                        MSize  vdoSize = MSize( entry.itemAt(i  , Type2Type<MINT32>()),
                                                entry.itemAt(i+1, Type2Type<MINT32>()) );
                        if ( vdoSize == mParams.mVideoSize )
                        {
                            //update hal seting
                            if ( fps>=240 )
                            {
                                MY_LOGD("%dx%d@%d -> direct link", vdoSize.w, vdoSize.h, fps);
                                {
                                    IMetadata::IEntry entry(MTK_P2NODE_HIGH_SPEED_VDO_FPS);
                                    entry.push_back( fps, Type2Type< MINT32 >());
                                    pMetadata->update(entry.tag(), entry);
                                }
                                {
                                    IMetadata::IEntry entry(MTK_P2NODE_HIGH_SPEED_VDO_SIZE);
                                    entry.push_back( vdoSize, Type2Type< MSize >());
                                    pMetadata->update(entry.tag(), entry);
                                }
                            }
                            break;
                        }
                    }
                }
            }
        #endif
        #if MTKCAM_HAVE_ADV_SETTING
            //update feature relative metadata, including vHDR
            if( mCommonInfo->mpAdvSettingMgr != NULL)
            {
                IMetadata* pAppMetaControl = request.pRequest->vIMetaBuffers[0]->tryWriteLock(__FUNCTION__);
                if( ! pAppMetaControl ) {
                    MY_LOGE("cannot get control meta");
                    return UNKNOWN_ERROR;
                }

                NSCam::AdvCamSettingMgr::PipelineParam pipelineParam;
                pipelineParam.mResizedRawSize = mParams->mHwParams.mResizedrawSize;
                pipelineParam.mSensorMode = mParams->mHwParams.mSensorMode;
                pipelineParam.mSensorSize = mParams->mHwParams.mSensorSize;
                pipelineParam.mVideoSize = mParams->mPipelineConfigParams.mVideoSize;
                pipelineParam.mMaxStreamSize = mParams->mPipelineConfigParams.mMaxStreamSize;
                pipelineParam.currentAdvSetting = mParams->mStreamingPipeParams.mpAdvSetting;
                NSCam::AdvCamSettingMgr::AdvCamInputParam advCamInput{
                    mParams->mPipelineConfigParams.mbHasRecording,
                    mParams->mPipelineConfigParams.mVideoSize,
                    mParams->mPipelineConfigParams.mb4KRecording,
                    mParams->mStreamingPipeParams.mpAdvSetting,
                    mParams->mConfigParams.mOperation_mode
                };

                NSCam::AdvCamSettingMgr::RequestParam reqParam;
                reqParam.mHasEncodeBuf = result.hasEncOut;
                reqParam.mIsRepeatingReq = result.isRepeating;

                mCommonInfo->mpAdvSettingMgr->updateRequestMeta(pMetadata, pAppMetaControl, pipelineParam, reqParam, advCamInput);
                if( pAppMetaControl )
                    request.pRequest->vIMetaBuffers[0]->unlock(
                        __FUNCTION__, pAppMetaControl
                    );
            }
        #endif
            //
            pBuf->unlock(LOG_TAG, pMetadata);
        }
        else
        {
            MY_LOGE("cannot get hal control meta sb.");
            return UNKNOWN_ERROR;
        }
    }
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
sp<IPipelineFrame>
PipelineDefaultImp::RequestHandler::
buildPipelineFrameLocked(
    MUINT32                 requestNo,
    evaluateRequestResult&  evaluateResult
)
{
    CAM_TRACE_NAME(__FUNCTION__);
    //
    RequestBuilder builder;
    builder.setRootNode( evaluateResult.roots );
    builder.setNodeEdges( evaluateResult.edges );
    //
    for( size_t i = 0; i < evaluateResult.vUpdatedImageInfos.size(); i++ )
    {
        builder.replaceStreamInfo(
                evaluateResult.vUpdatedImageInfos.keyAt(i),
                evaluateResult.vUpdatedImageInfos.valueAt(i)
                );
    }
    //
#define try_setIOMap(_nodeId_)                                                        \
    do {                                                                              \
        ssize_t idx_image = evaluateResult.nodeIOMapImage.indexOfKey(_nodeId_);       \
        ssize_t idx_meta  = evaluateResult.nodeIOMapMeta.indexOfKey(_nodeId_);        \
        builder.setIOMap(                                                             \
                _nodeId_,                                                             \
                (0 <= idx_image ) ?                                                   \
                evaluateResult.nodeIOMapImage.valueAt(idx_image) : IOMapSet::empty(), \
                (0 <= idx_meta ) ?                                                    \
                evaluateResult.nodeIOMapMeta.valueAt(idx_meta) : IOMapSet::empty()    \
                );                                                                    \
    } while(0)
    //
    try_setIOMap(eNODEID_P1Node);
    try_setIOMap(eNODEID_P2Node);
    try_setIOMap(eNODEID_P2Node_VSS);
    try_setIOMap(eNODEID_RAW16Out);
    try_setIOMap(eNODEID_FDNode);
    try_setIOMap(eNODEID_JpegNode);
    //
#undef try_setIOMap
    //
#define setStreamBuffers(_sb_type_, _type_, _vStreamBuffer_, _builder_)    \
    do {                                                                   \
        for (size_t i = 0; i < _vStreamBuffer_.size(); i++ )               \
        {                                                                  \
            StreamId_T streamId                = _vStreamBuffer_.keyAt(i); \
            sp<_sb_type_> buffer = _vStreamBuffer_.valueAt(i);             \
            _builder_.set##_type_##StreamBuffer(streamId, buffer);         \
        }                                                                  \
    } while(0)
    //
    setStreamBuffers(IImageStreamBuffer  , Image, evaluateResult.vAppImageBuffers, builder);
    setStreamBuffers(HalImageStreamBuffer, Image, evaluateResult.vHalImageBuffers, builder);
    setStreamBuffers(IMetaStreamBuffer   , Meta , evaluateResult.vAppMetaBuffers , builder);
    setStreamBuffers(HalMetaStreamBuffer , Meta , evaluateResult.vHalMetaBuffers , builder);
#undef setStreamBuffers
    //
    sp<IPipelineFrame> pFrame = builder
        .updateFrameCallback(this)
        .build(requestNo, mCommonInfo->mpPipelineContext);
    //
    return pFrame;
}

/******************************************************************************
 *
 ******************************************************************************/
#define max(a,b)  ((a) < (b) ? (b) : (a))
#define min(a,b)  ((a) < (b) ? (a) : (b))
MERROR
PipelineDefaultImp::RequestHandler::
createStreamInfoLocked_Jpeg_YUV(
    IMetadata const* pMetadata,
    android::sp<IImageStreamInfo>& rpStreamInfo
) const
{
    if ( mParams->mStreamSet.mpHalImage_Jpeg_YUV == 0 ) {
        MY_LOGW("No config stream: Jpeg_YUV");
        return NO_INIT;
    }
    IMetadata::IEntry const& entryJpegOrientation = pMetadata->entryFor(MTK_JPEG_ORIENTATION);
    if  ( entryJpegOrientation.isEmpty() ) {
        MY_LOGW("No tag: MTK_JPEG_ORIENTATION");
        return NAME_NOT_FOUND;
    }
    //
    MINT32 const jpegOrientation = entryJpegOrientation.itemAt(0, Type2Type<MINT32>());
    MUINT32      jpegTransform   = 0;
    if ( 0==jpegOrientation )
        jpegTransform = 0;
    else if ( 90==jpegOrientation )
        jpegTransform = eTransform_ROT_90;
    else if ( 180==jpegOrientation )
        jpegTransform = eTransform_ROT_180;
    else if ( 270==jpegOrientation )
        jpegTransform = eTransform_ROT_270;
    else
         MY_LOGW("Invalid Jpeg Orientation value: %d", jpegOrientation);
    //
    MUINT32 const imgTransform   = mParams->mStreamSet.mpHalImage_Jpeg_YUV->getTransform();
    MY_LOGD_IF( 1, "Jpeg orientation from metadata:%d transform current(%d) & previous(%d)",
                jpegOrientation, jpegTransform, imgTransform);
    if ( imgTransform == jpegTransform ) {
        rpStreamInfo = NULL;
        return OK;
    }
    MSize size;
    if ( jpegTransform&eTransform_ROT_90 ) { // pillarbox
        size.w = min(mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgSize().w, mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgSize().h);
        size.h = max(mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgSize().w, mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgSize().h);
    } else { // letterbox
        size.w = max(mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgSize().w, mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgSize().h);
        size.h = min(mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgSize().w, mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgSize().h);
    }
    MINT const format = mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgFormat();
    MUINT const usage = mParams->mStreamSet.mpHalImage_Jpeg_YUV->getUsageForAllocator();
    sp<ImageStreamInfo>
        pStreamInfo = createImageStreamInfo(
            mParams->mStreamSet.mpHalImage_Jpeg_YUV->getStreamName(),
            mParams->mStreamSet.mpHalImage_Jpeg_YUV->getStreamId(),
            mParams->mStreamSet.mpHalImage_Jpeg_YUV->getStreamType(),
            mParams->mStreamSet.mpHalImage_Jpeg_YUV->getMaxBufNum(),
            mParams->mStreamSet.mpHalImage_Jpeg_YUV->getMinInitBufNum(),
            mParams->mStreamSet.mpHalImage_Jpeg_YUV->getUsageForAllocator(),
            mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgFormat(),
            size, jpegTransform
        );
    if( pStreamInfo == NULL ) {
        MY_LOGE(
            "fail to new ImageStreamInfo: %s %#" PRIx64,
            mParams->mStreamSet.mpHalImage_Jpeg_YUV->getStreamName(),
            mParams->mStreamSet.mpHalImage_Jpeg_YUV->getStreamId()
        );
        return NO_MEMORY;
    }
    rpStreamInfo = pStreamInfo;
    MY_LOGD_IF(
        1,
        "streamId:%#" PRIx64 " %s %p (%p) yuvsize:%dx%d jpegOrientation:%d",
        rpStreamInfo->getStreamId(),
        rpStreamInfo->getStreamName(),
        rpStreamInfo.get(),
        mParams->mStreamSet.mpHalImage_Jpeg_YUV.get(),
        rpStreamInfo->getImgSize().w, rpStreamInfo->getImgSize().h, jpegOrientation
    );
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::RequestHandler::
createStreamInfoLocked_Thumbnail_YUV(
    IMetadata const* pMetadata,
    android::sp<IImageStreamInfo>& rpStreamInfo
) const
{
    if  ( mParams->mStreamSet.mpHalImage_Thumbnail_YUV == 0 ) {
        MY_LOGW("No config stream: Thumbnail_YUV");
        return NO_INIT;
    }
    //
    IMetadata::IEntry const& entryThumbnailSize = pMetadata->entryFor(MTK_JPEG_THUMBNAIL_SIZE);
    if  ( entryThumbnailSize.isEmpty() ) {
        MY_LOGW("No tag: MTK_JPEG_THUMBNAIL_SIZE");
        return NAME_NOT_FOUND;
    }
    MSize const& thumbnailSize = entryThumbnailSize.itemAt(0, Type2Type<MSize>());
    if  ( ! thumbnailSize ) {
        MY_LOGW("Bad thumbnail size: %dx%d", thumbnailSize.w, thumbnailSize.h);
        return NOT_ENOUGH_DATA;
    }
    MY_LOGD_IF( 1, "thumbnail size from metadata: %dx%d", thumbnailSize.w, thumbnailSize.h);
    //
    //
    IMetadata::IEntry const& entryJpegOrientation = pMetadata->entryFor(MTK_JPEG_ORIENTATION);
    if  ( entryJpegOrientation.isEmpty() ) {
        MY_LOGW("No tag: MTK_JPEG_ORIENTATION");
        return NAME_NOT_FOUND;
    }
    //
    MSize const yuvthumbnailsize = calcThumbnailYuvSize(
                                        mParams->mStreamSet.mpHalImage_Jpeg_YUV->getImgSize(),
                                        thumbnailSize
                                        );
    //
    MINT32  jpegOrientation = 0;
    MUINT32 jpegTransform   = 0;
    MSize   thunmbSize      = yuvthumbnailsize; // default thumbnail size
    //
    MINT const format = mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getImgFormat();
    IImageStreamInfo::BufPlanes_t bufPlanes;
    switch (format)
    {
    case eImgFmt_YUY2:{
        IImageStreamInfo::BufPlane bufPlane;
        bufPlane.rowStrideInBytes = (yuvthumbnailsize.w << 1);
        bufPlane.sizeInBytes = bufPlane.rowStrideInBytes * yuvthumbnailsize.h;
        bufPlanes.push_back(bufPlane);
        }break;
    default:
        MY_LOGE("not supported format: %#x", format);
        break;
    }
    //
    rpStreamInfo = new ImageStreamInfo(
        mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getStreamName(),
        mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getStreamId(),
        mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getStreamType(),
        mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getMaxBufNum(),
        mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getMinInitBufNum(),
        mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getUsageForAllocator(),
        format,
        thunmbSize,
        bufPlanes,
        jpegTransform
    );
    if  ( rpStreamInfo == 0 ) {
        MY_LOGE(
            "fail to new ImageStreamInfo: %s %#" PRIx64,
            mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getStreamName(),
            mParams->mStreamSet.mpHalImage_Thumbnail_YUV->getStreamId()
        );
        return NO_MEMORY;
    }
    //
    MY_LOGD_IF(
        1,
        "streamId:%#" PRIx64 " %s %p %p yuvthumbnailsize:%dx%d jpegOrientation:%d",
        rpStreamInfo->getStreamId(),
        rpStreamInfo->getStreamName(),
        rpStreamInfo.get(),
        mParams->mStreamSet.mpHalImage_Thumbnail_YUV.get(),
        thunmbSize.w, thunmbSize.h, jpegOrientation
    );
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MSize
PipelineDefaultImp::RequestHandler::
calcThumbnailYuvSize(
    MSize const rPicSize,
    MSize const rThumbnailsize
) const
{
#define align2(x) (((x) + 1) & (~0x1))
    MSize size;
    MUINT32 const val0 = rPicSize.w * rThumbnailsize.h;
    MUINT32 const val1 = rPicSize.h * rThumbnailsize.w;
    if( val0 > val1 ) {
        size.w = align2(val0/rPicSize.h);
        size.h = rThumbnailsize.h;
    }
    else if( val0 < val1 ) {
        size.w = rThumbnailsize.w;
        size.h = align2(val1/rPicSize.w);
    }
    else {
        size = rThumbnailsize;
    }
#undef align2
    MY_LOGD_IF(1, "thumb %dx%d, pic %dx%d -> yuv for thumb %dx%d",
            rThumbnailsize.w, rThumbnailsize.h,
            rPicSize.w, rPicSize.h,
            size.w, size.h
            );
    return size;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
PipelineDefaultImp::RequestHandler::
isFdEnable(
    parsedAppRequest const& request,
    evaluateRequestResult& result
)
{
    MBOOL bSupported = MFALSE;
    IMetadata* pAppMetaControl = request.pRequest->vIMetaBuffers[0]->tryReadLock(__FUNCTION__);
    if( ! pAppMetaControl ) {
        MY_LOGE("cannot get control meta");
        return bSupported;
    }
    //
    if( mParams->mPipelineConfigParams.mbUseFDNode &&
        !result.isOpaqueReprocIn() && !result.isYuvReprocIn() )
    {
        bSupported = isFdEnable(pAppMetaControl);
    }
    //
    if( pAppMetaControl )
        request.pRequest->vIMetaBuffers[0]->unlock(__FUNCTION__, pAppMetaControl);
    //
    return bSupported;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
PipelineDefaultImp::RequestHandler::
isFdEnable(
    IMetadata const* pMetadata
)
{
    //  If Face detection is not OFF or scene mode is face priority,
    //  add App:Meta:FD_result stream to Output App Meta Streams.
    //return 0;

    IMetadata::IEntry const& entryFdMode = pMetadata->entryFor(MTK_STATISTICS_FACE_DETECT_MODE);
    IMetadata::IEntry const& entryfaceScene = pMetadata->entryFor(MTK_CONTROL_SCENE_MODE);
    IMetadata::IEntry const& entryGdMode = pMetadata->entryFor(MTK_FACE_FEATURE_GESTURE_MODE);
    IMetadata::IEntry const& entrySdMode = pMetadata->entryFor(MTK_FACE_FEATURE_SMILE_DETECT_MODE);
    IMetadata::IEntry const& entryAsdMode = pMetadata->entryFor(MTK_FACE_FEATURE_ASD_MODE);

    MBOOL FDMetaEn, FDEnable;
    //
    FDMetaEn =   //(0 != mDebugFdMode) ||
             ( !entryFdMode.isEmpty() && MTK_STATISTICS_FACE_DETECT_MODE_OFF != entryFdMode.itemAt(0, Type2Type<MUINT8>())) ||
             ( !entryfaceScene.isEmpty() && MTK_CONTROL_SCENE_MODE_FACE_PRIORITY == entryfaceScene.itemAt(0, Type2Type<MUINT8>())) ||
             ( !entryGdMode.isEmpty() && MTK_FACE_FEATURE_GESTURE_MODE_OFF != entryGdMode.itemAt(0, Type2Type<MINT32>())) ||
             ( !entrySdMode.isEmpty() && MTK_FACE_FEATURE_SMILE_DETECT_MODE_OFF != entrySdMode.itemAt(0, Type2Type<MINT32>())) ||
             ( !entryAsdMode.isEmpty() && MTK_FACE_FEATURE_ASD_MODE_OFF != entryAsdMode.itemAt(0, Type2Type<MINT32>()));
    FDEnable = mPrevFDEn || FDMetaEn;
    mPrevFDEn = FDMetaEn;
    return FDEnable;

}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
PipelineDefaultImp::RequestHandler::
isTimeSharingForJpegSource(
    parsedAppRequest const& request
) const
{
    IMetadata* pAppMetaControl = request.pRequest->vIMetaBuffers[0]->tryReadLock(__FUNCTION__);
    if( ! pAppMetaControl ) {
        MY_LOGW("cannot get control meta");
        return MFALSE;
    }
    MBOOL bTS = MFALSE;
    // vss
    if ( mParams->mPipelineConfigParams.mbHasRecording && request.vOImageInfos_Jpeg.size()>0 )
        bTS = MTRUE;
    // reserved for c-shot(vendortag) / reprocessing in the future.
    if ( request.vOImageInfos_Jpeg.size()>0 )
    {
        if ( request.vIImageInfos_Opaque.size() || request.vIImageInfos_Yuv.size() )
        {
            MY_LOGW("reprocessing not use time-sharing flow");
            // bTS = MTRUE;
        }
        // bTS = MTRUE;
    }
    // others ...
    //
    if( pAppMetaControl )
        request.pRequest->vIMetaBuffers[0]->unlock(
                __FUNCTION__, pAppMetaControl
            );
    //
    return bTS;
}


/*******************************************************************************
 *
 ********************************************************************************/
PipelineDefaultImp::RequestHandler::
RequestHandler(
    std::shared_ptr<CommonInfo> pCommonInfo,
    std::shared_ptr<MyProcessedParams> pParams,
    wp<IPipelineModelMgr::IAppCallback> pAppCallback,
    sp<PipelineDefaultImp::ConfigHandler> pConfigHandler,
    sp<PipelineDefaultImp::VendorHandler> pVendorHandler
)
    : mpAppCallback(pAppCallback)
    , mCommonInfo(pCommonInfo)
    , mParams(pParams)
    , mPrevFDEn(MFALSE)
    , mConfigHandler(pConfigHandler)
    , mVendorHandler(pVendorHandler)
{
    FUNC_START;
    FUNC_END;
}


/*******************************************************************************
 *
 ********************************************************************************/
PipelineDefaultImp::RequestHandler::
~RequestHandler()
{
    FUNC_START;
    FUNC_END;
}


/******************************************************************************
 *  IPipelineBufferSetFrameControl::IAppCallback Interfaces.
 ******************************************************************************/
MVOID
PipelineDefaultImp::RequestHandler::
updateFrame(
    MUINT32 const frameNo,
    MINTPTR const userId,
    Result const& result
)
{
    if ( result.bFrameEnd ) return;

    MY_LOGD_IF( mCommonInfo->mLogLevel>=2, "frameNo %d, user %#" PRIxPTR ", AppLeft %zu, appMeta %zu, HalLeft %zu, halMeta %zu",
                frameNo, userId,
                result.nAppOutMetaLeft, result.vAppOutMeta.size(),
                result.nHalOutMetaLeft, result.vHalOutMeta.size()
                );
    sp<IPipelineModelMgr::IAppCallback> pAppCallback;
    pAppCallback = mpAppCallback.promote();
    if ( ! pAppCallback.get() ) {
        MY_LOGE("Have not set callback to device");
        FUNC_END;
        return;
    }
    pAppCallback->updateFrame(frameNo, userId, result.nAppOutMetaLeft, result.vAppOutMeta);
}
