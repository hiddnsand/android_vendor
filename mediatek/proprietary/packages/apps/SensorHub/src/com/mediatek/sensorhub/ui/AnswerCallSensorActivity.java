package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class AnswerCallSensorActivity extends BaseTriggerSensorActivity {

    public AnswerCallSensorActivity() {
        super(Utils.KEY_ANSWER_CALL_STATUS, Utils.KEY_ANSWER_CALL_NOTIFY_STATUS,
                Utils.KEY_ANSWER_CALL_AUTO_STATUS, Sensor.TYPE_ANSWER_CALL);
    }
}
