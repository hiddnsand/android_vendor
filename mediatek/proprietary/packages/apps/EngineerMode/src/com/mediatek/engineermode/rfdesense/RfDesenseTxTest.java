/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.rfdesense;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ListAdapter;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.ModemCategory;
import com.mediatek.engineermode.R;

import java.util.ArrayList;

public class RfDesenseTxTest extends Activity implements OnItemClickListener, OnClickListener {
    public static final String TAG = "RfDesenseTxTest";

    public static final String PREF_FILE = "tx_test";
    private static final String KEY_REBOOT = "rebooted";

    private static final int DIALOG_CONFIRM = 0;
    private static final int STOP_MODE = 0;

    ArrayList<String> mItems;
    private int mPosition = 0;

    private Button mStopButton;
    private Phone mPhone;
    private Phone mCdmaPhone;
    private boolean mStopFlag = false;

//    private ProgressDialog mIndicator = null;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == STOP_MODE) {

                AsyncResult ar = (AsyncResult) msg.obj;
                String text;
                if (ar.exception == null) {
                    mStopFlag = true;
                    text = "reboot modem.";
                    RfDesenseTxTest.this.finish();
                } else {
                    text = "Failed to reboot modem.";
                }
                Toast.makeText(RfDesenseTxTest.this, text, Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rf_desense_tx_test);

        ListView simTypeListView = (ListView) findViewById(R.id.list);
        mStopButton = (Button) findViewById(R.id.button_stop_total);

        mItems = new ArrayList<String>();
        mItems.add(getString(R.string.rf_desense_tx_test_gsm));
        if (ModemCategory.getModemType() == ModemCategory.MODEM_TD) {
            mItems.add(getString(R.string.rf_desense_tx_test_td));
        } else if (ModemCategory.getModemType() == ModemCategory.MODEM_FDD) {
            mItems.add(getString(R.string.rf_desense_tx_test_fd));
        }
        if (FeatureSupport.isSupported(FeatureSupport.FK_LTE_SUPPORT)) {
            mItems.add(getString(R.string.rf_desense_tx_test_lte));
        }
        if (ModemCategory.isCdma()) {
            mItems.add(getString(R.string.rf_desense_tx_test_cdma));
        }
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mItems);
        simTypeListView.setAdapter(adapter);
        simTypeListView.setOnItemClickListener(this);
        setListViewItemsHeight(simTypeListView);
        mStopButton.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int phoneId = ModemCategory.getCapabilitySim();
        mPhone = PhoneFactory.getPhone(phoneId);
        if (FeatureSupport.isSupported(FeatureSupport.FK_MTK_C2K_SUPPORT)) {
            mCdmaPhone = ModemCategory.getCdmaPhone();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!mStopFlag) {
            String[] cmd = new String[2];
            cmd[0] = "AT+CFUN=1,1";
            cmd[1] = "";
            Elog.i(TAG, "send: " + cmd[0]);
            mPhone.invokeOemRilRequestStrings(cmd, mHandler.obtainMessage(STOP_MODE));
        }
    }

    private void setListViewItemsHeight(ListView listview) {
        if (listview == null) {
            return;
        }
        ListAdapter adapter = listview.getAdapter();
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View itemView = adapter.getView(i, null, listview);
            itemView.measure(0, 0);
            totalHeight += itemView.getMeasuredHeight();
        }
        totalHeight += (adapter.getCount() - 1) * listview.getDividerHeight();
        ViewGroup.LayoutParams params = listview.getLayoutParams();
        params.height = totalHeight;
        listview.setLayoutParams(params);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SharedPreferences pref = getSharedPreferences(PREF_FILE,
                MODE_PRIVATE);
        if (pref.getBoolean(KEY_REBOOT, false)) {
            resetSettings();
        }

        mPosition = position;
        start();
    }

    @Override
    public void onClick(final View arg0) {
        if (arg0 == mStopButton) {
            if (!mStopFlag) {
                String[] cmd = new String[2];
                cmd[0] = "AT+CFUN=1,1";
                cmd[1] = "";
                Elog.i(TAG, "send: " + cmd[0]);
                mPhone.invokeOemRilRequestStrings(cmd, mHandler.obtainMessage(STOP_MODE));
            }
        }
    }
    private void start() {
        Intent intent = new Intent();
        String item = mItems.get(mPosition);
        if (item.equals(getString(R.string.rf_desense_tx_test_gsm))) {
            intent.setClass(this, RfDesenseTxTestGsm.class);
            this.startActivity(intent);
        } else if (item.equals(getString(R.string.rf_desense_tx_test_fd))
                || item.equals(getString(R.string.rf_desense_tx_test_td))) {
            intent.setClass(this, RfDesenseTxTestTd.class);
            this.startActivity(intent);
        } else if (item.equals(getString(R.string.rf_desense_tx_test_lte))) {
            intent.setClass(this, RfDesenseTxTestLte.class);
            this.startActivity(intent);
        } else if (item.equals(getString(R.string.rf_desense_tx_test_cdma))) {
            intent.setClass(this, RfDesenseTxTestCdma.class);
            this.startActivity(intent);
        }
    }


    private void resetSettings() {
        SharedPreferences.Editor pref = getSharedPreferences(PREF_FILE,
                MODE_PRIVATE).edit();
        pref.clear();
        pref.commit();
    }
}
