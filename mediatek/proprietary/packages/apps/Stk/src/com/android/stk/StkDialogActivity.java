/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.stk;

import com.android.internal.telephony.cat.TextMessage;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.provider.Settings;
import android.view.Gravity;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import com.android.internal.telephony.cat.CatLog;
import com.android.internal.telephony.RILConstants;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.PhoneInternalInterface;

//Stk modification for TDD data only
import android.database.ContentObserver;
import android.net.Uri;

//import com.mediatek.telephony.TelephonyManagerEx;

import com.mediatek.internal.telephony.cat.MtkAppInterface;
import com.mediatek.internal.telephony.cat.MtkCatLog;


/**
 * AlretDialog used for DISPLAY TEXT commands.
 *
 */
public class StkDialogActivity extends Activity {
    // members
    private static final String className = new Object(){}.getClass().getEnclosingClass().getName();
    private static final String LOG_TAG = className.substring(className.lastIndexOf('.') + 1);
    //keys) for saving the state of the dialog in the icicle
    private static final String TEXT = "text";
    TextMessage mTextMsg = null;
    protected boolean mIsResponseSent = false;
    private int mSlotId = -1;
    private String mStkSource = null;
    private Context mContext = null;
    private StkAppService appService = StkAppService.getInstance();
    // Utilize AlarmManager for real-time countdown
    private PendingIntent mTimeoutIntent = null;
    private AlarmManager mAlarmManager = null;

    private boolean mIsRegisterReceiverDone = false;

    protected static final int MIN_LENGTH = 6;
    protected static final int MIN_WIDTH = 170;

    private TimeoutReceiver mTimeoutReceiver = null;
    private String sOperatorSpec = SystemProperties.get("persist.operator.optr", "OM");
    private static final String ALARM_TIMEOUT = "android.stkDialog.TIMEOUT";

    private AlertDialog.Builder alertDialogBuilder;
    private AlertDialog mAlertDialog;


    private final IntentFilter mSIMStateChangeFilter =
            new IntentFilter(TelephonyIntents.ACTION_SIM_STATE_CHANGED);

    private final BroadcastReceiver mSIMStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(intent.getAction())) {
                String simState = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
                int slotId = intent.getIntExtra(PhoneConstants.SLOT_KEY, -1);

                MtkCatLog.v(LOG_TAG, "mSIMStateChangeReceiver() - slotId[" + slotId +
                        "]  state[" + simState + "], mSlotId: " + mSlotId);
                if ((slotId == mSlotId) &&
                    ((IccCardConstants.INTENT_VALUE_ICC_ABSENT.equals(simState)) ||
                    (IccCardConstants.INTENT_VALUE_ICC_NOT_READY.equals(simState)))) {
                    if (IccCardConstants.INTENT_VALUE_ICC_NOT_READY.equals(simState)) {
                        showTextToast(getApplicationContext(),
                        getString(R.string.lable_sim_not_ready));
                    }
                    cancelTimeOut();
                    mIsResponseSent = true;
                    finish();
                }
            }
        }
    };

    private final IntentFilter mStkIccStateChangeFilter =
            new IntentFilter(MtkAppInterface.CAT_ICC_STATUS_CHANGE);

    private final BroadcastReceiver mStkIccStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (MtkAppInterface.CAT_ICC_STATUS_CHANGE.equals(intent.getAction())) {

                int slotId = intent.getIntExtra(StkAppService.SLOT_ID,0);
                boolean catdState = intent.getBooleanExtra(MtkAppInterface.CARD_STATUS, false);
                MtkCatLog.v(LOG_TAG, "mStkIccStateChangeReceiver() - slotId[" + slotId +
                        "]  card_state[" + catdState + "], mSlotId: " + mSlotId);

                // If the Card is absent then check if the StkAppService is even
                if ((slotId == mSlotId) &&
                    (intent.getBooleanExtra(MtkAppInterface.CARD_STATUS, false) == false)) {
                    MtkCatLog.v(this, "card state false, card absent, finish dialog");
                    cancelTimeOut();
                    mIsResponseSent = true;
                    finish();
                }
            }
        }
    };


    private final IntentFilter mAirplaneModeFilter = new IntentFilter(
        Intent.ACTION_AIRPLANE_MODE_CHANGED);

    private BroadcastReceiver mAirplaneModeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean airplaneModeEnabled = isAirplaneModeOn(mContext);
            StkAppInstaller appInstaller = StkAppInstaller.getInstance();
            MtkCatLog.v(LOG_TAG, "mAirplaneModeReceiver AIRPLANE_MODE_CHANGED: " +
            airplaneModeEnabled);
            if (airplaneModeEnabled) {
                mIsResponseSent = true;
                cancelTimeOut();
                finish();
            }
        }
    };

    private boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        MtkCatLog.v(LOG_TAG, "onCreate");

        // appService can be null if this activity is automatically recreated by the system
        // with the saved instance state right after the phone process is killed.
        if (appService == null) {
            MtkCatLog.d(LOG_TAG, "onCreate - appService is null");
            finish();
            return;
        }

        initFromIntent(getIntent());
        if (mTextMsg == null) {
            mIsRegisterReceiverDone = false;
            finish();
            return;
        }

        alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setPositiveButton(R.string.button_ok, new
                DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (appService != null) {
                            MtkCatLog.d(LOG_TAG, "OK Clicked! isCurCmdSetupCall[" +
                                    appService.isCurCmdSetupCall(mSlotId) +
                                    "], mSlotId: " + mSlotId);
                        }
                        if ((appService != null) && appService.isCurCmdSetupCall(mSlotId)) {
                            MtkCatLog.v(LOG_TAG, "dailStkCall");
                            appService.dailStkCall(mSlotId);
                        }
                        cancelTimeOut();
                        sendResponse(StkAppService.RES_ID_CONFIRM, true);
                        finish();
                    }
                });

        alertDialogBuilder.setNegativeButton(R.string.button_cancel, new
                DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        MtkCatLog.d(LOG_TAG, "Cancel Clicked!, mSlotId: " + mSlotId);
                        cancelTimeOut();
                        sendResponse(StkAppService.RES_ID_CONFIRM, false);
                        finish();
                    }
                });

        alertDialogBuilder.setOnKeyListener(new Dialog.OnKeyListener () {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                MtkCatLog.v(LOG_TAG, "onKeyDown - keyCode:" + keyCode);
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                        if (event.getAction() == KeyEvent.ACTION_DOWN) {
                            cancelTimeOut();
                            sendResponse(StkAppService.RES_ID_BACKWARD);
                            finish();
                        }
                        break;

                    default:
                        break;
                }
                return false;
            }
        });

        alertDialogBuilder.setCancelable(false);

        mContext = getBaseContext();

        Bitmap iconBitmap = mTextMsg.icon;;

        LayoutInflater inflater = this.getLayoutInflater();
        View titleView = inflater.inflate(R.layout.stk_dialog_title, null);
        alertDialogBuilder.setCustomTitle(titleView);

        TextView tv = (TextView)titleView.findViewById(R.id.display_message);
        ImageView iv = (ImageView)titleView.findViewById(R.id.display_icon);

        if (mTextMsg.icon == null) {
            iconBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                    com.android.internal.R.drawable.stat_notify_sim_toolkit);
        }
        iv.setImageBitmap(iconBitmap);

        if (mTextMsg.title != null && mTextMsg.title.length() > 0) {
            tv.setText(mTextMsg.title);
        } else {
            tv.setVisibility(View.GONE);
        }

        if (!(mTextMsg.iconSelfExplanatory && mTextMsg.icon != null)) {
            alertDialogBuilder.setMessage(mTextMsg.text);
        }

        mAlertDialog = alertDialogBuilder.create();

        MtkCatLog.d(LOG_TAG, "dont show menu in navigation bar");
        WindowManager.LayoutParams lp = mAlertDialog.getWindow().getAttributes();
        lp.needsMenuKey = WindowManager.LayoutParams.NEEDS_MENU_SET_FALSE;
        mAlertDialog.getWindow().setAttributes(lp);

        MtkCatLog.d(LOG_TAG, "alertDialog's window: " + mAlertDialog.getWindow());

        mAlertDialog.show();

        if (mAlarmManager == null) {
            if (mContext != null) {
                MtkCatLog.v(LOG_TAG, "get mAlarmManager");
                mAlarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
            } else {
                MtkCatLog.w(LOG_TAG, "mContext is null");
            }
        }
        if (mTimeoutReceiver == null) {
            MtkCatLog.v(LOG_TAG, "new TimeoutReceiver");
            mTimeoutReceiver = new TimeoutReceiver();
        }
        IntentFilter filter = new IntentFilter(ALARM_TIMEOUT);
        MtkCatLog.v(LOG_TAG, "mTimeoutReceiver");
        registerReceiver(mTimeoutReceiver, filter);

        registerReceiver(mSIMStateChangeReceiver, mSIMStateChangeFilter);
        registerReceiver(mAirplaneModeReceiver, mAirplaneModeFilter);
        registerReceiver(mStkIccStateChangeReceiver, mStkIccStateChangeFilter);

        mIsRegisterReceiverDone = true;

        setFinishOnTouchOutside(false);

        // Set a new task description to change icon
        if (sOperatorSpec.equals("OP02") && PhoneConstants.SIM_ID_1 < mSlotId) {
            setTaskDescription(new ActivityManager.TaskDescription(null,
            BitmapFactory.decodeResource(getResources(),
            R.drawable.ic_launcher_sim2_toolkit)));
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        MtkCatLog.v(LOG_TAG, "onNewIntent - mIsResponseSent[" + mIsResponseSent + "]"
                + ", mSlotId: " + mSlotId);
        initFromIntent(intent);
        if (mTextMsg == null) {
            finish();
            return;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        MtkCatLog.v(LOG_TAG, "onKeyDown - keyCode:" + keyCode);
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                cancelTimeOut();
                sendResponse(StkAppService.RES_ID_BACKWARD);
                finish();
                break;
            }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        MtkCatLog.d(LOG_TAG, "onResume - mIsResponseSent[" + mIsResponseSent +
                "] mTextMsg.responseNeeded: " + mTextMsg.responseNeeded +
                " sim id: " + mSlotId);

        if (mAlertDialog != null) {
            WindowManager.LayoutParams dlglp = mAlertDialog.getWindow().getAttributes();
            dlglp.needsMenuKey = WindowManager.LayoutParams.NEEDS_MENU_SET_FALSE;
            mAlertDialog.getWindow().setAttributes(dlglp);
            MtkCatLog.d(LOG_TAG, "alertDialog's window: " + mAlertDialog.getWindow());
        }

        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.needsMenuKey = WindowManager.LayoutParams.NEEDS_MENU_SET_FALSE;
        getWindow().setAttributes(lp);
        MtkCatLog.d(LOG_TAG, "activity's window: " + getWindow());

        /*
         * If the userClear flag is set and dialogduration is set to 0, the display Text
         * should be displayed to user forever until some high priority event occurs
         * (incoming call, MMI code execution etc as mentioned under section
         * ETSI 102.223, 6.4.1)
         */
        if (StkApp.calculateDurationInMilis(mTextMsg.duration) == 0 &&
                !mTextMsg.responseNeeded && mTextMsg.userClear) {
            MtkCatLog.d(LOG_TAG, "User should clear text..showing message forever");
            return;
        }

        //For performance auto test case, do not delete this log.
        MtkCatLog.v(LOG_TAG, "Stk_Performance time: " + SystemClock.elapsedRealtime());
        if (appService != null) {
            appService.setDisplayTextDlgVisibility(true, mSlotId);
        } else {
           MtkCatLog.v(LOG_TAG, "onPause, appService is null.");
           mIsResponseSent = true;//Skip TR since this is not a real activity triggered from sim.
           showTextToast(getApplicationContext(), getString(R.string.lable_not_available));
           finish();
           return;
        }

        // For Immediate Response case
        if (false == mTextMsg.responseNeeded && null != appService) {
            appService.getStkContext(mSlotId).setPendingDialogInstance(this);
        }
        /*
         * When another activity takes the foreground, we do not want the Terminal
         * Response timer to be restarted when our activity resumes. Hence we will
         * check if there is an existing timer, and resume it. In this way we will
         * inform the SIM in correct time when there is no response from the User
         * to a dialog.
         */
        if (mTimeoutIntent != null) {
            MtkCatLog.v(LOG_TAG, "Pending Alarm! Let it finish counting down...");
        } else {
            MtkCatLog.v(LOG_TAG, "No Pending Alarm! OK to start timer...");
            startTimeOut(mTextMsg.userClear);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        MtkCatLog.v(LOG_TAG, "onPause, sim id: " + mSlotId);
        if (appService != null) {
            appService.setDisplayTextDlgVisibility(false, mSlotId);
        }
        /* For operator lab test, cancelTimeOut() should be removed.
           When HOME key pressed, the timer should be counted continually for sending TR.*/
        /*
         * do not cancel the timer here cancelTimeOut(). If any higher/lower
         * priority events such as incoming call, new sms, screen off intent,
         * notification alerts, user actions such as 'User moving to another activtiy'
         * etc.. occur during Display Text ongoing session,
         * this activity would receive 'onPause()' event resulting in
         * cancellation of the timer. As a result no terminal response is
         * sent to the card.
         */
    }

    @Override
    protected void onStart() {
        super.onStart();
        mIsResponseSent = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        MtkCatLog.v(LOG_TAG, "onStop - before Send CONFIRM false mIsResponseSent[" +
                mIsResponseSent + "], sim id: " + mSlotId);
        if (null == appService) {
            MtkCatLog.w(LOG_TAG, "null appService");
            return;
        }
        if (null == appService.getStkContext(mSlotId)) {
            MtkCatLog.w(LOG_TAG, "null stk context");
            return;
        }

        if (!mTextMsg.responseNeeded) {
            return;
        }

        // Display Text with Immediate Response has 3 cases to be clean.
        // In these 3 cases, we don't want setPendingDialogInstance(this) to be
        // called in onStop(). Below are 3 cases:
        // 1. Clean by user click button, mIsResponseSent is set as true in sendResponse().
        // 2. Clean by timer expiration, mIsResponseSent is set as true in sendResponse().
        // 3. Clean by "dialog.finish()" in StkAppService, mIsResponseSent is flase.
        if (!mIsResponseSent) {
            appService.getStkContext(mSlotId).setPendingDialogInstance(this);
            //sendResponse(StkAppService.RES_ID_CONFIRM, false);
            //finish();
        } else {
            MtkCatLog.v(LOG_TAG, "onStop(), finish.");
            appService.getStkContext(mSlotId).setPendingDialogInstance(null);
            cancelTimeOut();
            finish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MtkCatLog.v(LOG_TAG, "onDestroy - before Send CONFIRM false mIsResponseSent["
                + mIsResponseSent + "], sim id: " + mSlotId);

        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            MtkCatLog.v(LOG_TAG, "dismiss");
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }

        if (!mIsResponseSent) {
            if (null == appService) {
                //To get instance again, if stkappservice has created before onDestroy.
                appService = StkAppService.getInstance();
            }
            if (null != appService) {
                if (!appService.isDialogPending(mSlotId)) {
                    MtkCatLog.v(LOG_TAG, "handleDestroy - Send false confirm.");
                    sendResponse(StkAppService.RES_ID_CONFIRM, false);
                }
            }
        }
        if (appService != null) {
            appService.setDisplayTextDlgVisibility(false, mSlotId);
        }
        cancelTimeOut();
        if (mIsRegisterReceiverDone) {
            unregisterReceiver(mTimeoutReceiver);
            unregisterReceiver(mSIMStateChangeReceiver);
            unregisterReceiver(mAirplaneModeReceiver);
            unregisterReceiver(mStkIccStateChangeReceiver);
        }
        mTimeoutReceiver = null;
        mAlarmManager = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        MtkCatLog.v(LOG_TAG, "onSaveInstanceState");

        super.onSaveInstanceState(outState);
        outState.putParcelable(TEXT, mTextMsg);
        outState.putBoolean("RESPONSE_SENT", mIsResponseSent);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mTextMsg = savedInstanceState.getParcelable(TEXT);
        MtkCatLog.v(LOG_TAG, "onRestoreInstanceState - [" + mTextMsg + "]");
        mIsResponseSent = savedInstanceState.getBoolean("RESPONSE_SENT");
    }

    private void sendResponse(int resId) {
        sendResponse(resId, true);
    }

    private void initFromIntent(Intent intent) {

        if (intent != null) {
            mTextMsg = intent.getParcelableExtra("TEXT");
            mSlotId = intent.getIntExtra(StkAppService.SLOT_ID, -1);
            mStkSource = intent.getStringExtra(StkAppService.STK_SOURCE_KEY);
            if (appService != null) {
                if (!appService.isValidStkSourceKey(mStkSource)) {
                    mIsResponseSent = true;
                    if (!(TelephonyManager.getDefault().hasIccCard(mSlotId))) {
                        showTextToast(getApplicationContext(),
                                getString(R.string.no_sim_card_inserted));
                    }
                    finish();
                    appService.restoreCurrentCmd(mSlotId);
                    return;
                }
            } else {
                 MtkCatLog.v(LOG_TAG, "appService is null!");
                 mIsResponseSent = true;
                 finish();
                 return;
            }
        } else {
            finish();
        }

        MtkCatLog.v(LOG_TAG, "initFromIntent - [" + mTextMsg + "], sim id: " + mSlotId);
    }
    private void cancelTimeOut() {
        if (mTimeoutIntent != null) {
            MtkCatLog.v(LOG_TAG, "cancelTimeOut:"+ mSlotId + "mAlarmManager cancel");
            if (null !=  mAlarmManager) {
                mAlarmManager.cancel(mTimeoutIntent);
            }
        }
    }

    private void startTimeOut(boolean waitForUserToClear) {
        // Reset timeout.
        cancelTimeOut();
        int dialogDuration = StkApp.calculateDurationInMilis(mTextMsg.duration);
        // case 1  userClear = true & responseNeeded = false,
        // Dialog always exists.
        if (mTextMsg.userClear == true
                && mTextMsg.responseNeeded == false
                && 0 == dialogDuration) {
            return;
        } else {
            // userClear = false. will dissapear after a while.
            if (dialogDuration == 0) {
                if (waitForUserToClear) {
                    dialogDuration = StkApp.DISP_TEXT_WAIT_FOR_USER_TIMEOUT;
                } else {
                    dialogDuration = StkApp.DISP_TEXT_CLEAR_AFTER_DELAY_TIMEOUT;
                }
            }
            Intent mAlarmIntent = new Intent(ALARM_TIMEOUT, null);
            mAlarmIntent.putExtra(StkAppService.SLOT_ID, mSlotId);
            mTimeoutIntent = PendingIntent.getBroadcast(mContext, 0, mAlarmIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);

            // Try to use a more stringent timer not affected by system sleep.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                mAlarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                        SystemClock.elapsedRealtime() + dialogDuration, mTimeoutIntent);
            } else {
                mAlarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                        SystemClock.elapsedRealtime() + dialogDuration, mTimeoutIntent);
            }
            MtkCatLog.v(LOG_TAG, "mSlotId: " + mSlotId
                    + "startTimeOut dialogDuration: " + dialogDuration);
        }
    }
    private void sendResponse(int resId, boolean confirmed) {
        if (mSlotId == -1) {
            MtkCatLog.w(LOG_TAG, "sim id is invalid");
            return;
        }

        if (StkAppService.getInstance() == null) {
            MtkCatLog.w(LOG_TAG, "Ignore response: id is " + resId);
            return;
        }

        MtkCatLog.v(LOG_TAG, "sendResponse resID[" + resId + "] confirmed[" + confirmed + "]");

        mIsResponseSent = true;
        Bundle args = new Bundle();
        args.putInt(StkAppService.OPCODE, StkAppService.OP_RESPONSE);
        args.putInt(StkAppService.SLOT_ID, mSlotId);
        args.putInt(StkAppService.RES_ID, resId);
        args.putBoolean(StkAppService.CONFIRMATION, confirmed);
        startService(new Intent(this, StkAppService.class).putExtras(args));
    }

    private class TimeoutReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            int slotID = intent.getIntExtra(StkAppService.SLOT_ID, 0);
            MtkCatLog.v(LOG_TAG, "TimeoutReceiver onReceive, action:" +
                    action + ",slotID: " + slotID);
            if (action == null || slotID != mSlotId) {
                return;
            }
            if (action.equals(ALARM_TIMEOUT)) {
                MtkCatLog.v(LOG_TAG, "ALARM_TIMEOUT rcvd");
                mTimeoutIntent = null;
                sendResponse(StkAppService.RES_ID_TIMEOUT);
                finish();
            }
        }
    }

    void showTextToast(Context context, String msg) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();
    }
    //Stk modification for TDD data only
    /*
    private void register4GDataModeObserver() {
        int subId[] = SubscriptionManager.getSubId(mSlotId);
        MtkCatLog.v(this, "register 4G data only observer subId: " + subId[0]
                + " for slotId: " + mSlotId);
        Uri uri = Settings.Global.getUriFor(
            android.provider.Settings.Global.PREFERRED_NETWORK_MODE + subId[0]);
        mContext.getContentResolver().registerContentObserver(
            uri, true, m4GDataModeObserver);
    }

    private void unregister4GDataModeObserver() {
        MtkCatLog.v(this, "unregister 4G data only observer");
        mContext.getContentResolver().unregisterContentObserver(
                m4GDataModeObserver);
    }
    */
}
