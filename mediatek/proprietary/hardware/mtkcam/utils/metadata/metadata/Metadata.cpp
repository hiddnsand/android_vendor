/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/******************************************************************************
 *
 ******************************************************************************/
#define LOG_TAG "MtkCam/Metadata"
//
#include <stdio.h>
#include <stdlib.h>
//
#include <utils/Vector.h>
#include <utils/KeyedVector.h>
//
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/utils/std/common.h>
//
#include <mtkcam/utils/metadata/mtk_metadata_types.h>
//
namespace NSCam {
    typedef int MERROR;
};
//
using namespace NSCam;
using namespace android;
using namespace std;
//
#define get_mtk_metadata_tag_type(x) mType
//
/******************************************************************************
 *
 ******************************************************************************/
#if (MTKCAM_HAVE_AEE_FEATURE == 1)
#include <aee.h>
#define AEE_ASSERT(String) \
    do { \
        CAM_LOGE("ASSERT("#String") fail"); \
        aee_system_exception( \
            "mtkcam/Metadata", \
            NULL, \
            DB_OPT_DEFAULT, \
            String); \
    } while(0)
#else
#define AEE_ASSERT(String)
#endif
/******************************************************************************
 *
 ******************************************************************************/
class IMetadata::IEntry::Implementor
{
private:
    Tag_t                           mTag;
    int                             mType;

public:     ////                    Instantiation.
    virtual                         ~Implementor();
                                    Implementor(Tag_t tag);
    Implementor&                    operator=(Implementor const& other);
                                    Implementor(Implementor const& other);

public:     ////                    Accessors.

    /**
     * Return the tag.
     */
    virtual MUINT32                 tag() const;

    /**
     * Return the type.
     */
    virtual MINT32                  type() const;

    /**
     * Check to see whether it is empty (no items) or not.
     */
    virtual MBOOL                   isEmpty() const;

    /**
     * Return the number of items.
     */
    virtual MUINT                   count() const;

    /**
     * Return how many items can be stored without reallocating the backing store.
     */
    virtual MUINT                   capacity() const;

    /**
     * Set the capacity.
     */
    virtual MBOOL                   setCapacity(MUINT size);

public:     ////                    Operations.

    /**
     * Clear all items.
     * Note: Copy-on write.
     */
    virtual MVOID                   clear();

    /**
     * Delete an item at a given index.
     * Note: Copy-on write.
     */
    virtual MERROR                  removeAt(MUINT index);


#define IMETADATA_IENTRY_OPS_DECLARATION(_T) \
    virtual MVOID                   push_back(_T const& item, Type2Type<_T>) \
                                    {  \
                                        mType = TYPE_##_T;\
                                        mStorage_##_T.push_back(item); \
                                    } \
    virtual _T&                     editItemAt(MUINT index, Type2Type<_T>){ return mStorage_##_T.editItemAt(index); } \
    virtual _T const&               itemAt(MUINT index, Type2Type<_T>) const { return reinterpret_cast<const _T&>(mStorage_##_T.itemAt(index)); } \
    Vector<_T>                      mStorage_##_T;

    IMETADATA_IENTRY_OPS_DECLARATION(MUINT8)
    IMETADATA_IENTRY_OPS_DECLARATION(MINT32)
    IMETADATA_IENTRY_OPS_DECLARATION(MFLOAT)
    IMETADATA_IENTRY_OPS_DECLARATION(MINT64)
    IMETADATA_IENTRY_OPS_DECLARATION(MDOUBLE)
    IMETADATA_IENTRY_OPS_DECLARATION(MRational)
    IMETADATA_IENTRY_OPS_DECLARATION(MPoint)
    IMETADATA_IENTRY_OPS_DECLARATION(MSize)
    IMETADATA_IENTRY_OPS_DECLARATION(MRect)
    IMETADATA_IENTRY_OPS_DECLARATION(IMetadata)
    IMETADATA_IENTRY_OPS_DECLARATION(Memory)

#undef  IMETADATA_IENTRY_OPS_DECLARATION

};

/******************************************************************************
 *
 ******************************************************************************/
#define RETURN_TYPE_OPS(_Type, _Ops) \
    _Type == TYPE_MUINT8 ? \
        mStorage_MUINT8._Ops : \
    _Type == TYPE_MINT32 ? \
        mStorage_MINT32._Ops : \
    _Type == TYPE_MFLOAT ? \
        mStorage_MFLOAT._Ops : \
    _Type == TYPE_MINT64 ? \
        mStorage_MINT64._Ops : \
    _Type == TYPE_MDOUBLE ? \
        mStorage_MDOUBLE._Ops : \
    _Type == TYPE_MRational ? \
        mStorage_MRational._Ops : \
    _Type == TYPE_MPoint ? \
        mStorage_MPoint._Ops : \
    _Type == TYPE_MSize ? \
        mStorage_MSize._Ops : \
    _Type == TYPE_Memory ? \
        mStorage_Memory._Ops : \
    _Type == TYPE_MRect ? \
        mStorage_MRect._Ops : \
        mStorage_IMetadata._Ops; \

#define SET_TYPE_OPS(_Type, _Ops, _Val) \
    _Type == TYPE_MUINT8 ? \
        mStorage_MUINT8._Ops(_Val) : \
    _Type == TYPE_MINT32 ? \
        mStorage_MINT32._Ops(_Val) : \
    _Type == TYPE_MFLOAT ? \
        mStorage_MFLOAT._Ops(_Val) : \
    _Type == TYPE_MINT64 ? \
        mStorage_MINT64._Ops(_Val) : \
    _Type == TYPE_MDOUBLE ? \
        mStorage_MDOUBLE._Ops(_Val) : \
    _Type == TYPE_MRational ? \
        mStorage_MRational._Ops(_Val) : \
    _Type == TYPE_MPoint ? \
        mStorage_MPoint._Ops(_Val) : \
    _Type == TYPE_MSize ? \
        mStorage_MSize._Ops(_Val) : \
    _Type == TYPE_Memory ? \
        mStorage_Memory._Ops(_Val) : \
    _Type == TYPE_MRect ? \
        mStorage_MRect._Ops(_Val) : \
        mStorage_IMetadata._Ops(_Val);

#define SRC_DST_OPERATOR(_Type, _Ops, _Src) \
    if(_Type == TYPE_MUINT8) \
        (mStorage_MUINT8 _Ops _Src.mStorage_MUINT8); \
    else if(_Type == TYPE_MINT32) \
        (mStorage_MINT32 _Ops _Src.mStorage_MINT32); \
    else if(_Type == TYPE_MFLOAT) \
        (mStorage_MFLOAT _Ops _Src.mStorage_MFLOAT); \
    else if(_Type == TYPE_MINT64) \
        (mStorage_MINT64 _Ops _Src.mStorage_MINT64); \
    else if(_Type == TYPE_MDOUBLE) \
        (mStorage_MDOUBLE _Ops _Src.mStorage_MDOUBLE); \
    else if(_Type == TYPE_MRational) \
        (mStorage_MRational _Ops _Src.mStorage_MRational); \
    else if(_Type == TYPE_MPoint) \
        (mStorage_MPoint _Ops _Src.mStorage_MPoint); \
    else if(_Type == TYPE_MSize) \
        (mStorage_MSize _Ops _Src.mStorage_MSize); \
    else if(_Type == TYPE_MRect) \
        (mStorage_MRect _Ops _Src.mStorage_MRect); \
    else if(_Type == TYPE_Memory) \
        (mStorage_Memory _Ops _Src.mStorage_Memory); \
    else if(_Type == TYPE_IMetadata) \
        (mStorage_IMetadata _Ops _Src.mStorage_IMetadata);
/******************************************************************************
 *
 ******************************************************************************/
IMetadata::IEntry::Implementor::
Implementor(Tag_t tag)
    : mTag(tag)
    , mType(-1)
#define STORAGE_DECLARATION(_T) \
    , mStorage_##_T()

    STORAGE_DECLARATION(MUINT8)
    STORAGE_DECLARATION(MINT32)
    STORAGE_DECLARATION(MFLOAT)
    STORAGE_DECLARATION(MINT64)
    STORAGE_DECLARATION(MDOUBLE)
    STORAGE_DECLARATION(MRational)
    STORAGE_DECLARATION(MPoint)
    STORAGE_DECLARATION(MSize)
    STORAGE_DECLARATION(MRect)
    STORAGE_DECLARATION(IMetadata)
    STORAGE_DECLARATION(Memory)

#undef STORAGE_DECLARATION
{
}


IMetadata::IEntry::Implementor::
Implementor(IMetadata::IEntry::Implementor const& other)
    : mTag(other.mTag)
    , mType(other.mType)
#define STORAGE_DECLARATION(_T) \
    , mStorage_##_T(other.mStorage_##_T)

    STORAGE_DECLARATION(MUINT8)
    STORAGE_DECLARATION(MINT32)
    STORAGE_DECLARATION(MFLOAT)
    STORAGE_DECLARATION(MINT64)
    STORAGE_DECLARATION(MDOUBLE)
    STORAGE_DECLARATION(MRational)
    STORAGE_DECLARATION(MPoint)
    STORAGE_DECLARATION(MSize)
    STORAGE_DECLARATION(MRect)
    STORAGE_DECLARATION(IMetadata)
    STORAGE_DECLARATION(Memory)

#undef STORAGE_DECLARATION
{
}


IMetadata::IEntry::Implementor&
IMetadata::IEntry::Implementor::
operator=(IMetadata::IEntry::Implementor const& other)
{
    if (this != &other)
    {
        mTag = other.mTag;
        mType = other.mType;
        SRC_DST_OPERATOR(get_mtk_metadata_tag_type(mTag), =, other);
    }
    else {
        CAM_LOGW("this(%p) == other(%p)", this, &other);
    }

    return *this;
}


IMetadata::IEntry::Implementor::
~Implementor()
{
}


MUINT32
IMetadata::IEntry::Implementor::
tag() const
{
    return mTag;
}

MINT32
IMetadata::IEntry::Implementor::
type() const
{
    return mType;
}

MBOOL
IMetadata::IEntry::Implementor::
isEmpty() const
{
    return get_mtk_metadata_tag_type(mTag) == -1 ?
           MTRUE : RETURN_TYPE_OPS(get_mtk_metadata_tag_type(mTag), isEmpty());
}


MUINT
IMetadata::IEntry::Implementor::
count() const
{
    return get_mtk_metadata_tag_type(mTag) == -1 ?
           0 : RETURN_TYPE_OPS(get_mtk_metadata_tag_type(mTag), size());
}


MUINT
IMetadata::IEntry::Implementor::
capacity() const
{
    return get_mtk_metadata_tag_type(mTag) == -1 ?
           0 : RETURN_TYPE_OPS(get_mtk_metadata_tag_type(mTag), capacity());
}


MBOOL
IMetadata::IEntry::Implementor::
setCapacity(MUINT size)
{
    MERROR ret = get_mtk_metadata_tag_type(mTag) == -1 ?
                 MFALSE : SET_TYPE_OPS(get_mtk_metadata_tag_type(mTag), setCapacity, size);
    return ret == NO_MEMORY ? MFALSE : ret;
}


MVOID
IMetadata::IEntry::Implementor::
clear()
{
      if (get_mtk_metadata_tag_type(mTag) != -1)
        RETURN_TYPE_OPS(get_mtk_metadata_tag_type(mTag), clear());
}


MERROR
IMetadata::IEntry::Implementor::
removeAt(MUINT index)
{
    MERROR ret = get_mtk_metadata_tag_type(mTag) == -1 ?
                  BAD_VALUE : SET_TYPE_OPS(get_mtk_metadata_tag_type(mTag), removeAt, index);
    return ret == BAD_VALUE ? BAD_VALUE : OK;
}


#undef RETURN_TYPE_OPS
#undef SET_TYPE_OPS
#undef SRC_DST_OPERATOR
/******************************************************************************
 *
 ******************************************************************************/
#define AEE_IF_TAG_ERROR(_TAG_) \
    if (_TAG_ == (uint32_t)-1) \
    { \
        CAM_LOGE("tag(%d) error", _TAG_); \
        AEE_ASSERT("tag error"); \
    }

IMetadata::IEntry::
IEntry(Tag_t tag)
    : mpImp(new Implementor(tag))
{
}


IMetadata::IEntry::
IEntry(IMetadata::IEntry const& other)
    : mpImp(new Implementor(*(other.mpImp)))
{
}


IMetadata::IEntry::
~IEntry()
{
    if(mpImp) delete mpImp;
}


IMetadata::IEntry&
IMetadata::IEntry::
operator=(IMetadata::IEntry const& other)
{
    if (this != &other) {
        delete mpImp;
        mpImp = new Implementor(*(other.mpImp));
    }
    else {
        CAM_LOGW("this(%p) == other(%p)", this, &other);
    }

    return *this;
}


MUINT32
IMetadata::IEntry::
tag() const
{
    return mpImp->tag();
}

MINT32
IMetadata::IEntry::
type() const
{
    return mpImp->type();
}

MBOOL
IMetadata::IEntry::
isEmpty() const
{
    //AEE_IF_TAG_ERROR(tag())
    return mpImp->isEmpty();
}


MUINT
IMetadata::IEntry::
count() const
{
    //AEE_IF_TAG_ERROR(tag())
    return mpImp->count();
}


MUINT
IMetadata::IEntry::
capacity() const
{
    AEE_IF_TAG_ERROR(tag())
    return mpImp->capacity();
}


MBOOL
IMetadata::IEntry::
setCapacity(MUINT size)
{
    AEE_IF_TAG_ERROR(tag())
    return mpImp->setCapacity(size);
}


MVOID
IMetadata::IEntry::
clear()
{
    AEE_IF_TAG_ERROR(tag())
    mpImp->clear();
}


MERROR
IMetadata::IEntry::
removeAt(MUINT index)
{
    AEE_IF_TAG_ERROR(tag())
    return mpImp->removeAt(index);
}


#define ASSERT_CHECK(_defaultT, _T) \
      CAM_LOGE_IF( TYPE_##_T != _defaultT, "tag(%x), type(%d) should be (%d)", tag(), TYPE_##_T, _defaultT); \
      if (TYPE_##_T != _defaultT) { \
          Utils::dumpCallStack(); \
          AEE_ASSERT("type mismatch"); \
      }
#undef  ASSERT_CHECK

#define IMETADATA_IENTRY_OPS_DECLARATION(_T) \
MVOID \
IMetadata::IEntry:: \
push_back(_T const& item, Type2Type<_T> type) \
{ \
    AEE_IF_TAG_ERROR(tag()) \
    mpImp->push_back(item, type); \
} \
_T& \
IMetadata::IEntry:: \
editItemAt(MUINT index, Type2Type<_T> type) \
{ \
    AEE_IF_TAG_ERROR(tag()) \
    return mpImp->editItemAt(index, type); \
} \
_T const& \
IMetadata::IEntry:: \
itemAt(MUINT index, Type2Type<_T> type) const \
{ \
    AEE_IF_TAG_ERROR(tag()) \
    return mpImp->itemAt(index, type); \
}

IMETADATA_IENTRY_OPS_DECLARATION(MUINT8)
IMETADATA_IENTRY_OPS_DECLARATION(MINT32)
IMETADATA_IENTRY_OPS_DECLARATION(MFLOAT)
IMETADATA_IENTRY_OPS_DECLARATION(MINT64)
IMETADATA_IENTRY_OPS_DECLARATION(MDOUBLE)
IMETADATA_IENTRY_OPS_DECLARATION(MRational)
IMETADATA_IENTRY_OPS_DECLARATION(MPoint)
IMETADATA_IENTRY_OPS_DECLARATION(MSize)
IMETADATA_IENTRY_OPS_DECLARATION(MRect)
IMETADATA_IENTRY_OPS_DECLARATION(IMetadata)
IMETADATA_IENTRY_OPS_DECLARATION(IMetadata::Memory)
#undef  IMETADATA_IENTRY_OPS_DECLARATION

#undef  AEE_IF_TAG_ERROR
/******************************************************************************
 *
 ******************************************************************************/
class IMetadata::Implementor
{
public:     ////                        Instantiation.
    virtual                            ~Implementor();
                                        Implementor();
    Implementor&                        operator=(Implementor const& other);
                                        Implementor(Implementor const& other);

public:     ////                        operators
    Implementor&                        operator+=(Implementor const& other);
    Implementor const                   operator+(Implementor const& other);

public:     ////                        Accessors.

    /**
     * Check to see whether it is empty (no entries) or not.
     */
    virtual MBOOL                       isEmpty() const;

    /**
     * Return the number of entries.
     */
    virtual MUINT                       count() const;

public:     ////                        Operations.

    /**
     * Clear all entries.
     * Note: Copy-on write.
     */
    virtual MVOID                       clear();

    /**
     * Delete an entry by tag.
     * Note: Copy-on write.
     */
    virtual MERROR                      remove(Tag_t tag);

    /**
     * Sort all entries for faster find.
     * Note: Copy-on write.
     */
    virtual MERROR                      sort();

    /**
     * Update metadata entry. An entry will be created if it doesn't exist already.
     * Note: Copy-on write.
     */
    virtual MERROR                      update(Tag_t tag, IEntry const& entry);


    /**
    * Get metadata entry by tag for editing.
    * Note: Copy-on write.
    */
    virtual IEntry&                     editEntryFor(Tag_t tag);

    /**
    * Get metadata entry by tag, with no editing.
    */
    virtual IEntry const&               entryFor(Tag_t tag) const;

    /**
     * Get metadata entry by index for editing.
     */
    virtual IEntry&                     editEntryAt(MUINT index);

    /**
     * Get metadata entry by index, with no editing.
     */
    virtual IEntry const&               entryAt(MUINT index) const;

    /**
     * Flatten IMetadata.
     */
    virtual ssize_t                     flatten(char* buf, size_t buf_size) const;

    /**
     * Unflatten IMetadata.
     */
    virtual ssize_t                     unflatten(char* buf, size_t buf_size);

    virtual void                        dump(int layer=0);

protected:
    DefaultKeyedVector<Tag_t, IEntry>   mMap;
};


/******************************************************************************
 *
 ******************************************************************************/
IMetadata::Implementor::
Implementor()
    : mMap()
{

}


IMetadata::Implementor::
Implementor(IMetadata::Implementor const& other)
    : mMap(other.mMap)
{
}


IMetadata::Implementor::
~Implementor()
{

}


IMetadata::Implementor&
IMetadata::Implementor::
operator+=(IMetadata::Implementor const& other)
{
    if (this != &other)
    {
        if( mMap.size() >= other.mMap.size() ) {
            for(size_t idx = 0; idx < other.mMap.size(); idx++ )
                mMap.add( other.mMap.keyAt(idx), other.mMap.valueAt(idx) );
        } else {
            auto temp = other.mMap;
            for(size_t idx = 0; idx < mMap.size(); idx++ )
                temp.add( mMap.keyAt(idx), mMap.valueAt(idx) );
            mMap = temp;
        }
    }
    else {
        CAM_LOGW("this(%p) == other(%p)", this, &other);
    }

    return *this;
}


IMetadata::Implementor const
IMetadata::Implementor::
operator+(IMetadata::Implementor const& other)
{
    return Implementor(*this) += other;
}


IMetadata::Implementor&
IMetadata::Implementor::
operator=(IMetadata::Implementor const& other)
{
    if (this != &other)
    {
        //release mMap'storage
        //assign other.mMap's storage pointer to mMap
        //add 1 to storage's sharebuffer
        mMap = other.mMap;
    }
    else {
        CAM_LOGW("this(%p) == other(%p)", this, &other);
    }

    return *this;
}


MBOOL
IMetadata::Implementor::
isEmpty() const
{
    return mMap.isEmpty();
}


MUINT
IMetadata::Implementor::
count() const
{
    return mMap.size();
}


MVOID
IMetadata::Implementor::
clear()
{
    mMap.clear();
}


MERROR
IMetadata::Implementor::
remove(Tag_t tag)
{
    return mMap.removeItem(tag);
}


MERROR
IMetadata::Implementor::
sort()
{

    //keyedVector always sorted.
    return OK;
}


MERROR
IMetadata::Implementor::
update(Tag_t tag, IEntry const& entry)
{
    return mMap.add(tag, entry);

}


IMetadata::IEntry&
IMetadata::Implementor::
editEntryFor(Tag_t tag)
{
    return mMap.editValueFor(tag);
}


IMetadata::IEntry const&
IMetadata::Implementor::
entryFor(Tag_t tag) const
{
    return mMap.valueFor(tag);
}


IMetadata::IEntry&
IMetadata::Implementor::
editEntryAt(MUINT index)
{
    return mMap.editValueAt(index);

}


IMetadata::IEntry const&
IMetadata::Implementor::
entryAt(MUINT index) const
{
    return mMap.valueAt(index);

}

#define DUMP_METADATA_STRING(_layer_, _msg_) \
        CAM_LOGD("(L%d) %s", _layer_, _msg_.string());


void
IMetadata::Implementor::
dump(int layer)
{
    for (size_t i = 0; i < mMap.size(); ++i) {
        IMetadata::IEntry entry = mMap.valueAt(i);
        android::String8 msg = String8::format( "[%s] Map(%zu/%zu) tag(0x%x) type(%d) count(%d) ",
            __FUNCTION__, i, mMap.size(), entry.tag(), entry.type(), entry.count());
        //
        if ( TYPE_IMetadata == entry.type() ) {
            for( size_t j = 0; j < entry.count(); ++j ) {
                IMetadata* meta = &entry.editItemAt(j, Type2Type< IMetadata >());
                msg += String8::format("metadata.. ");
                DUMP_METADATA_STRING(layer, msg);
                meta->dump(layer+1);
            }
        } else {
            switch( entry.type() )
            {
            case TYPE_MUINT8:
                for (size_t j=0; j<entry.count(); j++)
                    msg += String8::format("%d ", entry.itemAt(j, Type2Type< MUINT8 >() ));
                break;
            case TYPE_MINT32:
                for (size_t j=0; j<entry.count(); j++)
                    msg += String8::format("%d ", entry.itemAt(j, Type2Type< MINT32 >() ));
                break;
            case TYPE_MINT64:
                for (size_t j=0; j<entry.count(); j++)
                    msg += String8::format("%" PRId64 " ", entry.itemAt(j, Type2Type< MINT64 >() ));
                break;
            case TYPE_MFLOAT:
                for (size_t j=0; j<entry.count(); j++)
                    msg += String8::format("%f ", entry.itemAt(j, Type2Type< MFLOAT >() ));
                break;
            case TYPE_MDOUBLE:
                for (size_t j=0; j<entry.count(); j++)
                    msg += String8::format("%lf ", entry.itemAt(j, Type2Type< MDOUBLE >() ));
                break;
            case TYPE_MSize:
                for (size_t j=0; j<entry.count(); j++)
                {
                    MSize src_size = entry.itemAt(j, Type2Type< MSize >());
                    msg += String8::format( "size(%d,%d) ", src_size.w, src_size.h );
                }
                break;
            case TYPE_MRect:
                for (size_t j=0; j<entry.count(); j++)
                {
                    MRect src_rect = entry.itemAt(j, Type2Type< MRect >());
                    msg += String8::format( "rect(%d,%d,%d,%d) ",
                                           src_rect.p.x, src_rect.p.y,
                                           src_rect.s.w, src_rect.s.h );
                }
                break;
            case TYPE_MPoint:
                for (size_t j=0; j<entry.count(); j++)
                {
                    MPoint src_point = entry.itemAt(j, Type2Type< MPoint >());
                    msg += String8::format( "point(%d,%d) ", src_point.x, src_point.y );
                }
                break;
            case TYPE_MRational:
                for (size_t j=0; j<entry.count(); j++)
                {
                    MRational src_rational = entry.itemAt(j, Type2Type< MRational >());
                    msg += String8::format( "rational(%d,%d) ", src_rational.numerator, src_rational.denominator );
                }
                break;
            case TYPE_Memory:
                msg += String8::format("Memory type: not dump!");
                break;
            default:
                msg += String8::format("unsupported type(%d)", entry.type());
            }
        }
    DUMP_METADATA_STRING(layer, msg);
    }
}


/******************************************************************************
 *
 ******************************************************************************/
#if 0
static int sFlattenLayer = 0;
static int sUnflattenLayer = 0;
#define CAM_LOGD_TIME(fmt, arg...)     CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#else
#define CAM_LOGD_TIME(fmt, arg...)
#endif

struct metadata_buffer_entry_info {
    MUINT32 tag;
    MUINT8 type;
    MUINT8 count;
};

struct metadata_buffer_handle {

    metadata_buffer_handle(char* b, size_t s)
        :buffer(b),
         size(s),
         offset(0)
    {}

    MERROR get_entry_count(MUINT32 &entry_count)
    {
        align(sizeof(entry_count));

        entry_count = *((MUINT32*)current());
        move(sizeof(entry_count));

        return OK;
    }

    MERROR set_entry_count(MUINT32 entry_count)
    {
        align(sizeof(entry_count));

        *((MUINT32*)current()) = entry_count;
        move(sizeof(entry_count));

        return OK;
    }

    inline MERROR get_entry_info(metadata_buffer_entry_info& info)
    {
        align(alignof(metadata_buffer_entry_info));

        if (!check_size(sizeof(metadata_buffer_entry_info))) {
            CAM_LOGE("[get_entry_info] out of boundary");
            return NO_MEMORY;
        }

        memcpy(&info, current(), sizeof(metadata_buffer_entry_info));
        move(sizeof(metadata_buffer_entry_info));

        //CAM_LOGD("[get_info] tag:%d type:%d count:%d offset:%zu", info.tag, info.type, info.count, offset);
        return OK;
    }

    inline MERROR set_entry_info(metadata_buffer_entry_info& info)
    {
        align(alignof(metadata_buffer_entry_info));

        if (!check_size(sizeof(metadata_buffer_entry_info))) {
            CAM_LOGE("[set_entry_info] out of boundary");
            return NO_MEMORY;
        }

        memcpy(current(), &info, sizeof(metadata_buffer_entry_info));
        move(sizeof(metadata_buffer_entry_info));

        //CAM_LOGD("[set_info] tag:%d type:%d count:%d offset:%zu", info.tag, info.type, info.count, offset);
        return OK;
    }

    inline MBOOL check_size(size_t data_size)
    {
        if (size < offset + data_size) {
            CAM_LOGE("memory not enough, size=%zu, offset=%zu, data=%zu",
                    size, offset, data_size);
            return MFALSE;
        }
        return MTRUE;
    }

    inline MVOID align(MUINT8 alignment)
    {
        offset += alignment - ((uintptr_t)current() % alignment);
    }

    inline char* current()
    {
        return buffer + offset;
    }

    inline size_t move(size_t m)
    {
        offset += m;
        return offset;
    }

    char* buffer;
    size_t size;
    size_t offset;
};

template<class T>
static MERROR
write_to_buffer(metadata_buffer_handle& handle, const IMetadata::IEntry& entry)
{
    if (!handle.check_size(sizeof(T) * entry.count()))
        return NO_MEMORY;

    for(MUINT8 i = 0; i < entry.count(); i++) {
        const T& t = entry.itemAt(i, Type2Type<T>());
        *((T*)handle.current()) = t;
        handle.move(sizeof(T));
    }

    return OK;
}

template<class T>
static MERROR
read_from_buffer(metadata_buffer_handle& handle, IMetadata::IEntry& entry, MINT32 count)
{
    if (!handle.check_size(sizeof(T) * count))
        return NO_MEMORY;

    entry.setCapacity(count);

    T *p;
    for(MUINT8 i = 0; i < count; i++) {
        p = (T*)handle.current();
        entry.push_back(*p, Type2Type<T>());
        handle.move(sizeof(T));
    }

    return OK;
}

template<>
MERROR
write_to_buffer<IMetadata::Memory>(metadata_buffer_handle& handle, const IMetadata::IEntry& entry)
{
    for(uint32_t i = 0; i < entry.count(); i++) {
        const IMetadata::Memory& m = entry.itemAt(i, Type2Type<IMetadata::Memory>());

        if (i)
            handle.align(sizeof(MUINT32));

        if (!handle.check_size(sizeof(MUINT32) + sizeof(MUINT8) * m.size()))
            return NO_MEMORY;

        size_t s = m.size();
        *((size_t*)handle.current()) = s;
        handle.move(sizeof(s));
        memcpy(handle.current(), m.array(), m.size());
        handle.move(sizeof(MUINT8) * m.size());
    }

    return OK;
}

template<>
MERROR
read_from_buffer<IMetadata::Memory>(metadata_buffer_handle& handle, IMetadata::IEntry& entry, MINT32 count)
{
    MUINT32 s;
    for (MUINT8 i = 0; i < count; i++) {
        if (i)
            handle.align(sizeof(MUINT32));

        if (!handle.check_size(sizeof(MUINT32)))
            return NO_MEMORY;

        s = *((size_t*)handle.current());
        handle.move(sizeof(MUINT32));

        if (!handle.check_size(sizeof(MUINT8) * s))
            return NO_MEMORY;

        IMetadata::Memory mMemory;
        mMemory.resize(s);
        memcpy(mMemory.editArray(), handle.current(), sizeof(MUINT8) * s);
        handle.move(s);

        entry.push_back(mMemory, Type2Type<IMetadata::Memory>());
    }

    return OK;
}

template<>
MERROR
write_to_buffer<IMetadata>(metadata_buffer_handle& handle, const IMetadata::IEntry& entry)
{
    for(uint32_t i = 0; i < entry.count(); i++) {
        const IMetadata& m = entry.itemAt(i, Type2Type<IMetadata>());
        handle.offset += m.flatten(handle.buffer + handle.offset, handle.size - handle.offset);
    }

    return OK;
}

template<>
MERROR
read_from_buffer<IMetadata>(metadata_buffer_handle& handle, IMetadata::IEntry& entry, MINT32 count)
{
    for (MUINT8 i = 0; i < count; i++) {
        IMetadata meta;
        handle.offset += meta.unflatten(handle.buffer + handle.offset, handle.size - handle.offset);
        entry.push_back(meta, Type2Type<IMetadata>());
    }
    return OK;
}


ssize_t
IMetadata::Implementor::
flatten(char* buf, size_t buf_size) const
{
    MINT64 start_time = static_cast<MINT64>(::systemTime());
    CAM_LOGD_TIME("layer:%d", ++sFlattenLayer);

    int ret = 0;
    metadata_buffer_handle handle(buf, buf_size);
    metadata_buffer_entry_info info;
    handle.set_entry_count(mMap.size());


    for(MUINT32 i = 0; i < mMap.size(); ++i) {
        const IMetadata::IEntry& entry = mMap.valueAt(i);

        info.tag = entry.tag();
        info.type = entry.type();
        info.count = entry.count();
        handle.set_entry_info(info);

        typedef NSCam::IMetadata IMetadata;
        switch (info.type) {

#define CASE_WRITER(_type_)                                         \
            case TYPE_##_type_ :                                    \
                ret = write_to_buffer<_type_>(handle, entry);       \
                break;

            CASE_WRITER(MUINT8);
            CASE_WRITER(MINT32);
            CASE_WRITER(MFLOAT);
            CASE_WRITER(MINT64);
            CASE_WRITER(MDOUBLE);
            CASE_WRITER(MRational);
            CASE_WRITER(MPoint);
            CASE_WRITER(MSize);
            CASE_WRITER(MRect);
            CASE_WRITER(IMetadata);
            CASE_WRITER(Memory);
#undef CASE_WRITER
            default:
                CAM_LOGE("[flatten] unsupported format:%ul", info.type);
                ret = BAD_VALUE;
                continue;
        }

        if (ret < 0) return ret;
    }
    MINT64 end_time = static_cast<MINT64>(::systemTime());
    CAM_LOGD_TIME("layer:%d offset:%zu time:%" PRIu64 "us",
            sFlattenLayer--, handle.offset, (end_time - start_time) / 1000);

    return handle.offset;
}


ssize_t
IMetadata::Implementor::
unflatten(char* buf, size_t buf_size)
{
    MINT64 start_time = static_cast<MINT64>(::systemTime());
    CAM_LOGD_TIME("layer:%d", ++sUnflattenLayer);

    int ret = 0;
    metadata_buffer_handle handle(buf, buf_size);
    metadata_buffer_entry_info info;
    MUINT32 entry_count;
    handle.get_entry_count(entry_count);
    mMap.setCapacity(entry_count);

    for (MUINT32 i = 0; i < entry_count; i++) {
        handle.get_entry_info(info);
        IMetadata::IEntry entry(info.tag);

        typedef NSCam::IMetadata IMetadata;
        switch (info.type) {

#define CASE_READER(_type_)                                                 \
            case TYPE_##_type_ :                                            \
                ret = read_from_buffer<_type_>(handle, entry, info.count);  \
                break;

            CASE_READER(MUINT8);
            CASE_READER(MINT32);
            CASE_READER(MFLOAT);
            CASE_READER(MINT64);
            CASE_READER(MDOUBLE);
            CASE_READER(MRational);
            CASE_READER(MPoint);
            CASE_READER(MSize);
            CASE_READER(MRect);
            CASE_READER(IMetadata);
            CASE_READER(Memory);
#undef CASE_READER
            default:
                CAM_LOGE("[unflatten] unsupported format:%ul", info.type);
                ret = BAD_VALUE;
                continue;
        }

        if (ret < 0) return ret;

        mMap.add(info.tag, entry);
    }

    MINT64 end_time = static_cast<MINT64>(::systemTime());
    CAM_LOGD_TIME("layer:%d offset:%zu time:%" PRIu64 "us",
            sUnflattenLayer--, handle.offset, (end_time - start_time) / 1000);

    return handle.offset;
}


/******************************************************************************
 *
 ******************************************************************************/
IMetadata::
IMetadata()
    : mpImp(new Implementor())
{

}


IMetadata::IMetadata(IMetadata const& other)
    : mpImp(new Implementor(*(other.mpImp)))
{
}


IMetadata::
~IMetadata()
{
     if(mpImp) delete mpImp;

}


IMetadata&
IMetadata::operator=(IMetadata const& other)
{
    if (this != &other) {
        delete mpImp;
        mpImp = new Implementor(*(other.mpImp));
    }
    else {
        CAM_LOGW("this(%p) == other(%p)", this, &other);
    }

    return *this;
}


IMetadata&
IMetadata::operator+=(IMetadata const& other)
{
    *mpImp += *other.mpImp;
    return *this;
}


IMetadata const
IMetadata::operator+(IMetadata const& other)
{
    return IMetadata(*this) += other;
}


MBOOL
IMetadata::
isEmpty() const
{
    return mpImp->isEmpty();
}


MUINT
IMetadata::
count() const
{
    return mpImp->count();
}


MVOID
IMetadata::
clear()
{
    mpImp->clear();
}


MERROR
IMetadata::
remove(Tag_t tag)
{
    return mpImp->remove(tag) >= 0 ? OK : BAD_VALUE;
}


MERROR
IMetadata::
sort()
{
    return mpImp->sort();
}


MERROR
IMetadata::
update(Tag_t tag, IMetadata::IEntry const& entry)
{
    MERROR ret = mpImp->update(tag, entry);  //keyedVector has two possibilities: BAD_VALUE/NO_MEMORY
    return ret >= 0 ? (MERROR)OK : (MERROR)ret;
}


IMetadata::IEntry&
IMetadata::
editEntryFor(Tag_t tag)
{
    return mpImp->editEntryFor(tag);
}


IMetadata::IEntry const&
IMetadata::
entryFor(Tag_t tag) const
{
    return mpImp->entryFor(tag);
}

IMetadata::IEntry&
IMetadata::
editEntryAt(MUINT index)
{
    return mpImp->editEntryAt(index);

}


IMetadata::IEntry const&
IMetadata::
entryAt(MUINT index) const
{
    return mpImp->entryAt(index);
}

ssize_t
IMetadata::
flatten(void* buf, size_t buf_size) const
{
    return mpImp->flatten(static_cast<char*>(buf), buf_size);
}

ssize_t
IMetadata::
unflatten(void* buf, size_t buf_size)
{
    return mpImp->unflatten(static_cast<char*>(buf), buf_size);
}

void IMetadata::
dump(int layer)
{
    mpImp->dump(layer);
}
