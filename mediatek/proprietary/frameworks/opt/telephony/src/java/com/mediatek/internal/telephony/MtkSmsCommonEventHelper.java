/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */


package com.mediatek.internal.telephony;

import android.os.PowerManager;
import android.os.Handler;
import android.os.Build;
import android.os.Binder;
import android.os.Message;
import android.os.SystemProperties;
import android.app.ActivityManager;
import android.provider.Telephony;
import android.provider.Telephony.Sms;
import android.content.Intent;
import android.content.Context;

import android.telephony.Rlog;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Phone;

import mediatek.telephony.MtkTelephony;

import java.util.Iterator;
import java.util.List;


public class MtkSmsCommonEventHelper extends Handler {
    static final String TAG = "MtkSmsCommonEventHelper";    // accessed from inner class

    /**
     * Hold the wake lock for 5 seconds, which should be enough time for
     * any receiver(s) to grab its own wake lock.(SMS ready intent)
     */
    private static final int WAKE_LOCK_TIMEOUT = 500;
    /** Wake lock to ensure device stays awake while dispatching the SMS ready intent. */
    private PowerManager.WakeLock mWakeLock;

    private boolean mSmsReady = false;

    /** SMS subsystem in the modem is ready. */
    static final protected int EVENT_SMS_READY = 0;

    private static final boolean ENG = "eng".equals(Build.TYPE);

    private static MtkSmsCommonEventHelper[] sInstances =
            new MtkSmsCommonEventHelper[TelephonyManager.getDefault().getPhoneCount()];

    private Phone mPhone;
    private CommandsInterface mCi;

    public static final String SELECT_BY_REFERENCE = "address=? AND reference_number=? AND " +
            "count=? AND deleted=0 AND sub_id=?";

    private MtkSmsCommonEventHelper(Phone phone) {
        mPhone = phone;
        mCi = phone.mCi;

        // Register EVENT_SMS_READY
        createWakelock();
        MtkRIL ci = (MtkRIL)mCi;
        ci.registerForSmsReady(this, EVENT_SMS_READY, null);
        Rlog.d(TAG, "SmsCommonEventHelp created for Phone " + phone.getPhoneId());
    }

    public static MtkSmsCommonEventHelper getInstance(Phone phone) {
        if (sInstances[phone.getPhoneId()] == null) {
            sInstances[phone.getPhoneId()] = new MtkSmsCommonEventHelper(phone);
        }

        return sInstances[phone.getPhoneId()];
    }

    /**
     * @param msg the message to handle
     */
    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
        case EVENT_SMS_READY:
        {
            Rlog.d(TAG, "SMS is ready, Phone: " + mPhone.getPhoneId());
            mSmsReady = true;

            notifySmsReady(mSmsReady);
            break;
        }

        default:
            Rlog.e(TAG, "handleMessage() ignoring message of unexpected type " + msg.what);
        }
    }

    /**
     * create wake lock for sms ready intent.
     */
    private void createWakelock() {
        PowerManager pm = (PowerManager) mPhone.getContext().getSystemService(
                Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SmsCommonEventHelp");
        mWakeLock.setReferenceCounted(true);
    }

    private void notifySmsReady(boolean isReady) {
        // broadcast SMS_STATE_CHANGED_ACTION intent
        Intent intent = new Intent(MtkTelephony.SMS_STATE_CHANGED_ACTION);
        intent.putExtra("ready", isReady);
        SubscriptionManager.putPhoneIdAndSubIdExtra(intent, mPhone.getPhoneId());
        mWakeLock.acquire(WAKE_LOCK_TIMEOUT);
        mPhone.getContext().sendBroadcast(intent);
    }

    public boolean isSmsReady() {
        return mSmsReady;
    }

    /**
     * Indicates isPrimary for ETWS..
     * @hide pending API council approval
     */
    public static final String IS_EMERGENCY_CB_PRIMARY = "isPrimary";

    private static final boolean IS_PRIVACY_PROTECTION_LOCK_SUPPORT =
            SystemProperties.get("ro.mtk_privacy_protection_lock").equals("1");

    private static final boolean IS_WAPPUSH_SUPPORT =
            SystemProperties.get("ro.mtk_wappush_support").equals("1");

    public static boolean isPrivacyLockSupport() {
        return IS_PRIVACY_PROTECTION_LOCK_SUPPORT;
    }

    public static boolean isWapPushSupport() {
        return IS_WAPPUSH_SUPPORT;
    }

    /**
     * Because it may have multiple apks use the same uid, ex. Mms.apk and omacp.apk, we need to
     * exactly find the correct calling apk. We should use running process to check the correct
     * apk. If we could not find the process via pid, this apk may be killed. We will use the
     * default behavior, find the first package name via uid.
     *
     * @param packageNames package names query from user id
     * @return null if package names length is zero or could not match the process id;
     *         package name match the process id
     */
    public String getPackageNameViaProcessId(Context context, String[] packageNames) {
        String packageName = null;

        if (packageNames.length == 1) {
            packageName = packageNames[0];
        } else if (packageNames.length > 1) {
            int callingPid = Binder.getCallingPid();
            Iterator index = null;

            ActivityManager am = (ActivityManager) context.getSystemService(
                    Context.ACTIVITY_SERVICE);
            List processList = am.getRunningAppProcesses();
            if (processList != null) {
                index = processList.iterator();
                while (index.hasNext()) {
                    ActivityManager.RunningAppProcessInfo processInfo =
                            (ActivityManager.RunningAppProcessInfo) (index.next());
                    if (callingPid == processInfo.pid) {
                        for (String pkgInProcess : processInfo.pkgList) {
                            for (String pkg : packageNames) {
                                if (pkg.equals(pkgInProcess)) {
                                    packageName = pkg;
                                    break;
                                }
                            }
                            if (packageName != null) {
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }

        return packageName;
    }
}
