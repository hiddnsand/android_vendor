/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.gallery.op01;

import android.content.Context;
import android.os.SystemProperties;
import android.util.Log;

import com.android.gallery3d.data.MediaItem;
import com.android.gallery3d.ui.PhotoView.Picture;
import com.android.gallery3d.ui.PositionController;
import com.android.gallery3d.ui.ScreenNail;
import com.android.gallery3d.ui.TileImageViewAdapter;

import com.mediatek.gallery3d.ext.DefaultImageOptionsExt;
import com.mediatek.gallerybasic.base.MediaData;

/**
 * OP01 plugin implementation of gallery image feature.
 */
public class Op01ImageOptionsExt extends DefaultImageOptionsExt {
    private static final String TAG = "Gallery2/Op01ImageOptionsExt";
    public static final int THUMBNAIL_TARGET_SIZE = 640;
    private static final boolean DBG = false;
    private static final boolean VDBG = SystemProperties.get("ro.build.type").equals("eng") ?
            true : false;

    public Op01ImageOptionsExt(Context context) {
        super(context);
    }

    @Override
    public float getImageDisplayScale(float initScale, MediaItem mediaItem) {
        if (DBG) {
            Log.i(TAG, "<getImageDisplayScale> input initScale: " + initScale);
        }
        if (mediaItem == null || !checkIfNeedOriginalSize(mediaItem)) {
            return initScale;
        }
        // we think of specified size image as special image
        // such as 1024 x 1, 1600 x 1
        int w = mediaItem.getWidth();
        int h = mediaItem.getHeight();
        int scale = Math.max(w, h) / THUMBNAIL_TARGET_SIZE;
        if (scale != 0 && (w / scale == 0 || h / scale == 0)) {
            if (VDBG) {
                Log.i(TAG, "<getImageDisplayScale> is special image, w: " + w
                        + ", h: " + h + ", not use original size!");
            }
            return initScale;
        }
        float result = Math.min(initScale, 1.0f);
        if (DBG) {
            Log.i(TAG, "<getImageDisplayScale> final initScale: " + result);
        }
        return result;
    }

    @Override
    public float getMinScaleLimit(MediaData mediaData, float scale) {
        if (mediaData == null || mediaData.isVideo) {
            if (DBG) {
                Log.i(TAG, "<getMinScaleLimit> video or null, not use original size!");
            }
            return scale;
        }
        float minScaleLimit = Math.min(scale, 1.0f);
        if (DBG) {
            Log.i(TAG, "<getMinScaleLimit> minScaleLimit: " + minScaleLimit);
        }
        return minScaleLimit;
    }

    @Override
    public void updateTileProviderWithScreenNail(TileImageViewAdapter adapter,
            ScreenNail screenNail) {
        if (adapter == null || screenNail == null
                || screenNail.getMediaItem() == null) {
            return;
        }

        if (!checkIfNeedOriginalSize(screenNail.getMediaItem())) {
            return;
        }

        if (VDBG) {
            Log.i(TAG, "<updateTileProviderWithScreenNail> screenNail: " + screenNail +
                ", MediaItem: " + screenNail.getMediaItem());
        }

        adapter.updateWidthAndHeight(screenNail.getMediaItem());
    }

    @Override
    public void updateMediaData(Picture picture, ScreenNail screenNail) {
        if (picture == null || screenNail == null
                || screenNail.getMediaItem() == null
                || screenNail.getMediaItem().getMediaData() == null) {
            if (picture != null) {
                picture.updateMediaData(null);
            }
            if (VDBG) {
                Log.d(TAG, "[DBG] updateMediaData returned for item or data null, picture: "
                    + picture);
            }
            return;
        }
        MediaData data = screenNail.getMediaItem().getMediaData();
        if (DBG) {
            Log.i(TAG, "<updateMediaData> mediaData " + data);
        }

        picture.updateMediaData(data);
    }

    @Override
    public void updateBoxMediaData(PositionController controller, int index,
            MediaData mediaData) {
        if (controller == null) {
            if (DBG) {
                Log.i(TAG, "<updateBoxMediaData> return!");
            }
            return;
        }
        controller.setMediaData(index, mediaData);
    }

    private boolean checkIfNeedOriginalSize(MediaItem item) {
        if (item == null) {
            if (DBG) {
                Log.i(TAG, "<checkIfNeedOriginalSize> MediaItem is null or unknow!");
            }
            return false;
        }

        MediaData data = item.getMediaData();

        // Host remove VideoItem since Android N and there will
        // be no MediaType.VIDEO. We need to use MediaData.isVideo
        // to check if it's video or not
        if (data == null || data.isVideo) {
            if (VDBG) {
                Log.i(TAG, "<checkIfNeedOriginalSize> video or null, not use original size!");
            }
            return false;
        }
        if (VDBG) {
            Log.i(TAG, "<checkIfNeedOriginalSize> use original size, item: " +
                item + ", data: " + data);
        }
        return true;
    }
}
