package com.mediatek.op06.phone;

import android.content.Context;



/**
 * Class to support operator customizations for WFC settings.
 */
public class OP06WfcSettings {

    private static final String TAG = "Op06WfcSettings";
    private static final int WFC_USER_MODE_CHANGE_TYPE = 1;

    private static OP06WfcSettings sWfcSettings = null;
    private Context mPluginContext;
    private WfcSwitchController mController;

    private OP06WfcSettings(Context context) {
       mPluginContext = context;
       mController = WfcSwitchController.getInstance(context);
    }

    /** Returns instance of OP06WfcSettings.
         * @param context context
         * @return OP06WfcSettings
         */
    public static OP06WfcSettings getInstance(Context context) {

        if (sWfcSettings == null) {
            sWfcSettings = new OP06WfcSettings(context);
        }
        return sWfcSettings;
    }

    /** Customize WFC pref as per operator requirement,
         * i.e. add WFC switch instead of AOSP pref screen for UK & DE
         * @param context context
         * @param preferenceScreen preferenceScreen
         * @return
         */
    public void customizedWfcPreference(Context context, Object preferenceScreen) {
        mController.customizedWfcPreference(context, preferenceScreen);
    }

    /** Returns instance of OP18WfcSettings.
     * @param preferenceScreen preferenceScreen of WFC settings
     * @return
     */
    public void removeWfcPreference(Object preferenceScreen) {
        mController.removeWfcPreference(preferenceScreen);
    }
}
