/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M Texture2D interface.
 */

package com.mediatek.j3m;

/* Extend JNI Overview, part of H2 Appearances */ /*! \addtogroup a3mJusr

<h3>2D Textures</h3>

A Texture2D is simply a data array with each byte or set of bytes representing a
pixel of colour or similar data.  Many formats are possible from FORMAT_RGBA
that has 4 'channels' of colour data per pixel, to FORMAT_DEPTH that has one
'channel' of coded depth information per pixel.

How these items are arranged in a data word is defined by the packing type, for
example: TYPE_UNSIGNED_BYTE where each channel occupies one byte (so RGBA is a
32-bit word), and TYPE_UNSIGNED_SHORT where each channel occupies 16 bits,
typically used for a depth texture (where 8 bits of depth resolution is rarely
sufficient).

Other parameters affect how the texture is used, what happens when the renderer
reaches the edge of the texture (does it repeat or reflect?) or needs to be
scaled.

*/

/*!
 * \ingroup a3mJusrAppear
 * @{
 */
/**
 * Texture2D encapsulates a 2D texture object.
 * This class wraps native methods implemented in a3m::Texture2D class.
 */
public interface Texture2D extends Texture {
    /**
     * Returns the width of the texture in pixels.
     *
     * @return Texture width
     */
    int getWidth();

    /**
     * Returns the height of the texture in pixels.
     *
     * @return Texture height
     */
    int getHeight();

    /**
     * Sets the horizontal wrap mode.
     * This specifies what happens when the U texture coordinate does outside
     * of the normal range (0 - 1).
     *
     * @param mode Wrap mode
     */
    void setHorizontalWrap(int mode);

    /**
     * Returns the horizontal wrap mode.
     *
     * @return Wrap mode
     */
    int getHorizontalWrap();

    /**
     * Sets the vertical wrap mode.
     * This specifies what happens when the V texture coordinate does outside
     * of the normal range (0 - 1).
     *
     * @param mode Wrap mode
     */
    void setVerticalWrap(int mode);

    /**
     * Returns the vertical wrap mode.
     *
     * @return Wrap mode
     */
    int getVerticalWrap();

    /**
     * Sets the minification filter mode.
     * This specifies how a texture is downsampled.
     *
     * @param mode Filter mode
     */
    void setMinFilter(int mode);

    /**
     * Returns the minification filter mode.
     *
     * @return Filter mode
     */
    int getMinFilter();

    /**
     * Sets the magnification filter mode.
     * This specifies how a texture is upsampled.
     *
     * @param mode Filter mode
     */
    void setMagFilter(int mode);

    /**
     * Returns the magnification filter mode.
     *
     * @return Filter mode
     */
    int getMagFilter();

    /**
     * Returns the amount of memory occupied by this texture in bytes.
     * This figure is a best estimation, and might not be representative of the
     * exact amount of memory used by any particular OpenGL implementation.
     *
     * @return Memory usage estimation in bytes
     */
    int getMemoryUsage();

    /**
     * Returns the internal OpenGL texture ID.
     *
     * @return OpenGL texture ID
     */
    int getOpenGlTextureId();

    void release();	
}

/*! @} */
