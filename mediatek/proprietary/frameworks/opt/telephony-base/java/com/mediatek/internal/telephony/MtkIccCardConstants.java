/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony;

import android.telephony.TelephonyManager;
import com.android.internal.telephony.IccCardConstants;

public class MtkIccCardConstants extends IccCardConstants{

    // MTK-START: SIM ME LOCK
    // Added by M begin
    /**
     * NETWORK_SUBSET means ICC is locked on NETWORK SUBSET PERSONALIZATION.
     * @internal
     */
    public static final String INTENT_VALUE_LOCKED_NETWORK_SUBSET = "NETWORK_SUBSET";
    /**
     * CORPORATE means ICC is locked on CORPORATE PERSONALIZATION.
     * @internal
     */
    public static final String INTENT_VALUE_LOCKED_CORPORATE = "CORPORATE";
    /**
     * SERVICE_PROVIDER means ICC is locked on SERVICE_PROVIDER PERSONALIZATION.
     * @internal
     */
    public static final String INTENT_VALUE_LOCKED_SERVICE_PROVIDER = "SERVICE_PROVIDER";
    /**
     * SIM means ICC is locked on SIM PERSONALIZATION.
     * @internal
     */
    public static final String INTENT_VALUE_LOCKED_SIM = "SIM";
    // Added by M end
    // MTK-END

    /**
     * This card type is report by CDMA(VIA) modem.
     * for CT requset to detect card type then give a warning
     * @deprecated - use IccCardType instead
     */
    @Deprecated public enum CardType {
        UIM_CARD(1),             //ICC structure, non CT UIM card
        SIM_CARD(2),             //ICC structure, non CT SIM card
        UIM_SIM_CARD(3),         //ICC structure, non CT dual mode card
        UNKNOW_CARD(4),          //card is present, but can't detect type
        CT_3G_UIM_CARD(5),       //ICC structure, CT 3G UIM card
        CT_UIM_SIM_CARD(6),      //ICC structure, CT 3G dual mode card
        PIN_LOCK_CARD(7),        //this card need PIN
        CT_4G_UICC_CARD(8),      //UICC structure, CT 4G dual mode UICC card
        NOT_CT_UICC_CARD(9),     //UICC structure, Non CT 4G dual mode UICC card
        LOCKED_CARD(18),         //card is locked
        CARD_NOT_INSERTED(255);  //card is not inserted

        private int mValue;

        public int getValue() {
            return mValue;
        }

        /**
         * Get CardType from integer.
         * ASSERT: Please DON'T directly use CardType.values(), otherwise JE will occur
         * @param cardTypeInt for cardType index.
         * @return CardType.
         */
        public static MtkIccCardConstants.CardType getCardTypeFromInt(int cardTypeInt) {
            CardType cardType = CARD_NOT_INSERTED;
            CardType[] cardTypes = CardType.values();
            for (int i = 0; i < cardTypes.length; i++) {
                if (cardTypes[i].getValue() == cardTypeInt) {
                    cardType = cardTypes[i];
                    break;
                }
            }
            return cardType;
        }

        /**
         * Check if it is 4G card.
         * @return true if it is 4G card
         */
        public boolean is4GCard() {
            return ((this == CT_4G_UICC_CARD) || (this == NOT_CT_UICC_CARD));
        }

        /**
         * Check if it is 3G card.
         * @return true if it is 3G card
         */
        public boolean is3GCard() {
            return ((this == UIM_CARD) || (this == CT_UIM_SIM_CARD)
                    || (this == CT_3G_UIM_CARD) || (this == CT_UIM_SIM_CARD));
        }

        /**
         * Check if it is dual mode card.
         * @return true if it is dual mode card
         */
        public boolean isDualModeCard() {
            return ((this == UIM_SIM_CARD) || (this == CT_UIM_SIM_CARD)
                    || (this == CT_4G_UICC_CARD) || (this == NOT_CT_UICC_CARD));
        }

        /**
         * Check if it is OP09 card.
         * @return true if it is OP09 card
         */
        public boolean isOPO9Card() {
            return ((this == CT_3G_UIM_CARD) || (this == CT_UIM_SIM_CARD)
                    || (this == CT_4G_UICC_CARD));
        }

        private CardType(int value) {
            mValue = value;
        }
    }

    // External SIM [START]
    /**
     *  Use the detemine VSIM card type.
     */
    public enum VsimType {
        LOCAL_SIM,
        REMOTE_SIM,
        SOFT_AKA_SIM,
        PHYSICAL_AKA_SIM,
        PHYSICAL_SIM;

        public boolean isUserDataAllowed() {
            return ((this == SOFT_AKA_SIM) || (this == PHYSICAL_AKA_SIM));
        }

        public boolean isDataRoamingAllowed() {
            return ((this == SOFT_AKA_SIM) || (this == REMOTE_SIM));
        }

        public boolean isAllowVsimConnection() {
            return ((this == SOFT_AKA_SIM) || (this == PHYSICAL_AKA_SIM));
        }

        public boolean isAllowReqNonVsimNetwork() {
            return (this != SOFT_AKA_SIM);
        }

        public boolean isAllowOnlyVsimNetwork() {
            return (this == SOFT_AKA_SIM);
        }

        public boolean isVsimCard() {
            return ((this != PHYSICAL_SIM) && (this != PHYSICAL_AKA_SIM));
        }
    }
    // External SIM [END]
}
