//#define LOG_TAG "FpsPolicy"
#define ATRACE_TAG ATRACE_TAG_GRAPHICS

#include <dlfcn.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <inttypes.h>
#include <cutils/log.h>
#include <cutils/properties.h>
#include <utils/SortedVector.h>
#include <binder/PermissionCache.h>

#include <cutils/memory.h>
#include <utils/Trace.h>

#include "dfps/FpsPolicyService.h"
#include "dfps/FpsPolicy.h"

#if !defined(__STDC_FORMAT_MACROS)
#define __STDC_FORMAT_MACROS 1
#endif

#define FPS_ATRACE_NAME(name) android::ScopedTrace ___tracer(ATRACE_TAG, name)

#define FPS_ATRACE_BUFFER(x, ...)                                               \
    if (ATRACE_ENABLED()) {                                                     \
        char ___traceBuf[256];                                                  \
        snprintf(___traceBuf, sizeof(___traceBuf), x, ##__VA_ARGS__);           \
        android::ScopedTrace ___bufTracer(ATRACE_TAG, ___traceBuf);             \
    }

#define UNUSED(param) do { (void)(param); } while(0)

namespace android {

#define DFRC_NODE_PATH "/dev/mtk_dfrc"

FpsPolicyService::FpsPolicyService()
    : mStop(false)
    , mCount(0)
    , mProcessedCount(0)
    , mFps(0)
    , mSwMode(0)
    , mForceFps(0)
    , mForceApi(FpsPolicy::API_UNKNOWN)
    , mSingleLayer(0)
    , mNumDisplay(0)
    , mFgPid(0)
    , mWindowFlag(0)
    , mNeedInitTracker(true)
    , mForbidVsync(false)
{
    UNUSED(mStop);
    UNUSED(mCount);
    UNUSED(mProcessedCount);
    UNUSED(mFps);
    UNUSED(mSwMode);
    UNUSED(mForceFps);
    UNUSED(mForceApi);
    UNUSED(mSingleLayer);
    UNUSED(mNumDisplay);
    UNUSED(mFgPid);
    UNUSED(mWindowFlag);
    UNUSED(mNeedInitTracker);
    UNUSED(mForbidVsync);

    mDfrcFd = -1;
    mForceInputWindowInfo.ownerPid = 0;
    mForceInputWindowInfo.hasFocus = false;
    memset(&mRequest, 0, sizeof(mRequest));
    memset(&mPanelInfo, 0, sizeof(mPanelInfo));
}

FpsPolicyService::~FpsPolicyService()
{
    if (mDfrcFd >= 0) {
        close(mDfrcFd);
    }
}

status_t FpsPolicyService::regPolicy(const sp<FpsPolicy>& policy, const String8& name,
                                     const FpsPolicyInfo& info, sp<DupBitTube>& channel)
{
    UNUSED(policy);
    UNUSED(name);
    UNUSED(info);
    UNUSED(channel);
    return NO_ERROR;
}

status_t FpsPolicyService::unregPolicy(uint64_t sequence)
{
    return unregPolicy(sequence, false);
}

status_t FpsPolicyService::unregPolicy(uint64_t sequence, bool binderDied)
{
    UNUSED(sequence);
    UNUSED(binderDied);
    return NO_ERROR;
}

status_t FpsPolicyService::setFps(const FpsPolicyInfo& info)
{
    UNUSED(info);
    return NO_ERROR;
}

status_t FpsPolicyService::setSingleLayer(int32_t single)
{
    UNUSED(single);
    return NO_ERROR;
}

status_t FpsPolicyService::setNumberDisplay(int32_t number)
{
    UNUSED(number);
    return NO_ERROR;
}

status_t FpsPolicyService::setHwcInfo(const HwcInfo& info)
{
    UNUSED(info);
    return NO_ERROR;
}

status_t FpsPolicyService::setInputWindows(const String8& name, const SimpleInputWindowInfo& info)
{
    UNUSED(name);
    UNUSED(info);
    return NO_ERROR;
}

status_t FpsPolicyService::regInfo(const sp<FpsInfo>& info, uint64_t id)
{
    UNUSED(info);
    UNUSED(id);
    return NO_ERROR;
}

status_t FpsPolicyService::unregInfo(uint64_t id)
{
    UNUSED(id);
    return NO_ERROR;
}

void FpsPolicyService::packDfrcPolicy(sp<FpsPolicyInfoPack> policy, DFRC_DRV_POLICY *dfrcPolicy)
{
    UNUSED(policy);
    UNUSED(dfrcPolicy);
}

status_t FpsPolicyService::getPanelInfo(PanelInfo& info)
{
    UNUSED(info);
    return NO_ERROR;
}

status_t FpsPolicyService::setForegroundInfo(int32_t pid, String8& packageName)
{
    UNUSED(pid);
    UNUSED(packageName);
    return NO_ERROR;
}

status_t FpsPolicyService::getFpsRange(FpsRange& range)
{
    UNUSED(range);
    return NO_ERROR;
}

status_t FpsPolicyService::setWindowFlag(int32_t flag, int32_t mask)
{
    UNUSED(flag);
    UNUSED(mask);
    return NO_ERROR;
}

status_t FpsPolicyService::enableTracker(int32_t enable)
{
    UNUSED(enable);
    return NO_ERROR;
}

status_t FpsPolicyService::forbidAdjustingVsync(int32_t forbid)
{
    UNUSED(forbid);
    return NO_ERROR;
}

static const String16 sDump("android.permission.DUMP");
status_t FpsPolicyService::dump(int fd, const Vector<String16>& /*args*/)
{
    String8 result;

    UNUSED(fd);
    return NO_ERROR;
}

void FpsPolicyService::printSystemInfoLocked(String8& result)
{
    result.appendFormat("Request SW VSync Mode: %d  FPS: %d\n", mRequest.mode, mRequest.fps);
    result.appendFormat("Number of display[%d]  Single Layer[%d]\n", mNumDisplay, mSingleLayer);
    result.appendFormat("Forced API[%d]  Forced FPS[%d]\n", mForceApi, mForceFps);
    result.appendFormat("Foreground pid[%d]  Foreground Name[%s]\n", mFgPid, mFgPackageName.string());
    result.appendFormat("Force window pid[%d]  Force window name[%s]\n",
            mForceInputWindowInfo.ownerPid, mForceInputWindowName.string());
    result.appendFormat("Window flags[%08x]\n", mWindowFlag);
    result.appendFormat("Forbid adjusting VSync[%d]\n", mForbidVsync);
    result.appendFormat("\n");
}

void FpsPolicyService::printListFpsRangeLocked(String8& result)
{
    result.appendFormat("Panel info: 90Hz[%d] 120Hz[%d]\n", mPanelInfo.support_90, mPanelInfo.support_120);
    result.appendFormat("Total number of fps index: %d\n", mPanelInfo.num);
    for (int i = 0; i < mPanelInfo.num; i++) {
        result.appendFormat("  + index_%d: %d~%d\n", mRange[i].index, mRange[i].min_fps, mRange[i].max_fps);
    }
    result.appendFormat("\n");
}

void FpsPolicyService::printListPolicyInfoLocked(String8& result)
{
    result.appendFormat("All Fps Policy(count = %zu)\n", mList.size());
    for (size_t i = 0; i < mList.size(); i++) {
        sp<FpsPolicyInfoPack> pack = mList[i];
        result.appendFormat("  + FpsPolicy %" PRIx64 " (%s)\n",
                pack->mInfo.sequence, pack->mName.string());
        result.appendFormat("    API: %s   FPS: %d   MODE: %d\n",
                FpsPolicy::getApiString(pack->mInfo.api), pack->mInfo.fps, pack->mInfo.mode);
        result.appendFormat("    TARGET: %d   GlContextId: %" PRIx64 "\n",
                pack->mInfo.targetPid, pack->mInfo.glContextId);
    }
    result.appendFormat("\n");
}

void FpsPolicyService::printListInfoLocked(String8& result)
{
    result.appendFormat("All Fps Info(count = %zu)\n", mCbList.size());
    for (size_t i = 0; i < mCbList.size(); i++) {
        sp<FpsInfoPack> info = mCbList[i];
        result.appendFormat("  + FpsInfo %" PRIx64 "\n", info->mId);
    }
    result.appendFormat("\n");
}

status_t FpsPolicyService::readyToRun()
{
    mStop = false;

    return 0;
}

void FpsPolicyService::packFpsPolicyInfo(const int num, FpsPolicyInfo *info,
                                         const DFRC_DRV_VSYNC_REQUEST *request,
                                         const DFRC_DRC_REQUEST_SET *requestSet,
                                         const sp<FpsPolicyInfoPack> &pack)
{
    UNUSED(num);
    UNUSED(info);
    UNUSED(request);
    UNUSED(requestSet);
    UNUSED(pack);
}

void FpsPolicyService::packFpsRequest(const int num, FpsRequest *report,
                                      const DFRC_DRV_VSYNC_REQUEST *request,
                                      const DFRC_DRC_REQUEST_SET *requestSet,
                                      const sp<FpsPolicyInfoPack> &pack)
{
    UNUSED(num);
    UNUSED(report);
    UNUSED(request);
    UNUSED(requestSet);
    UNUSED(pack);
}

bool FpsPolicyService::threadLoop()
{
    Mutex::Autolock l(mInfoLock);
    if (mStop) {
        return false;
    }
    Condition cond;
    cond.wait(mInfoLock);
    return false;
}

void FpsPolicyService::tryGetRequestLocked()
{
    mCount++;
    mCondition.signal();
}

void FpsPolicyService::dumpVsyncRequestLocked(const DFRC_DRV_VSYNC_REQUEST& request,
                                              const DFRC_DRC_REQUEST_SET& requestSet)
{
    ALOGI("oldRequest: fps[%d] mode[%d] --> newRequest: fps[%d] mode[%d] num[r:%d|rs:%d]",
          mRequest.fps, mRequest.mode, request.fps, request.mode,
          request.num_policy, requestSet.num);

    FPS_ATRACE_BUFFER("oldRequest: fps[%d] mode[%s] --> newRequest: fps[%d] mode[%s]",
                      mRequest.fps, FpsPolicy::getModeString(mRequest.mode),
                      request.fps, FpsPolicy::getModeString(request.mode));
    FPS_ATRACE_BUFFER("detail: fps[%d] mode[%d] swFps[%d] swMode[%d] validInfo[%d] transState[%d] forbidVsync[%d]",
                      request.fps, request.mode, request.sw_fps, request.sw_mode,
                      request.valid_info, request.transient_state, request.forbid_vsync);
    FPS_ATRACE_BUFFER("numPolicy[%d]  numRequestSet[%d]", request.num_policy, requestSet.num);
    for (int i = 0; i < requestSet.num; i++) {
        DFRC_DRV_POLICY *policy = &requestSet.policy[i];
        FPS_ATRACE_BUFFER("seq[%llu]  api[%s]  pid[%d]  fps[%d]  mode[%s]  tPid[%d]  glId[%llu]",
                          policy->sequence, FpsPolicy::getApiString(policy->api), policy->pid,
                          policy->fps, FpsPolicy::getModeString(policy->mode),
                          policy->target_pid, policy->gl_context_id);
        ALOGI("seq[%llu]  api[%s]  pid[%d]  fps[%d]  mode[%s]  tPid[%d]  glId[%llu]",
                          policy->sequence, FpsPolicy::getApiString(policy->api), policy->pid,
                          policy->fps, FpsPolicy::getModeString(policy->mode),
                          policy->target_pid, policy->gl_context_id);
    }
}

};
