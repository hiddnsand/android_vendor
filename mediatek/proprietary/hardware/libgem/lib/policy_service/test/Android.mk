##########
#  build the test file of api sample
##########
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	main.cpp

LOCAL_C_INCLUDES := \
	$(TOP)/$(MTK_PATH_SOURCE)/hardware/libgem/inc

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
	libui \
	libgui \
	libfpspolicy \
	libbinder \
	liblog

LOCAL_MULTILIB := both
LOCAL_MODULE := test-policy
LOCAL_MODULE_STEM_64:= ${LOCAL_MODULE}_64
LOCAL_MODULE_STEM_32:= ${LOCAL_MODULE}_32

LOCAL_MODULE_TAGS := tests

include $(BUILD_EXECUTABLE)


##########
#  build the test file of swtich arr's fps
##########
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	fps_switch_arr.cpp

LOCAL_C_INCLUDES := \
	$(TOP)/$(MTK_PATH_SOURCE)/hardware/libgem/inc

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
	libui \
	libgui \
	libfpspolicy \
	libbinder \
	liblog

LOCAL_MULTILIB := both
LOCAL_MODULE := test-switchfps-arr
LOCAL_MODULE_STEM_64:= ${LOCAL_MODULE}_64
LOCAL_MODULE_STEM_32:= ${LOCAL_MODULE}_32

LOCAL_MODULE_TAGS := tests

include $(BUILD_EXECUTABLE)


##########
#  build the test file of swtich frr's fps
##########
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	fps_switch_frr.cpp

LOCAL_C_INCLUDES := \
	$(TOP)/$(MTK_PATH_SOURCE)/hardware/libgem/inc

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
	libui \
	libgui \
	libfpspolicy \
	libbinder \
	liblog

LOCAL_MULTILIB := both
LOCAL_MODULE := test-switchfps-frr
LOCAL_MODULE_STEM_64:= ${LOCAL_MODULE}_64
LOCAL_MODULE_STEM_32:= ${LOCAL_MODULE}_32

LOCAL_MODULE_TAGS := tests

include $(BUILD_EXECUTABLE)


##########
#  build the test file of swtich isw's fps
##########
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	fps_switch_isw.cpp

LOCAL_C_INCLUDES := \
	$(TOP)/$(MTK_PATH_SOURCE)/hardware/libgem/inc

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
	libui \
	libgui \
	libfpspolicy \
	libbinder \
	liblog

LOCAL_MULTILIB := both
LOCAL_MODULE := test-switchfps-isw
LOCAL_MODULE_STEM_64:= ${LOCAL_MODULE}_64
LOCAL_MODULE_STEM_32:= ${LOCAL_MODULE}_32

LOCAL_MODULE_TAGS := tests

include $(BUILD_EXECUTABLE)
