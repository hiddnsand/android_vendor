/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.wfo.impl;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.SystemProperties;
import android.os.Message;

import android.telephony.Rlog;
import android.telephony.TelephonyManager;

import com.mediatek.wfo.IMwiService;
import com.mediatek.wfo.IWifiOffloadService;

/**
 * Mobile Wi-Fi Interaction Service.
 */
public class MwiService extends IMwiService.Stub {
    static final String MWIS_LOG_TAG = "MWIS";
    private static final boolean VDBG = true;  // STOPSHIP if true

    private Context mContext;
    private TelephonyManager mTelephonyManager;

    private int mSimCount;

    WifiPdnHandler mWifiPdnHandler;
    WfcHandler mWfcHandler;
    WfcLocationHandler mWfcLocationHandler;
    MwisIndicationReceiver mIndicationReceiver;

    public MwiService(Context context) {
        logd("Construct MwiService");
        mContext = context;

        mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephonyManager != null) {
            mSimCount = mTelephonyManager.getSimCount();
        } else {
            logd("mTelephonyManager = null");
        }
        logd("mSimCount: " + mSimCount);

        mWifiPdnHandler = new WifiPdnHandler(mContext, mSimCount);
        mWfcHandler = new WfcHandler(mContext, mWifiPdnHandler, mSimCount);
        mWfcLocationHandler = new WfcLocationHandler(mContext);
        mIndicationReceiver = new MwisIndicationReceiver(mContext);

        mIndicationReceiver.setWifiPdnHandler(mWifiPdnHandler);
        mIndicationReceiver.setWfcHandler(mWfcHandler);
        mIndicationReceiver.setWfcLocationHandler(mWfcLocationHandler);
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            AsyncResult ar;
            logd("handleMessage: " + messageToString(msg));
            switch (msg.what) {
                default:
                    break;
            }
        }

        private String messageToString(Message msg) {
            switch (msg.what) {
                default:
                    return "UNKNOWN";
            }
        }
    };

    private boolean is93RilMode() {
        if (SystemProperties.get("ro.mtk_ril_mode").equals("c6m_1rild")) {
            return true;
        }
        return false;
    }

    @Override
    public IWifiOffloadService getWfcHandlerInterface() {
        logd("getWfcHandlerInterface");
        return mWfcHandler.getWfoInterface();
    }

    public void dispose() {
        logd("dispose()");
    }

    private static void logd(String l) {
        Rlog.d(MWIS_LOG_TAG, "[MwiService] " + l);
    }

    private static void loge(String l) {
        Rlog.e(MWIS_LOG_TAG, "[MwiService] " + l);
    }

    private static void logi(String l) {
        Rlog.i(MWIS_LOG_TAG, "[MwiService] " + l);
    }

    private static void logw(String l) {
        Rlog.w(MWIS_LOG_TAG, "[MwiService] " + l);
    }
}
