package com.mediatek.ims.ril;


/**
 * IMS RIL call information.
 */
public class ImsCallInfo {
    /* State is indicated for call status */
    public enum State {
        ACTIVE,
        HOLDING,
        ALERTING,   // MO call only
        INCOMING,   // MT call only
        INVALID;
    }

    public String mCallNum;
    public String mCallId;
    public boolean mIsConference;
    public State mState;

    //***** Constructors
    public ImsCallInfo(String callId, String callNum,
                       boolean isConference, State state) {

        mCallId = callId;
        mCallNum = callNum;
        mIsConference = isConference;
        mState = state;
    }
}