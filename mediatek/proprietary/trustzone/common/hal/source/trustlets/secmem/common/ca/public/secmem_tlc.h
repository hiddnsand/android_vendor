#ifndef SECMEM_TLC_H
#define SECMEM_TLC_H

#ifdef __cplusplus
extern "C" {
#endif

//#define SECMEM_TLC_DEBUG

/*
 * Memory handle define
 *
 * Handle is used to communicate with normal world:
 * 1. Memory information can not expose to normal world. (Major, important!)
 * 2. Too many informations, and thet can be grouped by handle.
 *
 * All kinds of memory use the same handle define.
 * According to their different purpose, they are redefined to specific name.
 * Just for easy programming.
 */

// Shared memory handle define
typedef uint32_t UREE_SHAREDMEM_HANDLE;

// Secure memory handle define
typedef uint32_t UREE_SECUREMEM_HANDLE;

// Secure chunk memory handle define
typedef uint32_t UREE_SECURECM_HANDLE;

typedef uint32_t UREE_RELEASECM_HANDLE;

/* Error code used in secmem_tlc internally */
/* Error code used in SWd secmem_drv need to check secmem_core.h */
#define UREE_SUCCESS				(0U)
#define UREE_ERR_INVALID_PARA		(0xFFFFFFFFU) /* -1 */
#define UREE_ERR_INVALID_OPERATION	(0xFFFFFFFEU) /* -2 */
#define UREE_ERR_OPEN_DEV_FAIL		(0xFFFFFFFDU) /* -3 */


/*
 * Shared memory
 *
 * A shared memory is normal memory, which can be seen by Normal world and Secure world.
 * It is used to create the comminicattion between two worlds.
 * Currently, zero-copy data transfer is supportted, for simple and efficient design.
 *
 * The shared memory lifetime:
 * 1. CA (Client Application at REE) prepares memory
 * 2. CA registers it to TEE scope.
 * 3. A handle is returned. CA can use it to communicate with TEE.
 * 4. If shared memory is not used, CA unregisters it.
 * 5. CA frees memory.
 *
 * Because it is zero-copy shared memory, the memory characteritics is inherited.
 * If the shared memory will be used for HW, CA must allocate physical continuous memory.
 *
 * Note: Because shared memory can be seen by both Normal and Secure world.
 * It is a possible weakpoint to bed attacked or leak secure data.
 */
/**
 * Secure memory
 *
 * A secure memory can be seen only in Secure world.
 * Secure memory, here, is defined as external memory (ex: DRAM) protected by trustzone.
 * It can protect from software attack very well, but can not protect from physical attack, like memory probe.
 * CA (Client Application at REE) can ask TEE for a secure buffer, then control it:
 * to reference, or to free...etc.
 *
 * Secure memory spec.:
 * 1. Protected by trustzone (NS = 0).
 * 2. External memory (ex: external DRAM).
 * 3. With cache.
 */

#if 0
uint32_t UREE_AllocSecuremem_Debug(uint32_t *mem_handle, uint32_t alignment, uint32_t size, uint8_t *owner, uint32_t id);
uint32_t UREE_AllocSecurememZero_Debug(uint32_t *mem_handle, uint32_t alignment, uint32_t size, uint8_t *owner, uint32_t id);
uint32_t UREE_ReferenceSecuremem_Debug(uint32_t mem_handle, uint32_t *refcount, uint8_t *owner, uint32_t id);
uint32_t UREE_UnreferenceSecuremem_Debug(uint32_t mem_handle, uint32_t *refcount, uint8_t *owner, uint32_t id);
uint32_t UREE_AllocSecurememTBL_Debug(uint32_t *mem_handle, uint32_t alignment, uint32_t size, uint8_t *owner, uint32_t id);
uint32_t UREE_AllocSecurememTBLZero_Debug(uint32_t *mem_handle, uint32_t alignment, uint32_t size, uint8_t *owner, uint32_t id);
uint32_t UREE_UnreferenceSecurememTBL_Debug(uint32_t mem_handle, uint32_t *refcount, uint8_t *owner, uint32_t id);

#define UREE_AllocSecuremem(mhdl, align, size)      UREE_AllocSecuremem_Debug(mhdl, align, size, (uint8_t*)__FILE__, __LINE__)
#define UREE_AllocSecurememZero(mhdl, align, size)  UREE_AllocSecurememZero_Debug(mhdl, align, size, (uint8_t*)__FILE__, __LINE__)
#define UREE_ReferenceSecuremem(mhdl, refcnt)       UREE_ReferenceSecuremem_Debug(mhdl, refcnt, (uint8_t*)__FILE__, __LINE__)
#define UREE_UnreferenceSecuremem(mhdl, refcnt)     UREE_UnreferenceSecuremem_Debug(mhdl, refcnt, (uint8_t*)__FILE__, __LINE__)
#define UREE_AllocSecurememTBL(mhdl, align, size)   UREE_AllocSecurememTBL_Debug(mhdl, align, size, (uint8_t*)__FILE__, __LINE__)
#define UREE_AllocSecurememTBLZero(mhdl, align, size)   UREE_AllocSecurememTBLZero_Debug(mhdl, align, size, (uint8_t*)__FILE__, __LINE__)
#define UREE_UnreferenceSecurememTBL(mhdl, refcnt)  UREE_UnreferenceSecurememTBL_Debug(mhdl, refcnt, (uint8_t*)__FILE__, __LINE__)

#else
/**
 * Secure memory allocation
 *
 * Allocate one memory.
 * If memory is allocated successfully, a handle will be provided.
 *
 * Memory lifetime:
 * 1. Allocate memory, and get the handle.
 * 2. If other process wants to use the same memory, reference it.
 * 3. If they stop to use it, unreference it.
 * 4. Free it (by unreference), if it is not used.
 *
 * Simple rules:
 * 1. start by allocate, end by unreference (for free).
 * 2. start by reference, end by unreference.
 *
 * @param session    The session handle.
 * @param mem_handle    [out] A pointer to secure memory handle.
 * @param alignment    Memory alignment in bytes.
 * @param size    The size of the buffer to be allocated in bytes.
 * @return    return code.
 */
uint32_t UREE_AllocSecuremem(uint32_t *mem_handle, uint32_t alignment, uint32_t size, uint8_t *owner, uint32_t id);
uint32_t UREE_AllocSecurememZero(uint32_t *mem_handle, uint32_t alignment, uint32_t size, uint8_t *owner, uint32_t id);


/**
 * Secure memory reference
 *
 * Reference memory.
 * Referen count will be increased by 1 after reference.
 *
 * Reference lifetime:
 * 1. Reference the memory before using it, if the memory is allocated by other process.
 * 2. Unreference it if it is not used.
 *
 * @param session    The session handle.
 * @param mem_handle    The secure memory handle.
 * @param return    return code.
 */
uint32_t UREE_ReferenceSecuremem(uint32_t mem_handle, uint32_t *refcount, uint8_t *owner, uint32_t id);

/**
 * Secure memory unreference
 *
 * Unreference memory.
 * Reference count will be decreased by 1 after unreference.
 * Once reference count is zero, memory will be freed.
 *
 * @param session    The session handle.
 * @param mem_handle    The secure memory handle.
 * @param return    return code.
 */
uint32_t UREE_UnreferenceSecuremem(uint32_t mem_handle, uint32_t *refcount, uint8_t *owner, uint32_t id);

//APIs for t-Play
uint32_t UREE_AllocSecurememTBL(uint32_t *mem_handle, uint32_t alignment, uint32_t size, uint8_t *owner, uint32_t id);
uint32_t UREE_AllocSecurememTBLZero(uint32_t *mem_handle, uint32_t alignment, uint32_t size, uint8_t *owner, uint32_t id);


uint32_t UREE_UnreferenceSecurememTBL(uint32_t mem_handle, uint32_t *refcount, uint8_t *owner, uint32_t id);
#endif

#define UREE_FreeSecuremem(fmt, args...)    UREE_UnreferenceSecuremem(fmt, ##args)
#define UREE_FreeSecurememTBL(fmt, args...) UREE_UnreferenceSecurememTBL(fmt, ##args)

uint32_t UREE_DumpSecurememInfo(void);

uint32_t tlcMemOpen(void);
uint32_t tlcMemClose(void);

#ifndef SKIP_DEBUG_ALIAS_MACRO
#define UREE_AllocSecuremem(mhdl, align, size)      UREE_AllocSecuremem(mhdl, align, size, (uint8_t*)__FILE__, __LINE__)
#define UREE_AllocSecurememZero(mhdl, align, size)  UREE_AllocSecurememZero(mhdl, align, size, (uint8_t*)__FILE__, __LINE__)
#define UREE_ReferenceSecuremem(mhdl, refcnt)       UREE_ReferenceSecuremem(mhdl, refcnt, (uint8_t*)__FILE__, __LINE__)
#define UREE_UnreferenceSecuremem(mhdl, refcnt)     UREE_UnreferenceSecuremem(mhdl, refcnt, (uint8_t*)__FILE__, __LINE__)
#define UREE_AllocSecurememTBL(mhdl, align, size)   UREE_AllocSecurememTBL(mhdl, align, size, (uint8_t*)__FILE__, __LINE__)
#define UREE_AllocSecurememTBLZero(mhdl, align, size)   UREE_AllocSecurememTBLZero(mhdl, align, size, (uint8_t*)__FILE__, __LINE__)
#define UREE_UnreferenceSecurememTBL(mhdl, refcnt)  UREE_UnreferenceSecurememTBL(mhdl, refcnt, (uint8_t*)__FILE__, __LINE__)
#endif

#ifdef __cplusplus
}
#endif

#endif /* SECMEM_TLC_H */

