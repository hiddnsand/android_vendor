/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 */

#include <utils/Log.h>
#include <utils/RefBase.h>
#include <utils/String8.h>
#include <utils/Trace.h>
#include <utils/Thread.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include "MTKProgram.h"
#include "MTKProgramCache.h"
#include "Properties.h"

/// M: [ProgramBinaryAtlas] For using program binaries
#include "mediatek/ExtensionUtils.h"

#include <stdio.h>

#define ATRACE_TAG ATRACE_TAG_VIEW

namespace android {
namespace uirenderer {

///////////////////////////////////////////////////////////////////////////////
// Defines
///////////////////////////////////////////////////////////////////////////////
#define CACHE_PATH "/data/data/%s/program_cache"
static programid gKeys[] = {
0x0000000000000000, 0x0000001000500040, 0x0000003800000000, 0x0002000000000001,
0x0002000000000003, 0x0002000000500041, 0x0000001000000000, 0x0002000800000003,
0x0000010000000008, 0x0002000800000001, 0x0000001800000000, 0x0002001800500044,
0x0000001000000008, 0x0002000000900041, 0x0000081000000000, 0x0002080800000001,
0x0002080000d00041, 0x0002080000900041, 0x0002080800000003, 0x0000000800500040,
0x0000000000000001, 0x0000000800000000, 0x0000000000500040, 0x0002000000d00041,
0x000201080000000b, 0x0000001800500040
};

///////////////////////////////////////////////////////////////////////////////
// Constructors/destructors
///////////////////////////////////////////////////////////////////////////////

MTKProgramCache::MTKProgramCache(const Extensions& extensions, bool autoDeleteAtlas)
        : ProgramCache(extensions), mAutoDeleteAtlas(autoDeleteAtlas) {
        programAtlas = ExtensionUtils::getProgramAtlas();
}

MTKProgramCache::~MTKProgramCache() {
    if (mAutoDeleteAtlas) {
        ExtensionUtils::deleteProgramAtlas();
    }
}

/**
 * M: Task to save program keys to disk asynchronously
 */
class SaveToDiskTask : public Thread {
public:
    SaveToDiskTask() {};
    virtual ~SaveToDiskTask() {};
    Vector<programid> keys;
private:
     virtual bool threadLoop() {
         ATRACE_NAME("SaveToDisk");
         char file[512];
         snprintf(file, sizeof(file), CACHE_PATH, ExtensionUtils::getBasicInfo()->getProcessName());
         FILE* fPtr = fopen(file, "w");
         if (fPtr != NULL) {
             size_t count = keys.size();
             for (size_t i = 0; i < count; i++) {
                 programid key = keys.itemAt(i);
                 fwrite(&key, sizeof(programid), 1, fPtr);
             }
             fclose(fPtr);
         }
         return false;
     }
};


///////////////////////////////////////////////////////////////////////////////
// Cache management
///////////////////////////////////////////////////////////////////////////////

void MTKProgramCache::init() {
    ALOGD("MTKProgramCache.init: enable enhancement %d", (programAtlas != NULL));
    if (programAtlas != NULL) {
        /// M: [ProgramBinaryAtlas] Initialize program atlas.
        programAtlas->init();

        /// M: Preload programs into cache.
        if (programAtlas->getServiceEnabled()) {
            int size = int(sizeof(gKeys) / sizeof(gKeys[0]));
            int max = size;
            int index = -1;
            programid buffer[max];
            MTKProgramDescription description;
            char file[512];
            snprintf(file, sizeof(file), CACHE_PATH, ExtensionUtils::getBasicInfo()->getProcessName());
            FILE* fPtr = fopen(file, "r");
            if (fPtr != NULL) {
                while (index < max && !feof(fPtr)) {
                    fread(&buffer[++index], sizeof(programid), 1, fPtr);
                }
                fclose(fPtr);
            }
            ALOGD("Initializing program cache from %p, size = %d", fPtr, index);

            for (int i = 0; i < index; i ++) {
                int j = 0;
                // only preload programs that there are binaries prepared
                for (j = 0; j < size; ++j) {
                    if (buffer[i] == gKeys[j]) {
                        break;
                    }
                }
                if (j < size) {
                    description.set(buffer[i]);
                    get(description);
                }

                ALOGD("-- %sinit (key = 0x%.8x%.8x)", j < size ? "" : "not ", uint32_t(buffer[i] >> 32),
                        uint32_t(buffer[i] & 0xffffffff));
            }
        }
    }
}

void MTKProgramCache::saveToDisk() {
    if (programAtlas != NULL && programAtlas->getServiceEnabled()) {
        sp<SaveToDiskTask> task = new SaveToDiskTask();
        size_t count = mCache.size();

        ALOGD("MTKProgramCache save to disk, size = %d", (int)count);
        for (std::map<programid, std::unique_ptr<Program>>::iterator it = mCache.begin(); it != mCache.end(); ++it) {
            task->keys.add(it->first);
        }
        task->run("SaveToDisk");
    }
}

int MTKProgramCache::createPrograms(int64_t* map, int* mapLength) {
    // must make sure that egl env is constructed before calling this function
    MTKProgramDescription description;
    int64_t offset = 0;
    int count = 0;
    int size = int(sizeof(gKeys) / sizeof(gKeys[0]));
    if (size > *mapLength / 4) size = *mapLength / 4;

    for (int i = 0; i < size; ++i) {
        uint64_t startInner = 0;
        startInner = systemTime(SYSTEM_TIME_MONOTONIC);

        description.set(gKeys[i]);
        String8 vertexShader = generateVertexShader(description);
        String8 fragmentShader = generateFragmentShader(description);

        GLuint newFS, newVS;
        GLuint newProgram;
        GLint success;
        const GLchar* source[1];

        //
        //  Create new shader/program objects and attach them together.
        //
        newVS = glCreateShader(GL_VERTEX_SHADER);
        newFS = glCreateShader(GL_FRAGMENT_SHADER);
        newProgram = glCreateProgram();
        glAttachShader(newProgram, newVS);
        glAttachShader(newProgram, newFS);

        //
        //  Supply GLSL source shaders, compile, and link them
        //
        source[0] = vertexShader.string();
        glShaderSource(newVS, 1, source, NULL);
        glCompileShader(newVS);
        source[0] = fragmentShader.string();
        glShaderSource(newFS, 1, source, NULL);
        glCompileShader(newFS);

        glBindAttribLocation(newProgram, Program::kBindingPosition, "position");
        if (description.hasTexture || description.hasExternalTexture) {
            glBindAttribLocation(newProgram, Program::kBindingTexCoords, "texCoords");
        }

        glLinkProgram(newProgram);
        glGetProgramiv(newProgram, GL_LINK_STATUS, &success);

        if (success) {
            GLint binaryLength;

            //
            //  Retrieve the binary from the program object
            //
            glGetProgramiv(newProgram, GL_PROGRAM_BINARY_LENGTH_OES, &binaryLength);

            map[count++] = static_cast<int64_t>(description.key());
            map[count++] = static_cast<int64_t>(offset);
            map[count++] = static_cast<int64_t>(binaryLength);
            map[count++] = static_cast<int64_t>(newProgram); // temp save program id here

            uint64_t endInner = 0;
            endInner = systemTime(SYSTEM_TIME_MONOTONIC);
            ALOGD("%d Program id %d, key 0x%.8x%.8x, offset %" PRId64 ", binaryLength %d within %dns",
                i, newProgram, uint32_t(description.key() >> 32), uint32_t(description.key() & 0xffffffff),
                offset, binaryLength, (int) ((endInner - startInner) / 1000));

            offset += binaryLength;

            //
            // Clean up
            //
            glDetachShader(newProgram, newVS);
            glDetachShader(newProgram, newFS);
            glDeleteShader(newVS);
            glDeleteShader(newFS);

        } else {
            ALOGE("Error while linking shaders:");
            GLint infoLen = 0;
            glGetProgramiv(newProgram, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen > 1) {
                GLchar log[infoLen];
                glGetProgramInfoLog(newProgram, infoLen, 0, &log[0]);
                ALOGE("%s", log);
            }

            //
            // Clean up
            //
            glDetachShader(newProgram, newVS);
            glDetachShader(newProgram, newFS);
            glDeleteShader(newVS);
            glDeleteShader(newFS);
            glDeleteProgram(newProgram);
        }
    }

    *mapLength = count;

    return offset;
}

void MTKProgramCache::loadProgramBinariesAndDelete(int64_t* map, int mapLength, void* buffer, int length) {
    for (int i = 0; i < mapLength;) {
        i++;  // for key
        int64_t offset = map[i++];
        GLint binaryLength = static_cast<GLint>(map[i++]);
        GLuint newProgram = static_cast<GLuint>(map[i]);
        GLenum binaryFormat = 0;

        if (offset + binaryLength > length || buffer == NULL) {
            // this should not happen
            map[i++] = 0;
            glDeleteProgram(newProgram);
            break;
        }

        void* binary = reinterpret_cast<void*>(reinterpret_cast<int64_t>(buffer) + offset);
        glGetProgramBinaryOES(newProgram, binaryLength, NULL, &binaryFormat, binary);
        map[i++] = binaryFormat;
        glDeleteProgram(newProgram);
    }
}

///////////////////////////////////////////////////////////////////////////////
// Program generation
///////////////////////////////////////////////////////////////////////////////

Program* MTKProgramCache::generateProgram(const ProgramDescription& description, programid key) {
    if (programAtlas != NULL) {
        /// M: [ProgramBinaryAtlas] enhancement which creating program by binary
        ALOGD("MTKProgramCache.generateProgram: %" PRId64, (int64_t) key);
        IProgramAtlas::ProgramEntry* entry = programAtlas->getProgramEntry(key);
        if (entry) {
            Program* program = new MTKProgram(description, entry->binary, entry->binaryLength, entry->binaryFormat);
            if (program->isInitialized()) return program;
            else delete program;
        }
    }

    return ProgramCache::generateProgram(description, key);
}

}; // namespace uirenderer
}; // namespace android
