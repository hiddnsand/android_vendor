#
# TSF_Test
#
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

#
LOCAL_SRC_FILES := \
    main.cpp \
    main_dpestream.cpp \

-include $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk
-include $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/driver.mk

ifeq ($(MTKDPE_SUPPORT), 1)
ifneq (,$(filter $(strip $(TARGET_BOARD_PLATFORM)), mt6797))
 		TEST_SRC_FOLDER := $(TARGET_BOARD_PLATFORM)
else
 		TEST_SRC_FOLDER := v1.1
endif
 		LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/src/dpe/1.0/hw/$(MTKCAM_DRV_PLATFORM)
else
 		TEST_SRC_FOLDER := v1.1
 		LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/src/dpe/1.0/hw/dummy
endif
ifeq ($(TEST_SRC_FOLDER), v1.1)

LOCAL_SRC_FILES += hw/$(TEST_SRC_FOLDER)/dpe/dpe_testcommon.cpp
LOCAL_SRC_FILES += hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/MultiEnque_dpe_test_case_00_frame_0_dpe_setting_00.cpp
LOCAL_SRC_FILES += hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/MultiEnque_dpe_test_case_00_frame_2_dpe_setting_00.cpp

LOCAL_SRC_FILES+= \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/dpe_default_frame_0.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_confo_l_frame_00_01verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_confo_r_frame_00_01verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_dvi_l_frame_00verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_dvi_r_frame_00verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_dvo_l_frame_00_01verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_dvo_r_frame_00_01verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_imgi_l_frame_00verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_imgi_r_frame_00verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_maski_l_frame_00verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_maski_r_frame_00verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_respo_l_frame_00_01verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_respo_r_frame_00_01verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_0verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_1verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_2verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_3verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_4verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_0verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_1verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_2verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_3verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_4verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_0verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_1verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_2verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_3verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_4verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_maski_frame_00_00_0verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_maski_frame_00_00_1verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_0verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_1verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_2verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_3verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_4verif.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/dpe_default_frame_0_dpe_setting_00.cpp

LOCAL_SRC_FILES+= \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_confo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_confo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_dvi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_dvi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_dvo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_dvo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_imgi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_imgi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_maski_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_maski_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_respo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_respo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_maski_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_maski_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_setting_00.cpp

LOCAL_SRC_FILES+= \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_confo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_confo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_dvi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_dvi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_dvo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_dvo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_imgi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_imgi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_maski_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_maski_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_respo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_respo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_dpi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_dpi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_dpi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_dpi_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_dpi_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_dpo_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_dpo_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_dpo_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_dpo_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_dpo_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_imgi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_imgi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_imgi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_imgi_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_imgi_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_maski_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_maski_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_tbli_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_tbli_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_tbli_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_tbli_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_wmf_tbli_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_setting_00.cpp

LOCAL_SRC_FILES+= \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_confo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_confo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_dvi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_dvi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_dvo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_dvo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_imgi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_imgi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_maski_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_maski_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_respo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_respo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_dpi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_dpi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_dpi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_dpi_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_dpi_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_dpo_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_dpo_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_dpo_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_dpo_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_dpo_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_imgi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_imgi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_imgi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_imgi_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_imgi_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_maski_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_maski_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_tbli_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_tbli_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_tbli_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_tbli_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_wmf_tbli_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_setting_00.cpp

LOCAL_SRC_FILES+= \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_confo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_confo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_dvi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_dvi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_dvo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_dvo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_imgi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_imgi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_maski_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_maski_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_respo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_respo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_dpi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_dpi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_dpi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_dpi_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_dpi_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_dpo_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_dpo_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_dpo_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_dpo_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_dpo_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_imgi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_imgi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_imgi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_imgi_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_imgi_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_maski_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_maski_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_tbli_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_tbli_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_tbli_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_tbli_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_wmf_tbli_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_setting_00.cpp

LOCAL_SRC_FILES+= \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_confo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_confo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_dvi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_dvi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_dvo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_dvo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_imgi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_imgi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_maski_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_maski_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_respo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_respo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_dpi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_dpi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_dpi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_dpi_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_dpi_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_dpo_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_dpo_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_dpo_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_dpo_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_dpo_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_imgi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_imgi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_imgi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_imgi_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_imgi_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_maski_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_maski_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_tbli_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_tbli_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_tbli_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_tbli_frame_00_00_3verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_wmf_tbli_frame_00_00_4verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_setting_00.cpp

else

LOCAL_SRC_FILES += hw/$(TEST_SRC_FOLDER)/dpe/dpe_testcommon.cpp
LOCAL_SRC_FILES += hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/MultiEnque_dpe_test_case_00_frame_0_dpe_setting_00.cpp
LOCAL_SRC_FILES += hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/MultiEnque_dpe_test_case_00_frame_2_dpe_setting_00.cpp

LOCAL_SRC_FILES +=  \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/dpe_default_frame_0.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_confo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_confo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_dvi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_dvi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_dvo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_dvo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_imgi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_imgi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_maski_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_maski_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_respo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_respo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_default/frame_0/dpe_default_frame_0_dpe_setting_00.cpp

LOCAL_SRC_FILES +=  \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_confo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_confo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_dvi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_dvi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_dvo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_dvo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_imgi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_imgi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_maski_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_maski_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_respo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_respo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_dpi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_dpi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_dpi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_dpo_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_dpo_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_dpo_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_imgi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_imgi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_imgi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_tbli_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_tbli_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_tbli_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_0/frame_0_dpe_setting_00.cpp \

LOCAL_SRC_FILES +=  \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_confo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_confo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_dvi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_dvi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_dvo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_dvo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_imgi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_imgi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_maski_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_maski_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_respo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_respo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_dpi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_dpi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_dpi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_dpo_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_dpo_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_dpo_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_imgi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_imgi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_imgi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_tbli_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_tbli_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_tbli_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_1/frame_1_dpe_setting_00.cpp \

LOCAL_SRC_FILES +=  \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_confo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_confo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_dvi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_dvi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_dvo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_dvo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_imgi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_imgi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_maski_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_maski_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_respo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_respo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_dpi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_dpi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_dpi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_dpo_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_dpo_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_dpo_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_imgi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_imgi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_imgi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_tbli_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_tbli_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_tbli_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_2/frame_2_dpe_setting_00.cpp \

LOCAL_SRC_FILES +=  \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_confo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_confo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_dvi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_dvi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_dvo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_dvo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_imgi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_imgi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_maski_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_maski_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_respo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_respo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_dpi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_dpi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_dpi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_dpo_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_dpo_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_dpo_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_imgi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_imgi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_imgi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_tbli_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_tbli_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_tbli_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_3/frame_3_dpe_setting_00.cpp \

LOCAL_SRC_FILES +=  \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4.cpp \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_confo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_confo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_dvi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_dvi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_dvo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_dvo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_imgi_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_imgi_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_maski_l_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_maski_r_frame_00verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_respo_l_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_respo_r_frame_00_01verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_dpi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_dpi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_dpi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_dpo_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_dpo_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_dpo_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_imgi_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_imgi_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_imgi_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_tbli_frame_00_00_0verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_tbli_frame_00_00_1verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_tbli_frame_00_00_2verif.c \
  hw/$(TEST_SRC_FOLDER)/dpe/dpe_test_case_00/frame_4/frame_4_dpe_setting_00.cpp \


endif

# vector
LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libutils

LOCAL_SHARED_LIBRARIES += libstdc++

LOCAL_SHARED_LIBRARIES +=  libmtkcam_dpe
LOCAL_SHARED_LIBRARIES += libcamdrv_imem

LOCAL_STATIC_LIBRARIES := \

LOCAL_WHOLE_STATIC_LIBRARIES := \

LOCAL_MODULE := Dpe_Test
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_TAGS := eng

LOCAL_PRELINK_MODULE := false

#-----------------------------------------------------------
LOCAL_C_INCLUDES += $(LOCAL_PATH)/hw/$(TEST_SRC_FOLDER)/
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/include
LOCAL_C_INCLUDES += $(MTKCAM_DRV_INCLUDE)
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_PLATFORM)/drv
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_PLATFORM)/imageio

#-----------------------------------------------------------
ifeq ($(TARGET_BUILD_VARIANT), user)
MTKCAM_LOGENABLE_DEFAULT   := 0
else
MTKCAM_LOGENABLE_DEFAULT   := 1
endif

#-----------------------------------------------------------
LOCAL_CFLAGS += -DMTKCAM_LOGENABLE_DEFAULT=$(MTKCAM_LOGENABLE_DEFAULT)
# End of common part ---------------------------------------
#
include $(BUILD_EXECUTABLE)


#
#include $(call all-makefiles-under,$(LOCAL_PATH))
