package com.mediatek.mms.plugin;

import android.content.Context;
import android.util.Log;

import com.mediatek.mms.ext.IOpMessagePluginExt;
import com.mediatek.mms.ext.OpMmsCustomizationFactoryBase;

public class Op03MmsCustomizationFactory extends OpMmsCustomizationFactoryBase {

    private static String TAG = "Op03MmsCustomizationFactory";

    public Op03MmsCustomizationFactory(Context context) {
        super(context);
        Log.d(TAG, "[Op03MmsCustomizationFactory]context=" + context.getApplicationInfo());
    }

    @Override
    public IOpMessagePluginExt makeOpMessagePluginExt() {
        return new Op03MessagePluginExt(mContext);
    }
}
