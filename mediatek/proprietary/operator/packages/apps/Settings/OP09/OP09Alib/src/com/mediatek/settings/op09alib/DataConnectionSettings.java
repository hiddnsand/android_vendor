/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.app.ActionBar;
import android.app.ActivityManager;
import android.app.StatusBarManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.Switch;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.internal.telephony.MtkSubscriptionManager;

import java.util.List;

/**
 * For CT IRAT feature: User can enable data and choose the sim.
 */
public class DataConnectionSettings extends PreferenceActivity {

    private static final String TAG = "DataConnectionSettings";

    private static final String SIM_SLOT_1_KEY = "data_connection_sim_slot_1";
    private static final String SIM_SLOT_2_KEY = "data_connection_sim_slot_2";

    private IntentFilter mIntentFilter;
    private RadioPreference mSlot1Preference;
    private RadioPreference mSlot2Preference;
    private DataConnectionEnabler mDataConnectionEnabler;
    private TelephonyManager mTelephonyManager;
    private PhoneStateListener mPhoneStateListener;
    private ITelephony mTelephony;
    private StatusBarManager mStatusBarManager;
    private int mResetSubId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
    private boolean mIsSlot1Switching = false;
    private boolean mIsSlot2Switching = false;
    private boolean mIsRegObserver = false;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "onReceive action=" + action);
            if (action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
                updateScreen();
            } else if (action.equals(TelephonyIntents.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED)) {
                updateScreen();
            } else if (isSimSwitchAction(action)) {
                handleSimSwitchBroadcast(intent);
            }
        }
    };

    private ContentObserver mDataConnectionSlotObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            Log.d(TAG, "mDataConnectionSlotObserver onChange selfChange=" + selfChange);
            if (!selfChange) {
                updateScreen();
            }
        }
    };

    private void setSlotPreferenceEnable(boolean flag) {
        Log.d(TAG, "setSlotPreferenceEnable: " + flag);
        mSlot1Preference.setEnabled(flag);
        mSlot2Preference.setEnabled(flag);
        if (!flag) {
            mIsSlot1Switching = true;
            mIsSlot2Switching = true;
        }
    }

    private ContentObserver mDataConnectionObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            Log.d(TAG, "mDataConnectionObserver onChange selfChange=" + selfChange);
            if (!selfChange) {
                updateScreen();
            }
        }
    };

    private ContentObserver mDataSlot1Observer = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            Log.d(TAG, "mDataSlot1Observer onChange selfChange=" + selfChange
                + " " + mIsSlot1Switching + " " + mIsSlot2Switching);
            if (!selfChange) {
                mIsSlot1Switching = false;
                updateScreen();
            }
        }
    };
    private ContentObserver mDataSlot2Observer = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            Log.d(TAG, "mDataSlot2Observer onChange selfChange=" + selfChange
                + " " + mIsSlot1Switching + " " + mIsSlot2Switching);
            if (!selfChange) {
                mIsSlot2Switching = false;
                updateScreen();
            }
        }
    };

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.data_connection_settings);
        this.getListView().setBackgroundColor(
                getResources().getColor(R.color.dashboard_background_color));
        mSlot1Preference = (RadioPreference) findPreference(SIM_SLOT_1_KEY);
        mSlot2Preference = (RadioPreference) findPreference(SIM_SLOT_2_KEY);
        if (TelephonyManager.from(this).getSimCount() == 1) {
            Log.d(TAG, "Single SIM project only show one item");
            getPreferenceScreen().removePreference(mSlot2Preference);
        }
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        mIntentFilter.addAction(TelephonyIntents.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED);
        //mIntentFilter.addAction(TelephonyIntents.ACTION_SET_PHONE_RAT_FAMILY_DONE);
        //mIntentFilter.addAction(TelephonyIntents.ACTION_SET_PHONE_RAT_FAMILY_FAILED);
        mTelephonyManager = TelephonyManager.from(this);
        mTelephony = ITelephony.Stub.asInterface(ServiceManager
                .getService(Context.TELEPHONY_SERVICE));

        LayoutInflater inflater =
            (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Switch actionBarSwitch = new Switch(inflater.getContext());
        final int padding = getResources().getDimensionPixelSize(R.dimen.action_bar_switch_padding);
        actionBarSwitch.setPadding(0, 0, padding, 0);
        getActionBar().setDisplayOptions(
                ActionBar.DISPLAY_SHOW_CUSTOM, ActionBar.DISPLAY_SHOW_CUSTOM);
        getActionBar().setCustomView(
                actionBarSwitch,
                new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER_VERTICAL | Gravity.END));
        mDataConnectionEnabler = new DataConnectionEnabler(this, actionBarSwitch);
        setTaskDescription(new ActivityManager.TaskDescription(
                getResources().getString(R.string.data_connection_summary_title),
                BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_data_connection_set)
                ));
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mReceiver, mIntentFilter);
        registerPhoneStateListener();
        getContentResolver().registerContentObserver(
                Settings.Global.getUriFor(Settings.Global.MOBILE_DATA),
                true, mDataConnectionObserver);
        getContentResolver().registerContentObserver(
                Settings.System.getUriFor(Settings.System.GPRS_CONNECTION_SETTING),
                true, mDataConnectionSlotObserver);
        registerObserver();
        updateScreen();
        if (mDataConnectionEnabler != null) {
            mDataConnectionEnabler.resume();
        }
    }

    private void registerObserver() {
        Log.d(TAG, "registerObserver: " + mIsRegObserver);
        if (mIsRegObserver) {
            return;
        }
        int subId1 = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
        int subId2 = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_2);
        if (SubscriptionManager.isValidSubscriptionId(subId1) &&
            SubscriptionManager.isValidSubscriptionId(subId2)) {
            Log.d(TAG, "register observer!");
            mIsRegObserver = true;
            getContentResolver().registerContentObserver(
                    Settings.Global.getUriFor(Settings.Global.MOBILE_DATA + subId1),
                    true, mDataSlot1Observer);
            getContentResolver().registerContentObserver(
                    Settings.Global.getUriFor(Settings.Global.MOBILE_DATA + subId2),
                    true, mDataSlot2Observer);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterPhoneStateListener();
        unregisterReceiver(mReceiver);
        getContentResolver().unregisterContentObserver(mDataConnectionObserver);
        getContentResolver().unregisterContentObserver(mDataConnectionSlotObserver);
        if (mIsRegObserver) {
            getContentResolver().unregisterContentObserver(mDataSlot1Observer);
            getContentResolver().unregisterContentObserver(mDataSlot2Observer);
            mIsRegObserver = false;
        }
        mIsSlot1Switching = false;
        mIsSlot1Switching = false;
        if (mDataConnectionEnabler != null) {
            mDataConnectionEnabler.pause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference.equals(mSlot1Preference)) {
            handleDataConnectionChange(PhoneConstants.SIM_ID_1);
            return true;
        } else if (preference.equals(mSlot2Preference)) {
            handleDataConnectionChange(PhoneConstants.SIM_ID_2);
            return true;
        } else {
            return super.onPreferenceTreeClick(preferenceScreen, preference);
        }
    }

    private void registerPhoneStateListener() {
        mPhoneStateListener = getPhoneStateListener();
        mTelephonyManager.listen(mPhoneStateListener,
                PhoneStateListener.LISTEN_SERVICE_STATE);
        Log.d(TAG, "Register registerPhoneStateListener");
    }

    private void unregisterPhoneStateListener() {
        mTelephonyManager.listen(mPhoneStateListener,
                PhoneStateListener.LISTEN_NONE);
        Log.d(TAG, "Register unregisterPhoneStateListener");
    }

    private PhoneStateListener getPhoneStateListener() {
        return new PhoneStateListener() {
            @Override
            public void onServiceStateChanged(ServiceState state) {
                Log.d(TAG, "PhoneStateListener: onServiceStateChanged");
                updateScreen();
            }
        };
    }

    private void updateScreen() {
        boolean slot1RadioOn = Utils.isTargetSimRadioOn(PhoneConstants.SIM_ID_1);
        boolean slot2RadioOn = Utils.isTargetSimRadioOn(PhoneConstants.SIM_ID_2);
        boolean slot1SimReady = Utils.isSimStateReady(this, PhoneConstants.SIM_ID_1);
        boolean slot2SimReady = Utils.isSimStateReady(this, PhoneConstants.SIM_ID_2);
        int dataConnectionId = SubscriptionManager.getDefaultDataSubscriptionId();
        Log.d(TAG, "updateSwitcherState "
                + ", slot1RadioOn=" + slot1RadioOn + ", slot2RadioOn=" + slot2RadioOn
                + ", slot1SimReady=" + slot1SimReady + ", slot2SimReady=" + slot2SimReady
                + ", dataConnectionId=" + dataConnectionId
                + ", mIsSlot1Switching=" + mIsSlot1Switching
                + ", mIsSlot2Switching=" + mIsSlot2Switching);
        if (!mIsSlot1Switching && !mIsSlot2Switching) {
            mSlot1Preference.setEnabled(slot1RadioOn && slot1SimReady);
            mSlot2Preference.setEnabled(slot2RadioOn && slot2SimReady);
            mDataConnectionEnabler.setUpdateSwitcherEnable(true);
            mDataConnectionEnabler.updateSwitcherState();
        }
        mSlot1Preference.setChecked(
                SubscriptionManager.getSlotIndex(dataConnectionId) == PhoneConstants.SIM_ID_1);
        mSlot2Preference.setChecked(
                SubscriptionManager.getSlotIndex(dataConnectionId) == PhoneConstants.SIM_ID_2);
    }

    private void handleDataConnectionChange(int newSlot) {
        Log.d(TAG, "handleDataConnectionChange newSlot=" + newSlot);
        if (SubscriptionManager.getSlotIndex(
                SubscriptionManager.getDefaultDataSubscriptionId()) != newSlot) {
            List<SubscriptionInfo> si =
                    SubscriptionManager.from(this).getActiveSubscriptionInfoList();
            if (si != null && si.size() > 0) {
                for (int i = 0; i < si.size(); i++) {
                    SubscriptionInfo subInfoRecord = si.get(i);
                    if (newSlot == subInfoRecord.getSimSlotIndex()) {
                        int subId = subInfoRecord.getSubscriptionId();
                        Log.d(TAG, "handleDataConnectionChange newSlot = "
                                + newSlot + " subId = " + subId);
                        switchDataDefaultSub(subId);
                        return;
                    }
                }
            }
        }
    }

    /**
     * switch data connection default SIM.
     *
     * @param value: sim id of the new default SIM
     */
    private void switchDataDefaultSub(int subId) {
        boolean enableBefore = mTelephonyManager.getDataEnabled();
        mResetSubId = SubscriptionManager.getDefaultDataSubscriptionId();
        Log.d(TAG, "switchDataDefaultSub enableBefore: " + enableBefore +
            ", mResetSubId: " + mResetSubId + ", subId: " + subId);
        if (SubscriptionManager.isValidSubscriptionId(subId) &&
            subId != mResetSubId) {
            SubscriptionManager.from(this).setDefaultDataSubId(subId);
            if (enableBefore) {
                registerObserver();
                setSlotPreferenceEnable(false);
                mDataConnectionEnabler.setUpdateSwitcherEnable(false);
                mTelephonyManager.setDataEnabled(subId, true);
                mTelephonyManager.setDataEnabled(mResetSubId, false);
            }
        }
    }

    private void resetDefaultDataSubId() {
        Log.d(TAG, "resetDefaultDataSubId()... + resetSubId: " + mResetSubId);
        SubscriptionManager.from(this).setDefaultDataSubId(mResetSubId);
    }

    private void setStatusBarEnableStatus(boolean enabled) {
        Log.i(TAG, "setStatusBarEnableStatus(" + enabled + ")");
        if (mStatusBarManager == null) {
            mStatusBarManager = (StatusBarManager) getSystemService(Context.STATUS_BAR_SERVICE);
        }
        if (mStatusBarManager != null) {
            if (enabled) {
                mStatusBarManager.disable(StatusBarManager.DISABLE_NONE);
            } else {
                mStatusBarManager.disable(StatusBarManager.DISABLE_EXPAND |
                        StatusBarManager.DISABLE_RECENT |
                        StatusBarManager.DISABLE_HOME);
            }
        } else {
            Log.e(TAG, "Fail to get status bar instance");
        }
    }

    private boolean isSimSwitchAction(String action) {
        return false;
        //return action.equals(TelephonyIntents.ACTION_SET_PHONE_RAT_FAMILY_DONE) ||
               //action.equals(TelephonyIntents.ACTION_SET_PHONE_RAT_FAMILY_FAILED);
    }

    private void handleSimSwitchBroadcast(Intent intent) {
        setStatusBarEnableStatus(true);
        updateScreen();
        //If switch fail, rollback default data value
        //if (TelephonyIntents.ACTION_SET_PHONE_RAT_FAMILY_FAILED.equals(intent.getAction())) {
        //    resetDefaultDataSubId();
        //}
    }

    public static boolean isLteSupport() {
        return SystemProperties.get("ro.boot.opt_lte_support").equals("1");
    }

}
