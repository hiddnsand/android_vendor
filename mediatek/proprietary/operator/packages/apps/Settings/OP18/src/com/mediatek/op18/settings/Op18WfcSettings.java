package com.mediatek.op18.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import com.android.ims.ImsManager;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.TelephonyIntents;

/**
 * Class to support operator customizations for WFC settings.
 */
public class Op18WfcSettings {

    private static final String TAG = "Op18WfcSettings";
    public static final String NOTIFY_CALL_STATE = "OP18:call_state_Change";
    public static final String CALL_STATE = "call_state";
    public static final int CALL_STATE_IDLE = 1;
    public static final int CALL_STATE_CS = 2;
    public static final int CALL_STATE_PS = 3;
    static Op18WfcSettings sWfcSettings = null;

    Context mContext;
    IntentFilter mIntentFilter;
    private WfcSwitchController mController;

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "action: " + intent.getAction());
            if (!ImsManager.isWfcEnabledByPlatform(context)) {
                Log.d(TAG, "isWfcEnabledByPlatform: false");
                return;
            }
            if (NOTIFY_CALL_STATE.equals(intent.getAction())) {
                int callState = intent.getIntExtra(CALL_STATE, CALL_STATE_IDLE);
                Log.v(TAG, "br call_satte: " + callState);
                mController.updateWfcSwitchState(callState);
            } else if (TelephonyIntents.ACTION_SET_RADIO_CAPABILITY_DONE.equals(intent.getAction())
                    || (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(intent.getAction())
                            && IccCardConstants.INTENT_VALUE_ICC_LOADED.equals(intent
                            .getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE)))) {
                if (ImsManager.isWfcEnabledByPlatform(context)) {
                    Log.d(TAG, "Sim is of RJIL, add WFC setting");
                    mController.addWfcPreference();
                } else {
                    Log.d(TAG, "Sim not RJIL, remove WFC setting");
                    mController.removeWfcPreference();
                }
            }
        }
    };

    private final ContentObserver mContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            this.onChange(selfChange, Settings.Global
            .getUriFor(Settings.Global.WFC_IMS_ENABLED));
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            int callState = Settings.Global.getInt(mContext.getContentResolver(),
                    Op18WfcSettings.CALL_STATE, Op18WfcSettings.CALL_STATE_IDLE);
            mController.updateWfcSwitchState(callState);
        }
    };

    private Op18WfcSettings(Context context) {
       mContext = context;
       mController = WfcSwitchController.getInstance(context);
       mIntentFilter = new IntentFilter(NOTIFY_CALL_STATE);
       mIntentFilter.addAction(TelephonyIntents.ACTION_SET_RADIO_CAPABILITY_DONE);
       mIntentFilter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
    }

    /** Returns instance of Op18WfcSettings.
         * @param context context
         * @return OP18WfcSettings
         */
    public static Op18WfcSettings getInstance(Context context) {

        if (sWfcSettings == null) {
            sWfcSettings = new Op18WfcSettings(context);
        }
        return sWfcSettings;
    }

    /** Customize WFC pref as per operator requirement
         * @param context context
         * @param preferenceScreen preferenceScreen
         * @return
         */
    public void customizedWfcPreference(Context context, Object preferenceScreen) {
        Log.d(TAG, "Wireless setting preferenceScreen:");
        mController.customizedWfcPreference(context, preferenceScreen);
    }

    /** Returns instance of Op18WfcSettings.
     * @return
     */
    public void removeWfcPreference() {
        mController.removeWfcPreference();
    }

    /** Registers receiver.
     * @return
     */
    public void register() {
        mContext.registerReceiver(mBroadcastReceiver, mIntentFilter);
        mContext.getContentResolver().registerContentObserver(Settings.Global
                .getUriFor(Settings.Global.WFC_IMS_ENABLED), false, mContentObserver);
    }

    /** Unregisters receiver.
     * @return
     */
    public void unRegister() {
        mContext.unregisterReceiver(mBroadcastReceiver);
        mContext.getContentResolver().unregisterContentObserver(mContentObserver);
    }
}
