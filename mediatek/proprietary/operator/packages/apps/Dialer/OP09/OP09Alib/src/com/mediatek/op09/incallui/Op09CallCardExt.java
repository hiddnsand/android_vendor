package com.mediatek.op09.incallui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.SystemProperties;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.incallui.call.DialerCall;
import com.android.incallui.InCallPresenter;
import com.mediatek.incallui.ext.DefaultCallCardExt;
import com.mediatek.op09.dialer.R;

import java.lang.ref.WeakReference;

/**
 * callcard extension plugin for op09.
*/
public class Op09CallCardExt extends DefaultCallCardExt {

    private static final String TAG = "Op09CallCardExt";

    private TelecomManager mTelecomManager;
    private TelephonyManager mTelephonyManager;

    private Context mContext;

    /// Optimize log output
    private static final String PROP_FORCE_DEBUG_KEY = "persist.log.tag.tel_dbg";
    public static final boolean FORCE_DEBUG =
            (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1);

    private WeakReference<DialerCall> mDialerCall;
    private static final int ICON_ID = 0x10ffffff;

    private static final String SIM_LABEL = "call_provider_name";
    private static final String ID = "id";

    public Op09CallCardExt(Context context) {
        super();
        Log.d(TAG, "Op09CallCardExt");
        mContext = context;
        mTelephonyManager = getTelephonyManager(context);
        mTelecomManager = getTelecomManager(context);
    }

    /**
     * Return the icon drawable to represent the call provider.
     *
     * @param context for get service.
     * @param handle  for get icon.
     * @return The icon.
    */
    @Override
    public Drawable getCallProviderIcon(Context context, PhoneAccountHandle handle){
        log("getCallProviderIcon account handle:" + handle);
        if (null != mTelephonyManager &&
                    mTelephonyManager.getDefault().getPhoneCount() < 2) {
            return null;
        }

        if (handle != null && getTelecomManager(
                        context).getCallCapablePhoneAccounts().size() > 0) {
            String subId = handle.getId();
            if (null != mContext && subId != null) {
                PhoneAccount phoneAccount = mTelecomManager.getPhoneAccount(handle);
                int defaultSubId = mTelephonyManager.getSubIdForPhoneAccount(phoneAccount);
                int slotId = SubscriptionManager.getSlotIndex(defaultSubId);
                if (0 == slotId) {
                    return mContext.getResources().getDrawable(R.drawable.ct_sim_indicator_1);
                } else if (1 == slotId) {
                    return mContext.getResources().getDrawable(R.drawable.ct_sim_indicator_2);
                }
            }
        }
        return null;
    }

    /**
     * Return the string label to represent the call provider.
     *
     * @param context  for get service.
     * @param account for get lable.
     * @return The lable.
    */
    @Override
    public String getCallProviderLabel(Context context, PhoneAccountHandle handle) {
        log("getCallProviderLabel account handle:" + handle);
        mTelephonyManager = getTelephonyManager(context);
        if (null != mTelephonyManager &&
                    mTelephonyManager.getDefault().getPhoneCount() < 2) {
            return null;
        }

        PhoneAccount account = mTelecomManager.getPhoneAccount(handle);
        if (account != null && getTelecomManager(
                    context).getCallCapablePhoneAccounts().size() > 0) {
            log("getCallProviderLabel account:" + account);
            log("getCallProviderLabel lable:" + account.getLabel().toString());
            return account.getLabel().toString();
        }
        return null;
    }


    @Override
    public void onHoldViewCreated(View view) {
        if (mDialerCall == null) {
            return;
        }

        DialerCall call = mDialerCall.get();
        if (call == null) {
            return;
        }

        PhoneAccountHandle accountHandle = call.getAccountHandle();
        PhoneAccount account = mTelecomManager.getPhoneAccount(accountHandle);

        int color = InCallPresenter.getInstance().getPrimaryColorFromCall(call);

        Context context = view.getContext();
        Resources resource = context.getResources();
        String packageName = context.getPackageName();

        View simLabel =
                view.findViewById(
                        resource.getIdentifier(SIM_LABEL, ID, packageName));
        updateSecondarySimLabel(simLabel, color, account);
        createSecondarySimIndicator(simLabel, account);
    }

    @Override
    public void onStateChange(Object primaryCall, Object secondaryCall) {
        if (secondaryCall == null) {
            mDialerCall = null;
            return;
        }

        if (secondaryCall instanceof DialerCall) {
            DialerCall call = (DialerCall) secondaryCall;
            mDialerCall = new WeakReference<DialerCall> (call);
        }
    }

    private void updateSecondarySimLabel(View view, int color, PhoneAccount account) {
        if (view == null || account == null) {
            return;
        }

        String label = account.getLabel().toString();
        TextView textView = (TextView) view;
        textView.setText(label);
        textView.setTextColor(color);
        //textView.setVisibility(View.INVISIBLE);
    }


    private void createSecondarySimIndicator(View view, PhoneAccount account) {
        if (!shouldShowSecondaryCallInfo()) {
            return;
        }

        ViewGroup groupParent;

        if (view != null) {
            groupParent = (ViewGroup) view.getParent();
            groupParent.removeView(view);
            view.setVisibility(View.VISIBLE);
        } else {
            log("Secondary view is null, just return.");
            return;
        }

        if (account == null) {
            log("phone account is null, just return.");
            return;
        }

        ViewGroup viewChild = (ViewGroup) groupParent.findViewById(ICON_ID);
        Bitmap iconBitmap = account.getIcon().getBitmap();

        if (null == viewChild && null != iconBitmap && null != groupParent) {
            LayoutInflater mInflater;
            mInflater = LayoutInflater.from(mContext);
            ViewGroup iconLayout =
                    (ViewGroup) mInflater.inflate(R.layout.ct_sim_indicator, groupParent, false);
            iconLayout.setId(ICON_ID);
            ImageView simIndicator = (ImageView)iconLayout.findViewById(R.id.simIndicator);
            simIndicator.setVisibility(View.VISIBLE);
            simIndicator.setImageDrawable(new BitmapDrawable(iconBitmap));
            iconLayout.addView(view, 1);
            groupParent.addView(iconLayout, 1);
        } else if (null != iconBitmap && null != groupParent) {
            viewChild.setVisibility(View.VISIBLE);
            View simIndicator = viewChild.getChildAt(0);
            if (null != simIndicator && (simIndicator instanceof ImageView)) {
                simIndicator.setVisibility(View.VISIBLE);
                ((ImageView)simIndicator).setImageDrawable(new BitmapDrawable(iconBitmap));
            }
        }
    }

    private TelecomManager getTelecomManager(Context context) {
        if (mTelecomManager == null) {
            mTelecomManager =
                    (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);
        }
        return mTelecomManager;
    }

    private TelephonyManager getTelephonyManager(Context context) {
        if (mTelephonyManager == null) {
            mTelephonyManager = (TelephonyManager) context.
                    getSystemService(Context.TELEPHONY_SERVICE);
        }
        return mTelephonyManager;
    }

    /**
     * Called when op09 plug in need to show call account icon.
     *
     * @return true if need to show.
     */
    private boolean shouldShowSecondaryCallInfo() {
        if (null != mTelephonyManager &&
                (mTelephonyManager.getDefault().getPhoneCount() < 2)) {
            return false;
        }
        return true;
    }

    /**
     * simple log info.
     *
     * @param msg need print out string.
     * @return void.
     */
    private static void log(String msg) {
        if (FORCE_DEBUG) {
            Log.d(TAG, msg);
        }
    }
}
