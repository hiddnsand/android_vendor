#ifndef __IFPSINFO_H__
#define __IFPSINFO_H__

#include <binder/IInterface.h>

namespace android {

class IFpsInfo : public IInterface {
protected:
    enum {
        INFO_CB_GET_FPS = IBinder::FIRST_CALL_TRANSACTION
    };

public:
    DECLARE_META_INTERFACE(FpsInfo);

    virtual status_t cbGetFps(int32_t fps) = 0;
};

class BnFpsInfo : public BnInterface<IFpsInfo>
{
    virtual status_t onTransact(uint32_t code,
                                const Parcel& data,
                                Parcel* reply,
                                uint32_t flags = 0);
};

};

#endif
