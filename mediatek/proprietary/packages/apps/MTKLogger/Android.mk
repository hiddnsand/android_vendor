LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

ifneq ($(TARGET_BUILD_VARIANT), eng)
LOCAL_MANIFEST_FILE := user/AndroidManifest.xml
endif

LOCAL_MODULE_TAGS := optional
LOCAL_AAPT_INCLUDE_ALL_RESOURCES := true
#use this to distinguish android version which support or not support ActionBar and Switch
LOCAL_SUPPORT_ACTION_BAR := yes

SRC_ROOT := src/com/mediatek/mtklogger
LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := MTKLogger
LOCAL_MODULE_OWNER := mtk

LOCAL_CERTIFICATE := platform

ifneq ($(MTK_BASIC_PACKAGE), yes)
LOCAL_JAVA_LIBRARIES := mediatek-framework
endif
LOCAL_JAVA_LIBRARIES += org.apache.http.legacy

LOCAL_EMMA_COVERAGE_FILTER := @$(LOCAL_PATH)/emma_filter.txt,--$(LOCAL_PATH)/emma_filter_method.txt
#EMMA_INSTRUMENT := true

LOCAL_PROGUARD_FLAG_FILES := proguard.flags

include $(BUILD_PACKAGE)

# Use the folloing include to make our test apk.
include $(call all-makefiles-under,$(LOCAL_PATH))