/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Android Resource Stream Interface
 */

#pragma once
#ifndef JNI_RESOURCE_STREAM_H
#define JNI_RESOURCE_STREAM_H

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <string>                 /* for std::string                          */
#include <a3m/stream.h>           /* for Stream and StreamSource              */
#include <utility.h>              /* for GlobalRef                            */

/**
 * Stream encapsulating an Android resource.
 * \note
 * This class calls code through the JNI interface, and so is coupled with
 * Java.
 */
class ResourceStream : public a3m::Stream
{
public:
  /** Smart pointer type for this class */
  typedef a3m::SharedPtr< ResourceStream > Ptr;

  /**
   * Constructor.
   */
  ResourceStream(
    JNIEnv* env,
    /**< JNI environment */
    jbyteArray jByteArray
    /**< Java ByteArray to stream */);

  // Override
  A3M_BOOL valid();

  // Override
  A3M_BOOL eof();

  // Override
  A3M_INT32 size();

  // Override
  A3M_INT32 seek(A3M_UINT32 offset);

  // Override
  A3M_INT32 tell();

  // Override
  A3M_INT32 read(void* dest, A3M_UINT32 byteLength);

  // Override
  A3M_INT32 write(const void* source, A3M_UINT32 byteLength);

private:
  CByteArray m_byteArray; /**< Byte array object holding Java ByteArray */
  A3M_INT32 m_index; /**< Pointer into the byte array */
};

/**
 * StreamSource for Android resources.
 * \note
 * This class calls code through the JNI interface, and so is coupled with
 * Java.
 */
class ResourceStreamSource : public a3m::StreamSource
{
public:
  /** Smart pointer type for this class */
  typedef a3m::SharedPtr< ResourceStreamSource > Ptr;

  /**
   * Constructor.
   */
  ResourceStreamSource(
    JNIEnv* env,
    /**< JNI environment */
    jobject jResourceDataSource
    /**< Android Resources object */);

  // Override
  A3M_BOOL exists(const A3M_CHAR8* stream);

  // Override
  a3m::Stream::Ptr open(
    const A3M_CHAR8* stream, A3M_BOOL writable = A3M_FALSE);

  // Override
  A3M_CHAR8 const* getName() const;

private:
  std::string m_name; /**< Name of the stream source */
  JavaVM* m_vm; /**< Java VM */
  GlobalRef<jobject> m_jResourceDataSource; /**< A3M ResourceDataSource */
  jmethodID m_jGet; /**< Class get() method */
  jmethodID m_jExists; /**< Class exists() method */
};

#endif /* JNI_RESOURCE_STREAM_H */
