/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op12.phone;

import android.content.Context;
import android.os.SystemProperties;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.util.Log;

import com.android.internal.telephony.Phone;


import com.mediatek.internal.telephony.ratconfiguration.RatConfiguration;
import com.mediatek.phone.ext.DefaultMobileNetworkSettingsExt;
import com.mediatek.op12.phone.R;



/**
 * Plugin implementation for WFC Settings plugin
 */
public class OP12MobileNetworkSettingsExt extends DefaultMobileNetworkSettingsExt {
    private static final String TAG = "OP12MobileNetworkSettingsExt";
    private Context mContext = null;
    public static final String BUTTON_ENABLED_NETWORKS_KEY = "enabled_networks_key";

    /** Constructor.
     * @param context context
     */
    public OP12MobileNetworkSettingsExt(Context context) {
        super();
        mContext = context;
    }


    /**
     * Updating entry and entry values of list preference.
     * @param activity preference activity
     * @param subId phone id
     */
    @Override
    public void initOtherMobileNetworkSettings(PreferenceActivity activity, int subId) {
        Log.d(TAG, "update entry and entry values of list preference");
        PreferenceScreen prefSet = activity.getPreferenceScreen();
        ListPreference buttonEnabledNetworks = (ListPreference) prefSet.findPreference(
                BUTTON_ENABLED_NETWORKS_KEY);
        if (buttonEnabledNetworks == null || !isMtkLCSupport()) {
            Log.d(TAG, "buttonEnabledNetworks is null");
            return;
        }
        CharSequence entries[] = mContext.getResources().getTextArray(R.array.network_mode_options);
        CharSequence entryValues[] = mContext.getResources().getTextArray(
                R.array.network_mode_options_values);
        buttonEnabledNetworks.setEntries(entries);
        buttonEnabledNetworks.setEntryValues(entryValues);
        buttonEnabledNetworks.setValueIndex(0);
    }

    /**
     * Updating network mode and summary.
     * @param buttonEnabledNetworks list preference
     * @param networkMode network mode
     */
    @Override
    public void updatePreferredNetworkValueAndSummary(ListPreference buttonEnabledNetworks,
            int networkMode) {
        Log.d(TAG, "Updating network mode and summary. networkMode:" + networkMode);
        if (buttonEnabledNetworks == null) {
            Log.d(TAG, "buttonEnabledNetworks is null");
            return;
        }
        switch (networkMode) {
            case Phone.NT_MODE_LTE_CDMA_AND_EVDO:
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_LTE_CDMA_AND_EVDO));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.global_mode));
                break;
            case Phone.NT_MODE_LTE_ONLY:
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_LTE_ONLY));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_only_mode));
                break;
            default:
        }
    }


    /**
     * C2k LCSupport (C/Lf).
     * @return true if c2k LC supported.
     */
    public boolean isMtkLCSupport() {
        boolean isSupport = RatConfiguration.isC2kSupported() &&
                RatConfiguration.isLteFddSupported() &&
                !RatConfiguration.isGsmSupported() &&
                !RatConfiguration.isWcdmaSupported() &&
                !RatConfiguration.isTdscdmaSupported();
        boolean lcSupport = false;
        if ((SystemProperties.get("ro.mtk_protocol1_rat_config")).equals("C/Lf")) {
            lcSupport = true;
        }
        Log.d(TAG, "isMtkLCSupport(): " + isSupport);
        return (isSupport && lcSupport);
    }



    /**
     * Judge if it is CT Plugin.
     * @return true if it it CT Plugin.
     */
    @Override
    public boolean isCtPlugin() {
        return true;
    }

    @Override
    public void  customizeDualVolteOpHide(PreferenceScreen preferenceScreen,
         Preference preference, boolean showPreference) {
         Log.d(TAG, "customizeDualVolteOpHide");
         if (preference != null) {
             preferenceScreen.removePreference(preference);
             preference = null;
             Log.d(TAG, "customizeDualVolteOpHide: Remove VoLTE button");
         }
    }
}
