/*
 * Copyright (C) 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Contributed by: Giesecke & Devrient GmbH.
 */

package org.simalliance.openmobileapi.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.SystemProperties;
import android.util.Log;

public class SmartcardServiceBootCompletedBroadcastReceiver extends BroadcastReceiver {
    public final static String _TAG = "SmartcardService";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null) {
            return;
        }
        String intent_boot_completed = "android.intent.action.BOOT_COMPLETED";
        final boolean bootCompleted = intent_boot_completed.equals(intent.getAction());
        // Some test (NS-IOT) need accss SIM when bootup, so we can not start SmartcardService
        // when bootup In case many logical channel access
        final String off = SystemProperties.get("persist.radio.seapi.off", "0");

        String version = "";
        try {
            if (context != null) {
                version = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0).versionName;
            }
        } catch (NameNotFoundException e) {
        }

        Log.v(_TAG, Thread.currentThread().getName() + " Received broadcast" +
            ",persist.radio.seapi.off=" + off +
            ",MTK version=" + version);

        // Only targetSdkVersion <= 25 could start background service from background app
        /*
        if(bootCompleted && off.equals("0")){
            Log.v(_TAG, "Starting smartcard service after boot completed");
            Intent serviceIntent = new Intent(context,
                org.simalliance.openmobileapi.service.SmartcardService.class );
            context.startService(serviceIntent);
        }
        */
    }
}
