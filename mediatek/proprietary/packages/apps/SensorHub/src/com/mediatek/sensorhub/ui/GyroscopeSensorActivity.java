package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class GyroscopeSensorActivity extends CustomerSensorBaseActivity {

    public GyroscopeSensorActivity() {
        super(Utils.KEY_GYROSCOPE_STATUS, Sensor.TYPE_GYROSCOPE);
    }
}
