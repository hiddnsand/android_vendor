package com.mediatek.op16.settings;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.support.v7.preference.PreferenceScreen;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.android.settings.SettingsActivity;
import com.android.settings.widget.SwitchBar;

import com.mediatek.ims.WfcReasonInfo;
import com.mediatek.settings.ext.DefaultWfcSettingsExt;
import com.mediatek.telephony.MtkTelephonyManagerEx;

/**
 * Plugin implementation for WFC Settings plugin
 */
public class Op16WfcSettingsExt extends DefaultWfcSettingsExt {

    private static final String TAG = "Op16WfcSettingsExt";
    private static final String CALL_STATE = "call_state";
    private static final String PHONE_TYPE = "phone_type";

    private Context mContext;
    private Context mAppContext;
    private WfcSettings  mWfcSettings = null;
    private SwitchBar mHotspotSwitchBar;

    /** Constructor.
     * @param context context
     */
    public Op16WfcSettingsExt(Context context) {
        super();
        mContext = context;
    }

    @Override
     /** Show tethering alert dialog.
     * @param context context
     * @return boolean
     */
    public boolean showWfcTetheringAlertDialog(Context context) {
        // Show alert only if WFC is registered
        boolean isWfcRegistered = MtkTelephonyManagerEx.getDefault()
              .isWifiCallingEnabled(SubscriptionManager.getDefaultVoiceSubscriptionId());

        Log.d(TAG, "[showWfcTetheringAlertDialog]isWfcRegistered:" + isWfcRegistered);
        int wfcState = WfcReasonInfo.CODE_WFC_DEFAULT;
        if (isWfcRegistered) {
            wfcState = WfcReasonInfo.CODE_WFC_SUCCESS;
        }
        if (wfcState != WfcReasonInfo.CODE_WFC_SUCCESS) {
            return false;
        }

        mHotspotSwitchBar = ((SettingsActivity) context).getSwitchBar();
        if (mHotspotSwitchBar != null) {
            mHotspotSwitchBar.setChecked(false);
        }

        new AlertDialog.Builder(context)
            .setCancelable(true)
            .setTitle(mContext.getText(R.string.wfc_wifi_hotspot_alert_title))
            .setMessage(mContext.getText(R.string.wfc_wifi_hotspot_alert_message))
            .setPositiveButton(mContext.getText(android.R.string.ok), new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Log.d(TAG, "Enabling hotspot");
                    // Turn on Wifi Hotspot
                    final ContentResolver cr = mContext.getContentResolver();

                    // Disable Wifi if enabling tethering
                    WifiManager wifiManager = (WifiManager) mContext
                            .getSystemService(Context.WIFI_SERVICE);
                    int wifiState = wifiManager.getWifiState();
                    if ((wifiState == WifiManager.WIFI_STATE_ENABLING) ||
                            (wifiState == WifiManager.WIFI_STATE_ENABLED)) {
                        wifiManager.setWifiEnabled(false);
                        android.provider.Settings.Global.putInt(cr, android.provider.Settings
                                .Global.WIFI_SAVED_STATE, 1);
                    }

                    OnStartTetheringCallback callback = new OnStartTetheringCallback();
                    ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(
                            Context.CONNECTIVITY_SERVICE);
                    cm.startTethering(ConnectivityManager.TETHERING_WIFI, false, callback);
                    /*if (wifiManager.setWifiApEnabled(null, true)) {
                        mHotspotSwitchBar.setEnabled(false);
                    } else {
                        mHotspotSwitchBar.setEnabled(true);
                    }*/
                }
            })
            .setNegativeButton(mContext.getText(android.R.string.cancel), new OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.dismiss();
                }
            })
            .show();
        Log.d(TAG, "Alert shown");
        return true;
    }

    @Override
    /**
     * @param context Context
     * @param preferenceScreen preferenceScreen
     * @return
     * Customize wfc preference
     */
    public void customizedWfcPreference(Context context, PreferenceScreen preferenceScreen) {
        mAppContext = context;
        mWfcSettings = WfcSettings.getInstance(context);
        mWfcSettings.register(mContext);
        mWfcSettings.customizedWfcPreference(context, preferenceScreen);
    }

     /** Called on events like onResume/onPause etc from WirelessSettings.
    * @param event resume/puase etc.
    * @return
    */
    /*
    @Override
    public void onWirelessSettingsEvent(int event) {
        Log.d("@M_" + TAG, "Wireless setting event:" + event);
        mWfcSettings = WfcSettings.getInstance(mContext);
        switch (event) {
            case DefaultWfcSettingsExt.RESUME:
                mWfcSettings.register(mContext);
                break;

            case DefaultWfcSettingsExt.PAUSE:
                mWfcSettings.unRegister(mContext);
                break;
            case DefaultWfcSettingsExt.CONFIG_CHANGE:
                if (!ImsManager.isWfcEnabledByPlatform(mContext)) {
                    mWfcSettings.removeWfcPreference();
                }
                break;
            default:
                break;
        }
    }*/

    /**.
     * Callback class of tethering state
     */
    private class OnStartTetheringCallback extends
            ConnectivityManager.OnStartTetheringCallback {
        @Override
        public void onTetheringStarted() {
            mHotspotSwitchBar.setEnabled(false);
        }
        @Override
        public void onTetheringFailed() {
            mHotspotSwitchBar.setEnabled(true);
        }
    }
}
