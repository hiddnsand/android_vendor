package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class AccSensorActivity extends CustomerSensorBaseActivity {

    public AccSensorActivity() {
        super(Utils.KEY_ACCELEROMETER_STATUS, Sensor.TYPE_ACCELEROMETER);
    }
}
