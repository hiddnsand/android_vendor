package com.mediatek.systemui.op09;

import java.util.HashMap;

import android.content.Context;
import android.text.TextUtils;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.content.res.ColorStateList;
import android.provider.Settings;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.internal.telephony.cdma.EriInfo;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;

import com.mediatek.internal.telephony.MtkGsmCdmaPhone;
import com.mediatek.internal.telephony.MtkIccCardConstants;
import com.mediatek.internal.telephony.MtkIccCardConstants.CardType;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.systemui.op09.R;
import com.mediatek.systemui.ext.DefaultSystemUIStatusBarExt;
import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.util.ArrayList;

/**
 * M: Op09 implementation of Plug-in definition of Status bar.
 */
public class Op09SystemUIStatusBarExt extends DefaultSystemUIStatusBarExt {

    private static final String TAG = "Op09SystemUIStatusBarExt";
    private static final boolean DEBUG = !isUserLoad();

    private Context mContext;

    private final int mCount;
    private int mSlotId = SubscriptionManager.INVALID_SIM_SLOT_INDEX;
    private int mSubId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
    private int mNetworkTypeIconId = 0;
    private int mDataTypeIconId = 0;
    private int mSignalStrengthIconId = 0;

    //add for log print
    private static HashMap<Integer, Boolean> mLastIsSimOffline =
            new HashMap<Integer, Boolean>();
    private static HashMap<Integer, Integer> mLastNetworkTypeIconId =
            new HashMap<Integer, Integer>();
    private static HashMap<Integer, Integer> mLastDataTypeIconId =
            new HashMap<Integer, Integer>();
    private static HashMap<Integer, Integer> mLastSignalStrengthIconId =
            new HashMap<Integer, Integer>();

    private ViewGroup mRoot = null;
    private ImageView mDataTypeView;
    private ImageView mDataActivityView;
    private ImageView mNetworkTypeView;
    private ImageView mSignalStrengthView;
    /// google default nosim view
    private ImageView mNoSim;
    private NoSimView mNoSimView;
    private ServiceState mServiceState;
    private boolean mIsRoaming;

    private static HashMap<Integer, Boolean> sIsRoamingMap =
            new HashMap<Integer, Boolean>();
    private static HashMap<Integer, ServiceState> mServiceStateMap =
            new HashMap<Integer, ServiceState>();
    private static boolean mSimInserted[] = null;
    private static boolean mRegisterDone = false;
    private static int mSimCount = 2;

    // Network type icons
    private final int NETWORK_TYPE_2G = R.drawable.stat_sys_network_type_2g;
    private final int NETWORK_TYPE_3G = R.drawable.stat_sys_network_type_3g;
    private final int NETWORK_TYPE_4G = R.drawable.stat_sys_network_type_4g;
    private final int NETWORK_TYPE_3G_2G = R.drawable.stat_sys_network_type_3g_2g;
    private final int NETWORK_TYPE_4G_2G = R.drawable.stat_sys_network_type_4g_2g;
    private final int NETWORK_TYPE_ONLY_4G = R.drawable.stat_sys_network_type_4g_data_only;
    // CA update
    private final int NETWORK_TYPE_4G_PLUS = R.drawable.stat_sys_network_type_4g_plus;
    private final int NETWORK_TYPE_4G_PLUS_2G = R.drawable.stat_sys_network_type_4gplus_2g;
    private final int NETWORK_TYPE_ONLY_4G_PLUS =
            R.drawable.stat_sys_network_type_4g_plus_data_only;

    // Data type icons
    private final int DATA_TYPE_2G = R.drawable.stat_sys_data_fully_connected_2g;
    private final int DATA_TYPE_3G = R.drawable.stat_sys_data_fully_connected_3g;
    private final int DATA_TYPE_4G = R.drawable.stat_sys_data_fully_connected_4g;
    private final int DATA_TYPE_4G_PLUS = R.drawable.stat_sys_data_fully_connected_4ga;

    // Data activity icons
    private final int DATA_ACTIVITY_NOT_INOUT = R.drawable.stat_sys_signal_not_inout;
    private final int DATA_ACTIVITY_IN = R.drawable.stat_sys_signal_in;
    private final int DATA_ACTIVITY_OUT = R.drawable.stat_sys_signal_out;
    private final int DATA_ACTIVITY_INOUT = R.drawable.stat_sys_signal_inout;

    // Signal strength icons
    private final int SIGNAL_STRENGTH_NULL_ONE_SIM = R.drawable.stat_sys_signal_null;
    private final int SIGNAL_STRENGTH_NULL_TWO_SIMS = R.drawable.stat_sys_signal_null_two_sims;
    private final int SIGNAL_STRENGTH_OFFLINE = R.drawable.stat_sys_signal_radio_off;

 private static final String PROPERTY_3G_SIM = "persist.radio.simswitch";
    // for dis volte icon
    private static HashMap<Integer, CardType> sCardTypeMap =
            new HashMap<Integer, CardType>();
    private static HashMap<Integer, Integer> sImsRegStateMap =
            new HashMap<Integer, Integer>();
    private static HashMap<Integer, Boolean> sIsOverwfcMap =
            new HashMap<Integer, Boolean>();
    private static HashMap<Integer, Boolean> sIsImsUseEnabledMap =
            new HashMap<Integer, Boolean>();
    private final Object mLock = new Object();
    private final int mDisVolteIconId = R.drawable.stat_sys_volte_dis;

    private static HashMap<Integer, ArrayList> sUpdateDisVolteIconMap =
            new HashMap<Integer, ArrayList>();

    // Data roaming indicator icon
    private final int DATA_ROAMING_INDICATOR = R.drawable.stat_sys_data_fully_connected_roam;

    private final int[] SIGNAL_STRENGTH_SINGLE = {
        R.drawable.stat_sys_signal_1_fully, //R.drawable.stat_sys_signal_0_fully,
        R.drawable.stat_sys_signal_1_fully,
        R.drawable.stat_sys_signal_2_fully,
        R.drawable.stat_sys_signal_3_fully,
        R.drawable.stat_sys_signal_4_fully
    };

    private final int[][] SIGNAL_STRENGTH_TOWER = {
        {
            R.drawable.stat_sys_signal_1_1_fully, //R.drawable.stat_sys_signal_0_0_fully,
            R.drawable.stat_sys_signal_1_1_fully, //R.drawable.stat_sys_signal_0_1_fully,
            R.drawable.stat_sys_signal_1_2_fully, //R.drawable.stat_sys_signal_0_2_fully,
            R.drawable.stat_sys_signal_1_3_fully, //R.drawable.stat_sys_signal_0_3_fully,
            R.drawable.stat_sys_signal_1_4_fully  //R.drawable.stat_sys_signal_0_4_fully
        },
        {
            R.drawable.stat_sys_signal_1_1_fully, //R.drawable.stat_sys_signal_1_0_fully,
            R.drawable.stat_sys_signal_1_1_fully,
            R.drawable.stat_sys_signal_1_2_fully,
            R.drawable.stat_sys_signal_1_3_fully,
            R.drawable.stat_sys_signal_1_4_fully
        },
        {
            R.drawable.stat_sys_signal_2_1_fully, //R.drawable.stat_sys_signal_2_0_fully,
            R.drawable.stat_sys_signal_2_1_fully,
            R.drawable.stat_sys_signal_2_2_fully,
            R.drawable.stat_sys_signal_2_3_fully,
            R.drawable.stat_sys_signal_2_4_fully
        },
        {
            R.drawable.stat_sys_signal_3_1_fully, //R.drawable.stat_sys_signal_3_0_fully,
            R.drawable.stat_sys_signal_3_1_fully,
            R.drawable.stat_sys_signal_3_2_fully,
            R.drawable.stat_sys_signal_3_3_fully,
            R.drawable.stat_sys_signal_3_4_fully
        },
        {
            R.drawable.stat_sys_signal_4_1_fully, //R.drawable.stat_sys_signal_4_0_fully,
            R.drawable.stat_sys_signal_4_1_fully,
            R.drawable.stat_sys_signal_4_2_fully,
            R.drawable.stat_sys_signal_4_3_fully,
            R.drawable.stat_sys_signal_4_4_fully
        }
    };

    private ContentObserver mImsSwitchChangeObserver  = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            int mainPhoneSubId = MtkSubscriptionManager.getSubIdUsingPhoneId(getMainPhoneId());
            //boolean imsUseEnabled = isImsUseEnabled();
            synchronized (mLock) {
                boolean imsUseEnabled = (1 == Settings.Global.getInt(mContext.getContentResolver(),
                        Settings.Global.ENHANCED_4G_MODE_ENABLED, 0));
                sIsImsUseEnabledMap.put(mainPhoneSubId, imsUseEnabled);
                if (DEBUG) {
                    Log.d(TAG, "mImsSwitchChangeObserver: onChange=" + selfChange +
                            ",subId: " + mainPhoneSubId + ",ImsUseEnabled: " + imsUseEnabled);
                }
            }

            if (sUpdateDisVolteIconMap.containsKey(mainPhoneSubId)) {
                ArrayList<ImageView> mUpdateViewList = sUpdateDisVolteIconMap.get(mainPhoneSubId);
                for (ImageView view: mUpdateViewList) {
                    updateDisVolteView(mainPhoneSubId, view);
                }
                mUpdateViewList = null;
            }
        }
    };

    /**
     * Constructs a new Op09SystemUIStatusBarExt instance with Context.
     * @param context The Context object
     */
    public Op09SystemUIStatusBarExt(Context context) {
        super(context);
        mContext = context;
        mCount = TelephonyManager.getDefault().getSimCount();
        initSimInserted(mCount);
        if (DEBUG) {
            Log.d(TAG, "mCount=" + mCount);
        }

        if (!mRegisterDone) {
            mContext.getContentResolver().registerContentObserver(
                    Settings.Global.getUriFor(Settings.Global.ENHANCED_4G_MODE_ENABLED), true,
                    mImsSwitchChangeObserver);
            mRegisterDone = true;
        }
    }

    @Override
    public boolean disableHostFunction() {
        return true;
    }

    /**
    * Get the main phone id
    * @return
    */
    public static int getMainPhoneId() {
        int mainPhoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        String curr3GSim = SystemProperties.get(PROPERTY_3G_SIM, "1");
        if (!TextUtils.isEmpty(curr3GSim)) {
            int curr3GPhoneId = Integer.parseInt(curr3GSim);
            mainPhoneId = curr3GPhoneId - 1;
        }
        if (DEBUG) Log.d(TAG, "getMainPhoneId: " + mainPhoneId);
        return mainPhoneId;
    }

    private void initImsRegisterState(int subId) {
        int slotId = SubscriptionManager.getSlotIndex(subId);
        boolean imsRegInfo = false;
        if (slotId == getMainPhoneId()) {
            imsRegInfo = MtkTelephonyManagerEx.getDefault().isImsRegistered(subId);
            synchronized (mLock) {
                if (imsRegInfo) {
                    sImsRegStateMap.put(subId, ServiceState.STATE_IN_SERVICE);
                } else {
                    sImsRegStateMap.put(subId, ServiceState.STATE_OUT_OF_SERVICE);
                }
                sIsOverwfcMap.put(subId, false);

                boolean imsUseEnabled = (1 == Settings.Global.getInt(mContext.getContentResolver(),
                                    Settings.Global.ENHANCED_4G_MODE_ENABLED, 0));
                sIsImsUseEnabledMap.put(subId, imsUseEnabled);

                if (DEBUG) {
                    Log.d(TAG, "initImsRegisterState get volte subId: " + subId +
                            ",ImsUseEnabled: " + imsUseEnabled + ",imsRegInfo: " + imsRegInfo);
                }
            }
        }
    }

    @Override
    public void setNoSimIconTint(int tint, ImageView nosim) {
        setTint(nosim, tint);
        setTint(mNoSim, tint);
    }

    @Override
    public void setIconTint(int tint, float darkIntensity) {
        if (mNetworkTypeView != null) {
            setTint(mNetworkTypeView, tint);
        }
        if (mDataTypeView != null) {
            setTint(mDataTypeView, tint);
        }
        if (mDataActivityView != null) {
            setTint(mDataActivityView, tint);
        }
        if (mNoSimView != null && mNoSimView.mNoSimView != null) {
            setTint(mNoSimView.mNoSimView, tint);
        }
        if (mSignalStrengthView != null) {
            setTint(mSignalStrengthView, tint);
        }
    }

    private void setTint(ImageView v, int tint) {
        v.setImageTintList(ColorStateList.valueOf(tint));
    }

    @Override
    public void getServiceStateForCustomizedView(int subId) {
        mServiceState = getServiceStateMap(subId);
    }

    @Override
    public int getCustomizeCsState(ServiceState serviceState, int state) {
        if (serviceState != null) {
            return serviceState.getVoiceRegState();
        } else {
            return state;
        }
    }

    @Override
    public int getNetworkTypeIcon(int subId, int iconId, int networkType,
                    ServiceState serviceState) {
        int newType = getNetworkTypeFromServiceState(networkType, serviceState);
        if (newType != networkType) {
            Log.d(TAG, "getNetworkTypeIcon(), revise networkType,"
                + " subId=" + subId
                + ", oldType=" + networkType
                + ", newType=" + newType);
            networkType = newType;
        }

        if (isSimOffline(subId, networkType, serviceState) ||
                !hasService(serviceState)) {
            mNetworkTypeIconId = 0;
        } else if (isShow4GDataOnly(subId, networkType, serviceState)) {
            if (isCASupport() || networkType == TelephonyManager.NETWORK_TYPE_LTE_CA) {
                mNetworkTypeIconId = NETWORK_TYPE_ONLY_4G_PLUS;
            } else {
                mNetworkTypeIconId = NETWORK_TYPE_ONLY_4G;
            }
        } else {
            mNetworkTypeIconId =
                getNetworkTypeIconId(networkType, mIsRoaming);
        }

        if (!mLastNetworkTypeIconId.containsKey(subId) ||
                (mLastNetworkTypeIconId.containsKey(subId)
                        && mLastNetworkTypeIconId.get(subId) != mNetworkTypeIconId)) {
            Log.d(TAG, "getNetworkTypeIcon(), subId=" + subId + ", networkType=" + networkType
                    + ", iconId=" + mNetworkTypeIconId);
        }
        mLastNetworkTypeIconId.put(subId, mNetworkTypeIconId);

        return mNetworkTypeIconId;
    }

    @Override
    public int getDataTypeIcon(int subId, int iconId,
                    int dataType, int dataState, ServiceState serviceState) {
        mDataTypeIconId = getDataTypeIconId(dataType, dataState);

        if (!mLastDataTypeIconId.containsKey(subId) || (mLastDataTypeIconId.containsKey(subId)
                && mLastDataTypeIconId.get(subId) != mDataTypeIconId)) {
            Log.d(TAG, "getDataTypeIcon(), subId=" + subId + ", dataType=" + dataType
                    + ", dataState=" + dataState + ", iconId=" + mDataTypeIconId);
        }
        mLastDataTypeIconId.put(subId, mDataTypeIconId);

        return mDataTypeIconId;
    }

    @Override
    public int getCustomizeSignalStrengthIcon(int subId, int iconId,
                    SignalStrength signalStrength, int networkType,
                    ServiceState serviceState) {
        boolean showTower = false;
        int newType = getNetworkTypeFromServiceState(networkType, serviceState);
        if (newType != networkType) {
            if (DEBUG) {
                Log.d(TAG, "getCustomizeSignalStrengthIcon(), revise networkType,"
                    + " subId=" + subId
                    + ", oldType=" + networkType
                    + ", newType=" + newType);
            }
            networkType = newType;
        }

        updateServiceStateMap(subId, serviceState);

        if (isSimOffline(subId, networkType, serviceState) ||
                !hasService(serviceState)) {
            mSignalStrengthIconId = SIGNAL_STRENGTH_OFFLINE;
        } else {
            boolean is4GDataOnly =
                isShow4GDataOnly(subId, networkType, serviceState);
            mIsRoaming  = isRoaming(serviceState, signalStrength);
            updateRoamingStatusMap(subId, mIsRoaming);

            if (!is4GDataOnly
                && !mIsRoaming
                && (networkType == TelephonyManager.NETWORK_TYPE_EVDO_0
                    || networkType == TelephonyManager.NETWORK_TYPE_EVDO_A
                    || networkType == TelephonyManager.NETWORK_TYPE_EVDO_B
                    || networkType == TelephonyManager.NETWORK_TYPE_EHRPD
                    || networkType == TelephonyManager.NETWORK_TYPE_LTE
                    || networkType == TelephonyManager.NETWORK_TYPE_LTE_CA)) {
                showTower = true;
            }
            if (isImsUseEnabled() && isCT4GTurnOn() &&
                    (networkType == TelephonyManager.NETWORK_TYPE_LTE ||
                     networkType == TelephonyManager.NETWORK_TYPE_LTE_CA)) {
                showTower = false;
            }
            mSignalStrengthIconId = getSignalStrengthIconId(
                signalStrength, showTower, networkType);
        }

        if (!mLastSignalStrengthIconId.containsKey(subId) ||
                (mLastSignalStrengthIconId.containsKey(subId) &&
                        mLastSignalStrengthIconId.get(subId) != mSignalStrengthIconId)) {
            Log.d(TAG, "getCustomizeSignalStrengthIcon(), subId=" + subId
                    + ", networkType=" + networkType
                    + ", showTower=" + showTower
                    + ", iconId=" + mSignalStrengthIconId
                    + ", signalStrength=" + signalStrength);
        }
        mLastSignalStrengthIconId.put(subId, mSignalStrengthIconId);

        return mSignalStrengthIconId;
    }

    @Override
    public void addCustomizedView(int subId,
                    Context context, ViewGroup root) {
        int slotId = SubscriptionManager.getSlotIndex(subId);
        FrameLayout dataGroup = (FrameLayout) LayoutInflater.from(mContext)
            .inflate(R.layout.mobile_data_group, null);

        mRoot = root;
        mDataTypeView = (ImageView) dataGroup.findViewById(R.id.data_type);
        mDataActivityView = (ImageView)
            dataGroup.findViewById(R.id.data_activity);

        // Add the data group view in the forefront of the view group
        root.addView(dataGroup, 1);

        // Add the no sim view for another slot
        if (mCount == 2) {
            mNoSimView = new NoSimView(mContext);
            mNoSimView.mNoSimView.setImageDrawable(
                mContext.getResources().getDrawable(
                    SIGNAL_STRENGTH_NULL_ONE_SIM));
            if (slotId == 0) {
                mNoSimView.mSlotId = 1;
                root.addView(mNoSimView.mNoSimView);
            } else if (slotId == 1) {
                mNoSimView.mSlotId = 0;
                root.addView(mNoSimView.mNoSimView, 0);
            }
        }

        initImsRegisterState(subId);
        if (DEBUG) {
            Log.d(TAG, "addCustomizedView(), subId=" + subId
                + ", slotId=" + slotId
                + ", root=" + root);
        }
    }

    @Override
    public void SetHostViewInvisible(ImageView view) {
        view.setVisibility(View.GONE);
    }

    @Override
    public void setCustomizedNetworkTypeView(int subId,
                    int networkTypeId, ImageView networkTypeView) {
        mNetworkTypeView = networkTypeView;
        if (networkTypeView != null) {
            if (!mLastNetworkTypeIconId.containsKey(subId) ||
                    (mLastNetworkTypeIconId.containsKey(subId) &&
                            mLastNetworkTypeIconId.get(subId) != networkTypeId)) {
                Log.d(TAG, "NetworkTypeView, subId=" + subId + " networkTypeId=" + networkTypeId);
            }

            if (isSimOfflineBySub(subId, mServiceState) || !isSimInsert(subId) ||
                    !hasService(mServiceState)) {
                networkTypeView.setVisibility(View.GONE);
                return;
            }

            if (networkTypeId != 0) {
                networkTypeView.setImageDrawable(
                    mContext.getResources().getDrawable(networkTypeId));
                networkTypeView.setVisibility(View.VISIBLE);
            } else {
                networkTypeView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void setCustomizedDataTypeView(int subId,
                    int dataTypeId, boolean dataIn, boolean dataOut) {
        if (mRoot != null) {
            if (!mLastDataTypeIconId.containsKey(subId) || (mLastDataTypeIconId.containsKey(subId)
                    && mLastDataTypeIconId.get(subId) != dataTypeId)) {
                Log.d(TAG, "DataTypeView, subId=" + subId + " dataTypeId=" + dataTypeId);
            }

            if (isSimOfflineBySub(subId, mServiceState) || !isSimInsert(subId) ||
                    !hasService(mServiceState)) {
                mDataTypeView.setVisibility(View.GONE);
                mDataActivityView.setVisibility(View.GONE);
                return;
            }

            if (dataTypeId != 0) {
                //dataTypeId = getDataNetworkTypeId(dataTypeId);
                mDataTypeView.setImageDrawable(
                    mContext.getResources().getDrawable(dataTypeId));
                mDataTypeView.setVisibility(View.VISIBLE);

                int dataActivityId = getDataActivityIconId(dataIn, dataOut);
                if (dataActivityId != 0) {
                    mDataActivityView.setImageDrawable(
                        mContext.getResources().getDrawable(dataActivityId));
                    mDataActivityView.setVisibility(View.VISIBLE);
                } else {
                    mDataActivityView.setVisibility(View.GONE);
                }
            } else {
                mDataTypeView.setVisibility(View.GONE);
                mDataActivityView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void setCustomizedMobileTypeView(int subId,
                    ImageView mobileTypeView) {
        if (mobileTypeView != null) {
            int networkType = getNetworkTypeFromServiceState(0, mServiceState);
            boolean isOffline = isSimOffline(subId, networkType, mServiceState);
            boolean isRoaming = getRoamingStatusByMap(subId);

            if (!isOffline && isRoaming && hasService(mServiceState)) {
                mobileTypeView.setImageDrawable(
                    mContext.getResources().getDrawable(DATA_ROAMING_INDICATOR));
                mobileTypeView.setVisibility(View.VISIBLE);
            } else {
                mobileTypeView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void setImsRegInfo(int subId, int imsRegState, boolean isOverwfc,
            boolean isImsOverVoice) {
        synchronized (mLock) {
            sImsRegStateMap.put(subId, imsRegState);
            sIsOverwfcMap.put(subId, isOverwfc);
        }
    }

    private void updateCardTypeMap(int slotId) {
        MtkIccCardConstants.CardType cardType =
                MtkTelephonyManagerEx.getDefault().getCdmaCardType(slotId);
        if (!sCardTypeMap.containsKey(slotId) || (sCardTypeMap.containsKey(slotId) &&
                !sCardTypeMap.get(slotId).equals(cardType))) {
            sCardTypeMap.put(slotId, cardType);
        }
    }

    private void cacheVolteView(int subId, ImageView volteView) {
        ArrayList<ImageView> mUpdateViewList;
        if (sUpdateDisVolteIconMap.containsKey(subId)) {
            mUpdateViewList = sUpdateDisVolteIconMap.get(subId);
        } else {
            mUpdateViewList = new ArrayList<>();
        }
        if (!mUpdateViewList.contains(volteView)) {
            mUpdateViewList.add(volteView);
        }
        sUpdateDisVolteIconMap.put(subId, mUpdateViewList);
        mUpdateViewList = null;
    }

    private void updateDisVolteView(int subId, ImageView volteView) {
        ServiceState ss =getServiceStateMap(subId);
        int networkType = getNetworkTypeFromServiceState(0, ss);
        int mainPhoneSubId = MtkSubscriptionManager.getSubIdUsingPhoneId(getMainPhoneId());
        int slotId = SubscriptionManager.getSlotIndex(subId);
        synchronized (mLock) {
            if (sCardTypeMap.containsKey(slotId)) {
                CardType cardType = sCardTypeMap.get(slotId);
                // just for CT card
                if (isCTCardType(cardType)) {
                    if (isSimOfflineBySub(subId, ss)) {
                        volteView.setImageDrawable(
                        mContext.getResources().getDrawable(mDisVolteIconId));
                        volteView.setVisibility(View.VISIBLE);
                    } else if ((subId == mainPhoneSubId && getImsUseEnabledStatus(subId)) &&
                               (networkType == TelephonyManager.NETWORK_TYPE_LTE ||
                                networkType == TelephonyManager.NETWORK_TYPE_LTE_CA) &&
                                (sImsRegStateMap.containsKey(subId) &&
                                (sImsRegStateMap.get(subId) != ServiceState.STATE_IN_SERVICE)) &&
                                (sIsOverwfcMap.containsKey(subId) && !sIsOverwfcMap.get(subId))) {
                        volteView.setImageDrawable(
                                mContext.getResources().getDrawable(mDisVolteIconId));
                        volteView.setVisibility(View.VISIBLE);
                    } else if ((subId == mainPhoneSubId && !getImsUseEnabledStatus(subId)) ||
                            (subId != mainPhoneSubId)) {
                        // CT card is primary sim but ims disable, or CT card is not primary
                        // so it's ims also disable, do not show this icon.
                        volteView.setVisibility(View.GONE);
                        if (DEBUG) {
                            Log.d(TAG, "CT sim ims turn off do not show DisVolteIcon, slotId: "
                                        + slotId);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void setDisVolteView(int subId, int iconId, ImageView volteView) {
        cacheVolteView(subId, volteView);
        if (iconId > 0) {
            return;
        } else {
            updateDisVolteView(subId, volteView);
        }
    }

    private boolean isCTCardType(CardType cardType) {
        if (((MtkIccCardConstants.CardType.CT_4G_UICC_CARD).equals(cardType) ||
                (MtkIccCardConstants.CardType.CT_3G_UIM_CARD).equals(cardType) ||
                (MtkIccCardConstants.CardType.CT_UIM_SIM_CARD).equals(cardType))) {
            return true;
        } else {
            return false;
        }
    }

    private boolean getImsUseEnabledStatus(int subId) {
        if (sIsImsUseEnabledMap.containsKey(subId)) {
            return sIsImsUseEnabledMap.get(subId);
        } else {
            return false;
        }
    }

    @Override
    public void setCustomizedSignalStrengthView(int subId,
                    int signalStrengthId, ImageView signalStrengthView) {
        mSignalStrengthView = signalStrengthView;
        if (signalStrengthView != null) {
            if (!mLastSignalStrengthIconId.containsKey(subId) ||
                    (mLastSignalStrengthIconId.containsKey(subId) &&
                            mLastSignalStrengthIconId.get(subId) != signalStrengthId)) {
                if (DEBUG) {
                    Log.d(TAG, "SignalView, subId=" + subId + " signalId=" + signalStrengthId);
                }
            }

            if (!isSimInsert(subId)) {
                signalStrengthView.setVisibility(View.GONE);
                return;
            }
            if (isSimOfflineBySub(subId, mServiceState) || !hasService(mServiceState)) {
                signalStrengthId = SIGNAL_STRENGTH_OFFLINE;
            }

            if (signalStrengthId != 0) {
                signalStrengthView.setImageDrawable(
                    mContext.getResources().getDrawable(signalStrengthId));
                signalStrengthView.setVisibility(View.VISIBLE);
            } else {
                signalStrengthView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void setCustomizedView(int subId) {
        if (mNoSimView != null) {
            // Show the no sim for other slot when needed
            if (SubscriptionManager.isValidSlotIndex(mNoSimView.mSlotId)) {
                boolean inserted = getSimInserted(mNoSimView.mSlotId);

                if (!inserted) {
                    mNoSimView.mNoSimView.setVisibility(View.VISIBLE);

                    if (DEBUG) {
                        Log.d(TAG, "setCustomizedView(), subId=" + subId
                            + ", another SIM slotId=" + mNoSimView.mSlotId
                            + ", inserted=" + inserted);
                    }
                } else {
                    mNoSimView.mNoSimView.setVisibility(View.GONE);
                }
            } else {
                mNoSimView.mNoSimView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void setCustomizedNoSimView(ImageView noSimView) {
        mNoSim = noSimView;
        if (noSimView != null) {
            int iconId = 0;
            switch (mCount) {
                case 1:
                    iconId = SIGNAL_STRENGTH_NULL_ONE_SIM;
                    break;

                case 2:
                    iconId = SIGNAL_STRENGTH_NULL_TWO_SIMS;
                    break;

                default:
                    break;
            }

            if (iconId != 0) {
                noSimView.setImageDrawable(
                    mContext.getResources().getDrawable(iconId));
            }
        }
    }

    @Override
    public void setCustomizedAirplaneView(View noSimView, boolean airplaneMode) {
        if ((noSimView != null) && airplaneMode) {
            noSimView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setSimInserted(int slotId, boolean insert) {
        updateSimInserted(slotId, insert);
        updateCardTypeMap(slotId);
    }

    public class NoSimView {
        public int mSlotId;
        public ImageView mNoSimView;

        public NoSimView(Context context) {
            mSlotId = SubscriptionManager.INVALID_SIM_SLOT_INDEX;
            mNoSimView = new ImageView(mContext);
        }
    }

    public static final boolean isUserLoad() {
        return SystemProperties.get("ro.build.type").equals("user") ||
                    SystemProperties.get("ro.build.type").equals("userdebug");
    }

    public static final boolean isCASupport() {
        return SystemProperties.get("gsm.lte.ca.support").equals("1");
    }

    private static ServiceState getServiceState(int subId) {
        ServiceState serviceState = TelephonyManager.getDefault().
                getServiceStateForSubscriber(subId);
        return serviceState;
    }

    private boolean is4GDataOnlyMode(int subId) {
        int mode = Settings.Global.getInt(
                mContext.getContentResolver(),
                Settings.Global.LTE_ON_CDMA_RAT_MODE,
                Phone.NT_MODE_LTE_CDMA_AND_EVDO);

        if (DEBUG) {
            Log.d(TAG, "is4GDataOnlyMode(), mode=" + mode
                + "4gDataOnly=" + (mode == MtkGsmCdmaPhone.NT_MODE_LTE_TDD_ONLY));
        }

        return mode == MtkGsmCdmaPhone.NT_MODE_LTE_TDD_ONLY;
    }

    private boolean isShow4GDataOnly(int subId, int networkType,
                        ServiceState serviceState) {
        boolean dataOnly = false;

        if ((networkType == TelephonyManager.NETWORK_TYPE_LTE
             || networkType == TelephonyManager.NETWORK_TYPE_LTE_CA)
            && serviceState != null) {
            if (serviceState.getVoiceRegState() != ServiceState.STATE_IN_SERVICE) {
                if (serviceState.getDataRegState() == ServiceState.STATE_IN_SERVICE
                    || is4GDataOnlyMode(subId)) {
                    dataOnly = true;
                }
            }
        }

        return dataOnly;
    }

    private boolean isImsUseEnabled() {
        boolean imsUseEnabled;
        imsUseEnabled = ImsManager.isVolteEnabledByPlatform(mContext) &&
                ImsManager.isEnhanced4gLteModeSettingEnabledByUser(mContext) &&
                ImsManager.isNonTtyOrTtyOnVolteEnabled(mContext);
        if (DEBUG) {
            Log.d(TAG, "imsUseEnabled: " + imsUseEnabled);
        }
        return imsUseEnabled;
    }

    private boolean isCT4GTurnOn() {
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
        int pattern = Settings.Global.getInt(mContext.getContentResolver(),
                Settings.Global.PREFERRED_NETWORK_MODE + subId,
                Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
        if (pattern == Phone.NT_MODE_LTE_CDMA_AND_EVDO
                || pattern == Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isSimRadioOn(int subId) {
        ITelephony telephony = ITelephony.Stub.asInterface(ServiceManager
                .getService(Context.TELEPHONY_SERVICE));
        try {
            if (telephony != null) {
                return telephony.isRadioOnForSubscriber(
                            subId, mContext.getPackageName());
            }
        } catch (RemoteException e) {
            Log.e(TAG, "ITelephony.isRadioOnForSubscriber exception");
        }
        return false;
    }

    private boolean isSimRoaming(int subId, ServiceState serviceState) {
        boolean isRoaming = false;

        if (serviceState != null) {
            isRoaming = !MtkTelephonyManagerEx.getDefault().isInHomeNetwork(subId);
        }

        if (DEBUG) {
            Log.d(TAG, "isSimRoaming: roaming=" + isRoaming
                + ", subId=" + subId
                + ", serviceState=" + serviceState);
        }

        return isRoaming;
    }

    private boolean isRoaming(ServiceState serviceState, SignalStrength signalStrength) {
        if ((signalStrength != null) && !signalStrength.isGsm()) {
            final int iconMode = serviceState.getCdmaEriIconMode();
            return serviceState.getCdmaEriIconIndex() != EriInfo.ROAMING_INDICATOR_OFF
                    && (iconMode == EriInfo.ROAMING_ICON_MODE_NORMAL
                        || iconMode == EriInfo.ROAMING_ICON_MODE_FLASH);
        } else {
            return serviceState != null && serviceState.getRoaming();
        }
    }

    private static void updateRoamingStatusMap(int subId, boolean isRoaming) {
        if (!sIsRoamingMap.containsKey(subId) || isRoaming != sIsRoamingMap.get(subId)) {
            sIsRoamingMap.put(subId, isRoaming);
        }
    }

    private static boolean getRoamingStatusByMap(int subId) {
        if (sIsRoamingMap.containsKey(subId)) {
            return sIsRoamingMap.get(subId);
        } else {
            return false;
        }
    }

    private static void updateServiceStateMap(int subId, ServiceState serviceState) {
        mServiceStateMap.put(subId, serviceState);
    }

    private static ServiceState getServiceStateMap(int subId) {
        ServiceState serviceState;
        if (mServiceStateMap.containsKey(subId)) {
            serviceState = mServiceStateMap.get(subId);
        } else {
            serviceState = getServiceState(subId);
            mServiceStateMap.put(subId, serviceState);
            if (DEBUG) {
                Log.d(TAG, "getServiceStateMap()");
            }
        }

        return serviceState;
    }

    private static void initSimInserted(int simCount) {
        mSimCount = simCount;
        if (mSimInserted == null) {
            Log.d(TAG, "initSimInserted() mSimCount=" + mSimCount);
            mSimInserted = new boolean[mSimCount + 1];

            for (int i = 0; i < mSimCount; i++) {
                mSimInserted[i] = TelephonyManager.getDefault().hasIccCard(i);
            }
        }
    }

    private static void updateSimInserted(int slotId, boolean insert) {
        if (slotId < mSimCount) {
            mSimInserted[slotId] = insert;
        }
    }

    private static boolean getSimInserted(int slotId) {
        if (slotId < mSimCount) {
            return mSimInserted[slotId];
        }
        return false;
    }

    private boolean isSimOffline(int subId, int networkType,
                        ServiceState serviceState) {
        boolean ret;

        boolean is4GDataOnly =
            isShow4GDataOnly(subId, networkType, serviceState);
        boolean isEmergencyOnly =
            serviceState != null ? serviceState.isEmergencyOnly() : false;
        boolean isRadioOn = ((serviceState != null) &&
                             (serviceState.getState() != ServiceState.STATE_POWER_OFF));

        if (isEmergencyOnly) {
            if (is4GDataOnly) {
                ret = false;
            } else {
                ret = true;
            }
        } else {
            ret = !(isRadioOn || (serviceState != null ?
                serviceState.getDataRegState() != ServiceState.STATE_POWER_OFF
                : false));
        }

        if (mLastIsSimOffline.containsKey(subId) && mLastIsSimOffline.get(subId) != ret) {
            Log.d(TAG, "isSimOffline: offline=" + ret
                + ", subId=" + subId
                + ", networkType=" + networkType
                + ", serviceState=" + serviceState);
        }
        mLastIsSimOffline.put(subId, ret);

        return ret;
    }

    private boolean isSimOfflineBySub(int subId, ServiceState serviceState) {
        int networkType = getNetworkTypeFromServiceState(0, serviceState);
        return isSimOffline(subId, networkType, serviceState);
    }

    private boolean isSimInsert(int subId) {
        int slotId = SubscriptionManager.getSlotIndex(subId);
        boolean inserted = false;
        if (slotId >= 0) {
            inserted = getSimInserted(slotId);
        }
        return inserted;
    }

    private boolean hasService(ServiceState serviceState) {
        if (serviceState != null) {
            // Consider the device to be in service if either voice or data
            // service is available. Some SIM cards are marketed as data-only
            // and do not support voice service, and on these SIM cards, we
            // want to show signal bars for data service as well as the "no
            // service" or "emergency calls only" text that indicates that voice
            // is not available.
            switch (serviceState.getVoiceRegState()) {
                case ServiceState.STATE_POWER_OFF:
                    return false;
                case ServiceState.STATE_OUT_OF_SERVICE:
                case ServiceState.STATE_EMERGENCY_ONLY:
                    return serviceState.getDataRegState() == ServiceState.STATE_IN_SERVICE;
                default:
                    return true;
            }
        } else {
            return false;
        }
    }

    private int getNetworkTypeIconId(int networkType, boolean isRoaming) {
        int iconId = 0;

        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                break;

            case TelephonyManager.NETWORK_TYPE_GSM:
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                iconId = NETWORK_TYPE_2G;
                break;

            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
            case TelephonyManager.NETWORK_TYPE_UMTS:
                iconId = NETWORK_TYPE_3G;
                break;

            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                if (isRoaming) {
                    iconId = NETWORK_TYPE_3G;
                } else {
                    iconId = NETWORK_TYPE_3G_2G;
                }
                break;

            case TelephonyManager.NETWORK_TYPE_LTE:
            case TelephonyManager.NETWORK_TYPE_LTE_CA:
                if ((isImsUseEnabled() && isCT4GTurnOn()) || isRoaming) {
                    if (isCASupport() || networkType == TelephonyManager.NETWORK_TYPE_LTE_CA) {
                        iconId = NETWORK_TYPE_4G_PLUS;
                    } else {
                        iconId = NETWORK_TYPE_4G;
                    }
                } else {
                    if (isCASupport() || networkType == TelephonyManager.NETWORK_TYPE_LTE_CA) {
                        iconId = NETWORK_TYPE_4G_PLUS_2G;
                    } else {
                        iconId = NETWORK_TYPE_4G_2G;
                    }
                }
                break;

            default:
                break;
        }

        return iconId;
    }

    private int getDataTypeIconId(int dataType, int dataState) {
        int iconId = 0;

        if (dataState == TelephonyManager.DATA_CONNECTED) {
            switch (dataType) {
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                    break;

                case TelephonyManager.NETWORK_TYPE_GSM:
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    iconId = DATA_TYPE_2G;
                    break;

                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                    iconId = DATA_TYPE_3G;
                    break;

                case TelephonyManager.NETWORK_TYPE_LTE:
                    if (isCASupport()) {
                        iconId = DATA_TYPE_4G_PLUS;
                    } else {
                        iconId = DATA_TYPE_4G;
                    }
                    break;

                case TelephonyManager.NETWORK_TYPE_LTE_CA:
                    iconId = DATA_TYPE_4G_PLUS;
                    break;

                default:
                    break;
            }
        }

        return iconId;
    }

    private int getDataActivityIconId(boolean dataIn, boolean dataOut) {
        int iconId = 0;

        if (dataIn && dataOut) {
            iconId = DATA_ACTIVITY_INOUT;
        } else if (dataIn) {
            iconId = DATA_ACTIVITY_IN;
        } else if (dataOut) {
            iconId = DATA_ACTIVITY_OUT;
        }

        return iconId;
    }

    private int getSignalStrengthIconId(SignalStrength signalStrength,
                    boolean showTower, int networkType) {
        int level1 = 0;
        int level2 = 0;

        if (signalStrength != null) {
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                case TelephonyManager.NETWORK_TYPE_GSM:
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    level1 = signalStrength.getLevel();
                    break;

                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    level1 = signalStrength.getCdmaLevel();
                    break;

                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    level1 = signalStrength.getLevel();
                    break;

                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                    level1 = signalStrength.getEvdoLevel();
                    level2 = signalStrength.getCdmaLevel();
                    break;

                case TelephonyManager.NETWORK_TYPE_LTE:
                case TelephonyManager.NETWORK_TYPE_LTE_CA:
                    level1 = signalStrength.getLteLevel();
                    level2 = signalStrength.getCdmaLevel();
                    break;

                default:
                    break;
            }
        }

        if (showTower) {
            return SIGNAL_STRENGTH_TOWER[level1][level2];
        } else {
            return SIGNAL_STRENGTH_SINGLE[level1];
        }
    }

    private int getNetworkTypeFromServiceState(
                    int networkType, ServiceState serviceState) {
        int type = networkType;
        if (networkType == TelephonyManager.NETWORK_TYPE_LTE_CA) {
            return type;
        }
        if (serviceState != null) {
            type = serviceState.getDataRegState() == ServiceState.STATE_IN_SERVICE &&
                    serviceState.getDataNetworkType() != TelephonyManager.NETWORK_TYPE_UNKNOWN
                ? serviceState.getDataNetworkType()
                : serviceState.getVoiceNetworkType();
        }
        return type;
    }

    private int getDataNetworkTypeId(int dataTypeId) {
        int type;

        if (mServiceState == null) {
            return dataTypeId;
        }

        type = mServiceState.getDataNetworkType();
        /*
        if (type == TelephonyManager.NETWORK_TYPE_LTE
               || type == TelephonyManager.NETWORK_TYPE_LTE_CA) {
            if (mServiceState != null) {
                type = (mServiceState.getProprietaryDataRadioTechnology() == 0 ?
                    TelephonyManager.NETWORK_TYPE_LTE : TelephonyManager.NETWORK_TYPE_LTE_CA);
            }
        }
        */
        if (type != TelephonyManager.NETWORK_TYPE_UNKNOWN) {
            dataTypeId = getDataTypeIconId(type, TelephonyManager.DATA_CONNECTED);
        }

        return dataTypeId;
    }

}
