/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "[VT]VcodecCap"

#include <utils/KeyedVector.h>
#include <unistd.h>

#include "IVcodecCap.h"
#include "VcodecCap.h"


using namespace android;

///////////////////////////////////////////////////////////////////////////////////
// OP01 (CMCC)
static const int MAX_H264_MEDIA_PROFILE_NUM_OP01  = 26;
static video_media_profile_t rmedia_profile_op01[MAX_H264_MEDIA_PROFILE_NUM_OP01] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1,  176,  144, 15,  145 * 1000, 192 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 526 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_1,  480,  320, 30,  450 * 1000, 594 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_2,  640,  480, 15,  495 * 1000, 640 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3,    640,  480, 30,  742 * 1000, 960 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1, 1280,  720, 30, 1314 * 1000,  1700 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 526 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_2_1,  480,  320, 30,  450 * 1000, 594 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_2_2,  640,  480, 15,  495 * 1000, 640 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_3,    640,  480, 30,  742 * 1000, 960 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_3_1, 1280,  720, 30,  1314 * 1000, 1700 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_4, 1280,  720, 30, 1314 * 1000,  1700 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 526 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_2_1,  480,  320, 30,  450 * 1000, 594 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_2_2,  640,  480, 15,  495 * 1000, 640 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_3,    640,  480, 30,  742 * 1000, 960 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_3_1, 1280,  720, 30,  1314 * 1000, 1700 * 1000, 1},
};
static const int MAX_QUALITY_NUM  = 4;
static QualityInfo_t rQualityInfo_OP01[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1},
};

///////////////////////////////////////////////////////////////////////////////////
// OP06 (Vodafone)
static const int MAX_H264_MEDIA_PROFILE_NUM_OP06  = 24;
static video_media_profile_t rmedia_profile_op06[MAX_H264_MEDIA_PROFILE_NUM_OP06] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 526 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_1,  480,  320, 30,  450 * 1000, 594 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_2,  640,  480, 15,  495 * 1000, 640 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3,    640,  480, 30,  742 * 1000, 960 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1, 1280,  720, 30, 1314 * 1000,  1700 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 526 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_2_1,  480,  320, 30,  450 * 1000, 594 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_2_2,  640,  480, 15,  495 * 1000, 640 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_3,    640,  480, 30,  742 * 1000, 960 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_3_1, 1280,  720, 30,  1314 * 1000, 1700 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 526 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_2_1,  480,  320, 30,  450 * 1000, 594 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_2_2,  640,  480, 15,  495 * 1000, 640 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_3,    640,  480, 30,  742 * 1000, 960 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_3_1, 1280,  720, 30,  1314 * 1000, 1700 * 1000, 1},
};

static QualityInfo_t rQualityInfo_OP06[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_3_1},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_3},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_1},
};


///////////////////////////////////////////////////////////////////////////////////
// OP08 (TMO)
static const int MAX_H264_MEDIA_PROFILE_NUM_OP08  = 21;
static video_media_profile_t rmedia_profile_op08[MAX_H264_MEDIA_PROFILE_NUM_OP08] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2,    352,  288, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_1,  480,  320, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_2,  640,  480, 15,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3,    640,  480, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_2,  320,  240, 20,  218 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_2,    352,  288, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_2_1,  480,  320, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_2_2,  640,  480, 15,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_3,    640,  480, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_2,  320,  240, 20,  218 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_2,    352,  288, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_2_1,  480,  320, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_2_2,  640,  480, 15,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_3,    640,  480, 30,  327 * 1000, 416 * 1000, 1},
};

static QualityInfo_t rQualityInfo_OP08[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1},
};
///////////////////////////////////////////////////////////////////////////////////
// OP09 (CT)
static const int MAX_H264_MEDIA_PROFILE_NUM_OP09  = 24;
static video_media_profile_t rmedia_profile_op09[MAX_H264_MEDIA_PROFILE_NUM_OP09] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 526 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_1,  480,  320, 30,  450 * 1000, 594 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_2,  640,  480, 15,  495 * 1000, 640 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3,    640,  480, 30,  742 * 1000, 960 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1, 1280,  720, 30, 1314 * 1000,  1700 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 526 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_2_1,  480,  320, 30,  450 * 1000, 594 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_2_2,  640,  480, 15,  495 * 1000, 640 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_3,    640,  480, 30,  742 * 1000, 960 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_3_1, 1280,  720, 30,  1314 * 1000, 1700 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 526 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_2_1,  480,  320, 30,  450 * 1000, 594 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_2_2,  640,  480, 15,  495 * 1000, 640 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_3,    640,  480, 30,  742 * 1000, 960 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_3_1, 1280,  720, 30,  1314 * 1000, 1700 * 1000, 1},
};

static QualityInfo_t rQualityInfo_OP09[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1},
};
///////////////////////////////////////////////////////////////////////////////////
// OP18 (Reliance)
static const int MAX_H264_MEDIA_PROFILE_NUM_OP18  = 9;
static video_media_profile_t rmedia_profile_op18[MAX_H264_MEDIA_PROFILE_NUM_OP08] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_MAIN, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_HIGH, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 384 * 1000, 1},
};

static QualityInfo_t rQualityInfo_OP18[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1},
};

///////////////////////////////////////////////////////////////////////////////////
// OP112 (Telcel)
static const int MAX_H264_MEDIA_PROFILE_NUM_OP112  = 3;
static video_media_profile_t rmedia_profile_op112[MAX_H264_MEDIA_PROFILE_NUM_OP112] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  409 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 15,  409 * 1000, 512 * 1000, 1},
};

static QualityInfo_t rQualityInfo_OP112[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1},
};

///////////////////////////////////////////////////////////////////////////////////
// OP117 (SmartFren)
static const int MAX_H264_MEDIA_PROFILE_NUM_OP117  = 7;
static video_media_profile_t rmedia_profile_op117[MAX_H264_MEDIA_PROFILE_NUM_OP117] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2,    352,  288, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_1,  480,  320, 30,  327 * 1000, 416 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_2,  640,  480, 15,  327 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3,    640,  480, 30,  327 * 1000, 1024 * 1000, 1},

};

static QualityInfo_t rQualityInfo_OP117[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1},
};

///////////////////////////////////////////////////////////////////////////////////
// OP120 (Claro Peru)
static const int MAX_H264_MEDIA_PROFILE_NUM_OP120  = 3;
static video_media_profile_t rmedia_profile_op120[MAX_H264_MEDIA_PROFILE_NUM_OP120] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  409 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 15,  409 * 1000, 512 * 1000, 1},
};

static QualityInfo_t rQualityInfo_OP120[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1},
};


///////////////////////////////////////////////////////////////////////////////////
// OP122 (AIS)
static const int MAX_H264_MEDIA_PROFILE_NUM_OP122  = 8;
static video_media_profile_t rmedia_profile_op122[MAX_H264_MEDIA_PROFILE_NUM_OP122] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_1,  480,  320, 30,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_2,  640,  480, 15,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3,    640,  480, 30,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1, 1280,  720, 30, 900 * 1000,  1000 * 1000, 1},
};

static QualityInfo_t rQualityInfo_OP122[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_2},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1},
};

///////////////////////////////////////////////////////////////////////////////////
// OP125 (DTAC/Dewav )
static const int MAX_H264_MEDIA_PROFILE_NUM_OP125  = 8;
static video_media_profile_t rmedia_profile_op125[MAX_H264_MEDIA_PROFILE_NUM_OP125] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_1,  480,  320, 30,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_2,  640,  480, 15,  466 * 1000, 666 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3,    640,  480, 30,  700 * 1000, 1000 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1, 1280,  720, 30, 700 * 1000,  1000 * 1000, 1},
};

static QualityInfo_t rQualityInfo_OP125[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1},
};


///////////////////////////////////////////////////////////////////////////////////
// OP126 (AVEA)
static const int MAX_H264_MEDIA_PROFILE_NUM_OP126  = 8;
static video_media_profile_t rmedia_profile_op126[MAX_H264_MEDIA_PROFILE_NUM_OP126] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_1,  480,  320, 30,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_2,  640,  480, 15,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3,    640,  480, 30,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1, 1280,  720, 30, 900 * 1000,  1000 * 1000, 1},
};

static QualityInfo_t rQualityInfo_OP126[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_2},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1},
};

///////////////////////////////////////////////////////////////////////////////////
// OP143 (AVEA)
static const int MAX_H264_MEDIA_PROFILE_NUM_OP143  = 8;
static video_media_profile_t rmedia_profile_op143[MAX_H264_MEDIA_PROFILE_NUM_OP143] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  145 * 1000, 270 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  218 * 1000, 384 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 30,  327 * 1000, 462 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2,    352,  288, 30,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_1,  480,  320, 30,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_2,  640,  480, 15,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3,    640,  480, 30,  360 * 1000, 512 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1, 1280,  720, 30, 900 * 1000,  1000 * 1000, 1},
};

static QualityInfo_t rQualityInfo_OP143[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_3_1},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_2_2},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1},
};

///////////////////////////////////////////////////////////////////////////////////
// OP153 (VHA)
static const int MAX_H264_MEDIA_PROFILE_NUM_OP153  = 3;
static video_media_profile_t rmedia_profile_op153[MAX_H264_MEDIA_PROFILE_NUM_OP153] =
{
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1,  320,  240, 10,  225 * 1000, 282 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2,  320,  240, 15,  340 * 1000, 424 * 1000, 1},
    {VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3,  320,  240, 15,  340 * 1000, 424 * 1000, 1},
};

static QualityInfo_t rQualityInfo_OP153[MAX_QUALITY_NUM] =
{
    {VIDEO_QUALITY_FINE, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_3},
    {VIDEO_QUALITY_HIGH, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_MEDIUM, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_2},
    {VIDEO_QUALITY_LOW, VIDEO_H264, VIDEO_PROFILE_BASELINE, VIDEO_LEVEL_1_1},
};

void getDefaultH264MediaProfileByOperator(int opid, video_media_profile_t **ppvideo_media_profile_t, int *pCount)
{
    switch (opid)
    {
        case 1: // CMCC
            *ppvideo_media_profile_t = rmedia_profile_op01;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP01;
            break;
        case 6:     // Vodafone
            *ppvideo_media_profile_t = rmedia_profile_op06;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP06;
            break;
        case 8: // TMO
            *ppvideo_media_profile_t = rmedia_profile_op08;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP08;
            break;
        case 9: // CT
            *ppvideo_media_profile_t = rmedia_profile_op09;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP09;
            break;
        case 12:    // VzW
            *ppvideo_media_profile_t = rmedia_profile_op18;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP18;
            break;
        case 18:    // Reliance
            *ppvideo_media_profile_t = rmedia_profile_op18;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP18;
            break;
        case 112:   // Telcel
            *ppvideo_media_profile_t = rmedia_profile_op112;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP112;
            break;
        case 117:    // SmartFren
            *ppvideo_media_profile_t = rmedia_profile_op117;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP117;
            break;
        case 120:   // Claro Peru
            *ppvideo_media_profile_t = rmedia_profile_op120;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP120;
            break;
        case 122:    // AIS
            *ppvideo_media_profile_t = rmedia_profile_op122;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP122;
            break;
        case 125:   // DTAC/Dewav
            *ppvideo_media_profile_t = rmedia_profile_op125;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP125;
            break;
        case 126:   // AVEA
            *ppvideo_media_profile_t = rmedia_profile_op126;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP126;
            break;
        case 143:   // Turkcell
            *ppvideo_media_profile_t = rmedia_profile_op143;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP143;
            break;
        case 153:   // VHA
            *ppvideo_media_profile_t = rmedia_profile_op153;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP153;
            break;
        default:
            *ppvideo_media_profile_t = rmedia_profile_op08;
            *pCount = MAX_H264_MEDIA_PROFILE_NUM_OP08;
            break;
    }
}

void createH264QualityMediaProfileByOperator(int opid, QualityInfo_t *pQualityInfo)
{
    switch (opid)
    {
        case 1: // CMCC
            {
                memcpy(pQualityInfo, rQualityInfo_OP01, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 6: // Vodafone
            {
                memcpy(pQualityInfo, rQualityInfo_OP06, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 8: // TMO
            {
                memcpy(pQualityInfo, rQualityInfo_OP08, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 9: // CT
            {
                memcpy(pQualityInfo, rQualityInfo_OP09, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 12: // VzW
            {
                memcpy(pQualityInfo, rQualityInfo_OP18, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 18: // Reliance
            {
                memcpy(pQualityInfo, rQualityInfo_OP18, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 112: // Telcel
            {
                memcpy(pQualityInfo, rQualityInfo_OP112, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 117: // SmartFren
            {
                memcpy(pQualityInfo, rQualityInfo_OP117, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 120: // Claro Peru
            {
                memcpy(pQualityInfo, rQualityInfo_OP120, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 122: // AIS
            {
                memcpy(pQualityInfo, rQualityInfo_OP122, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 125: // DTAC/Dewav
            {
                memcpy(pQualityInfo, rQualityInfo_OP125, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 126: // AVEA
            {
                memcpy(pQualityInfo, rQualityInfo_OP126, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 143: // AIS
            {
                memcpy(pQualityInfo, rQualityInfo_OP143, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        case 153: // VHA
            {
                memcpy(pQualityInfo, rQualityInfo_OP153, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
        default:
            {
                memcpy(pQualityInfo, rQualityInfo_OP08, sizeof(QualityInfo_t) * MAX_QUALITY_NUM);
            }
            break;
    }
}
