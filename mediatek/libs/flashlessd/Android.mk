LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/external/flashlessd))
include $(CLEAR_VARS)
LOCAL_MODULE = flashlessd
LOCAL_MODULE_CLASS = EXECUTABLES
LOCAL_MODULE_OWNER = mtk
LOCAL_PROPRIETARY_MODULE = true
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libcutils libc2kutils libnvram libfile_op liblog
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/flashlessd
include $(BUILD_PREBUILT)
endif
