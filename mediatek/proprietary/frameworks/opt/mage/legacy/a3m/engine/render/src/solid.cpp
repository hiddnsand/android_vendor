/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * A3M Solid Implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/noncopyable.h>          /* for NonCopyable */
#include <a3m/properties.h>           /* for J_COUNT and J_WORLD */
#include <a3m/rendercontext.h>       /* for RenderContext */
#include <a3m/solid.h>                /* This class's API */
#include <a3m/scenenodevisitor.h>     /* for SceneNodeVisitor interface */

namespace
{

  using namespace a3m;

  A3M_INT32 const MAX_JOINTS_PER_VERTEX = 4;

  /*
   * Automatic "resource management" class which reverses the winding mode of
   * an appearance, and reverts it on destruction.
   */
  class ReverseWinding : private NonCopyable
  {
  public:
    ReverseWinding( Appearance::Ptr const& appearance, A3M_BOOL activate ) :
      m_activated( activate )
    {
      if( m_activated )
      {
        m_appearance = appearance;
        m_winding = m_appearance->getWindingOrder();

        if (m_winding == WIND_CCW)
        {
          m_appearance->setWindingOrder( WIND_CW );
        }
        else
        {
          m_appearance->setWindingOrder( WIND_CCW );
        }
      }
    }

    ~ReverseWinding()
    {
      if( m_activated )
      {
        m_appearance->setWindingOrder( m_winding );
      }
    }

  private:
    A3M_BOOL m_activated;
    Appearance::Ptr m_appearance;
    WindingOrder m_winding;
  };

} // namespace

namespace a3m
{
  // Instantiate the type ID for this class (please ensure this is unique!)
  A3M_UINT32 const Solid::NODE_TYPE = 'S';

  /*
   * Constructor
   */
  Solid::Solid()
    : m_jointsPerVertex( 0 )
  {
  }

  /* Solid constructor
   * Constructs a Solid from a Mesh and an Appearance
   */
  Solid::Solid( Mesh::Ptr const& mesh, Appearance::Ptr const& appearance )
    : m_jointsPerVertex( 0 )
  {
    setMesh( mesh );
    setAppearance( appearance );
  }

  void Solid::setJointsPerVertex(A3M_INT32 count)
  {
    if (count > MAX_JOINTS_PER_VERTEX)
    {
      A3M_LOG_WARN(
        "%d is greater than the maximum joints per vertex; clamping to %d",
        count, MAX_JOINTS_PER_VERTEX);
      m_jointsPerVertex = MAX_JOINTS_PER_VERTEX;
    }
    else
    {
      m_jointsPerVertex = count;
    }
  }

  void Solid::setBindShapeTransform(Matrix4f const& transform)
  {
    m_bindShapeTransform = transform;

    // Recalculate all joint transforms
    for (A3M_UINT32 i = 0; i < m_jointAttachments.size(); ++i)
    {
      calculateJointTransform(m_jointAttachments[i]);
    }
  }

  void Solid::attachJoint(
    SceneNode::Ptr const& sceneNode,
    Matrix4f const& inverseBindPoseTransform)
  {
    JointAttachment attachment;
    attachment.sceneNode = sceneNode;
    attachment.inverseBindPoseTransform = inverseBindPoseTransform;

    // Pre-calculate the joint transform for efficiency
    calculateJointTransform(attachment);

    m_jointAttachments.push_back(attachment);
  }

  void Solid::draw(RenderContext& context)
  {
    draw(context, *m_appearance);
  }

  void Solid::draw(RenderContext& context, Appearance& appearance)
  {
    /*
     * We have three cached matrices per joint attachment:
     *
     *   1) jointTransform - Calculated when attaching the joint
     *   2) poseTransform  - Stores previous joint pose to tell when joint moves
     *   3) worldTransform - Calculated whenever joint moves
     *
     * Below is a table showing the number of matrix multiplies saved for all
     * possibilities.  As shown, matrix 1 always saves a single matrix multiply
     * when the joint is moving, but only gives a saving when the joint is not
     * moving if matrices 2 and 3 are not used.
     *
     *   +---------------------------------------------------+
     *   | Matrices Cached | Joint Moving | Joint Not Moving |
     *   +---------------------------------------------------+
     *   |       None      |      0       |        0         |
     *   |     1           |      1       |        1         |
     *   |        2, 3     |      0       |        2         |
     *   |     1, 2, 3     |      1       |        2         |
     *   +---------------------------------------------------+
     *
     * The use of matrix 2 could be avoided if there were some other way of
     * telling when a scene nodes had moved (likely to be tricky).
     */

    A3M_INT32 jointCount = static_cast<A3M_INT32>(m_jointAttachments.size());

    for (A3M_INT32 i = 0; i < jointCount; ++i)
    {
      JointAttachment& attachment = m_jointAttachments[i];
      Matrix4f const& poseTransform = attachment.sceneNode->getWorldTransform();

      // If joint has moved, recalculate the joint's world transform
      if (poseTransform != attachment.poseTransform)
      {
        attachment.poseTransform = poseTransform;
        attachment.worldTransform = poseTransform * attachment.jointTransform;
      }

      context.getProperty(properties::J_WORLD)->setValue(attachment.worldTransform, i);
    }

    context.getProperty(properties::J_COUNT)->setValue(m_jointsPerVertex);

    ReverseWinding reverse( m_appearance, getWorldMirrored() );

    if( appearance.getShaderProgram() )
    {
      appearance.enable( context );
      appearance.getShaderProgram()->bind( getMesh()->getVertexBuffer() );
      getMesh()->getIndexBuffer().draw();
    }
  }

  /*
   * Accept a visitor.
   */
  void Solid::accept( SceneNodeVisitor& visitor )
  {
    visitor.visit( this );
  }

  void Solid::calculateJointTransform(JointAttachment& attachment)
  {
    attachment.jointTransform =
      attachment.inverseBindPoseTransform * m_bindShapeTransform;
  }

} /* namespace a3m */
