/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

///////////////////////////////////////////////////////////////////////////////
// No Warranty
// Except as may be otherwise agreed to in writing, no warranties of any
// kind, whether express or implied, are given by MTK with respect to any MTK
// Deliverables or any use thereof, and MTK Deliverables are provided on an
// "AS IS" basis.  MTK hereby expressly disclaims all such warranties,
// including any implied warranties of merchantability, non-infringement and
// fitness for a particular purpose and any warranties arising out of course
// of performance, course of dealing or usage of trade.  Parties further
// acknowledge that Company may, either presently and/or in the future,
// instruct MTK to assist it in the development and the implementation, in
// accordance with Company's designs, of certain softwares relating to
// Company's product(s) (the "Services").  Except as may be otherwise agreed
// to in writing, no warranties of any kind, whether express or implied, are
// given by MTK with respect to the Services provided, and the Services are
// provided on an "AS IS" basis.  Company further acknowledges that the
// Services may contain errors, that testing is important and Company is
// solely responsible for fully testing the Services and/or derivatives
// thereof before they are used, sublicensed or distributed.  Should there be
// any third party action brought against MTK, arising out of or relating to
// the Services, Company agree to fully indemnify and hold MTK harmless.
// If the parties mutually agree to enter into or continue a business
// relationship or other arrangement, the terms and conditions set forth
// hereunder shall remain effective and, unless explicitly stated otherwise,
// shall prevail in the event of a conflict in the terms in any agreements
// entered into between the parties.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008, MediaTek Inc.
// All rights reserved.
//
// Unauthorized use, practice, perform, copy, distribution, reproduction,
// or disclosure of this information in whole or in part is prohibited.

 
#define LOG_TAG "tsfstream_test"

#include <vector>

#include <sys/time.h>
#include <sys/stat.h>
#include <sys/prctl.h>

#include <stdio.h>
#include <stdlib.h>
//
#include <errno.h>
#include <fcntl.h>

#include <mtkcam/def/common.h>

//
#include <semaphore.h>
#include <pthread.h>
#include <utils/threads.h>
#include <../ITsfStream.h>

//#include <mtkcam/iopipe/PostProc/IFeatureStream.h>

//#include <mtkcam/imageio/ispio_pipe_ports.h>
#include <drv/imem_drv.h>
//#include <mtkcam/drv/isp_drv.h>

//
using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSTsf;

#define TSFI_X_SIZE 19200
#define TSFI_Y_SIZE 1

#define TSFO_X_SIZE 2400
#define TSFO_Y_SIZE 1

#include "tsfi.h"
#include "tsfo_a_golden.h"

#define TSF_MEMALIGNED 16
#define TSF_MEMALIGNED_MINUS1 (TSF_MEMALIGNED - 1)


static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size);

int tsf_default();

MBOOL g_bTSFCallback;

MVOID TSFCallback(TSFParams& rParams);

/*******************************************************************************
*  Main Function 
*  
********************************************************************************/

int test_tsfStream(int argc, char** argv)
{
    int ret = 0; 
    int testCase = -1;
    int i;
    if (argc>=1)
    {
        testCase = atoi(argv[0]);
    }

    switch(testCase)
    {
        case 0:  //Unit Test Case, Only One DVE Request, and One WMFE Request
            ret=tsf_default();
            break;
        default:
            break;
    }
    
    ret = 1;
    return ret; 
}

MVOID TSFCallback(TSFParams& rParams)
{
    (void)rParams;
    printf("--- [TSF callback func]\n");
    g_bTSFCallback = MTRUE;
}

int tsf_default()
{
    int ret=0;
    printf("--- [tsf_default]\n");
    MUINTPTR temptsfinphy, temptsfinvir;
    MUINTPTR temptsfoutphy, temptsfoutvir;
    
    //i need a varaible to switch the difference between test code and dpe_drv.cpp
    
    NSCam::NSIoPipe::NSTsf::ITsfStream* pStream;
    pStream= NSCam::NSIoPipe::NSTsf::ITsfStream::createInstance("test_tsf_default");
    pStream->init();   
    printf("--- [test_tsf_default...TsfStream init done\n");

    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

  
   
    printf("--- tsf input allocate init\n");
    IMEM_BUF_INFO buf_tsfi_frame;
    buf_tsfi_frame.size= (TSFI_X_SIZE*TSFI_Y_SIZE) + TSF_MEMALIGNED_MINUS1;
    //buf_tsfi_frame.size= sizeof(g_tsfi_array)+ TSF_MEMALIGNED_MINUS1;
    printf("tsf input size:%d",(TSFI_X_SIZE*TSFI_Y_SIZE) + TSF_MEMALIGNED_MINUS1);
    printf("tsf input aary size:%ld",sizeof(g_tsfi_array));

    
    mpImemDrv->allocVirtBuf(&buf_tsfi_frame);
    mpImemDrv->mapPhyAddr(&buf_tsfi_frame);

    printf("in tsf va:0x%lx, mva:0x%lx\n",(unsigned long)buf_tsfi_frame.virtAddr, (unsigned long)buf_tsfi_frame.phyAddr);
    temptsfinphy = (MUINTPTR)((buf_tsfi_frame.phyAddr + TSF_MEMALIGNED_MINUS1) & ~TSF_MEMALIGNED_MINUS1);
    temptsfinvir = (MUINTPTR)((buf_tsfi_frame.virtAddr + TSF_MEMALIGNED_MINUS1) & ~TSF_MEMALIGNED_MINUS1);
    printf("temp in tsf va:0x%lx, mva:0x%lx\n",(unsigned long)temptsfinvir, (unsigned long)temptsfinphy);
    //memcpy( (MUINT8*)(buf_tsfi_frame.virtAddr), (MUINT8*)(g_tsfi_array), sizeof(g_tsfi_array));
    memcpy( (MUINT8*)(temptsfinvir), (MUINT8*)(g_tsfi_array), sizeof(g_tsfi_array));
     
    printf("--- tsf output allocate init\n");
    IMEM_BUF_INFO buf_tsfo_frame;
    buf_tsfo_frame.size= (TSFO_X_SIZE*TSFO_Y_SIZE) + TSF_MEMALIGNED_MINUS1;
    //buf_tsfo_frame.size= sizeof(g_tsfo_a_golden_array)+ TSF_MEMALIGNED_MINUS1;
    printf("tsf output size:%d",(TSFO_X_SIZE*TSFO_Y_SIZE) + TSF_MEMALIGNED_MINUS1);
    printf("tsf output aary size:%ld",sizeof(g_tsfo_a_golden_array));

    mpImemDrv->allocVirtBuf(&buf_tsfo_frame);
    mpImemDrv->mapPhyAddr(&buf_tsfo_frame);
    printf("out tsf va:0x%lx, mva:0x%lx\n",(unsigned long)buf_tsfo_frame.virtAddr, (unsigned long)buf_tsfo_frame.phyAddr);
    memset((char*)buf_tsfo_frame.virtAddr, 0, buf_tsfo_frame.size);
    temptsfoutphy = (MUINTPTR)((buf_tsfo_frame.phyAddr + TSF_MEMALIGNED_MINUS1) & ~TSF_MEMALIGNED_MINUS1);
    temptsfoutvir = (MUINTPTR)((buf_tsfo_frame.virtAddr + TSF_MEMALIGNED_MINUS1) & ~TSF_MEMALIGNED_MINUS1);
    printf("temp out tsf va:0x%lx, mva:0x%lx\n",(unsigned long)temptsfoutvir, (unsigned long)temptsfoutphy);
     


    TSFParams rTsfParams;
    int coef[8];

    for( unsigned int ii = 0; ii < 10; ii++ ){
        MUINT8* g_golden = (MUINT8*)g_tsfo_a_golden_array;
        switch( ii%3 ){
            case 0:
                coef[0]=-1;
                coef[1]=-1;
                coef[2]=-1;
                coef[3]=-1;
                coef[4]=-1;
                coef[5]=-1;
                coef[6]=-1;
                coef[7]=-1;
                g_golden = (MUINT8*)g_tsfo_a_golden_array_m1;
            break;
            case 1:
                coef[0]=-411;
                coef[1]=81;
                coef[2]=-157;
                coef[3]=35;
                coef[4]=-8;
                coef[5]=-44;
                coef[6]=3;
                coef[7]=-44;
                g_golden = (MUINT8*)g_tsfo_a_golden_array_r;
            break;
            case 2:
                coef[0]=0;
                coef[1]=1;
                coef[2]=0;
                coef[3]=1;
                coef[4]=0;
                coef[5]=1;
                coef[6]=0;
                coef[7]=1;
                g_golden = (MUINT8*)g_tsfo_a_golden_array_01i;
            break;
        }

        /*
            rTsfParams.mTSFConfig.TSF_COEFF_1 = 0;//0x563C972A;
            rTsfParams.mTSFConfig.TSF_COEFF_2 = 0;//0x0411C3AF;
            rTsfParams.mTSFConfig.TSF_COEFF_3 = 0;//0x15A6464A;
            rTsfParams.mTSFConfig.TSF_COEFF_4 = 0;//0x40CB414F;
            */
        rTsfParams.mTSFConfig.TSF_COEFF_1 = ((coef[1] << 16) | ( coef[0] & 0x0000ffff));
        rTsfParams.mTSFConfig.TSF_COEFF_2 = ((coef[3] << 16) | ( coef[2] & 0x0000ffff));
        rTsfParams.mTSFConfig.TSF_COEFF_3 = ((coef[5] << 16) | ( coef[4] & 0x0000ffff));
        rTsfParams.mTSFConfig.TSF_COEFF_4 = ((coef[7] << 16) | ( coef[6] & 0x0000ffff));

        rTsfParams.mTSFConfig.DMA_Tsfi.dmaport = DMA_TSFI;
        rTsfParams.mTSFConfig.DMA_Tsfi.u4BufPA = temptsfinphy;
        rTsfParams.mTSFConfig.DMA_Tsfi.u4Width = TSFI_X_SIZE;
        rTsfParams.mTSFConfig.DMA_Tsfi.u4Height = TSFI_Y_SIZE;
        rTsfParams.mTSFConfig.DMA_Tsfi.u4Stride = TSFI_X_SIZE;

        rTsfParams.mTSFConfig.DMA_Tsfo.dmaport = DMA_TSFO;
        rTsfParams.mTSFConfig.DMA_Tsfo.u4BufPA = temptsfoutphy;
        rTsfParams.mTSFConfig.DMA_Tsfo.u4Width = TSFO_X_SIZE;
        rTsfParams.mTSFConfig.DMA_Tsfo.u4Height = TSFO_Y_SIZE;
        rTsfParams.mTSFConfig.DMA_Tsfo.u4Stride = TSFO_X_SIZE;

        rTsfParams.mpfnCallback = TSFCallback;  

        g_bTSFCallback = MFALSE;

        //enque
        ret=pStream->TSFenque(rTsfParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [tsf_default..tsf enque fail\n]");
        }
        //else
        {
            //printf("---[tsf_default..tsf enque done\n]");
        }
        do{
            if (g_bTSFCallback ==  MTRUE){
                g_bTSFCallback =  MFALSE;
                break;
            }
        }while(1);

        MUINT8* g_tsfhw = (MUINT8*)temptsfoutvir;
        
        unsigned long i = 0;
            int j = 0;
            int tsf_err_cnt = 0;
            
            for(i = 0; i < sizeof(g_tsfo_a_golden_array); i++) // Only need to compare fd_num number data,  *4 is due to one fd_data is 128 bits long = 4 int size
            {
                if(g_tsfhw[i] != g_golden[i])
                {
                    printf("Loop[%ld] Compare Error!  tsf hw:(0x%lx, 0x%x), tsf golden:(0x%lx, 0x%x)\n", i, (unsigned long)(g_tsfhw + i), g_tsfhw[i], (unsigned long)(g_golden + i), g_golden[i]);
                    tsf_err_cnt++;
                }
            }

            //printf("TSF bit-true match done!  fderrcnt(%d)", tsf_err_cnt);

            if(tsf_err_cnt != 0)
            {
                printf("(%d) TSF bit-true Fail!!\n",ii);

            }
            //else
            //{
                //printf("(%d) TSF bit-true Pass!!\n",ii);
            //}


        }
#if 0

    MUINT8* g_tsfhw = (MUINT8*)temptsfoutvir;
    
    int i = 0, j = 0;
    int tsf_err_cnt = 0;
    
    for(i = 0; i < sizeof(g_tsfo_a_golden_array); i++) // Only need to compare fd_num number data,  *4 is due to one fd_data is 128 bits long = 4 int size
    {
        if(g_tsfhw[i] != g_golden[i])
        {
            printf("Loop[%d] Compare Error!  tsf hw:(0x%x, 0x%x), tsf golden:(0x%x, 0x%x)\n", i, (g_tsfhw + i), g_tsfhw[i], (g_golden + i), g_golden[i]);
            tsf_err_cnt++;
        }
    }

    //printf("TSF bit-true match done!  fderrcnt(%d)", tsf_err_cnt);

    if(tsf_err_cnt != 0)
    {
        printf("(%d) TSF bit-true Fail!!\n",ii);

    }
    //else
    //{
        //printf("(%d) TSF bit-true Pass!!\n",ii);
    //}
}
#endif

    mpImemDrv->freeVirtBuf(&buf_tsfi_frame);
    mpImemDrv->freeVirtBuf(&buf_tsfo_frame);

    pStream->uninit();   
    printf("--- [TsfStream uninit done\n");
    pStream->destroyInstance("test_tsf_default"); 

    mpImemDrv->uninit();
    printf("--- [Imem uninit done\n");

    return ret;
}



/******************************************************************************
* save the buffer to the file
*******************************************************************************/
static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)
{
    int nw, cnt = 0;
    uint32_t written = 0;

    //LOG_INF("(name, buf, size) = (%s, %x, %d)", fname, buf, size);
    //LOG_INF("opening file [%s]\n", fname);
    int fd = ::open(fname, O_RDWR | O_CREAT, S_IRWXU);
    if (fd < 0) {
        printf(": failed to create file [%s]: %s \n", fname, ::strerror(errno));
        return false;
    }

    //LOG_INF("writing %d bytes to file [%s]\n", size, fname);
    while (written < size) {
        nw = ::write(fd,
                     buf + written,
                     size - written);
        if (nw < 0) {
            printf(": failed to write to file [%s]: %s\n", fname, ::strerror(errno));
            break;
        }
        written += nw;
        cnt++;
    }
    //LOG_INF("done writing %d bytes to file [%s] in %d passes\n", size, fname, cnt);
    ::close(fd);
    return true;
}



