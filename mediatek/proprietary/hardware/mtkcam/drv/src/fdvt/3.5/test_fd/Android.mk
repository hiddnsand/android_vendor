#
# fdtest
#
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

#
LOCAL_SRC_FILES := \
    main.cpp \
    test_fd.cpp \
	
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(TARGET_BOARD_PLATFORM)/drv

LOCAL_SHARED_LIBRARIES += \
    libmtkcam_fdvt \

# Imem
LOCAL_SHARED_LIBRARIES += \
	libcamdrv_imem
    
LOCAL_STATIC_LIBRARIES := \

#
LOCAL_WHOLE_STATIC_LIBRARIES := \

#
LOCAL_MODULE := FdTest
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_MULTILIB := 32
#
LOCAL_MODULE_TAGS := eng

#
LOCAL_PRELINK_MODULE := false

#-----------------------------------------------------------
LOCAL_CFLAGS += $(MTKCAM_CFLAGS)

#-----------------------------------------------------------
LOCAL_C_INCLUDES += $(MTKCAM_C_INCLUDES)

# End of common part ---------------------------------------
#
include $(MTK_EXECUTABLE)

#
include $(call all-makefiles-under,$(LOCAL_PATH))
