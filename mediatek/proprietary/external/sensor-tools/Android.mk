# Copyright 2006 The Android Open Source Project
###############################################################################
LOCAL_PATH:= $(call my-dir)
###############################################################################
include $(CLEAR_VARS)
LOCAL_SRC_FILES := libhwm.c
LOCAL_MODULE := libhwm
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_SHARED_LIBRARIES += libnvram liblog libfile_op
#bobule workaround pdk build error, needing review
#LOCAL_UNSTRIPPED_PATH := $(TARGET_ROOT_OUT_SBIN_UNSTRIPPED)
LOCAL_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_HEADER_LIBRARIES := \
	libnvram_headers \
	libfile_op_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/include
include $(MTK_SHARED_LIBRARY)
###############################################################################
include $(CLEAR_VARS)
LOCAL_SRC_FILES := libhwm.c
LOCAL_MODULE := libhwm_mtk
LOCAL_MODULE_OWNER := mtk
LOCAL_SHARED_LIBRARIES += libnvram_mtk liblog libfile_op_mtk
#bobule workaround pdk build error, needing review
#LOCAL_UNSTRIPPED_PATH := $(TARGET_ROOT_OUT_SBIN_UNSTRIPPED)
LOCAL_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_HEADER_LIBRARIES := \
	libnvram_headers \
	libfile_op_headers
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/include
include $(MTK_SHARED_LIBRARY)
###############################################################################

ifneq ($(BOARD_MTK_LIBSENSORS_NAME),)
ifneq ($(BOARD_MTK_LIB_SENSOR),)
ifneq ($(BOARD_MTK_LIB_SENSOR_NO),)
###############################################################################
include $(CLEAR_VARS)
LOCAL_SRC_FILES := cmdtool.c
LOCAL_MODULE := sensor_cmd
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR_OPTIONAL_EXECUTABLES)	#install to system/xbin
LOCAL_UNSTRIPPED_PATH := $(TARGET_ROOT_OUT_SBIN_UNSTRIPPED)
LOCAL_STATIC_LIBRARIES += libm
LOCAL_SHARED_LIBRARIES += libnvram libhwm libfile_op libc libcutils
include $(MTK_EXECUTABLE)
endif
endif
endif
