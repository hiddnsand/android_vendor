/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2011 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Mesh utility functions implementaion
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <algorithm>  /* for std::max() */

#include <a3m/fpmaths.h>  /* for Trig functions */
#include <a3m/indexbuffer.h>  /* for IndexBuffer */
#include <a3m/mesh.h>  /* for Mesh */
#include <a3m/meshutility.h>  /* Mesh utility API */
#include <a3m/vector2.h>  /* for Vector2f */
#include <a3m/vector3.h>  /* for Vector3f */
#include <a3m/vertexarray.h>  /* for VertexArray */
#include <a3m/vertexbuffer.h>  /* for VertexBuffer */

#include <a3m/log.h>  /* for Log */

namespace
{
  // Performs manual mirroring of negative texture coordinates, i.e. if the
  // texture coordinates are negative, they are adjusted to be in the range 0 to
  // 1.  This allows non-power-of-two textures and coordinates that do not cover
  // the entire texture to be mirrored.
  void scaleTextureCoordinates(a3m::Vector2f& uvStart, a3m::Vector2f& uvEnd)
  {
    for (A3M_UINT32 i = 0; i < 2; ++i)
    {
      // uvStart is always expected to be (0, 0)
      A3M_ASSERT(uvStart[i] == 0);

      // If negative, reverse the UV coordinates over the range 0 to 1
      if (uvEnd[i] < 0)
      {
        uvStart[i] = 1;
        uvEnd[i] = 1 + uvEnd[i];
      }
    }
  }

} // namespace

namespace a3m
{
  // \todo Add tangent attributes to meshes

  // All meshes are given unit dimensions for consistency, and so that the size
  // of a shape is equal to its scaling factor.  Rotation and offset are also
  // zero, as they may also be specified by applying transformations in the
  // graph.  UV coordinates can be scaled to allow textures to be flipped, or
  // repeated multiple times over the shape.  UV offset is not a parameter, as
  // it is unlikely to be required as a static mesh parameter, although it could
  // be added in the future if required.

  // The square mesh points along the postive z-axis, as this will be aligned
  // with the screen plane when no rotational transformations have been applied.
  // This is useful for creating GUI elements, and other screen-space entities.

  Mesh::Ptr createSquareMesh(MeshCache& meshCache, Vector2f const& uvScale)
  {
    // Construct vertex buffer

    A3M_FLOAT const normalArray[] = { 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 };

    a3m::VertexArray::Ptr positions(new a3m::VertexArray(4, 3,
                                    static_cast<A3M_FLOAT*>(0)));
    a3m::VertexArray::Ptr normals(new a3m::VertexArray(4, 3, normalArray));
    a3m::VertexArray::Ptr texcoords(new a3m::VertexArray(4, 2,
                                    static_cast<A3M_FLOAT*>(0)));

    A3M_FLOAT* positionData = positions->data<A3M_FLOAT>();
    A3M_FLOAT* texcoordData = texcoords->data<A3M_FLOAT>();

    // Four vertices
    positionData[0] = -0.5;
    positionData[1] = -0.5;
    positionData[2] =  0;
    positionData[3] =  0.5;
    positionData[4] = -0.5;
    positionData[5] =  0;
    positionData[6] =  0.5;
    positionData[7] =  0.5;
    positionData[8] =  0;
    positionData[9] = -0.5;
    positionData[10] = 0.5;
    positionData[11] = 0;

    // Adjust UV coords to account for negative scaling
    Vector2f uvStart(0, 0);
    Vector2f uvEnd = uvScale;
    scaleTextureCoordinates(uvStart, uvEnd);

    // Map scaled texture onto square
    texcoordData[0] = uvStart.x;
    texcoordData[1] = uvStart.y;
    texcoordData[2] = uvEnd.x;
    texcoordData[3] = uvStart.y;
    texcoordData[4] = uvEnd.x;
    texcoordData[5] = uvEnd.y;
    texcoordData[6] = uvStart.x;
    texcoordData[7] = uvEnd.y;

    a3m::VertexBuffer::Ptr vertices = meshCache.vertexBufferCache()->create();

    vertices->addAttrib(positions, "a_position");
    vertices->addAttrib(normals, "a_normal");
    vertices->addAttrib(texcoords, "a_uv0");

    vertices->commit();

    // Construct index buffer

    // Two triangles make up square
    A3M_UINT16 indexData[] = { 0, 1, 3, 1, 2, 3 };

    a3m::IndexBuffer::Ptr indices = meshCache.indexBufferCache()->create(
                                      IndexBuffer::PRIMITIVE_TRIANGLES, 6, indexData);

    indices->commit();

    // Create mesh

    MeshHeader header;
    header.boundingRadius = sqrt(2);

    // \todo Could cache the mesh by name (probably have to come up with some
    // naming convention for internally created assets that won't clash with
    // loaded assets), to avoid creating the meshes multiple times.
    Mesh::Ptr mesh = meshCache.create(header, indices, vertices);

    return mesh;
  }

  Mesh::Ptr createCubeMesh(MeshCache& meshCache, Vector2f const& uvScale)
  {
    // Construct vertex buffer

    A3M_UINT32 vertexCount = 24;

    a3m::VertexArray::Ptr positions(new a3m::VertexArray(vertexCount, 3,
                                    static_cast<A3M_FLOAT*>(0)));
    a3m::VertexArray::Ptr normals(new a3m::VertexArray(vertexCount, 3,
                                  static_cast<A3M_FLOAT*>(0)));
    a3m::VertexArray::Ptr texcoords(new a3m::VertexArray(vertexCount, 2,
                                    static_cast<A3M_FLOAT*>(0)));

    A3M_FLOAT* positionData = positions->data<A3M_FLOAT>();
    A3M_FLOAT* normalData = normals->data<A3M_FLOAT>();
    A3M_FLOAT* texcoordData = texcoords->data<A3M_FLOAT>();

    // Construct index buffer

    A3M_UINT32 const indexCount = 36;
    A3M_UINT16 indexData[indexCount];

    // Adjust UV coords to account for negative scaling
    Vector2f uvStart(0, 0);
    Vector2f uvEnd = uvScale;
    scaleTextureCoordinates(uvStart, uvEnd);

    // Indices to vertex and index data
    A3M_UINT32 vi = 0;
    A3M_UINT32 ti = 0;
    A3M_UINT32 ii = 0;
    A3M_UINT32 vc = 0;

    // Loop for each dimension
    for (A3M_UINT32 i = 0; i < 3; ++i)
    {
      // Loop for positive and negative sides of axes
      for (A3M_INT32 j = -1; j <= 1; j += 2)
      {
        // Rotate the dimensions to form each face
        A3M_UINT32 dim0 = i;
        A3M_UINT32 dim1 = (i + 1) % 3;
        A3M_UINT32 dim2 = (i + 2) % 3;

        // We flip two axes instead of just the one to maintain the winding
        // direction
        positionData[vi + dim0]     = -0.5f * j;
        positionData[vi + dim1]     = -0.5f;
        positionData[vi + dim2]     =  0.5f * j;
        positionData[vi + dim0 + 3] =  0.5f * j;
        positionData[vi + dim1 + 3] = -0.5f;
        positionData[vi + dim2 + 3] =  0.5f * j;
        positionData[vi + dim0 + 6] =  0.5f * j;
        positionData[vi + dim1 + 6] =  0.5f;
        positionData[vi + dim2 + 6] =  0.5f * j;
        positionData[vi + dim0 + 9] = -0.5f * j;
        positionData[vi + dim1 + 9] =  0.5f;
        positionData[vi + dim2 + 9] =  0.5f * j;

        // Vertex normals equal the face normals
        normalData[vi + dim0]     = 0;
        normalData[vi + dim1]     = 0;
        normalData[vi + dim2]     = static_cast<A3M_FLOAT>(j);
        normalData[vi + dim0 + 3] = 0;
        normalData[vi + dim1 + 3] = 0;
        normalData[vi + dim2 + 3] = static_cast<A3M_FLOAT>(j);
        normalData[vi + dim0 + 6] = 0;
        normalData[vi + dim1 + 6] = 0;
        normalData[vi + dim2 + 6] = static_cast<A3M_FLOAT>(j);
        normalData[vi + dim0 + 9] = 0;
        normalData[vi + dim1 + 9] = 0;
        normalData[vi + dim2 + 9] = static_cast<A3M_FLOAT>(j);

        // Map scaled texture onto square
        texcoordData[ti + 0] = uvStart.x;
        texcoordData[ti + 1] = uvStart.y;
        texcoordData[ti + 2] = uvEnd.x;
        texcoordData[ti + 3] = uvStart.y;
        texcoordData[ti + 4] = uvEnd.x;
        texcoordData[ti + 5] = uvEnd.y;
        texcoordData[ti + 6] = uvStart.x;
        texcoordData[ti + 7] = uvEnd.y;

        // Two triangles make up each face
        indexData[ii]     = vc + 0;
        indexData[ii + 1] = vc + 1;
        indexData[ii + 2] = vc + 3;
        indexData[ii + 3] = vc + 1;
        indexData[ii + 4] = vc + 2;
        indexData[ii + 5] = vc + 3;

        // Move to next face
        vi += 12;
        ti += 8;
        ii += 6;
        vc += 4;
      }
    }

    // Create and commit vertex buffer
    a3m::VertexBuffer::Ptr vertices = meshCache.vertexBufferCache()->create();

    vertices->addAttrib(positions, "a_position");
    vertices->addAttrib(normals, "a_normal");
    vertices->addAttrib(texcoords, "a_uv0");

    vertices->commit();

    // Create and commit index buffer
    a3m::IndexBuffer::Ptr indices = meshCache.indexBufferCache()->create(
                                      IndexBuffer::PRIMITIVE_TRIANGLES,
                                      indexCount, indexData);

    indices->commit();

    // Create mesh
    MeshHeader header;
    header.boundingRadius = sqrt(3);

    Mesh::Ptr mesh = meshCache.create(header, indices, vertices);

    return mesh;
  }

  // There are typically two types of sphere used in 3D graphics: regular
  // spheres and geospheres.  Regular spheres have longitudinal edges which
  // converge at a single point at each pole, and thus can have texture-mapping
  // and lighting artefacts where these edges form very narrow triangles.
  // Geospheres are regular geometric shapes made of repeating patterns of
  // tesselated triangles, and thus allow for more even texture mapping (when
  // using more advanced texture mapping techniques, such as cube-mapping).
  // However, geospheres have a defined number of discrete configurations,
  // whereas the resolution of a regular sphere can be continuously adjusted by
  // modifying number of segments and wedges.  This implementation using regular
  // spheres.
  Mesh::Ptr createSphereMesh(MeshCache& meshCache, A3M_UINT32 segmentCount,
                             A3M_UINT32 wedgeCount, Vector2f const& uvScale)
  {
    if (segmentCount < 2)
    {
      A3M_LOG_WARN( "Enforced minimum of 2 sphere segments. (was %d)",
                    segmentCount);
      segmentCount = 2;
    }

    if (wedgeCount < 2)
    {
      A3M_LOG_WARN( "Enforced minimun of 2 sphere wedges. (was %d)",
                    wedgeCount);
      wedgeCount = 2;
    }

    // Construct vertex buffer

    A3M_UINT32 vertexCount = (wedgeCount + 1) * (segmentCount + 1);

    a3m::VertexArray::Ptr positions(new a3m::VertexArray(vertexCount, 3,
                                    static_cast<A3M_FLOAT*>(0)));
    a3m::VertexArray::Ptr normals(new a3m::VertexArray(vertexCount, 3,
                                  static_cast<A3M_FLOAT*>(0)));
    a3m::VertexArray::Ptr texcoords(new a3m::VertexArray(vertexCount, 2,
                                    static_cast<A3M_FLOAT*>(0)));

    A3M_FLOAT* positionData = positions->data<A3M_FLOAT>();
    A3M_FLOAT* normalData = normals->data<A3M_FLOAT>();
    A3M_FLOAT* texcoordData = texcoords->data<A3M_FLOAT>();

    // Adjust UV coords to account for negative scaling
    Vector2f uvStart(0, 0);
    Vector2f uvEnd = uvScale;
    scaleTextureCoordinates(uvStart, uvEnd);
    Vector2f uvMiddle = (uvStart + uvEnd) / 2.0f;
    Vector2f uvVector = uvEnd - uvStart;

    // Vertex and index array indices
    A3M_UINT32 vi = 0;
    A3M_UINT32 ti = 0;
    A3M_UINT32 ii = 0;

    // Construct index buffer.  Index buffer must be heap-allocated as its size
    // is not known at compile time, even though g++ doesn't prohibit allocation
    // on the stack.
    A3M_UINT32 indexCount = 6 * wedgeCount * segmentCount;
    A3M_UINT16* indexData = new A3M_UINT16[indexCount];

    for (A3M_UINT32 segment = 0; segment < segmentCount + 1; ++segment)
    {
      // To allow spherical mapping, the sphere must not share vertices at the
      // UV seam, so we define vertices twice at 0/360 degrees.
      for (A3M_UINT32 wedge = 0; wedge < wedgeCount + 1; ++wedge)
      {
        // Segment angle is zero at south pole, and pi at north pole
        // Wedge angle is zero at z-axis maximum, and increases anti-clockwise
        Anglef segmentAngle = radians(static_cast<A3M_FLOAT>(segment) /
                                      segmentCount * FLOAT_PI);
        Anglef wedgeAngle = radians(static_cast<A3M_FLOAT>(wedge) /
                                    wedgeCount * FLOAT_TWO_PI);

        // One vertex is defined for each segment-wedge pair: the bottom-right
        // hand vertex
        normalData[vi]     = sin(segmentAngle) * -sin(wedgeAngle);
        normalData[vi + 1] = -cos(segmentAngle);
        normalData[vi + 2] = sin(segmentAngle) * cos(wedgeAngle);

        positionData[vi]     = normalData[vi]     * 0.5f;
        positionData[vi + 1] = normalData[vi + 1] * 0.5f;
        positionData[vi + 2] = normalData[vi + 2] * 0.5f;

        // Really simple spherical UV mapping (mapping latitude and longitude
        // directly onto the U and V coordinates) with scale factor.
        texcoordData[ti]     = uvStart.x +
                               (getRadians(wedgeAngle) / FLOAT_TWO_PI * uvVector.x);
        texcoordData[ti + 1] = uvStart.y +
                               (getRadians(segmentAngle) / FLOAT_PI * uvVector.y);

        // Move on to next vertex
        vi += 3;
        ti += 2;

        // Define indices for all segments and wedges
        if (segment < segmentCount && wedge < wedgeCount)
        {
          // Indices to corners of quad
          // Left edge is this wedge, right edge is next wedge
          // Bottom edge is this segment, top edge is next segment
          A3M_UINT32 bl =  wedge      +  segment      * (wedgeCount + 1);
          A3M_UINT32 tl =  wedge      + (segment + 1) * (wedgeCount + 1);
          A3M_UINT32 br = (wedge + 1) +  segment      * (wedgeCount + 1);
          A3M_UINT32 tr = (wedge + 1) + (segment + 1) * (wedgeCount + 1);

          // Check that we have not exceeded the vertex buffer.
          A3M_ASSERT(bl < vertexCount * 3);
          A3M_ASSERT(tl < vertexCount * 3);
          A3M_ASSERT(br < vertexCount * 3);
          A3M_ASSERT(tr < vertexCount * 3);

          indexData[ii]     = tr;
          indexData[ii + 1] = br;
          indexData[ii + 2] = bl;
          ii += 3;

          indexData[ii]     = bl;
          indexData[ii + 1] = tl;
          indexData[ii + 2] = tr;
          ii += 3;
        }
      }
    }

    // Check that we have exactly filled the buffers.
    A3M_ASSERT(vi == vertexCount * 3);
    A3M_ASSERT(ti == vertexCount * 2);
    A3M_ASSERT(ii == indexCount);

    // Create and commit vertex buffer
    a3m::VertexBuffer::Ptr vertices = meshCache.vertexBufferCache()->create();

    vertices->addAttrib(positions, "a_position");
    vertices->addAttrib(normals, "a_normal");
    vertices->addAttrib(texcoords, "a_uv0");

    vertices->commit();

    // Create and commit index buffer
    a3m::IndexBuffer::Ptr indices = meshCache.indexBufferCache()->create(
                                      IndexBuffer::PRIMITIVE_TRIANGLES,
                                      indexCount, indexData);

    delete[] indexData;
    indices->commit();

    // Create mesh
    MeshHeader header;
    header.boundingRadius = 0.5f;

    Mesh::Ptr mesh = meshCache.create(header, indices, vertices);

    return mesh;
  }

} /* namespace a3m */

