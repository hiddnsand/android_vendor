#ifndef HWC_PLATFORM_H_
#define HWC_PLATFORM_H_

#include <utils/Singleton.h>

using namespace android;

struct hwc_layer_1;
struct hwc_buffer_info_t;

// ---------------------------------------------------------------------------

enum OVL_GEN {
    OVL_GEN_6589 = 0x0001,
    OVL_GEN_6582 = 0x0002,
    OVL_GEN_6592 = 0x0004,
    OVL_GEN_6595 = 0x0008,
};

enum MDP_GEN {
    MDP_GEN_6589 = 0x0001,
    MDP_GEN_6582 = 0x0002,
    MDP_GEN_6595 = 0x0004,
};

enum PLATFORM_INFO {
    PLATFORM_NOT_DEFINE = 0,

    PLATFORM_MT6589 = OVL_GEN_6589 | (MDP_GEN_6589 << 16),
    PLATFORM_MT8135 = OVL_GEN_6589 | (MDP_GEN_6589 << 16),

    PLATFORM_MT6572 = OVL_GEN_6582 | (MDP_GEN_6582 << 16),
    PLATFORM_MT6582 = OVL_GEN_6582 | (MDP_GEN_6582 << 16),
    PLATFORM_MT8127 = OVL_GEN_6582 | (MDP_GEN_6582 << 16),

    PLATFORM_MT6571 = OVL_GEN_6592 | (MDP_GEN_6582 << 16),
    PLATFORM_MT6592 = OVL_GEN_6592 | (MDP_GEN_6582 << 16),

    PLATFORM_MT6595 = OVL_GEN_6595 | (MDP_GEN_6595 << 16),
};

class DevicePlatform : public Singleton<DevicePlatform>
{
public:
    // COND_NOT_ALIGN:
    //   pitch of buffers do not align any value
    // COND_ALIGNED:
    //   pitch of buffers is aligned
    // COND_ALIGNED_SHIFT:
    //   pitch of buffers is aligned, and start address of each line is shifted
    enum ALIGN_CONDITION {
        COND_NOT_ALIGN     = 0,
        COND_ALIGNED       = 1,
        COND_ALIGNED_SHIFT = 2
    };

    DevicePlatform();
    ~DevicePlatform() { };

    // initOverlay() is used to init overlay related setting
    void initOverlay();

    // isUILayerValid is ued to verify
    // if ui layer could be handled by hwcomposer
    bool isUILayerValid(struct hwc_layer_1* layer,
            struct hwc_buffer_info_t* info);

    // isMMLayerValid is used to verify
    // if mm layer could be handled by hwcomposer
    bool isMMLayerValid(struct hwc_layer_1* layer,
            struct hwc_buffer_info_t* info);

    // For 82 ovl hw limitation, pitch of buffers must allign 128 bytes.
    int computePitch(const int& pitch_bytes, const int& width_bytes, int* status);

    struct PlatformConfig
    {
        PlatformConfig()
            : platform(PLATFORM_NOT_DEFINE)
            , compose_level(COMPOSE_DISABLE_ALL)
            , client_id(-1)
            , shift_bytes(0)
        { }

        // platform define related hw family, includes ovl and mdp engine
        int platform;

        // compose_level defines default compose level
        int compose_level;

        // client_id identifies m4u client id
        int client_id;

        // [NOTE] for mt6582 hw limitation
        // should be removed after mt6582 phasing out
        int shift_bytes;
    };
    static PlatformConfig m_config;
};

#endif // HWC_PLATFORM_H_
