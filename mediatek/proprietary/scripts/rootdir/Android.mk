ifneq ($(MTK_BOARD_ROOT_EXTRA_FOLDERS),)

LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := gen_mount_point_for_ab
LOCAL_POST_INSTALL_CMD := $(hide) mkdir -p $(addprefix $(TARGET_OUT_VENDOR)/,$(MTK_BOARD_ROOT_EXTRA_FOLDERS))
include $(BUILD_PHONY_PACKAGE)

endif