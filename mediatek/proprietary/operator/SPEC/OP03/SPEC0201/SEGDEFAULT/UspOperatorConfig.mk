
## This is OP03 operator config file

# Set for carrier express features in this File for OP03

ifeq ($(strip $(MTK_CIP_SUPPORT)), yes)
USP_PATH_ROOT := /custom
else
USP_PATH_ROOT := /system
endif

USP_OPERATOR_APK_PATH := $(USP_PATH_ROOT)/plugin/Rcse/ \
                $(USP_PATH_ROOT)/app/RcsStack/

USP_OPERATOR_PACKAGES := com.mediatek.op03.dialer \
                com.mediatek.op03.phone \
                com.mediatek.op03.settings \
                com.mediatek.op03.settingsProvider \
                com.mediatek.browser.plugin

USP_OPERATOR_FEATURES := MTK_EPDG_CIP_SUPPORT=yes \
                MTK_WFC_SUPPORT=yes \
                MTK_VOLTE_SUPPORT=yes \
                MTK_VILTE_SUPPORT=no \
                MTK_IMS_SUPPORT=yes \
                MTK_FLIGHT_MODE_POWER_OFF_MD=yes \
                MTK_RCS_UA_SUPPORT=yes




                