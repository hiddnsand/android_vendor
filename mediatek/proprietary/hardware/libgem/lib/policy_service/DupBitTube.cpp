/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "dfps/DupBitTube.h"

#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <fcntl.h>
#include <unistd.h>

#include <utils/Errors.h>

#include <binder/Parcel.h>

namespace android {
// ----------------------------------------------------------------------------

// Socket buffer size.  The default is typically about 128KB, which is much larger than
// we really need.  So we make it smaller.
static const size_t DEFAULT_SOCKET_BUFFER_SIZE = 4 * 1024;


DupBitTube::DupBitTube()
    : mSendFd(-1), mReceiveFd(-1)
{
    init(DEFAULT_SOCKET_BUFFER_SIZE, DEFAULT_SOCKET_BUFFER_SIZE);
}

DupBitTube::DupBitTube(size_t bufsize)
    : mSendFd(-1), mReceiveFd(-1)
{
    init(bufsize, bufsize);
}

DupBitTube::DupBitTube(const Parcel& data)
    : mSendFd(-1), mReceiveFd(-1)
{
    int fd = data.readFileDescriptor();
    if (fd >= 0) {
        mReceiveFd = dup(fd);
        if (mReceiveFd < 0) {
            mReceiveFd = -errno;
            ALOGE("DupBitTube(Parcel): can't dup filedescriptor (%s)",
                    strerror(-mReceiveFd));
        }
    } else {
            ALOGE("DupBitTube(Parcel): get an invalid filedescriptor (%d)",
                    fd);
    }
}

DupBitTube::~DupBitTube()
{
    if (mSendFd >= 0)
        close(mSendFd);

    if (mReceiveFd >= 0)
        close(mReceiveFd);
}

void DupBitTube::init(size_t rcvbuf, size_t sndbuf) {
    int sockets[2];
    if (socketpair(AF_UNIX, SOCK_SEQPACKET, 0, sockets) == 0) {
        size_t size = DEFAULT_SOCKET_BUFFER_SIZE;
        int res  = 0;
        res = setsockopt(sockets[0], SOL_SOCKET, SO_RCVBUF, &rcvbuf, sizeof(rcvbuf));
        if (res < 0) {
            ALOGE("DupBitTube: failed to set the receive buffer");
        }
        res = setsockopt(sockets[1], SOL_SOCKET, SO_SNDBUF, &sndbuf, sizeof(sndbuf));
        if (res < 0) {
            ALOGE("DupBitTube: failed to set the send buffer");
        }
        // sine we don't use the "return channel", we keep it small...
        res = setsockopt(sockets[0], SOL_SOCKET, SO_SNDBUF, &size, sizeof(size));
        if (res < 0) {
            ALOGW("DupBitTube: failed to set the size of send buffer");
        }
        res = setsockopt(sockets[1], SOL_SOCKET, SO_RCVBUF, &size, sizeof(size));
        if (res < 0) {
            ALOGW("DupBitTube: failed to set the size of receive buffer");
        }
        res = fcntl(sockets[0], F_SETFL, O_NONBLOCK);
        if (res == -1) {
            ALOGE("DupBitTube: failed to manipulate receive filedescriptor");
        }
        res = fcntl(sockets[1], F_SETFL, O_NONBLOCK);
        if (res == -1) {
            ALOGE("DupBitTube: failed to manipulate send filedescriptor");
        }
        mReceiveFd = sockets[0];
        mSendFd = sockets[1];
    } else {
        mReceiveFd = -errno;
        ALOGE("DupBitTube: pipe creation failed (%s)", strerror(-mReceiveFd));
    }
}

status_t DupBitTube::initCheck() const
{
    if (mReceiveFd < 0) {
        return status_t(mReceiveFd);
    }
    return NO_ERROR;
}

int DupBitTube::getFd() const
{
    return mReceiveFd;
}

int DupBitTube::getSendFd() const
{
    return mSendFd;
}

ssize_t DupBitTube::write(void const* vaddr, size_t size)
{
    ssize_t err, len;
    do {
        len = ::send(mSendFd, vaddr, size, MSG_DONTWAIT | MSG_NOSIGNAL);
        // cannot return less than size, since we're using SOCK_SEQPACKET
        err = len < 0 ? errno : 0;
    } while (err == EINTR);
    return err == 0 ? len : -err;
}

ssize_t DupBitTube::read(void* vaddr, size_t size)
{
    ssize_t err, len;
    do {
        len = ::recv(mReceiveFd, vaddr, size, MSG_DONTWAIT);
        err = len < 0 ? errno : 0;
    } while (err == EINTR);
    if (err == EAGAIN || err == EWOULDBLOCK) {
        // EAGAIN means that we have non-blocking I/O but there was
        // no data to be read. Nothing the client should care about.
        return 0;
    }
    return err == 0 ? len : -err;
}

status_t DupBitTube::writeToParcel(Parcel* reply) const
{
    if (mReceiveFd < 0)
        return -EINVAL;

    status_t result = reply->writeDupFileDescriptor(mReceiveFd);
    close(mReceiveFd);
    mReceiveFd = -1;
    return result;
}


ssize_t DupBitTube::sendObjects(const sp<DupBitTube>& tube,
        void const* events, size_t count, size_t objSize)
{
    const char* vaddr = reinterpret_cast<const char*>(events);
    ssize_t size = tube->write(vaddr, count*objSize);

    // should never happen because of SOCK_SEQPACKET
    LOG_ALWAYS_FATAL_IF((size >= 0) && (size % static_cast<ssize_t>(objSize)),
            "DupBitTube::sendObjects(count=%zu, size=%zu), res=%zd (partial events were sent!)",
            count, objSize, size);

    //ALOGE_IF(size<0, "error %d sending %d events", size, count);
    return size < 0 ? size : size / static_cast<ssize_t>(objSize);
}

ssize_t DupBitTube::recvObjects(const sp<DupBitTube>& tube,
        void* events, size_t count, size_t objSize)
{
    char* vaddr = reinterpret_cast<char*>(events);
    ssize_t size = tube->read(vaddr, count*objSize);

    // should never happen because of SOCK_SEQPACKET
    LOG_ALWAYS_FATAL_IF((size >= 0) && (size % static_cast<ssize_t>(objSize)),
            "DupBitTube::recvObjects(count=%zu, size=%zu), res=%zd (partial events were received!)",
            count, objSize, size);

    //ALOGE_IF(size<0, "error %d receiving %d events", size, count);
    return size < 0 ? size : size / static_cast<ssize_t>(objSize);
}

// ----------------------------------------------------------------------------
}; // namespace android
