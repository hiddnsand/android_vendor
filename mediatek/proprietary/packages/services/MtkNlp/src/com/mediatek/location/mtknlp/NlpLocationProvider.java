/*
* Copyright (C) 2015 MediaTek Inc.
* Modification based on code covered by the mentioned copyright
* and/or permission notice(s).
*/
/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mediatek.location.mtknlp;


import java.io.FileDescriptor;
import java.io.PrintWriter;

import com.android.internal.location.ProviderProperties;
import com.android.internal.location.ILocationProvider;
import com.android.internal.location.ProviderRequest;

import com.android.location.provider.LocationProviderBase;
import com.android.location.provider.ProviderPropertiesUnbundled;
import com.android.location.provider.LocationRequestUnbundled;
import com.android.location.provider.ProviderRequestUnbundled;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.location.LocationRequest;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.WorkSource;
import android.util.Log;

import com.mediatek.location.mtknlp.NlpSwitcher;
import java.util.ArrayList;

public class NlpLocationProvider extends LocationProviderBase {
    private static final String TAG = "NlpLocationProvider";
    private static final String GMS_PACKAGE_NAME = "com.google.android.gms";
    public static final String NETWORK_LOCATION_SERVICE_ACTION =
            "com.android.location.service.v3.NetworkLocationProvider";
    private static final String PROP_NLP_PACKAGE = "persist.sys.nlp.package";

    private static ProviderPropertiesUnbundled PROPERTIES = ProviderPropertiesUnbundled.create(
            false, false, false, false, true, true, true, Criteria.POWER_LOW,
            Criteria.ACCURACY_FINE);

    private static final int MSG_ENABLE = 1;
    private static final int MSG_DISABLE = 2;
    private static final int MSG_SET_REQUEST = 3;
    private static final int MSG_CONNECTED = 4;
    private static final int MSG_REPORT_LAST_LOC = 5;
    private static final int MSG_CLEAR_LAST_LOC = 6;
    private static final int MSG_REBIND_NLP = 7;

    private static final int DELAY_REPORT_LAST_LOCATION = (1*1000); //ms
    private static final int LAST_LOCATION_EXPIRED_TIMEOUT = (10*60*1000); //ms

    private final Context mContext;
    private PackageManager mPackageManager;
    private NlpServiceWatcher mNlpServiceWatcher;
    private boolean mEnabled = false;
    private boolean mDoStopNlp = false;
    private boolean mDelay2ReportLastLocation = false;
    private ProviderRequest mRequest = null;
    private WorkSource mWorkSource;
    private String mVendorPackageName;
    private LocationManager mLocationManager;
    private Location mLastLocation;
    private HandlerThread mHandlerThread;
    private LocationWorkerHandler mHandler;

    private static class RequestWrapper {
        public ProviderRequestUnbundled request;
        public WorkSource source;
        public RequestWrapper(ProviderRequestUnbundled request, WorkSource source) {
            this.request = request;
            this.source = source;
        }
    }

    public NlpLocationProvider(Context context, String vendorPackageName) {
        super(TAG, PROPERTIES);
        mContext = context;
        mVendorPackageName = vendorPackageName;
        mPackageManager = mContext.getPackageManager();

        mHandlerThread = new HandlerThread("MtkNlpThread");
        mHandlerThread.start();
        mHandler = new LocationWorkerHandler(mHandlerThread.getLooper());
        mNlpServiceWatcher = new NlpServiceWatcher(mContext, "MtkNlp",
                NETWORK_LOCATION_SERVICE_ACTION,  // action
                GMS_PACKAGE_NAME, mVendorPackageName, GMS_PACKAGE_NAME, mNewServiceWork, mHandler);
        mNlpServiceWatcher.start();

        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        mLocationManager.requestLocationUpdates(
                LocationManager.PASSIVE_PROVIDER, 0, 0,
                mPassiveLocationListener);
    }

    private void deInit() {
        mNlpServiceWatcher.stop();
        mLocationManager.removeUpdates(mPassiveLocationListener);
    }

    private class LocationWorkerHandler extends Handler {
        public LocationWorkerHandler(Looper looper) {
            super(looper, null, true);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_ENABLE:
                    handleEnabled();
                    break;
                case MSG_DISABLE:
                    handleDisabled();
                    if (mDoStopNlp) {
                        deInit();
                        mDoStopNlp = false;
                    }
                    break;
                case MSG_SET_REQUEST: {
                    RequestWrapper wrapper = (RequestWrapper) msg.obj;
                    handleSetRequest(wrapper.request, wrapper.source);
                    break;
                }
                case MSG_CONNECTED:
                    handleConnected();
                    break;
                case MSG_REPORT_LAST_LOC:
                    reportLastLocation();
                    break;
                case MSG_CLEAR_LAST_LOC:
                    clearLastLocation();
                    break;
                case MSG_REBIND_NLP:
                    handleRebindNlp(msg.arg1 == 1);
                    break;
            }
        }
    };

    @Override
    public void onEnable() {
        mHandler.sendEmptyMessage(MSG_ENABLE);
    }

    private void handleEnabled() {
        Log.i(TAG, "handleEnabled() mEnabled=" + mEnabled);
        mEnabled = true;
        ILocationProvider service = getService();
        if (service == null) {
            Log.i(TAG, "handleEnabled() service is null");
            return;
        }

        try {
            service.enable();
        } catch (RemoteException e) {
            Log.w(TAG, e);
        } catch (Exception e) {
            // never let remote service crash system server
            Log.e(TAG, "Exception from " + mNlpServiceWatcher.getBestPackageName(), e);
        }
    }

    @Override
    public void onDisable() {
        mHandler.sendEmptyMessage(MSG_DISABLE);
    }

    private void handleDisabled() {
        Log.i(TAG, "handleDisabled() mEnabled=" + mEnabled);
        mEnabled = false;
        ILocationProvider service = getService();
        if (service == null) {
            Log.i(TAG, "handleDisabled() service is null");
            return;
        }

        try {
            service.disable();
        } catch (RemoteException e) {
            Log.w(TAG, e);
        } catch (Exception e) {
            // never let remote service crash system server
            Log.e(TAG, "Exception from " + mNlpServiceWatcher.getBestPackageName(), e);
        }
    }

    @Override
    public void onSetRequest(ProviderRequestUnbundled requestUnbundled, WorkSource source) {
        mHandler.obtainMessage(MSG_SET_REQUEST, new RequestWrapper(requestUnbundled, source)).sendToTarget();
    }

    private void handleSetRequest(ProviderRequestUnbundled requestUnbundled, WorkSource source) {
        Log.i(TAG, "handleSetRequest() request=" + requestUnbundled);
        ProviderRequest tmpRequest = mRequest;
        ProviderRequest providerRequest = new ProviderRequest();

        for (LocationRequestUnbundled locRequestUnbundled : requestUnbundled.getLocationRequests()) {
            LocationRequest locationRequest = LocationRequest.createFromDeprecatedProvider(
                    LocationManager.NETWORK_PROVIDER,
                    locRequestUnbundled.getInterval(),
                    locRequestUnbundled.getSmallestDisplacement(),
                    false);
            providerRequest.locationRequests.add(locationRequest);
            if (locationRequest.getInterval() < providerRequest.interval) {
                providerRequest.reportLocation = true;
                providerRequest.interval = locationRequest.getInterval();
            }
        }
        mRequest = providerRequest;
        mWorkSource = source;

        ILocationProvider service = getService();
        if (service == null) {
            Log.i(TAG, "handleSetRequest() service is null");
            return;
        }

        try {
            service.setRequest(providerRequest, source);
        } catch (RemoteException e) {
            Log.w(TAG, e);
        } catch (Exception e) {
            // never let remote service crash system server
            Log.e(TAG, "Exception from " + mNlpServiceWatcher.getBestPackageName(), e);
        }

        if (mRequest.reportLocation) {
            if (!mDelay2ReportLastLocation) {
                sendCommandDelayed(MSG_REPORT_LAST_LOC, DELAY_REPORT_LAST_LOCATION);
                mDelay2ReportLastLocation = true;
            }
        } else {
            if (mDelay2ReportLastLocation) {
                mHandler.removeMessages(MSG_REPORT_LAST_LOC);
                mDelay2ReportLastLocation = false;
            }
        }
    }

    @Override
    public void onDump(FileDescriptor fd, PrintWriter pw, String[] args) {
    }

    @Override
    public int onGetStatus(Bundle extras) {
        return LocationProvider.AVAILABLE;
    }

    @Override
    public long onGetStatusUpdateTime() {
        return 0;
    }

    /**
     * Work to apply current state to a newly connected provider.
     * Remember we can switch the service that implements a providers
     * at run-time, so need to apply current state.
     */
    private Runnable mNewServiceWork = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "applying state to connected service");
            mHandler.sendEmptyMessage(MSG_CONNECTED);
        }
    };

    private void handleConnected() {
        boolean enabled;
        ProviderProperties properties = null;
        ProviderRequest request;
        WorkSource source;
        ILocationProvider service;
        String packageName;

        enabled = mEnabled;
        request = mRequest;
        source = mWorkSource;
        service = getService();
        packageName = getNetworkProviderPackage();

        if (service == null) return;

        try {
            // load properties from provider
            properties = service.getProperties();
            if (properties == null) {
                Log.e(TAG, packageName + " has invalid locatino provider properties");
            }

            Log.e(TAG, "handleConnected properties: " + properties +
                    " enabled=" + enabled +
                    " request=" + request);

            SystemProperties.set(PROP_NLP_PACKAGE, packageName);
            if (!GMS_PACKAGE_NAME.equals(packageName)) {
                grantPermissions(packageName);
            }

            // apply current state to new service
            if (enabled) {
                service.enable();
                if (request != null) {
                    service.setRequest(request, source);
                    sendCommandDelayed(MSG_REPORT_LAST_LOC, DELAY_REPORT_LAST_LOCATION);
                    mDelay2ReportLastLocation = true;
                }
            }
        } catch (RemoteException e) {
            Log.e(TAG, "handleConnected RemoteException from " + packageName);
            Log.w(TAG, e);
        } catch (Exception e) {
            // never let remote service crash system server
            Log.e(TAG, "Exception from " + packageName, e);
        }

        //mProperties = properties;
    }

    public void onStop() {
        mDoStopNlp = true;
        onDisable();
    }

    public void reBindNlp(boolean bindGmsPackage) {
        mHandler.obtainMessage(MSG_REBIND_NLP, bindGmsPackage ? 1 : 0, 0, null).sendToTarget();
    }

    private void handleRebindNlp(boolean bindGmsPackage) {
        Log.d(TAG, "reBindNetworkProviderLock bindGmsPackage: " + bindGmsPackage);

        ILocationProvider service = getService();
        if (service != null) {
            try {
                service.disable();
            } catch (RemoteException e) {
                Log.w(TAG, e);
            } catch (Exception e) {
                // never let remote service crash system server
                Log.e(TAG, "Exception from " + mNlpServiceWatcher.getBestPackageName(), e);
            }
            Log.d(TAG, "bound NLP package name: " + getNetworkProviderPackage());
        } else {
            Log.d(TAG, "currently there is no NLP provided.");
            return;
        }

        //unbind previous network provider
        if (mNlpServiceWatcher != null) {
            mNlpServiceWatcher.stop();
        }

        // try to bind to new network provider
        String preferPackageName;
        if (bindGmsPackage) {
            preferPackageName = GMS_PACKAGE_NAME;
        } else {
            preferPackageName = mVendorPackageName;
        }

        // bind to network provider
        mNlpServiceWatcher = new NlpServiceWatcher(mContext, "MtkNlp",
            NETWORK_LOCATION_SERVICE_ACTION,  // action
            GMS_PACKAGE_NAME,
            mVendorPackageName,
            preferPackageName,
            mNewServiceWork,
            mHandler);

        if (mNlpServiceWatcher.start()) {
            Log.d(TAG, "Successfully bind package:" + getNetworkProviderPackage());
        } else {
            Log.d(TAG, "Failed to bind specified package service");
        }
    }

    private ILocationProvider getService() {
        return ILocationProvider.Stub.asInterface(mNlpServiceWatcher.getBinder());
    }

    public String getNetworkProviderPackage() {
        return mNlpServiceWatcher.getBestPackageName();
    }

    private LocationListener mPassiveLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            if (!LocationManager.NETWORK_PROVIDER.equals(location.getProvider())) {
                return;
            }
            mLastLocation = location;

            // reset the timer to clear last location
            mHandler.removeMessages(MSG_CLEAR_LAST_LOC);
            sendCommandDelayed(MSG_CLEAR_LAST_LOC, LAST_LOCATION_EXPIRED_TIMEOUT);
            if (mDelay2ReportLastLocation) {
                // avoid reporting cache location
                mHandler.removeMessages(MSG_REPORT_LAST_LOC);
                mDelay2ReportLastLocation = false;
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    private void sendCommandDelayed(int cmd, long delayMs) {
        Message msg = Message.obtain();
        msg.what = cmd;
        mHandler.sendMessageDelayed(msg, delayMs);
    }

    private void clearLastLocation() {
        Log.d(TAG, "clearLastLocation");
        mLastLocation = null;
    }

    private void reportLastLocation() {
        Log.d(TAG, "reportLastLocation");
        if (mRequest != null && mRequest.reportLocation) {
            if (mLastLocation != null) {
                reportLocation(mLastLocation);
            }
        }
        mDelay2ReportLastLocation = false;
    }

    private void grantPermissions(String packageName) {
        Log.d(TAG, "GrantRuntimePermission package: " + packageName);
        grantPermission(packageName, android.Manifest.permission.READ_PHONE_STATE);
        grantPermission(packageName, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        grantPermission(packageName, android.Manifest.permission.ACCESS_FINE_LOCATION);
        grantPermission(packageName, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        grantPermission(packageName, android.Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    private void grantPermission(String packageName, String permission) {
        if (packageName != null) {
            try {
                mPackageManager.grantRuntimePermission(packageName, permission,
                        new UserHandle(UserHandle.USER_SYSTEM));
            } catch (IllegalArgumentException e) {
                Log.w(TAG, "GrantRuntimePermission IllegalArgumentException: " + permission);
            } catch (SecurityException e) {
                Log.w(TAG, "GrantRuntimePermission SecurityException: " + permission);
            } catch (Exception e) {
                Log.w(TAG, "GrantRuntimePermission Exception: " + permission);
            }
        }
    }
}
