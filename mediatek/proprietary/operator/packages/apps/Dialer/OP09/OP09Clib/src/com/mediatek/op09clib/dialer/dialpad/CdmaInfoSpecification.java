package com.mediatek.op09clib.dialer.dialpad;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.os.Environment;
import android.os.StatFs;
import android.os.SystemProperties;

import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
//import android.telephony.CellLocation;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.internal.telephony.GsmCdmaPhone;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.uicc.UiccController;

import com.mediatek.custom.CustomProperties;
import com.mediatek.op09clib.dialer.R;
import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.IllegalArgumentException;
import java.util.concurrent.Executors;
import java.util.HashMap;
import java.util.Set;

/**
 * implement display for version info plugin for op09.
 */
public class CdmaInfoSpecification extends PreferenceActivity {

    private static final String TAG = "CdmaInfoSpecification";
    private static final String PACKAGE_NAME = "com.mediatek.sysinfo";
    private static final String FILENAME_MSV = "/sys/board_properties/soc/msv";

    private static final String KEY_PRODUCT_MODEL = "product_model";
    private static final String KEY_HARDWARE_VERSION = "hardware_version";
    private static final String KEY_SOFTWARE_VERSION = "software_version";

    public static final String KEY_CDMA_INFO = "cdma_info";
    public static final String KEY_PRL_VERSION = "prl_version";
    public static final String KEY_SID = "sid";
    public static final String KEY_NID = "nid";
    public static final String KEY_BASE_ID = "base_id";

    public static final String KEY_MEID = "meid";
    public static final String KEY_ESN = "esn";
    public static final String KEY_IMEI_1 = "imei1";
    public static final String KEY_IMEI_2 = "imei2";

    public static final String KEY_ICCID_1 = "iccid1";
    public static final String KEY_ICCID_2 = "iccid2";

    public static final String KEY_CDMA_IMSI = "cdma_imsi";
    public static final String KEY_CDMA_IMSI_1 = "cdma_imsi1";
    public static final String KEY_CDMA_IMSI_2 = "cdma_imsi2";

    public static final String KEY_LTE_IMSI = "imsi_lte";
    public static final String KEY_LTE_IMSI_1 = "imsi_lte1";
    public static final String KEY_LTE_IMSI_2 = "imsi_lte2";

    public static final String KEY_NETWORK_1 = "network1";
    public static final String KEY_NETWORK_2 = "network2";

    public static final String KEY_ANDROID_VERSION = "android";
    public static final String KEY_STORAGE = "storage";

    public static final String KEY_OPERATOR_1 = "operator1";
    public static final String KEY_OPERATOR_2 = "operator2";

    public static final String KEY_UIM_ID = "uim_id";
    public static final String KEY_SUB_ID = "subid";

    public static final String SOFTWARE_VERSION_DEFAULT = "MT6735.P0";
    public static final String HARDWARE_DEFAULT = "V1";

    private static final int SLOT1 = 0;
    private static final int SLOT2 = 1;

    private static final String PREF_DEVINFO_TAG = "pref_devinfo";
    private static final String PREF_IMEI_ITEM1 = "pref_imei_item1";
    private static final String PREF_IMEI_ITEM2 = "pref_imei_item2";
    public  static final String PREF_IMEI_MEID = "pref_imei_meid";

    private TelephonyManager mTelephonyManager;
    private boolean mFlightMode;
    private boolean mSlotSwitch = false;
    private boolean mSinglePhone = false;

    private static final int SIM_FILE_ID = 28465;
    private static final int SIM_CMD_ID = 176;
    private static final String RUIM_PATH = "3F007F25";
    private static final String CSIM_PATH = "3F007FFF";

    private static final int     LOG_LENGTH = 5;
    private static final boolean DEBUG = ("eng".equals(Build.TYPE) ||
                                        "userdebug".equals(Build.TYPE));


    private HashMap<String, String> mInfoMap = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log("onCreate()");
        //Init all information
        mTelephonyManager = TelephonyManager.from(this);

        initDeviceInfo();
        addPreferencesFromResource(R.xml.cdma_info_specifications);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mFlightMode = Settings.Global.getInt(this.getContentResolver(),
                                    Settings.Global.AIRPLANE_MODE_ON, 0) > 0;
        log("onResume(), mFlightMode = " + mFlightMode);
        initSimInfo();
        setDeviceValuesToPreferences();
        setInfoToPreference();
    }
    /**
     * set the info from phone to preference.
     *
     * @return void.
     */
    private void setDeviceValuesToPreferences() {
        log("setPhoneValuesToPreferences()");
        PreferenceScreen parent = (PreferenceScreen) getPreferenceScreen();
        Preference preference = parent.findPreference(KEY_PRODUCT_MODEL);
        if (null != preference) {
            preference.setSummary(mInfoMap.get(KEY_PRODUCT_MODEL));
        }

        preference = parent.findPreference(KEY_HARDWARE_VERSION);
        if (null != preference) {
            preference.setSummary(mInfoMap.get(KEY_HARDWARE_VERSION));
        }
        preference = parent.findPreference(KEY_SOFTWARE_VERSION);
        if (null != preference) {
            preference.setSummary(mInfoMap.get(KEY_SOFTWARE_VERSION));
        }

        preference = parent.findPreference(KEY_ANDROID_VERSION);
        if (null != preference) {
            preference.setSummary(mInfoMap.get(KEY_ANDROID_VERSION));
        }

        preference = parent.findPreference(KEY_STORAGE);
        if (null != preference) {
            preference.setSummary(mInfoMap.get(KEY_STORAGE));
        }
    }

    private void updatePreferenceByKey(String key, String result) {
        log("updatePreferenceByKey");
        if (mInfoMap == null) {
            return;
        }

        mInfoMap.put(key, result);
        PreferenceScreen parent = (PreferenceScreen) getPreferenceScreen();
        if (parent == null) {
            return;
        }

        Preference preference = findPreference(key);
        if (null != preference) {
            log("updatePreferenceByKey, setSummary");
            preference.setSummary(mInfoMap.get(key));
        }
    }

    /**
     * set the info from cdma phone to preference.
     *
     * @param slot indicator which slot is cdma phone or invalid.
     * @return void.
     */
    private void setInfoToPreference() {
        PreferenceScreen parent = (PreferenceScreen) getPreferenceScreen();

        Preference preference = findPreference(KEY_PRL_VERSION);
        if (null != preference) {
            preference.setSummary(mInfoMap.get(KEY_PRL_VERSION));
        }

        preference = findPreference(KEY_SID);
        if (null != preference) {
            preference.setSummary(mInfoMap.get(KEY_SID));
        }

        preference = findPreference(KEY_NID);
        if (null != preference) {
            preference.setSummary(mInfoMap.get(KEY_NID));
        }

        preference = findPreference(KEY_MEID);
        if (null != preference) {
            preference.setSummary(mInfoMap.get(KEY_MEID));
        }

        preference = findPreference(KEY_BASE_ID);
        if (null != preference) {
            preference.setSummary(mInfoMap.get(KEY_BASE_ID));
        }

        preference = findPreference(KEY_IMEI_1);
        if (null != preference && null != mInfoMap.get(KEY_IMEI_1)) {
            preference.setSummary(mInfoMap.get(KEY_IMEI_1));
        } else if (null != preference) {
            parent.removePreference(preference);
        }

        preference = findPreference(KEY_IMEI_2);
        if (!mSinglePhone && null != preference && null != mInfoMap.get(KEY_IMEI_2)) {
            preference.setSummary(mInfoMap.get(KEY_IMEI_2));
        } else if (null != preference) {
            parent.removePreference(preference);
        }

        preference = findPreference(KEY_ICCID_1);
        if (mSlotSwitch) {
            if (null != preference && null != mInfoMap.get(KEY_ICCID_2)) {
                preference.setSummary(mInfoMap.get(KEY_ICCID_2));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        } else {
            if (null != preference && null != mInfoMap.get(KEY_ICCID_1)) {
                preference.setSummary(mInfoMap.get(KEY_ICCID_1));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        }

        preference = findPreference(KEY_ICCID_2);
        if (mSlotSwitch) {
            if (null != preference && null != mInfoMap.get(KEY_ICCID_1)) {
                preference.setSummary(mInfoMap.get(KEY_ICCID_1));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        } else {
            if (null != preference && null != mInfoMap.get(KEY_ICCID_2)) {
                preference.setSummary(mInfoMap.get(KEY_ICCID_2));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        }

        preference = findPreference(KEY_LTE_IMSI_1);
        if (mSlotSwitch) {
            if (null != preference && null != mInfoMap.get(KEY_LTE_IMSI_2)) {
                preference.setSummary(mInfoMap.get(KEY_LTE_IMSI_2));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        } else {
            if (null != preference && null != mInfoMap.get(KEY_LTE_IMSI_1)) {
                preference.setSummary(mInfoMap.get(KEY_LTE_IMSI_1));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        }

        preference = findPreference(KEY_LTE_IMSI_2);
        if (mSlotSwitch) {
            if (null != preference && null != mInfoMap.get(KEY_LTE_IMSI_1)) {
                preference.setSummary(mInfoMap.get(KEY_LTE_IMSI_1));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        } else {
            if (null != preference && null != mInfoMap.get(KEY_LTE_IMSI_2)) {
                preference.setSummary(mInfoMap.get(KEY_LTE_IMSI_2));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        }

        preference = findPreference(KEY_CDMA_IMSI_1);
        if (mSlotSwitch) {
            if (null != preference && null != mInfoMap.get(KEY_CDMA_IMSI_2)) {
                preference.setSummary(mInfoMap.get(KEY_CDMA_IMSI_2));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        } else {
            if (null != preference && null != mInfoMap.get(KEY_CDMA_IMSI_1)) {
                preference.setSummary(mInfoMap.get(KEY_CDMA_IMSI_1));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        }

        preference = findPreference(KEY_CDMA_IMSI_2);
        if (mSlotSwitch) {
            if (null != preference && null != mInfoMap.get(KEY_CDMA_IMSI_1)) {
                preference.setSummary(mInfoMap.get(KEY_CDMA_IMSI_1));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        } else {
            if (null != preference && null != mInfoMap.get(KEY_CDMA_IMSI_2)) {
                preference.setSummary(mInfoMap.get(KEY_CDMA_IMSI_2));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        }


        preference = findPreference(KEY_NETWORK_1);
        if (mSlotSwitch) {
            if (null != preference && null != mInfoMap.get(KEY_NETWORK_2)) {
                preference.setSummary(mInfoMap.get(KEY_NETWORK_2));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        } else {
            if (null != preference && null != mInfoMap.get(KEY_NETWORK_1)) {
                preference.setSummary(mInfoMap.get(KEY_NETWORK_1));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        }

        preference = findPreference(KEY_NETWORK_2);
        if (mSlotSwitch) {
            if (null != preference && null != mInfoMap.get(KEY_NETWORK_1)) {
                preference.setSummary(mInfoMap.get(KEY_NETWORK_1));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        } else {
            if (null != preference && null != mInfoMap.get(KEY_NETWORK_2)) {
                preference.setSummary(mInfoMap.get(KEY_NETWORK_2));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        }


        preference = findPreference(KEY_UIM_ID);
        if (null != preference && null != mInfoMap.get(KEY_UIM_ID)) {
            preference.setSummary(mInfoMap.get(KEY_UIM_ID));
        } else if (null != preference) {
            parent.removePreference(preference);
        }

        preference = findPreference(KEY_OPERATOR_1);
        if (mSlotSwitch) {
            if (null != preference && null != mInfoMap.get(KEY_OPERATOR_2)) {
                preference.setSummary(mInfoMap.get(KEY_OPERATOR_2));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        } else {
            if (null != preference && null != mInfoMap.get(KEY_OPERATOR_1)) {
                preference.setSummary(mInfoMap.get(KEY_OPERATOR_1));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        }

        preference = findPreference(KEY_OPERATOR_2);
        if (mSlotSwitch) {
            if (null != preference && null != mInfoMap.get(KEY_OPERATOR_1)) {
                preference.setSummary(mInfoMap.get(KEY_OPERATOR_1));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        } else {
            if (null != preference && null != mInfoMap.get(KEY_OPERATOR_2)) {
                preference.setSummary(mInfoMap.get(KEY_OPERATOR_2));
            } else if (null != preference) {
                parent.removePreference(preference);
            }
        }
    }

    /**
     * Returns " (ENGINEERING)" if the msv file has a zero value, else returns "".
     *
     * @return a string to append to the model number description.
     */
    private String getMsvSuffix() {
        // Production devices should have a non-zero value. If we can't read it, assume it's a
        // production device so that we don't accidentally show that it's an ENGINEERING device.
        try {
            String msv = readLine(FILENAME_MSV);
            if (msv == null) {
                return "";
            }

            // Parse as a hex number. If it evaluates to a zero, then it's an engineering build.
            if (Long.parseLong(msv, 16) == 0) {
                return " (ENGINEERING)";
            }
        } catch (IOException ioe) {
            // Fail quietly, as the file may not exist on some devices.
        } catch (NumberFormatException nfe) {
            // Fail quietly, returning empty string should be sufficient
        }
        return "";
    }

    /**
     * Reads a line from the specified file.
     *
     * @param filename the file to read from.
     * @return the first line, if any.
     * @throws IOException if the file couldn't be read.
     */
    private String readLine(String filename) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename), 256);
        try {
            return reader.readLine();
        } finally {
            reader.close();
        }
    }

    /**
     * simple log info.
     *
     * @param msg need print out string.
     * @return void.
     */
    private static void log(String msg) {
        Log.d(TAG, msg);
    }

    private void fillSimInfo() {
        log("fillSimInfo");
        fillIccid();
        fillImsi();
        fillCdmaImsi();
        fillNetworkInfo();
        fillIMEIInfo();
        fillCdmaInfo();
    }

    private void initDeviceInfo() {
        log("initDeviceInfo");
        mInfoMap.clear();
        fillDeviceInfo();
    }

    private void initSimInfo() {
        log("initSimInfo");
        initSlotInfo();
        fillSimInfo();
        adjustInfoByRule();
        dumpInfoMap();
    }

    private void adjustInfoByRule() {
        //Decide which need to be display/undisplay.
    }

    private void initSlotInfo() {
        mSlotSwitch = false;
        int mainSlotId = getMainSlotId();
        if (mainSlotId == SLOT2 && TelephonyManager.from(this).hasIccCard(SLOT2)) {
            mSlotSwitch = true;
        }
        log("initSlotInfo, mSlotSwitch = " + mSlotSwitch);
    }

    private void fillDeviceInfo() {
        mInfoMap.put(KEY_PRODUCT_MODEL, Build.MODEL + getMsvSuffix());

        String hardWare = CustomProperties.getString(CustomProperties.MODULE_DM,
                "HardwareVersion", HARDWARE_DEFAULT);
        mInfoMap.put(KEY_HARDWARE_VERSION, hardWare);

        String softWare = CustomProperties.getString(CustomProperties.MODULE_DM,
                "SoftwareVersion", SOFTWARE_VERSION_DEFAULT);
        mInfoMap.put(KEY_SOFTWARE_VERSION, softWare);

        String version = android.os.Build.VERSION.RELEASE;
        String swVersion = "Android" + version;
        mInfoMap.put(KEY_ANDROID_VERSION, swVersion);

        long storageSize = getCustomizeStorageSize();
        String size = "" + storageSize + "G";
        mInfoMap.put(KEY_STORAGE, size);
    }

    private void fillIMEIInfo() {
        Phone[] phones = PhoneFactory.getPhones();
        if (phones != null && phones.length >= 2) {
            String imei1 = "" + TelephonyManager.getDefault().getImei(SLOT1);
            String imei2 = "" + TelephonyManager.getDefault().getImei(SLOT2);
            log("fillIMEIInfo, imei1 = " + formatLogValue(imei1) +
                                    ", imei2 = " + formatLogValue(imei2));
            mInfoMap.put(KEY_IMEI_1, imei1);
            mInfoMap.put(KEY_IMEI_2, imei2);
            mInfoMap.put(KEY_OPERATOR_1, getNetworkId(phones[0]));
            mInfoMap.put(KEY_OPERATOR_2, getNetworkId(phones[1]));
        } else if (phones != null && phones.length == 1) {
            String imei1 = "" + TelephonyManager.getDefault().getImei(SLOT1);
            mInfoMap.put(KEY_IMEI_1, imei1);
            mInfoMap.put(KEY_IMEI_2, null);

            mInfoMap.put(KEY_OPERATOR_1, getNetworkId(phones[0]));
            mInfoMap.put(KEY_OPERATOR_2, null);
            mSinglePhone = true;
        } else {
            mInfoMap.put(KEY_IMEI_1, null);
            mInfoMap.put(KEY_IMEI_2, null);

            mInfoMap.put(KEY_OPERATOR_1, null);
            mInfoMap.put(KEY_OPERATOR_2, null);
        }
    }

    private void fillIccid() {
        String iccid1 = SystemProperties.get("ril.iccid.sim1");
        String iccid2 = SystemProperties.get("ril.iccid.sim2");

        if (iccid1 == null || iccid1.isEmpty() || "N/A".equals(iccid1)) {
            mInfoMap.put(KEY_ICCID_1, null);
        } else {
            mInfoMap.put(KEY_ICCID_1, iccid1);
        }

        if (iccid2 == null || iccid2.isEmpty() || "N/A".equals(iccid2)) {
            mInfoMap.put(KEY_ICCID_2, null);
        } else {
            mInfoMap.put(KEY_ICCID_2, iccid2);
        }
    }

    private void fillImsi() {
        int phoneCount = TelephonyManager.from(this).getPhoneCount();

        for (int i = 0; i < phoneCount; i++) {
            int id = i + 1;
            int[] subIdList = SubscriptionManager.getSubId(i);
            if (subIdList == null) {
                continue;
            }

            String imsi = TelephonyManager.from(this).getSubscriberId(subIdList[0]);
            if (imsi == null || imsi.isEmpty() || "N/A".equals(imsi)) {
                mInfoMap.put(KEY_LTE_IMSI + id, null);
            } else {
                mInfoMap.put(KEY_LTE_IMSI + id, imsi);
            }
        }
    }

    private void fillCdmaImsi() {
        int phoneCount = TelephonyManager.from(this).getPhoneCount();
        for (int i = 0; i < phoneCount; i++) {
            int id = i + 1;
            int[] subIdList = SubscriptionManager.getSubId(i);
            if (subIdList == null) {
                continue;
            }

            String cdmaImsi = MtkTelephonyManagerEx.getDefault().getUimSubscriberId(subIdList[0]);
            if (cdmaImsi == null || cdmaImsi.isEmpty() || "N/A".equals(cdmaImsi)) {
                mInfoMap.put(KEY_CDMA_IMSI + id, null);
            } else {
                mInfoMap.put(KEY_CDMA_IMSI + id, cdmaImsi);
            }
        }
    }

    private int getNetworkType(int subId) {
        int networkType = TelephonyManager.NETWORK_TYPE_UNKNOWN;
        if (mFlightMode) {
            return networkType;
        }

        final int dataNetworkType = mTelephonyManager.getDataNetworkType(subId);
        final int voiceNetworkType = mTelephonyManager.getVoiceNetworkType(subId);
        log("updateNetworkType(), dataNetworkType = " + dataNetworkType
                + ", voiceNetworkType = " + voiceNetworkType);
        if (TelephonyManager.NETWORK_TYPE_UNKNOWN != dataNetworkType) {
            networkType = dataNetworkType;
        } else if (TelephonyManager.NETWORK_TYPE_UNKNOWN != voiceNetworkType) {
            networkType = voiceNetworkType;
        }
        return networkType;
    }

    private void fillNetworkInfo() {
        int sub1 = getSubIdBySlotId(SLOT1);
        log("getSubId, sub1 = " + sub1);
        if (sub1 > SubscriptionManager.INVALID_SIM_SLOT_INDEX) {
            int network = getNetworkType(sub1);
            String networkType = parseNetwokType(network);
            mInfoMap.put(KEY_NETWORK_1, networkType);
        } else {
            mInfoMap.put(KEY_NETWORK_1, null);
        }

        int sub2 = getSubIdBySlotId(SLOT2);
        log("getSubId, sub2 = " + sub2);
        if (sub2 > SubscriptionManager.INVALID_SIM_SLOT_INDEX) {
            int network = getNetworkType(sub2);
            String networkType = parseNetwokType(network);
            mInfoMap.put(KEY_NETWORK_2, networkType);
        } else {
            mInfoMap.put(KEY_NETWORK_2, null);
        }
    }

    private long getSystemStorageSize() {
        File path = null;
        try {
            path = Environment.getRootDirectory();
            log("System storage path is " + path);
            StatFs statFs = new StatFs(path.getPath());
            long blockSize = statFs.getBlockSize();
            long blockCount = statFs.getBlockCount();
            log("System storage blockSize is " + blockSize + ", blockCount is " + blockCount);
            long size = blockSize * blockCount;
            return size;
        } catch (IllegalArgumentException e) {
            return 0;
        }
    }

    private long getDataStorageSize() {
        File path = null;
        try {
            boolean sharedSd = SystemProperties.get("ro.mtk_shared_sdcard").equals("1");
            if (sharedSd) {
                path = Environment.getLegacyExternalStorageDirectory();
            } else {
                path = Environment.getDataDirectory();
            }
            log("Data storage path is " + path);
            StatFs statFs = new StatFs(path.getPath());
            long blockSize = statFs.getBlockSize();
            long blockCount = statFs.getBlockCount();
            log("Data storage blockSize is " + blockSize + ", blockCount is " + blockCount);
            long size = blockSize * blockCount;
            return size;
        } catch (IllegalArgumentException e) {
            return 0;
        }
    }

    private int getCustomizeStorageSize() {
        int count = 0;
        long totalSize = getSystemStorageSize() + getDataStorageSize();
        float size = (float) (totalSize / (1024*1024*1024));
        log("getCustomizeStorageSize, size = " + size);
        while(size > 1.0f) {
            size = size/2;
            count++;
        }
        return 1<<count;
    }

    private int getSubIdBySlotId(int slot) {
        int[] subIds = SubscriptionManager.getSubId(slot);
        if (subIds == null) {
            return SubscriptionManager.INVALID_SIM_SLOT_INDEX;
        }

        return subIds[0];
    }

    private String parseNetwokType(int network) {
        log("parseNetwokType network = " + network);
        String networkType = null;
        switch (network) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
                networkType = "GPRS";
                break;
            case TelephonyManager.NETWORK_TYPE_EDGE:
                networkType = "EDGE";
                break;
            case TelephonyManager.NETWORK_TYPE_UMTS:
                networkType = "UMTS";
                break;
            case TelephonyManager.NETWORK_TYPE_CDMA:
                networkType = "IS95";
                break;
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                networkType = "EVDO_0";
                break;
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                networkType = "EVDO_A";
                break;
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                networkType = "1xRTT";
                break;
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                networkType = "HSDPA";
                break;
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                networkType = "HSUPA";
                break;
            case TelephonyManager.NETWORK_TYPE_HSPA:
                networkType = "HSPA";
                break;
            case TelephonyManager.NETWORK_TYPE_IDEN:
                networkType = "IDEN";
                break;
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                networkType = "EVDO_B";
                break;
            case TelephonyManager.NETWORK_TYPE_LTE:
                networkType = "LTE";
                break;
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                networkType = "eHRPD";
                break;
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                networkType = "HSPA+";
                break;
            case TelephonyManager.NETWORK_TYPE_GSM:
                networkType = "GSM";
                break;
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                networkType = "Unknown";
                break;
            default:
                break;
        }
        return networkType;
    }

    private int getMainSlotId() {
        int mainSlotId = SubscriptionManager.INVALID_SIM_SLOT_INDEX;
        int subId = SubscriptionManager.getDefaultDataSubscriptionId();
        mainSlotId = SubscriptionManager.getSlotIndex(subId);
        if (mainSlotId != SubscriptionManager.INVALID_SIM_SLOT_INDEX) {
            log("getMainSlotId(), default data sim = " + mainSlotId);
            return mainSlotId;
        }

        String currMainSim = SystemProperties.get("persist.radio.simswitch", "");
        log("getMainSlotId(), currMainSim 3/4G Sim = " + currMainSim);
        if (!TextUtils.isEmpty(currMainSim)) {
            mainSlotId = Integer.parseInt(currMainSim) - 1;
            if (mainSlotId != SubscriptionManager.INVALID_SIM_SLOT_INDEX &&
                TelephonyManager.from(this).hasIccCard(mainSlotId)) {
                return mainSlotId;
            }
        }

        int phoneCount = TelephonyManager.from(this).getPhoneCount();
        for (int i = 0; i < phoneCount; i++) {
            if(TelephonyManager.from(this).hasIccCard(i)) {
                mainSlotId = i;
                log("getMainSlotId(), first slot = " + mainSlotId);
                break;
            }
        }

        return mainSlotId;
    }

    private GsmCdmaPhone getPhoneById(int index) {
        log( "gsmPhoneId(), index = " + index);
        int mainPhoneId = index;
        GsmCdmaPhone gsmPhone = null;
        Phone phone = null;
        Phone[] phones = PhoneFactory.getPhones();
        for (Phone p : phones) {
            if (p.getPhoneType() == PhoneConstants.PHONE_TYPE_GSM ||
                p.getPhoneType() == PhoneConstants.PHONE_TYPE_IMS ||
                p.getPhoneType() == PhoneConstants.PHONE_TYPE_CDMA) {
                if (p instanceof GsmCdmaPhone) {
                    gsmPhone = (GsmCdmaPhone) p;
                }

                if (gsmPhone == null) {
                    continue;
                }

                log( "gsmPhoneId = " + gsmPhone.getPhoneId());
                if (mainPhoneId != -1) {
                    if (mainPhoneId == gsmPhone.getPhoneId()) {
                        break;
                    }
                } else {
                    if (gsmPhone.getSubId() > 0) {
                        break;
                    }
                }
                gsmPhone = null;
            }
        }
        log("getPhoneById = " + gsmPhone);
        return gsmPhone;
    }

    private boolean isSupportCdma(int slotId) {
        boolean isSupportCdma = false;
        String[] type = MtkTelephonyManagerEx.getDefault().getSupportCardType(slotId);
        if (type != null) {
            for (int i = 0; i < type.length; i++) {
                if ("RUIM".equals(type[i]) || "CSIM".equals(type[i])) {
                    isSupportCdma = true;
                }
            }
        }
        log("slotId = " + slotId + " isSupportCdma = " + isSupportCdma);
        return isSupportCdma;
    }

    private boolean isCdmaCard(int slotId) {
        boolean isCdmaCard = false;
        if (isSupportCdma(slotId) || MtkTelephonyManagerEx.getDefault().isCt3gDualMode(slotId)) {
            isCdmaCard = true;
        }
        log("slotId = " + slotId + " isCdmaCard = " + isCdmaCard);
        return isCdmaCard;
    }

    private int getCDMAPhoneId() {
        log("getCDMAPhoneId()");
        int phoneCount = TelephonyManager.from(this).getPhoneCount();
        for (int i = 0; i < phoneCount; i++) {
            if(!TelephonyManager.from(this).hasIccCard(i)) {
                continue;
            }

            int[] subIdList = SubscriptionManager.getSubId(i);
            if (subIdList != null && isCdmaCard(i)) {
                return i;
            }
        }
        return SubscriptionManager.INVALID_SIM_SLOT_INDEX;
    }

    private String fillPRLVersion(GsmCdmaPhone phone) {
        int subId = phone.getSubId();
        String prlVersion = MtkTelephonyManagerEx.getDefault().getPrlVersion(subId);
        log("subId = " + subId + ", prlVersion = " + prlVersion);
        if (prlVersion == null || prlVersion.isEmpty()) {
            prlVersion = "";
        }
        mInfoMap.put(KEY_PRL_VERSION, prlVersion);
        return prlVersion;
    }

    private void startQueryUIMId(GsmCdmaPhone phone) {
        int slotId = SubscriptionManager.getSlotIndex(phone.getSubId());
        String[] type = MtkTelephonyManagerEx.getDefault().getSupportCardType(slotId);
        String path = "";
        if (type != null) {
            for (int i = 0; i < type.length; i++) {
                if ("CSIM".equals(type[i])){
                    path = CSIM_PATH;
                    break;
                } else if ("RUIM".equals(type[i])) {
                    path =RUIM_PATH;
                    break;
                }
            }
        }
        log("startQueryUIMId, slotId = " + slotId + ", path = " + formatLogValue(path));
        SimInfoArg args =
                new SimInfoArg(slotId, UiccController.APP_FAM_3GPP2, SIM_FILE_ID, path);
        new SimInfoAsyncTask().executeOnExecutor(Executors.newCachedThreadPool(), args);
    }

    private void fillCdmaPhoneInfo(GsmCdmaPhone gsmPhone) {
        if (gsmPhone == null) {
            return;
        }


        startQueryUIMId(gsmPhone);
        mInfoMap.put(KEY_UIM_ID, "");

        fillPRLVersion(gsmPhone);
        fillPhoneInfo(gsmPhone);

        if (mInfoMap.get(KEY_BASE_ID) != null) {
            return;
        }

        if (!(gsmPhone.getCellLocation() instanceof CdmaCellLocation)) {
            return;
        }

        int cdmaSubId = gsmPhone.getSubId();
        int baseId = ((CdmaCellLocation)gsmPhone.getCellLocation()).getBaseStationId();
        if (baseId > 0 && cdmaSubId > 0 && !mFlightMode) {
            mInfoMap.put(KEY_BASE_ID, "" + baseId);
        } else {
            mInfoMap.put(KEY_BASE_ID, null);
        }
        log("fillCdmaPhoneInfo, cdmaSubId = " + cdmaSubId + ", baseId = " + baseId);
    }

    private void fillPhoneInfo(GsmCdmaPhone gsmPhone) {
        if (gsmPhone == null) {
            return;
        }

        boolean isInService = false;
        if (gsmPhone.getServiceState().getVoiceRegState()
                == ServiceState.STATE_IN_SERVICE
                || gsmPhone.getServiceState().getDataRegState()
                == ServiceState.STATE_IN_SERVICE) {
            isInService = true;
        }

        int gsmSubId = gsmPhone.getSubId();

        int sid = gsmPhone.getServiceState().getSystemId();
        if (sid <= 0 || !isInService || gsmSubId < 0) {
            mInfoMap.put(KEY_SID, "");
        } else {
            mInfoMap.put(KEY_SID, "" + sid);
        }

        int nid = gsmPhone.getServiceState().getNetworkId();
        if (nid <= 0 || !isInService || gsmSubId < 0) {
            mInfoMap.put(KEY_NID, "");
        } else {
            mInfoMap.put(KEY_NID, "" + nid);
        }

        if (!(gsmPhone.getCellLocation() instanceof GsmCellLocation)) {
            mInfoMap.put(KEY_BASE_ID, null);
            return;
        }

        int cid = ((GsmCellLocation) gsmPhone.getCellLocation()).getCid();
        if (gsmSubId>0 && cid > 0 && !mFlightMode) {
            mInfoMap.put(KEY_BASE_ID, "" + cid);
        }

        log("fillPhoneInfo, isInService = " + isInService + ", sid = " + sid +
            ", nid = " + nid + ", cid = " + cid);

    }

    private void fillCdmaInfo() {
        int phoneId = getMainSlotId();
        if (getCDMAPhoneId() != SubscriptionManager.INVALID_SIM_SLOT_INDEX) {
            if (!isCdmaCard(phoneId)) {
                phoneId = getCDMAPhoneId();
            }
            GsmCdmaPhone cdmaPhone = getPhoneById(phoneId);
            fillCdmaPhoneInfo(cdmaPhone);
        } else {
            GsmCdmaPhone gsmPhone = getPhoneById(phoneId);
            fillPhoneInfo(gsmPhone);
        }
        mInfoMap.put(KEY_MEID, getMeid());
    }

    private String getMeid() {
        String meid = "";
        int count = TelephonyManager.getDefault().getPhoneCount();
        for (int i = 0; i < count; i++) {
            if (TextUtils.isEmpty(meid)) {
                meid = TelephonyManager.getDefault().getMeid(i);
                log("getMeid, meid = " + formatLogValue(meid));
            }
        }
        return meid;
    }

    private String getNetworkId(Phone phone) {
        //Because of C2K will return in service state even no uim inserted,
        //so check the card state first.
        String nid = null;
        String mnc = null;
        if (mFlightMode || phone == null) {
            return null;
        }

        ServiceState serviceState = phone.getServiceState();
        if (serviceState != null) {
            if ((serviceState.getDataRegState() == ServiceState.STATE_IN_SERVICE) ||
                (serviceState.getVoiceRegState() == ServiceState.STATE_IN_SERVICE)) {
                nid = serviceState.getOperatorNumeric();
                if (nid != null && nid.length() > 3) {
                    mnc = nid.substring(0, 3) + "、" + nid.substring(3);
                }
            }

            if ((serviceState.getDataRegState() == ServiceState.STATE_IN_SERVICE) &&
                (serviceState.getDataNetworkType() == TelephonyManager.NETWORK_TYPE_LTE)) {
                nid = serviceState.getDataOperatorNumeric();
                if (nid != null && nid.length() > 3) {
                    mnc = nid.substring(0, 3) + "、" + nid.substring(3);
                }
            }
        }
        log("getNetworkId(), mnc = " + mnc);
        return mnc;
    }

    private void dumpInfoMap() {
        Set<String> set = mInfoMap.keySet();
        log("CdmaInfoSpecification dump start");

        log(KEY_PRODUCT_MODEL + " = " + formatLogValue(mInfoMap.get(KEY_PRODUCT_MODEL)));
        log(KEY_HARDWARE_VERSION + " = " + formatLogValue(mInfoMap.get(KEY_HARDWARE_VERSION)));
        log(KEY_SOFTWARE_VERSION + " = " + formatLogValue(mInfoMap.get(KEY_SOFTWARE_VERSION)));

        log(KEY_PRL_VERSION + " = " + formatLogValue(mInfoMap.get(KEY_PRL_VERSION)));
        log(KEY_SID + " = " + formatLogValue(mInfoMap.get(KEY_SID)));
        log(KEY_NID + " = " + formatLogValue(mInfoMap.get(KEY_NID)));
        log(KEY_MEID + " = " + formatLogValue(mInfoMap.get(KEY_MEID)));
        log(KEY_UIM_ID + " = " + formatLogValue(mInfoMap.get(KEY_UIM_ID)));
        log(KEY_BASE_ID + " = " + formatLogValue(mInfoMap.get(KEY_BASE_ID)));

        log(KEY_IMEI_1 + " = " + formatLogValue(mInfoMap.get(KEY_IMEI_1)));
        log(KEY_IMEI_2 + " = " + formatLogValue(mInfoMap.get(KEY_IMEI_2)));

        log(KEY_ICCID_1 + " = " + formatLogValue(mInfoMap.get(KEY_ICCID_1)));
        log(KEY_ICCID_2 + " = " + formatLogValue(mInfoMap.get(KEY_ICCID_2)));

        log(KEY_OPERATOR_1 + " = " + formatLogValue(mInfoMap.get(KEY_OPERATOR_1)));
        log(KEY_OPERATOR_2 + " = " + formatLogValue(mInfoMap.get(KEY_OPERATOR_2)));

        log(KEY_LTE_IMSI_1 + " = " + formatLogValue(mInfoMap.get(KEY_LTE_IMSI_1)));
        log(KEY_LTE_IMSI_2 + " = " + formatLogValue(mInfoMap.get(KEY_LTE_IMSI_2)));
        log(KEY_CDMA_IMSI_1 + " = " + formatLogValue(mInfoMap.get(KEY_CDMA_IMSI_1)));
        log(KEY_CDMA_IMSI_2 + " = " + formatLogValue(mInfoMap.get(KEY_CDMA_IMSI_2)));

        log(KEY_NETWORK_1 + " = " + formatLogValue(mInfoMap.get(KEY_NETWORK_1)));
        log(KEY_NETWORK_2 + " = " + formatLogValue(mInfoMap.get(KEY_NETWORK_2)));

        log(KEY_STORAGE+ " = " + formatLogValue(mInfoMap.get(KEY_STORAGE)));
        log(KEY_ANDROID_VERSION+ " = " + mInfoMap.get(KEY_ANDROID_VERSION));

        log("CdmaInfoSpecification dump end");
    }

    private String formatLogValue(String value) {
        String formatValue = value;
        if (!DEBUG) {
            if (!TextUtils.isEmpty(formatValue)) {
                int i = formatValue.length() - LOG_LENGTH;
                if (i > 0) {
                    formatValue = String.format("XXXXX%s", formatValue.substring(i));
                }
            }

        }
        return formatValue;
    }

    private class SimInfoArg {
        public SimInfoArg(int slotId, int family, int file, String path) {
            this.mSlotId = slotId;
            this.mFamily = family;
            this.mFileId = file;
            this.mPath = path;
        }
        int    mSlotId;
        int    mFamily;
        int    mFileId;
        String mPath;
    }

    private class SimInfoAsyncTask extends AsyncTask<SimInfoArg, Void, String> {
        @Override
        protected String doInBackground(SimInfoArg... params) {
            log("doInBackground");
            String result = "";
            byte[] uimId = MtkTelephonyManagerEx.getDefault().loadEFTransparent(
                    params[0].mSlotId, params[0].mFamily, params[0].mFileId, params[0].mPath);
            if (uimId != null) {
                int length = (int) uimId[0];
                log("doInBackground, length = " + length + ", uimlen = " + uimId.length);
                if (length > uimId.length - 1) {
                    return result;
                }

                for (int i = length; i > 0; i--) {
                    String hex = Integer.toHexString(uimId[i] & 0xFF);
                    if (hex.length() == 1) {
                        hex = '0' + hex;
                    }
                    if (hex != null) {
                        hex = hex.toUpperCase();
                    }
                    result = result + hex;
                }
                log("doInBackground, result = " + formatLogValue(result));
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                log("onPostExecute, result = " + formatLogValue(result));
                updatePreferenceByKey(KEY_UIM_ID, result);
            }
        }
    }
}
