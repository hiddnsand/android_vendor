#include "mrdump_support_ext4.h"

#include <cutils/properties.h>
#include <mrdump_user.h>
#include <sys/system_properties.h>
#include <zlib.h>

#ifndef LOG_TAG
    #define LOG_TAG "AEE_MRDUMP"
#endif

struct palloc_file {
    int fd;
    int blksize;
    int lbaooo;
};

static struct palloc_file *palloc_file_open(const char *allocfile)
{
    struct stat statinfo;

    struct palloc_file *pfile = malloc(sizeof(struct palloc_file));
    pfile->fd = -1;
    pfile->blksize = 0;
    pfile->lbaooo = 0;

    /////////////////////////////////////////////////////
    // open file handle
    pfile->fd = open(allocfile, O_RDWR | O_SYNC, 0400);
    if(0 > pfile->fd) {
        MD_LOGE("%s: marker open failed(%d), %s\n", __func__, errno, strerror(errno));
        return NULL;
    }
    // Get file statinfo
    if(0 > fstat(pfile->fd, &statinfo)) {
        MD_LOGE("%s: marker stat failed(%d), %s\n", __func__, errno, strerror(errno));
	goto cleanup;
    }

    if(0 > ioctl(pfile->fd, FIGETBSZ, &pfile->blksize)) {
        MD_LOGE("%s: FIGETGSZ failed(%d), %s", __func__, errno, strerror(errno));
	goto cleanup;
    }

    pfile->lbaooo = mrdump_fiemap_get_entry_lba(pfile->fd, pfile->blksize, 0);
    if (pfile->lbaooo < 0) {
        MD_LOGE("%s: get lba failed", __func__);
	goto cleanup;
    }
    return pfile;

  cleanup:
    close(pfile->fd);
    pfile->fd = -1;
    pfile->blksize = 0;
    pfile->lbaooo = 0;
    return NULL;
}

void palloc_file_close(struct palloc_file *pfile)
{
    close(pfile->fd);
    free(pfile);
}

static int fop_file_write_string(const char *path, const char *content, ...)
{
    int ret_val = 0;

    int fd = open(path, O_WRONLY);
    if (fd >= 0) {
        char content_buf[1024];
        va_list ap;

        va_start(ap, content);
        int n = vsnprintf(content_buf, sizeof(content_buf), content, ap);
        va_end(ap);
        int val = write(fd, content_buf, n);
        if (val != n) {
            if (val < 0) {
                ret_val = -errno;
            }
            else {
                ret_val = -EIO;
            }
        }
        close(fd);
	if (ret_val == 0) {
	  ret_val = n;
	}
    }
    else {
        ret_val = -errno;
    }
    return ret_val;
}

static int fop_file_is_exist(const char *path)
{
  struct stat s;
  if (!path)
      return 0;
  if (stat(path, &s) != 0)
      return 0;
  return 1;
}

////////////////////////////////////////////
// common function (static in this file)  //
////////////////////////////////////////////
static bool ext4_add_attr(char const *allocfile, unsigned int flags)
{
    int fd;
    unsigned int myflags;

    if(allocfile == NULL) {
        allocfile = MRDUMP_EXT4_ALLOCATE_FILE;
    }

    fd = open(allocfile, O_RDONLY);
    if(0 > fd) {
        MD_LOGE("%s: open allocfile failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    if(0 > ioctl(fd, FS_IOC_GETFLAGS, &myflags)) {
        MD_LOGE("%s: FS_IOC_GETFLAGS failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    myflags |= flags;
    if(0 > ioctl(fd, FS_IOC_SETFLAGS, &myflags)) {
        MD_LOGE("%s: FS_IOC_SETFLAGS failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    close(fd);
    return true;
}

static off64_t ext4_block_lseek(int fd, unsigned lba, int blksize)
{
    unsigned long long location = (unsigned long long) lba * (unsigned long long) blksize;
    return lseek64(fd, location, SEEK_SET);
}

static char *ext4_get_device_node_from_fstab(const char *fstab, const char *mountp)
{
    int ret;
    FILE *fp;
    char c, myline[1024];
    char *delim="\x09\x20";
    char *DeviceNode, *MountPoint;

    DeviceNode = NULL;
    fp = fopen(fstab, "r");
    if(fp == NULL)
        return NULL;

    while(!feof(fp)) {

        // getline
        ret = fscanf(fp, "%[^\n]", myline);

        // strtok strings
        if(ret > 0) {
            if(myline[0] == '/') {
                DeviceNode = strtok(myline, delim);
                MountPoint = strtok(NULL, delim);
                if(MountPoint != NULL) {
                    if(!strcmp(MountPoint, mountp)) {
                        fclose(fp);
                        return strdup(DeviceNode);
                    }
                }
            }
        }

        /* clear newline character */
        ret = fscanf(fp, "%c", &c);
        if (ret != 1) {
            MD_LOGE("%s: not EOL.", __func__);
            fclose(fp);
            return NULL;
        }
    }
    fclose(fp);
    return NULL;
}

static const char *fstab_path_prefix[] = {
    "/vendor/etc/fstab",
    "/fstab",
    NULL
};

static char *ext4_get_device_node(const char *mountp)
{
    int i;
    char *DeviceNode = NULL;
    char fstab_filename[PROPERTY_VALUE_MAX];

    /* get hardware parts */
    char propbuf[PROPERTY_VALUE_MAX];
    if(property_get("ro.hardware", propbuf, NULL) == 0)
        property_get("ro.board.platform", propbuf, "");

    for(i = 0; fstab_path_prefix[i] != NULL; i++) {
        snprintf(fstab_filename, sizeof(fstab_filename), "%s.%s", fstab_path_prefix[i], propbuf);
        if(fop_file_is_exist(fstab_filename))
            DeviceNode =  ext4_get_device_node_from_fstab(fstab_filename, mountp);
        if(DeviceNode)
	    return DeviceNode;
    }

    /* search for path: /fstab */
    DeviceNode =  ext4_get_device_node_from_fstab("/fstab", mountp);
    return DeviceNode;
}

static int ext4_open_device_node(const char *mountp)
{
    char *devicenode = ext4_get_device_node(MRDUMP_EXT4_MOUNT_POINT);
    if(devicenode == NULL) {
        MD_LOGE("%s: Get devicenode return null\n", __func__);
        return -1;
    }
    int fd = open(devicenode, O_RDWR | O_SYNC, 0400);
    if(0 > fd) {
        MD_LOGE("%s: open devicenode failed(%d), %s\n", __func__, errno, strerror(errno));
        free(devicenode);
        return -1;
    }
    free(devicenode);
    return fd;
}

static bool mount_as_ext4(const char *mountp)
{
    struct statfs fs;
    if(statfs(mountp, &fs) == 0) {
        /*
         * bionic header didn't define EXT4_SUPER_MAGIC
         * use EXT3_SUPER_MAGIC instead
         */
        if(fs.f_type == EXT3_SUPER_MAGIC)
            return true;

        MD_LOGE("%s: %s is not ext4.\n", __func__, mountp);
        return false;
    }
    MD_LOGE("%s: statfs error.\n", __func__);
    return false;
}

static uint64_t get_partition_free_size(const char *mountp)
{
    struct statvfs vfs;
    uint64_t psize;

    if(statvfs(mountp, &vfs) == 0) {
        psize = (uint64_t) vfs.f_frsize * (uint64_t) vfs.f_bfree;
        MD_LOGD("%s: size of %s: %" PRId64 " bytes.\n", __func__, mountp, psize);
        return psize;
    }

    MD_LOGE("%s: statvfs of %s got failed(%d), %s\n", __func__, mountp, errno, strerror(errno));
    return 0;
}

static int ext4_get_blocksize(const char *myfile)
{
    int fd, blksize;

    fd = open(myfile, O_RDWR | O_SYNC, 0400);
    if(0 > fd) {
        MD_LOGE("%s: open failed(%d), %s", __func__, errno, strerror(errno));
        return -1;
    }

    blksize = 512;
    if(0 > ioctl(fd, FIGETBSZ, &blksize)) {
        MD_LOGE("%s: FIGETGSZ failed(%d), %s", __func__, errno, strerror(errno));
        return -1;
    }

    close(fd);
    return blksize;
}

static int ext4_get_next_bidx(int myfd, unsigned int *pBlock, unsigned int bbidx, unsigned int bs, unsigned int moves)
{
    unsigned int rlba, mycrc;

    if(bbidx > MRDUMP_EXT4_LBA_PER_BLOCK)
        return -1;

    if(bbidx == MRDUMP_EXT4_LBA_PER_BLOCK) {
        rlba = pBlock[bbidx];
        // seek to location
        if(-1 == ext4_block_lseek(myfd, rlba, bs)) {
            MD_LOGE("%s: lseek64 location failed(%d), %s", __func__, errno, strerror(errno));
            return -1;
        }
        // read to pBlock
        if(0 > read(myfd, pBlock, bs)) {
            MD_LOGE("%s: read lbaooo error!(%d), %s", __func__, errno, strerror(errno));
            return -1;
        }
        // check crc32
        mycrc = crc32(0, Z_NULL, 0);
        mycrc = crc32(mycrc, (void *)pBlock, (bs-4));
        if(mycrc != pBlock[1023]) {
            MD_LOGE("%s: crc32 error!(%d), %s", __func__, errno, strerror(errno));
            return -1;
        }
        bbidx = 0;
    } else {
        bbidx+=moves;
    }
    return bbidx;
}

static unsigned int ext4_num_to_join(unsigned int *pBlock, unsigned int bbidx)
{
    unsigned int i, j;
    for(i=0, j=0; i<MRDUMP_EXT4_MAX_CONTINUE; i++) {
        if((pBlock[bbidx+i] - pBlock[bbidx]) == i) {
            j++;
            continue;
        }
        break;
    }
    return j;
}

static int construct_a_block(int myfds, int mybs, int myloop, int myoffset, struct fiemap_info *mapinfo, unsigned int rows)
{
    unsigned int i, lba, num, mycrc;
    unsigned int block[1024]; // assume max BLOCKSIZE=4096, int(4byte) for addressing

    // Zero blocks
    memset(block, 0, sizeof(block));

    // number of address per block
    // 1024 = Normal_LBA[1022] + Next_LBA + crc32
    num = (mybs+sizeof(int)-1) / sizeof(int);

    // Indexing by 0. --> (i-1)
    // i     = 1~MRDUMP_EXT4_LBA_PER_BLOCK(1022)
    // index = 0~1021
    i = 1;
    while(i < (num - 1)) {
        lba = myoffset + (i-1);
        lba = mrdump_fiemap_get_lba_of_block(mapinfo, rows, lba);
        if(0 > lba) {
            MD_LOGE("%s: mrdump_fiemap_get_lba_of_block(1021) failed(%d), %s", __func__, errno, strerror(errno));
            return -1;
        }
        block[(i-1)] = lba;
        i++;
    }

    /* coverity: to avoid while(i < (num - 1)) taking false branch */
    if (i < 2) {
        MD_LOGE("%s: number of address per block should be more than 2\n", __func__);
        return -1;
    }

    // i     = 1023
    // index = MRDUMP_EXT4_LBA_PER_BLOCK(1022)
    if (block[(i-2)] == 0) {
        // End of File
        block[(i-1)] = 0;
    } else {
        // lba = (myloop+1); --> Need ++ For InfoLBA[EXT4_LBA_INFO_NUM];
        // block starts from 2nd Block
        lba = myloop + 2;
        lba = mrdump_fiemap_get_lba_of_block(mapinfo, rows, lba);
        if(0 > lba) {
            MD_LOGE("%s: mrdump_fiemap_get_lba_of_block(1022) failed(%d), %s", __func__, errno, strerror(errno));
            return -1;
        }
        block[(i-1)] = lba;
    }

    // i     = 1023: crc32
    // index = 1023: (NO block[i+1] now...)
    // PS: index = 1023 now... i takes no changes.
    mycrc = crc32(0, Z_NULL, 0);
    mycrc = crc32(mycrc, (void *)block, (mybs-4));
    block[i] = mycrc;

    // Prepare to write onto the Blocks in the front ...
    //
    // the address (LBA) where to write on.
    // lba = myloop; --> Need ++ For InfoLBA[EXT4_LBA_INFO_NUM];
    lba = myloop+1;
    lba = mrdump_fiemap_get_lba_of_block(mapinfo, rows, lba);
    if(0 > lba) {
        MD_LOGE("%s: mrdump_fiemap_get_lba_of_block(write) failed(%d), %s", __func__, errno, strerror(errno));
        return -1;
    }
    // Write block onto location;
    if (-1 == ext4_block_lseek(myfds, lba, mybs)) {
        MD_LOGE("%s: lseek64 location failed(%d), %s", __func__, errno, strerror(errno));
        return -1;
    }
    if(0 > write(myfds, (void *)block, sizeof(block))) {
        MD_LOGE("%s: write block error!(%d), %s", __func__, errno, strerror(errno));
        return -1;
    }

    // return the new offset
    // i = 1023
    if (block[(i-1)] == 0) {
        myoffset = 0;
    } else {
        myoffset = myoffset + (i - 1);
    }

    return (int)myoffset;
}

static bool ext4_lba_mark_header(const char *allocfile)
{
    int fd, blksize, num_blocks, rsv_blocks;
    struct stat statinfo;
    unsigned int mycrc, block, InfoLBA[EXT4_LBA_INFO_NUM];

    /////////////////////////////////////////////////////
    // Zero InfoLBA ...
    memset(InfoLBA, 0, sizeof(InfoLBA));

    /////////////////////////////////////////////////////
    // open file handle
    fd = open(allocfile, O_RDWR | O_SYNC, 0400);
    if(0 > fd) {
        MD_LOGE("%s: marker open failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }
    // Get file statinfo
    if(0 > fstat(fd, &statinfo)) {
        MD_LOGE("%s: marker stat failed(%d), %s\n", __func__, errno, strerror(errno));
        close(fd);
        return false;
    }
    // Get Blocksize and counting blocks
    blksize = ext4_get_blocksize(allocfile);
    if(blksize != (int)statinfo.st_blksize) {
        MD_LOGE("%s: marker blksize not matched.(%d), %s\n", __func__, errno, strerror(errno));
        close(fd);
        return false;
    }
    // Counting blocks
    num_blocks = (statinfo.st_size + blksize - 1) / blksize;

    // Get 1st LBA
    block = mrdump_fiemap_get_entry_lba(fd, blksize, 0);
    InfoLBA[EXT4_1ST_LBA] = block;

    // Get 2nd LBA
    if(2 > mrdump_fiemap_get_entry_tot(fd, blksize, 0))
        InfoLBA[EXT4_2ND_LBA] = mrdump_fiemap_get_entry_lba(fd, blksize, 1);
    else
        InfoLBA[EXT4_2ND_LBA] = InfoLBA[EXT4_1ST_LBA] + 1;

    // Set CORE DUMP SIZE = 0
    InfoLBA[EXT4_CORE_DUMP_SIZE] = 0;

    // Reserved space for Header and Tailer
    rsv_blocks = num_blocks/MRDUMP_EXT4_LBA_PER_BLOCK;   // Header
    rsv_blocks+= 2;                 // Tailer
    InfoLBA[EXT4_USER_FILESIZE] = (num_blocks - rsv_blocks) * blksize;

    // check crc32 of InfoLBA
    mycrc = crc32(0, Z_NULL, 0);
    mycrc = crc32(mycrc, (void *)InfoLBA, (sizeof(InfoLBA)-4));
    InfoLBA[EXT4_INFOBLOCK_CRC] = mycrc;

    // Close(allocfile)
    close(fd);

    fd = ext4_open_device_node(MRDUMP_EXT4_MOUNT_POINT);
    if (fd < 0) {
        return false;
    }

    // Save InfoLBA First
    if(-1 == ext4_block_lseek(fd, InfoLBA[EXT4_1ST_LBA], blksize)) {
        MD_LOGE("%s: marker header lseek64 InfoLBA failed(%d), %s\n", __func__, errno, strerror(errno));
        close(fd);
        return false;
    }
    if(0 > write(fd, (void *)InfoLBA, (sizeof(InfoLBA)))) {
        MD_LOGE("%s: marker header write InfoLBA error!(%d), %s\n", __func__, errno, strerror(errno));
        close(fd);
        return false;
    }

    close(fd);
    return true;
}

static bool ext4_lba_mark_body(const char *allocfile)
{
    int             fd, fds;
    int             i, blksize, num_blocks, loop, offset;
    struct stat     statinfo;

    /////////////////////////////////////////////////////
    // open file handle
    fd = open(allocfile, O_RDWR | O_SYNC, 0400);
    if(0 > fd) {
        MD_LOGE("%s: marker open failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }
    // Get file statinfo
    if(0 > fstat(fd, &statinfo)) {
        MD_LOGE("%s: marker stat failed(%d), %s\n", __func__, errno, strerror(errno));
        close(fd);
        return false;
    }
    // Get Blocksize and counting blocks
    blksize = ext4_get_blocksize(allocfile);
    if(blksize != (int)statinfo.st_blksize) {
        MD_LOGE("%s: marker blksize not matched.(%d), %s\n", __func__, errno, strerror(errno));
        close(fd);
        return false;
    }
    // Counting blocks
    num_blocks = (statinfo.st_size + blksize - 1) / blksize;

    /////////////////////////////////////////////////////
    // begin to record (2-layer block writing)
    char *devicenode = ext4_get_device_node(MRDUMP_EXT4_MOUNT_POINT);
    if(devicenode == NULL) {
        MD_LOGE("%s: get devicenode failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    } else {
        fds = open(devicenode, O_RDWR | O_SYNC, 0400);
        if(0 > fds) {
            free(devicenode);
            MD_LOGE("%s: open devicenode failed(%d), %s\n", __func__, errno, strerror(errno));
            return false;
        }
    }
    free(devicenode);

    // for performance, we must finish fiemap here... but not in the loop
    unsigned int my_num = mrdump_fiemap_total_entries(fd, blksize);
    if(my_num == 0) {
        MD_LOGE("%s: mrdump_fiemap_total_entries error!(%d), %s\n", __func__, errno, strerror(errno));
        close(fds);
        close(fd);
        return false;
    }
    struct fiemap_info *myinfo = malloc(my_num * sizeof(struct fiemap_info));
    if(!mrdump_fiemap_get_entries(fd, blksize, myinfo, my_num)) {
        MD_LOGE("%s: mrdump_fiemap_get_entries error!(%d), %s\n", __func__, errno, strerror(errno));
        close(fds);
        close(fd);
        return false;
    }

    // go
    loop = (blksize / sizeof(unsigned int)) - 2;
    loop = (num_blocks + loop - 1) / loop;
    offset = loop + 1;
    for(i=0; i<loop; i++) {
        offset = construct_a_block(fds, blksize, i, offset, myinfo, my_num);
        if (offset == -1) {
            MD_LOGE("%s: marker construct block error!(%d), %s\n", __func__, errno, strerror(errno));
            close(fds);
            close(fd);
            return false;
        }
    }
    close(fds);
    close(fd);
    free(myinfo);
    return true;
}

static bool ext4_lba_marker(const char *allocfile)
{
    if (!ext4_lba_mark_header(allocfile)) {
        MD_LOGE("%s: mark header failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    if (!ext4_lba_mark_body(allocfile)) {
        MD_LOGE("%s: mark body failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    return true;
}

static int ext4_get_lbaooo_from_file(const char *allocfile)
{
    int fd, blksize;
    struct stat statinfo;

    /////////////////////////////////////////////////////
    // open file handle
    fd = open(allocfile, O_RDWR | O_SYNC, 0400);
    if(0 > fd) {
        MD_LOGE("%s: marker open failed(%d), %s\n", __func__, errno, strerror(errno));
        return -1;
    }
    // Get file statinfo
    if(0 > fstat(fd, &statinfo)) {
        MD_LOGE("%s: marker stat failed(%d), %s\n", __func__, errno, strerror(errno));
        close(fd);
        return -1;
    }
    // Get Blocksize and counting blocks
    blksize = ext4_get_blocksize(allocfile);
    if(blksize != (int)statinfo.st_blksize) {
        MD_LOGE("%s: marker blksize not matched.(%d), %s\n", __func__, errno, strerror(errno));
        close(fd);
        return -1;
    }

    int lbaooo = mrdump_fiemap_get_entry_lba(fd, blksize, 0);

    close(fd);
    return (int)lbaooo;
}

static bool ext4_set_sys_lbaooo(const char *allocfile)
{
    // check out lbaooo from allocfile
    int lbaooo = ext4_get_lbaooo_from_file(allocfile);
    if (0 > lbaooo) {
        MD_LOGE("%s: get lbaooo from file failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    int retval = fop_file_write_string(MRDUMP_EXT4_PARA_LBAOOO, "%u\n", (unsigned int) lbaooo);
    if (retval < 0) {
	MD_LOGE("%s: write %s failed(%d), %s\n", __func__, MRDUMP_EXT4_PARA_LBAOOO,
		-retval, strerror(-retval));
	return false;
    }
    return true;
}

static bool ext4_fallocate(const char *allocfile, uint64_t allocsize)
{
    int fd;

    if(allocfile == NULL) {
        MD_LOGE("%s: allocfile is NULL! allocsize = %" PRIu64 "\n", __func__, allocsize);
        return false;
    }

    fd = open(allocfile, O_RDWR | O_CREAT | O_SYNC, 0400);
    if(0 > fd) {
        MD_LOGE("%s: open allocfile failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    #if (!__LP64__)
        uint32_t low = allocsize & 0xffffffff;
        uint32_t high = allocsize >> 32;

        if(0 > syscall(__NR_fallocate, fd, 0, 0, 0, low, high)) {
            MD_LOGE("%s: fallocate32 failed(allocfile=%s, allocsize=%" PRIu64 ")\n", __func__, allocfile, allocsize);
            return false;
        }
    #else
        if(0 > syscall(__NR_fallocate, fd, 0, 0, allocsize)) {
            MD_LOGE("%s: fallocate64 failed(allocfile=%s, allocsize=%" PRIu64 ")\n", __func__, allocfile, allocsize);
            return false;
        }
    #endif

    close(fd);
    return true;
}

static bool ext4_new_fallocfile(const char *allocfile, uint64_t allocsize)
{
    uint64_t psize = get_partition_free_size(MRDUMP_EXT4_MOUNT_POINT);
    if (psize < (allocsize + MRDUMP_EXT4_REST_SPACE)) {
        MD_LOGE("Error: Partition %s has no enough free space(%" PRIu64 "MB), allocate size %" PRIu64 "MB\n",
                MRDUMP_EXT4_MOUNT_POINT, psize / (1024 * 1024), allocsize / (1024 * 1024));
        return false;
    }

    if (!ext4_fallocate(allocfile, allocsize)) {
        MD_LOGE("%s: new fallocate failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    if (!ext4_lba_marker(allocfile)) {
        MD_LOGE("%s: lba marker failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    // sync when finish lba_marker in new_fallocate.
    sync();

    if (!ext4_add_attr(allocfile, (FS_SECRM_FL))) {
        MD_LOGE("%s: add chattr failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    return true;
}

static bool ext4_bdata_is_ok(const char *allocfile)
{
    int fd, fds, blksize, num_blocks;
    struct stat statinfo;
    unsigned int mycrc, InfoLBA[EXT4_LBA_INFO_NUM], BlockLBA[1024];

    /////////////////////////////////////////////////////
    // open file handle
    fd = open(allocfile, O_RDWR | O_SYNC, 0400);
    if(0 > fd) {
        MD_LOGE("%s: bdata open failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }
    // Get file statinfo
    if(0 > fstat(fd, &statinfo)) {
        MD_LOGE("%s: bdata stat failed(%d), %s\n", __func__, errno, strerror(errno));
        close(fd);
        return false;
    }
    // Get Blocksize and counting blocks
    blksize = ext4_get_blocksize(allocfile);
    if(blksize != (int)statinfo.st_blksize) {
        MD_LOGE("%s: bdata blksize not matched.(%d), %s\n", __func__, errno, strerror(errno));
        close(fd);
        return false;
    }
    // Counting blocks
    num_blocks = (statinfo.st_size + blksize - 1) / blksize;
    num_blocks = (num_blocks + MRDUMP_EXT4_LBA_PER_BLOCK - 1) / MRDUMP_EXT4_LBA_PER_BLOCK;

    // open
    char *devicenode = ext4_get_device_node(MRDUMP_EXT4_MOUNT_POINT);
    if(devicenode == NULL) {
        MD_LOGE("%s: get devicenode failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    } else {
        fds = open(devicenode, O_RDWR | O_SYNC, 0400);
        if(0 > fds) {
            free(devicenode);
            MD_LOGE("%s: open devicenode failed(%d), %s\n", __func__, errno, strerror(errno));
            return false;
        }
    }
    free(devicenode);

    // Check each Block (Now: InfoLBA + Address Block)
    unsigned int lba;
    // 1. InfoLBA blocks
    lba = ext4_get_lbaooo_from_file(allocfile);
    if(-1 == ext4_block_lseek(fds, lba, blksize)) {
        MD_LOGE("%s: bdata lseek64 InfoLBA failed(%d), %s\n", __func__, errno, strerror(errno));
        close(fd);
        return false;
    }
    if(0 > read(fds, (void *)InfoLBA, (sizeof(InfoLBA)))) {
        MD_LOGE("%s: bdata read InfoLBA error!(%d), %s\n", __func__, errno, strerror(errno));
        close(fd);
        return false;
    }
    mycrc = crc32(0, Z_NULL, 0);
    mycrc = crc32(mycrc, (void *)InfoLBA, (sizeof(InfoLBA)-4));
    if (mycrc != InfoLBA[EXT4_INFOBLOCK_CRC]) {
        MD_LOGE("%s: bdata InfoLBA CRC error, %04x-%04x-%04x-%04x\n", __func__,
                InfoLBA[0],InfoLBA[1], InfoLBA[2], InfoLBA[3]);
        close(fd);
        return false;
    }

    // for performance, we must finish fiemap here... but not in the loop
    unsigned int my_num = mrdump_fiemap_total_entries(fd, blksize);
    if(my_num == 0) {
        MD_LOGE("%s: mrdump_fiemap_total_entries error!(%d), %s\n", __func__, errno, strerror(errno));
        close(fds);
        close(fd);
        return false;
    }
    struct fiemap_info *myinfo = malloc(my_num * sizeof(struct fiemap_info));
    if(!mrdump_fiemap_get_entries(fd, blksize, myinfo, my_num)) {
        MD_LOGE("%s: mrdump_fiemap_get_entries error!(%d), %s\n", __func__, errno, strerror(errno));
        close(fds);
        close(fd);
        return false;
    }

    // 2. Address Block sector wait until patch in lk
    unsigned int i;
    for (i=1; i<num_blocks; i++) {
        lba = (unsigned int)mrdump_fiemap_get_lba_of_block(myinfo, my_num, i);
        if(-1 == ext4_block_lseek(fds, lba, blksize)) {
            MD_LOGE("%s: bdata lseek64 BlockLBA failed(%d), %s\n", __func__, errno, strerror(errno));
            close(fd);
            return false;
        }
        if(0 > read(fds, (void *)BlockLBA, blksize)) {
            MD_LOGE("%s: bdata read BlockLBA error!(%d), %s\n", __func__, errno, strerror(errno));
            close(fd);
            return false;
        }
        mycrc = crc32(0, Z_NULL, 0);
        mycrc = crc32(mycrc, (void *)BlockLBA, (blksize-4));
        if (mycrc != BlockLBA[1023]) {
            MD_LOGE("%s: bdata BlockLBA %d CRC error(%08x, %08x)\n", __func__, i, mycrc, BlockLBA[1023]);
            close(fd);
            return false;
        }
    }

    close(fds);
    close(fd);
    free(myinfo);
    return true;

}

static bool mrdump_ext4_read_infolba(int fd, unsigned int lbaooo, unsigned blksize,
                                     unsigned int block0[EXT4_LBA_INFO_NUM])
{
    memset(block0, 0, sizeof(unsigned int) * EXT4_LBA_INFO_NUM);

    if(-1 == ext4_block_lseek(fd, lbaooo, blksize)) {
        MD_LOGE("%s: lseek64 InfoLBA failed(%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    if (0 > read(fd, block0, sizeof(unsigned int) * EXT4_LBA_INFO_NUM)) {
        MD_LOGE("%s: read InfoLBA error (%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    unsigned int crcval = crc32(0, Z_NULL, 0);
    crcval = crc32(crcval, (void *)block0, sizeof(unsigned int) * (EXT4_LBA_INFO_NUM - 1));
    if (crcval != block0[EXT4_INFOBLOCK_CRC]) {
        MD_LOGE("%s: LBA Info CRC check failed crc %08x infoblock crc %08x\n", __func__, crcval, block0[EXT4_INFOBLOCK_CRC]);
        return false;
    }
    return true;
}

////////////////////////////////////////////
// export function (extern of other files)//
////////////////////////////////////////////

bool mrdump_file_get_info(const char *allocfile, struct mrdump_file_info *info)
{
    int blksize, lbaooo;
    unsigned int InfoLBA[EXT4_LBA_INFO_NUM];

    struct palloc_file *pfile = palloc_file_open(allocfile);
    if (pfile == NULL) {
	return false;
    }
    lbaooo = pfile->lbaooo;
    blksize = pfile->blksize;
    palloc_file_close(pfile);

    int fd = ext4_open_device_node(MRDUMP_EXT4_MOUNT_POINT);
    if (fd < 0) {
        return false;
    }

    if (!mrdump_ext4_read_infolba(fd, lbaooo, blksize, InfoLBA)) {
        close(fd);
        return false;
    }
    close(fd);

    info->info_lba = InfoLBA[EXT4_1ST_LBA];
    info->addr_lba = InfoLBA[EXT4_2ND_LBA];
    info->filesize = InfoLBA[EXT4_USER_FILESIZE];
    info->coredump_size = InfoLBA[EXT4_CORE_DUMP_SIZE];
    return true;
}

static uint64_t ext4_default_filesize(int memsize)
{
    int ret = 0;
    FILE *fp;
    char c, myline[1024];
    char *delim="\x09\x20";
    uint64_t MySize;

    fp = fopen("/proc/meminfo", "r");
    if(fp == NULL)
        return ret;

    while(!feof(fp)) {

        // getline
        ret = fscanf(fp, "%[^\n]", myline);

        // strtok strings
        MySize = 0;
        if(ret > 0) {
            char *mystr = strtok(myline, delim);
            if(mystr != NULL) {
                if(!strcmp(mystr, "MemTotal:")) {
                    mystr = strtok(NULL, delim);
                    if(mystr != NULL) {
                        MySize = atol(mystr);
                    }
                    break;
                }
            }
        }

        /* clear newline character */
        ret = fscanf(fp, "%c", &c);
        if (ret != 1) {
            MD_LOGE("%s: not EOL.", __func__);
            fclose(fp);
            break;
        }
    }
    fclose(fp);

    // Count proper MySize about half size of MemTotal;
    ret = ((MySize*1000) + (1000*1000) - 1)/(1000000);
    if(memsize == DEFAULT_HALFMEM)
        ret/= 2;
    MySize = (uint64_t)ret*1024*1024;

    return (MySize);
}

static uint64_t mrdump_file_default_filesize(void)
{
    char propbuf[PROP_VALUE_MAX];
    int  f_mem = USERDEFINED_MEM;
    uint64_t fsize = 0;

    // defualt half-memory size
    property_get("persist.mtk.aee.allocate", propbuf, "fullmem");

    // check setting of "persist.mtk.aee.allocate"
    if (!strncmp(propbuf, "0", 1))
        return 0;
    else if (!strncmp(propbuf, "halfmem", 7)) {
        fsize = ext4_default_filesize(DEFAULT_HALFMEM);
        f_mem = DEFAULT_HALFMEM;
    }
    else if (!strncmp(propbuf, "fullmem", 7)) {
        fsize = ext4_default_filesize(DEFAULT_FULLMEM);
        f_mem = DEFAULT_FULLMEM;
    }
    // nnn or others
    else if(f_mem == USERDEFINED_MEM) {
        int value = atoi(propbuf);
        if(value > 0) {
            fsize = (uint64_t)value*1024*1024;
        } else {
            fsize = 0;
        }
    }
    return fsize;
}

////////////////////////////////////////////
// User API                                  //
////////////////////////////////////////////
static bool mrdump_ext4_reinit_allocfile(const char *allocfile, int64_t allocsize, bool reset)
{
    uint64_t realsize;

    // set size with unsigned int
    if (allocsize == -1) {
        realsize = mrdump_file_default_filesize();
    } else {
        realsize = (uint64_t)allocsize;
    }

    if (realsize > 0) {
	// New allocate
	if (!fop_file_is_exist(allocfile)) {
	    if(!ext4_new_fallocfile(allocfile, realsize)) {
		return false;
	    }
	}
	else {
	    // existed allocate
	    // Check Block Data validity
	    if (!ext4_bdata_is_ok(allocfile)) {
		unlink(allocfile);
		if(!ext4_new_fallocfile(allocfile, realsize)) {
		    MD_LOGE("%s: reinit new failed(%d), %s\n", __func__, errno, strerror(errno));
		    return false;
		}
	    }
	    else {
		if (reset) {
		    if (!ext4_lba_mark_header(allocfile)) {
			unlink(allocfile);
			MD_LOGE("%s: mark header failed(%d), %s\n", __func__, errno, strerror(errno));
			return false;
		    }
		}
	    }
	}
	if (!ext4_set_sys_lbaooo(allocfile)) {
	    unlink(allocfile);
	    MD_LOGE("%s: set sys lbaooo failed(%d), %s\n", __func__, errno, strerror(errno));
	    return false;
	}
    }
    else {
	MD_LOGE("%s: Size (%" PRIu64 ") is <= zero (alloc size %" PRIu64 ")\n", __func__,
		realsize, allocsize);
        return false;
    }
    return true;
}

bool mrdump_file_fetch_zip_coredump(const char *outfile)
{
    unsigned int       blknum, coresize;
    unsigned int       rlba, bidx, BlockLBA[1024], lbainfo[EXT4_LBA_INFO_NUM];
    unsigned int       len, mylen;
    unsigned char      MyData[MRDUMP_EXT4_EXSPACE];
    int       fpRead, fpWrite, ret;

    // outfile
    if(outfile == NULL) {
        MD_LOGE("%s: outfile is NULL! (%d), %s\n", __func__, errno, strerror(errno));
        return false;
    }

    struct palloc_file *pfile = palloc_file_open(MRDUMP_EXT4_ALLOCATE_FILE);
    if (pfile == 0) {
	return false;
    }

    fpRead = ext4_open_device_node(MRDUMP_EXT4_MOUNT_POINT);
    if (fpRead < -1) {
        goto cleanup0;
    }

    if (!mrdump_ext4_read_infolba(fpRead, pfile->lbaooo, pfile->blksize, lbainfo)) {
        close(fpRead);
        goto cleanup0;
    }
    if (lbainfo[EXT4_CORE_DUMP_SIZE] == 0) {
        MD_LOGI("Ramdump size is 0, no data to dump\n");
        close(fpRead);
        goto cleanup0;
    }

    // Write handle
    fpWrite = open(outfile, O_RDWR | O_CREAT, 0400);
    if(0 > fpWrite) {
        MD_LOGE("%s: open Write handle open failed(%d), %s\n", __func__, errno, strerror(errno));
        close(fpRead);
        goto cleanup0;
    }

    // Init BlockLBA
    rlba = lbainfo[EXT4_2ND_LBA];
    if(-1 == ext4_block_lseek(fpRead, rlba, pfile->blksize)) {
        MD_LOGE("%s: lseek64 Init BlockLBA failed(%d), %s\n", __func__, errno, strerror(errno));
        close(fpRead);
        close(fpWrite);
        unlink(outfile);
        goto cleanup0;
    }
    if(0 > read(fpRead, BlockLBA, sizeof(BlockLBA))) {
        MD_LOGE("%s: fetch read BlockLBA error!(%d), %s\n", __func__, errno, strerror(errno));
        close(fpRead);
        close(fpWrite);
        unlink(outfile);
        goto cleanup0;
    }

    // Fetching data
    bidx    = 0;
    rlba    =  BlockLBA[bidx];

    coresize = lbainfo[EXT4_CORE_DUMP_SIZE];
    while(coresize > 0) {

        // counting coutinue datas...
        blknum = ext4_num_to_join((unsigned int *)BlockLBA, bidx);
        len = blknum * pfile->blksize;
        if(coresize < len) {
            mylen = coresize;
        } else {
            mylen = len;
        }

        // Reading data to MyData
        if(-1 == ext4_block_lseek(fpRead, rlba, pfile->blksize)) {
            MD_LOGE("%s: lseek64 Read MyData failed(%d), %s", __func__, errno, strerror(errno));
            close(fpRead);
            close(fpWrite);
            unlink(outfile);
            goto cleanup0;
        }
        if(0 > read(fpRead, MyData, mylen)) {
            MD_LOGE("%s: fetch MyData error!(%d), %s", __func__, errno, strerror(errno));
            close(fpRead);
            close(fpWrite);
            unlink(outfile);
            goto cleanup0;
        }
        if(0 > write(fpWrite, MyData, mylen)) {
            MD_LOGE("%s: fetch MyData error!(%d), %s", __func__, errno, strerror(errno));
            close(fpRead);
            close(fpWrite);
            unlink(outfile);
            goto cleanup0;
        }

        ret = ext4_get_next_bidx(fpRead, (unsigned int *)BlockLBA, bidx, pfile->blksize, blknum);
        if(ret < 0){
            MD_LOGE("%s: fpRead failed to get next block idx(%d), %s", __func__, errno, strerror(errno));
            close(fpRead);
            close(fpWrite);
            unlink(outfile);
            goto cleanup0;
        }
        bidx = (unsigned int)ret;

        if(bidx > MRDUMP_EXT4_LBA_PER_BLOCK) {
            close(fpRead);
            close(fpWrite);
            unlink(outfile);
            goto cleanup0;
        }
        if(bidx == MRDUMP_EXT4_LBA_PER_BLOCK) {
            ret = ext4_get_next_bidx(fpRead, (unsigned int *)BlockLBA, bidx, pfile->blksize, blknum);
            if(ret < 0) {
                MD_LOGE("%s: bidx(1022) failed to get next block idx(%d), %s", __func__, errno, strerror(errno));
                close(fpRead);
                close(fpWrite);
                unlink(outfile);
                goto cleanup0;
            }
            bidx = (unsigned int)ret;
        }
        rlba = BlockLBA[bidx];
        coresize -= mylen;
    }

    close(fpRead);
    close(fpWrite);

    MD_LOGI("Ramdump write to %s size %u\n", outfile, lbainfo[EXT4_CORE_DUMP_SIZE]);
    return true;

 cleanup0:
    palloc_file_close(pfile);
    return false;
}

void mrdump_file_set_maxsize(int mrdump_size)
{
    if(fop_file_is_exist(MRDUMP_EXT4_ALLOCATE_FILE))
        unlink(MRDUMP_EXT4_ALLOCATE_FILE);

    char s[64];
    uint64_t fsize = 0;

    if(mrdump_size < 0)
        mrdump_size = 0;

    switch(mrdump_size){
        case 0:
            property_set("persist.mtk.aee.allocate","0");
            fsize = 0;
            break;
        case 1:
            property_set("persist.mtk.aee.allocate","halfmem");
            fsize = mrdump_file_default_filesize();
            break;
        case 2:
            property_set("persist.mtk.aee.allocate","fullmem");
            fsize = mrdump_file_default_filesize();
            break;
        default:
            snprintf(s, sizeof(s), "%d", mrdump_size);
            property_set("persist.mtk.aee.allocate", s);
            fsize = (uint64_t)mrdump_size*1024*1024;
            break;
    }

    if(fsize > 0) {
        if (mrdump_ext4_reinit_allocfile(MRDUMP_EXT4_ALLOCATE_FILE, fsize, true)) {
            fop_file_write_string("/sys/module/mrdump/parameters/enable", "y");
            printf("set MT-RAMDUMP allocated file size => %" PRIu64 " MB].\n", (fsize/1024/1024));
        }
    } else {
        fop_file_write_string("/sys/module/mrdump/parameters/lbaooo", "0");
        fop_file_write_string("/sys/module/mrdump/parameters/enable", "n");
    }
}

/* System startup setup
 * Sanity check
 * 1. Check if /data partition filesystem is supported
 * 2. Check if lk/kernel MT-RAMDUMP support is enabled
 * 3. Check if mrdump file size is set to non-zero
 * If above condition is failed, remove pre-allocated file and disable MRDUMP.
 * Try to pre-allocated file
 * 1. If file doesn't exist, create a new file
 * 2. If file exist and header/body corrupted, re-create a new file
 */
void mrdump_file_setup(bool reset)
{
    /* check if mount as ext4 partition */
    if(!mount_as_ext4(MRDUMP_EXT4_MOUNT_POINT)) {
	goto cleanup0;
    }

    if (!mrdump_is_supported()) {
        MD_LOGE("MRDUMP is not supported\n");
        goto cleanup0;
    }

    if (!mrdump_ext4_reinit_allocfile(MRDUMP_EXT4_ALLOCATE_FILE, -1, reset)) {
        goto cleanup0;
    }
    return;

  cleanup0:
    /* remove pre-allocated file */
    unlink(MRDUMP_EXT4_ALLOCATE_FILE);
    /* Set kernel parameters to ensure MT-RAMDUMP EXT4 is disabled */
    fop_file_write_string("/sys/module/mrdump/parameters/lbaooo", "0");
    fop_file_write_string("/sys/module/mrdump/parameters/enable", "n");
}
