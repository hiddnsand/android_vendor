package com.mediatek.media.mediascanner;

import android.content.ContentValues;
import android.content.Context;
import android.drm.DrmManagerClient;
import android.media.MediaFile;
import android.media.MediaScanner;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import com.mediatek.media.MtkMediaStore;
import com.mediatek.media.mediascanner.MediaScannerClientEx;

public class MediaScannerClientExImpl extends MediaScannerClientEx {
    private static final String TAG = "MediaScannerClientExImpl";
    private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG) || "eng".equals(Build.TYPE);
    private static final String CLASS_NAME_MY_MEDIA_SCANNER_CLIENT = "MyMediaScannerClient";
    private static final String FIELD_NAME_MY_MEDIA_SCANNER_CLIENT = "mClient";
    private static final String FIELD_NAME_CONTEXT = "mContext";
    private static final String FIELD_NAME_IS_DRM = "mIsDrm";
    private static final String FIELD_NAME_FILE_TYPE = "mFileType";
    private static final String FIELD_NAME_MIME_TYPE = "mMimeType";
    private static final String METHOD_IS_DRM_ENABLED = "isDrmEnabled";
    private static Class sClassMyMediaScannerClient;
    private static Field sFieldContext;
    private static Field sFieldClient;
    private static Field sFieldIsDrm;
    private static Field sFieldFileType;
    private static Field sFieldMimeType;
    private static Method sMethodIsDrmEnabled;

    private ExMetaData mExMetaData;

    static {
        sFieldClient =
                getField(MediaScanner.class, FIELD_NAME_MY_MEDIA_SCANNER_CLIENT);
        sFieldContext = getField(MediaScanner.class, FIELD_NAME_CONTEXT);
        sMethodIsDrmEnabled = getMethod(MediaScanner.class, METHOD_IS_DRM_ENABLED);

        Class[] classes = MediaScanner.class.getDeclaredClasses();
        for (Class clazz : classes) {
            if (clazz.getSimpleName().equals(CLASS_NAME_MY_MEDIA_SCANNER_CLIENT)) {
                sClassMyMediaScannerClient = clazz;
                sFieldIsDrm = getField(sClassMyMediaScannerClient, FIELD_NAME_IS_DRM);
                sFieldIsDrm.setAccessible(true);
                sFieldFileType = getField(sClassMyMediaScannerClient, FIELD_NAME_FILE_TYPE);
                sFieldFileType.setAccessible(true);
                sFieldMimeType = getField(sClassMyMediaScannerClient, FIELD_NAME_MIME_TYPE);
                sFieldMimeType.setAccessible(true);
                break;
            }
        }
    }

    /**
     * Meta data extended by mediatek
     *
     * @hide
     */
    public static class ExMetaData {
        public String mDrmContentUr = null;
        public long mDrmOffset = -1;
        public long mDrmDataLen = -1;
        public String mDrmRightsIssuer = null;
        public String mDrmContentName = null;
        public String mDrmContentDescriptioin = null;
        public String mDrmContentVendor = null;
        public String mDrmIconUri = null;
        public long mDrmMethod = -1;
        /// M: Add for fancy gallery homepage.
        public int mOrientation = 0;
    }

    /**
     * New @{@link ExMetaData}
     *
     * @return
     * @hide
     */
    public void init() {
        mExMetaData = new ExMetaData();
    }

    /**
     * Parse meta data info from name and value, update meta data info to metaData
     *
     * @param name    key
     * @param value   value
     * @param scanner Current MediaScanner instance
     * @hide
     */
    public void parseExMetaDataFromStringTag(String name, String value, MediaScanner scanner) {
        if (name.equalsIgnoreCase("drm_content_uri")) {
            mExMetaData.mDrmContentUr = value.trim();
        } else if (name.equalsIgnoreCase("drm_offset")) {
            mExMetaData.mDrmOffset = parseSubstring(value, 0, 0);
        } else if (name.equalsIgnoreCase("drm_dataLen")) {
            mExMetaData.mDrmDataLen = parseSubstring(value, 0, 0);
        } else if (name.equalsIgnoreCase("drm_rights_issuer")) {
            mExMetaData.mDrmRightsIssuer = value.trim();
        } else if (name.equalsIgnoreCase("drm_content_name")) {
            mExMetaData.mDrmContentName = value.trim();
        } else if (name.equalsIgnoreCase("drm_content_description")) {
            mExMetaData.mDrmContentDescriptioin = value.trim();
        } else if (name.equalsIgnoreCase("drm_content_vendor")) {
            mExMetaData.mDrmContentVendor = value.trim();
        } else if (name.equalsIgnoreCase("drm_icon_uri")) {
            mExMetaData.mDrmIconUri = value.trim();
        } else if (name.equalsIgnoreCase("drm_method")) {
            mExMetaData.mDrmMethod = parseSubstring(value, 0, 0);
        } else if (name.equalsIgnoreCase("rotation")) {
            /// M: Add for fancy gallery homepage.
            mExMetaData.mOrientation = parseSubstring(value, 0, 0);
        }
    }

    /**
     * Add meta data info to content values for use with the Media Content Provider.
     *
     * @param map     The target map that wait to update
     * @param scanner Current MediaScanner instance
     * @hide
     */
    public void addExMetaDataToContentValues(ContentValues map, MediaScanner scanner) {
        Object client = getFieldOnObject(sFieldClient, scanner);
        if (client == null) {
            Log.e(TAG, "[addExMetaDataToContentValues] client is null, return");
            return;
        }
        boolean isDrm = (boolean) getFieldOnObject(sFieldIsDrm, client);
        if (isDrm) {
            map.put(MtkMediaStore.MediaColumns.DRM_CONTENT_DESCRIPTION,
                    mExMetaData.mDrmContentDescriptioin);
            map.put(MtkMediaStore.MediaColumns.DRM_CONTENT_NAME, mExMetaData.mDrmContentName);
            map.put(MtkMediaStore.MediaColumns.DRM_CONTENT_URI, mExMetaData.mDrmContentUr);
            map.put(MtkMediaStore.MediaColumns.DRM_CONTENT_VENDOR, mExMetaData.mDrmContentVendor);
            map.put(MtkMediaStore.MediaColumns.DRM_DATA_LEN, mExMetaData.mDrmDataLen);
            map.put(MtkMediaStore.MediaColumns.DRM_ICON_URI, mExMetaData.mDrmIconUri);
            map.put(MtkMediaStore.MediaColumns.DRM_OFFSET, mExMetaData.mDrmOffset);
            map.put(MtkMediaStore.MediaColumns.DRM_RIGHTS_ISSUER, mExMetaData.mDrmRightsIssuer);
            map.put(MtkMediaStore.MediaColumns.DRM_METHOD, mExMetaData.mDrmMethod);
        }
        /// M: Add for fancy gallery homepage
        int fileType = (int) getFieldOnObject(sFieldFileType, client);
        if (MediaFile.isVideoFileType(fileType)) {
            map.put(MtkMediaStore.VideoColumns.ORIENTATION, mExMetaData.mOrientation);
        }
    }

    public void correctFileType(String path, String mimeType, MediaScanner scanner) {
        Object client = getFieldOnObject(sFieldClient, scanner);
        if (client == null) {
            Log.e(TAG, "[correctFileType] client is null, return");
            return;
        }

        /// M: OMA DRM v1: however, for those DCF file, normally when scanning it,
        // the input mime type should be "application/vnd.oma.drm.content";
        // however, there's case that the input mimetype is, for example, "image/*"
        // in these cases it will not call processFile() but processImageFile() instead.
        // for these cases, we change the {mFileType} back to ZERO,
        // and let Media.getFileType(path) to determine the type,
        // so that it can call processFile() as normal. @{
        int fileType = (int) getFieldOnObject(sFieldFileType, client);
        if (MediaFile.isImageFileType(fileType)) {
            int lastDot = path.lastIndexOf(".");
            if (lastDot > 0 && path.substring(lastDot + 1).toUpperCase().equals("DCF")) {
                if (DEBUG) {
                    Log.v(TAG, "[correctFileType] detect a *.DCF file with input mime type = "
                            + mimeType);
                }
                if (sFieldFileType != null) {
                    try {
                        sFieldFileType.setInt(client, 0);
                    } catch (IllegalAccessException e) {
                        Log.e(TAG, "[correctFileType] IllegalAccessException", e);
                    }
                }
            }
        }
        /// @}
    }

    public void correctMetaData(String path, MediaScanner scanner) {
        Object client = getFieldOnObject(sFieldClient, scanner);
        if (client == null) {
            Log.e(TAG, "[correctFileType] client is null, return");
            return;
        }

        boolean isDrmEnabled = (boolean) callMethodOnObject(scanner, sMethodIsDrmEnabled);
        if (isDrmEnabled && path.endsWith(".mudp")) {
            DrmManagerClient dmc = new DrmManagerClient(
                    (Context) getFieldOnObject(sFieldContext, scanner));
            if (dmc.canHandle(path, null)) {

                if (sFieldMimeType != null) {
                    try {
                        sFieldMimeType.set(client, dmc.getOriginalMimeType(path));
                    } catch (IllegalAccessException e) {
                        Log.e(TAG, "[correctMetaData] IllegalAccessException", e);
                    }
                }

                if (sFieldIsDrm != null) {
                    try {
                        sFieldIsDrm.setBoolean(client, true);
                    } catch (IllegalAccessException e) {
                        Log.e(TAG, "[correctMetaData] IllegalAccessException", e);
                    }
                }

                if (DEBUG) {
                    Log.d(TAG, "[correctMetaData] get cta file " + path
                            + " with original mimetype = "
                            + getFieldOnObject(sFieldMimeType, client));
                }
            }
        }
    }

   /**
     * Add extension contentValues for image.
     * @param values The contentValues write to database.
     * @param path The FileEntry path.
     */
    public void putExtensionContentValuesForImage(ContentValues values, String path) {
        // add CAMERA_REFOCUS.
        int refocus = isStereoPhoto(path) ? 1 : 0;
        values.put(MtkMediaStore.ImageColumns.CAMERA_REFOCUS, refocus);
    }

    /**M: Added for parsing stereo info.
     * @{**/
    private static final String XMP_HEADER_START = "http://ns.adobe.com/xap/1.0/\0";
    private static final String XMP_EXT_MAIN_HEADER1 = "http://ns.adobe.com/xmp/extension/";
    private static final String NS_GDEPTH = "http://ns.google.com/photos/1.0/depthmap/";
    private static final String MTK_REFOCUS_PREFIX = "MRefocus";

    private static final int SOI = 0xFFD8;
    private static final int SOS = 0xFFDA;
    private static final int APP1 = 0xFFE1;
    private static final int APPXTAG_PLUS_LENGTHTAG_BYTE_COUNT = 4;

    /**
     * Check if current photo is stereo or not.
     * @param filePath
     *            file path of photo for checking
     * @return true if stereo photo, false if not stereo photo
     */
    public static boolean isStereoPhoto(String filePath) {
        if (filePath == null) {
            if (DEBUG) {
                Log.d(TAG, "<isStereoPhoto> filePath is null!!");
            }
            return false;
        }

        File srcFile = new File(filePath);
        if (!srcFile.exists()) {
            if (DEBUG) {
                Log.d(TAG, "<isStereoPhoto> " + filePath + " not exists!!!");
            }
            return false;
        }

        long start = System.currentTimeMillis();
        ArrayList<Section> sections = parseApp1Info(filePath);
        if (sections == null || sections.size() < 0) {
            if (DEBUG) {
                Log.d(TAG, "<isStereoPhoto> " + filePath + ", no app1 sections");
            }
            return false;
        }
        RandomAccessFile rafIn = null;
        try {
            rafIn = new RandomAccessFile(filePath, "r");
            for (int i = 0; i < sections.size(); i++) {
                Section section = sections.get(i);
                if (isStereo(section, rafIn)) {
                    if (DEBUG) {
                        Log.d(TAG, "<isStereoPhoto> " + filePath + " is stereo photo");
                    }
                    return true;
                }
            }
            if (DEBUG) {
                Log.d(TAG, "<isStereoPhoto> " + filePath + " is not stereo photo");
            }
            return false;
        } catch (FileNotFoundException e) {
            Log.e(TAG, "<isStereoPhoto> FileNotFoundException:", e);
            return false;
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "<isStereoPhoto> IllegalArgumentException:", e);
            return false;
        } finally {
            try {
                if (rafIn != null) {
                    rafIn.close();
                    rafIn = null;
                }
            } catch (IOException e) {
                Log.e(TAG, "<isStereoPhoto> IOException:", e);
            }
            if (DEBUG) {
                Log.d(TAG, "<isStereoPhoto> <performance> costs(ms): "
                    + (System.currentTimeMillis() - start));
            }
        }
    }

    private static boolean isStereo(Section section, RandomAccessFile rafIn) {
        try {
            if (section.mIsXmpMain) {
                rafIn.seek(section.mOffset + 2);
                int len = rafIn.readUnsignedShort() - 2;
                rafIn.skipBytes(XMP_HEADER_START.length());
                byte[] xmpBuffer = new byte[len - XMP_HEADER_START.length()];
                rafIn.read(xmpBuffer, 0, xmpBuffer.length);
                String xmpContent = new String(xmpBuffer);
                if (xmpContent == null) {
                    if (DEBUG) {
                        Log.d(TAG, "<isStereo> xmpContent is null");
                    }
                    return false;
                }
                if (xmpContent.contains(MTK_REFOCUS_PREFIX)) {
                    return true;
                }
            }
            return false;
        } catch (IOException e) {
            Log.e(TAG, "<isStereo> IOException:", e);
            return false;
        }
    }

    private static ArrayList<Section> parseApp1Info(String filePath) {
        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(filePath, "r");
            int value = raf.readUnsignedShort();
            if (value != SOI) {
                if (DEBUG) {
                    Log.d(TAG, "<parseApp1Info> error, find no SOI");
                }
                return new ArrayList<Section>();
            }
            int marker = -1;
            long offset = -1;
            int length = -1;
            ArrayList<Section> sections = new ArrayList<Section>();

            while ((value = raf.readUnsignedShort()) != -1 && value != SOS) {
                marker = value;
                offset = raf.getFilePointer() - 2;
                length = raf.readUnsignedShort();
                if (value == APP1) {
                    Section section =
                            new Section(marker, offset, length);
                    long currentPos = raf.getFilePointer();
                    section = checkIfMainXmpInApp1(raf, section);
                    if (section != null && section.mIsXmpMain) {
                        sections.add(section);
                        break;
                    }
                    raf.seek(currentPos);
                }
                raf.skipBytes(length - 2);
            }

            return sections;
        } catch (IOException e) {
            Log.e(TAG, "<parseApp1Info> IOException, path " + filePath, e);
            return null;
        } finally {
            try {
                if (raf != null) {
                    raf.close();
                    raf = null;
                }
            } catch (IOException e) {
                Log.e(TAG, "<parseApp1Info> IOException, path " + filePath, e);
            }
        }
    }

    private static Section checkIfMainXmpInApp1(RandomAccessFile raf, Section section) {
        if (section == null) {
            if (DEBUG) {
                Log.d(TAG, "<checkIfMainXmpInApp1> section is null!!!");
            }
            return null;
        }
        byte[] buffer = null;
        String str = null;
        try {
            if (section.mMarker == APP1) {
                raf.seek(section.mOffset + APPXTAG_PLUS_LENGTHTAG_BYTE_COUNT);
                buffer = new byte[XMP_EXT_MAIN_HEADER1.length()];
                raf.read(buffer, 0, buffer.length);
                str = new String(buffer, 0, XMP_HEADER_START.length());
                if (XMP_HEADER_START.equals(str)) {
                    section.mIsXmpMain = true;
                }
            }
            return section;
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "<checkIfMainXmpInApp1> UnsupportedEncodingException" + e);
            return null;
        } catch (IOException e) {
            Log.e(TAG, "<checkIfMainXmpInApp1> IOException" + e);
            return null;
        }
    }

    /**
     * APP Section.
     */
    private static class Section {
        // e.g. 0xffe1, exif
        public int mMarker;
        // marker offset from start of file
        public long mOffset;
        // app length, follow spec, include 2 length bytes
        public int mLength;
        public boolean mIsXmpMain;

        /**
          * Create a Section.
          * @param marker section mark
          * @param offset section address offset
          * @param length section length
          */
        public Section(int marker, long offset, int length) {
            mMarker = marker;
            mOffset = offset;
            mLength = length;
        }
    }
    /**@}**/

    private static int parseSubstring(String s, int start, int defaultValue) {
        int length = s.length();
        if (start == length) return defaultValue;

        char ch = s.charAt(start++);
        // return defaultValue if we have no integer at all
        if (ch < '0' || ch > '9') return defaultValue;

        int result = ch - '0';
        while (start < length) {
            ch = s.charAt(start++);
            if (ch < '0' || ch > '9') return result;
            result = result * 10 + (ch - '0');
        }

        return result;
    }

    private static Field getField(Class<?> clazz, String fieldName) {
        try {
            Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field;
        } catch (NoSuchFieldException e) {
            Log.e(TAG, "[getField]", e);
        }
        return null;
    }

    private static Object getFieldOnObject(Field field, Object object) {
        if (field != null) {
            try {
                return field.get(object);
            } catch (IllegalAccessException e) {
                Log.e(TAG, "[getFieldOnObject]", e);
            }
        }
        return null;
    }

    public static Method getMethod(Class<?> clazz, String methodName,
                                   Class<?>... parameterTypes) {
        try {
            Method method = clazz.getDeclaredMethod(methodName, parameterTypes);
            method.setAccessible(true);
            return method;
        } catch (NoSuchMethodException e) {
            Log.e(TAG, "[getMethod]", e);
        }
        return null;
    }

    public static Object callMethodOnObject(Object receiver, Method method, Object... args) {
        try {
            return method.invoke(receiver, args);
        } catch (InvocationTargetException e2) {
            Log.e(TAG, "[callMethodOnObject]", e2);
        } catch (IllegalAccessException e3) {
            Log.e(TAG, "[callMethodOnObject]", e3);
        }
        return null;
    }
}