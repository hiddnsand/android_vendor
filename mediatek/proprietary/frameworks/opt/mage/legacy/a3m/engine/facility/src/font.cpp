/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2011 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Font Implementaion
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <algorithm> /* std::copy etc. */
#include <sstream> /* std::ostringstream */
#include <a3m/log.h> /* A3M log API */
#include <a3m/font.h> /* Font API */

namespace
{

  /*
   * Appends a font size onto a font name to give the full asset name.
   */
  std::string getFontName(std::string const& name, A3M_UINT32 size)
  {
    std::ostringstream stream;
    stream << name << ";" << size;
    return stream.str();
  }

} // namespace

namespace a3m
{
  FontCache::FontCache(Texture2DCache::Ptr const& texture2DCache) :
    m_texture2DCache(texture2DCache)
  {
  }

  Font::Ptr FontCache::create(
    Texture2D::Ptr const& texture,
    A3M_INT32 size,
    A3M_FLOAT ascent,
    A3M_FLOAT descent,
    A3M_FLOAT lineGap,
    A3M_INT32 firstCharacter,
    A3M_INT32 nCharacters,
    Font::CharacterData const* characterData,
    A3M_CHAR8 const* name)
  {
    Font::Ptr font(new Font(texture, size, ascent, descent, lineGap,
                            firstCharacter, nCharacters, characterData));
    add(font, name);

    return font;
  }

  Font::Ptr get(
    FontCache& cache,
    A3M_CHAR8 const* name,
    A3M_UINT32 size)
  {
    return cache.get(getFontName(name, size).c_str());
  }

  A3M_BOOL exists(
    FontCache& cache,
    A3M_CHAR8 const* name,
    A3M_UINT32 size)
  {
    return cache.exists(getFontName(name, size).c_str());
  }

  /*
   * Constructor.
   */
  Font::Font( Texture2D::Ptr const& texture,
              A3M_INT32 size,
              A3M_FLOAT ascent,
              A3M_FLOAT descent,
              A3M_FLOAT lineGap,
              A3M_INT32 firstCharacter,
              A3M_INT32 nCharacters,
              CharacterData const* characterData )
    :  m_texture( texture ),
       m_size( size ),
       m_ascent( ascent ),
       m_descent( descent ),
       m_lineGap( lineGap ),
       m_firstCharacter( firstCharacter ),
       m_nCharacters( nCharacters ),
       m_characterData( 0 )
  {
    if( nCharacters > 0 )
    {
      m_characterData =
        new CharacterData[ static_cast< A3M_UINT32 >( nCharacters ) ];
      std::copy( characterData, characterData + nCharacters, m_characterData );
    }
  }

  /*
   * Destructor
   */
  Font::~Font()
  {
    delete [] m_characterData;
  }

  /*
   * Get bounding box.
   */
  Font::CharacterData const& Font::getCharacterData( A3M_INT32 character ) const
  {
    static CharacterData nullBox;

    if( m_characterData &&
        ( character >= m_firstCharacter ) &&
        ( character < ( m_firstCharacter + m_nCharacters ) ) )
    {
      return m_characterData[ character - m_firstCharacter ];
    }
    else
    {
      return nullBox;
    }
  }

} /* namespace a3m */
