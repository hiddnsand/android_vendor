#ifndef MLOG_H
#define MLOG_H

#ifdef MTK_LOG_ENABLE
#undef MTK_LOG_ENABLE
#endif
#define MTK_LOG_ENABLE 1

#define TO_DATA     (0)
#define TO_SDCARD   (1)
#define TO_BOTH     (2)

#define MLOG(type, ...) __mb_log(type, __VA_ARGS__)
#define MLOGE(type, ...)    \
    ({                      \
        MLOG(type, __VA_ARGS__);    \
        ALOGE(__VA_ARGS__); \
     })

#define MLOGI(type, ...)    \
    ({                      \
        MLOG(type, __VA_ARGS__);    \
        ALOGI(__VA_ARGS__); \
     })

#ifndef MBLOG_DEBUG
#define MLOGD(...)          ((void)0)
#else
#define MLOGD(...)          MLOGE(TO_DATA, __VA_ARGS__)
#endif
#define MLOGE_DATA(...)     MLOGE(TO_DATA, __VA_ARGS__)
#define MLOGI_DATA(...)     MLOGI(TO_DATA, __VA_ARGS__)
#define MLOGE_SDCARD(...)   MLOGE(TO_SDCARD, __VA_ARGS__)
#define MLOGI_SDCARD(...)   MLOGI(TO_SDCARD, __VA_ARGS__)
#define MLOGE_BOTH(...)     MLOGE(TO_BOTH, __VA_ARGS__)
#define MLOGI_BOTH(...)     MLOGI(TO_BOTH, __VA_ARGS__)

#define WARN_CONFIG_FAIL    0
#define WARN_SOCKET_FAIL    1
#define WARN_LISTEN_FAIL    2
#define WARN_PTHREAD_FAIL   3
#define WARN_COPY_FAIL      4
#define WARN_OUTPUT_FAIL    5
#define WARN_READLOG_FAIL   6
#define WARN_SHM_FAIL       7
#define WARN_FORMAT_FAIL    8
#define WARN_ROTATE_FAIL    9
#define WARN_DEVICE_FAIL    10
#define WARN_SDFULL_FAIL    11
#define WARN_TIMEOUT_FAIL   12

void __mb_log(int type, char *fmt, ...);

#endif
