##
# Here set for particular operators features for dynamic configuration
##

#reset all features here with default value: we should use new feature options other than used in projectconfig.mk file otherwise there will conflict with global vaiables whose purpose is different
#Since these variables are used only for property overridden under CIP

## This is default operator config file

# Set for carrier express default values
USP_OPERATOR_APK_PATH :=
USP_OPERATOR_PACKAGES :=
USP_OPERATOR_FEATURES := MTK_EPDG_CIP_SUPPORT=yes \
                MTK_WFC_SUPPORT=yes \
                MTK_VOLTE_SUPPORT=yes \
                MTK_VILTE_SUPPORT=yes \
                MTK_IMS_SUPPORT=yes \
                MTK_FLIGHT_MODE_POWER_OFF_MD=yes \
                MTK_DTAG_DUAL_APN_SUPPORT=no \
                MTK_MULTI_SIM_SUPPORT=ss \
                MTK_SHARE_MODEM_CURRENT=1 \
                MTK_RCS_UA_SUPPORT=no \
                MTK_SIM_SWITCH_POLICY=1 \
                MTK_DISABLE_CAPABILITY_SWITCH=no \
                MTK_CC33_SUPPORT=no \
                MTK_CLR_CODE_SUPPORT=no


ifdef OPTR_SPEC_SEG_DEF
ifneq ($(strip $(OPTR_SPEC_SEG_DEF)),NONE)
OP := $(word 1, $(subst _,$(space),$(OPTR_SPEC_SEG_DEF)))
SP := $(word 2, $(subst _,$(space),$(OPTR_SPEC_SEG_DEF)))
SG := $(word 3, $(subst _,$(space),$(OPTR_SPEC_SEG_DEF)))
ifneq ("$(wildcard vendor/mediatek/proprietary/operator/SPEC/$(OPTR)/$(SPEC)/$(SEG)/$(MTK_TARGET_PROJECT).mk)","")
-include vendor/mediatek/proprietary/operator/legacy/$(OP)/$(SP)/$(SG)/$(MTK_TARGET_PROJECT).mk
else ifneq ("$(wildcard vendor/mediatek/proprietary/operator/SPEC/$(OPTR)/$(SPEC)/$(SEG)/$(TARGET_BOARD_PLATFORM).mk)","")
-include vendor/mediatek/proprietary/operator/legacy/$(OP)/$(SP)/$(SG)/$(TARGET_BOARD_PLATFORM).mk
else
-include vendor/mediatek/proprietary/operator/SPEC/$(OP)/$(SP)/$(SG)/UspOperatorConfig.mk
endif
endif
endif