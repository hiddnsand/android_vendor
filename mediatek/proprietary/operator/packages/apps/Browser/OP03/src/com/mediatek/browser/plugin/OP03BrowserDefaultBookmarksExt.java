
/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.browser.plugin;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.SystemProperties;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.browser.provider.BrowserContract;
import com.mediatek.browser.ext.DefaultBrowserRegionalPhoneExt;
//import com.mediatek.common.PluginImpl;
//import com.mediatek.op03.plugin.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

//@PluginImpl(interfaceName = "com.mediatek.browser.ext.IBrowserRegionalPhoneExt")
public class OP03BrowserDefaultBookmarksExt extends DefaultBrowserRegionalPhoneExt {

    private static final String TAG = "OP03BrowserDefaultBookmarksExt";
    private static final int OP03_OPERATOR_ID = 3;

    private static final String GOOGLE_URL = "http://www.google.com/";
    private static final String TABLE_BOOKMARKS = "bookmarks";
    private static final String TABLE_IMAGES = "images";
    private static final long FIXED_ID_ROOT = 1;
    private Context mContext = null;
    private static final String SHARED_PREFRENCE_STRING = "default_bookmarks_operator";
    public static final String CURRENT_OPERATOR_PREF = "operator_id";
    private SharedPreferences mPrefs;
    // M : flag for auto on/off log
    final static boolean DEBUG =
        (!Build.TYPE.equals("user")) ? true :
            SystemProperties.getBoolean("ro.mtk_browser_debug_enablelog", false);

    public OP03BrowserDefaultBookmarksExt(Context context) {
        mContext = context;
    }
    /**
     * Update default bookmarks for OP03(Orange).
     * @param cntx The Context
     */
    public void updateBookmarks(Context context) {
        /*if (readPrefValue(cntx, CURRENT_OPERATOR_PREF) == OP03_OPERATOR_ID) {
            Log.i(TAG, "Current OP bookmarks = " + OP03_OPERATOR_ID + " ,no need update");
            return true;
        }*/
        if (needUpdateBookmarks(context)) {
            Uri bkmUri = BrowserContract.Bookmarks.CONTENT_URI;
            SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = p.edit();
            editor.putString("operator_bookmarks", "OP03");
            editor.commit();
            removeOMBookmarks(context);
            //Remove Default OM bookmarks.
            //Add OP03 bookmarks.
            TypedArray preloads = null;
            Log.i(TAG, "Enter: addDefaultBookmarksForCustomer OP03 implement");
            Resources res = mContext.getResources();
            final CharSequence[] bookmarks = res.getTextArray(
                    R.array.bookmarks_for_op03);
            preloads = res.obtainTypedArray(R.array.bookmark_preloads_for_op03);
            int size = bookmarks.length;
            if (DEBUG) {
                Log.i(TAG, "bookmarks count = " + size);
            }
            try {
                String parent = Long.toString(FIXED_ID_ROOT);
                String now = Long.toString(System.currentTimeMillis());
                for (int i = 0; i < size; i = i + 2) {
                    Uri newUri = null;
                    boolean isUpdated = false;
                    CharSequence bookmarkDestination = bookmarks[i + 1];
                    int thumbId = preloads.getResourceId(i + 1, 0);
                    byte[] thumb = null, favicon = null;
                    try {
                            thumb = readRaw(res, thumbId);
                        } catch (IOException e) {
                              Log.i(TAG, "IOException for thumb");
                        }
                    ContentValues values = new ContentValues();
                    values.put(BrowserContract.Bookmarks.TITLE, bookmarks[i].toString());
                    values.put(BrowserContract.Bookmarks.URL, bookmarkDestination.toString());
                    values.put(BrowserContract.Bookmarks.IS_FOLDER, 0);
                    values.put(BrowserContract.Bookmarks.THUMBNAIL, thumb);
                    values.put(BrowserContract.Bookmarks.PARENT, parent);
                    values.put(BrowserContract.Bookmarks.POSITION, (Integer.toString(i)));
                    values.put(BrowserContract.Bookmarks.DATE_CREATED, now);
                    newUri = mContext.getContentResolver().insert(bkmUri, values);
                    if (newUri != null) {
                        isUpdated = true;
                    }
                    Log.i(TAG, "for " + i + "update result = " + isUpdated);
               }
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.i(TAG, "ArrayIndexOutOfBoundsException is caught");
            } finally {
                preloads.recycle();
                //updatePrefValue(CURRENT_OPERATOR_PREF, OP03_OPERATOR_ID);
            }
        }
           // return size;
    }

    private byte[] readRaw(Resources res, int id) throws IOException {
        if (id == 0) {
            return null;
        }
        InputStream is = res.openRawResource(id);
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[4096];
            int read;
            while ((read = is.read(buf)) > 0) {
                bos.write(buf, 0, read);
            }
            bos.flush();
            return bos.toByteArray();
        } finally {
            is.close();
        }
    }

    public void removeOMBookmarks(Context context) {
        Resources res = null;
        Uri bkmUri = BrowserContract.Bookmarks.CONTENT_URI;
        try {
            res = context.getPackageManager().getResourcesForApplication("com.android.browser");
            int resourceId = res.getIdentifier("com.android.browser:array/bookmarks_for_yahoo"
                                            , null, null);
            Log.d(TAG , "yahoo resourceId = " + resourceId);
            CharSequence[] bookmarks = null;
            int delCount = 0;
            if (0 != resourceId) {
                bookmarks = res.getTextArray(resourceId);
            }
            if (bookmarks != null) {
                if (DEBUG) {
                    Log.d(TAG, " yahoo bkms size = " + bookmarks.length);
                }
                int size = (bookmarks.length / 2);
                String[] mEntriesString = new String[size];
                for (int i = 0; i < size ; i++) {
                    CharSequence ch = bookmarks[2*i+1];
                    mEntriesString[i] = ch.toString();
                }
                delCount = context.getContentResolver().delete(bkmUri
                    , "url" + makeInQueryString(size), mEntriesString);
                Log.d(TAG, "Delete count = " + delCount);
            }
            resourceId = res.getIdentifier("com.android.browser:array/bookmarks", null, null);
            Log.d(TAG , "other resourceId = " + resourceId);
            bookmarks = null;
            if(0 != resourceId) {
                bookmarks = res.getTextArray(resourceId);
            }
            if (bookmarks != null) {
                Log.d(TAG, " other bkms size = " + bookmarks.length);
                int size = (bookmarks.length / 2);
                String[] mEntriesString = new String[size];
                for (int i= 0; i < size ; i++) {
                    CharSequence ch = bookmarks[2*i+1];
                    mEntriesString[i] = ch.toString();
                }
                delCount = context.getContentResolver().delete(BrowserContract.Bookmarks.CONTENT_URI
                    , "url" + makeInQueryString(size), mEntriesString);
                Log.d(TAG, "Other Delete count = " + delCount);
            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String makeInQueryString(int size) {
        StringBuilder sb = new StringBuilder();
        if (size > 0) {
            sb.append(" IN ( ");
            String placeHolder = "";
            for (int i = 0; i < size; i++) {
                sb.append(placeHolder);
                sb.append("?");
                placeHolder = ",";
            }
            sb.append(" )");
        }
        return sb.toString();
    }

    private int readPrefValue(Context cntx, String prefKey) {
        mPrefs = cntx.getSharedPreferences(SHARED_PREFRENCE_STRING, 0);
        int operatorID = mPrefs.getInt(prefKey, 0);
        return operatorID;
    }

    private void updatePrefValue(String prefKey, int prefValue) {
        SharedPreferences.Editor edit = mPrefs.edit();
        edit.putInt(prefKey, prefValue);
        edit.commit();
    }

    private boolean needUpdateBookmarks(Context context) {
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
        String sysProperty = SystemProperties.get("persist.operator.optr");
        Log.d(TAG, "system property = " + sysProperty);
        String currentOperator = p.getString("operator_bookmarks", "OPNONE");
        Log.d(TAG, "currentOperator = " + currentOperator);
        boolean opUpdated = !(sysProperty.equals(currentOperator));
        return opUpdated;
    }
  }