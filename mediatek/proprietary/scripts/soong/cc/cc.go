package mtkCc

import (
	"reflect"
	"strings"

	"android/soong/android"
	"android/soong/cc"
	"android/soong/android/mediatek"
)

func init() {
	android.RegisterModuleType("mtk_cc_library", mtkLibrary)
	android.RegisterModuleType("mtk_cc_library_static", mtkLibraryStatic)
	android.RegisterModuleType("mtk_cc_library_shared", mtkLibraryShared)
	android.RegisterModuleType("mtk_cc_binary", mtkBinary)

	android.RegisterModuleType("mtk_cc_defaults", mtkCcDefaultsFactory)
	android.RegisterModuleType("mtk_global_defaults", mtkGlobalDefaultsFactory)
}

func mtkCcLoadHook(ctx android.LoadHookContext, c interface{}) {
	customStruct := reflect.ValueOf(c).Elem().FieldByName("Mediatek_variables")
	for j := 0; j < customStruct.NumField(); j++ {
		variableStruct := customStruct.Field(j)
		if variableStruct.Kind() == reflect.Ptr {
			variableStruct = variableStruct.Elem()
		}
		if variableStruct.Kind() == reflect.Struct {
			variableName := customStruct.Type().Field(j).Name
			featureName := strings.ToUpper(variableName)
			featureBool := true
			if strings.HasPrefix(variableName, "Not_") {
				featureBool = false
				featureName = strings.ToUpper(strings.TrimLeft(variableName, "Not_"))
			}
			featureValue := mediatek.GetFeature(featureName)
			if ((featureBool == true) && (featureValue == "yes" || featureValue == "true")) ||
				((featureBool == false) && (featureValue != "yes") && (featureValue != "true")) {
				ctx.AppendProperties(variableStruct.Addr().Interface())
			}
		}
	}
	return
}

func mtkLibrary() android.Module {
	m, _ := cc.NewLibrary(android.HostAndDeviceSupported)
	module := m.Init()
	c := mediatek.InitVariableProperties().Interface()
	module.AddProperties(c)
	android.AddLoadHook(module, func(ctx android.LoadHookContext) { mtkCcLoadHook(ctx, c) })
	return module
}

func mtkLibraryStatic() android.Module {
	m, library := cc.NewLibrary(android.HostAndDeviceSupported)
	library.BuildOnlyStatic()
	module := m.Init()
	c := mediatek.InitVariableProperties().Interface()
	module.AddProperties(c)
	android.AddLoadHook(module, func(ctx android.LoadHookContext) { mtkCcLoadHook(ctx, c) })
	return module
}

func mtkLibraryShared() android.Module {
	m, library := cc.NewLibrary(android.HostAndDeviceSupported)
	library.BuildOnlyShared()
	module := m.Init()
	c := mediatek.InitVariableProperties().Interface()
	module.AddProperties(c)
	android.AddLoadHook(module, func(ctx android.LoadHookContext) { mtkCcLoadHook(ctx, c) })
	return module
}

func mtkBinary() android.Module {
	m, _ := cc.NewBinary(android.HostAndDeviceSupported)
	module := m.Init()
	c := mediatek.InitVariableProperties().Interface()
	module.AddProperties(c)
	android.AddLoadHook(module, func(ctx android.LoadHookContext) { mtkCcLoadHook(ctx, c) })
	return module
}

func mtkCcDefaultsFactory() android.Module {
	module := cc.DefaultsFactory()
	c := mediatek.InitVariableProperties().Interface()
	module.AddProperties(c)
	android.AddLoadHook(module, func(ctx android.LoadHookContext) { mtkCcLoadHook(ctx, c) })
	return module
}

func mtkGlobalDefaultsFactory() android.Module {
	module := cc.DefaultsFactory()
	android.AddLoadHook(module, mtkGlobalDefaults)
	return module
}
