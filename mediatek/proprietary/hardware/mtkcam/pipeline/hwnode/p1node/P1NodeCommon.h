/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_INCLUDE_MTKCAM_PIPELINE_HWNODE_P1NODE_COMMON_H_
#define _MTK_HARDWARE_INCLUDE_MTKCAM_PIPELINE_HWNODE_P1NODE_COMMON_H_
//
#include <sys/prctl.h>
#include <sys/resource.h>
//
#include <system/thread_defs.h>
#include <cutils/properties.h>
#include <utils/Atomic.h>
#include <utils/RWLock.h>
#include <utils/Thread.h>
//
#include <vector>
#include <queue>
//
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/utils/std/common.h>
#include <mtkcam/utils/std/DebugScanLine.h>
//
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
//
#include <mtkcam/utils/metastore/IMetadataProvider.h>
//
#include <mtkcam/utils/imgbuf/IIonImageBufferHeap.h>
#include <mtkcam/utils/imgbuf/IDummyImageBufferHeap.h>
//
#include <mtkcam/drv/iopipe/CamIO/INormalPipe.h>
#include <mtkcam/drv/iopipe/CamIO/rss_cb.h>
#include <mtkcam/aaa/IHal3A.h>
#include <mtkcam/aaa/lcs/lcs_hal.h>
#include <mtkcam/feature/vhdr/vhdr_hal.h>
//
#include <mtkcam/pipeline/stream/IStreamInfo.h>
#include <mtkcam/pipeline/stream/IStreamBuffer.h>
#include <mtkcam/pipeline/utils/streambuf/IStreamBufferPool.h>
#include <mtkcam/pipeline/hwnode/P1Node.h>
//
#include <mtkcam/utils/hw/GyroCollector.h>
//
#include "../BaseNode.h"
#include "../hwnode_utilities.h"
#include "../Profile.h"
//
//
#if (HWNODE_HAVE_AEE_FEATURE)
#include <aee.h>
#ifdef AEE_ASSERT
#undef AEE_ASSERT
#endif
#define AEE_ASSERT(String) \
    do { \
        CAM_LOGE("ASSERT("#String") fail"); \
        aee_system_exception( \
            LOG_TAG, \
            NULL, \
            DB_OPT_DEFAULT, \
            String); \
    } while(0)
#else
#define AEE_ASSERT(String)
#endif
//
using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
//using namespace NSCam::Utils;
using namespace NSCam::Utils::Sync;
using namespace NSCam::NSIoPipe;
using namespace NSCam::NSIoPipe::NSCamIOPipe;
using namespace NS3Av3;

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)                  CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)                  CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)                  CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)                  CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)                  CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)                  CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)                  CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD_WITH_OPENID(fmt, arg...)      CAM_LOGD("[%s] [Cam::%d] " fmt, __FUNCTION__, getOpenId(), ##arg)
#define MY_LOGI_WITH_OPENID(fmt, arg...)      CAM_LOGI("[%s] [Cam::%d] " fmt, __FUNCTION__, getOpenId(), ##arg)

//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF_P1(cond, ...)    do { if ( (cond) ) { MY_LOGD_WITH_OPENID(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF_P1(cond, ...)    do { if ( (cond) ) { MY_LOGI_WITH_OPENID(__VA_ARGS__); } }while(0)

/******************************************************************************
 *
 ******************************************************************************/

#define MY_LOGD1(...)           MY_LOGD_IF_P1(1<=mLogLevel, __VA_ARGS__)
#define MY_LOGD2(...)           MY_LOGD_IF_P1(2<=mLogLevel, __VA_ARGS__)
#define MY_LOGD3(...)           MY_LOGD_IF_P1(3<=mLogLevel, __VA_ARGS__)

#define MY_LOGI1(...)           MY_LOGI_IF_P1(1<=mLogLevelI, __VA_ARGS__)
#define MY_LOGI2(...)           MY_LOGI_IF_P1(2<=mLogLevelI, __VA_ARGS__)
#define MY_LOGI3(...)           MY_LOGI_IF_P1(3<=mLogLevelI, __VA_ARGS__)

#define FUNCTION_S_IN           MY_LOGD_IF(1<=mLogLevel, "+");
#define FUNCTION_S_OUT          MY_LOGD_IF(1<=mLogLevel, "-");
#define FUNCTION_IN             MY_LOGD_IF(2<=mLogLevel, "+");
#define FUNCTION_OUT            MY_LOGD_IF(2<=mLogLevel, "-");
#define PUBLIC_APIS_IN          MY_LOGD_IF_P1(1<=mLogLevel, "API +");
#define PUBLIC_APIS_OUT         MY_LOGD_IF_P1(1<=mLogLevel, "API -");
#define PUBLIC_API_IN           MY_LOGD_IF_P1(2<=mLogLevel, "API +");
#define PUBLIC_API_OUT          MY_LOGD_IF_P1(2<=mLogLevel, "API -");
#define FUNCTION_P1S_IN         MY_LOGD_IF_P1(1<=mLogLevel, "+");
#define FUNCTION_P1S_OUT        MY_LOGD_IF_P1(1<=mLogLevel, "-");
#define FUNCTION_P1_IN          MY_LOGD_IF_P1(2<=mLogLevel, "+");
#define FUNCTION_P1_OUT         MY_LOGD_IF_P1(2<=mLogLevel, "-");

#define IS_P1_LOGI              (MTKCAM_HW_NODE_LOG_LEVEL_DEFAULT >= 2) // for system use LOGI
#define IS_P1_LOGD              (MTKCAM_HW_NODE_LOG_LEVEL_DEFAULT >= 3) // for system use LOGD
#define P1_LOG_LEN              (256)

#define P1_SUPPORT_DIR_RESTREAM (1)

#ifdef P1_LOG
#undef P1_LOG
#endif
#define P1_LOG                                                      \
    char strLog[P1_LOG_LEN] = {0};                                  \
    snprintf(strLog, P1_LOG_LEN,                                    \
        "Cam::%d R%d S%d E%d D%d O%d #%d@%d", getOpenId(),          \
        mTagReq.get(), mTagSet.get(),                               \
        mTagEnq.get(), mTagDeq.get(),                               \
        mTagOut.get(), mTagList.get(), mReqState);

/* P1_LOGD only use in P1NodeImp */
#if (IS_P1_LOGI) // for system use LOGI
#define P1_LOGI(lv, fmt, arg...)                                    \
    do {                                                            \
        if (mLogLevelI >= lv) {                                     \
            P1_LOG;                                                 \
            CAM_LOGI("[%s] [%s] " fmt, __FUNCTION__, strLog, ##arg);\
        }                                                           \
    } while(0)
#else
#define P1_LOGI(lv, fmt, arg...)
#endif

/* P1_LOGD only use in P1NodeImp */
#if (IS_P1_LOGD) // for system use LOGD
#define P1_LOGD(lv, fmt, arg...)                                    \
    do {                                                            \
        if (mLogLevel >= lv) {                                      \
            P1_LOG;                                                 \
            CAM_LOGD("[%s] [%s] " fmt, __FUNCTION__, strLog, ##arg);\
        }                                                           \
    } while(0)
#else
#define P1_LOGD(lv, fmt, arg...)
#endif

#ifdef P1_CAM_TRACE_FUNC
#undef P1_CAM_TRACE_FUNC
#endif
#define P1_CAM_TRACE_FUNC        CAM_TRACE_NAME(__FUNCTION__)

#define P1THREAD_POLICY         (SCHED_OTHER)
#define P1THREAD_PRIORITY       (ANDROID_PRIORITY_FOREGROUND-2)

#define P1SOFIDX_INIT_VAL       (0)
#define P1SOFIDX_LAST_VAL       (0xFF)
#define P1SOFIDX_NULL_VAL       (0xFFFFFFFF)

#define P1GET_FRM_NUM(frame) ((frame == NULL) ?\
                                (MUINT32)0xFFFFFFFF : frame->getFrameNo())
#define P1GET_REQ_NUM(frame) ((frame == NULL) ?\
                                (MUINT32)0xFFFFFFFF : frame->getRequestNo())

#define P1INFO_NODE_STR "(%d)(%d:%d)@(%d)[T:%d O:0x%x R:0x%x S:%d C:%d F:%d]"
#define P1INFO_NODE_VAR(node) node.magicNum,\
    P1GET_FRM_NUM(node.appFrame), P1GET_REQ_NUM(node.appFrame), node.sofIdx,\
    node.reqType, node.reqOutSet, node.expRec, node.exeState, node.capType,\
    node.needFlush

#define P1INFO_STREAM_IMG_STR "StreamImg[%d](%#" PRIx64 ")"\
    "[ImgBuf:%p-H:%p SB:%p L:%d T:%d]"
#define P1INFO_STREAM_IMG_VAR(node) streamImg,\
    mvStreamImg[streamImg]->getStreamId(),\
    node.streamBufImg[streamImg].spImgBuf.get(),\
    ((node.streamBufImg[streamImg].spImgBuf != NULL) ?\
    (node.streamBufImg[streamImg].spImgBuf->getImageBufferHeap()) : (NULL)),\
    node.streamBufImg[streamImg].spStreamBuf.get(),\
    node.streamBufImg[streamImg].eLockState,\
    node.streamBufImg[streamImg].eSrcType

#define P1INFO_NODE(LOG_LEVEL, node) MY_LOGD##LOG_LEVEL(\
    P1INFO_NODE_STR, P1INFO_NODE_VAR(node));

#define P1INFO_NODE_STREAM(LOG_LEVEL, node, stream) MY_LOGD##LOG_LEVEL(\
    P1INFO_NODE_STR " stream(%d)", P1INFO_NODE_VAR(node), stream);

#define P1_CHECK_STREAM(TYPE, stream)\
    if (stream < (STREAM_##TYPE)STREAM_ITEM_START ||\
        stream >= STREAM_##TYPE##_NUM) {\
        MY_LOGE("stream index invalid %d/%d", stream, STREAM_##TYPE##_NUM);\
        return INVALID_OPERATION;\
    };

#define P1_CHECK_CONFIG_STREAM(TYPE, node, stream)\
    if (mvStream##TYPE[stream] == NULL) {\
        MY_LOGW("StreamId is NULL %d@%d", stream, node.magicNum);\
        return BAD_VALUE;\
    };\

#define P1_CHECK_NODE_STREAM(TYPE, node, stream)\
    if (node.appFrame == NULL) {\
        MY_LOGW("pipeline frame is NULL %d@%d", stream, node.magicNum);\
        return INVALID_OPERATION;\
    };\
    P1_CHECK_CONFIG_STREAM(TYPE, node, stream);\
    if (!node.streamBuf##TYPE[stream].bExist) {\
        MY_LOGD1("stream is not exist %d@%d", stream, node.magicNum);\
        return OK;\
    };

//
#ifdef EN_START_CAP
#undef EN_START_CAP
#endif
#define EN_START_CAP    (mEnableCaptureFlow)
//
#ifdef EN_INIT_REQ
#undef EN_INIT_REQ
#endif
#define EN_INIT_REQ     (mInitReqSet > 0)
//
#ifdef P1NODE_DEF_SHUTTER_DELAY
#undef P1NODE_DEF_SHUTTER_DELAY
#endif
#define P1NODE_DEF_SHUTTER_DELAY (2)
//

/*
#ifdef P1_TIMING_CHECK
#undef P1_TIMING_CHECK
#endif
#if (IS_P1_LOGD) // for system use LOGD
#define P1_TIMING_CHECK(str, timeout_ms, type)\
    sp<TimingChecker::Client> TimingCheckerClient =\
    (mpTimingCheckerMgr != NULL) ?\
    (mpTimingCheckerMgr->createClient(str, timeout_ms, type)) :\
    (NULL);
#else
#define P1_TIMING_CHECK(str, timeout_ms, type)  ;
#endif

#ifdef TC_W
#undef TC_W
#endif
#define TC_W TimingChecker::EVENT_TYPE_WARNING
#ifdef TC_E
#undef TC_E
#endif
#define TC_E TimingChecker::EVENT_TYPE_ERROR
#ifdef TC_F
#undef TC_F
#endif
#define TC_F TimingChecker::EVENT_TYPE_FATAL
*/

#ifdef ONE_MS_TO_NS
#undef ONE_MS_TO_NS
#endif
#define ONE_MS_TO_NS (1000000LL)

#ifdef ONE_US_TO_NS
#undef ONE_US_TO_NS
#endif
#define ONE_US_TO_NS (1000LL)

/******************************************************************************
 *
 ******************************************************************************/

#ifdef MAX
#undef MAX
#endif
#ifdef MIN
#undef MIN
#endif
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define MIN(a,b) ((a) < (b) ? (a) : (b))

#ifdef ALIGN_UPPER
#undef ALIGN_UPPER
#endif
#ifdef ALIGN_LOWER
#undef ALIGN_LOWER
#endif
#define ALIGN_UPPER(x,a)    (((x)+((typeof(x))(a)-1))&~((typeof(x))(a)-1))
#define ALIGN_LOWER(x,a)    (((x))&~((typeof(x))(a)-1))


#ifdef NEED_LOCK
#undef NEED_LOCK
#endif
#define NEED_LOCK(need, mutex)\
    if (need) {\
        mutex.lock();\
    }
#ifdef NEED_UNLOCK
#undef NEED_UNLOCK
#endif
#define NEED_UNLOCK(need, mutex)\
    if (need) {\
        mutex.unlock();\
    }
//
#ifdef P1_FILL_BYTE
#undef P1_FILL_BYTE
#endif
#define P1_FILL_BYTE        (0xFF) // byte
//
#ifdef CHECK_LAST_FRAME_SKIPPED
#undef CHECK_LAST_FRAME_SKIPPED
#endif
#define CHECK_LAST_FRAME_SKIPPED(LAST_SOF_IDX, THIS_SOF_IDX)\
    ((LAST_SOF_IDX == P1SOFIDX_NULL_VAL) ?\
        (true) :\
        ((LAST_SOF_IDX == P1SOFIDX_LAST_VAL) ?\
            ((THIS_SOF_IDX != 0) ? (true) : (false)) :\
            ((THIS_SOF_IDX != (LAST_SOF_IDX + 1)) ? (true) : (false))));

#ifdef RESIZE_RATIO_MAX_10X
#undef RESIZE_RATIO_MAX_10X
#endif
#define RESIZE_RATIO_MAX_10X    (4)

#ifdef P1_EISO_MIN_HEIGHT
#undef P1_EISO_MIN_HEIGHT
#endif
#define P1_EISO_MIN_HEIGHT      (160)

#ifdef P1_RSSO_MIN_HEIGHT
#undef P1_RSSO_MIN_HEIGHT
#endif
#define P1_RSSO_MIN_HEIGHT      (22)

#ifdef P1_RRZO_MIN_HEIGHT
#undef P1_RRZO_MIN_HEIGHT
#endif
#define P1_RRZO_MIN_HEIGHT      (2)

#ifdef P1_STUFF_BUF_HEIGHT
#undef P1_STUFF_BUF_HEIGHT
#endif
#define P1_STUFF_BUF_HEIGHT(rrzo, config)   (\
    (rrzo) ? (MAX (\
    ((IS_PORT(CONFIG_PORT_EISO, config)) ?\
    (P1_EISO_MIN_HEIGHT) : (P1_RRZO_MIN_HEIGHT)),\
    ((IS_PORT(CONFIG_PORT_RSSO, config)) ?\
    (P1_RSSO_MIN_HEIGHT) : (P1_RRZO_MIN_HEIGHT)))) :\
    (1))
//
#ifdef P1_IMGO_DEF_FMT
#undef P1_IMGO_DEF_FMT
#endif
#define P1_IMGO_DEF_FMT (eImgFmt_BAYER10)
//
#ifdef P1NODE_METADATA_INVALID_VALUE
#undef P1NODE_METADATA_INVALID_VALUE
#endif
#define P1NODE_METADATA_INVALID_VALUE (-1)
//
#ifdef P1_STRIDE
#undef P1_STRIDE
#endif
#define P1_STRIDE(planes, n)\
    ((planes.size() > n) ? (planes.itemAt(n).rowStrideInBytes) : (0))
//
#ifdef IS_RAW_FMT_PACK_FULL
#undef IS_RAW_FMT_PACK_FULL
#endif
#define IS_RAW_FMT_PACK_FULL(fmt) ((                    \
    ((EImageFormat)fmt == eImgFmt_BAYER14_UNPAK)    ||  \
    ((EImageFormat)fmt == eImgFmt_BAYER12_UNPAK)    ||  \
    ((EImageFormat)fmt == eImgFmt_BAYER10_UNPAK)    ||  \
    ((EImageFormat)fmt == eImgFmt_BAYER8_UNPAK)         \
    ) ? (MFALSE) : (MTRUE))
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
 *
 ******************************************************************************/
typedef IHal3A IHal3A_T;


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
 *
 ******************************************************************************/
enum EXE_STATE
{
    EXE_STATE_NULL          = 0,
    EXE_STATE_REQUESTED,
    EXE_STATE_PROCESSING,
    EXE_STATE_DONE
};
//
#if 1//SUPPORT_UNI_SWITCH
enum UNI_SWITCH_STATE
{
    UNI_SWITCH_STATE_NONE           = 0, // no UNI switch-out request
    UNI_SWITCH_STATE_REQ, // received the switch-out request, need to switch-out
    UNI_SWITCH_STATE_ACT_ACCEPT, // UNI is held and it will switch-out
    UNI_SWITCH_STATE_ACT_IGNORE, // UNI is not held, ignore this switch-out
    UNI_SWITCH_STATE_ACT_REJECT  // UNI is switching and reject this switch-out
};
#endif
//
#if 1//SUPPORT_TG_SWITCH
enum TG_SWITCH_STATE
{
    TG_SWITCH_STATE_NONE           = 0, // no TG switch request
    TG_SWITCH_STATE_REQ, // received the TG switch request
    TG_SWITCH_STATE_DONE_ACCEPT, // TG switch command done and it accept
    TG_SWITCH_STATE_DONE_IGNORE, // TG switch command done and it ignore
    TG_SWITCH_STATE_DONE_REJECT  // TG switch command done and it reject
};
#endif
//
enum REQ_TYPE
{
    REQ_TYPE_UNKNOWN        = 0,
    REQ_TYPE_NORMAL,
    REQ_TYPE_INITIAL,
    //REQ_TYPE_PADDING,
    //REQ_TYPE_DUMMY,
    REQ_TYPE_REDO,
    REQ_TYPE_YUV
};
//
#ifdef REQ_SET
#undef REQ_SET
#endif
#define REQ_SET(bit)        ((MUINT32)(0x1 << bit))
#ifdef REQ_SET_NONE
#undef REQ_SET_NONE
#endif
#define REQ_SET_NONE        (0x0)
#ifdef IS_OUT
#undef IS_OUT
#endif
#define IS_OUT(out, set)    ((set & REQ_SET(out)) == REQ_SET(out))
enum REQ_OUT
{
    REQ_OUT_RESIZER         = 0,    // 0x 01
    REQ_OUT_RESIZER_STUFF,          // 0x 02
    REQ_OUT_LCSO,                   // 0x 04
    REQ_OUT_LCSO_STUFF,             // 0x 08
    REQ_OUT_FULL_PURE,              // 0x 10
    REQ_OUT_FULL_PROC,              // 0x 20
    REQ_OUT_FULL_OPAQUE,            // 0x 40
    REQ_OUT_FULL_STUFF,             // 0x 80
    REQ_OUT_RSSO,                   // 0x 0100
    REQ_OUT_RSSO_STUFF,             // 0x 0200
    REQ_OUT_MAX
};
//
#ifdef EXP_REC
#undef EXP_REC
#endif
#define EXP_REC(bit)        ((MUINT32)(0x1 << bit))
#ifdef EXP_REC_NONE
#undef EXP_REC_NONE
#endif
#define EXP_REC_NONE        (0x0)
#ifdef IS_EXP
#undef IS_EXP
#endif
#define IS_EXP(exp, rec)    ((rec & EXP_REC(exp)) == EXP_REC(exp))
enum EXP_EVT
{
    EXP_EVT_UNKNOWN         = 0,
    EXP_EVT_NOBUF_RRZO,
    EXP_EVT_NOBUF_IMGO,
    EXP_EVT_NOBUF_EISO,
    EXP_EVT_NOBUF_LCSO,
    EXP_EVT_NOBUF_RSSO,
    EXP_EVT_MAX
};
//
#ifdef P1_PORT_BUF_IDX_NONE
#undef P1_PORT_BUF_IDX_NONE
#endif
#define P1_PORT_BUF_IDX_NONE (0xFFFFFFFF) // MUINT32 (4 bytes with P1_FILL_BYTE)
//
enum P1_OUTPUT_PORT
{
    P1_OUTPUT_PORT_RRZO     = 0,
    P1_OUTPUT_PORT_IMGO,    //1
    P1_OUTPUT_PORT_EISO,    //2
    P1_OUTPUT_PORT_LCSO,    //3
    P1_OUTPUT_PORT_RSSO,    //4
    P1_OUTPUT_PORT_TOTAL    // (max:32) for CONFIG_PORT (MUINT32)
};
//
#ifdef IS_PORT
#undef IS_PORT
#endif
#define IS_PORT(port, set)  ((set & port) == port)
enum CONFIG_PORT
{
    CONFIG_PORT_NONE        = (0x0),
    CONFIG_PORT_RRZO        = (0x1 << P1_OUTPUT_PORT_RRZO), // 0x01
    CONFIG_PORT_IMGO        = (0x1 << P1_OUTPUT_PORT_IMGO), // 0x02
    CONFIG_PORT_EISO        = (0x1 << P1_OUTPUT_PORT_EISO), // 0x04
    CONFIG_PORT_LCSO        = (0x1 << P1_OUTPUT_PORT_LCSO), // 0x08
    CONFIG_PORT_RSSO        = (0x1 << P1_OUTPUT_PORT_RSSO), // 0x10
    CONFIG_PORT_ALL         = (0xFFFFFFFF) // MUINT32
};
//
enum ENQ_TYPE
{
    ENQ_TYPE_NORMAL         = 0,
    ENQ_TYPE_INITIAL,
    ENQ_TYPE_DIRECTLY
};
//
#ifdef BIN_RESIZE
#undef BIN_RESIZE
#endif
#define BIN_RESIZE(x)  (x = (x >> 1))

#ifdef BIN_REVERT
#undef BIN_REVERT
#endif
#define BIN_REVERT(x)  (x = (x << 1))

#endif //_MTK_HARDWARE_INCLUDE_MTKCAM_PIPELINE_HWNODE_P1NODE_COMMON_H_

