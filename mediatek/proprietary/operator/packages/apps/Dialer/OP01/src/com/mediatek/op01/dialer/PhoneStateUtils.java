package com.mediatek.op01.dialer;

import android.app.Activity;
import android.content.Context;
import android.os.SystemProperties;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.SubscriptionManager.OnSubscriptionsChangedListener;

import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.lang.ref.WeakReference;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


public class PhoneStateUtils {
    private static final String TAG = "PhoneStateUtils";

    private TelephonyManager               mTelephonyManager;
    //Listen for subinfo change, like sim hot plugin-out
    private SubscriptionManager            mSubscriptionManager;
    private OnSubscriptionsChangedListener mSubscriptionsChangedListener;

    private final Set<OnChangedListener> mListeners = Collections.newSetFromMap(
            new ConcurrentHashMap<OnChangedListener, Boolean>(5, 0.9f, 1));

    private HashMap<Integer, OP01PhoneStateListener> mPhoneStateListeners =
            new HashMap<Integer, OP01PhoneStateListener>();

    private static TelecomManager mTelecommManager;
    private static PhoneStateUtils mPhoneStateUtils;
    private static boolean mBlockViLTE = false;
    private static final boolean mSvLTESupport =
              SystemProperties.get("ro.boot.opt_c2k_lte_mode").equals("1");

    private WeakReference<Activity> mDialtactsActivity;
    private static Context mContext;

    public static PhoneStateUtils getInstance(Context cnx) {
        if (mPhoneStateUtils == null) {
            mPhoneStateUtils = new PhoneStateUtils(cnx);
        }
        return mPhoneStateUtils;
    }

    private PhoneStateUtils(Context cnx) {
        mContext = cnx;
        init(cnx);
        registerStateListener();
        mTelecommManager = (TelecomManager) mContext.getSystemService(Context.TELECOM_SERVICE);
    }

    //Interface for CallStatusServicePresenter
    public interface OnChangedListener {
        public void onCallStatusChange(int state);
    }

    public void setCurrentActiviy(Activity activity) {
        mDialtactsActivity = new WeakReference<Activity> (activity);
    }

    public void addPhoneStateListener(OnChangedListener listener) {
        mListeners.add(listener);
    }

    private void updateState(final int state) {
        if (mDialtactsActivity == null ||
            mDialtactsActivity.get() == null) {
            return;
        }

        Activity activity = mDialtactsActivity.get();
        activity.runOnUiThread( new Runnable() {
                public void run() {
                    for (OnChangedListener listener : mListeners) {
                        listener.onCallStatusChange(state);
                    }
                }
            });
    }

    private void init(Context cnx) {
        mTelephonyManager = TelephonyManager.from(cnx);
        mSubscriptionManager = SubscriptionManager.from(cnx);

        mSubscriptionsChangedListener = new OnSubscriptionsChangedListener() {
                                                @Override
                                                public void onSubscriptionsChanged() {
                                                    updateStateListener();
                                                }
                                            };
    }

    private void registerStateListener() {
        android.util.Log.d(TAG, "registerStateListener");
        List<SubscriptionInfo> infos =
                mSubscriptionManager.getActiveSubscriptionInfoList();
        if (infos == null) {
            return;
        }

        for (SubscriptionInfo info : infos) {
            int subId = info.getSubscriptionId();
            if (!mPhoneStateListeners.containsKey(subId)) {
                OP01PhoneStateListener listener = new OP01PhoneStateListener(subId);
                mPhoneStateListeners.put(subId, listener);
            }
        }

        //Listen for phone state changed
        for (OP01PhoneStateListener listener : mPhoneStateListeners.values()) {
            mTelephonyManager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);
        }

        //Listen for sim changed.
        mSubscriptionManager.addOnSubscriptionsChangedListener(mSubscriptionsChangedListener);
    }

    private void updateStateListener() {
        android.util.Log.d(TAG, "updateStateListener");
        List<SubscriptionInfo> subInfos = mSubscriptionManager.getActiveSubscriptionInfoList();
        if (subInfos == null) {
            return;
        }

        //We should remove all the item in mRichCallStateListener, and then add the listener again.
        for (Integer id : mPhoneStateListeners.keySet()) {
            int subId = id.intValue();
            OP01PhoneStateListener listener = mPhoneStateListeners.get(id);
            mTelephonyManager.listen(listener, PhoneStateListener.LISTEN_NONE);
        }
        mPhoneStateListeners.clear();

        for (int i = 0; i < subInfos.size(); i++) {
            int subId = subInfos.get(i).getSubscriptionId();
            if (!mPhoneStateListeners.containsKey(subId)) {
                OP01PhoneStateListener listener = new OP01PhoneStateListener(subId);
                mPhoneStateListeners.put(subId, listener);
                mTelephonyManager.listen(listener,
                        PhoneStateListener.LISTEN_CALL_STATE);
            }
        }
    }
    public static boolean canStartVideoCall() {
        return hasVideoCapability() &&
          ((getPhoneState() == TelephonyManager.CALL_STATE_IDLE) || supportDsdaViLTE());
    }

    public static boolean hasVideoCapability() {
      if (mTelecommManager == null) {
          mTelecommManager = (TelecomManager) mContext.getSystemService(Context.TELECOM_SERVICE);
      }

        List<PhoneAccountHandle> accountHandles = mTelecommManager.getCallCapablePhoneAccounts();
        for (PhoneAccountHandle accountHandle : accountHandles) {
            PhoneAccount account = mTelecommManager.getPhoneAccount(accountHandle);
            if (account != null && account.hasCapabilities(
                    PhoneAccount.CAPABILITY_VIDEO_CALLING)) {
                return true;
            }
        }
        return false;
    }

    public static int getPhoneState() {
        int state = TelephonyManager.CALL_STATE_IDLE;

        if (mTelecommManager == null) {
            mTelecommManager = (TelecomManager) mContext
                    .getSystemService(Context.TELECOM_SERVICE);
        }

        if (mTelecommManager != null) {
            state = mTelecommManager.getCallState();
        }

        android.util.Log.d(TAG, "getPhoneState, state = " + state);
        return state;
    }

    public static boolean supportDsdaViLTE() {
        boolean supportDsda = false;
        if (mSvLTESupport) {
            if (!mBlockViLTE) {
                supportDsda = true;
            }
        }

        android.util.Log.d(TAG, "supportDsdaViLTE, supportDsda = " + supportDsda);
        return supportDsda;
    }

    private class OP01PhoneStateListener extends PhoneStateListener {
        private int mSubId;
        public OP01PhoneStateListener(int subId) {
            super(subId);
            mSubId = subId;
        }

        @Override
        public void onCallStateChanged(int state, String number) {
            if (mSvLTESupport) {
                if (isCDMACard(mSubId)) {
                    return;
                }

                if (state == TelephonyManager.CALL_STATE_IDLE) {
                    mBlockViLTE = false;
                } else {
                    mBlockViLTE = true;
                }
            }

            android.util.Log.d(TAG, "onCallStateChanged, state = "
                    + state + ", mBlockViLTE = " + mBlockViLTE);

            if (hasVideoCapability()) {
                updateState(state);
            }
        }

        private boolean isSupportCdma(int slotId) {
            boolean isSupportCdma = false;
            String[] type = MtkTelephonyManagerEx.getDefault().getSupportCardType(slotId);
            if (type != null) {
                for (int i = 0; i < type.length; i++) {
                    if ("RUIM".equals(type[i]) || "CSIM".equals(type[i])) {
                        isSupportCdma = true;
                    }
                }
            }
            return isSupportCdma;
        }

        private boolean isCDMACard(int subId) {
            boolean isCdmaCard = false;
            int slotId = SubscriptionManager.getSlotIndex(subId);
            if (slotId == SubscriptionManager.INVALID_SIM_SLOT_INDEX) {
                return isCdmaCard;
            }

            if (isSupportCdma(slotId) ||
                MtkTelephonyManagerEx.getDefault().isCt3gDualMode(slotId)) {
                isCdmaCard = true;
            }

            android.util.Log.d(TAG, "subId = " +
                    subId + ", slotId = " + slotId + ", isCdmaCard = " + isCdmaCard);
            return isCdmaCard;
        }
    }

}
