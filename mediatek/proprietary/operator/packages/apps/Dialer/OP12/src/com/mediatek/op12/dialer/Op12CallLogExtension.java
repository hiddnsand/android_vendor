package com.mediatek.op12.dialer;


import java.util.List;
import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuff;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CallLog.Calls;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import android.view.View;

import com.android.dialer.app.calllog.CallLogActivity;
import com.android.dialer.app.calllog.CallLogFragment;
import com.android.dialer.app.calllog.CallLogListItemViewHolder;
import com.android.dialer.calllogutils.CallTypeIconsView;
import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.mediatek.dialer.ext.DefaultCallLogExtension;
import com.mediatek.dialer.util.DialerFeatureOptions;


public class Op12CallLogExtension extends DefaultCallLogExtension {

    private static final String TAG = "Op12CallLogExtension";
    private static final boolean TAB_INCOMING_OUTGOING_DISABLE = !DialerFeatureOptions
            .isCallLogIOFilterEnabled();
    private static final String FRAGMENT_TAG_VOICE = "fragment_tag_voice";
    private static final String FRAGMENT_TAG_VIDEO = "fragment_tag_video";
    private static final int TAB_INDEX_VOICE = 2;
    private static final int TAB_INDEX_VIDEO = 3;
    private static final String CALL_LOG_TYPE_FILTER = "call_log_type_filter";
    private static final int CALL_TYPE_ALL = -1;
    private static final int CALL_TYPE_VOICE = -100;
    private static final int CALL_TYPE_VIDEO = -200;
    private static final int MAX_TAB_COUNT = 4;
    private static final int MIN_TAB_COUNT = 2;
    private static final int INCOMING_PULLED_AWAY_TYPE = 10;
    private static final int OUTGOING_PULLED_AWAY_TYPE = 11;

    private static int sFilterFlag = CALL_TYPE_ALL;

    private static final int FEATURE_VOICE = 0x0;
    private static final int FEATURE_VIDEO = 0x1;
    private static final int FEATURES_VoWIFI = 0x4;
    private static final int FEATURES_VoLTE = 0x8;
    private static final int FEATURES_ViWIFI = 0x10;

    private Drawable mVoWifiCall;
    private Drawable mVolteCall;
    private Drawable mViWifiCall;
    private Bitmap mVoWifiIcon;
    private Bitmap mVolteIcon;
    private Bitmap mViWifiIcon;
    private Context mContext;
    private Bundle mInstanceState = null;
    private String mVoiceCallsFragmentTag = null;
    private String mVideoCallsFragmentTag = null;
    private CallLogFragment mVoiceCallsFragment;
    private CallLogFragment mVideoCallsFragment;
    private View mVideoView = null;
    private boolean mLVCEnabled;
    private boolean mIsVolteProvisioned;
    private IntentFilter mIntentFilter;
    private Activity mCallLogActivity = null;
    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "onReceive()... " + action);
            if (action.equals("com.android.intent.action.IMS_CONFIG_CHANGED")) {
                try {
                    ImsConfig imsConfig = ImsManager.getInstance(mContext, SubscriptionManager
                                     .getDefaultVoicePhoneId()).getConfigInterface();
                    mIsVolteProvisioned = ImsManager.isVolteProvisionedOnDevice(mContext);
                    mLVCEnabled = (1 == imsConfig.getProvisionedValue(
                              ImsConfig.ConfigConstants.LVC_SETTING_ENABLED));
                    Log.d(TAG, "mIsVolteProvisioned: " + mIsVolteProvisioned + " mLVCEnabled: " +
                            mLVCEnabled);
                    if (mCallLogActivity != null) {
                        android.os.Process.killProcess(android.os.Process.myPid());
                    }
                } catch (ImsException e) {
                    Log.e(TAG, "Advanced settings not updated, ImsConfig null");
                    e.printStackTrace();
                }
                if (mVideoView != null) {
                      mVideoView.setVisibility(mIsVolteProvisioned && mLVCEnabled ?
                                    View.VISIBLE : View.GONE);
                }
            } else if (action.equals("com.mediatek.intent.action.VT_CHANGED")) {
                boolean vtOption = intent.getBooleanExtra("VT", false);
                boolean advancedCalling = ImsManager
                                .isEnhanced4gLteModeSettingEnabledByUser(mContext);
                Log.d(TAG, " advancedCalling:" + advancedCalling + " VT:" + vtOption);
                if (mIsVolteProvisioned && mLVCEnabled && mVideoView != null) {
                    mVideoView.setAlpha(vtOption && advancedCalling ? 1.0f : 0.3f);
                }
            }
        }
    };

    public Op12CallLogExtension(Context context) {
        mContext = context;
        try {
            ImsConfig imsConfig = ImsManager.getInstance(mContext, SubscriptionManager
                          .getDefaultVoicePhoneId()).getConfigInterface();
            mIsVolteProvisioned = ImsManager.isVolteProvisionedOnDevice(mContext);
            mLVCEnabled = (1 == imsConfig.getProvisionedValue(
                       ImsConfig.ConfigConstants.LVC_SETTING_ENABLED));
        } catch (ImsException e) {
            Log.e(TAG, "Advanced settings not updated, ImsConfig null");
            e.printStackTrace();
        }
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("com.mediatek.intent.action.VT_CHANGED");
        mIntentFilter.addAction("com.android.intent.action.IMS_CONFIG_CHANGED");
        mContext.registerReceiver(mIntentReceiver, mIntentFilter);
    }

    @Override
    public void customizeBindActionButtons(Object object) {
        Log.d(TAG, "customizeBindActionButtons");
        CallLogListItemViewHolder viewHolder = (CallLogListItemViewHolder) object;
        mVideoView = viewHolder.videoCallButtonView;
        boolean advancedCalling = ImsManager.isEnhanced4gLteModeSettingEnabledByUser(mContext);
        Log.d(TAG, "mIsVolteProvisioned: " + mIsVolteProvisioned + " mLVCEnabled: " +
                mLVCEnabled + " advancedCalling:" + advancedCalling);
        viewHolder.videoCallButtonView.setVisibility(mIsVolteProvisioned && mLVCEnabled ?
                           View.VISIBLE : View.GONE);
        if (mIsVolteProvisioned && mLVCEnabled) {
            Log.d(TAG, "Volte provisioned/LVC Enabled, so unhide video button in call log");
            if (ImsManager.isVtEnabledByUser(mContext) && advancedCalling) {
                Log.d(TAG, "Enable Video button in call log");
                viewHolder.videoCallButtonView.setAlpha(1.0f);
            } else {
                Log.d(TAG, "Grey out Video button in call log");
                viewHolder.videoCallButtonView.setAlpha(0.3f);
            }
        } else {
        Log.d(TAG, "Volte Deprovisioned/LVC Disabled, so hide video button in call log");
        }
    }

    @Override
    public int getTabIndexCount() {
        Log.d(TAG, "getTabIndexCount");
        if (TAB_INCOMING_OUTGOING_DISABLE && mLVCEnabled && mIsVolteProvisioned) {
            return MAX_TAB_COUNT;
        } else {
            return MIN_TAB_COUNT;
        }
    }

    @Override
    public void initCallLogTab(CharSequence[] tabTitles, ViewPager viewPager) {
        Log.d(TAG, "initCallLogTab");
        if (TAB_INCOMING_OUTGOING_DISABLE && mLVCEnabled && mIsVolteProvisioned) {
            tabTitles[2] = mContext.getString(R.string.call_log_voice_title);
            tabTitles[3] = mContext.getString(R.string.call_log_video_title);
            viewPager.setOffscreenPageLimit(3);
        } else {
            viewPager.setOffscreenPageLimit(1);
        }
    }

    @Override
    public void restoreFragments(Context context,
            FragmentPagerAdapter pagerAdapter, HorizontalScrollView tabs) {
         Log.d(TAG, "restoreFragments");
         CallLogActivity callLogActivity = (CallLogActivity) context;

         if (TAB_INCOMING_OUTGOING_DISABLE && mLVCEnabled && mIsVolteProvisioned) {
             if (mInstanceState != null) {
                 mVoiceCallsFragmentTag = mInstanceState.getString(FRAGMENT_TAG_VOICE, null);
                 mVideoCallsFragmentTag = mInstanceState.getString(FRAGMENT_TAG_VIDEO, null);
             }
             if (mVoiceCallsFragment == null
                                    && mVoiceCallsFragmentTag != null) {
                  mVoiceCallsFragment = (CallLogFragment) callLogActivity
                       .getFragmentManager().findFragmentByTag(mVoiceCallsFragmentTag);
                  Log.d(TAG, "findFragment voice ~ " + mVoiceCallsFragment);
             }
             if (mVideoCallsFragment == null
                                    && mVideoCallsFragmentTag != null) {
                  mVideoCallsFragment = (CallLogFragment) callLogActivity
                       .getFragmentManager().findFragmentByTag(mVideoCallsFragmentTag);
                  Log.d(TAG, "findFragment video ~ " + mVideoCallsFragment);
             }
         }
    }

    @Override
    public void onSaveInstanceState(Activity activity, Bundle outState) {
        Log.d(TAG, "onSaveInstanceState");
        CallLogActivity callLogActivity = (CallLogActivity) activity;
        if (TAB_INCOMING_OUTGOING_DISABLE && mLVCEnabled && mIsVolteProvisioned) {
            if (mVoiceCallsFragment != null) {
                outState.putString(FRAGMENT_TAG_VOICE,
                        mVoiceCallsFragment.getTag());
            }
            if (mVideoCallsFragment != null) {
                outState.putString(FRAGMENT_TAG_VIDEO,
                        mVideoCallsFragment.getTag());
            }
        }
        mCallLogActivity = activity;
    }

    @Override
    public void setCurrentCallLogFragment(Activity activity, int position,
                          Fragment fragment) {
        Log.d(TAG, "setCurrentCallLogFragment");
        if (TAB_INCOMING_OUTGOING_DISABLE && mLVCEnabled && mIsVolteProvisioned) {
             if (position == TAB_INDEX_VOICE) {
                 fragment = mVoiceCallsFragment;
                 Log.d(TAG, "setCurrentCallLogFragment as Voice");
            } else if (position == TAB_INDEX_VIDEO) {
                 fragment = mVideoCallsFragment;
                 Log.d(TAG, "setCurrentCallLogFragment as Video");
            }
        }
        mCallLogActivity = activity;
    }

    @Override
    public Fragment getCallLogFragmentItem(int position) {
        Log.d(TAG, "getCallLogFragmentItem");
        if (TAB_INCOMING_OUTGOING_DISABLE && mLVCEnabled && mIsVolteProvisioned) {
            if (position == TAB_INDEX_VOICE) {
                return new CallLogFragment(CALL_TYPE_VOICE , true);
            } else if (position == TAB_INDEX_VIDEO) {
                return new CallLogFragment(CALL_TYPE_VIDEO, true);
            }
        }
        return null;
    }

    @Override
    public void instantiateCallLogFragmentItem(Activity activity, int position,
                               Fragment fragment) {
        Log.d(TAG, "instantiateCallLogFragmentItem");
        if (TAB_INCOMING_OUTGOING_DISABLE && mLVCEnabled && mIsVolteProvisioned) {
             if (position == TAB_INDEX_VOICE) {
                 mVoiceCallsFragment = (CallLogFragment) fragment;
             } else if (position == TAB_INDEX_VIDEO) {
                 mVideoCallsFragment = (CallLogFragment) fragment;
             }
        }
        mCallLogActivity = activity;
    }

    @Override
    public void onDeleteButtonClick(int position, Intent delIntent) {
        Log.d(TAG, "onDeleteButtonClick");
        if (TAB_INCOMING_OUTGOING_DISABLE && mLVCEnabled && mIsVolteProvisioned) {
            if (position == TAB_INDEX_VOICE) {
                delIntent.putExtra(CALL_LOG_TYPE_FILTER, CALL_TYPE_VOICE);
            } else if (position == TAB_INDEX_VIDEO) {
                delIntent.putExtra(CALL_LOG_TYPE_FILTER, CALL_TYPE_VIDEO);
            }
        }
    }

    @Override
    public int getFilterType(int filterType) {
        Log.d(TAG, "getFilterType");
        int msgId = -1;
        if (TAB_INCOMING_OUTGOING_DISABLE && mLVCEnabled && mIsVolteProvisioned) {
            if (filterType == CALL_TYPE_VOICE || filterType == CALL_TYPE_VIDEO) {
                sFilterFlag = filterType;
                return CALL_TYPE_ALL;
            }
        }
        sFilterFlag = filterType;
        return filterType;
    }

    @Override
    public void setEmptyViewText(TextView emptyTextView) {
        Log.d(TAG, "setEmptyViewText");
        int msgId = -1;
        if (TAB_INCOMING_OUTGOING_DISABLE && mLVCEnabled && mIsVolteProvisioned) {
            if (sFilterFlag == CALL_TYPE_VOICE) {
                emptyTextView.setText(mContext.getText(R.string.call_log_voice_empty));
            } else if (sFilterFlag == CALL_TYPE_VIDEO) {
                emptyTextView.setText(mContext.getText(R.string.call_log_video_empty));
            }
        }
    }

    @Override
    public void appendQuerySelection(int typeFilter, StringBuilder builder,
            List<String> selectionArgs) {
        Log.d(TAG, "appendQuerySelection");
        if (TAB_INCOMING_OUTGOING_DISABLE && mLVCEnabled && mIsVolteProvisioned) {
            if (typeFilter == CALL_TYPE_VOICE) {
                builder.append(" AND (").append(Calls.FEATURES).append(" = ?)");
                selectionArgs.add(Integer.toString(FEATURE_VOICE));
                builder.append(" OR (").append(Calls.FEATURES).append(" = ?)");
                selectionArgs.add(Integer.toString(FEATURES_VoWIFI));
                builder.append(" OR (").append(Calls.FEATURES).append(" = ?)");
                selectionArgs.add(Integer.toString(FEATURES_VoLTE));
            } else if (typeFilter == CALL_TYPE_VIDEO) {
                builder.append(" AND (").append(Calls.FEATURES).append(" = ?)");
                selectionArgs.add(Integer.toString(FEATURE_VIDEO));
                builder.append(" OR (").append(Calls.FEATURES).append(" = ?)");
                selectionArgs.add(Integer.toString(FEATURES_ViWIFI));
            }
        }
    }

    private int getCurrentCallLogFilterFeatures(int position) {
        if (position == TAB_INDEX_VOICE) {
            return FEATURE_VOICE;
        } else if (position == TAB_INDEX_VIDEO) {
            return FEATURE_VOICE;
        }
        return CALL_TYPE_ALL;
   }

    @Override
    public void drawWifiVolteCallIcon(int scaledHeight) {
        Log.d(TAG, "drawWifiVolteCallIcon");
        final android.content.res.Resources r = mContext.getResources();

        mVoWifiIcon = BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.vowifi_call);
        mVolteIcon = BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.volte_call);
        mViWifiIcon = BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.video_call_over_wifi);

        int scaledWidth1 = (int) ((float) mVoWifiIcon.getWidth() *
                ((float) scaledHeight /
                        (float) mVoWifiIcon.getHeight()));
        int scaledWidth2 = (int) ((float) mVolteIcon.getWidth() *
                ((float) scaledHeight /
                        (float) mVolteIcon.getHeight()));
        int scaledWidth3 = (int) ((float) mViWifiIcon.getWidth() *
                ((float) scaledHeight /
                        (float) mViWifiIcon.getHeight()));

        Bitmap scaled1 = Bitmap.createScaledBitmap(mVoWifiIcon, scaledWidth1, scaledHeight, false);
        Bitmap scaled2 = Bitmap.createScaledBitmap(mVolteIcon, scaledWidth2, scaledHeight, false);
        Bitmap scaled3 = Bitmap.createScaledBitmap(mViWifiIcon, scaledWidth3, scaledHeight, false);

        mVoWifiCall = new BitmapDrawable(mContext.getResources(), scaled1);
        mVoWifiCall.setColorFilter(r.getColor(R.color.dialtacts_secondary_text_color),
               PorterDuff.Mode.MULTIPLY);
        mVolteCall = new BitmapDrawable(mContext.getResources(), scaled2);
        mVolteCall.setColorFilter(r.getColor(R.color.dialtacts_secondary_text_color),
               PorterDuff.Mode.MULTIPLY);
        mViWifiCall = new BitmapDrawable(mContext.getResources(), scaled3);
        mViWifiCall.setColorFilter(r.getColor(R.color.dialtacts_secondary_text_color),
               PorterDuff.Mode.MULTIPLY);
        Log.d(TAG, "drawWifiVolteCanvas  mVoWifiIcon.getHeight()" +  mVoWifiIcon.getHeight());
        Log.d(TAG, "drawWifiVolteCanvas  mVoWifiCall.getIntrinsicHeight()" +
                mVoWifiCall.getIntrinsicHeight());
    }

    @Override
    public void drawWifiVolteCanvas(int left, Canvas canvas, Object callTypeIconViewObj) {
        CallTypeIconsView callTypeIcons = (CallTypeIconsView) callTypeIconViewObj;
        Log.d(TAG, "drawWifiVolteCanvas");

        if (callTypeIcons.mShowvowifi) {
            final Drawable drawable = mVoWifiCall;
            final int right = left + mVoWifiCall.getIntrinsicWidth();
            drawable.setBounds(left, 0, right, mVoWifiCall.getIntrinsicHeight());
            drawable.draw(canvas);
            Log.d(TAG, "drawWifiVolteCanvas ShowVoWifi:" + callTypeIcons.mShowvowifi);
        }
        if (callTypeIcons.mShowvolte) {
            final Drawable drawable = mVolteCall;
            final int right = left + mVolteCall.getIntrinsicWidth();
            drawable.setBounds(left, 0, right, mVolteCall.getIntrinsicHeight());
            drawable.draw(canvas);
            Log.d(TAG, "drawWifiVolteCanvas ShowVolte:" + callTypeIcons.mShowvolte);
        }
        if (callTypeIcons.mShowViWifi) {
            final Drawable drawable = mViWifiCall;
            final int right = left + mViWifiCall.getIntrinsicWidth();
            drawable.setBounds(left, 0, right, mViWifiCall.getIntrinsicHeight());
            drawable.draw(canvas);
            Log.d(TAG, "drawWifiVolteCanvas ShowViWifi:" + callTypeIcons.mShowViWifi);
        }
    }

    @Override
    public void setShowVolteWifi(Object object, int features) {
        Log.d(TAG, "setShowVolteWifi features:" + features);
        CallTypeIconsView callTypeIcons = (CallTypeIconsView) object;

        setShowVowifi(callTypeIcons, (features & FEATURES_VoWIFI) == FEATURES_VoWIFI);
        setShowVolte(callTypeIcons, (features & FEATURES_VoLTE) == FEATURES_VoLTE);
        setShowViWifi(callTypeIcons, (features & FEATURES_ViWIFI) == FEATURES_ViWIFI);

        if (callTypeIcons.mShowvowifi) {
            callTypeIcons.mWidth += mVoWifiCall.getIntrinsicWidth();
            callTypeIcons.mHeight = Math.max(callTypeIcons.mHeight,
                    mVoWifiCall.getIntrinsicHeight());
            callTypeIcons.invalidate();
        }
        if (callTypeIcons.mShowvolte) {
            callTypeIcons.mWidth += mVolteCall.getIntrinsicWidth();
            callTypeIcons.mHeight = Math.max(callTypeIcons.mHeight,
                    mVolteCall.getIntrinsicHeight());
            callTypeIcons.invalidate();
        }

        if (callTypeIcons.mShowViWifi) {
            callTypeIcons.mWidth += mViWifiCall.getIntrinsicWidth();
            callTypeIcons.mHeight = Math.max(callTypeIcons.mHeight,
                    mViWifiCall.getIntrinsicHeight());
            callTypeIcons.invalidate();
        }
    }

    @Override
    public boolean isViWifiShown(Object object) {
        Log.d(TAG, "isViWifiShown");
        CallTypeIconsView callTypeIcons = (CallTypeIconsView) object;
        return callTypeIcons.mShowViWifi;
    }

    @Override
    public boolean sameCallFeature(Cursor cursor) {
        Log.d(TAG, "sameCallFeature");
    final long currentCallFeature =
            cursor.getLong(cursor.getColumnIndex(Calls.FEATURES));

        cursor.moveToPrevious();
        final long prevCallFeature =
            cursor.getLong(cursor.getColumnIndex(Calls.FEATURES));
        cursor.moveToNext();
        final boolean sameCallFeature = (prevCallFeature == currentCallFeature);
        Log.d(TAG, "prevCallFeature:" + prevCallFeature + "currentCallFeature:"
                + currentCallFeature);
        return sameCallFeature;
    }

    private void setShowVowifi(CallTypeIconsView callTypeIcons, boolean showWifi) {
        callTypeIcons.mShowvowifi = showWifi;
        Log.d(TAG, "setShowVowifi:" + showWifi);
    }

    private void setShowVolte(CallTypeIconsView callTypeIcons, boolean showVolte) {
        callTypeIcons.mShowvolte = showVolte;
        Log.d(TAG, "setShowVolte:" + showVolte);
    }

    private void setShowViWifi(CallTypeIconsView callTypeIcons, boolean showViWifi) {
        callTypeIcons.mShowViWifi = showViWifi;
        Log.d(TAG, "setShowViWifi:" + showViWifi);
    }
}
