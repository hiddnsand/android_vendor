/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.test.ActivityInstrumentationTestCase2;
import android.test.SingleLaunchActivityTestCase;
import android.util.Log;
import android.widget.ListView;

import com.mediatek.engineermode.R;
import com.robotium.solo.Solo;
import com.mediatek.engineermode.hqanfc.NfcMainPage;
import com.mediatek.engineermode.hqanfc.ReaderMode;
import com.mediatek.engineermode.hqanfc.PeerToPeerMode;
import com.mediatek.engineermode.hqanfc.LoopBackTest;
import com.mediatek.engineermode.hqanfc.TxCarrierSignal;
import com.mediatek.engineermode.hqanfc.Option;
import com.mediatek.engineermode.hqanfc.CardEmulationMode;
import com.mediatek.engineermode.hqanfc.PollingLoopMode;
import com.mediatek.engineermode.hqanfc.SwpTest;
import com.mediatek.engineermode.hqanfc.PnfcCommand;
import com.mediatek.engineermode.hqanfc.VersionQuery;
import com.mediatek.engineermode.hqanfc.VirtualCardFunction;
import com.mediatek.engineermode.wifi.WiFi;

import android.net.wifi.WifiManager;
import android.os.Bundle;

import java.util.ArrayList;

public class HqaNfcTest extends SingleLaunchActivityTestCase<NfcMainPage> {
    private static final String TAG = "EMTest/NfcMainPage";
    private static boolean sFinished = false;
    private static Solo mSolo = null;
    private Context mContext = null;
    private Instrumentation mIns = null;
    private Activity mActivity = null;

    public HqaNfcTest() {
        super("com.mediatek.engineermode", NfcMainPage.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mIns = getInstrumentation();
        mContext = mIns.getTargetContext();
        mActivity = getActivity();
        mSolo = new Solo(mIns, mActivity);
    }

    @Override
    protected void tearDown() throws Exception {
        if (sFinished) {
            Log.i(TAG, "Nfc6605 not Support finish");
            mSolo.finishOpenedActivities();
        }
        super.tearDown();
    }

    @BasicFuncAnnotation
    public void test01_preCheck() {
        verifyPreconditions();

        if (!isNfc6605Support()) {
            Log.i(TAG, "Nfc6605 not Support");
            sFinished = true;
            return;
        }
        Log.i(TAG, "Nfc6605  Support");
        int jumpCount = 0;
        while (mSolo.searchText("Please Waiting...")
                && (jumpCount++ < 15)) {
            EmOperate.waitSomeTime(EmOperate.TIME_MID);
        }
        mSolo.sleep(EmOperate.TIME_LONG);
    }

    @UiAnnotation
    public void test02_TestReaderMode() {
        if (sFinished)
            return;

        if (!EmOperate.enterItem(mSolo, "Always in reader mode", "ReaderMode"))
            return;

        mSolo.sleep(EmOperate.TIME_LONG);
        mSolo.clickOnText("Clear All");
        mSolo.clickOnText("Default");
        mSolo.clickOnText("Return");

    }

    @UiAnnotation
    public void test03_TestP2pMode() {
        if (sFinished)
            return;
        if (!EmOperate.enterItem(mSolo, "Always in peer to peer mode", "PeerToPeerMode"))
            return;

        mSolo.clickOnText("TypeA");
        mSolo.clickOnCheckBox(0);
        mSolo.clickOnText("TypeF");
        mSolo.clickOnText("Clear All");
        mSolo.clickOnText("Default");
        mSolo.clickOnText("Return");
    }

    @UiAnnotation
    public void test04_TestCardEmul() {
        if (sFinished)
            return;
        if (!EmOperate.enterItem(mSolo, "Always in card emulation mode", "CardEmulationMode"))
            return;
        mSolo.clickOnText("Clear All");
        mSolo.clickOnText("Default");
        mSolo.clickOnText("Return");
    }

    @UiAnnotation
    public void test05_TestPollingLoopMode() {
        if (sFinished)
            return;
        if (!EmOperate.enterItem(mSolo, "Polling loop mode", "PollingLoopMode"))
            return;

        mSolo.clickOnText("Pause");
        mSolo.clickOnText("Listen");
        mSolo.clickOnText("Reader Mode");
        mSolo.clickOnText("dual subcarrier");
        mSolo.clickOnText("subcarrier");
        mSolo.clickOnText("P2P Mode");

        mSolo.clickOnText("Card Emulation Mode");

        mSolo.clickOnText("Clear All");
        mSolo.clickOnText("Default");
        mSolo.clickOnText("Return");
    }

    @UiAnnotation
    public void test06_TestTxSignal() {
        if (sFinished)
            return;
        if (!EmOperate.enterItem(mSolo, "TX carrier signal always on", "TxCarrierSignal"))
            return;

        mSolo.clickOnText("Start");

        assertTrue("start TestTxSignal failed", mSolo.waitForText("SUCCESS"));

        mSolo.clickOnText("Stop");

        assertTrue("start TestTxSignal failed", mSolo.waitForText("SUCCESS"));

        mSolo.clickOnText("Return");
    }

    @UiAnnotation
    public void test07_TestVirtualCard() {
        if (sFinished)
            return;
        if (!EmOperate.enterItem(mSolo, "Virtual card function", "VirtualCardFunction"))
            return;

        mSolo.clickOnText("TypeF");
        mSolo.clickOnText("Clear All");
        mSolo.clickOnText("Default");
        mSolo.clickOnText("Return");
    }

    @UiAnnotation
    public void test08_TestPnfcCommand() {
        if (sFinished)
            return;
        if (!EmOperate.enterItem(mSolo, "PNFC command", "PnfcCommand"))
            return;

        mSolo.clearEditText(0);
        mSolo.enterText(0, "0x00");
        mSolo.clickOnText("Send");
        assertTrue("start TestTxSignal failed", mSolo.waitForText("PnfcCommand Rsp"));

        mSolo.clickOnText("Return");
    }

    @UiAnnotation
    public void test09_TestVersionQuery() {
        if (sFinished)
            return;
        if (!EmOperate.enterItem(mSolo, "Version Query", "VersionQuery"))
            return;

        mSolo.clickOnText("Return");
    }

    @UiAnnotation
    public void test10_TestOption() {
        if (sFinished)
            return;
        if (!EmOperate.enterItem(mSolo, "Option", "Option"))
            return;

        mSolo.clickOnCheckBox(0);

        mSolo.clickOnText("Set");

        assertTrue("Set TestOption failed", mSolo.waitForText("SUCCESS"));

        mSolo.clickOnText("Return");
    }

    @UiAnnotation
    public void test11_TestLoopback() {
        if (sFinished)
            return;
        if (!EmOperate.enterItem(mSolo, "Loopback Test", "LoopBackTest"))
            return;

        mSolo.clickOnText("Start");

        assertTrue("Set TestLoopback failed", mSolo.waitForText("LoopBackTest Rsp Result"));

        mSolo.clickOnText("Return");
    }

    @UiAnnotation
    public void test12_TestSWP() {
        if (sFinished)
            return;
        sFinished = true;

        if (!EmOperate.enterItem(mSolo, "SWP", "SwpTest"))
            return;

        mSolo.clickOnText("Start");

        assertTrue("Set TestSWP failed", mSolo.waitForText("SWP Test Rsp Result"));
        mSolo.clickOnText("Return");
    }

    private void verifyPreconditions() {
        assertTrue(mIns != null);
        assertTrue(mActivity != null);
        assertTrue(mContext != null);
        assertTrue(mSolo != null);

        if (!mSolo.waitForActivity("NfcMainPage", EmOperate.TIME_MID)) {
            Bundle mBundle = new Bundle();
            mBundle.putString("Data", "data from TestBundle");
            launchActivity("com.mediatek.engineermode", NfcMainPage.class, mBundle);
        }

    }

    private boolean isNfc6605Support() {
        int versionCode = Settings.Global.getInt(mContext.getContentResolver(),
                "nfc_controller_code", -1);
        return versionCode == 0x02;

    }
}
