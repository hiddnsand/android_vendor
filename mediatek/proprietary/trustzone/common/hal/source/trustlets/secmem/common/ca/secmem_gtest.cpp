/*
 * Copyright (c) 2014 - 2016 MediaTek Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include <linux/ion.h>
#include <linux/ion_drv.h>
#include <linux/mtk_ion.h>
#include <ion/ion.h>
#include <ion.h>

#include "secmem_tlc.h"

#define SIZE_1K       0x00000400    /* 1K    */
#define SIZE_2K       0x00000800    /* 2K    */
#define SIZE_4K       0x00001000    /* 4K    */
#define SIZE_8K       0x00002000    /* 8K    */
#define SIZE_16K      0x00004000    /* 16K   */
#define SIZE_32K      0x00008000    /* 32K   */
#define SIZE_64K      0x00010000    /* 64K   */
#define SIZE_1M       0x00100000    /* 1M    */
#define SIZE_4M       0x00400000    /* 4M    */
#define SIZE_16M      0x01000000    /* 16M   */
#define SIZE_64M      0x04000000    /* 64M   */
#define SIZE_1G       0x40000000    /* 1G    */
#define SIZE_2G       0x80000000    /* 2G    */
#define SIZE_U32_MAX  0xFFFFFFFF    /* 4G-1  */

class SecmemTLCBaseTest : public ::testing::Test {
    public:
        static void SetUpTestCase() {
            ASSERT_EQ(UREE_SUCCESS, tlcMemOpen());
        }

        static void TearDownTestCase() {
            ASSERT_EQ(UREE_SUCCESS, tlcMemClose());
        }
};

class SecmemTLCTest : public SecmemTLCBaseTest {
};

class SecmemTLCAllocTest : public SecmemTLCBaseTest,
    public ::testing::WithParamInterface<uint32_t> {
};

class SecmemTLCAllocTBLTest : public SecmemTLCBaseTest,
    public ::testing::WithParamInterface<uint32_t> {
};

TEST_P(SecmemTLCAllocTest, AllocSecuremem_Success) {
    uint32_t refcount = 0;
    UREE_SECUREMEM_HANDLE handle;

    ASSERT_EQ(UREE_SUCCESS, UREE_AllocSecuremem(&handle, GetParam(), GetParam()));
    ASSERT_NE(0U, handle);
    ASSERT_EQ(UREE_SUCCESS, UREE_ReferenceSecuremem(handle, &refcount));
    ASSERT_EQ(2U, refcount);
    ASSERT_EQ(UREE_SUCCESS, UREE_UnreferenceSecuremem(handle, &refcount));
    ASSERT_EQ(1U, refcount);
    ASSERT_EQ(UREE_SUCCESS, UREE_FreeSecuremem(handle, &refcount));
    ASSERT_EQ(0U, refcount);
    /* SECMEM_ERROR_INCORRECT_HANDLE = 0xB, refer to secmem_core.h */
    ASSERT_EQ(0xBU, UREE_UnreferenceSecuremem(handle, &refcount));
    ASSERT_EQ(0U, refcount);
}

INSTANTIATE_TEST_CASE_P(ALLOC,
        SecmemTLCAllocTest,
        ::testing::Values(SIZE_1K, SIZE_4K, SIZE_16K, SIZE_64K,
            SIZE_1M, SIZE_4M, SIZE_16M, SIZE_64M));

TEST_P(SecmemTLCAllocTBLTest, AllocFSecurememTBL_Success) {
    uint32_t refcount = 0;
    UREE_SECUREMEM_HANDLE handle;

    ASSERT_EQ(UREE_SUCCESS, UREE_AllocSecurememTBL(&handle, GetParam(), GetParam()));
    ASSERT_NE(0U, handle);
    ASSERT_EQ(UREE_SUCCESS, UREE_FreeSecurememTBL(handle, &refcount));
    ASSERT_EQ(0U, refcount);
}

INSTANTIATE_TEST_CASE_P(ALLOC_TBL,
        SecmemTLCAllocTBLTest,
        ::testing::Values(SIZE_1K, SIZE_2K, SIZE_4K, SIZE_8K,
            SIZE_16K, SIZE_32K, SIZE_64K, SIZE_1M));

class IONBaseTest : public ::testing::Test {
    public:
        static void SetUpTestCase() {}
        static void TearDownTestCase() {}
};

class IONAllocTest : public IONBaseTest,
    public ::testing::WithParamInterface<uint32_t> {
};

class IONAllocZeroTest : public IONBaseTest,
    public ::testing::WithParamInterface<uint32_t> {
};

TEST_P(IONAllocTest, ion_alloc_free_Success) {
    int ion_fd;
    ion_user_handle_t handle;

    ASSERT_TRUE((ion_fd = ion_open()) >= 0);
    ASSERT_EQ(0, ion_alloc(ion_fd, GetParam(), 0, ION_HEAP_MULTIMEDIA_SEC_MASK, 0, &handle));
    ASSERT_NE(0, handle);
    ASSERT_EQ(0, ion_free(ion_fd, handle));
    ASSERT_EQ(0, ion_close(ion_fd));
}

INSTANTIATE_TEST_CASE_P(ION_ALLOC,
        IONAllocTest,
        ::testing::Values(SIZE_1K, SIZE_4K, SIZE_16K, SIZE_64K,
            SIZE_1M, SIZE_4M, SIZE_16M, SIZE_64M));

TEST_P(IONAllocZeroTest, ion_alloc_zero_free_Success) {
    int ion_fd;
    ion_user_handle_t handle;

    ASSERT_TRUE((ion_fd = ion_open()) >= 0);
    ASSERT_EQ(0, ion_alloc(ion_fd, GetParam(), 0, ION_HEAP_MULTIMEDIA_SEC_MASK, ION_FLAG_MM_HEAP_INIT_ZERO, &handle));
    ASSERT_NE(0, handle);
    ASSERT_EQ(0, ion_free(ion_fd, handle));
    ASSERT_EQ(0, ion_close(ion_fd));
}

INSTANTIATE_TEST_CASE_P(ION_ALLOC_ZERO,
        IONAllocZeroTest,
        ::testing::Values(SIZE_1K, SIZE_4K, SIZE_16K, SIZE_64K,
            SIZE_1M, SIZE_4M, SIZE_16M, SIZE_64M));

