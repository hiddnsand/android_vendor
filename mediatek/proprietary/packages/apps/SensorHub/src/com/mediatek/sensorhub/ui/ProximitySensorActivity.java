package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class ProximitySensorActivity extends CustomerSensorBaseActivity {

    public ProximitySensorActivity() {
        super(Utils.KEY_PROXIMITY_STATUS, Sensor.TYPE_PROXIMITY);
    }
}
