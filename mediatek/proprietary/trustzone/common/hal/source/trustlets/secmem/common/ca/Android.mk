#
# Copyright (c) 2016 MediaTek Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

LOCAL_PATH := $(call my-dir)

# ====================================================================================
# libsec_mem - allow user-space apps to alloc/free secure memory
# ====================================================================================
include $(CLEAR_VARS)

LOCAL_MODULE := libsec_mem
LOCAL_MODULE_TAGS := debug eng optional
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/public \

LOCAL_CFLAGS += -Werror

LOCAL_SRC_FILES	+= secmem_tlc.cpp
LOCAL_SHARED_LIBRARIES += liblog

include $(BUILD_SHARED_LIBRARY)

# ====================================================================================
# secmem_test - creating threads to test secure memory alloc/free
# ====================================================================================
include $(CLEAR_VARS)

LOCAL_MODULE	:= secmem_test
LOCAL_MODULE_TAGS := debug eng optional
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/public \

LOCAL_SRC_FILES	+= secmem_test.cpp
LOCAL_SHARED_LIBRARIES += libsec_mem
LOCAL_SHARED_LIBRARIES += liblog

include $(BUILD_EXECUTABLE)

# ====================================================================================
# secmem_gtest - basic tests of secure memory alloc/free using ION
# ====================================================================================
include $(CLEAR_VARS)
LOCAL_MODULE := secmem_gtest
LOCAL_MODULE_TAGS := debug eng optional
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/public \
    $(TOP)/system/core/include \
    $(TOP)/vendor/mediatek/proprietary/external/libion_mtk/include \
    $(TOP)/vendor/mediatek/proprietary/external/include \

LOCAL_CFLAGS += -Werror

LOCAL_SRC_FILES += secmem_gtest.cpp
LOCAL_SHARED_LIBRARIES += libsec_mem
LOCAL_SHARED_LIBRARIES += liblog
LOCAL_SHARED_LIBRARIES += libion libion_mtk
include $(BUILD_NATIVE_TEST)

# ====================================================================================
# secmem_dump - dumping secure memory usage
# ====================================================================================
include $(CLEAR_VARS)

LOCAL_MODULE	:= secmem_dump
LOCAL_MODULE_TAGS := debug eng optional
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/public \

LOCAL_CFLAGS += -Werror

LOCAL_SRC_FILES	+= secmem_dump.cpp
LOCAL_SHARED_LIBRARIES += libsec_mem
LOCAL_SHARED_LIBRARIES += liblog

include $(BUILD_EXECUTABLE)
