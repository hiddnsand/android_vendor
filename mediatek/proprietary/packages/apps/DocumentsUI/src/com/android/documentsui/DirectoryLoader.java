/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.documentsui;

import static com.android.documentsui.base.Shared.DEBUG;
import static com.android.documentsui.base.Shared.VERBOSE;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.AsyncTaskLoader;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Looper;
import android.os.OperationCanceledException;
import android.os.RemoteException;
import android.provider.DocumentsContract.Document;
import android.util.Log;

import com.android.documentsui.archives.ArchivesProvider;
import com.android.documentsui.base.DebugFlags;
import com.android.documentsui.base.DocumentInfo;
import com.android.documentsui.base.Features;
import com.android.documentsui.base.FilteringCursorWrapper;
import com.android.documentsui.base.RootInfo;
import com.android.documentsui.dirlist.DirectoryFragment;
import com.android.documentsui.files.FilesActivity;
import com.android.documentsui.picker.PickActivity;
import com.android.documentsui.roots.RootCursorWrapper;
import com.android.documentsui.sorting.SortModel;
/// M: DRM refactory
import com.mediatek.omadrm.OmaDrmStore;

import libcore.io.IoUtils;

public class DirectoryLoader extends AsyncTaskLoader<DirectoryResult> {

    private static final String TAG = "DirectoryLoader";

    private static final String[] SEARCH_REJECT_MIMES = new String[] { Document.MIME_TYPE_DIR };

    private final LockingContentObserver mObserver;
    private final RootInfo mRoot;
    private final Uri mUri;
    private final SortModel mModel;
    private final boolean mSearchMode;

    private DocumentInfo mDoc;
    private CancellationSignal mSignal;
    private DirectoryResult mResult;
    ///M: DRM refactory
    private int mDrmLevel;
    /// M: show previous loader's result @{
    private boolean mIsLoading = false;
    /// @}

    private Features mFeatures;

    public DirectoryLoader(
            Features freatures,
            Context context,
            RootInfo root,
            DocumentInfo doc,
            Uri uri,
            SortModel model,
            DirectoryReloadLock lock,
            boolean inSearchMode) {

        super(context, ProviderExecutor.forAuthority(root.authority));
        mFeatures = freatures;
        mRoot = root;
        mUri = uri;
        mModel = model;
        mDoc = doc;
        mSearchMode = inSearchMode;
        mObserver = new LockingContentObserver(lock, this::onContentChanged);
        /// M: DRM refactory
        try {
                mDrmLevel = ((FilesActivity) context).getIntent().getIntExtra
                 (OmaDrmStore.DrmIntentExtra.EXTRA_DRM_LEVEL, -1);
        } catch (ClassCastException e) {
                e.printStackTrace();
                mDrmLevel = ((PickActivity) context).getIntent().getIntExtra
                (OmaDrmStore.DrmIntentExtra.EXTRA_DRM_LEVEL, -1);
        }

    }

    @Override
    public final DirectoryResult loadInBackground() {
        mIsLoading = true;
        synchronized (this) {
            if (isLoadInBackgroundCanceled()) {
                throw new OperationCanceledException();
            }
            mSignal = new CancellationSignal();
        }

        final ContentResolver resolver = getContext().getContentResolver();
        final String authority = mUri.getAuthority();

        final DirectoryResult result = new DirectoryResult();
        result.doc = mDoc;

        ContentProviderClient client = null;
        Cursor cursor;
        try {
            client = DocumentsApplication.acquireUnstableProviderOrThrow(resolver, authority);
            if (mDoc.isInArchive()) {
                ArchivesProvider.acquireArchive(client, mUri);
            }
            result.client = client;

            Resources resources = getContext().getResources();
            if (mFeatures.isContentPagingEnabled()) {
                Bundle queryArgs = new Bundle();
                mModel.addQuerySortArgs(queryArgs);

                // TODO: At some point we don't want forced flags to override real paging...
                // and that point is when we have real paging.
                DebugFlags.addForcedPagingArgs(queryArgs);

                cursor = client.query(mUri, null, queryArgs, mSignal);
            } else {
                cursor = client.query(
                        mUri, null, null, null, mModel.getDocumentSortQuery(), mSignal);
            }

            if (cursor == null) {
                throw new RemoteException("Provider returned null");
            }

            cursor.registerContentObserver(mObserver);

            cursor = new RootCursorWrapper(mUri.getAuthority(), mRoot.rootId, cursor, -1);

            if (mSearchMode && !mFeatures.isFoldersInSearchResultsEnabled()) {
                // There is no findDocumentPath API. Enable filtering on folders in search mode.
                cursor = new FilteringCursorWrapper(cursor, null, SEARCH_REJECT_MIMES);
            } else {
                ///M: DRM refactory
                cursor = new FilteringCursorWrapper(cursor, mDrmLevel);
            }


            // TODO: When API tweaks have landed, use ContentResolver.EXTRA_HONORED_ARGS
            // instead of checking directly for ContentResolver.QUERY_ARG_SORT_COLUMNS (won't work)
            if (mFeatures.isContentPagingEnabled()
                        && cursor.getExtras().containsKey(ContentResolver.QUERY_ARG_SORT_COLUMNS)) {
                if (VERBOSE) Log.d(TAG, "Skipping sort of pre-sorted cursor. Booya!");
            } else {
                cursor = mModel.sortCursor(cursor);
            }
            result.cursor = cursor;
        } catch (Exception e) {
            Log.w(TAG, "Failed to query", e);
            result.exception = e;
        } finally {
            synchronized (this) {
                mSignal = null;
            }
            // TODO: Remove this call.
            ContentProviderClient.releaseQuietly(client);
        }
        mIsLoading = false;
        return result;
    }

    @Override
    public void cancelLoadInBackground() {
        super.cancelLoadInBackground();

        synchronized (this) {
            if (mSignal != null) {
                mSignal.cancel();
            }
        }
    }

    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager)
            getContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.android.documentsui.services.FileOperationService"
                .equals(service.service.getClassName())) {
                if (DEBUG) Log.d(TAG, "FileOpeartionService is running");
                return true;
            }
        }

        if (DEBUG) Log.d(TAG, "FileOpeartionService is not running");
        return false;
    }

    @Override
    public void deliverResult(DirectoryResult result) {
        if (isServiceRunning() && DirectoryFragment.isDeleteInProgress) {
            DirectoryFragment.delete_count--;
            if (DirectoryFragment.delete_count > 0) {
                Log.d(TAG, "deliverResult skip delete in progress delete_count "
                    + DirectoryFragment.delete_count);
                IoUtils.closeQuietly(result);
                return;
            }
            else {
                Log.d(TAG, "Reset isDeleteInProgress "
                                    + DirectoryFragment.delete_count);
                DirectoryFragment.isDeleteInProgress = false;
            }
        }
        else if (!isServiceRunning()) {
            DirectoryFragment.isDeleteInProgress = false;
            DirectoryFragment.delete_count = 0;
        }

        if (isReset()) {
            IoUtils.closeQuietly(result);
            return;
        }
        /// M: If the given result has exception with DeadObjectException type, it means
        /// client has died, we need load directory it again.
        if (isStarted() && result != null && result.exception != null
                && (result.exception instanceof DeadObjectException)) {
            Log.d(TAG, "deliverResult with client has dead, reload directory again");
            IoUtils.closeQuietly(result);
            forceLoad();
            return;
        }

        DirectoryResult oldResult = mResult;
        mResult = result;

        if (isStarted()) {
            super.deliverResult(result);
        }

        if (oldResult != null && oldResult != result) {
            IoUtils.closeQuietly(oldResult);
        }
    }

    @Override
    protected void onStartLoading() {
        /// M: show previous loader's result @{
        boolean contentChanged = takeContentChanged();
        if (mResult != null) {
            /// Check current contentprovider client, if the server has died, we need reload
            /// to register observer. @{
            try {
                mResult.client.canonicalize(mUri);
            deliverResult(mResult);
            } catch (Exception e) {
                contentChanged = true;
                Log.d(TAG, "onStartLoading with client has dead, reload to register obsever. " + e);
            }
            /// @}
        }
        Log.d(TAG, "onStartLoading contentChanged: " + contentChanged + ", mIsLoading: "
            + mIsLoading + ", mResult: " + mResult);
        if (!contentChanged && mIsLoading) {
            return;
        }

        ///M: For now we are always relying on force load to validate data.
        ///This is because of Google design in which multiple instances
        ///are created of Files APP. If this force load, creates problem
        ///we shall try for method of validating the cursor.
        //if (contentChanged || mResult == null) {
            forceLoad();
        //}
        /// @}
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    public void onCanceled(DirectoryResult result) {
        /// M: show previous loader's result @{
        if (result == null) {
            return;
        }
        if (result.exception != null && (result.exception instanceof OperationCanceledException)) {
        IoUtils.closeQuietly(result);
            Log.d(TAG, "DirectoryLoader: loading has been canceled, no deliver result");
            return;
        }
        if (!isReset() && (mResult == null)) {
            deliverResult(result);
            Log.d(TAG, "DirectoryLoader show result when onCanceled");
        } else {
            IoUtils.closeQuietly(result);
        }
        /// @}
    }

    @Override
    protected void onReset() {
        super.onReset();

        // Ensure the loader is stopped
        onStopLoading();

        IoUtils.closeQuietly(mResult);
        mResult = null;

        getContext().getContentResolver().unregisterContentObserver(mObserver);
    }

    private static final class LockingContentObserver extends ContentObserver {
        private final DirectoryReloadLock mLock;
        private final Runnable mContentChangedCallback;

        public LockingContentObserver(DirectoryReloadLock lock, Runnable contentChangedCallback) {
            super(new Handler(Looper.getMainLooper()));
            mLock = lock;
            mContentChangedCallback = contentChangedCallback;
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange) {
            mLock.tryUpdate(mContentChangedCallback);
        }
    }
}
