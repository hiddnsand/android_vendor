/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager.NameNotFoundException;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.util.Log;

//import com.android.internal.telephony.IccCardConstants.CardType;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;
import com.android.settings.dashboard.SummaryLoader;

import com.mediatek.internal.telephony.MtkIccCardConstants.CardType;
import com.mediatek.telephony.MtkTelephonyManagerEx;

/**
 * For dynamic show summary for DataConnectionSettings.
 */
public class UseLteDataSettingsSummaryProvider {

    public static final SummaryLoader.SummaryProviderFactory SUMMARY_PROVIDER_FACTORY
            = new SummaryLoader.SummaryProviderFactory() {
        @Override
        public SummaryProvider createSummaryProvider(Activity activity,
                SummaryLoader summaryLoader) {
            return new SummaryProvider(activity, summaryLoader);
        }
    };

    /**
     * For dynamic show summary implement.
     * @see com.android.settings.dashboard.SummaryLoader
     */
    private static class SummaryProvider implements SummaryLoader.SummaryProvider {
        private static final String TAG = "UseLteDataSettingsSummaryProvider";
        private static final String INTENT_ACTION_CARD_TYPE =
            TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED;
        private static final String INTENT_ACTION_DISMISS_DIALOG =
            "com.mediatek.settings.plugin.DISMISS_DIALOG";
        private final Context mContext;
        private final SummaryLoader mSummaryLoader;
        private Context mDesContext;

        public SummaryProvider(Context context, SummaryLoader summaryLoader) {
            mContext = context;
            mSummaryLoader = summaryLoader;
            try {
                mDesContext = mContext.createPackageContext(
                        "com.mediatek.settings.op09alib",
                        Context.CONTEXT_INCLUDE_CODE
                                | Context.CONTEXT_IGNORE_SECURITY);
            } catch (NameNotFoundException e) {
                Log.d(TAG, "NameNotFoundException", e);
            }
        }
        @Override
        public void setListening(boolean listening) {
            if (listening) {
                init();
                updateSummary();
            } else {
                stop();
            }
        }

        private void updateSummary() {
            String summary = getSummary();
            mSummaryLoader.setSummary(this, summary);
        }

        private String getSummary() {
            String summary = "unknown";
            boolean simInserted = Utils.isSIMInserted(PhoneConstants.SIM_ID_1);
            boolean airPlaneMode = Utils.isAirPlaneMode(mContext);
            boolean radioOn = Utils.isTargetSimRadioOn(PhoneConstants.SIM_ID_1);
            boolean callStateIdle = Utils.isCallStateIDLE(mContext);
            boolean simStateIsReady = Utils.isSimStateReady(mContext, PhoneConstants.SIM_ID_1);
            boolean isCTLTECard = MtkTelephonyManagerEx.getDefault()
                    .getCdmaCardType(PhoneConstants.SIM_ID_1) == CardType.CT_4G_UICC_CARD;
            boolean isTestCTCard = Utils.isCtTestcard();
            boolean isLTEPattern = Utils.isLTEPatternSupport(mContext);
            boolean nomalJudge = simInserted && !airPlaneMode && radioOn
                    && callStateIdle && simStateIsReady && isCTLTECard;

            Log.i(TAG, " nomalJudge = " + nomalJudge
                    + " simInserted = " + simInserted
                    + " airPlaneMode = " + airPlaneMode
                    + " radioOn = " + radioOn
                    + " callStateIdle = " + callStateIdle
                    + " simStateIsReady = " + simStateIsReady
                    + " isCTLTECard = " + isCTLTECard
                    + " isTestCTCard = " + isTestCTCard
                    + " isLTEPattern = " + isLTEPattern);
            if ((nomalJudge || isTestCTCard) && isLTEPattern) {
                summary = mDesContext.getResources().getString(R.string.uselte_summary_on);
            } else {
                summary = mDesContext.getResources().getString(R.string.uselte_summary_off);
            }
            return summary;
        }

        private void init() {
            TelephonyManager.getDefault().listen(mPhoneStateListener,
                    PhoneStateListener.LISTEN_CALL_STATE | PhoneStateListener.LISTEN_SERVICE_STATE);
            IntentFilter mIntentFilter = new IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED);
            mIntentFilter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
            mIntentFilter.addAction(ChangeNetworkType.ACTION_RAT_CHANGED);
            mIntentFilter.addAction(INTENT_ACTION_CARD_TYPE);
            mIntentFilter.addAction(INTENT_ACTION_DISMISS_DIALOG);
            mContext.registerReceiver(mReceiver, mIntentFilter);

        }

        private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                super.onCallStateChanged(state, incomingNumber);
                updateSummary();
            }

            @Override
            public void onServiceStateChanged(ServiceState serviceState) {
                Log.d(TAG, "onServiceStateChanged, new state = " + serviceState);
                updateSummary();
            }
        };
        private void stop() {
            TelephonyManager.getDefault().listen(
                    mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
            mContext.unregisterReceiver(mReceiver);
        }

        private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Log.i(TAG, "on receive broadcast action = " + action);
                if (action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
                    Log.i(TAG, "action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED) udpateSwitch");
                    updateSummary();
                } else if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(action)) {
                    int slotId = intent.getIntExtra(PhoneConstants.SLOT_KEY, 0);
                    if (slotId == PhoneConstants.SIM_ID_1) {
                        Log.i(TAG, "TelephonyIntents.ACTION_SIM_STATE_CHANGED udpateSwitch");
                        updateSummary();
                    }
                } else if (ChangeNetworkType.ACTION_RAT_CHANGED.equals(action)) {
                    updateSummary();
                } else if (TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED
                        .equals(intent.getAction())) {
                    updateSummary();
                }
            }
        };
    }

}
