/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Linux X11 Window implementation
 *
 */

// Required for key codes
#define XK_3270                       /* for print screen                    */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <X11/XKBlib.h>               /* for keysym bound to particular keys */
#include <X11/Xlib.h>                 /* for X11 functions                   */
#include <X11/keysym.h>               /* for key mapping                     */
#include <a3m/assert.h>               /* for A3M_ASSERT                      */
#include <a3m/log.h>                  /* for A3M_LOG_ERROR() etc.            */
#include <a3m/window.h>               /* for Window                          */
#include <eglcontext.h>               /* for EglContext                      */

namespace
{

  using namespace a3m;

  // This value is based on the observation that one turn of a scroll wheel on
  // Windows gives a value of 120, and one turn on Linux registers one mouse
  // press.  XInput may have a scroll amount parameter, but I couldn't find it.
  A3M_INT32 const MOUSE_WHEEL_SCROLL_SPEED = 120;

  // Mask all the required X events.
  A3M_INT32 const EVENT_MASK =
    StructureNotifyMask |   // Window move and resize
    KeyPressMask |          // Keyboard button press
    KeyReleaseMask |        // Keyboard button release
    ButtonPressMask |       // Mouse button press
    ButtonReleaseMask |     // Mouse button release
    ButtonMotionMask |      // Mouse motion while buttons pressed
    PointerMotionMask |     // Mouse motion while buttons not pressed
    LeaveWindowMask;        // Pointer leaving window

  /*
   * Converts an XInput mouse key to a mouse button value
   */
  MouseButton convertMouseButton(A3M_UINT32 button)
  {
    switch (button)
    {
    case Button1: return MOUSE_BUTTON_LEFT;
    case Button2: return MOUSE_BUTTON_RIGHT;
    case Button3: return MOUSE_BUTTON_MIDDLE;

    default:
      A3M_LOG_WARN("Mouse button %d (0x%X) is not supported.", button, button);
      return MOUSE_BUTTON_UNKNOWN;
    }
  }

  /*
   * Converts an XInput KeyCode to an A3M KeyCode.
   * XInput KeyCodes correspond to a physical key, and must be converted to a
   * KeySym, which represents a keyboard symbol, in order to be used.  The
   * problem is, KeyCodes correspond to multiple KeySyms.  Here, we simply take
   * the first available KeySym, but for a truly portable system, some kind of
   * KeySym priority might be required.
   */
  a3m::KeyCode convertKey(Display* display, ::KeyCode keyCode)
  {
    // Get the first KeySym associated with the code.
    A3M_INT32 keySymCount = 0;
    KeySym* keySyms = XGetKeyboardMapping(display, keyCode, 1, &keySymCount);
    KeySym keySym = (keySymCount == 0) ? XK_VoidSymbol : keySyms[0];
    XFree(keySyms);

    switch (keySym)
    {
    case XK_0:            return KEY_0;
    case XK_1:            return KEY_1;
    case XK_2:            return KEY_2;
    case XK_3:            return KEY_3;
    case XK_4:            return KEY_4;
    case XK_5:            return KEY_5;
    case XK_6:            return KEY_6;
    case XK_7:            return KEY_7;
    case XK_8:            return KEY_8;
    case XK_9:            return KEY_9;
    case XK_a: case XK_A: return KEY_A;
    case XK_b: case XK_B: return KEY_B;
    case XK_c: case XK_C: return KEY_C;
    case XK_d: case XK_D: return KEY_D;
    case XK_e: case XK_E: return KEY_E;
    case XK_f: case XK_F: return KEY_F;
    case XK_g: case XK_G: return KEY_G;
    case XK_h: case XK_H: return KEY_H;
    case XK_i: case XK_I: return KEY_I;
    case XK_j: case XK_J: return KEY_J;
    case XK_k: case XK_K: return KEY_K;
    case XK_l: case XK_L: return KEY_L;
    case XK_m: case XK_M: return KEY_M;
    case XK_n: case XK_N: return KEY_N;
    case XK_o: case XK_O: return KEY_O;
    case XK_p: case XK_P: return KEY_P;
    case XK_q: case XK_Q: return KEY_Q;
    case XK_r: case XK_R: return KEY_R;
    case XK_s: case XK_S: return KEY_S;
    case XK_t: case XK_T: return KEY_T;
    case XK_u: case XK_U: return KEY_U;
    case XK_v: case XK_V: return KEY_V;
    case XK_w: case XK_W: return KEY_W;
    case XK_x: case XK_X: return KEY_X;
    case XK_y: case XK_Y: return KEY_Y;
    case XK_z: case XK_Z: return KEY_Z;

    case XK_grave:        return KEY_GRAVE;
    case XK_minus:        return KEY_MINUS;
    case XK_equal:        return KEY_EQUALS;
    case XK_bracketleft:  return KEY_OPEN_BRACKET;
    case XK_bracketright: return KEY_CLOSE_BRACKET;
    case XK_semicolon:    return KEY_SEMICOLON;
    case XK_apostrophe:   return KEY_APOSTROPHE;
    case XK_numbersign:   return KEY_HASH;
    case XK_backslash:    return KEY_BACKSLASH;
    case XK_comma:        return KEY_COMMA;
    case XK_period:       return KEY_PERIOD;
    case XK_slash:        return KEY_SLASH;

    case XK_Escape:    return KEY_ESCAPE;
    case XK_Tab:       return KEY_TAB;
    case XK_Caps_Lock: return KEY_CAPS_LOCK;

    case XK_Shift_L:
    case XK_Shift_R:
      return KEY_SHIFT;

    case XK_Control_L:
    case XK_Control_R:
      return KEY_CONTROL;

    case XK_Alt_L:
    case XK_Alt_R:
      return KEY_ALT;

    case XK_Super_L:
    case XK_Super_R:
      return KEY_SUPER;

    case XK_Menu: return KEY_MENU;

    case XK_space:     return KEY_SPACE;
    case XK_BackSpace: return KEY_BACKSPACE;

    case XK_Return:
    case XK_KP_Enter:
      return KEY_RETURN;

    case XK_3270_PrintScreen:   return KEY_PRINT_SCREEN;
    case XK_Scroll_Lock:        return KEY_SCROLL_LOCK;
    case XK_Pause:              return KEY_PAUSE;

    case XK_Insert:    return KEY_INSERT;
    case XK_Delete:    return KEY_DELETE;
    case XK_Home:      return KEY_HOME;
    case XK_End:       return KEY_END;
    case XK_Prior:     return KEY_PAGE_UP;
    case XK_Next:      return KEY_PAGE_DOWN;

    case XK_Up:        return KEY_UP;
    case XK_Down:      return KEY_DOWN;
    case XK_Left:      return KEY_LEFT;
    case XK_Right:     return KEY_RIGHT;

    case XK_Num_Lock:                           return KEY_NUM_LOCK;
    case XK_KP_0:         case XK_KP_Insert:    return KEY_NUMPAD_0;
    case XK_KP_1:         case XK_KP_End:       return KEY_NUMPAD_1;
    case XK_KP_2:         case XK_KP_Down:      return KEY_NUMPAD_2;
    case XK_KP_3:         case XK_KP_Page_Down: return KEY_NUMPAD_3;
    case XK_KP_4:         case XK_KP_Left:      return KEY_NUMPAD_4;
    case XK_KP_5:         case XK_KP_Begin:     return KEY_NUMPAD_5;
    case XK_KP_6:         case XK_KP_Right:     return KEY_NUMPAD_6;
    case XK_KP_7:         case XK_KP_Home:      return KEY_NUMPAD_7;
    case XK_KP_8:         case XK_KP_Up:        return KEY_NUMPAD_8;
    case XK_KP_9:         case XK_KP_Page_Up:   return KEY_NUMPAD_9;
    case XK_KP_Add:                             return KEY_NUMPAD_ADD;
    case XK_KP_Subtract:                        return KEY_NUMPAD_SUBTRACT;
    case XK_KP_Multiply:                        return KEY_NUMPAD_MULTIPLY;
    case XK_KP_Divide:                          return KEY_NUMPAD_DIVIDE;
    case XK_KP_Decimal:   case XK_KP_Delete:    return KEY_NUMPAD_DECIMAL;
    case XK_KP_Separator:                       return KEY_NUMPAD_SEPARATOR;

    case XK_F1:        return KEY_F1;
    case XK_F2:        return KEY_F2;
    case XK_F3:        return KEY_F3;
    case XK_F4:        return KEY_F4;
    case XK_F5:        return KEY_F5;
    case XK_F6:        return KEY_F6;
    case XK_F7:        return KEY_F7;
    case XK_F8:        return KEY_F8;
    case XK_F9:        return KEY_F9;
    case XK_F10:       return KEY_F10;
    case XK_F11:       return KEY_F11;
    case XK_F12:       return KEY_F12;
    case XK_F13:       return KEY_F13;
    case XK_F14:       return KEY_F14;
    case XK_F15:       return KEY_F15;
    case XK_F16:       return KEY_F16;
    case XK_F17:       return KEY_F17;
    case XK_F18:       return KEY_F18;
    case XK_F19:       return KEY_F19;
    case XK_F20:       return KEY_F20;
    case XK_F21:       return KEY_F21;
    case XK_F22:       return KEY_F22;
    case XK_F23:       return KEY_F23;
    case XK_F24:       return KEY_F24;

    case XK_Cancel:    return KEY_CANCEL;
    case XK_3270_Play: return KEY_PLAY;
      // KEY_ZOOM not supported on Linux

    case XK_Clear:     return KEY_CLEAR;
    case XK_Execute:   return KEY_EXEC;
    case XK_Help:      return KEY_HELP;
    case XK_Print:     return KEY_PRINT;
    case XK_Select:    return KEY_SELECT;

    default:
      A3M_LOG_WARN("Key %d (0x%X) is not supported.", keySym, keySym);
      return KEY_UNKNOWN;
    }
  }

  /*
   * Implementation of Window interface for Linux.
   */
  class WindowLinux : public a3m::Window
  {
  public:
    /*
     * Constructor
     */
    WindowLinux(
      A3M_CHAR8 const* title,
      A3M_INT32 width,
      A3M_INT32 height,
      A3M_INT32 bpp,
      A3M_BOOL setClientArea);

    /*
     * Destructor
     */
    ~WindowLinux();

    // Override
    void refresh();
    // Override
    void setListener(WindowListener* listener);
    // Override
    void setSize(A3M_INT32 width, A3M_INT32 height);
    // Override
    A3M_INT32 getLeft() const;
    // Override
    A3M_INT32 getTop() const;
    // Override
    A3M_INT32 getWidth() const;
    // Override
    A3M_INT32 getHeight() const;
    // Override
    A3M_UINT32 getId() const;

  private:
    A3M_BOOL m_mouseIsCaptured;   /* Whether mouse is captured in window    */
    Display* m_display;           /* Display handle                         */
    ::Window m_window;            /* Window handle                          */
    Atom m_wmDeleteMessage;       /* XWindows window closed message         */
    A3M_INT32 m_left;             /* Window left edge position              */
    A3M_INT32 m_top;              /* Window top edge position               */
    A3M_INT32 m_width;            /* Window width                           */
    A3M_INT32 m_height;           /* Window height                          */
    EglContext::Ptr m_context;    /* EGL Context                            */
    WindowListener* m_listener;   /* Event listener                         */
  };

  WindowLinux::WindowLinux(
    A3M_CHAR8 const* title,
    A3M_INT32 width,
    A3M_INT32 height,
    A3M_INT32 bpp,
    A3M_BOOL setClientArea) :
    m_mouseIsCaptured(A3M_FALSE),
    m_display(0),
    m_left(0),
    m_top(0),
    m_width(width),
    m_height(height),
    m_listener(0)
  {
    // Open connection to the X-server.
    m_display = XOpenDisplay(0);

    // Root window, a.k.a. the screen.
    ::Window parent = DefaultRootWindow(m_display);

    A3M_INT32 blackColour = WhitePixel(m_display, DefaultScreen(m_display));

    m_window = XCreateSimpleWindow(
                 m_display,
                 parent,
                 0, // x
                 0, // y
                 m_width,
                 m_height,
                 0, // Border width
                 blackColour, // Border colour
                 blackColour); // Background colour

    // We only want to receive a certain set of events.
    XSetWindowAttributes attributes;
    attributes.event_mask = EVENT_MASK;

    XChangeWindowAttributes(
      m_display,
      m_window,
      CWEventMask,
      &attributes);

    // This allows us to intercept a click on the "close window" button.
    m_wmDeleteMessage = XInternAtom(m_display, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(m_display, m_window, &m_wmDeleteMessage, 1);

    // Show the window.
    XMapWindow(m_display, m_window);

    // Create OpenGL context.
    m_context.reset(new EglContext(m_display, m_window));

    if (!m_context->isValid())
    {
      A3M_LOG_ERROR("Failed to create a rendering context.");
      return;
    }
  }

  WindowLinux::~WindowLinux()
  {
    /* Destroy the rendering context */
    m_context.reset();

    if (m_window)
    {
      XDestroyWindow(m_display, m_window);
    }

    if (m_display)
    {
      XCloseDisplay(m_display);
    }
  }

  void WindowLinux::refresh()
  {
    /* Swap buffers before dispatching messages in case dispatching messages
     * causes a picking operation which might corrupt the back buffer */
    m_context->swapBuffers();

    XEvent xEvent;

    // Check for a "close window" button press.  The window will not close
    // automatically, so it's up to the listener to do decide what to do.
    if (XCheckTypedWindowEvent(
          m_display,
          m_window,
          ClientMessage,
          &xEvent))
    {
      if (static_cast<Atom>(xEvent.xclient.data.l[0]) == m_wmDeleteMessage)
      {
        if (m_listener)
        {
          m_listener->onClose(this);
        }

        return;
      }
    }

    // Check for keyboard and mouse input events.
    while (XCheckWindowEvent(
             m_display,
             m_window,
             EVENT_MASK,
             &xEvent))
    {
      if (m_listener)
      {
        switch (xEvent.type)
        {
        case KeyPress:
        {
          a3m::KeyCode key = convertKey(m_display, xEvent.xkey.keycode);
          m_listener->onKeyDown(this, key);
          break;
        }

        case KeyRelease:
        {
          a3m::KeyCode key = convertKey(m_display, xEvent.xkey.keycode);
          m_listener->onKeyUp(this, key);
          break;
        }

        case ButtonPress:
        {
          // XInput report mouse wheel scrolling as button presses.
          switch (xEvent.xbutton.button)
          {
            // Mouse scroll up
          case Button4:
          {
            m_listener->onMouseWheelScroll(this, MOUSE_WHEEL_SCROLL_SPEED);
            break;
          }

          // Mouse scroll down
          case Button5:
          {
            m_listener->onMouseWheelScroll(this, -MOUSE_WHEEL_SCROLL_SPEED);
            break;
          }

          default:
          {
            MouseButton button = convertMouseButton(xEvent.xbutton.button);
            A3M_INT32 x = xEvent.xbutton.x;
            A3M_INT32 y = xEvent.xbutton.y;
            m_listener->onMouseButtonDown(this, button, x, y);
            break;
          }
          }

          break;
        }

        case ButtonRelease:
        {
          switch (xEvent.xbutton.button)
          {
            // Ignore release of mouse scroll wheel "buttons".
          case Button4:
          case Button5:
          {
            break;
          }

          default:
          {
            MouseButton button = convertMouseButton(xEvent.xbutton.button);
            A3M_INT32 x = xEvent.xbutton.x;
            A3M_INT32 y = xEvent.xbutton.y;
            m_listener->onMouseButtonUp(this, button, x, y);
            break;
          }

          break;
          }
        }

        case MotionNotify:
        {
          A3M_INT32 x = xEvent.xbutton.x;
          A3M_INT32 y = xEvent.xbutton.y;
          m_listener->onMouseMove(this, x, y);
          break;
        }

        // Window state-change events.
        case ConfigureNotify:
        {
          A3M_INT32 left = xEvent.xconfigure.x;
          A3M_INT32 top = xEvent.xconfigure.y;

          if (left != m_left || top != m_top)
          {
            m_left = left;
            m_top = top;
            m_listener->onMove(this, m_left, m_top);
          }

          A3M_INT32 width = xEvent.xconfigure.width;
          A3M_INT32 height = xEvent.xconfigure.height;

          if (width != m_width || height != m_height)
          {
            m_width = width;
            m_height = height;
            m_listener->onResize(this, m_width, m_height);
          }

          break;
        }

        case LeaveNotify:
        {
          A3M_INT32 x = xEvent.xcrossing.x;
          A3M_INT32 y = xEvent.xcrossing.y;
          m_listener->onMouseLeave(this, x, y);
          break;
        }

        default:
          break;
        }
      }
    }
  }

  void WindowLinux::setListener(WindowListener* listener)
  {
    m_listener = listener;
  }

  // \todo This function has been added in Win32 version for gloviewer. There is
  // no demand in linux yet.
  void WindowLinux::setSize(A3M_INT32 width, A3M_INT32 height)
  {
    // Place holder.
  }

  A3M_INT32 WindowLinux::getLeft() const
  {
    return m_left;
  }

  A3M_INT32 WindowLinux::getTop() const
  {
    return m_top;
  }

  A3M_INT32 WindowLinux::getWidth() const
  {
    return m_width;
  }

  A3M_INT32 WindowLinux::getHeight() const
  {
    return m_height;
  }

  A3M_UINT32 WindowLinux::getId() const
  {
    return m_window;
  }

} // namespace

namespace a3m
{

  /*
   * Factory function.
   */
  Window::Ptr createWindow(
    const A3M_CHAR8* title,
    A3M_INT32 width,
    A3M_INT32 height,
    A3M_INT32 bpp,
    A3M_BOOL setClientArea)
  {
    return Window::Ptr(
             new WindowLinux(title, width, height, bpp, setClientArea));
  }

  void getScreenSize(A3M_INT32& width, A3M_INT32& height)
  {
    Display* display = XOpenDisplay(0);
    ::Window window = DefaultRootWindow(display);

    XWindowAttributes attributes;
    XGetWindowAttributes(display, window, &attributes);
    width = attributes.width;
    height = attributes.height;

    XCloseDisplay(display);
  }

} // namespace a3m

/*
 * This code demonstrates how to set an X11 window to fullscreen.  Currently,
 * we do not have similar functionality in Windows, but until we have the time
 * to implement fullscreen mode on all platforms, this code will remain here
 * as a reference.
 */
//static void pssSetWindowFullscreen(A3M_UINT32 window)
//{
//  XEvent event;
//  Atom wmState = XInternAtom(m_display, "_NET_WM_STATE", False);
//  Atom fullscreen = XInternAtom(m_display, "_NET_WM_STATE_FULLSCREEN", False);
//
//  event.type = ClientMessage;
//  event.xclient.window = window;
//  event.xclient.message_type = wmState;
//  event.xclient.format = 32;
//  event.xclient.data.l[0] = 1;
//  event.xclient.data.l[1] = fullscreen;
//  event.xclient.data.l[2] = 0;
//
//  XSendEvent(
//      m_display,
//      DefaultRootWindow(m_display),
//      False,
//      SubstructureNotifyMask,
//      &event);
//}
