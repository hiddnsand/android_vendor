package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class SmdSensorActivity extends BaseTriggerSensorActivity {

    public SmdSensorActivity() {
        super(Utils.KEY_SMD_STATUS, Utils.KEY_SMD_NOTIFY_STATUS, Utils.KEY_SMD_AUTO_STATUS,
                Sensor.TYPE_SIGNIFICANT_MOTION);
    }
}
