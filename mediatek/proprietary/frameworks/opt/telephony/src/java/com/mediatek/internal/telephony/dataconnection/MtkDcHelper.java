/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony.dataconnection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncResult;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.telephony.Rlog;
import android.telephony.CarrierConfigManager;
import android.telephony.ServiceState;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Xml;

import com.android.internal.telephony.dataconnection.ApnSetting;
import com.android.internal.telephony.IOnSubscriptionsChangedListener;
import com.android.internal.telephony.ITelephonyRegistry;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.SubscriptionController;
import com.android.internal.telephony.TelephonyDevController;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.uicc.IccRecords;
import com.android.internal.util.XmlUtils;

import com.mediatek.internal.telephony.IMtkTelephonyEx;
import com.mediatek.internal.telephony.MtkGsmCdmaPhone;
import com.mediatek.internal.telephony.MtkHardwareConfig;
import com.mediatek.internal.telephony.MtkSubscriptionController;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.MtkPhoneSwitcher;
import com.mediatek.internal.telephony.MtkSubscriptionController;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class MtkDcHelper extends Handler {
    private static final String LOG_TAG = "DcHelper";
    private static final boolean DBG = true;
    private static final boolean VDBG = SystemProperties.get("ro.build.type").
            equals("eng") ? true : false; // STOPSHIP if true

    // M: PS/CS concurrent
    private static MtkDcHelper sMtkDcHelper = null;
    protected Phone[] mPhones;
    protected int mPhoneNum;
    private Context mContext;

    // M: Query modem hardware capability
    private TelephonyDevController mTelDevController = TelephonyDevController.getInstance();

    // Multi-PS Attach
    private static final String PROP_DATA_CONFIG = "ro.mtk_data_config";
    private static final int DATA_CONFIG_MULTI_PS = 0x1;
    private boolean mHasFetchMpsAttachSupport = false;
    private boolean mMpsAttachSupport = false;
    private int mDefaultDataSubscription;
    private int mDefaultDataSlotId = SubscriptionManager.INVALID_SIM_SLOT_INDEX - 1;

    // event id must be multiple of EVENT_ID_INTVL
    private static final int EVENT_ID_INTVL = 10;
    private final static int EVENT_SUBSCRIPTION_CHANGED    = EVENT_ID_INTVL * 0;
    private static final int EVENT_RIL_CONNECTED = EVENT_ID_INTVL * 1;
    // M: PS/CS concurrent @{
    private static final int EVENT_VOICE_CALL_STARTED = EVENT_ID_INTVL * 2;
    private static final int EVENT_VOICE_CALL_ENDED = EVENT_ID_INTVL * 3;
    // M: PS/CS concurrent @}

    private static final int EVENT_DEFAULT_SUBSCRIPTION_CHANGED = EVENT_ID_INTVL * 4;

    // M: PS/CS concurrent @{
    private int mCallingPhoneId = SubscriptionManager.INVALID_PHONE_INDEX;
    // M: svlte denali can support two phone calls.
    private int mPrevCallingPhoneId = SubscriptionManager.INVALID_PHONE_INDEX;
    private static final String PROP_MTK_CDMA_LTE_MODE = "ro.boot.opt_c2k_lte_mode";
    public static final boolean MTK_SVLTE_SUPPORT = (SystemProperties.getInt(
            PROP_MTK_CDMA_LTE_MODE, 0) == 1);
    public static final boolean MTK_SRLTE_SUPPORT = (SystemProperties.getInt(
            PROP_MTK_CDMA_LTE_MODE, 0) == 2);
    // M: PS/CS concurrent @}

    /// M: Update APN bearer_bitmask field. @{
    public static final int APN_BEARER_BITMASK_ALL = 0x7FFFF;
    private static final String WIFI_APNS_PATH = "etc/wifi-apns.xml";
    private HashMap<String, Boolean> mWifiApns = new HashMap<String, Boolean>();
    /// @}

    // M: To get ICCID info.
    static final String PROPERTY_RIL_DATA_ICCID = "persist.radio.data.iccid";
    static private String[] PROPERTY_ICCID_SIM = {
        "ril.iccid.sim1",
        "ril.iccid.sim2",
        "ril.iccid.sim3",
        "ril.iccid.sim4",
    };

    private static final String[] PROPERTY_RIL_TEST_SIM = {
        "gsm.sim.ril.testsim",
        "gsm.sim.ril.testsim.2",
        "gsm.sim.ril.testsim.3",
        "gsm.sim.ril.testsim.4",
    };

    private static final String[]  PROPERTY_RIL_FULL_UICC_TYPE = {
        "gsm.ril.fulluicctype",
        "gsm.ril.fulluicctype.2",
        "gsm.ril.fulluicctype.3",
        "gsm.ril.fulluicctype.4",
    };

    private static final String INVALID_ICCID = "N/A";

    // M: Data on domestic roaming. @{
    public enum Operator {
        OP156, // Telenor
    }

    private static final Map<Operator, List> mOperatorMap = new HashMap<Operator, List>() {
        {
            put(Operator.OP156, Arrays.asList("23802", "23877"));
        }
    };
    // M: Data on domestic roaming. @}

    protected MtkDcHelper(Context context, Phone[] phones) {
        mContext = context;
        mPhones = phones;
        mPhoneNum = phones.length;
        registerEvents();
        loadWifiApnInfo();
    }

    public void dispose() {
        logd("MtkDcHelper.dispose");
        unregisterEvents();
    }

    public static MtkDcHelper makeMtkDcHelper(Context context, Phone[] phones) {

        if (context == null || phones == null) {
            throw new RuntimeException("param is null");
        }

        if (sMtkDcHelper == null) {
            logd("makeMtkDcHelper: phones.length=" + phones.length);
            sMtkDcHelper = new MtkDcHelper(context, phones);
        }

        logd("makesMtkDcHelper: X sMtkDcHelper =" + sMtkDcHelper);
        return sMtkDcHelper;
    }

    private Handler mRspHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // msg_id = n * EVENT_ID_INTVL + phone_id, use mod operator to get phone_id
            // event_id must be multiple of EVENT_ID_INTVL => n * EVENT_ID_INTVL
            int phoneId = msg.what % EVENT_ID_INTVL;
            int eventId = msg.what - phoneId;
            int restCallingPhoneId = SubscriptionManager.INVALID_PHONE_INDEX;
            AsyncResult ar;
            switch (eventId) {
                case EVENT_RIL_CONNECTED:
                    logd("EVENT_PHONE" + phoneId + "_EVENT_RIL_CONNECTED");
                    onCheckIfRetriggerDataAllowed(phoneId);
                    break;
                // M: PS/CS concurrent @{
                case EVENT_VOICE_CALL_STARTED:
                    if (MTK_SVLTE_SUPPORT &&
                        mCallingPhoneId != SubscriptionManager.INVALID_PHONE_INDEX) {
                        mPrevCallingPhoneId = mCallingPhoneId;
                        logd("SVLTE Voice Call2 Started, save first mPrevCallingPhoneId = "
                                + mPrevCallingPhoneId);
                    }
                    mCallingPhoneId = phoneId;
                    logd("Voice Call Started, mCallingPhoneId = " + mCallingPhoneId);
                    onVoiceCallStarted();
                    break;
                case EVENT_VOICE_CALL_ENDED:
                    logd("Voice Call Ended, mCallingPhoneId = " + mCallingPhoneId);
                    if (MTK_SVLTE_SUPPORT &&
                        mPrevCallingPhoneId != SubscriptionManager.INVALID_PHONE_INDEX) {
                        if (phoneId == mCallingPhoneId) {
                            restCallingPhoneId = mPrevCallingPhoneId;
                        } else {
                            restCallingPhoneId = mCallingPhoneId;
                        }
                        // record the left one call.
                        mCallingPhoneId = restCallingPhoneId;
                        mPrevCallingPhoneId = SubscriptionManager.INVALID_PHONE_INDEX;
                        logd("SVLTE Voice Call2 Ended, mCallingPhoneId = " + mCallingPhoneId);
                    }
                    onVoiceCallEnded();
                    mCallingPhoneId = SubscriptionManager.INVALID_PHONE_INDEX;
                    if (MTK_SVLTE_SUPPORT &&
                        restCallingPhoneId != SubscriptionManager.INVALID_PHONE_INDEX) {
                        mCallingPhoneId = restCallingPhoneId;
                        logd("SVLTE Voice Call Ended, restore first mCallingPhoneId = "
                                + mCallingPhoneId);
                    }
                    break;
                // M: PS/CS concurrent @}
                default:
                    logd("Unhandled message with number: " + msg.what);
                    break;
            }
        }
    };

    public static MtkDcHelper getInstance() {
        if (sMtkDcHelper == null) {
            throw new RuntimeException("Should not be called before makesMtkDcHelper");
        }
        return sMtkDcHelper;
    }

    @Override
    public void handleMessage(Message msg) {
        AsyncResult ar;
        int[] ints;
        Message message;
        logd("msg id = " + msg.what);
        switch (msg.what) {
            case EVENT_SUBSCRIPTION_CHANGED:
            case EVENT_DEFAULT_SUBSCRIPTION_CHANGED:
                logd("EVENT_SUBSCRIPTION_CHANGED");
                onSubscriptionChanged();
                break;
            default:
                logd("Unhandled msg: " + msg.what);
                break;
        }
    }

    // PS Attach enhancement start
    private final IOnSubscriptionsChangedListener mSubscriptionsChangedListener =
            new IOnSubscriptionsChangedListener.Stub() {
        @Override
        public void onSubscriptionsChanged() {
            Message msg = MtkDcHelper.this.obtainMessage(EVENT_SUBSCRIPTION_CHANGED);
            msg.sendToTarget();
        }
    };

    private final BroadcastReceiver mDefaultDataChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Message msg = MtkDcHelper.this.obtainMessage(EVENT_DEFAULT_SUBSCRIPTION_CHANGED);
            msg.sendToTarget();
        }
    };

    private void onSubscriptionChanged() {
        final int dataSub = SubscriptionController.getInstance().getDefaultDataSubId();
        if (dataSub != mDefaultDataSubscription) {
            mDefaultDataSubscription = dataSub;
            updateDefaultDataIccid(dataSub);
        }

        // M: Data Framework - common part enhancement @{
        final int slotId = MtkSubscriptionController.getMtkInstance().getSlotIndex(dataSub);
        if (slotId != mDefaultDataSlotId) {
            mDefaultDataSlotId = slotId;
            syncDefaultDataSlotId(slotId);
        }
        // M: Data Framework - common part enhancement @}
    }

    private void updateDefaultDataIccid(int dataSub) {
        int dataPhoneId = SubscriptionManager.getPhoneId(dataSub);
        String defaultIccid = "";
        if (dataPhoneId >= 0 && dataPhoneId < PROPERTY_ICCID_SIM.length) {
            defaultIccid = SystemProperties.get(PROPERTY_ICCID_SIM[dataPhoneId]);
            logd("updateDefaultDataIccid, Iccid = "
                    + SubscriptionInfo.givePrintableIccid(defaultIccid)
                    + ", dataPhoneId:" + dataPhoneId);
        } else {
            logd("updateDefaultDataIccid, invalid dataPhoneId:" + dataPhoneId);
        }

        if (defaultIccid.equals("") || defaultIccid.equals(INVALID_ICCID)) {
            /// M: The ICCID may be cleared as TRM or world mode switch,
            /// so, we need to get ICCID from subscriptioninfo.
            SubscriptionInfo subInfo = (SubscriptionInfo) MtkSubscriptionManager
                    .getSubInfo(null, dataSub);
            if (subInfo != null) {
                defaultIccid = subInfo.getIccId();
            }
        }
        logd("updateDefaultDataIccid: Set " + SubscriptionInfo.givePrintableIccid(defaultIccid));
        SystemProperties.set(PROPERTY_RIL_DATA_ICCID, defaultIccid);
    }

    // M: Retrigger data allow to phone switcher
    private void onCheckIfRetriggerDataAllowed(int phoneId) {
        logd("onCheckIfRetriggerDataAllowed: retriggerDataAllowed: mPhone[" + phoneId +"]");
        if (MtkPhoneSwitcher.getInstance() != null) {
            MtkPhoneSwitcher.getInstance().resendDataAllowed(phoneId);
        }
    }

    // PS Attach enhancement End

    public boolean isOperatorMccMnc(Operator opt, int phoneId) {
        String mccMnc = TelephonyManager.getDefault().getSimOperatorNumericForPhone(phoneId);
        boolean bMatched = mOperatorMap.get(opt).contains(mccMnc);

        logd("isOperatorMccMnc: mccmnc=" + mccMnc
                 + ", bMatched=" + bMatched);

        return bMatched;
    }

    // M: PS/CS concurrent feature start
    private void registerEvents() {
        logd("registerEvents");

        ITelephonyRegistry tr = ITelephonyRegistry.Stub.asInterface(
                ServiceManager.getService("telephony.registry"));
        try {
            tr.addOnSubscriptionsChangedListener("MtkDcHelper", mSubscriptionsChangedListener);
        } catch (RemoteException ex) {
            loge("Constructor Error:" + ex);
        }

        for (int i = 0; i < mPhoneNum; i++) {
            // Register event for radio capability change
            mPhones[i].mCi.registerForRilConnected(mRspHandler,
                    EVENT_RIL_CONNECTED + i, null);
            // M: PS/CS concurrent @{
            // Register events for GSM/CDMA call state.
            mPhones[i].getCallTracker().registerForVoiceCallStarted (mRspHandler,
                    EVENT_VOICE_CALL_STARTED + i, null);
            mPhones[i].getCallTracker().registerForVoiceCallEnded (mRspHandler,
                    EVENT_VOICE_CALL_ENDED + i, null);
            // M: PS/CS concurrent @}
        }
    }

    // M: PS/CS concurrent @{
    public void registerImsEvents(int phoneId) {
        logd("registerImsEvents, phoneId = " + phoneId);

        // Register events for IMS call state.
        // In multi PS project,  it allows VoLTE call in SIM1 while data on SIM2.
        // Therefore, it needs to handle the call state of IMS phone as well.
        // Only register events if IMS phone not null in the case of IMS service down.
        if (SubscriptionManager.isValidPhoneId(phoneId)
                && mPhones[phoneId].getImsPhone() != null) {
            mPhones[phoneId].getImsPhone().getCallTracker().
                    registerForVoiceCallStarted(mRspHandler,
                    EVENT_VOICE_CALL_STARTED + phoneId, null);
            mPhones[phoneId].getImsPhone().getCallTracker().
                    registerForVoiceCallEnded(mRspHandler,
                    EVENT_VOICE_CALL_ENDED + phoneId, null);
        } else {
            logd("Not register IMS phone calling state yet.");
        }
    }
    // M: PS/CS concurrent @}

    private void unregisterEvents() {
        logd("unregisterEvents");

        try {
            ITelephonyRegistry tr = ITelephonyRegistry.Stub.asInterface(ServiceManager.getService(
                    "telephony.registry"));
            if (tr != null) {
                tr.removeOnSubscriptionsChangedListener(
                        "MtkDcHelper", mSubscriptionsChangedListener);
            }
        } catch (RemoteException ex) {
            loge("dispose Error:" + ex);
        }

        // M: PS/CS concurrent @{
        for (int i = 0; i < mPhoneNum; i++) {
            // Unregister events for voice call
            mPhones[i].getCallTracker().unregisterForVoiceCallStarted(mRspHandler);
            mPhones[i].getCallTracker().unregisterForVoiceCallEnded(mRspHandler);
        }
        // M: PS/CS concurrent @}
    }

    // M: PS/CS concurrent @{
    public void unregisterImsEvents(int phoneId) {
        logd("unregisterImsEvents, phoneId = " + phoneId);

        // Unregister events for IMS call state.
        if (SubscriptionManager.isValidPhoneId(phoneId)
                && mPhones[phoneId].getImsPhone() != null) {
            mPhones[phoneId].getImsPhone().getCallTracker().
                    unregisterForVoiceCallStarted(mRspHandler);
            mPhones[phoneId].getImsPhone().getCallTracker().
                    unregisterForVoiceCallEnded(mRspHandler);
        } else {
            logd("Not unregister IMS phone calling state yet.");
        }
    }

    private void onVoiceCallStarted() {
        for (int i = 0; i < mPhoneNum; i++) {
            logd("onVoiceCallStarted: mPhone[ " + i +"]");
            ((MtkDcTracker) mPhones[i].mDcTracker).onVoiceCallStartedEx();
        }
    }

    private void onVoiceCallEnded() {
        for (int i = 0; i < mPhoneNum; i++) {
            logd("onVoiceCallEnded: mPhone[ " + i +"]");
            ((MtkDcTracker) mPhones[i].mDcTracker).onVoiceCallEndedEx();
        }
    }

    public boolean isDataSupportConcurrent(int phoneId) {
        if (mCallingPhoneId == SubscriptionManager.INVALID_PHONE_INDEX) {
            logd("isDataSupportConcurrent: invalid calling phone!");
            return false;
        }

        // SVLTE both phone is calling, it should be false.
        if (MTK_SVLTE_SUPPORT && mPrevCallingPhoneId != SubscriptionManager.INVALID_PHONE_INDEX) {
            return false;
        }

        // PS & CS on the same phone
        if (phoneId == mCallingPhoneId) {
            // Use sender's phone id (e.g. DcTracker) to query its services state.
            boolean isConcurrent = mPhones[phoneId].getServiceStateTracker()
                    .isConcurrentVoiceAndDataAllowed();
            logd("isDataSupportConcurrent:(PS&CS on the same phone) isConcurrent= "
                    + isConcurrent + "phoneId= " + phoneId + " mCallingPhoneId = "
                    + mCallingPhoneId);
            return isConcurrent;
        } else {
            // PS & CS not on the same phone
            if (MTK_SRLTE_SUPPORT) {
                //  For SRLTE, return false directly since DSDS.
                logd("isDataSupportConcurrent: support SRLTE ");
                return false;
            } else if (MTK_SVLTE_SUPPORT) {
                //  For SVLTE, need to check more conditions since DSDA.
                int phoneType = mPhones[mCallingPhoneId].getPhoneType();

                if (phoneType == PhoneConstants.PHONE_TYPE_CDMA) {
                    // If the calling phone is CDMA type(PS on the other phone is GSM), return true.
                    return true;
                } else {
                    // If the calling phone is GSM type, need to check the other phone's PS Rat.
                    // If the other phone's PS type is CDMA, return true, else, return false.
                    int rilRat = mPhones[phoneId].getServiceState().getRilDataRadioTechnology();
                    logd("isDataSupportConcurrent: support SVLTE RilRat = " + rilRat
                            + "calling phoneType: " + phoneType);

                    return (ServiceState.isCdma(rilRat));
                }
            } else {
                logd("isDataSupportConcurrent: not SRLTE or SVLTE ");
                return false;
            }
        }
    }

    public boolean isAllCallingStateIdle() {
        PhoneConstants.State [] state = new PhoneConstants.State[mPhoneNum];
        boolean allCallingState = false;
        for (int i = 0; i < mPhoneNum; i++) {
            state[i] = mPhones[i].getState();

            if (state[i] != null && state[i] == PhoneConstants.State.IDLE) {
                allCallingState = true;
            } else {
                allCallingState = false;
                break;
            }
        }

        if (!allCallingState && VDBG) {
            // For log reduction, only log if call state not IDLE and not shown in user load.
            for (int i = 0; i < mPhoneNum; i++) {
                logd("isAllCallingStateIdle: state[" + i + "]=" + state[i] +
                        " allCallingState = " + allCallingState);
            }
        }
        return allCallingState;
    }

    public boolean isWifiCallingEnabled() {
        boolean isWifiCallingEnabled = false;
        // in case the calling phone id being changed by other thread
        int callingPhoneId = mCallingPhoneId;
        int callingPhoneId2 = mPrevCallingPhoneId;

        IMtkTelephonyEx telephonyEx = IMtkTelephonyEx.Stub.asInterface(
                ServiceManager.getService("phoneEx"));
        if (telephonyEx != null) {
            try {
                if (SubscriptionManager.isValidPhoneId(callingPhoneId)) {
                    isWifiCallingEnabled = telephonyEx.isWifiCallingEnabled(
                            mPhones[callingPhoneId].getSubId());
                }
                if (MTK_SVLTE_SUPPORT && !isWifiCallingEnabled) {
                    if (SubscriptionManager.isValidPhoneId(callingPhoneId2)) {
                        isWifiCallingEnabled = telephonyEx.isWifiCallingEnabled(
                                mPhones[callingPhoneId2].getSubId());
                    }
                }
            } catch (RemoteException ex) {
                ex.printStackTrace();
            }
        }

        return isWifiCallingEnabled;
    }

    public static boolean isImsOrEmergencyApn(String[] apnTypes) {
        boolean isImsApn = true;
        if (apnTypes.length == 0) {
            return false;
        }
        for (String type : apnTypes) {
            if (!PhoneConstants.APN_TYPE_IMS.equals(type) &&
                    !PhoneConstants.APN_TYPE_EMERGENCY.equals(type)) {
                isImsApn = false;
                break;
            }
        }
        return isImsApn;
    }

    public static boolean hasVsimApn(String[] apnTypes) {
        boolean hasVsimApn = false;
        if (apnTypes.length == 0) {
            return false;
        }
        for (String type : apnTypes) {
            if (TextUtils.equals(MtkPhoneConstants.APN_TYPE_VSIM, type)) {
                hasVsimApn = true;
                break;
            }
        }
        return hasVsimApn;
    }
    // M: PS/CS concurrent @}

    public static boolean isSimInserted(int phoneId) {
        logd("isSimInserted:phoneId =" + phoneId);
        String iccid = SystemProperties.get(PROPERTY_ICCID_SIM[phoneId], "");
        return !TextUtils.isEmpty(iccid) && !INVALID_ICCID.equals(iccid);
    }

    public boolean isTestIccCard(int phoneId) {
        String testCard = null;

        testCard = SystemProperties.get(PROPERTY_RIL_TEST_SIM[phoneId], "");
        if (VDBG) logd("isTestIccCard: phoneId id = " + phoneId + ", iccType = " + testCard);
        return (testCard != null && testCard.equals("1"));
    }

    /**
    * M: Multi-PS attach support or not.
    * @return boolean true if support Multi-PS attach
    */
    public boolean isMultiPsAttachSupport() {
        if (!mHasFetchMpsAttachSupport) {
            int config = SystemProperties.getInt(PROP_DATA_CONFIG, 0);

            if ((config & DATA_CONFIG_MULTI_PS) == DATA_CONFIG_MULTI_PS) {
                mMpsAttachSupport = true;
            }
            mHasFetchMpsAttachSupport = true;
        }
        return mMpsAttachSupport;
    }

    // M: Data Framework - common part enhancement @{
    synchronized public void syncDefaultDataSlotId(int slotId) {
        // Set default data SIM to RILD
        int defaultSlotId = slotId;
        if (defaultSlotId <= SubscriptionManager.INVALID_SIM_SLOT_INDEX) {
            defaultSlotId = SubscriptionManager.INVALID_SIM_SLOT_INDEX;
        }
        logd("syncDefaultDataSlotId: slot ID: " + defaultSlotId);

        // Always use slot id 0 to set to MD.
        ((MtkDcTracker) mPhones[0].mDcTracker).syncDefaultDataSlotId(defaultSlotId);
    }
    // M: Data Framework - common part enhancement @}

    public boolean hasMdAutoSetupImsCapability() {
        if (mTelDevController != null && mTelDevController.getModem(0) != null
                && ((MtkHardwareConfig) mTelDevController.getModem(0))
                .hasMdAutoSetupImsCapability() == true) {
            logd("hasMdAutoSetupImsCapability: true");
            return true;
        }
        logd("hasMdAutoSetupImsCapability: false");
        return false;
    }

    /// M: Update APN bearer_bitmask field. @{
    private void loadWifiApnInfo() {
        new Thread("loadWifiApnInfo") {
            @Override
            public void run() {
                synchronized (mWifiApns) {
                    if (VDBG) logd("loadWifiApnInfo: Loading...");

                    File confFile = new File(Environment.getRootDirectory(), WIFI_APNS_PATH);
                    FileReader confreader = null;
                    XmlPullParser confparser = null;

                    try {
                        confreader = new FileReader(confFile);
                        confparser = Xml.newPullParser();
                        confparser.setInput(confreader);
                        XmlUtils.beginDocument(confparser, "apns");
                        XmlUtils.nextElement(confparser);

                        mWifiApns.clear();
                        while (confparser.getEventType() != XmlPullParser.END_DOCUMENT) {
                            if ("apn".equals(confparser.getName())) {
                                String mcc = confparser.getAttributeValue(null, "mcc");
                                String mnc = confparser.getAttributeValue(null, "mnc");
                                String apn = confparser.getAttributeValue(null, "apn");
                                String type = confparser.getAttributeValue(null, "type");
                                String mvnoType = confparser.getAttributeValue(null, "mvno_type");
                                String mvnoMatchData =
                                        confparser.getAttributeValue(null, "mvno_match_data");
                                String key = getWifiApnkey(mcc+mnc, apn, type, mvnoType,
                                        mvnoMatchData);
                                mWifiApns.put(key , true);
                            }
                            XmlUtils.nextElement(confparser);
                        }
                    } catch (FileNotFoundException e) {
                        loge("loadWifiApnInfo: File not found: '" +
                                Environment.getRootDirectory() + "/" + WIFI_APNS_PATH + "'");
                    } catch (XmlPullParserException e) {
                        loge("loadWifiApnInfo: XmlPullParserException while parsing '" +
                                confFile.getAbsolutePath() + "', " + e);
                    } catch (Exception e) {
                        loge("loadWifiApnInfo: Exception while parsing '" +
                                confFile.getAbsolutePath() + "', " + e);
                    } finally {
                        if (VDBG) logd("loadWifiApnInfo: Finish!");
                    }
                }
            }
        }.start();
    }

    public boolean isWifiApn(MtkApnSetting apn) {
        boolean isWifiApn = false;

        synchronized (mWifiApns) {
            for (String type: apn.types) {
                String key = getWifiApnkey(apn.numeric, apn.apn, type, apn.mvnoType,
                        apn.mvnoMatchData);
                isWifiApn = mWifiApns.containsKey(key);
                if (!isWifiApn) {
                    // If one of the apn type is not support wifi, then AP consider this APN
                    // is not support Wifi Bearer.
                    break;
                }
            }
        }

        return isWifiApn;
    }

    private String getWifiApnkey(String numeric, String apnName, String type,
                                 String mvnoType, String mvnoMatchData) {
        if (mvnoType == null)
            mvnoType = "";
        if (mvnoMatchData == null)
            mvnoMatchData = "";
        return (numeric + "/" + apnName + "/" + type + "/" + mvnoType + "/" + mvnoMatchData);
    }
    /// @}

    protected static void logv(String s) {
        if (DBG) {
            Rlog.v(LOG_TAG, s);
        }
    }

    protected static void logd(String s) {
        if (DBG) {
            Rlog.d(LOG_TAG, s);
        }
    }

    protected static void loge(String s) {
        if (DBG) {
            Rlog.e(LOG_TAG, s);
        }
    }

    protected static void logi(String s) {
        if (DBG) {
            Rlog.i(LOG_TAG, s);
        }
    }
}
