/*
 * Copyright (C) 2016 MediaTek Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See http://www.gnu.org/licenses/gpl-2.0.html for more details.
 */

/*
 * @file   tlDriverApi.h
 * @brief  Contains trustlet API definitions
 *
 */

#ifndef __TL_M_TPLAY_DRIVER_API_H__
#define __TL_M_TPLAY_DRIVER_API_H__

//#include "TlApi/TPlay.h"
#include "tlApitplay.h"

int32_t exDrmApi_AES128CTR_Init(/*OUT*/int32_t *pHandle, const uint8_t *pKey, uint32_t key_len);
int32_t exDrmApi_AES128CTR_DecryptData(int32_t Handle,
    const uint64_t pSrc, uint64_t pDst, uint32_t dataSize, /*INOUT*/ uint8_t iv[16], uint32_t aesSrcOffset, uint32_t bufOffset, EX_MEM_t DstMemType);
int32_t exDrmApi_AES128CTR_EncryptData(int32_t Handle,
    const uint64_t pSrc, uint64_t pDst, uint32_t dataSize, /*INOUT*/ uint8_t iv[16], uint32_t bufOffset, EX_MEM_t SrcMemType);
int32_t exDrmApi_AES128CTR_Finish(int32_t Handle);
int32_t exDrmApi_AES128CBC_Init(/*OUT*/int32_t *pHandle, const uint8_t *pKey, uint32_t key_len);
int32_t exDrmApi_AES128CBC_DecryptData(int32_t Handle,
    const uint64_t pSrc, uint64_t pDst, uint32_t dataSize, /*INOUT*/ uint8_t iv[16], uint32_t aesSrcOffset, uint32_t bufOffset, EX_MEM_t DstMemType);
int32_t exDrmApi_AES128CBC_Finish(int32_t Handle);
int32_t exDrmApi_CopyData( const void *pSrc, void *pDst, uint32_t dataSize, uint32_t bufOffset, EX_MEM_t DstMemType);
int32_t tlDrmApi_setHandleAddr( uint32_t handle_low_addr, uint32_t handle_high_addr );
int32_t tlDrmApi_dumpPhyAddr();
int32_t tlDebugApiGetDecryptionResult(/*OUT*/ uint8_t *pbOutBuffer, /*IN*/ uint32_t cbOutBufferLen, /*IN*/ uint32_t cbOutputOffset);

/** a single flag variable (32-bit) in the secured display driver.
 *  sets bit in the flag variable.
 *
 * @param bitToSet the bit need to be set
 */
int exDrmApi_setLinkFlag(unsigned int bitToSet);

/** a single flag variable (32-bit) in the secured display driver.
 *  clears specific bit in the flag variable.
 *
 * @param bitToSet the bit need to be cleared.
 */
int exDrmApi_clearLinkFlag(unsigned int bitToClear);


/** a single flag variable (32-bit) in the secured display driver.
 *  queries a single bit in the flag variable.
 *
 * @param bitToSet the bit need to be queried.
 * @return the value of the queried bit.
 */
int exDrmApi_testLinkFlag(unsigned int bitToTest);

#if 0
int _tlDrmApi_processDrmContent_DONT_USE (
    unsigned char                             sHandle,
    tlApiDrmDecryptContext_t            decryptCtx,
    unsigned char                             *input,
    tlApiDrmInputSegmentDescriptor_t    inputDesc,
    unsigned int16_t                            processMode,
    unsigned char                             *rfu);
#endif

#endif // __TLDRIVERAPI_H__
