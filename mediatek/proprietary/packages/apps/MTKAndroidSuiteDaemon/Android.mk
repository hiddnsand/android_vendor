ifneq ($(strip $(OPTR_SPEC_SEG_DEF)),NONE)
ifeq ($(strip $(OPTR_SPEC_SEG_DEF)),$(filter $(OPTR_SPEC_SEG_DEF),OP01_SPEC0200_SEGC OP02_SPEC0200_SEGA OP09_SPEC0212_SEGC OP09_SPEC0212_SEGDEFAULT))
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)
 
LOCAL_MODULE_TAGS := optional
 
LOCAL_STATIC_JAVA_LIBRARIES := libutil

LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_EMMA_COVERAGE_FILTER := @$(LOCAL_PATH)/emma_filter.txt,--$(LOCAL_PATH)/emma_method_filter.txt

LOCAL_PACKAGE_NAME := MTKAndroidSuiteDaemon
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_CERTIFICATE := platform
#LOCAL_JAVA_LIBRARIES += mediatek-framework telephony-common mediatek-telephony-common
LOCAL_JAVA_LIBRARIES += mediatek-framework
LOCAL_JAVA_LIBRARIES += mediatek-common
LOCAL_JAVA_LIBRARIES += mediatek-telephony-common mediatek-telephony-base
include $(BUILD_PACKAGE)
##################################################
include $(CLEAR_VARS)

LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := libutil:APSTEntity.jar

include $(BUILD_MULTI_PREBUILT)

# Use the folloing include to make our test apk.
include $(call all-makefiles-under,$(LOCAL_PATH))
endif
endif