/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#pragma once
#ifndef JNI_UTILITY_H
#define JNI_UTILITY_H

#include <jni.h> /* for JNI API */
#include <a3m/base_types.h>  /* for base types */
#include <a3m/log.h>         /* for logging */
#include <a3m/noncopyable.h> /* for NonCopyable */

/**
 * Manages a Java global reference.
 * It is essential when programming using the JNI, that any Java object stored
 * outside of the scope of the JNI function call be stored as a global
 * reference.  This is true whether the object was created manually, or passed
 * in from the Java side.  This class automatically increments and
 * decrements the reference count.
 *
 * The jclass type also count as a Java object, but jmethodID and jfieldID
 * don't; thus, fields and methods can be cached for later use without storing
 * a global reference (or even a global reference of the class to which they
 * belong).
 */
template<typename T>
class GlobalRef : a3m::NonCopyable
{
public:
  /**
   * Constructor.
   */
  GlobalRef(JNIEnv* env /**< Java environment */);

  /**
   * Constructor.
   */
  GlobalRef(
    JNIEnv* env,
    /**< Java environment */
    T jObject,
    /**< Java object to reference */
    A3M_BOOL incRefCount = A3M_TRUE
    /**< TRUE to increase the reference count */);

  /**
   * Destructor.
   */
  ~GlobalRef();

  /**
   * Sets the Java object referenced.
   */
  void reset(
    T jObject = 0,
    /**< Java object to reference */
    A3M_BOOL incRefCount = A3M_TRUE
    /**< TRUE to increase the reference count */);

  /**
   * Returns the Java object referenced.
   * \return Java object
   */
  T get() const;

private:
  JavaVM* m_vm;   /* Java VM                */
  T m_jObject;    /* Java object referenced */
};

/**
 * Utility class to convert a Java string to a C string and handle the memory
 * deallocation.
 */
class CString : a3m::NonCopyable
{
public:
  /**
   * Constructs a CString from a Java string.
   */
  CString(
      JNIEnv* env, /**< Java environment */
      jstring string /**< Java string */);

  /**
   * Constructs a CString from a null terminated C-string.
   */
  CString(
      JNIEnv* env, /**< Java environment */
      A3M_CHAR8 const* string /**< Null terminated C-string */);

  /**
   * Destructor.
   */
  ~CString();

  /**
   * Returns this string as a Java string.
   * \return Java string
   */
  jstring getJString() const;

  /**
   * Returns this string as a C-string.
   * \return C-string
   */
  A3M_CHAR8 const* getString() const;

  /**
   * Returns whether the string has been correctly constructed.
   * Do not use the string if this returns FALSE.
   * \return TRUE if the string is valid
   */
  bool isValid() const;

private:
  JavaVM* m_vm;                   /* Java VM      */
  GlobalRef<jstring> m_jString;   /* Java string  */
  const char* m_cString;          /* C-string     */
};

/**
 * Utility class to convert a Java byte array to a C byte array and handle the
 * memory deallocation.
 */
class CByteArray : a3m::NonCopyable
{
public:
  /**
   * Creates a CByteArray from a Java ByteArray.
   */
  CByteArray(
      JNIEnv* env,
      /**< Java environment */
      jbyteArray byteArray
      /**< Java ByteArray */);

  /**
   * Creates a new CByteArray of a given size.
   */
  CByteArray(
      JNIEnv* env, /**< Java environemtn */
      jsize size /**< Size of array */);

  /**
   * Destructor.
   */
  ~CByteArray();

  /**
   * Commits any changes made to the array to the linked Java ByteArray.
   * Calling commit will invalidate the array, and arrays constructed from a
   * Java ByteArray will not be committed back to the array.
   */
  void commit();

  /**
   * Returns the underlying Java ByteArray.
   * \return Java ByteArray
   */
  jbyteArray getJByteArray() const;

  /**
   * Returns a non-const pointer to the array contents.
   * This pointer can be used to modify the array, but the Java ByteArray will
   * not be updated until commit() is called.
   * \return Non-const array pointer
   */
  jbyte* getByteArray();

  /**
   * Returns a const pointer to the array contents.
   * \return Const array pointer
   */
  jbyte const* getByteArray() const;

  /**
   * Returns the length of the array.
   * \return Length of array
   */
  A3M_INT32 getLength() const;

  /**
   * Returns whether the array has been successfully constructed.
   * You should not use the array if it is not valid.
   * \return TRUE if the array is valid
   */
  A3M_BOOL isValid() const;

private:
  JavaVM* m_vm;                         /* Java VM        */
  GlobalRef<jbyteArray> m_jByteArray;   /* Java ByteArray */
  jbyte* m_cByteArray;                  /* C-byte array   */
  jint m_jMode;                         /* Commit mode    */
};

/*****************************************************************************
 *                              GlobalRef                                   *
 *****************************************************************************/

template<typename T>
GlobalRef<T>::GlobalRef(JNIEnv* env) :
  m_jObject(0)
{
  if (env->GetJavaVM(&m_vm) < 0)
  {
    A3M_LOG_ERROR("Failed to acquire JavaVM", 0);
    return;
  }
}

template<typename T>
GlobalRef<T>::GlobalRef(JNIEnv* env, T jObject, A3M_BOOL incRefCount) :
  m_jObject(0)
{
  if (env->GetJavaVM(&m_vm) < 0)
  {
    A3M_LOG_ERROR("Failed to acquire JavaVM", 0);
    return;
  }

  reset(jObject, incRefCount);
}

template<typename T>
GlobalRef<T>::~GlobalRef()
{
  reset();
}

template<typename T>
void GlobalRef<T>::reset(T jObject, A3M_BOOL incRefCount)
{
  JNIEnv* env;
  if (m_vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
  {
    A3M_LOG_ERROR("Failed to acquire JNIEnv", 0);
    return;
  }

  if (m_jObject)
  {
    env->DeleteGlobalRef(m_jObject);
  }

  m_jObject = jObject;

  if (incRefCount)
  {
    m_jObject = static_cast<T>(env->NewGlobalRef(m_jObject));
  }
}

template<typename T>
T GlobalRef<T>::get() const
{
  return m_jObject;
}

#endif // JNI_UTILITY_H
