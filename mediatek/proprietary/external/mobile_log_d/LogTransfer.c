#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <fcntl.h>
#include <termios.h>
#include <log/log.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <cutils/properties.h>

#include "global_var.h"
#include "mlog.h"

union ui2char {
	unsigned int d;
	char c[4];
};
typedef union ui2char ui2char_t;

struct file_header {
	char name[64]; // file name
	unsigned int len; // file size
	struct file_header *next;
};
typedef struct file_header header_t;

static void insert_to_list(header_t *node, header_t *list)
{
	node->next = list->next;
	list->next = node;
}

int port_fd = -1; // for send log to meta usb com

int init_com_port()
{
	/*   ::Test::
	#define dst  "/data/misc/mblog/aplog-com"
	dstFd = TEMP_FAILURE_RETRY(open(dst, O_WRONLY | O_CREAT | O_APPEND, 0664));
	if (dstFd < 0) {
		MLOGE_BOTH("open %s fail (%s)", dst, strerror(errno));
		return -1;
	}
	*/

	if (port_fd != -1) {
		close(port_fd);
	}

	const char* com_dev_name;
	com_dev_name = "/dev/ttyGS3";
	port_fd = open(com_dev_name, O_RDWR | O_NOCTTY);
	if (port_fd == -1) {
		MLOGE_BOTH("Open com device: %s fail (%s)", com_dev_name, strerror(errno));
		return -1;
	} else
		MLOGI_BOTH("Open com device: %s successfully, port_fd:%d", com_dev_name, port_fd);

	struct termios termOptions;

	if(fcntl(port_fd, F_SETFL, 0) == -1)
		MLOGE_BOTH("fcntl port_fd fail (%s)", strerror(errno));

	// Get its current options
	tcgetattr(port_fd, &termOptions);

	// raw mode
	termOptions.c_iflag &= ~(INLCR | ICRNL | IXON | IXOFF | IXANY);
	termOptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); /*raw input*/
	termOptions.c_oflag &= ~OPOST; /*raw output*/

	tcflush(port_fd, TCIFLUSH); //clear input buffer
	termOptions.c_cc[VTIME] = 1; /* inter-character timer unused */
	termOptions.c_cc[VMIN] = 0; /* blocking read until 0 character arrives */

	tcsetattr(port_fd, TCSANOW, &termOptions);
	return 0;
}
int send_to_com(const char* str, unsigned int size, const char* log_dir, header_t*  node)
{
	char src[256];
	int srcFd, dstFd;
	unsigned int len = 0;
	ui2char_t len_chars;
	int ret = 0;

	char buf[8192]; // buffer for read and write
	ssize_t readCount, writeCount;
	ssize_t total_len = 0;

	dstFd = port_fd;
	/* Only write special tag */
	if (log_dir == NULL || node == NULL) {
		if (str != NULL) {
			writeCount = TEMP_FAILURE_RETRY(write(dstFd, str, size));
			MLOGI_BOTH("str: %s, writeCount: %d", str, writeCount);
			if (writeCount < 0) {
				MLOGE_BOTH("Failed send to port: %d, %s", dstFd, strerror(errno));
				if (errno == 5 || errno == 9) {
					init_com_port();
				}
				ret = -1;
			}
		} else
			ret = -1;
		return ret;
	}

	// max size for written
	len = node->len;
	len_chars.d = len;
	// source file
	snprintf(src, sizeof(src), "%s%s", log_dir, node->name);
	// open src
	srcFd = TEMP_FAILURE_RETRY(open(src, O_RDONLY, 0));
	if (srcFd < 0) {
		MLOGE_BOTH("open %s fail (%s)", src, strerror(errno));
		return -1;
	}

	/* write file header */
	writeCount = TEMP_FAILURE_RETRY(write(dstFd, node->name, sizeof(node->name)));
	if (writeCount < 0) {
		MLOGE_BOTH("Failed to send %s, %s", node->name, strerror(errno));
		if (errno == 5 || errno == 9) {
			init_com_port();
		}
		ret = -1;
	}
	writeCount = TEMP_FAILURE_RETRY(write(dstFd, len_chars.c, sizeof(len_chars.c)));
	if (writeCount < 0) {
		MLOGE_BOTH("Failed to send %s, %s", len_chars.c, strerror(errno));
		if (errno == 5 || errno == 9) {
			init_com_port();
		}
		ret = -1;
	}

	/*
	*  Read a chunk, write it, and repeat.
	*/
	while (1) {
		readCount = TEMP_FAILURE_RETRY(read(srcFd, buf, sizeof(buf)));
		if (readCount < 0) {
			MLOGE_BOTH("Failed reading %s, %s", src, strerror(errno));
			ret = -1;
			break;
		}

		total_len += readCount;
		if (readCount > 0 && total_len > (ssize_t) len) {
			MLOGE_BOTH("read %s oversize, readCount: %d, over: %d", src, readCount, total_len - len);
			readCount -= total_len - len;
		}
		if (readCount > 0) {
			writeCount = TEMP_FAILURE_RETRY(write(dstFd, buf, readCount));
			if (writeCount < 0) {
				MLOGE_BOTH("Failed writing to port: %d, %s", dstFd, strerror(errno));
				if (errno == 5 || errno == 9) {
					init_com_port();
				}
				ret = -1;
				break;
			}
			if (writeCount != readCount) {
				MLOGE_BOTH("Partial write to port(%d), (%zd of %d)", dstFd, writeCount, readCount);
				ret = -1;
				break;
			}
		}

		if (total_len >= (ssize_t) len || readCount < (ssize_t) sizeof(buf)) {
			break;
		}

	}
	close(srcFd);
	return ret;
}

void* packlog_to_com(void* arg __attribute__((__unused__)))
{
	prctl(PR_SET_NAME, "mobile_log_d.pack_send");

	DIR *dp;
	struct dirent *entry;
	struct stat st;
	char log_dir[256];
	char file_path[256];
	unsigned int file_count = 0;

	header_t *list = NULL;
	header_t *node = NULL;
	header_t *cur_node = NULL;
	header_t *free_node = NULL;

	snprintf(log_dir, sizeof(log_dir), "%s", cur_logging_path);
	if (log_dir[strlen(log_dir) - 1] != '/')
		log_dir[strlen(log_dir)] = '/';
	MLOGI_BOTH("Pack Log: %s...........", log_dir);
	// test path::
	//strcpy(log_dir, "/mnt/sdcard/mtklog/mobilelog/APLog/");


	/* set prop, debug.MB.packed 0 */
	property_set(PROP_PACKED, "0");

	// init com port
	if (init_com_port() == -1) {
		MLOGE_BOTH("Failed to init com port");
		return NULL;
	}

	/* search AP log file to send */
	dp = opendir(log_dir);
	if (dp == NULL) {
		MLOGE_BOTH("Failed to opendir %s, (%s)", log_dir, strerror(errno));
		return NULL;
	}
	// init  file_header list
	list = (header_t *)malloc(sizeof(header_t));
	if (list == NULL) {
		MLOGE_BOTH("pack: malloc header_t list fail");
		closedir(dp);
		return NULL;
	}
	memset(list->name, 0x00, sizeof(list->name));
	list->len = 0;
	list->next = NULL;

	// Loop through the directory stream, create 'list'
	while((entry = readdir(dp)) != NULL) {
		if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")
			|| entry->d_type == DT_DIR) {
			//MLOGI_BOTH("pack_Log, file:%s, file type:%u", entry->d_name, entry->d_type);
			continue;
		}

		// create header_t node
		node = (header_t *)malloc(sizeof(header_t));
		if (node == NULL) {
			MLOGE_BOTH("pack: malloc header_t fail");
			continue;
		}
		memset(node->name, 0x00, sizeof(node->name));
		snprintf(node->name, sizeof(node->name), "%s", entry->d_name);
		snprintf(file_path, sizeof(file_path), "%s%s", log_dir, node->name);
		//MLOGI_BOTH("stat %s", file_path);
		if (stat(file_path, &st) == -1) {
			MLOGE_BOTH("Failed to stat %s (%s)", file_path, strerror(errno));
			node->len = 0;
		} else {
			file_count++;
			node->len = st.st_size;
		}
		node->next = NULL;
		// insert the 'node' to 'list'
		insert_to_list(node, list);

	}
	MLOGI_BOTH("Pack log file_count: %u", file_count);
	if (closedir(dp)) {
		MLOGE_BOTH("Failed to closedir %s, (%s)", log_dir, strerror(errno));
	}

	/**                                      --- send AP logs to com port ---
	*
	*
	*       16              4           64 + 4                           64 + 4                                   16 (Byte)
	*    PREFIX | file Count |file1 Header | file1 Data | file2 Header | file2 Data | ... | POSTFIX
	*
	*
	*/

	#define PREFIX       "mobilelog_start"
	#define POSTFIX      "mobilelog_stop"
	char prefix[16];
	char postfix[16];
	memset(prefix, 0x00, sizeof(prefix));
	memset(postfix, 0x00, sizeof(postfix));
	snprintf(prefix, sizeof(prefix), "%s", PREFIX);
	snprintf(postfix, sizeof(postfix), "%s", POSTFIX);
	ui2char_t file_cnt;
	file_cnt.d = file_count;

	send_to_com(prefix, 16, NULL, NULL); // Send PREFIX
	send_to_com(file_cnt.c, 4, NULL, NULL); // Send File Count

	// Send File Header + File Data
	///*
	cur_node = list->next;
	while (cur_node) {
		MLOGI_BOTH("pack log name: %s, len: %u", cur_node->name, cur_node->len);

		send_to_com(NULL, 0, log_dir, cur_node);
		cur_node = cur_node->next;
	}
	//*/
	send_to_com(postfix, 16, NULL, NULL); // Send POSTFIX

	usleep(50 * 1000); // wait for Data flush

	if (port_fd != -1) {
		close(port_fd);
	}

	/* set prop, debug.MB.packed 1, META AP_TST will check this property */
	property_set(PROP_PACKED, "1");

	/* free the whole 'list' */
	cur_node = list;
	while(cur_node) {
		free_node = cur_node;
		cur_node = cur_node->next;
		//MLOGI_BOTH("free %s", free_node->name);
		free(free_node);
	}

	//usleep(60 * 1000000); // 60 seconds
	return NULL;
}

