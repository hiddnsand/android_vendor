/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.server.am;

import static com.android.server.am.ActivityManagerDebugConfig.APPEND_CATEGORY_NAME;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_ALL;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_ALL_ACTIVITIES;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_ADD_REMOVE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_ANR;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_APP;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_BACKGROUND_CHECK;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_BACKUP;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_BROADCAST;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_BROADCAST_BACKGROUND;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_BROADCAST_LIGHT;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_CLEANUP;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_CONFIGURATION;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_CONTAINERS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_FOCUS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_IDLE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_IMMERSIVE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_LOCKSCREEN;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_LOCKTASK;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_LRU;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_MU;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_NETWORK;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_OOM_ADJ;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_OOM_ADJ_REASON;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_PAUSE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_POWER;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_POWER_QUICK;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_PROCESS_OBSERVERS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_PROCESSES;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_PROVIDER;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_PSS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_RECENTS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_RELEASE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_RESULTS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_SAVED_STATE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_SCREENSHOTS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_SERVICE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_FOREGROUND_SERVICE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_SERVICE_EXECUTING;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_STACK;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_STATES;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_SWITCH;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_TASKS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_THUMBNAILS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_TRANSITION;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_UID_OBSERVERS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_URI_PERMISSION;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_USER_LEAVING;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_VISIBILITY;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_USAGE_STATS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_PERMISSIONS_REVIEW;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_WHITELISTS;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.ArrayMap;
import android.util.Slog;
import android.util.SparseArray;

import com.android.internal.app.procstats.ProcessStats;
import com.android.internal.os.ProcessCpuTracker;
import com.android.server.am.ActivityRecord;
import com.android.server.am.ProcessRecord;

import com.mediatek.server.powerhal.PowerHalManager;
import com.mediatek.server.powerhal.PowerHalManagerImpl;
/// M: STK IDLE SCREEN feature {
import com.mediatek.stk.IdleScreen;
/// M: STK IDLE SCREEN feature }

/// M: DuraSpeed @{
import com.mediatek.duraspeed.appworkingset.IAWSProcessRecord;
import com.mediatek.duraspeed.appworkingset.IAWSStoreRecord;
import com.mediatek.duraspeed.service.DuraSpeedService;
/// @}

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class AmsExtImpl extends AmsExt {
    private static final String TAG = "AmsExtImpl";
    private boolean isDebug = false;
    private boolean isDuraSpeedSupport = "1".equals
            (SystemProperties.get("persist.duraspeed.support"));
    public PowerHalManagerImpl mPowerHalManagerImpl = null;
    /// M: STK IDLE SCREEN feature {
    private IdleScreen mIdleScreen = null;
    /// M: STK IDLE SCREEN feature }

    /// M: DuraSpeed @{
    private DuraSpeedService mDuraSpeedService;
    static final long APP_SWITCH_DELAY_TIME = 5*1000;
    /// @}
    private final String amsLogProp = "persist.sys.activitylog";

    public AmsExtImpl() {
        mPowerHalManagerImpl = new PowerHalManagerImpl();
        /// M: Duraspeed @{
        if (isDuraSpeedSupport) {
            mDuraSpeedService = DuraSpeedService.getInstance();
        }
        /// @}
    }

    @Override
    public void onSystemReady(Context context) {
        Slog.d(TAG, "onSystemReady");
        /// M: STK IDLE SCREEN feature {
        if (mIdleScreen == null) {
            mIdleScreen = new IdleScreen();
        }
        mIdleScreen.onSystemReady(context);
        /// M: STK IDLE SCREEN feature {

        /// M: Duraspeed @{
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            mDuraSpeedService.onSystemReady();
        }
        /// @}
    }

    @Override
    public void onBeforeActivitySwitch(ActivityRecord lastResumedActivity,
                                       ActivityRecord nextResumedActivity,
                                       boolean pausing,
                                       int nextResumedActivityType) {
        if (nextResumedActivity == null || nextResumedActivity.info == null
                || lastResumedActivity == null) {
            return;
        } else if (nextResumedActivity.packageName == lastResumedActivity.packageName
                && nextResumedActivity.info.name == lastResumedActivity.info.name) {
            // same activity
            return;
        }

        String lastResumedPackageName = lastResumedActivity.packageName;
        String nextResumedPackageName = nextResumedActivity.packageName;
        if (isDebug) {
            Slog.d(TAG, "onBeforeActivitySwitch, lastResumedPackageName=" + lastResumedPackageName
                    + ", nextResumedPackageName=" + nextResumedPackageName);
        }

        if (mPowerHalManagerImpl != null) {
            mPowerHalManagerImpl.amsBoostResume(lastResumedPackageName, nextResumedPackageName);
        }
        /// M: Duraspeed @{
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            boolean isNeedToPauseActivityFirst = pausing;
            mDuraSpeedService.onBeforeActivitySwitch(lastResumedPackageName,
                    nextResumedPackageName, isNeedToPauseActivityFirst,
                    nextResumedActivityType);
        }
        /// @}
    }

    @Override
    public void onAfterActivityResumed(ActivityRecord resumedActivity) {
        if (resumedActivity.app == null) {
            return;
        }

        int pid = resumedActivity.app.pid;
        String activityName = resumedActivity.info.name;
        String packageName = resumedActivity.info.packageName;
        if (isDebug) {
            Slog.d(TAG, "onAfterActivityResumed, pid=" + pid
                    + ", activityName=" + activityName
                    + ", packageName=" + packageName);
        }
        if (mPowerHalManagerImpl != null) {
            mPowerHalManagerImpl.amsBoostNotify(pid, activityName, packageName);
        }
        /// M: Duraspeed @{
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            mDuraSpeedService.onAfterActivityResumed(packageName);
        }
        /// @}
    }

    @Override
    public void onWindowsVisible() {
        if (isDebug) {
            Slog.d(TAG, "onWindowsVisible");
        }
    }

    @Override
    public void onStartProcess(String hostingType, String packageName) {
        if (isDebug) {
            Slog.d(TAG, "onStartProcess, hostingType=" + hostingType
                    + ", packageName=" + packageName);
        }

        if (mPowerHalManagerImpl != null) {
            mPowerHalManagerImpl.amsBoostProcessCreate(hostingType, packageName);
        }
    }

    @Override
    public void onAfterSetFocusedTask(ActivityRecord focuseActivity) {
        if (focuseActivity.app == null) {
            return;
        }

        int pid = focuseActivity.app.pid;
        String activityName = focuseActivity.info.name;
        String packageName = focuseActivity.info.packageName;
        if (isDebug) {
            Slog.d(TAG, "onAfterSetFocusedTask, pid=" + pid
                    + ", activityName=" + activityName
                    + ", packageName=" + packageName);
        }

    }

    @Override
    public void onEndOfActivityIdle(Context context, Intent idleIntent) {
        if (isDebug) {
            Slog.d(TAG, "onEndOfActivityIdle, idleIntent=" + idleIntent);
        }
        /// M: STK IDLE SCREEN feature {
        if (mIdleScreen != null) {
            mIdleScreen.onEndOfActivityIdle(context, idleIntent);
        }
        /// M: STK IDLE SCREEN feature }

        if (mPowerHalManagerImpl != null) {
            mPowerHalManagerImpl.amsBoostStop();
        }
    }

    @Override
    public void enableAmsLog(ArrayList<ProcessRecord> lruProcesses) {
        String activitylog = SystemProperties.get(amsLogProp, null);
        if (activitylog != null && !activitylog.equals("")) {
            if (activitylog.indexOf(" ") != -1
                    && activitylog.indexOf(" ") + 1 <= activitylog.length()) {
                String[] args = new String[2];
                args[0] = activitylog.substring(0, activitylog.indexOf(" "));
                args[1] = activitylog.substring(activitylog.indexOf(" ") + 1, activitylog.length());
                enableAmsLog(null, args, 0, lruProcesses);
            } else {
                SystemProperties.set(amsLogProp, "");
            }
        }
    }

    @Override
    public void enableAmsLog(PrintWriter pw, String[] args,
            int opti, ArrayList<ProcessRecord> lruProcesses) {
        String option = null;
        boolean isEnable = false;
        int indexLast = opti + 1;

        if (indexLast >= args.length) {
            if (pw != null) {
                pw.println("  Invalid argument!");
            }
            SystemProperties.set(amsLogProp, "");
        } else {
            option = args[opti];
            isEnable = "on".equals(args[indexLast]) ? true : false;
            SystemProperties.set(amsLogProp, args[opti] + " " + args[indexLast]);

            if (option.equals("x")) {
                enableAmsLog(isEnable, lruProcesses);
            } else {
                if (pw != null) {
                    pw.println("  Invalid argument!");
                }
                SystemProperties.set(amsLogProp, "");
            }
        }
    }

    private void enableAmsLog(boolean isEnable, ArrayList<ProcessRecord> lruProcesses) {
        isDebug = isEnable;

        APPEND_CATEGORY_NAME = isEnable;
        DEBUG_ALL = isEnable;
        DEBUG_ALL_ACTIVITIES = isEnable;
        DEBUG_ADD_REMOVE = isEnable;
        DEBUG_ANR = isEnable;
        DEBUG_APP = isEnable;
        DEBUG_BACKGROUND_CHECK = isEnable;
        DEBUG_BACKUP = isEnable;
        DEBUG_BROADCAST = isEnable;
        DEBUG_BROADCAST_BACKGROUND = isEnable;
        DEBUG_BROADCAST_LIGHT = isEnable;
        DEBUG_CLEANUP = isEnable;
        DEBUG_CONFIGURATION = isEnable;
        DEBUG_CONTAINERS = isEnable;
        DEBUG_FOCUS = isEnable;
        DEBUG_IDLE = isEnable;
        DEBUG_IMMERSIVE = isEnable;
        DEBUG_LOCKSCREEN = isEnable;
        DEBUG_LOCKTASK = isEnable;
        DEBUG_LRU = isEnable;
        DEBUG_MU = isEnable;
        DEBUG_NETWORK = isEnable;
        //DEBUG_OOM_ADJ = isEnable;
        //DEBUG_OOM_ADJ_REASON = isEnable;
        DEBUG_PAUSE = isEnable;
        DEBUG_POWER = isEnable;
        DEBUG_POWER_QUICK = isEnable;
        DEBUG_PROCESS_OBSERVERS = isEnable;
        DEBUG_PROCESSES = isEnable;
        DEBUG_PROVIDER = isEnable;
        DEBUG_PSS = isEnable;
        DEBUG_RECENTS = isEnable;
        DEBUG_RELEASE = isEnable;
        DEBUG_RESULTS = isEnable;
        DEBUG_SAVED_STATE = isEnable;
        DEBUG_SCREENSHOTS = isEnable;
        DEBUG_SERVICE = isEnable;
        DEBUG_FOREGROUND_SERVICE = isEnable;
        DEBUG_SERVICE_EXECUTING = isEnable;
        DEBUG_STACK = isEnable;
        DEBUG_STATES = isEnable;
        DEBUG_SWITCH = isEnable;
        DEBUG_TASKS = isEnable;
        DEBUG_THUMBNAILS = isEnable;
        DEBUG_TRANSITION = isEnable;
        DEBUG_UID_OBSERVERS = isEnable;
        DEBUG_URI_PERMISSION = isEnable;
        DEBUG_USER_LEAVING = isEnable;
        DEBUG_VISIBILITY = isEnable;
        DEBUG_USAGE_STATS = isEnable;
        DEBUG_PERMISSIONS_REVIEW = isEnable;
        DEBUG_WHITELISTS = isEnable;

        for (int i = 0; i < lruProcesses.size(); i++) {
            ProcessRecord app = lruProcesses.get(i);
            if (app != null && app.thread != null) {
                try {
                    app.thread.enableActivityThreadLog(isEnable);
                } catch (Exception e) {
                     Slog.e(TAG, "Error happens when enableActivityThreadLog", e);
                }
            }
        }
    }

    /// M: Duraspeed @{
    @Override
    public void onSystemUserUnlock() {
        Slog.d(TAG, "onSystemUserUnlock");
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            mDuraSpeedService.onSystemUserUnlock();
        }
    }

    @Override
    public void onWakefulnessChanged(int wakefulness) {
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            mDuraSpeedService.onWakefulnessChanged(wakefulness);
        }
    }

    @Override
    public void addDuraSpeedService() {
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            ServiceManager.addService("duraspeed", (IBinder) mDuraSpeedService, true);
        }
    }

    @Override
    public void startDuraSpeedService(Context context) {
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            mDuraSpeedService.startDuraSpeedService(context);
        }
    }

    private IAWSStoreRecord convertStoreRecord(ProcessRecord proc, long extraVal,
                                               SparseArray<ProcessRecord> pidsSelfLocked,
                                               String topPackageName) {
        return new IAWSStoreRecord() {
            public ArrayList<IAWSProcessRecord> getRecords() {
                ArrayList<IAWSProcessRecord> procList = new ArrayList<IAWSProcessRecord>();
                synchronized (pidsSelfLocked) {
                    final int size = pidsSelfLocked.size();
                    for (int i = 0; i < size; i++) {
                        final ProcessRecord proc = pidsSelfLocked.valueAt(i);
                        IAWSProcessRecord pr = convertProcessRecord(proc);
                        procList.add(pr);
                    }
                }
                return procList;
            }

            public IAWSProcessRecord getRecord() {
                if (proc != null) {
                    IAWSProcessRecord pr = convertProcessRecord(proc);
                    return pr;
                }
                return null;
            }

            public String getTopPkgName() {
                return topPackageName;
            }

            public long getExtraVal() {
                return extraVal;
            }
        };
    }

    private IAWSProcessRecord convertProcessRecord(ProcessRecord pr) {
        if (pr == null) {
            return null;
        }

        return new IAWSProcessRecord() {
            public String getPkgName() {
                return pr.info.packageName;
            }

            public int getPkgVer() {
                return pr.info.versionCode;
            }

            public String getProcName() {
                return pr.processName;
            }

            public int getPid() {
                return pr.pid;
            }

            public int getAdj() {
                return pr.curAdj;
            }

            public int getUid() {
                return pr.uid;
            }

            public int getprocState() {
                return pr.curProcState;
            }

            public ArrayMap<String, ProcessStats.ProcessStateHolder> getpkgList() {
                return pr.pkgList;
            }

            public boolean isKilledByAm() {
                return pr.killedByAm;
            }

            public boolean isKilled() {
                return pr.killed;
            }

            public String getWaitingToKill() {
                return pr.waitingToKill;
            }
        };
    }
    /// @}
}
