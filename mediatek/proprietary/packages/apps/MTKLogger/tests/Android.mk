ifeq ($(MTK_BASIC_PACKAGE), no)

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

# We only want this apk build for tests.
LOCAL_MODULE_TAGS := tests
LOCAL_CERTIFICATE := platform

LOCAL_JAVA_LIBRARIES := android.test.runner\
                        mediatek-framework
LOCAL_STATIC_JAVA_LIBRARIES := junit legacy-android-test libjunitreport-for-MTKLoggerTest \
                        librobotium4

# Include all test java files.
LOCAL_SRC_FILES := $(call all-java-files-under, src)

TEST_SRC_ROOT := src/com/mediatek

LOCAL_PACKAGE_NAME := MTKLoggerTests
LOCAL_MODULE_OWNER := mtk

LOCAL_INSTRUMENTATION_FOR := MTKLogger

include $(BUILD_PACKAGE)

#Add for junit report
include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := libjunitreport-for-MTKLoggerTest:libs/android-junit-report.jar
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := libjunitreport-for-MTKLoggerTest:libs/mtkatannotations.jar
include $(BUILD_MULTI_PREBUILT)

endif