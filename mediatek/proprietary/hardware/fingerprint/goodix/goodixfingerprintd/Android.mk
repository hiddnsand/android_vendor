#
# Copyright (C) 2013-2016, Shenzhen Huiding Technology Co., Ltd.
# All Rights Reserved.
#

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CFLAGS := -Wall -Wextra -Werror -Wunused -DGFD_VERSION=\"goodixfingerprintd_v1.1.1\"

LOCAL_MODULE := goodixfingerprintd
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/../public \
    $(LOCAL_PATH)/../gf_hal/public

LOCAL_SRC_FILES := \
    GoodixFingerprintDaemonProxy.cpp \
    IGoodixFingerprintDaemon.cpp \
    IGoodixFingerprintDaemonCallback.cpp \
    goodixfingerprintd.cpp \
    GoodixFingerprintDaemonCallbackProxy.cpp \
    IFingerService.cpp

LOCAL_SHARED_LIBRARIES := \
    libbinder \
    liblog \
    libhardware \
    libutils \
    libgf_hal \
    libgf_algo \
    libgf_ca

include $(MTK_EXECUTABLE)

include $(CLEAR_VARS)

LOCAL_MODULE := libgoodixfingerprintd_binder
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/../public \
    $(LOCAL_PATH)/../gf_hal/public

LOCAL_SRC_FILES := \
    IGoodixFingerprintDaemon.cpp \
    IGoodixFingerprintDaemonCallback.cpp \
    GoodixFingerprintDaemonCallbackProxy.cpp

LOCAL_SHARED_LIBRARIES := \
    libbinder \
    liblog \
    libhardware \
    libutils \
    libgf_hal \
    libgf_algo \
    libgf_ca

LOCAL_MODULE_TAGS := optional
include $(MTK_SHARED_LIBRARY)
