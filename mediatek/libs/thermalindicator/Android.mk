LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/hardware/gpu_ext))
include $(CLEAR_VARS)
LOCAL_MODULE = thermalindicator
LOCAL_MODULE_CLASS = EXECUTABLES
LOCAL_MODULE_OWNER = mtk
LOCAL_PROPRIETARY_MODULE = false
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libcutils libutils libEGL libGLESv2 libui libgui
LOCAL_INIT_RC = thermalindicator.rc
LOCAL_SRC_FILES = $(call get-prebuilt-src-arch,arm arm64)/thermalindicator
include $(BUILD_PREBUILT)
endif
