/*
 * Copyright (c) 2015-2017 MICROTRUST Incorporated
 * All rights reserved
 *
 * This file and software is confidential and proprietary to MICROTRUST Inc.
 * Unauthorized copying of this file and software is strictly prohibited.
 * You MUST NOT disclose this file and software unless you get a license
 * agreement from MICROTRUST Incorporated.
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <arch_helpers.h>
#include <platform.h>
#include <bl_common.h>
#include <runtime_svc.h>
#include <context_mgmt.h>
#include <bl31.h>
#include "teei_private.h"

#define MICROTRUST_LOG 0

/*******************************************************************************
 * This function takes an SP context pointer and:
 * 1. Applies the S-EL1 system register context from tsp_ctx->cpu_ctx.
 * 2. Saves the current C runtime state (callee saved registers) on the stack
 *    frame and saves a reference to this state.
 * 3. Calls el3_exit() so that the EL3 system and general purpose registers
 *    from the tsp_ctx->cpu_ctx are used to enter the secure payload image.
 ******************************************************************************/
uint64_t teei_synchronous_sp_entry(teei_context *teei_ctx)
{
	uint64_t rc;

	assert(teei_ctx->c_rt_ctx == 0);

	/* Apply the Secure EL1 system register context and switch to it */
	assert(cm_get_context_by_mpidr(read_mpidr(), SECURE) == &teei_ctx->cpu_ctx);
	cm_el1_sysregs_context_restore(SECURE);
	cm_set_next_eret_context(SECURE);

	rc = teei_enter_sp(&teei_ctx->c_rt_ctx);

#if DEBUG
	teei_ctx->c_rt_ctx = 0;
#endif

	return rc;
}

/*******************************************************************************
 * This function takes an SP context pointer and:
 * 1. Saves the S-EL1 system register context tp tsp_ctx->cpu_ctx.
 * 2. Restores the current C runtime state (callee saved registers) from the
 *    stack frame using the reference to this state saved in tspd_enter_sp().
 * 3. It does not need to save any general purpose or EL3 system register state
 *    as the generic smc entry routine should have saved those.
 ******************************************************************************/
void teei_synchronous_sp_exit(teei_context *teei_ctx, uint64_t ret, uint32_t save_sysregs)
{
	/* Save the Secure EL1 system register context */
	assert(cm_get_context_by_mpidr(read_mpidr(), SECURE) == &teei_ctx->cpu_ctx);

	if (save_sysregs)
		cm_el1_sysregs_context_save(SECURE);

	assert(teei_ctx->c_rt_ctx != 0);
	teei_exit_sp(teei_ctx->c_rt_ctx, ret);

	/* Should never reach here */
	assert(0);
}

/************************************************************************
 * This function translates SMC call id to string for debugging purpose
 ************************************************************************/
#define TZ_EMPTY_PARENTHESES(z) z
#define TZ_RET(x) TZ_EMPTY_PARENTHESES(ret##urn x)
/*#define SMCCASE(x) case x:  TZ_RET(#x)*/
#define SMCCASE(x) TZ_EMPTY_PARENTHESES(case x: TZ_RET(#x))

/*#define SMCDEFT(x) do {default: sprintf(smc_buf, "0x%x", x); return  smc_buf; } while (0)*/
/*#define SMCCASE(x) case x:{a = 1} return  #x*/
static char smc_buf[64];
const char *teei_smc_call_id_to_string(uint32_t id)
{
#if MICROTRUST_LOG
	switch (id) {
		SMCCASE(T_BOOT_NT_OS);
		SMCCASE(T_ACK_N_OS_READY);
		SMCCASE(T_ACK_N_FAST_CALL);
		SMCCASE(T_DUMP_STATE);
		SMCCASE(T_ACK_N_INIT_FC_BUF);
		SMCCASE(T_GET_BOOT_PARAMS);
		SMCCASE(T_GET_ANDROID_VERSION);
		SMCCASE(T_WDT_FIQ_DUMP);
		SMCCASE(T_VUART_LOG_CALL);
		SMCCASE(T_SET_TUI_EINT);
		SMCCASE(T_CLR_TUI_EINT);
		SMCCASE(T_SCHED_NT);
		SMCCASE(T_ACK_N_SYS_CTL);
		SMCCASE(T_ACK_N_NQ);
		SMCCASE(T_ACK_N_INVOKE_DRV);
		SMCCASE(T_INVOKE_N_DRV);
		SMCCASE(T_ACK_N_BOOT_OK);
		SMCCASE(T_INVOKE_N_LOAD_IMG);
		SMCCASE(T_ACK_N_KERNEL_OK);
		SMCCASE(T_SCHED_NT_IRQ);
		SMCCASE(T_NOTIFY_N_ERR);
		SMCCASE(T_SCHED_NT_LOG);
		SMCCASE(N_SWITCH_TO_T_OS_STAGE2);
		SMCCASE(N_GET_PARAM_IN);
		SMCCASE(N_INIT_T_FC_BUF);
		SMCCASE(N_INVOKE_T_FAST_CALL);
		SMCCASE(N_INIT_T_BOOT_STAGE1);
		SMCCASE(N_SWITCH_CORE);
		SMCCASE(N_GET_NON_IRQ_NUM);
		SMCCASE(NT_SCHED_T);
		SMCCASE(N_INVOKE_T_SYS_CTL);
		SMCCASE(N_INVOKE_T_NQ);
		SMCCASE(N_INVOKE_T_DRV);
		SMCCASE(N_ACK_T_INVOKE_DRV);
		SMCCASE(N_INVOKE_T_LOAD_TEE);
		SMCCASE(N_ACK_T_LOAD_IMG);
		SMCCASE(NT_CANCEL_T_TUI);
		break;
	default:
		sprintf(smc_buf, "0x%x", id);
		return  smc_buf;
	}
#endif
	return smc_buf;
}
