package com.mediatek.engineermode.videolog;


import android.os.SystemProperties;

import com.mediatek.engineermode.Elog;

/**
 * Class for all video log items.
 *
 */
public abstract class VideoLogItem {
    protected static final String LOG_PROP_1 = "test";
    protected static final String TAG = "EM/VideoLog";
    protected static final String LOG_ENABLE = "1";
    protected static final String LOG_DISABLE = "0";
    private static final String LOG_MUCH_PROP = "persist.logmuch.detect";
    private static final String LOG_MUCH_ENABLE = "true";
    private static final String LOG_MUCH_DISABLE = "false";
    protected boolean mCurStatus;

    boolean switchStatus() {
        Boolean result = switchLog(!mCurStatus);
        if (result) {
            mCurStatus = !mCurStatus;
        }
        return result;
    }

    boolean getStatus() {
        String strProp = getStatusProp();
        Elog.d(TAG, "getStatus: " + strProp);
        mCurStatus = LOG_ENABLE.equals(SystemProperties.get(strProp));
        Elog.d(TAG, "queryCurrent: " + mCurStatus);
        return mCurStatus;
    }

    static boolean switchLogMuchDetect(boolean on) {
        Elog.d(TAG, "switchLogMuchDetect " + on);

        String logMuchProTar = on ? LOG_MUCH_ENABLE : LOG_MUCH_DISABLE;
        SystemProperties.set(LOG_MUCH_PROP, logMuchProTar);

        return logMuchProTar.equals(SystemProperties.get(LOG_MUCH_PROP));
    }

    protected abstract String getStatusProp();
    protected abstract boolean switchLog(boolean enable);

}

/**
 * ACodec Log Item.
 *
 */
class ACodec extends VideoLogItem {

    protected static final String LOG_PROP_1 = "acodec.video.buflogging";
    private static final String LOG_PROP_2 = "sf.postbuffer.prof";

    @Override
    protected String getStatusProp() {
        // TODO Auto-generated method stub
        return LOG_PROP_1;
    }

    @Override
    protected boolean switchLog(boolean enable) {
        String proTar = enable ? LOG_ENABLE : LOG_DISABLE;
        SystemProperties.set(LOG_PROP_1, proTar);
        SystemProperties.set(LOG_PROP_2, proTar);
        return (proTar.equals(SystemProperties.get(LOG_PROP_1))
                && proTar.equals(SystemProperties.get(LOG_PROP_2)));
    }

}

/**
 * OMX Venc Log Item.
 *
 */
class OmxVenc extends VideoLogItem {

    protected static final String LOG_PROP_1 = "mtk.omx.enable.venc.log";

    @Override
    protected String getStatusProp() {
        // TODO Auto-generated method stub
        return LOG_PROP_1;
    }

    @Override
    protected boolean switchLog(boolean enable) {
        String proTar = enable ? LOG_ENABLE : LOG_DISABLE;
        SystemProperties.set(LOG_PROP_1, proTar);
        return proTar.equals(SystemProperties.get(LOG_PROP_1));
    }
}

/**
 * OMX Vdec Log Item.
 *
 */
class OmxVdec extends VideoLogItem {

    protected static final String LOG_PROP_1 = "mtk.omx.enable.mvamgr.log";
    private static final String LOG_PROP_2 = "mtk.omx.vdec.log";
    private static final String LOG_PROP_3 = "mtk.omx.vdec.perf.log";


    @Override
    protected String getStatusProp() {
        // TODO Auto-generated method stub
        return LOG_PROP_1;
    }

    @Override
    protected boolean switchLog(boolean enable) {
        String proTar = enable ? LOG_ENABLE : LOG_DISABLE;
        SystemProperties.set(LOG_PROP_1, proTar);
        SystemProperties.set(LOG_PROP_2, proTar);
        SystemProperties.set(LOG_PROP_3, proTar);
        return (proTar.equals(SystemProperties.get(LOG_PROP_1))
                && proTar.equals(SystemProperties.get(LOG_PROP_1))
                 && proTar.equals(SystemProperties.get(LOG_PROP_1)));
    }

}

/**
 * Vdec driver Log Item.
 *
 */
class VdecDriver extends VideoLogItem {

    private static final String LOG_PROP_1 = "mtk.vdec.log";

    @Override
    protected String getStatusProp() {
        // TODO Auto-generated method stub
        return LOG_PROP_1;
    }

    @Override
    protected boolean switchLog(boolean enable) {
        String proTar = enable ? LOG_ENABLE : LOG_DISABLE;
        SystemProperties.set(LOG_PROP_1, proTar);

        return proTar.equals(SystemProperties.get(LOG_PROP_1));
    }
}

/**
 * SVP Log Item.
 *
 */
class Svp extends VideoLogItem {

    private static final String LOG_PROP_1 = "mtk.vdectlc.log";

    @Override
    protected String getStatusProp() {
        // TODO Auto-generated method stub
        return LOG_PROP_1;
    }

    @Override
    protected boolean switchLog(boolean enable) {
        String proTar = enable ? LOG_ENABLE : LOG_DISABLE;
        SystemProperties.set(LOG_PROP_1, proTar);

        return proTar.equals(SystemProperties.get(LOG_PROP_1));
    }
}

/**
 * OMX Core Log Item.
 *
 */
class OmxCore extends VideoLogItem {

    private static final String LOG_PROP_1 = "mtk.omx.core.log";

    @Override
    protected String getStatusProp() {
        // TODO Auto-generated method stub
        return LOG_PROP_1;
    }

    @Override
    protected boolean switchLog(boolean enable) {
        String proTar = enable ? LOG_ENABLE : LOG_DISABLE;
        SystemProperties.set(LOG_PROP_1, proTar);

        return proTar.equals(SystemProperties.get(LOG_PROP_1));
    }
}

/**
 * Mjc Log Item.
 *
 */
class Mjc extends VideoLogItem {

    private static final String LOG_PROP_1 = "mtk.omxvdec.mjc.log";

    @Override
    protected String getStatusProp() {
        // TODO Auto-generated method stub
        return LOG_PROP_1;
    }

    @Override
    protected boolean switchLog(boolean enable) {
        String proTar = enable ? LOG_ENABLE : LOG_DISABLE;
        SystemProperties.set(LOG_PROP_1, proTar);

        return proTar.equals(SystemProperties.get(LOG_PROP_1));
    }
}

/**
 * H264 Venc driver Log Item.
 *
 */
class VencDriver extends VideoLogItem {

    private static final String LOG_PROP_1 = "mtk.venc.h264.showlog";

    @Override
    protected String getStatusProp() {
        // TODO Auto-generated method stub
        return LOG_PROP_1;
    }

    @Override
    protected boolean switchLog(boolean enable) {
        String proTar = enable ? LOG_ENABLE : LOG_DISABLE;
        SystemProperties.set(LOG_PROP_1, proTar);

        return proTar.equals(SystemProperties.get(LOG_PROP_1));
    }
}