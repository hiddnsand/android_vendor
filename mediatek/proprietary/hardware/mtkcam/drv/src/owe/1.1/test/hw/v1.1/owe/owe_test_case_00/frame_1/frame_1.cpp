#include "frame_1.h"
unsigned int frame_1_golden_dpe_confo_l_size = frame_1_dpe_confo_l_frame_00_01_size;
char* frame_1_golden_dpe_confo_l_frame = &frame_1_dpe_confo_l_frame_00_01[0];

unsigned int frame_1_golden_dpe_confo_r_size = frame_1_dpe_confo_r_frame_00_01_size;
char* frame_1_golden_dpe_confo_r_frame = &frame_1_dpe_confo_r_frame_00_01[0];

unsigned int frame_1_golden_dpe_dvo_l_size = frame_1_dpe_dvo_l_frame_00_01_size;
char* frame_1_golden_dpe_dvo_l_frame = &frame_1_dpe_dvo_l_frame_00_01[0];

unsigned int frame_1_golden_dpe_dvo_r_size = frame_1_dpe_dvo_r_frame_00_01_size;
char* frame_1_golden_dpe_dvo_r_frame = &frame_1_dpe_dvo_r_frame_00_01[0];

unsigned int frame_1_golden_dpe_respo_l_size = frame_1_dpe_respo_l_frame_00_01_size;
char* frame_1_golden_dpe_respo_l_frame = &frame_1_dpe_respo_l_frame_00_01[0];

unsigned int frame_1_golden_dpe_respo_r_size = frame_1_dpe_respo_r_frame_00_01_size;
char* frame_1_golden_dpe_respo_r_frame = &frame_1_dpe_respo_r_frame_00_01[0];

char* frame_1_in_dpe_wmf_dpi_frame_0 = &frame_1_dpe_wmf_dpi_frame_00_00_0[0];

char* frame_1_in_dpe_wmf_dpi_frame_1 = &frame_1_dpe_wmf_dpi_frame_00_00_1[0];

char* frame_1_in_dpe_wmf_dpi_frame_2 = &frame_1_dpe_wmf_dpi_frame_00_00_2[0];

char* frame_1_in_dpe_wmf_dpi_frame_3 = &frame_1_dpe_wmf_dpi_frame_00_00_3[0];

char* frame_1_in_dpe_wmf_dpi_frame_4 = &frame_1_dpe_wmf_dpi_frame_00_00_4[0];

unsigned int frame_1_golden_dpe_wmf_dpo_0_size = frame_1_dpe_wmf_dpo_frame_00_00_0_size;
char* frame_1_golden_dpe_wmf_dpo_frame_0 = &frame_1_dpe_wmf_dpo_frame_00_00_0[0];

unsigned int frame_1_golden_dpe_wmf_dpo_1_size = frame_1_dpe_wmf_dpo_frame_00_00_1_size;
char* frame_1_golden_dpe_wmf_dpo_frame_1 = &frame_1_dpe_wmf_dpo_frame_00_00_1[0];

unsigned int frame_1_golden_dpe_wmf_dpo_2_size = frame_1_dpe_wmf_dpo_frame_00_00_2_size;
char* frame_1_golden_dpe_wmf_dpo_frame_2 = &frame_1_dpe_wmf_dpo_frame_00_00_2[0];

unsigned int frame_1_golden_dpe_wmf_dpo_3_size = frame_1_dpe_wmf_dpo_frame_00_00_3_size;
char* frame_1_golden_dpe_wmf_dpo_frame_3 = &frame_1_dpe_wmf_dpo_frame_00_00_3[0];

unsigned int frame_1_golden_dpe_wmf_dpo_4_size = frame_1_dpe_wmf_dpo_frame_00_00_4_size;
char* frame_1_golden_dpe_wmf_dpo_frame_4 = &frame_1_dpe_wmf_dpo_frame_00_00_4[0];

char* frame_1_in_dpe_wmf_imgi_frame_0 = &frame_1_dpe_wmf_imgi_frame_00_00_0[0];

char* frame_1_in_dpe_wmf_imgi_frame_1 = &frame_1_dpe_wmf_imgi_frame_00_00_1[0];

char* frame_1_in_dpe_wmf_imgi_frame_2 = &frame_1_dpe_wmf_imgi_frame_00_00_2[0];

char* frame_1_in_dpe_wmf_imgi_frame_3 = &frame_1_dpe_wmf_imgi_frame_00_00_3[0];

char* frame_1_in_dpe_wmf_imgi_frame_4 = &frame_1_dpe_wmf_imgi_frame_00_00_4[0];

char* frame_1_in_dpe_wmf_maski_frame_0 = &frame_1_dpe_wmf_maski_frame_00_00_0[0];

char* frame_1_in_dpe_wmf_maski_frame_1 = &frame_1_dpe_wmf_maski_frame_00_00_1[0];

char* frame_1_in_dpe_wmf_tbli_frame_0 = &frame_1_dpe_wmf_tbli_frame_00_00_0[0];

char* frame_1_in_dpe_wmf_tbli_frame_1 = &frame_1_dpe_wmf_tbli_frame_00_00_1[0];

char* frame_1_in_dpe_wmf_tbli_frame_2 = &frame_1_dpe_wmf_tbli_frame_00_00_2[0];

char* frame_1_in_dpe_wmf_tbli_frame_3 = &frame_1_dpe_wmf_tbli_frame_00_00_3[0];

char* frame_1_in_dpe_wmf_tbli_frame_4 = &frame_1_dpe_wmf_tbli_frame_00_00_4[0];


void getframe_1GoldPointer(
	unsigned long* golden_dpe_dvo_l_frame,
	unsigned long* golden_dpe_dvo_r_frame,
	unsigned long* golden_dpe_confo_l_frame,
	unsigned long* golden_dpe_confo_r_frame,
	unsigned long* golden_dpe_respo_l_frame,
	unsigned long* golden_dpe_respo_r_frame,
	unsigned long* golden_dpe_wmf_dpo_frame_0,
	unsigned long* golden_dpe_wmf_dpo_frame_1,
	unsigned long* golden_dpe_wmf_dpo_frame_2,
	unsigned long* golden_dpe_wmf_dpo_frame_3,
	unsigned long* golden_dpe_wmf_dpo_frame_4
)
{
*golden_dpe_confo_l_frame = (unsigned long)&frame_1_dpe_confo_l_frame_00_01[0];
*golden_dpe_confo_r_frame = (unsigned long)&frame_1_dpe_confo_r_frame_00_01[0];
*golden_dpe_dvo_l_frame = (unsigned long)&frame_1_dpe_dvo_l_frame_00_01[0];
*golden_dpe_dvo_r_frame = (unsigned long)&frame_1_dpe_dvo_r_frame_00_01[0];
*golden_dpe_respo_l_frame = (unsigned long)&frame_1_dpe_respo_l_frame_00_01[0];
*golden_dpe_respo_r_frame = (unsigned long)&frame_1_dpe_respo_r_frame_00_01[0];
*golden_dpe_wmf_dpo_frame_0 = (unsigned long)&frame_1_dpe_wmf_dpo_frame_00_00_0[0];
*golden_dpe_wmf_dpo_frame_1 = (unsigned long)&frame_1_dpe_wmf_dpo_frame_00_00_1[0];
*golden_dpe_wmf_dpo_frame_2 = (unsigned long)&frame_1_dpe_wmf_dpo_frame_00_00_2[0];
*golden_dpe_wmf_dpo_frame_3 = (unsigned long)&frame_1_dpe_wmf_dpo_frame_00_00_3[0];
*golden_dpe_wmf_dpo_frame_4 = (unsigned long)&frame_1_dpe_wmf_dpo_frame_00_00_4[0];
}
