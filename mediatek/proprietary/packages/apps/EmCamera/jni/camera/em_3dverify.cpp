/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*
**
** Copyright 2008, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define MTK_LOG_ENABLE 1
#include "jni.h"
#include "JNIHelp.h"
#include "android_runtime/AndroidRuntime.h"
#include <cutils/log.h>
#include <stdlib.h>
#include <string.h>
#include "em_3dverify.h"
#include "MTK3dVerify.h"

#undef LOG_NDEBUG
#undef NDEBUG

#ifdef LOG_TAG
#undef LOG_TAG
#define LOG_TAG "emCamera-JNI"
#endif
namespace android{

#define LOGD(fmt, arg...)   ALOGD(fmt, ##arg)

#ifdef TIME_PROF
#include <sys/time.h>
static void GetTime(int *sec, int *usec)
{
    timeval time;
    gettimeofday(&time, NULL);
    *sec = time.tv_sec;
    *usec = time.tv_usec;
}
#else
static void GetTime(int *, int *){};
#endif

jobject em3dverify(JNIEnv* env, jstring near0, jstring near1, jstring far0, jstring far1
        , jstring result_name, jintArray sizes, jint type)
{
    MTK3dVerify* mtk3dverify = MTK3dVerify::createInstance(DRV_3D_VERIFY_OBJ_CPU);
    MTK3dVerifyImageInfo img_info;
    MTK3dVerifyResultInfo result_info;
    MUINT32 working_buf_size;
    MUINT8* working_buf;

    //init
    jint* arr;
    arr = (env)->GetIntArrayElements(sizes,NULL);

    img_info.main_w = (MINT32) arr[0];
    img_info.main_h = (MINT32) arr[1];
    img_info.sub_w = (MINT32) arr[2];
    img_info.sub_h = (MINT32) arr[3];
    img_info.pattern_w = 120;
    LOGD("img_size main_w %d, main_h %d, sub_w %d, sub_h %d, pattern_w %d\n"
        ,img_info.main_w,img_info.main_h,img_info.sub_w,img_info.sub_h,img_info.pattern_w);

    // Load data
    if(type != 1) {
        const char *main_20_str = (env)->GetStringUTFChars(near0, 0);
        LOGD("img1 %s, \n", main_20_str);

        const char *sub_20_str = (env)->GetStringUTFChars(near1, 0);
        LOGD("img2 %s, \n", sub_20_str);

        img_info.main_raw_near = (MUINT8*)main_20_str; //main cam image path
        img_info.sub_raw_near = (MUINT8*)sub_20_str; //sub cam image path
    }
    if (type != 0){
        const char *main_90_str = (env)->GetStringUTFChars(far0, 0);
        LOGD("img3 %s, \n", main_90_str);

        const char *sub_90_str = (env)->GetStringUTFChars(far1, 0);
        LOGD("img4 %s, \n", sub_90_str);

        img_info.main_raw_far = (MUINT8*)main_90_str;
        img_info.sub_raw_far = (MUINT8*)sub_90_str;
    }

    const char* config_string = CFG_FILE;
    img_info.Config = (MUINT8*)config_string; //module config file path @customer folder

    mtk3dverify->Init((MUINT32*)&img_info, NULL);

    //Allocate working buffer
    mtk3dverify->FeatureCtrl(MTK_3D_VERIFY_FEATURE_GET_WORKBUF_SIZE, NULL, &working_buf_size);
    working_buf = (MUINT8*) malloc (working_buf_size);
    LOGD("working_buf_size %d\n",working_buf_size);
    mtk3dverify->FeatureCtrl(MTK_3D_VERIFY_FEATURE_SET_WORKBUF_ADDR, working_buf, NULL);

    int start_sec, start_nsec;
    int end_sec, end_nsec;
    int total_time;
    GetTime(&start_sec, &start_nsec);
    //Add image info
    if(type == 0) {
        img_info.verify_type = MTK_3D_VERIFY_TYPE_NEAR;//do 1 distance verify
    } else if(type == 1) {
        img_info.verify_type = MTK_3D_VERIFY_TYPE_FAR;//do 1 distance verify
    } else {
        img_info.verify_type = MTK_3D_VERIFY_TYPE_NEAR_AND_FAR;//do 2 distance verify
    }
    LOGD("img_info.verify_type %d \n", img_info.verify_type);
    mtk3dverify->FeatureCtrl(MTK_3D_VERIFY_FEATURE_ADD_IMAGE, (MUINT32*)&img_info, NULL);

    //Main
    LOGD("mtk3dverify->Main\n");
    mtk3dverify->Main();

    GetTime(&end_sec, &end_nsec);
    total_time = (end_sec - start_sec) * 1000000 + (end_nsec - start_nsec);
    LOGD("stereo verify open img time %.1f ms\n", total_time/1000.0);

    //get result_info
    //show on panel
    mtk3dverify->FeatureCtrl(MTK_3D_VERIFY_FEATURE_GET_RESULT, NULL, &result_info);
    int i;
      jclass cls = (env)->FindClass("com/mediatek/emcamera/AssemblyVerifyResult");
      LOGD("FindClass\n");
      jfieldID status = (env)->GetFieldID(cls,"status","I");
      jfieldID geometry_near_status = (env)->GetFieldID(cls,"geometryNearStatus","I");
      jfieldID geometry_far_status = (env)->GetFieldID(cls,"geometryFarStatus","I");
      jfieldID near_boundary_distance = (env)->GetFieldID(cls,"nearBoundaryDistance","[I");
      jfieldID far_boundary_distance = (env)->GetFieldID(cls,"farBoundaryDistance","[I");
      jfieldID near_boundary_rotation = (env)->GetFieldID(cls,"nearBoundaryRotation","[F");
      jfieldID far_boundary_rotation = (env)->GetFieldID(cls,"farBoundaryRotation","[F");
      LOGD("Create result obj\n");
      jmethodID consID = (env)->GetMethodID(cls, "<init>", "()V");
      jobject obj = (env)->NewObject(cls, consID);
      LOGD("SetIntField status\n");
      (env)->SetIntField(obj,status, result_info.status);
      (env)->SetIntField(obj,geometry_near_status, result_info.geometry_near_status);
      (env)->SetIntField(obj,geometry_far_status, result_info.geometry_far_status);

      LOGD("SetIntArray distance\n");
      jintArray jnear_dist = (env)->NewIntArray(4);
      jint *near_bound_dist = env->GetIntArrayElements(jnear_dist, NULL);
      LOGD("SetIntArray distance: set values\n");
      for(i=0; i<4; i++)
          near_bound_dist[i] = result_info.near_boundary_distance[i];
      env->ReleaseIntArrayElements(jnear_dist, near_bound_dist, 0);
      (env)->SetObjectField(obj,near_boundary_distance, jnear_dist);

      jintArray jfar_dist = (env)->NewIntArray(4);
      jint *far_bound_dist = env->GetIntArrayElements(jfar_dist, NULL);
      for(i=0; i<4; i++)
          far_bound_dist[i] = result_info.far_boundary_distance[i];
      env->ReleaseIntArrayElements(jfar_dist, far_bound_dist, 0);
      (env)->SetObjectField(obj,far_boundary_distance, jfar_dist);

      LOGD("SetFloatArray rotation\n");
      jfloatArray jnear_rotat = (env)->NewFloatArray(3);
      jfloat *near_bound_rotat = env->GetFloatArrayElements(jnear_rotat, NULL);
      LOGD("SetFloatArray rotation:set values\n");
      for(i=0; i<3; i++)
          near_bound_rotat[i] = result_info.near_rotation[i];
      env->ReleaseFloatArrayElements(jnear_rotat, near_bound_rotat, 0);
      (env)->SetObjectField(obj,near_boundary_rotation, jnear_rotat);

      jfloatArray jfar_rotat = (env)->NewFloatArray(3);
      jfloat *far_bound_rotat = env->GetFloatArrayElements(jfar_rotat, NULL);
      for(i=0; i<3; i++)
          far_bound_rotat[i] = result_info.far_rotation[i];
      env->ReleaseFloatArrayElements(jfar_rotat, far_bound_rotat, 0);
      (env)->SetObjectField(obj,far_boundary_rotation, jfar_rotat);

    //debug log store in phone
    const char* res_str = (env)->GetStringUTFChars(result_name, 0);
    const char* res_str2 = "_verify_result.bin";
    int len = strlen(res_str);
    int len2 = strlen(res_str2);
    char* res_name = (char*) malloc ((len + len2 + 1) * sizeof(char));
    strcpy(res_name, res_str);
    strcat(res_name, res_str2);
    (env)->ReleaseStringUTFChars(result_name, res_str);
    LOGD("Result file path %s\n", res_name);

    FILE* fp_out;
    fp_out = fopen(res_name,"wb");
    fwrite(result_info.gVerify_Items,sizeof(unsigned char),720,fp_out);
    fclose(fp_out);

    LOGD("mtk3dverify->Reset\n");
    //reset
    mtk3dverify->Reset();

    LOGD("mtk3dverify->destroyInstance\n");
    //destory instance
    mtk3dverify->destroyInstance(mtk3dverify);
    free(working_buf);

    if(type != 1) {
        (env)->ReleaseStringUTFChars(near0, (const char*)img_info.main_raw_near);
        (env)->ReleaseStringUTFChars(near1, (const char*)img_info.sub_raw_near);
    }
    if (type != 0){
        (env)->ReleaseStringUTFChars(far0, (const char*)img_info.main_raw_far);
        (env)->ReleaseStringUTFChars(far1, (const char*)img_info.sub_raw_far);
    }

    jfieldID result_bin = (env)->GetFieldID(cls,"resultBin","Ljava/lang/String;");
    (env)->SetObjectField(obj, result_bin, (env)->NewStringUTF(res_name));

    free(res_name);
    LOGD("free done\n");
    return obj;
}
}