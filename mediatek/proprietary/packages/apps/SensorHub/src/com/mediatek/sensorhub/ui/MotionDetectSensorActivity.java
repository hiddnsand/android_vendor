package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class MotionDetectSensorActivity extends BaseTriggerSensorActivity {

    public MotionDetectSensorActivity() {
        super(Utils.KEY_MOTION_DETECT_STATUS, Utils.KEY_MOTION_DETECT_NOTIFY_STATUS,
                Utils.KEY_MOTION_DETECT_AUTO_STATUS, Sensor.TYPE_MOTION_DETECT);
    }
}
