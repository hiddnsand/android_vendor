/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.mms.plugin;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mediatek.mms.ext.DefaultOpConversationListItemExt;
import com.mediatek.mms.ext.IOpConversationExt;
import com.mediatek.mms.plugin.R;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * M: Op09ConversationListItemExt.
 */
public class Op09ConversationListItemExt extends DefaultOpConversationListItemExt {
    private static final String TAG = "Mms/Op09ConversationListItemExt";
    private static final int MAX_UNREAD_MESSAGES_COUNT = 999;
    private static final String MAX_UNREAD_MESSAGES_STRING = "999+";
    private static final int MAX_READ_MESSAGES_COUNT = 9999;
    private static final String MAX_READ_MESSAGES_STRING = "9999+";
    private static final int MSG_QUERY_NORMAL_URI_TOKEN = 1;
    private static final int MSG_QUERY_MASS_URI_TOKEN = 2;

    private int mSimId = -1;
    private int[] mSimInfo = null;
    private Context mContext;
    private ImageView mSimTypeView;
    private TextView mUnreadView;
    private Handler mHandler;
    private boolean mHasDraft;
    private int mCount = 0;
    private int mUnread = 0;
    private Future<Void> mFuture;

    private static final ExecutorService mExecutorService = Executors.newFixedThreadPool(8);
    final Object object = new Object();

    public Op09ConversationListItemExt(Context context) {
        super(context);
        mContext = context;
        mHandler = new queryHandler();
        mSimInfo = new int[5];
    }

    @Override
    public boolean  bind(Context context, TextView dateView, TextView unreadView, ImageView simType,
            IOpConversationExt opConversation, boolean showDraftIcon, LinearLayout la) {
        OP09ConversationExt op09Conversation = (OP09ConversationExt) opConversation;
        int unreadCount = op09Conversation.mUnreadCount;
        mSimTypeView = simType;
        mSimTypeView.setTag(op09Conversation.mUri);
        mUnreadView = unreadView;
        mHasDraft = showDraftIcon;
        if (unreadCount > 0 && op09Conversation.mRecipSize == 1) {
            String unreadString = null;
            if (unreadCount > MAX_UNREAD_MESSAGES_COUNT) {
                unreadString = MAX_UNREAD_MESSAGES_STRING;
            } else {
                unreadString = "" + unreadCount;
            }
            unreadView.setVisibility(View.VISIBLE);
            unreadView.setText(unreadString);
        } else {
            unreadView.setVisibility(View.GONE);
        }

        if (op09Conversation.mRecipSize == 1) {
            mFuture = mExecutorService.submit(
                new queryCallable(op09Conversation.mUri,
                                  op09Conversation.mRecipSize,
                                  MSG_QUERY_NORMAL_URI_TOKEN));
        } else {
            mFuture = mExecutorService.submit(
                new queryCallable(op09Conversation.mUri,
                                  op09Conversation.mRecipSize,
                                  MSG_QUERY_MASS_URI_TOKEN));
        }

        Op09MmsUtils utilsExt = Op09MmsUtils.getInstance();
        String dateStr = utilsExt.formatDateAndTimeStampString(this, op09Conversation.mDate,
                op09Conversation.mDateSent, false,
                utilsExt.formatTimeStampStringExtend(this, op09Conversation.mDate));
        dateView.setVisibility(View.VISIBLE);
        dateView.setText(dateStr);
        return true;
    }

    @Override
    public int formatMessage(ImageView view, IOpConversationExt opConversation, int defaultCount) {
        OP09ConversationExt op09Conversation = (OP09ConversationExt) opConversation;
        int count = defaultCount;
        if (op09Conversation.mRecipSize == 1) {
            return count;
        }
        // query returned
        if (mFuture.isDone() && mCount > 0) {
            return mCount;
        }
        // query cancelled
        if (mFuture.isCancelled()) {
            return count;
        }
        // wait query return
        synchronized (object) {
            try {
                int waitTime = 600;
                object.wait(waitTime);
            } catch (InterruptedException ex) {
                Log.e(TAG, "wait has been intrrupted", ex);
            }
        }
        // query returned
        if (mFuture.isDone() && mCount > 0) {
            return mCount;
        }

        return count;
    }

    @Override
    public boolean unbind() {
        Log.d(TAG, "unbind");
        mSimTypeView = null;
        mCount = 0;
        if (mFuture != null && !mFuture.isCancelled()) {
            boolean cancelled = mFuture.cancel(true);
            Log.d(TAG, "onOpUnbind, mFuture " + mFuture + " is cancelled:" + cancelled);
        }
        return true;
    }

    @Override
    public boolean bindDefault(ImageView simType) {
        if (simType != null) {
            simType.setVisibility(View.GONE);
        }
        return true;
    }

    class queryCallable implements Callable<Void> {
        private Uri conUri;
        private int queryToken;
        private int conRecipSize;

        public queryCallable(Uri uri, int recipSize, int token) {
            conUri = uri;
            queryToken = token;
            conRecipSize = recipSize;
        }

        public Void call() throws Exception {
            Log.d(TAG, "call conUri = " + conUri + ", conRecipSize = " +conRecipSize);

            if (conUri != null) {
                String newUriStr = "content://mms-sms/conversations/simid/"
                        + Long.parseLong(conUri.getLastPathSegment());
                Cursor cursor = mContext.getContentResolver().query(Uri.parse(newUriStr),
                    null, null, null, null);
                if (cursor != null) {
                    try {
                        if (cursor.moveToFirst()) {
                            mSimId = cursor.getInt(4);
                            mSimInfo[0] = cursor.getInt(0);
                            mSimInfo[1] = cursor.getInt(1);
                            mSimInfo[2] = cursor.getInt(2);
                            mSimInfo[3] = cursor.getInt(3);
                            mSimInfo[4] = cursor.getInt(4);

                            // update unread view or count
                            int allSms = mSimInfo[0];
                            int unreadSms = mSimInfo[1];
                            int allMms = mSimInfo[2];
                            int unreadMms = mSimInfo[3];
                            mUnread = (int) Math.ceil((double) unreadSms / (double) conRecipSize)
                                    + unreadMms;
                            mCount = (int) Math.ceil((double) allSms / (double) conRecipSize)
                                    + allMms;
                            Log.d(TAG, "call mUnread =" + mUnread + ", mCount = " + mCount);
                        } else {
                            mSimId = -1;
                        }
                    } finally {
                        cursor.close();
                    }
                }
            } else {
                mSimId = -1;
            }

            // send message to update UI
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(queryToken, conUri);
                Log.d(TAG, "call query back conUri = " + conUri);
                msg.sendToTarget();
            }

            synchronized (object) {
                object.notifyAll();
            }
            return null;
        }
    }

    private class queryHandler extends Handler {
        @Override
        public void handleMessage(final Message msg) {
            Log.d(TAG, "handleMessage msg.what =" + msg.what);
            Uri conversation = (Uri)msg.obj;
            Log.d(TAG, "handleMessage conversation =" + conversation);
            if (mSimTypeView == null || !((Uri)mSimTypeView.getTag()).equals(conversation)) {
                Log.d(TAG, "handleMessage mSimTypeView changed, no need to update");
                return;
            }
            switch (msg.what) {
            case MSG_QUERY_MASS_URI_TOKEN:
                updateUnreadView(mUnreadView, mUnread);

            case MSG_QUERY_NORMAL_URI_TOKEN:
                updateSimTypeView(mContext, mSimTypeView, mSimId, mHasDraft);
                break;

            default:
                super.handleMessage(msg);
                break;
            }
        }
    }

    private void updateUnreadView(TextView unreadView, int unreadCount) {
        Log.d(TAG, "updateUnreadView unreadCount=" + unreadCount);
        if (unreadView == null) {
            return;
        }
        String unreadString = null;
        if (unreadCount > 0) {
            if (unreadCount > MAX_UNREAD_MESSAGES_COUNT) {
                unreadString = MAX_UNREAD_MESSAGES_STRING;
            } else {
                unreadString = "" + unreadCount;
            }
            unreadView.setText(unreadString);
            unreadView.setVisibility(View.VISIBLE);
        } else {
            unreadView.setVisibility(View.GONE);
        }
    }

    private void updateSimTypeView(Context context, ImageView imageView,
                int subId, boolean hasDraft) {
        Log.d(TAG, "updateSimTypeView subId=" + subId + ", hasDraft=" + hasDraft);
        if (imageView == null) {
            return;
        }
        SubscriptionInfo subInfo = SubscriptionManager.from(context).getActiveSubscriptionInfo(
            subId);
        Drawable simTypeDraw = null;
        if (subInfo != null) {
            Bitmap origenBitmap = subInfo.createIconBitmap(context);
            Bitmap bitmap = MessageUtils.resizeBitmap(this, origenBitmap);
            Log.d(TAG, "showSimType for MessageListItem");
            simTypeDraw = new BitmapDrawable(context.getResources(), bitmap);
        } else {
            simTypeDraw = getResources().getDrawable(R.drawable.sim_indicator_no_sim_mms);
        }
        if (imageView != null && (hasDraft || simTypeDraw == null)) {
            imageView.setVisibility(View.GONE);
        } else if (!hasDraft && imageView != null && simTypeDraw != null) {
            imageView.setImageDrawable(simTypeDraw);
            imageView.setVisibility(View.VISIBLE);
        }
    }

}