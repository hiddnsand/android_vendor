/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */


package com.mediatek.internal.telephony.gsm;

import android.net.Uri;
import android.os.AsyncResult;
import android.os.Message;
import android.os.Build;
import android.os.Bundle;
import android.os.Binder;
import android.os.UserHandle;
import android.provider.Settings;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Sms.Intents;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.telephony.Rlog;
import android.telephony.ServiceState;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.ImsSMSDispatcher;
import com.android.internal.telephony.SmsConstants;
import com.android.internal.telephony.uicc.IccUtils;
import com.android.internal.telephony.SmsUsageMonitor;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.SmsResponse;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.gsm.GsmInboundSmsHandler;
import com.android.internal.telephony.gsm.GsmSMSDispatcher;
import com.android.internal.telephony.gsm.SmsMessage;
import com.android.internal.telephony.gsm.SmsBroadcastConfigInfo;
import com.android.internal.telephony.AsyncEmergencyContactNotifier;
import com.android.internal.telephony.TelephonyComponentFactory;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.android.internal.annotations.VisibleForTesting;


import java.util.ArrayList;
import android.telephony.PhoneNumberUtils;
import android.telephony.SubscriptionManager;

import com.android.internal.telephony.GsmAlphabet.TextEncodingDetails;
import com.android.internal.telephony.SmsRawData;
import java.util.List;

// Mobile manager service for phone privacy lock
import com.mediatek.internal.telephony.ppl.PplSmsFilterExtension;

import com.mediatek.internal.telephony.MtkSmsCommonEventHelper;
import com.mediatek.internal.telephony.MtkSmsHeader;
import com.mediatek.internal.telephony.gsm.MtkSmsMessage;

import static android.Manifest.permission.SEND_SMS_NO_CONFIRMATION;
import static android.telephony.SmsManager.STATUS_ON_ICC_READ;
import static android.telephony.SmsManager.STATUS_ON_ICC_UNREAD;
import static android.telephony.SmsManager.STATUS_ON_ICC_SENT;
import static android.telephony.SmsManager.STATUS_ON_ICC_UNSENT;
import static android.telephony.SmsManager.RESULT_ERROR_GENERIC_FAILURE;
import static android.telephony.SmsManager.RESULT_ERROR_NULL_PDU;
import static android.telephony.SmsManager.RESULT_ERROR_FDN_CHECK_FAILURE;
import static android.telephony.SmsManager.RESULT_ERROR_NO_SERVICE;
import static android.telephony.SmsManager.RESULT_ERROR_RADIO_OFF;
import static mediatek.telephony.MtkSmsManager.RESULT_ERROR_SUCCESS;
import static mediatek.telephony.MtkSmsManager.RESULT_ERROR_SIM_MEM_FULL;
import static mediatek.telephony.MtkSmsManager.RESULT_ERROR_INVALID_ADDRESS;
import static mediatek.telephony.MtkSmsManager.EXTRA_PARAMS_VALIDITY_PERIOD;



public class MtkGsmSMSDispatcher extends GsmSMSDispatcher {
    private static final String TAG = "MtkGsmSMSDispatcher";
    private static final boolean ENG = "eng".equals(Build.TYPE);

    // MTK_OPTR_PROTECT_START
    protected static boolean isDmLock = false;
    // MTK_OPTR_PROTECT_END

    // flag of storage status
    /** For FTA test only */
    private boolean mStorageAvailable = true;

    private boolean mSuccess = true;

    // for copying text message to ICC card
    protected int messageCountNeedCopy = 0;
    protected Object mLock = new Object();

    /** Mobile manager service for phone privacy lock */
    private PplSmsFilterExtension mPplSmsFilter = null;

    protected static String PDU_SIZE = "pdu_size";
    protected static String MSG_REF_NUM = "msg_ref_num";

    /**
     * This list is used to maintain the unsent Sms Tracker
     * we have this queue list to avoid we send a lot of SEND_SMS request to RIL
     * and block other commands.
     * So we only send the next SEND_SMS request after the previously request has been completed
     */
    protected ArrayList<SmsTracker> mSTrackersQueue = new ArrayList<SmsTracker>(MO_MSG_QUEUE_LIMIT);

    private MtkSmsCommonEventHelper mEventHelper;

    /* EVENT */
    /** copy text message to the ICC card */
    static final protected int EVENT_COPY_TEXT_MESSAGE_DONE = 106;

    /** delay handling message queue to avoid the ANR */
    static final protected int EVENT_DELAY_SEND_MESSAGE_QUEUE = 107;


    public MtkGsmSMSDispatcher(Phone phone, SmsUsageMonitor usageMonitor,
            ImsSMSDispatcher imsSMSDispatcher,
            GsmInboundSmsHandler gsmInboundSmsHandler) {
        super(phone, usageMonitor, imsSMSDispatcher, gsmInboundSmsHandler);
        mEventHelper = MtkSmsCommonEventHelper.getInstance(phone);
        mUiccController.unregisterForIccChanged(this);
        Integer phoneId = new Integer(mPhone.getPhoneId());
        mUiccController.registerForIccChanged(this, EVENT_ICC_CHANGED, phoneId);

        // MTK_OPTR_PROTECT_START
        // register DM broadcast receiver
        IntentFilter dmFilter = new IntentFilter();
        dmFilter.addAction("com.mediatek.dm.LAWMO_LOCK");
        dmFilter.addAction("com.mediatek.dm.LAWMO_UNLOCK");
        mContext.registerReceiver(mDMLockReceiver, dmFilter);
        // MTK_OPTR_PROTECT_END
        Rlog.d(TAG, "MtkGsmSMSDispatcher created");
    }

    // MTK_OPTR_PROTECT_START
    private BroadcastReceiver mDMLockReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Rlog.d(TAG, "[DM-Lock receive lock/unlock intent");
            if (intent.getAction() != null)
            {
                if (intent.getAction().equals("com.mediatek.dm.LAWMO_LOCK")) {
                    Rlog.d(TAG, "[DM-Lock DM is locked now");
                    isDmLock = true;
                } else if (intent.getAction().equals("com.mediatek.dm.LAWMO_UNLOCK")) {
                    Rlog.d(TAG, "[DM-Lock DM is unlocked now");
                    isDmLock = false;
                }
            }
        }
    };
    // MTK_OPTR_PROTECT_END

    /**
     * Handles 3GPP format-specific events coming from the phone stack.
     * Other events are handled by {@link SMSDispatcher#handleMessage}.
     *
     * @param msg the message to handle
     */
    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
        case EVENT_ICC_CHANGED:
            Integer phoneId = getUiccControllerPhoneId(msg);
            if (phoneId != mPhone.getPhoneId()) {
                Rlog.d(TAG, "Wrong phone id event coming, PhoneId: " + phoneId);
                break;
            } else {
                Rlog.d(TAG, "EVENT_ICC_CHANGED, PhoneId: " + phoneId + " match exactly.");
            }
            onUpdateIccAvailability();
            break;
        // Handle the next wait to send message in the message queue
        case EVENT_DELAY_SEND_MESSAGE_QUEUE:
            Rlog.d(TAG, "EVENT_DELAY_SEND_MESSAGE_QUEUE: " + msg.obj);
            handleSendNextTracker((SmsTracker) msg.obj);
            break;

        case EVENT_COPY_TEXT_MESSAGE_DONE:
        {
            AsyncResult ar;
            ar = (AsyncResult) msg.obj;
            synchronized (mLock) {
                mSuccess = (ar.exception == null);

                if (mSuccess == true) {
                    Rlog.d(TAG, "[copyText success to copy one");
                    messageCountNeedCopy -= 1;
                } else {
                    Rlog.d(TAG, "[copyText fail to copy one");
                    messageCountNeedCopy = 0;
                }

                mLock.notifyAll();
            }
            break;
        }
        default:
            super.handleMessage(msg);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void sendData(String destAddr, String scAddr, int destPort,
            byte[] data, PendingIntent sentIntent, PendingIntent deliveryIntent) {
        if (isDmLock == true) {
            Rlog.d(TAG, "DM status: lock-on");
            return;
        }

        super.sendData(destAddr, scAddr, destPort, data, sentIntent, deliveryIntent);
    }

    /** {@inheritDoc} */
    @VisibleForTesting
    @Override
    public void sendText(String destAddr, String scAddr, String text, PendingIntent sentIntent,
            PendingIntent deliveryIntent, Uri messageUri, String callingPkg,
            boolean persistMessage) {
        if (isDmLock == true) {
            Rlog.d(TAG, "DM status: lock-on");
            return;
        }

        super.sendText(destAddr, scAddr, text, sentIntent, deliveryIntent, messageUri, callingPkg,
                persistMessage);
    }

    /** {@inheritDoc} */
    @Override
    protected SmsTracker getNewSubmitPduTracker(String destinationAddress, String scAddress,
            String message, SmsHeader smsHeader, int encoding,
            PendingIntent sentIntent, PendingIntent deliveryIntent, boolean lastPart,
            AtomicInteger unsentPartCount, AtomicBoolean anyPartFailed, Uri messageUri,
            String fullMessageText) {
        if (ENG) {
            Rlog.d(TAG, "getNewSubmitPduTracker w/o validity");
        }
        MtkSmsMessage.SubmitPdu pdu = MtkSmsMessage.getSubmitPdu(scAddress, destinationAddress,
                message, deliveryIntent != null, MtkSmsHeader.toByteArray(smsHeader),
                encoding, smsHeader.languageTable, smsHeader.languageShiftTable, -1);
        if (pdu != null) {
            HashMap map =  getSmsTrackerMap(destinationAddress, scAddress,
                    message, pdu);
            return getSmsTracker(map, sentIntent,
                    deliveryIntent, getFormat(), unsentPartCount, anyPartFailed, messageUri,
                    smsHeader, !lastPart, fullMessageText, true /*isText*/,
                    // MTK-START, it should set true by default to ignore this variable
                    true /*persistMessage*/);
                    // MTK-END
        } else {
            Rlog.e(TAG, "GsmSMSDispatcher.sendNewSubmitPdu(): getSubmitPdu() returned null");
            return null;
        }
    }

    // MTK-START, support validity for operator feature
    /** {@inheritDoc} */
    protected SmsTracker getNewSubmitPduTracker(String destinationAddress, String scAddress,
            String message, SmsHeader smsHeader, int encoding,
            PendingIntent sentIntent, PendingIntent deliveryIntent, boolean lastPart,
            AtomicInteger unsentPartCount, AtomicBoolean anyPartFailed, Uri messageUri,
            String fullMessageText, int validityPeriod) {
        if (ENG) {
            Rlog.d(TAG, "getNewSubmitPduTracker w/ validity");
        }
        MtkSmsMessage.SubmitPdu pdu = MtkSmsMessage.getSubmitPdu(scAddress, destinationAddress,
                    message, (deliveryIntent != null), MtkSmsHeader.toByteArray(smsHeader),
                    encoding, smsHeader.languageTable, smsHeader.languageShiftTable,
                    validityPeriod);
        if (pdu != null) {
            HashMap map =  getSmsTrackerMap(destinationAddress, scAddress,
                    message, pdu);
            return getSmsTracker(map, sentIntent,
                    deliveryIntent, getFormat(), unsentPartCount, anyPartFailed, messageUri,
                    smsHeader, !lastPart, fullMessageText, true /*isText*/,
                    true /*persistMessage*/);
        } else {
            Rlog.e(TAG, "GsmSMSDispatcher.getNewSubmitPduTracker(): getSubmitPdu() returned null");
            return null;
        }
    }
    // MTK-END

    /** {@inheritDoc} */
    @Override
    protected void sendSms(SmsTracker tracker) {
        HashMap<String, Object> map = tracker.getData();

        byte pdu[] = (byte[]) map.get("pdu");

        boolean isReadySend = false;
        synchronized (mSTrackersQueue) {
            if (mSTrackersQueue.isEmpty() || mSTrackersQueue.get(0) != tracker) {
                Rlog.d(TAG, "Add tracker into the list: " + tracker);
                mSTrackersQueue.add(tracker);
            }
            if (mSTrackersQueue.get(0) == tracker) {
                isReadySend = true;
            }
        }

        if (!isReadySend) {
            Rlog.d(TAG, "There is another tracker in-queue and is sending");
            return;
        }

        super.sendSms(tracker);
    }

    /** {@inheritDoc} */
    @Override
    protected void sendSmsByPstn(SmsTracker tracker) {
        int ss = mPhone.getServiceState().getState();
        // if sms over IMS is not supported on data and voice is not available...
        // MTK-START
        // For the Wifi calling, We need to support sending SMS when radio is power off
        // and wifi calling is enabled. So we need to pass the SMS sending request to the
        // modem when radio is OFF.
        if (!isIms() && ss != ServiceState.STATE_IN_SERVICE
                && !mTelephonyManager.isWifiCallingAvailable()) {
        // MTK-END
            tracker.onFailed(mContext, getNotInServiceError(ss), 0/*errorCode*/);
            // MTK-START
            Message delay = obtainMessage(EVENT_DELAY_SEND_MESSAGE_QUEUE, tracker);
            // Delay 10 milliseconds to avoid ANR in lots of wait to send messages inside
            sendMessageDelayed(delay, 10);
            // MTK-END
            return;
        }

        HashMap<String, Object> map = tracker.getData();

        byte smsc[] = (byte[]) map.get("smsc");
        byte[] pdu = (byte[]) map.get("pdu");
        Message reply = obtainMessage(EVENT_SEND_SMS_COMPLETE, tracker);

        // sms over gsm is used:
        //   if sms over IMS is not supported AND
        //   this is not a retry case after sms over IMS failed
        //     indicated by mImsRetry > 0
        if (0 == tracker.mImsRetry && !isIms()) {
            if (tracker.mRetryCount > 0) {
                // per TS 23.040 Section 9.2.3.6:  If TP-MTI SMS-SUBMIT (0x01) type
                //   TP-RD (bit 2) is 1 for retry
                //   and TP-MR is set to previously failed sms TP-MR
                if (((0x01 & pdu[0]) == 0x01)) {
                    pdu[0] |= 0x04; // TP-RD
                    pdu[1] = (byte) tracker.mMessageRef; // TP-MR
                }
            }
            if (tracker.mRetryCount == 0 && tracker.mExpectMore) {
                mCi.sendSMSExpectMore(IccUtils.bytesToHexString(smsc),
                        IccUtils.bytesToHexString(pdu), reply);
            } else {
                mCi.sendSMS(IccUtils.bytesToHexString(smsc),
                        IccUtils.bytesToHexString(pdu), reply);
            }
        } else {
            mCi.sendImsGsmSms(IccUtils.bytesToHexString(smsc),
                    IccUtils.bytesToHexString(pdu), tracker.mImsRetry,
                    tracker.mMessageRef, reply);
            // increment it here, so in case of SMS_FAIL_RETRY over IMS
            // next retry will be sent using IMS request again.
            tracker.mImsRetry++;
        }
    }

    // MTK-START
    /**
     * Get the correct phone id to identify the event sent from which slot.
     *
     * @param msg message event
     *
     * @return integer value of phone id
     *
     */
    private Integer getUiccControllerPhoneId(Message msg) {
        AsyncResult ar;
        Integer phoneId = new Integer(SubscriptionManager.INVALID_PHONE_INDEX);

        ar = (AsyncResult) msg.obj;
        if (ar != null && ar.result instanceof Integer) {
            phoneId = (Integer) ar.result;
        }
        return phoneId;
    }

    /** {@inheritDoc} */
    public void sendData(String destAddr, String scAddr, int destPort, int originalPort,
            byte[] data, PendingIntent sentIntent, PendingIntent deliveryIntent) {
        Rlog.d(TAG, "GsmSmsDispatcher.sendData: enter");
        if (isDmLock == true) {
            Rlog.d(TAG, "DM status: lock-on");
            return;
        }

        MtkSmsMessage.SubmitPdu pdu = MtkSmsMessage.getSubmitPdu(
                scAddr, destAddr, destPort, originalPort, data, (deliveryIntent != null));
        if (pdu != null) {
            HashMap map = getSmsTrackerMap(destAddr, scAddr, destPort, data, pdu);
            SmsTracker tracker = getSmsTracker(map, sentIntent, deliveryIntent, getFormat(),
                    null /*messageUri*/, false /*isExpectMore*/, null /*fullMessageText*/,
                    false /*isText*/, true /*persistMessage*/);

            String carrierPackage = getCarrierAppPackageName();
            if (carrierPackage != null) {
                Rlog.d(TAG, "Found carrier package.");
                DataSmsSender smsSender = new DataSmsSender(tracker);
                smsSender.sendSmsByCarrierApp(carrierPackage, new SmsSenderCallback(smsSender));
            } else {
                Rlog.v(TAG, "No carrier package.");
                sendRawPdu(tracker);
            }
        } else {
            Rlog.e(TAG, "GsmSMSDispatcher.sendData(): getSubmitPdu() returned null");
        }
    }

    /** {@inheritDoc} */
    public void sendMultipartData(
            String destAddr, String scAddr, int destPort,
            ArrayList<SmsRawData> data, ArrayList<PendingIntent> sentIntents,
            ArrayList<PendingIntent> deliveryIntents) {
        // DM operator
        if (isDmLock == true) {
            Rlog.d(TAG, "DM status: lock-on");
            return;
        }

        if (data == null) {
            Rlog.e(TAG, "Cannot send multipart data when data is null!");
            return;
        }

        int refNumber = getNextConcatenatedRef() & 0x00FF;
        int msgCount = data.size();

        SmsTracker[] trackers = new SmsTracker[msgCount];

        for (int i = 0; i < msgCount; i++) {
            byte[] smsHeader = MtkSmsHeader.getSubmitPduHeader(
                    destPort, refNumber, i + 1, msgCount);   // 1-based sequence

            PendingIntent sentIntent = null;
            if (sentIntents != null && sentIntents.size() > i) {
                sentIntent = sentIntents.get(i);
            }

            PendingIntent deliveryIntent = null;
            if (deliveryIntents != null && deliveryIntents.size() > i) {
                deliveryIntent = deliveryIntents.get(i);
            }

            SmsMessage.SubmitPdu pdus = MtkSmsMessage.getSubmitPdu(scAddr, destAddr,
                    data.get(i).getBytes() , smsHeader, deliveryIntent != null);

            HashMap map =  getSmsTrackerMap(destAddr, scAddr, destPort, data.get(i).getBytes(),
                    pdus);

            // FIXME: should use getNewSubmitPduTracker?
            trackers[i] =
                getSmsTracker(map, sentIntent, deliveryIntent, getFormat(), null/*messageUri*/,
                        false /*isExpectMore*/, null /*fullMessageText*/, false /*isText*/,
                        true /*persistMessage*/);
        }

        if (trackers.length == 0 || trackers[0] == null) {
            Rlog.e(TAG, "Cannot send multipart data. parts=" + data + " trackers length = "
                    + trackers.length);
            return;
        }

        String carrierPackage = getCarrierAppPackageName();
        if (carrierPackage != null) {
            Rlog.d(TAG, "Found carrier package.");
            // FIXME: should extends to multipart data
            DataSmsSender smsSender = new DataSmsSender(trackers[0]);
            smsSender.sendSmsByCarrierApp(carrierPackage, new SmsSenderCallback(smsSender));
        } else {
            Rlog.v(TAG, "No carrier package.");
            for (SmsTracker tracker : trackers) {
                if (tracker != null) {
                    sendRawPdu(tracker);
                } else {
                    Rlog.e(TAG, "Null tracker.");
                }
            }
        }
    }

    public int copyTextMessageToIccCard(String scAddress, String address, List<String> text,
            int status, long timestamp) {
        Rlog.d(TAG, "GsmSMSDispatcher: copy text message to icc card");

        if (checkPhoneNumber(scAddress) == false) {
            Rlog.d(TAG, "[copyText invalid sc address");
            scAddress = null;
        }

        if (checkPhoneNumber(address) == false) {
            Rlog.d(TAG, "[copyText invalid dest address");
            return RESULT_ERROR_INVALID_ADDRESS;
        }

        mSuccess = true;

        boolean isDeliverPdu = true;

        int msgCount = text.size();
        // we should check the available storage of SIM here,
        // but now we suppose it always be true
        if (true) {
            Rlog.d(TAG, "[copyText storage available");
        } else {
            Rlog.d(TAG, "[copyText storage unavailable");
            return RESULT_ERROR_SIM_MEM_FULL;
        }

        if (status == STATUS_ON_ICC_READ || status == STATUS_ON_ICC_UNREAD) {
            Rlog.d(TAG, "[copyText to encode deliver pdu");
            isDeliverPdu = true;
        } else if (status == STATUS_ON_ICC_SENT || status == STATUS_ON_ICC_UNSENT) {
            isDeliverPdu = false;
            Rlog.d(TAG, "[copyText to encode submit pdu");
        } else {
            Rlog.d(TAG, "[copyText invalid status, default is deliver pdu");
            // isDeliverPdu = true;
            return RESULT_ERROR_GENERIC_FAILURE;
        }

        Rlog.d(TAG, "[copyText msgCount " + msgCount);
        if (msgCount > 1) {
            Rlog.d(TAG, "[copyText multi-part message");
        } else if (msgCount == 1) {
            Rlog.d(TAG, "[copyText single-part message");
        } else {
            Rlog.d(TAG, "[copyText invalid message count");
            return RESULT_ERROR_GENERIC_FAILURE;
        }

        int refNumber = getNextConcatenatedRef() & 0x00FF;
        int encoding = SmsConstants.ENCODING_UNKNOWN;
        TextEncodingDetails details[] = new TextEncodingDetails[msgCount];
        for (int i = 0; i < msgCount; i++) {
            details[i] = SmsMessage.calculateLength(text.get(i), false);
            if (encoding != details[i].codeUnitSize &&
                (encoding == SmsConstants.ENCODING_UNKNOWN ||
                 encoding == SmsConstants.ENCODING_7BIT)) {
                encoding = details[i].codeUnitSize;
            }
        }

        for (int i = 0; i < msgCount; ++i) {
            if (mSuccess == false) {
                Rlog.d(TAG, "[copyText Exception happened when copy message");
                return RESULT_ERROR_GENERIC_FAILURE;
            }
            int singleShiftId = -1;
            int lockingShiftId = -1;
            int language = details[i].shiftLangId;
            int encoding_method = encoding;

            if (encoding == SmsConstants.ENCODING_7BIT) {
                Rlog.d(TAG, "Detail: " + i + " ted" + details[i]);
                if (details[i].useLockingShift && details[i].useSingleShift) {
                    singleShiftId = language;
                    lockingShiftId = language;
                    encoding_method = MtkSmsMessage.ENCODING_7BIT_LOCKING_SINGLE;
                } else if (details[i].useLockingShift) {
                    lockingShiftId = language;
                    encoding_method = MtkSmsMessage.ENCODING_7BIT_LOCKING;
                } else if (details[i].useSingleShift) {
                    singleShiftId = language;
                    encoding_method = MtkSmsMessage.ENCODING_7BIT_SINGLE;
                }
            }

            byte[] smsHeader = null;
            if (msgCount > 1) {
                Rlog.d(TAG, "[copyText get pdu header for multi-part message");
                // 1-based sequence
                smsHeader = MtkSmsHeader.getSubmitPduHeaderWithLang(
                        -1, refNumber, i + 1, msgCount, singleShiftId, lockingShiftId);
            }

            if (isDeliverPdu) {
                MtkSmsMessage.DeliverPdu pdu = MtkSmsMessage.getDeliverPduWithLang(
                    scAddress, address, text.get(i), smsHeader, timestamp, encoding, language);

                if (pdu != null) {
                    Rlog.d(TAG, "[copyText write deliver pdu into SIM");
                    mCi.writeSmsToSim(status, IccUtils.bytesToHexString(pdu.encodedScAddress),
                            IccUtils.bytesToHexString(pdu.encodedMessage),
                            obtainMessage(EVENT_COPY_TEXT_MESSAGE_DONE));

                    synchronized (mLock) {
                        try {
                            Rlog.d(TAG, "[copyText wait until the message be wrote in SIM");
                            mLock.wait();
                        } catch (InterruptedException e) {
                            Rlog.d(TAG, "Fail to copy text message into SIM");
                            return RESULT_ERROR_GENERIC_FAILURE;
                        }
                    }
                }
            } else {
                SmsMessage.SubmitPdu pdu = MtkSmsMessage.getSubmitPduWithLang(scAddress, address,
                          text.get(i), false, smsHeader, encoding_method, language);

                if (pdu != null) {
                    Rlog.d(TAG, "[copyText write submit pdu into SIM");
                    mCi.writeSmsToSim(status, IccUtils.bytesToHexString(pdu.encodedScAddress),
                            IccUtils.bytesToHexString(pdu.encodedMessage),
                            obtainMessage(EVENT_COPY_TEXT_MESSAGE_DONE));

                    synchronized (mLock) {
                        try {
                            Rlog.d(TAG, "[copyText wait until the message be wrote in SIM");
                            mLock.wait();
                        } catch (InterruptedException e) {
                            Rlog.d(TAG, "fail to copy text message into SIM");
                            return RESULT_ERROR_GENERIC_FAILURE;
                        }
                    }
                }
            }

            Rlog.d(TAG, "[copyText thread is waked up");
        }

        if (mSuccess == true) {
            Rlog.d(TAG, "[copyText all messages have been copied into SIM");
            return RESULT_ERROR_SUCCESS;
        }

        Rlog.d(TAG, "[copyText copy failed");
        return RESULT_ERROR_GENERIC_FAILURE;
    }

    private boolean isValidSmsAddress(String address) {
        String encodedAddress = PhoneNumberUtils.extractNetworkPortion(address);

        return (encodedAddress == null) ||
                (encodedAddress.length() == address.length());
    }

    private boolean checkPhoneNumber(final char c) {
        return (c >= '0' && c <= '9') || (c == '*') || (c == '+')
                || (c == '#') || (c == 'N') || (c == ' ') || (c == '-');
    }

    private boolean checkPhoneNumber(final String address) {
        if (address == null) {
            return true;
        }

        Rlog.d(TAG, "checkPhoneNumber: " + address);
        for (int i = 0, n = address.length(); i < n; ++i) {
            if (checkPhoneNumber(address.charAt(i))) {
                continue;
            } else {
                return false;
            }
        }

        return true;
    }

    /** {@inheritDoc} */
    public void sendTextWithEncodingType(String destAddr, String scAddr, String text,
            int encodingType, PendingIntent sentIntent, PendingIntent deliveryIntent,
            Uri messageUri, String callingPkg, boolean persistMessage) {
        if (isDmLock == true) {
            Rlog.d(TAG, "DM status: lock-on");
            return;
        }

        int encoding = encodingType;
        TextEncodingDetails details = SmsMessage.calculateLength(text, false);
        if (encoding != details.codeUnitSize &&
                (encoding == SmsConstants.ENCODING_UNKNOWN ||
                encoding == SmsConstants.ENCODING_7BIT)) {
            Rlog.d(TAG, "[enc conflict between details[" + details.codeUnitSize
                    + "] and encoding " + encoding);
            details.codeUnitSize = encoding;
        }

        SmsMessage.SubmitPdu pdu = SmsMessage.getSubmitPdu(
                scAddr, destAddr, text, (deliveryIntent != null),
                null, encoding, details.languageTable, details.languageShiftTable);

        if (pdu != null) {
            HashMap map = getSmsTrackerMap(destAddr, scAddr, text, pdu);
            SmsTracker tracker = getSmsTracker(map, sentIntent, deliveryIntent, getFormat(),
                    messageUri, false /*isExpectMore*/, text /*fullMessageText*/, true /*isText*/,
                    persistMessage);

            String carrierPackage = getCarrierAppPackageName();
            if (carrierPackage != null) {
                Rlog.d(TAG, "Found carrier package.");
                TextSmsSender smsSender = new TextSmsSender(tracker);
                smsSender.sendSmsByCarrierApp(carrierPackage, new SmsSenderCallback(smsSender));
            } else {
                Rlog.v(TAG, "No carrier package.");
                sendRawPdu(tracker);
            }
        } else {
            Rlog.e(TAG,
                    "GsmSMSDispatcher.sendTextWithEncodingType(): getSubmitPdu() returned null");
            if (sentIntent != null) {
                try {
                    sentIntent.send(RESULT_ERROR_NULL_PDU);
                } catch (CanceledException ex) {
                    Rlog.e(TAG, "failed to send back RESULT_ERROR_NULL_PDU");
                }
            }
        }
    }

    /** {@inheritDoc} */
    public void sendMultipartTextWithEncodingType(String destAddr, String scAddr,
            ArrayList<String> parts, int encodingType, ArrayList<PendingIntent> sentIntents,
            ArrayList<PendingIntent> deliveryIntents, Uri messageUri, String callingPkg,
            boolean persistMessage) {
        // DM operator
        if (isDmLock == true) {
            Rlog.d(TAG, "DM status: lock-on");
            return;
        }

        if (parts == null) {
            Rlog.e(TAG, "Cannot send multipart text if parts is null!");
            return;
        }

        final String fullMessageText = getMultipartMessageText(parts);
        int refNumber = getNextConcatenatedRef() & 0xff;
        int msgCount = parts.size();
        int encoding = encodingType;

        TextEncodingDetails[] encodingForParts = new TextEncodingDetails[msgCount];
        for (int i = 0; i < msgCount; ++i) {
            TextEncodingDetails details = SmsMessage.calculateLength(parts.get(i), false);
            if (encoding != details.codeUnitSize &&
                (encoding == SmsConstants.ENCODING_UNKNOWN ||
                 encoding == SmsConstants.ENCODING_7BIT)) {
                Rlog.d(TAG, "[enc conflict between details[" + details.codeUnitSize
                        + "] and encoding " + encoding);
                details.codeUnitSize = encoding;
            }
            encodingForParts[i] = details;
        }

        SmsTracker[] trackers = new SmsTracker[msgCount];

        // States to track at the message level (for all parts)
        final AtomicInteger unsentPartCount = new AtomicInteger(msgCount);
        final AtomicBoolean anyPartFailed = new AtomicBoolean(false);

        for (int i = 0; i < msgCount; ++i) {
            SmsHeader.ConcatRef concatRef = new SmsHeader.ConcatRef();
            concatRef.refNumber = refNumber;
            concatRef.seqNumber = i + 1;  // 1-based sequence
            concatRef.msgCount = msgCount;
            // TODO: We currently set this to true since our messaging app will never
            // send more than 255 parts (it converts the message to MMS well before that).
            // However, we should support 3rd party messaging apps that might need 16-bit
            // references
            // Note:  It's not sufficient to just flip this bit to true; it will have
            // ripple effects (several calculations assume 8-bit ref).
            concatRef.isEightBits = true;
            MtkSmsHeader smsHeader = (MtkSmsHeader)
                    (TelephonyComponentFactory.getInstance().makeSmsHeader());
            smsHeader.concatRef = concatRef;
            if (encoding == SmsConstants.ENCODING_7BIT) {
                smsHeader.languageTable = encodingForParts[i].languageTable;
                smsHeader.languageShiftTable = encodingForParts[i].languageShiftTable;
            }

            PendingIntent sentIntent = null;
            if (sentIntents != null && sentIntents.size() > i) {
                sentIntent = sentIntents.get(i);
            }

            PendingIntent deliveryIntent = null;
            if (deliveryIntents != null && deliveryIntents.size() > i) {
                deliveryIntent = deliveryIntents.get(i);
            }

            trackers[i] =
                getNewSubmitPduTracker(destAddr, scAddr, parts.get(i), smsHeader, encoding,
                        sentIntent, deliveryIntent, (i == (msgCount - 1)),
                        unsentPartCount, anyPartFailed, messageUri, fullMessageText);
            trackers[i].mPersistMessage = persistMessage;
        }

        if (trackers.length == 0 || trackers[0] == null) {
            Rlog.e(TAG, "Cannot send multipart text. parts=" + parts + " trackers length = "
                    + trackers.length);
            return;
        }

        String carrierPackage = getCarrierAppPackageName();
        if (carrierPackage != null) {
            Rlog.d(TAG, "Found carrier package.");
            MultipartSmsSender smsSender = new MultipartSmsSender(parts, trackers);
            smsSender.sendSmsByCarrierApp(carrierPackage,
                    new MultipartSmsSenderCallback(smsSender));
        } else {
            Rlog.v(TAG, "No carrier package.");
            for (SmsTracker tracker : trackers) {
                if (tracker != null) {
                    sendSubmitPdu(tracker);
                } else {
                    Rlog.e(TAG, "Null tracker.");
                }
            }
        }
    }

    /**
     * Called when IccSmsInterfaceManager update SIM card fail due to SIM_FULL.
     */
    public void handleIccFull() {
        // broadcast SIM_FULL intent
        mGsmInboundSmsHandler.mStorageMonitor.handleIccFull();
    }

    /**
     * Called when a CB activation result is received.
     *
     * @param ar AsyncResult passed into the message handler.
     */
    public void handleQueryCbActivation(AsyncResult ar) {

        Boolean result = null;

        if (ar.exception == null) {
            ArrayList<SmsBroadcastConfigInfo> list =
                    (ArrayList<SmsBroadcastConfigInfo>) ar.result;

            if (list.size() == 0) {
                result = new Boolean(false);
            } else {
                SmsBroadcastConfigInfo cbConfig = list.get(0);
                Rlog.d(TAG, "cbConfig: " + cbConfig.toString());

                if (cbConfig.getFromCodeScheme() == -1 &&
                    cbConfig.getToCodeScheme() == -1 &&
                    cbConfig.getFromServiceId() == -1 &&
                    cbConfig.getToServiceId() == -1 &&
                    cbConfig.isSelected() == false) {

                    result = new Boolean(false);
                } else {
                    result = new Boolean(true);
                }
            }
        }

        Rlog.d(TAG, "queryCbActivation: " + result);
        AsyncResult.forMessage((Message) ar.userObj, result, ar.exception);
        ((Message) ar.userObj).sendToTarget();
    }

    /** {@inheritDoc} */
    public void sendTextWithExtraParams(String destAddr, String scAddr, String text,
            Bundle extraParams, PendingIntent sentIntent, PendingIntent deliveryIntent,
            Uri messageUri, String callingPkg, boolean persistMessage) {
        Rlog.d(TAG, "sendTextWithExtraParams");

        // DM operator
        if (isDmLock == true) {
            Rlog.d(TAG, "DM status: lock-on");
            return;
        }
        //int validityPeriod = extraParams.getInt(EXTRA_PARAMS_VALIDITY_PERIOD, -1);
        int validityPeriod = -1;
        Rlog.d(TAG, "validityPeriod is " + validityPeriod);

        SmsMessage.SubmitPdu pdu = MtkSmsMessage.getSubmitPdu(
                scAddr, destAddr, text, (deliveryIntent != null),
                null, SmsConstants.ENCODING_UNKNOWN, 0, 0, validityPeriod);

        if (pdu != null) {
            HashMap map = getSmsTrackerMap(destAddr, scAddr, text, pdu);
            SmsTracker tracker = getSmsTracker(map, sentIntent, deliveryIntent, getFormat(),
                    messageUri, false /*isExpectMore*/, text /*fullMessageText*/, true /*isText*/,
                    persistMessage);

            String carrierPackage = getCarrierAppPackageName();
            if (carrierPackage != null) {
                Rlog.d(TAG, "Found carrier package.");
                TextSmsSender smsSender = new TextSmsSender(tracker);
                smsSender.sendSmsByCarrierApp(carrierPackage, new SmsSenderCallback(smsSender));
            } else {
                Rlog.v(TAG, "No carrier package.");
                sendRawPdu(tracker);
            }
        } else {
            Rlog.e(TAG, "GsmSMSDispatcher.sendTextWithExtraParams(): getSubmitPdu() returned null");
            if (sentIntent != null) {
                try {
                    sentIntent.send(RESULT_ERROR_NULL_PDU);
                } catch (CanceledException ex) {
                    Rlog.e(TAG, "failed to send back RESULT_ERROR_NULL_PDU");
                }
            }
        }
    }

    /** {@inheritDoc} */
    public void sendMultipartTextWithExtraParams(String destAddr, String scAddr,
            ArrayList<String> parts, Bundle extraParams, ArrayList<PendingIntent> sentIntents,
            ArrayList<PendingIntent> deliveryIntents, Uri messageUri, String callingPkg,
            boolean persistMessage) {
        Rlog.d(TAG, "sendMultipartTextWithExtraParams");

        // DM operator
        if (isDmLock == true) {
            Rlog.d(TAG, "DM status: lock-on");
            return;
        }

        if (parts == null) {
            Rlog.e(TAG, "Cannot send multipart text. parts is null!");
            return;
        }

        //int validityPeriod = extraParams.getInt(EXTRA_PARAMS_VALIDITY_PERIOD, -1);
        int validityPeriod = -1;
        Rlog.d(TAG, "validityPeriod is " + validityPeriod);

        final String fullMessageText = getMultipartMessageText(parts);
        int refNumber = getNextConcatenatedRef() & 0x00FF;
        int msgCount = parts.size();
        int encoding = SmsConstants.ENCODING_UNKNOWN;

        TextEncodingDetails[] encodingForParts = new TextEncodingDetails[msgCount];
        for (int i = 0; i < msgCount; i++) {
            TextEncodingDetails details = calculateLength(parts.get(i), false);
            if (encoding != details.codeUnitSize &&
                (encoding == SmsConstants.ENCODING_UNKNOWN ||
                 encoding == SmsConstants.ENCODING_7BIT)) {
                encoding = details.codeUnitSize;
            }
            encodingForParts[i] = details;
        }

        SmsTracker[] trackers = new SmsTracker[msgCount];

        // States to track at the message level (for all parts)
        final AtomicInteger unsentPartCount = new AtomicInteger(msgCount);
        final AtomicBoolean anyPartFailed = new AtomicBoolean(false);

        for (int i = 0; i < msgCount; i++) {
            SmsHeader.ConcatRef concatRef = new SmsHeader.ConcatRef();
            concatRef.refNumber = refNumber;
            concatRef.seqNumber = i + 1;  // 1-based sequence
            concatRef.msgCount = msgCount;
            // TODO: We currently set this to true since our messaging app will never
            // send more than 255 parts (it converts the message to MMS well before that).
            // However, we should support 3rd party messaging apps that might need 16-bit
            // references
            // Note:  It's not sufficient to just flip this bit to true; it will have
            // ripple effects (several calculations assume 8-bit ref).
            concatRef.isEightBits = true;
            MtkSmsHeader smsHeader = (MtkSmsHeader)
                    (TelephonyComponentFactory.getInstance().makeSmsHeader());
            smsHeader.concatRef = concatRef;

            // Set the national language tables for 3GPP 7-bit encoding, if enabled.
            if (encoding == SmsConstants.ENCODING_7BIT) {
                smsHeader.languageTable = encodingForParts[i].languageTable;
                smsHeader.languageShiftTable = encodingForParts[i].languageShiftTable;
            }

            PendingIntent sentIntent = null;
            if (sentIntents != null && sentIntents.size() > i) {
                sentIntent = sentIntents.get(i);
            }

            PendingIntent deliveryIntent = null;
            if (deliveryIntents != null && deliveryIntents.size() > i) {
                deliveryIntent = deliveryIntents.get(i);
            }

            trackers[i] =
                getNewSubmitPduTracker(destAddr, scAddr, parts.get(i), smsHeader, encoding,
                        sentIntent, deliveryIntent, (i == (msgCount - 1)),
                        unsentPartCount, anyPartFailed, messageUri, fullMessageText,
                        validityPeriod);
            trackers[i].mPersistMessage = persistMessage;
        }

        if (trackers.length == 0 || trackers[0] == null) {
            Rlog.e(TAG, "Cannot send multipart text. parts=" + parts + " trackers length = "
                    + trackers.length);
            return;
        }

        String carrierPackage = getCarrierAppPackageName();
        if (carrierPackage != null) {
            Rlog.d(TAG, "Found carrier package.");
            MultipartSmsSender smsSender = new MultipartSmsSender(parts, trackers);
            smsSender.sendSmsByCarrierApp(carrierPackage,
                    new MultipartSmsSenderCallback(smsSender));
        } else {
            Rlog.v(TAG, "No carrier package.");
            for (SmsTracker tracker : trackers) {
                if (tracker != null) {
                    sendSubmitPdu(tracker);
                } else {
                    Rlog.e(TAG, "Null tracker.");
                }
            }
        }
    }

    /**
     * Set the memory storage status of the SMS
     * This function is used for FTA test only
     *
     * @param status false for storage full, true for storage available
     *
     */
    public void setSmsMemoryStatus(boolean status) {
        if (status != mStorageAvailable) {
            mStorageAvailable = status;
            mCi.reportSmsMemoryStatus(status, null);
        }
    }

    private void handleSendNextTracker(SmsTracker currentTracker) {
        int szPdu = 0;
        if (currentTracker != null) {
            HashMap map = currentTracker.mData;
            int smscLength = 0;
            int pduLength = 0;
            if (map != null) {
                if (map.get("smsc") != null) {
                    smscLength = ((byte[]) map.get("smsc")).length;
                }

                if (map.get("pdu") != null) {
                    pduLength = ((byte[]) map.get("pdu")).length;
                }
                szPdu = smscLength + pduLength;
            }
        } else {
            Rlog.d(TAG, "Current tracker is null");
        }

        SmsTracker nextTracker = null;
        synchronized (mSTrackersQueue) {
            // remove the first tracker and send the next one if any
            Rlog.d(TAG, "Remove Tracker");
            SmsTracker tempTracker = (!mSTrackersQueue.isEmpty()) ? mSTrackersQueue.remove(0) :
                    null;
            if (tempTracker != null && tempTracker.equals(currentTracker)) {
                if (ENG) {
                    Rlog.d(TAG, "[pdu size: " + szPdu);
                }
            }

            if (!mSTrackersQueue.isEmpty()) {
                nextTracker = mSTrackersQueue.get(0);
            }
        }

        if (nextTracker != null) {
            if (isFormatMatch(nextTracker, mPhone)) {
                sendSms(nextTracker);
            } else {
                nextTracker.onFailed(mContext, RESULT_ERROR_GENERIC_FAILURE, 0);
                if (ENG) {
                    Rlog.d(TAG, "The next tracker can't be sent because format doest not mach!");
                }
                Message delay = obtainMessage(EVENT_DELAY_SEND_MESSAGE_QUEUE, nextTracker);
                // Delay 10 milliseconds to avoid ANR in lots of wait to send messages inside
                sendMessageDelayed(delay, 10);
            }
        } else {
            Rlog.d(TAG, "mSTrackersQueue is empty");
        }
    }

    boolean isFormatMatch(SmsTracker tracker, Phone phone) {
        if (ENG) {
            Rlog.d(TAG, "isFormatMatch, isIms " + isIms() + ", ims sms format "
                    + getImsSmsFormat());
        }
        // Check IMS status first
        if (isIms() && tracker.mFormat.equals(getImsSmsFormat())) {
            return true;
        }
        // Then check CS
        if (tracker.mFormat.equals(SmsConstants.FORMAT_3GPP2) &&
            phone.getPhoneType() == PhoneConstants.PHONE_TYPE_CDMA) {
            return true;
        }
        if (tracker.mFormat.equals(SmsConstants.FORMAT_3GPP) &&
            phone.getPhoneType() == PhoneConstants.PHONE_TYPE_GSM) {
            return true;
        }
        return false;
    }

    /**
     * Called when SMS send completes. Broadcasts a sentIntent on success.
     * On failure, either sets up retries or broadcasts a sentIntent with
     * the failure in the result code.
     *
     * @param ar AsyncResult passed into the message handler.  ar.result should
     *           an SmsResponse instance if send was successful.  ar.userObj
     *           should be an SmsTracker instance.
     */
    protected void handleSendComplete(AsyncResult ar) {
        SmsTracker tracker = (SmsTracker) ar.userObj;
        PendingIntent sentIntent = tracker.mSentIntent;

        handleSendNextTracker(tracker);

        if (ar.result != null) {
            tracker.mMessageRef = ((SmsResponse)ar.result).mMessageRef;
        } else {
            Rlog.d(TAG, "SmsResponse was null");
        }

        if (ar.exception == null) {
            if (ENG) Rlog.d(TAG, "SMS send complete. Broadcasting intent: " + sentIntent);

            if (tracker.mDeliveryIntent != null) {
                // Expecting a status report.  Add it to the list.
                deliveryPendingList.add(tracker);
            }
            tracker.onSent(mContext);
        } else {
            if (ENG) Rlog.d(TAG, "SMS send failed");

            int ss = mPhone.getServiceState().getState();

            if ( tracker.mImsRetry > 0 && ss != ServiceState.STATE_IN_SERVICE) {
                // This is retry after failure over IMS but voice is not available.
                // Set retry to max allowed, so no retry is sent and
                //   cause RESULT_ERROR_GENERIC_FAILURE to be returned to app.
                tracker.mRetryCount = MAX_SEND_RETRIES;

                Rlog.d(TAG, "handleSendComplete: Skipping retry: "
                +" isIms()="+isIms()
                +" mRetryCount="+tracker.mRetryCount
                +" mImsRetry="+tracker.mImsRetry
                +" mMessageRef="+tracker.mMessageRef
                +" SS= "+mPhone.getServiceState().getState());
            }

            // if sms over IMS is not supported on data and voice is not available...
            if (!isIms() && ss != ServiceState.STATE_IN_SERVICE) {
                tracker.onFailed(mContext, getNotInServiceError(ss), 0/*errorCode*/);
            // MTK-START: No need to retry due to modem has already retry it
//          } else if ((((CommandException)(ar.exception)).getCommandError()
//                  == CommandException.Error.SMS_FAIL_RETRY) &&
//                 tracker.mRetryCount < MAX_SEND_RETRIES) {
//              // Retry after a delay if needed.
//              // TODO: According to TS 23.040, 9.2.3.6, we should resend
//              //       with the same TP-MR as the failed message, and
//              //       TP-RD set to 1.  However, we don't have a means of
//              //       knowing the MR for the failed message (EF_SMSstatus
//              //       may or may not have the MR corresponding to this
//              //       message, depending on the failure).  Also, in some
//              //       implementations this retry is handled by the baseband.
//              tracker.mRetryCount++;
//              Message retryMsg = obtainMessage(EVENT_SEND_RETRY, tracker);
//              sendMessageDelayed(retryMsg, SEND_RETRY_DELAY);
            // MTK-END: No need to retry due to modem has already retry it
            } else {
                int errorCode = 0;
                if (ar.result != null) {
                    errorCode = ((SmsResponse)ar.result).mErrorCode;
                }
                int error = RESULT_ERROR_GENERIC_FAILURE;
                if (((CommandException)(ar.exception)).getCommandError()
                        == CommandException.Error.FDN_CHECK_FAILURE) {
                    error = RESULT_ERROR_FDN_CHECK_FAILURE;
                }
                tracker.onFailed(mContext, error, errorCode);
            }
        }
    }

    public boolean isSmsReady() {
        return mEventHelper.isSmsReady();
    }

    /**
     * Send an SMS
     * @param tracker will contain:
     * -smsc the SMSC to send the message through, or NULL for the
     *  default SMSC
     * -pdu the raw PDU to send
     * -sentIntent if not NULL this <code>Intent</code> is
     *  broadcast when the message is successfully sent, or failed.
     *  The result code will be <code>Activity.RESULT_OK<code> for success,
     *  or one of these errors:
     *  <code>RESULT_ERROR_GENERIC_FAILURE</code>
     *  <code>RESULT_ERROR_RADIO_OFF</code>
     *  <code>RESULT_ERROR_NULL_PDU</code>
     *  <code>RESULT_ERROR_NO_SERVICE</code>.
     *  The per-application based SMS control checks sentIntent. If sentIntent
     *  is NULL the caller will be checked against all unknown applications,
     *  which cause smaller number of SMS to be sent in checking period.
     * -deliveryIntent if not NULL this <code>Intent</code> is
     *  broadcast when the message is delivered to the recipient.  The
     *  raw pdu of the status report is in the extended data ("pdu").
     * -param destAddr the destination phone number (for short code confirmation)
     */
    @Override
    public void sendRawPdu(SmsTracker tracker) {
        HashMap map = tracker.getData();
        byte pdu[] = (byte[]) map.get("pdu");

        if (mSmsSendDisabled) {
            Rlog.e(TAG, "Device does not support sending sms.");
            tracker.onFailed(mContext, RESULT_ERROR_NO_SERVICE, 0/*errorCode*/);
            return;
        }

        if (pdu == null) {
            Rlog.e(TAG, "Empty PDU");
            tracker.onFailed(mContext, RESULT_ERROR_NULL_PDU, 0/*errorCode*/);
            return;
        }

        // Get calling app package name via UID from Binder call
        PackageManager pm = mContext.getPackageManager();
        String[] packageNames = pm.getPackagesForUid(Binder.getCallingUid());

        if (packageNames == null || packageNames.length == 0) {
            // Refuse to send SMS if we can't get the calling package name.
            Rlog.e(TAG, "Can't get calling app package name: refusing to send SMS");
            tracker.onFailed(mContext, RESULT_ERROR_GENERIC_FAILURE, 0/*errorCode*/);
            return;
        }

        // MTK-START
        /* Because it may have multiple apks use the same uid, ex. Mms.apk and omacp.apk, we need to
         * exactly find the correct calling apk. We should use running process to check the correct
         * apk. If we could not find the process via pid, this apk may be killed. We will use the
         * default behavior, find the first package name via uid.
         */
        String packageName = mEventHelper.getPackageNameViaProcessId(mContext, packageNames);
        if (packageName != null) {
            packageNames[0] = packageName;
        }
        Rlog.d(TAG, "sendRawPdu and get the package name via process id: " + packageNames[0]);
        // MTK-END

        // Get package info via packagemanager
        PackageInfo appInfo;
        try {
            // XXX this is lossy- apps can share a UID
            appInfo = pm.getPackageInfoAsUser(
                    packageNames[0], PackageManager.GET_SIGNATURES, tracker.mUserId);
        } catch (PackageManager.NameNotFoundException e) {
            Rlog.e(TAG, "Can't get calling app package info: refusing to send SMS");
            tracker.onFailed(mContext, RESULT_ERROR_GENERIC_FAILURE, 0/*errorCode*/);
            return;
        }

        // checkDestination() returns true if the destination is not a premium short code or the
        // sending app is approved to send to short codes. Otherwise, a message is sent to our
        // handler with the SmsTracker to request user confirmation before sending.
        if (checkDestination(tracker)) {
            // check for excessive outgoing SMS usage by this app
            if (!mUsageMonitor.check(appInfo.packageName, SINGLE_PART_SMS)) {
                sendMessage(obtainMessage(EVENT_SEND_LIMIT_REACHED_CONFIRMATION, tracker));
                return;
            }

            sendSms(tracker);
        }

        if (PhoneNumberUtils.isLocalEmergencyNumber(mContext, tracker.mDestAddress)) {
            new AsyncEmergencyContactNotifier(mContext).execute();
        }
    }

    @Override
    protected SmsTracker getSmsTracker(HashMap<String, Object> data, PendingIntent sentIntent,
            PendingIntent deliveryIntent, String format, AtomicInteger unsentPartCount,
            AtomicBoolean anyPartFailed, Uri messageUri, SmsHeader smsHeader,
            boolean isExpectMore, String fullMessageText, boolean isText, boolean persistMessage) {
        // Get calling app package name via UID from Binder call
        PackageManager pm = mContext.getPackageManager();
        String[] packageNames = pm.getPackagesForUid(Binder.getCallingUid());

        final int userId = UserHandle.getCallingUserId();
        // Get package info via packagemanager
        PackageInfo appInfo = null;
        if (packageNames != null && packageNames.length > 0) {
            try {
                // XXX this is lossy- apps can share a UID
                // MTK-START
                /* Because it may have multiple apks use the same uid, ex. Mms.apk and omacp.apk,
                        * we need to exactly find the correct calling apk.
                        * We should use running process to check the correct apk.
                        * If we could not find the process via pid, this apk may be killed.
                        * We will use the default behavior, find the first package name via uid.
                        */
                String packageName = mEventHelper.getPackageNameViaProcessId(mContext,
                        packageNames);
                if (packageName != null) {
                    packageNames[0] = packageName;
                }
                Rlog.d(TAG, "SmsTrackerFactory and get the package name via process id: " +
                        packageNames[0]);
                // MTK-END
                appInfo = pm.getPackageInfoAsUser(
                        packageNames[0], PackageManager.GET_SIGNATURES, userId);
            } catch (PackageManager.NameNotFoundException e) {
                // error will be logged in sendRawPdu
            }
        }
        // Strip non-digits from destination phone number before checking for short codes
        // and before displaying the number to the user if confirmation is required.
        String destAddr = PhoneNumberUtils.extractNetworkPortion((String) data.get("destAddr"));
        SmsTracker tracker = new SmsTracker(data, sentIntent, deliveryIntent, appInfo, destAddr,
                format, unsentPartCount, anyPartFailed, messageUri, smsHeader, isExpectMore,
                fullMessageText, getSubId(), isText, persistMessage, userId);
        FilterOutByPpl(mContext, tracker);
        return tracker;
    }

    /**
     * Send the multi-part SMS based on multipart Sms tracker
     *
     * @param tracker holds the multipart Sms tracker ready to be sent
     */
    @Override
    protected void sendMultipartSms(SmsTracker tracker) {
        ArrayList<String> parts;
        ArrayList<PendingIntent> sentIntents;

        HashMap<String, Object> map = tracker.getData();

        parts = (ArrayList<String>) map.get("parts");
        sentIntents = (ArrayList<PendingIntent>) map.get("sentIntents");

        // check if in service
        int ss = mPhone.getServiceState().getState();
        // if sms over IMS is not supported on data and voice is not available...
        // MTK-START
        // For the Wifi calling, We need to support sending SMS when radio is power off
        // and wifi calling is enabled. So we need to pass the SMS sending request to the
        // modem when radio is OFF.
        if (!isIms() && ss != ServiceState.STATE_IN_SERVICE
                && !mTelephonyManager.isWifiCallingAvailable()) {
        // MTK-END
            for (int i = 0, count = parts.size(); i < count; i++) {
                PendingIntent sentIntent = null;
                if (sentIntents != null && sentIntents.size() > i) {
                    sentIntent = sentIntents.get(i);
                }
                handleNotInService(ss, sentIntent);
            }
            return;
        }

        super.sendMultipartSms(tracker);
    }

    /**
     * Filter out the MO sms by phone privacy lock.
     * For mobile manager service, the native apk needs to send a special sms to server and
     * doesn't want to show to end user. But sms frameworks will help to write to database if
     * app is not default sms application.
     * Therefore, sms framework need to filter out this kind of sms and not showing to end user.
     *
     * @param destAddr destination address
     * @param text content of message
     *
     * @return true filter out by ppl; false not filter out by ppl
     */
    private void FilterOutByPpl(Context context, SmsTracker tracker) {
        // Create the instance for phone privacy lock
        if (mPplSmsFilter == null) {
            mPplSmsFilter = new PplSmsFilterExtension(context);
        }

        boolean pplResult = false;

        if (!MtkSmsCommonEventHelper.isPrivacyLockSupport()) {
            return;
        }

        // Start to check phone privacy check if it does not need to write to database
        if (ENG) {
            Rlog.d(TAG, "[PPL] Phone privacy check start");
        }

        Bundle pplData = new Bundle();
        pplData.putString(mPplSmsFilter.KEY_MSG_CONTENT, tracker.mFullMessageText);
        pplData.putString(mPplSmsFilter.KEY_DST_ADDR, tracker.mDestAddress);
        pplData.putString(mPplSmsFilter.KEY_FORMAT, tracker.mFormat);
        pplData.putInt(mPplSmsFilter.KEY_SUB_ID, tracker.mSubId);
        pplData.putInt(mPplSmsFilter.KEY_SMS_TYPE, 1);

        pplResult = mPplSmsFilter.pplFilter(pplData);
        if (pplResult) {
            tracker.mPersistMessage = false;
        }

        if (ENG) {
            Rlog.d(TAG, "[PPL] Phone privacy check end, Need to filter(result) = "
                    + pplResult);
        }
    }
}
