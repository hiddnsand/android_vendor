/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */


package com.mediatek.entitlement.ux;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mediatek.entitlement.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Activity for emergency webview.
 */
public class E911WebsheetFragment extends Fragment {

    private static final String TAG = "[Entitlement]E911WebsheetFragment";

    private View mRootView;
    private WebView mEmergencyWebView;
    private ProgressBar mProgressBar;
    private ProgressDialog mLoadingDialog;
    E911WebSheetListener mCallback;

    //For Testing purpose only
    static String TestHtml = "<html><head><script>function count_rabbits(){WiFiCallingWebViewControllerCallback.phoneServicesAccountStatusChanged(true)}</script></head><body><h2>Press the button to start</h2><input type=\"button\" onclick=\"count_rabbits()\" value=\"Count rabbits!\"/></body></html>";

    // Container Activity must implement this interface
    public interface E911WebSheetListener {
        public boolean isServiceConnected();
        public String getUrl();
        public String getServerData();
        public void startEntitlement(boolean retry);
        public void webSheetStateChange(boolean isComplete);
    }

    public E911WebsheetFragment() {
        //empty constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.emergency_activity, container, false);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progressBar);
        mEmergencyWebView = (WebView) mRootView.findViewById(R.id.webview);
        mEmergencyWebView.addJavascriptInterface(new WiFiCallingWebViewControllerCallback(),
                "WiFiCallingWebViewController");
        mEmergencyWebView.setWebViewClient(new EntitlementWebViewClient());

        WebSettings webSettings = mEmergencyWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        boolean isConnected = mCallback.isServiceConnected();
        Log.d(TAG, "onActivityCreated, isConnected = " + isConnected);
        if (isConnected) {
            showWebView();
        } else {
            showLoadingScreen();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (E911WebSheetListener) activity;
            Log.d(TAG, "mCallback = " + mCallback);
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement E911WebSheetListener");
        }
    }

    /* NSDS 2.0 WebSheet Interface Description: Section 3.3:
        * In any case, navigation through the Web Sheet is intended to be entirely
        * programmatically controlled. Specifically, the NSDS 2.0 device should not make
        * available the back/forward navigation controls ordinarily found on Web browsers.
      */
    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    /**
     * Method to be called on Ses Service events.
     * @param event like service_connected, disconnected, ses state change
     * @return
     */
    /*public void onEvent(int event, int state) {
        switch(event) {
            case WfcSettingsActivity.EVT_ID_SERVICE_CONNECTED:
                showWebView();
                break;
            default:
                Log.d(TAG, "unknown event:" + event);
                break;
        }
    }*/

    private void showWebView() {
        //URL should be of http:// format
        String url = mCallback.getUrl();
        String postData = mCallback.getServerData();
        Log.d(TAG, "showWebView(), url = " + url + "postData = " + postData);
        dismissLoading();
        if (postData != null) {
            mEmergencyWebView.postUrl(url, postData.getBytes());
        } else {
            Toast.makeText(getActivity(), R.string.try_again, Toast.LENGTH_SHORT).show();
            mEmergencyWebView.post(new Runnable() {
                @Override
                public void run() {
                    finishWebView(false);
                }
            });
        }
        //mEmergencyWebView.loadData(TestHtml, "text/html", null);
        //String summary = "<html><body>You scored <b>192</b> points.</body></html>";
        //mEmergencyWebView.loadData(summary, "text/html", null);
    }


    /**
     * Class for customized webClient.
     */
    private class EntitlementWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d(TAG, "shouldOverrideUrlLoading, url:" + url);
           // view.loadUrl(url);
            mProgressBar.setVisibility(View.VISIBLE);
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            Log.d(TAG, "onPageFinished, url:" + url);
            mProgressBar.setVisibility(View.GONE);
            dismissLoading();
        }

        @Override
        public void onReceivedHttpError(android.webkit.WebView view,
                android.webkit.WebResourceRequest
                request, android.webkit.WebResourceResponse errorResponse) {
            int status = errorResponse.getStatusCode();
            Log.e(TAG, "HTTP error " + status);

            try {
                switch (status) {
                    case 400: // Bad Request
                        break;
                    case 401:
                        JSONObject body = new JSONObject(fromStream(errorResponse.getData()));
                        String error = body.getString("error");
                        Log.e(TAG, "error: " + error);
                        switch (error) {
                            case "invalid_authtoken":
                                // Nothing we can do...
                                break;
                            case "expired_authtoken":
                                // obtain a new server-data string from SES and try again
                                showWebView();
                                break;
                        }
                        break;
                    case 500:   // Internal Server Error
                        // TODO: should retry
                        break;
                }
            } catch (IOException | JSONException e) {
                Log.e(TAG, "failed to read body", e);
            }
        }
    }

    public static String fromStream(InputStream inputStream) throws IOException {
        int bufSize = 1028;
        byte[] buffer = new byte[bufSize];
        int inSize;
        StringBuilder stringBuilder = new StringBuilder();
        if (inputStream != null) {
            while ((inSize = inputStream.read(buffer)) > 0) {
                stringBuilder.append(new String(buffer, 0, inSize));
            }
        }
        return stringBuilder.toString();
    }
   private class WiFiCallingWebViewControllerCallback {
        @JavascriptInterface
        public void phoneServicesAccountStatusChanged(final boolean isFinished) {
            Log.d(TAG, "phoneServicesAccountStatusChanged: " + isFinished);

            mEmergencyWebView.post(new Runnable() {
                @Override
                public void run() {
                    finishWebView(isFinished);
                }
            });


            if (isFinished) {
                mCallback.startEntitlement(false);
            } else {
                mCallback.startEntitlement(true);
            }
        }
    }
    /* NSDS 2.0 WebSheet Interface Description: Section 3.3:
        * In any case, navigation through the Web Sheet is intended to be entirely
        * programmatically controlled. Specifically, the NSDS 2.0 device should not make
        * available the back/forward navigation controls ordinarily found on Web browsers.
      */
    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
   }*/
    private void finishWebView(boolean isFinished) {
        mEmergencyWebView.destroy();
        mEmergencyWebView.setVisibility(View.GONE);
        mCallback.webSheetStateChange(isFinished);
    }
    private void showLoadingScreen() {
        Context context = getActivity();
        mLoadingDialog = new ProgressDialog(context);
        mLoadingDialog.setMessage(context.getString(R.string.loading_message));
        mLoadingDialog.setCancelable(true);
        mLoadingDialog.setCanceledOnTouchOutside(false);
        mLoadingDialog.show();
    }

    private void dismissLoading() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.dismiss();
        }
    }
}
