/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "TPNodeTest"
//
#include <mtkcam/utils/std/Log.h>
//
#include "../TPNodePlugin.h"
#include "DefaultPlugin.h"

#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>

#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)

#define DUALCAM 1

#if DUALCAM
#define TP_PLUGIN_DEFAULT_ID  (MTK_PLUGIN_MODE_BOKEH_3RD_PARTY)
#else
#define TP_PLUGIN_DEFAULT_ID  (MTK_PLUGIN_MODE_HDR_3RD_PARTY)
#endif

using namespace NSCam;
using namespace NSCam::v3;

// mount the implementation of third party
REGISTER_TPNODE_PLUGIN(TP_PLUGIN_DEFAULT_ID, DefaultPlugin);

/******************************************************************************
 *
 ******************************************************************************/
DefaultPlugin::
DefaultPlugin()
{
}


/******************************************************************************
 *
 ******************************************************************************/
DefaultPlugin::
~DefaultPlugin()
{
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
DefaultPlugin::
init()
{
    MY_LOGD("default plugin:init +");
    usleep(300 * 1000);
    MY_LOGD("default plugin:init -");
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
DefaultPlugin::
uninit()
{
}

/******************************************************************************
 *
 ******************************************************************************/
TPNodePlugin::PluginProfile&
DefaultPlugin::
profile()
{
    static PluginProfile pf = {
                    .id = TP_PLUGIN_DEFAULT_ID,
                    .type = BLENDING
                };

    return pf;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
DefaultPlugin::
process(RequestParams const& params)
{
    MY_LOGD("default plugin:request[%u] +", params.uRequestNo);

    // dump param info
    for (size_t i = 0; i < params.uInCount; i++) {
        MY_LOGD("[input-%zd] AppMeta:%p HalMeta:%p FullYuv:%p RszYuv:%p",
                i,
                params.in[i].pAppMeta,
                params.in[i].pHalMeta,
                params.in[i].pFullYuv,
                params.in[i].pResizedYuv);
    }

    MY_LOGD("[output] AppMeta:%p HalMeta:%p FullYuv:%p Depth:%p",
            params.out.pAppMeta,
            params.out.pHalMeta,
            params.out.pFullYuv,
            params.out.pDepth);

    // use first frame to make a gray image
    IMetadata* pHalMeta = params.in[0].pHalMeta;
    IImageBuffer* pInputYuv = params.in[0].pFullYuv;
    IImageBuffer* pOutputYuv = params.out.pFullYuv;

    PluginId_T pluginId = 0;
    IMetadata::getEntry<MINT32>(pHalMeta, MTK_PLUGIN_MODE, pluginId);

    MY_LOGD("process request with plugin(%d)", pluginId);

    for (size_t i = 0; i < pInputYuv->getPlaneCount(); i++) {
        void *pInVa = (void *) (pInputYuv->getBufVA(i));
        void *pOutVa = (void *) (pOutputYuv->getBufVA(i));
        MUINT32 uBufSize = pInputYuv->getBufSizeInBytes(i);

        if (i == 0)
            memcpy(pOutVa, pInVa, uBufSize);
        else
            memset(pOutVa, 128, uBufSize);
    }

    pOutputYuv->syncCache(eCACHECTRL_FLUSH);

#if DUALCAM
    IImageBuffer* pOutputDepth = params.out.pDepth;
    void *pDepthVa = (void *) (pOutputDepth->getBufVA(0));
    MUINT32 uDepthSize = pOutputDepth->getBufSizeInBytes(0);
    MY_LOGD("process depth va(%p) size(%u)", pDepthVa, uDepthSize);
    memset(pDepthVa, 100, uDepthSize);

    pOutputDepth->syncCache(eCACHECTRL_FLUSH);
#endif

    response(params, OK);

    MY_LOGD("default plugin:request[%u] -", params.uRequestNo);
}

