/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/P1NodeImp"
//
#include "P1NodeCommon.h"
#include "P1NodeUtility.h"
#include "P1NodeConnectLMV.h"
#include <mtkcam/feature/eis/EisInfo.h>
//
using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::EIS;
using namespace NSCam::Utils::Sync;
using namespace NSCam::NSIoPipe;
using namespace NSCam::NSIoPipe::NSCamIOPipe;
using namespace NS3Av3;
//
/******************************************************************************
 *
 ******************************************************************************/
#define SUPPORT_3A              (1)
#define SUPPORT_ISP             (1)
#define SUPPORT_PERFRAME_CTRL   (0)

#if MTK_CAM_VHDR_SUPPORT
#define SUPPORT_VHDR            (1)
#else
#define SUPPORT_VHDR            (0)
#endif
#define SUPPORT_LCS             (1)
#define SUPPORT_RSS             (1)

//#define SUPPORT_SCALING_CROP      (1)
//#define SUPPORT_SCALING_CROP_IMGO (SUPPORT_SCALING_CROP && (0))
//#define SUPPORT_SCALING_CROP_RRZO (SUPPORT_SCALING_CROP && (1))

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/******************************************************************************
 *
 ******************************************************************************/
static INormalPipeModule* getNormalPipeModule()
{
    static auto pModule = INormalPipeModule::get();
    MY_LOGE_IF(!pModule, "INormalPipeModule::get() fail");
    return pModule;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL calculateCropInfoFull(
    MUINT32 pixelMode,
    MSize const& sensorSize,
    MSize const& bufferSize,
    MRect const& querySrcRect,
    MRect& resultSrcRect,
    MSize& resultDstSize,
    MINT32 mLogLevel = 0
)
{
    if ((querySrcRect.size().w == sensorSize.w) &&
        (querySrcRect.size().h == sensorSize.h)) {
        return false;
    }
    if ((querySrcRect.size().w > bufferSize.w || // cannot over buffer size
        querySrcRect.size().h > bufferSize.h) ||
        (((querySrcRect.leftTop().x + querySrcRect.size().w) > sensorSize.w) ||
        ((querySrcRect.leftTop().y + querySrcRect.size().h) > sensorSize.h))
        ) {
        MY_LOGD_IF((2 <= mLogLevel), "calculateCropInfoFull input invalid "
            "pixelMode(%d) sensorSize(%dx%d) bufferSize(%dx%d) "
            "querySrcRect_size(%dx%d) querySrcRect_start(%d,%d)", pixelMode,
            sensorSize.w, sensorSize.h, bufferSize.w, bufferSize.h,
            querySrcRect.size().w, querySrcRect.size().h,
            querySrcRect.leftTop().x, querySrcRect.leftTop().y);
        return false;
    }
    // TODO: query the valid value, currently do not crop in IMGO
    resultDstSize = MSize(sensorSize.w, sensorSize.h);
    resultSrcRect = MRect(MPoint(0, 0), resultDstSize);

    return true;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL calculateCropInfoResizer(
    MUINT32 pixelMode,
    MUINT32 imageFormat,
    MSize const& sensorSize,
    MSize const& bufferSize,
    MRect const& querySrcRect,
    MRect& resultSrcRect,
    MSize& resultDstSize,
    MINT32 mLogLevel = 0
)
{
    if ((querySrcRect.size().w == sensorSize.w) &&
        (querySrcRect.size().h == sensorSize.h)) {
        return false;
    }
    if ((((querySrcRect.leftTop().x + querySrcRect.size().w) > sensorSize.w) ||
        ((querySrcRect.leftTop().y + querySrcRect.size().h) > sensorSize.h))
        ) {
        MY_LOGD_IF((2 <= mLogLevel), "calculateCropInfoResizer input invalid "
            "pixelMode(%d) sensorSize(%dx%d) bufferSize(%dx%d) "
            "querySrcRect_size(%dx%d) querySrcRect_start(%d,%d)", pixelMode,
            sensorSize.w, sensorSize.h, bufferSize.w, bufferSize.h,
            querySrcRect.size().w, querySrcRect.size().h,
            querySrcRect.leftTop().x, querySrcRect.leftTop().y);
        return false;
    }
    //
    MPoint::value_type src_crop_x = querySrcRect.leftTop().x;
    MPoint::value_type src_crop_y = querySrcRect.leftTop().y;
    MSize::value_type src_crop_w = querySrcRect.size().w;
    MSize::value_type src_crop_h = querySrcRect.size().h;
    MSize::value_type dst_size_w = 0;
    MSize::value_type dst_size_h = 0;
    if (querySrcRect.size().w < bufferSize.w) {
        dst_size_w = querySrcRect.size().w;
        // check start.x
        if  ( auto pModule = getNormalPipeModule() )
        {
            NSCam::NSIoPipe::NSCamIOPipe::NormalPipe_QueryInfo info;
            pModule->query(
                NSCam::NSIoPipe::PORT_RRZO.index,
                NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_CROP_START_X,
                (EImageFormat)imageFormat,
                src_crop_x, info);
            src_crop_x = info.crop_x;
        }
        // check size.w
        if  ( auto pModule = getNormalPipeModule() )
        {
            NSCam::NSIoPipe::NSCamIOPipe::NormalPipe_QueryInfo info;
            pModule->query(
                NSCam::NSIoPipe::PORT_RRZO.index,
                NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_X_PIX|
                NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_STRIDE_BYTE,
                (EImageFormat)imageFormat,
                dst_size_w, info);
            dst_size_w = info.x_pix;
        }
        //
        dst_size_w = MIN(dst_size_w, sensorSize.w);
        src_crop_w = dst_size_w;
        if (src_crop_w > querySrcRect.size().w) {
            if ((src_crop_x + src_crop_w) > sensorSize.w) {
                src_crop_x = sensorSize.w - src_crop_w;
            }
        }
    } else {
        if ( auto pModule = getNormalPipeModule() ) {
            MUINT32 ratio = (RESIZE_RATIO_MAX_10X * 10);
            #if 1 // query width ratio from DRV
            NSCam::NSIoPipe::NSCamIOPipe::NormalPipe_QueryInfo info;
            pModule->query(
                NSCam::NSIoPipe::PORT_RRZO.index,
                NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_BS_RATIO,
                (EImageFormat)imageFormat, 0, info);
            ratio = info.bs_ratio;
            #endif
            if (((MUINT32)src_crop_w * ratio) > ((MUINT32)bufferSize.w * 100)) {
                MY_LOGW("calculateCropInfoResizer re-size width invalid "
                    "(%d):(%d) @(%d)", src_crop_w, bufferSize.w, ratio);
                return false;
            }
        }
        dst_size_w = bufferSize.w;
    }
    if (querySrcRect.size().h < bufferSize.h) {
        dst_size_h = querySrcRect.size().h;
        dst_size_h = MIN(ALIGN_UPPER(dst_size_h, 2), sensorSize.h);
        src_crop_h = dst_size_h;
        if (src_crop_h > querySrcRect.size().h) {
            if ((src_crop_y + src_crop_h) > sensorSize.h) {
                src_crop_y = sensorSize.h - src_crop_h;
            }
        }
    } else {
        if ( auto pModule = getNormalPipeModule() ) {
            MUINT32 ratio = (RESIZE_RATIO_MAX_10X * 10);
            #if 1 // query height ratio from DRV
            NSCam::NSIoPipe::NSCamIOPipe::NormalPipe_QueryInfo info;
            pModule->query(
                NSCam::NSIoPipe::PORT_RRZO.index,
                NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_BS_RATIO,
                (EImageFormat)imageFormat, 0, info);
            ratio = info.bs_ratio;
            #endif
            if (((MUINT32)src_crop_h * ratio) > ((MUINT32)bufferSize.h * 100)) {
                MY_LOGW("calculateCropInfoResizer re-size height invalid "
                    "(%d):(%d) @(%d)", src_crop_h, bufferSize.h, ratio);
                return false;
            }
        }
        dst_size_h = bufferSize.h;
    }
    resultDstSize = MSize(dst_size_w, dst_size_h);
    resultSrcRect = MRect(MPoint(src_crop_x, src_crop_y),
                            MSize(src_crop_w, src_crop_h));
    return true;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  .
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class P1NodeImp
    : public BaseNode
    , public P1Node
    , public IHal3ACb
    , protected Thread
{

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Implementations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////
    //
    #define STREAM_ITEM_START       (0)
    //
    enum STREAM_IMG
    {
        STREAM_IMG_IN_YUV           = STREAM_ITEM_START,
        STREAM_IMG_IN_OPAQUE,
        STREAM_IMG_OUT_OPAQUE,
        STREAM_IMG_OUT_FULL,
        STREAM_IMG_OUT_RESIZE,
        STREAM_IMG_OUT_LCS,
        STREAM_IMG_OUT_RSS,
        STREAM_IMG_NUM
    };
    #define IS_IN_STREAM_IMG(img)   (  (img == STREAM_IMG_IN_YUV)\
                                    || (img == STREAM_IMG_IN_OPAQUE)\
                                    )
    //
    enum STREAM_META
    {
        STREAM_META_IN_APP          = STREAM_ITEM_START,
        STREAM_META_IN_HAL,
        STREAM_META_OUT_APP,
        STREAM_META_OUT_HAL,
        STREAM_META_NUM
    };
    #define IS_IN_STREAM_META(meta) (  (meta == STREAM_META_IN_APP)\
                                    || (meta == STREAM_META_IN_HAL)\
                                    )
    //
    enum STREAM_BUF_LOCK
    {
        STREAM_BUF_LOCK_NONE        = 0,
        STREAM_BUF_LOCK_R,
        STREAM_BUF_LOCK_W
    };
    //
    enum IMG_BUF_SRC
    {
        IMG_BUF_SRC_NULL           = 0,
        IMG_BUF_SRC_POOL,
        IMG_BUF_SRC_STUFF,
        IMG_BUF_SRC_FRAME
    };

    struct NodeStreamMeta {
        MBOOL                           bExist;
        sp<IMetaStreamBuffer>           spStreamBuf;
        STREAM_BUF_LOCK                 eLockState;
        IMetadata*                      pMetadata;
        NodeStreamMeta()
            : bExist(MFALSE)
            , spStreamBuf(NULL)
            , eLockState(STREAM_BUF_LOCK_NONE)
            , pMetadata(NULL)
        {};
    };

    struct NodeStreamImage {
        MBOOL                           bExist;
        sp<IImageStreamBuffer>          spStreamBuf;
        STREAM_BUF_LOCK                 eLockState;
        //sp<IImageBufferHeap>            spHeap;
        sp<IImageBuffer>                spImgBuf;
        IMG_BUF_SRC                     eSrcType;
        NodeStreamImage()
            : bExist(MFALSE)
            , spStreamBuf(NULL)
            , eLockState(STREAM_BUF_LOCK_NONE)
            //, spHeap(NULL)
            , spImgBuf(NULL)
            , eSrcType(IMG_BUF_SRC_NULL)
        {};
    };

    struct NodePortOrder {
        MUINT32                         mPortOrder[];
    };

    struct QueNode_T {
        MUINT32                             magicNum;
        MUINT32                             sofIdx;
        sp<IPipelineFrame>                  appFrame;
        sp<IImageBuffer>                    buffer_eiso;
        //sp<IImageBuffer>                    buffer_lcso; // no use, need remove when lcso run imageStream
        NodeStreamImage                     streamBufImg[STREAM_IMG_NUM];
        NodeStreamMeta                      streamBufMeta[STREAM_META_NUM];
        MUINT32                             portBufIndex[P1_OUTPUT_PORT_TOTAL];
        MUINT32                             reqType;    /*REQ_TYPE*/
        MUINT32                             reqOutSet;  /*REQ_SET(REQ_OUT)*/
        MUINT32                             expRec;     /*EXP_REC(EXP_EVT)*/
        MUINT32                             exeState;   /*EXE_STATE*/
        MUINT32                             capType;    /*E_CAPTURE_TYPE*/
        MUINT32                             uniSwitchState; /*UNI_SWITCH_STATE*/
        MUINT8                              tgSwitchState; /*TG_SWITCH_STATE*/
        MUINT8                              tgSwitchNum;   /*TG_SWITCH_NUM*/
        MINT64                              frameExpDuration;
        MINT64                              frameTimeStamp;
        MBOOL                               needFlush;
        MSize                               dstSize_full;
        MSize                               dstSize_resizer;
        MRect                               cropRect_full;
        MRect                               cropRect_resizer;
        QueNode_T()
            : magicNum(0)
            , sofIdx(P1SOFIDX_INIT_VAL)
            , appFrame(NULL)
            , buffer_eiso(NULL)
            //, buffer_lcso(NULL)
            , reqType(REQ_TYPE_UNKNOWN)
            , reqOutSet(REQ_SET_NONE)
            , expRec(EXP_REC_NONE)
            , exeState(EXE_STATE_NULL)
            , capType(E_CAPTURE_NORMAL)
            , uniSwitchState(UNI_SWITCH_STATE_NONE)
            , tgSwitchState(TG_SWITCH_STATE_NONE)
            , tgSwitchNum(0)
            , frameExpDuration(0)
            , frameTimeStamp(0)
            , needFlush(MFALSE)
        {
            ::memset(portBufIndex, P1_FILL_BYTE, sizeof(portBufIndex));
        }
    };

    struct QueJob_T {
    public:
        Vector<QueNode_T>           mSet;
        MUINT8                      mMaxNum;
        MINT32                      mFirstMagicNum;
                                    QueJob_T()
                                    : mMaxNum(1)
                                    , mFirstMagicNum(0)
                                    {
                                        mSet.clear();
                                        mSet.setCapacity(mMaxNum);
                                    };
                                    QueJob_T(MUINT8 num)
                                    : mMaxNum(num)
                                    , mFirstMagicNum(0)
                                    {
                                        mSet.clear();
                                        mSet.setCapacity(mMaxNum);
                                    };
        virtual                     ~QueJob_T()
                                    {
                                        mSet.clear();
                                    };
    };

    typedef Vector<QueJob_T>        Que_T;
    //
    class DeliverMgr
        : public Thread
    {

    public:

        DeliverMgr()
            : mpP1NodeImp(NULL)
            , mLogLevel(0)
            , mLoopRunning(MFALSE)
            , mLoopState(LOOP_STATE_INIT)
            , mSentNum(0)
        {
            mNumList.clear();
            mNodeQueue.clear();
        };

        virtual ~DeliverMgr()
        {
            mNumList.clear();
            mNodeQueue.clear();
        };

        void init(sp<P1NodeImp> pP1NodeImp) {
            mpP1NodeImp = pP1NodeImp;
            if (mpP1NodeImp != NULL) {
                mLogLevel = mpP1NodeImp->mLogLevel;
                mNodeQueue.setCapacity((size_t)(mpP1NodeImp->mBurstNum >> 3));
            }
        };

        void uninit(void) {
            exit();
            if (mpP1NodeImp != NULL) {
                mpP1NodeImp = NULL;
            }
        };

        void runningSet(MBOOL bRunning) {
            Mutex::Autolock _l(mDeliverLock);
            mLoopRunning = bRunning;
            return;
        };

        MBOOL runningGet(void) {
            Mutex::Autolock _l(mDeliverLock);
            return mLoopRunning;
        };

        void exit(void) {
            MY_LOGD_IF(mLogLevel > 1, "DeliverMgr loop exit");
            Thread::requestExit();
            trigger();
            MY_LOGD_IF(mLogLevel > 1, "DeliverMgr loop join");
            Thread::join();
            MY_LOGD_IF(mLogLevel > 1, "DeliverMgr loop finish");
        };

    public:

        MBOOL registerNodeList(MINT32 num);

        MBOOL sendNodeQueue(QueNode_T & node, MBOOL needTrigger);

        MBOOL waitFlush(MBOOL needTrigger);

        MBOOL trigger(void);

    private:

        void dumpNumList(MBOOL isLock = MFALSE);

        void dumpNodeQueue(MBOOL isLock = MFALSE);

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //  Thread Interface.
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public:
        virtual status_t    readyToRun()
        {
            MY_LOGD("readyToRun DeliverMgr thread");

            // set name
            ::prctl(PR_SET_NAME, (unsigned long)"Cam@P1NodeMgr", 0, 0, 0);
            // set normal
            struct sched_param sched_p;
            sched_p.sched_priority = 0;
            ::sched_setscheduler(0, P1THREAD_POLICY, &sched_p);
            ::setpriority(PRIO_PROCESS, 0, P1THREAD_PRIORITY);
            //  Note: "priority" is nice value.
            //
            ::sched_getparam(0, &sched_p);
            MY_LOGD(
                "Tid: %d, policy: %d, priority: %d"
                , ::gettid(), ::sched_getscheduler(0)
                , sched_p.sched_priority
            );
            //
            return OK;
        };
    private:
        virtual bool        threadLoop()
        {
            if (exitPending()) {
                MY_LOGD("DeliverMgr try to leave");

                Mutex::Autolock _l(mDeliverLock);
                if (mNodeQueue.size() > 0) {
                    MY_LOGI("the deliver queue is not empty, go-on the loop");
                } else {
                    MY_LOGI("DeliverMgr Leaving");
                    return MFALSE;
                }
            }

            return deliverLoop();
        };

        enum LOOP_STATE
        {
            LOOP_STATE_INIT         = 0,
            LOOP_STATE_WAITING,
            LOOP_STATE_PROCESSING,
            LOOP_STATE_DONE
        };

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //  Data Member.
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    private:
        sp<P1NodeImp>       mpP1NodeImp;
        MINT32              mLogLevel;
        MBOOL               mLoopRunning;
        LOOP_STATE          mLoopState;
        Condition           mDoneCond;
        Condition           mDeliverCond;
        mutable Mutex       mDeliverLock;
        MINT32              mSentNum;
        List<MINT32>        mNumList;
        Vector<QueNode_T>   mNodeQueue;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //  Function Member.
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    private:
        MBOOL deliverLoop();


    };
    //
    class Tag {
        public:
                                    Tag()
                                    : mInfo(0)
                                    , mLock()
                                    {
                                    };
                                    ~Tag()
                                    {
                                    };
            void                    clear(void)
                                    {
                                        RWLock::AutoWLock _wl(mLock);
                                        mInfo = 0;
                                    };
            void                    set(MUINT32 info)
                                    {
                                        //RWLock::AutoWLock _wl(mLock);
                                        mInfo = info;
                                    };
            MUINT32                 get(void)
                                    {
                                        //RWLock::AutoRLock _rl(mLock);
                                        return mInfo;
                                    }
        private:
            MUINT32                 mInfo;
            mutable RWLock          mLock;
    };
    //
    enum REQ_STATE
    {
        REQ_STATE_WAITING           = 0,
        REQ_STATE_ONGOING,
        REQ_STATE_RECEIVE,
        REQ_STATE_CREATED
    };
    //
    enum START_CAP_STATE
    {
        START_CAP_STATE_NONE        = 0,
        START_CAP_STATE_WAIT_REQ,
        START_CAP_STATE_WAIT_CB,
        START_CAP_STATE_READY
    };
    //
protected:  ////                    Data Members. (Config)
    mutable RWLock                  mConfigRWLock;
    mutable Mutex                   mInitLock;
    MBOOL                           mInit;

    SortedVector<StreamId_T>        mInStreamIds;
    sp<IMetaStreamInfo>             mvStreamMeta[STREAM_META_NUM];
    sp<IImageStreamInfo>            mvStreamImg[STREAM_IMG_NUM];
    SensorParams                    mSensorParams;
    //RawParams                       mRawParams;
    sp<IImageStreamBufferPoolT>     mpStreamPool_full;
    sp<IImageStreamBufferPoolT>     mpStreamPool_resizer;
    sp<IImageStreamBufferPoolT>     mpStreamPool_lcso;
    sp<IImageStreamBufferPoolT>     mpStreamPool_rsso;
    MUINT8                          mBurstNum;
    QueJob_T                        mQueueJob;

    /**
     * the raw default type, if the request do not set the raw type,
     * it will use this setting to driver
     */
    MUINT32                         mRawDefType;

    /**
     * the raw type option means the capability recorded in each bit,
     * it is decided after the driver configuration
     */
    MUINT32                         mRawOption;
    MBOOL                           mDisableFrontalBinning;
    MBOOL                           mDisableDynamicTwin;

    MBOOL                           mEnableEISO;
    MBOOL                           mForceSetEIS;
    MUINT64                         mPackedEisInfo;
    MBOOL                           mEnableLCSO;
    MBOOL                           mEnableRSSO;
    MBOOL                           mEnableDualPD;
    MBOOL                           mEnableUniForcedOn;

    MBOOL                           mDisableHLR; // true:force-off false:auto
    MUINT32                         mPipeBit; //(E_CAM_PipelineBitDepth_SEL)

    MUINT8                          mTgNum;

    MINT                            mRawFormat;
    MUINT32                         mRawStride;
    MUINT32                         mRawLength;

    REV_MODE                        mReceiveMode;
    MUINT                           mSensorFormatOrder;

    LongExposureStatus              mLongExp;
    //Storage                         mImageStorage;

protected:  ////                    Data Members. (System capability)
    static const int                mNumInMeta = 2;
    static const int                mNumOutMeta = 3;
    int                             m3AProcessedDepth;
    int                             mNumHardwareBuffer;
    int                             mDelayframe;

protected:  ////
    MUINT32                         mLastNum;
    mutable Mutex                   mLastNumLock;
    MUINT32                         mLastSofIdx;
    MINT32                          mLastSetNum;

protected:  ////                    Data Members. (Hardware)
    mutable Mutex                   mHardwareLock;
    mutable Mutex                   mStopSttLock;
    mutable Mutex                   mActiveLock;
    MBOOL                           mActive;
    mutable Mutex                   mReadyLock;
    MBOOL                           mReady;
    MUINT32                         mCamIOVersion;
    INormalPipe*                    mpCamIO;
    IHal3A_T*                       mp3A;
    #if SUPPORT_VHDR
    VHdrHal*                        mpVhdr;
    #endif
    #if SUPPORT_LCS
    LcsHal*                         mpLCS;
    #endif
    //
    MRect                           mActiveArray;
    MUINT32                         mPixelMode;
    //
    MUINT32                         mConfigPort;
    MUINT32                         mConfigPortNum;
    MBOOL                           mIsBinEn;
    MBOOL                           mIsDynamicTwinEn;
    MBOOL                           mIsLegacyStandbyMode;
    MINT8                           mForceStandbyMode;
    //
    //DefaultKeyedVector< sp<IImageBuffer>, android::String8 >
    //                                mvStuffBufferInfo;
    //mutable Mutex                   mStuffBufferLock;
    //
    StuffBufferManager              mStuffBufMgr;
    //
    #define DRAWLINE_PORT_RRZO      0x1
    #define DRAWLINE_PORT_IMGO      0x2
    MUINT32                         mDebugScanLineMask;
    DebugScanLine*                  mpDebugScanLine;

protected:  ////                    Data Members. (Queue: Request)
    mutable Mutex                   mRequestQueueLock;
    //Condition                       mRequestQueueCond;
    Que_T                           mRequestQueue;

protected:  ////                    Data Members. (Queue: 3Alist)
    mutable Mutex                   mControls3AListLock;
    List<MetaSet_T>                 mControls3AList;
    Condition                       mControls3AListCond;
    //
    mutable Mutex                   mFrameSetLock;
    MBOOL                           mFrameSetAlready;
    //
    REQ_STATE                       mReqState;
    MBOOL                           mFirstReceived;
    //
    mutable Mutex                   mStartCaptureLock;
    Condition                       mStartCaptureCond;
    START_CAP_STATE                 mStartCaptureState;
    MUINT32                         mStartCaptureType;
    MUINT32                         mStartCaptureIdx;
    MINT64                          mStartCaptureExp;

protected:  ////
    mutable Mutex                   mStandbyLock;

protected:  ////
    //AAAResult                       m3AStorage;

protected:  ////                    Data Members. (Queue: Processing)
    mutable Mutex                   mProcessingQueueLock;
    Condition                       mProcessingQueueCond;
    Que_T                           mProcessingQueue;

protected:  ////                    Data Members. (Queue: drop)
    mutable Mutex                   mDropQueueLock;
    Vector<MUINT>                   mDropQueue;

protected:  ////                    Data Members.
    mutable Mutex                   mThreadLock;
    Condition                       mThreadCond;

protected:  ////                    Data Members.
    DurationProfile                 mDequeThreadProfile;

protected:  ////                    Data Members.
    mutable Mutex                   mPublicLock;

protected:  ////                    Data Members.
    MINT32                          mInFlightRequestCnt;

protected:  ////                    Data Members.
    sp<DeliverMgr>                  mpDeliverMgr;

protected:  ////                    Data Members.
    sp<P1NodeConnectLMV>            mpConnectLMV;

protected:  ////                    Data Members.
    sp<ConcurrenceControl>          mpConCtrl;

protected:  ////                    Data Members.
    sp<HardwareStateControl>        mpHwStateCtrl;

protected:  ////                    Data Members.
    sp<TimingCheckerMgr>            mpTimingCheckerMgr;
    MUINT32                         mTimingFactor;

protected:
    MINT32                          mLogLevel;
    MINT32                          mLogLevelI;
    MINT32                          mEnableDumpRaw;
    MINT32                          mDisableAEEIS;
    Tag                             mTagReq;
    Tag                             mTagSet;
    Tag                             mTagEnq;
    Tag                             mTagDeq;
    Tag                             mTagOut;
    Tag                             mTagList;

protected:  ////                    Data Members.
    MUINT32                         mInitReqSet; // the request set from user
    MUINT32                         mInitReqNum; // the total number need to recive
    MUINT32                         mInitReqCnt; // the currently recived count
    MUINT32                         mInitReqPad; // the number of the first padding request
    MINT32                          mInitSetNum; // the first set magic num

protected:  ////                    Data Members.
    MBOOL                           mEnableCaptureFlow;
    MBOOL                           mEnableFrameSync;
    MBOOL                           mStereoCamMode;

protected:  ////                    Operations.

    MVOID                           setActive(
                                        MBOOL active
                                    );

    MBOOL                           getActive(
                                        void
                                    );

    MVOID                           setReady(
                                        MBOOL ready
                                    );

    MBOOL                           getReady(
                                        void
                                    );

    MVOID                           setInit(
                                        MBOOL init
                                    );

    MBOOL                           getInit(
                                        void
                                    );

    MVOID                           onRequestFrameSet(
                                        MBOOL initial = MFALSE
                                    );

    MVOID                           onSyncEnd(
                                        void
                                    );

    MVOID                           onSyncBegin(
                                        MBOOL initial,
                                        RequestSet_T* reqSet = NULL,//MUINT32 magicNum = 0,
                                        MUINT32 sofIdx = P1SOFIDX_INIT_VAL,
                                        CapParam_T* capParam = NULL
                                    );

#if 0
    MVOID                           onProcess3AResult(
                                        MUINT32 magicNum,
                                        MUINT32 key,
                                        MUINT32 val
                                    );
#endif

    MERROR                          onProcessEnqueFrame(
                                        QueJob_T &job
                                    );

    MERROR                          onProcessDequeFrame(
                                    );

    MERROR                          onProcessDropFrame(
                                        MBOOL isTrigger = MFALSE
                                    );

    MVOID                           onCheckDropFrame(
                                        void
                                    );

    MBOOL                           getProcessingFrame_ByAddr(
                                        IImageBuffer* const imgBuffer,
                                        MINT32 magicNum,
                                        QueJob_T &job
                                    );

    QueJob_T                        getProcessingFrame_ByNumber(
                                        MINT32 magicNum
                                    );


    MVOID                           onHandleFlush(
                                        MBOOL wait
                                    );

    MVOID                           processRedoFrame(
                                        QueNode_T & node
                                    );

    MVOID                           processYuvFrame(
                                        QueNode_T & node
                                    );

    MVOID                           onReturnFrame(
                                        QueNode_T & node,
                                        MBOOL isFlush,
                                        MBOOL isTrigger = MTRUE
                                    );

    MVOID                           releaseNode(
                                        QueNode_T & node
                                    );

    MVOID                           onProcessResult(
                                        QueNode_T & node,
                                        QBufInfo const &deqBuf,
                                        MetaSet_T const &result3A,
                                        IMetadata const &resultAppend,
                                        MUINT32 const index = 0
                                    );

    MBOOL                           findPortBufIndex(
                                        QBufInfo & deqBuf,
                                        QueJob_T & job
                                    );

    MERROR                          findRequestStream(
                                        QueNode_T & node
                                    );

    MVOID                           receiveFrame(
                                        sp<IPipelineFrame> appframe,
                                        QueNode_T & pNode
                                    );

    MVOID                           createNode(sp<IPipelineFrame> appframe,
                                        QueNode_T *pNode,
                                        QueJob_T *job,
                                        Que_T *Queue,
                                        Mutex *QueLock,
                                        List<MetaSet_T> *list,
                                        Mutex *listLock
                                    );

    MVOID                           createNode(List<MetaSet_T> *list,
                                        Mutex *listLock
                                    );

    MVOID                           createNode(Que_T &Queue);

protected:  ////                    Hardware Operations.
    MERROR                          hardwareOps_start(
                                    );

    MERROR                          hardwareOps_enque(
                                        QueJob_T &job,
                                        ENQ_TYPE type = ENQ_TYPE_NORMAL,
                                        MINT64 data = 0
                                    );

    MERROR                          hardwareOps_deque(
                                        QBufInfo &deqBuf
                                    );

    MERROR                          hardwareOps_stop(
                                    );

    MERROR                          hardwareOps_request(
                                    );

    MERROR                          hardwareOps_capture(
                                    );

    MERROR                          hardwareOps_standby(
                                        List<MetaSet_T> *list,
                                        Mutex *listLock
                                    );

    MERROR                          procedureAid_start(
                                    );

    MERROR                          buildInitItem(
                                    );

    MERROR                          setupNode(
                                        QueNode_T & node,
                                        QBufInfo & info
                                    );

    MERROR                          createStuffBuffer(
                                        sp<IImageBuffer> & imageBuffer,
                                        sp<IImageStreamInfo> const& streamInfo,
                                        NSCam::MSize::value_type const
                                            changeHeight = 0
                                    );

    MERROR                          createStuffBuffer(
                                        sp<IImageBuffer> & imageBuffer,
                                        char const * szName,
                                        MINT32 format,
                                        MSize size,
                                        Vector<MUINT32> vStride
                                    );

    MERROR                          destroyStuffBuffer(
                                        sp<IImageBuffer> & imageBuffer
                                    );

    MVOID                           generateAppMeta(
                                        QueNode_T & node,
                                        MetaSet_T const &result3A,
                                        QBufInfo const &deqBuf,
                                        IMetadata &appMetadata,
                                        MUINT32 const index = 0
                                    );

    MVOID                           generateAppTagIndex(
                                        IMetadata &appMetadata,
                                        IMetadata &appTagIndex
                                    );

    MVOID                           generateHalMeta(
                                        QueNode_T & node,
                                        MetaSet_T const &result3A,
                                        QBufInfo const &deqBuf,
                                        IMetadata const &resultAppend,
                                        IMetadata const &inHalMetadata,
                                        IMetadata &halMetadata,
                                        MUINT32 const index = 0
                                    );

    MVOID                           prepareCropInfo(
                                       IMetadata* pAppMetadata,
                                       IMetadata* pHalMetadata,
                                       QueNode_T& node
                                    );

    MERROR                          check_config(
                                        ConfigParams const& rParams
                                    );

    MERROR                          requestMetadataEarlyCallback(
                                        QueNode_T const & node,
                                        STREAM_META const streamMeta,
                                        IMetadata * pMetadata
                                    );

    MERROR                          frameMetadataInit(
                                        QueNode_T & node,
                                        STREAM_META const streamMeta,
                                        sp<IMetaStreamBuffer> &
                                        pMetaStreamBuffer
                                    );

    MERROR                          frameMetadataGet(
                                        QueNode_T & node,
                                        STREAM_META const streamMeta,
                                        IMetadata * pOutMetadata,
                                        MBOOL toWrite = MFALSE,
                                        IMetadata * pInMetadata = NULL
                                    );

    MERROR                          frameMetadataPut(
                                        QueNode_T & node,
                                        STREAM_META const streamMeta
                                    );

    MERROR                          frameImageInit(
                                        QueNode_T & node,
                                        STREAM_IMG const streamImg,
                                        sp<IImageStreamBuffer> &
                                        pImageStreamBuffer
                                    );

    MERROR                          frameImageGet(
                                        QueNode_T & node,
                                        STREAM_IMG const streamImg,
                                        sp<IImageBuffer> &rImgBuf
                                    );

    MERROR                          frameImagePut(
                                        QueNode_T & node,
                                        STREAM_IMG const streamImg
                                    );

    MERROR                          poolImageGet(
                                        QueNode_T & node,
                                        STREAM_IMG const streamImg,
                                        sp<IImageBuffer> &rImgBuf
                                    );

    MERROR                          poolImagePut(
                                        QueNode_T & node,
                                        STREAM_IMG const streamImg
                                    );

    MERROR                          stuffImageGet(
                                        QueNode_T & node,
                                        STREAM_IMG const streamImg,
                                        MSize const dstSize,
                                        sp<IImageBuffer> &rImgBuf
                                    );

    MERROR                          stuffImagePut(
                                        QueNode_T & node,
                                        STREAM_IMG const streamImg
                                    );

    MUINT32                         get_and_increase_magicnum()
                                    {
                                        Mutex::Autolock _l(mLastNumLock);
                                        MUINT32 ret = mLastNum++;
                                        //skip num = 0 as 3A would callback 0 when request stack is empty
                                        //skip -1U as a reserved number to indicate that which would never happen in 3A queue
                                        if(ret==0 || ret==-1U) ret=mLastNum=1;
                                        return ret;
                                    }

    MUINT32                         get_last_magicnum()
                                    {
                                        Mutex::Autolock _l(mLastNumLock);
                                        MUINT32 ret =
                                            (mLastNum > 0) ? (mLastNum - 1) : 0;
                                        return ret;
                                    }

    MBOOL                           isRevMode(REV_MODE mode)
                                    {
                                        return (mode == mReceiveMode) ?
                                            (MTRUE) : (MFALSE);
                                    };

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Operations in base class Thread
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////
    // Ask this object's thread to exit. This function is asynchronous, when the
    // function returns the thread might still be running. Of course, this
    // function can be called from a different thread.
    virtual void                    requestExit();

    // Good place to do one-time initializations
    virtual status_t                readyToRun();

private:
    // Derived class must implement threadLoop(). The thread starts its life
    // here. There are two ways of using the Thread object:
    // 1) loop: if threadLoop() returns true, it will be called again if
    //          requestExit() wasn't called.
    // 2) once: if threadLoop() returns false, the thread will exit upon return.
    virtual bool                    threadLoop();

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Interface.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////                    Operations.
                                    P1NodeImp();
    virtual                        ~P1NodeImp();
    virtual MERROR                  config(ConfigParams const& rParams);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IPipelineNode Interface.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////                    Operations.

    virtual MERROR                  init(InitParams const& rParams);

    virtual MERROR                  uninit();

    virtual MERROR                  flush();

    virtual MERROR                  flush(
                                        android::sp<IPipelineFrame> const &pFrame
                                    );

    virtual MERROR                  queue(
                                        sp<IPipelineFrame> pFrame
                                    );

public:     ////                    Operations.

    virtual void                    doNotifyCb (
                                        MINT32  _msgType,
                                        MINTPTR _ext1,
                                        MINTPTR _ext2,
                                        MINTPTR _ext3
                                    );

    static void                     doNotifyDropframe(MUINT magicNum, void* cookie);
};


/******************************************************************************
 *
 ******************************************************************************/
P1NodeImp::
P1NodeImp()
    : BaseNode()
    , P1Node()
    , mConfigRWLock()
    , mInitLock()
    , mInit(MTRUE)
    //
    , mpStreamPool_full(NULL)
    , mpStreamPool_resizer(NULL)
    , mpStreamPool_lcso(NULL)
    , mpStreamPool_rsso(NULL)
    , mBurstNum(1)
    , mQueueJob(1)
    , mRawDefType(EPipe_PURE_RAW)
    , mRawOption(0)
    , mDisableFrontalBinning(MFALSE)
    , mDisableDynamicTwin(MFALSE)
    , mEnableEISO(MFALSE)
    , mForceSetEIS(MFALSE)
    , mPackedEisInfo(0)
    , mEnableLCSO(MFALSE)
    , mEnableRSSO(MFALSE)
    , mEnableDualPD(MFALSE)
    , mEnableUniForcedOn(MFALSE)
    , mDisableHLR(MFALSE)
    , mPipeBit(CAM_Pipeline_12BITS)
    , mTgNum(0)
    //
    , mRawFormat(P1_IMGO_DEF_FMT)
    , mRawStride(0)
    , mRawLength(0)
    //
    , mReceiveMode(REV_MODE_NORMAL)
    , mSensorFormatOrder(SENSOR_FORMAT_ORDER_NONE)
    //
    //, mImageStorage()
    //
    , m3AProcessedDepth(3)
    , mNumHardwareBuffer(3)
    , mDelayframe(3)
    , mLastNum(1)
    , mLastNumLock()
    , mLastSofIdx(P1SOFIDX_NULL_VAL)
    , mLastSetNum(0)
    , mHardwareLock()
    , mStopSttLock()
    , mActiveLock()
    , mActive(MFALSE)
    , mReadyLock()
    , mReady(MFALSE)
    , mCamIOVersion(0)
    , mpCamIO(NULL)
    , mp3A(NULL)
    #if SUPPORT_VHDR
    , mpVhdr(NULL)
    #endif
    #if SUPPORT_LCS
    , mpLCS(NULL)
    #endif
    //
    , mPixelMode(0)
    //
    , mConfigPort(CONFIG_PORT_NONE)
    , mConfigPortNum(0)
    , mIsBinEn(MFALSE)
    , mIsDynamicTwinEn(MFALSE)
    , mIsLegacyStandbyMode(MFALSE)
    , mForceStandbyMode(0)
    //
    , mStuffBufMgr()
    //
    , mDebugScanLineMask(0)
    , mpDebugScanLine(NULL)
    //
    , mRequestQueueLock()
    , mRequestQueue()
    //
    , mControls3AListLock()
    , mControls3AList()
    , mControls3AListCond()
    //
    , mFrameSetLock()
    , mFrameSetAlready(MFALSE)
    //
    , mReqState(REQ_STATE_CREATED)
    , mFirstReceived(MFALSE)
    //
    , mStartCaptureLock()
    , mStartCaptureCond()
    , mStartCaptureState(START_CAP_STATE_NONE)
    , mStartCaptureType(E_CAPTURE_NORMAL)
    , mStartCaptureIdx(0)
    , mStartCaptureExp(0)
    //
    , mStandbyLock()
    //
    //, m3AStorage()
    //
    , mProcessingQueueLock()
    , mProcessingQueueCond()
    , mProcessingQueue()
    //
    , mDropQueueLock()
    , mDropQueue()
    //
    , mThreadLock()
    , mThreadCond()
    //
    , mDequeThreadProfile("P1Node::deque", 30000000LL)
    , mInFlightRequestCnt(0)
    //
    , mpDeliverMgr(NULL)
    //
    , mpConnectLMV(NULL)
    //
    , mpConCtrl(NULL)
    //
    , mpHwStateCtrl(NULL)
    //
    , mpTimingCheckerMgr(NULL)
    , mTimingFactor(1)
    //
    , mLogLevel(0)
    , mLogLevelI(0)
    , mEnableDumpRaw(0)
    , mDisableAEEIS(0)
    , mTagReq()
    , mTagSet()
    , mTagEnq()
    , mTagDeq()
    , mTagOut()
    , mTagList()
    //
    , mInitReqSet(0)
    , mInitReqNum(0)
    , mInitReqCnt(0)
    , mInitReqPad(0)
    , mInitSetNum(0)
    //
    , mEnableCaptureFlow(MFALSE)
    , mEnableFrameSync(MFALSE)
    , mStereoCamMode(MFALSE)
{
    MINT32 cam_log = ::property_get_int32("debug.camera.log", 0);
    MINT32 p1_log = ::property_get_int32("debug.camera.log.p1node", 1);
    mLogLevel = MAX(cam_log, p1_log);
#if 0 // force to enable all p1 node log
    #warning "[FIXME] force to enable P1Node log"
    if (mLogLevel < 2) {
        mLogLevel = 2;
    }
#endif
    MBOOL buildLogD = MFALSE;
    MBOOL buildLogI = MFALSE;
#if (IS_P1_LOGI)
    //#warning "IS_P1_LOGI build LogI"
    mLogLevelI = (mLogLevel > 0) ? (mLogLevel - 1) : (mLogLevel);
    buildLogI = MTRUE;
#endif
#if (IS_P1_LOGD)
    //#warning "IS_P1_LOGD build LogD"
    mLogLevelI = mLogLevel;
    buildLogD = MTRUE;
#endif
    if (p1_log > 1) {
        mLogLevelI = mLogLevel;
    }
    //
    MINT32 p1_logi = ::property_get_int32("debug.camera.log.p1nodei", 0);
    if (p1_logi > 0) {
        mLogLevelI = p1_logi;
    }
    mStuffBufMgr.setLog(mLogLevel);
    //
    mEnableDumpRaw = property_get_int32("debug.feature.forceEnableIMGO", 0);

    mDisableAEEIS = property_get_int32("debug.eis.disableae", 0);
    //
    mDebugScanLineMask = ::property_get_int32("debug.camera.scanline.p1", 0);
    if ( mDebugScanLineMask != 0)
    {
        mpDebugScanLine = DebugScanLine::createInstance();
    }
    //
#if     (MTKCAM_HW_NODE_LOG_LEVEL_DEFAULT > 3)
    mTimingFactor = 32;  // for ENG build
#elif   (MTKCAM_HW_NODE_LOG_LEVEL_DEFAULT > 2)
    mTimingFactor = 2;  // for USERDEBUG build
#else
    mTimingFactor = 1;  // for USER build
#endif
    //
    MY_LOGI("LOGD[%d](%d) LOGI[%d](%d) prop(%d,%d,%d) Dp(%d) Db(%d) TF(%d)",
        buildLogD, mLogLevel, buildLogI, mLogLevelI, cam_log, p1_log, p1_logi,
        mEnableDumpRaw, mDebugScanLineMask, mTimingFactor);
}


/******************************************************************************
 *
 ******************************************************************************/
P1NodeImp::
~P1NodeImp()
{
    MY_LOGD("");
    if( mpDebugScanLine != NULL )
    {
        mpDebugScanLine->destroyInstance();
        mpDebugScanLine = NULL;
    }
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
init(InitParams const& rParams)
{
    FUNCTION_S_IN;

    {
        RWLock::AutoWLock _l(mConfigRWLock);
        //
        mOpenId  = rParams.openId;
        mNodeId  = rParams.nodeId;
        mNodeName= rParams.nodeName;
    }

    //  Select CamIO version
    {
        auto pModule = getNormalPipeModule();

        if  ( ! pModule ) {
            MY_LOGE("getNormalPipeModule() fail");
            return UNKNOWN_ERROR;
        }

        MUINT32 const* version = NULL;
        size_t count = 0;
        int err = pModule->get_sub_module_api_version(&version, &count, mOpenId);
        if  ( err < 0 || ! count || ! version ) {
            MY_LOGE(
                "[%d] INormalPipeModule::get_sub_module_api_version - err:%#x count:%zu version:%p",
                mOpenId, err, count, version
            );
            return UNKNOWN_ERROR;
        }

        mCamIOVersion = *(version + count - 1); //Select max. version
        MY_LOGD("[%d] count:%zu Selected CamIO Version:%0#x", mOpenId, count, mCamIOVersion);
    }

    mpConCtrl = new ConcurrenceControl();
    if (mpConCtrl == NULL || mpConCtrl->getStageCtrl() == NULL) {
        MY_LOGE("ConcurrenceControl create fail");
        return NO_MEMORY;
    }

    mpHwStateCtrl = new HardwareStateControl();
    if (mpHwStateCtrl == NULL) {
        MY_LOGE("HardwareStateControl create fail");
        return NO_MEMORY;
    }

    mpConnectLMV = new P1NodeConnectLMV(mLogLevel, getOpenId());
    if (mpConnectLMV == NULL) {
        MY_LOGE("ConnectLMV create fail (%d)(%d)", mLogLevel, getOpenId());
        return NO_MEMORY;
    }

    mpTimingCheckerMgr = new TimingCheckerMgr(mTimingFactor);
    if (mpTimingCheckerMgr == NULL) {
        MY_LOGE("TimingCheckerMgr create fail");
        return NO_MEMORY;
    }

    MERROR err = run("P1NodeImp::init");

    mpDeliverMgr = new DeliverMgr();
    if (mpDeliverMgr != NULL) {
        mpDeliverMgr->init(this);
    } else {
        MY_LOGE("DeliverMgr create fail");
        return NO_MEMORY;
    }

    FUNCTION_S_OUT;

    return err;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
uninit()
{
    PUBLIC_APIS_IN;

    // flush the left frames if exist
    onHandleFlush(MFALSE);

    requestExit();

    //mvStreamMeta.clear();
    for (int stream = STREAM_ITEM_START; stream < STREAM_META_NUM; stream++) {
        mvStreamMeta[stream] = NULL;
    }

    //mvStreamImg.clear();
    for (int stream = STREAM_ITEM_START; stream < STREAM_IMG_NUM; stream++) {
        mvStreamImg[stream] = NULL;
    }

    if (mpDeliverMgr != NULL) {
        mpDeliverMgr->uninit();
        mpDeliverMgr = NULL;
    }

    if (mpTimingCheckerMgr != NULL) {
        mpTimingCheckerMgr = NULL;
    }

    if (mpHwStateCtrl != NULL) {
        mpHwStateCtrl = NULL;
    }

    if (mpConCtrl != NULL) {
        mpConCtrl = NULL;
    }

    PUBLIC_APIS_OUT;

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
check_config(ConfigParams const& rParams)
{
    CAM_TRACE_NAME("P1:check_config");

    if (rParams.pInAppMeta == NULL) {
        MY_LOGE("in app metadata is null");
        return BAD_VALUE;
    }

    if (rParams.pInHalMeta == NULL) {
        MY_LOGE("in hal metadata is null");
        return BAD_VALUE;
    }

    if (rParams.pOutAppMeta == NULL) {
        MY_LOGE("out app metadata is null");
        return BAD_VALUE;
    }

    if (rParams.pOutHalMeta == NULL) {
        MY_LOGE("out hal metadata is null");
        return BAD_VALUE;
    }

    if (rParams.pvOutImage_full.size() == 0 &&
        rParams.pOutImage_resizer == NULL) {
        MY_LOGE("image is empty");
        return BAD_VALUE;
    }

    if (rParams.pStreamPool_full != NULL &&
        rParams.pvOutImage_full.size() == 0) {
        MY_LOGE("wrong full input");
        return BAD_VALUE;
    }

    if (rParams.pStreamPool_resizer != NULL &&
        rParams.pOutImage_resizer == NULL) {
        MY_LOGE("wrong resizer input");
        return BAD_VALUE;
    }
    #if SUPPORT_LCS
    if (rParams.pStreamPool_lcso != NULL &&
        rParams.pOutImage_lcso == NULL) {
        MY_LOGE("wrong lcso input");
        return BAD_VALUE;
    }
    if (rParams.enableLCS == MTRUE &&
        rParams.pOutImage_lcso == NULL) {
        MY_LOGE("LCS enable but no lcso input");
        return BAD_VALUE;
    }
    #endif
    #if SUPPORT_RSS
    if (rParams.pStreamPool_rsso != NULL &&
        rParams.pOutImage_rsso == NULL) {
        MY_LOGE("wrong rsso input");
        return BAD_VALUE;
    }
    if (rParams.enableRSS == MTRUE &&
        rParams.pOutImage_rsso == NULL) {
        MY_LOGE("RSS enable but no rsso input");
        return BAD_VALUE;
    }
    #endif
    //
    if (mpDeliverMgr != NULL && mpDeliverMgr->runningGet()/*isRunning()*/) {
        MY_LOGI("DeliverMgr thread is running");
        mpDeliverMgr->requestExit();
        mpDeliverMgr->trigger();
        mpDeliverMgr->join();
        mpDeliverMgr->runningSet(MFALSE);
    }

    // Get sensor format
    IHalSensorList *const pIHalSensorList = MAKE_HalSensorList();
    if (pIHalSensorList) {
        MUINT32 sensorDev = (MUINT32) pIHalSensorList->querySensorDevIdx(getOpenId());

        NSCam::SensorStaticInfo sensorStaticInfo;
        memset(&sensorStaticInfo, 0, sizeof(NSCam::SensorStaticInfo));
        pIHalSensorList->querySensorStaticInfo(sensorDev, &sensorStaticInfo);
        mSensorFormatOrder = sensorStaticInfo.sensorFormatOrder;
    }

    //
    {
        RWLock::AutoWLock _l(mConfigRWLock);
        //
        for (int meta = STREAM_ITEM_START; meta < STREAM_META_NUM; meta++) {
            mvStreamMeta[meta] = NULL;
        }
        if (rParams.pInAppMeta != NULL) {
            mvStreamMeta[STREAM_META_IN_APP] = rParams.pInAppMeta;
            //mInAppMeta = rParams.pInAppMeta;
        };
        if (rParams.pInHalMeta != NULL) {
            mvStreamMeta[STREAM_META_IN_HAL] = rParams.pInHalMeta;
            //mInHalMeta = rParams.pInHalMeta;
        };
        if (rParams.pOutAppMeta != NULL) {
            mvStreamMeta[STREAM_META_OUT_APP] = rParams.pOutAppMeta;
            //mOutAppMeta = rParams.pOutAppMeta;
        };
        if (rParams.pOutHalMeta != NULL) {
            mvStreamMeta[STREAM_META_OUT_HAL] = rParams.pOutHalMeta;
            //mOutHalMeta = rParams.pOutHalMeta;
        };
        //
        for (int img = STREAM_ITEM_START; img < STREAM_IMG_NUM; img++) {
            mvStreamImg[img] = NULL;
        }
        #if 1
        if (rParams.pInImage_yuv != NULL) {
            mvStreamImg[STREAM_IMG_IN_YUV] = rParams.pInImage_yuv;
        };
        if (rParams.pInImage_opaque != NULL) {
            mvStreamImg[STREAM_IMG_IN_OPAQUE] = rParams.pInImage_opaque;
        };
        if (rParams.pOutImage_opaque != NULL) {
            mvStreamImg[STREAM_IMG_OUT_OPAQUE] = rParams.pOutImage_opaque;
        };
        #if 1
        for (size_t i  = 0; i < rParams.pvOutImage_full.size(); i++) {
            if (rParams.pvOutImage_full[i] != NULL) { // pick the first item
                mvStreamImg[STREAM_IMG_OUT_FULL] = rParams.pvOutImage_full[i];
                break;
            }
        }
        #else
        if (rParams.pvOutImage_full.size() > 0) {
            mvStreamImg[STREAM_IMG_OUT_FULL] = rParams.pvOutImage_full[0];
            //mvStreamImg[STREAM_IMG_OUT_FULL] = rParams.pOutImage_full;
        };
        #endif
        if (rParams.pOutImage_resizer != NULL) {
            mvStreamImg[STREAM_IMG_OUT_RESIZE] = rParams.pOutImage_resizer;
        };
        #if SUPPORT_LCS
        if (rParams.pOutImage_lcso != NULL) {
            mvStreamImg[STREAM_IMG_OUT_LCS] = rParams.pOutImage_lcso;
        };
        #endif
        #if SUPPORT_RSS
        if (rParams.pOutImage_rsso != NULL) {
            mvStreamImg[STREAM_IMG_OUT_RSS] = rParams.pOutImage_rsso;
        };
        #endif
        //
        mpStreamPool_full = (rParams.pStreamPool_full != NULL) ?
            rParams.pStreamPool_full : NULL;
        mpStreamPool_resizer = (rParams.pStreamPool_resizer != NULL) ?
            rParams.pStreamPool_resizer : NULL;
        #if SUPPORT_LCS
        mpStreamPool_lcso = (rParams.pStreamPool_lcso != NULL) ?
            rParams.pStreamPool_lcso : NULL;
        #endif
        #if SUPPORT_RSS
        mpStreamPool_rsso = (rParams.pStreamPool_rsso != NULL) ?
            rParams.pStreamPool_rsso : NULL;
        #endif
        //
        {
            MBOOL deliver_mgr_send = MTRUE;
            #if 0
            #warning "[FIXME] force to change p1 dispatch frame directly"
            {
                MUINT8 dispatch =
                    ::property_get_int32("debug.camera.p1dispatch", 0);
                MY_LOGI("debug.camera.p1dispatch = %d", dispatch);
                if (dispatch > 0) {
                    deliver_mgr_send = MFALSE;
                };
            }
            #endif
            if (mvStreamImg[STREAM_IMG_IN_YUV] != NULL ||
                mvStreamImg[STREAM_IMG_IN_OPAQUE] != NULL) {
                deliver_mgr_send = MTRUE; // for reprocessing flow
            };
            MY_LOGD2("USE DeliverMgr Thread Loop : %d", deliver_mgr_send);
            if (deliver_mgr_send) {
                if (mpDeliverMgr != NULL &&
                    NO_ERROR == mpDeliverMgr->run("P1NodeImp::config")) {
                    MY_LOGD2("RUN DeliverMgr Thread OK");
                    mpDeliverMgr->runningSet(MTRUE);
                } else {
                    MY_LOGE("RUN DeliverMgr Thread FAIL");
                    return BAD_VALUE;
                }
            }
        }
        #endif
        //
        #if 0
        #warning "[FIXME] force to change p1 not use pool"
        {
            MUINT8 no_pool =
                    ::property_get_int32("debug.camera.p1nopool", 0);
            if (no_pool > 0) {
                mpStreamPool_full = NULL;
                mpStreamPool_resizer = NULL;
                mpStreamPool_lcso = NULL;
                mpStreamPool_rsso = NULL;
            }
            MY_LOGI("debug.camera.p1nopool = %d", no_pool);
        }
        #endif
        //
        mBurstNum = MAX(rParams.burstNum, 1);
        #if 0 // for SMVR IT
        #warning "[FIXME] force to change p1 burst number"
        {
            MUINT8 burst_num =
                    ::property_get_int32("debug.camera.p1burst", 0);
            if (burst_num > 0) {
                mBurstNum = burst_num;
            }
            MY_LOGI("debug.camera.p1burst = %d  -  BurstNum = %d",
                burst_num, mBurstNum);
        }
        #endif
        //
        mReceiveMode = rParams.receiveMode;
        if ((mBurstNum > 1)
            || (mvStreamImg[STREAM_IMG_IN_OPAQUE] != NULL)
            || (mvStreamImg[STREAM_IMG_IN_YUV] != NULL)
            ) {
            mReceiveMode = REV_MODE_CONSERVATIVE;
        }
        #if 0 // receive mode IT
        #warning "[FIXME] force to change p1 receive mode"
        {
            MUINT8 rev_mode =
                    ::property_get_int32("debug.camera.p1rev", 0);
            if (rev_mode > 0) {
                mReceiveMode = (REV_MODE)rev_mode;
            }
            MY_LOGI("debug.camera.p1rev = %d  - RevMode=%d BurstNum=%d",
                rev_mode, mReceiveMode, mBurstNum);
        }
        #endif
        //
        #if 1 // standby mode
        //#warning "[FIXME] force to change standby mode"
        {
            MINT8 standby_mode =
                    ::property_get_int32("debug.camera.standbymode", 0);
            if (standby_mode > 0) {
                mForceStandbyMode = standby_mode;
                MY_LOGI("debug.camera.standbymode = %d - ForceStandbyMode = %d",
                    standby_mode, mForceStandbyMode);
            }

        }
        #endif
        //
        mSensorParams = rParams.sensorParams;
        //
        mEnableDualPD = rParams.enableDualPD;
        //
        mRawDefType = EPipe_PURE_RAW;
        mRawOption = (1 << EPipe_PURE_RAW);
        if (rParams.rawProcessed == MTRUE) {
            // DualPD, raw type will be selected by driver
            mRawDefType = EPipe_PROCESSED_RAW;
            mRawOption |= (1 << EPipe_PROCESSED_RAW);
        }
        //
        mTgNum = rParams.tgNum;
        //
        mPipeBit = rParams.pipeBit;
        //
        mDisableHLR = rParams.disableHLR;
        //
        mDisableFrontalBinning = rParams.disableFrontalBinning;
        //
        mDisableDynamicTwin = rParams.disableDynamicTwin;
        //
        mEnableUniForcedOn = rParams.enableUNI;
        //
        if (IS_LMV(mpConnectLMV)) {
            mEnableEISO = rParams.enableEIS;
            mForceSetEIS = rParams.forceSetEIS;
            mPackedEisInfo = rParams.packedEisInfo;
        }
        #if SUPPORT_LCS
        mEnableLCSO = rParams.enableLCS;
        #endif
        #if SUPPORT_RSS
        mEnableRSSO = rParams.enableRSS;
        #endif
        //
        mEnableCaptureFlow = rParams.enableCaptureFlow;
        mEnableFrameSync = rParams.enableFrameSync;
        mStereoCamMode = rParams.stereoCamMode;
        if (EN_START_CAP) { // disable - acquire init buffer from pool
            mpStreamPool_full = NULL;
            mpStreamPool_resizer = NULL;
        }
        //
        {
            mInitReqSet = rParams.initRequest;
            #if 0 // init request set IT
            #warning "[FIXME] force to set init request"
            {
                MUINT8 init_req =
                        ::property_get_int32("debug.camera.p1init", 0);
                if (init_req > 0) {
                    mInitReqSet = init_req;
                }
                MY_LOGI("debug.camera.p1init = %d  - mInitReq=%d BurstNum=%d",
                    init_req, mInitReqSet, mBurstNum);
            }
            #endif
            if (EN_INIT_REQ && mInitReqSet <= P1NODE_DEF_SHUTTER_DELAY) {
                MY_LOGE("INVALID init request value (%d)", mInitReqSet);
                return BAD_VALUE;
            }
            mInitReqNum = mInitReqSet * mBurstNum;
            mInitReqCnt = 0;
            mInitReqPad = (mInitReqSet - P1NODE_DEF_SHUTTER_DELAY) * mBurstNum;
            MY_LOGI_IF(EN_INIT_REQ, "InitReq Set:%d Num:%d Cnt:%d",
                mInitReqSet, mInitReqNum, mInitReqCnt);
        }
        //
        #if 1 // check conflict configuration
        {
            if (EN_START_CAP) {
                if (EN_INIT_REQ || mBurstNum > 1) {
                    MY_LOGE("[Check_Config_Conflict] P1Node::ConfigParams:: "
                        "enableCaptureFlow(%d) initRequest(%d) burstNum(%d)",
                        rParams.enableCaptureFlow,
                        rParams.initRequest, rParams.burstNum);
                    return BAD_VALUE;
                }
            }
        }
        #endif
    }
    //
    if (mvStreamImg[STREAM_IMG_OUT_OPAQUE] != NULL) {
        if (mvStreamImg[STREAM_IMG_OUT_FULL] != NULL) {
            mRawFormat = mvStreamImg[STREAM_IMG_OUT_FULL]->getImgFormat();
            mRawStride = mvStreamImg[STREAM_IMG_OUT_FULL]->getBufPlanes().
                itemAt(0).rowStrideInBytes;
            mRawLength = mvStreamImg[STREAM_IMG_OUT_FULL]->getBufPlanes().
                itemAt(0).sizeInBytes;
        } else {
            mRawFormat = P1_IMGO_DEF_FMT;
            NSCam::NSIoPipe::NSCamIOPipe::NormalPipe_QueryInfo queryRst;
            getNormalPipeModule()->query(
                    NSCam::NSIoPipe::PORT_IMGO.index,
                    NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_STRIDE_BYTE,
                    (EImageFormat)mRawFormat,
                    mSensorParams.size.w,
                    queryRst
                    );
            mRawStride = queryRst.stride_byte;
            mRawLength = mRawStride * mSensorParams.size.h;
        }
    }
    //
    {
        sp<IMetadataProvider> pMetadataProvider =
            NSMetadataProviderManager::valueFor(getOpenId());
        if( ! pMetadataProvider.get() ) {
            MY_LOGE(" ! pMetadataProvider.get() ");
            return DEAD_OBJECT;
        }
        IMetadata static_meta =
            pMetadataProvider->getMtkStaticCharacteristics();
        if( tryGetMetadata<MRect>(&static_meta,
            MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION, mActiveArray) ) {
            MY_LOGD_IF(mLogLevel > 1, "active array(%d, %d, %dx%d)",
                    mActiveArray.p.x, mActiveArray.p.y,
                    mActiveArray.s.w, mActiveArray.s.h);
        } else {
            MY_LOGE("no static info: MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION");
            #ifdef USING_MTK_LDVT
            mActiveArray = MRect(mSensorParams.size.w, mSensorParams.size.h);
            MY_LOGI("set sensor size to active array(%d, %d, %dx%d)",
                    mActiveArray.p.x, mActiveArray.p.y,
                    mActiveArray.s.w, mActiveArray.s.h);
            #else
            return UNKNOWN_ERROR;
            #endif
        }
    }
    //
    //MY_LOGI("CAM_LOG_LEVEL %d", CAM_LOG_LEVEL);
    #if (IS_P1_LOGI)
    {
        android::String8 strInfo("");
        strInfo += String8::format("Cam::%d ", getOpenId());
        //
        strInfo += String8::format("Param[N:p%d,t%d,b%d,i%d,r%d,v%d_"
            "B:p%d,b%d,t%d,h%d,u%d,e%d,l%d,r%d,d%d,c%d,f%d,v%d,s%d] ",
            // Param-iNt/eNum
            rParams.pipeBit, rParams.tgNum, rParams.burstNum,
            rParams.initRequest, rParams.receiveMode, EisInfo::getEISMode(mPackedEisInfo),
            // Param-Bool
            rParams.rawProcessed, rParams.disableFrontalBinning,
            rParams.disableDynamicTwin, rParams.disableHLR, rParams.enableUNI,
            rParams.enableEIS, rParams.enableLCS, rParams.enableRSS,
            rParams.enableDualPD, rParams.enableCaptureFlow,
            rParams.enableFrameSync, rParams.forceSetEIS, rParams.stereoCamMode
            );
        //
        strInfo += String8::format("S(%d,%d,%d,%d,%dx%d) ",
            mSensorParams.mode, mSensorParams.fps, mSensorParams.pixelMode,
            mSensorParams.vhdrMode, mSensorParams.size.w, mSensorParams.size.h);
        strInfo += String8::format("R(0x%x,%d,%d,%d-0x%x) ",
            mRawFormat, mRawStride, mRawLength, mRawDefType, mRawOption);
        strInfo += String8::format("D(b%d,t%d,h%d) ",
            mDisableFrontalBinning, mDisableDynamicTwin, mDisableHLR);
        strInfo += String8::format("E(e%d,l%d,r%d,u%d,d%d,c%d,f%d,s%d) ",
            mEnableEISO, mEnableLCSO, mEnableRSSO, mEnableUniForcedOn,
            mEnableDualPD, mEnableCaptureFlow, mEnableFrameSync, mStereoCamMode);
        strInfo += String8::format("M(p0x%x,t%d,b%d,i%d,r%d,v%d) ",
            mPipeBit, mTgNum, mBurstNum, mInitReqSet, mReceiveMode, EisInfo::getEISMode(mPackedEisInfo));
        strInfo += String8::format("Dm(%d) ", mpDeliverMgr->runningGet());
        strInfo += String8::format("Pool(%p,%p,%p,%p) ",
            (mpStreamPool_full != NULL) ?
            (mpStreamPool_full.get()) : (NULL),
            (mpStreamPool_resizer != NULL) ?
            (mpStreamPool_resizer.get()) : (NULL),
            (mpStreamPool_lcso != NULL) ?
            (mpStreamPool_lcso.get()) : (NULL),
            (mpStreamPool_rsso != NULL) ?
            (mpStreamPool_rsso.get()) : (NULL));
        strInfo += String8::format(
            "MIA:%#" PRIx64 ",MIH:%#" PRIx64 ","
            "MOA:%#" PRIx64 ",MOH:%#" PRIx64 " ",
            (mvStreamMeta[STREAM_META_IN_APP] == NULL) ? (StreamId_T)(-1) :
            mvStreamMeta[STREAM_META_IN_APP]->getStreamId(),
            (mvStreamMeta[STREAM_META_IN_HAL] == NULL) ? (StreamId_T)(-1) :
            mvStreamMeta[STREAM_META_IN_HAL]->getStreamId(),
            (mvStreamMeta[STREAM_META_OUT_APP] == NULL) ? (StreamId_T)(-1) :
            mvStreamMeta[STREAM_META_OUT_APP]->getStreamId(),
            (mvStreamMeta[STREAM_META_OUT_HAL] == NULL) ? (StreamId_T)(-1) :
            mvStreamMeta[STREAM_META_OUT_HAL]->getStreamId());
        char const * img_name[STREAM_IMG_NUM] =
            {"YUVi", "RAWi", "RAWo", "IMGo", "RRZo", "LCSo", "RSSo"};
        for (int i = STREAM_ITEM_START; i < STREAM_IMG_NUM; i++) {
            if (mvStreamImg[i] != NULL) {
                strInfo += String8::format("%s:%d:%#" PRIx64 "(%dx%d)[0x%x] ",
                    ((img_name[i] != NULL) ? img_name[i] : "UNKNOWN"),
                    i, mvStreamImg[i]->getStreamId(),
                    mvStreamImg[i]->getImgSize().w,
                    mvStreamImg[i]->getImgSize().h,
                    mvStreamImg[i]->getImgFormat());
            };
        }
        //
        strInfo += String8::format("AA(%d,%d-%dx%d) ", mActiveArray.p.x,
            mActiveArray.p.y, mActiveArray.s.w, mActiveArray.s.h);
        //
        MY_LOGI("%s", strInfo.string());
    }
    #endif



    return OK;

}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
config(ConfigParams const& rParams)
{
    PUBLIC_APIS_IN;

    Mutex::Autolock _l(mPublicLock);

    if(getActive()) {
        MY_LOGD("active=%d", getActive());
        onHandleFlush(MFALSE);
    }

    //(1) check
    if (mpTimingCheckerMgr != NULL) {
        mpTimingCheckerMgr->setEnable(MTRUE);
    }
    //
    MERROR err = check_config(rParams);
    if (err != OK) {
        MY_LOGE("Config Param - Check fail (%d)", err);
        return err;
    }

    //(2) configure hardware
    if (mpConCtrl != NULL && (!EN_INIT_REQ)) { // init-request, no aid-start
        mpConCtrl->setAidUsage(MTRUE);
    }
    //
    if (mpTimingCheckerMgr != NULL) {
        mpTimingCheckerMgr->waitReady();
    }
    //
    err = hardwareOps_start();
    if ((!(EN_START_CAP || EN_INIT_REQ)) || (err != OK)) {
        if (mpConCtrl != NULL) {
            mpConCtrl->cleanAidStage();
        }
        if (mpTimingCheckerMgr != NULL) {
            mpTimingCheckerMgr->setEnable(MFALSE);
        }
    }
    if (err != OK) {
        MY_LOGE("Config Param - HW start fail (%d)", err);
        return err;
    }

    PUBLIC_APIS_OUT;

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
queue(
    sp<IPipelineFrame> pFrame
)
{
    PUBLIC_API_IN;
    //
    CAM_TRACE_FMT_BEGIN("P1:queue(%d,%d)",
        P1GET_FRM_NUM(pFrame), P1GET_REQ_NUM(pFrame));
    MY_LOGD2("active=%d", getActive());

    if (!getActive()) {
        MY_LOGI("HW start +++");
        if (mpConCtrl != NULL && (!EN_INIT_REQ)) { // init-request, no aid-start
            mpConCtrl->setAidUsage(MTRUE);
        }
        MERROR err = hardwareOps_start();
        if ((!(EN_START_CAP || EN_INIT_REQ)) || (err != OK)) {
            if (mpConCtrl != NULL) {
                mpConCtrl->cleanAidStage();
            }
            if (mpTimingCheckerMgr != NULL) {
                mpTimingCheckerMgr->setEnable(MFALSE);
            }
        }
        if (err != OK) {
            MY_LOGE("Queue Frame - HW start fail (%d)", err);
            return err;
        }
        MY_LOGI("HW start ---");
    }
    //
    size_t padding = (m3AProcessedDepth * mBurstNum);
    MBOOL isStartSet = MFALSE;
    MINT32 currReqCnt = 0;
    //
    if (EN_INIT_REQ) {
        if (mInitReqCnt <= (mInitReqNum + mBurstNum)) {
            mInitReqCnt ++;
        }
    }
    //
    QueNode_T node; // use only after createNode(..., &node, ...);
    // DO NOT return before createNode(..., Queue, ...)
    receiveFrame(pFrame, node);
    MBOOL isRestreaming = MFALSE;
    if (mpHwStateCtrl != NULL) {
        isRestreaming = mpHwStateCtrl->checkReceiveRestreaming();
    }
    if (isRestreaming) {
        mStandbyLock.lock();
    }
    if (EN_INIT_REQ && mInitReqCnt < mInitReqNum) {
        createNode(pFrame, &node, &mQueueJob, &mProcessingQueue,
            &mProcessingQueueLock, &mControls3AList, &mControls3AListLock);
    //
    // else if (mInitReqCnt == mInitReqNum) go-on the following flow
    } else if (isRevMode(REV_MODE_NORMAL)) {
        MBOOL skipWait = MFALSE;
        if (mpHwStateCtrl != NULL) {
            skipWait = mpHwStateCtrl->checkSkipWait();
            if (skipWait) {
                MY_LOGI("request - SkipWait(%d)", mReceiveMode);
            }
        }
        #if 1// for REV_MODE_NORMAL
        if (mFirstReceived && (!skipWait)) {
            CAM_TRACE_NAME("P1:queue_wait_sync");
            Mutex::Autolock _l(mControls3AListLock);
            mReqState = REQ_STATE_WAITING;
            P1_LOGI(1, "[P1::QUE] (%d,%d) wait[%d]: %zu > %d",
                P1GET_FRM_NUM(pFrame), P1GET_REQ_NUM(pFrame), mReceiveMode,
                mControls3AList.size(), m3AProcessedDepth);
            mControls3AListCond.wait(mControls3AListLock);
            mReqState = REQ_STATE_ONGOING;
        } else {
            mReqState = REQ_STATE_RECEIVE;
            MY_LOGI("the first request received");
        }
        #endif

        {
            Mutex::Autolock _l(mControls3AListLock);
            mReqState = REQ_STATE_RECEIVE;
            //compensate to the number of mProcessedDepth
            #if 1// for REV_MODE_NORMAL
            padding--;
            #endif
            while (mControls3AList.size() < padding
                && mQueueJob.mSet.empty())
            {
                createNode(&mControls3AList, NULL);
            }

            //push node from appFrame
            createNode(pFrame, &node, &mQueueJob, &mRequestQueue,
                &mRequestQueueLock, &mControls3AList, NULL);
            //
            #if 1// for REV_MODE_NORMAL
            mReqState = REQ_STATE_CREATED;
            mControls3AListCond.broadcast();
            #endif
            currReqCnt = android_atomic_inc(&mInFlightRequestCnt);
            ATRACE_INT("P1_request_cnt",
                android_atomic_acquire_load(&mInFlightRequestCnt));

            #if 1// for REV_MODE_NORMAL
            if (!mFirstReceived) {
                Mutex::Autolock _ll(mRequestQueueLock);
                if (!mRequestQueue.empty()) {
                    mFirstReceived = MTRUE;
                    isStartSet = MTRUE;
                    MY_LOGI1("[%s#%d] start to set ReqQ[%zu] List[%zu]",
                        __FUNCTION__, __LINE__,
                        mRequestQueue.size(), mControls3AList.size());
                }
            }
            #endif
            //
            MERROR err = hardwareOps_standby(&mControls3AList, NULL);
            if (err != OK) {
                MY_LOGE("Standy-Fail @ N (%d)", err);
                if (isRestreaming) {
                    mStandbyLock.unlock();
                }
                return err;
            }
        }
    } else if (isRevMode(REV_MODE_AGGRESSIVE)) {

        {
            Mutex::Autolock _l(mControls3AListLock);
            mReqState = REQ_STATE_RECEIVE;
            //compensate to the number of mProcessedDepth
            #if 1// for REV_MODE_AGGRESSIVE
            padding--;
            #endif
            while (mControls3AList.size() < padding
                && mQueueJob.mSet.empty())
            {
                createNode(&mControls3AList, NULL);
            }

            //push node from appFrame
            createNode(pFrame, &node, &mQueueJob, &mRequestQueue,
                &mRequestQueueLock, &mControls3AList, NULL);
            //
            #if 1// for REV_MODE_AGGRESSIVE
            mReqState = REQ_STATE_CREATED;
            mControls3AListCond.broadcast();
            #endif
            currReqCnt = android_atomic_inc(&mInFlightRequestCnt);
            ATRACE_INT("P1_request_cnt",
                android_atomic_acquire_load(&mInFlightRequestCnt));

            if (!mFirstReceived) {
                Mutex::Autolock _ll(mRequestQueueLock);
                if (!mRequestQueue.empty()) {
                    mFirstReceived = MTRUE;
                    isStartSet = MTRUE;
                    MY_LOGI1("[%s#%d] start to set ReqQ[%zu] List[%zu]",
                        __FUNCTION__, __LINE__,
                        mRequestQueue.size(), mControls3AList.size());
                }
            }
            //
            MERROR err = hardwareOps_standby(&mControls3AList, NULL);
            if (err != OK) {
                MY_LOGE("Standy-Fail @ A (%d)", err);
                if (isRestreaming) {
                    mStandbyLock.unlock();
                }
                return err;
            }
        }

        #if 1// for REV_MODE_AGGRESSIVE
        MBOOL skipBlock = MFALSE;
        if (mpHwStateCtrl != NULL) {
            skipBlock = mpHwStateCtrl->checkSkipBlock();
        }
        if (skipBlock) {
            MY_LOGI("request - skip block");
        } else if (isRestreaming) {
            MY_LOGI("request - Restreaming");
        } else if (!isStartSet) {
            CAM_TRACE_NAME("P1:queue_wait_send");
            Mutex::Autolock _l(mControls3AListLock);
            mReqState = REQ_STATE_WAITING;
            P1_LOGI(1, "[P1::QUE] (%d,%d) wait[%d]: %zu > %d",
                P1GET_FRM_NUM(pFrame), P1GET_REQ_NUM(pFrame), mReceiveMode,
                mControls3AList.size(), m3AProcessedDepth);
            mControls3AListCond.wait(mControls3AListLock);
            mReqState = REQ_STATE_ONGOING;
        } else {
            MY_LOGI("the first request received");
        }
        #endif
    } else { // REV_MODE_CONSERVATIVE

        {
            Mutex::Autolock _l(mControls3AListLock);
            MBOOL needRevReq = MFALSE; // it need to receive request forcibly
            if (EN_INIT_REQ && (mInitReqCnt <= (mInitReqNum + mBurstNum))) {
                MY_LOGI("RevInitReq(%d/%d)", mInitReqCnt, mInitReqNum);
                needRevReq = MTRUE;
            } else if (mpHwStateCtrl != NULL) {
                needRevReq = mpHwStateCtrl->checkSkipWait();
                if (needRevReq) {
                    MY_LOGI("request - SkipWait(%d)[%zu]", mReceiveMode,
                        mControls3AList.size());
                }
            }
            //
            #if 1// for REV_MODE_CONSERVATIVE
            //block condition 1: if pipeline is full
            while ((!needRevReq) && (mControls3AList.size() >
                (size_t)(((m3AProcessedDepth + 1) * mBurstNum) - 1)))
            {
                CAM_TRACE_NAME("P1:queue_wait_size");
                P1_LOGI(1, "[P1::QUE] (%d,%d) wait[%d]: %zu > %d",
                    P1GET_FRM_NUM(pFrame), P1GET_REQ_NUM(pFrame), mReceiveMode,
                    mControls3AList.size(), m3AProcessedDepth);
                status_t status = mControls3AListCond.wait(mControls3AListLock);
                MY_LOGD2("wait-");
                if  ( OK != status ) {
                    MY_LOGW(
                        "wait status:%d:%s, mControls3AList.size:%zu",
                        status, ::strerror(-status), mControls3AList.size()
                    );
                }
            }
            #endif
            //compensate to the number of mProcessedDepth
            #if 1// for REV_MODE_CONSERVATIVE
            if ((EN_START_CAP || EN_INIT_REQ) && (!mFirstReceived)) {
                padding --;
            }
            #endif
            while (mControls3AList.size() < padding
                && mQueueJob.mSet.empty())
            {
                createNode(&mControls3AList, NULL);
            }

            //push node from appFrame
            createNode(pFrame, &node, &mQueueJob, &mRequestQueue,
                &mRequestQueueLock, &mControls3AList, NULL);
            currReqCnt = android_atomic_inc(&mInFlightRequestCnt);
            ATRACE_INT("P1_request_cnt",
                android_atomic_acquire_load(&mInFlightRequestCnt));

            if (!mFirstReceived) {
                Mutex::Autolock _ll(mRequestQueueLock);
                if (!mRequestQueue.empty()) {
                    mFirstReceived = MTRUE;
                    isStartSet = MTRUE;
                    MY_LOGI1("[%s#%d] start to set ReqQ[%zu] List[%zu]",
                        __FUNCTION__, __LINE__,
                        mRequestQueue.size(), mControls3AList.size());
                }
            }
            //
            MERROR err = hardwareOps_standby(&mControls3AList, NULL);
            if (err != OK) {
                MY_LOGE("Standy-Fail @ C (%d)", err);
                if (isRestreaming) {
                    mStandbyLock.unlock();
                }
                return err;
            }
        }
    }
    //
    MY_LOGD3("InFlightRequestCount++ (%d) => (%d)", currReqCnt,
        android_atomic_acquire_load(&mInFlightRequestCnt));
    //
    #if 1 //SET_REQUEST_BEFORE_FIRST_DONE
    if (isStartSet) {
        if (EN_INIT_REQ && (!getReady())) {
            MY_LOGI("HW request +++");
            MERROR err = hardwareOps_request();
            if (mpConCtrl != NULL) {
                mpConCtrl->cleanAidStage();
            }
            if (mpTimingCheckerMgr != NULL) {
                mpTimingCheckerMgr->setEnable(MFALSE);
            }
            if (err != OK) {
                MY_LOGE("Queue Frame - HW request fail (%d)", err);
                if (isRestreaming) {
                    mStandbyLock.unlock();
                }
                return err;
            }
            MY_LOGI("HW request ---");
        } else if (EN_START_CAP && (!getReady())) {
            MY_LOGI("HW capture +++");
            MERROR err = hardwareOps_capture();
            if (mpConCtrl != NULL) {
                mpConCtrl->cleanAidStage();
            }
            if (mpTimingCheckerMgr != NULL) {
                mpTimingCheckerMgr->setEnable(MFALSE);
            }
            if (err != OK) {
                MY_LOGE("Queue Frame - HW capture fail (%d)", err);
                if (isRestreaming) {
                    mStandbyLock.unlock();
                }
                return err;
            }
            MY_LOGI("HW capture ---");
        } else {
            onRequestFrameSet(MTRUE);
        }
    }
    #endif
    //
    #if 1// for REV_MODE_AGGRESSIVE
    if (isStartSet && (!EN_START_CAP) && (!isRestreaming)) {
        if (isRevMode(REV_MODE_AGGRESSIVE)) {
            CAM_TRACE_NAME("P1:queue_wait_send_first");
            Mutex::Autolock _l(mControls3AListLock);
            mReqState = REQ_STATE_WAITING;
            P1_LOGI(1, "[P1::QUE] first-wait[%d]: %zu > %d", mReceiveMode,
                mControls3AList.size(), m3AProcessedDepth);
            mControls3AListCond.wait(mControls3AListLock);
            mReqState = REQ_STATE_ONGOING;
        }
    }
    #endif
    //
    if (mpHwStateCtrl != NULL) {
        mpHwStateCtrl->checkRequest();
    }
    //
    if (isRestreaming) {
        mStandbyLock.unlock();
    }
    CAM_TRACE_FMT_END();
    //
    PUBLIC_API_OUT;
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
flush(android::sp<IPipelineFrame> const &pFrame)
{
    return BaseNode::flush(pFrame);
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
flush()
{
    PUBLIC_APIS_IN;

    CAM_TRACE_NAME("P1:flush");

    Mutex::Autolock _l(mPublicLock);

    onHandleFlush(MFALSE);

    //[TODO]
    //wait until deque thread going back to waiting state;
    //in case next node receives queue() after flush()

    PUBLIC_APIS_OUT;

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
void
P1NodeImp::
requestExit()
{
    FUNCTION_P1_IN;

    //let deque thread back
    Thread::requestExit();
    {
        Mutex::Autolock _l(mThreadLock);
        mThreadCond.broadcast();
    }
    //
    join();

    //let enque thread back
    Mutex::Autolock _l(mControls3AListLock);
    mControls3AListCond.broadcast();

    FUNCTION_P1_OUT;
}


/******************************************************************************
 *
 ******************************************************************************/
status_t
P1NodeImp::
readyToRun()
{
    // set name
    ::prctl(PR_SET_NAME, (unsigned long)"Cam@P1NodeImp", 0, 0, 0);

    // set normal
    struct sched_param sched_p;
    sched_p.sched_priority = 0;
    ::sched_setscheduler(0, P1THREAD_POLICY, &sched_p);
    ::setpriority(PRIO_PROCESS, 0, P1THREAD_PRIORITY);   //  Note: "priority" is nice value.
    //
    ::sched_getparam(0, &sched_p);
    MY_LOGD(
        "Tid: %d, policy: %d, priority: %d"
        , ::gettid(), ::sched_getscheduler(0)
        , sched_p.sched_priority
    );

    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
bool
P1NodeImp::
threadLoop()
{
    // check if going to leave thread
    CAM_TRACE_NAME("P1:threadLoop");
    //
    {
        Mutex::Autolock _l(mThreadLock);

        if (!getActive()) {
            CAM_TRACE_NAME("P1:wait_active");
            MY_LOGD("wait active+");
            mThreadCond.wait(mThreadLock);
            MY_LOGD("wait active-");
        }

        if (exitPending()) {
            MY_LOGD2("leaving active");
            return false;
        }
    }
    //
    if (mpConCtrl != NULL && mpConCtrl->getAidUsage()) {
        procedureAid_start();
    }
    //
    {
        Mutex::Autolock _l(mThreadLock);

        if (getActive() && !getReady()) {
            CAM_TRACE_NAME("P1:wait_ready");
            MY_LOGD("wait ready+");
            mThreadCond.wait(mThreadLock);
            MY_LOGD("wait ready-");
        }

        if (exitPending()) {
            MY_LOGD1("leaving ready");
            return false;
        }
    }

    if (mpHwStateCtrl != NULL) {
        mpHwStateCtrl->checkThreadStandby();
    }

    // deque buffer, and handle frame and metadata
    onProcessDequeFrame();

    if (!getActive()) {
        MY_LOGI_IF(getInit(), "HW stopped , exit init");
        setInit(MFALSE);
    }

    // trigger point for the first time
    {
        if (getInit()) {
            setInit(MFALSE);
        }
    }

    if ((mpDeliverMgr != NULL) && (mpDeliverMgr->runningGet())) {
        onProcessDropFrame(MTRUE);
    };

    return true;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
setActive(
    MBOOL active
)
{
    Mutex::Autolock _l(mActiveLock);
    mActive = active;
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
P1NodeImp::
getActive(
    void
)
{
    Mutex::Autolock _l(mActiveLock);
    return mActive;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
setReady(
    MBOOL ready
)
{
    Mutex::Autolock _l(mReadyLock);
    mReady = ready;
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
P1NodeImp::
getReady(
    void
)
{
    Mutex::Autolock _l(mReadyLock);
    return mReady;
}



/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
setInit(
    MBOOL init
)
{
    Mutex::Autolock _l(mInitLock);
    mInit = init;
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
P1NodeImp::
getInit(
    void
)
{
    Mutex::Autolock _l(mInitLock);
    return mInit;
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
onRequestFrameSet(
    MBOOL initial
)
{
    FUNCTION_P1_IN;
    //
    Mutex::Autolock _ll(mFrameSetLock);
    if (!initial && !mFrameSetAlready) {
        MY_LOGI("frame set not init complete");
        return;
    }
    //
    size_t size = 0;
    size_t index = (m3AProcessedDepth - 1) * mBurstNum;
    size_t depth = m3AProcessedDepth * mBurstNum;
    MBOOL set = MFALSE;
    //
    #if (IS_P1_LOGI)
    String8 str("");
    #endif
    //
    if (isRevMode(REV_MODE_NORMAL) || isRevMode(REV_MODE_AGGRESSIVE)) {
        MBOOL isSetWaiting = MFALSE;
        if (!initial) {
            {
                CAM_TRACE_NAME("ReqSet_GetListLock_E");
                Mutex::Autolock _l(mControls3AListLock);
                size_t idx = index;
                MINT32 curNum = 0;
                if (mControls3AList.size() > index) {
                    List<MetaSet_T>::iterator it = mControls3AList.begin();
                    for (; it != mControls3AList.end(); it++) {
                        if (idx-- == 0) {
                            curNum = it->MagicNum;
                            if (1 == it->Dummy) {
                                P1_LOGI(1, "Check-Padding (%d)", curNum);
                                curNum = 0;
                            }
                            break;
                        }
                    }
                }
                //
                if (EN_INIT_REQ && mInitSetNum != 0 &&
                    mLastSetNum == mInitSetNum) {
                    while (mControls3AList.size() > index) { // && (index >= 0)
                        mControls3AList.erase(mControls3AList.begin());
                    }
                    mInitSetNum = 0; // reset InitSetNum
                } else if (mLastSetNum >= curNum) {
                    for (MUINT8 n = 0; (n < mBurstNum) &&
                        (!mControls3AList.empty()); n++) {
                        mControls3AList.erase(mControls3AList.begin());
                    }
                } else {
                    P1_LOGI(1, "Check-CtrlList (%d < %d)", mLastSetNum, curNum);
                }
                mTagList.set(mControls3AList.size());
                if (mControls3AList.size() < depth) {
                    if (mReqState == REQ_STATE_WAITING) {
                        isSetWaiting = MTRUE;
                    }
                    mControls3AListCond.broadcast();
                } else {
                    P1_LOGI(1, "Check-Request (%d) (%zu)", mLastSetNum, depth);
                }
            }
            //
            if (isSetWaiting) {
                CAM_TRACE_NAME("ReqSet_GetListLock_W");
                Mutex::Autolock _l(mControls3AListLock);
                if ((mReqState != REQ_STATE_CREATED) &&
                    (mControls3AList.size() < depth)) {
                    mControls3AListCond.wait(mControls3AListLock);
                }
            }
        }
    } else { // REV_MODE_CONSERVATIVE
        CAM_TRACE_NAME("ReqSet_GetListLock_B");
        Mutex::Autolock _l(mControls3AListLock);
        size_t idx = index;
        MINT32 curNum = 0;
        if (mControls3AList.size() > index) {
            List<MetaSet_T>::iterator it = mControls3AList.begin();
            for (; it != mControls3AList.end(); it++) {
                if (idx-- == 0) {
                    curNum = it->MagicNum;
                    if (1 == it->Dummy) {
                        P1_LOGI(1, "Check-Padding (%d)", curNum);
                        curNum = 0;
                    }
                    break;
                }
            }
        }
        //
        if (EN_INIT_REQ && mInitSetNum != 0 &&
            mLastSetNum == mInitSetNum) {
            for (MUINT i = 0; (i < mInitReqPad) &&
                (!mControls3AList.empty()); i++) {
                mControls3AList.erase(mControls3AList.begin());
            }
            mInitSetNum = 0; // reset InitSetNum
        } else if (mLastSetNum >= curNum) {
            for (MUINT8 n = 0; (n < mBurstNum) &&
                (!mControls3AList.empty()); n++) {
                mControls3AList.erase(mControls3AList.begin());
            }
        } else {
            P1_LOGI(1, "Check-CtrlList (%d < %d)", mLastSetNum, curNum);
        }
        mTagList.set(mControls3AList.size());
        mControls3AListCond.broadcast();
    }
    // check control list
    {
        Mutex::Autolock _l(mControls3AListLock);
        MINT32 num = 0;
        size_t idx = 0;
        size = mControls3AList.size();
        List<MetaSet_T>::iterator it = mControls3AList.begin();
        for (; it != mControls3AList.end(); it++) {
            num = it->MagicNum;
            #if (IS_P1_LOGI)
            if (mLogLevelI > 0) {
                if ((idx > 0) && (idx % mBurstNum == 0)) {
                    str += String8::format(", ");
                }
                str += String8::format("%d ", num);
            }
            #endif
            if ((idx == index) && (size >= depth)) {
                mLastSetNum = num;
            }
            if (idx == (depth - 1)) {
                mTagSet.set(num);
            }
            idx++;
        }
        if (size < depth) {
            P1_LOGI(1, "Check-Size (%zu < %zu)", size, depth);
            #if SUPPORT_3A
            if (mp3A) {
                CAM_TRACE_BEGIN("P1:3A-set");
                mp3A->set(mControls3AList);
                CAM_TRACE_END();
            }
            #endif
        } else {
            set = MTRUE;
        }
    }
    // call 3A.set
    if (set) {
        #if SUPPORT_3A
        if (mp3A) {
            CAM_TRACE_FMT_BEGIN("P1:3A-set(%d)", mLastSetNum);
            mp3A->set(mControls3AList);
            CAM_TRACE_FMT_END();
        }
        #endif
    }
    //
    mFrameSetAlready = MTRUE;
    //dump
#if (IS_P1_LOGI)
    if (mLogLevelI > 0) {
        P1_LOGI(1, "[P1::SET] (%d)(%d) Ctrl3AList[%zu] = [%s]", mLastSetNum,
            set, size, str.string());
    }
#endif
    //
    FUNCTION_P1_OUT;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
onSyncEnd()
{
    FUNCTION_P1_IN;
    //
    MBOOL toSet = MFALSE;
    if (mpHwStateCtrl != NULL) {
        if (mpHwStateCtrl->checkSkipSync()) {
            MY_LOGI("SyncEND was paused");
            return;
        }
        MBOOL first = mpHwStateCtrl->checkFirstSync();
        MY_LOGI_IF(first, "Got first CB after re-streaming");
        if (first && isRevMode(REV_MODE_CONSERVATIVE)) {
            toSet = MTRUE;
        }
    }
    //
    if (EN_START_CAP && (!getReady())) {
        Mutex::Autolock _l(mStartCaptureLock);
        MY_LOGD2("StartCaptureState(%d)", mStartCaptureState);
        if (mStartCaptureState != START_CAP_STATE_READY) {
            MY_LOGI("should not callback before capture ready (%d)",
                mStartCaptureState);
            return;
        }
    }
    //
    if (getInit()) {
        MY_LOGI("sync before buffer done");
    }
    {
        Mutex::Autolock _ll(mFrameSetLock);
        if (!mFrameSetAlready) {
            MY_LOGI("should not callback before first set");
            return;
        }
    }
    //
    CAM_TRACE_FMT_BEGIN("onSyncEnd (%d)", mLastSetNum);
    //
    if (isRevMode(REV_MODE_NORMAL) || isRevMode(REV_MODE_AGGRESSIVE) || toSet) {
        onRequestFrameSet(MFALSE);
    }
    //
    CAM_TRACE_FMT_END();
    //
    FUNCTION_P1_OUT;
    return;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
onSyncBegin(
    MBOOL initial,
    RequestSet_T* reqSet,//MUINT32 magicNum,
    MUINT32 sofIdx,
    CapParam_T* capParam
)
{
    FUNCTION_P1_IN;
    if (mpHwStateCtrl != NULL) {
        if (mpHwStateCtrl->checkSkipSync()) {
            MY_LOGI("SyncBGN was paused");
            return;
        }
        MBOOL first = mpHwStateCtrl->checkFirstSync();
        MY_LOGI_IF(first, "Got first CB after re-streaming");
    }
    //
    if (EN_START_CAP && (!getReady())) {
        Mutex::Autolock _l(mStartCaptureLock);
        MY_LOGD2("StartCaptureState(%d)", mStartCaptureState);
        if (mStartCaptureState == START_CAP_STATE_WAIT_CB) {
            if (capParam != NULL) {
                mStartCaptureType = capParam->u4CapType;
                mStartCaptureIdx = sofIdx;
                mStartCaptureExp = MAX(capParam->i8ExposureTime, 0);
                if (reqSet != NULL && reqSet->vNumberSet.size() > 0 &&
                    mBurstNum <= 1) {
                    mLongExp.set(reqSet->vNumberSet[0], mStartCaptureExp);
                }
            }
            mStartCaptureState = START_CAP_STATE_READY;
            mStartCaptureCond.broadcast();
            MY_LOGI1("StartCaptureReady @%d - init(%d) CapType(%d) CapIdx(%d) "
                "CapExp(%" PRId64 ")", sofIdx, getInit(),
                mStartCaptureType, mStartCaptureIdx, mStartCaptureExp);
            return;
        } else if (mStartCaptureState == START_CAP_STATE_WAIT_REQ) {
            MY_LOGI("should not callback before capture set (%d)",
                mStartCaptureState);
            return;
        }
    }
    //
    if (getInit()) {
        MY_LOGI("sync before buffer done");
    }
    {
        Mutex::Autolock _ll(mFrameSetLock);
        if (!mFrameSetAlready) {
            MY_LOGI("should not callback before first set");
            return;
        }
    }
    //
    MINT32 magicNum = 0;
    if (reqSet != NULL && reqSet->vNumberSet.size() > 0) {
        magicNum = reqSet->vNumberSet[0];
    }
    //
    CAM_TRACE_FMT_BEGIN("onSyncBegin (%d)", magicNum);
    //
    //(1)
    if((!initial) && (getReady())) { // [TODO] && VALID 3A PROCESSED NOTIFY
        QueJob_T job(mBurstNum);
        bool exist = false;
        {
            Mutex::Autolock _l(mRequestQueueLock);
            Que_T::iterator it = mRequestQueue.begin();
            for(; it != mRequestQueue.end(); it++) {
                if ((*it).mFirstMagicNum == magicNum) {
                    job = *it;
                    for (MUINT8 i = 0; i < job.mSet.size(); i++) {
                        job.mSet.editItemAt(i).sofIdx = sofIdx;
                        if (capParam != NULL) {
                            job.mSet.editItemAt(i).capType =
                                capParam->u4CapType;
                            job.mSet.editItemAt(i).frameExpDuration =
                                MAX(capParam->i8ExposureTime, 0);
                            if (mBurstNum <= 1) {
                                mLongExp.set(job.mSet[i].magicNum,
                                    job.mSet[i].frameExpDuration);
                            }
                            MY_LOGI_IF(((capParam->i8ExposureTime >= 400000000)
                                || (capParam->i8ExposureTime <= 0)),
                                "check CB num(%d) cap(%d) exp(%" PRId64 ")", magicNum,
                                capParam->u4CapType, capParam->i8ExposureTime);
                            if ((job.mSet[i].capType != E_CAPTURE_NORMAL) &&
                                (job.mSet[i].appFrame != NULL)) {
                                MY_LOGI_IF((mLogLevel > 1),
                                    "Job(%d) - Cap(%d)(%" PRId64 ") - "
                                    P1INFO_NODE_STR,
                                    job.mFirstMagicNum,
                                    capParam->u4CapType,
                                    capParam->i8ExposureTime,
                                    P1INFO_NODE_VAR(job.mSet[i]));
                            }
                        } else {
                            MY_LOGW("cannot find cap param (%d)", magicNum);
                        }
                    }
                    //
                    if (it != mRequestQueue.begin()) {
                        String8 str;
                        str += String8::format("MissingCallback from 3A : "
                            "this CB Mnum(%d) ; current ReqQ[%d] = [ ",
                            magicNum, (int)(mRequestQueue.size()));
                        Que_T::iterator it = mRequestQueue.begin();
                        for(; it != mRequestQueue.end(); it++) {
                            str += String8::format("%d ", (*it).mFirstMagicNum);
                        }
                        str += String8::format("] @ SOF(%d)", sofIdx);
                        MY_LOGW("%s", str.string());
                    }
                    //
                    mRequestQueue.erase(it);
                    exist = true;
                    break;
                }
            }
        }
        if (exist) {
            if (OK != onProcessEnqueFrame(job)) {
                MY_LOGE("frame en-queue fail (%d)", magicNum);
                for (MUINT8 i = 0; i < job.mSet.size(); i++) {
                    onReturnFrame(job.mSet.editItemAt(i), MTRUE, MTRUE);
                }
            } else {
                if ((mBurstNum <= 1) && // exclude burst mode
                    (job.mSet.size() == 1 && job.mSet[0].appFrame != NULL) &&
                    (capParam != NULL && capParam->metadata.count() > 0)) {
                    requestMetadataEarlyCallback(job.mSet[0], // const node
                        STREAM_META_OUT_HAL, &(capParam->metadata));
                }
                //
                if (mpHwStateCtrl != NULL) {
                    MBOOL isAct = MFALSE;
                    isAct = mpHwStateCtrl->checkSetNum(job.mFirstMagicNum);
                    // it might call doNotifyDropframe() in DRV->suspend()
                    if ((isAct) && ((mpDeliverMgr != NULL) &&
                        (mpDeliverMgr->runningGet()))) {
                        MY_LOGI("DRV-suspend executed : check drop-frame");
                        onProcessDropFrame(MTRUE);
                    };
                }
            }
        } else {
            //MY_LOGW_IF(magicNum!=0, "no: %d", magicNum);
            #if (IS_P1_LOGI)
            Mutex::Autolock _l(mRequestQueueLock);
            String8 str;
            str += String8::format("[req(%d)/size(%d)]: ",
                magicNum, (int)(mRequestQueue.size()));
            Que_T::iterator it = mRequestQueue.begin();
            for(; it != mRequestQueue.end(); it++) {
                str += String8::format("%d ", (*it).mFirstMagicNum);
            }
            MY_LOGI("%s", str.string());
            #endif
        }
    }
    //
    if (isRevMode(REV_MODE_CONSERVATIVE)) {
        MBOOL skip = MFALSE;
        if (mpHwStateCtrl != NULL) {
            skip = mpHwStateCtrl->checkSkipSync();
        }
        if (skip) {
            MY_LOGI("FrameSet was paused");
        } else {
            onRequestFrameSet(MFALSE);
        }
    }
    //
    CAM_TRACE_FMT_END();
    //
    FUNCTION_P1_OUT;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
onProcessEnqueFrame(
    QueJob_T &job
)
{
    FUNCTION_P1_IN;

    //(1)
    //pass request directly if it's a reprocessing one
    //[TODO]
    //if( mInHalMeta == NULL) {
    //    onDispatchFrame(pFrame);
    //    return;
    //}

    //(2)
    MERROR status = hardwareOps_enque(job);

    FUNCTION_P1_OUT;
    return status;
}


/******************************************************************************
 *
 ******************************************************************************/
P1NodeImp::QueJob_T
P1NodeImp::
getProcessingFrame_ByNumber(MINT32 magicNum)
{
    FUNCTION_P1_IN;
    QueJob_T job(mBurstNum);

    Mutex::Autolock _l(mProcessingQueueLock);
    if (mProcessingQueue.empty()) {
        MY_LOGE("mProcessingQueue is empty");
        return job;
    }

    #if 1
        Que_T::iterator it = mProcessingQueue.begin();
        for (; it != mProcessingQueue.end(); it++) {
            if ((*it).mFirstMagicNum == magicNum) {
                break;
            }
        }
        if (it == mProcessingQueue.end()) {
            MY_LOGI("cannot find the right node for num: %d", magicNum);
            job.mSet.clear();
            return job;
        }
        else {
            job = *it;
            mProcessingQueue.erase(it);
            mProcessingQueueCond.broadcast();
        }
    #else
        job = *mProcessingQueue.begin();
        mProcessingQueue.erase(mProcessingQueue.begin());
        mProcessingQueueCond.broadcast();
    #endif

    FUNCTION_P1_OUT;
    //
    return job;
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
P1NodeImp::
getProcessingFrame_ByAddr(IImageBuffer* const imgBuffer,
                          MINT32 magicNum,
                          QueJob_T &job
)
{
    FUNCTION_P1_IN;

    MBOOL ret = MFALSE;
    if (imgBuffer == NULL) {
        MY_LOGE("imgBuffer == NULL");
        return ret;
    }

    // get the right node from mProcessingQueue
    mProcessingQueueLock.lock();
    if (mProcessingQueue.empty()) {
        MY_LOGE("ProQ is empty");
        mProcessingQueueLock.unlock();
        return ret;
    }
    //
    MINT32 gotNum = 0;
    Vector<MINT32> vStoreNum;
    vStoreNum.clear();
    Que_T::iterator it = mProcessingQueue.begin();
    for (; it != mProcessingQueue.end(); it++) {
        if (imgBuffer ==
            (*it).mSet[0].streamBufImg[STREAM_IMG_OUT_FULL].spImgBuf.get() ||
            imgBuffer ==
            (*it).mSet[0].streamBufImg[STREAM_IMG_OUT_OPAQUE].spImgBuf.get() ||
            imgBuffer ==
            (*it).mSet[0].streamBufImg[STREAM_IMG_OUT_RESIZE].spImgBuf.get() ||
            imgBuffer ==
            (*it).mSet[0].streamBufImg[STREAM_IMG_OUT_LCS].spImgBuf.get() ||
            imgBuffer ==
            (*it).mSet[0].streamBufImg[STREAM_IMG_OUT_RSS].spImgBuf.get()) {
            gotNum = (*it).mFirstMagicNum;
            if ((*it).mFirstMagicNum == magicNum) {
                ret = MTRUE;
            } else {
                #if SUPPORT_PERFRAME_CTRL
                MY_LOGE("magicNum from driver(%d), should(%d)",
                       magicNum, (*it).mFirstMagicNum);
                #else
                if((magicNum & 0x40000000) != 0) {
                    MY_LOGW("magicNum from driver(0x%x) is uncertain",
                          magicNum);
                    ret = MFALSE;
                } else {
                    ret = MTRUE;
                    MY_LOGW("magicNum from driver(%d), should(%d)",
                          magicNum, (*it).mFirstMagicNum);
                }
                #endif
                // reset node from 3A info
                for (size_t i = 0; i < (*it).mSet.size(); i++) {
                    (*it).mSet.editItemAt(i).capType = E_CAPTURE_NORMAL;
                    (*it).mSet.editItemAt(i).frameExpDuration = 0;
                }
            }
            break;
        } else {
            continue;
        }
    }
    //
    if (it == mProcessingQueue.end()) {
        MY_LOGE("no node with imagebuf(%p), PA(0x%zx), num(%d)",
                 imgBuffer, imgBuffer->getBufPA(0), magicNum);
        #if 1 // dump ProcessingQ info
        char const * str[STREAM_IMG_NUM] =
            {"YUV-in", "RAW-in", "OPQ", "IMG", "RRZ", "LCS", "RSS"};
        for (Que_T::iterator j = mProcessingQueue.begin();
            j != mProcessingQueue.end(); j++) {
            for (size_t i = 0; i < (*j).mSet.size(); i++) {
                MY_LOGW("[ProQ] [%zu] : num(%d)", i, (*j).mSet[i].magicNum);
                for (int s = STREAM_ITEM_START; s < STREAM_IMG_NUM; s++) {
                    if ((*j).mSet[i].streamBufImg[s].bExist &&
                        (*j).mSet[i].streamBufImg[s].spImgBuf != NULL) {
                        sp<IImageBuffer> pBuf =
                            (*j).mSet[i].streamBufImg[s].spImgBuf;
                        MY_LOGW("[ProQ] [%zu] : %s(%p)(P:0x%zx)(V:0x%zx)", i,
                            ((str[s] != NULL) ? str[s] : "UNKNOWN"),
                            pBuf.get(), pBuf->getBufPA(0), pBuf->getBufVA(0));
                    };
                }
            }
        }
        #endif
    } else {
        //
        if (it != mProcessingQueue.begin()) {
            size_t queSize = mProcessingQueue.size();
            MINT queNum = 0;
            Que_T::iterator it_stored = mProcessingQueue.begin();
            for (; it_stored < it; it_stored ++) {
                for (size_t i = 0; i < (*it_stored).mSet.size(); i++) {
                    queNum = ((*it_stored).mSet[i].magicNum);
                    vStoreNum.push_back(queNum);
                    MY_LOGI("ProcQue[%zu] (%d<%d)", queSize, queNum, gotNum);
                }
            }
        }
        job = *it;
        mProcessingQueue.erase(it);
        mProcessingQueueCond.broadcast();
        MY_LOGD2("magic: %d", magicNum);
    }
    mProcessingQueueLock.unlock();
    // avoid to execute mpHwStateCtrl functions under mProcessingQueueLock
    if (mpHwStateCtrl != NULL) {
        if (gotNum > 0 && mpHwStateCtrl->checkDoneNum(gotNum)) {
            for (size_t idx = 0; idx < vStoreNum.size(); idx++) {
                MY_LOGI("DropStoreNum[%zu] : %d", idx, vStoreNum[idx]);
                mpHwStateCtrl->setDropNum(vStoreNum[idx]);
            }
        }
    }
    //
    FUNCTION_P1_OUT;
    //
    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
onCheckDropFrame(void)
{
    MUINT cnt = 0;
    MINT32 num = 0;
    if (mpHwStateCtrl != NULL) {
        do
        {
            num = mpHwStateCtrl->getDropNum();
            if (num > 0) {
                Mutex::Autolock _l(mDropQueueLock);
                mDropQueue.push_back(num);
                cnt ++;
            }
        } while (num > 0);
    }
    if ((cnt > 0) && (mpDeliverMgr != NULL) && (mpDeliverMgr->runningGet())) {
        MY_LOGI("check drop frame (%d)", cnt);
        onProcessDropFrame(MTRUE);
    };
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
onProcessDropFrame(MBOOL isTrigger)
{
    mDropQueueLock.lock();
    if (mDropQueue.empty()) {
        mDropQueueLock.unlock();
        return OK;
    }
    Vector<QueNode_T > nodeQ;
    for(size_t i = 0; i < mDropQueue.size(); i++) {
        QueJob_T job = getProcessingFrame_ByNumber(mDropQueue[i]);
        // if getProcessingFrame_ByNumber can not find the job
        // the job set size is 0
        for (MUINT8 i = 0; i < job.mSet.size(); i++) {
            QueNode_T node = job.mSet.editItemAt(i);
            nodeQ.push_back(node);
        }
        MY_LOGI("drop[%zu/%zu]: %d", i, mDropQueue.size(), mDropQueue[i]);
        P1_LOGI(0, "DropQueue[%zu/%zu] = %d",
            i, mDropQueue.size(), mDropQueue[i]);
    }
    mDropQueue.clear();
    mDropQueueLock.unlock();
    //
    for(size_t i = 0; i < nodeQ.size(); i++) {
        //
        if (mBurstNum <= 1) {
            mLongExp.reset(nodeQ[i].magicNum);
        }
        //
        if (IS_LMV(mpConnectLMV) && (nodeQ[i].buffer_eiso != NULL) &&
            getActive()) {
            mpConnectLMV->processDropFrame(nodeQ.editItemAt(i).buffer_eiso);
        }
        nodeQ.editItemAt(i).exeState = EXE_STATE_DONE;
        onReturnFrame(nodeQ.editItemAt(i), MTRUE,
            ((isTrigger) && (i == (nodeQ.size() - 1))) ? MTRUE : MFALSE);
    }

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
onProcessDequeFrame()
{

#if 0
    // [FIXME]  temp-WA for DRV currently not implement self-signal
    //          the dequeue might be blocked while en-queue empty
    //          it should be removed after DRV self-signal ready
    {
        Mutex::Autolock _ll(mProcessingQueueLock);
        if (mProcessingQueue.empty()) {
            return OK;
        }
    }
#endif

    FUNCTION_P1_IN;

    CAM_TRACE_NAME("P1:ProcessDequedFrame");

    MERROR ret= OK;
    QBufInfo deqBuf;
    MetaSet_T result3A;
    if(hardwareOps_deque(deqBuf) != OK) {
        return BAD_VALUE;
    }

    if (deqBuf.mvOut.size() == 0) {
        MBOOL normal_case = (!getActive());
        if ((!normal_case) && (mpHwStateCtrl != NULL)) {
            normal_case = (mpHwStateCtrl->checkBufferState());
        }
        MY_LOGI("DeqBuf Out Size is 0 (act:%d,%d)", getActive(), normal_case);
        return ((normal_case) ? OK : BAD_VALUE);
    }

    MY_LOGI2("HwLockProcessWait +++");
    Mutex::Autolock _l(mHardwareLock);
    MY_LOGI2("HwLockProcessWait ---");

    QueJob_T job(mBurstNum);
    MBOOL match = getProcessingFrame_ByAddr(deqBuf.mvOut[0].mBuffer,
                    deqBuf.mvOut[0].mMetaData.mMagicNum_hal, job);
    onCheckDropFrame(); // must call after getProcessingFrame_ByAddr()
    //
    if (mBurstNum <= 1) {
        mLongExp.reset(deqBuf.mvOut[0].mMetaData.mMagicNum_hal);
    }
    //
    if (!findPortBufIndex(deqBuf, job)) {
        return BAD_VALUE;
    }
    //
    for (MUINT8 i = 0; i < job.mSet.size(); i++) {
        QueNode_T node = job.mSet.editItemAt(i);

        // camera display systrace - DeQ
        if  ( ATRACE_ENABLED() && node.appFrame != 0 )
        {
            MINT64 const timestamp = deqBuf.mvOut[i].mMetaData.mTimeStamp;
            String8 const str = String8::format(
                "Cam:%d:IspP1:deq|timestamp(ns):%" PRId64
                " duration(ns):%" PRId64
                " request:%d frame:%d",
                getOpenId(), timestamp, ::systemTime()-timestamp,
                node.appFrame->getRequestNo(), node.appFrame->getFrameNo()
            );
            CAM_TRACE_BEGIN(str.string());
            CAM_TRACE_END();
        }

        MY_LOGD2("job(%d)[%d] = node(%d)",
            job.mFirstMagicNum, i, node.magicNum);
        mTagDeq.set(node.magicNum);
        #if SUPPORT_3A
        {
            Mutex::Autolock _ssl(mStopSttLock);
            if (getActive() && mp3A && node.reqType == REQ_TYPE_NORMAL) {
                MBOOL drop_notify = MFALSE;
                MINT32 ret = 0;
                mp3A->notifyP1Done(node.magicNum);
                if (match && node.capType == E_CAPTURE_HIGH_QUALITY_CAPTURE) {
                    CAM_TRACE_FMT_BEGIN("P1:3A-getCur(%d,%d)",
                        node.magicNum, node.sofIdx);
                    MY_LOGD1("GetCurrent(%d) +++", node.magicNum);
                    ret = mp3A->getCur(node.magicNum, result3A);
                    if (ret < 0) { // 0:success
                        drop_notify = MTRUE;
                        MY_LOGI("drop-frame by 3A GetC(%d) @ (%d)(%d:%d)", ret,
                            node.magicNum, P1GET_FRM_NUM(node.appFrame),
                            P1GET_REQ_NUM(node.appFrame));
                    }
                    MY_LOGD1("GetCurrent(%d) ---", node.magicNum);
                    CAM_TRACE_FMT_END();
                } else {
                    ret = mp3A->get(node.magicNum, result3A);
                    if (ret < 0) { // 0:success
                        drop_notify = MTRUE;
                        MY_LOGI("drop-frame by 3A Get(%d) @ (%d)(%d:%d)", ret,
                            node.magicNum, P1GET_FRM_NUM(node.appFrame),
                            P1GET_REQ_NUM(node.appFrame));
                    }
                }
                if (drop_notify) {
                    match = MFALSE;
                }
            }
        }
        #endif

        // check the ReqExpRec
        if (match && (node.expRec != EXP_REC_NONE)) {
            MY_LOGI("check ExpRec " P1INFO_NODE_STR, P1INFO_NODE_VAR(node));
            #if 0 // flush this frame
            match = MFALSE;
            #endif
        }
        // check the result of raw type
        if (match) {
            MUINT32 port_index = node.portBufIndex[P1_OUTPUT_PORT_IMGO];
            MBOOL raw_match = MTRUE;
            if (IS_OUT(REQ_OUT_FULL_PROC, node.reqOutSet) &&
                port_index != P1_PORT_BUF_IDX_NONE &&
                deqBuf.mvOut[port_index].mMetaData.mRawType !=
                EPipe_PROCESSED_RAW) {
                raw_match = MFALSE;
                // only check Processed-Raw with (mRawType == EPipe_PROCESSED_RAW)
                // in Pure-Raw, it would be EPipe_PURE_RAW or others
            }
            //MY_LOGD2("raw match: %d", raw_match);
            if (!raw_match) {
                MY_LOGE("RawType mismatch "
                    P1INFO_NODE_STR, P1INFO_NODE_VAR(node));
                #if 1 // flush this frame
                match = MFALSE;
                #endif
            }
        }
        //
        #if 0
        if (node.magicNum > 0 && node.magicNum < 15) {
            printf("[%d]node.img_resizer.get() = [%p]\n",
                node.magicNum, node.img_resizer.get());
            if (node.img_resizer.get() != NULL) {
                char filename[256] = {0};
                sprintf(filename, "/sdcard/raw/P1B_%d_%dx%d.raw",
                    node.magicNum,
                    node.img_resizer->getImgSize().w,
                    node.img_resizer->getImgSize().h
                    );
                printf("SAVE BUF [%s]\n", filename);
                node.img_resizer->saveToFile(filename);
            }
        }
        #endif
        node.frameTimeStamp = deqBuf.mvOut[i].mMetaData.mTimeStamp;
        node.exeState = EXE_STATE_DONE;
        if (!match || node.reqType == REQ_TYPE_INITIAL || !getActive()) {
            onReturnFrame(node, MTRUE, MTRUE);
            ret = BAD_VALUE;
        } else {
            IMetadata resultAppend;
            IMetadata inAPP,inHAL;
            //
            if (IS_LMV(mpConnectLMV)) {
                MBOOL enEIS = IS_PORT(CONFIG_PORT_EISO, mConfigPort);
                MBOOL enRRZ = IS_PORT(CONFIG_PORT_RRZO, mConfigPort);
                MUINT32 idxEIS = node.portBufIndex[P1_OUTPUT_PORT_EISO];
                MUINT32 idxRRZ = node.portBufIndex[P1_OUTPUT_PORT_RRZO];
                if (
                    #if 0
                    ((enEIS && idxEIS != P1_PORT_BUF_IDX_NONE) ||
                    (enRRZ && idxRRZ != P1_PORT_BUF_IDX_NONE)) &&
                    #endif
                    OK == frameMetadataGet(node, STREAM_META_IN_APP, &inAPP) &&
                    OK == frameMetadataGet(node, STREAM_META_IN_HAL, &inHAL)) {
                    mpConnectLMV->processResult(mIsBinEn, enEIS, enRRZ,
                        &inAPP, &inHAL, result3A, mp3A, node.magicNum,
                        node.sofIdx, mLastSofIdx, node.uniSwitchState,
                        deqBuf, idxEIS, idxRRZ, resultAppend);
                } else {
                    //MY_LOGI("not execute LMV process");
                };
            };
            //
            #if 1 // for RSSO update buffer
            if (IS_OUT(REQ_OUT_RSSO, node.reqOutSet) &&
                (!IS_EXP(EXP_EVT_NOBUF_RSSO, node.expRec))) {
                MUINT32 port_index = node.portBufIndex[P1_OUTPUT_PORT_RSSO];
                sp<IImageBuffer> spImgBuf =
                    node.streamBufImg[STREAM_IMG_OUT_RSS].spImgBuf;
                if (port_index != P1_PORT_BUF_IDX_NONE && spImgBuf != NULL) {
                    MSize size = deqBuf.mvOut[port_index].mMetaData.mDstSize;
                    MY_LOGD3("RSSO data size (%dx%d)", size.w, size.h);

                    IMetadata::IEntry entry(MTK_P1NODE_RSS_SIZE);
                    entry.push_back(size, Type2Type< MSize >());
                    resultAppend.update(MTK_P1NODE_RSS_SIZE, entry);
                }
            }
            #endif

            onProcessResult(node, deqBuf, result3A, resultAppend, i);
            mLastSofIdx = node.sofIdx;
            ret = OK;
        }
    }
    //
    if (IS_PORT(CONFIG_PORT_EISO, mConfigPort) && getActive()) {
        if (IS_LMV(mpConnectLMV)) {
            mpConnectLMV->processDequeFrame(deqBuf);
        }
    }

    // help to trigger GyroCollector.
    NSCam::Utils::GyroCollector::trigger();

    FUNCTION_P1_OUT;

    return ret;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
onHandleFlush(
    MBOOL wait
)
{
    FUNCTION_P1_IN;
    CAM_TRACE_NAME("P1:HandleFlush");

    //wake up queue thread.
    {
        Mutex::Autolock _l(mControls3AListLock);
        mControls3AListCond.broadcast();
    }

    //stop hardware
    if (!wait) {
        hardwareOps_stop(); //include hardware and 3A
    }

    //(1) clear request queue
    {
        Mutex::Autolock _l(mRequestQueueLock);
        //P1_LOGI(1, "Check-RQ (%zu)", mRequestQueue.size());
        while(!mRequestQueue.empty()) {
            QueJob_T job = *mRequestQueue.begin();
            mRequestQueue.erase(mRequestQueue.begin());
            for (MUINT8 i = 0; i < job.mSet.size(); i++) {
                QueNode_T node = job.mSet[i];
                onReturnFrame(node, MTRUE, MFALSE);
            }
        }
        if (!mQueueJob.mSet.empty()) {
            for (MUINT8 i = 0; i < mQueueJob.mSet.size(); i++) {
                QueNode_T node = mQueueJob.mSet[i];
                onReturnFrame(node, MTRUE, MFALSE);
            }
            mQueueJob.mSet.clear();
        }
    }

    //(2) clear processing queue
    //     wait until processing frame coming out
    if (wait) {
        Mutex::Autolock _l(mProcessingQueueLock);
        while(!mProcessingQueue.empty()) {
            mProcessingQueueCond.wait(mProcessingQueueLock);
        }
    } else {
        // must guarantee hardware has been stopped.
        Mutex::Autolock _l(mProcessingQueueLock);
        //P1_LOGI(1, "Check-PQ (%zu)", mProcessingQueue.size());
        while(!mProcessingQueue.empty()) {
            QueJob_T job = *mProcessingQueue.begin();
            mProcessingQueue.erase(mProcessingQueue.begin());
            for (MUINT8 i = 0; i < job.mSet.size(); i++) {
                QueNode_T node = job.mSet[i];
                onReturnFrame(node, MTRUE, MFALSE);
            }
        }
    }

    //(3) clear dummy queue

    //(4) clear drop frame queue
    onProcessDropFrame();

    if (mpDeliverMgr != NULL && !mpDeliverMgr->waitFlush(MTRUE)) {
        MY_LOGW("request not done");
    };

    //(5) clear all
    mRequestQueue.clear(); //suppose already clear
    mProcessingQueue.clear(); //suppose already clear
    mControls3AList.clear();
    //mImageStorage.uninit();
    //m3AStorage.clear();
    mLastNum = 1;

    FUNCTION_P1_OUT;
}


#if 0
/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
onProcess3AResult(
    MUINT32 magicNum,
    MUINT32 key,
    MUINT32 val
)
{
    MY_LOGD2("%d", magicNum);

    if(magicNum == 0) return;

    m3AStorage.add(magicNum, key, val);
    if(m3AStorage.isCompleted(magicNum)) {
        sp<IPipelineFrame> spFrame = m3AStorage.valueFor(magicNum).spFrame;
        StreamId_T const streamId_OutAppMeta = mOutAppMeta->getStreamId();
        IMetadata appMetadata = m3AStorage.valueFor(magicNum).resultVal;
        lock_and_returnMetadata(spFrame, streamId_OutAppMeta, appMetadata);
        m3AStorage.removeItem(magicNum);

        IStreamBufferSet& rStreamBufferSet  = spFrame->getStreamBufferSet();
        rStreamBufferSet.applyRelease(getNodeId());
    }
}
#endif

/******************************************************************************
 *
 ******************************************************************************/
void
P1NodeImp::
doNotifyCb(
    MINT32  _msgType,
    MINTPTR _ext1,
    MINTPTR _ext2,
    MINTPTR _ext3
)
{
    FUNCTION_P1_IN;
    MY_LOGD2("P1 doNotifyCb(%d) 0x%zd 0x%zd 0x%zd", _msgType, _ext1, _ext2, _ext3);
    Mutex::Autolock _l(mStandbyLock);
    switch(_msgType)
    {
        case IHal3ACb::eID_NOTIFY_3APROC_FINISH:
            if (_ext3 == 0) {
                MY_LOGE("CapParam NULL (%d) 0x%zd 0x%zd", _msgType, _ext1, _ext2);
            } else {
                RequestSet_T set = *(RequestSet_T*)(_ext1);
                CapParam_T param = *(CapParam_T*)(_ext3);
                onSyncBegin(MFALSE, &set, (MUINT32)_ext2, &param);
            }
            break;
        case IHal3ACb::eID_NOTIFY_CURR_RESULT:
            //onProcess3AResult((MUINT32)_ext1,(MUINT32)_ext2, (MUINT32)_ext3); //magic, key, val
            break;
        case IHal3ACb::eID_NOTIFY_VSYNC_DONE:
            onSyncEnd();
            break;
        default:
            break;
    }
    FUNCTION_P1_OUT;
}


/******************************************************************************
 *
 ******************************************************************************/
void
P1NodeImp::
doNotifyDropframe(MUINT magicNum, void* cookie)
{
    MY_LOGI("notify drop frame (%d)", magicNum);
    CAM_TRACE_FMT_BEGIN("P1:DRV-drop(%d)", magicNum);
    if (cookie == NULL) {
       MY_LOGE("return cookie is NULL");
       CAM_TRACE_FMT_END();
       return;
    }

    {
        Mutex::Autolock _l(reinterpret_cast<P1NodeImp*>(cookie)->mDropQueueLock);
        reinterpret_cast<P1NodeImp*>(cookie)->mDropQueue.push_back(magicNum);
    }

    if ((reinterpret_cast<P1NodeImp*>(cookie)->mpDeliverMgr != NULL) &&
        (reinterpret_cast<P1NodeImp*>(cookie)->mpDeliverMgr->runningGet())) {
        MY_LOGI("process drop frame");
        #if 0 // to simplify the drop callback function
        reinterpret_cast<P1NodeImp*>(cookie)->onProcessDropFrame(MTRUE);
        #else
        reinterpret_cast<P1NodeImp*>(cookie)->mpDeliverMgr->trigger();
        #endif
    };
    CAM_TRACE_FMT_END();
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
createStuffBuffer(sp<IImageBuffer> & imageBuffer,
    sp<IImageStreamInfo> const& streamInfo,
    NSCam::MSize::value_type const changeHeight)
{
    Vector<MUINT32> vStride;
    vStride.clear();
    for (size_t i = 0; i < streamInfo->getBufPlanes().size(); i++) {
        vStride.push_back((MUINT32)
            (streamInfo->getBufPlanes()[i].rowStrideInBytes));
    }
    //for (size_t v = 0; v < vStride.size(); v++) MY_LOGI("stride[%zu/%zu]=%d", v, vStride.size(), vStride[v]);
    //
    MSize size = streamInfo->getImgSize();
    // change the height while changeHeight > 0
    if (changeHeight > 0) {
        size.h = changeHeight;
    }
    //
    return createStuffBuffer(imageBuffer, streamInfo->getStreamName(),
        streamInfo->getImgFormat(), size, vStride);
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
createStuffBuffer(sp<IImageBuffer> & imageBuffer,
    char const* szName, MINT32 format, MSize size, Vector<MUINT32> vStride)
{
    return mStuffBufMgr.acquireStoreBuffer(imageBuffer, szName, format, size,
        vStride, mBurstNum, (mDebugScanLineMask != 0) ? MTRUE : MFALSE);
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
destroyStuffBuffer(sp<IImageBuffer> & imageBuffer)
{
    if (imageBuffer == NULL) {
        MY_LOGW("Stuff ImageBuffer not exist");
        return BAD_VALUE;
    }
    return mStuffBufMgr.releaseStoreBuffer(imageBuffer);
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
hardwareOps_start()
{
#if SUPPORT_ISP
    FUNCTION_P1_IN;
    CAM_TRACE_NAME("P1:hardwareOps_start");

    Mutex::Autolock _l(mHardwareLock);

    {
        Mutex::Autolock _l(mThreadLock);
        setActive(MTRUE);
        mThreadCond.broadcast();
    }
    setInit(MTRUE);
    mLastSofIdx = P1SOFIDX_NULL_VAL;
    mLastSetNum = 0;

    #if SUPPORT_VHDR
    mpVhdr = NULL;
    #endif
    #if SUPPORT_LCS
    mpLCS = NULL;
    #endif
    mConfigPort = CONFIG_PORT_NONE;
    mConfigPortNum = 0;

    mReqState = REQ_STATE_CREATED;
    mFirstReceived = MFALSE;
    //
    {
        Mutex::Autolock _ll(mFrameSetLock);
        mFrameSetAlready = MFALSE;
    }
    //
    {
        mQueueJob.mMaxNum = mBurstNum;
        mQueueJob.mSet.setCapacity(mBurstNum);
        mQueueJob.mSet.clear();
    }
    //
    mTagReq.clear();
    mTagSet.clear();
    mTagEnq.clear();
    mTagDeq.clear();
    mTagOut.clear();
    mDequeThreadProfile.reset();
    //mImageStorage.init(mLogLevel);
    //
    {
        MERROR err = OK;

    #ifdef USING_MTK_LDVT
        err = getNormalPipeModule()->createSubModule(
            getOpenId(), "iopipeUseTM", mCamIOVersion, (MVOID**)&mpCamIO);
    #else
        err = getNormalPipeModule()->createSubModule(
            getOpenId(), getNodeName(), mCamIOVersion, (MVOID**)&mpCamIO);
    #endif

        P1_TIMING_CHECK("P1:DRV-init", 20, TC_W);
        CAM_TRACE_BEGIN("P1:DRV-init");
        MY_LOGI1("mpCamIO->init +++");
        if  ( err < 0 || ! mpCamIO || ! mpCamIO->init() ) {
            MY_LOGE("hardware init fail - err:%#x mpCamIO:%p", err, mpCamIO);
            return DEAD_OBJECT;
        }
        MY_LOGI1("mpCamIO->init ---");
        CAM_TRACE_END();
    }

#if SUPPORT_LCS
    if (mEnableLCSO) {
        P1_TIMING_CHECK("P1:LCS-init", 10, TC_W);
        CAM_TRACE_BEGIN("P1:LCS-init");
        mpLCS = MAKE_LcsHal(LOG_TAG, getOpenId());
        if(mpLCS == NULL)
        {
            MY_LOGE("mpLCS is NULL");
            return DEAD_OBJECT;
        }
        if( mpLCS->Init() != LCS_RETURN_NO_ERROR)
        {
            mpLCS->DestroyInstance(LOG_TAG);
            mpLCS = NULL;
        }
        CAM_TRACE_END();
    }
#endif
#if SUPPORT_VHDR
    {
        P1_TIMING_CHECK("P1:VHDR-init", 10, TC_W);
        CAM_TRACE_BEGIN("P1:VHDR-init");
        mpVhdr = VHdrHal::CreateInstance(LOG_TAG, getOpenId());
        if(mpVhdr == NULL)
        {
            MY_LOGE("mpVhdr is NULL");
            return DEAD_OBJECT;
        }
        if( mpVhdr->Init(mSensorParams.vhdrMode) != VHDR_RETURN_NO_ERROR)
        {
            //mpVhdr->DestroyInstance(LOG_TAG); // instance always exist until process kill
            mpVhdr = NULL;
        }
        CAM_TRACE_END();
    }
#endif
    //
    sp<IImageBuffer> pEISOBuf = NULL;
    if (mEnableEISO) {
        P1_TIMING_CHECK("P1:LMV-init", 20, TC_W);
        CAM_TRACE_BEGIN("P1:LMV-init");
        if (IS_LMV(mpConnectLMV)) {
            MINT32 mode = EisInfo::getEISMode(mPackedEisInfo);
            MINT32 factor = EisInfo::getEISFactor(mPackedEisInfo);
            if (MFALSE == mpConnectLMV->init(pEISOBuf, mode, factor)) {
                MY_LOGE("ConnectLMV create fail");
                return BAD_VALUE;
            }
        }
        CAM_TRACE_END();
    }
    //
    #if 1
    IHalSensor::ConfigParam sensorCfg;
    ::memset(&sensorCfg, 0, sizeof(IHalSensor::ConfigParam));
    sensorCfg.index = (MUINT)getOpenId();
    sensorCfg.crop = mSensorParams.size;
    sensorCfg.scenarioId = mSensorParams.mode;
    sensorCfg.isBypassScenario = 0;
    sensorCfg.isContinuous = 1;
    #if SUPPORT_VHDR
    sensorCfg.HDRMode = mSensorParams.vhdrMode;
    #else
    sensorCfg.HDRMode = MFALSE;
    #endif
    #ifdef USING_MTK_LDVT
    sensorCfg.framerate = 1;
    #else
    sensorCfg.framerate = mSensorParams.fps;
    #endif
    sensorCfg.twopixelOn = 0;
    sensorCfg.debugMode = 0;
    #else
    IHalSensor::ConfigParam sensorCfg =
    {
        (MUINT)getOpenId(),                 /* index            */
        mSensorParams.size,                 /* crop */
        mSensorParams.mode,                 /* scenarioId       */
        0,                                  /* isBypassScenario */
        1,                                  /* isContinuous     */
#if SUPPORT_VHDR
        mSensorParams.vhdrMode,             /* vHDROn mode          */
#else
        MFALSE,                             /* vHDROn mode          */
#endif
        #ifdef USING_MTK_LDVT
        1,
        #else
        mSensorParams.fps,                  /* framerate        */
        #endif
        0,                                  /* two pixel on     */
        0,                                  /* debugmode        */
    };
    #endif

    vector<IHalSensor::ConfigParam> vSensorCfg;
    vSensorCfg.push_back(sensorCfg);

    //
    vector<portInfo> vPortInfo;
    if (mvStreamImg[STREAM_IMG_OUT_FULL] != NULL)
    {
        MINT fmt = mvStreamImg[STREAM_IMG_OUT_FULL]->getImgFormat();
        IImageStreamInfo::BufPlanes_t const & planes =
            mvStreamImg[STREAM_IMG_OUT_FULL]->getBufPlanes();
        portInfo OutPort(
            PORT_IMGO,
            (EImageFormat)fmt,
            mvStreamImg[STREAM_IMG_OUT_FULL]->getImgSize(),
            MRect(MPoint(0,0), mSensorParams.size),
            P1_STRIDE(planes, 0), P1_STRIDE(planes, 1), P1_STRIDE(planes, 2),
            0, // pureraw
            MTRUE/*IS_RAW_FMT_PACK_FULL(fmt)*/); //packed
            // by driver request, the PAK should be ON even if un-packed raw
        vPortInfo.push_back(OutPort);
        mConfigPort |= CONFIG_PORT_IMGO;
        mConfigPortNum ++;
    } else if (mvStreamImg[STREAM_IMG_OUT_OPAQUE] != NULL) {
        portInfo OutPort(
            PORT_IMGO,
            (EImageFormat)mRawFormat,
            mSensorParams.size,
            MRect(MPoint(0,0), mSensorParams.size),
            mRawStride, 0/*StrideInByte[1]*/, 0/*StrideInByte[2]*/,
            0, // pureraw
            MTRUE/*IS_RAW_FMT_PACK_FULL(mRawFormat)*/); //packed
            // by driver request, the PAK should be ON even if un-packed raw
        vPortInfo.push_back(OutPort);
        mConfigPort |= CONFIG_PORT_IMGO;
        mConfigPortNum ++;
    }
    //
    if (mvStreamImg[STREAM_IMG_OUT_RESIZE] != NULL)
    {
        IImageStreamInfo::BufPlanes_t const & planes =
            mvStreamImg[STREAM_IMG_OUT_RESIZE]->getBufPlanes();
        portInfo OutPort(
            PORT_RRZO,
            (EImageFormat)mvStreamImg[STREAM_IMG_OUT_RESIZE]->getImgFormat(),
            mvStreamImg[STREAM_IMG_OUT_RESIZE]->getImgSize(),
            MRect(MPoint(0,0), mSensorParams.size),
            P1_STRIDE(planes, 0), P1_STRIDE(planes, 1), P1_STRIDE(planes, 2),
            0, // pureraw
            MTRUE);              //packed
        vPortInfo.push_back(OutPort);
        mConfigPort |= CONFIG_PORT_RRZO;
        mConfigPortNum ++;
    }

    if (mEnableLCSO && mvStreamImg[STREAM_IMG_OUT_LCS] != NULL)
    {
        IImageStreamInfo::BufPlanes_t const & planes =
            mvStreamImg[STREAM_IMG_OUT_LCS]->getBufPlanes();
        portInfo OutPort(
            PORT_LCSO,
            (EImageFormat)mvStreamImg[STREAM_IMG_OUT_LCS]->getImgFormat(),
            mvStreamImg[STREAM_IMG_OUT_LCS]->getImgSize(),
            MRect(MPoint(0,0),  mvStreamImg[STREAM_IMG_OUT_LCS]->getImgSize()),
            P1_STRIDE(planes, 0), P1_STRIDE(planes, 1), P1_STRIDE(planes, 2),
            0, // pureraw
            MTRUE);              //packed
        vPortInfo.push_back(OutPort);
        mConfigPort |= CONFIG_PORT_LCSO;
        mConfigPortNum ++;
    }

    if (mEnableRSSO && mvStreamImg[STREAM_IMG_OUT_RSS] != NULL)
    {
        IImageStreamInfo::BufPlanes_t const & planes =
            mvStreamImg[STREAM_IMG_OUT_RSS]->getBufPlanes();
        portInfo OutPort(
            PORT_RSSO,
            (EImageFormat)mvStreamImg[STREAM_IMG_OUT_RSS]->getImgFormat(),
            mvStreamImg[STREAM_IMG_OUT_RSS]->getImgSize(),
            MRect(MPoint(0,0),  mvStreamImg[STREAM_IMG_OUT_RSS]->getImgSize()),
            P1_STRIDE(planes, 0), P1_STRIDE(planes, 1), P1_STRIDE(planes, 2),
            0, // pureraw
            MTRUE);              //packed
        vPortInfo.push_back(OutPort);
        mConfigPort |= CONFIG_PORT_RSSO;
        mConfigPortNum ++;
    }
    //
    if (mEnableEISO && pEISOBuf != NULL)
    {
        portInfo OutPort(
                PORT_EISO,
                (EImageFormat)pEISOBuf->getImgFormat(),
                pEISOBuf->getImgSize(),
                MRect(MPoint(0,0),  pEISOBuf->getImgSize()),
                pEISOBuf->getBufStridesInBytes(0),
                0, //pPortCfg->mStrideInByte[1],
                0, //pPortCfg->mStrideInByte[2],
                0, // pureraw
                MTRUE);              //packed

        vPortInfo.push_back(OutPort);
        mConfigPort |= CONFIG_PORT_EISO;
        mConfigPortNum ++;
    }
    //
    MBOOL bSupportDynamicTwin = MFALSE;
    if  ( auto pModule = getNormalPipeModule() ) {
        NSCam::NSIoPipe::NSCamIOPipe::NormalPipe_QueryInfo info;
        pModule->query(0, NSCam::NSIoPipe::NSCamIOPipe::ENPipeQueryCmd_D_Twin,
            0, 0, info);
        bSupportDynamicTwin = info.D_TWIN;
    }
    mIsLegacyStandbyMode = (bSupportDynamicTwin) ? MFALSE : MTRUE;
    mIsDynamicTwinEn = (bSupportDynamicTwin && (!mDisableDynamicTwin)) ?
        MTRUE : MFALSE;
    E_SEN senNum = E_1_SEN;
    switch (mTgNum) {
        case 0:
        case 1:
            senNum = E_1_SEN;
            break;
        case 2:
        default:
            senNum = E_2_SEN;
            break;
    };
    //
    MBOOL bDynamicRawType = MTRUE;  // true:[ON] ; false:[OFF]
    QInitParam halCamIOinitParam(
               0,                           /*sensor test pattern */
               10,                          /* bit depth*/
               vSensorCfg,
               vPortInfo,
               bDynamicRawType);
    halCamIOinitParam.m_Func.Raw = 0;
    if ((((mRawOption & (1 << EPipe_PROCESSED_RAW)) > 0) && (!mEnableDualPD))
        || (mDisableFrontalBinning)
        ) {
        // In DualPD case, the frontal binning will be decided by driver.
        // therefore, it do not need to adjust for this case.
        halCamIOinitParam.m_Func.Bits.OFF_BIN = 1;
        //halCamIOinitParam.m_bOffBin = MTRUE;
    }
    halCamIOinitParam.m_Func.Bits.DATA_PATTERN =
        (mEnableDualPD) ? (EPipe_Dual_pix) : (EPipe_Normal);
    halCamIOinitParam.m_Func.Bits.SensorNum = senNum;
    halCamIOinitParam.m_pipelinebitdepth = (E_CAM_PipelineBitDepth_SEL)mPipeBit;
    halCamIOinitParam.m_DynamicTwin = mIsDynamicTwinEn;
    halCamIOinitParam.m_DropCB = doNotifyDropframe;
    halCamIOinitParam.m_returnCookie = this;
    // enable frame sync
    if (mEnableFrameSync) {
      MY_LOGI("P1 node(%d) is in synchroized mode", getOpenId());
      halCamIOinitParam.m_bN3D = MTRUE;
    } else {
      halCamIOinitParam.m_bN3D = MFALSE;
    }

    // enable EIS
    if(mForceSetEIS){
        MY_LOGI("force set EIS enable(%d) id(%d)", mEnableEISO, getOpenId());
        if(mEnableEISO)
            halCamIOinitParam.m_UniLinkSel = E_UNI_LINK_SEL::E_UNI_LINK_ON;
        else
            halCamIOinitParam.m_UniLinkSel = E_UNI_LINK_SEL::E_UNI_LINK_OFF;
    }
    else{
        halCamIOinitParam.m_UniLinkSel = (mEnableUniForcedOn) ?
        E_UNI_LINK_ON : E_UNI_LINK_AUTO;
    }

    MY_LOGD1("ConfigPipe-Bit(%d)-N3d(%d)-Uni(%d)-Func(0x%X)",
        halCamIOinitParam.m_pipelinebitdepth, halCamIOinitParam.m_bN3D,
        halCamIOinitParam.m_UniLinkSel, halCamIOinitParam.m_Func.Raw);
    //
    MSize binInfoSize = mSensorParams.size;
    mIsBinEn = false;
    MSize rawSize[2];
    MSize * pSizeProc = &rawSize[0]; // 0 = EPipe_PROCESSED_RAW
    MSize * pSizePure = &rawSize[1]; // 1 = EPipe_PURE_RAW
    *pSizeProc = MSize(0, 0);
    *pSizePure = MSize(0, 0);
    if (mpCamIO != NULL) {
        P1_TIMING_CHECK("P1:DRV-configPipe", 500, TC_W);
        CAM_TRACE_BEGIN("P1:DRV-configPipe");
        MY_LOGI1("mpCamIO->configPipe +++");
        if(!mpCamIO->configPipe(halCamIOinitParam, mBurstNum))
        {
            MY_LOGE("mpCamIO->configPipe fail");
            return BAD_VALUE;
        } else {
            CAM_TRACE_BEGIN("P1:DRV-GetBinInfo");
            if (mpCamIO->sendCommand(ENPipeCmd_GET_BIN_INFO,
                (MINTPTR)&binInfoSize.w, (MINTPTR)&binInfoSize.h,
                (MINTPTR)NULL)) {
                if (binInfoSize.w < mSensorParams.size.w ||
                    binInfoSize.h < mSensorParams.size.h) {
                    mIsBinEn = true;
                }
            }
            CAM_TRACE_END();
            //
            {
                MBOOL notSupportProc = MFALSE;
                MBOOL notSupportPure = MFALSE;
                MUINT32 newDefType = mRawDefType;
                MUINT32 newOption = mRawOption;
                if (mpCamIO->sendCommand(ENPipeCmd_GET_IMGO_INFO,
                    (MINTPTR)(&rawSize), (MINTPTR)NULL, (MINTPTR)NULL)) {
                    if (pSizeProc->w == 0 || pSizeProc->h == 0) {
                        notSupportProc = MTRUE;
                    }
                    if (pSizePure->w == 0 || pSizePure->h == 0) {
                        notSupportPure = MTRUE;
                    }
                }
                if ((!notSupportProc) && (!notSupportPure)) {
                    // both Proc raw and Pure raw are supported
                    // not change the raw type setting
                } else if ((!notSupportProc) && (notSupportPure)) {
                    // only support Proc raw
                    newDefType = EPipe_PROCESSED_RAW;
                    newOption = (1 << EPipe_PROCESSED_RAW);
                } else if ((notSupportProc) && (!notSupportPure)) {
                    // only support Pure raw
                    newDefType = EPipe_PURE_RAW;
                    newOption = (1 << EPipe_PURE_RAW);
                } else {
                    // not support Proc raw and Pure raw
                    MY_LOGE("DualPD(%d) Raw(%d,0x%x) Proc(%dx%d) Pure(%dx%d) "
                    "- Not Support", mEnableDualPD, mRawDefType, mRawOption,
                        pSizeProc->w, pSizeProc->h, pSizePure->w, pSizePure->h);
                    return BAD_VALUE;
                }
                MY_LOGI_IF(((mEnableDualPD) ||
                    (mRawDefType != newDefType) || (mRawOption != newOption)),
                    "[RAW_TYPE] Raw(%d,0x%x) => New(%d,0x%x) : DualPD(%d) "
                    "Proc(%dx%d) Pure(%dx%d)", mEnableDualPD,
                    mRawDefType, mRawOption, newDefType, newOption,
                    pSizeProc->w, pSizeProc->h, pSizePure->w, pSizePure->h);
                //
                mRawDefType = newDefType;
                mRawOption = newOption;
            }
        }
        MY_LOGI1("mpCamIO->configPipe ---");
        CAM_TRACE_END();
    }

#if SUPPORT_3A
    {
        P1_TIMING_CHECK("P1:3A-create-setSensorMode", 10, TC_W);
        CAM_TRACE_BEGIN("P1:3A-create-setSensorMode");
        mp3A = MAKE_Hal3A(getOpenId(), getNodeName());
        if(mp3A == NULL) {
            MY_LOGE("mp3A is NULL");
            return DEAD_OBJECT;
        }
        mp3A->setSensorMode(mSensorParams.mode);
        CAM_TRACE_END();
    }
#endif

    if (IS_LMV(mpConnectLMV)) {
        P1_TIMING_CHECK("P1:LMV-config", 20, TC_W);
        CAM_TRACE_BEGIN("P1:LMV-config");
        mpConnectLMV->config();
        CAM_TRACE_END();
    }

    #if SUPPORT_RSS
    if (mEnableRSSO) {
        P1_TIMING_CHECK("P1:RSS-register", 20, TC_W);
        CAM_TRACE_BEGIN("P1:RSS-register");
        RssP1Cb* pRssCb = RssP1Cb::createInstance(getOpenId(), LOG_TAG);
        if (pRssCb != NULL) {
            pRssCb->registerP1Notify(mpCamIO);
        } else {
            MY_LOGW("RssP1Cb not get");
        }
        CAM_TRACE_END();
    }
    #endif

    #if SUPPORT_VHDR
    if(mpVhdr)
    {
        VHDR_HAL_CONFIG_DATA vhdrConfig;
        vhdrConfig.cameraVer = VHDR_CAMERA_VER_3;
        //
        P1_TIMING_CHECK("P1:VHDR-config", 20, TC_W);
        CAM_TRACE_BEGIN("P1:VHDR-config");
        mpVhdr->ConfigVHdr(vhdrConfig);
        CAM_TRACE_END();
    }
    #endif

    #if SUPPORT_LCS
    if(mpLCS)
    {
        LCS_HAL_CONFIG_DATA lcsConfig;
        lcsConfig.cameraVer = LCS_CAMERA_VER_3;
        if (mvStreamImg[STREAM_IMG_OUT_LCS] != NULL) {
            lcsConfig.lcsOutWidth =
                mvStreamImg[STREAM_IMG_OUT_LCS]->getImgSize().w;
            lcsConfig.lcsOutHeight =
                mvStreamImg[STREAM_IMG_OUT_LCS]->getImgSize().h;
        } else {
            MY_LOGI("LCS enable but no LCS stream info");
            lcsConfig.lcsOutWidth = 0;
            lcsConfig.lcsOutHeight = 0;
        }
        //
        P1_TIMING_CHECK("P1:LCS-config", 20, TC_W);
        CAM_TRACE_BEGIN("P1:LCS-config");
        mpLCS->ConfigLcsHal(lcsConfig);
        CAM_TRACE_END();
    }
    #endif

    if (mpConCtrl != NULL && mpConCtrl->getStageCtrl() != NULL) {
        mpConCtrl->getStageCtrl()->done((MUINT32)STAGE_DONE_START, MTRUE);
    }

    #if SUPPORT_3A
    if (mp3A) {
        P1_TIMING_CHECK("P1:3A-config", 300, TC_W);
        CAM_TRACE_BEGIN("P1:3A-config");
        MY_LOGI1("mp3A->config +++");
        if (mEnableDualPD) {
            mp3A ->send3ACtrl(E3ACtrl_SetEnablePBin, 1, 0);
        }
        if (EIS_MODE_IS_EIS_ADVANCED_ENABLED(EisInfo::getEISMode(mPackedEisInfo)) &&
            mSensorParams.vhdrMode == SENSOR_VHDR_MODE_NONE &&
            !mDisableAEEIS)
        {
            mp3A ->send3ACtrl(E3ACtrl_SetAEEISRecording, 1, 0);
            MY_LOGD2("mEisMode:%d => Set EIS AE P-line \n", EisInfo::getEISMode(mPackedEisInfo));
        }
        mp3A->attachCb(IHal3ACb::eID_NOTIFY_3APROC_FINISH, this);
        mp3A->attachCb(IHal3ACb::eID_NOTIFY_CURR_RESULT, this);
        mp3A->attachCb(IHal3ACb::eID_NOTIFY_VSYNC_DONE, this);
        NS3Av3::ConfigInfo_T config;
        NS3Av3::EBitMode_T b = NS3Av3::EBitMode_12Bit;
        switch (mPipeBit) {
            case CAM_Pipeline_10BITS: b = NS3Av3::EBitMode_10Bit; break;
            case CAM_Pipeline_12BITS: b = NS3Av3::EBitMode_12Bit; break;
            case CAM_Pipeline_14BITS: b = NS3Av3::EBitMode_14Bit; break;
            case CAM_Pipeline_16BITS: b = NS3Av3::EBitMode_16Bit; break;
            default:
                MY_LOGW("CANNOT map the pipeline bit mode");
                break;
        };
        config.i4BitMode = b;
        config.i4SubsampleCount = (MINT32)(MAX(mBurstNum, 1));
        config.i4HlrOption = (mDisableHLR) ?
            (NS3Av3::EHlrOption_ForceOff) : (NS3Av3::EHlrOption_Auto);
        mp3A->config(config);
        //m3AProcessedDepth = mp3A->getCapacity();
        MY_LOGI1("mp3A->config ---");
        CAM_TRACE_END();
    }
    #endif
    //
    if (mpHwStateCtrl != NULL) {
        mpHwStateCtrl->config(mLogLevel, getOpenId(), mBurstNum, mpCamIO, mp3A);
    }
    //
    if (EN_INIT_REQ) {
        mInitReqNum = mInitReqSet * mBurstNum;
        mInitReqCnt = 0;
        MY_LOGI("InitRqeFlow return %d %d %d",
            mInitReqSet, mInitReqNum, mInitReqCnt);
        return OK;
    }
    //
    if (EN_START_CAP) {
        Mutex::Autolock _l(mStartCaptureLock);
        mStartCaptureState = START_CAP_STATE_WAIT_REQ;
        mStartCaptureType = E_CAPTURE_NORMAL;
        mStartCaptureIdx = 0;
        mStartCaptureExp = 0;
        MY_LOGI("EnableCaptureFlow(%d) return", mEnableCaptureFlow);
        return OK;
    }

    #if SUPPORT_3A
    if (mp3A) {
        P1_TIMING_CHECK("P1:3A-start", 100, TC_W);
        CAM_TRACE_BEGIN("P1:3A-start");
        MY_LOGI1("mp3A->start +++");
        mp3A->start();
        MY_LOGI1("mp3A->start ---");
        CAM_TRACE_END();
    }
    #endif

    //[TODO] in case that delay frame is above 3 but memeory has only 3, pending aquirefromPool
    {
        if (mpConCtrl != NULL && mpConCtrl->getStageCtrl() != NULL) {
            MBOOL success = MFALSE;
            mpConCtrl->getStageCtrl()->wait(
                (MUINT32)STAGE_DONE_INIT_ITEM, success);
            if (!success) {
                MY_LOGE("stage - init item fail");
                return BAD_VALUE;
            }
        }
        //
        MERROR status = hardwareOps_enque(
            mProcessingQueue.editItemAt(mProcessingQueue.size()-1),
            ENQ_TYPE_INITIAL);
        if (status != OK) {
            MY_LOGE("hardware init-enque fail (%d)", status);
            return status;
        }
        // Due to pipeline latency, delay frame should be above 3
        // if delay frame is more than 3, add node to mProcessingQueue here.
        for (int i = 0; i < mDelayframe - mNumHardwareBuffer; i++) {
            for (MUINT8 n = 0; n < mBurstNum; n++) {
                createNode(&mControls3AList, &mControls3AListLock);
            }
        }
    }
    //
    #if 1
    if (mpCamIO != NULL) {
        P1_TIMING_CHECK("P1:DRV-start", 100, TC_W);
        CAM_TRACE_BEGIN("P1:DRV-start");
        MY_LOGI1("mpCamIO->start +++");
        if(!mpCamIO->start()) {
            MY_LOGE("mpCamIO->start fail");
            return BAD_VALUE;
        }
        MY_LOGI1("mpCamIO->start ---");
        CAM_TRACE_END();
    }
    #endif

    if (IS_LMV(mpConnectLMV)) {
        P1_TIMING_CHECK("P1:LMV-sensor", 100, TC_W);
        CAM_TRACE_BEGIN("P1:LMV-sensor");
        mpConnectLMV->enableSensor();
        CAM_TRACE_END();
    }

    {
        Mutex::Autolock _l(mThreadLock);
        setReady(MTRUE);
        mThreadCond.broadcast();
    }

    MY_LOGI("Cam::%d Sensor(%dx%d) Raw(%d,0x%x)-Proc(%dx%d)-Pure(%dx%d) "
        "Bin(%dx%d) BinEn=%d TG(%d:%d) DTwin(%d@%d)=%d#%d ConfigPort[%d]:0x%x",
        getOpenId(),
        mSensorParams.size.w, mSensorParams.size.h, mRawDefType, mRawOption,
        pSizeProc->w, pSizeProc->h, pSizePure->w, pSizePure->h,
        binInfoSize.w, binInfoSize.h, mIsBinEn, mTgNum, senNum,
        mDisableDynamicTwin, bSupportDynamicTwin, mIsDynamicTwinEn,
        mIsLegacyStandbyMode, mConfigPortNum, mConfigPort);

    FUNCTION_P1_OUT;

    return OK;
#else
    return OK;
#endif

}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
hardwareOps_request()
{
#if SUPPORT_ISP
    FUNCTION_P1S_IN;
    //
    Mutex::Autolock _l(mHardwareLock);
    //
    MINT32 num = 0;
    //
#if SUPPORT_3A
    if (mp3A) {
        P1_TIMING_CHECK("P1:3A-startRequest", 200, TC_W);
        CAM_TRACE_BEGIN("P1:3A-startRequest");
        MINT32 type = ESTART_CAP_NORMAL;
        {
            {   // get the first setting number
                Mutex::Autolock _l(mRequestQueueLock);
                if (mRequestQueue.empty()) {
                    MY_LOGE("init request set is not ready");
                    return BAD_VALUE;
                }
                Que_T::iterator it = mRequestQueue.begin();
                num = it->mSet[0].magicNum;
            }
            {   // assign the last set number as onRequestFrameSet()
                Mutex::Autolock _l(mControls3AListLock);
                mInitSetNum = num;
                mLastSetNum = num;
                mTagSet.set(num);
            }
            mp3A->startRequestQ(mControls3AList);//mp3A->start();
            //
            {
                Mutex::Autolock _ll(mFrameSetLock);
                mFrameSetAlready = MTRUE;
            }
        }
        CAM_TRACE_END();
    }
#endif
    //
    MINT32 total = mProcessingQueue.size();
    if (total < (MINT32)(mInitReqSet - 1)) {
        MY_LOGE("init request set is not enough (%d < %d)", total, mInitReqSet);
        return BAD_VALUE;
    }
    MINT32 index = 0;
    for (index = 0; index < total; index++) {
        MY_LOGD("InitReq HW_ENQ (%d/%d) +++", index, total);
        MERROR status = hardwareOps_enque(
            mProcessingQueue.editItemAt(index), ENQ_TYPE_INITIAL);
        if (status != OK) {
            MY_LOGE("hardware req-init-enque fail (%d)@(%d)", status, index);
            return status;
        }
        MY_LOGD("InitReq HW_ENQ (%d/%d) ---", index, total);
    }
    //
    #if 1
    if (mpCamIO) {
        P1_TIMING_CHECK("P1:DRV-start", 100, TC_W);
        CAM_TRACE_BEGIN("P1:DRV-start");
        MY_LOGD2("mpCamIO->start +++");
        if(!mpCamIO->start()) {
            MY_LOGE("hardware start fail");
            return BAD_VALUE;
        }
        MY_LOGD2("mpCamIO->start ---");
        CAM_TRACE_END();
    }
    #endif
    //
    {
        Mutex::Autolock _l(mThreadLock);
        setReady(MTRUE);
        mThreadCond.broadcast();
    }
    //
    MY_LOGI("Cam::%d BinEn:%d ConfigPort[%d]:0x%x",
        getOpenId(), mIsBinEn,
        mConfigPortNum, mConfigPort);

    FUNCTION_P1S_OUT;

    return OK;
#else
    return OK;
#endif
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
hardwareOps_capture()
{
#if SUPPORT_ISP
    FUNCTION_P1S_IN;
    //
    Mutex::Autolock _l(mHardwareLock);
    //
    MINT32 num = 0;
    MBOOL isManualCap = MFALSE;
    //
    if (EN_START_CAP) {
        Mutex::Autolock _l(mStartCaptureLock);
        mStartCaptureState = START_CAP_STATE_WAIT_CB;
    }
    //
#if SUPPORT_3A
    if (mp3A) {
        P1_TIMING_CHECK("P1:3A-startCapture", 200, TC_W);
        CAM_TRACE_BEGIN("P1:3A-startCapture");
        MINT32 type = ESTART_CAP_NORMAL;
        {
            Mutex::Autolock _ll(mFrameSetLock);
            type = mp3A->startCapture(mControls3AList);//mp3A->start();
            mFrameSetAlready = MTRUE;
        }
        if (type != ESTART_CAP_NORMAL) {
            isManualCap = MTRUE;
            MY_LOGI("capture in manual flow %d", type);
        }
        CAM_TRACE_END();
    }
#endif
    //
    if (mpConCtrl != NULL && mpConCtrl->getStageCtrl() != NULL) {
        MBOOL success = MFALSE;
        mpConCtrl->getStageCtrl()->wait(
            (MUINT32)STAGE_DONE_INIT_ITEM, success);
        if (!success) {
            MY_LOGE("stage - cap init item fail");
            return BAD_VALUE;
        }
    }
    //
    {
        MERROR status = hardwareOps_enque(
            mProcessingQueue.editItemAt(mProcessingQueue.size()-1),
            ENQ_TYPE_INITIAL);
        if (status != OK) {
            MY_LOGE("hardware cap-init-enque fail (%d)", status);
            return status;
        }
    }
    //
    if (!isManualCap) {
        CAM_TRACE_BEGIN("Cap Normal EnQ");
        {
            QueJob_T job(mBurstNum);
            {
                Mutex::Autolock _l(mRequestQueueLock);
                Que_T::iterator it = mRequestQueue.begin();
                job = *it;
                mRequestQueue.erase(it);
            }
            MERROR status = onProcessEnqueFrame(job);
            if (status != OK) {
                MY_LOGE("hardware cap-enque-normal fail (%d)", status);
                return status;
            }
            num = job.mSet[0].magicNum;
        }
        CAM_TRACE_END();
        //
        if (num > 0) {
            Mutex::Autolock _l(mControls3AListLock);
            mLastSetNum = num;
            mTagSet.set(num);
        }
    }
    //
    #if 1
    if (mpCamIO) {
        P1_TIMING_CHECK("P1:DRV-start", 100, TC_W);
        CAM_TRACE_BEGIN("P1:DRV-start");
        MY_LOGD2("mpCamIO->start +++");
        if(!mpCamIO->start()) {
            MY_LOGE("hardware start fail");
            return BAD_VALUE;
        }
        MY_LOGD2("mpCamIO->start ---");
        CAM_TRACE_END();
    }
    #endif
    //
    if (isManualCap) {
        CAM_TRACE_BEGIN("Cap Manual EnQ");
        {
            QueJob_T job(mBurstNum);
            {
                Mutex::Autolock _l(mRequestQueueLock);
                if (!mRequestQueue.isEmpty()) {
                    Que_T::iterator it = mRequestQueue.begin();
                    job = *it;
                    mRequestQueue.erase(it);
                }
            }
            MERROR status = onProcessEnqueFrame(job);
            if (status != OK) {
                MY_LOGE("hardware cap-enque-manual fail (%d)", status);
                return status;
            }
            num = job.mSet[0].magicNum;
        }
        CAM_TRACE_END();
        //
        if (num > 0) {
            Mutex::Autolock _l(mControls3AListLock);
            mLastSetNum = num;
            mTagSet.set(num);
        }
    }
    //
    {
        Mutex::Autolock _l(mThreadLock);
        setReady(MTRUE);
        mThreadCond.broadcast();
    }
    //
    MY_LOGI("Cam::%d BinEn:%d ConfigPort[%d]:0x%x",
        getOpenId(), mIsBinEn,
        mConfigPortNum, mConfigPort);

    FUNCTION_P1S_OUT;

    return OK;
#else
    return OK;
#endif
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
procedureAid_start()
{
    CAM_TRACE_NAME("P1:aid_start");
    MERROR status = OK;
    if (mpConCtrl != NULL && mpConCtrl->getStageCtrl() != NULL) {
        MBOOL success = MFALSE;
        mpConCtrl->getStageCtrl()->wait(
            (MUINT32)STAGE_DONE_START, success);
        if (!success) {
            MY_LOGE("stage - aid start fail");
            return BAD_VALUE;
        }
    }
    //
    MBOOL init_success = MTRUE;
    status = buildInitItem();
    if (OK != status) {
        init_success = MFALSE;
        MY_LOGE("CANNOT build init item");
    }
    if (mpConCtrl != NULL && mpConCtrl->getStageCtrl() != NULL) {
        mpConCtrl->getStageCtrl()->done(
            (MUINT32)STAGE_DONE_INIT_ITEM, init_success);
    }
    //
    return status;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
buildInitItem()
{
    CAM_TRACE_NAME("P1:reserve_init");
    if (getReady()) {
        MY_LOGW("it should be executed before start ready");
        return BAD_VALUE;
    }
    //
    QueJob_T job(mBurstNum);
    for (MUINT8 n = 0; n < mBurstNum; n++) {
        createNode(NULL, NULL, &job, &mProcessingQueue, &mProcessingQueueLock,
                             &mControls3AList, &mControls3AListLock);
    }
    //
    QueJob_T& p_job = mProcessingQueue.editItemAt(mProcessingQueue.size()-1);
    QBufInfo* pEnBuf = NULL;
    if (mpConCtrl == NULL || (!mpConCtrl->initBufInfo_create(&pEnBuf))
        || pEnBuf == NULL) {
        MY_LOGE("CANNOT create the initBufInfo");
        return BAD_VALUE;
    }
    for (size_t i = 0; i < p_job.mSet.size(); i++) {
        MY_LOGD2("p_job(%d)(%zu/%zu)",
            p_job.mFirstMagicNum, i, p_job.mSet.size());
        if (OK != setupNode(p_job.mSet.editItemAt(i), (*pEnBuf))) {
            MY_LOGE("setup enque node fail");
            return BAD_VALUE;
        }
        p_job.mSet.editItemAt(i).exeState = EXE_STATE_PROCESSING;
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
generateAppMeta(QueNode_T & node, MetaSet_T const &result3A,
    QBufInfo const &deqBuf, IMetadata &appMetadata, MUINT32 const index)
{
    if (node.appFrame == NULL) {
        MY_LOGW("pipeline frame is NULL (%d)", node.magicNum);
        return;
    }
    sp<IPipelineFrame> request = node.appFrame;

    //[3A/Flash/sensor section]
    appMetadata = result3A.appMeta;

    MBOOL needOverrideTimestamp = MFALSE;
    if (tryGetMetadata<MBOOL>(&result3A.halMeta, MTK_EIS_NEED_OVERRIDE_TIMESTAMP, needOverrideTimestamp)
        && needOverrideTimestamp) {
        IMetadata::IEntry entry(MTK_EIS_FEATURE_ISNEED_OVERRIDE_TIMESTAMP);
        entry.push_back( 1, Type2Type< MUINT8 >()); // Need Override timestamp
        entry.push_back( 0, Type2Type< MUINT8 >()); // timestamp not overrided yet
        appMetadata.update(MTK_EIS_FEATURE_ISNEED_OVERRIDE_TIMESTAMP, entry);
    }

    //[request section]
    // android.request.frameCount
    {
        IMetadata::IEntry entry(MTK_REQUEST_FRAME_COUNT);
        entry.push_back( request->getFrameNo(), Type2Type< MINT32 >());
        appMetadata.update(MTK_REQUEST_FRAME_COUNT, entry);
    }
    // android.request.metadataMode
    {
        IMetadata::IEntry entry(MTK_REQUEST_METADATA_MODE);
        entry.push_back(MTK_REQUEST_METADATA_MODE_FULL, Type2Type< MUINT8 >());
        appMetadata.update(MTK_REQUEST_METADATA_MODE, entry);
    }

    //[sensor section]
    // android.sensor.timestamp
    {
        MINT64 frame_duration = 0;
        //IMetadata::IEntry entry(MTK_SENSOR_FRAME_DURATION);
        //should get from control.
        #if 1 // modify timestamp
        frame_duration = node.frameExpDuration;
        #endif
        MINT64 timestamp =
            deqBuf.mvOut[index].mMetaData.mTimeStamp - frame_duration;
        IMetadata::IEntry entry(MTK_SENSOR_TIMESTAMP);
        entry.push_back(timestamp, Type2Type< MINT64 >());
        appMetadata.update(MTK_SENSOR_TIMESTAMP, entry);
    }

    //[sensor section]
    // android.sensor.rollingshutterskew
    // [TODO] should query from sensor
    {
        IMetadata::IEntry entry(MTK_SENSOR_ROLLING_SHUTTER_SKEW);
        entry.push_back(33000000, Type2Type< MINT64 >());
        appMetadata.update(MTK_SENSOR_ROLLING_SHUTTER_SKEW, entry);
    }


}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
generateAppTagIndex(IMetadata &appMetadata, IMetadata &appTagIndex)
{
    IMetadata::IEntry entryTagIndex(MTK_P1NODE_METADATA_TAG_INDEX);

    for (size_t i = 0; i < appMetadata.count(); ++i) {
        IMetadata::IEntry entry = appMetadata.entryAt(i);
        entryTagIndex.push_back((MINT32)entry.tag(), Type2Type<MINT32>());
    }

    if (OK != appTagIndex.update(entryTagIndex.tag(), entryTagIndex)) {
        MY_LOGE("fail to update index");
    }
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
generateHalMeta(QueNode_T & node, MetaSet_T const &result3A,
    QBufInfo const &deqBuf, IMetadata const &resultAppend,
    IMetadata const &inHalMetadata, IMetadata &halMetadata, MUINT32 const index)
{
    if (deqBuf.mvOut.size() == 0) {
        MY_LOGE("deqBuf is empty");
        return;
    }

    // 3a tuning
    halMetadata = result3A.halMeta;

    // append
    halMetadata += resultAppend;

    // in hal meta
    halMetadata += inHalMetadata;

    {
        IMetadata::IEntry entry(MTK_P1NODE_SENSOR_MODE);
        entry.push_back(mSensorParams.mode, Type2Type< MINT32 >());
        halMetadata.update(MTK_P1NODE_SENSOR_MODE, entry);
    }

    {
        IMetadata::IEntry entry(MTK_P1NODE_SENSOR_VHDR_MODE);
        entry.push_back(mSensorParams.vhdrMode, Type2Type< MINT32 >());
        halMetadata.update(MTK_P1NODE_SENSOR_VHDR_MODE, entry);
    }

    {
        IMetadata::IEntry entry(MTK_PIPELINE_FRAME_NUMBER);
        entry.push_back(node.appFrame->getFrameNo(), Type2Type< MINT32 >());
        halMetadata.update(MTK_PIPELINE_FRAME_NUMBER, entry);
    }

    //rrzo
    MUINT32 port_index = node.portBufIndex[P1_OUTPUT_PORT_RRZO];
    if (port_index != P1_PORT_BUF_IDX_NONE) {
        NSCam::NSIoPipe::NSCamIOPipe::ResultMetadata const * result =
            &(deqBuf.mvOut[port_index].mMetaData);
        if (result == NULL) {
            MY_LOGE("CANNOT get result at (%d) for (%d)", port_index, index);
            return;
        }
        //crop region
        {
            MRect crop = result->mCrop_s;
            if (mIsBinEn) {
                BIN_REVERT(crop.p.x);
                BIN_REVERT(crop.p.y);
                BIN_REVERT(crop.s.w);
                BIN_REVERT(crop.s.h);
            }
            IMetadata::IEntry entry(MTK_P1NODE_SCALAR_CROP_REGION);
            entry.push_back(crop, Type2Type< MRect >());
            halMetadata.update(MTK_P1NODE_SCALAR_CROP_REGION, entry);
        }
        //
        {
            IMetadata::IEntry entry(MTK_P1NODE_DMA_CROP_REGION);
            entry.push_back(result->mCrop_d, Type2Type< MRect >());
            halMetadata.update(MTK_P1NODE_DMA_CROP_REGION, entry);
        }
        //
        {
            IMetadata::IEntry entry(MTK_P1NODE_RESIZER_SIZE);
            entry.push_back(result->mDstSize, Type2Type< MSize >());
            halMetadata.update(MTK_P1NODE_RESIZER_SIZE, entry);
        }
        MY_LOGD3("[Crop_Info] CropS(%d, %d, %dx%d) "
            "CropD(%d, %d, %dx%d) DstSize(%dx%d)",
            result->mCrop_s.leftTop().x,
            result->mCrop_s.leftTop().y,
            result->mCrop_s.size().w,
            result->mCrop_s.size().h,
            result->mCrop_d.leftTop().x,
            result->mCrop_d.leftTop().y,
            result->mCrop_d.size().w,
            result->mCrop_d.size().h,
            result->mDstSize.w,
            result->mDstSize.h);
    }
    //
    {
        MINT64 timestamp =
            deqBuf.mvOut[index].mMetaData.mTimeStamp;
        IMetadata::IEntry entry(MTK_P1NODE_FRAME_START_TIMESTAMP);
        entry.push_back(timestamp, Type2Type< MINT64 >());
        halMetadata.update(MTK_P1NODE_FRAME_START_TIMESTAMP, entry);
    }
    //
    if ((mIsDynamicTwinEn) && (mpCamIO != NULL)) {
        MBOOL ret = MFALSE;
        MINT32 status = MTK_P1_TWIN_STATUS_NONE;
        NSCam::NSIoPipe::NSCamIOPipe::E_CamHwPathCfg curCfg = eCamHwPathCfg_Num;
        ret = mpCamIO->sendCommand(ENPipeCmd_GET_HW_PATH_CFG,
            (MINTPTR)(&curCfg), (MINTPTR)NULL, (MINTPTR)NULL);
        if (ret) {
            switch (curCfg) {
                case eCamHwPathCfg_One_TG:
                    status = MTK_P1_TWIN_STATUS_TG_MODE_1;
                    break;
                case eCamHwPathCfg_Two_TG:
                    status = MTK_P1_TWIN_STATUS_TG_MODE_2;
                    break;
                //case eCamHwPathCfg_Num:
                default:
                    MY_LOGI("CamHwPathCfg_Num(%d) not defined", curCfg);
                    break;
            }
            IMetadata::IEntry entry(MTK_P1NODE_TWIN_STATUS);
            entry.push_back(status, Type2Type< MINT32 >());
            halMetadata.update(MTK_P1NODE_TWIN_STATUS, entry);
        } else {
            MY_LOGI("cannot get ENPipeCmd_GET_HW_PATH_CFG (%d)", ret);
        }
        MY_LOGD3("(%d)=GET_HW_PATH_CFG(%d) (%d) @ (%d)(%d:%d)", ret, curCfg,
            status, node.magicNum,
            P1GET_FRM_NUM(node.appFrame), P1GET_REQ_NUM(node.appFrame));
    }
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
setupNode(
    QueNode_T &node,
    QBufInfo &info
)
{
    FUNCTION_P1_IN;
#if SUPPORT_ISP
    MUINT32 out = 0;
    //
    sp<IImageStreamInfo> pImgStreamInfo = NULL;
    sp<IImageBuffer> pImgBuf = NULL;
    //
    NSCam::NSIoPipe::PortID portID = NSCam::NSIoPipe::PortID();
    MSize dstSize = MSize(0, 0);
    MRect cropRect = MRect(MPoint(0, 0), MSize(0, 0));
    MUINT32 rawOutFmt = 0;
    //
    STREAM_IMG streamImg = STREAM_IMG_NUM;
    //
    if ((node.reqType == REQ_TYPE_UNKNOWN) ||
        (node.reqType == REQ_TYPE_REDO) ||
        (node.reqType == REQ_TYPE_YUV)) {
        MY_LOGW("mismatch node type " P1INFO_NODE_STR, P1INFO_NODE_VAR(node));
        return BAD_VALUE;
    }
    //
    CAM_TRACE_FMT_BEGIN("Setup(%d@%d)(%d,%d)", node.magicNum, node.sofIdx,
        P1GET_FRM_NUM(node.appFrame), P1GET_REQ_NUM(node.appFrame));
    //
    #if (IS_P1_LOGI)
    android::String8 strInfo("");
    #endif
    //
    for (out = 0; out < REQ_OUT_MAX; out++) {
        if (!(IS_OUT(out, node.reqOutSet))) {
            continue;
        }
        CAM_TRACE_FMT_BEGIN("REQ_OUT_%d", out);
        //pBufPool = NULL;
        pImgStreamInfo = NULL;
        //pImgStreamBuf = NULL;
        pImgBuf = NULL;
        streamImg = STREAM_IMG_NUM;
        switch (out) {
            case REQ_OUT_LCSO:
            case REQ_OUT_LCSO_STUFF:
                streamImg = STREAM_IMG_OUT_LCS;
                portID = PORT_LCSO;
                dstSize = mvStreamImg[streamImg]->getImgSize();
                cropRect = MRect(MPoint(0, 0), mvStreamImg[streamImg]->getImgSize());
                rawOutFmt = (EPipe_PROCESSED_RAW);
                if (out == REQ_OUT_LCSO_STUFF) {
                    // use stuff buffer with height:1
                    //dstSize.h = P1_STUFF_BUF_HEIGHT(MTRUE, mConfigPort); .// TODO LCSO can use height 1 ???
                    cropRect.s = dstSize;
                }
                break;

            case REQ_OUT_RSSO:
            case REQ_OUT_RSSO_STUFF:
                streamImg = STREAM_IMG_OUT_RSS;
                portID = PORT_RSSO;
                dstSize = mvStreamImg[streamImg]->getImgSize();
                cropRect = MRect(MPoint(0, 0), mvStreamImg[streamImg]->getImgSize());
                rawOutFmt = (EPipe_PROCESSED_RAW);
                if (out == REQ_OUT_RSSO_STUFF) {
                    // use stuff buffer with height:1
                    //dstSize.h = P1_STUFF_BUF_HEIGHT(MTRUE, mConfigPort); .// TODO RSSO can use height 1 ???
                    cropRect.s = dstSize;
                }
                break;

            case REQ_OUT_RESIZER:
            case REQ_OUT_RESIZER_STUFF:
                streamImg = STREAM_IMG_OUT_RESIZE;
                portID = PORT_RRZO;
                dstSize = node.dstSize_resizer;
                cropRect = node.cropRect_resizer;
                rawOutFmt = (EPipe_PROCESSED_RAW);
                if (out == REQ_OUT_RESIZER_STUFF) {
                    // use stuff buffer with height:1
                    dstSize.h = P1_STUFF_BUF_HEIGHT(MTRUE, mConfigPort);
                    cropRect.s = dstSize;
                }
                break;

            case REQ_OUT_FULL_PROC:
            case REQ_OUT_FULL_PURE:
            case REQ_OUT_FULL_OPAQUE:
            case REQ_OUT_FULL_STUFF:
                streamImg = STREAM_IMG_OUT_FULL;
                if (out == REQ_OUT_FULL_OPAQUE || (out == REQ_OUT_FULL_STUFF &&
                    node.streamBufImg[STREAM_IMG_OUT_OPAQUE].bExist)) {
                    streamImg = STREAM_IMG_OUT_OPAQUE;
                } else if (mvStreamImg[STREAM_IMG_OUT_FULL] != NULL) {
                    streamImg = STREAM_IMG_OUT_FULL;
                } else if (mvStreamImg[STREAM_IMG_OUT_OPAQUE] != NULL) {
                    streamImg = STREAM_IMG_OUT_OPAQUE;
                };
                portID = PORT_IMGO;
                dstSize = node.dstSize_full;
                cropRect = node.cropRect_full;
                rawOutFmt = (MUINT32)(((out == REQ_OUT_FULL_PROC) ||
                            ((out == REQ_OUT_FULL_STUFF) &&
                            (mRawDefType == EPipe_PROCESSED_RAW))) ?
                            (EPipe_PROCESSED_RAW) : (EPipe_PURE_RAW));
                if (out == REQ_OUT_FULL_STUFF) {
                    if (mEnableDumpRaw && node.reqType == REQ_TYPE_NORMAL) {
                        // If user wants to dump pure raw, Full Raw can not use height 1
                    } else {
                        dstSize.h = P1_STUFF_BUF_HEIGHT(MFALSE, mConfigPort);
                    }
                    cropRect.s = dstSize;
                };
                break;

            //case REQ_OUT_MAX:
            // for this loop, all cases should be listed
            // and the default is an unreachable path
            /*
            default:
                continue;
            */
        };
        //
        if (streamImg < STREAM_IMG_NUM) {
            pImgStreamInfo = mvStreamImg[streamImg];
        } else {
            MY_LOGW("cannot find the StreamImg num:%d out:%d "
                "streamImg:%d", node.magicNum, out, streamImg);
            return BAD_VALUE;
        }
        if (pImgStreamInfo == NULL) {
            MY_LOGW("cannot find the ImgStreamInfo num:%d out:%d "
                "streamImg:%d", node.magicNum, out, streamImg);
            return BAD_VALUE;
        }
        //
        MERROR err = OK;
        if (out == REQ_OUT_FULL_STUFF || out == REQ_OUT_RESIZER_STUFF ||
            out == REQ_OUT_LCSO_STUFF || out == REQ_OUT_RSSO_STUFF) {
            err = stuffImageGet(node, streamImg, dstSize, pImgBuf);
        } else if (node.reqType == REQ_TYPE_INITIAL) {
            // the initial node with the pool, it do not use the stuff buffer
            err = poolImageGet(node, streamImg, pImgBuf);
        } else { // REQ_TYPE_NORMAL
            if (OK != frameImageGet(node, streamImg, pImgBuf)) {
                #if 1 // keep en-queue/de-queue processing
                if (out == REQ_OUT_LCSO || out == REQ_OUT_RSSO ||
                    (mEnableDumpRaw && (out == REQ_OUT_FULL_PURE ||
                    out == REQ_OUT_FULL_PROC || out == REQ_OUT_FULL_OPAQUE))) {
                    MY_LOGI("keep the output size out:%d", out);
                } else {
                    dstSize.h = P1_STUFF_BUF_HEIGHT(
                        (out == REQ_OUT_RESIZER ? MTRUE : MFALSE), mConfigPort);
                    cropRect.s.h = dstSize.h;
                };
                err = stuffImageGet(node, streamImg, dstSize, pImgBuf);
                if (out == REQ_OUT_RESIZER) {
                    node.expRec |= EXP_REC(EXP_EVT_NOBUF_RRZO);
                } else if (out == REQ_OUT_LCSO) {
                    node.expRec |= EXP_REC(EXP_EVT_NOBUF_LCSO);
                } else if (out == REQ_OUT_RSSO) {
                    node.expRec |= EXP_REC(EXP_EVT_NOBUF_RSSO);
                } else {
                    node.expRec |= EXP_REC(EXP_EVT_NOBUF_IMGO);
                }
                MY_LOGI("underway-stuff-buffer status(%d) out(0x%x) "
                    "stream(%#" PRIx64 ") " P1INFO_NODE_STR, err, out,
                    pImgStreamInfo->getStreamId(), P1INFO_NODE_VAR(node));
                #else
                MY_LOGE("(%d) frameImageGet failed on StreamId=0x%X",
                    node.magicNum, pImgStreamInfo->getStreamId());
                err = BAD_VALUE;
                #endif
            };
        }
        //
        if ((pImgBuf == NULL) || (err != OK)) {
            MY_LOGE("(%d) Cannot get image buffer : (%d)", node.magicNum, err);
            return BAD_VALUE;
        }
        //
        #if (IS_P1_LOGI)
        if (mLogLevelI > 0) {
            strInfo += String8::format(
                "[%s](%d)"
                "[Buf](%dx%d-%d-%d P:%p V:%p)"
                "[Crop](%d,%d-%dx%d)(%dx%d) ",
                ((portID.index == PORT_RRZO.index) ? "RRZ" :
                ((portID.index == PORT_IMGO.index) ? "IMG" :
                ((portID.index == PORT_LCSO.index) ? "LCS" :
                ((portID.index == PORT_RSSO.index) ? "RSS" :
                "UNKNOWN")))),
                out, pImgBuf->getImgSize().w, pImgBuf->getImgSize().h,
                (int)pImgBuf->getBufStridesInBytes(0),
                (int)pImgBuf->getBufSizeInBytes(0),
                (void*)pImgBuf->getBufPA(0), (void*)pImgBuf->getBufVA(0),
                cropRect.p.x, cropRect.p.y, cropRect.s.w, cropRect.s.h,
                dstSize.w, dstSize.h
                );
        }
        #endif
        NSCam::NSIoPipe::NSCamIOPipe::BufInfo rBufInfo(
            portID,
            pImgBuf.get(),
            dstSize,
            cropRect,
            node.magicNum,
            node.sofIdx,
            rawOutFmt);
        info.mvOut.push_back(rBufInfo);
        CAM_TRACE_FMT_END();
    }; // end of the loop for each out
    //
    {

        MSize dstSizeNone = MSize(0, 0);
        MRect cropRectNone = MRect(MPoint(0, 0), MSize(0, 0));
        // EISO
        if (IS_PORT(CONFIG_PORT_EISO, mConfigPort)) {
            sp<IImageBuffer> pImgBuf = NULL;
            // [TODO] get pImgBuf from LMV
            if (IS_LMV(mpConnectLMV)) {
                mpConnectLMV->getBuf(pImgBuf);
            }
            if (pImgBuf == NULL) {
                MY_LOGE("(%d) Cannot get LMV buffer", node.magicNum);
                return BAD_VALUE;
            }
            //MY_LOGD1("GetBufLMV: %p ",pImgBuf->getBufVA(0));
            node.buffer_eiso = pImgBuf;
            #if (IS_P1_LOGI)
            if (mLogLevelI > 0) {
                if (pImgBuf != NULL) {
                    strInfo += String8::format("[LMV](P:%p V:%p) ",
                        (void*)pImgBuf->getBufPA(0),
                        (void*)pImgBuf->getBufVA(0));
                }
            }
            #endif
            NSCam::NSIoPipe::NSCamIOPipe::BufInfo rBufInfo(
                PORT_EISO,
                pImgBuf.get(),
                pImgBuf->getImgSize(),
                MRect(MPoint(0, 0), pImgBuf->getImgSize()),
                node.magicNum,
                node.sofIdx);
            info.mvOut.push_back(rBufInfo);
        }
    }
    mTagEnq.set(node.magicNum);
    P1_LOGI(1, "[P1::ENQ] " P1INFO_NODE_STR " %s",
        P1INFO_NODE_VAR(node), strInfo.string());
    //
    CAM_TRACE_FMT_END();
#endif
    FUNCTION_P1_OUT;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
hardwareOps_enque(
    QueJob_T &job,
    ENQ_TYPE type,
    MINT64 data
)
{
    FUNCTION_P1_IN;
    CAM_TRACE_NAME("P1:enque");

    if (!getActive()) {
        return BAD_VALUE;
    }
    if (mpCamIO == NULL) {
        MY_LOGE("NormalPipe is NULL");
        return BAD_VALUE;
    }
    MY_LOGD3("EnQ[%d](%" PRId64 ") @ (%d)", type, data, job.mFirstMagicNum);
    MBOOL toPush = (type == ENQ_TYPE_INITIAL) ? MFALSE : MTRUE;
    MBOOL toSwitchUNI = MFALSE;
    MUINT8 toSwtTgNum = 0;
#if SUPPORT_ISP
    //
    QBufInfo enBuf;
    QBufInfo* pEnBuf = &enBuf;
    if ((type == ENQ_TYPE_INITIAL) && (!EN_INIT_REQ)) {
        if (mpConCtrl == NULL || (!mpConCtrl->initBufInfo_get(&pEnBuf))
            || pEnBuf == NULL) {
            MY_LOGE("CANNOT get the initBufInfo");
            return BAD_VALUE;
        }
    } else {
        for (size_t i = 0; i < job.mSet.size(); i++) {
            MY_LOGD2("job(%d)(%zu/%zu)", job.mFirstMagicNum, i, job.mSet.size());
            QueNode_T & node = job.mSet.editItemAt(i);
            if (OK != setupNode(node, enBuf)) {
                MY_LOGE("setup enque node fail");
                return BAD_VALUE;
            }
            if (i == 0 && node.reqType == REQ_TYPE_NORMAL) {
                if (type == ENQ_TYPE_DIRECTLY) {
                    node.frameExpDuration = data * ONE_US_TO_NS;
                }
                enBuf.mShutterTimeNs = node.frameExpDuration;
            }
            if (node.uniSwitchState == UNI_SWITCH_STATE_REQ) {
                toSwitchUNI = MTRUE;
            }
            if (node.tgSwitchState == TG_SWITCH_STATE_REQ) {
                toSwtTgNum = node.tgSwitchNum;
            }
            node.exeState = EXE_STATE_PROCESSING;
        }
    }
    //
    if (EN_START_CAP && (!getReady()) && (type == ENQ_TYPE_NORMAL)) {
        Mutex::Autolock _l(mStartCaptureLock);
        MY_LOGD2("StartCaptureState(%d)", mStartCaptureState);
        if (mStartCaptureState == START_CAP_STATE_WAIT_CB) {
            CAM_TRACE_BEGIN("StartCapture wait");
            mStartCaptureCond.wait(mStartCaptureLock);
            CAM_TRACE_END();
        }
        job.mSet.editItemAt(0).capType = mStartCaptureType;
        job.mSet.editItemAt(0).frameExpDuration = mStartCaptureExp;
        job.mSet.editItemAt(0).sofIdx = mStartCaptureIdx;
        for (size_t i = 0; i < pEnBuf->mvOut.size(); i++) {
            pEnBuf->mvOut[i].FrameBased.mSOFidx = mStartCaptureIdx;
        }
        pEnBuf->mShutterTimeNs = mStartCaptureExp;
    }
    //
    if (toSwitchUNI) {
        UNI_SWITCH_STATE uniState = UNI_SWITCH_STATE_REQ;
        MUINT32 switchState = 0;
        MBOOL res = MFALSE;
        if (mpCamIO->sendCommand(ENPipeCmd_GET_UNI_SWITCH_STATE,
            (MINTPTR)(&switchState), (MINTPTR)NULL, (MINTPTR)NULL) &&
            switchState == 0) { // DRV: If switch state is NULL, then do switch.
            res = mpCamIO->sendCommand(ENPipeCmd_UNI_SWITCH,
                (MINTPTR)NULL, (MINTPTR)NULL, (MINTPTR)NULL);
            if (res) {
                uniState = UNI_SWITCH_STATE_ACT_ACCEPT;
            } else {
                uniState = UNI_SWITCH_STATE_ACT_IGNORE;
            }
        } else {
            uniState = UNI_SWITCH_STATE_ACT_REJECT;
        }
        //
        for (size_t i = 0; i < job.mSet.size(); i++) {
            QueNode_T & node = job.mSet.editItemAt(i);
            if (node.uniSwitchState == UNI_SWITCH_STATE_REQ) {
                node.uniSwitchState = uniState;
                MY_LOGD("UNI-Switch(%d)(%d,%d) drv(%d,%d):(%d)", node.magicNum,
                    P1GET_FRM_NUM(node.appFrame), P1GET_REQ_NUM(node.appFrame),
                    switchState, res, uniState);
            }
        }
    }
    //
    if (toSwtTgNum) {
        TG_SWITCH_STATE tgState = TG_SWITCH_STATE_DONE_IGNORE;
        MBOOL res = MFALSE;
        MBOOL ret = MFALSE;
        MBOOL rev = MFALSE;
        MBOOL isOn = MFALSE;
        NSCam::NSIoPipe::NSCamIOPipe::E_CamHwPathCfg curCfg = eCamHwPathCfg_Num;
        NSCam::NSIoPipe::NSCamIOPipe::E_CamHwPathCfg tarCfg = eCamHwPathCfg_Num;
        switch (toSwtTgNum) {
            case 1:
                tarCfg = NSCam::NSIoPipe::NSCamIOPipe::eCamHwPathCfg_One_TG;
                break;
            case 2:
                tarCfg = NSCam::NSIoPipe::NSCamIOPipe::eCamHwPathCfg_Two_TG;
                break;
            default:
                MY_LOGI("check node TG state num (%d)", toSwtTgNum);
                break;
        }
        if (mpCamIO != NULL) {
            res = mpCamIO->sendCommand (ENPipeCmd_GET_DTwin_INFO,
                (MINTPTR)(&isOn), (MINTPTR)NULL, (MINTPTR)NULL);
            if (res && isOn) {
                ret = mpCamIO->sendCommand(ENPipeCmd_GET_HW_PATH_CFG,
                    (MINTPTR)(&curCfg), (MINTPTR)NULL, (MINTPTR)NULL);
            }
            if (!res) {
                MY_LOGI("sendCmd ENPipeCmd_GET_DTwin_INFO (%d)", res);
            } else if (!isOn) {
                MY_LOGI("DynamicTwin not ready (%d)", isOn);
            } else if (!ret) {
                MY_LOGI("sendCmd ENPipeCmd_GET_HW_PATH_CFG (%d)", ret);
            } else if (curCfg == eCamHwPathCfg_Num) {
                MY_LOGI("check current num (%d)", curCfg);
            } else if (tarCfg == eCamHwPathCfg_Num) {
                MY_LOGI("check target num (%d)", tarCfg);
            } else if (curCfg == tarCfg) {
                MY_LOGI("CamHwPathCfg is ready (%d) == (%d)", curCfg, tarCfg);
            } else {
                rev = mpCamIO->sendCommand(ENPipeCmd_SET_HW_PATH_CFG,
                    (MINTPTR)(tarCfg), (MINTPTR)NULL, (MINTPTR)NULL);
                if (!rev) {
                    MY_LOGI("sendCmd ENPipeCmd_SET_HW_PATH_CFG (%d)", rev);
                    tgState = TG_SWITCH_STATE_DONE_REJECT;
                } else {
                    tgState = TG_SWITCH_STATE_DONE_ACCEPT;
                }
            }
        }
        //
        for (size_t i = 0; i < job.mSet.size(); i++) {
            QueNode_T & node = job.mSet.editItemAt(i);
            if (node.tgSwitchState == TG_SWITCH_STATE_REQ) {
                node.tgSwitchState = tgState;
                node.tgSwitchNum = 0;
                MY_LOGI("TG(%d)(%d,%d) Drv(%d) Swt(%d)(%d,%d)(%d,%d,%d):%d",
                    node.magicNum,
                    P1GET_FRM_NUM(node.appFrame), P1GET_REQ_NUM(node.appFrame),
                    isOn, toSwtTgNum, curCfg, tarCfg, res, ret, rev, tgState);
            }
        }
    }
    //
    if (toPush) {
        Mutex::Autolock _l(mProcessingQueueLock);
        mProcessingQueue.push_back(job);
        MY_LOGD2("Push(%d) to ProQ(%zu)",
            job.mFirstMagicNum, mProcessingQueue.size());
    }
    //
    MBOOL isErr = MFALSE;
    MUINT32 numF = P1GET_FRM_NUM(job.mSet[0].appFrame);
    MUINT32 numR = P1GET_REQ_NUM(job.mSet[0].appFrame);
    if (type == ENQ_TYPE_DIRECTLY) {
        CAM_TRACE_FMT_BEGIN("P1:DRV-resume(%d)(%d,%d)@%d",
            job.mSet[0].magicNum, numF, numR, job.mSet[0].sofIdx);
        MY_LOGD2("mpCamIO->resume +++");
        if(!mpCamIO->resume((QBufInfo const *)(pEnBuf))) {
            MY_LOGE("resume fail");
            isErr = MTRUE;
        }
        MY_LOGD2("mpCamIO->resume ---");
        CAM_TRACE_FMT_END();
    } else { // ENQ_TYPE_NORMAL / ENQ_TYPE_INITIAL
        CAM_TRACE_FMT_BEGIN("P1:DRV-enque(%d)(%d,%d)@%d",
            job.mSet[0].magicNum, numF, numR, job.mSet[0].sofIdx);
        MY_LOGD2("mpCamIO->enque +++");
        if(!mpCamIO->enque(*pEnBuf)) {
            MY_LOGE("enque fail");
            isErr = MTRUE;
        }
        MY_LOGD2("mpCamIO->enque ---");
        CAM_TRACE_FMT_END();
    }
    //
    if (isErr) {
        if (toPush) {
            Mutex::Autolock _l(mProcessingQueueLock);
            Que_T::iterator it = mProcessingQueue.begin();
            for (; it != mProcessingQueue.end(); it++) {
                if ((*it).mFirstMagicNum == job.mFirstMagicNum) {
                    break;
                }
            }
            if (it != mProcessingQueue.end()) {
                mProcessingQueue.erase(it);
            }
            MY_LOGD2("Erase(%d) from ProQ(%zu)",
                job.mFirstMagicNum, mProcessingQueue.size());
        }
        return BAD_VALUE;
    }
    //
    if (type == ENQ_TYPE_INITIAL) {
        mpConCtrl->initBufInfo_clean();
    } else if (type == ENQ_TYPE_DIRECTLY) {
        if (mpHwStateCtrl != NULL) {
            mpHwStateCtrl->checkThreadWeakup();
        }
    }
#endif
    FUNCTION_P1_OUT;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
hardwareOps_deque(QBufInfo &deqBuf)
{

#if SUPPORT_ISP

    FUNCTION_P1_IN;
    //CAM_TRACE_NAME("P1:deque");

    if (!getActive()) {
        return BAD_VALUE;
    }

    Mutex::Autolock _l(mHardwareLock);
    if (!getActive()) {
        return BAD_VALUE;
    }

    {
        // deque buffer, and handle frame and metadata
        MY_LOGD2("%" PRId64 ", %f", mDequeThreadProfile.getAvgDuration(), mDequeThreadProfile.getFps());
        QPortID PortID;
        if (IS_PORT(CONFIG_PORT_IMGO, mConfigPort)) {//(mvOutImage_full.size() > 0) {
            PortID.mvPortId.push_back(PORT_IMGO);
        }
        if (IS_PORT(CONFIG_PORT_RRZO, mConfigPort)) {//(mOutImage_resizer != NULL) {
            PortID.mvPortId.push_back(PORT_RRZO);
        }
        if (IS_PORT(CONFIG_PORT_EISO, mConfigPort)) {
            PortID.mvPortId.push_back(PORT_EISO);
        }
        if (IS_PORT(CONFIG_PORT_LCSO, mConfigPort)) {
            PortID.mvPortId.push_back(PORT_LCSO);
        }
        if (IS_PORT(CONFIG_PORT_RSSO, mConfigPort)) {
            PortID.mvPortId.push_back(PORT_RSSO);
        }

        // for mBurstNum: 4 and port: I+R+E+L, the buffer is as
        // [I1][I2][I3][I4][R1][R2][R3][R4][E1][E2][E3][E4][L1][L2][L3][L4]
        mDequeThreadProfile.pulse_down();
        //
        CAM_TRACE_FMT_BEGIN("P1:DRV-deque@[0x%X]", mConfigPort);
        MY_LOGD2("mpCamIO->deque +++");
        if(!mpCamIO->deque(PortID, deqBuf)) {
            if(getActive()) {
                MY_LOGE("deque fail");
            } else {
                MY_LOGW("deque fail - after stop");
                return OK;
            }
            CAM_TRACE_FMT_END();
            AEE_ASSERT("\nCRDISPATCH_KEY:MtkCam/P1Node:ISP pass1 deque fail");
            return BAD_VALUE;
        }
        MY_LOGD2("mpCamIO->deque ---");
        CAM_TRACE_FMT_END();
        //
        mDequeThreadProfile.pulse_up();
    }
    //
    if( mDebugScanLineMask != 0 &&
        mpDebugScanLine != NULL)
    {
        for(size_t i = 0; i < deqBuf.mvOut.size(); i++)
        {
            if( (   deqBuf.mvOut[i].mPortID.index == PORT_RRZO.index &&
                    mDebugScanLineMask & DRAWLINE_PORT_RRZO  ) ||
                (   deqBuf.mvOut[i].mPortID.index == PORT_IMGO.index &&
                    mDebugScanLineMask & DRAWLINE_PORT_IMGO  )   )
            {
                mpDebugScanLine->drawScanLine(
                                    deqBuf.mvOut[i].mBuffer->getImgSize().w,
                                    deqBuf.mvOut[i].mBuffer->getImgSize().h,
                                    (void*)(deqBuf.mvOut[i].mBuffer->getBufVA(0)),
                                    deqBuf.mvOut[i].mBuffer->getBufSizeInBytes(0),
                                    deqBuf.mvOut[i].mBuffer->getBufStridesInBytes(0));

            }
        }
    }
    static bool shouldPrint = false;
    if (shouldPrint) {
        for(size_t i = 0; i < deqBuf.mvOut.size(); i++) {
            char filename[256];
            sprintf(filename, "/data/P1_%d_%d_%d.raw", deqBuf.mvOut.at(i).mMetaData.mMagicNum_hal,
                deqBuf.mvOut.at(i).mBuffer->getImgSize().w,
                deqBuf.mvOut.at(i).mBuffer->getImgSize().h);
            NSCam::Utils::saveBufToFile(filename, (unsigned char*)deqBuf.mvOut.at(i).mBuffer->getBufVA(0), deqBuf.mvOut.at(i).mBuffer->getBufSizeInBytes(0));
            shouldPrint = false;
        }
    }
    #if 1
    if (mEnableDumpRaw && deqBuf.mvOut.size() > 0) {
        MUINT32 magicNum = deqBuf.mvOut.at(0).mMetaData.mMagicNum_hal;

        /* Record previous "debug.p1.pureraw_dump" prop value.
        * When current prop value is not equal to previous prop value, it will start dump raw.
        * When current prop value is > 0 value, it will dump continuous raw.
        * For example, assume current prop value is 10 ,it will start continuous 10 raw dump.
        */
        static MINT32 prevDumpProp = 0;
        static MUINT32 continueDumpCount = 0;

        /* If current "debug.p1.pureraw_dump" prop value < 0, this variable will save it.
        * This variable is used to continuous magic number dump raws.
        * For example, assume current prop value is -20. When pipeline starts, it will dump frames with magic num < 20.
        */
        static MUINT32 indexRawDump = 0;

        MINT32 currentDumpProp = property_get_int32("debug.p1.pureraw_dump",0);

        if (prevDumpProp != currentDumpProp)
        {
            if(currentDumpProp == 0 ){
                prevDumpProp = 0;
                indexRawDump = 0;
                continueDumpCount = 0;
            } else if(currentDumpProp < 0){
                indexRawDump = (MUINT32)(-currentDumpProp);
            } else if(currentDumpProp > 0){
                continueDumpCount = (MUINT32)currentDumpProp;
            }
            prevDumpProp = currentDumpProp;
        }

        if ( (magicNum <= indexRawDump) || continueDumpCount > 0 )
        {
            if(continueDumpCount > 0)
                continueDumpCount--;

            for(size_t i = 0; i < deqBuf.mvOut.size(); i++) {
                char filename[256] = {0};
                sprintf(filename, "/sdcard/raw/p1_%u_%d_%04dx%04d_%04d_%d.raw",
                    magicNum,
                    ((deqBuf.mvOut.at(i).mPortID.index == PORT_RRZO.index) ?
                    (0) : (1)),
                    (int)deqBuf.mvOut.at(i).mBuffer->getImgSize().w,
                    (int)deqBuf.mvOut.at(i).mBuffer->getImgSize().h,
                    (int)deqBuf.mvOut.at(i).mBuffer->getBufStridesInBytes(0),
                    (int)mSensorFormatOrder );
                deqBuf.mvOut.at(i).mBuffer->saveToFile(filename);
                MY_LOGI("save to file : %s", filename);
            }
        }
    }
    #endif


    FUNCTION_P1_OUT;

    return OK;
#else
    return OK;
#endif

}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
hardwareOps_stop()
{
#if SUPPORT_ISP
    CAM_TRACE_NAME("P1:hardwareOps_stop");

    //(1) handle active flag
    if (!getActive()) {
        MY_LOGD("active=%d - return", getActive());
        return OK;
    }

    FUNCTION_P1_IN;
    MY_LOGI("Cam::%d Req=%d Set=%d Enq=%d Deq=%d Out=%d", getOpenId(),
        mTagReq.get(), mTagSet.get(), mTagEnq.get(), mTagDeq.get(),
        mTagOut.get());

    setActive(MFALSE);
    setReady(MFALSE);

    if (getInit()) {
        MY_LOGI("mHardwareLock waiting +++");
        Mutex::Autolock _l(mHardwareLock);
        MY_LOGI("mHardwareLock waiting ---");
    }

    if (mpHwStateCtrl != NULL) {
        mpHwStateCtrl->reset();
    }

    //(2.2) stop 3A stt
    #if SUPPORT_3A
    CAM_TRACE_BEGIN("P1:3A-stopStt");
    MY_LOGI1("mp3A->stopStt +++");
    if (mp3A) {
        Mutex::Autolock _sl(mStopSttLock);
        mp3A->stopStt();
    }
    MY_LOGI1("mp3A->stopStt ---");
    CAM_TRACE_END();
    #endif

    //(2.3) stop isp
    if (mpCamIO == NULL) {
        MY_LOGE("hardware CamIO not exist");
        return BAD_VALUE;
    }
    {
        //Mutex::Autolock _l(mHardwareLock);
        if (mLongExp.get()) {
            CAM_TRACE_BEGIN("P1:DRV-abort");
            MY_LOGI1("mpCamIO->abort +++");
            if(!mpCamIO->abort()) {
                MY_LOGE("hardware abort fail");
                //return BAD_VALUE;
            }
            MY_LOGI1("mpCamIO->abort ---");
            CAM_TRACE_END();
        } else {
            CAM_TRACE_BEGIN("P1:DRV-stop");
            MY_LOGI1("mpCamIO->stop +++");
            if(!mpCamIO->stop(MTRUE)) {
                MY_LOGE("hardware stop fail");
                //return BAD_VALUE;
            }
            MY_LOGI1("mpCamIO->stop ---");
            CAM_TRACE_END();
        }
        /*
        if(IS_PORT(CONFIG_PORT_RRZO, mConfigPort))
            mpCamIO->abortDma(PORT_RRZO,getNodeName());
        if(IS_PORT(CONFIG_PORT_IMGO, mConfigPort))
            mpCamIO->abortDma(PORT_IMGO,getNodeName());
        if(IS_PORT(CONFIG_PORT_EISO, mConfigPort))
            mpCamIO->abortDma(PORT_EISO,getNodeName());
        if(IS_PORT(CONFIG_PORT_LCSO, mConfigPort))
            mpCamIO->abortDma(PORT_LCSO,getNodeName());
        */
    }

    MY_LOGI1("HwLockStopWait +++");
    Mutex::Autolock _l(mHardwareLock);
    MY_LOGI1("HwLockStopWait ---");

    //(3.0) stop 3A
    #if SUPPORT_3A
    if (mp3A) {
        CAM_TRACE_BEGIN("P1:LMV-enableOIS");
        if (IS_LMV(mpConnectLMV)) {
            mpConnectLMV->enableOIS(mp3A);
        }
        if (EIS_MODE_IS_EIS_ADVANCED_ENABLED(EisInfo::getEISMode(mPackedEisInfo)) &&
            mSensorParams.vhdrMode == SENSOR_VHDR_MODE_NONE &&
            !mDisableAEEIS)
        {
            mp3A ->send3ACtrl(E3ACtrl_SetAEEISRecording, 0, 0);
        }
        CAM_TRACE_END();
        //
        mp3A->detachCb(IHal3ACb::eID_NOTIFY_3APROC_FINISH, this);
        mp3A->detachCb(IHal3ACb::eID_NOTIFY_CURR_RESULT, this);
        mp3A->detachCb(IHal3ACb::eID_NOTIFY_VSYNC_DONE, this);
        CAM_TRACE_BEGIN("P1:3A-stop");
        MY_LOGI1("mp3A->stop +++");
        mp3A->stop();
        MY_LOGI1("mp3A->stop ---");
        CAM_TRACE_END();
    }
    #endif

    //(3.1) destroy 3A
    #if SUPPORT_3A
    CAM_TRACE_BEGIN("P1:3A-destroy");
    if (mp3A) {
        MY_LOGI1("mp3A->destroyInstance +++");
        mp3A->destroyInstance(getNodeName());
        MY_LOGI1("mp3A->destroyInstance ---");
        mp3A = NULL;
    }
    CAM_TRACE_END();
    #endif

    //(3.2) destroy isp
    {
        if (IS_LMV(mpConnectLMV)) {
            mpConnectLMV->uninit();
        }
        //
        #if SUPPORT_VHDR
        if(mpVhdr)
        {
            mpVhdr->Uninit();
            //mpVhdr->DestroyInstance(LOG_TAG); // instance always exist until process kill
            mpVhdr = NULL;
        }
        #endif
        #if SUPPORT_LCS
        if(mpLCS)
        {
            mpLCS->Uninit();
            mpLCS->DestroyInstance(LOG_TAG); // instance always exist until process kill
            mpLCS = NULL;
        }
        #endif
        //
        CAM_TRACE_BEGIN("P1:DRV-uninit");
        MY_LOGI1("mpCamIO->uninit +++");
        if(!mpCamIO->uninit() )
        {
            MY_LOGE("hardware uninit fail");
            //return BAD_VALUE;
        }
        MY_LOGI1("mpCamIO->uninit ---");
        MY_LOGI1("mpCamIO->destroyInstance +++");
        #ifdef USING_MTK_LDVT
        mpCamIO->destroyInstance("iopipeUseTM");
        #else
        mpCamIO->destroyInstance(getNodeName());
        #endif
        MY_LOGI1("mpCamIO->destroyInstance ---");
        mpCamIO = NULL;
        CAM_TRACE_END();
    }
    //
    {
        Mutex::Autolock _ll(mFrameSetLock);
        mFrameSetAlready = MFALSE;
    }
    //
    FUNCTION_P1_OUT;

    return OK;

#else
    return OK;
#endif

}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
hardwareOps_standby(List<MetaSet_T> *list, Mutex *listLock)
{
    MERROR status = OK;
    if (mpHwStateCtrl != NULL) {
        MBOOL isSkipEnQ = (mBurstNum > 1) ? MTRUE : MFALSE;
        MBOOL isNeedEnQ = MFALSE;
        MINT32 iShutterTimeUs = 0;
        MINT32 iLastNum = get_last_magicnum();
        size_t reqQueSize = 0;
        if ((mIsLegacyStandbyMode) || (mForceStandbyMode != 0)) {
            isSkipEnQ = MTRUE;
        }
        mpHwStateCtrl->checkReceiveNode(iLastNum, isSkipEnQ,
            isNeedEnQ, iShutterTimeUs);
        if ((!isSkipEnQ) && (isNeedEnQ)) {
            MY_LOGI("StandyEnQ(%d:%d)-Num(%d)", isSkipEnQ, isNeedEnQ, iLastNum);
            QueJob_T job(mBurstNum);
            {
                Mutex::Autolock _l(mRequestQueueLock);
                reqQueSize = mRequestQueue.size();
                Que_T::iterator it = mRequestQueue.begin();
                for(; it != mRequestQueue.end(); it++) {
                    if (it->mFirstMagicNum == iLastNum) {
                        job = *it;
                        mRequestQueue.erase(it);
                        break;
                    }
                }
            }
            if (iLastNum != job.mFirstMagicNum) {
                MY_LOGE("No Job Ready - LastNum(%d) JobNum(%d) ReqQue[%zu]",
                    iLastNum, job.mFirstMagicNum, reqQueSize);
                return BAD_VALUE;
            }
            // prepare to enq with this request
            if (list != NULL) {
                if (listLock != NULL) {
                    listLock->lock();
                }
                mLastSetNum = iLastNum;
                mTagSet.set(iLastNum);
                #if 0 // skip the first frame after re-streaming
                MY_LOGI("skip frame by (%d)@[%zu]", mLastSetNum, list->size());
                #else
                if (!list->empty()) {
                    list->erase(list->begin());
                }
                #endif
                if (listLock != NULL) {
                    listLock->unlock();
                }
            }
            hardwareOps_enque(job, ENQ_TYPE_DIRECTLY, (MINT64)iShutterTimeUs);
            //
            mpHwStateCtrl->checkFirstSync();
            mpHwStateCtrl->checkNotePass();
        }
    }
    return status;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
prepareCropInfo(
    IMetadata* pAppMetadata,
    IMetadata* pHalMetadata,
    QueNode_T& node)
{
    MSize refSensorSize = mSensorParams.size;
    MBOOL isFullBin = MFALSE;
    if (mIsBinEn) {
        BIN_RESIZE(refSensorSize.w);
        BIN_RESIZE(refSensorSize.h);
        if (node.reqType == REQ_TYPE_NORMAL &&
            (IS_OUT(REQ_OUT_FULL_PROC, node.reqOutSet))) {
            isFullBin = MTRUE;
        }
    }
    MY_LOGD2("IsBinEn:%d IsFullBin:%d sensor(%dx%d) ref(%dx%d)",
        mIsBinEn, isFullBin,
        mSensorParams.size.w, mSensorParams.size.h,
        refSensorSize.w, refSensorSize.h);
    if (mvStreamImg[STREAM_IMG_OUT_FULL] != NULL) {
        node.dstSize_full = mvStreamImg[STREAM_IMG_OUT_FULL]->getImgSize();
        node.cropRect_full = MRect(MPoint(0, 0),
            (isFullBin) ? refSensorSize : mSensorParams.size);
    } else if (mvStreamImg[STREAM_IMG_OUT_OPAQUE] != NULL) {
        node.dstSize_full = mSensorParams.size;
        node.cropRect_full = MRect(MPoint(0, 0),
            (isFullBin) ? refSensorSize : mSensorParams.size);
    } else {
        node.dstSize_full = MSize(0, 0);
        node.cropRect_full = MRect(MPoint(0, 0), MSize(0, 0));
    }
    if (mvStreamImg[STREAM_IMG_OUT_RESIZE] != NULL) {
        node.dstSize_resizer = mvStreamImg[STREAM_IMG_OUT_RESIZE]->getImgSize();
        node.cropRect_resizer = MRect(MPoint(0, 0), refSensorSize);
    } else {
        node.dstSize_resizer= MSize(0, 0);
        node.cropRect_resizer = MRect(MPoint(0, 0), MSize(0, 0));
    }
    //
    if (pAppMetadata != NULL && pHalMetadata != NULL) {
        MRect cropRect_metadata;    // get from metadata
        MRect cropRect_control;     // set to node

        if( !tryGetMetadata<MRect>(pAppMetadata, MTK_SCALER_CROP_REGION, cropRect_metadata) ) {
            MY_LOGI("Metadata exist - no MTK_SCALER_CROP_REGION, "
                "crop size set to full(%dx%d) resizer(%dx%d)",
                node.dstSize_full.w, node.dstSize_full.h,
                node.dstSize_resizer.w, node.dstSize_resizer.h);
        } else {
            if( !tryGetMetadata<MRect>(pHalMetadata, MTK_P1NODE_SENSOR_CROP_REGION, cropRect_control) )
            {
                MY_LOGD2("cannot get MTK_P1NODE_SENSOR_CROP_REGION, use MTK_SCALER_CROP_REGION");
                if (mIsBinEn) {
                    BIN_RESIZE(cropRect_metadata.p.x);
                    BIN_RESIZE(cropRect_metadata.p.y);
                    BIN_RESIZE(cropRect_metadata.s.w);
                    BIN_RESIZE(cropRect_metadata.s.h);
                }
                simpleTransform tranActive2Sensor = simpleTransform(
                        MPoint(0,0), mActiveArray.size(), mSensorParams.size);
                cropRect_control.p = transform(tranActive2Sensor,
                                                cropRect_metadata.leftTop());
                cropRect_control.s = transform(tranActive2Sensor,
                                                cropRect_metadata.size());
            }
            else
            {
                MY_LOGD2("get MTK_P1NODE_SENSOR_CROP_REGION success");
                if (mIsBinEn) {
                    BIN_RESIZE(cropRect_control.p.x);
                    BIN_RESIZE(cropRect_control.p.y);
                    BIN_RESIZE(cropRect_control.s.w);
                    BIN_RESIZE(cropRect_control.s.h);
                }
            }
            MY_LOGD2("crop size set to (%d,%d,%dx%d))",
                cropRect_control.p.x, cropRect_control.p.y,
                cropRect_control.s.w, cropRect_control.s.h);

            if (IS_LMV(mpConnectLMV)) {
                mpConnectLMV->adjustCropInfo(pAppMetadata, pHalMetadata,
                    cropRect_control, refSensorSize,//mSensorParams.size,
                    mEnableFrameSync, mStereoCamMode);
            };

            /*
            MY_LOGD("[CropInfo] metadata(%d, %d, %dx%d) "
                "control(%d, %d, %dx%d) "
                "active(%d, %d, %dx%d) "
                "sensor(%dx%d)",
                cropRect_metadata.leftTop().x,
                cropRect_metadata.leftTop().y,
                cropRect_metadata.size().w, cropRect_metadata.size().h,
                cropRect_control.leftTop().x,
                cropRect_control.leftTop().y,
                cropRect_control.size().w, cropRect_control.size().h,
                mActiveArray.leftTop().x,
                mActiveArray.leftTop().y,
                mActiveArray.size().w, mActiveArray.size().h,
                mSensorParams.size.w, mSensorParams.size.h);
            */
            // TODO: check more case about crop region
            if ((cropRect_control.size().w < 0) ||
                (cropRect_control.size().h < 0) ||
                (cropRect_control.leftTop().x < 0) ||
                (cropRect_control.leftTop().y < 0) ||
                (cropRect_control.leftTop().x >= refSensorSize.w) ||
                (cropRect_control.leftTop().y >= refSensorSize.h)) {
                MY_LOGW("Metadata exist - invalid cropRect_control"
                    "(%d, %d, %dx%d) sensor(%dx%d)",
                    cropRect_control.leftTop().x,
                    cropRect_control.leftTop().y,
                    cropRect_control.size().w, cropRect_control.size().h,
                    refSensorSize.w, refSensorSize.h);
                return;
            }
            if ((cropRect_control.p.x + cropRect_control.s.w) >
                refSensorSize.w) {
                cropRect_control.s.w = refSensorSize.w -
                                        cropRect_control.p.x;
            }
            if ((cropRect_control.p.y + cropRect_control.s.h) >
                refSensorSize.h) {
                cropRect_control.s.h = refSensorSize.h -
                                        cropRect_control.p.y;
            }
            // calculate the crop region validity
            if (mvStreamImg[STREAM_IMG_OUT_FULL] != NULL) {
                MRect cropRect_full = cropRect_control;
                if (isFullBin) {
                    BIN_REVERT(cropRect_full.p.x);
                    BIN_REVERT(cropRect_full.p.y);
                    BIN_REVERT(cropRect_full.s.w);
                    BIN_REVERT(cropRect_full.s.h);
                }
                calculateCropInfoFull(
                    mSensorParams.pixelMode,
                    (isFullBin) ? refSensorSize : mSensorParams.size,
                    mvStreamImg[STREAM_IMG_OUT_FULL]->getImgSize(),
                    (isFullBin) ? (cropRect_control) :
                    cropRect_full,
                    node.cropRect_full,
                    node.dstSize_full,
                    mLogLevel);
            }
            if (mvStreamImg[STREAM_IMG_OUT_RESIZE] != NULL) {
                calculateCropInfoResizer(
                    mSensorParams.pixelMode,
                    (mvStreamImg[STREAM_IMG_OUT_RESIZE]->getImgFormat()),
                    refSensorSize,
                    (mvStreamImg[STREAM_IMG_OUT_RESIZE]->getImgSize()),
                    cropRect_control,
                    node.cropRect_resizer,
                    node.dstSize_resizer,
                    mLogLevel);
            }
        }
    }
    node.dstSize_full.w =
        MIN(node.dstSize_full.w, node.cropRect_full.s.w);
    node.dstSize_full.h =
        MIN(node.dstSize_full.h, node.cropRect_full.s.h);
    node.dstSize_resizer.w =
        MIN(node.dstSize_resizer.w, node.cropRect_resizer.s.w);
    node.dstSize_resizer.h =
        MIN(node.dstSize_resizer.h, node.cropRect_resizer.s.h);
    MY_LOGD2("Crop-Info F(%d,%d,%dx%d)(%dx%d) R(%d,%d,%dx%d)(%dx%d)",
            node.cropRect_full.p.x, node.cropRect_full.p.y,
            node.cropRect_full.s.w, node.cropRect_full.s.h,
            node.dstSize_full.w, node.dstSize_full.h,
            node.cropRect_resizer.p.x, node.cropRect_resizer.p.y,
            node.cropRect_resizer.s.w, node.cropRect_resizer.s.h,
            node.dstSize_resizer.w, node.dstSize_resizer.h);
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
receiveFrame(sp<IPipelineFrame> appFrame, QueNode_T & node)
{
    MetaSet_T metaInfo;
    IMetadata* pAppMeta = NULL;
    IMetadata* pHalMeta = NULL;
    CAM_TRACE_FMT_BEGIN("receiveFrame(%d,%d)",
        P1GET_FRM_NUM(appFrame), P1GET_REQ_NUM(appFrame));
    if (appFrame != NULL) {
        node.appFrame = appFrame;
        findRequestStream(node);
        //CAM_TRACE_FMT_BEGIN("prepareMeta");
        if (mvStreamMeta[STREAM_META_IN_APP] != NULL) {
            if (OK == frameMetadataGet(
                node, STREAM_META_IN_APP, &metaInfo.appMeta)) {
                pAppMeta = &(metaInfo.appMeta);
            } else {
                MY_LOGI("can not lock the app metadata");
            }
        }
        if (mvStreamMeta[STREAM_META_IN_HAL] != NULL) {
            if (OK == frameMetadataGet(
                node, STREAM_META_IN_HAL, &metaInfo.halMeta)) {
                pHalMeta = &(metaInfo.halMeta);
            } else {
                MY_LOGI("can not lock the hal metadata");
            }
        }
        //CAM_TRACE_FMT_END();
    }
    MY_LOGD2("receiveFrame(%d,%d) cnt(%d,%d)",
        P1GET_FRM_NUM(appFrame), P1GET_REQ_NUM(appFrame),
        (pAppMeta)?pAppMeta->count() : 0, (pHalMeta)?pHalMeta->count() : 0);
    #if 0
    if (pAppMeta != NULL) {
    }
    #endif
    if (pHalMeta != NULL) {
        if (mpHwStateCtrl != NULL) {
            mpHwStateCtrl->checkReceiveFrame(pHalMeta);
        }
    }
    //
    CAM_TRACE_FMT_END();
    return;
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
createNode(sp<IPipelineFrame> appframe,
           QueNode_T *pNode,
           QueJob_T *job,
           Que_T *Queue,
           Mutex *QueLock,
           List<MetaSet_T> *list,
           Mutex *listLock)
{
    //create queue node
    MUINT32 newNum = get_and_increase_magicnum();
    MetaSet_T metaInfo;
    IMetadata* pAppMeta = NULL;
    IMetadata* pHalMeta = NULL;
    //
    MINT32 meta_raw_type = (MINT32)mRawDefType;
    MBOOL meta_raw_exist = MFALSE;
    //
    QueNode_T newNode;
    QueNode_T & node = ((pNode != NULL) ? (*pNode) : (newNode));
    //
    CAM_TRACE_FMT_BEGIN("Create(%d)(%d,%d)", newNum,
        P1GET_FRM_NUM(appframe), P1GET_REQ_NUM(appframe));
    //
    node.magicNum = newNum;
    /*
    // set as constructor
    node.sofIdx = P1SOFIDX_INIT_VAL;
    node.appFrame = NULL;
    node.reqType = REQ_TYPE_UNKNOWN;
    node.reqOutSet = REQ_SET_NONE;
    node.expRec = EXP_REC_NONE;
    node.exeState = EXE_STATE_NULL;
    */
    //
    if (appframe != NULL) { // for REQ_TYPE_NORMAL
        mTagReq.set(newNum);
        if (node.appFrame != appframe) { // node.appFrame == NULL
            node.appFrame = appframe;
            findRequestStream(node);
            MY_LOGI("CreateNode(%d,%d) assign frame",
                P1GET_FRM_NUM(appframe), P1GET_REQ_NUM(appframe));
        }
        //
        //CAM_TRACE_FMT_BEGIN("createMeta");
        if (mvStreamMeta[STREAM_META_IN_APP] != NULL) {
            if (OK == frameMetadataGet(
                node, STREAM_META_IN_APP, &metaInfo.appMeta)) {
                pAppMeta = &(metaInfo.appMeta);
            } else {
                MY_LOGI("can not lock the app metadata");
            }
        }
        if (mvStreamMeta[STREAM_META_IN_HAL] != NULL) {
            if (OK == frameMetadataGet(
                node, STREAM_META_IN_HAL, &metaInfo.halMeta)) {
                pHalMeta = &(metaInfo.halMeta);
            } else {
                MY_LOGI("can not lock the hal metadata");
            }
        }
        //CAM_TRACE_FMT_END();
        #if 1 // add raw type from hal meta
        if (pHalMeta != NULL) {
            MINT32 raw_type = meta_raw_type;
            if (mEnableDualPD == MFALSE && // DualPD must be Driver Selected raw
                tryGetMetadata<MINT32>(
                pHalMeta, MTK_P1NODE_RAW_TYPE, raw_type)) {
                MY_LOGD1("raw type set from outside %d", raw_type);
                if ((mRawOption & (MUINT32)(1 << raw_type)) > 0) {
                    meta_raw_type = raw_type;
                    meta_raw_exist = MTRUE;
                }
            }
            //
            if (IS_LMV(mpConnectLMV) &&
                mpConnectLMV->checkSwitchOut(pHalMeta)) {
                node.uniSwitchState = UNI_SWITCH_STATE_REQ;
            }
            //
            if (mIsDynamicTwinEn) {
                MINT32 tg_num = MTK_P1_TWIN_SWITCH_NONE;
                tryGetMetadata<MINT32>(
                    pHalMeta, MTK_P1NODE_TWIN_SWITCH, tg_num);
                if (tg_num != MTK_P1_TWIN_SWITCH_NONE) {
                    node.tgSwitchState = TG_SWITCH_STATE_REQ;
                    switch (tg_num) {
                        case MTK_P1_TWIN_SWITCH_ONE_TG:
                            node.tgSwitchNum = 1;
                            break;
                        case MTK_P1_TWIN_SWITCH_TWO_TG:
                            node.tgSwitchNum = 2;
                            break;
                        default:
                            MY_LOGI("check MTK_P1NODE_TWIN_SWITCH %d", tg_num);
                            break;
                    }
                }
            }
        }
        #endif
        //
        if (node.streamBufImg[STREAM_IMG_IN_YUV].bExist) {
            node.reqType = REQ_TYPE_YUV;
        } else if (node.streamBufImg[STREAM_IMG_IN_OPAQUE].bExist) {
            node.reqType = REQ_TYPE_REDO;
        } else {
            node.reqType = REQ_TYPE_NORMAL;
            if (IS_PORT(CONFIG_PORT_IMGO, mConfigPort) &&
                node.streamBufImg[STREAM_IMG_OUT_OPAQUE].bExist) {
                node.reqOutSet |= REQ_SET(REQ_OUT_FULL_OPAQUE);
            }
            if (IS_PORT(CONFIG_PORT_IMGO, mConfigPort) &&
                node.streamBufImg[STREAM_IMG_OUT_FULL].bExist) {
                if (meta_raw_type == EPipe_PROCESSED_RAW) {
                    node.reqOutSet |= REQ_SET(REQ_OUT_FULL_PROC);
                } else {
                    node.reqOutSet |= REQ_SET(REQ_OUT_FULL_PURE);
                }
            }
            if (IS_PORT(CONFIG_PORT_RRZO, mConfigPort) &&
                node.streamBufImg[STREAM_IMG_OUT_RESIZE].bExist) {
                node.reqOutSet |= REQ_SET(REQ_OUT_RESIZER);
            }
            if (IS_PORT(CONFIG_PORT_LCSO, mConfigPort) &&
                node.streamBufImg[STREAM_IMG_OUT_LCS].bExist) {
                node.reqOutSet |= REQ_SET(REQ_OUT_LCSO);
            }
            if (IS_PORT(CONFIG_PORT_RSSO, mConfigPort) &&
                node.streamBufImg[STREAM_IMG_OUT_RSS].bExist) {
                node.reqOutSet |= REQ_SET(REQ_OUT_RSSO);
            }
            //MY_LOGD2("normal (%d) 0x%x raw(%d %d)", node.magicNum,
            //    node.reqOutSet, meta_raw_exist, meta_raw_type);
        }
    } else {
        node.reqType = REQ_TYPE_INITIAL;
        if (IS_PORT(CONFIG_PORT_IMGO, mConfigPort) && mpStreamPool_full != NULL) {
            if (meta_raw_type == EPipe_PROCESSED_RAW) {
                node.reqOutSet |= REQ_SET(REQ_OUT_FULL_PROC);
            } else {
                node.reqOutSet |= REQ_SET(REQ_OUT_FULL_PURE);
            }
        }
        if (IS_PORT(CONFIG_PORT_RRZO, mConfigPort) && mpStreamPool_resizer != NULL) {
            node.reqOutSet |= REQ_SET(REQ_OUT_RESIZER);
        }
        if (IS_PORT(CONFIG_PORT_LCSO, mConfigPort) && mpStreamPool_lcso != NULL) {
            node.reqOutSet |= REQ_SET(REQ_OUT_LCSO);
        }
        if (IS_PORT(CONFIG_PORT_RSSO, mConfigPort) && mpStreamPool_rsso != NULL) {
            node.reqOutSet |= REQ_SET(REQ_OUT_RSSO);
        }
    }
    //
    MBOOL dummy_yuv = (node.reqType == REQ_TYPE_YUV) ? MTRUE: MFALSE;
    if (dummy_yuv && (!mFirstReceived)) {
        MY_LOGI("check-dummy_yuv(%d)-first_rev(%d)", dummy_yuv, mFirstReceived);
        dummy_yuv = MFALSE;
    }
    if ((list != NULL) && (node.reqType == REQ_TYPE_NORMAL ||
        node.reqType == REQ_TYPE_INITIAL || dummy_yuv)) {
        metaInfo.MagicNum = newNum;
        IMetadata::IEntry entry_num(MTK_P1NODE_PROCESSOR_MAGICNUM);
        entry_num.push_back(newNum, Type2Type< MINT32 >());
        metaInfo.halMeta.update(MTK_P1NODE_PROCESSOR_MAGICNUM, entry_num);
        //
        MUINT8 is_dummy = (node.reqType == REQ_TYPE_NORMAL) ? 0 : 1;
        metaInfo.Dummy = is_dummy;
        IMetadata::IEntry entry_dummy(MTK_HAL_REQUEST_DUMMY);
        entry_dummy.push_back(is_dummy, Type2Type< MUINT8 >());
        metaInfo.halMeta.update(MTK_HAL_REQUEST_DUMMY, entry_dummy);
        //
        #if 0
        IMetadata::IEntry entry_repeat(MTK_HAL_REQUEST_REPEAT);
        entry_repeat.push_back(0, Type2Type< MUINT8 >());
        metaInfo.halMeta.update(MTK_HAL_REQUEST_REPEAT, entry_repeat);
        #endif
        //
        #if 1 // add raw type to hal meta
        if (node.reqType == REQ_TYPE_NORMAL && !meta_raw_exist) {
            IMetadata::IEntry entryRawType(MTK_P1NODE_RAW_TYPE);
            entryRawType.push_back(meta_raw_type, Type2Type< MINT32 >());
            metaInfo.halMeta.update(MTK_P1NODE_RAW_TYPE, entryRawType);
        }
        #endif
        //
        if(listLock != NULL) {
            Mutex::Autolock _l(*listLock);
            (*list).push_back(metaInfo);
            mTagList.set((*list).size());
        } else {
            (*list).push_back(metaInfo);
            mTagList.set((*list).size());
        }
    }
    //
    if (node.reqType != REQ_TYPE_UNKNOWN) {
        mpDeliverMgr->registerNodeList(newNum);
    }
    //
    if (node.reqType == REQ_TYPE_NORMAL || node.reqType == REQ_TYPE_INITIAL) {
        if (IS_PORT(CONFIG_PORT_IMGO, mConfigPort)
            && (0 == (IS_OUT(REQ_OUT_FULL_PROC, node.reqOutSet) ||
                    IS_OUT(REQ_OUT_FULL_PURE, node.reqOutSet) ||
                    IS_OUT(REQ_OUT_FULL_OPAQUE, node.reqOutSet))
                )
            ) {
            node.reqOutSet |= REQ_SET(REQ_OUT_FULL_STUFF);
        }
        if (IS_PORT(CONFIG_PORT_RRZO, mConfigPort)
            && (0 == IS_OUT(REQ_OUT_RESIZER, node.reqOutSet))) {
            node.reqOutSet |= REQ_SET(REQ_OUT_RESIZER_STUFF);
        }
        if (IS_PORT(CONFIG_PORT_LCSO, mConfigPort)
            && (0 == IS_OUT(REQ_OUT_LCSO, node.reqOutSet))) {
            node.reqOutSet |= REQ_SET(REQ_OUT_LCSO_STUFF);
        }
        if (IS_PORT(CONFIG_PORT_RSSO, mConfigPort)
            && (0 == IS_OUT(REQ_OUT_RSSO, node.reqOutSet))) {
            node.reqOutSet |= REQ_SET(REQ_OUT_RSSO_STUFF);
        }
        //
        prepareCropInfo(pAppMeta, pHalMeta, node);
        //
        if (Queue == &mProcessingQueue) {
            node.exeState = EXE_STATE_PROCESSING;
        } else if (Queue == &mRequestQueue) {
            node.exeState = EXE_STATE_REQUESTED;
        }
        //
        job->mSet.push_back(node);
        if (job->mSet.size() == 1) {
            job->mFirstMagicNum = newNum;
        }
        //
        if(Queue != NULL) {
            if(QueLock != NULL) {
                Mutex::Autolock _l(*QueLock);
                if (job->mSet.size() == job->mMaxNum) {
                    (*Queue).push_back((*job));
                    job->mSet.clear();
                    job->mFirstMagicNum = 0;
                }
            } else {
                if (job->mSet.size() == job->mMaxNum) {
                    (*Queue).push_back((*job));
                    job->mSet.clear();
                    job->mFirstMagicNum = 0;
                }
            }
        }
        //MY_LOGD2("normal/initial (%d) 0x%x raw(%d %d) exe:%d", node.magicNum,
        //    node.reqOutSet, meta_raw_exist, meta_raw_type, node.exeState);
    } else if (node.reqType == REQ_TYPE_YUV || node.reqType == REQ_TYPE_REDO) {
        node.exeState = EXE_STATE_DONE;
        MY_LOGD2("send the ZSL request and try to trigger");
        onReturnFrame(node, MFALSE, MTRUE);
    }
    //
    P1_LOGI(1, "[P1::REQ] [%s] " P1INFO_NODE_STR " - job(%d)(%zu/%d)",
        (appframe != NULL) ? "New-Request" : "New-Dummy", P1INFO_NODE_VAR(node),
        job->mFirstMagicNum, job->mSet.size(), job->mMaxNum);
    //
    CAM_TRACE_FMT_END();
    //
    return;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
createNode(List<MetaSet_T> *list,
           Mutex *listLock)
{
    if (list == NULL) {
        MY_LOGW("list not exist");
        return;
    }
    MUINT32 newNum = get_and_increase_magicnum();
    MetaSet_T metaInfo;
    //fill in hal metadata
    metaInfo.MagicNum = newNum;
    IMetadata::IEntry entry1(MTK_P1NODE_PROCESSOR_MAGICNUM);
    entry1.push_back(newNum, Type2Type< MINT32 >());
    metaInfo.halMeta.update(MTK_P1NODE_PROCESSOR_MAGICNUM, entry1);
    //
    #if 0
    IMetadata::IEntry entry2(MTK_HAL_REQUEST_REPEAT);
    entry2.push_back(0, Type2Type< MUINT8 >());
    metaInfo.halMeta.update(MTK_HAL_REQUEST_REPEAT, entry2);
    #endif
    //
    MUINT8 is_dummy = 1;
    metaInfo.Dummy = is_dummy;
    IMetadata::IEntry entry3(MTK_HAL_REQUEST_DUMMY);
    entry3.push_back(is_dummy, Type2Type< MUINT8 >());
    metaInfo.halMeta.update(MTK_HAL_REQUEST_DUMMY, entry3);

    if(listLock != NULL) {
        Mutex::Autolock _l(*listLock);
        (*list).push_back(metaInfo);
        mTagList.set((*list).size());
    } else {
        (*list).push_back(metaInfo);
        mTagList.set((*list).size());
    }

    P1_LOGI(1, "[New-Padding] (%d)", newNum);
    return;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
createNode(Que_T &Queue)
{
    QueJob_T job(mBurstNum);
    for (MUINT8 n = 0; n < mBurstNum; n++) {
        MUINT32 newNum = get_and_increase_magicnum();
        {
            QueNode_T node;
            node.magicNum = newNum;
            node.sofIdx = P1SOFIDX_INIT_VAL;
            node.appFrame = NULL;
            job.mSet.push_back(node);
            //job.mCurNum ++;
            if (n == 0) {
                job.mFirstMagicNum = newNum;
            }
        }
        P1_LOGI(1, "[New Dummy] M(%d) job(%d)-(%zu/%d)",
            newNum, job.mFirstMagicNum, job.mSet.size(), job.mMaxNum);
    }

    Queue.push_back(job);

    return;
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
onProcessResult(
    QueNode_T & node,
    QBufInfo const &deqBuf,
    MetaSet_T const &result3A,
    IMetadata const &resultAppend,
    MUINT32 const index
)
{
    FUNCTION_P1_IN;
    //
    CAM_TRACE_FMT_BEGIN("Return(%d@%d)(%d,%d)", node.magicNum, node.sofIdx,
        P1GET_FRM_NUM(node.appFrame), P1GET_REQ_NUM(node.appFrame));
    //
    if (node.appFrame != 0) {

        // APP out Meta Stream
        if (mvStreamMeta[STREAM_META_OUT_APP] != NULL) {
            IMetadata appMetadata;
            generateAppMeta(node, result3A, deqBuf, appMetadata, index);
            if ((IS_OUT(REQ_OUT_FULL_OPAQUE, node.reqOutSet)) &&
                (node.streamBufImg[STREAM_IMG_OUT_OPAQUE].spImgBuf != NULL) &&
                (!IS_EXP(EXP_EVT_NOBUF_IMGO, node.expRec))) {
                // app metadata index
                IMetadata appTagIndex;
                generateAppTagIndex(appMetadata, appTagIndex);
                sp<IImageBufferHeap> pImageBufferHeap =
                    node.streamBufImg[STREAM_IMG_OUT_OPAQUE].spImgBuf->
                    getImageBufferHeap();
                MERROR status = OpaqueReprocUtil::setAppMetadataToHeap(
                    pImageBufferHeap,
                    appTagIndex);
                MY_LOGD2("setAppMetadataToHeap (%d)", status);
            }
            if (OK != frameMetadataGet(node, STREAM_META_OUT_APP,
                NULL, MTRUE, &appMetadata)) {
                MY_LOGW("cannot write out-app-metadata");
            }
        }

        // HAL out Meta Stream
        if (mvStreamMeta[STREAM_META_OUT_HAL] != NULL) {
            IMetadata inHalMetadata;
            IMetadata outHalMetadata;
            if (OK != frameMetadataGet(node, STREAM_META_IN_HAL,
                &inHalMetadata)) {
                MY_LOGW("cannot get in-hal-metadata");
            }
            generateHalMeta(node, result3A, deqBuf, resultAppend, inHalMetadata,
                outHalMetadata, index);
            if ((IS_OUT(REQ_OUT_FULL_OPAQUE, node.reqOutSet)) &&
                (node.streamBufImg[STREAM_IMG_OUT_OPAQUE].spImgBuf != NULL) &&
                (!IS_EXP(EXP_EVT_NOBUF_IMGO, node.expRec))) {
                sp<IImageBufferHeap> pImageBufferHeap =
                    node.streamBufImg[STREAM_IMG_OUT_OPAQUE].spImgBuf->
                    getImageBufferHeap();
                MERROR status = OpaqueReprocUtil::setHalMetadataToHeap(
                    pImageBufferHeap,
                    outHalMetadata);
                MY_LOGD2("setHalMetadataToHeap (%d)", status);
            }
            if (OK != frameMetadataGet(node, STREAM_META_OUT_HAL,
                NULL, MTRUE, &outHalMetadata)) {
                MY_LOGW("cannot write out-hal-metadata");
            }
        }

        #if (IS_P1_LOGI)
        if (mLogLevelI > 0) {
            android::String8 strInfo("");
            strInfo += String8::format("[P1::DEQ] " P1INFO_NODE_STR
                " job(%d/%d) ", P1INFO_NODE_VAR(node), index, mBurstNum);
            for (size_t n = index; n < deqBuf.mvOut.size(); n += mBurstNum) {
                if (deqBuf.mvOut[n].mPortID.index == PORT_IMGO.index) {
                    strInfo += String8::format("IMG(%s) ", (deqBuf.mvOut[n].
                        mMetaData.mRawType == EPipe_PROCESSED_RAW) ?
                        "proc" : "pure");
                } else if (deqBuf.mvOut[n].mPortID.index == PORT_RRZO.index) {
                    MRect crop_s = deqBuf.mvOut[n].mMetaData.mCrop_s;
                    MRect crop_d = deqBuf.mvOut[n].mMetaData.mCrop_d;
                    MSize size_d = deqBuf.mvOut[n].mMetaData.mDstSize;
                    strInfo += String8::format(
                        "RRZ%d(%d-%d-%dx%d)(%d-%d-%dx%d)(%dx%d) ", mIsBinEn,
                        crop_s.p.x, crop_s.p.y, crop_s.s.w, crop_s.s.h,
                        crop_d.p.x, crop_d.p.y, crop_d.s.w, crop_d.s.h,
                        size_d.w, size_d.h);
               }
            }
            P1_LOGI(1, "%sT(%" PRId64 ")(%" PRId64 ")", strInfo.string(),
                node.frameExpDuration, node.frameTimeStamp);
        }
        #endif
    }
    //
    #if 1 // trigger only at the end of this job
    onReturnFrame(node, MFALSE,
        ((mBurstNum <= 1) || (index == (MUINT32)(mBurstNum - 1))) ?
        MTRUE : MFALSE);
    #else
    onReturnFrame(node, MFALSE, MTRUE);
    #endif
    //
    CAM_TRACE_FMT_END();
    //
    FUNCTION_P1_OUT;
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
processRedoFrame(
    QueNode_T & node
)
{
    FUNCTION_P1_IN;
    //
    if (node.needFlush) {
        MY_LOGD("need to flush, skip frame processing");
        return;
    };
    //MBOOL outAppMetaSuccess = MFALSE;
    //MBOOL outHalMetaSuccess = MFALSE;
    IMetadata appMeta;
    IMetadata halMeta;
    sp<IImageBuffer> imgBuf;
    //
    if (OK != frameImageGet(node, STREAM_IMG_IN_OPAQUE, imgBuf)) {
        MY_LOGE("Can not get in-opaque buffer from frame");
    } else {
        sp<IImageBufferHeap> pHeap = imgBuf->getImageBufferHeap();
        IMetadata appMetaTagIndex;
        if (OK == OpaqueReprocUtil::getAppMetadataFromHeap
            (pHeap, appMetaTagIndex)) {
            // get the input of app metadata
            IMetadata metaInApp;
            if (OK != frameMetadataGet(node, STREAM_META_IN_APP,
                &metaInApp)) {
                MY_LOGW("cannot get in-app-metadata");
            }
            // get p1node's tags from opaque buffer
            IMetadata::IEntry entryTagIndex =
                appMetaTagIndex.entryFor(MTK_P1NODE_METADATA_TAG_INDEX);
            for (MUINT i = 0; i < entryTagIndex.count(); i++) {
                MUINT32 tag =
                    (MUINT32)entryTagIndex.editItemAt(i, Type2Type<MINT32>());
                IMetadata::IEntry& entryInApp = metaInApp.editEntryFor(tag);
                appMeta.update(tag, entryInApp);
            }
            // Workaround: do not return the duplicated key for YUV reprocessing
            appMeta.remove(MTK_JPEG_THUMBNAIL_SIZE);
            appMeta.remove(MTK_JPEG_ORIENTATION);
            if (OK != frameMetadataGet(node, STREAM_META_OUT_APP,
                NULL, MTRUE, &appMeta)) {
                MY_LOGW("cannot write out-app-metadata");
            }
        } else {
            MY_LOGW("Can not get app meta from in-opaque buffer");
        }
        if (OK == OpaqueReprocUtil::getHalMetadataFromHeap(pHeap, halMeta)) {
            IMetadata::IEntry entry(MTK_HAL_REQUEST_REQUIRE_EXIF);
            entry.push_back(1, Type2Type<MUINT8>());
            halMeta.update(entry.tag(), entry);
            if (OK != frameMetadataGet(node, STREAM_META_OUT_HAL,
                NULL, MTRUE, &halMeta)) {
                MY_LOGW("cannot write out-hal-metadata");
            }
        } else {
            MY_LOGW("Can not get hal meta from in-opaque buffer");
        }
    }
    //
    FUNCTION_P1_OUT;
    //
    return;
};

/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
processYuvFrame(
    QueNode_T & node
)
{
    FUNCTION_P1_IN;
    //
    if (node.needFlush) {
        MY_LOGD("need to flush, skip frame processing");
        return;
    };
    IMetadata inAppMetadata;
    IMetadata outAppMetadata;
    IMetadata inHalMetadata;
    IMetadata outHalMetadata;
    MINT64 timestamp = 0;
    // APP in Meta Stream
    if (OK != frameMetadataGet(node, STREAM_META_IN_APP, &inAppMetadata)) {
        MY_LOGW("cannot get in-app-metadata");
    } else {
        if (tryGetMetadata< MINT64 >(
            &inAppMetadata, MTK_SENSOR_TIMESTAMP, timestamp)) {
            MY_LOGD1("timestamp from in-app %" PRId64 , timestamp);
        } else {
            MY_LOGI("cannot find timestamp from in-app");
            timestamp = 0;
        }
    };
    // APP out Meta Stream
    if (trySetMetadata< MINT64 > (
        &outAppMetadata, MTK_SENSOR_TIMESTAMP, timestamp)) {
        if (OK != frameMetadataGet(node, STREAM_META_OUT_APP, NULL,
                    MTRUE, &outAppMetadata)) {
            MY_LOGW("cannot write out-app-metadata");
        }
    } else {
        MY_LOGW("cannot update MTK_SENSOR_TIMESTAMP");
    }
    // HAL in/out Meta Stream
    if (OK != frameMetadataGet(node, STREAM_META_IN_HAL, &inHalMetadata)) {
        MY_LOGW("cannot get in-hal-metadata");
    } else {
        outHalMetadata = inHalMetadata;
        if (!trySetMetadata< MINT32 > (
            &outHalMetadata, MTK_P1NODE_SENSOR_MODE, mSensorParams.mode)) {
            MY_LOGW("cannot update MTK_P1NODE_SENSOR_MODE");
        }
        if (!trySetMetadata< MINT32 > (
            &outHalMetadata, MTK_P1NODE_SENSOR_VHDR_MODE, mSensorParams.vhdrMode)) {
            MY_LOGW("cannot update MTK_P1NODE_SENSOR_MODE");
        }
        if (!trySetMetadata< MRect > (
            &outHalMetadata, MTK_P1NODE_SCALAR_CROP_REGION,
            MRect(mSensorParams.size.w, mSensorParams.size.h))) {
            MY_LOGW("cannot update MTK_P1NODE_SCALAR_CROP_REGION");
        }
        if (!trySetMetadata< MRect > (
            &outHalMetadata, MTK_P1NODE_DMA_CROP_REGION,
            MRect(mSensorParams.size.w, mSensorParams.size.h))) {
            MY_LOGW("cannot update MTK_P1NODE_DMA_CROP_REGION");
        }
        if (!trySetMetadata< MSize > (
            &outHalMetadata, MTK_P1NODE_RESIZER_SIZE, mSensorParams.size)) {
            MY_LOGW("cannot update MTK_P1NODE_RESIZER_SIZE");
        }
        if (OK != frameMetadataGet(node, STREAM_META_OUT_HAL, NULL,
                    MTRUE, &outHalMetadata)) {
            MY_LOGW("cannot write out-hal-metadata");
        }
    };
    //
    FUNCTION_P1_OUT;
    //
    return;
};

/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::
releaseNode(
    QueNode_T & node
)
{
    FUNCTION_P1_IN;
    //
    CAM_TRACE_FMT_BEGIN("Release(%d@%d)(%d,%d)", node.magicNum, node.needFlush,
        P1GET_FRM_NUM(node.appFrame), P1GET_REQ_NUM(node.appFrame));
    //
    MY_LOGD3(P1INFO_NODE_STR " begin", P1INFO_NODE_VAR(node));
    //
    if (!node.needFlush) {
        if (node.reqType == REQ_TYPE_REDO) {
            processRedoFrame(node);
        } else if (node.reqType == REQ_TYPE_YUV) {
            processYuvFrame(node);
        }
    };
    //
    for (int stream = STREAM_ITEM_START; stream < STREAM_META_NUM; stream++) {
        if (node.streamBufMeta[stream].bExist) {
            if (OK != frameMetadataPut(node, (STREAM_META)stream)) {
                MY_LOGD("cannot put metadata stream(%d)", stream);
            }
        };
    };
    //
    for (int stream = STREAM_ITEM_START; stream < STREAM_IMG_NUM; stream++) {
        // TODO: input image buffer return
        if ((!node.streamBufImg[stream].bExist) && // for INITIAL node
            (node.streamBufImg[stream].eSrcType == IMG_BUF_SRC_NULL)) {
            continue; // this stream is not existent and no pool/stuff buffer
        }
        switch (node.streamBufImg[stream].eSrcType) {
            case IMG_BUF_SRC_STUFF:
                if (OK != stuffImagePut(node, (STREAM_IMG)stream)) {
                    MY_LOGD("cannot put stuff image stream(%d)", stream);
                };
                break;
            case IMG_BUF_SRC_POOL:
                if (OK != poolImagePut(node, (STREAM_IMG)stream)) {
                    MY_LOGD("cannot put pool image stream(%d)", stream);
                };
                break;
            case IMG_BUF_SRC_FRAME:
            case IMG_BUF_SRC_NULL: // for flush node, buf src is not decided
                if (OK != frameImagePut(node, (STREAM_IMG)stream)) {
                    MY_LOGD("cannot put frame image stream(%d)", stream);
                };
                break;
            default:
                MY_LOGW("node buffer source is not defined");
                MY_LOGW("check node exe "
                    P1INFO_NODE_STR, P1INFO_NODE_VAR(node));
                break;
        };
    };
    //
    if (node.reqType == REQ_TYPE_INITIAL) {
        MY_LOGD3(P1INFO_NODE_STR " INITIAL return", P1INFO_NODE_VAR(node));
        CAM_TRACE_FMT_END();
        return;
    }
    //
    MY_LOGD3(P1INFO_NODE_STR " applyrelease", P1INFO_NODE_VAR(node));
    //
    // Apply buffers to release
    IStreamBufferSet& rStreamBufferSet  = node.appFrame->getStreamBufferSet();
    rStreamBufferSet.applyRelease(getNodeId());

    #if 1
    // camera display systrace - Dispatch
    if (node.reqType == REQ_TYPE_NORMAL &&
        node.appFrame != NULL &&
        node.frameTimeStamp > 0) {
        if  ( ATRACE_ENABLED() ) {
            MINT64 const timestamp = node.frameTimeStamp;
            String8 const str = String8::format(
                "Cam:%d:IspP1:dispatch|timestamp(ns):%" PRId64
                " duration(ns):%" PRId64
                " request:%d frame:%d",
                getOpenId(), timestamp, ::systemTime()- timestamp,
                node.appFrame->getRequestNo(), node.appFrame->getFrameNo()
            );
            CAM_TRACE_BEGIN(str.string());
            CAM_TRACE_END();
        }
    }
    #endif
    //
    MY_LOGD3(P1INFO_NODE_STR " dispatch", P1INFO_NODE_VAR(node));
    //
    // dispatch to next node
    CAM_TRACE_BEGIN("P1:onDispatchFrame");
    onDispatchFrame(node.appFrame);
    CAM_TRACE_END();

    MINT32 currReqCnt = 0;
    currReqCnt = android_atomic_dec(&mInFlightRequestCnt);
    ATRACE_INT("P1_request_cnt",
        android_atomic_acquire_load(&mInFlightRequestCnt));
    MY_LOGD3("InFlightRequestCount-- (%d) => (%d)", currReqCnt,
        android_atomic_acquire_load(&mInFlightRequestCnt));
    //
    MY_LOGD3(P1INFO_NODE_STR " end", P1INFO_NODE_VAR(node));
    //
    mTagOut.set(node.magicNum);
    #if (IS_P1_LOGI)
    if (mLogLevelI > 0) {
        android::String8 strInfo("");
        strInfo += String8::format("[P1::OUT] [Release-%d] " P1INFO_NODE_STR,
            node.needFlush, P1INFO_NODE_VAR(node));
        P1_LOGI(1, "%s", strInfo.string());
    }
    #endif
    //
    CAM_TRACE_FMT_END();
    //
    FUNCTION_P1_OUT;
    //
    return;
};

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
requestMetadataEarlyCallback(
    QueNode_T const & node, STREAM_META const streamMeta,
    IMetadata * pMetadata
)
{
    P1_CHECK_STREAM(META, streamMeta);
    P1_CHECK_NODE_STREAM(Meta, node, streamMeta);
    //
    if (pMetadata == NULL) {
        MY_LOGD1("Result Metadata is Null");
        return BAD_VALUE;
    }
    if (pMetadata->count() == 0) {
        MY_LOGD1("Result Metadata is Empty");
        return OK;
    }
    MY_LOGD2("Meta[%d]=(%d) EarlyCB " P1INFO_NODE_STR,
        streamMeta, pMetadata->count(), P1INFO_NODE_VAR(node));
    //
    IMetadata outMetadata = *(pMetadata);
    android::String8 strInfo("");
    strInfo += String8::format("Meta[%d]=(%d) EarlyCB " P1INFO_NODE_STR,
        streamMeta, pMetadata->count(), P1INFO_NODE_VAR(node));
    DurationProfile duration(strInfo.string(), 5000000LL); // 5ms
    duration.pulse_up();
    onEarlyCallback(node.appFrame, mvStreamMeta[streamMeta]->getStreamId(),
        outMetadata);
    duration.pulse_down();
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
frameMetadataInit(QueNode_T & node, STREAM_META const streamMeta,
    sp<IMetaStreamBuffer> &pMetaStreamBuffer)
{
    P1INFO_NODE_STREAM(3, node, streamMeta);
    //
    P1_CHECK_STREAM(META, streamMeta);
    P1_CHECK_NODE_STREAM(Meta, node, streamMeta);
    //
    StreamId_T const streamId = mvStreamMeta[streamMeta]->getStreamId();
    IStreamBufferSet& rStreamBufferSet =
        node.appFrame->getStreamBufferSet();
    MERROR const err = ensureMetaBufferAvailable_(
        node.appFrame->getFrameNo(),
        streamId,
        rStreamBufferSet,
        pMetaStreamBuffer
    );
    if (err != OK) {
        MY_LOGW("check status(%d) MetaStreamBuffer(%d) ID(%#" PRIx64 ") @%d",
            err, streamMeta, streamId, node.magicNum);
        return err;
    }
    if (pMetaStreamBuffer != NULL) {
        node.streamBufMeta[streamMeta].spStreamBuf = pMetaStreamBuffer;
        node.streamBufMeta[streamMeta].eLockState = STREAM_BUF_LOCK_NONE;
    } else {
        MY_LOGI("cannot get MetaStreamBuffer(%d) ID(%#" PRIx64 ") @%d",
            streamMeta, streamId, node.magicNum);
        return BAD_VALUE;
    }
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
frameMetadataGet(
    QueNode_T & node, STREAM_META const streamMeta, IMetadata * pOutMetadata,
    MBOOL toWrite, IMetadata * pInMetadata)
{
    P1INFO_NODE_STREAM(3, node, streamMeta);
    //
    P1_CHECK_STREAM(META, streamMeta);
    P1_CHECK_NODE_STREAM(Meta, node, streamMeta);
    //
    sp<IMetaStreamBuffer> pMetaStreamBuffer = NULL;
    pMetaStreamBuffer = node.streamBufMeta[streamMeta].spStreamBuf;
    if (pMetaStreamBuffer == NULL && // call frameMetadataInit while NULL
        OK != frameMetadataInit(node, streamMeta, pMetaStreamBuffer)) {
        MY_LOGW("Check Stream(%d)-ID(%#" PRIx64 ") of Metadata "
            P1INFO_NODE_STR, streamMeta,
            mvStreamMeta[streamMeta]->getStreamId(), P1INFO_NODE_VAR(node));
        return BAD_VALUE;
    }
    //
    STREAM_BUF_LOCK curLock = node.streamBufMeta[streamMeta].eLockState;
    // current-lock != needed-lock
    if (((toWrite) && (curLock == STREAM_BUF_LOCK_R))
        || ((!toWrite) && (curLock == STREAM_BUF_LOCK_W))) {
        if (node.streamBufMeta[streamMeta].pMetadata == NULL) {
            MY_LOGE("previous pMetadata is NULL, Meta:%d:%#" PRIx64 " Lock:%d "
                "Write:%d", streamMeta, mvStreamMeta[streamMeta]->getStreamId(),
                curLock, toWrite);
            return BAD_VALUE;
        }
        pMetaStreamBuffer->unlock(getNodeName(),
            node.streamBufMeta[streamMeta].pMetadata);
        //
        node.streamBufMeta[streamMeta].eLockState = STREAM_BUF_LOCK_NONE;
        node.streamBufMeta[streamMeta].pMetadata = NULL;
    }
    // current-lock == STREAM_BUF_LOCK_NONE
    if (node.streamBufMeta[streamMeta].eLockState == STREAM_BUF_LOCK_NONE) {
        IMetadata* pMetadata = NULL;
        if (toWrite) {
            pMetadata = pMetaStreamBuffer->tryWriteLock(getNodeName());
        } else {
            pMetadata = pMetaStreamBuffer->tryReadLock(getNodeName());
        }
        if (pMetadata == NULL) {
            MY_LOGE("get pMetadata is NULL, Meta:%d:%#" PRIx64 " Lock:%d "
                "Write:%d", streamMeta, mvStreamMeta[streamMeta]->getStreamId(),
                curLock, toWrite);
            return BAD_VALUE;
        };
        //
        node.streamBufMeta[streamMeta].eLockState = (toWrite) ?
            STREAM_BUF_LOCK_W : STREAM_BUF_LOCK_R;
        node.streamBufMeta[streamMeta].pMetadata = pMetadata;
    }
    //
    if (node.streamBufMeta[streamMeta].pMetadata == NULL) {
        MY_LOGE("stored pMetadata is NULL, Meta:%d:%#" PRIx64 " Lock:%d "
            "Write:%d", streamMeta, mvStreamMeta[streamMeta]->getStreamId(),
            curLock, toWrite);
        return BAD_VALUE;
    }
    //
    if (toWrite && (pInMetadata != NULL)) {
        pMetaStreamBuffer->markStatus(STREAM_BUFFER_STATUS::WRITE_OK);
        *(node.streamBufMeta[streamMeta].pMetadata) = *pInMetadata;
    }
    if (pOutMetadata != NULL) {
        *pOutMetadata = *(node.streamBufMeta[streamMeta].pMetadata);
    }
    MY_LOGD3("MetaGet(%p)(%p), write(%d) Stream(%d:%#" PRIx64 ") Lock(%d>%d) "
        P1INFO_NODE_STR, pOutMetadata, pInMetadata, toWrite, streamMeta,
        mvStreamMeta[streamMeta]->getStreamId(), curLock,
        node.streamBufMeta[streamMeta].eLockState, P1INFO_NODE_VAR(node));
    //
    return OK;
};

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
frameMetadataPut(QueNode_T & node, STREAM_META const streamMeta)
{
    P1INFO_NODE_STREAM(3, node, streamMeta);
    //
    P1_CHECK_STREAM(META, streamMeta);
    P1_CHECK_NODE_STREAM(Meta, node, streamMeta);
    //
    StreamId_T const streamId = mvStreamMeta[streamMeta]->getStreamId();
    //
    STREAM_BUF_LOCK curLock = node.streamBufMeta[streamMeta].eLockState;
    //
    #if 1 // keep input stream status
    if (!IS_IN_STREAM_META(streamMeta))
    #endif
    {
        if (node.needFlush) {
            sp<IMetaStreamBuffer> pMetaStreamBuffer = NULL;
            pMetaStreamBuffer = node.streamBufMeta[streamMeta].spStreamBuf;
            if (pMetaStreamBuffer == NULL && // call frameMetaInit while NULL
                OK != frameMetadataInit(node, streamMeta, pMetaStreamBuffer)) {
                MY_LOGE("get IMetaStreamBuffer but NULL, Stream:%d:%#"
                    PRIx64 " Lock:%d>%d @%d", streamMeta, streamId, curLock,
                    node.streamBufMeta[streamMeta].eLockState, node.magicNum);
                return BAD_VALUE;
            }
            pMetaStreamBuffer->markStatus(STREAM_BUFFER_STATUS::WRITE_ERROR);
        }
    }
    //
    if (curLock != STREAM_BUF_LOCK_NONE) {
        if (node.streamBufMeta[streamMeta].spStreamBuf != NULL) {
            if (node.streamBufMeta[streamMeta].pMetadata != NULL) {
                node.streamBufMeta[streamMeta].spStreamBuf->unlock(
                    getNodeName(), node.streamBufMeta[streamMeta].pMetadata);
                node.streamBufMeta[streamMeta].eLockState =
                    STREAM_BUF_LOCK_NONE;
            } else {
                MY_LOGW("MetaStream locked but no Metadata, Stream:%d:%#"
                    PRIx64 " Lock:%d>%d @%d", streamMeta, streamId, curLock,
                    node.streamBufMeta[streamMeta].eLockState, node.magicNum);
            }
        } else {
            MY_LOGW("MetaStream locked but no StreamBuf, Stream:%d:%#" PRIx64
                " Lock:%d>%d @%d", streamMeta, streamId, curLock,
                node.streamBufMeta[streamMeta].eLockState, node.magicNum);
        }
    }
    //
    IStreamBufferSet& rStreamBufferSet  = node.appFrame->getStreamBufferSet();
    rStreamBufferSet.markUserStatus(
        streamId, getNodeId(),
        #if 0 // if it is not used, only mark RELEASE
        (curLock == STREAM_BUF_LOCK_NONE &&
        node.exeState == EXE_STATE_REQUESTED) ?
        (IUsersManager::UserStatus::RELEASE) :
        #endif
        (IUsersManager::UserStatus::RELEASE | IUsersManager::UserStatus::USED)
    );
    //
    MY_LOGD3("MetaPut, Stream(%d:%#" PRIx64 ") Lock(%d>%d) " P1INFO_NODE_STR,
        streamMeta, streamId, curLock,
        node.streamBufMeta[streamMeta].eLockState, P1INFO_NODE_VAR(node));
    node.streamBufMeta[streamMeta].pMetadata = NULL;
    node.streamBufMeta[streamMeta].spStreamBuf = NULL;
    //
    return OK;
};

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
frameImageInit(QueNode_T & node, STREAM_IMG const streamImg,
    sp<IImageStreamBuffer> &pImageStreamBuffer)
{
    P1INFO_NODE_STREAM(3, node, streamImg);
    //
    P1_CHECK_STREAM(IMG, streamImg);
    P1_CHECK_NODE_STREAM(Img, node, streamImg);
    //
    StreamId_T const streamId = mvStreamImg[streamImg]->getStreamId();
    IStreamBufferSet& rStreamBufferSet =
        node.appFrame->getStreamBufferSet();
    MERROR const err = ensureImageBufferAvailable_(
        node.appFrame->getFrameNo(),
        streamId,
        rStreamBufferSet,
        pImageStreamBuffer
    );
    if (err != OK) {
        MY_LOGI("check status(%d) ImageStreamBuffer(%d) ID(%#" PRIx64 ") @%d",
            err, streamImg, streamId, node.magicNum);
        return err;
    }
    if (pImageStreamBuffer != NULL) {
        node.streamBufImg[streamImg].spStreamBuf = pImageStreamBuffer;
        node.streamBufImg[streamImg].eLockState = STREAM_BUF_LOCK_NONE;
    } else {
        MY_LOGI("cannot get ImageStreamBuffer(%d) ID(%#" PRIx64 ") @%d",
            streamImg, streamId, node.magicNum);
        return BAD_VALUE;
    }
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
frameImageGet(
    QueNode_T & node, STREAM_IMG const streamImg, sp<IImageBuffer> &rImgBuf)
{
    P1INFO_NODE_STREAM(3, node, streamImg);
    //
    P1_CHECK_STREAM(IMG, streamImg);
    P1_CHECK_NODE_STREAM(Img, node, streamImg);
    //
    sp<IImageStreamBuffer> pImageStreamBuffer = NULL;
    pImageStreamBuffer = node.streamBufImg[streamImg].spStreamBuf;
    if (pImageStreamBuffer == NULL && // call frameImageInit while NULL
        OK != frameImageInit(node, streamImg, pImageStreamBuffer)) {
        MY_LOGI("Check Stream(%d)-ID(%#" PRIx64 ") in Frame "
            P1INFO_NODE_STR, streamImg, mvStreamImg[streamImg]->getStreamId(),
            P1INFO_NODE_VAR(node));
        return BAD_VALUE;
    }
    //
    STREAM_BUF_LOCK curLock = node.streamBufImg[streamImg].eLockState;
    // for stream image, only implement Write Lock
    MUINT groupUsage = 0x0;
    if (curLock == STREAM_BUF_LOCK_NONE) {
        groupUsage = pImageStreamBuffer->queryGroupUsage(getNodeId());
        if(mDebugScanLineMask != 0) {
            groupUsage |= GRALLOC_USAGE_SW_WRITE_OFTEN;
        }
        sp<IImageBufferHeap>  pImageBufferHeap =
            pImageStreamBuffer->tryWriteLock(getNodeName());
        if (pImageBufferHeap == NULL) {
            MY_LOGE("ImageBufferHeap == NULL "
                P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
                P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
            return BAD_VALUE;
        }
        #if 1 // for opaque out image stream add info
        if (streamImg == STREAM_IMG_OUT_OPAQUE) {
            pImageBufferHeap->lockBuf(getNodeName());
            if (OK != OpaqueReprocUtil::setOpaqueInfoToHeap(
                pImageBufferHeap,
                mSensorParams.size,
                mRawFormat,
                mRawStride,
                mRawLength)) {
                MY_LOGW("OUT_OPAQUE setOpaqueInfoToHeap fail "
                    P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
                    P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
            };
            pImageBufferHeap->unlockBuf(getNodeName());
        }
        #endif
        // get ImageBuffer from ImageBufferHeap
        sp<IImageBuffer> pImageBuffer = NULL;
        if (streamImg == STREAM_IMG_OUT_OPAQUE ||
            streamImg == STREAM_IMG_IN_OPAQUE) {
            pImageBufferHeap->lockBuf(getNodeName());
            MERROR status = OpaqueReprocUtil::getImageBufferFromHeap(
                    pImageBufferHeap,
                    pImageBuffer);
            pImageBufferHeap->unlockBuf(getNodeName());
            if ( status != OK) {
                MY_LOGE("Cannot get ImageBuffer from opaque ImageBufferHeap "
                    P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
                    P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
                return BAD_VALUE;
            }
        } else {
            pImageBuffer = pImageBufferHeap->createImageBuffer();
        }
        if (pImageBuffer == NULL) {
            MY_LOGE("ImageBuffer == NULL "
                P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
                P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
            return BAD_VALUE;
        } else {
            pImageBuffer->lockBuf(getNodeName(), groupUsage);
            node.streamBufImg[streamImg].spImgBuf = pImageBuffer;
            node.streamBufImg[streamImg].eLockState = STREAM_BUF_LOCK_W;
            node.streamBufImg[streamImg].eSrcType = IMG_BUF_SRC_FRAME;
        }
    }
    //
    if (node.streamBufImg[streamImg].eLockState == STREAM_BUF_LOCK_W) {
        if (node.streamBufImg[streamImg].spImgBuf == NULL) {
            MY_LOGE("stored ImageBuffer is NULL "
                P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
                P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
            return BAD_VALUE;
        }
        rImgBuf = node.streamBufImg[streamImg].spImgBuf;
    }
    MY_LOGD3("ImgGet-frame, " P1INFO_STREAM_IMG_STR " Lock(%d>%d) Usage(0x%x) "
        P1INFO_NODE_STR, P1INFO_STREAM_IMG_VAR(node), curLock,
        node.streamBufImg[streamImg].eLockState, groupUsage,
        P1INFO_NODE_VAR(node));
    //
    return OK;
};

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
frameImagePut(QueNode_T & node, STREAM_IMG const streamImg)
{
    P1INFO_NODE_STREAM(3, node, streamImg);
    //
    P1_CHECK_STREAM(IMG, streamImg);
    P1_CHECK_NODE_STREAM(Img, node, streamImg);
    //
    StreamId_T const streamId = mvStreamImg[streamImg]->getStreamId();
    //
    STREAM_BUF_LOCK curLock = node.streamBufImg[streamImg].eLockState;
    //
    #if 1 // keep input stream status
    if (!IS_IN_STREAM_IMG(streamImg))
    #endif
    {
        if (node.exeState == EXE_STATE_REQUESTED ||
            node.exeState == EXE_STATE_PROCESSING ||
            node.exeState == EXE_STATE_DONE) {
            sp<IImageStreamBuffer> pImageStreamBuffer = NULL;
            pImageStreamBuffer = node.streamBufImg[streamImg].spStreamBuf;
            if (pImageStreamBuffer == NULL && // call frameImgInit while NULL
                OK != frameImageInit(node, streamImg, pImageStreamBuffer)) {
                MY_LOGE("get ImageStreamBuffer but NULL, "
                    P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
                    P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
                return BAD_VALUE;
            }
            pImageStreamBuffer->markStatus((node.needFlush) ?
                STREAM_BUFFER_STATUS::WRITE_ERROR :
                STREAM_BUFFER_STATUS::WRITE_OK);
        }
    }
    //
    if (curLock != STREAM_BUF_LOCK_NONE) {
        if (node.streamBufImg[streamImg].spStreamBuf != NULL) {
            if (node.streamBufImg[streamImg].spImgBuf != NULL) {
                node.streamBufImg[streamImg].spImgBuf->unlockBuf(getNodeName());
                node.streamBufImg[streamImg].spStreamBuf->unlock(getNodeName(),
                    node.streamBufImg[streamImg].spImgBuf->getImageBufferHeap()
                    );
                node.streamBufImg[streamImg].eLockState = STREAM_BUF_LOCK_NONE;
            } else {
                MY_LOGW("ImageStream locked but no ImageBuffer, Stream:%d:%#"
                    PRIx64 " Lock:%d>%d @%d", streamImg, streamId, curLock,
                    node.streamBufImg[streamImg].eLockState, node.magicNum);
            }
        } else {
            MY_LOGW("ImageStream locked but no StreamBuf, Stream:%d:%#" PRIx64
                " Lock:%d>%d @%d", streamImg, streamId, curLock,
                node.streamBufImg[streamImg].eLockState, node.magicNum);
        }
    }
    MY_LOGD3("ImgPut-frame, " P1INFO_STREAM_IMG_STR " Lock(%d>%d) "
        P1INFO_NODE_STR, P1INFO_STREAM_IMG_VAR(node), curLock,
        node.streamBufImg[streamImg].eLockState, P1INFO_NODE_VAR(node));
    //
    IStreamBufferSet& rStreamBufferSet = node.appFrame->getStreamBufferSet();
    rStreamBufferSet.markUserStatus(
        streamId, getNodeId(),
        #if 0 // if it is not used, only mark RELEASE
        (curLock == STREAM_BUF_LOCK_NONE &&
        node.exeState == EXE_STATE_REQUESTED) ?
        (IUsersManager::UserStatus::RELEASE) :
        #endif
        (IUsersManager::UserStatus::RELEASE | IUsersManager::UserStatus::USED)
    );
    //
    node.streamBufImg[streamImg].spImgBuf = NULL;
    node.streamBufImg[streamImg].spStreamBuf = NULL;
    node.streamBufImg[streamImg].eSrcType = IMG_BUF_SRC_NULL;
    //
    return OK;
};

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
poolImageGet(
    QueNode_T & node, STREAM_IMG const streamImg, sp<IImageBuffer> &rImgBuf)
{
    P1INFO_NODE_STREAM(3, node, streamImg);
    //
    P1_CHECK_STREAM(IMG, streamImg);
    P1_CHECK_CONFIG_STREAM(Img, node, streamImg);
    //
    MERROR err = OK;
    sp<IImageStreamBufferPoolT> pStreamBufPool = NULL;
    switch (streamImg) {
        case STREAM_IMG_OUT_FULL:
        case STREAM_IMG_OUT_OPAQUE:
            pStreamBufPool = mpStreamPool_full;
            break;
        case STREAM_IMG_OUT_RESIZE:
            pStreamBufPool = mpStreamPool_resizer;
            break;
        case STREAM_IMG_OUT_LCS:
            pStreamBufPool = mpStreamPool_lcso;
            break;
        case STREAM_IMG_OUT_RSS:
            pStreamBufPool = mpStreamPool_rsso;
            break;
        default:
            MY_LOGE("INVALID POOL %d", streamImg);
            return INVALID_OPERATION;
    };
    if (pStreamBufPool == NULL) {
        MY_LOGE("StreamBufPool is NULL "
            P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
            P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
        return BAD_VALUE;
    }
    err = pStreamBufPool->acquireFromPool(
        getNodeName(), node.streamBufImg[streamImg].spStreamBuf, ::s2ns(300));
    if (err != OK) {
        if(err == TIMED_OUT) {
            MY_LOGW("acquire timeout "
                P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
                P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
        } else {
            MY_LOGW("acquire failed "
                P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
                P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
        }
        pStreamBufPool->dumpPool();
        return BAD_VALUE;
    }
    //
    if (node.streamBufImg[streamImg].spStreamBuf == NULL) {
        MY_LOGE("ImageStreamBuffer is NULL "
            P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
            P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
        return BAD_VALUE;
    }
    //
    MUINT usage = GRALLOC_USAGE_SW_READ_OFTEN |
        GRALLOC_USAGE_HW_CAMERA_READ | GRALLOC_USAGE_HW_CAMERA_WRITE;
    if(mDebugScanLineMask != 0) {
        usage |= GRALLOC_USAGE_SW_WRITE_OFTEN;
    }
    sp<IImageBufferHeap> pImageBufferHeap =
        node.streamBufImg[streamImg].spStreamBuf->tryWriteLock(getNodeName());
    if (pImageBufferHeap == NULL) {
        MY_LOGE("pImageBufferHeap == NULL "
            P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
            P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
        return BAD_VALUE;
    }
    rImgBuf = pImageBufferHeap->createImageBuffer();
    if (rImgBuf == NULL) {
        MY_LOGE("pImageBuffer == NULL "
            P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
            P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
        return BAD_VALUE;
    }
    rImgBuf->lockBuf(getNodeName(), usage);
    node.streamBufImg[streamImg].spImgBuf = rImgBuf;
    node.streamBufImg[streamImg].eLockState = STREAM_BUF_LOCK_W;
    node.streamBufImg[streamImg].eSrcType = IMG_BUF_SRC_POOL;
    MY_LOGD3("ImgGet-pool, " P1INFO_STREAM_IMG_STR " Usage(0x%x) "
        P1INFO_NODE_STR, P1INFO_STREAM_IMG_VAR(node), usage,
        P1INFO_NODE_VAR(node));
    //
    return OK;
};

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
poolImagePut(QueNode_T & node, STREAM_IMG const streamImg)
{
    P1INFO_NODE_STREAM(3, node, streamImg);
    //
    P1_CHECK_STREAM(IMG, streamImg);
    P1_CHECK_CONFIG_STREAM(Img, node, streamImg);
    //
    sp<IImageStreamBufferPoolT> pStreamBufPool = NULL;
    switch (streamImg) {
        case STREAM_IMG_OUT_FULL:
        case STREAM_IMG_OUT_OPAQUE:
            pStreamBufPool = mpStreamPool_full;
            break;
        case STREAM_IMG_OUT_RESIZE:
            pStreamBufPool = mpStreamPool_resizer;
            break;
        case STREAM_IMG_OUT_LCS:
            pStreamBufPool = mpStreamPool_lcso;
            break;
        case STREAM_IMG_OUT_RSS:
            pStreamBufPool = mpStreamPool_rsso;
            break;
        default:
            MY_LOGE("INVALID POOL %d", streamImg);
            return INVALID_OPERATION;
    };
    if (pStreamBufPool == NULL) {
        MY_LOGE("StreamBufPool is NULL "
            P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
            P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
        return BAD_VALUE;
    }
    MY_LOGD3("ImgPut-pool, " P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
        P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
    if (node.streamBufImg[streamImg].eLockState != STREAM_BUF_LOCK_NONE) {
        node.streamBufImg[streamImg].spImgBuf->unlockBuf(getNodeName());
        node.streamBufImg[streamImg].spStreamBuf->unlock(getNodeName(),
            node.streamBufImg[streamImg].spImgBuf->getImageBufferHeap());
    }
    //
    pStreamBufPool->releaseToPool(getNodeName(),
        node.streamBufImg[streamImg].spStreamBuf);
    //
    node.streamBufImg[streamImg].spImgBuf = NULL;
    node.streamBufImg[streamImg].spStreamBuf = NULL;
    node.streamBufImg[streamImg].eLockState = STREAM_BUF_LOCK_NONE;
    node.streamBufImg[streamImg].eSrcType = IMG_BUF_SRC_NULL;
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
stuffImageGet(
    QueNode_T & node, STREAM_IMG const streamImg,
    MSize const dstSize, sp<IImageBuffer> &rImgBuf)
{
    P1INFO_NODE_STREAM(3, node, streamImg);
    //
    P1_CHECK_STREAM(IMG, streamImg);
    P1_CHECK_CONFIG_STREAM(Img, node, streamImg);
    //
    MERROR err = OK;
    if (streamImg == STREAM_IMG_OUT_OPAQUE) {
        char const* szName = "Hal:Image:P1:OPAQUESTUFFraw";
        Vector<MUINT32> stride;
        stride.clear();
        stride.push_back(mRawStride); // OpaqueRaw : 1-plane
        err = createStuffBuffer(rImgBuf, szName, mRawFormat,
            MSize(mSensorParams.size.w, dstSize.h), stride);
    } else {
        if (mvStreamImg[streamImg] == NULL) {
            MY_LOGE("create stuff buffer without stream info "
                P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
                P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
            return BAD_VALUE;
        }
        err = createStuffBuffer(rImgBuf, mvStreamImg[streamImg], dstSize.h);
    };
    if (err != OK) {
        MY_LOGE("create stuff buffer with stream info failed "
            P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
            P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
        return BAD_VALUE;
    };
    //
    if (rImgBuf != NULL) {
        node.streamBufImg[streamImg].spImgBuf = rImgBuf;
        node.streamBufImg[streamImg].eLockState = STREAM_BUF_LOCK_W;
        node.streamBufImg[streamImg].eSrcType = IMG_BUF_SRC_STUFF;
    };
    MY_LOGD3("ImgGet-stuff, " P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
        P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
    //
    return OK;
};

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
stuffImagePut(QueNode_T & node, STREAM_IMG const streamImg)
{
    P1INFO_NODE_STREAM(3, node, streamImg);
    //
    P1_CHECK_STREAM(IMG, streamImg);
    P1_CHECK_CONFIG_STREAM(Img, node, streamImg);
    //
    if (node.streamBufImg[streamImg].spImgBuf == NULL) {
        MY_LOGE("destroy stuff buffer without ImageBuffer "
            P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
            P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
        return BAD_VALUE;
    }
    if (node.streamBufImg[streamImg].eLockState == STREAM_BUF_LOCK_NONE) {
        MY_LOGI("destroy stuff buffer skip "
            P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
            P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
        return BAD_VALUE;
    }
    MY_LOGD3("ImgPut-stuff, " P1INFO_STREAM_IMG_STR " " P1INFO_NODE_STR,
        P1INFO_STREAM_IMG_VAR(node), P1INFO_NODE_VAR(node));
    //
    destroyStuffBuffer(node.streamBufImg[streamImg].spImgBuf);
    //
    node.streamBufImg[streamImg].eLockState = STREAM_BUF_LOCK_NONE;
    node.streamBufImg[streamImg].spImgBuf = NULL;
    node.streamBufImg[streamImg].spStreamBuf = NULL;
    node.streamBufImg[streamImg].eSrcType = IMG_BUF_SRC_NULL;
    //
    return OK;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
P1NodeImp::
findPortBufIndex(QBufInfo & deqBuf, QueJob_T & job) {
    size_t job_size = job.mSet.size();
    if ((job_size == 0) || (deqBuf.mvOut.size() % job_size > 0)) {
        MY_LOGE("Output size is not match");
        return MFALSE;
    };
    // assume the port order is the same in each de-queue set,
    // it only check the first de-queue set and apply to each node
    P1_OUTPUT_PORT port = P1_OUTPUT_PORT_TOTAL;
    MUINT32 group = 0;
    MUINT32 index = 0;
    for (size_t i = 0; i < (deqBuf.mvOut.size()); i += job_size) {
        index = deqBuf.mvOut[i].mPortID.index;
        port = P1_OUTPUT_PORT_TOTAL;
        if (index == PORT_RRZO.index) {
            port = P1_OUTPUT_PORT_RRZO;
        } else if (index == PORT_IMGO.index) {
            port = P1_OUTPUT_PORT_IMGO;
        } else if (index == PORT_EISO.index) {
            port = P1_OUTPUT_PORT_EISO;
        } else if (index == PORT_LCSO.index) {
            port = P1_OUTPUT_PORT_LCSO;
        } else if (index == PORT_RSSO.index) {
            port = P1_OUTPUT_PORT_RSSO;
        } else {
            MY_LOGE("Output port is not match");
            return MFALSE;
        }
        //
        if (port < P1_OUTPUT_PORT_TOTAL) {
            for (size_t j = 0; j < job_size; j++) {
                job.mSet.editItemAt(j).portBufIndex[port] =
                    (group * job_size) + j;
            }
        }
        group ++;
    }
    return MTRUE;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
P1NodeImp::
findRequestStream(QueNode_T & node)
{
    if (node.appFrame == NULL) {
        return OK;
    }
    IPipelineFrame::InfoIOMapSet rIOMapSet;
    if(OK != node.appFrame->queryInfoIOMapSet(getNodeId(), rIOMapSet)) {
        MY_LOGE("queryInfoIOMap failed");
        return BAD_VALUE;
    }
    //
    IPipelineFrame::ImageInfoIOMapSet& imageIOMapSet =
                                            rIOMapSet.mImageInfoIOMapSet;
    if(imageIOMapSet.isEmpty()) {
        MY_LOGW("no imageIOMap in frame");
        return BAD_VALUE;
    }
    //
    IPipelineFrame::MetaInfoIOMapSet& metaIOMapSet =
                                            rIOMapSet.mMetaInfoIOMapSet;
    if(metaIOMapSet.isEmpty()) {
        MY_LOGW("no metaIOMap in frame");
        return BAD_VALUE;
    }
    //
    #define REG_STREAM_IMG(id, item)    \
        if ((!node.streamBufImg[item].bExist) &&\
            (mvStreamImg[item] != NULL) &&\
            (mvStreamImg[item]->getStreamId() == id)) {\
            node.streamBufImg[item].bExist |= MTRUE; continue; };
    #define REG_STREAM_META(id, item)    \
        if ((!node.streamBufMeta[item].bExist) &&\
            (mvStreamMeta[item] != NULL) &&\
            (mvStreamMeta[item]->getStreamId() == id)) {\
            node.streamBufMeta[item].bExist |= MTRUE; continue; };
    //
    for (size_t i = 0; i < imageIOMapSet.size(); i++) {
        IPipelineFrame::ImageInfoIOMap const& imageIOMap = imageIOMapSet[i];
        if (imageIOMap.vIn.size() > 0) {
            for (size_t j = 0; j < imageIOMap.vIn.size(); j++) {
                StreamId_T const streamId = imageIOMap.vIn.keyAt(j);
                REG_STREAM_IMG(streamId, STREAM_IMG_IN_YUV);
                REG_STREAM_IMG(streamId, STREAM_IMG_IN_OPAQUE);
            }
        }
        if (imageIOMap.vOut.size() > 0) {
            for (size_t j = 0; j < imageIOMap.vOut.size(); j++) {
                StreamId_T const streamId = imageIOMap.vOut.keyAt(j);
                REG_STREAM_IMG(streamId, STREAM_IMG_OUT_OPAQUE);
                REG_STREAM_IMG(streamId, STREAM_IMG_OUT_FULL);
                REG_STREAM_IMG(streamId, STREAM_IMG_OUT_RESIZE);
                REG_STREAM_IMG(streamId, STREAM_IMG_OUT_LCS);
                REG_STREAM_IMG(streamId, STREAM_IMG_OUT_RSS);
            }
        }
    }
    //
    for (size_t i = 0; i < metaIOMapSet.size(); i++) {
        IPipelineFrame::MetaInfoIOMap const& metaIOMap = metaIOMapSet[i];
        if (metaIOMap.vIn.size() > 0) {
            for (size_t j = 0; j < metaIOMap.vIn.size(); j++) {
                StreamId_T const streamId = metaIOMap.vIn.keyAt(j);
                REG_STREAM_META(streamId, STREAM_META_IN_APP);
                REG_STREAM_META(streamId, STREAM_META_IN_HAL);
            }
        }
        if (metaIOMap.vOut.size() > 0) {
            for (size_t j = 0; j < metaIOMap.vOut.size(); j++) {
                StreamId_T const streamId = metaIOMap.vOut.keyAt(j);
                REG_STREAM_META(streamId, STREAM_META_OUT_APP);
                REG_STREAM_META(streamId, STREAM_META_OUT_HAL);
            }
        }
    }
    //
    #undef REG_STREAM_IMG
    #undef REG_STREAM_META
    //
    #if (IS_P1_LOGI)
    if (mLogLevelI > 1) {
        android::String8 strInfo("");
        strInfo += String8::format("(%d) IMG[", node.magicNum);
        for (int stream = STREAM_ITEM_START; stream < STREAM_IMG_NUM;
            stream++) {
            strInfo +=
                String8::format("%d ", node.streamBufImg[stream].bExist);
        };
        strInfo += String8::format("] META[");
        for (int stream = STREAM_ITEM_START; stream < STREAM_META_NUM;
            stream++) {
            strInfo +=
                String8::format("%d ", node.streamBufMeta[stream].bExist);
        };
        strInfo += String8::format("]");
        MY_LOGD2("%s", strInfo.string());
    }
    #endif
    //
    return OK;
};

/******************************************************************************
 *
 ******************************************************************************/
MVOID
P1NodeImp::onReturnFrame(QueNode_T & node, MBOOL isFlush, MBOOL isTrigger)
{
    node.needFlush = isFlush;
    if (node.needFlush && getActive()) {
        MY_LOGD("need flush node " P1INFO_NODE_STR, P1INFO_NODE_VAR(node));
    };
    //
    if (mpDeliverMgr != NULL && mpDeliverMgr->runningGet()) {
        mpDeliverMgr->sendNodeQueue(node, isTrigger);
    } else {
        releaseNode(node);
    }
    return;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
P1NodeImp::DeliverMgr::
deliverLoop() {
    MBOOL res = MTRUE;
    Vector<QueNode_T> outQueue;

    if (mpP1NodeImp != NULL && mpP1NodeImp->mpTimingCheckerMgr != NULL) {
        mpP1NodeImp->mpTimingCheckerMgr->onCheck();
    }
    //
    if (mpP1NodeImp != NULL) { // check the DropQ
        mpP1NodeImp->onProcessDropFrame(MFALSE);
    }
    //
    {
        Mutex::Autolock _l(mDeliverLock);
        MINT32 currentNum = 0;
        //
        mLoopState = LOOP_STATE_INIT;
        currentNum = (mNodeQueue.empty()) ? (0) :
            ((mNodeQueue.end() - 1)->magicNum);
        if (!exitPending()) {
            //MY_LOGD("deliverLoop Num(%d == %d)", currentNum, mSentNum);
            if (currentNum == mSentNum) {
                mLoopState = LOOP_STATE_WAITING;
                MY_LOGD_IF(mLogLevel > 1, "deliverLoop wait ++");
                mDeliverCond.wait(mDeliverLock);
                MY_LOGD_IF(mLogLevel > 1, "deliverLoop wait --");
            } // else , there is new coming node need to check
        } else {
            MY_LOGD("deliverLoop need to exit");
        }
        //
        mLoopState = LOOP_STATE_PROCESSING;
        mSentNum = currentNum;
        //
        //dumpNumList();
        //dumpNodeQueue();
        //
        List<MINT32>::iterator it_list = mNumList.begin();
        Vector<QueNode_T>::iterator it_node = mNodeQueue.begin();
        MBOOL isFound = MFALSE;
        size_t i = mNumList.size();
        //MY_LOGD_IF(mLogLevel > 1, "mNodeQueue(%d) +++", mNodeQueue.size());
        for (; it_list != mNumList.end() && i > 0; i--) {
            isFound = MFALSE;
            it_node = mNodeQueue.begin();
            for(; it_node != mNodeQueue.end(); ) {
                if (it_node->magicNum == (MUINT32)(*it_list)) {
                    outQueue.push_back(*it_node);
                    it_node = mNodeQueue.erase(it_node);
                    isFound = MTRUE;
                    break;
                } else {
                    it_node++;
                }
            }
            if (isFound) {
                it_list = mNumList.erase(it_list);
            } else {
                break;
            }
        }
        //MY_LOGD_IF(mLogLevel > 1, "mNodeQueue(%d) ---", mNodeQueue.size());
    }
    //
    if (outQueue.empty()) {
        //MY_LOGD_IF(mLogLevel > 1, "there is no node need to deliver");
    } else {
        Vector<QueNode_T>::iterator it = outQueue.begin();
        //MY_LOGD_IF(mLogLevel > 1, "outQueue.size(%d)", outQueue.size());
        for(; it != outQueue.end(); it++) {
            //MY_LOGD_IF(mLogLevel > 1, "it->magicNum : (%d)", it->magicNum);
            mpP1NodeImp->releaseNode(*it);
        }
        outQueue.clear();
    }

    {
        Mutex::Autolock _l(mDeliverLock);
        mLoopState = LOOP_STATE_DONE;
        mDoneCond.broadcast();
    }

    return res;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
P1NodeImp::DeliverMgr::
registerNodeList(MINT32 num)
{
    Mutex::Autolock _l(mDeliverLock);
    if (!mLoopRunning) {
        return MFALSE;
    }
    mNumList.push_back(num);
    return (!mNumList.empty());
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
P1NodeImp::DeliverMgr::
sendNodeQueue(QueNode_T & node, MBOOL needTrigger)
{
    {
        Mutex::Autolock _l(mDeliverLock);
        mNodeQueue.push_back(node);
    }
    if (needTrigger) {
        trigger();
    };
    //
    return MTRUE;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
P1NodeImp::DeliverMgr::
waitFlush(MBOOL needTrigger)
{
    size_t queueSize = 0;
    MBOOL isListEmpty = MFALSE;
    if (!runningGet()) {
        return MTRUE;
    };
    //
    {
        Mutex::Autolock _l(mDeliverLock);
        queueSize = mNodeQueue.size();
        isListEmpty = (mNumList.size() > 0) ? (MFALSE) : (MTRUE);
    }
    //MY_LOGI("QueSize(%d) ListEmpty(%d) - start", queueSize, isListEmpty);
    while (queueSize > 0) {
        if (needTrigger) {
            trigger();
        }
        {
            Mutex::Autolock _l(mDeliverLock);
            if ((mNodeQueue.size() > 0 && mLoopState == LOOP_STATE_WAITING) ||
                mLoopState == LOOP_STATE_PROCESSING) {
                MY_LOGD("doneLoop wait ++");
                mDoneCond.wait(mDeliverLock);
                MY_LOGD("doneLoop wait --");
            }
            queueSize = mNodeQueue.size();
            isListEmpty = (mNumList.size() > 0) ? (MFALSE) : (MTRUE);
        }
        //MY_LOGI("QueSize(%d) ListEmpty(%d) - next", queueSize, isListEmpty);
    };
    //MY_LOGI("QueSize(%d) ListEmpty(%d) - end", queueSize, isListEmpty);
    //
    if (!isListEmpty) {
        MY_LOGW("ListEmpty(%d)", isListEmpty);
        dumpNumList(MTRUE);
        dumpNodeQueue(MTRUE);
        return MFALSE;
    }
    return MTRUE;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
P1NodeImp::DeliverMgr::
trigger(void)
{
    Mutex::Autolock _l(mDeliverLock);
    /*if (!mNodeQueue.empty())*/
    if (mLoopRunning) {
        MY_LOGD_IF(mLogLevel > 1, "DeliverMgr trigger (%zu)", mNodeQueue.size());
        mDeliverCond.broadcast();
    }
    return MTRUE;
};

/******************************************************************************
 *
 ******************************************************************************/
void
P1NodeImp::DeliverMgr::
dumpNumList(MBOOL isLock)
{
    String8 str("");
    size_t size = 0;
    //
    NEED_LOCK(isLock, mDeliverLock);
    //
    size= mNumList.size();
    List<MINT32>::iterator it = mNumList.begin();
    for (; it != mNumList.end(); it++) {
        str += String8::format("%d ", *it);
    }
    //
    NEED_UNLOCK(isLock, mDeliverLock);
    //
    MY_LOGI("dump NumList[%zu] = {%s}", size, str.string());
};

/******************************************************************************
 *
 ******************************************************************************/
void
P1NodeImp::DeliverMgr::
dumpNodeQueue(MBOOL isLock)
{
    String8 str("");
    size_t size = 0;
    //
    NEED_LOCK(isLock, mDeliverLock);
    //
    size = mNodeQueue.size();
    Vector<QueNode_T>::iterator it = mNodeQueue.begin();
    for(; it != mNodeQueue.end(); it++) {
        str += String8::format("%d ", (*it).magicNum);
    }
    //
    NEED_UNLOCK(isLock, mDeliverLock);
    //
    MY_LOGI("dump NodeQueue[%zu] = {%s}", size, str.string());
};

/******************************************************************************
 *
 ******************************************************************************/
sp<P1Node>
P1Node::
createInstance()
{
    return new P1NodeImp();

}

