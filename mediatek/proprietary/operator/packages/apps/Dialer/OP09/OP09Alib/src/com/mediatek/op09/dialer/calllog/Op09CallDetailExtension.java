package com.mediatek.op09.dialer.calllog;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mediatek.dialer.ext.DefaultCallDetailExtension;
import com.mediatek.op09.dialer.Op09DialerPluginUtil;
import com.mediatek.op09.dialer.R;

public class Op09CallDetailExtension extends DefaultCallDetailExtension {
    private static final String TAG = "Op09CallDetailExtension";
    private static final String ACCOUNT_LABEL = "phone_account_label";
    private static final String ID = "id";
    public static final int ICON_ID = 100;

    private static final String CALLER_NAME = "caller_name";
    /**
     * for OP09
     * @param Context context
     * @param PhoneAccountHandle phoneAccountHandle
     */
    public void setCallAccountForCallDetail(Context context,
                                            PhoneAccountHandle phoneAccountHandle) {
        log("setCallAccountForCallDetail");
        TelephonyManager telephonyManager = (TelephonyManager) context.
                getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.getDefault().getPhoneCount() < 2) {
            return;
        }
        Op09DialerPluginUtil dialerPlugin = new Op09DialerPluginUtil(context);
        Context pluginContext = dialerPlugin.getPluginContext();
        Resources resource = context.getResources();
        String packageName = context.getPackageName();

        View accountLable =
                ((Activity) context).findViewById(
                        resource.getIdentifier(ACCOUNT_LABEL, ID, packageName));

        View callerName = ((Activity) context).findViewById(
                        resource.getIdentifier(CALLER_NAME, ID, packageName));
        LinearLayout callDetail = (LinearLayout) callerName.getParent();

        TelecomManager telecomManager = (TelecomManager) context.getSystemService(
                Context.TELECOM_SERVICE);
        PhoneAccount account = telecomManager.getPhoneAccount(phoneAccountHandle);

        if (null == callDetail || null == account) {
            return;
        }
        //callDetail.setOrientation(LinearLayout.VERTICAL);
        callDetail.removeView(accountLable);
        Bitmap iconBitmap = account.getIcon().getBitmap();
        View viewChild = callDetail.findViewById(ICON_ID);
        if (null == viewChild && null != iconBitmap) {
            LayoutInflater mInflater;
            mInflater = LayoutInflater.from(pluginContext);
            LinearLayout iconLayout = (LinearLayout)
              mInflater.inflate(R.layout.ct_sim_indicator, null);
            iconLayout.setId(ICON_ID);
            ImageView simIndicator = (ImageView)iconLayout.findViewById(R.id.simIndicator);
            simIndicator.setVisibility(View.VISIBLE);
            simIndicator.setImageDrawable(new BitmapDrawable(iconBitmap));
            iconLayout.addView(accountLable, 1);
            callDetail.addView(iconLayout, 2);
        }
    }

    private void log(String msg) {
        Log.d(TAG, msg);
    }
}
