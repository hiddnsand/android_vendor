/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.eminfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemProperties;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import com.android.internal.telephony.PhoneConstants;
import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ComponentSelectActivity extends Activity implements OnCheckedChangeListener {
    private static final String COMPONENT_RSRPSINR_CONFIG_SHAREPRE=
        "telephony_rsrpsinr_config_settings";
    private static final String TAG = "EmInfo";
    private static final String PREF_FILE = "pref_file";
    private static final String KEY_SELECTED = "selecte";
    private static final int CHECK_INFOMATION_ID = Menu.FIRST; // only use Menu.FIRST
    private static final int SELECT_ALL_ID = Menu.FIRST + 1;
    private static final int SELECT_NONE_ID = Menu.FIRST + 2;

    private List<CheckBox> mCheckBoxes;
    private int mSimType;
    private SharedPreferences mPref;
    private Set<String> mSelected;


    private static final int STRONG_POINT1_X_DEFAULT = -80;
    private static final int STRONG_POINT1_Y_DEFAULT = 30;
    private static final int STRONG_POINT2_X_DEFAULT = -80;
    private static final int STRONG_POINT2_Y_DEFAULT = 20;
    private static final int STRONG_POINT3_X_DEFAULT = -30;
    private static final int STRONG_POINT3_Y_DEFAULT = 20;

    private static final int MEDIUMWEAK_POINT1_X_DEFAULT = -140;
    private static final int MEDIUMWEAK_POINT1_Y_DEFAULT = 10;
    private static final int MEDIUMWEAK_POINT2_X_DEFAULT = -90;
    private static final int MEDIUMWEAK_POINT2_Y_DEFAULT = 10;
    private static final int MEDIUMWEAK_POINT3_X_DEFAULT = -90;
    private static final int MEDIUMWEAK_POINT3_Y_DEFAULT = -20;

    private static final int WEAK_POINT1_X_DEFAULT = -140;
    private static final int WEAK_POINT1_Y_DEFAULT = 5;
    private static final int WEAK_POINT2_X_DEFAULT = -100;
    private static final int WEAK_POINT2_Y_DEFAULT = 5;
    private static final int WEAK_POINT3_X_DEFAULT = -100;
    private static final int WEAK_POINT3_Y_DEFAULT = -20;

    private static int mStrongPoint1X = -80;
    private static int mStrongPoint1Y = 30;
    private static int mStrongPoint2X = -80;
    private static int mStrongPoint2Y = 20;
    private static int mStrongPoint3X = -30;
    private static int mStrongPoint3Y = 20;
    private static int mMediumWeakPoint1X = -140;
    private static int mMediumWeakPoint1Y = 10;
    private static int mMediumWeakPoint2X = -90;
    private static int mMediumWeakPoint2Y = 10;
    private static int mMediumWeakPoint3X = -90;
    private static int mMediumWeakPoint3Y = -20;
    private static int mWeakPoint1X = -140;
    private static int mWeakPoint1Y = 5;
    private static int mWeakPoint2X = -100;
    private static int mWeakPoint2Y = 5;
    private static int mWeakPoint3X = -100;
    private static int mWeakPoint3Y = -20;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.component_select);
        ViewGroup list = (ViewGroup) findViewById(R.id.list);
        LayoutParams param = new LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        param.setMargins(0, 5, 0, 5);
        LayoutParams param2 = new LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        param2.setMargins(30, 5, 0, 5);

        Intent intent = getIntent();
        mSimType = intent.getIntExtra("mSimType", PhoneConstants.SIM_ID_1);

        mPref = getSharedPreferences(PREF_FILE, MODE_PRIVATE);
        mSelected = mPref.getStringSet(KEY_SELECTED, null);
        if (mSelected == null) {
            mSelected = new HashSet<String>();
        }

        List<Component> allComponents = Component.getComponents(this);
        Component[] tmp = allComponents.toArray(new Component[allComponents.size()]);
        Arrays.sort(tmp);
        allComponents = Arrays.asList(tmp);

        List<String> groups = new ArrayList<String>();
        for (Component c : allComponents) {
            if (!groups.contains(c.getGroup())) {
                groups.add(c.getGroup());
            }
        }
        String[] groupName = groups.toArray(new String[groups.size()]);
        Arrays.sort(groupName);
        groups = Arrays.asList(groupName);

        mCheckBoxes = new ArrayList<CheckBox>();
        for (String g : groups) {
            CheckBox checkBox = new CheckBox(this);
            checkBox.setText(g);
            ArrayList<CheckBox> groupComponents = new ArrayList<CheckBox>();
            checkBox.setTag(groupComponents);
            checkBox.setOnCheckedChangeListener(this);
            checkBox.setChecked(mSelected.contains(g));
            list.addView(checkBox, param);
            mCheckBoxes.add(checkBox);

            for (Component c : allComponents) {
                if(mSimType == PhoneConstants.SIM_ID_2 && (!c.supportMultiSIM())) {
                    continue;
                }
                if (c.getGroup().equals(g)) {
                    checkBox = new CheckBox(this);
                    checkBox.setText(c.getName());
                    checkBox.setTag(c);
                    checkBox.setOnCheckedChangeListener(this);
                    checkBox.setChecked(mSelected.contains(c.getName()));
                    list.addView(checkBox, param2);
                    mCheckBoxes.add(checkBox);
                    groupComponents.add(checkBox);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, CHECK_INFOMATION_ID, 0, getString(R.string.networkinfo_check));
        menu.add(0, SELECT_ALL_ID, 0, "Select All");
        menu.add(0, SELECT_NONE_ID, 0, "Select None");
        return true;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getTag() != null && buttonView.getTag() instanceof ArrayList) {
            ArrayList<CheckBox> groupComponents = (ArrayList<CheckBox>) buttonView.getTag();
            for (CheckBox c : groupComponents) {
                c.setChecked(isChecked);
            }
        }
        if (isChecked) {
            mSelected.add(buttonView.getText().toString());
        } else {
            mSelected.remove(buttonView.getText().toString());
        }
        mPref.edit().putStringSet(KEY_SELECTED, mSelected).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem aMenuItem) {
        switch (aMenuItem.getItemId()) {
        case CHECK_INFOMATION_ID:
            ArrayList<String> components = new ArrayList<String>();
            boolean bNeedShowConfig = false;
            for (int i = 0; i < mCheckBoxes.size(); i++) {
                CheckBox c = mCheckBoxes.get(i);
                if (c.isChecked() && c.getTag() instanceof Component) {
                    components.add(c.getText().toString());
                    if (c.getText().toString().equals("CC0/CC1 RSRP and SINR")) {
                        bNeedShowConfig = true;
                    }
                }
            }
            if (bNeedShowConfig) {
                showRsrpSinrConfigDlg(components);
            } else {
                Intent intent = new Intent(this, ComponentDetailActivity.class);
                intent.putExtra("mChecked", components);
                intent.putExtra("mSimType", mSimType);
                this.startActivity(intent);
            }
            break;
        case SELECT_ALL_ID:
            for (CheckBox c : mCheckBoxes) {
                c.setChecked(true);
            }
            break;
        case SELECT_NONE_ID:
            for (CheckBox c : mCheckBoxes) {
                c.setChecked(false);
            }
            break;
        default:
            break;
        }
        return super.onOptionsItemSelected(aMenuItem);
    }


    private interface InputDialogOnClickListener {
        public void onClick(DialogInterface dialog, int which, String inputStrong1XStr,
            String inputStrong1YStr, String inputStrong2XStr, String inputStrong2YStr,
            String inputStrong3XStr, String inputStrong3YStr, String inputMediumWeak1XStr,
            String inputMediumWeak1YStr, String inputMediumWeak2XStr, String inputMediumWeak2YStr,
            String inputMediumWeak3XStr,String inputMediumWeak3YStr, String inputWeak1XStr,
            String inputWeak1YStr,String inputWeak2XStr, String inputWeak2YStr,
            String inputWeak3XStr,
            String inputWeak3YStr);
    }

    private void showMultiInputDlg(CharSequence title, final InputDialogOnClickListener listener) {
        View inputView = getLayoutInflater().inflate(R.layout.em_multi_input_layout, null);
        Button okBtn = (Button) inputView.findViewById(R.id.em_multi_input_ok_btn);
        Button cancelBtn = (Button) inputView.findViewById(R.id.em_multi_input_cancel_btn);
        final EditText strongPoint1XEdit = (EditText) inputView.findViewById(
            R.id.strong_point1_x_edit);
        final EditText strongPoint1YEdit = (EditText) inputView.findViewById(
            R.id.strong_point1_y_edit);
        final EditText strongPoint2XEdit = (EditText) inputView.findViewById(
            R.id.strong_point2_x_edit);
        final EditText strongPoint2YEdit = (EditText) inputView.findViewById(
            R.id.strong_point2_y_edit);
        final EditText strongPoint3XEdit = (EditText) inputView.findViewById(
            R.id.strong_point3_x_edit);
        final EditText strongPoint3YEdit = (EditText) inputView.findViewById(
            R.id.strong_point3_y_edit);
        final EditText mediumweakPoint1XEdit = (EditText) inputView.findViewById(
            R.id.mediumweak_point1_x_edit);
        final EditText mediumweakPoint1YEdit = (EditText) inputView.findViewById(
            R.id.mediumweak_point1_y_edit);
        final EditText mediumweakPoint2XEdit = (EditText) inputView.findViewById(
            R.id.mediumweak_point2_x_edit);
        final EditText mediumweakPoint2YEdit = (EditText) inputView.findViewById(
            R.id.mediumweak_point2_y_edit);
        final EditText mediumweakPoint3XEdit = (EditText) inputView.findViewById(
            R.id.mediumweak_point3_x_edit);
        final EditText mediumweakPoint3YEdit = (EditText) inputView.findViewById(
            R.id.mediumweak_point3_y_edit);
        final EditText weakPoint1XEdit = (EditText) inputView.findViewById(
            R.id.weak_point1_x_edit);
        final EditText weakPoint1YEdit = (EditText) inputView.findViewById(
            R.id.weak_point1_y_edit);
        final EditText weakPoint2XEdit = (EditText) inputView.findViewById(
            R.id.weak_point2_x_edit);
        final EditText weakPoint2YEdit = (EditText) inputView.findViewById(
            R.id.weak_point2_y_edit);
        final EditText weakPoint3XEdit = (EditText) inputView.findViewById(
            R.id.weak_point3_x_edit);
        final EditText weakPoint3YEdit = (EditText) inputView.findViewById(
            R.id.weak_point3_y_edit);

        final SharedPreferences rsrpSinrConfigSh = getSharedPreferences(
        COMPONENT_RSRPSINR_CONFIG_SHAREPRE,android.content.Context.MODE_PRIVATE);
        int strongPoint1X = rsrpSinrConfigSh.getInt(getString(
            R.string.strong_point_1x), STRONG_POINT1_X_DEFAULT);
        int strongPoint1Y = rsrpSinrConfigSh.getInt(getString(
            R.string.strong_point_1y), STRONG_POINT1_Y_DEFAULT);
        int strongPoint2X = rsrpSinrConfigSh.getInt(getString(
            R.string.strong_point_2x), STRONG_POINT2_X_DEFAULT);
        int strongPoint2Y = rsrpSinrConfigSh.getInt(getString(
            R.string.strong_point_2y), STRONG_POINT2_Y_DEFAULT);
        int strongPoint3X = rsrpSinrConfigSh.getInt(getString(
            R.string.strong_point_3x), STRONG_POINT3_X_DEFAULT);
        int strongPoint3Y = rsrpSinrConfigSh.getInt(getString(
            R.string.strong_point_3y), STRONG_POINT3_Y_DEFAULT);
        int mediumWeakPoint1X = rsrpSinrConfigSh.getInt(getString(
            R.string.mediumweak_point_1x), MEDIUMWEAK_POINT1_X_DEFAULT);
        int mediumWeakPoint1Y = rsrpSinrConfigSh.getInt(getString(
            R.string.mediumweak_point_1y), MEDIUMWEAK_POINT1_Y_DEFAULT);
        int mediumWeakPoint2X = rsrpSinrConfigSh.getInt(getString(
            R.string.mediumweak_point_2x), MEDIUMWEAK_POINT2_X_DEFAULT);
        int mediumWeakPoint2Y = rsrpSinrConfigSh.getInt(getString(
            R.string.mediumweak_point_2y), MEDIUMWEAK_POINT2_Y_DEFAULT);
        int mediumWeakPoint3X = rsrpSinrConfigSh.getInt(getString(
            R.string.mediumweak_point_3x), MEDIUMWEAK_POINT3_X_DEFAULT);
        int mediumWeakPoint3Y = rsrpSinrConfigSh.getInt(getString(
            R.string.mediumweak_point_3y), MEDIUMWEAK_POINT3_Y_DEFAULT);
        int weakPoint1X = rsrpSinrConfigSh.getInt(getString(
            R.string.weak_point_1x), WEAK_POINT1_X_DEFAULT);
        int weakPoint1Y = rsrpSinrConfigSh.getInt(getString(
            R.string.weak_point_1y), WEAK_POINT1_Y_DEFAULT);
        int weakPoint2X = rsrpSinrConfigSh.getInt(getString(
            R.string.weak_point_2x), WEAK_POINT2_X_DEFAULT);
        int weakPoint2Y = rsrpSinrConfigSh.getInt(getString(
            R.string.weak_point_2y), WEAK_POINT2_Y_DEFAULT);
        int weakPoint3X = rsrpSinrConfigSh.getInt(getString(
            R.string.weak_point_3x), WEAK_POINT3_X_DEFAULT);
        int weakPoint3Y = rsrpSinrConfigSh.getInt(getString(
            R.string.weak_point_3y), WEAK_POINT3_Y_DEFAULT);

        strongPoint1XEdit.setText(String.valueOf(strongPoint1X));
        strongPoint1YEdit.setText(String.valueOf(strongPoint1Y));
        strongPoint2XEdit.setText(String.valueOf(strongPoint2X));
        strongPoint2YEdit.setText(String.valueOf(strongPoint2Y));
        strongPoint3XEdit.setText(String.valueOf(strongPoint3X));
        strongPoint3YEdit.setText(String.valueOf(strongPoint3Y));
        mediumweakPoint1XEdit.setText(String.valueOf(mediumWeakPoint1X));
        mediumweakPoint1YEdit.setText(String.valueOf(mediumWeakPoint1Y));
        mediumweakPoint2XEdit.setText(String.valueOf(mediumWeakPoint2X));
        mediumweakPoint2YEdit.setText(String.valueOf(mediumWeakPoint2Y));
        mediumweakPoint3XEdit.setText(String.valueOf(mediumWeakPoint3X));
        mediumweakPoint3YEdit.setText(String.valueOf(mediumWeakPoint3Y));
        weakPoint1XEdit.setText(String.valueOf(weakPoint1X));
        weakPoint1YEdit.setText(String.valueOf(weakPoint1Y));
        weakPoint2XEdit.setText(String.valueOf(weakPoint2X));
        weakPoint2YEdit.setText(String.valueOf(weakPoint2Y));
        weakPoint3XEdit.setText(String.valueOf(weakPoint3X));
        weakPoint3YEdit.setText(String.valueOf(weakPoint3Y));

        final AlertDialog dialog = new AlertDialog.Builder(this).setCancelable(
                false).setTitle(title).setView(inputView).create();
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(dialog, DialogInterface.BUTTON_POSITIVE,
                    strongPoint1XEdit.getText().toString(),
                    strongPoint1YEdit.getText().toString(),
                    strongPoint2XEdit.getText().toString(),
                    strongPoint2YEdit.getText().toString(),
                    strongPoint3XEdit.getText().toString(),
                    strongPoint3YEdit.getText().toString(),
                    mediumweakPoint1XEdit.getText().toString(),
                    mediumweakPoint1YEdit.getText().toString(),
                    mediumweakPoint2XEdit.getText().toString(),
                    mediumweakPoint2YEdit.getText().toString(),
                    mediumweakPoint3XEdit.getText().toString(),
                    mediumweakPoint3YEdit.getText().toString(),
                    weakPoint1XEdit.getText().toString(),
                    weakPoint1YEdit.getText().toString(),
                    weakPoint2XEdit.getText().toString(),
                    weakPoint2YEdit.getText().toString(),
                    weakPoint3XEdit.getText().toString(),
                    weakPoint3YEdit.getText().toString());
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE,
                    strongPoint1XEdit.getText().toString(),
                    strongPoint1YEdit.getText().toString(),
                    strongPoint2XEdit.getText().toString(),
                    strongPoint2YEdit.getText().toString(),
                    strongPoint3XEdit.getText().toString(),
                    strongPoint3YEdit.getText().toString(),
                    mediumweakPoint1XEdit.getText().toString(),
                    mediumweakPoint1YEdit.getText().toString(),
                    mediumweakPoint2XEdit.getText().toString(),
                    mediumweakPoint2YEdit.getText().toString(),
                    mediumweakPoint3XEdit.getText().toString(),
                    mediumweakPoint3YEdit.getText().toString(),
                    weakPoint1XEdit.getText().toString(),
                    weakPoint1YEdit.getText().toString(),
                    weakPoint2XEdit.getText().toString(),
                    weakPoint2YEdit.getText().toString(),
                    weakPoint3XEdit.getText().toString(),
                    weakPoint3YEdit.getText().toString());
            }
        });
        dialog.show();
    }

    private void showRsrpSinrConfigDlg(final ArrayList<String> componentPara) {
        String title = getString(R.string.rsrp_sinr_config_input_tip);
        showMultiInputDlg(title, new InputDialogOnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, String inputStrong1XStr,
            String inputStrong1YStr, String inputStrong2XStr, String inputStrong2YStr,
            String inputStrong3XStr, String inputStrong3YStr, String inputMediumWeak1XStr,
            String inputMediumWeak1YStr, String inputMediumWeak2XStr, String inputMediumWeak2YStr,
            String inputMediumWeak3XStr, String inputMediumWeak3YStr, String inputWeak1XStr,
            String inputWeak1YStr, String inputWeak2XStr, String inputWeak2YStr,
            String inputWeak3XStr,
            String inputWeak3YStr) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    boolean validInput = true;
                    try {
                        mStrongPoint1X = Integer.parseInt(inputStrong1XStr);
                        mStrongPoint1Y = Integer.parseInt(inputStrong1YStr);
                        mStrongPoint2X = Integer.parseInt(inputStrong2XStr);
                        mStrongPoint2Y = Integer.parseInt(inputStrong2YStr);
                        mStrongPoint3X = Integer.parseInt(inputStrong3XStr);
                        mStrongPoint3Y = Integer.parseInt(inputStrong3YStr);
                        mMediumWeakPoint1X = Integer.parseInt(inputMediumWeak1XStr);
                        mMediumWeakPoint1Y = Integer.parseInt(inputMediumWeak1YStr);
                        mMediumWeakPoint2X = Integer.parseInt(inputMediumWeak2XStr);
                        mMediumWeakPoint2Y = Integer.parseInt(inputMediumWeak2YStr);
                        mMediumWeakPoint3X = Integer.parseInt(inputMediumWeak3XStr);
                        mMediumWeakPoint3Y = Integer.parseInt(inputMediumWeak3YStr);
                        mWeakPoint1X = Integer.parseInt(inputWeak1XStr);
                        mWeakPoint1Y = Integer.parseInt(inputWeak1YStr);
                        mWeakPoint2X = Integer.parseInt(inputWeak2XStr);
                        mWeakPoint2Y = Integer.parseInt(inputWeak2YStr);
                        mWeakPoint3X = Integer.parseInt(inputWeak3XStr);
                        mWeakPoint3Y = Integer.parseInt(inputWeak3YStr);
                    } catch (NumberFormatException e) {
                        validInput = false;
                    }
                    if (validInput) {
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mStrongPoint1X " + mStrongPoint1X);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mStrongPoint1Y " + mStrongPoint1Y);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mStrongPoint2X " + mStrongPoint2X);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mStrongPoint2Y " + mStrongPoint2Y);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mStrongPoint3X " + mStrongPoint3X);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mStrongPoint3Y " + mStrongPoint3Y);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mMediumWeakPoint1X " +
                        //mMediumWeakPoint1X);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mMediumWeakPoint1Y " +
                        //mMediumWeakPoint1Y);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mMediumWeakPoint2X " +
                        //mMediumWeakPoint2X);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mMediumWeakPoint2Y " +
                        //mMediumWeakPoint2Y);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mMediumWeakPoint3X " +
                        //mMediumWeakPoint3X);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mMediumWeakPoint3Y " +
                        //mMediumWeakPoint3Y);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mWeakPoint1X " + mWeakPoint1X);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mWeakPoint1Y " + mWeakPoint1Y);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mWeakPoint2X " + mWeakPoint2X);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mWeakPoint2Y " + mWeakPoint2Y);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mWeakPoint3X " + mWeakPoint3X);
                        //Elog.d(TAG, " showRsrpSinrConfigDlg  mWeakPoint3Y " + mWeakPoint3Y);
                        if (mStrongPoint1X < -140 || mStrongPoint1X > -30
                            || mStrongPoint2X < -140 || mStrongPoint2X > -30
                            || mStrongPoint3X < -140 || mStrongPoint3X > -30
                            || mMediumWeakPoint1X < -140 || mMediumWeakPoint1X > -30
                            || mMediumWeakPoint2X < -140 || mMediumWeakPoint2X > -30
                            || mMediumWeakPoint3X < -140 || mMediumWeakPoint3X > -30
                            || mWeakPoint1X < -140 || mWeakPoint1X > -30
                            || mWeakPoint2X < -140 || mWeakPoint2X > -30
                            || mWeakPoint3X < -140 || mWeakPoint3X > -30) {
                               Toast.makeText(ComponentSelectActivity.this,
                               "The Point X must be -140 ~ -30",
                                                         Toast.LENGTH_SHORT).show();
                        }else if(mStrongPoint1Y < -20 || mStrongPoint1Y > 30
                            || mStrongPoint2Y < -20 || mStrongPoint2Y > 30
                            || mStrongPoint3Y < -20 || mStrongPoint3Y > 30
                            || mMediumWeakPoint1Y < -20 || mMediumWeakPoint1Y > 30
                            || mMediumWeakPoint2Y < -20 || mMediumWeakPoint2Y > 30
                            || mMediumWeakPoint3Y < -20 || mMediumWeakPoint3Y > 30
                            || mWeakPoint1Y < -20 || mWeakPoint1Y > 30
                            || mWeakPoint2Y < -20 || mWeakPoint2Y > 30
                            || mWeakPoint3Y < -20 || mWeakPoint3Y > 30) {
                               Toast.makeText(ComponentSelectActivity.this,
                                   "The Point Y must be -20 ~ 30",
                                                         Toast.LENGTH_SHORT).show();
                        }else {
                            dialog.dismiss();
                            writeRsrpSinrSharedPreference(mStrongPoint1X, mStrongPoint1Y,
                                mStrongPoint2X, mStrongPoint2Y,
                                mStrongPoint3X,mStrongPoint3Y,
                                mMediumWeakPoint1X, mMediumWeakPoint1Y,
                                mMediumWeakPoint2X, mMediumWeakPoint2Y,
                                mMediumWeakPoint3X, mMediumWeakPoint3Y,
                                mWeakPoint1X, mWeakPoint1Y,
                                mWeakPoint2X, mWeakPoint2Y,
                                mWeakPoint3X, mWeakPoint3Y);

                            Intent intent = new Intent(ComponentSelectActivity.this,
                                ComponentDetailActivity.class);
                            intent.putExtra("mChecked", componentPara);
                            intent.putExtra("mSimType", mSimType);
                            ComponentSelectActivity.this.startActivity(intent);
                        }
                    } else {
                        Toast.makeText(ComponentSelectActivity.this, "Invalid RSRP/SINR",
                            Toast.LENGTH_SHORT).show();
                    }
                } else if(which == DialogInterface.BUTTON_NEGATIVE) {
                    mStrongPoint1X = -80;
                    mStrongPoint1Y = 30;
                    mStrongPoint2X = -80;
                    mStrongPoint2Y = 20;
                    mStrongPoint3X = -30;
                    mStrongPoint3Y = 20;
                    mMediumWeakPoint1X = -140;
                    mMediumWeakPoint1Y = 10;
                    mMediumWeakPoint2X = -90;
                    mMediumWeakPoint2Y = 10;
                    mMediumWeakPoint3X = -90;
                    mMediumWeakPoint3Y = -20;
                    mWeakPoint1X = -140;
                    mWeakPoint1Y = 5;
                    mWeakPoint2X = -100;
                    mWeakPoint2Y = 5;
                    mWeakPoint3X = -100;
                    mWeakPoint3Y = -20;
                    dialog.dismiss();
                    writeRsrpSinrSharedPreference(mStrongPoint1X, mStrongPoint1Y,
                        mStrongPoint2X, mStrongPoint2Y,
                        mStrongPoint3X, mStrongPoint3Y,
                        mMediumWeakPoint1X, mMediumWeakPoint1Y,
                        mMediumWeakPoint2X, mMediumWeakPoint2Y,
                        mMediumWeakPoint3X, mMediumWeakPoint3Y,
                        mWeakPoint1X, mWeakPoint1Y,
                        mWeakPoint2X, mWeakPoint2Y,
                        mWeakPoint3X, mWeakPoint3Y);
                    Intent intent = new Intent(ComponentSelectActivity.this,
                        ComponentDetailActivity.class);
                    intent.putExtra("mChecked", componentPara);
                    intent.putExtra("mSimType", mSimType);
                    ComponentSelectActivity.this.startActivity(intent);
                }
            }
        });
    }

    private void writeRsrpSinrSharedPreference(int strongPoint1X,
        int strongPoint1Y, int strongPoint2X, int strongPoint2Y,
        int strongPoint3X, int strongPoint3Y, int mediumWeakPoint1X,
        int mediumWeakPoint1Y, int mediumWeakPoint2X, int mediumWeakPoint2Y,
        int mediumWeakPoint3X, int mediumWeakPoint3Y, int weakPoint1X,
        int weakPoint1Y, int weakPoint2X, int weakPoint2Y,
        int weakPoint3X, int weakPoint3Y) {
        final SharedPreferences rsrpSinrConfigSh = getSharedPreferences(
                   COMPONENT_RSRPSINR_CONFIG_SHAREPRE, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = rsrpSinrConfigSh.edit();
        editor.putInt(getString(R.string.strong_point_1x), strongPoint1X);
        editor.putInt(getString(R.string.strong_point_1y), strongPoint1Y);
        editor.putInt(getString(R.string.strong_point_2x), strongPoint2X);
        editor.putInt(getString(R.string.strong_point_2y), strongPoint2Y);
        editor.putInt(getString(R.string.strong_point_3x), strongPoint3X);
        editor.putInt(getString(R.string.strong_point_3y), strongPoint3Y);
        editor.putInt(getString(R.string.mediumweak_point_1x), mediumWeakPoint1X);
        editor.putInt(getString(R.string.mediumweak_point_1y), mediumWeakPoint1Y);
        editor.putInt(getString(R.string.mediumweak_point_2x), mediumWeakPoint2X);
        editor.putInt(getString(R.string.mediumweak_point_2y), mediumWeakPoint2Y);
        editor.putInt(getString(R.string.mediumweak_point_3x), mediumWeakPoint3X);
        editor.putInt(getString(R.string.mediumweak_point_3y), mediumWeakPoint3Y);
        editor.putInt(getString(R.string.weak_point_1x), weakPoint1X);
        editor.putInt(getString(R.string.weak_point_1y), weakPoint1Y);
        editor.putInt(getString(R.string.weak_point_2x), weakPoint2X);
        editor.putInt(getString(R.string.weak_point_2y), weakPoint2Y);
        editor.putInt(getString(R.string.weak_point_3x), weakPoint3X);
        editor.putInt(getString(R.string.weak_point_3y), weakPoint3Y);
        editor.commit();
    }

}
