#include <stdio.h>
#include <fcntl.h>
#include <log/log.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <cutils/sockets.h>
#include <cutils/properties.h>

#include "global_var.h"
#include "mlog.h"
#include "config.h"
#include "bootmode.h"
#include "daemon.h"
#include "dump.h"
#include "libfunc.h"
#include "logging.h"
#include "debug_config.h"


BOOTMODE g_bootmode;
int ext_sdcard_ready = 0;
int meta_mblogenable = 0;
int normal_chiptest = 0;

/*
 * Read this time boot mode
 */

void set_status(MBLOGSTATUS status)
{
	g_mblog_status = status;
}

#define BOOT_MODE_INFO_FILE "/sys/class/BOOT/BOOT/boot/boot_mode"
#define BOOT_MODE_STR_LEN 1
int get_bootmode(void)
{
	int mode = -1;
	char buf[BOOT_MODE_STR_LEN + 1];
	int bytes_read = 0;
	int fd,res = 0;

	fd = open(BOOT_MODE_INFO_FILE, O_RDONLY);
	if (fd != -1) {
		memset(buf, 0, BOOT_MODE_STR_LEN + 1);
		while (bytes_read < BOOT_MODE_STR_LEN) {
			res = read(fd, buf + bytes_read, BOOT_MODE_STR_LEN);
			if (res > 0)
				bytes_read += res;
			else
				break;
		}
		close(fd);
		mode = atoi(buf);
	} else {
		MLOGE_DATA("open %s fail,%s", BOOT_MODE_INFO_FILE,strerror(errno));
		return -1;
	}

	MLOGI_DATA("boot mode is %d",mode);
	return mode;
}

int init_bootmode_and_config()
{
	int bootmode;

	bootmode = get_bootmode();
	if (bootmode == -1) {
		MLOGE_DATA("Get bootmode fail");
		g_bootmode = UNKNOWN_BOOT;
		return 0;
	}

	g_bootmode = (BOOTMODE)bootmode;

	if (access(CONFIG_FILE, F_OK) != 0) {
		MLOGI_DATA("Can not access %s, %s",CONFIG_FILE,strerror(errno));
		create_config_file();
	}
	return 1;
}

/*
 */
char *read_bootmode_config(char *key)
{
	if (!strcmp(key, KEY_SIZE) && g_mblog_status != SAVE_TO_SDCARD)
		return DATA_SIZE_DEFAULT;
	return read_config_value(key);
}

int check_external_sdcard()
{
	struct timeval begin, current;
	struct stat exlink;
	int ret,e;

	char ext_path[255];
	char uuid[20];
	DIR *src_dir;
	struct dirent *entry;

	bool found = false;
	#define ROOTDIR  PATH_EXTSD_PARENT

	// check inter-sdcard
	gettimeofday(&begin, NULL);
	suseconds_t timeout = 60000000; /*60s timeout*/
	while (1) {
		gettimeofday(&current, NULL);
		if (1000000 * (current.tv_sec - begin.tv_sec) + (current.tv_usec - begin.tv_usec) > timeout) {
			MLOGE_BOTH("check internal sdcard timeout!");
			//return 0; // fail
			break;
		}

		ret = lstat(PATH_DEFAULT, &exlink);
		e = errno;
		if (ret == -1) {
			MLOGE_BOTH("check '%s' fail(%s)", PATH_DEFAULT, strerror(e));
			usleep(1000000);
			continue;
		} else
			MLOGI_BOTH("Inter sdcard has been mounted !");
		break;
	}

	// Ensure inter-sdcard mounted, then check ext-sdcard
	int i = 15;
	while (i--) {
		src_dir = opendir(ROOTDIR);
		if (src_dir == NULL) {
			MLOGE_BOTH("Failed to opendir %s (%s)", ROOTDIR, strerror(errno));
			return 0; // fail
		}

		while((entry = readdir(src_dir)) != NULL) {
			if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")
				|| entry->d_type != DT_DIR) {
				//MLOGI_BOTH("/storage/.., file:%s, file type:%u", entry->d_name, entry->d_type);
				continue;
			}
			if (strlen(entry->d_name) == 9) {
				snprintf(uuid, sizeof(uuid), "%s", entry->d_name);
				if (uuid[4] == '-') {
					found = true;
					break;
				}
			}

		}

		if (closedir(src_dir)) {
			MLOGE_BOTH("Failed to closedir %s, (%s)", ROOTDIR, strerror(errno));
		}

		if (found == true) {
			snprintf(ext_path, sizeof(ext_path), "%s%s/", ROOTDIR, uuid);
			update_sd_context(ext_path);
			MLOGI_BOTH("Ext sdcard path: %s", ext_path);
			break;
		} else
			usleep(1000000);
	}

	if (found == false) {
		MLOGI_BOTH("can't find external sdcard !");
		return 0; // fail
	}

	return 1; // success
}

/*
 * after start, different bootmode may do different works
 *
 */
void follow_up_work()
{
	int isboot;
	char chiptest_state[PROPERTY_VALUE_MAX];
	char buf[PROPERTY_VALUE_MAX];

	if ((g_bootmode != META_BOOT) && (g_bootmode != FACTORY_BOOT) && (g_bootmode != ATE_FACTORY_BOOT)) {
		isboot = atoi(read_bootmode_config(KEY_BOOT));
		if (!isboot) {
			MLOGE_DATA("no start logging");
			set_status(STOPPED);
			property_set(PROP_RUNNING, "0");
			return;
		}
	}
	// bootup log saving
	set_status(SAVE_TO_BOOTUP);

	switch (g_bootmode) {
		case NORMAL_BOOT:
			set_cur_logpath(PATH_NORMAL);
			prepare_logging();
			/* if normal boot for chiptest */
			property_get("persist.chiptest.enable", chiptest_state, "0");
			if (!strcmp(chiptest_state, "1"))
				normal_chiptest = 1;
			else
				return;
			break;

		case META_BOOT:
			set_cur_logpath(PATH_META);
			prepare_logging();
			/* meta boot : ro.boot.mblogenable should be detected */
			property_get("ro.boot.mblogenable", buf, "0");
			if (buf[0] == '1')
				meta_mblogenable = 1;
			break;

		case FACTORY_BOOT:
		case ATE_FACTORY_BOOT:
			set_cur_logpath(PATH_FACTORY);
			prepare_logging();
			break;
		default:
			set_cur_logpath(PATH_OTHER_BOOT);
			prepare_logging();
			return;
	}

	// for normal chiptest=1, Meta, factory, ate_factory
	ext_sdcard_ready = check_external_sdcard();

	if (ext_sdcard_ready || (meta_mblogenable && g_bootmode == META_BOOT)) {
		if (!ext_sdcard_ready) // If meta enable logging, but ext-sdcard is not ready. Then logging to inter-sd
			update_sd_context(PATH_DEFAULT); // default path is /sdcard/ (internal sd path)
		if (!make_full_path()) {
			MLOGE_BOTH("make full path failed!");
			set_status(STOPPED);
			return;
		}
		set_status(SAVE_TO_SDCARD);
		set_redirect_flag();
		copy_and_dump();

	} else
		set_status(STOPPED);

	return;
}

/*
 * The follow two functions will be call when start/stop
 */
int init_debug_config()
{
	if (g_bootmode == NORMAL_BOOT) {
		MLOGI_BOTH("init debug");
		InitDebugConfig();
	}

	return 1;
}

int deinit_debug_config()
{
	if (g_bootmode == NORMAL_BOOT) {
		MLOGI_BOTH("deinit debug");
		DeInitDebugConfig();
	}

	return 1;
}

/*
 * Select() should timeout in NORMAL_BOOT and META_BOOT FACTORY_BOOT
 * if sdcard has not mount for a long time
 */
int timeout_in_this_boot()
{
	int isboot = atoi(read_bootmode_config(KEY_BOOT));

	if (!isboot)
		return 0;

	switch (g_bootmode) {
		case NORMAL_BOOT:
			return 1;
			break;

		default:
			return 0;
			break;
	}
}

/* Get sd type on current time  */
sd_type_t get_sd_type()
{
	char *log_path;

	log_path = read_bootmode_config(KEY_PATH);
	if (!strcmp(log_path, PATH_INTERNAL))
		return INTERNAL_SD;
	else if (!strcmp(log_path, PATH_EXTERNAL))
		return EXTERNAL_SD;
	else {
		MLOGE_BOTH("log path wrong when read folder file");
		return INTERNAL_SD; // When Wrong path, focus on internal storage.
	}

}

