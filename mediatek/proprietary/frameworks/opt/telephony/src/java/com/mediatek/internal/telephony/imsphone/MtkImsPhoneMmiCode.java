/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony.imsphone;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;
import android.os.SystemProperties;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.telephony.Rlog;

import com.android.ims.ImsException;
import com.android.ims.ImsReasonInfo;
import com.android.ims.ImsSsInfo;
import com.android.ims.ImsUtInterface;
import com.android.internal.telephony.CallForwardInfo;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.GsmCdmaPhone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.uicc.IccRecords;

import static com.android.internal.telephony.CommandsInterface.SERVICE_CLASS_NONE;
import static com.android.internal.telephony.CommandsInterface.SERVICE_CLASS_VOICE;
import static com.android.internal.telephony.CommandsInterface.SERVICE_CLASS_DATA;
import static com.android.internal.telephony.CommandsInterface.SERVICE_CLASS_FAX;
import static com.android.internal.telephony.CommandsInterface.SERVICE_CLASS_SMS;
import static com.android.internal.telephony.CommandsInterface.SERVICE_CLASS_DATA_SYNC;
import static com.android.internal.telephony.CommandsInterface.SERVICE_CLASS_DATA_ASYNC;
import static com.android.internal.telephony.CommandsInterface.SERVICE_CLASS_PACKET;
import static com.android.internal.telephony.CommandsInterface.SERVICE_CLASS_PAD;
import static com.android.internal.telephony.CommandsInterface.SERVICE_CLASS_MAX;

import static com.mediatek.internal.telephony.MtkGsmCdmaPhone.IMS_DEREG_PROP;
import static com.mediatek.internal.telephony.MtkGsmCdmaPhone.IMS_DEREG_ON;
import static com.mediatek.internal.telephony.MtkGsmCdmaPhone.IMS_DEREG_OFF;
import static com.mediatek.internal.telephony.MtkRIL.SERVICE_CLASS_VIDEO;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.PROPERTY_TERMINAL_BASED_CALL_WAITING_MODE;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.TERMINAL_BASED_CALL_WAITING_DISABLED;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.TERMINAL_BASED_CALL_WAITING_ENABLED_OFF;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.TERMINAL_BASED_CALL_WAITING_ENABLED_ON;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.PROPERTY_UT_CFU_NOTIFICATION_MODE;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.UT_CFU_NOTIFICATION_MODE_DISABLED;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.UT_CFU_NOTIFICATION_MODE_ON;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.UT_CFU_NOTIFICATION_MODE_OFF;


import com.mediatek.ims.MtkImsReasonInfo;

import com.android.internal.telephony.MmiCode;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.imsphone.ImsPhone;
import com.android.internal.telephony.imsphone.ImsPhoneMmiCode;

import com.mediatek.internal.telephony.MtkGsmCdmaPhone;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.MtkRIL;

import com.mediatek.internal.telephony.imsphone.MtkImsPhone;

import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.Matcher;


/**
 * {@hide}
 */
public final class MtkImsPhoneMmiCode extends ImsPhoneMmiCode {
    static final String LOG_TAG = "MtkImsPhoneMmiCode";

    private static final int EVENT_SET_CLIR_COMPLETE       = 101;

    //***** Public Class methods

    /**
     * Some dial strings in GSM are defined to do non-call setup
     * things, such as modify or query supplementary service settings (eg, call
     * forwarding). These are generally referred to as "MMI codes".
     * We look to see if the dial string contains a valid MMI code (potentially
     * with a dial string at the end as well) and return info here.
     *
     * If the dial string contains no MMI code, we return an instance with
     * only "dialingNumber" set
     *
     * Please see flow chart in TS 22.030 6.5.3.2
     */

    public static MtkImsPhoneMmiCode newFromDialString(String dialString, ImsPhone phone) {
       return newFromDialString(dialString, phone, null);
    }

    public static MtkImsPhoneMmiCode newFromDialString(String dialString,
                                             ImsPhone phone, ResultReceiver wrappedCallback) {
        Matcher m;
        MtkImsPhoneMmiCode ret = null;

        m = sPatternSuppService.matcher(dialString);

        // Is this formatted like a standard supplementary service code?
        if (m.matches()) {
            ret = new MtkImsPhoneMmiCode(phone);
            ret.mPoundString = makeEmptyNull(m.group(MATCH_GROUP_POUND_STRING));
            ret.mAction = makeEmptyNull(m.group(MATCH_GROUP_ACTION));
            ret.mSc = makeEmptyNull(m.group(MATCH_GROUP_SERVICE_CODE));
            ret.mSia = makeEmptyNull(m.group(MATCH_GROUP_SIA));
            ret.mSib = makeEmptyNull(m.group(MATCH_GROUP_SIB));
            ret.mSic = makeEmptyNull(m.group(MATCH_GROUP_SIC));
            ret.mPwd = makeEmptyNull(m.group(MATCH_GROUP_PWD_CONFIRM));
            ret.mDialingNumber = makeEmptyNull(m.group(MATCH_GROUP_DIALING_NUMBER));
            ret.mCallbackReceiver = wrappedCallback;
            // According to TS 22.030 6.5.2 "Structure of the MMI",
            // the dialing number should not ending with #.
            // The dialing number ending # is treated as unique USSD,
            // eg, *400#16 digit number# to recharge the prepaid card
            // in India operator(Mumbai MTNL)
            if (ret.mDialingNumber != null &&
                    ret.mDialingNumber.endsWith("#") &&
                    dialString.endsWith("#")){
                ret = new MtkImsPhoneMmiCode(phone);
                ret.mPoundString = dialString;
            }
        } else if (dialString.endsWith("#")) {
            // TS 22.030 sec 6.5.3.2
            // "Entry of any characters defined in the 3GPP TS 23.038 [8] Default Alphabet
            // (up to the maximum defined in 3GPP TS 24.080 [10]), followed by #SEND".

            ret = new MtkImsPhoneMmiCode(phone);
            ret.mPoundString = dialString;
        } else if (isTwoDigitShortCode(phone.getContext(), dialString)) {
            //Is a country-specific exception to short codes as defined in TS 22.030, 6.5.3.2
            ret = null;
        } else if (isShortCode(dialString, phone)) {
            // this may be a short code, as defined in TS 22.030, 6.5.3.2
            ret = new MtkImsPhoneMmiCode(phone);
            ret.mDialingNumber = dialString;
        }

        return ret;
    }

    public static MtkImsPhoneMmiCode
    newNetworkInitiatedUssd(String ussdMessage, boolean isUssdRequest, MtkImsPhone phone) {
        MtkImsPhoneMmiCode ret;

        ret = new MtkImsPhoneMmiCode(phone);

        ret.mMessage = ussdMessage;
        ret.mIsUssdRequest = isUssdRequest;

        // If it's a request, set to PENDING so that it's cancelable.
        if (isUssdRequest) {
            ret.mIsPendingUSSD = true;
            ret.mState = State.PENDING;
        } else {
            ret.mState = State.COMPLETE;
        }

        return ret;
    }

    public static MtkImsPhoneMmiCode newFromUssdUserInput(String ussdMessge, MtkImsPhone phone) {
        MtkImsPhoneMmiCode ret = new MtkImsPhoneMmiCode(phone);

        ret.mMessage = ussdMessge;
        ret.mState = State.PENDING;
        ret.mIsPendingUSSD = true;

        return ret;
    }

    private static int
    siToServiceClass(String si) {
        if (si == null || si.length() == 0) {
                return  SERVICE_CLASS_NONE;
        } else {
            // NumberFormatException should cause MMI fail
            int serviceCode = Integer.parseInt(si, 10);

            switch (serviceCode) {
                case 10: return SERVICE_CLASS_SMS + SERVICE_CLASS_FAX  + SERVICE_CLASS_VOICE;
                case 11: return SERVICE_CLASS_VOICE;
                case 12: return SERVICE_CLASS_SMS + SERVICE_CLASS_FAX;
                case 13: return SERVICE_CLASS_FAX;

                case 16: return SERVICE_CLASS_SMS;

                case 19: return SERVICE_CLASS_FAX + SERVICE_CLASS_VOICE;

                case 20: return SERVICE_CLASS_DATA_ASYNC + SERVICE_CLASS_DATA_SYNC;

                case 21: return SERVICE_CLASS_PAD + SERVICE_CLASS_DATA_ASYNC;
                case 22: return SERVICE_CLASS_PACKET + SERVICE_CLASS_DATA_SYNC;
                //case 24: return SERVICE_CLASS_DATA_SYNC;
                case 24: return SERVICE_CLASS_DATA_SYNC + MtkRIL.SERVICE_CLASS_VIDEO;
                case 25: return SERVICE_CLASS_DATA_ASYNC;
                case 26: return SERVICE_CLASS_DATA_SYNC + SERVICE_CLASS_VOICE;
                case 99: return SERVICE_CLASS_PACKET;

                default:
                    throw new RuntimeException("unsupported MMI service code " + si);
            }
        }
    }

    //***** Constructor

    public MtkImsPhoneMmiCode(ImsPhone phone) {
        super(phone);
    }

    // inherited javadoc suffices
    @Override
    public void
    cancel() {
        // Complete or failed cannot be cancelled
        if (mState == State.COMPLETE || mState == State.FAILED) {
            return;
        }

        mState = State.CANCELLED;

        if (mIsPendingUSSD) {
            /// M: Assign return message for terminating USSI @{
            ((MtkImsPhone)mPhone).cancelUSSD(obtainMessage(EVENT_USSD_CANCEL_COMPLETE, this));
            /// @}
        } else {
            mPhone.onMMIDone (this);
        }

    }

    @Override
    public boolean
    isSupportedOverImsPhone() {
        if (isShortCode()) return true;
        else if (mDialingNumber != null) return false;
        else if (mSc != null && mSc.equals(SC_CNAP)) return false;
        else if (isServiceCodeCallForwarding(mSc)
                || isServiceCodeCallBarring(mSc)
                || (mSc != null && mSc.equals(SC_WAIT))
                || (mSc != null && mSc.equals(SC_CLIR))
                || (mSc != null && mSc.equals(SC_CLIP))
                || (mSc != null && mSc.equals(SC_COLR))
                || (mSc != null && mSc.equals(SC_COLP))
                || (mSc != null && mSc.equals(SC_BS_MT))
                || (mSc != null && mSc.equals(SC_BAICa))) {
            if (supportMdAutoSetupIms()) {
                try {
                    int serviceClass = siToServiceClass(mSib);
                    if (((serviceClass & CommandsInterface.SERVICE_CLASS_VOICE) != 0) ||
                        ((serviceClass & MtkRIL.SERVICE_CLASS_VIDEO) != 0) ||
                        (serviceClass == CommandsInterface.SERVICE_CLASS_NONE)) {
                        Rlog.d(LOG_TAG, "isSupportedOverImsPhone(), return true!");
                        return true;
                    }
                    /// @}
                    return false;
                } catch (RuntimeException exc) {
                    Rlog.d(LOG_TAG, "Invalid service class " + exc);
                }
            } else {
                if (mPhone.isVolteEnabled()
                    || (mPhone.isWifiCallingEnabled()
                    && ((MtkGsmCdmaPhone) mPhone.mDefaultPhone).isWFCUtSupport())) {
                    try {
                        int serviceClass = siToServiceClass(mSib);
                        /// M:  ALPS03012236,handle with voice, video and NONE in Ut. @{
                        if (((serviceClass & CommandsInterface.SERVICE_CLASS_VOICE) != 0) ||
                            ((serviceClass & MtkRIL.SERVICE_CLASS_VIDEO) != 0) ||
                             (serviceClass == CommandsInterface.SERVICE_CLASS_NONE)) {
                            Rlog.d(LOG_TAG, "isSupportedOverImsPhone(), return true!");
                            return true;
                        }
                        /// @}
                    } catch (RuntimeException exc) {
                        Rlog.d(LOG_TAG, "exc.toString() = " + exc.toString());
                    }
                }
            }
            return false;
        } else if (isPinPukCommand()
                || (mSc != null
                    && (mSc.equals(SC_PWD) || mSc.equals(SC_CLIP) || mSc.equals(SC_CLIR)))) {
            return false;
        } else if (mPoundString != null) return true;

        return false;
    }

    /** Process a MMI code or short code...anything that isn't a dialing number */
    @Override
    public void
    processCode () throws CallStateException {
        try {
            if ((supportMdAutoSetupIms() == false) &&
                    ((MtkGsmCdmaPhone)mPhone.mDefaultPhone).getCsFallbackStatus()
                    != MtkPhoneConstants.UT_CSFB_PS_PREFERRED) {
                Rlog.d(LOG_TAG, "processCode(): getCsFallbackStatus(): CS Fallback!");
                ((MtkImsPhone)mPhone).removeMmi(this);
                throw new CallStateException(Phone.CS_FALLBACK);
            }

            if (isShortCode()) {
                Rlog.d(LOG_TAG, "isShortCode");

                /// M: Send USSD over IMS if USSI is supported. @{
                if (SystemProperties.get("persist.mtk_ussi_support").equals("1")) {
                    Rlog.d(LOG_TAG, "Sending short code '"
                           + mDialingNumber + "' over IMS.");
                    sendUssd(mDialingNumber);
                } else {
                    // These just get treated as USSD.
                    Rlog.d(LOG_TAG, "Sending short code '"
                           + mDialingNumber + "' over CS pipe.");
                    ((MtkImsPhone)mPhone).removeMmi(this);
                    throw new CallStateException(Phone.CS_FALLBACK);
                }
                /// @}
            } else if (isServiceCodeCallForwarding(mSc)) {
                Rlog.d(LOG_TAG, "is CF");

                String dialingNumber = mSia;
                int reason = scToCallForwardReason(mSc);
                int serviceClass = siToServiceClass(mSib);
                int time = siToTime(mSic);

                if (isInterrogate()) {
                    if (serviceClass != SERVICE_CLASS_NONE) {
                        if ((MtkGsmCdmaPhone)mPhone.mDefaultPhone instanceof MtkGsmCdmaPhone) {
                            ((MtkGsmCdmaPhone) mPhone.mDefaultPhone).setServiceClass(serviceClass);
                        }
                    }
                    mPhone.getCallForwardingOption(reason,
                            obtainMessage(EVENT_QUERY_CF_COMPLETE, this));
                } else {
                    int cfAction;

                    if (isActivate()) {
                        // 3GPP TS 22.030 6.5.2
                        // a call forwarding request with a single * would be
                        // interpreted as registration if containing a forwarded-to
                        // number, or an activation if not
                        if (isEmptyOrNull(dialingNumber)) {
                            cfAction = CommandsInterface.CF_ACTION_ENABLE;
                            mIsCallFwdReg = false;
                        } else {
                            cfAction = CommandsInterface.CF_ACTION_REGISTRATION;
                            mIsCallFwdReg = true;
                        }
                    } else if (isDeactivate()) {
                        cfAction = CommandsInterface.CF_ACTION_DISABLE;
                    } else if (isRegister()) {
                        cfAction = CommandsInterface.CF_ACTION_REGISTRATION;
                    } else if (isErasure()) {
                        cfAction = CommandsInterface.CF_ACTION_ERASURE;
                    } else {
                        throw new RuntimeException ("invalid action");
                    }

                    int isSettingUnconditional =
                            ((reason == CommandsInterface.CF_REASON_UNCONDITIONAL) ||
                             (reason == CommandsInterface.CF_REASON_ALL)) ? 1 : 0;

                    int isEnableDesired =
                        ((cfAction == CommandsInterface.CF_ACTION_ENABLE) ||
                                (cfAction == CommandsInterface.CF_ACTION_REGISTRATION)) ? 1 : 0;

                    Rlog.d(LOG_TAG, "is CF setCallForward");

                    if (((MtkGsmCdmaPhone) mPhone.mDefaultPhone).isOpReregisterForCF()) {
                        Rlog.i(LOG_TAG, "Set ims dereg to ON.");
                        SystemProperties.set(IMS_DEREG_PROP, IMS_DEREG_ON);
                    }

                    mPhone.setCallForwardingOption(cfAction, reason,
                            dialingNumber, serviceClass, time, obtainMessage(
                                    EVENT_SET_CFF_COMPLETE,
                                    isSettingUnconditional,
                                    isEnableDesired, this));
                }
            } else if (isServiceCodeCallBarring(mSc)) {
                // sia = password
                // sib = basic service group
                // service group is not supported

                String password = mSia;
                int serviceClass = siToServiceClass(mSib);
                String facility = scToBarringFacility(mSc);

                if (serviceClass != SERVICE_CLASS_NONE) {
                    if (mPhone.mDefaultPhone instanceof MtkGsmCdmaPhone) {
                        ((MtkGsmCdmaPhone) mPhone.mDefaultPhone).setServiceClass(serviceClass);
                    }
                }

                if (isInterrogate()) {
                    mPhone.getCallBarring(facility,
                            obtainMessage(EVENT_SUPP_SVC_QUERY_COMPLETE, this));
                } else if (isActivate() || isDeactivate()) {
                    mPhone.setCallBarring(facility, isActivate(), password,
                            obtainMessage(EVENT_SET_COMPLETE, this));
                } else {
                    throw new RuntimeException ("Invalid or Unsupported MMI Code");
                }
            } else if (mSc != null && mSc.equals(SC_CLIR)) {
                // NOTE: Since these supplementary services are accessed only
                //       via MMI codes, methods have not been added to ImsPhone.
                //       Only the UT interface handle is used.
                if (isActivate()) {
                    if ((supportMdAutoSetupIms() == false) &&
                            ((MtkGsmCdmaPhone) mPhone.mDefaultPhone).isOpTbClir()) {
                        ((MtkGsmCdmaPhone) mPhone.mDefaultPhone).mCi.setCLIR(
                            CommandsInterface.CLIR_INVOCATION,
                            obtainMessage(EVENT_SET_CLIR_COMPLETE,
                                CommandsInterface.CLIR_INVOCATION, 0, this));
                        return;
                    }

                    try {
                        mPhone.mCT.getUtInterface().updateCLIR(CommandsInterface.CLIR_INVOCATION,
                            obtainMessage(EVENT_SET_CLIR_COMPLETE,
                                CommandsInterface.CLIR_INVOCATION, 0, this));
                    } catch (ImsException e) {
                        Rlog.d(LOG_TAG, "Could not get UT handle for updateCLIR.");
                    }
                } else if (isDeactivate()) {
                    if ((supportMdAutoSetupIms() == false) &&
                            ((MtkGsmCdmaPhone) mPhone.mDefaultPhone).isOpTbClir()) {
                        ((MtkGsmCdmaPhone) mPhone.mDefaultPhone).mCi.setCLIR(
                            CommandsInterface.CLIR_SUPPRESSION,
                            obtainMessage(EVENT_SET_CLIR_COMPLETE,
                                CommandsInterface.CLIR_SUPPRESSION, 0, this));
                        return;
                    }

                    try {
                        mPhone.mCT.getUtInterface().updateCLIR(CommandsInterface.CLIR_SUPPRESSION,
                            obtainMessage(EVENT_SET_CLIR_COMPLETE,
                                CommandsInterface.CLIR_SUPPRESSION, 0, this));
                    } catch (ImsException e) {
                        Rlog.d(LOG_TAG, "Could not get UT handle for updateCLIR.");
                    }
                } else if (isInterrogate()) {
                    if ((supportMdAutoSetupIms() == false) &&
                            ((MtkGsmCdmaPhone) mPhone.mDefaultPhone).isOpTbClir()) {
                        Message msg = obtainMessage(EVENT_GET_CLIR_COMPLETE, this);
                        if (msg != null) {
                            int[] result =
                                ((MtkGsmCdmaPhone) mPhone.mDefaultPhone).getSavedClirSetting();
                            Bundle info = new Bundle();
                            info.putIntArray(UT_BUNDLE_KEY_CLIR, result);

                            AsyncResult.forMessage(msg, info, null);
                            msg.sendToTarget();
                        }
                        return;
                    }

                    try {
                        mPhone.mCT.getUtInterface()
                            .queryCLIR(obtainMessage(EVENT_GET_CLIR_COMPLETE, this));
                    } catch (ImsException e) {
                        Rlog.d(LOG_TAG, "Could not get UT handle for queryCLIR.");
                    }
                } else {
                    throw new RuntimeException ("Invalid or Unsupported MMI Code");
                }
            } else if (mSc != null && mSc.equals(SC_CLIP)) {
                // NOTE: Refer to the note above.
                if (isInterrogate()) {
                    try {
                        mPhone.mCT.getUtInterface()
                            .queryCLIP(obtainMessage(EVENT_SUPP_SVC_QUERY_COMPLETE, this));
                    } catch (ImsException e) {
                        Rlog.d(LOG_TAG, "Could not get UT handle for queryCLIP.");
                    }
                } else if (isActivate() || isDeactivate()) {
                    try {
                        mPhone.mCT.getUtInterface().updateCLIP(isActivate(),
                                obtainMessage(EVENT_SET_COMPLETE, this));
                    } catch (ImsException e) {
                        Rlog.d(LOG_TAG, "Could not get UT handle for updateCLIP.");
                    }
                } else {
                    throw new RuntimeException ("Invalid or Unsupported MMI Code");
                }
            } else if (mSc != null && mSc.equals(SC_COLP)) {
                // NOTE: Refer to the note above.
                if (isInterrogate()) {
                    try {
                        mPhone.mCT.getUtInterface()
                            .queryCOLP(obtainMessage(EVENT_SUPP_SVC_QUERY_COMPLETE, this));
                    } catch (ImsException e) {
                        Rlog.d(LOG_TAG, "Could not get UT handle for queryCOLP.");
                    }
                } else if (isActivate() || isDeactivate()) {
                    try {
                        mPhone.mCT.getUtInterface().updateCOLP(isActivate(),
                                 obtainMessage(EVENT_SET_COMPLETE, this));
                     } catch (ImsException e) {
                         Rlog.d(LOG_TAG, "Could not get UT handle for updateCOLP.");
                     }
                } else {
                    throw new RuntimeException ("Invalid or Unsupported MMI Code");
                }
            } else if (mSc != null && mSc.equals(SC_COLR)) {
                // NOTE: Refer to the note above.
                if (isActivate()) {
                    try {
                        mPhone.mCT.getUtInterface().updateCOLR(NUM_PRESENTATION_RESTRICTED,
                                obtainMessage(EVENT_SET_COMPLETE, this));
                    } catch (ImsException e) {
                        Rlog.d(LOG_TAG, "Could not get UT handle for updateCOLR.");
                    }
                } else if (isDeactivate()) {
                    try {
                        mPhone.mCT.getUtInterface().updateCOLR(NUM_PRESENTATION_ALLOWED,
                                obtainMessage(EVENT_SET_COMPLETE, this));
                    } catch (ImsException e) {
                        Rlog.d(LOG_TAG, "Could not get UT handle for updateCOLR.");
                    }
                } else if (isInterrogate()) {
                    try {
                        mPhone.mCT.getUtInterface()
                            .queryCOLR(obtainMessage(EVENT_SUPP_SVC_QUERY_COMPLETE, this));
                    } catch (ImsException e) {
                        Rlog.d(LOG_TAG, "Could not get UT handle for queryCOLR.");
                    }
                } else {
                    throw new RuntimeException ("Invalid or Unsupported MMI Code");
                }
            } else if (mSc != null && (mSc.equals(SC_BS_MT))) {
                try {
                    if (isInterrogate()) {
                        mPhone.mCT.getUtInterface()
                        .queryCallBarring(ImsUtInterface.CB_BS_MT,
                                          obtainMessage(EVENT_QUERY_ICB_COMPLETE,this));
                    } else {
                        processIcbMmiCodeForUpdate();
                    }
                 // TODO: isRegister() case needs to be handled.
                } catch (ImsException e) {
                    Rlog.d(LOG_TAG, "Could not get UT handle for ICB.");
                }
            } else if (mSc != null && mSc.equals(SC_BAICa)) {
                int callAction =0;
                // TODO: Should we route through queryCallBarring() here?
                try {
                    if (isInterrogate()) {
                        mPhone.mCT.getUtInterface()
                        .queryCallBarring(ImsUtInterface.CB_BIC_ACR,
                                          obtainMessage(EVENT_QUERY_ICB_COMPLETE,this));
                    } else {
                        if (isActivate()) {
                            callAction = CommandsInterface.CF_ACTION_ENABLE;
                        } else if (isDeactivate()) {
                            callAction = CommandsInterface.CF_ACTION_DISABLE;
                        }
                        mPhone.mCT.getUtInterface()
                                .updateCallBarring(ImsUtInterface.CB_BIC_ACR,
                                callAction,
                                obtainMessage(EVENT_SET_COMPLETE,this),
                                null);
                    }
                } catch (ImsException e) {
                    Rlog.d(LOG_TAG, "Could not get UT handle for ICBa.");
                }
            } else if (mSc != null && mSc.equals(SC_WAIT)) {
                // sia = basic service group
                int serviceClass = siToServiceClass(mSib);

                if (supportMdAutoSetupIms()) {
                    // 93 AOSP logic
                    if (isActivate() || isDeactivate()) {
                        mPhone.setCallWaiting(isActivate(), serviceClass,
                                obtainMessage(EVENT_SET_COMPLETE, this));
                    } else if (isInterrogate()) {
                        mPhone.getCallWaiting(obtainMessage(EVENT_QUERY_COMPLETE, this));
                    } else {
                        throw new RuntimeException ("Invalid or Unsupported MMI Code");
                    }

                } else {
                    // non 93 logic
                    if (isActivate() || isDeactivate()) {
                        /* Terminal-based Call Waiting */
                        if (((MtkGsmCdmaPhone) mPhone.mDefaultPhone).isOpNwCW()) {
                            Rlog.d(LOG_TAG, "setCallWaiting() by Ut interface.");
                            mPhone.setCallWaiting(isActivate(), serviceClass,
                                obtainMessage(EVENT_SET_COMPLETE, this));
                        } else {
                            String tbcwMode = TelephonyManager.getTelephonyProperty(
                                    mPhone.mDefaultPhone.getPhoneId(),
                                    PROPERTY_TERMINAL_BASED_CALL_WAITING_MODE,
                                    TERMINAL_BASED_CALL_WAITING_DISABLED);
                            Rlog.d(LOG_TAG, "setCallWaiting(): tbcwMode = " + tbcwMode
                                    + ", enable = " + isActivate());
                            if (TERMINAL_BASED_CALL_WAITING_ENABLED_ON.equals(tbcwMode)) {
                                if (!isActivate()) {
                                    TelephonyManager.setTelephonyProperty(
                                            mPhone.mDefaultPhone.getPhoneId(),
                                            PROPERTY_TERMINAL_BASED_CALL_WAITING_MODE,
                                            TERMINAL_BASED_CALL_WAITING_ENABLED_OFF);
                                }
                                Message msg = obtainMessage(EVENT_SET_COMPLETE, null);
                                AsyncResult.forMessage(msg, null, null);
                                sendMessage(msg);
                            } else if (TERMINAL_BASED_CALL_WAITING_ENABLED_OFF.equals(tbcwMode)) {
                                if (isActivate()) {
                                    TelephonyManager.setTelephonyProperty(
                                            mPhone.mDefaultPhone.getPhoneId(),
                                            PROPERTY_TERMINAL_BASED_CALL_WAITING_MODE,
                                            TERMINAL_BASED_CALL_WAITING_ENABLED_ON);
                                }
                                Message msg = obtainMessage(EVENT_SET_COMPLETE, null);
                                AsyncResult.forMessage(msg, null, null);
                                sendMessage(msg);
                            } else {
                                Rlog.d(LOG_TAG, "setCallWaiting() by Ut interface.");
                                mPhone.setCallWaiting(isActivate(), serviceClass,
                                    obtainMessage(EVENT_SET_COMPLETE, this));
                            }
                        }
                    } else if (isInterrogate()) {
                        if (((MtkGsmCdmaPhone) mPhone.mDefaultPhone).isOpNwCW()) {
                            Rlog.d(LOG_TAG, "getCallWaiting() by Ut interface.");
                            mPhone.getCallWaiting(obtainMessage(EVENT_QUERY_COMPLETE, this));
                        } else {
                            /* Terminal-based Call Waiting */
                            String tbcwMode = TelephonyManager.getTelephonyProperty(
                                                    mPhone.mDefaultPhone.getPhoneId(),
                                                    PROPERTY_TERMINAL_BASED_CALL_WAITING_MODE,
                                                    TERMINAL_BASED_CALL_WAITING_DISABLED);
                            Rlog.d(LOG_TAG, "SC_WAIT isInterrogate() tbcwMode = " + tbcwMode);
                            if (TERMINAL_BASED_CALL_WAITING_ENABLED_ON.equals(tbcwMode)) {
                                int[] cwInfos = new int[2];
                                cwInfos[0] = 1;
                                cwInfos[1] = SERVICE_CLASS_VOICE;

                                Message msg = obtainMessage(EVENT_QUERY_COMPLETE, null);
                                AsyncResult.forMessage(msg, cwInfos, null);
                                sendMessage(msg);
                            } else if (TERMINAL_BASED_CALL_WAITING_ENABLED_OFF.equals(tbcwMode)) {
                                int[] cwInfos = new int[2];
                                cwInfos[0] = 0;

                                Message msg = obtainMessage(EVENT_QUERY_COMPLETE, null);
                                AsyncResult.forMessage(msg, cwInfos, null);
                                sendMessage(msg);
                            } else {
                                Rlog.d(LOG_TAG, "getCallWaiting() by Ut interface.");
                                mPhone.getCallWaiting(obtainMessage(EVENT_QUERY_COMPLETE, this));
                            }
                       }
                    } else {
                        throw new RuntimeException ("Invalid or Unsupported MMI Code");
                    }
                }
            } else if (mPoundString != null) {
                /// M: Send USSD over IMS if USSI is supported. @{
                if (SystemProperties.get("persist.mtk_ussi_support").equals("1")) {
                    Rlog.d(LOG_TAG, "Sending pound string '"
                           + mPoundString + "' over IMS.");
                    sendUssd(mPoundString);
                } else {
                    Rlog.d(LOG_TAG, "Sending pound string '"
                           + mDialingNumber + "' over CS pipe.");
                    ((MtkImsPhone)mPhone).removeMmi(this);
                    throw new CallStateException(Phone.CS_FALLBACK);
                }
                /// @}
            } else {
                throw new RuntimeException ("Invalid or Unsupported MMI Code");
            }
        } catch (RuntimeException exc) {
            mState = State.FAILED;
            mMessage = mContext.getText(com.android.internal.R.string.mmiError);
            mPhone.onMMIDone(this);
        }
    }

    @Override
    protected void
    onSetComplete(Message msg, AsyncResult ar){
        StringBuilder sb = new StringBuilder(getScString());
        sb.append("\n");

        if (ar.exception != null) {
            mState = State.FAILED;

            if (ar.exception instanceof CommandException) {
                CommandException err = (CommandException) ar.exception;
                if (err.getCommandError() == CommandException.Error.PASSWORD_INCORRECT) {
                    sb.append(mContext.getText(
                            com.android.internal.R.string.passwordIncorrect));
                } else if (err.getMessage() != null) {
                    sb.append(err.getMessage());
                } else {
                    sb.append(mContext.getText(com.android.internal.R.string.mmiError));
                }
            } else {
                ImsException error = (ImsException) ar.exception;
                if (error.getMessage() != null) {
                    sb.append(error.getMessage());
                } else {
                    sb.append(getErrorMessage(ar));
                }
            }
        } else if (isActivate()) {
            mState = State.COMPLETE;
            if (mIsCallFwdReg) {
                sb.append(mContext.getText(
                        com.android.internal.R.string.serviceRegistered));
            } else {
                sb.append(mContext.getText(
                        com.android.internal.R.string.serviceEnabled));
            }
        } else if (isDeactivate()) {
            mState = State.COMPLETE;
            sb.append(mContext.getText(
                    com.android.internal.R.string.serviceDisabled));
        } else if (isRegister()) {
            mState = State.COMPLETE;
            sb.append(mContext.getText(
                    com.android.internal.R.string.serviceRegistered));
        } else if (isErasure()) {
            mState = State.COMPLETE;
            sb.append(mContext.getText(
                    com.android.internal.R.string.serviceErased));
        } else {
            mState = State.FAILED;
            sb.append(mContext.getText(
                    com.android.internal.R.string.mmiError));
        }

        mMessage = sb;
        Rlog.d(LOG_TAG, "onSetComplete: mmi=" + this + " message=" + sb);
        mPhone.onMMIDone(this);
    }

    /** Called from ImsPhone.handleMessage; not a Handler subclass */
    @Override
    public void
    handleMessage (Message msg) {
        AsyncResult ar;

        if (supportMdAutoSetupIms() == false) {
            // for non 93 logic
            ar = (AsyncResult) msg.obj;
            if (ar != null && ar.exception != null) {
                if (ar.exception instanceof CommandException) {
                    CommandException cmdException = (CommandException) ar.exception;
                    if (cmdException.getCommandError()
                            == CommandException.Error.OPERATION_NOT_ALLOWED) {
                        Rlog.d(LOG_TAG, "handleMessage():"
                                + " CommandException.Error.UT_XCAP_403_FORBIDDEN");
                        ((MtkImsPhone)mPhone).handleMmiCodeCsfb(
                                MtkImsReasonInfo.CODE_UT_XCAP_403_FORBIDDEN, this);
                        return;
                    } else if (cmdException.getCommandError()
                            == CommandException.Error.NO_NETWORK_FOUND) {
                        Rlog.d(LOG_TAG, "handleMessage(): CommandException.Error.UT_UNKNOWN_HOST");
                        ((MtkImsPhone)mPhone).handleMmiCodeCsfb(
                                MtkImsReasonInfo.CODE_UT_UNKNOWN_HOST, this);
                        return;
                    }
                } else if (ar.exception instanceof ImsException) {
                    ImsException imsException = (ImsException) ar.exception;
                    if (imsException.getCode() == MtkImsReasonInfo.CODE_UT_XCAP_403_FORBIDDEN) {
                        Rlog.d(LOG_TAG, "handleMessage():"
                                + " ImsReasonInfo.CODE_UT_XCAP_403_FORBIDDEN");
                        ((MtkImsPhone)mPhone).handleMmiCodeCsfb(
                                MtkImsReasonInfo.CODE_UT_XCAP_403_FORBIDDEN, this);
                        return;
                    } else if (imsException.getCode() == MtkImsReasonInfo.CODE_UT_UNKNOWN_HOST) {
                        Rlog.d(LOG_TAG, "handleMessage(): ImsReasonInfo.CODE_UT_UNKNOWN_HOST");
                        ((MtkImsPhone)mPhone).handleMmiCodeCsfb(
                                MtkImsReasonInfo.CODE_UT_UNKNOWN_HOST, this);
                        return;
                    }
                }
            }
        }

        switch (msg.what) {
            case EVENT_SET_COMPLETE:
                ar = (AsyncResult) (msg.obj);

                onSetComplete(msg, ar);
                break;

            /* M: SS part */
            case EVENT_SET_CLIR_COMPLETE:
                ar = (AsyncResult) (msg.obj);

                if (ar.exception == null) {
                    ((MtkGsmCdmaPhone) mPhone.mDefaultPhone).saveClirSetting(msg.arg1);
                }

                onSetComplete(msg, ar);
                break;
            /* M: SS part end */

            case EVENT_SET_CFF_COMPLETE:
                ar = (AsyncResult) (msg.obj);

                /*
                * msg.arg1 = 1 means to set unconditional voice call forwarding
                * msg.arg2 = 1 means to enable voice call forwarding
                */
                /* M: SS part */
                if ((ar.exception == null) && (msg.arg1 == 1)) {
                    boolean cffEnabled = (msg.arg2 == 1);  //The action value from user.
                    if (((MtkGsmCdmaPhone) mPhone.mDefaultPhone).queryCFUAgainAfterSet()) {
                        if (ar.result != null) {
                            CallForwardInfo[] cfInfos = (CallForwardInfo[]) ar.result;
                            if (cfInfos == null || cfInfos.length == 0) {
                                Rlog.i(LOG_TAG, "cfInfo is null or length is 0.");
                            } else {
                                for (int i = 0; i < cfInfos.length; i++) {
                                    if ((cfInfos[i].serviceClass & SERVICE_CLASS_VOICE) != 0) {
                                        // reset the value which from query.
                                        if (cfInfos[i].status == 1) {
                                            Rlog.i(LOG_TAG, "Set CF_ENABLE, serviceClass: "
                                                + cfInfos[i].serviceClass);
                                            cffEnabled = true;
                                        } else {
                                            Rlog.i(LOG_TAG, "Set CF_DISABLE, serviceClass: "
                                                + cfInfos[i].serviceClass);
                                            cffEnabled = false;
                                        }
                                        break;
                                    }
                                }
                            }
                        } else {
                            Rlog.i(LOG_TAG, "ar.result is null.");
                        }
                    }
                    Rlog.i(LOG_TAG, "EVENT_SET_CFF_COMPLETE: cffEnabled:" + cffEnabled
                            + ", mDialingNumber=" + mDialingNumber
                            + ", mIccRecords=" + mIccRecords);
                    if (mIccRecords != null) {
                        ((MtkGsmCdmaPhone) mPhone.mDefaultPhone).setVoiceCallForwardingFlag(1,
                                cffEnabled, mDialingNumber);
                        /// M: SS OP01 Ut
                        ((MtkImsPhone)mPhone).saveTimeSlot(null);
                    }
                }
                /* M: SS part end */

                onSetComplete(msg, ar);
                break;

            case EVENT_QUERY_CF_COMPLETE:
                ar = (AsyncResult) (msg.obj);
                onQueryCfComplete(ar);
                break;

            case EVENT_QUERY_COMPLETE:
                ar = (AsyncResult) (msg.obj);
                onQueryComplete(ar);
                break;

            case EVENT_USSD_COMPLETE:
                ar = (AsyncResult) (msg.obj);

                if (ar.exception != null) {
                    /// M: Send USSI fails, set as CSFB @{
                    ((MtkImsPhone)mPhone).mUssiCSFB = true;
                    /// @}
                    mState = State.FAILED;
                    mMessage = getErrorMessage(ar);

                    mPhone.onMMIDone(this);
                }

                // Note that unlike most everything else, the USSD complete
                // response does not complete this MMI code...we wait for
                // an unsolicited USSD "Notify" or "Request".
                // The matching up of this is done in ImsPhone.

                break;

            case EVENT_USSD_CANCEL_COMPLETE:
                mPhone.onMMIDone(this);
                break;

            case EVENT_SUPP_SVC_QUERY_COMPLETE:
                ar = (AsyncResult) (msg.obj);
                onSuppSvcQueryComplete(ar);
                break;

            case EVENT_QUERY_ICB_COMPLETE:
                ar = (AsyncResult) (msg.obj);
                onIcbQueryComplete(ar);
                break;

            case EVENT_GET_CLIR_COMPLETE:
                ar = (AsyncResult) (msg.obj);
                onQueryClirComplete(ar);
                break;

            default:
                break;
        }
    }

    /***
     * Utility function to check whether we should show the time template information or not.
     * @return boolean true if rule is matched.
     */
    @Override
    protected boolean needTimeTemplate(CallForwardInfo info) {
        return (info.reason == CommandsInterface.CF_REASON_NO_REPLY
                && info.timeSeconds >=0 );
    }

    /// M: for SS part. @{
    /**
     Utility function to check if it is USSD number.
     @return boolean true if this an USSD number.
    */
    public boolean isUssdNumber() {
        if (isTemporaryModeCLIR()) {
            return false;
        }
        if (isShortCode() || mDialingNumber != null) {
            return true;
        }
        if (mSc != null
                && (mSc.equals(SC_CNAP)
                || mSc.equals(SC_CLIP)
                || mSc.equals(SC_CLIR)
                || mSc.equals(SC_COLP)
                || mSc.equals(SC_COLR)
                || isServiceCodeCallForwarding(mSc)
                || isServiceCodeCallBarring(mSc)
                || mSc.equals(SC_PWD)
                || mSc.equals(SC_WAIT)
                || isPinPukCommand()
                )) {
            return false;
        }
        if (mPoundString != null) {
            return true;
        }
        return false;
    }
    public String getUssdDialString() {
        Rlog.d(LOG_TAG, "getUssdDialString(): mDialingNumber="
                + mDialingNumber + ", mPoundString=" + mPoundString);
        if (mDialingNumber != null)
            return mDialingNumber;
        else
            return mPoundString;
    }

    public static boolean isUtMmiCode(String dialString, ImsPhone dialPhone) {

        MtkImsPhoneMmiCode mmi = MtkImsPhoneMmiCode.newFromDialString(dialString, dialPhone);
        if (mmi == null || mmi.isTemporaryModeCLIR()) {
            return false;
        }

        if (mmi.isShortCode() || mmi.mDialingNumber != null) {
            return false;
        } else if (mmi.mSc != null
                && (mmi.mSc.equals(SC_CLIP)
                || mmi.mSc.equals(SC_CLIR)
                || mmi.mSc.equals(SC_COLP)
                || mmi.mSc.equals(SC_COLR)
                || isServiceCodeCallForwarding(mmi.mSc)
                || isServiceCodeCallBarring(mmi.mSc)
                || mmi.mSc.equals(SC_WAIT)
                || mmi.mSc.equals(SC_BS_MT)
                || mmi.mSc.equals(SC_BAICa)
                )) {
            return true;
        }
        return false;
    }
    /// @}

    private boolean supportMdAutoSetupIms() {
        boolean r = false;
        if (SystemProperties.get("ro.md_auto_setup_ims").equals("1")) {
            r = true;
        }
        return r;
    }

    @Override
    protected void onQueryCfComplete(AsyncResult ar) {
        StringBuilder sb = new StringBuilder(getScString());
        sb.append("\n");

        if (ar.exception != null) {
            mState = State.FAILED;

            if (ar.exception instanceof ImsException) {
                ImsException error = (ImsException) ar.exception;
                if (error.getMessage() != null) {
                    sb.append(error.getMessage());
                } else {
                    sb.append(getErrorMessage(ar));
                }
            }
            else {
                sb.append(getErrorMessage(ar));
            }
        } else {
            CallForwardInfo infos[];

            infos = (CallForwardInfo[]) ar.result;

            if (infos.length == 0) {
                // Assume the default is not active
                sb.append(mContext.getText(com.android.internal.R.string.serviceDisabled));

                // Set unconditional CFF in SIM to false
                if (mIccRecords != null) {
                    mPhone.setVoiceCallForwardingFlag(1, false, null);
                }
            } else {

                SpannableStringBuilder tb = new SpannableStringBuilder();

                // Each bit in the service class gets its own result line
                // The service classes may be split up over multiple
                // CallForwardInfos. So, for each service class, find out
                // which CallForwardInfo represents it and then build
                // the response text based on that

                for (int serviceClassMask = 1;
                        serviceClassMask <= MtkRIL.SERVICE_CLASS_MTK_MAX;
                        serviceClassMask <<= 1) {
                    if (serviceClassMask == MtkRIL.SERVICE_CLASS_LINE2) continue;

                    for (int i = 0, s = infos.length; i < s ; i++) {
                        if ((serviceClassMask & infos[i].serviceClass) != 0) {
                            tb.append(makeCFQueryResultMessage(infos[i],
                                            serviceClassMask));
                            tb.append("\n");
                        }
                    }
                }
                sb.append(tb);
            }

            mState = State.COMPLETE;
        }

        mMessage = sb;
        Rlog.d(LOG_TAG, "onQueryCfComplete: mmi=" + this);
        mPhone.onMMIDone(this);

    }

    @Override
    protected void onQueryClirComplete(AsyncResult ar) {
        if (supportMdAutoSetupIms()) {
            // Terminal based will return 832 error cause in 93
            if (ar.exception != null) {
                ImsException imsException = (ImsException)ar.exception;
                if (imsException.getCode() == MtkImsReasonInfo.CODE_UT_XCAP_832_TERMINAL_BASE_SOLUTION) {
                    StringBuilder sb = new StringBuilder(getScString());
                    sb.append("\n");
                    mState = State.FAILED;

                    int[] clirInfo = ((MtkGsmCdmaPhone) mPhone.mDefaultPhone).getSavedClirSetting();
                    Rlog.d(LOG_TAG, "Terminal based and getSavedClirSetting:" +
                            Arrays.toString(clirInfo));

                    // 'm' parameter.
                    switch (clirInfo[1]) {
                        case CLIR_NOT_PROVISIONED:
                            sb.append(mContext.getText(
                                    com.android.internal.R.string.serviceNotProvisioned));
                            mState = State.COMPLETE;
                            break;
                        case CLIR_PROVISIONED_PERMANENT:
                            sb.append(mContext.getText(
                                    com.android.internal.R.string.CLIRPermanent));
                            mState = State.COMPLETE;
                            break;
                        case CLIR_PRESENTATION_RESTRICTED_TEMPORARY:
                            // 'n' parameter.
                            switch (clirInfo[0]) {
                                case CLIR_DEFAULT:
                                    sb.append(mContext.getText(
                                            com.android.internal.R.string.CLIRDefaultOnNextCallOn));
                                    mState = State.COMPLETE;
                                    break;
                                case CLIR_INVOCATION:
                                    sb.append(mContext.getText(
                                            com.android.internal.R.string.CLIRDefaultOnNextCallOn));
                                    mState = State.COMPLETE;
                                    break;
                                case CLIR_SUPPRESSION:
                                    sb.append(mContext.getText(
                                            com.android.internal.R.string.CLIRDefaultOnNextCallOff));
                                    mState = State.COMPLETE;
                                    break;
                                default:
                                    sb.append(mContext.getText(
                                            com.android.internal.R.string.mmiError));
                                    mState = State.FAILED;
                            }
                            break;
                        case CLIR_PRESENTATION_ALLOWED_TEMPORARY:
                            // 'n' parameter.
                            switch (clirInfo[0]) {
                                case CLIR_DEFAULT:
                                    sb.append(mContext.getText(
                                            com.android.internal.R.string.CLIRDefaultOffNextCallOff));
                                    mState = State.COMPLETE;
                                    break;
                                case CLIR_INVOCATION:
                                    sb.append(mContext.getText(
                                            com.android.internal.R.string.CLIRDefaultOffNextCallOn));
                                    mState = State.COMPLETE;
                                    break;
                                case CLIR_SUPPRESSION:
                                    sb.append(mContext.getText(
                                            com.android.internal.R.string.CLIRDefaultOffNextCallOff));
                                    mState = State.COMPLETE;
                                    break;
                                default:
                                    sb.append(mContext.getText(
                                            com.android.internal.R.string.mmiError));
                                    mState = State.FAILED;
                            }
                            break;
                        default:
                            sb.append(mContext.getText(
                                    com.android.internal.R.string.mmiError));
                            mState = State.FAILED;
                    }

                    mMessage = sb;
                    Rlog.d(LOG_TAG, "onQueryClirComplete mmi=" + this);
                    mPhone.onMMIDone(this);
                    return;
                }
            }
        }
        super.onQueryClirComplete(ar);
    }

    /**
     * @param serviceClass 1 bit of the service class bit vectory
     * @return String to be used for call forward query MMI response text.
     *        Returns null if unrecognized
     */
    @Override
    protected CharSequence serviceClassToCFString (int serviceClass) {
        Rlog.d(LOG_TAG, "serviceClassToCFString, serviceClass = " + serviceClass);
        switch (serviceClass) {
            case SERVICE_CLASS_VOICE:
                return mContext.getText(com.android.internal.R.string.serviceClassVoice);
            case SERVICE_CLASS_DATA:
                return mContext.getText(com.android.internal.R.string.serviceClassData);
            case SERVICE_CLASS_FAX:
                return mContext.getText(com.android.internal.R.string.serviceClassFAX);
            case SERVICE_CLASS_SMS:
                return mContext.getText(com.android.internal.R.string.serviceClassSMS);
            case SERVICE_CLASS_DATA_SYNC:
                return mContext.getText(com.android.internal.R.string.serviceClassDataSync);
            case SERVICE_CLASS_DATA_ASYNC:
                return mContext.getText(com.android.internal.R.string.serviceClassDataAsync);
            case SERVICE_CLASS_PACKET:
                return mContext.getText(com.android.internal.R.string.serviceClassPacket);
            case SERVICE_CLASS_PAD:
                return mContext.getText(com.android.internal.R.string.serviceClassPAD);
            /* M: SS part*/
            /// M: [mtk04070][111125][ALPS00093395]MTK added for line2 and video call. @{
            case MtkRIL.SERVICE_CLASS_LINE2:
            case MtkRIL.SERVICE_CLASS_VIDEO:
                return mContext.getText(com.mediatek.R.string.serviceClassVideo);
            /// @}
            /* M: SS part end */
            default:
                return null;
        }
    }
}
