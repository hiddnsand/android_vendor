/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 */

#include <utils/Log.h>
#include <utils/Timers.h>
#include <utils/Trace.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include "MTKProgram.h"

#define ATRACE_TAG ATRACE_TAG_VIEW

namespace android {
namespace uirenderer {

MTKProgram::MTKProgram(const ProgramDescription& description, const char* vertex, const char* fragment)
    : Program(description, vertex, fragment) {
}

/// M: [ProgramBinaryAtlas] Creates a new program with the specified binary @{
MTKProgram::MTKProgram(const ProgramDescription& description, void* binary, GLint length, GLenum format) {
    ATRACE_NAME("Program by binary");
    description.log("Program by binary");
    uint64_t start = 0;
    start = systemTime(SYSTEM_TIME_MONOTONIC);
    programid key = 0;
    key = description.key();

    mInitialized = false;
    mHasColorUniform = false;
    mHasSampler = false;
    mUse = false;
    mVertexShader = 0;
    mFragmentShader = 0;

    //
    //  Load the binary into the program object -- no need to link!
    //
    mProgramId = glCreateProgram();
    ATRACE_BEGIN("glProgramBinaryOES");
    glProgramBinaryOES(mProgramId, format, binary, length);
    ATRACE_END();

    GLint success;
    glGetProgramiv(mProgramId, GL_LINK_STATUS, &success);
    if (success) {
        uint64_t end = 0;
        end = systemTime(SYSTEM_TIME_MONOTONIC);
        ALOGD("createProgram 0x%.8x%.8x, binary %p, length %d, format %d within %dns", uint32_t(key >> 32),
            uint32_t(key & 0xffffffff), binary, length, format, (int) ((end - start) / 1000));

        mInitialized = true;
        bindAttrib("position", kBindingPosition);
        if (description.hasTexture || description.hasExternalTexture) {
            texCoords = bindAttrib("texCoords", kBindingTexCoords);
        } else {
            texCoords = -1;
        }

        transform = addUniform("transform");
        projection = addUniform("projection");
    } else {
        ALOGD("createProgram 0x%.8x%.8x by binary but failed", uint32_t(key >> 32), uint32_t(key & 0xffffffff));

        glDeleteProgram(mProgramId);
        mProgramId = 0;
    }
}

}; // namespace uirenderer
}; // namespace android
