/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * A fragment shader for ripple effect
 *
 */


// Reflection brightness. Higher values result in brighter reflections.
// This is now defined as a vector because at least one version of the IMG
// shader compiler will not compile the code when it is defined as a single
// float.
// #define REFLECTION_BRIGHTNESS 0.7
#define REFLECTION_BRIGHTNESS vec4( 0.7, 0.7, 0.7, 0.7 )

// Offset multiplier. Higher values result in more exaggerated ripples.
#define OFFSET_MULTIPLIER 0.05

precision lowp float;
precision mediump int;

/* Material uniforms */
uniform sampler2D u_m_diffuseTexture;
uniform sampler2D u_mirrorTexture;

varying mediump vec2 v_texCoord0;
varying mediump vec4 v_texCoord1;

void main()
{
  lowp vec4 offset = texture2D( u_m_diffuseTexture, v_texCoord0.xy) * 2.0 - 1.0;

  vec2 screen = ( v_texCoord1.xy / v_texCoord1.w ) * 0.5 + 0.5;

  screen.y = 1.0 - screen.y;

  gl_FragColor = REFLECTION_BRIGHTNESS * texture2D( u_mirrorTexture,
    screen + offset.xy * OFFSET_MULTIPLIER );
  gl_FragColor.a = 1.0;
}
