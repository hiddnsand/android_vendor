/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.telephony.CarrierConfigManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.Rlog;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.os.Build;
import android.os.SystemProperties;

import com.android.ims.ImsConferenceState;
import com.mediatek.ims.OperatorUtils;
import com.mediatek.ims.internal.CallControlDispatcher;
import com.mediatek.ims.internal.ConferenceCallMessageHandler;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.Math;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * ImsConferenceHandler, singleton object to handle CEP and local participants
 */
public class ImsConferenceHandler {

    public abstract static class Listener {
        public void onParticipantsUpdate(ImsConferenceState confState) {};
        public void onAutoTerminate() {};
    }

    private static final String LOG_TAG = "ImsConferenceHandler";
    private static final boolean DBG = true;
    private static final boolean VDBG = false; // STOPSHIP if true

    // Sensitive log task
    private static final String PROP_FORCE_DEBUG_KEY = "persist.log.tag.tel_dbg";
    private static final boolean SENLOG = TextUtils.equals(Build.TYPE, "user");
    private static final boolean TELDBG = (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1);

    private static final String USER_ENTITY = "user-entity";
    private static final String CONF_HOST = "host";

    private static ImsConferenceHandler mConfHdler;

    private static Listener mListener;

    private Context mContext;

    // Keep the conference participants
    private LinkedHashMap mConfParticipants = new LinkedHashMap<String, Bundle>();
    private List<Bundle> mUnknowParticipants = new ArrayList<Bundle>();

    // If the CEP @FT NW is wrong, please turn on this flag.
    private static final boolean mUpdateWithLocalCache = false;
    // Local participant addr - CEP addr maps
    private ArrayList<String> mLocalParticipants = new ArrayList<String>();
    private String mAddingParticipant;
    private String mRemovingParticipant;

    private int mConfCallId = -1;
    private String mHostAddr;
    private Bundle mHostInfo;
    private int mCepVersion = -1;

    public static ImsConferenceHandler getInstance() {
        if (mConfHdler == null) {
            mConfHdler = new ImsConferenceHandler();
        }
        return mConfHdler;
    }

    private ImsConferenceHandler() {
        Rlog.d(LOG_TAG, "ImsConferenceHandler()");
        /* Register for receiving conference call xml message */
    }

    public void startConference(Context ctx, Listener listener, String callId) {
        if (mContext != null) {
            Rlog.d(LOG_TAG, "startConference() failed, a conference is ongoing");
            return;
        }
        Rlog.d(LOG_TAG, "startConference()");
        mListener = listener;
        mContext = ctx;
        final IntentFilter filter = new IntentFilter();
        filter.addAction(ImsConstants.ACTION_IMS_CONFERENCE_CALL_INDICATION);
        mContext.registerReceiver(mBroadcastReceiver, filter);
        mConfCallId = Integer.parseInt(callId);
    }

    public void closeConference(String callId) {
        if (callId == null || mConfCallId != Integer.parseInt(callId)) {
            return;
        }
        Rlog.d(LOG_TAG, "closeConference()");
        mListener = null;
        if (mContext != null) {
            mContext.unregisterReceiver(mBroadcastReceiver);
            mContext = null;
        }
        // clean the member variable
        mLocalParticipants.clear();
        mAddingParticipant = null;
        mRemovingParticipant = null;
        mConfCallId = -1;
        mCepVersion = -1;
        mHostAddr = null;
        mHostInfo = null;
        mConfParticipants.clear();
        mUnknowParticipants.clear();
    }

    public void firstMerge(String num_1, String num_2) {
        mLocalParticipants.clear();
        mLocalParticipants.add(CONF_HOST);
        mLocalParticipants.add(num_1);
        mLocalParticipants.add(num_2);
    }

    public void tryAddParticipant(String addr) {
        mAddingParticipant = addr;
    }

    public void tryRemoveParticipant(String addr) {
        mRemovingParticipant = addr;
    }

    public void modifyParticipantComplete() {
        if (mAddingParticipant != null && !mLocalParticipants.contains(mAddingParticipant)) {
            mLocalParticipants.add(mAddingParticipant);
        }
        if (mRemovingParticipant != null) {
            mLocalParticipants.remove(mRemovingParticipant);
        }
        mAddingParticipant = null;
        mRemovingParticipant = null;
        if (!SENLOG || TELDBG) {
            Rlog.d(LOG_TAG, "modifyParticipantComplete: "+ mLocalParticipants);
        } else {
            Rlog.d(LOG_TAG, "modifyParticipantComplete: [hidden]");
        }
    }

    public void modifyParticipantFailed() {
        mAddingParticipant = null;
        mRemovingParticipant = null;
        if (!SENLOG || TELDBG) {
            Rlog.d(LOG_TAG, "modifyParticipantFailed: "+ mLocalParticipants);
        } else {
            Rlog.d(LOG_TAG, "modifyParticipantFailed: [hidden]");
        }
    }

    public String getConfParticipantUri(String addr) {
        Bundle confInfo = (Bundle)mConfParticipants.get(addr);
        if (confInfo == null) {
            return addr;
        }
        String participantUri = confInfo.getString(USER_ENTITY);
        if (participantUri == null || !participantUri.startsWith("sip:")) {
            participantUri = addr;
        }
        if (!SENLOG || TELDBG) {
            Rlog.d(LOG_TAG, "removeParticipants uri: " + participantUri +
                    " addr: " + addr);
        } else {
            Rlog.d(LOG_TAG, "removeParticipants uri:[hidden] addr:[hidden]");
        }
        return participantUri;
    }

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        private ConferenceCallMessageHandler parseXmlPackage(int len, String data) {
            try {
                //Read conference data and parse it
                InputStream inStream =
                    new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));
                SAXParserFactory factory = SAXParserFactory.newInstance();
                SAXParser saxParse = factory.newSAXParser();
                ConferenceCallMessageHandler xmlData = new ConferenceCallMessageHandler();
                if (xmlData == null) {
                    return null;
                }
                saxParse.parse(inStream, xmlData);
                return xmlData;
            } catch (Exception ex) {
                Rlog.d(LOG_TAG, "Parsing exception: " + ex.toString());
                updateConferenceStateWithLocalCache();
                return null;
            }
        }

        private void updateConferenceStateWithLocalCache() {
            Rlog.d(LOG_TAG, "updateConferenceStateWithLocalCache()");

            if (mLocalParticipants.size() == 0 && shouldAutoTerminateConf()) {
                if (mListener != null) {
                    mListener.onAutoTerminate();
                }
                Rlog.d(LOG_TAG, "no participants");
                return;
            }

            ImsConferenceState confState = new ImsConferenceState();

            for (String addr : mLocalParticipants) {
                Bundle userInfo = createFakeInfo(addr);
                confState.mParticipants.put(addr, userInfo);
                if (!SENLOG || TELDBG) {
                    Rlog.d(LOG_TAG, "submit participants: " + addr);
                } else {
                    Rlog.d(LOG_TAG, "submit participants: [hidden]");
                }
            }

            if (mListener != null) {
                mListener.onParticipantsUpdate(confState);
            }
        }

        private void setupHost(ConferenceCallMessageHandler xmlData) {
            // get host address from the optional xml element <host-info>
            mHostAddr = getUserNameFromSipTelUriString(xmlData.getHostInfo());
            if (mHostAddr != null && mHostAddr.trim().length() > 0) {
                if (!SENLOG || TELDBG) {
                    Rlog.d(LOG_TAG, "host-info is included in xml: " + mHostAddr);
                } else {
                    Rlog.d(LOG_TAG, "host-info is included in xml: [hidden]");
                }
                finalizeHost(xmlData.getUsers());
                return;
            }

            if (xmlData.getUsers().size() == 0) {
                // For the first CEP with no member (such as op12, op07, test sim),
                // Generate a fake host, the fake host will be ignored in ImsConference
                mHostAddr = "host";
                mHostInfo = createFakeInfo(mHostAddr);
                return;
            }

            // use the first user entity as the host address
            ConferenceCallMessageHandler.User user = xmlData.getUsers().get(0);
            mHostAddr = getUserNameFromSipTelUriString(user.getEntity());
            mHostInfo = packUserInfo(user);
            if (!SENLOG || TELDBG) {
                Rlog.d(LOG_TAG, "consider the first user as host: " + mHostAddr);
            } else {
                Rlog.d(LOG_TAG, "consider the first user as host: [hidden]");
            }
        }

        private Bundle packUserInfo(ConferenceCallMessageHandler.User user) {
            String entity = user.getEntity();
            String userAddr = getUserNameFromSipTelUriString(entity);
            Bundle userInfo = new Bundle();
            userInfo.putString(ImsConferenceState.USER, userAddr);
            userInfo.putString(ImsConferenceState.DISPLAY_TEXT, user.getDisplayText());
            userInfo.putString(ImsConferenceState.ENDPOINT, user.getEndPoint());
            userInfo.putString(ImsConferenceState.STATUS, user.getStatus());
            userInfo.putString(USER_ENTITY, entity);
            if (!SENLOG || TELDBG) {
                Rlog.d(LOG_TAG, "pack user addr: " + userAddr + " status: " + user.getStatus() +
                        " uri: " + entity);
            } else {
                Rlog.d(LOG_TAG, "pack user addr: [hidden]" + " status: " + user.getStatus() +
                        " uri: [hidden]");
            }
            return userInfo;
        }

        private Bundle createFakeInfo(String addr) {
            Bundle userInfo = new Bundle();
            userInfo.putString(ImsConferenceState.USER, addr);
            userInfo.putString(ImsConferenceState.DISPLAY_TEXT, addr);
            userInfo.putString(ImsConferenceState.ENDPOINT, addr);
            userInfo.putString(ImsConferenceState.STATUS, ImsConferenceState.STATUS_CONNECTED);
            return userInfo;
        }

        private void finalizeHost(List<ConferenceCallMessageHandler.User> users) {
            Rlog.d(LOG_TAG, "finalizeHost");
            // Find host in the CEP
            for (ConferenceCallMessageHandler.User user : users) {
                String entity = user.getEntity();
                String userAddr = getUserNameFromSipTelUriString(entity);

                if (PhoneNumberUtils.compareLoosely(userAddr, mHostAddr)) {
                    mHostInfo = packUserInfo(user);
                    mHostAddr = userAddr;
                    if (!SENLOG || TELDBG) {
                        Rlog.d(LOG_TAG, userAddr + " is host");
                    } else {
                        Rlog.d(LOG_TAG, "[hidden] is host");
                    }
                    break;
                }
            }
            if (mHostInfo == null) {
                Rlog.d(LOG_TAG, "No host in CEP, generate a fake one");
                mHostInfo = createFakeInfo(mHostAddr);
            }
        }

        private void fullUpdateParticipants(List<ConferenceCallMessageHandler.User> users) {
            Rlog.d(LOG_TAG, "reset all users as participants");
            mUnknowParticipants.clear();
            mConfParticipants.clear();

            // add host as first participant.
            mConfParticipants.put(mHostAddr, mHostInfo);

            for (ConferenceCallMessageHandler.User user : users) {
                String entity = user.getEntity();
                String userAddr = getUserNameFromSipTelUriString(entity);
                Bundle userInfo = packUserInfo(user);
                if (!SENLOG || TELDBG) {
                    Rlog.d(LOG_TAG, "handle user: " +  entity + " addr: " + userAddr);
                } else {
                    Rlog.d(LOG_TAG, "handle user: [hidden], addr: [hidden]");
                }

                if (userAddr == null || userAddr.trim().length() == 0) {
                    mUnknowParticipants.add(userInfo);
                    Rlog.d(LOG_TAG, "add unknow participants");
                } else {
                    mConfParticipants.put(userAddr, userInfo);
                    if (!SENLOG || TELDBG) {
                        Rlog.d(LOG_TAG, "add participants: " + userAddr);
                    } else {
                        Rlog.d(LOG_TAG, "add participants: [hidden]");
                    }
                }
            }
        }

        private void partialUpdateParticipants(List<ConferenceCallMessageHandler.User> users) {
            Rlog.d(LOG_TAG, "partial update participants");
            for (ConferenceCallMessageHandler.User user : users) {
                String entity = user.getEntity();
                String userAddr = getUserNameFromSipTelUriString(entity);
                Bundle userInfo = packUserInfo(user);
                if (!SENLOG || TELDBG) {
                    Rlog.d(LOG_TAG, "handle user: " +  entity + " addr: " + userAddr);
                } else {
                    Rlog.d(LOG_TAG, "handle user: [hidden], addr: [hidden]");
                }

                String status = user.getStatus();
                // update participants
                if(userAddr == null || userAddr.trim().length() == 0) {
                    if (status.equals(ConferenceCallMessageHandler.STATUS_CONNECTED)) {
                        mUnknowParticipants.add(userInfo);
                        Rlog.d(LOG_TAG, "add unknow participants");
                    } else if (status.equals(ConferenceCallMessageHandler.STATUS_DISCONNECTED)){
                        // remove last unknow participants
                        mUnknowParticipants.remove(mUnknowParticipants.size() - 1);
                        Rlog.d(LOG_TAG, "remove unknow participants");
                    }
                } else {
                    if (!SENLOG || TELDBG) {
                        Rlog.d(LOG_TAG, "update participants: " + userAddr);
                    } else {
                        Rlog.d(LOG_TAG, "update participants: [hidden]");
                    }
                    if (!status.equals(ConferenceCallMessageHandler.STATUS_DIALING_OUT)) {
                        mConfParticipants.put(userAddr, userInfo);
                        Rlog.d(LOG_TAG, "update participants: " + userAddr);
                    }
                }
            }
        }

        private void notifyConfStateUpdate() {
            Rlog.d(LOG_TAG, "notifyConfStateUpdate()");

            ImsConferenceState confState = new ImsConferenceState();

            Iterator<Entry<String, Bundle>> iterator = mConfParticipants.entrySet().iterator();
            while (iterator.hasNext()) {
                Entry<String, Bundle> entry = iterator.next();
                confState.mParticipants.put(entry.getKey(), entry.getValue());
                if (!SENLOG || TELDBG) {
                    Rlog.d(LOG_TAG, "submit participants: " + entry.getKey());
                } else {
                    Rlog.d(LOG_TAG, "submit participants: [hidden]");
                }
            }

            int key = 0;
            for (Bundle userInfo: mUnknowParticipants) {
                confState.mParticipants.put(Integer.toString(key), userInfo);
                if (!SENLOG || TELDBG) {
                    Rlog.d(LOG_TAG, "submit unknow participants: " + Integer.toString(key));
                } else {
                    Rlog.d(LOG_TAG, "submit unknow participants: [hidden]");
                }
                ++key;
            }

            if (mListener != null) {
                mListener.onParticipantsUpdate(confState);
            }
        }

        private boolean isEmptyConference() {
            int userCount = mUnknowParticipants.size();

            Iterator<Entry<String, Bundle>> iterator = mConfParticipants.entrySet().iterator();
            while (iterator.hasNext()) {
                Entry<String, Bundle> entry = iterator.next();
                Bundle confInfo = entry.getValue();
                String status = confInfo.getString(ImsConferenceState.STATUS);
                if (!status.equals(ConferenceCallMessageHandler.STATUS_DISCONNECTED)) {
                    ++userCount;
                }
            }

            if (userCount <= 1) {
                return true;
            }
            return false;
        }

        /**
        * To handle IMS conference call message
        *
        * @param len    The length of data
        * @param data   Conference call message
        */
        private void handleImsConfCallMessage(int len, String data) {
            if ((data == null) || (data.equals(""))) {
                Rlog.e(LOG_TAG, "Failed to handleImsConfCallMessage due to data is empty");
                return;
            }

            if (mUpdateWithLocalCache) {
                Rlog.d(LOG_TAG, "Update the participants with local cache");
                updateConferenceStateWithLocalCache();
                return;
            }

            Rlog.d(LOG_TAG, "handleVoLteConfCallMessage, data length = " + data.length());

            ConferenceCallMessageHandler xmlData = parseXmlPackage(len, data);
            if (xmlData == null) {
                Rlog.e(LOG_TAG, "can't create xmlData object, update conf state with local cache");
                updateConferenceStateWithLocalCache();
                return;
            }

            int version = xmlData.getVersion();
            if (mCepVersion >= version && mCepVersion != -1) {
                Rlog.e(LOG_TAG, "version is equal or less than local version" +
                        mCepVersion + "," + version);
                return;
            }
            mCepVersion = version;

            // get user data from xml and fill them into ImsConferenceState data structure.
            List<ConferenceCallMessageHandler.User> users = xmlData.getUsers();
            boolean isFirstCEP = false;
            boolean isPartialCEP = false;
            // setup host
            if (mHostAddr == null) {
                setupHost(xmlData);
                isFirstCEP = true;
            }

            // get optional xml element: user count
            int userCount = xmlData.getUserCount();

            // get CEP state
            int cepState = xmlData.getCEPState();

            // no optional user count element,
            // remove the participants who is not included in the xml.
            switch (cepState) {
                case ConferenceCallMessageHandler.CEP_STATE_FULL:
                    fullUpdateParticipants(users);
                    break;
                case ConferenceCallMessageHandler.CEP_STATE_PARTIAL:
                    partialUpdateParticipants(users);
                    isPartialCEP = true;
                    break;
                default:
                    if (userCount == -1 || userCount == users.size()) {
                        fullUpdateParticipants(users);
                    } else {
                        partialUpdateParticipants(users);
                        isPartialCEP = true;
                    }
            }
            Rlog.d(LOG_TAG, "isPartialCEP: " + isPartialCEP);
            // Terminate the empty conference for specific operator
            if (isEmptyConference() && shouldAutoTerminateConf() && !isFirstCEP && !isPartialCEP) {
                Rlog.d(LOG_TAG, "no participants, terminate the conference");
                if (mListener != null) {
                    mListener.onAutoTerminate();
                }
                return;
            }

            notifyConfStateUpdate();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Rlog.d(LOG_TAG, "received broadcast " + action);
            /* Handle IMS conference call xml message */
            if (ImsConstants.ACTION_IMS_CONFERENCE_CALL_INDICATION.equals(action)) {
                String data = intent.getStringExtra(ImsConstants.EXTRA_MESSAGE_CONTENT);
                if ((data != null) && (!data.equals(""))) {
                    handleImsConfCallMessage(data.length(), data);
                }
            } else {
                Rlog.e(LOG_TAG, "can't handle conference message since no call ID. Abnormal Case");
            }
        }
    };

    // Customize for specific operator and location.
    private boolean shouldAutoTerminateConf() {
        boolean shouldTerminate = false;
        Rlog.d(LOG_TAG, "shouldTerminate:" + shouldTerminate);
        return shouldTerminate;
    }

    private String getUserNameFromSipTelUriString(String uriString) {
        if (uriString == null) {
            return null;
        }

        Uri uri = Uri.parse(uriString);

        // Gets the address part, i.e. everything between 'sip:' and the fragment separator '#'.
        // ex: '+8618407404132@10.185.184.137:5087;transport=UDP'
        // or '1234;phone-context=munich.example.com;isub=@1134'
        String address = uri.getSchemeSpecificPart();
        if (address == null) {
            return null;
        }

        // Gets user name, i.e. everything before '@'.
        // ex: '+8618407404132' or '1234;phone-context=munich.example.com;isub='
        String userName = PhoneNumberUtils.getUsernameFromUriNumber(address);
        if (userName == null) {
            return null;
        }

        // Gets pure user name part, i.e. everything before ';' or ','.
        // ex: '+8618407404132' or '1234'
        int pIndex = userName.indexOf(';');    //WAIT
        int wIndex = userName.indexOf(',');    //PAUSE

        if (pIndex >= 0 && wIndex >= 0) {
            return userName.substring(0, Math.min(pIndex, wIndex));
        } else if (pIndex >= 0) {
            return userName.substring(0, pIndex);
        } else if (wIndex >= 0) {
            return userName.substring(0, wIndex);
        } else {
            return userName;
        }
    }

}
