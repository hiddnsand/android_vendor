package com.orangelabs.rcs.core.ims.rcsua;

import android.content.Context;

import com.orangelabs.rcs.provider.settings.RcsSettings;
import com.orangelabs.rcs.utils.logger.Logger;
import com.orangelabs.rcs.core.ims.ImsModule;
import com.orangelabs.rcs.core.ims.network.ImsNetworkInterface;
import com.orangelabs.rcs.core.ims.rcsua.RcsUaAdapter.RcsUaEvent;

/**
 * The Class RcsProxyRegistrationHandler.
 */
public class RcsProxyRegistrationHandler implements RcsUaEventDispatcher.RCSEventDispatcher {

    private Logger logger = Logger.getLogger(this.getClass().getName());
    
    /**
     * IMS network interface
     */
    private ImsNetworkInterface networkInterface;
    
    private static final int REGISTRATION_TIMER = 1000;
    private static final int MAX_REGISTRATION_TIMER = 20000;

    private static final String TAG = "RcsProxyRegistrationHandler";
    private static RcsUaAdapter mRcsuaAdapt = null;
    private static boolean mIsRegistrationSuccessful = false;
    private static boolean mIsDeRegistrationRequestSent = false;
    /**
     * Wait  answer for register/deregister 
     */
    private static Object mWaitRegisterResponse = new Object();
    private static Object mWaitDeRegisterResponse = new Object();
    private static Object mWaitReRegisterResponse = new Object();

    /**
     * Instantiates a new rcs proxy registration handler.
     *
     * @param context the context
     */
    public RcsProxyRegistrationHandler(Context context) {
        if (mRcsuaAdapt == null) {
            mRcsuaAdapt = RcsUaAdapter.getInstance();
        }
    }

    public ImsNetworkInterface RcsProxyRegistrationNetworkInterface() {
        return networkInterface;
    }

    /**
     * Register.
     *
     * @return true, if successful
     */
    public boolean register(ImsNetworkInterface mNetworkInterface) {
        try {
            if(!mRcsuaAdapt.isRegisteredbyVolte()) {
                
            synchronized (mWaitRegisterResponse) {
                mRcsuaAdapt.setRegisteringState(true);
                logger.debug( TAG+"Register : send request AT+EIMSRCS=1,15");
                boolean addStatus=RcsSettings.getInstance().sendAtCommand("AT+EIMSRCS=1,15");
                if(addStatus){
                    mRcsuaAdapt.setRegisteringState(true);
                }
                logger.debug( TAG+"register : send register request through AT");
                
                networkInterface = mNetworkInterface;
                String rcsCapabilityFeatureTags = RcsUaAdapter.getInstance().getRCSFeatureTag() + "\0";
                RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsuaAdapt.RCS_PROXY_EVENT_RCS_REGISTER);
                event.putString(rcsCapabilityFeatureTags, rcsCapabilityFeatureTags.length());
                mRcsuaAdapt.writeEvent(event);
                
                //wait for 1 second to allow status to get updated 
                int i = 0;
                while(mRcsuaAdapt.isRegistering()){
                	i= i+ REGISTRATION_TIMER;
                	
                    if(i>MAX_REGISTRATION_TIMER)
                    	break;
                	
                    mWaitRegisterResponse.wait(REGISTRATION_TIMER);
                }
                logger.debug(TAG+"  after register request response ; registration status : "+ mIsRegistrationSuccessful+i);

            }
            } else {
                logger.debug(TAG+"  Already RCS tags are present in first request of IMS, no need to add Tags");
                networkInterface = mNetworkInterface;
                mIsRegistrationSuccessful=true;
                mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
                mRcsuaAdapt.setRegisteringState(false);
                
            }
        } catch (InterruptedException e) {
            return false;
        }finally{
            mRcsuaAdapt.setRegisteringState(false);
        }
        mNetworkInterface.getSipManager().getSipStack().setServiceRoutePath(mRcsuaAdapt.getmRoute());
        mNetworkInterface.getSipManager().getSipStack().setInstanceId(mRcsuaAdapt.getmInstanceId());
        ImsModule.IMS_USER_PROFILE.setAssociatedUri(mRcsuaAdapt.getmAssociatedUri());
        return mIsRegistrationSuccessful;
    }
    
    
    public boolean reRegister(ImsNetworkInterface mNetworkInterface) {
        mIsRegistrationSuccessful=false;
        mRcsuaAdapt.setRegisteredbyVolte(0);
        mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
        try {
            synchronized (mWaitReRegisterResponse) {
                boolean addStatus=RcsSettings.getInstance().sendAtCommand("AT+EIMSRCS=2,15");
                if(addStatus)
                    mRcsuaAdapt.setRegisteringState(true);
                logger.debug( TAG+"ReRegister : send request AT+EIMSRCS=2,15");
                
                networkInterface = mNetworkInterface;
                //wait for 1 second to allow status to get updated 
                int i = 0;
                while(mRcsuaAdapt.isRegistering()){
                    i= i+ REGISTRATION_TIMER;
                    
                    if(i>MAX_REGISTRATION_TIMER)
                        break;
                    
                    mWaitReRegisterResponse.wait(REGISTRATION_TIMER);
                }
                logger.debug(TAG+"  after Reregister request response ; registration status : "+ mIsRegistrationSuccessful+i);

            }
           
        } catch (InterruptedException e) {
            return false;
        }finally{
        mRcsuaAdapt.setRegisteringState(false);
        }
        mIsRegistrationSuccessful = true;
        mRcsuaAdapt.setRegisteringState(false);
        logger.debug(TAG+"setRegistrationStatus : "+ mIsRegistrationSuccessful);
        mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
        mNetworkInterface.getSipManager().getSipStack().setServiceRoutePath(mRcsuaAdapt.getmRoute());
        mNetworkInterface.getSipManager().getSipStack().setInstanceId(mRcsuaAdapt.getmInstanceId());
        ImsModule.IMS_USER_PROFILE.setAssociatedUri(mRcsuaAdapt.getmAssociatedUri());
        return mIsRegistrationSuccessful;
    }
    


    /**
     * Unregister.
     *
     * @return true, if successful
     */
    public boolean deRegister() {
    	
    	//problem because AT command disables when REGISTER is slow, TODO find better way
    	 if(RcsSettings.getInstance().isSupportOP07()){
    	    return true;
    	 }
        try {
            synchronized (mWaitDeRegisterResponse) {
                boolean addStatus=RcsSettings.getInstance().sendAtCommand("AT+EIMSRCS=0,15");
                if(addStatus){
                    mRcsuaAdapt.setRegisteringState(false);}
                logger.debug( TAG+"De-register : send request  AT AT+EIMSRCS=0,15");
                mIsDeRegistrationRequestSent = true;
                logger.debug( "deregister : send request");
                
                //send register request
                String rcsCapabilityFeatureTags = RcsUaAdapter.getInstance().getRCSFeatureTag()+"\0";
                RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsuaAdapt.RCS_PROXY_EVENT_RCS_DEREGISTER);
                event.putString(rcsCapabilityFeatureTags, rcsCapabilityFeatureTags.length());
                mRcsuaAdapt.writeEvent(event);
               
                
                //wait for 1 sec
                mWaitDeRegisterResponse.wait(REGISTRATION_TIMER);
                logger.debug( TAG+"after deregister request response ; deregistration status : "+ mIsDeRegistrationRequestSent);
            }
        } catch (InterruptedException e) {
        	mIsRegistrationSuccessful = false;
        }
        return mIsRegistrationSuccessful;
    }

    /**
     * Event callback. 
     *
     * @param event the event
     * runs in RCSUA event dispatcher 
     */
    public void EventCallback(RcsUaEvent event) {

        try {
            int requestId = event.getRequestID();;
            String state = event.getString(event.getDataLen());

            switch (requestId) {
            case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_REGISTERING:
                logger.debug(TAG+"registering state : " +state);                 
                 if(state.equals("1")){
                    logger.debug("RCS_UA registering currently. wait for REG Success update");
                 }else{
                    logger.debug("RCS_UA couldn't add capab successfully. stop registering process");
					synchronized (mWaitRegisterResponse) {
						//failure set registration as failed.
						mIsRegistrationSuccessful = false;
						logger.debug(TAG+"setRegistrationStatus : "+ mIsRegistrationSuccessful);
						mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
						mWaitRegisterResponse.notify();
					}
                 }
                break;
                
            case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_REGISTER:
                logger.debug(TAG+"RCS_PROXY_EVENT_RSP_RCS_REGISTER , state: "+state);                                 
                //NOTIFY LISTENING MODULE read the content and based on that set the value                     
                synchronized (mWaitRegisterResponse) {
                    if(state.equals("1")){
    					mIsRegistrationSuccessful = true;
    				}
                    else{
                        mIsRegistrationSuccessful = false;
                    }
                    //set the registration status and notify
                    mRcsuaAdapt.setRegisteringState(false);
                    logger.debug(TAG+"setRegistrationStatus : "+ mIsRegistrationSuccessful);
                    mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
                    mWaitRegisterResponse.notify();
                }
                break;

            case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_DEREGISTER:
                logger.debug( TAG+"Deregistered successfully");
                //read the content and based on that set the value                     
                synchronized (mWaitDeRegisterResponse) {
                	 //set the registration status and notify
	                mIsRegistrationSuccessful = false;
                    logger.debug(TAG+"setRegistrationStatus : "+ mIsRegistrationSuccessful);
	                mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
	                mWaitDeRegisterResponse.notify();
                }

                //set the registration status 
                mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
                break;
            case RcsUaAdapter.RCS_PROXY_EVENT_RES_REG_INFO:
                if(mRcsuaAdapt.isRegisteredbyVolte()) {
                    synchronized (mWaitReRegisterResponse) {
                        
                        mWaitReRegisterResponse.notify();
                    }
                }
                break;   
            default:
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Enable request.
     */
    public void enableRequest() {
    }

    /**
     * Disable request.
     */
    public void disableRequest() {
    }

}
