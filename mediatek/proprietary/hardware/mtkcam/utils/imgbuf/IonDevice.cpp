/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-IonDevice"

#include "IIonDevice.h"
#include "MyUtils.h"
#include <mtkcam/utils/std/LogTool.h>
#include <mtkcam/utils/debug/debug.h>
//
#include <ion/ion.h>
#include <libion_mtk/include/ion.h>
//
#include <list>
#include <string>
//
#include <utils/Mutex.h>
//
using namespace NSCam;

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

/******************************************************************************
 *
 ******************************************************************************/
namespace {
class IonDeviceProviderImpl : public IIonDeviceProvider
                            , public IDebuggee
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Definitions.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////

    struct  IonDevice;
    using   UserListT = std::list<std::weak_ptr<IonDevice>>;

    struct  IonDevice : public IIonDevice
    {
    public:
        std::string const   mUserName;
        struct timespec     mTimestamp;
        int                 mDeviceFd = -1;

        using IteratorT = UserListT::iterator;
        IteratorT           mIterator;

    public:
                            IonDevice(char const* userName);
        virtual             ~IonDevice();
        virtual auto        getDeviceFd() const -> int { return mDeviceFd; }
    };

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////
    static const std::string            mDebuggeeName;
    std::shared_ptr<IDebuggeeCookie>    mDebuggeeCookie = nullptr;
    NSCam::Utils::LogTool*              mLogTool = nullptr;

    mutable android::Mutex  mLock;
    int                     mIonDevFd = -1; //  ION Device FD.
    UserListT               mUserList;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Interface.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////            IDebuggee
    virtual auto            debuggeeName() const -> std::string { return mDebuggeeName; }
    virtual auto            debug(
                                android::Printer& printer,
                                const std::vector<std::string>& options __unused
                            ) -> void
                            {
                                print(printer);
                            }

public:
    static  auto            get() -> IonDeviceProviderImpl*;
                            IonDeviceProviderImpl();
    auto                    detach(IonDevice* pDevice) -> void;
    auto                    attach(std::shared_ptr<IonDevice>& pDevice) -> bool;

public:

    virtual auto            print(::android::Printer& printer) const -> void;

    virtual auto            makeIonDevice(
                                char const* userName
                            ) -> std::shared_ptr<IIonDevice>;

};
};


/******************************************************************************
 *
 ******************************************************************************/
const std::string IonDeviceProviderImpl::mDebuggeeName{"NSCam::IIonDeviceProvider"};

/******************************************************************************
 *
 ******************************************************************************/
auto
IonDeviceProviderImpl::get() -> IonDeviceProviderImpl*
{
    static auto inst = std::make_shared<IonDeviceProviderImpl>();
    static auto init = [](){
        if ( auto pDbgMgr = IDebuggeeManager::get() ) {
            inst->mDebuggeeCookie = pDbgMgr->attach(inst);
        }
        return true;
    }();
    return inst.get();
}


/******************************************************************************
 *
 ******************************************************************************/
auto
IIonDeviceProvider::get() -> IIonDeviceProvider*
{
    return IonDeviceProviderImpl::get();
}


/******************************************************************************
 *
 ******************************************************************************/
IonDeviceProviderImpl::
IonDeviceProviderImpl()
    : IIonDeviceProvider()
    , mLogTool(NSCam::Utils::LogTool::get())
{
}


/******************************************************************************
 *
 ******************************************************************************/
auto
IonDeviceProviderImpl::
print(::android::Printer& printer) const -> void
{
    android::Mutex::Autolock _l(mLock);

    printer.printFormatLine("## ion device fd: %d", mIonDevFd);

    for (auto const& v : mUserList) {
        auto s = v.lock();
        if ( s == nullptr ) {
            printer.printLine("dead user");
        }
        else {
            printer.printFormatLine("  %s %s",
                mLogTool->convertToFormattedLogTime(&s->mTimestamp).c_str(),
                s->mUserName.c_str()
            );
        }
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
IonDeviceProviderImpl::
makeIonDevice(
    char const* userName
) -> std::shared_ptr<IIonDevice>
{
    std::shared_ptr<IonDevice> pDevice = std::make_shared<IonDevice>(userName);
    if ( pDevice == nullptr ) {
        MY_LOGE("User %s: fail to make a new device", userName);
        return nullptr;
    }

    if ( ! attach(pDevice) ) {
        MY_LOGE("User %s: fail to attach the device", userName);
        return nullptr;
    }

    return pDevice;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
IonDeviceProviderImpl::
attach(std::shared_ptr<IonDevice>& pDevice) -> bool
{
    android::Mutex::Autolock _l(mLock);

    if ( 0 > mIonDevFd ) {
#if defined(MTK_ION_SUPPORT)
        mIonDevFd = ::mt_ion_open("mtkcam");
        if  ( 0 > mIonDevFd ) {
            MY_LOGE("User %s: mt_ion_open() return %d", pDevice->mUserName.c_str(), mIonDevFd);
            return false;
        }
#else
        MY_LOGE("not defined: MTK_ION_SUPPORT");
        return false;
#endif
    }

    auto iter = mUserList.insert(mUserList.end(), pDevice);
    pDevice->mIterator = iter;
    pDevice->mDeviceFd = mIonDevFd;
    mLogTool->getCurrentLogTime(&pDevice->mTimestamp);

    return true;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
IonDeviceProviderImpl::
detach(IonDevice* pDevice) -> void
{
    android::Mutex::Autolock _l(mLock);

    mUserList.erase(pDevice->mIterator);

    if ( mUserList.empty() ) {
#if defined(MTK_ION_SUPPORT)
        if ( 0 <= mIonDevFd ) {
            ::ion_close(mIonDevFd);
            mIonDevFd = -1;
        }
#endif
    }
}


/******************************************************************************
 *
 ******************************************************************************/
IonDeviceProviderImpl::
IonDevice::
IonDevice(char const* userName)
    : IIonDevice()
    , mUserName(userName)
{
    ::memset(&mTimestamp, 0, sizeof(mTimestamp));
}


/******************************************************************************
 *
 ******************************************************************************/
IonDeviceProviderImpl::
IonDevice::
~IonDevice()
{
    IonDeviceProviderImpl::get()->detach(this);
}

