package com.mediatek.config;

import com.mediatek.config.configutil.ConfigUtil;
import android.util.Log;
import android.os.SystemProperties;

public class SeRuntimeOptions {
    static public ConfigUtil.IParser sConfigFileParser;

    static private final String CFG_FILE_PATH = "/vendor/etc/nfcse.cfg";
    static private final String CFG_FILE_RULES[] = {
        /**
         *  MultiSE related config
         */
        "SWP1=1:SIM1=1,SIM2=2",
        "SWP2=2:SIM1=1,SIM2=2,ESE=3",
        "SD=3:NO=0,YES=1",
        "ESE=4:NO=0,YES=1",
        "NON_NFC_SIM_POPUP=5:NO=0,YES=1",
        "EVT_BROADCAST_AC=6:DEFAULT=0,BYPASS=1",
        "BUNDLE_SIM_STATE=7:NO=0,YES=1",
        "NO_EMU_IN_FLYMODE=8:NO=0,YES=1",
        "BEAM_SEND_FAIL_CNT=9:OPT0=0,OPT1=1,OPT2=2,OPT3=3,OPT4=4,OPT5=5," +
            "OPT6=6,OPT7=7,OPT8=8,OPT9=9",
        "BEAM_RECV_FAIL_CNT=10:OPT0=0,OPT1=1,OPT2=2,OPT3=3,OPT4=4,OPT5=5," +
            "OPT6=6,OPT7=7,OPT8=8,OPT9=9",
        "BEAM_SEND_SLEEP_TIME=11:OPT0=0,OPT1=1,OPT2=2,OPT3=3,OPT4=4,OPT5=5," +
            "OPT6=6,OPT7=7,OPT8=8,OPT9=9",
        "BEAM_RECV_SLEEP_TIME=12:OPT0=0,OPT1=1,OPT2=2,OPT3=3,OPT4=4,OPT5=5," +
            "OPT6=6,OPT7=7,OPT8=8,OPT9=9",
        "BEAM_SETUP_CONNECTIION_TIME=13:OPT0=0,OPT1=1,OPT2=2,OPT3=3,OPT4=4,OPT5=5," +
            "OPT6=6,OPT7=7,OPT8=8,OPT9=9",
        "GSMA_EVT_BROADCAST=14:NO=0,YES=1",
        "SEAPI_SUPPORT_CMCC=15:NO=0,YES=1",
        "SEAPI_SUPPORT_PKCS15_EF_DIR_SELECTION=16:NO=0,YES=1",
        "SEAPI_SUPPORT_ARF_ONLY=17:NO=0,YES=1",
        "SEAPI_SUPPORT_PKCS15_EF_DIR_SELECTION_ONLY=18:NO=0,YES=1",
        "SEAPI_LOG_ENHANCEMENT=19:NO=0,YES=1"
    };

    /**
     * Options
     */
    static private final int NON_NFC_SIM_POPUP = 5;
    static private final int EVT_BROADCAST_AC = 6;
    static private final int BUNDLE_SIM_STATE = 7;
    static private final int NO_EMU_IN_FLYMODE = 8;
    static private final int BEAM_SEND_FAIL_CNT= 9;
    static private final int BEAM_RECV_FAIL_CNT= 10;
    static private final int BEAM_SEND_SLEEP_TIME= 11;
    static private final int BEAM_RECV_SLEEP_TIME= 12;
    static private final int BEAM_SETUP_CONNECTIION_TIME= 13;
    static private final int GSMA_EVT_BROADCAST = 14;
    static private final int SEAPI_SUPPORT_CMCC = 15;
    static private final int SEAPI_SUPPORT_PKCS15_EF_DIR_SELECTION = 16;
    static private final int SEAPI_SUPPORT_ARF_ONLY = 17;
    static private final int SEAPI_SUPPORT_PKCS15_EF_DIR_SELECTION_ONLY = 18;
    static private final int SEAPI_LOG_ENHANCEMENT = 19;

    /**
     * Values
     */
    static private final int NO = 0;
    static private final int YES = 1;

    /**
     * EVT_BROADCAST_AC values
     */
    static private final int EVT_AC_DEFAULT = 0;
    static private final int EVT_AC_BYPASS = 1;

    static private final String TAG = "SeRuntimeOptions";
    static private int seapiUiccNum = -1;
    static private boolean isCfgfileExist = false;
    static private int mGSMASupport = -1;

    static {
        sConfigFileParser = ConfigUtil.createParser(CFG_FILE_RULES);
        isCfgfileExist = sConfigFileParser.parse(CFG_FILE_PATH);
    }

    static public ConfigUtil.IParser getParser() {
        return sConfigFileParser;
    }

    static public int getUiccNum(){
        return seapiUiccNum;
    }

    static public boolean isSeapiCoupleWithNfc() {
        int swp1Config[] = new int[1];
        int swp2Config[] = new int[1];
        int swp3ConfigSsd[] = new int[1];
        int swp3ConfigEse[] = new int[1];
        boolean ret = false;
        int swp1Uicc = 0;
        int swp2Uicc = 0;
        int swp3 = 0;

        if (isCfgfileExist == false) {
            Log.d(TAG, "vendor/etc/nfcse.cfg doesn't exist");
            seapiUiccNum = 0;
            return false;
        }

        try {
            if (sConfigFileParser.get(1, swp1Config)) {
                try {
                    if (swp1Config[0] == 1 || swp1Config[0] == 2) {/// SIM1
                        swp1Uicc = swp1Config[0];
                        Log.d(TAG, "swp1Uicc is " + swp1Uicc);
                    }else {
                        Log.d(TAG, "swp1 wrong value in nfcse.cfg");
                    }
                } catch (Exception e) {
                    Log.d(TAG, "CHIP_SWP1 not hit in current SE list");
                }
            } else {
                Log.d(TAG, "swp1 is off");
            }
        } catch (Exception e) {}

        try {
            if (sConfigFileParser.get(2, swp2Config)) {
                try {
                    if (swp2Config[0] == 1 || swp2Config[0] == 2 || swp2Config[0] == 3) {/// SIM2
                        swp2Uicc = swp2Config[0];
                        Log.d(TAG, "swp2Uicc is " + swp2Uicc);
                    } else {
                        Log.d(TAG, "swp2 wrong value in nfcse.cfg");
                    }
                } catch (Exception e) {
                    Log.d(TAG, "CHIP_SWP2 not hit in current SE list");
                }
            } else {
                Log.d(TAG, "swp2 is off");
            }

        } catch (Exception e) {}

        try {
            if (sConfigFileParser.get(3, swp3ConfigSsd) &&
                    sConfigFileParser.get(4, swp3ConfigEse)) {
                try {
                    if (swp3ConfigSsd[0] == 1 && swp3ConfigEse[0] == 1) {
                        Log.d(TAG, "wrong value in nfcse.cfg");
                    } else if (swp3ConfigSsd[0] == 1) {/// SSD
                        swp3 = 3;
                        Log.d(TAG, "swp3 is " + swp3);
                    } else if (swp3ConfigEse[0] == 1) {/// ESE
                        swp3 = 4;
                        Log.d(TAG, "swp3 is " + swp3);
                    } else {
                        Log.d(TAG, "swp3 is off");
                    }
                } catch (Exception e) {
                    Log.d(TAG, "CHIP_SWP3 not hit in current SE list");
                }
            }
        } catch (Exception e) {}

        seapiUiccNum = -1;
        if ((swp1Uicc > 0 && swp2Uicc > 0) ||
                (swp1Uicc > 0 && swp3 > 0) ||
                (swp2Uicc > 0 && swp3 > 0)) {
            ret = true;
        }else {
            if (swp1Uicc == 1 || swp1Uicc == 2) {
                seapiUiccNum = swp1Uicc - 1;
            }else if (swp2Uicc == 1 || swp2Uicc == 2){
                seapiUiccNum = swp2Uicc - 1;
            }else {
                ret = true;
            }
            Log.d(TAG, "seapiUiccNum is " + seapiUiccNum);
        }
        Log.d(TAG, "isSeapiCoupleWithNfc is " + ret);
        return ret;
    }

    static public boolean isSeapiSupportEse() {
        boolean ret = false;
        int swp3ConfigSsd[] = new int[1];
        int swp3ConfigEse[] = new int[1];
        int swp3 = 0;

        try {
            if (sConfigFileParser.get(3, swp3ConfigSsd) &&
                    sConfigFileParser.get(4, swp3ConfigEse)) {
                try {
                    if (swp3ConfigSsd[0] == 1 && swp3ConfigEse[0] == 1) {
                        Log.d(TAG, "wrong value in nfcse.cfg");
                    } else if (swp3ConfigSsd[0] == 1) {/// SSD
                        swp3 = 3;
                        Log.d(TAG, "swp3 is " + swp3);
                    } else if (swp3ConfigEse[0] == 1) {/// ESE
                        swp3 = 4;
                        Log.d(TAG, "swp3 is " + swp3);
                    } else {
                        Log.d(TAG, "wrong value in nfcse.cfg");
                    }
                } catch (Exception e) {
                    Log.d(TAG, "CHIP_SWP3 not hit in current SE list");
                }
            }
        } catch (Exception e) {}

        if (swp3 == 4) {
            ret = true;
        }
        Log.d(TAG, "isSeapiSupportEse is " + ret);
        return ret;
    }

    static public boolean isSupportPKCS15EFDirSelection() {
        int userConfig[] = new int[1];
        boolean ret = false;
        if (isCfgfileExist == false) {
            Log.d(TAG, "vendor/etc/nfcse.cfg doesn't exist");
            return ret;
        }
        try {
            if (sConfigFileParser.get(SEAPI_SUPPORT_PKCS15_EF_DIR_SELECTION, userConfig)) {
                ret = (userConfig[0] == YES) ? true : false;
            }
            if (ret == false) {
                ret = isGSMASupport();
            }
        } catch (Exception e) { }
        return ret;
    }

    static public boolean isSupportARFOnly() {
        int userConfig[] = new int[1];
        boolean ret = false;
        if (isCfgfileExist == false) {
            Log.d(TAG, "vendor/etc/nfcse.cfg doesn't exist");
            return ret;
        }
        try {
            if (sConfigFileParser.get(SEAPI_SUPPORT_ARF_ONLY, userConfig)) {
                ret = (userConfig[0] == YES) ? true : false;
            }
        } catch (Exception e) { }
        return ret;
    }

    static public boolean isSupportPKCS15EFDirSelectionOnly() {
        int userConfig[] = new int[1];
        boolean ret = false;
        if (isCfgfileExist == false) {
            Log.d(TAG, "vendor/etc/nfcse.cfg doesn't exist");
            return ret;
        }
        try {
            if (sConfigFileParser.get(SEAPI_SUPPORT_PKCS15_EF_DIR_SELECTION_ONLY, userConfig)) {
                ret = (userConfig[0] == YES) ? true : false;
            }
        } catch (Exception e) { }
        return ret;
    }

    static public boolean isGSMASupport() {
        boolean ret = false;
        if (mGSMASupport < 0) {
            Log.d(TAG, "Detect for the GSMA Support!");
            if(SystemProperties.get("ro.mtk_nfc_gsma_support", "0").equals("0")) {
                mGSMASupport = 0;
            } else {
                mGSMASupport = 1;
            }
            if (mGSMASupport == 1) {
                Log.d(TAG, "Support GSMA!");
                ret = true;
            } else {
                Log.d(TAG, "Does not support GSMA!");
            }
        } else if (mGSMASupport == 1) {
            Log.d(TAG, "GSMA is supported!");
            ret = true;
        } else if (mGSMASupport == 0) {
            Log.d(TAG, "GSMA does not support!");
        } else {
            Log.d(TAG, "Check GSMA support failure!");
        }

        return ret;
    }

    static public boolean isLogEnhancement() {
        int userConfig[] = new int[1];
        boolean ret = false;
        if (isCfgfileExist == false) {
            Log.d(TAG, "vendor/etc/nfcse.cfg doesn't exist");
            return ret;
        }
        try {
            if (sConfigFileParser.get(SEAPI_LOG_ENHANCEMENT, userConfig)) {
                ret = (userConfig[0] == YES) ? true : false;
            }
        } catch (Exception e) { }
        return ret;
    }

}
