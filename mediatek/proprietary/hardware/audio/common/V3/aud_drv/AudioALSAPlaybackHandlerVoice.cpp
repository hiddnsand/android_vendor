#include "AudioALSAPlaybackHandlerVoice.h"

#include "SpeechDriverFactory.h"
#include "SpeechBGSPlayer.h"

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "AudioALSAPlaybackHandlerVoice"

#define calc_time_diff(x,y) ((x.tv_sec - y.tv_sec )+ (double)( x.tv_nsec - y.tv_nsec ) / (double)1000000000)

namespace android {

AudioALSAPlaybackHandlerVoice::AudioALSAPlaybackHandlerVoice(const stream_attribute_t *stream_attribute_source) :
    AudioALSAPlaybackHandlerBase(stream_attribute_source),
    mBGSPlayer(BGSPlayer::GetInstance()) {
    ALOGD("%s()", __FUNCTION__);
    mPlaybackHandlerType = PLAYBACK_HANDLER_VOICE;
    mBGSPlayBuffer = NULL;

    memset((void *)&mNewtime, 0, sizeof(mNewtime));
    memset((void *)&mOldtime, 0, sizeof(mOldtime));
    mlatency = 0;

    if (mStreamAttributeSource->mAudioOutputFlags & AUDIO_OUTPUT_FLAG_FAST) {
        const uint8_t size_per_channel = audio_bytes_per_sample(mStreamAttributeSource->audio_format);
        const uint8_t size_per_frame = mStreamAttributeSource->num_channels * size_per_channel;
        mlatency = (mStreamAttributeSource->buffer_size + 0.0) / (mStreamAttributeSource->sample_rate
                                                                  * size_per_frame);
        mlatency -= 0.001;  // avoid MD can't pull data on time.

        //ALOGD("%s, mlatency %1.6lf, buffer_size %d, sample_rate %d, size_per_frame %d",
        //      __FUNCTION__, mlatency, mStreamAttributeSource->buffer_size,
        //      mStreamAttributeSource->sample_rate, size_per_frame);
    }
}


AudioALSAPlaybackHandlerVoice::~AudioALSAPlaybackHandlerVoice() {
    ALOGD("%s()", __FUNCTION__);
}

status_t AudioALSAPlaybackHandlerVoice::open() {
    ALOGD("+%s(), audio_mode = %d, u8BGSUlGain = %d, u8BGSDlGain = %d", __FUNCTION__, mStreamAttributeSource->audio_mode, mStreamAttributeSource->u8BGSUlGain, mStreamAttributeSource->u8BGSDlGain);

    ALOGD("%s() mStreamAttributeSource->audio_format =%d", __FUNCTION__, mStreamAttributeSource->audio_format);

    // debug pcm dump
    OpenPCMDump(LOG_TAG);


    // HW attribute config // TODO(Harvey): query this
    mStreamAttributeTarget.audio_format = AUDIO_FORMAT_PCM_16_BIT;
    mStreamAttributeTarget.audio_channel_mask = mStreamAttributeSource->audio_channel_mask; // same as source stream
    mStreamAttributeTarget.num_channels = popcount(mStreamAttributeTarget.audio_channel_mask);
    mStreamAttributeTarget.sample_rate = BGS_TARGET_SAMPLE_RATE; // same as source stream
    ALOGD("mStreamAttributeTarget sample_rate = %d mStreamAttributeSource sample_rate = %d", mStreamAttributeTarget.sample_rate, mStreamAttributeSource->sample_rate);
    mStreamAttributeTarget.u8BGSDlGain = mStreamAttributeSource->u8BGSDlGain;
    mStreamAttributeTarget.u8BGSUlGain = mStreamAttributeSource->u8BGSUlGain;
    mStreamAttributeTarget.buffer_size = BGS_PLAY_BUFFER_LEN;

    // bit conversion
    initBitConverter();


    // open background sound
    AL_LOCK(mBGSPlayer->mBGSMutex);

    if (mStreamAttributeTarget.num_channels > 2) {
        mBGSPlayBuffer = mBGSPlayer->CreateBGSPlayBuffer(
                             mStreamAttributeSource->sample_rate,
                             2,
                             mStreamAttributeTarget.audio_format);

    } else {
        mBGSPlayBuffer = mBGSPlayer->CreateBGSPlayBuffer(
                             mStreamAttributeSource->sample_rate,
                             mStreamAttributeTarget.num_channels,
                             mStreamAttributeTarget.audio_format);
    }
    mBGSPlayer->Open(SpeechDriverFactory::GetInstance()->GetSpeechDriver(), mStreamAttributeTarget.u8BGSUlGain, mStreamAttributeTarget.u8BGSDlGain);

    AL_UNLOCK(mBGSPlayer->mBGSMutex);

    ALOGD("-%s()", __FUNCTION__);
    return NO_ERROR;
}


status_t AudioALSAPlaybackHandlerVoice::close() {
    ALOGD("+%s()", __FUNCTION__);

    // close background sound
    AL_LOCK(mBGSPlayer->mBGSMutex);

    mBGSPlayer->Close();

    mBGSPlayer->DestroyBGSPlayBuffer(mBGSPlayBuffer);

    AL_UNLOCK(mBGSPlayer->mBGSMutex);


    // bit conversion
    deinitBitConverter();


    // debug pcm dump
    ClosePCMDump();


    ALOGD("-%s()", __FUNCTION__);
    return NO_ERROR;
}


status_t AudioALSAPlaybackHandlerVoice::routing(const audio_devices_t output_devices __unused) {
    return INVALID_OPERATION;
}

uint32_t AudioALSAPlaybackHandlerVoice::ChooseTargetSampleRate(uint32_t SampleRate) {
    ALOGD("ChooseTargetSampleRate SampleRate = %d ", SampleRate);
    uint32_t TargetSampleRate = 44100;
    if ((SampleRate % 8000) == 0) { // 8K base
        TargetSampleRate = 48000;
    }
    return TargetSampleRate;
}
int AudioALSAPlaybackHandlerVoice::pause() {
    return -ENODATA;
}

int AudioALSAPlaybackHandlerVoice::resume() {
    return -ENODATA;
}

int AudioALSAPlaybackHandlerVoice::flush() {
    return 0;
}

status_t AudioALSAPlaybackHandlerVoice::setVolume(uint32_t vol __unused) {
    return INVALID_OPERATION;
}

int AudioALSAPlaybackHandlerVoice::drain(audio_drain_type_t type __unused) {
    return 0;
}

ssize_t AudioALSAPlaybackHandlerVoice::write(const void *buffer, size_t bytes) {
    ALOGV("%s()", __FUNCTION__);

    void *newbuffer[96 * 1024] = {0};
    unsigned char *aaa;
    unsigned char *bbb;
    size_t i = 0;
    size_t j = 0;
    int retval = 0;
    // const -> to non const
    void *pBuffer = const_cast<void *>(buffer);
    ASSERT(pBuffer != NULL);

    aaa = (unsigned char *)newbuffer;
    bbb = (unsigned char *)buffer;


    if (mStreamAttributeSource->audio_format == AUDIO_FORMAT_PCM_16_BIT) {
        if (mStreamAttributeTarget.num_channels == 8) {
            for (i = 0 ; j < bytes; i += 4) {
                memcpy(aaa + i, bbb + j, 4);
                j += 16;
            }
            bytes = (bytes >> 2);
        } else if (mStreamAttributeTarget.num_channels == 6) {
            for (i = 0 ; j < bytes; i += 4) {
                memcpy(aaa + i, bbb + j, 4);
                j += 12;
            }
            bytes = (bytes / 3);
        } else {
            memcpy(aaa, bbb, bytes);
        }
    } else {
        if (mStreamAttributeTarget.num_channels == 8) {
            for (i = 0 ; j < bytes; i += 8) {
                memcpy(aaa + i, bbb + j, 8);
                j += 32;
            }
            bytes = (bytes >> 2);
        } else if (mStreamAttributeTarget.num_channels == 6) {
            for (i = 0 ; j < bytes; i += 8) {
                memcpy(aaa + i, bbb + j, 8);
                j += 24;
            }
            bytes = (bytes / 3);
        } else {
            memcpy(aaa, bbb, bytes);
        }

    }

    // bit conversion
    void *pBufferAfterBitConvertion = NULL;
    uint32_t bytesAfterBitConvertion = 0;
    doBitConversion(newbuffer, bytes, &pBufferAfterBitConvertion, &bytesAfterBitConvertion);


    // write data to background sound
    WritePcmDumpData(pBufferAfterBitConvertion, bytesAfterBitConvertion);
    uint32_t u4WrittenBytes = BGSPlayer::GetInstance()->Write(mBGSPlayBuffer, pBufferAfterBitConvertion, bytesAfterBitConvertion);
    if (u4WrittenBytes != bytesAfterBitConvertion) { // TODO: 16/32
        ALOGE("%s(), BGSPlayer::GetInstance()->Write() error, u4WrittenBytes(%u) != bytesAfterBitConvertion(%u)", __FUNCTION__, u4WrittenBytes, bytesAfterBitConvertion);
    }

    if (mStreamAttributeSource->mAudioOutputFlags & AUDIO_OUTPUT_FLAG_FAST) {
        double latencyTime;
        double ns = 0;

        clock_gettime(CLOCK_MONOTONIC, &mNewtime);
        latencyTime = calc_time_diff(mNewtime, mOldtime);
        if (latencyTime < mlatency) {
            ns = mlatency - latencyTime;
            usleep(ns * 1000000);
        }
        clock_gettime(CLOCK_MONOTONIC, &mOldtime);

        //ALOGD("latency %1.3lf, ns %1.3lf", latencyTime, ns);
    }

    return bytes;
}


} // end of namespace android
