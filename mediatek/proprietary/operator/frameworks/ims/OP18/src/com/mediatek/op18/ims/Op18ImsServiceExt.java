/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op18.ims;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.Uri;

import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.android.ims.ImsManager;
import com.mediatek.ims.ext.ImsServiceExt;
import com.mediatek.ims.internal.MtkImsManager;
import com.mediatek.ims.ril.ImsRILAdapter;
import com.mediatek.wfo.WifiOffloadManager;

/**
 * Plugin implementation for ImsService.
 */
public class Op18ImsServiceExt extends ImsServiceExt {

    private static final String TAG = "Op18ImsServiceExt";
    private static final String IMS_SIM = "imssim";
    private Context mContext;
    private static ContentObserver mImsSimChangeObserver = null;
    private static int mEnhanced4GLte = 0;
    private static int mVtSetting = 0;
    private static int mWfcSetting = 0;
    private static int PHONEID_SIM1 = 0;
    private static int PHONEID_SIM2 = 1;
    private boolean mIsImsServiceDown = true;
    private static final int DELAY_TIMER = 10000; // 10 SECONDS
    private static boolean mIsImsSimSwitchDone = true;
    private static final String IMS_INTENT = "com.mediatek.ims.notify_ims_switch_done";
    private static final String EXTRA_PHONE_ID = "phone_id";

    private static final int EVENT_SET_IMS_ENABLED_DONE   = 3;
    private static final int EVENT_SET_IMS_DISABLE_DONE   = 4;
    private static final int EVENT_IMS_DISABLED_URC   = 5;
    private static final int EVENT_IMS_ENABLING_URC   = 10;
    private static final int EVENT_SET_IMS_DISABLE_SIM2_DONE   = 21;
    private static final int EVENT_SET_IMS_ENABLED_SIM2_DONE   = 20;
    private static final int IMS_SWITCHING_COMPLETE = 1;
    private static final int IMS_SWITCHING_IN_PROGRESS = 0;

    /// M: Event for IMS RTP Report @{
    protected static final int EVENT_IMS_RTP_INFO_URC = 19;
    protected static final int EVENT_SET_IMS_RTP_INFO_DONE = 100;
    private static final int WIFI_PDN_ID = 0;
    private static final int DEFAULT_RTP_REPORT_TIMER = 20000;
    /// @}

    private static final String PRIMARY_SIM = "primary_sim";
   /**
    * Constructor
    */
    public Op18ImsServiceExt(Context context) {
        super(context);
        mContext = context;
        setImsSwitchCompleteStatus(IMS_SWITCHING_COMPLETE);
    }

    /**
     * Register IMS SIM observer.
     * @param context hostapp context
     */
    private void registerImsSimObserver(Context context) {
        if (!SystemProperties.get("ro.md_auto_setup_ims").equals("1") &&
                SystemProperties.getInt("ro.mtk_multiple_ims_support", 1) != 1) {
            Log.d(TAG, "registerImsSimObserver");
            if (mImsSimChangeObserver == null) {
                registerImsSimChange(new Handler());
            }
        }
    }

    private void unregisterImsSimObserver(Context context) {
        if (!SystemProperties.get("ro.md_auto_setup_ims").equals("1") &&
                SystemProperties.getInt("ro.mtk_multiple_ims_support", 1) != 1) {
            Log.d(TAG, "unregisterImsSimObserver");
            if (mImsSimChangeObserver != null) {
                mContext.getContentResolver().unregisterContentObserver(mImsSimChangeObserver);
            }
        }
    }

   /**
    * Observes primary SIM changes.
    */
    private void registerImsSimChange(Handler handler) {
        Log.d(TAG, "registerImsSimChange");
        mImsSimChangeObserver = new ContentObserver(handler) {
                @Override
                public void onChange(boolean selfChange) {
                    this.onChange(selfChange, Settings.Global
                            .getUriFor(PRIMARY_SIM));
                }

                @Override
                public void onChange(boolean selfChange, Uri uri) {
                    Log.d(TAG, "onChange primary SIM:");
                    getCurrentSettings();
                    int subId = Settings.Global.getInt(mContext.getContentResolver(),
                            PRIMARY_SIM, -1);
                    Log.d(TAG, "onChange primary SIM: , subId = " + subId);
                    if (subId != -1) {
                        int phoneId = SubscriptionManager.getPhoneId(subId);
                        int oldPhoneId = (phoneId == PHONEID_SIM1) ? PHONEID_SIM2 : PHONEID_SIM1;
                        Log.d(TAG, "new phoneId: " + phoneId
                                + "oldPhoneId:" + oldPhoneId);
                        if (mEnhanced4GLte == 0 && mVtSetting == 0 && mWfcSetting == 0) {
                            Log.d(TAG, "do nothing:");
                        } else {
                            updateImsSettings(oldPhoneId, false);
                            mIsImsServiceDown = false;
                            mIsImsSimSwitchDone = false;
                            setImsSwitchCompleteStatus(IMS_SWITCHING_IN_PROGRESS);
                            mHandler.postDelayed(mRunnable, 10000);
                            Log.d(TAG, "postDelayed starts");
                        }
                    }
                }
            };
        mContext.getContentResolver().registerContentObserver(
                Settings.Global.getUriFor(PRIMARY_SIM),
                false, mImsSimChangeObserver);
    }

    private void handleImsDisableDone(int phoneId) {
        // disable IMS here for old primary SIM
        Log.d(TAG, "handleImsDisableDone for phoneId = " + phoneId);
        int respPhoneId = phoneId; //intent.getIntExtra(EXTRA_PHONE_ID, 0);
        int reqPhoneId = getPrimarySimPhoneId();
        Log.d(TAG, "respPhoneId: " + respPhoneId + ", reqPhoneId:" + reqPhoneId);
        if (respPhoneId != reqPhoneId && !mIsImsServiceDown) {
            Log.d(TAG, "IMS service down for old IMS sim received");
            mIsImsServiceDown = true;
            mIsImsSimSwitchDone = false;
            setImsSwitchCompleteStatus(IMS_SWITCHING_IN_PROGRESS);
            updateImsSettings(reqPhoneId, true);
        }
    }

    private void handleImsEnableDone(int phoneId) {
        Log.d(TAG, "handleImsEnableDone for phoneId = " + phoneId);
        int respPhoneId = phoneId;
        int reqPhoneId = getPrimarySimPhoneId();
        if (respPhoneId == reqPhoneId && !mIsImsSimSwitchDone) {
            Log.d(TAG, "mIsImsSimSwitchDone for phoneId " + reqPhoneId);
            mIsImsSimSwitchDone = true;
            setImsSwitchCompleteStatus(IMS_SWITCHING_COMPLETE);
            sendBroadCast();
        }
    }

    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "Runnable starts");
            if (!mIsImsServiceDown) {
                updateImsSettingWhenNoImsServiceDown();
            }
        }
    };

    private void updateImsSettingWhenNoImsServiceDown() {
        int phoneId = getPrimarySimPhoneId();
        Log.d(TAG, "updateImsSettingWhenNoImsServiceDown");
        mIsImsServiceDown = true;
        mIsImsSimSwitchDone = false;
        setImsSwitchCompleteStatus(IMS_SWITCHING_IN_PROGRESS);
        updateImsSettings(phoneId, true);
    }

    private void updateImsSettings(int phoneId, boolean enable) {
        Log.d(TAG, "updateImsSettings: , phoneId = " + phoneId + ", enable = " + enable);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    Log.d(TAG, "updateImsServiceConfig:" + ", enable = " + enable);
                    ImsManager.updateImsServiceConfig(mContext, phoneId, true);
               } catch (Exception e) {
                    Log.d(TAG, "updateImsSettings Exception: " + e);
               }
            }
        };
        Log.d(TAG, "start thread");
        t.start();
    }

    private int getPrimarySimPhoneId() {
        int phoneId = 0;
        int subId = Settings.Global.getInt(mContext.getContentResolver(),
                PRIMARY_SIM, -1);
        Log.d(TAG, "getPrimarySimPhoneId for: , subId = " + subId);
        if (subId != -1) {
            phoneId = SubscriptionManager.getPhoneId(subId);
        }
        Log.d(TAG, "getPrimarySimPhoneId: , phoneId = " + phoneId);
        return phoneId;
    }

    private void getCurrentSettings() {
        mEnhanced4GLte = android.provider.Settings.Global.getInt(mContext.getContentResolver(),
                        android.provider.Settings.Global.ENHANCED_4G_MODE_ENABLED, 0);
        mVtSetting = android.provider.Settings.Global.getInt(mContext.getContentResolver(),
                        android.provider.Settings.Global.VT_IMS_ENABLED, 0);
        mWfcSetting = android.provider.Settings.Global.getInt(mContext.getContentResolver(),
                        android.provider.Settings.Global.WFC_IMS_ENABLED, 0);
        Log.d(TAG, "mEnhanced4GLte :" + mEnhanced4GLte + ", mVtSetting: " + mVtSetting +
                ", mWfcSetting: " + mWfcSetting);
    }

    private void sendBroadCast() {
        Log.d(TAG, "sendBroadCast IMS_INTENT");
        Intent i = new Intent();
        i.setAction(IMS_INTENT);
        mContext.sendBroadcast(i);
    }

    /**
     * Api to set IMS SIM switching is complete or not.
     * @return boolean mIsImsSimSwitchDone
     */
    private void setImsSwitchCompleteStatus(int status) {
        Log.d(TAG, "getImsSwitchCompleteStatus = " + status);
        android.provider.Settings.System.putInt(
                mContext.getContentResolver(), IMS_SIM, status);
    }

    /**
     * Notify about IMS service events.
     * @param phoneId phoneId
     * @param context hostContext
     * @param msg message received at ImsService
     */
    @Override
    public void notifyImsServiceEvent(int phoneId, Context context, Message msg) {
        Log.d(TAG, "ImsService event: " + msg.what);
        boolean isMultiImsSupported =
                (!SystemProperties.get("ro.md_auto_setup_ims").equals("1")) &&
                (SystemProperties.getInt("ro.mtk_multiple_ims_support", 1) != 1);
        AsyncResult ar;

        switch(msg.what) {
            case EVENT_SET_IMS_DISABLE_DONE:
                ar = (AsyncResult) msg.obj;
                if (isMultiImsSupported && ar.exception == null) {
                    Log.d(TAG, "EVENT_SET_IMS_DISABLE_DONE");
                    handleImsDisableDone(PHONEID_SIM1);
                }
                break;
            case EVENT_SET_IMS_DISABLE_SIM2_DONE:
                ar = (AsyncResult) msg.obj;
                if (isMultiImsSupported && ar.exception == null) {
                    Log.d(TAG, "EVENT_SET_IMS_DISABLE_SIM2_DONE");
                    handleImsDisableDone(PHONEID_SIM2);
                }
                break;
            case EVENT_SET_IMS_ENABLED_DONE:
                ar = (AsyncResult) msg.obj;
                if (isMultiImsSupported && ar.exception == null) {
                    Log.d(TAG, "EVENT_SET_IMS_ENABLED_DONE");
                    handleImsEnableDone(PHONEID_SIM1);
                }
                break;
            case EVENT_SET_IMS_ENABLED_SIM2_DONE:
                ar = (AsyncResult) msg.obj;
                if (isMultiImsSupported && ar.exception == null) {
                    Log.d(TAG, "EVENT_SET_IMS_ENABLED_SIM2_DONE");
                    handleImsEnableDone(PHONEID_SIM2);
                }
                break;
            case EVENT_IMS_RTP_INFO_URC:
                // +EIMSRTPRPT=<default_ebi>, <network_id>, <timer>,
                //    <send pkt lost>, <recv pkt lost>
                ar = (AsyncResult) msg.obj;
                Intent intent;
                String[] rtpInfo = (String[]) ar.result;
                Log.d(TAG, "receive EVENT_IMS_RTP_INFO_URC, pdn id:" +
                        Integer.parseInt(rtpInfo[0]));
                if (WIFI_PDN_ID == Integer.parseInt(rtpInfo[0])) {
                    intent = new Intent(MtkImsManager.ACTION_IMS_RTP_INFO);
                   intent.putExtra(MtkImsManager.EXTRA_RTP_PDN_ID,
                           Integer.parseInt(rtpInfo[0]));
                   intent.putExtra(MtkImsManager.EXTRA_RTP_NETWORK_ID,
                           Integer.parseInt(rtpInfo[1]));
                   intent.putExtra(MtkImsManager.EXTRA_RTP_TIMER,
                           Integer.parseInt(rtpInfo[2]));
                   intent.putExtra(MtkImsManager.EXTRA_RTP_SEND_PKT_LOST,
                           Integer.parseInt(rtpInfo[3]));
                   intent.putExtra(MtkImsManager.EXTRA_RTP_RECV_PKT_LOST,
                           Integer.parseInt(rtpInfo[4]));
                    mContext.sendBroadcast(intent);
                }
                break;
           case EVENT_SET_IMS_RTP_INFO_DONE:
               // Only log for tracking
               Log.d(TAG, "receive EVENT_SET_IMS_RTP_INFO_DONE");
               break;
            case EVENT_IMS_ENABLING_URC:
                registerImsSimObserver(mContext);
                break;
            case EVENT_IMS_DISABLED_URC:
                unregisterImsSimObserver(mContext);
                break;
           default:
               break;
        }
    }

    /**
     * Notify registrationStateChange.
     *
     * @param ranType ims RAN type
     * @param handler ImsService main handler
     * @param imsRILAdapter ImsRILAdapter instance
     * @return
     */
    @Override
    public void notifyRegistrationStateChange(int ranType, Handler handler,
            Object imsRILAdapter) {
        if (ranType == WifiOffloadManager.RAN_TYPE_WIFI) {
            // set IMS RTP info only for RJIL WFC
            setImsRtpInfo(handler, (ImsRILAdapter) imsRILAdapter);
        }
    }

    /**
     * Notify MD to report RTP info. Currently this is for RJIL(OP18) and report
     * WFC RTP status only.
     *
     * @hide
     */
    private void setImsRtpInfo(Handler handler, ImsRILAdapter imsRILAdapter) {
        Log.d(TAG, "setImsRtpInfo");
        int networkId = -1;

        ConnectivityManager connectivityManager = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        Network[] allNetworks = connectivityManager.getAllNetworks();
        for (Network network : allNetworks) {
            Log.d(TAG, "Checking network:" + network);
            NetworkCapabilities nc = connectivityManager.getNetworkCapabilities(network);
            if (nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_IMS)) {
                Log.d(TAG, "Found IMS capability, netId=" + network.netId);
                networkId = network.netId;
                break;
            }
        }
        if (networkId < 0) {
            Log.d(TAG, "IMS capability not found, skip");
            return;
        }

        imsRILAdapter.setImsRtpInfo(WIFI_PDN_ID, networkId, DEFAULT_RTP_REPORT_TIMER,
                handler.obtainMessage(EVENT_SET_IMS_RTP_INFO_DONE));
    }
}