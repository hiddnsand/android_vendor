/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op09alib;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.ContentObserver;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.internal.telephony.MtkGsmCdmaPhone;
import com.mediatek.internal.telephony.MtkIccCardConstants.CardType;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.telephony.MtkTelephonyManagerEx;

/**
 * CT feature for add swith for LET data enable or not.
 */
public class UseLteDataSettings extends Activity {

    private static final String TAG = "UseLteDataSettings";
    private static final String INTENT_ACTION_CARD_TYPE =
            TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED;
    static final String INTENT_ACTION_DISMISS_DIALOG =
        "com.mediatek.settings.plugin.DISMISS_DIALOG";
    private static final String MODE_STATUS = "mode_status";
    private static final String LTETDD_CDMA = "ltetdd_cdma";
    private static final int PROGRESS_DIALOG = 1000;

    private IntentFilter mIntentFilter;
    private Switch mSwitchBar;
    private boolean mSwitching;
    private boolean mSwitchBarChecked;
    private boolean mIsActiveWindow;
    private boolean mIsDialogShowing;
    private boolean mIsLteCardType;
    private AlertDialog mDialog = null;
    static boolean sSet4GMode = false;
    private boolean mBound = false;
    private ChangeRatModeService mService = null;
    private MtkTelephonyManagerEx mTelephonyManagerEx = null;
    private static boolean mIsUseSelect = true;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i(TAG, "on receive broadcast action = " + action);
            if (action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
                Log.i(TAG, "action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED) udpateSwitch");
                updateSwitch();
            } else if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(action)) {
                int slotId = intent.getIntExtra(PhoneConstants.SLOT_KEY, 0);
                if (slotId == PhoneConstants.SIM_ID_1) {
                    Log.i(TAG, "TelephonyIntents.ACTION_SIM_STATE_CHANGED udpateSwitch");
                    updateSwitch();
                }
            } else if (ChangeNetworkType.ACTION_RAT_CHANGED.equals(action)) {
                mSwitching = false;
                updateSwitch();
                hideProgressDlg();
            } else if (TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED.equals(intent.getAction())) {
                CardType cardType = null;
                cardType = mTelephonyManagerEx.getCdmaCardType(PhoneConstants.SIM_ID_1);

                if (cardType.equals(CardType.CT_4G_UICC_CARD)) {
                    mIsLteCardType = true;
                } else {
                    mIsLteCardType = false;
                }
                Log.i(TAG, "intent cardType = " + cardType.toString()
                        + ", mIsLteCardType " + mIsLteCardType);
                updateSwitch();
            } else if (action.equals(INTENT_ACTION_DISMISS_DIALOG)) {
                Log.i(TAG, "receive INTENT_ACTION_DISMISS_DIALOG.");
                if (mDialog != null && mDialog.isShowing()) {
                    Log.i(TAG, "data service is done, dismiss dialog.");
                    mDialog.dismiss();
                }
            }
        }
    };

    private ContentObserver mDataConnectionObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            Log.d(TAG, "onChange selfChange=" + selfChange);
            if (!selfChange) {
                updateSwitch();
            }
        }
    };

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            ChangeRatModeService.SetRatMode setRatMode = (ChangeRatModeService.SetRatMode) binder;
            mService = setRatMode.getService();
            mBound = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName className) {
            mBound = false;
        }
    };

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.use_lte_data_settings);
        initialize();
        registerReceiver(mReceiver, mIntentFilter);
        setTaskDescription(new ActivityManager.TaskDescription(
                getResources().getString(R.string.enable_lte_data),
                BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_lte_data_set)
                ));
    }
    @Override
    public void onStart() {
        super.onStart();
        Intent intent = new Intent(this, ChangeRatModeService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        unregisterReceiver(mReceiver);
        this.getContentResolver().unregisterContentObserver(mDataConnectionObserver);
        super.onDestroy();
    }

    private void initialize() {
        mTelephonyManagerEx = MtkTelephonyManagerEx.getDefault();
        LayoutInflater inflater = (LayoutInflater) getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        mSwitchBar = new Switch(inflater.getContext());
        int padding = getResources().getDimensionPixelSize(R.dimen.action_bar_switch_padding);
        mSwitchBar.setPadding(0, 0, padding, 0);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM);
        getActionBar().setCustomView(mSwitchBar,
              new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                      ActionBar.LayoutParams.WRAP_CONTENT,
                      Gravity.CENTER_VERTICAL | Gravity.END));
        mIntentFilter = new IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        mIntentFilter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        mIntentFilter.addAction(ChangeNetworkType.ACTION_RAT_CHANGED);
        mIntentFilter.addAction(INTENT_ACTION_CARD_TYPE);
        mIntentFilter.addAction(INTENT_ACTION_DISMISS_DIALOG);
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
        this.getContentResolver().registerContentObserver(
            Settings.Global.getUriFor(Settings.Global.PREFERRED_NETWORK_MODE + subId),
                true, mDataConnectionObserver);
        int pattern = Settings.Global.getInt(this.getContentResolver(),
                Settings.Global.PREFERRED_NETWORK_MODE + subId,
                Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
        if (pattern == Phone.NT_MODE_CDMA || pattern == Phone.NT_MODE_GLOBAL) {
            mSwitchBarChecked = false;
        } else {
            mSwitchBarChecked = true;
        }
        if (sSet4GMode) {
            mSwitchBarChecked = true;
        }
        mSwitchBar.setChecked(mSwitchBarChecked);
        mSwitchBar.setOnCheckedChangeListener(mSwitchBarListener);
        createWarningDialog();
    }
    private void createWarningDialog() {
        mDialog = new AlertDialog.Builder(this)
            .setMessage(R.string.whether_enable_4G)
            .setPositiveButton(R.string.lte_only_dialog_button_yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        Log.i(TAG, "User select yes!");
                        sSet4GMode = false;
                        switchSvlteRatMode(true);
                    }
                 })
            .setNegativeButton(R.string.lte_only_dialog_button_no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        Log.i(TAG, "User select no!");
                        openSwitcher();
                        sSet4GMode = true;
                    }
                 })
            .setOnCancelListener(new DialogInterface.OnCancelListener() {
                 public void onCancel(DialogInterface dialog) {
                     Log.i(TAG, "User didn't select, dialog cancel!");
                     sSet4GMode = true;
                 }
             })
            .create();

    }
    private void showWarningDialog() {
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
        int pattern = Settings.Global.getInt(this.getContentResolver(),
            Settings.Global.PREFERRED_NETWORK_MODE + subId,
            Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
        Log.d(TAG, "showWarningDialog current mode: " + pattern);
        if (mDialog != null && !mDialog.isShowing()
                && pattern != Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA
                && mIsUseSelect) {
            Log.d(TAG, "showWarningDialog show");
            sSet4GMode = true;
            mDialog.show();
        }
        Log.d(TAG, "showWarningDialog mIsUseSelect = true");
        mIsUseSelect = true;
    }

    private void openSwitcher() {
        boolean simInserted = Utils.isSIMInserted(PhoneConstants.SIM_ID_1);
        boolean airPlaneMode = Utils.isAirPlaneMode(this);
        boolean radioOn = Utils.isTargetSimRadioOn(PhoneConstants.SIM_ID_1);
        boolean callStateIdle = Utils.isCallStateIDLE(this);
        boolean simStateIsReady
            = Utils.isSimStateReady(UseLteDataSettings.this, PhoneConstants.SIM_ID_1);
        if ((simInserted && !airPlaneMode && radioOn
                && callStateIdle && !mSwitching
                && simStateIsReady && mIsLteCardType)
                || (useCtTestcard()))  {
            if (!mSwitchBar.isChecked()) {
                Log.i(TAG, "if user select no, open the switcher!");
                mSwitchBar.setChecked(true);
            }
            mSwitchBar.setEnabled(true);
        } else {
            mSwitchBar.setEnabled(false);
        }
    }
    private OnCheckedChangeListener mSwitchBarListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                boolean isChecked) {
            TelephonyManager telephony = (TelephonyManager) UseLteDataSettings.this
            .getSystemService(Context.TELEPHONY_SERVICE);
            int dataFlag = TelephonyManager.DATA_ACTIVITY_NONE;
            dataFlag = telephony.getDataActivity();
            boolean isCTdataEnable = telephony.getDataEnabled(
                   MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1));

            Log.i(TAG, "mSwitchBarListener isChecked = "
                    + isChecked + " dataFlag = " + dataFlag
                    + " isCTdataEnable " + isCTdataEnable);
            if (isChecked && isCTdataEnable &&
                    (dataFlag == TelephonyManager.DATA_ACTIVITY_IN
                    || dataFlag == TelephonyManager.DATA_ACTIVITY_OUT
                    || dataFlag == TelephonyManager.DATA_ACTIVITY_INOUT)) {
                Log.i(TAG, "mSwitchBarListener isChecked = "
                        + isChecked + " may be show dialog");
                showWarningDialog();
            } else {
                mIsUseSelect = true;
                Log.i(TAG, "mSwitchBarListener isChecked = "
                        + isChecked + "mIsUseSelect = true");
                if (!isChecked) {
                    int subId = MtkSubscriptionManager
                        .getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
                    int lastMode = Settings.Global.
                                        getInt(UseLteDataSettings.this.getContentResolver(),
                                            Settings.Global.PREFERRED_NETWORK_MODE + subId,
                                            Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
                    Log.d(TAG, "mSwitchBarListener isChecked = "+ isChecked
                            + " OnClickListener lastMode = " + lastMode);
                    if (lastMode == Phone.NT_MODE_CDMA || lastMode == Phone.NT_MODE_GLOBAL) {
                        Log.d(TAG, "mSwitchBarListener isChecked = "+ isChecked
                                + " OnClickListener close switch in the 3G mode!");
                        sSet4GMode = false;
                        return;
                    }
                }
                Log.i(TAG, "mSwitchBarListener isChecked = "
                        + isChecked + " switchSvlteRatMode");
                switchSvlteRatMode(isChecked);
                sSet4GMode = false;
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        mIsActiveWindow = true;
        mSwitching = false;
        updateSwitch();
        TelephonyManager telephonyManager =
            (TelephonyManager) UseLteDataSettings.this.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(mPhoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE | PhoneStateListener.LISTEN_SERVICE_STATE);
    }

    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            Log.d(TAG, "onCallStateChanged, new state = " + state);
            updateSwitch();
        }

        @Override
        public void onServiceStateChanged(ServiceState serviceState) {
            Log.d(TAG, "onServiceStateChanged, new state = " + serviceState);
            updateSwitch();
        }
    };

    private void updateSwitch() {
        boolean simInserted = Utils.isSIMInserted(PhoneConstants.SIM_ID_1);
        boolean airPlaneMode = Utils.isAirPlaneMode(this);
        boolean radioOn = Utils.isTargetSimRadioOn(PhoneConstants.SIM_ID_1);
        boolean callStateIdle = Utils.isCallStateIDLE(this);
        boolean simStateIsReady =
            Utils.isSimStateReady(UseLteDataSettings.this, PhoneConstants.SIM_ID_1);
        if ((simInserted && !airPlaneMode && radioOn
             && callStateIdle && !mSwitching
             && simStateIsReady && mIsLteCardType)
             || (useCtTestcard())) {
            mSwitchBar.setEnabled(true);
        } else {
            mSwitchBar.setEnabled(false);
        }
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(PhoneConstants.SIM_ID_1);
        int pattern = Settings.Global.getInt(this.getContentResolver(),
                            Settings.Global.PREFERRED_NETWORK_MODE + subId,
                            Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA);
        Log.d(TAG, "updateSwitch pattern = " + pattern);
        if (pattern == Phone.NT_MODE_LTE_CDMA_AND_EVDO
                || pattern == Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA
                || pattern == MtkGsmCdmaPhone.NT_MODE_LTE_TDD_ONLY
        ) {
            mSwitchBarChecked = true;
        } else {
            mSwitchBarChecked = false;
        }
        if (sSet4GMode) {
            mSwitchBarChecked = true;
        }
        Log.d(TAG, "updateSwitch() simInserted=" + simInserted
                + ", airPlaneMode=" + airPlaneMode
                + ", radioOn=" + radioOn
                + ", callStateIdle=" + callStateIdle
                + ", mSwitching=" + mSwitching
                + ", mSwitchBarChecked =" + mSwitchBarChecked
                + ", simStateIsReady = " + simStateIsReady
                + ", mIsLteCardType = " + mIsLteCardType
                + ", isChecked = " + mSwitchBar.isChecked()
                + ", sub id = " + subId);
        if (!simInserted || subId < 0) {
            return;
        }
        if ((mSwitchBar.isChecked() && mSwitchBarChecked == false)
            || (!mSwitchBar.isChecked() && mSwitchBarChecked == true)) {
            mIsUseSelect = false;
            Log.i(TAG, "update the switcher mIsUseSelect = false");
            mSwitchBar.setChecked(mSwitchBarChecked);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mIsActiveWindow = false;
        TelephonyManager.getDefault().listen(
                mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
    }

    private void switchSvlteRatMode(boolean isChecked) {
        if (mIsActiveWindow && mService != null) {
            try {
                showProgressDlg();
                mService.saveData(isChecked);
                mService.switchSvlte(isChecked);
            } catch (RemoteException e) {
                hideProgressDlg();
                Log.i(TAG, "telephony.switchSvlteRatMode() has exception");
            }
            mSwitching = true;
            updateSwitch();
        }
    }

    private void showProgressDlg() {
        Log.d(TAG, "showProgressDlg() with dialogMsg");
        mIsDialogShowing = true;
        showDialog(PROGRESS_DIALOG);
    }

    @Override
    public Dialog onCreateDialog(int id) {
        switch (id) {
        case PROGRESS_DIALOG:
            ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage(getResources().getString(
                    R.string.gemini_data_connection_progress_message));
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            return dialog;
        default:
            return null;
        }
    }

    private void hideProgressDlg() {
        Log.d(TAG, "hideProgressDlg()");
        if (mIsDialogShowing) {
            dismissDialog(PROGRESS_DIALOG);
            mIsDialogShowing = false;
        }
    }

    private boolean useCtTestcard() {
        boolean testCard = SystemProperties.get("persist.sys.forcttestcard").equals("1");
        Log.i(TAG, "useCtTestcard, testCard = " + testCard);
        return testCard;
    }

}
