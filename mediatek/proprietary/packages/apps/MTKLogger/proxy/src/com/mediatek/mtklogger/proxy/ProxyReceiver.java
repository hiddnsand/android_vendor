package com.mediatek.mtklogger.proxy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mediatek.mtklogger.proxy.utils.Utils;

/**
 * @author MTK81255
 *
 */
public class ProxyReceiver extends BroadcastReceiver {

    private static final String TAG = Utils.TAG + "/ProxyReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Utils.logi(TAG, " -->onReceive(), action=" + action);
        if ("android.intent.action.BOOT_COMPLETED".equalsIgnoreCase(action)) {
            if (Utils.BUILD_TYPE.equals("eng")) {
                intent.setClass(context, ProxyService.class);
                context.startService(intent);
            }
        } else if (Utils.PROXY_ACTION_RECEIVER.equalsIgnoreCase(action)) {
            intent.setClass(context, ProxyService.class);
            context.startService(intent);
        }
    }
}
