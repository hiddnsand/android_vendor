
LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := libifcutils_mtk
LOCAL_SRC_FILES := ifc_utils.c
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_TAGS := optional
LOCAL_MULTILIB := both

LOCAL_C_INCLUDES += \
$(TOP)/vendor/mediatek/proprietary/system/netdagent/include \

LOCAL_SHARED_LIBRARIES := \
        libcutils \
        liblog

include $(MTK_SHARED_LIBRARY)