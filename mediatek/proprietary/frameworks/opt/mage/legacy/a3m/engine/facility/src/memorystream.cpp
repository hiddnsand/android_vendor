/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Memory Stream Implementation
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <cstring> /* for memcpy */

#include <a3m/memorystream.h> /* this file's header */
#include <a3m/log.h> /* pssLogError */

namespace a3m
{

  /******************
   * MemoryStream *
   ******************/

  MemoryStream::MemoryStream(void const* data, A3M_INT32 size) :
    m_data(static_cast<A3M_CHAR8 const*>(data)),
    m_size(size),
    m_index(0)
  {
  }

  MemoryStream::Ptr MemoryStream::clone()
  {
    return MemoryStream::Ptr(new MemoryStream(m_data, m_size));
  }

  A3M_BOOL MemoryStream::valid()
  {
    return A3M_TRUE;
  }

  A3M_BOOL MemoryStream::eof()
  {
    return (m_index >= m_size);
  }

  A3M_INT32 MemoryStream::size()
  {
    return m_size;
  }

  A3M_INT32 MemoryStream::seek(A3M_UINT32 offset)
  {
    m_index = std::min(static_cast<A3M_INT32>(offset), size());
    return m_index;
  }

  A3M_INT32 MemoryStream::tell()
  {
    return m_index;
  }

  A3M_INT32 MemoryStream::read(void* dest, A3M_UINT32 byteLength)
  {
    if (!(m_data && dest))
    {
      return 0;
    }

    A3M_INT32 actualByteLength
      = std::min(static_cast<A3M_INT32>(byteLength), size() - m_index);

    if (actualByteLength > 0)
    {
      memcpy(dest, m_data + m_index, actualByteLength);
      m_index += actualByteLength;
    }

    return actualByteLength;
  }

  A3M_INT32 MemoryStream::write(const void* source, A3M_UINT32 byteLength)
  {
    // Stream is not writable
    return 0;
  }

  /************************
   * MemoryStreamSource *
   ************************/

  MemoryStreamSource::MemoryStreamSource() :
    m_name("MemoryStreamSource")
  {
  }

  void MemoryStreamSource::add(
    A3M_CHAR8 const* name,
    MemoryStream::Ptr const& stream)
  {
    if (exists(name))
    {
      A3M_LOG_ERROR("MemoryStream \"%s\" already added to source", name);
      return;
    }

    m_streams[name] = stream;
  }

  A3M_BOOL MemoryStreamSource::exists(const A3M_CHAR8* stream)
  {
    return m_streams.find(stream) != m_streams.end();
  }

  a3m::Stream::Ptr MemoryStreamSource::open(
    const A3M_CHAR8* stream,
    A3M_BOOL writable)
  {
    MemoryStream::Ptr streamPtr;

    if (writable)
    {
      A3M_LOG_ERROR("MemoryStream streams cannot be writable: %s", stream);
    }
    else
    {
      MemoryStreamMap::iterator it = m_streams.find(stream);

      if (it != m_streams.end())
      {
        // Clone stream, as we want to return unique stream objects
        streamPtr = it->second->clone();
      }
    }

    return streamPtr;
  }

  A3M_CHAR8 const* MemoryStreamSource::getName() const
  {
    return m_name.c_str();
  }

}; /* namespace a3m */

