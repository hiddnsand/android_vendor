/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.wfo.impl;

import java.lang.reflect.Field;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.RemoteCallbackList;
import android.os.SystemProperties;
import android.telephony.Rlog;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;

import android.text.TextUtils;


import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.internal.telephony.PhoneConstants;
import com.mediatek.internal.telephony.RadioManager;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.MtkTelephonyIntents;
import com.mediatek.telephony.MtkTelephonyManagerEx;
import com.mediatek.wfo.DisconnectCause;
import com.mediatek.wfo.WifiOffloadManager;
import com.mediatek.wfo.IWifiOffloadService;
import com.mediatek.wfo.IWifiOffloadListener;

import com.mediatek.wfo.op.OpWosCustomizationUtils;
import com.mediatek.wfo.op.OpWosCustomizationFactoryBase;
import com.mediatek.wfo.op.IWosExt;

/**
 * Top-level Application class for the WifiOffload app.
 */
public class WfcHandler {
    private static final String TAG = "WfcHandler";
    private static final int MAX_VALID_SIM_COUNT = 4;
    private static final String NOTIFICATION_TAG = "wifi_calling";
    private static final int NOTIFICATION_ID = 1;

    private static final String AID_SETTING_URI_STR = "";
    private static final Uri AID_SETTING_URI = Settings.Global.getUriFor(AID_SETTING_URI_STR);

    private final static String RADIO_MANAGER_POWER_ON_MODEM =
                                 "mediatek.intent.action.WFC_POWER_ON_MODEM";
    private final static String EXTRA_POWER_ON_MODEM_KEY = "mediatek:POWER_ON_MODEM";

    private Context mContext;
    private int mSimCount;
    private boolean mIsWifiEnabled;
    private boolean mIsWifiL2Connected = false;
    private boolean[] mIsWfcSettingsOn;

    // Register for Android settings constant
    private String[] mWfcSettingKeys;

    private WifiPdnHandler mWifiPdnHandler;
    private DisconnectCause mDisconnectCause[];
    private String mWfcEccAid;

    private RemoteCallbackList<IWifiOffloadListener> mListeners =
        new RemoteCallbackList<IWifiOffloadListener>();

    // for operater add-on
    private OpWosCustomizationFactoryBase  mWosFactory = null;
    private IWosExt mWosExt = null;

    // Message codes. See mHandler below.
    private static final int BASE = 2000;
    private static final int EVENT_HANDLE_MODEM_POWER               = BASE + 0;
    private static final int EVENT_SET_WFC_EMERGENCY_ADDRESS_ID     = BASE + 1;
    private static final int EVENT_NOTIFY_WIFI_NO_INTERNET          = BASE + 2;

    public static final int EVENT_ON_PDN_HANDOVER                   = BASE + 100;
    public static final int EVENT_ON_PDN_ERROR                      = BASE + 101;
    public static final int EVENT_ON_ROVE_OUT                       = BASE + 102;
    public static final int EVENT_ON_WFC_PDN_STATE_CHANGED          = BASE + 103;

    private static final int RESPONSE_SET_WFC_EMERGENCY_ADDRESS_ID  = BASE + 200;

    private static int WIFI_NO_INTERNET_ERROR_CODE = 1081;
    private static int WIFI_NO_INTERNET_TIMEOUT = 8000;
    private static final int RETRY_TIMEOUT = 3000;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Rlog.d(TAG, "handleMessage: " + messageToString(msg));
            AsyncResult ar;
            switch (msg.what) {
                /* handle internal event start */
                case EVENT_HANDLE_MODEM_POWER:
                    handleModemPower();
                    break;
                case EVENT_SET_WFC_EMERGENCY_ADDRESS_ID:
                    setEmergencyAddressId();
                    break;
                case EVENT_NOTIFY_WIFI_NO_INTERNET:
                    checkIfShowNoInternetError(true);
                    break;
                /* handle internal event end */
                /* handle UNSOL start */
                case EVENT_ON_PDN_HANDOVER:
                    onPdnHandover(msg);
                    break;
                case EVENT_ON_PDN_ERROR:
                    onWfcPdnError(msg);
                    break;
                case EVENT_ON_ROVE_OUT:
                    onWifiRoveout(msg);
                    break;
                case EVENT_ON_WFC_PDN_STATE_CHANGED:
                    onWfcPdnStateChanged(msg);
                    break;
                /* handle UNSOL end */
                case RESPONSE_SET_WFC_EMERGENCY_ADDRESS_ID:
                    ar = (AsyncResult) msg.obj;
                    handleRetry(EVENT_SET_WFC_EMERGENCY_ADDRESS_ID, ar);
                    break;
                default:
                    break;
            }
        }

        private void handleRetry(int msgId, AsyncResult result) {
            if (mHandler.hasMessages(msgId)) {
                return;
            }

            //if (result != null && result.exception != null) {
                sendEmptyMessageDelayed(msgId, RETRY_TIMEOUT);
            //}
        }

        private String messageToString(Message msg) {
            switch (msg.what) {

                case EVENT_HANDLE_MODEM_POWER:
                    return "EVENT_HANDLE_MODEM_POWER";
                case EVENT_SET_WFC_EMERGENCY_ADDRESS_ID:
                    return "EVENT_SET_WFC_EMERGENCY_ADDRESS_ID";
                case EVENT_NOTIFY_WIFI_NO_INTERNET:
                    return "EVENT_NOTIFY_WIFI_NO_INTERNET";
                case EVENT_ON_PDN_HANDOVER:
                    return "EVENT_ON_PDN_HANDOVER";
                case EVENT_ON_PDN_ERROR:
                    return "EVENT_ON_PDN_ERROR";
                case EVENT_ON_ROVE_OUT:
                    return "EVENT_ON_ROVE_OUT";
                case EVENT_ON_WFC_PDN_STATE_CHANGED:
                    return "EVENT_ON_WFC_PDN_STATE_CHANGED";
                case RESPONSE_SET_WFC_EMERGENCY_ADDRESS_ID:
                    return "RESPONSE_SET_WFC_EMERGENCY_ADDRESS_ID";

                default:
                    return "UNKNOWN";
            }
        }
    };

    public Handler getHandler() {
        return mHandler;
    }

    private IWifiOffloadService mWfoService = new IWifiOffloadService.Stub() {
        @Override
        public void registerForHandoverEvent(IWifiOffloadListener listener) {
            Rlog.d(TAG, "registerForHandoverEvent for " + listener.asBinder());
            mListeners.register(listener);
        }

        @Override
        public void unregisterForHandoverEvent(IWifiOffloadListener listener) {
            Rlog.d(TAG, "unregisterForHandoverEvent for " + listener.asBinder());
            mListeners.unregister(listener);
        }

        @Override
        public int getRatType(int simIdx) {
            Rlog.d(TAG, "getRatType() not supported");
            return 0;
        }

        @Override
        public DisconnectCause getDisconnectCause(int simIdx) {
            return getDisconnectCause(simIdx);
        }

        @Override
        public void setEpdgFqdn(int simIdx, String fqdn, boolean wfcEnabled) {
            Rlog.d(TAG, "setEpdgFqdn() not supported");
        }

        @Override
        public void updateCallState(int simIdx, int callId, int callType, int callState) {
            Rlog.d(TAG, "updateCallState() not supported");
        }

        @Override
        public boolean isWifiConnected() {
            return mWifiPdnHandler.isWifiConnected();
        }

        @Override
        public void updateRadioState(int simIdx, int radioState) {
            Rlog.d(TAG, "updateRadioState() not supported");
        }

        @Override
        public boolean setMccMncAllowList(String[] allowList) {
            Rlog.d(TAG, "setMccMncAllowList() not supported");
            return false;
        }

        @Override
        public String[] getMccMncAllowList(int mode) {
            Rlog.d(TAG, "getMccMncAllowList() not supported");
            return null;
        }
    };

    public WfcHandler(Context context, WifiPdnHandler wifiHandler, int simCount) {
        mContext = context;
        mWifiPdnHandler = wifiHandler;

        // for operator add-on
        mWosFactory = OpWosCustomizationUtils.getOpFactory(mContext);
        mWosExt = mWosFactory.makeWosExt(mContext);

        mSimCount = (simCount <= MAX_VALID_SIM_COUNT) ? simCount : MAX_VALID_SIM_COUNT;

        mDisconnectCause = new DisconnectCause[mSimCount];

        registerForBroadcast();

        // register for WFC settings
        mWfcSettingKeys = wfcSettingKeysInitialization();
        registerForWfcSettingsObserver();
        registerForWfcAidObserver();
        mIsWfcSettingsOn = new boolean[mSimCount];

        // handle modem if the Mwis restart
        updateWfcUISetting();
        WifiManager wifiMngr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        mIsWifiEnabled = wifiMngr.isWifiEnabled();
        mHandler.sendMessage(mHandler.obtainMessage(EVENT_HANDLE_MODEM_POWER));
    }

    private DisconnectCause getDisconnectCause(int simIdx) {
        if (checkInvalidSimIdx(simIdx, "getDisconnectCause()")) {
            return null;
        }
        return mDisconnectCause[simIdx];
    }

    private void registerForWfcSettingsObserver() {
        if (mContext == null) {
            return;
        }
        for (int i = 0; i < mSimCount; ++i) {
            String key = mWfcSettingKeys[i];
            mContext.getContentResolver().registerContentObserver(
                    Settings.Global.getUriFor(key), false, mContentObserver);
            Rlog.d(TAG, "registerForWfcSettingsObserver(): " + key);
        }
    }

    private void registerForWfcAidObserver() {
        if (mContext == null) {
            return;
        }
        mContext.getContentResolver().registerContentObserver(
                AID_SETTING_URI, false, mContentObserver);
        Rlog.d(TAG, "registerForWfcAidObserver()");
    }

    private void registerForBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(CarrierConfigManager.ACTION_CARRIER_CONFIG_CHANGED);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);

        mContext.registerReceiver(mReceiver, filter);
    }

    private int[] getIntArrayFromMsg(Message msg) {
        AsyncResult ar = (AsyncResult) msg.obj;
        Intent intent = (Intent) ar.result;
        return getIntArrayFromIntent(intent);
    }

    private int[] getIntArrayFromIntent(Intent intent) {
        String extra = null;
        switch(intent.getAction()) {
            case MtkTelephonyIntents.ACTION_WFC_PDN_ERROR:
                extra = MtkTelephonyIntents.EXTRA_WFC_PDN_ERROR;
                break;
            case MtkTelephonyIntents.ACTION_PDN_HANDOVER:
                extra = MtkTelephonyIntents.EXTRA_PDN_HANDOVER;
                break;
            case MtkTelephonyIntents.ACTION_PDN_ROVE_OUT:
                extra = MtkTelephonyIntents.EXTRA_ROVE_OUT;
                break;
            case MtkTelephonyIntents.ACTION_WFC_PDN_STATE_CHANGED:
                extra = MtkTelephonyIntents.EXTRA_WFC_PDN_STATE;
                break;
            default:
                Rlog.d(TAG, intent.getAction() + " no implements");
        }
        if (null == extra) {
            return null;
        }
        return intent.getExtras().getIntArray(extra);
    }

    private void onPdnHandover(Message msg) {
        int[] result = getIntArrayFromMsg(msg);
        if (null == result) {
            Rlog.e(TAG, "onPdnHandover(): result is null");
            return;
        }
        if (result.length < 5) {
            Rlog.e(TAG, "onPdnHandover(): Bad params");
            return;
        }
        int pdnType = result[0];
        if (pdnType != 0) {
            Rlog.d(TAG, "onPdnHandover(): Not IMS PDN, ignore");
            return;
        }

        int stage = result[1];
        int srcRat = result[2];
        int desRat = result[3];
        int simIdx = result[4];
        notifyOnHandover(simIdx, stage, desRat);

        if (stage == WifiOffloadManager.HANDOVER_END &&
                desRat != WifiOffloadManager.RAN_TYPE_WIFI) {
            mWosExt.clearPDNErrorMessages();
        }
    }

    private void onWfcPdnError(Message msg) {
        int[] result = getIntArrayFromMsg(msg);
        if (null == result) {
            Rlog.e(TAG, "onWfcPdnError(): result is null");
            return;
        }
        int errCount = result[0];
        int mainErr = result[1];
        int subErr = result[2];
        int simIdx = result[3];
        mDisconnectCause[simIdx] = new DisconnectCause(mainErr, subErr);
        if (errCount == 0) {
            mWosExt.clearPDNErrorMessages();
            return;
        }
        mWosExt.showPDNErrorMessages(mainErr);
    }

    private void onWifiRoveout(Message msg) {
        int[] result = getIntArrayFromMsg(msg);
        if (null == result) {
            Rlog.e(TAG, "onWifiRoveout(): result is null");
            return;
        }
        if (result.length < 2) {
            Rlog.e(TAG, "onWifiRoveout(): Bad params");
            return;
        }
        int simIdx = result[1];
        boolean roveOut = (result[0] == 1);
        notifyOnRoveOut(simIdx, roveOut, mWifiPdnHandler.getLastRssi());
    }

    private void onWfcPdnStateChanged(Message msg) {
        int[] result = getIntArrayFromMsg(msg);
        if (null == result) {
            Rlog.e(TAG, "onWfcPdnStateChanged(): result is null");
            return;
        }
        int state  = result[0];
        int simIdx = result[1];
        Rlog.d(TAG, "onWfcPdnStateChanged() state:" + state + " simIdx:" + simIdx);
        if (1 == state) {
            mWosExt.clearPDNErrorMessages();
        }
    }

    private boolean checkInvalidSimIdx(int simIdx, String dbgMsg) {
        if (simIdx < 0 || simIdx >= mSimCount) {
            Rlog.d(TAG, dbgMsg);
            return true;
        }
        return false;
    }

    private void setEmergencyAddressId() {
        if (TextUtils.isEmpty(mWfcEccAid)) {
            Rlog.d(TAG, "Current AID is empty");
            return;
        }
        Message result = mHandler.obtainMessage(RESPONSE_SET_WFC_EMERGENCY_ADDRESS_ID);
        boolean ret = MtkTelephonyManagerEx.getDefault().setEmergencyAddressId(
                getMainCapabilityPhoneId(), mWfcEccAid);
        if (!ret) {
            result.sendToTarget();
        }
    }

    private void handleModemPower() {
        Rlog.d(TAG, "handleModemPower() mIsWifiEnabled:" +
                mIsWifiEnabled + " mIsWfcSettingsOn: " + isWfcSettingsEnabledAny());
        /* If the flight mode is off, RadioManager will ignore the notify from WoS.
         * If the flight mode is on, handle the modem power by following rules:
         * 1. WFC enabled and Wifi is enabled, turn on modem
         * 2. WFC settings enabled and Wifi is enabled, turn on modem
         * 3. If Wfc settings or Wifi off, turn off modem
         */
        if (mIsWifiEnabled && isWfcSettingsEnabledAny()) {
            notifyPowerOnModem(true);
        } else {
            notifyPowerOnModem(false);
        }
    }

    private void notifyPowerOnModem(boolean isModemOn) {
        if (!RadioManager.isFlightModePowerOffModemConfigEnabled()) {
            Rlog.d(TAG, "modem always on, no need to control it!");
            return;
        }

        if (mContext == null) {
            Rlog.d(TAG, "context is null, can't control modem!");
            return;
        }

        Intent intent = new Intent(RADIO_MANAGER_POWER_ON_MODEM);
        intent.putExtra(EXTRA_POWER_ON_MODEM_KEY, isModemOn);
        mContext.sendBroadcast(intent);
    }

    private boolean isWfcSettingsEnabledAny() {
        for (int i = 0; i < mSimCount; ++i) {

            if (mIsWfcSettingsOn[i]) {
                Rlog.d(TAG, "isWfcSettingsEnabledAny: found Wfc settings enabled on SIM: " + i);
                return true;
            }
        }
        return false;
    }

    private boolean isWfcSettingUpdated(Uri uri) {
        for (int i = 0; i < mSimCount; ++i) {
            String key = mWfcSettingKeys[i];
            if (uri.equals(Settings.Global.getUriFor(key))) {
                return true;
            }
        }
        return false;
    }

    // Monitor wfc settings
    private final ContentObserver mContentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange, Uri uri) {
            Rlog.d(TAG, "SettingsObserver.onChange(): " + uri);

            if (AID_SETTING_URI.equals(uri)) {
                mWfcEccAid = Settings.Global.getString(
                        mContext.getContentResolver(), AID_SETTING_URI_STR);
                mHandler.sendMessage(mHandler.obtainMessage(EVENT_SET_WFC_EMERGENCY_ADDRESS_ID));
            } else if (isWfcSettingUpdated(uri)) {
                if (updateWfcUISetting()) {
                    mHandler.sendMessage(mHandler.obtainMessage(EVENT_HANDLE_MODEM_POWER));

                    checkIfShowNoInternetError(false);
                }
            }
        }
    };

    private boolean updateWfcUISetting() {
        boolean ret = false;
        try {
            for (int i = 0; i < mSimCount; ++i) {
                String key = mWfcSettingKeys[i];
                mIsWfcSettingsOn[i] =
                    Settings.Global.getInt(mContext.getContentResolver(), key) == 1;

                Rlog.d(TAG, "WfcSetting simId: " + i + " enabled: " + mIsWfcSettingsOn[i]);
                ret = true;
            }
        } catch (SettingNotFoundException e) {
            Rlog.e(TAG, "Can not get WFC setting");
        }
        return ret;
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Rlog.d(TAG, "onReceive action:" + intent.getAction());
            if (intent.getAction().equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                WifiManager wifiMngr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                mIsWifiEnabled = wifiMngr.isWifiEnabled();
                mHandler.sendMessage(mHandler.obtainMessage(EVENT_HANDLE_MODEM_POWER));
            } else if (intent.getAction().equals(
                    CarrierConfigManager.ACTION_CARRIER_CONFIG_CHANGED)) {
                int phoneId = intent.getIntExtra(PhoneConstants.PHONE_KEY, -1);
                if (!ImsManager.isWfcEnabledByPlatform(context, phoneId)) {
                    Rlog.d(TAG, "isWfcEnabledByPlatform(" + phoneId +
                        ") is false, clearPDNErrorMessages");
                    mWosExt.clearPDNErrorMessages();
                }
            } else if (intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                Parcelable parcelableExtra = intent
                    .getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (null != parcelableExtra) {
                    NetworkInfo networkInfo = (NetworkInfo) parcelableExtra;
                    State state = networkInfo.getState();
                    mIsWifiL2Connected = state == State.CONNECTED;

                    checkIfShowNoInternetError(false);
                }
            }
        }
    };

    private void notifyOnHandover(int simIdx, int stage, int ratType) {
        Rlog.d(TAG, "onHandover simIdx: " + simIdx + " stage: " + stage + " rat: " + ratType);
        int i = mListeners.beginBroadcast();
        while (i > 0) {
            i--;
            try {
                mListeners.getBroadcastItem(i).onHandover(simIdx, stage, ratType);
            } catch (RemoteException e) {
                // The RemoteCallbackList will take care of removing
                // the dead object for us.
                Rlog.d(TAG, "onHandover: RemoteException occurs!");
            }
        }
        mListeners.finishBroadcast();
    }

    private void notifyOnRoveOut(int simIdx, boolean roveOut, int rssi) {
        Rlog.d(TAG, "onRoveOut simIdx: " + simIdx + " roveOut: " + roveOut + " rssi: " + rssi);
        int i = mListeners.beginBroadcast();
        while (i > 0) {
            i--;
            try {
                mListeners.getBroadcastItem(i).onRoveOut(simIdx, roveOut, rssi);
            } catch (RemoteException e) {
                Rlog.d(TAG, "onRoveOut: RemoteException occurs!");
            }
        }
        mListeners.finishBroadcast();
    }

    public IWifiOffloadService getWfoInterface() {
        return mWfoService;
    }

    /**
     * to get main capability phone id.
     *
     * @return The phone id with highest capability.
     */
    private int getMainCapabilityPhoneId() {
       int phoneId = SystemProperties.getInt(MtkPhoneConstants.PROPERTY_CAPABILITY_SWITCH, 1) - 1;
       if (phoneId < 0 || phoneId >= TelephonyManager.getDefault().getPhoneCount()) {
           phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
       }
       Rlog.d(TAG, "getMainCapabilityPhoneId = " + phoneId);
       return phoneId;
    }

    /**
     * Detect BSP/TK branch to initialize WFC Setting Keys
     *
     * @return String[] wfcSettingsKeys
     */
    private String[] wfcSettingKeysInitialization() {
        String[] ret = new String[mSimCount];
        Class<?> clazz = null;

        if (mSimCount < 1) {
            Rlog.d(TAG, "No WFC Settings Support - wfcSettingKeysInitialization simCount = "
                + mSimCount);
            return null;
        }

        for (int i = 0; i < mSimCount; i++) {
            ret[i] = "";
        }

        try {
            clazz = Class.forName("android.provider.Settings.Global");
            Field field = null;

            field = clazz.getField("WFC_IMS_ENABLED");
            ret[0] = (String) field.get(null);

            for (int i = 1; i < mSimCount; i++) {
                String fieldName = "WFC_IMS_ENABLED_SIM" + (i + 1);
                Rlog.d(TAG, "wfcSettingKeysInitialization: fieldName = " + fieldName);
                field = clazz.getField(fieldName);
                ret[i] = (String) field.get(null);
            }
        } catch (Exception e) {
            Rlog.d(TAG, "wfcSettingKeysInitialization: " +
                "android.provider.Settings.Global field not available");
        }
        return ret;
    }

    private void checkIfShowNoInternetError(boolean showImmediately) {

        int mainCapabilityPhoneId = getMainCapabilityPhoneId();
        int subId = getSubIdBySlot(mainCapabilityPhoneId);

        boolean isImsReg = MtkTelephonyManagerEx.getDefault().isImsRegistered(subId);
        boolean isWifiConnected = mWifiPdnHandler.isWifiConnected();

        if (mIsWfcSettingsOn[mainCapabilityPhoneId] && !isImsReg &&
            mIsWifiL2Connected && !isWifiConnected) {

            if (showImmediately) {
                mWosExt.showPDNErrorMessages(WIFI_NO_INTERNET_ERROR_CODE);
                return;
            }

            if (!mHandler.hasMessages(EVENT_NOTIFY_WIFI_NO_INTERNET)) {
                Rlog.d(TAG, "checkIfShowNoInternetError(): start 8s timeout");
                mHandler.sendMessageDelayed(
                    mHandler.obtainMessage(EVENT_NOTIFY_WIFI_NO_INTERNET),
                    WIFI_NO_INTERNET_TIMEOUT);
            }

        } else {
            if (mHandler.hasMessages(EVENT_NOTIFY_WIFI_NO_INTERNET)) {
                Rlog.d(TAG, "checkIfShowNoInternetError(): cancel 8s timeout");
                mHandler.removeMessages(EVENT_NOTIFY_WIFI_NO_INTERNET);
            }
        }
    }

    private int getSubIdBySlot(int slot) {
        int [] subId = SubscriptionManager.getSubId(slot);
        return (subId != null) ? subId[0] : SubscriptionManager.getDefaultSubscriptionId();
    }
}
