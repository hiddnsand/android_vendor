package com.mediatek.backuprestore.utils;

import android.content.Context;
import android.os.Looper;
import android.os.RemoteException;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.os.Environment;
import android.os.SystemProperties;
import android.util.Log;

import com.mediatek.backuprestore.R;
import com.mediatek.backuprestore.utils.Constants.LogTag;
import com.mediatek.backuprestore.utils.Constants.ModulePath;
import com.mediatek.storage.StorageManagerEx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SDCardUtils {

    public static final int MINIMUM_SIZE = 512;
    //private static String usbPath = Environment.DIRECTORY_USBOTG;

    public static String getStoragePath(String storagePath) {
        if (storagePath == null) {
            return null;
        }
        String storageMountPath = storagePath + File.separator + ModulePath.FOLDER_BACKUP;
        MyLogger.logD(LogTag.LOG_TAG, "getStoragePath: path is " + storagePath);
        if (checkStoragePath(storageMountPath) != null) {
            return storageMountPath;
        }
        return null;
    }

    public static String getStoragePath(Context context) {
        String sdcardMountPath = getSdCardMountPath(context);
        if (sdcardMountPath == null) {
            MyLogger.logD(LogTag.LOG_TAG, "getStoragePath getSdCardMountPath() == null");
            return null;
        }
        String storagePath = sdcardMountPath + File.separator;
        MyLogger.logD(LogTag.LOG_TAG, "getStoragePath: path is " + storagePath);
        if (storagePath == null) {
            return null;
        }
        if (checkStoragePath(storagePath) != null) {
            return storagePath;
        } else {
            String phonePath = getPhoneStoragePath(context);
            if (sdcardMountPath == null) {
                MyLogger.logD(LogTag.LOG_TAG, "getStoragePath getSdCardMountPath() == null");
                return null;
            }
            String phoneStoragePath = phonePath + File.separator + "backup";
            if (checkStoragePath(phoneStoragePath) != null) {
                return phoneStoragePath;
            } else {
                MyLogger.logD(LogTag.LOG_TAG, "the phone is no storage");
                return null;
            }

        }
    }

    private static String checkStoragePath(String storagePath) {
        File file = new File(storagePath);
        if (file != null) {
            if (file.exists() && file.isDirectory()) {
                File temp = new File(storagePath + File.separator + ".BackupRestoretemp");
                boolean ret;
                if (temp.exists()) {
                    ret = temp.delete();
                } else {
                    try {
                        ret = temp.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                        MyLogger.logE(LogTag.LOG_TAG, "getStoragePath: " + e.getMessage());
                        ret = false;
                    } finally {
                        temp.delete();
                    }
                }
                if (ret) {
                    return storagePath;
                } else {
                    return null;
                }

            } else if (file.mkdir()) {
                return storagePath;
            }
        }
        return null;
    }

    private static boolean isNeedGetSdCardStatus = true;
    private static boolean isSupportSdCard = true;
    public static boolean isSupprotSDcard() {
        if (isNeedGetSdCardStatus) {
            MyLogger.logD(LogTag.LOG_TAG,"get sdcard support state ");
            isSupportSdCard = getSupprotSDcardInfo();
            isNeedGetSdCardStatus = false;
        }
        MyLogger.logD(LogTag.LOG_TAG, "isSupportSdCard = " + isSupportSdCard);
        return isSupportSdCard;
    }

    public static boolean getSupprotSDcardInfo() {
        File dir = new File("vendor/etc");
        File[] files = null;
        if (dir != null && dir.exists()) {
            files = dir.listFiles();
        } else {
            MyLogger.logD(LogTag.LOG_TAG, "vendor/etc path is null, return false");
            return false;
        }

        File fstabFile = null;
        for (File file:files) {
            if (file.getName().startsWith("fstab")) {
                MyLogger.logD(LogTag.LOG_TAG, "dfstabFile path = " + file.getAbsolutePath());
                fstabFile = file;
                break;
            }
        }

        BufferedReader buffreader;
        if (fstabFile != null && fstabFile.exists()) {
            try {
                buffreader = new BufferedReader(new InputStreamReader(
                        new FileInputStream(fstabFile)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            MyLogger.logD(LogTag.LOG_TAG, "fstab file is not exit, return false");
            return false;
        }

        String line =null;
        try {
            while ((line = buffreader.readLine()) != null) {
                if (line.contains("voldmanaged=sdcard1")) {
                    MyLogger.logD(LogTag.LOG_TAG,"support sdcard return true");
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        MyLogger.logD(LogTag.LOG_TAG, " not find return false ");
        return false;
    }

    public static String getPhoneStoragePath(Context context) {
        StorageManager storageManager = null;
        String mPhonePath = null;
        storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        StorageVolume[] volumes = storageManager.getVolumeList();
        if (volumes != null) {
            for (StorageVolume volume : volumes) {
                if (!volume.isRemovable()) {
                    mPhonePath = volume.getPath();
                    return mPhonePath;
                }
            }
        }
        return mPhonePath;
    }

    public static String getSdCardMountPath(Context context) {
       /* String externalPath = StorageManagerEx.getExternalStoragePath();
        if(externalPath != null && externalPath != "") {
            MyLogger.logD(LogTag.LOG_TAG,"getSdCardMountPath() externalPath = "+ externalPath);
            return externalPath;
        }
        MyLogger.logD(LogTag.LOG_TAG, "getSdCardMountPath() = null");
        return null;*/
        StorageManager storageManager = null;
        String sdcardPath = null;
        storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        StorageVolume[] volumes = storageManager.getVolumeList();
        if (volumes != null) {
            for (StorageVolume volume : volumes) {
                if (volume.isRemovable()) {
                    sdcardPath = volume.getPath();
                    MyLogger.logD(LogTag.LOG_TAG, "getSdCardMountPath = " + sdcardPath);
                    return sdcardPath;
                }
            }
        }
        MyLogger.logD(LogTag.LOG_TAG, "getSdCardMountPath() = null");
        return sdcardPath;
    }

    public static List<String> getPathList(Context context) {
        StorageManager storageManager = null;
        List<String> pathList = new ArrayList<String>();
        storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        StorageVolume[] volumes = storageManager.getVolumeList();
        if (volumes != null) {
            for (StorageVolume volume : volumes) {
                String path = volume.getPath();
              //  if (path != null && !StorageManagerEx.isUSBOTG(path)) {
                    if (path != null) {
                    pathList.add(path);
                }
            }
        }
        return pathList;
    }

    public static String getPersonalDataBackupPath(String path) {
        if (path != null) {
            return path + File.separator + ModulePath.FOLDER_DATA;
        }
        return path;
    }

    public static String getAppsBackupPath(String path) {
        if (path != null) {
            return path + File.separator + ModulePath.FOLDER_APP;
        }
        return path;
    }

    public static boolean isSdCardAvailable(Context context) {
        return (getSDCardDataPath(context) != null);
    }

    public static boolean isSdcardFull(Context context) {
        String path = getSdCardMountPath(context);
        if (path == null) {
            return false;
        }
        MyLogger.logD(LogTag.LOG_TAG, "path =  " + path);

        long count = 0;
        try {
            android.os.StatFs stat = new android.os.StatFs(path);
            count = stat.getBlockCount();
        } catch (IllegalArgumentException e) {
            MyLogger.logD(LogTag.LOG_TAG, "android.os.StatFs exception.");
            e.printStackTrace();
            return false;
        }

        long size = getAvailableSize(path);
        if (size <= 512 && count > 0) {
            MyLogger.logD(LogTag.LOG_TAG, "isSdcardFull = true.");
            return true;
        } else {
            MyLogger.logD(LogTag.LOG_TAG, "isSdcardFull = false");
            return false;
        }
    }

    public static long getAvailableSize(String file) {
        android.os.StatFs stat = new android.os.StatFs(file);
        long count = stat.getAvailableBlocks();
        long size = stat.getBlockSize();
        long totalSize = count * size;
        MyLogger.logD(LogTag.LOG_TAG, "file remain size = " + totalSize);
        return totalSize;
    }

    public static String getSDStatueMessage(Context context) {
        String message = context.getString(R.string.nosdcard_notice);
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_SHARED) || status.equals(Environment.MEDIA_UNMOUNTED)) {
            message = context.getString(R.string.sdcard_busy_message);
        }
        return message;
    }

    public static String getSDCardDataPath(Context context) {
        return SDCardUtils.getStoragePath(SDCardUtils.getSdCardMountPath(context));
    }

    public static String getPhoneDataPath(Context context) {
        return SDCardUtils.getStoragePath(SDCardUtils.getPhoneStoragePath(context));
    }

    public static boolean checkedPath(String path) {
        File checkedFile = new File(path);
        if (checkedFile != null) {
            if (checkedFile.exists() && checkedFile.isDirectory()) {
                File temp = new File(path + File.separator + ".BackupRestoretemp");
                boolean ret;
                if (temp.exists()) {
                    ret = temp.delete();
                } else {
                    try {
                        ret = temp.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(LogTag.LOG_TAG, "checkedPath: " + e.getMessage());
                        ret = false;
                    } finally {
                        temp.delete();
                    }
                }
                Log.e(LogTag.LOG_TAG, "checkedPath: ret = " + ret);
                if (ret) {
                    return true;
                } else {
                    return false;
                }
            }

            if (checkedFile.exists() && checkedFile.isFile()) {
                Log.e(LogTag.LOG_TAG, "checkedPath: the path is a File and it is exists! ");
                return true;
            }
        }
        return false;
    }
}
