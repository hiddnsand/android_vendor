#define DEBUG_LOG_TAG "DEV"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h>
#include <sys/ioctl.h>

#include <linux/ion_drv.h>
#include <ion/ion.h>
#include <ion.h>

#include "m4u_lib.h"

#include <linux/mtkfb_info.h>
#include <linux/hdmitx.h>
#include "ddp_ovl.h"

#include "utils/debug.h"
#include "utils/tools.h"
#include <utils/Trace.h>
#include "hwdev.h"
#include "display.h"
#include "overlay.h"
#include "platform.h"

#ifdef MTK_SVP_SUPPORT

#ifdef MTK_TEE_TRUSTONIC
    #ifdef MTK_TEE_INHOUSE
        #error "DEFINE TWO TEE SOLUTIONS !!"
    #endif
    #include <tlc_sec_mem.h>
    #include <tlcsecmem.h>
#else
    #ifndef MTK_TEE_INHOUSE
        #error "NOT DEFINE TEE SOLUTION !!"
    #endif
#endif

#endif // MTK_SVP_SUPPORT

#define FB_DEV_NODE "/dev/graphics/fb0"
#define HDMI_DEV_NODE "/dev/hdmitx"
#define ION_DEV_NODE "/dev/ion"

// ---------------------------------------------------------------------------
DEFINE_LOG_BUF(no_used_layer_log);

DISP_SESSION_TYPE g_session_type[DisplayManager::MAX_DISPLAYS] = {
    DISP_SESSION_PRIMARY,
    DISP_SESSION_EXTERNAL,
    DISP_SESSION_MEMORY
};

DISP_FORMAT mapDispFormat(int format, bool for_input)
{
    switch (format)
    {
        case HAL_PIXEL_FORMAT_RGBA_8888:
            return DISP_FORMAT_ABGR8888;

        case HAL_PIXEL_FORMAT_YV12:
            if (for_input)
                return DISP_FORMAT_YUV422;
            else
                return DISP_FORMAT_YV12;
        case HAL_PIXEL_FORMAT_RGBX_8888:
            return DISP_FORMAT_XBGR8888;

        case HAL_PIXEL_FORMAT_BGRA_8888:
            return DISP_FORMAT_ARGB8888;

        case HAL_PIXEL_FORMAT_IMG1_BGRX_8888:
            return DISP_FORMAT_XRGB8888;

        case HAL_PIXEL_FORMAT_RGB_888:
            return DISP_FORMAT_RGB888;

        case HAL_PIXEL_FORMAT_RGB_565:
            return DISP_FORMAT_RGB565;

        case HAL_PIXEL_FORMAT_I420:
        case HAL_PIXEL_FORMAT_NV12_BLK:
        case HAL_PIXEL_FORMAT_NV12_BLK_FCM:
        case HAL_PIXEL_FORMAT_YUV_PRIVATE:
            return DISP_FORMAT_YUV422;
    }
    HWC_LOGW("Not support format(%d), use default RGBA8888", format);
    return DISP_FORMAT_ABGR8888;
}

ANDROID_SINGLETON_STATIC_INSTANCE(DispDevice);

DispDevice::DispDevice()
{
    m_dev_fd = open(FB_DEV_NODE, O_RDONLY);
    if (m_dev_fd <= 0)
    {
        HWC_LOGE("Failed to open fb device: %s ", strerror(errno));
        abort();
    }

    m_ovl_input_num = getMaxOverlayInputNum();

    int size = sizeof(disp_session_input_config) * DisplayManager::MAX_DISPLAYS;
    memset(m_input_config, 0, size);
    for (int i = 0; i < DisplayManager::MAX_DISPLAYS; i++)
    {
        m_input_config[i].session_id = DISP_INVALID_SESSION;
    }
}

DispDevice::~DispDevice()
{
    close(m_dev_fd);
}

void DispDevice::initOverlay()
{
    DevicePlatform::getInstance().initOverlay();
}

int DispDevice::getMaxOverlayInputNum()
{
    return MAX_INPUT_CONFIG;
}

status_t DispDevice::createOverlaySession(int dpy, int mode)
{
    int session_id = m_input_config[dpy].session_id;
    if (DISP_INVALID_SESSION != session_id)
    {
        HWC_LOGW("(%d) Failed to create existed DispSession (id=0x%x)", dpy, session_id);
        return INVALID_OPERATION;
    }

    disp_session_config config;
    memset(&config, 0, sizeof(disp_session_config));

    config.type       = g_session_type[dpy];
    config.device_id  = dpy;
    config.mode       = (DISP_MODE)mode;
    config.session_id = DISP_INVALID_SESSION;

    int err = ioctl(m_dev_fd, DISP_IOCTL_CREATE_SESSION, &config);
    if (err < 0)
    {
        m_input_config[dpy].session_id = DISP_INVALID_SESSION;

        HWC_LOGE("(%d) Failed to create DispSession (mode=%d)!!", dpy, mode);

        return -EINVAL;
    }

    m_input_config[dpy].session_id = config.session_id;

    HWC_LOGD("(%d) Create DispSession (id=0x%x, mode=%d)", dpy, config.session_id, mode);

    return NO_ERROR;
}

void DispDevice::destroyOverlaySession(int dpy)
{
    int session_id = m_input_config[dpy].session_id;
    if (DISP_INVALID_SESSION == session_id)
    {
        HWC_LOGW("(%d) Failed to destroy invalid DispSession", dpy);
        return;
    }

    disp_session_config config;
    memset(&config, 0, sizeof(disp_session_config));

    config.type       = g_session_type[dpy];
    config.device_id  = dpy;
    config.session_id = session_id;

    ioctl(m_dev_fd, DISP_IOCTL_DESTROY_SESSION, &config);
    m_input_config[dpy].session_id = DISP_INVALID_SESSION;

    HWC_LOGD("(%d) Destroy DispSession (id=0x%x)", dpy, session_id);
}

status_t DispDevice::triggerOverlaySession(int dpy, int present_fence_idx)
{
    int session_id = m_input_config[dpy].session_id;
    if (DISP_INVALID_SESSION == session_id)
    {
        HWC_LOGW("(%d) Failed to trigger invalid DispSession", dpy);
        return INVALID_OPERATION;
    }

    disp_session_config config;
    memset(&config, 0, sizeof(disp_session_config));

    config.type       = g_session_type[dpy];
    config.device_id  = dpy;
    config.session_id = session_id;
    config.present_fence_idx = present_fence_idx;

    ioctl(m_dev_fd, DISP_IOCTL_TRIGGER_SESSION, &config);

    HWC_LOGD("(%d) Trigger DispSession (id=0x%x, p_fence=%d)", dpy, session_id, present_fence_idx);

    return NO_ERROR;
}

void DispDevice::disableOverlaySession(int dpy, int num)
{
    int session_id = m_input_config[dpy].session_id;
    if (DISP_INVALID_SESSION == session_id)
    {
        HWC_LOGW("(%d) Failed to disable invalid DispSession", dpy);
        return;
    }

    for (int i = 0; i < m_ovl_input_num; i++)
    {
        disp_input_config* input = &m_input_config[dpy].config[i];

        if (i >= num)
        {
            input->layer_id = m_ovl_input_num + 1;
            continue;
        }

        input->layer_id     = i;
        input->layer_enable = 0;
        HWC_LOGD("(%d:%d) Layer-", dpy, i);
    }

    ioctl(m_dev_fd, DISP_IOCTL_SET_INPUT_BUFFER, &m_input_config[dpy]);

    disp_session_config config;
    memset(&config, 0, sizeof(disp_session_config));

    config.type       = g_session_type[dpy];
    config.device_id  = dpy;
    config.session_id = session_id;
    config.present_fence_idx = DISP_NO_PRESENT_FENCE;

    ioctl(m_dev_fd, DISP_IOCTL_TRIGGER_SESSION, &config);
    HWC_LOGD("(%d) Disable DispSession (id=0x%x)", dpy, session_id);
}

status_t DispDevice::getOverlaySessionInfo(int dpy, disp_session_info* info)
{
    int session_id = m_input_config[dpy].session_id;
    if (DISP_INVALID_SESSION == session_id)
    {
        HWC_LOGW("(%d) Failed to get info for invalid DispSession", dpy);
        return INVALID_OPERATION;
    }

    info->session_id = session_id;

    ioctl(m_dev_fd, DISP_IOCTL_GET_SESSION_INFO, info);

    return NO_ERROR;
}

int DispDevice::getAvailableOverlayInput(int dpy)
{
    int session_id = m_input_config[dpy].session_id;
    if (DISP_INVALID_SESSION == session_id)
    {
        HWC_LOGW("(%d) Failed to get info for invalid DispSession", dpy);
        return 0;
    }

    disp_session_info info;
    memset(&info, 0, sizeof(disp_session_info));

    info.session_id = session_id;

    ioctl(m_dev_fd, DISP_IOCTL_GET_SESSION_INFO, &info);

    return info.maxLayerNum;
}

void DispDevice::prepareOverlayInput(
    int dpy, OverlayPrepareParam* param)
{
    int session_id = m_input_config[dpy].session_id;
    if (DISP_INVALID_SESSION == session_id)
    {
        HWC_LOGW("(%d) Failed to preapre invalid DispSession (input)", dpy);
        return;
    }

    disp_buffer_info buffer;
    memset(&buffer, 0, sizeof(disp_buffer_info));

    buffer.session_id = session_id;
    buffer.layer_id   = param->id;
    buffer.layer_en   = 1;
    buffer.ion_fd     = param->ion_fd;
    buffer.cache_sync = param->is_need_flush;
    buffer.index      = -1;
    buffer.fence_fd   = -1;

    ioctl(m_dev_fd, DISP_IOCTL_PREPARE_INPUT_BUFFER, &buffer);

    param->fence_index = buffer.index;
    param->fence_fd    = buffer.fence_fd;
}

void DispDevice::enableOverlayInput(
    int dpy, OverlayPortParam* param, int id)
{
    // TODO
}

void DispDevice::updateOverlayInputs(
    int dpy, OverlayPortParam* const* params, int num)
{
    int session_id = m_input_config[dpy].session_id;
    if (DISP_INVALID_SESSION == session_id)
    {
        HWC_LOGW("(%d) Failed to update invalid DispSession (input)", dpy);
        return;
    }

    int config_layer_num = 0;

    INIT_LOG_BUF(no_used_layer_log);

    for (int i = 0; i < m_ovl_input_num; i++)
    {
        disp_input_config* input = &m_input_config[dpy].config[i];

        if (i >= num || (OVL_IN_PARAM_IGNORE == params[i]->state))
        {
            input->layer_id = DISP_NO_USE_LAEYR_ID;

            COMPACT_LOG(no_used_layer_log, "(%d:%d) Layer*, ", dpy, i);
            continue;
        }

        config_layer_num++;

        if (OVL_IN_PARAM_DISABLE == params[i]->state)
        {
            input->layer_id     = i;
            input->layer_enable = 0;

            COMPACT_LOG(no_used_layer_log, "(%d:%d) Layer-, ", dpy, i);
            continue;
        }

        input->layer_id       = i;
        input->layer_enable   = 1;
        input->layer_rotation = DISP_ORIENTATION_0;
        input->layer_type     = DISP_LAYER_2D;
        input->src_base_addr  = params[i]->va;
        input->src_phy_addr   = params[i]->mva;
        input->src_pitch      = params[i]->pitch;
        input->src_fmt        = mapDispFormat(params[i]->format, true);
        input->src_offset_x   = params[i]->src_crop.left;
        input->src_offset_y   = params[i]->src_crop.top;
        input->src_width      = params[i]->src_crop.getWidth();
        input->src_height     = params[i]->src_crop.getHeight();
        input->tgt_offset_x   = params[i]->dst_crop.left;
        input->tgt_offset_y   = params[i]->dst_crop.top;
        input->tgt_width      = params[i]->dst_crop.getWidth();
        input->tgt_height     = params[i]->dst_crop.getHeight();
        input->isTdshp        = params[i]->is_sharpen;
        input->next_buff_idx  = params[i]->fence_index;
        input->identity       = params[i]->identity;
        input->connected_type = params[i]->connected_type;
        input->alpha_enable   = params[i]->alpha_enable;
        input->alpha          = params[i]->alpha;
        input->frm_sequence   = params[i]->sequence;

        if (params[i]->secure)
        {
            if (params[i]->need_shift)
                input->security = DISP_SECURE_BUFFER_SHIFT;
            else
                input->security = DISP_SECURE_BUFFER;
        }
        else if (params[i]->protect)
        {
            input->security   = DISP_PROTECT_BUFFER;
        }
        else
        {
            input->security   = DISP_NORMAL_BUFFER;
        }

        HWC_LOGD("(%d:%d) Layer+ (mva=0x%x, ion=%d, idx=%d, sec=%d, protect=%d"
                 ", x=%d y=%d w=%d h=%d s=%d -> x=%d y=%d w=%d h=%d, alpha=%d:0x%02x)",
                 dpy, i, (int)params[i]->mva, params[i]->ion_fd,
                 params[i]->fence_index, params[i]->secure, params[i]->protect,
                 params[i]->src_crop.left, params[i]->src_crop.top,
                 params[i]->src_crop.getWidth(), params[i]->src_crop.getHeight(), params[i]->pitch,
                 params[i]->dst_crop.left, params[i]->dst_crop.top,
                 params[i]->dst_crop.getWidth(), params[i]->dst_crop.getHeight(),
                 params[i]->alpha_enable, params[i]->alpha);
    }

#ifndef MTK_USER_BUILD
    HWC_LOGD("%s", no_used_layer_log);
#endif
    //m_input_config[dpy].config_layer_num = config_layer_num;

    ioctl(m_dev_fd, DISP_IOCTL_SET_INPUT_BUFFER, &m_input_config[dpy]);
}

void DispDevice::prepareOverlayOutput(int dpy, OverlayPrepareParam* param)
{
    int session_id = m_input_config[dpy].session_id;
    if (DISP_INVALID_SESSION == session_id)
    {
        HWC_LOGW("(%d) Failed to prepare invalid DispSession (output)", dpy);
        return;
    }

    disp_buffer_info buffer;
    memset(&buffer, 0, sizeof(disp_buffer_info));

    buffer.session_id = session_id;
    buffer.layer_id   = param->id;
    buffer.layer_en   = 1;
    buffer.ion_fd     = param->ion_fd;
    buffer.cache_sync = param->is_need_flush;
    buffer.index      = -1;
    buffer.fence_fd   = -1;

    ioctl(m_dev_fd, DISP_IOCTL_PREPARE_OUTPUT_BUFFER, &buffer);

    param->fence_index = buffer.index;
    param->fence_fd    = buffer.fence_fd;
}

void DispDevice::enableOverlayOutput(int dpy, OverlayPortParam* param)
{
    int session_id = m_input_config[dpy].session_id;
    if (DISP_INVALID_SESSION == session_id)
    {
        HWC_LOGW("(%d) Failed to update invalid DispSession (output)", dpy);
        return;
    }

    disp_session_output_config output_config;
    output_config.session_id    = session_id;
    output_config.config.va     = param->va;
    output_config.config.pa     = param->mva;
    output_config.config.fmt    = mapDispFormat(param->format, false);
    output_config.config.x      = param->dst_crop.left;
    output_config.config.y      = param->dst_crop.top;
    output_config.config.width  = param->dst_crop.getWidth();
    output_config.config.height = param->dst_crop.getHeight();
    output_config.config.pitch  = param->pitch;

    if (param->secure)
        output_config.config.security = DISP_SECURE_BUFFER;
    else
        output_config.config.security = DISP_NORMAL_BUFFER;

    output_config.config.buff_idx = param->fence_index;

    output_config.config.frm_sequence = param->sequence;

    HWC_LOGD("(%d) Output+ (ion=%d, idx=%d, sec=%d"
             ", x=%d y=%d w=%d h=%d s=%d)",
             dpy, param->ion_fd, param->fence_index, param->secure,
             param->dst_crop.left, param->dst_crop.top,
             param->dst_crop.getWidth(), param->dst_crop.getHeight(), param->pitch);

    ioctl(m_dev_fd, DISP_IOCTL_SET_OUTPUT_BUFFER, &output_config);
}

void DispDevice::prepareOverlayPresentFence(int dpy, OverlayPrepareParam* param)
{
    int session_id = m_input_config[dpy].session_id;
    if (DISP_INVALID_SESSION == session_id)
    {
        HWC_LOGW("(%d) Failed to preapre invalid DispSession (present)", dpy);
        return;
    }

    {
        disp_present_fence fence;

        fence.session_id = session_id;

        int err = ioctl(m_dev_fd, DISP_IOCTL_GET_PRESENT_FENCE  , &fence);

        param->fence_index = fence.present_fence_index;
        param->fence_fd    = fence.present_fence_fd;

        if (0 < err)
        {
            HWC_LOGD("(%d) Prepare Present Fence (id=0x%x, err:%s)", dpy, session_id, strerror(err));
        }
    }
}

void DispDevice::setPowerMode(int mode)
{
    int session_id = m_input_config[0].session_id;
    if (DISP_INVALID_SESSION == session_id)
    {
        HWC_LOGW("(%d) Failed to SetOverlayPowerMode", 0);
        return;
    }

    ioctl(m_dev_fd, MTKFB_SET_DISPLAY_POWER_MODE, &mode);
    HWC_LOGD("(%d) set overlay power (mode=%d)", 0, mode);
}

// ---------------------------------------------------------------------------

ANDROID_SINGLETON_STATIC_INSTANCE(HdmiDevice);

HdmiDevice::HdmiDevice()
{
    m_dev_fd = open(HDMI_DEV_NODE, O_RDONLY);
    if (m_dev_fd <= 0)
    {
        HWC_LOGE("Failed to open hdmi device: %s ", strerror(errno));
        //abort();
    }
}

HdmiDevice::~HdmiDevice()
{
    if (m_dev_fd > 0) close(m_dev_fd);
}

void HdmiDevice::getDisplayInfo(int dpy, mtk_dispif_info* info)
{
    if (m_dev_fd <= 0) return;

    info->display_id = dpy;
    ioctl(m_dev_fd, MTK_HDMI_GET_DEV_INFO, info);
}

void HdmiDevice::prepareInput(HdmiInputInfo& param)
{
    if (m_dev_fd <= 0)
    {
        param.fence_fd = -1;
        return;
    }

    hdmi_buffer_info buffer_info;
    memset(&buffer_info, 0, sizeof(hdmi_buffer_info));

    buffer_info.ion_fd   = param.ion_fd;
    buffer_info.index    = -1;
    buffer_info.fence_fd = -1;

    ioctl(m_dev_fd, MTK_HDMI_PREPARE_BUFFER, &buffer_info);

    param.fence_index = buffer_info.index;
    param.fence_fd = buffer_info.fence_fd;

    HWC_LOGD("HdmiDevice::prepareInput: ion_fd(%d)", param.ion_fd);
}

void HdmiDevice::enableInput(
    HdmiInputInfo& param,
    const Rect& src_crop, const Rect& dst_crop)
{
    if (m_dev_fd <= 0) return;

    hdmi_video_buffer_info input;
    memset(&input, 0, sizeof(hdmi_video_buffer_info));

    input.src_base_addr  = param.va;
    input.src_phy_addr   = param.mva;
    input.src_pitch      = param.pitch;
    input.src_fmt        = mapDispFormat(param.format, true);
    input.src_offset_x   = src_crop.left;
    input.src_offset_y   = src_crop.top;
    input.src_width      = src_crop.getWidth();
    input.src_height     = src_crop.getHeight();
    input.security       = param.secure;
    input.next_buff_idx  = param.fence_index;

    HWC_LOGD("Enable HDMI Input: sec(%d), mva(%d), idx(%d)"
             ", x=%d y=%d w=%d h=%d -> x=%d y=%d w=%d h=%d",
             param.secure, param.mva, param.fence_index,
             src_crop.left, src_crop.top,
             src_crop.getWidth(), src_crop.getHeight(),
             dst_crop.left, dst_crop.top,
             dst_crop.getWidth(), dst_crop.getHeight());

    ioctl(m_dev_fd, MTK_HDMI_POST_VIDEO_BUFFER, &input);
}

void HdmiDevice::disableInput()
{
    if (m_dev_fd <= 0) return;

    hdmi_video_buffer_info layer;
    memset(&layer, 0, sizeof(hdmi_video_buffer_info));

    HWC_LOGD("Disable HDMI Input");
    ioctl(m_dev_fd, MTK_HDMI_POST_VIDEO_BUFFER, &layer);
}

// ---------------------------------------------------------------------------

ANDROID_SINGLETON_STATIC_INSTANCE(MMUDevice);

MMUDevice::MMUDevice()
{
    m_dev = new MTKM4UDrv();

    if (NULL == m_dev)
    {
        HWC_LOGE("Failed to initialize m4u driver");
        abort();
    }
}

MMUDevice::~MMUDevice()
{
    delete m_dev;
}

int MMUDevice::enable(int client)
{
    return m_dev->m4u_enable_m4u_func((M4U_MODULE_ID_ENUM) client);
}

int MMUDevice::map(int client, unsigned int vaddr, int size, unsigned int* mva)
{
    M4U_MODULE_ID_ENUM module = (M4U_MODULE_ID_ENUM) client;
    M4U_STATUS_ENUM err;

    err = m_dev->m4u_alloc_mva(module, vaddr, size, 0, 0, mva);
    if (M4U_STATUS_OK != err)
    {
        HWC_LOGE("Failed to allocate MVA, client=%d, va=0x%08x, size=%d",
              client, vaddr, size);
        return -1;
    }

    err = m_dev->m4u_insert_tlb_range(module, *mva, *mva + size - 1,
                                      RT_RANGE_HIGH_PRIORITY, 1);
    if (M4U_STATUS_OK != err)
    {
        HWC_LOGE("Failed to insert TLB, client=%d, va=0x%08x, size=%d",
              client, vaddr, size);
        m_dev->m4u_dealloc_mva(module, vaddr, size, *mva);
        return -1;
    }

    // do cache flush once for first usage
    m_dev->m4u_cache_sync(module, M4U_CACHE_FLUSH_BEFORE_HW_READ_MEM, vaddr, size);

    HWC_LOGD("Map MVA(0x%x) with VA(0x%x) Size(%d)", *mva, vaddr, size);

    return 0;
}

int MMUDevice::unmap(int client, unsigned int vaddr, int size, unsigned int mva)
{
    M4U_MODULE_ID_ENUM module = (M4U_MODULE_ID_ENUM) client;
    M4U_STATUS_ENUM err;

    err = m_dev->m4u_invalid_tlb_range(module, mva, mva + size - 1);
    if (M4U_STATUS_OK != err)
    {
        HWC_LOGE("Failed to invalid TLB, client=%d, va=0x%08x, mva=0x%08x, size=%d",
              client, vaddr, mva, size);
        return -1;
    }

    err = m_dev->m4u_dealloc_mva(module, vaddr, size, mva);
    if (M4U_STATUS_OK != err)
    {
        HWC_LOGE("Failed to dealloc MVA, client=%d, va=0x%08x, mva=0x%08x, size=%d",
              client, vaddr, mva, size);
        return -1;
    }

    HWC_LOGD("Unmap MVA(0x%x) with VA(0x%x) Size(%d)", mva, vaddr, size);

    return 0;
}

int MMUDevice::flush(int client, unsigned int vaddr, int size)
{
    M4U_MODULE_ID_ENUM module = (M4U_MODULE_ID_ENUM) client;

    m_dev->m4u_cache_sync(module, M4U_CACHE_FLUSH_BEFORE_HW_READ_MEM, vaddr, size);

    return 0;
}

int MMUDevice::config(int port, bool enabled)
{
    M4U_PORT_STRUCT m4u_port;
    M4U_STATUS_ENUM err;

    m4u_port.ePortID    = (M4U_PORT_ID_ENUM)port;
    m4u_port.Virtuality = (int) enabled;
    m4u_port.Security   = 0;
    m4u_port.Distance   = 1;
    m4u_port.Direction  = 0;

    err = m_dev->m4u_config_port(&m4u_port);
    if (M4U_STATUS_OK != err)
    {
        HWC_LOGE("Failed to config M4U port(%d)", port);
        return -1;
    }

    return 0;
}

// ---------------------------------------------------------------------------

ANDROID_SINGLETON_STATIC_INSTANCE(IONDevice);

IONDevice::IONDevice()
{
    m_dev_fd = open(ION_DEV_NODE, O_RDONLY);
    if (m_dev_fd <= 0)
    {
        HWC_LOGE("Failed to open ION device: %s ", strerror(errno));
    }
}

IONDevice::~IONDevice()
{
    if (m_dev_fd > 0) close(m_dev_fd);
}

unsigned int IONDevice::import(int ion_fd)
{
    if (m_dev_fd <= 0) return 0;

    ion_user_handle_t ion_hnd;
    if (ion_import(m_dev_fd, ion_fd, &ion_hnd))
    {
        HWC_LOGE("Failed to import ION handle: %s ", strerror(errno));
        return 0;
    }

    return (unsigned int)ion_hnd;
}

unsigned int IONDevice::alloc(int size)
{
    if (m_dev_fd <= 0) return 0;

    ion_user_handle_t ion_hnd;
	//this buffer is nocached
    if (ion_alloc_mm(m_dev_fd, size, 4, 0, &ion_hnd))
    {
        HWC_LOGE("Failed to allocate ION memory: %s ", strerror(errno));
        return 0;
    }

    return (unsigned int)ion_hnd;
}

void IONDevice::close(int ion_hnd)
{
    if (ion_free(m_dev_fd, (ion_user_handle_t) ion_hnd))
    {
        HWC_LOGE("Failed to free ION handle: %s ", strerror(errno));
    }
}

unsigned int IONDevice::getMMUAddress(int client, int ion_hnd)
{
    if (m_dev_fd <= 0) return 0;

    // configure before querying physical address
    struct ion_mm_data mm_data;
    mm_data.mm_cmd                        = ION_MM_CONFIG_BUFFER;
    mm_data.config_buffer_param.handle    = (ion_user_handle_t) ion_hnd;
    mm_data.config_buffer_param.eModuleID = (M4U_MODULE_ID_ENUM) client;
    mm_data.config_buffer_param.security  = 0;
    mm_data.config_buffer_param.coherent  = 0;

    int status = ion_custom_ioctl(m_dev_fd, ION_CMD_MULTIMEDIA, &mm_data);
    if ((status > 0) && (status != ION_ERROR_CONFIG_LOCKED))
    {
        HWC_LOGE("Failed to config ION memory: %s", strerror(errno));
        return 0;
    }

    // get physical address
    struct ion_sys_data sys_data;
    sys_data.sys_cmd               = ION_SYS_GET_PHYS;
    sys_data.get_phys_param.handle = (ion_user_handle_t) ion_hnd;
    if (ion_custom_ioctl(m_dev_fd, ION_CMD_SYSTEM, &sys_data))
    {
        HWC_LOGE("Failed to get MVA from ION: %s", strerror(errno));
        return 0;
    }

    return sys_data.get_phys_param.phy_addr;
}

unsigned int IONDevice::getCPUAddress(int ion_hnd)
{
    if (m_dev_fd <= 0) return 0;

    // TODO
    return 0;
}

void IONDevice::flush(int ion_hnd)
{
    if (m_dev_fd <= 0) return;

    struct ion_sys_data sys_data;
    sys_data.sys_cmd                    = ION_SYS_CACHE_SYNC;
    sys_data.cache_sync_param.handle    = (ion_user_handle_t) ion_hnd;
    sys_data.cache_sync_param.sync_type = ION_CACHE_FLUSH_ALL;
    if (ion_custom_ioctl(m_dev_fd, ION_CMD_SYSTEM, &sys_data))
    {
        HWC_LOGE("Failed to flush cache through ION: %s", strerror(errno));
    }
}

// ---------------------------------------------------------------------------

ANDROID_SINGLETON_STATIC_INSTANCE(TEEMemDevice);

void TEEMemDevice::open()
{
#ifdef MTK_TEE_TRUSTONIC
    tlcMemOpen();
#endif
}

void TEEMemDevice::close()
{
#ifdef MTK_TEE_TRUSTONIC
    tlcMemClose();
#endif
}

unsigned int TEEMemDevice::alloc(int size)
{
#ifdef MTK_TEE_TRUSTONIC
    AutoMutex l(m_ref_lock);

    if (!m_ref_cnt) open();
    m_ref_cnt++;

    UREE_SECURECM_HANDLE handle = 0;
    mcResult_t err = UREE_AllocSecuremem(&handle, 1024, size);
    if (MC_DRV_OK != err || handle == 0)
        return 0;

    HWC_LOGD("Alloc secure memory (handle=0x%x)", handle);

    return (unsigned int)handle;
#else
    return -1;
#endif
}

void TEEMemDevice::free(unsigned int handle)
{
#ifdef MTK_TEE_TRUSTONIC
    AutoMutex l(m_ref_lock);

    if (m_ref_cnt <= 0)
    {
        HWC_LOGW("No reference to TEE memory controller!!");
        return;
    }

    HWC_LOGD("Free secure memory (handle=0x%x)", handle);

    unsigned int ref_count;
    UREE_UnreferenceSecuremem((UREE_SECURECM_HANDLE)handle, &ref_count);

    m_ref_cnt--;
    if (m_ref_cnt <= 0) close();
#endif
}
