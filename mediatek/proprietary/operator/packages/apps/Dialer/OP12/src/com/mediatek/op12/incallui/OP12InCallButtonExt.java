package com.mediatek.op12.incallui;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.mediatek.incallui.ext.DefaultInCallButtonExt;
import com.mediatek.op12.dialer.R;

import mediatek.telecom.MtkTelecomManager;

/**
 * Plug in implementation for OP12 InCallButton interfaces.
 */
public class OP12InCallButtonExt extends DefaultInCallButtonExt {
    private static final String TAG = "OP12InCallButtonExt";
    private Context mContext;

    /** Constructor.
     * @param context context
     */
    public OP12InCallButtonExt(Context context) {
        mContext = context;
    }

    @Override
    public void showToastForGTT(Bundle extra) {
        int gttLocal = extra.getInt(MtkTelecomManager.EXTRA_GTT_MODE_LOCAL,
                MtkTelecomManager.GTT_MODE_OFF);
        int gttRemote = extra.getInt(MtkTelecomManager.EXTRA_GTT_MODE_REMOTE,
                MtkTelecomManager.GTT_MODE_OFF);
        Log.d(TAG, "showToastForGTT gttLocal = " + gttLocal + " gttRemote = " + gttRemote);
        if (gttRemote == MtkTelecomManager.GTT_MODE_ON && gttLocal ==
                MtkTelecomManager.GTT_MODE_OFF) {
            Log.d(TAG, "gtt local if off and gtt remote if on");
            Toast.makeText(mContext, mContext.getString(R.string.open_gtt),
                    Toast.LENGTH_LONG).show();
        }
    }
}