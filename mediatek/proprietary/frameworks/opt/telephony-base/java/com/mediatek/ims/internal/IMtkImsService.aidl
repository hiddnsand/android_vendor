/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
/*
 * Copyright (c) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mediatek.ims.internal;

import com.android.ims.ImsCallProfile;
import com.android.ims.internal.IImsCallSessionListener;
import com.android.ims.internal.IImsUt;

import com.mediatek.gba.NafSessionKey;
import com.mediatek.ims.internal.IMtkImsConfig;
import com.mediatek.ims.internal.IMtkImsCallSession;
import com.mediatek.ims.internal.IMtkImsUt;

/**
 * {@hide}
 */
interface IMtkImsService {
    /**
     * call interface for allowing/refusing the incoming call indication send to App.
     */
    void setCallIndication(int serviceId, String callId, String callNum, int seqNum,
            boolean isAllow);

    IMtkImsCallSession createCallSession(int serviceId, in ImsCallProfile profile,
            in IImsCallSessionListener listener);
    IMtkImsCallSession getPendingMtkCallSession(int serviceId, String callId);

    /**
     * Use to query ims enable/disable status.
     */
    int getImsState(int phoneId);

    /**
     * Use to hang up all calls.
     */
    void hangupAllCall(int serviceId);

    ///M: Used to deregister IMS @ {
    /**
     * Used to deregister IMS.
     */
    void deregisterIms(int phoneId);
    /// @}

    ///M: Used to notify radio state change @ {
    /**
     * Used to notify radio state change.
     */
    void updateRadioState(int radioState, int phoneId);
    /// @}

    /**
     * MTK Config interface to get/set IMS service/capability parameters.
     */
    IMtkImsConfig getConfigInterfaceEx(int phoneId);

    /**
     * Ut interface for the supplementary service configuration.
     */
    IImsUt getUtInterface(int serviceId);

    /**
     * MTK UT interface to get/set call forwarding with timeslot.
     */
    IMtkImsUt getMtkUtInterface(int serviceId);

    ///M: Used to trigger GBA in native SS solution @ {
    /**
     * Used to trigger GBA in native SS solution.
     */
    NafSessionKey runGbaAuthentication(String nafFqdn,
            in byte[] nafSecureProtocolId, boolean forceRun, int netId, int phoneId);
    /// @}

    /**
     * Used to get modem multiple IMS count.
     */
    int getModemMultiImsCount();
}
