package com.mediatek.mms.plugin;

import android.content.Context;
import android.util.Log;

import com.mediatek.mms.ext.IOpMessagePluginExt;
import com.mediatek.mms.ext.OpMmsCustomizationFactoryBase;

public class Op07MmsCustomizationFactory extends OpMmsCustomizationFactoryBase {

    private static String TAG = "Op07MmsCustomizationFactory";

    public Op07MmsCustomizationFactory(Context context) {
        super(context);
        Log.d(TAG, "[Op07MmsCustomizationFactory]context=" + context.getApplicationInfo());
    }

    @Override
    public IOpMessagePluginExt makeOpMessagePluginExt() {
        return new Op07MessagePluginExt(mContext);
    }
}
