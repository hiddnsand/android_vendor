/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony.uicc;

import static com.android.internal.telephony.TelephonyProperties.PROPERTY_TEST_CSIM;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.AsyncResult;
import android.os.Message;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.telephony.Rlog;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.internal.telephony.cdma.sms.UserData;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.CommandsInterface.RadioState;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.MccTable;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.SubscriptionController;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.TelephonyProperties;
import com.android.internal.telephony.uicc.IccCardApplicationStatus.AppType;
import com.android.internal.telephony.uicc.IccRefreshResponse;
import com.android.internal.telephony.uicc.IccUtils;
import com.android.internal.telephony.uicc.RuimRecords;
import com.android.internal.telephony.uicc.UiccCardApplication;
import com.android.internal.util.BitwiseInputStream;
// PHB START @{
import com.mediatek.internal.telephony.MtkTelephonyIntents;
import com.mediatek.internal.telephony.MtkRIL;
import com.mediatek.internal.telephony.phb.CsimPhbUtil;
import com.mediatek.internal.telephony.phb.MtkAdnRecordCache;
// PHB END @}
import com.mediatek.internal.telephony.uicc.IccServiceInfo;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

/**
 * This file is used to process EF related files for other module using
 */
public class MtkRuimRecords extends RuimRecords implements MtkIccConstants {
    static final String LOG_TAG = "MtkRuimRecords";

    // ***** Event Constants
    private static final int EVENT_GET_CDMA_ECC_DONE = 500;
    private static final int EVENT_GET_EST_DONE = 501;
    // PHB START @{
    private static final int EVENT_RADIO_STATE_CHANGED     = 502;
    private static final int EVENT_DELAYED_SEND_PHB_CHANGE = 503;
    private static final int EVENT_PHB_READY               = 504;
    // PHB END @}

    private static final int MCC_LEN = 3;
    private static final int IMSI_MIN_LEN = 6;
    private static final int IMSI_MAX_LEN = 15;

    private static final int MAX_DECIMAL_NUMBER = 9;
    private static final int ECC_LEN_BYTE = 3;
    private static final int MASK_CODE = 0xf;

    private static final int RUIM_FDN_SERVICE_MASK_EXIST_ACTIVE = 0x30;
    private static final int RUIM_FDN_SERVICE_MASK_EXIST_INACTIVE = 0x10;
    private static final int CSIM_FDN_SERVICE_MASK_EXIST = 0x02;
    private static final int CSIM_FDN_SERVICE_MASK_ACTIVE = 0x01;

    private String mRuimImsi = null;
    private String mEfEcc = "";
    private int mPhoneId = -1;
    private Phone mPhone;
    private byte[] mSimService;
    private byte[] mEnableService;
    // Send PHB ready after boot complete
    private boolean mPendingPhbNotify = false;

    private static final String[]  PROPERTY_RIL_FULL_UICC_TYPE = {
        "gsm.ril.fulluicctype",
        "gsm.ril.fulluicctype.2",
        "gsm.ril.fulluicctype.3",
        "gsm.ril.fulluicctype.4",
    };
    private String[] RUIMRECORDS_PROPERTY_ECC_LIST = {
        "ril.cdma.ecclist",
        "ril.cdma.ecclist1",
        "ril.cdma.ecclist2",
        "ril.cdma.ecclist3",
    };

    // PHB START @{
    // Use different system properties to indicate the GSM/C2K PHB ready state
    static final String[] PROPERTY_RIL_C2K_PHB_READY  = {
        "cdma.sim.ril.phbready",
        "cdma.sim.ril.phbready.2",
        "cdma.sim.ril.phbready.3",
        "cdma.sim.ril.phbready.4"
    };

    static final String[] PROPERTY_RIL_GSM_PHB_READY  = {
        "gsm.sim.ril.phbready",
        "gsm.sim.ril.phbready.2",
        "gsm.sim.ril.phbready.3",
        "gsm.sim.ril.phbready.4"
    };

    /* To distinguish the PHB event between GSM and C2K,
       we take the first bit as the ready/not ready value
       and the second bit as the type: 0 for GSM and 1 for C2K
    */
    public static final int GSM_PHB_NOT_READY     = 0;
    public static final int GSM_PHB_READY         = 1;
    public static final int C2K_PHB_NOT_READY     = 2;
    public static final int C2K_PHB_READY         = 3;

    public static final int PHB_DELAY_SEND_TIME = 500;

    private int mSubId = -1;
    private boolean mPhbReady = false;
    private boolean mPhbWaitSub = false;
    private boolean mDispose = false;
    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver () {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TelephonyIntents.ACTION_RADIO_TECHNOLOGY_CHANGED)) {
                // listener radio technology changed. If it not own object phone
                // broadcast false. if it own object, send delay message to broadcast PHB_CHANGE
                // event. APP will receive PHB_CHANGE broadcast and init phonebook.
                int phoneId = intent.getIntExtra(PhoneConstants.PHONE_KEY, -1);
                log("[onReceive] ACTION_RADIO_TECHNOLOGY_CHANGED phoneId : " + phoneId);
                if (null != mParentApp && mParentApp.getPhoneId() == phoneId) {
                    String activePhoneName = intent.getStringExtra(PhoneConstants.PHONE_NAME_KEY);
                    int subId = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY, -1);
                    log("[onReceive] ACTION_RADIO_TECHNOLOGY_CHANGED activePhoneName: " +
                            activePhoneName + ", subId : " + subId + ", phoneId: " + phoneId);
                    if ("CDMA".equals(activePhoneName)) {
                        sendMessageDelayed(obtainMessage(EVENT_DELAYED_SEND_PHB_CHANGE),
                                PHB_DELAY_SEND_TIME);
                        mAdnCache.reset();
                    }
                }
            } else if (action.equals(TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED)
                    && mParentApp != null) {
                // Handle sub ready event to notify phb ready event which not
                // sent to application before because sub not ready
                log("[onReceive] onReceive ACTION_SUBINFO_RECORD_UPDATED mPhbWaitSub: "
                        + mPhbWaitSub);
                if (mPhbWaitSub == true) {
                    mPhbWaitSub = false;
                    broadcastPhbStateChangedIntent(mPhbReady);
                    return;
                }
            } else if (action.equals(TelephonyIntents.ACTION_SIM_STATE_CHANGED)
                    && mParentApp != null) {
                int slot = intent.getIntExtra(PhoneConstants.SLOT_KEY, PhoneConstants.SIM_ID_1);
                String simState = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
                if (slot == mPhoneId) {
                    String strPhbReady = null;
                    if (CsimPhbUtil.isUsingGsmPhbReady(mFh)) {
                        strPhbReady = SystemProperties.get(PROPERTY_RIL_GSM_PHB_READY[mPhoneId],
                                "false");
                    } else {
                        strPhbReady = SystemProperties.get(PROPERTY_RIL_C2K_PHB_READY[mPhoneId],
                                "false");
                    }
                    //Update phb ready by sim state.
                    log("sim state: " + simState + ", mPhbReady: " + mPhbReady +
                            ",strPhbReady: " + strPhbReady.equals("true"));
                    if (IccCardConstants.INTENT_VALUE_ICC_READY.equals(simState)) {
                        if (false == mPhbReady && strPhbReady.equals("true")) {
                            mPhbReady = true;
                            broadcastPhbStateChangedIntent(mPhbReady);
                        } else if (true == mPhbWaitSub && strPhbReady.equals("true")) {
                            log("mPhbWaitSub is " + mPhbWaitSub + ", broadcast if need");
                            mPhbWaitSub = false;
                            broadcastPhbStateChangedIntent(mPhbReady);
                        }
                    }
                }
            } else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
                log("[onReceive] ACTION_BOOT_COMPLETED mPendingPhbNotify : " + mPendingPhbNotify);
                if (mPendingPhbNotify) {
                    broadcastPhbStateChangedIntent(isPhbReady());
                    mPendingPhbNotify = false;
                }
            }
        }
    };
    // PHB END @}

    public MtkRuimRecords(MtkUiccCardApplication app, Context c, CommandsInterface ci) {
        super(app, c, ci);
        mPhoneId = app.getPhoneId();
        mPhone = PhoneFactory.getPhone(app.getPhoneId());
        if (DBG) log("MtkRuimRecords X ctor this=" + this);

        // PHB START @{
        mAdnCache = new MtkAdnRecordCache(mFh, ci, app);
        ((MtkRIL)mCi).registerForPhbReady(this, EVENT_PHB_READY, null);
        mCi.registerForRadioStateChanged(this, EVENT_RADIO_STATE_CHANGED, null);

        mAdnCache.reset();

        IntentFilter filter = new IntentFilter();
        filter.addAction(TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED);
        filter.addAction(TelephonyIntents.ACTION_RADIO_TECHNOLOGY_CHANGED);
        filter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        filter.addAction(Intent.ACTION_BOOT_COMPLETED);
        mContext.registerReceiver(mIntentReceiver, filter);

        // mAdnCache is needed before onUpdateIccAvailability.
        if (DBG) log("updateIccRecords in IccPhoneBookeInterfaceManager");
        if (mPhone.getIccPhoneBookInterfaceManager() != null) {
            mPhone.getIccPhoneBookInterfaceManager().updateIccRecords(this);
        }

        // Check if phb is ready or not, if phb was already ready,
        // we won't wait for phb ready
        if (isPhbReady()) {
            broadcastPhbStateChangedIntent(isPhbReady());
        }
        // PHB END
    }

    @Override
    public void dispose() {
        if (DBG) log("Disposing MtkRuimRecords " + this);
        // MTK-START
        mDispose = true;
        if (!isCdma4GDualModeCard()) {
            log("dispose, reset operator numeric, name and country iso");
            mTelephonyManager.setSimOperatorNumericForPhone(mParentApp.getPhoneId(), "");
            mTelephonyManager.setSimOperatorNameForPhone(mParentApp.getPhoneId(), "");
            mTelephonyManager.setSimCountryIsoForPhone(mParentApp.getPhoneId(),  "");
        }

        // PHB START @{
        if (mPhbReady) {
            log("Disposing RuimRecords set PHB unready");
            mPhbReady = false;
            broadcastPhbStateChangedIntent(mPhbReady);
        }
        mParentApp.unregisterForReady(this);
        mPhbWaitSub = false;
        mCi.unregisterForRadioStateChanged(this);
        ((MtkRIL)mCi).unregisterForPhbReady(this);
        mContext.unregisterReceiver(mIntentReceiver);
        //alps01292449 when radio off, no need reset adnCache
        mAdnCache.reset();

        mPendingPhbNotify = false;
        // PHB END @}

        super.dispose();
    }

    @Override
    protected void resetRecords() {
        super.resetRecords();
    }

    @Override
    public String getOperatorNumeric() {
        if (mImsi == null) {
            return null;
        }

        if (mMncLength != UNINITIALIZED && mMncLength != UNKNOWN) {
            return mImsi.substring(0, MCC_LEN + mMncLength);
        }
        int mcc = Integer.parseInt(mImsi.substring(0, MCC_LEN));
        return mImsi.substring(0, MCC_LEN + MccTable.smallestDigitsMccForMnc(mcc));
    }

    @Override
    public void handleMessage(Message msg) {
        AsyncResult ar;

        byte data[];

        boolean isRecordLoadResponse = false;

        if (mDestroyed.get()) {
            loge("Received message " + msg +
                    "[" + msg.what + "] while being destroyed. Ignoring.");
            return;
        }

        try {
            switch (msg.what) {
            case EVENT_APP_READY:
                onReady();
                if (DBG) log("handleMessage (EVENT_APP_READY)");
                fetchEccList();
                break;

            /* IO events */
            case EVENT_GET_IMSI_DONE:
                isRecordLoadResponse = true;

                ar = (AsyncResult)msg.obj;
                if (ar.exception != null) {
                    loge("Exception querying IMSI, Exception:" + ar.exception);
                    break;
                }

                mImsi = (String) ar.result;

                // IMSI (MCC+MNC+MSIN) is at least 6 digits, but not more
                // than 15 (and usually 15).
                if (mImsi != null && (mImsi.length() < IMSI_MIN_LEN
                        || mImsi.length() > IMSI_MAX_LEN)) {
                    loge("invalid IMSI " + mImsi);
                    mImsi = null;
                }
                // MTK-START
                if (mImsi != null) {
                    log("IMSI: " + mImsi.substring(0, IMSI_MIN_LEN) + "xxxxxxxxx");
                }

                String operatorNumeric = getRUIMOperatorNumeric();
                if (operatorNumeric != null) {
                    if (operatorNumeric.length() <= IMSI_MIN_LEN) {
                        log("update mccmnc=" + operatorNumeric);
                        MccTable.updateMccMncConfiguration(mContext, operatorNumeric, false);
                    }
                }
                if (mImsi != null && (!mImsi.equals("") && mImsi.length() >= MCC_LEN)) {
                    SystemProperties.set("cdma.icc.operator.mcc", mImsi.substring(0, MCC_LEN));
                }
                if ((mImsi != null) && !mImsi.equals(mRuimImsi)) {
                    mRuimImsi = mImsi;
                    mImsiReadyRegistrants.notifyRegistrants();
                    log("MtkRuimRecords: mImsiReadyRegistrants.notifyRegistrants");
                }
                // MTK-END
            break;

            // TODO: probably EF_CST should be read instead
            case EVENT_GET_SST_DONE:
                log("Event EVENT_GET_SST_DONE Received");
                // MTK-START
                isRecordLoadResponse = true;
                ar = (AsyncResult)msg.obj;
                if (ar.exception != null) {
                    logi("EVENT_GET_SST_DONE failed");
                    break;
                }
                mSimService = (byte[])ar.result;

                log("mSimService[0]: " + mSimService[0] + ", data.length: " + mSimService.length);
                updateIccFdnStatus();
                // MTK-END
                break;

            // For SIM PHB - FDN
            case EVENT_GET_EST_DONE:
                isRecordLoadResponse = true;
                log("Event EVENT_GET_EST_DONE Received");
                ar = (AsyncResult)msg.obj;
                if (ar.exception != null) {
                    logi("EVENT_GET_EST_DONE failed");
                    break;
                }
                mEnableService = (byte[])ar.result;

                log("mEnableService[0]: " + mEnableService[0] +
                        ", mEnableService.length: " + mEnableService.length);
                updateIccFdnStatus();
                break;

            case EVENT_GET_CDMA_ECC_DONE:
                if (DBG) log("handleMessage (EVENT_GET_CDMA_ECC_DONE)");

                ar = (AsyncResult) msg.obj;
                if (ar.exception != null) {
                    if (DBG) loge("Get CDMA ecc with exception: " + ar.exception);
                    break;
                }
                data = (byte[]) ar.result;
                for (int i = 0 ; i + (ECC_LEN_BYTE - 1) < data.length ; i += ECC_LEN_BYTE) {
                    if ((data[i] & MASK_CODE) > MAX_DECIMAL_NUMBER) {
                        log("Skip tailing byte");
                        break;
                    }
                    String eccNum;
                    eccNum = IccUtils.cdmaBcdToString(data, i, ECC_LEN_BYTE);
                    if (eccNum != null && !eccNum.equals("") && !mEfEcc.equals("")) {
                        mEfEcc = mEfEcc + ",";
                    }
                    mEfEcc = mEfEcc + eccNum ;
                }
                if (DBG) log("CDMA mEfEcc is " + mEfEcc);

                SystemProperties.set(RUIMRECORDS_PROPERTY_ECC_LIST[mPhoneId], mEfEcc);
                break;

            // PHB START @{
            case EVENT_PHB_READY:
                ar = (AsyncResult) msg.obj;
                log("[DBG]EVENT_PHB_READY ar:" + ar);
                if (ar != null && ar.exception == null && ar.result != null) {
                    int[] isPhbReady = (int[]) ar.result;
                    String strAllSimState = "";
                    String strCurSimState = "";
                    boolean isSimLocked = false;
                    int phoneId = mParentApp.getPhoneId();
                    strAllSimState = SystemProperties.get(TelephonyProperties.PROPERTY_SIM_STATE);
                    if ((strAllSimState != null) && (strAllSimState.length() > 0)) {
                        String values[] = strAllSimState.split(",");
                        if ((phoneId >= 0) && (phoneId < values.length)
                                && (values[phoneId] != null)) {
                            strCurSimState = values[phoneId];
                        }
                    }

                    // if power on and in PUK_REQUIRED state, ap will not receive phb ready.
                    isSimLocked = (strCurSimState.equals("NETWORK_LOCKED") || strCurSimState
                            .equals("PIN_REQUIRED"));
                    updatePhbStatus(isPhbReady[0], isSimLocked);
                    updateIccFdnStatus();
                }
                break;

            case EVENT_DELAYED_SEND_PHB_CHANGE:
                boolean isReady = isPhbReady();
                log("[EVENT_DELAYED_SEND_PHB_CHANGE] isReady : " + isReady);
                broadcastPhbStateChangedIntent(isReady);
                break;
            // PHB END @}

            default:
                super.handleMessage(msg);   // call super to handles generic record load responses
            }
        } catch (RuntimeException exc) {
            // I don't want these exceptions to be fatal
            Rlog.w(LOG_TAG, "Exception parsing RUIM record", exc);
        } finally {
            // Count up record load responses even if they are fails
            if (isRecordLoadResponse) {
                onRecordLoaded();
            }
        }
    }

    @Override
    protected void onAllRecordsLoaded() {
        if (DBG) log("record load complete");

        // Further records that can be inserted are Operator/OEM dependent
        // FIXME: CSIM IMSI may not contain the MNC.
        // MTK-START
        if ((mPhone.getPhoneType() == PhoneConstants.PHONE_TYPE_CDMA) && isCdmaOnly()) {
        // MTK-END
            String operator = getRUIMOperatorNumeric();
            if (!TextUtils.isEmpty(operator)) {
                log("onAllRecordsLoaded set 'gsm.sim.operator.numeric' to operator='" +
                        operator + "'");
                mTelephonyManager.setSimOperatorNumericForPhone(
                        mParentApp.getPhoneId(), operator);
            } else {
                log("onAllRecordsLoaded empty 'gsm.sim.operator.numeric' skipping");
            }

            if (!TextUtils.isEmpty(mImsi)) {
                log("onAllRecordsLoaded set mcc imsi=" + (VDBG ? ("=" + mImsi) : ""));
                mTelephonyManager.setSimCountryIsoForPhone(
                        mParentApp.getPhoneId(),
                        MccTable.countryCodeForMcc(
                        Integer.parseInt(mImsi.substring(0,MCC_LEN))));
            } else {
                log("onAllRecordsLoaded empty imsi skipping setting mcc");
            }
        }

        Resources resource = Resources.getSystem();
        if (resource.getBoolean(com.android.internal.R.bool.config_use_sim_language_file)) {
            setSimLanguage(mEFli, mEFpl);
        }

        mRecordsLoadedRegistrants.notifyRegistrants(
            new AsyncResult(null, null, null));

        // TODO: The below is hacky since the SubscriptionController may not be ready at this time.
        if (!TextUtils.isEmpty(mMdn)) {
            int phoneId = mParentApp.getUiccCard().getPhoneId();
            int subId = SubscriptionController.getInstance().getSubIdUsingPhoneId(phoneId);
            if (SubscriptionManager.isValidSubscriptionId(subId)) {
                SubscriptionManager.from(mContext).setDisplayNumber(mMdn, subId);
            } else {
                log("Cannot call setDisplayNumber: invalid subId");
            }
        }
    }

    @Override
    protected void fetchRuimRecords() {
        super.fetchRuimRecords();
        // For SIM PHB - FDN
        if (DBG) log("fetchRuimRecords mParentApp.getType() = " + mParentApp.getType());
        if (mParentApp.getType() == AppType.APPTYPE_RUIM) {
            mFh.loadEFTransparent(EF_CST, obtainMessage(EVENT_GET_SST_DONE));
            mRecordsToLoad++;
        } else if (mParentApp.getType() == AppType.APPTYPE_CSIM) {
            mFh.loadEFTransparent(EF_CST, obtainMessage(EVENT_GET_SST_DONE));
            mRecordsToLoad++;
            mFh.loadEFTransparent(EF_EST, obtainMessage(EVENT_GET_EST_DONE));
            mRecordsToLoad++;
        }
        if (DBG) log("fetchRuimRecords " + mRecordsToLoad + " requested: " + mRecordsRequested);
    }

    /**
     * {@inheritDoc}
     *
     * Display rule for RUIMs.
     */
    @Override
    public int getDisplayRule(String plmn) {
        int rule;
        String spn = getServiceProviderName();
        log("getDisplayRule mParentApp is " +
            ((mParentApp != null) ? mParentApp : "null"));

        if (mParentApp != null && mParentApp.getUiccCard() != null &&
              mParentApp.getUiccCard().getOperatorBrandOverride() != null) {
            // If the operator has been overridden, treat as the SPN file on the SIM did not exist.
            log("getDisplayRule, getOperatorBrandOverride is not null");
            rule = SPN_RULE_SHOW_PLMN;
        } else if (mCsimSpnDisplayCondition == false) {
            // SPN is not required to display
            log("getDisplayRule, no EF_SPN");
            rule = SPN_RULE_SHOW_PLMN;
        } else if (!TextUtils.isEmpty(spn) && !spn.equals("")) {
            log("getDisplayRule, show spn");
            rule = SPN_RULE_SHOW_SPN;
        } else {
            log("getDisplayRule, show plmn");
            rule = SPN_RULE_SHOW_PLMN;
        }
        return rule;
    }

    @Override
    protected void log(String s) {
        Rlog.d(LOG_TAG, "[MtkRuimRecords] " + s + " (phoneId " + mPhoneId + ")");
    }

    @Override
    protected void loge(String s) {
        Rlog.e(LOG_TAG, "[MtkRuimRecords] " + s + " (phoneId " + mPhoneId + ")");
    }

    protected void logi(String s) {
        Rlog.i(LOG_TAG, "[MtkRuimRecords] " + s + " (phoneId " + mPhoneId + ")");
    }

    private void fetchEccList() {
        // [Backward compatible]
        // If "ril.ef.ecc.support" is "1", means modem will send URC to notify
        // EF_ECC's value and phone number utility module will take over the
        // system properties.
        int eccFromModemUrc = SystemProperties.getInt("ril.ef.ecc.support", 0);
        if (DBG) {
            log("fetchEccList(), eccFromModemUrc:" + eccFromModemUrc);
        }
        if (eccFromModemUrc == 0) {
            mEfEcc = "";
            mFh.loadEFTransparent(EF_CDMA_ECC, obtainMessage(EVENT_GET_CDMA_ECC_DONE));
        }
    }

    public IccServiceInfo.IccServiceStatus getSIMServiceStatus(
            IccServiceInfo.IccService enService) {
        IccServiceInfo.IccServiceStatus simServiceStatus = IccServiceInfo.IccServiceStatus.UNKNOWN;
        if (mParentApp == null) {
            log("getSIMServiceStatus enService: " + enService + ", mParentApp = null.");
            return simServiceStatus;
        } else {
            log("getSIMServiceStatus enService: " + enService + ", mParentApp.getType(): "
                    + mParentApp.getType());
        }
        if (enService == IccServiceInfo.IccService.FDN && mSimService != null
                && mParentApp.getType() == AppType.APPTYPE_RUIM) {
            log("getSIMServiceStatus mSimService[0]: " + mSimService[0]);
            if (((int)mSimService[0] & RUIM_FDN_SERVICE_MASK_EXIST_ACTIVE)
                    == RUIM_FDN_SERVICE_MASK_EXIST_ACTIVE) {
                simServiceStatus = IccServiceInfo.IccServiceStatus.ACTIVATED;
            } else if (((int)mSimService[0] & RUIM_FDN_SERVICE_MASK_EXIST_INACTIVE)
                    == RUIM_FDN_SERVICE_MASK_EXIST_INACTIVE) {
                simServiceStatus = IccServiceInfo.IccServiceStatus.INACTIVATED;
            } else {
                // FIXME: AOSP has no IccServiceInfo.IccServiceStatus.NOT_EXIST_IN_RUIM;
                simServiceStatus = IccServiceInfo.IccServiceStatus.NOT_EXIST_IN_SIM;
            }
        } else if (enService == IccServiceInfo.IccService.FDN && mSimService != null
                && mEnableService != null && mParentApp.getType() == AppType.APPTYPE_CSIM) {
            log("getSIMServiceStatus mSimService[0]: " + mSimService[0] +
            ", mEnableService[0]: " + mEnableService[0]);
            // FIXME: C.S0065 FDN allocate state is in bit 2 of EFcsim_st, but FDN enable state
            // is in bit 1 of EFest, seems something wrong, need confirm with MD
            if (((int)mSimService[0] & CSIM_FDN_SERVICE_MASK_EXIST) == CSIM_FDN_SERVICE_MASK_EXIST
                    && ((int)mEnableService[0] & CSIM_FDN_SERVICE_MASK_ACTIVE)
                    == CSIM_FDN_SERVICE_MASK_ACTIVE) {
                simServiceStatus = IccServiceInfo.IccServiceStatus.ACTIVATED;
            } else if (((int)mSimService[0] & CSIM_FDN_SERVICE_MASK_EXIST)
                    == CSIM_FDN_SERVICE_MASK_EXIST) {
                simServiceStatus = IccServiceInfo.IccServiceStatus.INACTIVATED;
            } else {
                // FIXME: AOSP has no IccServiceInfo.IccServiceStatus.NOT_EXIST_IN_CSIM;
                simServiceStatus = IccServiceInfo.IccServiceStatus.NOT_EXIST_IN_USIM;
            }
        }
        return simServiceStatus;
    }

    /**
     * Check if current card type is cdma only card.
     * @return true : cdma only card  false: not cdma only card
     */
    public boolean isCdmaOnly() {
        String prop = null;
        String values[] = null;

        if (mPhoneId < 0 || mPhoneId >= PROPERTY_RIL_FULL_UICC_TYPE.length) {
            log("isCdmaOnly: invalid PhoneId " + mPhoneId);
            return false;
        }
        prop = SystemProperties.get(PROPERTY_RIL_FULL_UICC_TYPE[mPhoneId]);
        if ((prop != null) && (prop.length() > 0)) {
            values = prop.split(",");
        }
        log("isCdmaOnly PhoneId " + mPhoneId + ", prop value= " + prop +
                ", size= " + ((values != null) ? values.length : 0));
        if (values != null) {
            return !(Arrays.asList(values).contains("USIM")
                    || Arrays.asList(values).contains("SIM"));
        } else {
            return false;
        }
    }

    /**
     * Check if card type is cdma 4G dual mode card.
     * @return true : cdma 4G dual card  false: not
     */
    public boolean isCdma4GDualModeCard() {
        String prop = null;
        String values[] = null;

        if (mPhoneId < 0 || mPhoneId >= PROPERTY_RIL_FULL_UICC_TYPE.length) {
            log("isCdma4GDualModeCard: invalid PhoneId " + mPhoneId);
            return false;
        }
        prop = SystemProperties.get(PROPERTY_RIL_FULL_UICC_TYPE[mPhoneId]);
        if ((prop != null) && (prop.length() > 0)) {
            values = prop.split(",");
        }
        log("isCdma4GDualModeCard PhoneId " + mPhoneId + ", prop value= " + prop +
                ", size= " + ((values != null) ? values.length : 0));
        if (values != null) {
            return (Arrays.asList(values).contains("USIM")
                    && Arrays.asList(values).contains("CSIM"));
        } else {
            return false;
        }
    }

    // FDN status in modem3 should query after PHB ready
    protected void updateIccFdnStatus() {
        log("updateIccFdnStatus mParentAPP=" + mParentApp
                + "  getSIMServiceStatus(Phone.IccService.FDN)="
                + getSIMServiceStatus(IccServiceInfo.IccService.FDN)
                + "  IccServiceStatus.ACTIVATE="
                + IccServiceInfo.IccServiceStatus.ACTIVATED);
        if (mParentApp != null
                && (getSIMServiceStatus(IccServiceInfo.IccService.FDN)
                == IccServiceInfo.IccServiceStatus.ACTIVATED)) {
            mParentApp.queryFdn();
        }
    }

    // PHB START
    public boolean isPhbReady() {
        if (DBG) log("[isPhbReady] Start mPhbReady: " + (mPhbReady ? "true" : "false"));
        String strPhbReady = "false";
        String strAllSimState = "";
        String strCurSimState = "";
        boolean isSimLocked = false;
        if (mParentApp == null) {
            return false;
        }
        int phoneId = mParentApp.getPhoneId();

        if (CsimPhbUtil.isUsingGsmPhbReady(mFh)) {
            strPhbReady = SystemProperties.get(PROPERTY_RIL_GSM_PHB_READY[mPhoneId], "false");
        } else {
            strPhbReady = SystemProperties.get(PROPERTY_RIL_C2K_PHB_READY[mPhoneId], "false");
        }
        strAllSimState = SystemProperties.get(TelephonyProperties.PROPERTY_SIM_STATE);

        if ((strAllSimState != null) && (strAllSimState.length() > 0)) {
            String values[] = strAllSimState.split(",");
            if ((phoneId >= 0) && (phoneId < values.length) && (values[phoneId] != null)) {
                strCurSimState = values[phoneId];
            }
        }

        isSimLocked = (strCurSimState.equals("NETWORK_LOCKED") ||
                       strCurSimState.equals("PIN_REQUIRED"));
        //In PUK_REQUIRED state, phb can be accessed.

        // Use different system properties to indicate the GSM/C2K PHB
        // ready state, so we can use system property to check the state.
        if (strPhbReady.equals("true") && false == isSimLocked) {
            mPhbReady = true;
        } else {
            mPhbReady = false;
        }

        if (DBG) {
            log("[isPhbReady] End mPhbReady: " + (mPhbReady ? "true" : "false")
                    + ", strCurSimState: " + strCurSimState);
        }
        return mPhbReady;
    }

    private void broadcastPhbStateChangedIntent(boolean isReady) {
        // Avoid repeate intent from GSMPhone and CDMAPhone
        // For RuimRecords dispose case, ignore the condition because the phone type may be changed
        if ((mPhone.getPhoneType() != PhoneConstants.PHONE_TYPE_CDMA) && !(mDispose && !isReady)) {
            log("broadcastPhbStateChangedIntent, Not active Phone.");
            return;
        }

        log("broadcastPhbStateChangedIntent, mPhbReady " + mPhbReady);
        if (isReady == true) {
            int[] subIds = SubscriptionManager.getSubId(mPhoneId);
            if (subIds != null && subIds.length > 0) {
                mSubId = subIds[0];
            }

            if (mSubId <= 0) {
                log("broadcastPhbStateChangedIntent, mSubId <= 0");
                mPhbWaitSub = true;
                return;
            }
        } else {
            if (mSubId <= 0) {
                log("broadcastPhbStateChangedIntent, isReady == false and mSubId <= 0");
                return;
            }
        }

        if (!SystemProperties.get("sys.boot_completed").equals("1")) {
            log("broadcastPhbStateChangedIntent, boot not completed");
            mPendingPhbNotify = true;
            return;
        }

        Intent intent = new Intent(MtkTelephonyIntents.ACTION_PHB_STATE_CHANGED);
        intent.putExtra("ready", isReady);
        intent.putExtra(PhoneConstants.SUBSCRIPTION_KEY, mSubId);
        if (DBG) log("Broadcasting intent ACTION_PHB_STATE_CHANGED " + isReady
                    + " sub id " + mSubId + " phoneId " + mParentApp.getPhoneId());
        mContext.sendBroadcastAsUser(intent, UserHandle.ALL);

        if (isReady == false) {
            mSubId = -1;
        }
    }

    private void updatePhbStatus(int status, boolean isSimLocked) {
        log("[updatePhbStatus] status: " + status + ", isSimLocked: " + isSimLocked
               + ", mPhbReady: " + mPhbReady);
        boolean isReady = false;
        if (CsimPhbUtil.isUsingGsmPhbReady(mFh)) {
            if (status == GSM_PHB_READY) {
                isReady = true;
            } else if (status == GSM_PHB_NOT_READY) {
                isReady = false;
            } else {
                // not GSM PHB status
                log("[updatePhbStatus] not GSM PHB status");
                return;
            }
        } else {
            if (status == C2K_PHB_READY) {
                isReady = true;
            } else if (status == C2K_PHB_NOT_READY) {
                isReady = false;
            } else {
                // not C2K PHB status
                log("[updatePhbStatus] not C2K PHB status");
                return;
            }
        }

        if (isReady) {
            if (false == isSimLocked) {
                if (mPhbReady == false) {
                    mPhbReady = true;
                    broadcastPhbStateChangedIntent(mPhbReady);
                } else {
                    broadcastPhbStateChangedIntent(mPhbReady);
                }
            } else {
                log("[updatePhbStatus] phb ready but sim is not ready.");
                mPhbReady = false;
                broadcastPhbStateChangedIntent(mPhbReady);
            }
        } else {
            if (mPhbReady == true) {
                mAdnCache.reset();
                mPhbReady = false;
                broadcastPhbStateChangedIntent(mPhbReady);
            } else {
                broadcastPhbStateChangedIntent(mPhbReady);
            }
        }
    }
    // PHB END @}

    @Override
    protected void handleRuimRefresh(IccRefreshResponse refreshResponse) {

        if (refreshResponse == null) {
            if (DBG)
                log("mtk handleRuimRefresh received without input");
            return;
        }

        if (!TextUtils.isEmpty(refreshResponse.aid)
                && !refreshResponse.aid.equals(mParentApp.getAid())) {
            // This is for different app. Ignore.
            return;
        }

        switch (refreshResponse.refreshResult) {
            case MtkIccRefreshResponse.REFRESH_INIT_FULL_FILE_UPDATED:
                if (DBG) {
                    log("mtk handleRuimRefresh with REFRESH_INIT_FULL_FILE_UPDATED");
                }
                onIccRefreshInit();
                break;
            default:
                super.handleRuimRefresh(refreshResponse);
        }
    }
}
