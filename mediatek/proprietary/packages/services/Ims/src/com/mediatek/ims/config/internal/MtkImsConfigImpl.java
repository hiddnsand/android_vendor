package com.mediatek.ims.config.internal;

import android.content.Context;
import android.os.Binder;
import android.os.Build;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.telephony.ims.stub.ImsConfigImplBase;
import android.util.Log;

import com.android.ims.ImsConfig;
import com.android.ims.ImsConfigListener;
import com.android.ims.ImsException;
import com.android.ims.ImsReasonInfo;
import com.android.ims.internal.IImsConfig;

import com.android.internal.telephony.IccCardConstants;

import com.mediatek.ims.internal.IMtkImsConfig;
import com.mediatek.ims.ril.ImsCommandsInterface;

import java.util.HashMap;

/**
 * Class handles IMS parameter provisioning by carrier.
 *
 *  @hide
 */
public class MtkImsConfigImpl extends IMtkImsConfig.Stub {
    private static final String TAG = "MtkImsConfigImpl";
    private static final String PROP_FORCE_DEBUG_KEY = "persist.log.tag.tel_dbg";
    private static final boolean DEBUG = TextUtils.equals(Build.TYPE, "eng")
            || (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1);

    private Context mContext;
    private int mPhoneId;
    private ImsCommandsInterface mRilAdapter;
    private ImsConfigStorage mStorage = null;
    private ImsConfig mImsConfig = null;

    /// M: Dynamic IMS Switch @{
    /// <ImsConfig.FeatureConstants, ImsConfig.FeatureValueConstants>
    private HashMap<Integer, Boolean> mImsCapabilitiesIsCache =
            new HashMap<Integer, Boolean>();
    private HashMap<Integer, Integer> mImsCapabilities =
            new HashMap<Integer, Integer>();
    /// @}

    private MtkImsConfigImpl() {};

    /**
     *
     * Construction function for MtkImsConfigImpl.
     *
     * @param context the application context
     * @param phoneId the phone id this instance handle for
     *
     */
    public MtkImsConfigImpl(Context context, ImsCommandsInterface imsRilAdapter,
                            ImsConfig imsConfig, ImsConfigStorage storage, int phoneId) {
        mContext = context;
        mPhoneId = phoneId;
        mRilAdapter = imsRilAdapter;
        mImsConfig = imsConfig;
        mStorage = storage;

        // M: Dynamic IMS Switch, default volte only. @{
        mImsCapabilities.put(ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE,
                ImsConfig.FeatureValueConstants.ON);
        mImsCapabilities.put(ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE,
                ImsConfig.FeatureValueConstants.OFF);
        mImsCapabilities.put(ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI,
                ImsConfig.FeatureValueConstants.OFF);
        /// @}

        // M: Use cache to reduce access DB frequently. @{
        mImsCapabilitiesIsCache.put(ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE, false);
        mImsCapabilitiesIsCache.put(ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE, false);
        mImsCapabilitiesIsCache.put(ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI, false);
        /// @}
    }

    /**
     * Gets the value for ims service/capabilities parameters from the provisioned
     * value storage. Synchronous blocking call.
     *
     * @param item, as defined in com.android.ims.ImsConfig#ConfigConstants.
     * @return value in Integer format.
     */
    @Override
    public int getProvisionedValue(int item) {
        try {
            int result = mImsConfig.getProvisionedValue(item);
            Log.i(TAG, "getProvisionedValue(" + item + ") : " + result +
                    " on phone" + mPhoneId + " from binder pid " + Binder.getCallingPid() +
                    ", binder uid " + Binder.getCallingUid() + ", process pid " + Process.myPid() +
                    ", process uid " + Process.myUid());

            return result;
        } catch (ImsException e) {
            Log.e(TAG, "getProvisionedValue(" + item + ") failed, code: " + e.getCode());
            throw new RuntimeException(e);
        }
    }

    /**
     * Gets the value for ims service/capabilities parameters from the provisioned
     * value storage. Synchronous blocking call.
     *
     * @param item, as defined in com.android.ims.ImsConfig#ConfigConstants.
     * @return value in String format.
     */
    @Override
    public String getProvisionedStringValue(int item) {
        try {
            String result = mImsConfig.getProvisionedStringValue(item);
            Log.i(TAG, "getProvisionedStringValue(" + item + ") : " + result +
                    " on phone " + mPhoneId + " from binder pid " + Binder.getCallingPid() +
                    ", binder uid " + Binder.getCallingUid() + ", process pid " + Process.myPid() +
                    ", process uid " + Process.myUid());

            return result;
        } catch (ImsException e) {
            Log.e(TAG, "getProvisionedStringValue(" + item + ") failed, code: " + e.getCode());
            throw new RuntimeException(e);
        }
    }

    /**
     * Sets the value for IMS service/capabilities parameters by the operator device
     * management entity. It sets the config item value in the provisioned storage
     * from which the master value is derived. Synchronous blocking call.
     *
     * @param item, as defined in com.android.ims.ImsConfig#ConfigConstants.
     * @param value in Integer format.
     * @return as defined in com.android.ims.ImsConfig#OperationStatusConstants.
     */
    @Override
    public int setProvisionedValue(int item, int value) {
        try {
            Log.i(TAG, "setProvisionedValue(" + item + ", " + value +
                    ") on phone " + mPhoneId + " from pid " + Binder.getCallingPid() +
                    ", uid " + Binder.getCallingUid());
            return mImsConfig.setProvisionedValue(item, value);
        } catch (ImsException e) {
            Log.e(TAG, "setProvisionedValue(" + item + ") failed, code: " + e.getCode());
            return ImsConfig.OperationStatusConstants.FAILED;
        }
    }

    /**
     * Sets the value for IMS service/capabilities parameters by the operator device
     * management entity. It sets the config item value in the provisioned storage
     * from which the master value is derived.  Synchronous blocking call.
     *
     * @param item as defined in com.android.ims.ImsConfig#ConfigConstants.
     * @param value in String format.
     * @return as defined in com.android.ims.ImsConfig#OperationStatusConstants.
     */
    @Override
    public int setProvisionedStringValue(int item, String value) {
        try {
            Log.i(TAG, "setProvisionedStringValue(" + item + ", " + value +
                    ") on phone " + mPhoneId + " from pid " + Binder.getCallingPid() +
                    ", uid " + Binder.getCallingUid());
            return mImsConfig.setProvisionedStringValue(item, value);
        } catch (ImsException e) {
            Log.e(TAG, "setProvisionedValue(" + item + ") failed, code: " + e.getCode());
            return ImsConfig.OperationStatusConstants.FAILED;
        }
    }

    /**
     * Gets the value of the specified IMS feature item for specified network type.
     * This operation gets the feature config value from the master storage (i.e. final
     * value). Asynchronous non-blocking call.
     *
     * @param feature as defined in com.android.ims.ImsConfig#FeatureConstants.
     * @param network as defined in android.telephony.TelephonyManager#NETWORK_TYPE_XXX.
     * @param listener feature value returned asynchronously through listener.
     */
    @Override
    public void getFeatureValue(int feature, int network, ImsConfigListener listener) {
        try {
            Log.i(TAG, "getFeatureValue(" + feature + ", " + network + ") : on phone " + mPhoneId);
            mImsConfig.getFeatureValue(feature, network, listener);
        } catch (ImsException e) {
            Log.e(TAG, "getFeatureValue(" + feature + ") remote failed!");
            throw new RuntimeException(e);
        }
    }

    /**
     * Sets the value for IMS feature item for specified network type.
     * This operation stores the user setting in setting db from which master db
     * is derived.
     *
     * @param feature as defined in com.android.ims.ImsConfig#FeatureConstants.
     * @param network as defined in android.telephony.TelephonyManager#NETWORK_TYPE_XXX.
     * @param value as defined in com.android.ims.ImsConfig#FeatureValueConstants.
     * @param listener, provided if caller needs to be notified for set result.
     */
    @Override
    public void setFeatureValue(int feature, int network, int value, ImsConfigListener listener) {
        try {
            Log.i(TAG, "setFeatureValue(" + feature + ", " + network + ", " + value +
                  ") on phone " + mPhoneId + " from pid " + Binder.getCallingPid() +
                  ", uid " + Binder.getCallingUid() + ", listener " + listener);
            mImsConfig.setFeatureValue(feature, network, value, listener);
        } catch (ImsException e) {
            Log.e(TAG, "setFeatureValue(" + feature + ") remote failed!");
            throw new RuntimeException(e);
        }
    }

    /**
     * Gets the value for IMS feature item for video call quality.
     *
     * @param listener, provided if caller needs to be notified for set result.
     * @return void
     */
    @Override
    public void getVideoQuality(ImsConfigListener listener) {

    }

    /**
     * Sets the value for IMS feature item video quality.
     *
     * @param quality, defines the value of video quality.
     * @param listener, provided if caller needs to be notified for set result.
     * @return void
     */
    @Override
    public void setVideoQuality(int quality, ImsConfigListener listener) {

    }

    @Override
    public void setImsResCapability(int feature, int value) {
        // Still keep old design, store res value in runtime
        mImsCapabilities.put(feature, value);
        // Use cache to reduce access DB frequently
        mImsCapabilitiesIsCache.put(feature, true);

        // Resource value will be stored in ImsConfig DB
        try {
            Log.i(TAG, "setImsResCapability(" + feature + ") : " + value + " on phone " + mPhoneId +
                    " from binder pid " + Binder.getCallingPid() +
                    ", binder uid " + Binder.getCallingUid());
            mStorage.setImsResCapability(feature, value);
        } catch (ImsException e) {
            Log.e(TAG, "setImsResCapability(" + feature + ") failed, code: " + e.getCode());
        }
    }

    @Override
    public int getImsResCapability(int feature) {
        try {
            int value;

            if (mImsCapabilitiesIsCache.get(feature)) {
                value = mImsCapabilities.get(feature);
            } else {
                value = mStorage.getImsResCapability(feature);
            }

            Log.i(TAG, "getImsResCapability(" + feature + ") res value:" + value +
                    " on phone " + mPhoneId + " from binder pid " + Binder.getCallingPid() +
                    ", binder uid " + Binder.getCallingUid());

            if (value != ImsConfig.FeatureValueConstants.ON &&
                    value != ImsConfig.FeatureValueConstants.OFF) {
                throw new ImsException(" result value:" + value + " incorrect!",
                        ImsReasonInfo.CODE_LOCAL_ILLEGAL_ARGUMENT);
            }

            return value;
        } catch (ImsException e) {
            Log.e(TAG, "getImsResCapability(" + feature + ") failed, code: " + e.getCode());

            // no value return old design's default value, volte/vilte/wfc:[1/0/0]
            return mImsCapabilities.get(feature);
        }
    }

    @Override
    public void setWfcMode(int mode) {
        Log.i(TAG, "setWfcMode(" + mode + ")");

        // Default value of wfc mode is wifi prefer
        // ImsConfig.WfcModeFeatureValueConstants.WIFI_PREFERRED is mapping to 1 in MTK design
        int rilWfcMode = 1;

        switch (mode) {
            case ImsConfig.WfcModeFeatureValueConstants.WIFI_ONLY:
                rilWfcMode = 3;
                break;
            case ImsConfig.WfcModeFeatureValueConstants.CELLULAR_PREFERRED:
                rilWfcMode = 2;
                break;
            case ImsConfig.WfcModeFeatureValueConstants.WIFI_PREFERRED:
                rilWfcMode = 1;
                break;
            default:
                Log.i(TAG, "setWfcMode mapping error, value is invalid!");
                break;
        }
        mStorage.sendWfcProfileInfo(rilWfcMode);
    }

    /**
     * Sync Volte preference for voice domain to modem.
     * @param Volte preference mode.
     * @return void.
     */
    @Override
    public void setVoltePreference(int mode) {
        mStorage.setVoltePreference(mode);
    }

    /**
     * Update multiple Ims config to modem.
     * @param keys as multiple Ims config keys.
     * @param values as multiple Ims config values.
     * @return value in int array format (success is 0, fail is -1)
     */
    @Override
    public int[] setModemImsCfg(String[] keys, String[] values, int phoneId) {
        int[] result = null;
        int type = ImsConfigUtils.MdConfigType.TYPE_IMSCFG;

        Log.i(TAG, "setModemImsCfg phoneId:" + phoneId);
        result = mStorage.setModemImsCfg(keys, values, type, phoneId);

        return result;
    }

    /**
     * Update multiple Ims WO config to modem.
     * @param keys as multiple Ims WO config keys.
     * @param values as multiple Ims IWLAN config values.
     * @return value in int array format (success is 0, fail is -1)
     */
    @Override
    public int[] setModemImsWoCfg(String[] keys, String[] values, int phoneId) {
        int[] result = null;
        int type = ImsConfigUtils.MdConfigType.TYPE_IMSWOCFG;

        Log.i(TAG, "setModemImsWoCfg phoneId:" + phoneId);
        result = mStorage.setModemImsCfg(keys, values, type, phoneId);

        return result;
    }

    /**
     * Update multiple Ims IWLAN config to modem.
     * @param keys as multiple Ims IWLAN config keys.
     * @param values as multiple Ims IWLAN config values.
     * @return value in int array format (success is 0, fail is -1)
     */
    @Override
    public int[] setModemImsIwlanCfg(String[] keys, String[] values, int phoneId) {
        int[] result = null;
        int type = ImsConfigUtils.MdConfigType.TYPE_IMSIWLANCFG;

        Log.i(TAG, "setModemImsIwlanCfg phoneId:" + phoneId);
        result = mStorage.setModemImsCfg(keys, values, type, phoneId);

        return result;
    }
}
