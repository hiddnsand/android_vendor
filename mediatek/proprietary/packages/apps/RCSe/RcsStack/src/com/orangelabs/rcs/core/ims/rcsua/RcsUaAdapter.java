package com.orangelabs.rcs.core.ims.rcsua;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Looper;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.Handler;
import android.os.HwBinder;
import android.os.Message;

import com.android.ims.ImsCallProfile;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.ims.ImsServiceClass;
import com.orangelabs.rcs.core.CoreException;
import com.orangelabs.rcs.core.ims.ImsModule;
import com.orangelabs.rcs.core.ims.network.ImsNetworkInterface;
import com.orangelabs.rcs.core.ims.network.sip.FeatureTags;
import com.orangelabs.rcs.core.ims.protocol.sip.SipException;
import com.orangelabs.rcs.platform.AndroidFactory;
import com.orangelabs.rcs.platform.network.NetworkFactory;
import com.orangelabs.rcs.provider.settings.RcsSettings;
import com.orangelabs.rcs.provider.settings.RcsSettingsData;
import com.mediatek.internal.telephony.MtkPhoneConstants;

import java.lang.Thread.State;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;
import java.util.List;
import java.util.Scanner;
import java.io.InterruptedIOException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.DataInputStream;
import java.io.BufferedOutputStream;
import javax2.sip.ListeningPoint;
import android.telephony.ServiceState;
import android.text.TextUtils;

import com.android.ims.ImsReasonInfo;
import com.android.ims.ImsConfig;
import com.android.ims.ImsConnectionStateListener;
import com.android.ims.ImsServiceClass;
import com.orangelabs.rcs.utils.logger.Logger;
//import com.mediatek.ims.WfcReasonInfo;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import vendor.mediatek.hardware.rcs.V1_0.IRcs;
import vendor.mediatek.hardware.rcs.V1_0.IRcsIndication;

/**
 * The Class RcsUaAdapter.
 */
public class RcsUaAdapter {

    private static int MSG_ID_FOR_RCS_PROXY_TEST = 1;
    private static int MSG_ID_FOR_RCS_PROXY_TEST_RSP = 2;

    final static public int RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE = 3;
    final static public int RCS_PROXY_EVENT_REQ_REG_INFO = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 1; // registartion ino required
    final static public int RCS_PROXY_EVENT_RES_REG_INFO = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 2; //reg info response
    final static public int RCS_PROXY_EVENT_RCS_REGISTER = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 3; //reg info response
    final static public int RCS_PROXY_EVENT_RCS_DEREGISTER = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 4; //reg info response
    final static public int RCS_PROXY_EVENT_REQ_SIP_Send = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 5; //sip request sent
    final static public int RCS_PROXY_EVENT_RSP_SIP_Send = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 6; //SIp request response
    final static public int RCS_PROXY_EVENT_RSP_RCS_REGISTER = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 7; //reg info response
    final static public int RCS_PROXY_EVENT_RSP_RCS_DEREGISTER = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 8;
    final static public int RCS_PROXY_EVENT_RSP_RCS_REGISTERING = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 9; //reg info response
    final static public int RCS_PROXY_EVENT_RSP_RCS_DEREGISTERING = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 10;

    final static public int RCS_PROXY_EVENT_RCS_INIT = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 11;
    final static public int RCS_PROXY_EVENT_RCS_ACTIVE = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 12;
    final static public int RCS_PROXY_EVENT_RCS_INACTIVE = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 13;
    final static public int RCS_PROXY_EVENT_RCS_DEINIT = RCS_PROXY_EVENT_TO_RCSPROXY_START_CODE + 14;

    final static public int OOS_STATE = 7;
    private static boolean  hackyFlag = false;
    //IMS specific information
    private static String IMSProxyAddr = "";
    private static int IMSProxyPort = 0;
    private static String SIPDefaultProtocolForVoLTE = "UDP";
    private static String mLocalIPAddr = "";
    private static String mPrivateID = "";
    private static String mUserAgent = "";
    private static String mAssociatedUri = "";
    private static String mRoute = "";
    private static String mPANI = "";
    private static String[] mRoutes = {};
    private static String[] mAssociatedUris = {};
    private static String mInstanceId = "";
    private static int mTagStatus = 0;
    private static int mTagState = 0;
    private static int state=0;
    private static final String ACCESS_TYPE_PRELUDE="IEEE";
    private ImsConnectionStateListener mImsConnectionStateListener;
    /* Rcs HIDL */
    private volatile IRcs mRcsHal;
    private final IRcsDeathRecipient mIRcsDeathRecipient = new IRcsDeathRecipient();
    private final AtomicLong mRcsHalCookie = new AtomicLong(0);
    private RcsIndication mRcsIndication = new RcsIndication();
    public String[] getmAssociatedUri() {
        return mAssociatedUris;
    }

    public String getmInstanceId() {
        return mInstanceId;
    }

    public String[] getmRoute() {
        return mRoutes;
    }

    //RCS SIP stack related details
    private static int RCSSIPStackPort = 0;

    //intent
    public static final String VOLTE_SERVICE_NOTIFY_INTENT = "COM.MEDIATEK.RCS.IMS.VOLTE_SERVICE_NOTIFICATION";

    private Object mListenerLock = new Object();

    /**
     * The logger
     */
    private  Logger logger = Logger.getLogger(this.getClass().getName());

    //enums for registration state
    public enum IMSRegState {
        REGISTERED(1),
        UNREGISTERING(2),
        UNREGISTERED(3),
        REGISTERING(4),
        DISCONNECTED(5),
        AUTHENTICATING(6),
        OOS(7);

        private int state;

        private IMSRegState(int state) {
            this.state = state;
        }

        public int getState() {
            return this.state;
        }

        @Override
        public String toString() {
            return "" + this.state;
        }

        public boolean equals(String state) {
            return this.toString().equals(state);
        }
    }

    //RCS UA event
    /**
     * The Class RcsUaEvent.
     */
    public static class RcsUaEvent {
        public static final int MAX_DATA_LENGTH = 70960;
        private int requestId;
        private int dataLen;
        private int readOffset;
        private byte data[];
        private int eventmaxdataLen = MAX_DATA_LENGTH;

        /**
         * Instantiates a new rcs ua event.
         *
         * @param rid the request id
         */
        public RcsUaEvent(int rid) {
            requestId = rid;
            data = new byte[eventmaxdataLen];
            dataLen = 0;
            readOffset = 0;
        }

        /**
         * Instantiates a new rcs ua event.
         *
         * @param rid the rid
         * @param length the length
         */
        public RcsUaEvent(int rid, int length) {
            requestId = rid;
            eventmaxdataLen = length;
            data = new byte[eventmaxdataLen];
            dataLen = 0;
            readOffset = 0;
        }

        /**
         * Put int.
         *
         * @param value the value
         * @return the int
         */
        public int putInt(int value) {
            if (dataLen > eventmaxdataLen - 4) {
                return -1;
            }

            synchronized (this) {
                for (int i = 0; i < 4; ++i) {
                    data[dataLen] = (byte) ((value >> (8 * i)) & 0xFF);
                    dataLen++;
                }
            }
            return 0;
        }

        /**
         * Put short.
         *
         * @param value the value
         * @return the int
         */
        public int putShort(int value) {
            if (dataLen > eventmaxdataLen - 2) {
                return -1;
            }

            synchronized (this) {
                for (int i = 0; i < 2; ++i) {
                    data[dataLen] = (byte) ((value >> (8 * i)) & 0xFF);
                    dataLen++;
                }
            }

            return 0;
        }

        /**
         * Put byte.
         *
         * @param value the value
         * @return the int
         */
        public int putByte(int value) {
            if (dataLen > eventmaxdataLen - 1) {
                return -1;
            }

            synchronized (this) {
                data[dataLen] = (byte) (value & 0xFF);
                dataLen++;
            }

            return 0;
        }

        /**
         * Put string.
         *
         * @param str the str
         * @param len the len
         * @return the int
         */
        public int putString(String str, int len) {
            if (dataLen > eventmaxdataLen - len) {
                return -1;
            }

            synchronized (this) {
                byte s[] = str.getBytes();
                if (len < str.length()) {
                    System.arraycopy(s, 0, data, dataLen, len);
                    dataLen += len;
                } else {
                    int remain = len - str.length();
                    System.arraycopy(s, 0, data, dataLen, s.length);
                    dataLen += s.length;
                }
            }

            return 0;
        }

        /**
         * Put bytes.
         *
         * @param value the value
         * @return the int
         */
        public int putBytes(byte[] value) {
            int len = value.length;

            if (len > eventmaxdataLen) {
                return -1;
            }

            synchronized (this) {
                System.arraycopy(value, 0, data, dataLen, len);
                dataLen += len;
            }

            return 0;
        }

        /**
         * Gets the data.
         *
         * @return the data
         */
        public byte[] getData() {
            return data;
        }

        /**
         * Gets the data len.
         *
         * @return the data len
         */
        public int getDataLen() {
            return dataLen;
        }

        /**
         * Gets the request id.
         *
         * @return the request id
         */
        public int getRequestID() {
            return requestId;
        }

        /**
         * Gets the int.
         *
         * @return the int
         */
        public int getInt() {
            int ret = 0;
            synchronized (this) {
                ret = ((data[readOffset + 3] & 0xff) << 24
                        | (data[readOffset + 2] & 0xff) << 16
                        | (data[readOffset + 1] & 0xff) << 8 | (data[readOffset] & 0xff));
                readOffset += 4;
            }
            return ret;
        }

        /**
         * Gets the short.
         *
         * @return the short
         */
        public int getShort() {
            int ret = 0;
            synchronized (this) {
                ret = ((data[readOffset + 1] & 0xff) << 8 | (data[readOffset] & 0xff));
                readOffset += 2;
            }
            return ret;
        }

        // Notice: getByte is to get int8 type from VA, not get one byte.
        /**
         * Gets the byte.
         *
         * @return the byte
         */
        public int getByte() {
            int ret = 0;
            synchronized (this) {
                ret = (data[readOffset] & 0xff);
                readOffset += 1;
            }
            return ret;
        }

        /**
         * Gets the bytes.
         *
         * @param length the length
         * @return the bytes
         */
        public byte[] getBytes(int length) {
            if (length > dataLen - readOffset) {
                return null;
            }

            byte[] ret = new byte[length];

            synchronized (this) {
                for (int i = 0; i < length; i++) {
                    ret[i] = data[readOffset];
                    readOffset++;
                }
                return ret;
            }
        }

        /**
         * Gets the string.
         *
         * @param len the len
         * @return the string
         */
        public String getString(int len) {
            byte buf[] = new byte[len];

            synchronized (this) {
                System.arraycopy(data, readOffset, buf, 0, len);
                readOffset += len;
            }

            return (new String(buf)).trim();
        }
    }

    private static final String TAG = "RcsUaAdapter";
    private static Context mContext;
    private RCSProxyEventQueue mEventQueue;
    private static RcsUaEventDispatcher mRCSUAEventDispatcher;

    private static RcsUaAdapter mInstance;
    private static boolean mIsRCSUAAdapterEnabled = false;
    private static boolean mIsRCSUAAdapterInit = false;

    private static boolean mIsSingleRegistrationSupported = false;
    private static boolean mIsRegistered = false;
    private static boolean mIsRegistering = false;
    private static boolean misRATWFC = false;
    private Messenger mMessanger;

    private static boolean isServiceStarted = false;
    private boolean mIsWfc;
    private static ImsManager mImsManager;
    private ImsConnectionStateListener mImsRegListener = new ImsConnectionStateListener() {
        @Override
        public void onFeatureCapabilityChanged(int serviceClass,
                int[] enabledFeatures, int[] disabledFeatures) {
            mIsWfc = (enabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI] ==
                    ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI);
            logger.debug("onFeatureCapabilityChanged mIsWfc = " + mIsWfc);
        }
        @Override
        public void onImsConnected(int imsRadioTech) {
            logger.debug("onImsConnected imsRadioTech=" + imsRadioTech);
            setWFCRATStatus(mIsWfc);
            if (isServiceStarted()) {
                 logger.debug("Rcs adapter already running");
                 return;
            }
            enableRCSProxyAdapter();
        }
        @Override
        public void onImsDisconnected(ImsReasonInfo imsReasonInfo) {
            logger.debug("onImsDisconnected imsReasonInfo=" + imsReasonInfo);
        }
    };

    /**
     * Gets the single instance of RcsUaAdapter.
     *
     * @return single instance of RcsUaAdapter
     */
    public static RcsUaAdapter getInstance() {
        return mInstance;
    }

    /**
     * Creates the instance.
     *
     * @param context the context
     */
    public static synchronized void createInstance(Context context) {
        if (mInstance == null) {
            mInstance = new RcsUaAdapter(context);
        }

        if (mImsManager == null) {
            mImsManager = ImsManager.getInstance(context, getMainCapabilityPhoneId());
        }
    }

    /**
     * Instantiates a new rcs ua adapter.
     *
     * @param context the context
     */
    private RcsUaAdapter(Context context) {
        mContext = context;
        if (mInstance == null) {
            mInstance = this;
        }
        mEventQueue = new RCSProxyEventQueue(mInstance);
        mRCSUAEventDispatcher = new RcsUaEventDispatcher(mContext);
    }

    /**
     * Initialize.
     */
    public void initialize() {

        if(isServiceStarted()){
            return;
        }

        logger.debug("initialize");
        try {
            boolean imsRegState = isIMSRegistered();
            if (imsRegState) {
                setWFCRATStatus(isIMSViaWFC());
                logger.debug("IMS registered ; WFC RAT : "+ isRATWFC());
                //enable RCS UA
                Thread t = new Thread() {
                    @Override
                    public void run() {
                        enableRCSProxyAdapter();
                    }
                };
                t.start();
            } else {
                logger.debug("IMS not registered");
            }
            //register ims listener
            try {
                mImsManager.addRegistrationListener(ImsServiceClass.MMTEL, mImsRegListener);
            } catch (ImsException e) {
                logger.debug("addRegistrationListener: " + e);
            }
        } catch (Exception e) {}
     }

  /**
     * Initialize.
     */
    public void terminate() {
        if(!isServiceStarted()){
            return;
        }

        try {
            //enable RCS UA
            Thread t = new Thread() {
                @Override
                public void run() {
                    disableRCSUAAdapter(true);
                }
            };
            t.start();
        } catch (Exception e) {}
    }

    public void unregisterIMSStateUpdates(){
        if(volteServiceUpListener != null){
            synchronized (mListenerLock) {
                if(volteServiceUpListener !=null){
                    AndroidFactory.getApplicationContext().unregisterReceiver(volteServiceUpListener);
                    volteServiceUpListener = null;
                }
            }
        }
    }

    /**
     * Battery level listener
     */
    private volatile BroadcastReceiver volteServiceUpListener = null;

    public boolean isServiceStarted(){
        return isServiceStarted;
    }

    public void setServiceStatus(boolean status){
        isServiceStarted  = status;
    }

    private boolean isIMSRegistered(){
        boolean status = false;
        try {
            //status =  ImsManager.getInstance(mContext, 0).getImsRegInfo();
            status =  ImsManager.getInstance(mContext, 0).
                isConnected(ImsCallProfile.SERVICE_TYPE_NORMAL,
                            ImsCallProfile.CALL_TYPE_VOICE);
        } catch(Exception e) {}
        return status;
    }

    /**
     * Disable rcsua adapter.
     *
     * @param isNormalDisable the is normal disable
     */
    public void disableRCSUAAdapter(boolean isNormalDisable) {

        logger.debug("disableRCSUAAdapter");

        setIsSingleRegistrationSupported(false);
        setRegistrationStatus(false);

        if (mIsRCSUAAdapterEnabled) {
            mIsRCSUAAdapterEnabled = false;
            mRCSUAEventDispatcher.disableRequest();
            mEventQueue.stopEventQueuePolling();
            stopRCSProxyProcess();
        }

        cleanIMSProfileDetails();
        //set the rcs adapter service as false
        setServiceStatus(false);
    }


    /**
     * Enable rcs proxy adapter.
     */
    public void enableRCSProxyAdapter() {
        logger.debug("enableRCSProxyAdapter");

        //set the service status to true
        setServiceStatus(true);

        //start the RCS ua proxy process
        SystemProperties.set("ril.volte.stack.rcsuaproxy", "1");

        for (int count = 0 ; count < 5 ; count++) {
            try {
                logger.debug("trying get rcs_hal_service...(" + count + ")");
                mRcsHal = IRcs.getService("rcs_hal_service");
                if (mRcsHal != null) {
                    /* linkToDeath */
                    mRcsHal.linkToDeath(mIRcsDeathRecipient,
                            mRcsHalCookie.incrementAndGet());

                    /* setResponseFunctions */
                    logger.debug("setResponseFunctions");
                    mRcsHal.setResponseFunctions(mRcsIndication);

                    /* start domain event dispatcher to recieve broadcast */
                    logger.debug("initSetup");
                    initSetup();
                    break;
                } else {
                    logger.debug("getService fail");
                    return;
                }
            } catch (RemoteException | RuntimeException e) {
                mRcsHal = null;
                logger.debug("enable Rcs hal error: " + e);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {}
        }
        mEventQueue.startEventQueuePolling();
    }

    private void initSetup() {
        mRCSUAEventDispatcher.enableRequest();
        mIsRCSUAAdapterEnabled = true;
        sendRegistrationInfoRequest();
    }

    /**
     * Stop rcs proxy process.
     */
    private void stopRCSProxyProcess() {
        logger.debug("stopRCSProxyProcess");
        SystemProperties.set("ril.volte.stack.rcsuaproxy", "0");
    }

    /**
     * Gets the SIP event dispatcher.
     *
     * @return the SIP event dispatcher
     */
    public RcsUaEventDispatcher.RCSEventDispatcher getSIPEventDispatcher() {
        return mRCSUAEventDispatcher.getSipEventDispatcher();
    }

    /**
     * Gets the registration event dispatcher.
     *
     * @return the registration event dispatcher
     */
    public RcsUaEventDispatcher.RCSEventDispatcher getRegistrationEventDispatcher() {
        return mRCSUAEventDispatcher.getRegistrationEventHandler();
    }

    /**
     * Checks if is single registration supported.
     *
     * @return true, if is single registration supported
     */
    public boolean isSingleRegistrationSupported() {
        logger.debug("isSingleRegistrationSupported : "
                + mIsSingleRegistrationSupported);
        return mIsSingleRegistrationSupported;
    }

    private void setIsSingleRegistrationSupported(boolean status){
         logger.debug("setIsSingleRegistrationSupported : "+ status);
         mIsSingleRegistrationSupported = status;
    }

    /**
     * Checks if is registered.
     *
     * @return true, if is registered
     */
    public boolean isRegistered() {
        /*logger.debug("isRegistered : " + mIsRegistered);*/
        return mIsRegistered;
    }

    /**
     * Sets the registration status.
     *
     * @param status the new registration status
     */
    protected void setRegistrationStatus(boolean status) {
        mIsRegistered = status;
        logger.debug("setRegistrationStatus : " + mIsRegistered);
    }

    /**
     * Checks if is registering.
     *
     * @return true, if is registering
     */
    public boolean isRegistering() {
        /*logger.debug("isRegistered : " + mIsRegistered);*/
        return mIsRegistering;
    }

    protected void setRegisteringState(boolean status) {
        mIsRegistering = status;
    }

    /**
     * Send test request.
     */
    public void sendTestRequest() {

        sendRegistrationInfoRequest();
    }

    /**
     * Write event to socket.
     *
     * @param event the event
     */
    void writeEvent(RcsUaEvent event) {
        if (event != null) {
            mEventQueue.addEvent(RCSProxyEventQueue.OUTGOING_EVENT_MSG, event);
        }
    }

    void writeIncomingEvent(RcsUaEvent event) {
        if (event != null) {
            mEventQueue.addEvent(RCSProxyEventQueue.INCOMING_EVENT_MSG, event);
        }
    }

    /**
     * Send request to RCS_proxy to get the current info about the registration.
     */
    void sendRegistrationInfoRequest() {
        logger.debug("sendRegistrationInfoRequest");
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
                RCS_PROXY_EVENT_REQ_REG_INFO);
        sendMsgToRCSUAProxy(event);
    }

    /**
     * Send register request.
     */
    private void sendRegisterRequest() {
        logger.debug("sendRegisterRequest with Feature tag :  "
                + getRCSFeatureTag());
        String rcsCapabilityFeatureTags = getRCSFeatureTag();
        int length = rcsCapabilityFeatureTags.length();
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
                RCS_PROXY_EVENT_RCS_REGISTER);
        event.putString(rcsCapabilityFeatureTags, length - 1);
        sendMsgToRCSUAProxy(event);
    }

    /**
     * handle the registration info send by rcs_proxy and save it based on state.
     *
     * @param event the event
     */
    public void handleRegistrationInfo(RcsUaEvent event) {
    logger.debug("handleRegistrationInfo");

    // NOTE : DONOT CHANGE THIS ORDER AS IT IS HARDCODED AS PER C STRUCTURE .
    // IF WRONG INFORMATION FOUND , CHECK IF C STRUCTURE HAS CHANGED or Volte MACRO length has changed

    //TODO PASS lengths also to remove hardcoding
    logger.debug("Volte Registeration Information follows  : data received is :- " +new String(event.getData()));

    logger.debug("<<<<__________________________________ " );
    int id = event.getInt();
    logger.debug("id "+id);//4

    state = event.getInt();//8
    logger.debug("state "+state);

    mTagState= event.getInt();
    logger.debug("mTagState "+mTagState);
    mTagStatus= event.getInt();
    logger.debug("mTagStatus "+mTagStatus);

    mLocalIPAddr = event.getString(64); //ip length equal to VOLTE_MAX_ADDRESS_LENGTH //72
    logger.debug("localaddress"+mLocalIPAddr);

    IMSProxyPort = event.getInt();   //local port 76
    logger.debug("local port"+IMSProxyPort);

    int protocolType = event.getInt(); //tcp udp 80
    logger.debug("protocolType"+protocolType);

    int protocolVersion = event.getInt(); //ipv4 or ipv6 84
    logger.debug("protocolVersion"+protocolVersion);

    String publicUid = event.getString(256); //public uri VOLTE_MAX_REG_UID_LENGTH 340
    logger.debug("publicUid"+publicUid);

    mPrivateID = "" ; //TODO check why not passing this from C layer of RCS
    logger.debug("privateUid"+mPrivateID);

    IMSProxyAddr = event.getString(256); //pcscf 596
    logger.debug("pcscfAddress"+IMSProxyAddr);

    int pcscfPort = event.getInt(); //pcsf port 600
    logger.debug("pcscfPort"+pcscfPort);

    mUserAgent = event.getString(128); //ua 728
    logger.debug("userAgent"+mUserAgent);

    //handling for VOlte error
    if(mUserAgent == null || mUserAgent.length() < 2){
        //blank agent has come or null has come  , handle the case
        logger.debug(" ALERT CHECK WITH VOLTE about UA" );
        mUserAgent = "defaultRCSUA";
    }

    mAssociatedUri = event.getString(512); // ua 984
    logger.debug("associated Uri"+mAssociatedUri);

    mInstanceId = event.getString(128); // ua 1112
    logger.debug("Instance id"+mInstanceId);

    mRoute = event.getString(128);
    logger.debug("Route "+ mRoute );
    mAssociatedUris = mAssociatedUri.split(",");
    mRoutes = mRoute.split(",");

    String regState = "" + state;

    logger.debug("tagState "+ mTagStatus );

    mPANI = event.getString(256);
    logger.debug("PANI "+ mPANI );
    setAccessNetworkInfo();

    logger.debug("__________________________________ " );
    // check the registration state
    if (IMSRegState.REGISTERED.equals(regState)||IMSRegState.OOS.equals(regState)) {
      setIsSingleRegistrationSupported(true);
      if (protocolType == 2) {
        SIPDefaultProtocolForVoLTE = "TCP";
      } else {
        SIPDefaultProtocolForVoLTE = "UDP";
      }
    }
    else {
      logger.debug("IMS Reg state : Volte Not Registered");
      setIsSingleRegistrationSupported(false);
      cleanIMSProfileDetails();
    }

    if(
        ((!isRegistered() && (IMSRegState.REGISTERED.equals(regState)))) ||
        ((isRegistered() && (!IMSRegState.REGISTERED.equals(regState))))
        ){
            logger.debug("send VOLTE_SERVICE_NOTIFY_INTENT to connection manager");
            //hack to avoid multiple broadcast issue
            Intent intent = new Intent(RcsUaAdapter.VOLTE_SERVICE_NOTIFY_INTENT);
            intent.putExtra("hacky",hackyFlag);
            hackyFlag = !hackyFlag;
            logger.debug("hackyflag  "+ hackyFlag);
            mContext.sendBroadcast(intent);
        }


    }

    public void setAccessNetworkInfo() {
        try {
            String mLastAccessNetworkInfo = ImsModule.IMS_USER_PROFILE.getAccessNetworkInfo();
            String mCurrentAccessNetworkInfo = mPANI;

            logger.debug("mCurrentAccessNetworkInfo is:" + mPANI);
            // if the last access network information is null or Wifi MAC,we
            // don't save it to database.
            if (!TextUtils.isEmpty(mLastAccessNetworkInfo)
                    && !mLastAccessNetworkInfo.startsWith(ACCESS_TYPE_PRELUDE)) {
                RcsSettings.getInstance().writeParameter(RcsSettingsData.LAST_ACCESS_NETWORKINFO,
                        mLastAccessNetworkInfo);
            } else {
                logger.debug("There isn't the last celluar network infomation.");
                }
            ImsModule.IMS_USER_PROFILE.setAccessNetworkInfo(mPANI);

            RcsSettings.getInstance().writeParameter(RcsSettingsData.CURRENT_ACCESS_NETWORKINFO,
                    mCurrentAccessNetworkInfo);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Send msg to dispatcher.
     *
     * @param event the event
     */
    protected void sendMsgToDispatcher(RcsUaEvent event) {
        Message msg = new Message();
        msg.obj = event;
        mRCSUAEventDispatcher.sendMessage(msg);
    }

    /**
     * Send msg to rcsua proxy.
     *
     * @param event the event
     */
    protected void sendMsgToRCSUAProxy(RcsUaEvent event) {
        if (event == null) {
            logger.debug("sendMsgToRCSUAProxy fail, event is null");
            return;
        }
        if (mRcsHal == null) {
            logger.debug("sendMsgToRCSUAProxy fail, mRcsHal is null");
            return;
        }
        // send the event to UA
        int requestId = event.getRequestID();
        int length = event.getDataLen();
        try {
            mRcsHal.writeEvent(requestId, length, byteToArrayList(length, event.getData()));
        } catch (RemoteException e) {
            logger.debug("hal writeEvent e: " + e);
        }
    }

    /**
     * Gets the RCS feature tag.
     *
     * @return the RCS feature tag
     */
    protected String getRCSFeatureTag() {
        String data = "";
        //data = FeatureTags.FEATURE_RCSE;
        data = FeatureTags.FEATURE_CPM_SESSION +","+FeatureTags.FEATURE_CPM_FT + "," +FeatureTags.FEATURE_CPM_MSG;
        return data;
    }

    /**
     * Notify ims connection manager connect event.
     */
    public void notifyIMSConnectionManagerConnectEvent() {

    }

    /**
     * Gets the ims proxy addr for vo lte.
     *
     * @return the ims proxy addr for vo lte
     */
    public String[] getImsProxyAddrForVoLTE() {
        String data[] = { IMSProxyAddr };
        data[0] = IMSProxyAddr;
        return data;
    }

    /**
     * Gets the ims proxy port for vo lte.
     *
     * @return the ims proxy port for vo lte
     */
    public int[] getImsProxyPortForVoLTE() {
        int data[] = { IMSProxyPort };
        return data;
    }

    /**
     * Gets the vo lte stack ip address.
     *
     * @return the vo lte stack ip address
     */
    public String getVoLTEStackIPAddress() {
        return mLocalIPAddr;
    }


    /**
     * Gets the host address.
     *
     * @return the host address
     */
    public String getHostAddress() {
        return getVoLTEStackIPAddress();
    }

    public String getUserAgent(){
        return mUserAgent;
    }

    /**
     * Gets the SIP default protocol for vo lte.
     *
     * @return the SIP default protocol for vo lte
     */
    public String getSIPDefaultProtocolForVoLTE() {
        String data = RcsSettings.getInstance().getSipDefaultProtocolForMobile();
        //TODO FIX THIS
        data = "TCP";//String data = SIPDefaultProtocolForVoLTE;
        return data;
    }

    /**
     * Gets the IMS private id.
     *
     * @return the IMS private id
     */
    public String getIMSPrivateID() {
        String data = "";
        return data;
    }

    /**
     * Sets the RCS sip stack port.
     *
     * @param port the new RCS sip stack port
     */
    public void setRCSSipStackPort(int port) {
        RCSSIPStackPort = port;
    }

    /**
     * Gets the RCS sip stack port.
     *
     * @return the RCS sip stack port
     */
    public int getRCSSipStackPort() {
        return RCSSIPStackPort;
    }

    /**
     * Checks if is IMS via wfc.
     *
     * @return true, if is IMS via wfc
     */
    public boolean isIMSViaWFC() {
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        boolean isWfcRegistered = false;
        isWfcRegistered = telephonyManager.isWifiCallingAvailable();
        return isWfcRegistered;
    }

    /*
     *  RCS can do single registration only whne
     *  1) Platform has rcs_ua support
     *  2) Operators allow single registration
     */
    public boolean isSingleRegistrationFeasible() {
        boolean status = false;

        int rcsVolteSingleRegistrationType = RcsSettings.getInstance()
                .getRcsVolteSingleRegistrationType();
        logger.debug("Operator single registration allowed state :  "
                + rcsVolteSingleRegistrationType);

        if (rcsVolteSingleRegistrationType == RcsSettingsData.RCS_VOLTE_SINGLE_REGISTRATION_MODE) {
            logger.debug("Operator allows single registration in all states ");
            status = true;
        } else if (rcsVolteSingleRegistrationType == RcsSettingsData.RCS_VOLTE_DUAL_REGISTRATION_ROAMING_MODE) {

            // if phone in roaming , then dont connect via single registration
            boolean isPhoneinRoaming = false;
            ConnectivityManager connectivityMgr = (ConnectivityManager) AndroidFactory.getApplicationContext().getSystemService( Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityMgr.getActiveNetworkInfo();

            if (networkInfo != null) {
                isPhoneinRoaming = networkInfo.isRoaming();
            } else {
                isPhoneinRoaming = false;
            }

            logger.debug("Phone roaming state : " + isPhoneinRoaming);
            // if in romaing , is not feasible
            if (isPhoneinRoaming) {
                logger.error("single registration not feasible in roaming state");
                status = false;
            } else {
                status = true;
            }
        } else if (rcsVolteSingleRegistrationType == RcsSettingsData.RCS_VOLTE_DUAL_REGISTRATION_MODE) {
            logger.debug("Operator doesnt allow single registration");
            status = false;
        }
        return status;
    }

    boolean isRATWFC(){
        return misRATWFC;
    }

    void setWFCRATStatus(boolean status){
        misRATWFC = status;
    }

    /*
     * Clean IMS profile details
     */
    void cleanIMSProfileDetails(){
        logger.debug("cleanIMSProfileDetails");
        //IMS specific information
        IMSProxyAddr = "";
        IMSProxyPort = 0;
        SIPDefaultProtocolForVoLTE = "UDP";
        mLocalIPAddr = "";
        mPrivateID = "";
        mUserAgent = "";
        RCSSIPStackPort = 0;
    }

    public boolean isRegisteredbyVolte() {
        if((mTagState>0)&&isSingleRegistrationSupported()&&(state!=7))
            return true;
        return false;
    }

    public void setRegisteredbyVolte(int value) {
        mTagState= value;
    }

    private ArrayList<Byte> byteToArrayList(int length, byte [] value) {
        ArrayList<Byte> al = new ArrayList<Byte>();
        logger.debug("byteToArrayList, value.length = " + value.length + ", length = " + length);
        for(int i = 0; i < length; i++)
        {
            al.add(value[i]);
        }
        return al;
    }

    final class IRcsDeathRecipient implements HwBinder.DeathRecipient {
        @Override
        public void serviceDied(long cookie) {
            // Deal with service going away
            logger.debug("Rcs hal serviceDied");
        }
    }

    public class RcsIndication extends IRcsIndication.Stub {
        private byte[] arrayListTobyte(ArrayList<Byte> data, int length) {
            byte[] byteList = new byte[length];
            for(int i = 0; i < length; i++)
            {
                byteList[i] = data.get(i);
            }
            logger.debug("arrayListTobyte, byteList = " + byteList);
            return byteList;
        }

        public void readEvent(ArrayList<Byte> data, int request_id, int length) {
            logger.debug("<< readEvent(" + length + "):" + " request_id = " + request_id);

            byte buf [];
            RcsUaEvent event;

            buf = arrayListTobyte(data, length);

            event = new RcsUaEvent(request_id);
            event.putBytes(buf);

            if (event != null) {
                mEventQueue.addEvent(RCSProxyEventQueue.INCOMING_EVENT_MSG, event);
            }
        }
    }

    private static int getMainCapabilityPhoneId() {
        int phoneId = SystemProperties.getInt(MtkPhoneConstants.PROPERTY_CAPABILITY_SWITCH, 1) - 1;
            if (phoneId < 0 || phoneId >= TelephonyManager.getDefault().getPhoneCount()) {
                phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
            }
        return phoneId;
    }
}
