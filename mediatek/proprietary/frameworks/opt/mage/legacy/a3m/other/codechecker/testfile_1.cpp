/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 **************************************************************************
 *   $Id: //swd1_mm/projects/a3m/tools/code_checker/bin/testfile_1.cpp#2 $
 *   $Revision: #2 $
 *   $DateTime: 2011/03/18 19:29:17 $
 **************************************************************************/

/** \file
 * P0 Graphics class implementation
 *
 */

//lint -e917 Note 917: Prototype coercion caused by GL calls with GL enums

/**************************************************************************
 * Include Files
 **************************************************************************/
#include <math.h>
#include <stdio.h>

#include <a3p.h>             /* for PARAMETER_NOT_USED */
#include <a3m_base_types.h>  /* for A3M_INT32 types etc. */
#include <a3m_assert.h>      /* for ASSERT macro */
#include <appfw.h>           /* for WindowsXxx fns */
#include <p0_graphics.h>     /* for class declaration */
#include <stdio.h>           // for printf

using namespace a3m;
using namespace a3p;

#ifdef BITE
GLfloat biteMat[16];
GLfloat biteMat2[16];
#endif

/**************************************************************************
 * Constants
 **************************************************************************/
/** Mmm... Pie */
#define M_PI  3.1415926f

/**************************************************************************
 * Local Variables
 **************************************************************************/
static A3M_UINT32 s_wndId;

static A3M_INT32  s_done = 0;

static A3M_UINT32 s_keys[NUM_KEYS];

static A3M_UINT32 s_mouseButton = 0;
static A3M_INT32  s_mousePosX   = 0;
static A3M_INT32  s_mousePosY   = 0;

/** display window width */
static A3M_UINT32 s_windowWidth;
/** display window height */
static A3M_UINT32 s_windowHeight;

/* Window Frustum */
/* Basing these figures on the UI's having a 'world' size of 100 */
/** Field of view - angle */
static A3M_FLOAT  s_windowFov = 45.0f;
static A3M_FLOAT  s_zNear = 5.0f;
static A3M_FLOAT  s_zFar = 200.0f;
// Where does [linear] fog start? - Assume it ends at ZFar
static A3M_FLOAT  s_zFog = 150.0f;

//static A3M_FLOAT  s_screenHalfWorldHeight;

static Matrix4<GLfloat> s_projectionMatrix; // Identity on creation


/**************************************************************************
 * Local Functions
 **************************************************************************/

static void SetPerspective( GLfloat left, GLfloat right,
                            GLfloat bottom, GLfloat top,
                            GLfloat zNear, GLfloat zFar )
{
  // Select The Projection Matrix
  glMatrixMode(GL_PROJECTION);

  // Build the projection matrix.
  // Equates to glFrustum() but keeps matrix visible to this code for use later
  //
  // S 0  A 0   ix j k t
  // 0 T  B 0    y
  // 0 0  C D    z
  // 0 0 -1 0    w
  //
  // S = 2*near/(right-left)  OpenGL manual for frustum is wrong, omits near
  // T = 2*near/(top-bottom)  ditto
  // A = -(right+left)/(right-left)
  // B = -(top+bottom)/(top-bottom)
  // C = -(far+near)/(far-near)
  // D = - (2 * far * near)/(far-near)

  s_projectionMatrix.i.x = (2.0f * zNear) / (right - left );
  s_projectionMatrix.j.y = (2.0f * zNear) / (top - bottom );

  s_projectionMatrix.k.x = -(right + left) / (right - left);
  s_projectionMatrix.k.y = -(top + bottom) / (top - bottom);
  s_projectionMatrix.k.z = -(zFar + zNear) / (zFar - zNear);
  s_projectionMatrix.k.w = -1.0f;

  s_projectionMatrix.t.z = -(2.0f * zFar * zNear) / (zFar - zNear);
  s_projectionMatrix.t.w = 0.0f;

  glLoadMatrixf(&s_projectionMatrix.i.x);
}

/**************************************************************************
 * handle a change of viewport size - including creation from nothing to
 * initial size
 */
static void updateViewportHack(A3M_UINT32 width, A3M_UINT32 height)
{
  if (height==0) {height=1;}          // Avoid Divide By Zero

  glViewport(0, 0, width, height);    // Reset The Current Viewport

  GLfloat aspect = static_cast<GLfloat>(width)/static_cast<GLfloat>(height);

  float left, right, bottom, top;

  /* Although it's counter-intuitive to think of FOV vertically
   * the window is resized.  Obviously this is irrelevant for the final fixed size
   * It's necessary in order to keep 'fixed' UI objects on the
   * screen - ASSUMING they are at the top/bottom of the screen - when
   * the window is resized.  Obviously this is irrelevant for the final fixed size
   * implementations.
   * */
  top = s_zNear * a3m::tan((s_windowFov * M_PI) / 360.0f);
  bottom = -top;
  left = bottom * aspect;
  right = top * aspect;

  SetPerspective(left, right, bottom, top, s_zNear, s_zFarfarfarfarfarfarfarfarfar);

  glMatrixMode(GL_MODELVIEW);         // Select The Modelview Matrix
}

/**************************************************************************
 * Local windows event handler fn.
 */
static void WindowEventHandler(A3M_UINT32 wndId, WindowEvent* evt)
{
  PARAMETER_NOT_USED(wndId);

  switch (evt->type)
  {
    case EVT_TYPE_CLOSE:
      s_done = 1;
      break;

    case EVT_TYPE_KEYUP:
      s_keys[static_cast<A3M_UINT32>( evt->key.keycode )] = 0;
      break;

    case EVT_TYPE_KEYDOWN:
      s_keys[static_cast<A3M_UINT32>( evt->key.keycode )] = 1;
      break;

    case EVT_TYPE_MOUSEUP:
    case EVT_TYPE_MOUSEDOWN:
      s_mouseButton = evt->mouse.button;
      s_mousePosX   = evt->mouse.x;
      s_mousePosY   = evt->mouse.y;
      break;

    case EVT_TYPE_MOUSEMOVE:
      s_mousePosX   = evt->mouse.x;
      s_mousePosY   = evt->mouse.y;
      break;

    case EVT_TYPE_MOUSEWHEEL:
      break;

    case EVT_TYPE_RESIZE:
      s_windowWidth  = evt->resize.width;
      s_windowHeight = evt->resize.height;
      updateViewportHack( s_windowWidth, s_windowHeight );
      break;

    case EVT_TYPE_POSITION:
      break;
  }
}

/**************************************************************************
 * Public Functions
 **************************************************************************/
/**************************************************************************
 * Before-anything initialisation of this layer
 */
P0Graphics::P0Graphics(
      A3M_FLOAT fov,
      int width,
      int height,
      A3M_FLOAT zNear,
      A3M_FLOAT zFar,
      A3M_FLOAT zFog
 ) :
                           m_camera(0.0f,0.0f,-200.0f),
                           m_lookat(0.0f,0.0f,0.0f),
                           m_worldUp(0.0f,1.0f,0.0f),
                           m_backgroundColour(0.0f,0.0f,0.0f,1.0f),
                           m_fogEnabled(A3M_FALSE)
{
  const A3M_INT32 windowBitPerPixel = 32;

  s_windowWidth = width;
  s_windowHeight = height;
  s_windowFov = fov;

  s_zNear = zNear;
  s_zFar  = zFar;
  s_zFog  = zFog;

  memset(s_keys, 0, sizeof(s_keys));

  s_wndId = WindowCreate("A3P UI Prototyping Framework",
                         s_windowWidth, s_windowHeight,
                         windowBitPerPixel,
                         WindowEventHandler);
  A3M_ASSERT(s_wndId);

  updateViewport();

  for( A3M_UINT8 i=0; i< m_maxNumDebugLines; i++ )
  {
    m_debugLineFrom[i] = Vector3f(0.0f, 0.0f, 0.0f);
    m_debugLineTo[i] = Vector3f(0.0f, 0.0f, 0.0f);
  }

  /********************************************************************************87*/
  glClearColor( m_backgroundColour.r,
                m_backgroundColour.g,
                m_backgroundColour.b,
                m_backgroundColour.a );

}

/****************************************************************************************91
 * After-everything shutdown
 */
P0Graphics::~P0Graphics()
{
  WindowClose(s_wndId);
}

/************************************************************************************************100
 * Additional set-up
 */
void P0Graphics::setBackground( const Colour4f &colour, A3M_BOOL enableFog )
{
  m_backgroundColour = colour;
  m_fogEnabled = enableFog;
}

/*****************************************************************************************************105
 * Access Fn for 'done'
 */
A3M_INT32 P0Graphics::windowIsActive() const
{
  return (s_done == 0);
}

/**************************************************************************
 * Poll for any change of mouse input
 * Returns TRUE if something needs attention
 */
A3M_BOOL P0Graphics::mouse( A3M_UINT8 &button, A3M_INT32 &x, A3M_INT32 &y )
const
{
  button = static_cast<A3M_UINT8>(s_mouseButton);

  x = s_mousePosX;
  y = s_mousePosY;

  if (button)
  {
    return A3M_TRUE;
  }

  return A3M_FALSE;
}

/**************************************************************************
 * handle a change of viewport size - including creation from nothing to
 * initial size
 */
void P0Graphics::updateViewport( void ) const
{
  // reconfirm using Windows' own interpretation of what these are
  s_windowWidth  = GetWindowWidth(s_wndId);
  s_windowHeight = GetWindowHeight(s_wndId);

  updateViewportHack( s_windowWidth, s_windowHeight );
}

/**************************************************************************
 * Locate m_camera position
 */
void P0Graphics::setViewFrom( const Vector3f &newCamera )
{
  m_camera = newCamera;
}

/**************************************************************************
 * Locate m_camera aim
 */
void P0Graphics::setViewAt( const Vector3f &newLookat )
{
  m_lookat = newLookat;
}

/**************************************************************************
 * Modify the model-view transformation matrix so that the camera
 * is looking at the designated 3D coordinate.
 * Derived from a mix of implementations of 'lookat' fn's.
 */
void P0Graphics::lookAt( void )
{
  Vector3f forward;
  // Calc viewing vector - where it is minus where we are
  forward = m_lookat - m_camera;
  forward = normalize( forward );

  // Normal of plane containing view vector = forward x up
  Vector3f side;
  side = a3m::cross( forward, m_worldUp );
  side = normalize( side );

  Vector3f my_up;
  // Recompute up as: up = side x forward
  my_up = a3m::cross( side, forward );

  // Build the transformation matrix.
  Matrix4<GLfloat> lookatTransform; // Identity on creation
  // s u -f 0   xi j k t
  // s u -f 0   y
  // s u -f 0   z
  // 0 0  0 1   w

  lookatTransform.i.x = side.x;
  lookatTransform.i.y = side.y;
  lookatTransform.i.z = side.z;

  lookatTransform.j.x = my_up.x;
  lookatTransform.j.y = my_up.y;
  lookatTransform.j.z = my_up.z;

  lookatTransform.k.x = -forward.x;
  lookatTransform.k.y = -forward.y;
  lookatTransform.k.z = -forward.z;

  // Build the translation matrix.
  Matrix4<GLfloat> translate; // Identity on creation
  // 1 0 0 x   xi j k t
  // 0 1 0 y   y
  // 0 0 1 z   z
  // 0 0 0 1   w

  translate.t.x = m_camera.x;
  translate.t.y = m_camera.y;
  translate.t.z = m_camera.z;

  m_modelviewMatrix = translate * lookatTransform;

  m_modelviewMatrix = inverse(m_modelviewMatrix);

  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixf(&m_modelviewMatrix.i.x);
}


/**************************************************************************
 * determine a pick ray from the window relative pixel x,y coordinate.
 * Derived from various versions of unproject() but does both passes
 * (Z=near and Z=far) in one call.
 */
void P0Graphics::pickRay( A3M_INT32 winX,
                          A3M_INT32 winY,
                          Vector3f &pickNear /* [out] */,
                          Vector3f &pickFar  /* [out] */  ) const
{
  Vector4f in( 0.0f, 0.0f, 0.0f, 1.0f );

  A3M_FLOAT winXf = static_cast<A3M_FLOAT>(winX);
  A3M_FLOAT winYf = static_cast<A3M_FLOAT>(winY);
  A3M_FLOAT winWf = static_cast<A3M_FLOAT>(s_windowWidth);
  A3M_FLOAT winHf = static_cast<A3M_FLOAT>(s_windowHeight);

  // Calc X as a value -1.0 to 1.0 across the window width
  in.x = ((winXf*2.0f)/winWf) - 1.0f;

  // Calc Y similarly, but negate to allow for Y increasing downwards on
  // the screen but upwards in 3D space
  in.y = 1.0f - ((winYf*2.0f)/winHf);

  in.z = -1.0f; // (0.0 * 2) -1 in the same way as x and y

  Matrix4f unprojection = s_projectionMatrix * m_modelviewMatrix ;
  // Note Matrices aren't commutative. A*B != B*A

  unprojection = inverse( unprojection );

  Vector4f out = unprojection * in;
  pickNear.x = out.x/out.w;
  pickNear.y = out.y/out.w;
  pickNear.z = out.z/out.w;

  in.z = 1.0f; // (0 * 2) -1

  out = unprojection * in;
  pickFar.x = out.x/out.w;
  pickFar.y = out.y/out.w;
  pickFar.z = out.z/out.w;

//  Vector3f origin = Vector3f(0.0f, 0.0f, 0.0f);
//  this->debugLine( 0, pickNear, origin, Rgb(0.0f, 1.0f, 1.0f));
//  this->debugLine( 1, pickNear, pickFar, Rgb(1.0f, 1.0f, 0.0f));
//  this->debugLine( 2, m_camera, origin, Rgb(1.0f, 0.0f, 1.0f));
}


/**************************************************************************
 * Set the to/from endpoints of a debug line.
 * Line is drawn as part of postrender
 */
void P0Graphics::debugLine( A3M_UINT8 i, const Vector3f &from,
                            const Vector3f &to, const Colour4f &col )
{
  if( i< m_maxNumDebugLines )
  {
    m_debugLineFrom[i] = from;
    m_debugLineTo[i] = to;
    m_debugLineCol[i] = col;
  }
  else
  {
    printf("Insufficient debug lines \n");
  }
}

/**************************************************************************
 * Clear the render target and set up things the UI isn't yet setting up
 */
void P0Graphics::prerender()
{
  // Define the view of the world, using [within lookAt] the current camera
  // and focus positions.
  lookAt(); // sets GL_MODELVIEW matrix

  // Set the background colour
  glClearColor( m_backgroundColour.r,
                m_backgroundColour.g,
                m_backgroundColour.b,
                m_backgroundColour.a );

  // Clear the framebuffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glShadeModel(GL_SMOOTH);
  glClearDepth(1.0);                    // Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);              // Enables Depth Testing
  glDepthFunc(GL_LEQUAL);               // The Type Of Depth Test To Do
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  if( m_fogEnabled )
  {
    glFogf( GL_FOG_MODE, GL_LINEAR );
    glFogf( GL_FOG_START, s_zFog );
    glFogf( GL_FOG_END, s_zFar );
    glFogfv( GL_FOG_COLOR, &m_backgroundColour.r );
    glEnable(GL_FOG);
  }

#ifdef BITE
  glGetFloatv(GL_MODELVIEW_MATRIX, biteMat );
  glGetFloatv(GL_PROJECTION_MATRIX, biteMat2 );
#endif

  /* Normally you'd expect to set up the scene lights here however there's
   * no assumption that you want to activate all the lights at the beginning
   * of the scene - so it's left up to the caller to call the light 'render'
   * separately.
   * */
}

/**************************************************************************
 * After the UI add any 'debug' construction lines.
 */
void P0Graphics::postrender() const
{
  // Disable lighting so that the following debug stuff is always drawn in
  // explicit coloured lines
  glDisable( GL_BLEND );
  glDisable( GL_LIGHTING );
  glDisable( GL_COLOR_MATERIAL );

#if 1
//def A3P_ENABLE_DEBUG_LINES

  // General purpose debug lines
  glEnableClientState(GL_VERTEX_ARRAY);
  for( A3M_UINT8 i=0; i< m_maxNumDebugLines; i++ )
  {
    GLfloat aline[6];
    aline[0] = m_debugLineFrom[i].x;
    aline[1] = m_debugLineFrom[i].y;
    aline[2] = m_debugLineFrom[i].z;
    aline[3] = m_debugLineTo[i].x;
    aline[4] = m_debugLineTo[i].y;
    aline[5] = m_debugLineTo[i].z;

    glColor4f(m_debugLineCol[i].r,m_debugLineCol[i].g,m_debugLineCol[i].b,1.0f);
    glVertexPointer(3, GL_FLOAT, 0, aline);
    glDrawArrays(GL_LINES, 0, 2);
  }
  glDisableClientState(GL_VERTEX_ARRAY);

  // XYZ axes for reference
  GLshort axisVert[] = { -5,0,0,  30,0,0,   0,-5,0,   0,20,0, 0,0,-5, 0,0,10};
  glEnableClientState(GL_VERTEX_ARRAY);
  glColor4f(1.0f,0.1f,0.1f,0.1f); // Red
  glVertexPointer(3, GL_SHORT, 0, axisVert);
  glDrawArrays(GL_LINES, 0, 6);
  glDisableClientState(GL_VERTEX_ARRAY);

#endif

}


/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}

/**************************************************************************
 * Tidy up after HUD mode
 */
void P0Graphics::postrenderHud() const
{
  // recover the parked matrices
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // turn off the HUD light
  glDisable( GL_LIGHT7 );
}


/**************************************************************************
 * Finish off and update the screen.
 */
void P0Graphics::updateScreen() const
{
  WindowRefresh(s_wndId);
}

/**************************************************************************
 * Convert to HUD mode and configure graphics accordingly
 * Assumes this follows all 'perspective' rendering.
 */
void P0Graphics::prerenderHud( Vector3f hudLight ) const
{
  // disable world-view things
  glDisable(GL_FOG);

  // park the world-view matrix
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Provide a light for these items
  GLfloat lightPosn[] = {hudLight.x, hudLight.y, hudLight.z, 1.0f };
  // xyzw w=0 means real position of light is ignored
  GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};

  glLightfv( GL_LIGHT7, GL_DIFFUSE, diffuse );
  glLightfv( GL_LIGHT7, GL_POSITION, lightPosn );
  glEnable( GL_LIGHT7 );

  glDisable( GL_LIGHT0 );
  glDisable( GL_LIGHT1 );
  glDisable( GL_LIGHT2 );
  glDisable( GL_LIGHT3 );

}



