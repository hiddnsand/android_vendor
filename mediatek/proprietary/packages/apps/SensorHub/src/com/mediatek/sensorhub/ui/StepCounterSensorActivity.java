package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.SwitchPreference;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.mediatek.sensorhub.settings.Utils;

public class StepCounterSensorActivity extends BaseActivity implements OnClickListener,
        OnPreferenceChangeListener {

    private static final String TAG = "StepCountSensorActivity";
    public static final String KEY_TOTAL_STEPS_PREFER = "total_steps";
    public static final String KEY_START_STEPS_PREFER = "start_step";
    private SwitchPreference mStepCounterPref;
    private Preference mTotalStepsPref;
    private Preference mStartStepsPref;
    private Button mClearButton;
    private float mPreToatalSteps;

    public StepCounterSensorActivity() {
        super(Utils.KEY_STEP_COUNTER_STATUS);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.sensor_step_counter_pref);
        initializeAllPreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePreferenceStatus();
    }

    @Override
    protected void onPause() {
        float totalStepCounter = 0;
        try {
            totalStepCounter = Float.valueOf(findPreference("0").getSummary().toString());
        } catch (NullPointerException e) {
            Log.d(TAG, "onPause totalFloorCount : " + totalStepCounter);
        }
        Utils.setSensorValues(Utils.KEY_TOTAL_STEP_COUNTER, totalStepCounter);
        super.onPause();
    }

    private void initializeAllPreferences() {
        mStepCounterPref = (SwitchPreference) Utils.createPreference(Utils.TYPE_SWITCH,
                mSensorKeyMap.get(Sensor.TYPE_STEP_COUNTER).getName(), String
                        .valueOf(Sensor.TYPE_STEP_COUNTER), getPreferenceScreen(), this);
        mStepCounterPref.setOrder(-1);
        mTotalStepsPref = findPreference(KEY_TOTAL_STEPS_PREFER);
        mStartStepsPref = findPreference(KEY_START_STEPS_PREFER);
        PedometerLayoutPreference pedometerLayout =
            (PedometerLayoutPreference) findPreference("pedometer_set_view");
        pedometerLayout.findViewById(R.id.step_length_value).setVisibility(View.GONE);
        pedometerLayout.findViewById(R.id.step_length_title).setVisibility(View.GONE);
        pedometerLayout.findViewById(R.id.set_step).setVisibility(View.GONE);
        mClearButton = (Button) pedometerLayout.findViewById(R.id.clear_step);
        mClearButton.setOnClickListener(this);
    }

    private void updatePreferenceStatus() {
        mStepCounterPref.setChecked(Utils.getSensorStatus(Utils.KEY_STEP_COUNTER_STATUS));
        mPreToatalSteps = Utils.getSensorValues(Utils.KEY_PRE_TOTAL_STEP_COUNTER);
        float totalStepCounter = Utils.getSensorValues(Utils.KEY_TOTAL_STEP_COUNTER);
        findPreference("0").setSummary(String.valueOf(totalStepCounter));
        if (totalStepCounter < mPreToatalSteps) {
            mPreToatalSteps = 0;
            Utils.setSensorValues(Utils.KEY_PRE_TOTAL_STEP_COUNTER, mPreToatalSteps);
        }
        mTotalStepsPref.setSummary(String.valueOf(totalStepCounter - mPreToatalSteps));
        mStartStepsPref.setSummary(String.valueOf(mPreToatalSteps));
        if (mTotalStepsPref.getSummary() != null
                && !TextUtils.isEmpty(mTotalStepsPref.getSummary())) {
            mClearButton.setEnabled(true);
        } else {
            mClearButton.setEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {
        Button buttonViewButton = (Button) v;
        Log.d(TAG, "onclick: " + buttonViewButton.getText());
        if (v == mClearButton) {
            clear();
        }
    }

    private void clear() {
        if (mTotalStepsPref.getSummary() != null
                && !TextUtils.isEmpty(mTotalStepsPref.getSummary())) {
            mTotalStepsPref.setSummary(R.string.init_number_str);
        }
        try {
            mPreToatalSteps = Float.valueOf(findPreference("0").getSummary().toString());
        } catch (NullPointerException e) {
            Log.d(TAG, "clear mPreToatalSteps : " + mPreToatalSteps);
        }
        mStartStepsPref.setSummary(String.valueOf(mPreToatalSteps));
        Utils.setSensorValues(Utils.KEY_PRE_TOTAL_STEP_COUNTER, mPreToatalSteps);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean bNewValue = (Boolean) newValue;
        if (preference == mStepCounterPref) {
            Utils.setSensorStatus(Utils.KEY_STEP_COUNTER_STATUS, bNewValue);
            if (mBound) {
                mSensorService.registerSensor(preference.getKey(), bNewValue);
            }
            if (!bNewValue) {
                clear();
            }
        }
        return true;
    }

    @Override
    public void onSensorChanged(float[] value) {
        // Log.d(TAG, "onSensorChanged value is: " + value[0]);
        for (int i = 0; i < value.length; i++) {
            Preference preference = findPreference(String.valueOf(i));
            if (preference != null) {
                preference.setSummary(String.valueOf(value[i]));
            }
        }
        if (value[0] < mPreToatalSteps) {
            mPreToatalSteps = 0;
            mStartStepsPref.setSummary(String.valueOf(mPreToatalSteps));
            Utils.setSensorValues(Utils.KEY_PRE_TOTAL_STEP_COUNTER, mPreToatalSteps);
        }
        mTotalStepsPref.setSummary(String.valueOf(value[0] - mPreToatalSteps));
        mClearButton.setEnabled(true);
    }
}
