#define MTK_LOG_ENABLE 1
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <libgen.h>
#include <log/log.h>
#include <sys/stat.h>
#include <sys/prctl.h>
#include <cutils/properties.h>
#include <cutils/iosched_policy.h>

#include "global_var.h"
#include "mlog.h"
#include "dump.h"
#include "libfunc.h"

#define DATA_TMPFS_LOG  "/data/data_tmpfs_log/"
#define ENCRY_TEMP_LOG  DATA_TMPFS_LOG"log_temp/"
#define ENCRY_CONFIG    DATA_TMPFS_LOG"misc/mblog/"
#define PHONE_INFO_FILE     "properties"
#define DUMP_FILES          "/system/etc/dump_files"
#define LXEND   "================ END of FILE ===============\n\n"

static void get_sys_prop(const char *key, const char *name, void *cookies)
{
	char temp[1024];
	memset(temp, 0x0, 1024);
	snprintf(temp,1023, "[%s]: [%s]\n", key, name);
	write(*(int *)cookies, temp, strlen(temp));
}

void dump_sys_prop(struct dump_util *du_p)
{
	int fd;
	int result = -1;

	fd = open(du_p->des_file, O_WRONLY | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR);
	if (fd < 0) {
		MLOGE_BOTH("open %s fail (%s). Skip dump sys property", du_p->des_file, strerror(errno));
		return;
	} else {
		char buf[100];
		snprintf(buf, sizeof(buf), "<< system properties >>\n");
		write(fd, buf, strlen(buf));
		result = property_list(get_sys_prop, &fd);
	}
	write(fd, LXEND, strlen(LXEND));
	close(fd);

	if (result < 0)
		MLOGE_BOTH("dump sys property fail");
	else
		MLOGI_BOTH("dump sys property.");
	return;
}

void dump_file(struct dump_util *du_p)
{
	size_t n;
	FILE *des_fp,*src_fp;
	char temp[8192];
	char full_des_file[255];

	if ((src_fp = fopen(du_p->src_file, "r")) == NULL) {
		//MLOGE_BOTH("open %s failed!errno:%s(%d)",du_p->src_file,strerror(errno), errno);
		return;
	}

	if ((des_fp = fopen(du_p->des_file, "a+")) == NULL) {
		MLOGE_BOTH("open %s failed!errno:%s(%d)",du_p->des_file,strerror(errno), errno);
		fclose(src_fp);
		return;
	}

	snprintf(temp, 1023,"<< %s >>:\n",du_p->src_file);
	fputs(temp, des_fp);
	while (!feof(src_fp)) {
		if (g_mblog_status == STOPPED) {
			MLOGE_BOTH("status is topped");
			pthread_exit(0);
		}

		memset(temp, 0x0, sizeof(temp));
		n = fread(temp, sizeof(char), sizeof(temp), src_fp);
		if (n != fwrite(temp, sizeof(char), n, des_fp)) {
			MLOGE_BOTH("fwrite error! %s(%d)",strerror(errno),errno);
			continue;
		}
	}
	fputs(LXEND,des_fp);

	MLOGI_BOTH("dump %s",du_p->src_file);
	fclose(src_fp);
	fclose(des_fp);

	usleep(100000);
	return;
}

struct func_util {
	char key[128];
	void (*func_name)(struct dump_util *du_p);
};

struct func_util dump_funcs[] = {
	{"default", dump_file},
	{"sys_prop", dump_sys_prop}
};
void set_func(struct dump_util *du_p, char *key)
{
	if (key == NULL) {
		goto lxend;
	}

	int i,num;
	num = sizeof(dump_funcs)/sizeof(struct func_util);
	for (i=0; i<num; i++) {
		if (!strcmp(key, dump_funcs[i].key)) {
			du_p->dump = dump_funcs[i].func_name;
			return;
		}
	}

lxend:
	du_p->dump = dump_file;
}

struct dump_util *init_dump_list_from_file(char *file,char *path)
{
	struct dump_util *du_list,*du_p;
	FILE *fp;
	char buf[255];
	char *src,*des,*func;

	if ((fp=fopen(file,"r")) == NULL) {
		MLOGE_BOTH("open %s fail,%s",file,strerror(errno));
		return NULL;
	}

	int i=0;
	du_list = NULL;
	while (!feof(fp)) {
		if (fgets(buf,sizeof(buf),fp) == NULL)
			continue;
		if (buf[0] == '#' || buf[0] == '\n')
			continue;

		char *p = strchr(buf,'\n');
		if (p) *p = '\0';

		des = func = NULL;
		src = strtok(buf," ");
		if (src != NULL) {
			des = strtok(NULL, " ");
			if (des != NULL)
				func =  strtok(NULL, " ");
		}

		du_p = (struct dump_util*)malloc(sizeof(struct dump_util));
		if (du_p == NULL) {
			MLOGE_BOTH("malloc() fail for %s", buf);
			continue;
		}
		memset(du_p,0x0,sizeof(struct dump_util));
		if (src != NULL)
			strncpy(du_p->src_file,src,sizeof(du_p->src_file)-1);
		if (des != NULL)
			snprintf(du_p->des_file, sizeof(du_p->des_file), "%s%s",path,des);
		else
			snprintf(du_p->des_file, sizeof(du_p->des_file), "%s%s",path,PHONE_INFO_FILE);

		set_func(du_p,func);

		du_p->next = du_list;
		du_list = du_p;
	}
	fclose(fp);
	return du_list;
}

struct dump_util *add_dump_file(char *src,char *des,char *path, void* func)
{
	struct dump_util *du_p;
	du_p = (struct dump_util*)malloc(sizeof(struct dump_util));
	if (du_p == NULL)
		return NULL;

	strncpy(du_p->src_file,src,sizeof(du_p->src_file)-1);
	snprintf(du_p->des_file, sizeof(du_p->des_file), "%s%s",path,des);
	du_p->dump = func;
	du_p->next = NULL;

	return du_p;
}

void add_to_dump_list(struct dump_util **du_list,struct dump_util *item)
{
	if (item == NULL)
		return;
	item->next = *du_list;
	*du_list = item;
}

struct dump_util *default_init_dump_list(char *des_path)
{
	struct dump_util *du_list,*du_p;

	du_list = NULL;
	du_p = add_dump_file("property", PHONE_INFO_FILE,des_path,dump_sys_prop);
	add_to_dump_list(&du_list,du_p);

	du_p = add_dump_file("/proc/last_kmsg", "last_kmsg",des_path, dump_file);
	add_to_dump_list(&du_list,du_p);
	du_p = add_dump_file("/system/vendor/data/misc/ProjectConfig.mk", "ProjectConfig.mk",des_path, dump_file);
	add_to_dump_list(&du_list,du_p);
	du_p = add_dump_file("/proc/cmdline", "cmdline",des_path, dump_file);
	add_to_dump_list(&du_list,du_p);
	du_p = add_dump_file("/proc/bootprof", "bootprof",des_path, dump_file);
	add_to_dump_list(&du_list,du_p);
	du_p = add_dump_file("/proc/pl_lk", "pl_lk",des_path, dump_file);
	add_to_dump_list(&du_list,du_p);

	du_p = add_dump_file(MBLOG_HISTORY,     PHONE_INFO_FILE, des_path, dump_file);
	add_to_dump_list(&du_list,du_p);

	du_p = add_dump_file(CONFIG_FILE,       PHONE_INFO_FILE, des_path, dump_file);
	add_to_dump_list(&du_list,du_p);

	du_p = add_dump_file(INTER_DIR_FILE,    PHONE_INFO_FILE, des_path, dump_file);
	add_to_dump_list(&du_list,du_p);

	du_p = add_dump_file(OUTER_DIR_FILE,    PHONE_INFO_FILE, des_path, dump_file);
	add_to_dump_list(&du_list,du_p);

	du_p = add_dump_file(DUMP_FILES,        PHONE_INFO_FILE, des_path, dump_file);
	add_to_dump_list(&du_list,du_p);

	return du_list;
}

struct dump_util *init_dump_list(char *des_path)
{
	if (access(DUMP_FILES,F_OK) == 0)
		return init_dump_list_from_file(DUMP_FILES,des_path);
	else
		return default_init_dump_list(des_path);
}

void dump_system_info()
{
	struct dump_util *du_list,*du_p;

	du_list = init_dump_list(cur_logging_path);

	if (du_list == NULL) {
		MLOGE_BOTH("init dump list fail");
		return;
	}

	while (du_list) {
		du_p = du_list;
		du_list = du_list->next;
		du_p->dump(du_p);
		free(du_p);
	}
}

char need_copy_dirs[][128] = {
	PATH_NORMAL,
	PATH_IPO,
	PATH_META,
	PATH_FACTORY,
	PATH_OTHER_BOOT
};
void copy_to_sdcard()
{
	int i,num;
	char temp[128]= {0};
	char ptfix[128]= {0};
	char srcdir[128]= {0};

	num = sizeof(need_copy_dirs)/sizeof(need_copy_dirs[0]);
	for (i=0; i<num; i++) {
		snprintf(temp, sizeof(temp), "%s", need_copy_dirs[i]);
		if (!access(need_copy_dirs[i], F_OK)) {
			copy_dir(need_copy_dirs[i], cur_logging_path, basename(temp));
			MLOGI_BOTH("copy %s done", need_copy_dirs[i]);
		}
	}
	for (i=0; i<num; i++) {
		snprintf(temp, sizeof(temp), "%s", basename(need_copy_dirs[i]));
		snprintf(ptfix, sizeof(ptfix), "%s%s",temp, ".encry");
		snprintf(temp, sizeof(temp), "%s%s", basename(need_copy_dirs[i]), "/");
		snprintf(srcdir, sizeof(srcdir), "%s%s", ENCRY_TEMP_LOG, temp);
		if (!access(srcdir,F_OK)) {
			copy_dir(srcdir,cur_logging_path,ptfix);
			MLOGI_BOTH("copy %s done",srcdir);
		}
	}
	snprintf(temp, sizeof(temp), "rm -rf %s%s", PATH_TEMP, "*");
	system(temp);
	snprintf(temp, sizeof(temp), "rm -rf %s", ENCRY_TEMP_LOG);
	system(temp);
}

void copy_dir(char *srcpath, char *despath, char *postfix)
{
	copy_logs(srcpath, despath, "main_log", postfix);
	copy_logs(srcpath, despath, "crash_log", postfix);
	copy_logs(srcpath, despath, "sys_log", postfix);
	copy_logs(srcpath, despath, "events_log", postfix);
	copy_logs(srcpath, despath, "radio_log", postfix);
	copy_logs(srcpath, despath, "kernel_log", postfix);
	copy_logs(srcpath, despath, "gz_log", postfix);
	copy_logs(srcpath, despath, "atf_log", postfix);
	copy_logs(srcpath, despath, "scp_log", postfix);
	copy_logs(srcpath, despath, "scp_b_log", postfix);
	copy_logs(srcpath, despath, "sspm_log", postfix);
	// can be optimised
	copy_logs(srcpath, despath, "mmedia_log", postfix);

	copy_logs(srcpath, despath, "bsp_log", postfix);
}

static void insert_to_ordinal_list(file_node_t *node, file_node_t *list)
{
	file_node_t *cur_node;
//	file_node_t *pre_node;
	int result = 0;

	cur_node = list;
	while (cur_node->next) {
		result = (node->index >= cur_node->next->index) ? 1 : -1;
		if (result < 0) {
			node->next = cur_node->next;
			cur_node->next = node;
			break;
		} else {
			cur_node = cur_node->next;
		}
	}

	if (cur_node->next == NULL) {
		cur_node->next = node;
	}

}


void copy_logs(char *srcpath, char *despath, char *filename, char *postfix)
{
	char srcfile[255], dfile[255];
	DIR *src_dir;
	struct dirent *entry;
	file_node_t *list = NULL;
	file_node_t *node = NULL;
	char *st, *end;

	file_node_t *cur_node = NULL;
	file_node_t *free_node = NULL;

	src_dir = opendir(srcpath);
	if (src_dir == NULL) {
		MLOGE_BOTH("Failed to opendir %s, %s, (%s)", srcpath, filename, strerror(errno));
		return;
	}

	/* init list header */
	list = (file_node_t *)malloc(sizeof(file_node_t));
	if (list == NULL) {
		MLOGE_BOTH("dump: malloc list head fail");
		closedir(src_dir);
		return;
	}
	memset(list->name, 0x00, sizeof(list->name));
	list->index = 0;
	list->next = NULL;

	/* * * Loop through the directory stream, create the ordinal 'list'
	*
	*     entry name include 'filename'
	*     eg, 'filename' = kernel_log
	*           entry->d_name : kernel_log_6__2017_0614_102703
	*           entry->d_name : kernel_log_2017_0614_103048.curf
	*
	* * */
	while((entry = readdir(src_dir)) != NULL) {
		if (!strncmp(filename, entry->d_name, strlen(filename))) {
			//MLOGI_BOTH("%s, insert: %s", filename, entry->d_name);
			node = (file_node_t *)malloc(sizeof(file_node_t));
			if (node == NULL) {
				MLOGE_BOTH("dump: malloc list node fail");
				continue;
			}

			snprintf(node->name, sizeof(node->name), "%s", entry->d_name);
			st = strstr(node->name, "_log_");
			end = strstr(node->name, "__");
			if (st != NULL && end != NULL && end > st) {
				node->index = strtol(st + 5 * sizeof(char), NULL, 10);
				//MLOGI_BOTH("index: %ld", node->index);
			} else
				node->index = 1000000;
			node->next = NULL;
			// insert the 'node' to ordinal 'list'
			insert_to_ordinal_list(node, list);

		}
	}

	if (closedir(src_dir)) {
		MLOGI_BOTH("Failed to closedir %s, (%s)", srcpath, strerror(errno));
	}

	/* dump all log files of this kind-log during bootup period into 'dfile' */
	snprintf(dfile, sizeof(dfile), "%s%s.%s", despath, filename, postfix);
	//MLOGI_BOTH("current path: %s", despath);
	cur_node = list->next;
	while (cur_node) {
		snprintf(srcfile, sizeof(srcfile), "%s%s", srcpath, cur_node->name);
		//MLOGI_BOTH("copied file: %s", srcfile);
		cur_node = cur_node->next;
		if (!access(srcfile, F_OK))
			copy_file(srcfile, dfile);
	}

	/* free the whole ordinal 'list' */
	cur_node = list;
	while(cur_node) {
		free_node = cur_node;
		cur_node = cur_node->next;
		//MLOGI_BOTH("free %s", free_node->name);
		free(free_node);
	}

}

void do_copy_and_dump()
{
	prctl(PR_SET_NAME, "mobile_log_d.cp");
	IoSchedClass mbISC;
	int pid,mb_io_prio;
	int ret = 0;

	MLOGI_BOTH("create thread %d do copy dump", gettid());

	pid = getpid();
	ret = android_get_ioprio(pid, &mbISC, &mb_io_prio);
	if (-1 != ret) {
		MLOGI_BOTH("change ioprio, default:%d,%d",mbISC,mb_io_prio);
		if (android_set_ioprio(pid, IoSchedClass_BE,7))
			MLOGE_BOTH("set ioprio failed (%s)", strerror(errno));
	} else
		MLOGE_BOTH("get ioprio failed (%s)", strerror(errno));

	copy_to_sdcard(); // copy
	dump_system_info(); // dump

	if (-1 != ret) {
		if (android_set_ioprio(pid, mbISC, mb_io_prio))
			MLOGE_BOTH("set ioprio back failed (%s)", strerror(errno));
	}

	MLOGI_BOTH("copydump over");
}



void copy_and_dump()
{
	pthread_t cydp_t;
	pthread_attr_t attr;

	if (!pthread_attr_init(&attr)) {
		if (!pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED)) {
			if (!pthread_create(&cydp_t, &attr, (void *)do_copy_and_dump, NULL))
				pthread_attr_destroy(&attr);
		}
	}
}
