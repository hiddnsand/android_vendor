package com.mediatek.op09.telecom;

import android.content.Context;

import com.mediatek.server.telecom.ext.ICallMgrExt;
import com.mediatek.server.telecom.ext.IPhoneAccountExt;
import com.mediatek.server.telecom.ext.OpTelecomCustomizationFactoryBase;

public class Op09TelecomCustomizationFactory extends OpTelecomCustomizationFactoryBase {
    public Context mContext;
    public Op09TelecomCustomizationFactory (Context context){
        mContext = context;
    }

    public ICallMgrExt makeCallMgrExt() {
        return new Op09CallMgrExt();
    }

    public IPhoneAccountExt makePhoneAccountExt() {
        return new Op09PhoneAccountExt();
    }
}
