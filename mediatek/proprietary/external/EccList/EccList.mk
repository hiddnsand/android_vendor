#
# Ecc List XML File
# 

LOCAL_PATH := vendor/mediatek/proprietary/external/EccList

OPTR := NONE
ifdef OPTR_SPEC_SEG_DEF
   ifneq ($(OPTR_SPEC_SEG_DEF),NONE)
       OPTR := $(word 1,$(subst _,$(space),$(OPTR_SPEC_SEG_DEF)))
   endif
endif

ifneq ($(wildcard $(LOCAL_PATH)/ecc_list_$(OPTR).xml),)
    PRODUCT_COPY_FILES += $(LOCAL_PATH)/ecc_list_$(OPTR).xml:$(TARGET_COPY_OUT_VENDOR)/etc/ecc_list.xml:mtk
    ifeq ($(strip $(MTK_CIP_SUPPORT)), yes)
        PRODUCT_COPY_FILES += $(LOCAL_PATH)/ecc_list_$(OPTR).xml:custom/etc/ecc_list.xml
    endif
else
    PRODUCT_COPY_FILES += $(LOCAL_PATH)/ecc_list.xml:$(TARGET_COPY_OUT_VENDOR)/etc/ecc_list.xml:mtk
endif

ifneq ($(wildcard $(LOCAL_PATH)/cdma_ecc_list_$(OPTR).xml),)
    PRODUCT_COPY_FILES += $(LOCAL_PATH)/cdma_ecc_list_$(OPTR).xml:$(TARGET_COPY_OUT_VENDOR)/etc/cdma_ecc_list.xml:mtk
else
    PRODUCT_COPY_FILES += $(LOCAL_PATH)/cdma_ecc_list.xml:$(TARGET_COPY_OUT_VENDOR)/etc/cdma_ecc_list.xml:mtk
endif

PRODUCT_COPY_FILES += $(LOCAL_PATH)/cdma_ecc_list_ss.xml:$(TARGET_COPY_OUT_VENDOR)/etc/cdma_ecc_list_ss.xml:mtk
