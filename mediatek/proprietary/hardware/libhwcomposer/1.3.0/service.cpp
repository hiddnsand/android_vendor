#define DEBUG_LOG_TAG "SERVICE"

#include "service.h"

#include <binder/IServiceManager.h>

ANDROID_SINGLETON_STATIC_INSTANCE(ServiceManager);

ServiceManager::ServiceManager()
{
}

ServiceManager::~ServiceManager()
{
}

void ServiceManager::init()
{
    sp<IServiceManager> sm(defaultServiceManager());
    if (m_guiext == NULL) {
        m_guiext = new GuiExtService();
        sm->addService(String16(GuiExtService::getServiceName()), m_guiext, false);
    }
}
