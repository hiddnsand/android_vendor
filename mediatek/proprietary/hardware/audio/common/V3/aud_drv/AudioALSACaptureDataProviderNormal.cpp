#ifdef LOG_TAG
#undef LOG_TAG
#endif

#define LOG_TAG "AudioALSACaptureDataProviderNormal"

#include "AudioALSACaptureDataProviderNormal.h"

#include <pthread.h>
#include <sys/prctl.h>

#include "AudioALSADriverUtility.h"
#include "AudioType.h"
#include "AudioSpeechEnhanceInfo.h"
#include "AudioALSASpeechPhoneCallController.h"
#include "AudioALSAStreamManager.h"
#include "AudioALSAHardwareResourceManager.h"

#ifdef MTK_LATENCY_DETECT_PULSE
#include "AudioDetectPulse.h"
#endif



#define calc_time_diff(x,y) ((x.tv_sec - y.tv_sec )+ (double)( x.tv_nsec - y.tv_nsec ) / (double)1000000000)


#define AUDIO_CHANNEL_IN_3MIC (AUDIO_CHANNEL_IN_LEFT | AUDIO_CHANNEL_IN_RIGHT | AUDIO_CHANNEL_IN_BACK)
#define AUDIO_CHANNEL_IN_4MIC (AUDIO_CHANNEL_IN_LEFT | AUDIO_CHANNEL_IN_RIGHT | AUDIO_CHANNEL_IN_BACK) // ch4 == ch3

#define UPLINK_SET_AMICDCC_BUFFER_TIME_MS 80

namespace android {


/*==============================================================================
 *                     Constant
 *============================================================================*/


static uint32_t kReadBufferSize = 0;
static const uint32_t kDCRReadBufferSize = 0x2EE00; //48K\stereo\1s data , calculate 1time/sec

//static FILE *pDCCalFile = NULL;
static bool btempDebug = false;



/*==============================================================================
 *                     Utility
 *============================================================================*/

#define LINEAR_4CH_TO_3CH(linear_buf, data_size, type) \
    ({ \
        uint32_t __channel_size = (data_size >> 2); \
        uint32_t __num_sample = __channel_size / sizeof(type); \
        uint32_t __data_size_3ch = __channel_size * 3; \
        type    *__linear_4ch = (type *)(linear_buf); \
        type    *__linear_3ch = (type *)(linear_buf); \
        uint32_t __idx_sample = 0; \
        for (__idx_sample = 0; __idx_sample < __num_sample; __idx_sample++) { \
            memcpy(__linear_3ch, __linear_4ch, 3 * sizeof(type)); \
            __linear_3ch += 3; \
            __linear_4ch += 4; \
        } \
        __data_size_3ch; \
    })


static uint32_t doDownMixFrom4chTo3ch(void *linear_buf, uint32_t data_size, uint32_t audio_format) {
    if (audio_format == AUDIO_FORMAT_PCM_16_BIT) {
        return LINEAR_4CH_TO_3CH(linear_buf, data_size, int16_t);
    } else if (audio_format == AUDIO_FORMAT_PCM_32_BIT ||
               audio_format == AUDIO_FORMAT_PCM_8_24_BIT) {
        return LINEAR_4CH_TO_3CH(linear_buf, data_size, int32_t);
    } else {
        return 0;
    }
}


/*==============================================================================
 *                     Implementation
 *============================================================================*/

AudioALSACaptureDataProviderNormal *AudioALSACaptureDataProviderNormal::mAudioALSACaptureDataProviderNormal = NULL;
AudioALSACaptureDataProviderNormal *AudioALSACaptureDataProviderNormal::getInstance() {
    static AudioLock mGetInstanceLock;
    AL_AUTOLOCK(mGetInstanceLock);

    if (mAudioALSACaptureDataProviderNormal == NULL) {
        mAudioALSACaptureDataProviderNormal = new AudioALSACaptureDataProviderNormal();
    }
    ASSERT(mAudioALSACaptureDataProviderNormal != NULL);
    return mAudioALSACaptureDataProviderNormal;
}

AudioALSACaptureDataProviderNormal::AudioALSACaptureDataProviderNormal():
    mMixer(AudioALSADriverUtility::getInstance()->getMixer()),
    hReadThread(0),
    mCaptureDropSize(0),
    hDCCalThread(0),
    mDCCalEnable(false),
    mDCCalBufferFull(false),
    mDCCalDumpFile(NULL) {
    ALOGD("%s()", __FUNCTION__);
    memset(&mNewtime, 0, sizeof(mNewtime));
    memset(&mOldtime, 0, sizeof(mOldtime));
    memset(timerec, 0, sizeof(timerec));
    memset((void *)&mDCCalBuffer, 0, sizeof(mDCCalBuffer));
}

AudioALSACaptureDataProviderNormal::~AudioALSACaptureDataProviderNormal() {
    ALOGD("%s()", __FUNCTION__);
}


status_t AudioALSACaptureDataProviderNormal::open() {
    AL_AUTOLOCK(*AudioALSADriverUtility::getInstance()->getStreamSramDramLock());
    ALOGD("%s(+)", __FUNCTION__);

    ASSERT(mEnable == false);

    int pcmindex = AudioALSADeviceParser::getInstance()->GetPcmIndexByString(keypcmUl1Capture);
    int cardindex = AudioALSADeviceParser::getInstance()->GetCardIndexByString(keypcmUl1Capture);
    bool audiomode = AudioALSAStreamManager::getInstance()->isModeInPhoneCall(); // TODO: should not use AudioALSAStreamManager.......
    ALOGD("audiomode=%d, cardindex = %d, pcmindex = %d", audiomode, cardindex, pcmindex);

    struct pcm_params *params;
    params = pcm_params_get(cardindex, pcmindex,  PCM_IN);

    ASSERT(params != NULL);

    unsigned int buffersizemax = pcm_params_get_max(params, PCM_PARAM_BUFFER_BYTES);
    pcm_params_free(params);

    bool bHifiRecord = AudioSpeechEnhanceInfo::getInstance()->GetHifiRecord();
    ALOGD("bHifiRecord: %d", bHifiRecord);

    //debug++
    btempDebug = AudioSpeechEnhanceInfo::getInstance()->GetDebugStatus();
    ALOGD("btempDebug: %d", btempDebug);
    //debug--


#ifndef MTK_AURISYS_FRAMEWORK_SUPPORT
    mConfig.channels = 2; // non aurisys... use 2ch
#else
    if (mStreamAttributeSource.input_device == AUDIO_DEVICE_IN_WIRED_HEADSET ||
        mStreamAttributeSource.input_source == AUDIO_SOURCE_UNPROCESSED) {
        mConfig.channels = 1;
    } else if (mStreamAttributeSource.input_device == AUDIO_DEVICE_IN_BUILTIN_MIC) {
        mConfig.channels = AudioALSAHardwareResourceManager::getInstance()->getNumPhoneMicSupport();

        switch (mConfig.channels) {
        case 1:
        case 2:
            break;
        case 3:
            ALOGV("alsa not support 3ch... use 4ch"); // ch4 is same as ch3
            mConfig.channels = 4;
            break;
        case 4:
            break;
        default:
            ALOGW("mConfig.channels = %d!! use 2ch", mConfig.channels);
            mConfig.channels = 2;
        }
    } else {
        ALOGW("device 0x%x not support... use 2ch", mStreamAttributeSource.input_device);
        mConfig.channels = 2;
    }
#endif // end of MTK_AURISYS_FRAMEWORK_SUPPORT


    mConfig.rate = 48000;

    mlatency = UPLINK_NORMAL_LATENCY_MS ; //20ms
#ifdef UPLINK_LOW_LATENCY
    if (HasLowLatencyCapture()) { mlatency = UPLINK_LOW_LATENCY_MS; }
#endif

    if (bHifiRecord == true) {
        mConfig.rate = 96000;
    }

#ifdef MTK_DMIC_SR_LIMIT
    if (IsAudioSupportFeature(AUDIO_SUPPORT_DMIC)) {
        if (mStreamAttributeSource.input_device != AUDIO_DEVICE_IN_WIRED_HEADSET) {
            mConfig.rate = 32000;
        }
    }
#endif

    if (audiomode == true) {
        //if not BT phonecall
        if (!(audio_is_bluetooth_sco_device(mStreamAttributeSource.output_devices))) {
            mConfig.rate = AudioALSASpeechPhoneCallController::getInstance()->getSampleRate();
            ALOGD("phone call mode,change smaple rate: audiomode=%d, mConfig.rate=%d", audiomode, mConfig.rate);
        }
    }

#ifdef RECORD_INPUT_24BITS
    mConfig.format = PCM_FORMAT_S32_LE;
    mStreamAttributeSource.audio_format = AUDIO_FORMAT_PCM_8_24_BIT;
#else
    mConfig.format = PCM_FORMAT_S16_LE;
    mStreamAttributeSource.audio_format = AUDIO_FORMAT_PCM_16_BIT;
#endif

    // config attribute (will used in client SRC/Enh/... later)
    switch (mConfig.channels) {
    case 1:
        mStreamAttributeSource.audio_channel_mask = AUDIO_CHANNEL_IN_MONO;
        break;
    case 2:
        mStreamAttributeSource.audio_channel_mask = AUDIO_CHANNEL_IN_STEREO;
        break;
    case 3:
        mStreamAttributeSource.audio_channel_mask = AUDIO_CHANNEL_IN_3MIC;
        break;
    case 4:
        mStreamAttributeSource.audio_channel_mask = AUDIO_CHANNEL_IN_4MIC;
        break;
    default:
        mStreamAttributeSource.audio_channel_mask = AUDIO_CHANNEL_IN_STEREO;
    }

    mStreamAttributeSource.num_channels = mConfig.channels;
    mStreamAttributeSource.sample_rate = mConfig.rate;  //48000;
#ifdef MTK_DMIC_SR_LIMIT
    mStreamAttributeSource.sample_rate = mConfig.rate;  //32000;
#endif

    // Reset frames readed counter
    mStreamAttributeSource.Time_Info.total_frames_readed = 0;

    kReadBufferSize = getPeriodBufSize(&mStreamAttributeSource, mlatency);

    // Memory 64bytes alignment limitation would cause latency variation isssue
    // Less read buffer size Less the latency variation to pass CTS test
    kReadBufferSize = kReadBufferSize / 2;
#if 0 // cause latency issue...... = =a
    kReadBufferSize &= 0xFFFFFFC0; // (UL)48K\5ms data\stereo\4byte\(Align64byte)
#endif


    // should set mStreamAttributeSource.num_channels after getPeriodBufSize
    if (mConfig.channels == 4) {
        ALOGV("src not support 4ch -> 3ch... use 3ch");
        mStreamAttributeSource.num_channels = 3;
    }

#ifdef UPLINK_LOW_LATENCY
    mConfig.period_size = kReadBufferSize / mConfig.channels / (pcm_format_to_bits(mConfig.format) / 8);  //period size will impact the interrupt interval
#else
    mConfig.period_size = (buffersizemax / mConfig.channels / (pcm_format_to_bits(mConfig.format) / 8) / mConfig.period_count;
#endif

    mConfig.period_count = buffersizemax / (mConfig.period_size * mConfig.channels * pcm_format_to_bits(mConfig.format) / 8);

    ALOGD("pcm format: %d, channels: %d, rate: %d, period_size: %d, period_count: %d, buffersizemax: %d",
          mConfig.format, mConfig.channels, mConfig.rate, mConfig.period_size, mConfig.period_count, buffersizemax);
    mConfig.start_threshold = 0;
    mConfig.stop_threshold = 0;
    mConfig.silence_threshold = 0;

    mCaptureDataProviderType = CAPTURE_PROVIDER_NORMAL;

#ifdef MTK_VOW_DCCALI_SUPPORT
    //DC cal
    memset((void *)&mDCCalBuffer, 0, sizeof(mDCCalBuffer));
    mDCCalEnable = false;
    mDCCalBufferFull = false;
    mDCCalDumpFile = NULL;
#endif


    OpenPCMDump(LOG_TAG);

    ALOGD("latency: %d, kReadBufferSize: %u, mCaptureDropSize: %u",
          mlatency, kReadBufferSize, mCaptureDropSize);

    // enable pcm
    ASSERT(mPcm == NULL);
    int pcmIdx = AudioALSADeviceParser::getInstance()->GetPcmIndexByString(keypcmUl1Capture);
    int cardIdx = AudioALSADeviceParser::getInstance()->GetCardIndexByString(keypcmUl1Capture);

#if defined(CAPTURE_MMAP) // must be after pcm open
    unsigned int flag = PCM_IN | PCM_MONOTONIC | PCM_MMAP;
    openPcmDriverWithFlag(pcmindex, flag);
#else
    mPcm = pcm_open(cardIdx, pcmIdx, PCM_IN | PCM_MONOTONIC, &mConfig);
#endif
    ASSERT(mPcm != NULL && pcm_is_ready(mPcm) == true);
    ALOGV("%s(), mPcm = %p", __FUNCTION__, mPcm);

    int prepare_error = pcm_prepare(mPcm);
    if (prepare_error != 0) {
        ASSERT(0);
        pcm_close(mPcm);
        mPcm = NULL;
        return UNKNOWN_ERROR;
    }

    // Need to enable PMIC device before AFE to reduce mute data in the beginning
    enablePimcInputDevice(true);

    if (isNeedSyncPcmStart() == false) {
        pcm_start(mPcm);
    } else {
        mStart = false;
        mReadThreadReady = false;
    }

    // create reading thread
    mEnable = true;
    int ret = pthread_create(&hReadThread, NULL, AudioALSACaptureDataProviderNormal::readThread, (void *)this);
    if (ret != 0) {
        ALOGE("%s() create hReadThread fail!!", __FUNCTION__);
        return UNKNOWN_ERROR;
    }


#ifdef MTK_VOW_DCCALI_SUPPORT
    //create DC cal thread
    ret = pthread_create(&hDCCalThread, NULL, AudioALSACaptureDataProviderNormal::DCCalThread, (void *)this);
    if (ret != 0) {
        ALOGE("%s() create DCCal thread fail!!", __FUNCTION__);
        return UNKNOWN_ERROR;
    }

    ret = pthread_mutex_init(&mDCCal_Mutex, NULL);
    if (ret != 0) {
        ALOGE("%s, Failed to initialize mDCCal_Mutex!", __FUNCTION__);
    }

    ret = pthread_cond_init(&mDCCal_Cond, NULL);
    if (ret != 0) {
        ALOGE("%s, Failed to initialize mDCCal_Cond!", __FUNCTION__);
    }

    mDCCalEnable = true;

    mDCCalBuffer.pBufBase = new char[kDCRReadBufferSize];
    mDCCalBuffer.bufLen   = kDCRReadBufferSize;
    mDCCalBuffer.pRead    = mDCCalBuffer.pBufBase;
    mDCCalBuffer.pWrite   = mDCCalBuffer.pBufBase;
    ASSERT(mDCCalBuffer.pBufBase != NULL);

    AL_AUTOLOCK(mDCCalEnableLock);
    mDCCalEnable = true;

    OpenDCCalDump();
#if 0
    pDCCalFile = fopen("/sdcard/mtklog/DCCalFile.pcm", "wb");
    if (pDCCalFile == NULL) {
        ALOGW("%s(), create pDCCalFile fail ", __FUNCTION__);
    }
#endif
    //create DC cal ---
#endif

    ALOGD("%s(-)", __FUNCTION__);
    return NO_ERROR;
}


status_t AudioALSACaptureDataProviderNormal::close() {
    ALOGD("%s()", __FUNCTION__);

    mEnable = false;
    pthread_join(hReadThread, NULL);
    ALOGD("pthread_join hReadThread done");

    AL_AUTOLOCK(*AudioALSADriverUtility::getInstance()->getStreamSramDramLock());

    ClosePCMDump();

    pcm_stop(mPcm);
    pcm_close(mPcm);
    mPcm = NULL;

#ifdef MTK_VOW_DCCALI_SUPPORT
    mDCCalEnable = false;
    AL_AUTOLOCK(mDCCalEnableLock);
    pthread_cond_signal(&mDCCal_Cond);

    CloseDCCalDump();

    if (mDCCalBuffer.pBufBase != NULL) { delete[] mDCCalBuffer.pBufBase; }
#endif

    ALOGD("-%s()", __FUNCTION__);
    return NO_ERROR;
}

void *AudioALSACaptureDataProviderNormal::readThread(void *arg) {
    status_t retval = NO_ERROR;
    int ret = 0;
    uint32_t counter = 1;

    AudioALSACaptureDataProviderNormal *pDataProvider = static_cast<AudioALSACaptureDataProviderNormal *>(arg);

    uint32_t open_index = pDataProvider->mOpenIndex;

    char nameset[32];
    sprintf(nameset, "%s%d", __FUNCTION__, pDataProvider->mCaptureDataProviderType);
    prctl(PR_SET_NAME, (unsigned long)nameset, 0, 0, 0);
    pDataProvider->setThreadPriority();

    ALOGD("+%s(), pid: %d, tid: %d, kReadBufferSize=0x%x, open_index=%d, UPLINK_SET_AMICDCC_BUFFER_TIME_MS = %d, counter=%d ", __FUNCTION__, getpid(), gettid(), kReadBufferSize, open_index, UPLINK_SET_AMICDCC_BUFFER_TIME_MS, counter);

    pDataProvider->waitPcmStart();

    // read raw data from alsa driver
    char linear_buffer[kReadBufferSize];
    uint32_t Read_Size = kReadBufferSize;
    uint32_t kReadBufferSize_new;
    while (pDataProvider->mEnable == true) {
        ASSERT(open_index == pDataProvider->mOpenIndex);
        ASSERT(pDataProvider->mPcm != NULL);

        if (btempDebug) {
            clock_gettime(CLOCK_REALTIME, &pDataProvider->mNewtime);
            pDataProvider->timerec[0] = calc_time_diff(pDataProvider->mNewtime, pDataProvider->mOldtime);
            pDataProvider->mOldtime = pDataProvider->mNewtime;
        }

        ret = pDataProvider->pcmRead(pDataProvider->mPcm, linear_buffer, kReadBufferSize);
        if (ret != 0) {
            ALOGE("%s(), pcm_read() error, retval = %d", __FUNCTION__, retval);
            clock_gettime(CLOCK_REALTIME, &pDataProvider->mOldtime);
            continue;
        }

        //struct timespec tempTimeStamp;
        retval = pDataProvider->GetCaptureTimeStamp(&pDataProvider->mStreamAttributeSource.Time_Info, kReadBufferSize);
#if 0
        if (retval != NO_ERROR) {
            clock_gettime(CLOCK_REALTIME, &pDataProvider->mOldtime);
            continue;
        }
#endif

        if (btempDebug) {
            clock_gettime(CLOCK_REALTIME, &pDataProvider->mNewtime);
            pDataProvider->timerec[1] = calc_time_diff(pDataProvider->mNewtime, pDataProvider->mOldtime);
            pDataProvider->mOldtime = pDataProvider->mNewtime;
        }

#ifdef MTK_LATENCY_DETECT_PULSE
        stream_attribute_t *attribute = &(pDataProvider->mStreamAttributeSource);
        AudioDetectPulse::doDetectPulse(TAG_CAPTURE_DATA_PROVIDER, PULSE_LEVEL, 0, (void *)linear_buffer,
                                        kReadBufferSize, attribute->audio_format, attribute->num_channels,
                                        attribute->sample_rate);
#endif

#ifdef MTK_VOW_DCCALI_SUPPORT
        //copy data to DC Cal
        pDataProvider->copyCaptureDataToDCCalBuffer(linear_buffer, kReadBufferSize_new);
#endif

        if (pDataProvider->mConfig.channels == 4 && pDataProvider->mPCMDumpFile4ch) {
            AudioDumpPCMData(linear_buffer, kReadBufferSize, pDataProvider->mPCMDumpFile4ch);
        }

        // 4ch to 3ch
        if (pDataProvider->mConfig.channels == 4 &&
            pDataProvider->mStreamAttributeSource.num_channels == 3) {
            Read_Size = doDownMixFrom4chTo3ch(
                            linear_buffer,
                            kReadBufferSize,
                            pDataProvider->mStreamAttributeSource.audio_format);
        } else {
            Read_Size = kReadBufferSize;
        }

        // Adjust AMIC 3DB Corner clock setting
        if (counter <= (UPLINK_SET_AMICDCC_BUFFER_TIME_MS / pDataProvider->mlatency)) {
            if (counter == (UPLINK_SET_AMICDCC_BUFFER_TIME_MS / pDataProvider->mlatency)) {
                pDataProvider->adjustSpike();
            }
            counter++;
        }

        // use ringbuf format to save buffer info
        pDataProvider->mPcmReadBuf.pBufBase = linear_buffer;
        pDataProvider->mPcmReadBuf.bufLen   = Read_Size + 1; // +1: avoid pRead == pWrite
        pDataProvider->mPcmReadBuf.pRead    = linear_buffer;
        pDataProvider->mPcmReadBuf.pWrite   = linear_buffer + Read_Size;
        pDataProvider->provideCaptureDataToAllClients(open_index);

        if (btempDebug) {
            clock_gettime(CLOCK_REALTIME, &pDataProvider->mNewtime);
            pDataProvider->timerec[2] = calc_time_diff(pDataProvider->mNewtime, pDataProvider->mOldtime);
            pDataProvider->mOldtime = pDataProvider->mNewtime;

            ALOGD("%s, latency_in_us,%1.6lf,%1.6lf,%1.6lf", __FUNCTION__, pDataProvider->timerec[0], pDataProvider->timerec[1], pDataProvider->timerec[2]);
        }
    }

    ALOGD("-%s(), pid: %d, tid: %d", __FUNCTION__, getpid(), gettid());
    pthread_exit(NULL);
    return NULL;
}

void  AudioALSACaptureDataProviderNormal::adjustSpike()
{
    status_t retval = mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mAudioALSACaptureDataProviderNormal->mMixer, "Audio_AMIC_DCC_Setting"), "On");
    if (retval != 0) {
        ALOGD("%s(), Can not find Audio_AMIC_DCC_Setting!", __FUNCTION__);
    }
}

#ifdef MTK_VOW_DCCALI_SUPPORT
void AudioALSACaptureDataProviderNormal::copyCaptureDataToDCCalBuffer(void *buffer, size_t size) {
    size_t copysize = size;
    uint32_t freeSpace = RingBuf_getFreeSpace(&mDCCalBuffer);
    ALOGV("%s(), freeSpace(%u), dataSize(%u),mDCCalBufferFull=%d", __FUNCTION__, freeSpace, size, mDCCalBufferFull);

    if (mDCCalBufferFull == false) {
        if (freeSpace > 0) {
            if (freeSpace < size) {
                ALOGD("%s(), freeSpace(%u) < dataSize(%u), buffer full!!", __FUNCTION__, freeSpace, size);
                //ALOGD("%s before,pBase = 0x%x pWrite = 0x%x  bufLen = %d  pRead = 0x%x",__FUNCTION__,
                //mDCCalBuffer.pBufBase,mDCCalBuffer.pWrite, mDCCalBuffer.bufLen,mDCCalBuffer.pRead);

                RingBuf_copyFromLinear(&mDCCalBuffer, (char *)buffer, freeSpace);

                //ALOGD("%s after,pBase = 0x%x pWrite = 0x%x  bufLen = %d  pRead = 0x%x",__FUNCTION__,
                //mDCCalBuffer.pBufBase,mDCCalBuffer.pWrite, mDCCalBuffer.bufLen,mDCCalBuffer.pRead);
            } else {
                //ALOGD("%s before,pBase = 0x%x pWrite = 0x%x  bufLen = %d  pRead = 0x%x",__FUNCTION__,
                //mDCCalBuffer.pBufBase,mDCCalBuffer.pWrite, mDCCalBuffer.bufLen,mDCCalBuffer.pRead);

                RingBuf_copyFromLinear(&mDCCalBuffer, (char *)buffer, size);

                //ALOGD("%s after,pBase = 0x%x pWrite = 0x%x  bufLen = %d  pRead = 0x%x",__FUNCTION__,
                //mDCCalBuffer.pBufBase,mDCCalBuffer.pWrite, mDCCalBuffer.bufLen,mDCCalBuffer.pRead);
            }
        } else {
            mDCCalBufferFull = true;
            pthread_cond_signal(&mDCCal_Cond);
        }
    }
}

size_t AudioALSACaptureDataProviderNormal::CalulateDC(short *buffer, size_t size) {
    //ALOGV("%s()+,Size(%u)", __FUNCTION__, size);
    int checksize = size >> 2;  //stereo, 16bits
    int count = checksize;
    int accumulateL = 0, accumulateR = 0;
    short DCL = 0, DCR = 0;

#if 0
    if (pDCCalFile != NULL) {
        fwrite(buffer, sizeof(char), size, pDCCalFile);
    }
#endif
    WriteDCCalDumpData((void *)buffer, size);

    while (count) {
        accumulateL += *(buffer);
        accumulateR += *(buffer + 1);
        buffer += 2;
        count--;
    }
    DCL = (short)(accumulateL / checksize);
    DCR = (short)(accumulateR / checksize);

    ALOGD("%s()- ,checksize(%d),accumulateL(%d),accumulateR(%d), DCL(%d), DCR(%d)", __FUNCTION__, checksize, accumulateL, accumulateR, DCL, DCR);
    return size;
}

void *AudioALSACaptureDataProviderNormal::DCCalThread(void *arg) {
    prctl(PR_SET_NAME, (unsigned long)__FUNCTION__, 0, 0, 0);

    ALOGD("+%s(), pid: %d, tid: %d, kDCRReadBufferSize=%x", __FUNCTION__, getpid(), gettid(), kDCRReadBufferSize);

    status_t retval = NO_ERROR;
    AudioALSACaptureDataProviderNormal *pDataProvider = static_cast<AudioALSACaptureDataProviderNormal *>(arg);


    //char linear_buffer[kDCRReadBufferSize];
    char *plinear_buffer = new char[kDCRReadBufferSize];
    uint32_t Read_Size = kDCRReadBufferSize;
    while (pDataProvider->mDCCalEnable == true) {
        pthread_mutex_lock(&pDataProvider->mDCCal_Mutex);
        pthread_cond_wait(&pDataProvider->mDCCal_Cond, &pDataProvider->mDCCal_Mutex);
        //ALOGD("%s(), signal get", __FUNCTION__);

        retval = AL_LOCK_MS(pDataProvider->mDCCalEnableLock, 300);
        ASSERT(retval == NO_ERROR);
        if (pDataProvider->mDCCalEnable == false) {
            AL_UNLOCK(pDataProvider->mDCCalEnableLock);
            pthread_mutex_unlock(&pDataProvider->mDCCal_Mutex);
            break;
        }

        if (pDataProvider->mDCCalBufferFull) {
            Read_Size = RingBuf_getDataCount(&pDataProvider->mDCCalBuffer);
            //ALOGD("%s(), Read_Size =%u, kDCRReadBufferSize=%u", __FUNCTION__,Read_Size,kDCRReadBufferSize);
            if (Read_Size > kDCRReadBufferSize) {
                Read_Size = kDCRReadBufferSize;
            }

            //ALOGD("%s,pBase = 0x%x pWrite = 0x%x  bufLen = %d  pRead = 0x%x",__FUNCTION__,
            //pDataProvider->mDCCalBuffer.pBufBase,pDataProvider->mDCCalBuffer.pWrite, pDataProvider->mDCCalBuffer.bufLen,pDataProvider->mDCCalBuffer.pRead);

            RingBuf_copyToLinear(plinear_buffer, &pDataProvider->mDCCalBuffer, Read_Size);
            //ALOGD("%s after copy,pBase = 0x%x pWrite = 0x%x  bufLen = %d  pRead = 0x%x",__FUNCTION__,
            //pDataProvider->mDCCalBuffer.pBufBase,pDataProvider->mDCCalBuffer.pWrite, pDataProvider->mDCCalBuffer.bufLen,pDataProvider->mDCCalBuffer.pRead);
            pDataProvider->CalulateDC((short *)plinear_buffer, Read_Size);

            pDataProvider->mDCCalBufferFull = false;
        }

        AL_UNLOCK(pDataProvider->mDCCalEnableLock);
        pthread_mutex_unlock(&pDataProvider->mDCCal_Mutex);
    }

    ALOGD("-%s(), pid: %d, tid: %d", __FUNCTION__, getpid(), gettid());
    delete[] plinear_buffer;
    pthread_exit(NULL);
    return NULL;
}


void AudioALSACaptureDataProviderNormal::OpenDCCalDump() {
    ALOGV("%s()", __FUNCTION__);
    char DCCalDumpFileName[128];
    sprintf(DCCalDumpFileName, "%s.pcm", "/sdcard/mtklog/audio_dump/DCCalData");

    mDCCalDumpFile = NULL;
    mDCCalDumpFile = AudioOpendumpPCMFile(DCCalDumpFileName, streamin_propty);

    if (mDCCalDumpFile != NULL) {
        ALOGD("%s, DCCalDumpFileName = %s", __FUNCTION__, DCCalDumpFileName);
    }
}

void AudioALSACaptureDataProviderNormal::CloseDCCalDump() {
    if (mDCCalDumpFile) {
        AudioCloseDumpPCMFile(mDCCalDumpFile);
        ALOGD("%s()", __FUNCTION__);
    }
}

void  AudioALSACaptureDataProviderNormal::WriteDCCalDumpData(void *buffer, size_t size) {
    if (mDCCalDumpFile) {
        //ALOGD("%s()", __FUNCTION__);
        AudioDumpPCMData((void *)buffer, size, mDCCalDumpFile);
    }
}
#else
void AudioALSACaptureDataProviderNormal::copyCaptureDataToDCCalBuffer(void *buffer __unused, size_t size __unused) {
    ALOGE("%s() unsupport", __FUNCTION__);
}

size_t AudioALSACaptureDataProviderNormal::CalulateDC(short *buffer __unused, size_t size __unused) {
    ALOGE("%s() unsupport", __FUNCTION__);
    return 0;
}

void *AudioALSACaptureDataProviderNormal::DCCalThread(void *arg __unused) {
    ALOGE("%s() unsupport", __FUNCTION__);
    return NULL;
}

void AudioALSACaptureDataProviderNormal::OpenDCCalDump() {
    ALOGE("%s() unsupport", __FUNCTION__);
}

void AudioALSACaptureDataProviderNormal::CloseDCCalDump() {
    ALOGE("%s() unsupport", __FUNCTION__);
}

void  AudioALSACaptureDataProviderNormal::WriteDCCalDumpData(void *buffer __unused, size_t size __unused) {
    ALOGE("%s() unsupport", __FUNCTION__);
}

#endif

} // end of namespace android
