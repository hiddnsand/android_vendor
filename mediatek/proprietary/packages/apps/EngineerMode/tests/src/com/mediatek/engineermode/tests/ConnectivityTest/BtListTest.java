/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Bundle;
import android.test.ActivityInstrumentationTestCase2;
import android.test.SingleLaunchActivityTestCase;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.bluetooth.BtList;
import com.mediatek.engineermode.bluetooth.BleTestMode;
import com.mediatek.engineermode.bluetooth.BtRelayerModeActivity;
import com.mediatek.engineermode.bluetooth.NoSigRxTestActivity;
import com.mediatek.engineermode.bluetooth.SspDebugModeActivity;
import com.mediatek.engineermode.bluetooth.TestModeActivity;
import com.mediatek.engineermode.bluetooth.TxOnlyTestActivity;

public class BtListTest extends SingleLaunchActivityTestCase<BtList> {

    private static final String TAG = "EMTest/BtList";
    private static final String EM_BAKC_FAIL_MSG = "Back to BtList fail";
    private Solo mSolo = null;
    private Activity mActivity = null;
    private Context mContext = null;
    private Instrumentation mInst = null;
    private BluetoothAdapter adapter = null;
    private static boolean sFinished = false;

    public BtListTest() {
        super("com.mediatek.engineermode", BtList.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mInst = getInstrumentation();
        mContext = mInst.getTargetContext();
        mActivity = getActivity();
        mSolo = new Solo(mInst, mActivity);
        disableBTAdaptor();
    }

    @Override
    protected void tearDown() throws Exception {
        if (sFinished) {
            mSolo.finishOpenedActivities();
        }
        super.tearDown();
    }

    private void disableBTAdaptor() {
        adapter = BluetoothAdapter.getDefaultAdapter();
        if (BluetoothAdapter.STATE_OFF != adapter.getState()) {
            adapter.disable();
            int jumpCount = 0;
            while ((BluetoothAdapter.STATE_OFF != adapter.getState()) && (jumpCount++ < 15)) {
                EmOperate.waitSomeTime(EmOperate.TIME_SUPER_LONG);
            }
        }
        assertTrue("disable Bt adapter have issues",
                BluetoothAdapter.STATE_OFF == adapter.getState());

        // while (mSolo.searchText(mActivity.getString(R.string.BT_init_dev))) {
        // mSolo.sleep(EmOperate.TIME_MID);
        // }

    }

    private void verifyPreconditions() {
        assertTrue(mInst != null);
        assertTrue(mActivity != null);
        assertTrue(mContext != null);
        assertTrue(mSolo != null);

        if (!mSolo.waitForActivity("BtList", EmOperate.TIME_MID)) {
            Bundle mBundle = new Bundle();
            mBundle.putString("Data", "data from TestBundle");
            launchActivity("com.mediatek.engineermode", BtList.class, mBundle);
        }
    }

    @BasicFuncAnnotation
    public void test01_Precondition() {
        verifyPreconditions();
    }

    @UiAnnotation
    public void test02_TxOnlyTest() {
        if (null == adapter || BluetoothAdapter.STATE_OFF != adapter.getState()) {
            return;
        }

        if (!EmOperate.enterItem(mSolo, "TX Only test", "TxOnlyTestActivity"))
            return;
        mSolo.enterText(0, "10");
        mSolo.enterText(1, "20");

        mSolo.pressSpinnerItem(0, 2);
        mSolo.pressSpinnerItem(1, 1);
        mSolo.pressSpinnerItem(2, 0);

        mSolo.pressMenuItem(0);

        mSolo.sleep(EmOperate.TIME_MID);
        mSolo.goBack();
    }

    @UiAnnotation
    public void test03_NoSigRx() {

        if (null == adapter || BluetoothAdapter.STATE_OFF != adapter.getState()) {
            return;
        }

        if (!EmOperate.enterItem(mSolo, "Non-signaling RX Test", "NoSigRxTestActivity"))
            return;

        mSolo.pressSpinnerItem(0, 1);
        mSolo.pressSpinnerItem(1, 2);

        mSolo.clearEditText(0);
        mSolo.clearEditText(1);

        mSolo.enterText(0, "20");
        mSolo.enterText(1, "15");

        mSolo.clickOnButton(0); // start test

        mSolo.sleep(EmOperate.TIME_SUPER_LONG);

        mSolo.clickOnButton(0); // end test

        mSolo.sleep(EmOperate.TIME_LONG);

        // error test
        mSolo.enterText(0, "90");
        mSolo.enterText(1, "15");

        mSolo.clickOnButton(0);
        mSolo.sleep(EmOperate.TIME_LONG);

        mSolo.clickOnButton("OK");
        mSolo.sleep(EmOperate.TIME_LONG);

        mSolo.goBack();
    }

    @UiAnnotation
    public void test04_TestMode() {
        if (null == adapter || BluetoothAdapter.STATE_OFF != adapter.getState()) {
            return;
        }
        if (!EmOperate.enterItem(mSolo, "Bluetooth_TestMode", "TestModeActivity"))
            return;

        mSolo.clearEditText(0);
        mSolo.enterText(0, "2");
        mSolo.clickOnCheckBox(0);
        mSolo.sleep(EmOperate.TIME_LONG);

        mSolo.clearEditText(0);
        mSolo.enterText(0, "7");
        mSolo.clickOnCheckBox(0);
        mSolo.sleep(EmOperate.TIME_LONG);

        EmOperate.backKey(mInst);
    }

    @UiAnnotation
    public void test05_SspDebug() {

        if (!EmOperate.enterItem(mSolo, "Bluetooth SSP Debug Mode", "SspDebugModeActivity"))
            return;

        if (!mSolo.isCheckBoxChecked(0)) {
            mSolo.clickOnCheckBox(0);
            mSolo.sleep(EmOperate.TIME_LONG);

            mSolo.goBack();

            EmOperate.enterItem(mSolo, "Bluetooth SSP Debug Mode", "SspDebugModeActivity");
            assertTrue("CheckSpinnerItemBack have issues", mSolo.isCheckBoxChecked(0));
            mSolo.clickOnCheckBox(0);

        } else {
            mSolo.sleep(EmOperate.TIME_MID);
            mSolo.clickOnCheckBox(0);
            mSolo.sleep(EmOperate.TIME_LONG);
        }
        mSolo.goBack();
    }

    @UiAnnotation
    public void test06_BleTest() {

        if (null == adapter || BluetoothAdapter.STATE_OFF != adapter.getState()) {
            return;
        }

        if (!EmOperate.enterItem(mSolo, "BLE Test Mode", "BleTestMode"))
            return;

        mSolo.pressSpinnerItem(0, 1);
        mSolo.pressSpinnerItem(1, 2);
        mSolo.clickOnRadioButton(0);
        mSolo.clickOnCheckBox(0);
        mSolo.sleep(EmOperate.TIME_LONG);
        mSolo.clickOnButton(0);

        mSolo.sleep(EmOperate.TIME_LONG);
        mSolo.clickOnButton(1);

        mSolo.sleep(EmOperate.TIME_LONG);

        EmOperate.backKey(mInst);
    }

    @UiAnnotation
    public void test07_RelayerMode() {
        if (null == adapter || BluetoothAdapter.STATE_OFF != adapter.getState()) {
            return;
        }

        if (!EmOperate.enterItem(mSolo, mSolo.getString("BT_RelayerModeTitle"),
                "BtRelayerModeActivity"))
            return;

        mSolo.pressSpinnerItem(0, 1);
        mSolo.pressSpinnerItem(1, 2);
        mSolo.sleep(EmOperate.TIME_LONG);
        mSolo.clickOnButton(0);

        mSolo.sleep(EmOperate.TIME_LONG);
        EmOperate.backKey(mInst);
    }


}
