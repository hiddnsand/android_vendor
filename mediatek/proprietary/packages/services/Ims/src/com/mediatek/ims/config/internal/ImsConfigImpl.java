package com.mediatek.ims.config.internal;

import android.content.Context;
import android.os.Binder;
import android.os.Build;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.telephony.ims.stub.ImsConfigImplBase;
import android.util.Log;

import com.android.ims.ImsConfig;
import com.android.ims.ImsConfigListener;
import com.android.ims.ImsException;
import com.android.ims.internal.IImsConfig;

import com.android.internal.telephony.IccCardConstants;

import com.mediatek.ims.ImsCommonUtil;
import com.mediatek.ims.ril.ImsCommandsInterface;

import java.util.HashMap;

/**
 * Class handles IMS parameter provisioning by carrier.
 *
 *  @hide
 */
public class ImsConfigImpl extends ImsConfigImplBase {
    private static final String TAG = "ImsConfigImpl";
    private static final String PROP_FORCE_DEBUG_KEY = "persist.log.tag.tel_dbg";
    private static final boolean DEBUG = TextUtils.equals(Build.TYPE, "eng")
            || (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1);
    /// M: Sync volte setting value. @{
    private static final String PROPERTY_IMSCONFIG_FORCE_NOTIFY = "ril.imsconfig.force.notify";
    /// @}

    private Context mContext;
    private int mPhoneId;
    private ImsCommandsInterface mRilAdapter;
    private ImsConfigStorage mStorage = null;

    private ImsConfigImpl() {};

    /**
     *
     * Construction function for ImsConfigImpl.
     *
     * @param context the application context
     * @param phoneId the phone id this instance handle for
     *
     */
    public ImsConfigImpl(Context context, ImsCommandsInterface imsRilAdapter,
                         ImsConfigStorage storage, int phoneId) {
        mContext = context;
        mPhoneId = phoneId;
        mRilAdapter = imsRilAdapter;
        mStorage = storage;
    }

    /**
     * Gets the value for ims service/capabilities parameters from the provisioned
     * value storage. Synchronous blocking call.
     *
     * @param item, as defined in com.android.ims.ImsConfig#ConfigConstants.
     * @return value in Integer format.
     */
    @Override
    public int getProvisionedValue(int item) {
        try {
            int result = mStorage.getProvisionedValue(item);
            Log.i(TAG, "getProvisionedValue(" + item + ") : " + result +
                    " on phone" + mPhoneId + " from binder pid " + Binder.getCallingPid() +
                    ", binder uid " + Binder.getCallingUid() + ", process pid " + Process.myPid() +
                    ", process uid " + Process.myUid());
            return result;
        } catch (ImsException e) {
            Log.e(TAG, "getProvisionedValue(" + item + ") failed, code: " + e.getCode());

            if (Binder.getCallingPid() == Process.myPid()) {
                return 0;
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Gets the value for ims service/capabilities parameters from the provisioned
     * value storage. Synchronous blocking call.
     *
     * @param item, as defined in com.android.ims.ImsConfig#ConfigConstants.
     * @return value in String format.
     */
    @Override
    public String getProvisionedStringValue(int item) {
        try {
            String result = mStorage.getProvisionedStringValue(item);
            Log.i(TAG, "getProvisionedStringValue(" + item + ") : " + result +
                    " on phone " + mPhoneId + " from binder pid " + Binder.getCallingPid() +
                    ", binder uid " + Binder.getCallingUid() + ", process pid " + Process.myPid() +
                    ", process uid " + Process.myUid());
            return result;
        } catch (ImsException e) {
            Log.e(TAG, "getProvisionedStringValue(" + item + ") failed, code: " + e.getCode());

            if (Binder.getCallingPid() == Process.myPid()) {
                return "Unknown";
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Sets the value for IMS service/capabilities parameters by the operator device
     * management entity. It sets the config item value in the provisioned storage
     * from which the master value is derived. Synchronous blocking call.
     *
     * @param item, as defined in com.android.ims.ImsConfig#ConfigConstants.
     * @param value in Integer format.
     * @return as defined in com.android.ims.ImsConfig#OperationStatusConstants.
     */
    @Override
    public int setProvisionedValue(int item, int value) {
        try {
            Log.i(TAG, "setProvisionedValue(" + item + ", " + value +
                    ") on phone " + mPhoneId + " from pid " + Binder.getCallingPid() +
                    ", uid " + Binder.getCallingUid());
            mStorage.setProvisionedValue(item, value);
        } catch (ImsException e) {
            Log.e(TAG, "setProvisionedValue(" + item + ") failed, code: " + e.getCode());
            return ImsConfig.OperationStatusConstants.FAILED;
        }
        return ImsConfig.OperationStatusConstants.SUCCESS;
    }

    /**
     * Sets the value for IMS service/capabilities parameters by the operator device
     * management entity. It sets the config item value in the provisioned storage
     * from which the master value is derived.  Synchronous blocking call.
     *
     * @param item as defined in com.android.ims.ImsConfig#ConfigConstants.
     * @param value in String format.
     * @return as defined in com.android.ims.ImsConfig#OperationStatusConstants.
     */
    @Override
    public int setProvisionedStringValue(int item, String value) {
        try {
            Log.i(TAG, "setProvisionedStringValue(" + item + ", " + value +
                    ") on phone " + mPhoneId + " from pid " + Binder.getCallingPid() +
                    ", uid " + Binder.getCallingUid());
            mStorage.setProvisionedStringValue(item, value);
        } catch (ImsException e) {
            Log.e(TAG, "setProvisionedValue(" + item + ") failed, code: " + e.getCode());
            return ImsConfig.OperationStatusConstants.FAILED;
        }
        return ImsConfig.OperationStatusConstants.SUCCESS;
    }

    /**
     * Gets the value of the specified IMS feature item for specified network type.
     * This operation gets the feature config value from the master storage (i.e. final
     * value). Asynchronous non-blocking call.
     *
     * @param feature as defined in com.android.ims.ImsConfig#FeatureConstants.
     * @param network as defined in android.telephony.TelephonyManager#NETWORK_TYPE_XXX.
     * @param listener feature value returned asynchronously through listener.
     */
    @Override
    public void getFeatureValue(int feature, int network, ImsConfigListener listener) {
        try {
            try {
                int value = mStorage.getFeatureValue(feature, network);
                Log.i(TAG, "getFeatureValue(" + feature + ", " + network + ") : " + value +
                        " on phone " + mPhoneId);
                listener.onGetFeatureResponse(
                        feature, network, value, ImsConfig.OperationStatusConstants.SUCCESS);
            } catch (ImsException e) {
                Log.e(TAG, "getFeatureValue(" + feature + ") failed, code: " + e.getCode());
                // Return OFF if failed
                listener.onGetFeatureResponse(
                        feature, network, ImsConfig.FeatureValueConstants.OFF,
                        ImsConfig.OperationStatusConstants.FAILED);
            }
        } catch (RemoteException e) {
            Log.e(TAG, "getFeatureValue(" + feature + ") remote failed!");
            throw new RuntimeException(e);
        }
    }

    /**
     * Sets the value for IMS feature item for specified network type.
     * This operation stores the user setting in setting db from which master db
     * is derived.
     *
     * @param feature as defined in com.android.ims.ImsConfig#FeatureConstants.
     * @param network as defined in android.telephony.TelephonyManager#NETWORK_TYPE_XXX.
     * @param value as defined in com.android.ims.ImsConfig#FeatureValueConstants.
     * @param listener, provided if caller needs to be notified for set result.
     */
    @Override
    public void setFeatureValue(int feature, int network, int value, ImsConfigListener listener) {
        try {
            try {
                Log.i(TAG, "setFeatureValue(" + feature + ", " + network + ", " + value +
                      ") on phone " + mPhoneId + " from pid " + Binder.getCallingPid() +
                      ", uid " + Binder.getCallingUid() + ", listener " + listener);
                mStorage.setFeatureValue(feature, network, value);

                // 93 after logic
                if (ImsCommonUtil.supportMdAutoSetupIms()) {
                    // check allow send AT cmd start
                    int oldFeatureValue = ImsConfig.FeatureValueConstants.OFF;
                    boolean isAllow = false;
                    String simState = ImsConfigProvider.LatestSimState.get(mPhoneId);

                    if (simState != null) {
                        // 1) sim loaded case
                        if (simState.equals(IccCardConstants.INTENT_VALUE_ICC_LOADED)) {
                            Log.d(TAG, "93Gen check if already send feature AT Cmd once: " +
                                    mStorage.FeatureSendArray[mPhoneId].getFeatureMap().get(feature));
                            if (mStorage.FeatureSendArray[mPhoneId].getFeatureMap().get(feature)) {
                                // compare if the feature value is same or not
                                switch(feature) {
                                    case ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE:
                                        oldFeatureValue = ImsConfigUtils.getFeaturePropValue(
                                                ImsConfigUtils.PROPERTY_VILTE_ENALBE, mPhoneId);
                                        break;
                                    case ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_WIFI:
                                        oldFeatureValue = ImsConfigUtils.getFeaturePropValue(
                                                ImsConfigUtils.PROPERTY_VIWIFI_ENALBE, mPhoneId);
                                        break;
                                    case ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI:
                                        oldFeatureValue = ImsConfigUtils.getFeaturePropValue(
                                                ImsConfigUtils.PROPERTY_WFC_ENALBE, mPhoneId);
                                        break;
                                    case ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE:
                                        oldFeatureValue = ImsConfigUtils.getFeaturePropValue(
                                                ImsConfigUtils.PROPERTY_VOLTE_ENALBE, mPhoneId);
                                        break;
                                    default:
                                        break;
                                }
                                Log.d(TAG, "93Gen comparing feature value old:" + oldFeatureValue +
                                        ", new:" + value);
                                if (value != oldFeatureValue) {
                                    isAllow = true;
                                } else {
                                    isAllow = false;
                                }
                            } else {
                                Log.d(TAG, "93Gen at least to send feature: " + feature +
                                        " once after sim loaded");
                                mStorage.FeatureSendArray[mPhoneId].getFeatureMap().put(feature, true);
                                isAllow = true;
                            }
                        }

                        // 2) sim absent case
                        if (simState.equals(IccCardConstants.INTENT_VALUE_ICC_ABSENT)) {
                            Log.d(TAG, "93Gen ECCAllow:" + ImsConfigProvider.ECCAllowSendCmd.get(mPhoneId));
                            if (ImsConfigProvider.ECCAllowSendCmd.get(mPhoneId) == null) {
                                isAllow = false;
                            } else {
                                isAllow = (value == ImsConfig.FeatureValueConstants.ON &&
                                        feature == ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE
                                        && ImsConfigProvider.ECCAllowSendCmd.get(mPhoneId) == true);
                                if (isAllow) {
                                    Log.d(TAG, "93Gen allow to send enable VoLTE AT cmd once for ECC");
                                    ImsConfigProvider.ECCAllowSendCmd.put(mPhoneId, false);
                                }
                            }
                        }
                    }
                    /// M: Sync volte setting value. @{
                    if (!isAllow) {
                        isAllow =
                          (feature == ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE ||
                          feature == ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE) &&
                          (SystemProperties.getInt(PROPERTY_IMSCONFIG_FORCE_NOTIFY, 0) == 1);
                    }
                    /// @}
                    Log.d(TAG, "93Gen if allow to send AT cmd, feature:" + feature +
                            ", value:" + value + ", simState:" + simState + ", isAllow:" + isAllow);
                    // check allow send AT cmd end

                    if (isAllow) {
                        switch(feature) {
                            case ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE:
                                if (value == ImsConfig.FeatureValueConstants.ON) {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_VILTE_ENALBE, "1", mPhoneId);
                                } else {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_VILTE_ENALBE, "0", mPhoneId);
                                }
                                break;
                            case ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_WIFI:
                                if (value == ImsConfig.FeatureValueConstants.ON) {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_VIWIFI_ENALBE, "1", mPhoneId);
                                } else {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_VIWIFI_ENALBE, "0", mPhoneId);
                                }
                                break;
                            case ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI:
                                if (value == ImsConfig.FeatureValueConstants.ON) {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_WFC_ENALBE, "1", mPhoneId);
                                } else {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_WFC_ENALBE, "0", mPhoneId);
                                }
                                break;
                            case ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE:
                                if (value == ImsConfig.FeatureValueConstants.ON) {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_VOLTE_ENALBE, "1", mPhoneId);
                                } else {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_VOLTE_ENALBE, "0", mPhoneId);
                                }
                                break;
                            default:
                                break;
                        }
                        ImsConfigUtils.triggerSendCfg(mContext, mRilAdapter, mPhoneId);
                    }
                } else {
                    // Ims ctrl old logic with WOS.
                    switch(feature) {
                        case ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE:
                        case ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_WIFI:
                            int oldVideoValue = ImsConfigUtils.getFeaturePropValue(
                                    ImsConfigUtils.PROPERTY_IMS_VIDEO_ENALBE, mPhoneId);
                            if (value != oldVideoValue) {
                                if (value == ImsConfig.FeatureValueConstants.ON) {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_IMS_VIDEO_ENALBE, "1", mPhoneId);
                                } else {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_IMS_VIDEO_ENALBE, "0", mPhoneId);
                                }
                            }
                            break;
                        case ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI:
                            int oldWfcValue = ImsConfigUtils.getFeaturePropValue(
                                    ImsConfigUtils.PROPERTY_WFC_ENALBE, mPhoneId);
                            int volteEnable = ImsConfigUtils.getFeaturePropValue(
                                    ImsConfigUtils.PROPERTY_VOLTE_ENALBE, mPhoneId);
                            if (value != oldWfcValue) {
                                if (value == ImsConfig.FeatureValueConstants.ON) {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_WFC_ENALBE, "1", mPhoneId);
                                } else {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_WFC_ENALBE, "0", mPhoneId);
                                }
                            }
                            break;
                        case ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE:
                            int oldVoLTEValue = ImsConfigUtils.getFeaturePropValue(
                                    ImsConfigUtils.PROPERTY_VOLTE_ENALBE, mPhoneId);
                            int wfcEnable = ImsConfigUtils.getFeaturePropValue(
                                    ImsConfigUtils.PROPERTY_WFC_ENALBE, mPhoneId);
                            if (value != oldVoLTEValue) {
                                if (value == ImsConfig.FeatureValueConstants.ON) {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_VOLTE_ENALBE, "1", mPhoneId);
                                } else {
                                    ImsConfigUtils.setFeaturePropValue(
                                            ImsConfigUtils.PROPERTY_VOLTE_ENALBE, "0", mPhoneId);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }

                if (listener != null) {
                    listener.onSetFeatureResponse(
                            feature, network, value, ImsConfig.OperationStatusConstants.SUCCESS);
                }
            } catch (ImsException e) {
                Log.e(TAG, "setFeatureValue(" + feature + ") failed, code: " + e.getCode());
                if (listener != null) {
                    // Return OFF if failed
                    listener.onSetFeatureResponse(
                            feature, network, ImsConfig.FeatureValueConstants.OFF,
                            ImsConfig.OperationStatusConstants.FAILED);
                }
            }
        } catch (RemoteException e) {
            Log.e(TAG, "setFeatureValue(" + feature + ") remote failed!");
            throw new RuntimeException(e);
        }
    }

    /**
     * Gets the value for IMS volte provisioned.
     * This should be the same as the operator provisioned value if applies.
     *
     * @return boolean
     */
    @Override
    public boolean getVolteProvisioned() {
        // Move caching of VoLTE provisioned value from ImsConfigImpl to ImsManager
        return true;
    }
}
