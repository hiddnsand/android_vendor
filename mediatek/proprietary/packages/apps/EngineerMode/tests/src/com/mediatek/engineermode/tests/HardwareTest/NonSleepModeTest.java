package com.mediatek.engineermode.tests;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.widget.EditText;

import com.robotium.solo.Solo;
import com.mediatek.engineermode.R;
import com.mediatek.engineermode.nonsleep.NonSleepMode;
import com.mediatek.engineermode.touchscreen.TsMultiTouch;

import android.provider.Settings;
import android.os.PowerManager;

import static android.provider.Settings.System.SCREEN_OFF_TIMEOUT;

public class NonSleepModeTest extends ActivityInstrumentationTestCase2<NonSleepMode> {
    private static final String TAG = "EMTest/engineermodeNonsleep";
    private Solo mSolo;
    private Context mContext;
    private Instrumentation mIns;
    private Activity mActivity;
    long systemTimeout = 0;
    long currentTimeout = 0;
    PowerManager pm = null;
    boolean isScreenOn;
    private static final String KEY_SCREEN_TIMEOUT = "screen_timeout";
    private static final int FALLBACK_SCREEN_TIMEOUT_VALUE = 30000;

    public NonSleepModeTest() {
        super("com.mediatek.engineermode", NonSleepMode.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mIns = getInstrumentation();
        mContext = mIns.getTargetContext();
        mActivity = getActivity();
        mSolo = new Solo(mIns, mActivity);
        pm = (PowerManager) mContext.getSystemService(mContext.POWER_SERVICE);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    private void verifyPreconditions() {
        assertTrue(mIns != null);
        assertTrue(mActivity != null);
        assertTrue(mContext != null);
        assertTrue(mSolo != null);
    }

    @BasicFuncAnnotation
    public void testCase01_VerifyPreconditions() {
        verifyPreconditions();
    }

    @UiAnnotation
    public void testCase02_CheckBtn() throws Exception {
        boolean textFound;

        mSolo.assertCurrentActivity("Not NonSleepMode Class", NonSleepMode.class);

        mSolo.sleep(EmOperate.TIME_MID);

        // enable non-sleep mode
        mSolo.clickOnButton(0);
        mSolo.sleep(EmOperate.TIME_MID);

        // release non-sleep mode
        mSolo.clickOnButton(0);
        mSolo.sleep(EmOperate.TIME_MID);
        /*
         * systemTimeout =
         * Settings.System.getLong(getActivity().getContentResolver(),
         * SCREEN_OFF_TIMEOUT, FALLBACK_SCREEN_TIMEOUT_VALUE);
         * Settings.System.putInt(getActivity().getContentResolver(),
         * SCREEN_OFF_TIMEOUT, 1000); // enable non-sleep mode
         * mSolo.clickOnButton("Enable Non-sleep mode");
         * mSolo.sleep(EmOperate.TIME_MID); textFound =
         * mSolo.waitForText("(?i).*?Disable Non-sleep mode");
         * assertTrue("Disable Non-sleep mode is not found", textFound);
         *
         * mSolo.sleep(EmOperate.TIME_SUPER_LONG);
         * mSolo.sleep(EmOperate.TIME_SUPER_LONG);
         *
         * isScreenOn = pm.isScreenOn(); Log.i("@M_" + TAG, "screen is: " +
         * isScreenOn); assertTrue("Enable Non-sleep mode is failed",
         * isScreenOn);
         *
         * // release non-sleep mode
         * mSolo.clickOnButton("Disable Non-sleep mode"); textFound =
         * mSolo.waitForText("(?i).*?Enable Non-sleep mode");
         * assertTrue("Enable Non-sleep mode is not found", textFound);
         *
         * Settings.System.putLong(getActivity().getContentResolver(),
         * SCREEN_OFF_TIMEOUT, systemTimeout);
         */
        mSolo.goBack();

    }

}
