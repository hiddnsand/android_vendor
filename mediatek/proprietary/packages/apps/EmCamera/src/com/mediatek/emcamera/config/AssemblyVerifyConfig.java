/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
package com.mediatek.emcamera.config;

import android.app.Activity;
import android.content.SharedPreferences.Editor;
import android.hardware.Camera.Parameters;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.mediatek.emcamera.AssemblyVerify;
import com.mediatek.emcamera.CameraJni;
import com.mediatek.emcamera.Elog;
import com.mediatek.emcamera.Util;
import com.mediatek.emcamera.R;

public class AssemblyVerifyConfig extends ConfigBase {
    protected static final String TAG = AssemblyVerifyConfig.class.getSimpleName();
    private static final String KEY_IMAGE_REFOCUS_SUPPORTED = "stereo-image-refocus-values";

    public static final String PREFERENCE_KEY[] = {"camera_inter_settings"
        , "camera_inter_settings2"};

    public static final int VERIFY_OFF_INDEX = 0;
    public static final int VERIFY_ON_INDEX = 1;

    public static final int LOCATION_NEAR = 0; //Near
    public static final int LOCATION_FAR = 1; //Far

    private Spinner mVerifySpnr;
    private RadioButton mOneLocationRb;
    private RadioButton mTwoLocationRb;
    private View mRadioGroup;
    private View mLocationLayout;
    private Spinner mLocationSpnr;

    private Listener mListener;

    private int mLocation;
    private boolean mNeedSyncLocation2;

    public interface Listener {
        void saveAllValues(String preferenceKey);
    }

    public AssemblyVerifyConfig(Activity activity) {
        super(activity);
        mLocation = LOCATION_NEAR;
        mNeedSyncLocation2 = false;
    }

    public void registerListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void initComponents() {
        mVerifySpnr = (Spinner) mActivity.findViewById(R.id.auto_clibr_verify_spnr);
        mVerifySpnr.setOnItemSelectedListener(mVerifyListener);

        mRadioGroup = mActivity.findViewById(R.id.auto_clibr_assembly_group);
        mOneLocationRb = (RadioButton) mActivity.findViewById(R.id.auto_clibr_assembly_one_radio);
        mTwoLocationRb = (RadioButton) mActivity.findViewById(R.id.auto_clibr_assembly_two_radio);
        mOneLocationRb.setOnCheckedChangeListener(mCheckedChangeListener);

        mLocationLayout = mActivity.findViewById(R.id.auto_clibr_location_settings_ll);
        mLocationSpnr = (Spinner) mActivity
                .findViewById(R.id.auto_clibr_location_settings_spnr);
        mLocationSpnr.setOnItemSelectedListener(mLocationListener);

        mTwoLocationRb.setChecked(true);
        mVerifySpnr.setSelection(VERIFY_OFF_INDEX);

    }

    private OnItemSelectedListener mVerifyListener = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            Elog.v(TAG, "[VerifySpnr] Turn on/off verify " + arg2);
            if(arg2 == VERIFY_OFF_INDEX) {
                mRadioGroup.setVisibility(View.GONE);
                mLocationLayout.setVisibility(View.GONE);
            } else {
                mRadioGroup.setVisibility(View.VISIBLE);
                mLocationLayout.setVisibility(View.VISIBLE);
                if(mTwoLocationRb.isChecked()) {
                    mNeedSyncLocation2 = true;
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {

        }
    };

    private OnCheckedChangeListener mCheckedChangeListener = new OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(CompoundButton button, boolean isChecked) {
            if (isChecked) {
                String title = mActivity.getString(R.string.auto_clibr_assembly_verify);
                String msg = mActivity.getString(R.string.auto_clibr_assembly_verify_warning_msg);
                Util.showMsgDialog(mActivity, title, msg);
                mNeedSyncLocation2 = false;
            } else {
                mNeedSyncLocation2 = true;
            }
        }

    };

    private OnItemSelectedListener mLocationListener = new OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            if(mTwoLocationRb.isChecked() && mLocation != arg2 ) {
                Elog.v(TAG, "[LocationSettingSpnr]Save configurations for " + mLocation);
                mListener.saveAllValues(PREFERENCE_KEY[mLocation]);
                mNeedSyncLocation2 = false;
                mLocation = arg2;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {

        }
    };

    @Override
    public void initSupportedUi(Parameters parameters) {
        if(!CameraJni.isFeatureSupported(CameraJni.MTK_CAM_DUAL_ZOOM_SUPPORT)) {
            (mActivity.findViewById(R.id.auto_clibr_assembly_ll)).setVisibility(View.GONE);
        }
    }

    @Override
    public void setStatusToDefault() {
        //There is no settings about camera parameters, no need to configure this.
    }

    @Override
    public boolean saveValues(Editor editor) {
        int verify = mVerifySpnr.getSelectedItemPosition();
        editor.putInt(mActivity.getString(R.string.auto_clibr_assembly_verify), verify);
        if(verify == VERIFY_ON_INDEX) {
            if (mOneLocationRb.isChecked()) {
                editor.putInt(mActivity.getString(R.string.auto_clibr_location_num), 1);
                editor.putInt(mActivity.getString(R.string.auto_clibr_location_type),
                        mLocationSpnr.getSelectedItemPosition());
            } else {
                editor.putInt(mActivity.getString(R.string.auto_clibr_location_num), 2);
                editor.putInt(mActivity.getString(R.string.auto_clibr_location_type),
                        AssemblyVerify.TYPE_NEAR_AND_FAR);
            }
        }
        return true;
    }

    public String getPreferenceKey() {
        if(mTwoLocationRb.isChecked()) {
            return PREFERENCE_KEY[mLocation];
        } else {
            //It's default preferences
            return PREFERENCE_KEY[LOCATION_NEAR];
        }
    }

    public String getFarPreferenceKey() {
        return PREFERENCE_KEY[LOCATION_FAR];
    }

    public boolean getNeedSyncSettingsToFar() {
        return mNeedSyncLocation2;
    }
}
