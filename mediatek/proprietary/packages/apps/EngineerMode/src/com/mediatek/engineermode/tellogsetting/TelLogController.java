package com.mediatek.engineermode.tellogsetting;

import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.ShellExe;
import com.mediatek.internal.telephony.IMtkTelephonyEx;

import java.io.IOException;

/**
 * Class to control telephony log.
 *
 */
public class TelLogController {

    private static final String TAG = "EM/telLogSetting";

    private static final String TEL_LOG_PROP = "persist.log.tag.tel_dbg";
    private static final String LOG_MUCH_PROP = "persist.logmuch.detect";

    private static final String TEL_LOG_ENABLE = "1";
    private static final String TEL_LOG_DISABLE = "0";
    private static final String LOG_MUCH_ENABLE = "true";
    private static final String LOG_MUCH_DISABLE = "false";

    private IMtkTelephonyEx mTelEx  = null;

    /**
     * Constructor function.
     */
    public TelLogController() {
        mTelEx = IMtkTelephonyEx.Stub.asInterface(
                ServiceManager.getService("phoneEx"));

    }

    boolean getTelLogStatus() {
        String value = SystemProperties.get(TEL_LOG_PROP);
        Elog.d(TAG, "TEL_LOG_PROP: " + value);
        return (TEL_LOG_ENABLE.equals(value));
    }

    boolean switchTelLog(boolean enable) {
        Log.d(TAG, "switchTelLog " + enable);
        String telLogProTar = enable ? TEL_LOG_ENABLE : TEL_LOG_DISABLE;
        String logMuchProTar = enable ? LOG_MUCH_DISABLE : LOG_MUCH_ENABLE;
        SystemProperties.set(TEL_LOG_PROP, telLogProTar);
        SystemProperties.set(LOG_MUCH_PROP, logMuchProTar);

        if ((!telLogProTar.equals(SystemProperties.get(TEL_LOG_PROP)))
                || (!logMuchProTar.equals(SystemProperties.get(LOG_MUCH_PROP)))) {
            return false;
        }

        if (mTelEx != null) {
            try {
                mTelEx.setTelLog(enable);
                Log.d(TAG, "telEx.setTelLog:" + enable);
            } catch (RemoteException e) {
                Log.e(TAG, "ITelephonyEx RemoteException");
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

}