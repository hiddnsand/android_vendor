LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

# We only want this apk build for tests.
LOCAL_MODULE_TAGS := grouptests

LOCAL_JAVA_LIBRARIES := android.test.runner
LOCAL_JAVA_LIBRARIES += ims-common
LOCAL_APK_LIBRARIES += MtkMms


# Include all test java files.
LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_STATIC_JAVA_LIBRARIES := librobotium4

LOCAL_PACKAGE_NAME := GroupTests

LOCAL_INSTRUMENTATION_FOR := Rcse

include $(BUILD_PACKAGE)
