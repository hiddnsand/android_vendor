/*
 * Copyright (C) 2013-2016, Shenzhen Huiding Technology Co., Ltd.
 * All Rights Reserved.
 */

#ifndef GOODIX_FINGERPRINT_DAEMON_CALLBACK_PROXY_H_
#define GOODIX_FINGERPRINT_DAEMON_CALLBACK_PROXY_H_

#include "gf_fingerprint.h"
#include "IGoodixFingerprintDaemonCallback.h"

namespace android {

class GoodixFingerprintDaemonCallbackProxy: public BnGoodixFingerprintDaemonCallback {
public:
    static void setDevice(gf_fingerprint_device_t *device) {
        mDevice = device;
    }

    virtual status_t onEnrollResult(int64_t devId, int32_t fpId, int32_t gpId, int32_t rem);
    virtual status_t onAcquired(int64_t devId, int32_t acquiredInfo);
    virtual status_t onAuthenticated(int64_t devId, int32_t fingerId, int32_t groupId, const uint8_t* hat, int32_t hatSize);
    virtual status_t onError(int64_t devId, int32_t error);
    virtual status_t onRemoved(int64_t devId, int32_t fingerId, int32_t groupId);
    virtual status_t onEnumerate(int64_t devId, int32_t fpId, int32_t gpId, int32_t rem);
    virtual status_t onTestCmd(int64_t devId, int32_t cmdId, const int8_t *result, int32_t resultLen);
    virtual status_t onHbdData(int64_t devId, int32_t heartBeatRate, int32_t status, const int32_t* displayData, int32_t resultLen, const int32_t* rawData, int32_t rawDataLen);
    virtual status_t onDump(int64_t devId, int32_t cmdId, const int8_t *data, int32_t dataLen);
    virtual status_t onAuthenticatedFido(int64_t devId, int32_t fpId, const uint8_t *uvtData, int32_t uvtDataLen);
    GoodixFingerprintDaemonCallbackProxy();
    virtual ~GoodixFingerprintDaemonCallbackProxy();
private:
    static gf_fingerprint_device_t *mDevice;
};

}

#endif // GOODIX_FINGERPRINT_DAEMON_CALLBACK_PROXY_H_
