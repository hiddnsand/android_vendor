#if 1				/* def JPEG_THREAD_SUPPORT */
#ifdef USE_WIN32_THREAD_MTK
#define _WIN32_WINNT 0x500
#include <windows.h>
#endif				/* USE_WIN32_THREAD */

#define MCU_ROWS_IN_PER_THREAD_MTK	1

enum {
	RUNNING_MTK,
	RUNNING_IDLE_MTK,
	STOP_DONE_MTK,
	STOP_DIE_MTK
};

#ifdef USE_WIN32_THREAD_MTK
typedef struct {
	void *handle;
	void *(*func) (void *arg);
	void *arg;
	void *ret;
} Thread_t_MTK;
#define Thread_attr_t_MTK int

/* the conditional variable api for windows 6.0+ uses critical sections and not mutexes */
typedef CRITICAL_SECTION Thread_mutex_t_MTK;
#define JPEGTHREAD_MUTEX_INITIALIZER_MTK {0}
#define Thread_mutexattr_t_MTK int

/* This is the CONDITIONAL_VARIABLE typedef for using Window's native conditional variables on kernels 6.0+.
 * MinGW does not currently have this typedef. */
typedef struct {
	void *ptr;
} Thread_cond_t_MTK;
#define Thread_condattr_t_MTK int
#else				/* USE_WIN32_THREAD */
/****** Pthread define******/
typedef long Thread_t_MTK;
typedef struct {
	unsigned int flags;
	void *stack_base;
	unsigned int stack_size;
	unsigned int guard_size;
	unsigned int sched_policy;
	unsigned int sched_priority;
} Thread_attr_t_MTK;

typedef struct {
	int volatile value;
} Thread_mutex_t_MTK;
typedef long Thread_mutexattr_t_MTK;

typedef struct {
	int volatile value;
} Thread_cond_t_MTK;
typedef long Thread_condattr_t_MTK;
/****** End of Pthread define******/
#endif				/* USE_WIN32_THREAD */

typedef struct {
	Thread_t_MTK *self;
	Thread_mutex_t_MTK *mutex[2];
	Thread_cond_t_MTK *cond[2];
	void *parent;
	int index;
	int state;
	/* int rows_num; */
	/* u32 loading; */
	/* u32 retval; */
} ThreadContext_t_MTK;

int JPEGThreading_init_MTK(void);
void JPEGThreading_destroy_MTK(void);
int JPEGThread_create_MTK(Thread_t_MTK *thread, const Thread_attr_t_MTK *attr,
		      void *(*start_routine) (void *), void *arg);
int JPEGThread_join_MTK(Thread_t_MTK thread, void **value_ptr);
int JPEGThread_mutex_init_MTK(Thread_mutex_t_MTK *mutex, const Thread_mutexattr_t_MTK *attr);
int JPEGThread_mutex_destroy_MTK(Thread_mutex_t_MTK *mutex);
int JPEGThread_mutex_lock_MTK(Thread_mutex_t_MTK *mutex);
int JPEGThread_mutex_unlock_MTK(Thread_mutex_t_MTK *mutex);
int JPEGThread_cond_init_MTK(Thread_cond_t_MTK *cond, const Thread_condattr_t_MTK *attr);
int JPEGThread_cond_destroy_MTK(Thread_cond_t_MTK *cond);
int JPEGThread_cond_broadcast_MTK(Thread_cond_t_MTK *cond);
int JPEGThread_cond_wait_MTK(Thread_cond_t_MTK *cond, Thread_mutex_t_MTK *mutex);
int JPEGThread_cond_signal_MTK(Thread_cond_t_MTK *cond);

#ifdef USE_WIN32_THREAD_MTK
#define JPEGThread_attr_init_MTK(a) 0
#define JPEGThread_attr_destroy_MTK(a) 0
#else				/* USE_WIN32_THREAD */
int JPEGthread_attr_init_MTK(Thread_attr_t_MTK *attr);
int JPEGthread_attr_destroy_MTK(Thread_attr_t_MTK *attr);
int JPEGBindingCore_MTK(Thread_t_MTK ThreadHandle, int CPUid);
#endif				/* USE_WIN32_THREAD */

#endif				/* JPEG_THREAD_SUPPORT */
