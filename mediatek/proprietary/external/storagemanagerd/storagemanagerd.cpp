/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <poll.h>

#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>

#include <android-base/logging.h>
#include <android-base/stringprintf.h>

#include <cutils/sockets.h>
#include <private/android_filesystem_config.h>

static void usage(char *progname);
static int do_monitor(int sock);
static int do_cmd(int sock, int argc, char **argv);

static constexpr int kCommandTimeoutMs = 20 * 1000;


#include <thread>

enum class Type {
    kPublic = 0,
    kPrivate,
    kEmulated,
    kAsec,
    kObb,
};

static const int VolumeCreated = 650;
static const int MOUNT_FLAG_PRIMARY = 1 << 0;
static const int MOUNT_FLAG_VISIBLE = 1 << 1;
int g_seq = 0;
static bool g_volome_mount_all = false;

static int onEvent(int sock, char* msg);
static int do_cmd_only(int sock, char * cmd_str);

int main(int argc, char **argv) {
    int sock;
    int wait_for_socket;
    char *progname;
    std::thread mThread;

    progname = argv[0];
    android::base::InitLogging(argv, android::base::LogdLogger(android::base::SYSTEM));

    wait_for_socket = argc > 1 && strcmp(argv[1], "--wait") == 0;
    if (wait_for_socket) {
        argv++;
        argc--;
    }

    if (argc < 2) {
        usage(progname);
        exit(5);
    }

    const char* sockname = "vold";
    if (!strcmp(argv[1], "cryptfs")) {
        sockname = "cryptd";
    }

    while ((sock = socket_local_client(sockname,
                                 ANDROID_SOCKET_NAMESPACE_RESERVED,
                                 SOCK_STREAM)) < 0) {
        if (!wait_for_socket) {
            PLOG(ERROR) << "Error connecting to " << sockname;
            exit(4);
        } else {
            usleep(10000);
        }
    }

    if (!strcmp(argv[1], "monitor")) {
        exit(do_monitor(sock));
    } else {
        if(argc >=3 && !strcmp(argv[1], "volume") && !strcmp(argv[2], "mount_all")) {
            char vold_cmd[256];
            snprintf(vold_cmd, sizeof(vold_cmd), "volume reset");
            do_cmd_only(sock, vold_cmd);
            g_volome_mount_all = true;
        }
        else {
          do_cmd(sock, argc, argv);
        }
        mThread= std::thread(&do_monitor, sock);
        mThread.join();
    }
}

static int do_cmd(int sock, int argc, char **argv) {
    int seq = getpid()+ g_seq;
    g_seq++;;

    std::string cmd(android::base::StringPrintf("%d ", seq));
    for (int i = 1; i < argc; i++) {
        if (!strchr(argv[i], ' ')) {
            cmd.append(argv[i]);
        } else {
            cmd.push_back('\"');
            cmd.append(argv[i]);
            cmd.push_back('\"');
        }

        if (i < argc - 1) {
            cmd.push_back(' ');
        }
    }

    fprintf(stdout, "[%s]: %s\n", __FUNCTION__, cmd.c_str());
    LOG(INFO) << __FUNCTION__ << ": " << cmd;

    if (TEMP_FAILURE_RETRY(write(sock, cmd.c_str(), cmd.length() + 1)) < 0) {
        PLOG(ERROR) << "Failed to write command";
        return errno;
    }

    return 0;
}

static int do_cmd_only(int sock, char * cmd_str) {
    int seq = getpid()+ g_seq;
    g_seq++;

    std::string cmd(android::base::StringPrintf("%d ", seq));
    cmd.append(cmd_str);
    fprintf(stdout, "[%s]: %s\n", __FUNCTION__, cmd.c_str());
    LOG(INFO) << __FUNCTION__ << ": " << cmd;
    if (TEMP_FAILURE_RETRY(write(sock, cmd.c_str(), cmd.length() + 1)) < 0) {
        PLOG(ERROR) << "Failed to write command";
        return errno;
    }
    return 0;
}

static int do_monitor(int sock) {
    char buffer[4096];
    int timeout = kCommandTimeoutMs;

    while (1) {
        struct pollfd poll_sock = { sock, POLLIN, 0 };
        int rc = TEMP_FAILURE_RETRY(poll(&poll_sock, 1, timeout));
        if (rc == 0) {
            LOG(ERROR) << "Timeout waiting for " << (g_seq-1);
            return ETIMEDOUT;
        } else if (rc < 0) {
            PLOG(ERROR) << "Failed during poll";
            return errno;
        }

        if (!(poll_sock.revents & POLLIN)) {
            LOG(INFO) << "No data; trying again";
            continue;
        }

        memset(buffer, 0, sizeof(buffer));
        rc = TEMP_FAILURE_RETRY(read(sock, buffer, sizeof(buffer)));
        if (rc == 0) {
            LOG(ERROR) << "Lost connection, did vold crash?";
            return ECONNRESET;
        } else if (rc < 0) {
            PLOG(ERROR) << "Error reading data";
            return errno;
        }

        int offset = 0;
        for (int i = 0; i < rc; i++) {
            if (buffer[i] == '\0') {
                char* res = buffer + offset;
                onEvent(sock, res);
                offset = i + 1;
            }
        }
    }
    return EIO;
}

static void usage(char *progname) {
    LOG(INFO) << "Usage: " << progname << " [--wait] <monitor>|<cmd> [arg1] [arg2...]";
}

static int onEvent(int sock, char* msg){
    char vold_cmd[256];
    fprintf(stdout, "%s\n", msg);
    LOG(INFO) << __FUNCTION__ << ": " << msg;

    int code = atoi(strtok(msg, " "));
    if (code == VolumeCreated) {
        int vol_mountFlags = MOUNT_FLAG_VISIBLE;
        int vol_mountUserId = -1;

        char* vol_id = strtok(nullptr, " ");
        Type vol_type = (Type)atoi(strtok(nullptr, " "));
        if (vol_type == Type::kEmulated) {
            vol_mountFlags += MOUNT_FLAG_PRIMARY;
        }
        else if (vol_type == Type::kPublic) {
            vol_mountUserId = 0;
        }

        snprintf(vold_cmd, sizeof(vold_cmd), "volume mount %s %d %d", vol_id, vol_mountFlags, vol_mountUserId);
        do_cmd_only(sock, vold_cmd);

        if(g_volome_mount_all && (vol_mountFlags&MOUNT_FLAG_PRIMARY)) {
           g_volome_mount_all = false;
           snprintf(vold_cmd, sizeof(vold_cmd), "volume user_started 0");
           do_cmd_only(sock, vold_cmd);
        }
    }
    return 0;
}

