/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <system/camera_metadata.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
//
typedef NSCam::IVendorTagDescriptor::VendorTag          VendorTag_t;
typedef NSCam::IVendorTagDescriptor::VendorTagSection   VendorTagSection_t;

/******************************************************************************
 *
 ******************************************************************************/
#define _TAG_(_tagId_, _tagName_, _tagType_) \
    { \
        static_cast<uint32_t>(_tagId_), \
        { \
            .tagName = _tagName_, \
            .tagId   = static_cast<uint32_t>(_tagId_), \
            .tagType = static_cast<uint32_t>(_tagType_), \
        }, \
    }


#define _SECTION_(_sectionName_, _tagStart_, _tagEnd_, _tags_) \
    { \
        .sectionName= _sectionName_, \
        .tags       = _tags_, \
        .tagStart   = static_cast<uint32_t>(_tagStart_), \
        .tagEnd     = static_cast<uint32_t>(_tagEnd_), \
    }


/******************************************************************************
 *
 ******************************************************************************/
static auto& _FaceFeature_()
{
    static const std::map<uint32_t, VendorTag_t>
    sInst = {
        _TAG_(MTK_FACE_FEATURE_GESTURE_MODE,
            "gesturemode",   TYPE_INT32),
        _TAG_(MTK_FACE_FEATURE_GESTURE_RESULT,
            "gestureresult",   TYPE_INT32),
        _TAG_(MTK_FACE_FEATURE_SMILE_DETECT_MODE,
            "smiledetectmode",   TYPE_INT32),
        _TAG_(MTK_FACE_FEATURE_SMILE_DETECT_RESULT,
            "smiledetectresult",   TYPE_INT32),
        _TAG_(MTK_FACE_FEATURE_ASD_MODE,
            "asdmode",   TYPE_INT32),
        _TAG_(MTK_FACE_FEATURE_ASD_RESULT,
            "asdresult",   TYPE_INT32),
        _TAG_(MTK_FACE_FEATURE_AVAILABLE_GESTURE_MODES,
            "availablegesturemodes",   TYPE_INT32),
        _TAG_(MTK_FACE_FEATURE_AVAILABLE_SMILE_DETECT_MODES,
            "availablesmiledetectmodes",   TYPE_INT32),
        _TAG_(MTK_FACE_FEATURE_AVAILABLE_ASD_MODES,
            "availableasdmodes",   TYPE_INT32),
        _TAG_(MTK_FACE_FEATURE_FORCE_FACE_3A,
            "forceface3a",   TYPE_INT32),
        _TAG_(MTK_FACE_FEATURE_AVAILABLE_FORCE_FACE_3A,
            "availableforceface3a",   TYPE_INT32),
    };
    //
    return sInst;
}

static auto& _NRFeature_()
{
    static const std::map<uint32_t, VendorTag_t>
    sInst = {
        _TAG_(MTK_NR_FEATURE_3DNR_MODE,
            "3dnrmode",   TYPE_INT32),
        _TAG_(MTK_NR_FEATURE_AVAILABLE_3DNR_MODES,
            "available3dnrmodes",   TYPE_INT32),
    };
    //
    return sInst;
}

static auto& _StereoFeature_()
{
    static const std::map<uint32_t, VendorTag_t>
    sInst = {
        _TAG_(MTK_STEREO_FEATURE_DOF_LEVEL,
            "doflevel",    TYPE_INT32),
        _TAG_(MTK_STEREO_FEATURE_SUPPORTED_DOF_LEVEL,
            "supporteddoflevel", TYPE_INT32),
        _TAG_(MTK_STEREO_FEATURE_RESULT_DOF_LEVEL,
            "resultdoflevel", TYPE_INT32),
        _TAG_(MTK_STEREO_FEATURE_WARNING,
            "stereowarning", TYPE_INT32),
        _TAG_(MTK_STEREO_FEATURE_DEPTH_AF_ON,
            "depthafon", TYPE_INT32),
        _TAG_(MTK_STEREO_FEATURE_DISTANCE_MEASURE_ON,
            "distancemeasureon", TYPE_INT32),
        _TAG_(MTK_STEREO_FEATURE_RESULT_DISTANCE,
            "resultdistance", TYPE_FLOAT),
        _TAG_(MTK_STEREO_FEATURE_TOUCH_POSITION,
            "touchposition", TYPE_INT32),
        _TAG_(MTK_STEREO_FEATURE_SHOT_MODE,
            "shotmode", TYPE_BYTE),
        _TAG_(MTK_STEREO_FEATURE_STATE,
            "stereostatus", TYPE_BYTE),
    };
    //
    return sInst;
}

static auto& _HDRFeature_()
{
    static const std::map<uint32_t, VendorTag_t>
    sInst = {
        _TAG_(MTK_HDR_FEATURE_HDR_MODE,
            "hdrMode",   TYPE_INT32),
        _TAG_(MTK_HDR_FEATURE_HDR_DETECTION_RESULT,
            "hdrDetectionResult",   TYPE_INT32),
        _TAG_(MTK_HDR_FEATURE_AVAILABLE_HDR_MODES_PHOTO,
            "availableHdrModesPhoto",   TYPE_INT32),
        _TAG_(MTK_HDR_FEATURE_AVAILABLE_HDR_MODES_VIDEO,
            "availableHdrModesVideo",   TYPE_INT32),
        _TAG_(MTK_HDR_FEATURE_AVAILABLE_VHDR_MODES,
            "availableVhdrModes",   TYPE_INT32),
        _TAG_(MTK_HDR_FEATURE_AVAILABLE_SINGLE_FRAME_HDR,
            "availableSingleFrameHdr",   TYPE_BYTE),
    };
    //
    return sInst;
}

static auto& _EISFeature_()
{
    static const std::map<uint32_t, VendorTag_t>
    sInst = {
        _TAG_(MTK_EIS_FEATURE_ISNEED_OVERRIDE_TIMESTAMP,
            "isNeedOverrideTimestamp",   TYPE_BYTE),
        _TAG_(MTK_EIS_FEATURE_NEW_SHUTTER_TIMESTAMP,
            "newShutterTimestamp",   TYPE_INT64),
    };
    //
    return sInst;
}

static auto& _StreamingFeature_()
{
    static const std::map<uint32_t, VendorTag_t>
    sInst = {
        _TAG_(MTK_STREAMING_FEATURE_RECORD_STATE,
            "recordState",   TYPE_INT32),
        _TAG_(MTK_STREAMING_FEATURE_AVAILABLE_RECORD_STATES,
            "availableRecordStates",   TYPE_INT32),
    };
    //
    return sInst;
}

static auto& _MFNRFeature_()
{
    static const std::map<uint32_t, VendorTag_t>
    sInst = {
        _TAG_(MTK_MFNR_FEATURE_MFB_MODE,
            "mfbmode",   TYPE_INT32),
        _TAG_(MTK_MFNR_FEATURE_MFB_RESULT,
            "mfbresult",   TYPE_INT32),
        _TAG_(MTK_MFNR_FEATURE_AVAILABLE_MFB_MODES,
            "availablemfbmodes",   TYPE_INT32),
        _TAG_(MTK_MFNR_FEATURE_AIS_MODE,
            "aismode",   TYPE_INT32),
        _TAG_(MTK_MFNR_FEATURE_AIS_RESULT,
            "aisresult",   TYPE_INT32),
        _TAG_(MTK_MFNR_FEATURE_AVAILABLE_AIS_MODES,
            "availableaismodes",   TYPE_INT32),
    };
    //
    return sInst;
}

static auto& _3AFeature_()
{
    static const std::map<uint32_t, VendorTag_t>
    sInst = {
        _TAG_(MTK_3A_FEATURE_AE_ROI,
            "aeroi",    TYPE_INT32),
        _TAG_(MTK_3A_FEATURE_AF_ROI,
            "afroi",    TYPE_INT32),
        _TAG_(MTK_3A_FEATURE_AWB_ROI,
            "awbroi",   TYPE_INT32),
    };
    //
    return sInst;
}


/******************************************************************************
 *
 ******************************************************************************/
static auto& getGlobalSections()
{
    static const std::vector<VendorTagSection_t>
    sMTKSections = {
        _SECTION_( "com.mediatek.facefeature",
                    MTK_FACE_FEATURE_START,
                    MTK_FACE_FEATURE_END,
                    _FaceFeature_() ),

        _SECTION_( "com.mediatek.nrfeature",
                    MTK_NR_FEATURE_START,
                    MTK_NR_FEATURE_END,
                    _NRFeature_() ),

        _SECTION_( "com.mediatek.stereofeature",
                    MTK_STEREO_FEATURE_START,
                    MTK_STEREO_FEATURE_END,
                    _StereoFeature_() ),

        _SECTION_( "com.mediatek.hdrfeature",
                    MTK_HDR_FEATURE_START,
                    MTK_HDR_FEATURE_END,
                    _HDRFeature_() ),

        _SECTION_( "com.mediatek.eisfeature",
                    MTK_EIS_FEATURE_START,
                    MTK_EIS_FEATURE_END,
                    _EISFeature_() ),

        _SECTION_( "com.mediatek.streamingfeature",
                    MTK_STREAMING_FEATURE_START,
                    MTK_STREAMING_FEATURE_END,
                    _StreamingFeature_() ),

        _SECTION_( "com.mediatek.mfnrfeature",
                    MTK_MFNR_FEATURE_START,
                    MTK_MFNR_FEATURE_END,
                    _MFNRFeature_() ),

        _SECTION_( "com.mediatek.3afeature",
                    MTK_3A_FEATURE_START,
                    MTK_3A_FEATURE_END,
                    _3AFeature_() ),

    };
    //
    return sMTKSections;
}
