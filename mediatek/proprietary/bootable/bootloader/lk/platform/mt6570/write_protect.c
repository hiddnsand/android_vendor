/* Copyright Statement:
*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein
* is confidential and proprietary to MediaTek Inc. and/or its licensors.
* Without the prior written permission of MediaTek inc. and/or its licensors,
* any reproduction, modification, use or disclosure of MediaTek Software,
* and information contained herein, in whole or in part, shall be strictly prohibited.
*/
/* MediaTek Inc. (C) 2015. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
* AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
* NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
* SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
* SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
* THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
* THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
* CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
* SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
* CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
* AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
* OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
* MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*/
#include <platform/partition.h>
#include <platform/partition_wp.h>
#include <printf.h>
#include <platform/boot_mode.h>
#include <platform/mtk_wdt.h>
#include <platform/env.h>


#ifdef MTK_SIM_LOCK_POWER_ON_WRITE_PROTECT
extern int is_protect2_ready_for_wp(void);
extern int sync_sml_data(void);
#endif

int set_write_protect()
{
	int err = 0;
	if (g_boot_mode == NORMAL_BOOT) {
		/*Group 1*/
		dprintf(INFO, "[%s]: Lock boot region \n", __func__);
		err = partition_write_prot_set("preloader", "preloader", WP_POWER_ON);

		if (err != 0) {
			dprintf(CRITICAL, "[%s]: Lock boot region failed: %d\n", __func__,err);
			return err;
		}
		/*Group 2*/
#ifdef MTK_SIM_LOCK_POWER_ON_WRITE_PROTECT
		/* sync protect1 sml data to protect2 if needed */
		int sync_ret = 0;

		mtk_wdt_restart();

		sync_ret = sync_sml_data();
		if (0 != sync_ret) {
			dprintf(INFO,"sml data not sync \n");
		} else {
			dprintf(INFO,"sml data sync \n");
		}


		if (g_boot_mode == NORMAL_BOOT) {
			if (0 == is_protect2_ready_for_wp()) {
				dprintf(CRITICAL,"[%s]: protect2 is fmt \n", __func__);
				dprintf(CRITICAL, "[%s]: Lock protect2 \n", __func__);
				err = partition_write_prot_set("protect2", "protect2" , WP_POWER_ON);
				if (err != 0) {
					dprintf(CRITICAL, "[%s]: Lock protect region failed:%d\n", __func__,err);
					return err;
				}
			}
		}
#endif

		/*Group 4*/
		#ifdef MTK_SECURITY_SW_SUPPORT
		if (TRUE == seclib_sec_boot_enabled(TRUE)) {
			dprintf(INFO, "[%s]: Lock oemkeystore->system \n", __func__);
			err = partition_write_prot_set("oemkeystore", "system", WP_POWER_ON);
			if (err != 0) {
				dprintf(CRITICAL, "[%s]: Lock oemkeystore->system failed:%d\n", __func__,err);
				return err;
			}
		}
		else
		#endif
		{
			dprintf(INFO, "[%s]: Lock oemkeystore->keystore \n", __func__);
			err = partition_write_prot_set("oemkeystore", "keystore", WP_POWER_ON);
			if (err != 0) {
				dprintf(CRITICAL, "[%s]: Lock oemkeystore->keystore failed:%d\n", __func__,err);
				return err;
			}
		}
	}

	/*Group 3*/
	dprintf(INFO, "[%s]: Lock seccfg \n", __func__);
	err = partition_write_prot_set("seccfg", "seccfg", WP_POWER_ON);
	if (err != 0) {
		dprintf(CRITICAL, "[%s]: Lock seccfg  failed:%d\n", __func__,err);
		return err;
	}



	return 0;
}

void write_protect_flow()
{
	int bypass_wp = 0;
	int ret = 0;

#ifndef USER_BUILD
	bypass_wp= atoi(get_env("bypass_wp"));
	dprintf(ALWAYS, "bypass write protect flag = %d! \n",bypass_wp);
#endif

	if (!bypass_wp) {
		ret = set_write_protect();
		if (ret != 0)
			dprintf(CRITICAL, "write protect fail! \n");
		else
			dprintf(ALWAYS, "write protect Done! \n");
	} else
		dprintf(ALWAYS, "Bypass write protect! \n");
}

