/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**
 * @file BayerMonoQTemplateProvider.cpp
 * @brief QParams template creator for stereo feature on different platforms
 */

// Standard C header file

// Android system/core header file

// mtkcam custom header file
#include <camera_custom_stereo.h>
// mtkcam global header file
#include <mtkcam/def/common.h>
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>
#include <mtkcam/drv/iopipe/PostProc/IHalPostProcPipe.h>
#include <mtkcam/feature/stereo/hal/stereo_setting_provider.h>
// Module header file
#include <stereo_tuning_provider.h>
// Local header file
#include "BayerMonoQTemplateProvider.h"
//
#undef PIPE_CLASS_TAG
#define PIPE_MODULE_TAG "DepthMapPipe"
#define PIPE_CLASS_TAG "BayerMonoQTemplateProvider"
#include <featurePipe/core/include/PipeLog.h>


/*******************************************************************************
* Namespace start.
********************************************************************************/
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe_DepthMap {

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Instantiation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
BayerMonoQTemplateProvider::
BayerMonoQTemplateProvider(
    sp<DepthMapPipeSetting> pSetting,
    sp<DepthMapPipeOption> pPipeOption,
    DepthMapFlowOption* pFlowOption
)
: BayerBayerQTemplateProvider(pSetting, pPipeOption, pFlowOption)
{
}

BayerMonoQTemplateProvider::
~BayerMonoQTemplateProvider()
{
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  BayerMonoQTemplateProvider Public Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

MBOOL
BayerMonoQTemplateProvider::
init(BaseBufferSizeMgr* pSizeMgr)
{
    VSDOF_LOGD("+");
    mpSizeMgr = pSizeMgr;
    MBOOL bRet = BayerBayerQTemplateProvider::init(pSizeMgr);
    bRet &= prepareTemplateParams_Bayer();
    VSDOF_LOGD("-");
    return bRet;
}


MBOOL
BayerMonoQTemplateProvider::
buildQParams_CAPTURE(
    DepthMapRequestPtr& rEffReqPtr,
    const Stereo3ATuningRes& tuningResult,
    QParams& rOutParm
)
{
    CAM_TRACE_NAME("P2AFMNode::buildQParams_CAPTURE");
    VSDOF_PRFLOG("+, reqID=%d", rEffReqPtr->getRequestNo());
    // copy template
    rOutParm = mQParam_CAPTURE;
    // filler
    QParamTemplateFiller qParamFiller(rOutParm);

    MBOOL bRet = buildQParam_frame0_capture(rEffReqPtr, tuningResult, qParamFiller);
    bRet &= buildQParam_frame1_capture(rEffReqPtr, tuningResult, qParamFiller);
    bRet &= buildQParam_frame2_capture(rEffReqPtr, tuningResult, qParamFiller);
    // denoise special case
    if(mpPipeOption->mFeatureMode == eDEPTHNODE_MODE_DENOISE )
        bRet &= buildQParam_frame3_denoise_capture(rEffReqPtr, tuningResult, qParamFiller);
    else
        bRet &= buildQParam_frame3_capture(rEffReqPtr, tuningResult, qParamFiller);
    bRet &= buildQParam_frame4(rEffReqPtr, tuningResult, qParamFiller);
    bRet &= buildQParam_frame5(rEffReqPtr, tuningResult, qParamFiller);
    bRet &= buildQParam_frame6to9(rEffReqPtr, tuningResult, qParamFiller);

    if(!bRet)
        return MFALSE;

    bRet = qParamFiller.validate();
    VSDOF_PRFLOG("-, reqID=%d", rEffReqPtr->getRequestNo());
    return bRet;
}


MBOOL
BayerMonoQTemplateProvider::
buildQParams_BAYER_NORMAL(
    DepthMapRequestPtr& rEffReqPtr,
    const Stereo3ATuningRes& tuningResult,
    QParams& rOutParm
)
{
    VSDOF_LOGD("+");
    // copy template
    rOutParm = mQParam_BAYER_NORMAL;
    // filler
    QParamTemplateFiller qParamFiller(rOutParm);
    MUINT iFrameNum = 0;
    DepthMapPipeNodeID nodeID = eDPETHMAP_PIPE_NODEID_P2ABAYER;
    sp<BaseBufferHandler> pBufferHandler = rEffReqPtr->getBufferHandler();
    // input : RSRAW
    IImageBuffer* frameBuf_RSRAW1 = pBufferHandler->requestBuffer(nodeID, BID_P2A_IN_RSRAW1);
    // tuning result
    AAATuningResult tuningRes = tuningResult.tuningRes_main1;
    // check exist Request FD buffer or not
    IImageBuffer* pFdBuf;
    MBOOL bExistFD = rEffReqPtr->getRequestImageBuffer({.bufferID=BID_P2A_OUT_FDIMG,
                                                        .ioType=eBUFFER_IOTYPE_OUTPUT}, pFdBuf);
    // fill buffer
    qParamFiller.insertTuningBuf(iFrameNum, tuningRes.tuningBuffer). // insert tuning data
            insertInputBuf(iFrameNum, PORT_IMGI, frameBuf_RSRAW1); // input: Main1 RSRAW

    if(bExistFD)
    {
        // insert FD output
        qParamFiller.insertOutputBuf(iFrameNum, PORT_IMG2O, pFdBuf).
                setCrop(iFrameNum, eCROP_CRZ, MPoint(0,0), frameBuf_RSRAW1->getImgSize(), pFdBuf->getImgSize());
    }
    else
    {   // no FD-> remove port in template.
        qParamFiller.delOutputPort(iFrameNum, PORT_IMG2O, eCROP_CRZ);
    }

    // CROP info
    MPoint ptCrop(0,0);
    MSize szCrop(frameBuf_RSRAW1->getImgSize().w, frameBuf_RSRAW1->getImgSize().h);
    IMetadata* pMeta_InHal = pBufferHandler->requestMetadata(nodeID, BID_META_IN_HAL_MAIN1);
    // get EIS crop if EIS is on
    if (rEffReqPtr->getRequestAttr().isEISOn)
    {
        eis_region region;
        if(queryEisRegion(pMeta_InHal, region))
        {
            ptCrop = MPoint(region.x_int, region.y_int);
            szCrop = region.s;
        }
    }
    // output: BID_P2A_OUT_MY_S
    IImageBuffer* pImgBuf_MY_S = pBufferHandler->requestBuffer(nodeID, BID_P2A_OUT_MY_S);
    // output: MV_F
    IImageBuffer* pImgBuf_MV_F = pBufferHandler->requestBuffer(nodeID, BID_P2A_OUT_MV_F);

    MBOOL bRet = qParamFiller.insertOutputBuf(iFrameNum, PORT_WDMAO, pImgBuf_MV_F).
                    setCrop(iFrameNum, eCROP_WDMA, ptCrop, szCrop, pImgBuf_MV_F->getImgSize()).
                    insertOutputBuf(iFrameNum, PORT_WROTO, pImgBuf_MY_S).
                    setCrop(iFrameNum, eCROP_WROT, ptCrop, szCrop, pImgBuf_MY_S->getImgSize()).
                    validate();

    VSDOF_LOGD("- res=%d", bRet);
    return bRet;
}

MBOOL
BayerMonoQTemplateProvider::
buildQParams_BAYER_CAPTURE(
    DepthMapRequestPtr& rEffReqPtr,
    const Stereo3ATuningRes& tuningResult,
    QParams& rOutParm
)
{
    VSDOF_LOGD("+");
    // copy template
    rOutParm = mQParam_BAYER_CAPTURE;
    // filler
    QParamTemplateFiller qParamFiller(rOutParm);
    //
    MUINT iFrameNum = 0;
    DepthMapPipeNodeID nodeID = eDPETHMAP_PIPE_NODEID_P2ABAYER;
    sp<BaseBufferHandler> pBufferHandler = rEffReqPtr->getBufferHandler();
    // tuning result
    AAATuningResult tuningRes = tuningResult.tuningRes_main1;
    // input : FSRAW
    IImageBuffer* pImgBuf_FSRAW1 = pBufferHandler->requestBuffer(nodeID, BID_P2A_IN_FSRAW1);
    // output: FD
    IImageBuffer* pImgBuf_FD = pBufferHandler->requestBuffer(nodeID, BID_P2A_OUT_FDIMG);
    // output: MV_F_CAP
    IImageBuffer* pImgBuf_MV_F_CAP = pBufferHandler->requestBuffer(nodeID, BID_P2A_OUT_MV_F_CAP);
    // output: BID_P2A_OUT_MY_SLL
    IImageBuffer* pImgBuf_MY_SLL = pBufferHandler->requestBuffer(nodeID, BID_P2A_OUT_MY_SLL);

    #ifndef GTEST
    // LSC
    IImageBuffer* pLSC2Src;
    if(tuningRes.tuningResult.pLsc2Buf != NULL)
    {
        // input: LSC2 buffer (for tuning)
        pLSC2Src = static_cast<IImageBuffer*>(tuningRes.tuningResult.pLsc2Buf);
        qParamFiller.insertInputBuf(iFrameNum, PORT_DEPI, pLSC2Src);
    }
    else
    {
        MY_LOGE("LSC2 buffer from 3A is NULL!!");
        return MFALSE;
    }
    #endif

    // FOV crop: crop settings already configured at preparation stage
    // fill buffer
    MBOOL bRet = qParamFiller.insertTuningBuf(iFrameNum, tuningRes.tuningBuffer). // insert tuning data
                    insertInputBuf(iFrameNum, PORT_IMGI, pImgBuf_FSRAW1). // input: Main1 FSRAW
                    insertOutputBuf(iFrameNum, PORT_IMG2O, pImgBuf_FD).   // output: FD
                    setCropResize(iFrameNum, eCROP_CRZ, pImgBuf_FD->getImgSize()).
                    insertOutputBuf(iFrameNum, PORT_WDMAO, pImgBuf_MV_F_CAP). // output: MV_F_CAP
                    setCropResize(iFrameNum, eCROP_WDMA, pImgBuf_MV_F_CAP->getImgSize()).
                    insertOutputBuf(iFrameNum, PORT_WROTO, pImgBuf_MY_SLL). // output: MY_SLL
                    setCropResize(iFrameNum, eCROP_WROT, pImgBuf_MY_SLL->getImgSize()).
                    validate();

    VSDOF_LOGD("- res=%d", bRet);
    return bRet;

}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  BayerBayerQTemplateProvider Protected Member
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

NSCam::NSIoPipe::PortID
BayerMonoQTemplateProvider::
mapToPortID(DepthMapBufferID bufferID)
{
    switch(bufferID)
    {
        case BID_P2A_OUT_MY_S:
            return PORT_WROTO;
        default:
            return BayerBayerQTemplateProvider::mapToPortID(bufferID);
    }
}


MBOOL
BayerMonoQTemplateProvider::
prepareQParam_frame1(
    DepthMapPipeOpState state,
    MINT32 iModuleTrans,
    QParams& rQParam
)
{
    // B+M VSDOF/Denoise: frame 1 is different from VSDOF.
    MUINT iFrameNum = 1;
    const P2ABufferSize& rP2AFMSize = mpSizeMgr->getP2A(eSTEREO_SCENARIO_CAPTURE);
    //
    QParamTemplateGenerator generator =
        QParamTemplateGenerator(iFrameNum, miSensorIdx_Main1, ENormalStreamTag_Normal).
        addInput(PORT_IMGI);

    // crop information
    MPoint ptFE1B_CROP(0,0);
    MSize szFE1B_CROP = rP2AFMSize.mFEB_INPUT_SIZE_MAIN1;

    if(state == eSTATE_CAPTURE)
    {
        #ifndef GTEST
        // capture request need extra input port for LSC buffer
        generator.addInput(PORT_DEPI);
        #endif
        // need FOV crop
        ptFE1B_CROP = rP2AFMSize.mFEB_INPUT_CROP_MAIN1.startPt;
        szFE1B_CROP = rP2AFMSize.mFEB_INPUT_CROP_MAIN1.contentSize();
    }
    // no FD and MV_F buffer for B+M mode
    MBOOL bSuccess =
        // WROT: FE1BO input
        generator.addCrop(eCROP_WROT, ptFE1B_CROP, szFE1B_CROP, rP2AFMSize.mFEB_INPUT_SIZE_MAIN1).
            addOutput(mapToPortID(BID_P2A_FE1B_INPUT), iModuleTrans).    // do module rotation
            generate(rQParam);

    return bSuccess;
}


MBOOL
BayerMonoQTemplateProvider::
buildQParam_frame0_capture(
    DepthMapRequestPtr& rEffReqPtr,
    const Stereo3ATuningRes& tuningResult,
    QParamTemplateFiller& rQFiller
)
{
    MUINT iFrameNum = 0;
    DepthMapPipeNodeID nodeID = eDPETHMAP_PIPE_NODEID_P2AFM;

    sp<BaseBufferHandler> pBufferHandler = rEffReqPtr->getBufferHandler();
    IImageBuffer* frameBuf_RSRAW2 = pBufferHandler->requestBuffer(nodeID, BID_P2A_IN_RSRAW2);

    AAATuningResult tuningRes = tuningResult.tuningRes_main2;
    // output: FE2B input(WROT)
    IImageBuffer* pImgBuf = pBufferHandler->requestBuffer(nodeID, BID_P2A_FE2B_INPUT);

    // default crop: use input raw region-> no crop
    MPoint cropStart(0, 0);
    MSize cropSize(frameBuf_RSRAW2->getImgSize());

    // denoise v2 need to do sensor crop of P1 rrzo crop of original flow
    if(mpPipeOption->mFeatureMode == eDEPTHNODE_MODE_DENOISE && getBMDeNoiseVersion() == VERSION_2)
    {
        IMetadata* pMeta_InHal = pBufferHandler->requestMetadata(nodeID, BID_META_IN_HAL_MAIN2);
        MRect cropRect;
        if(tryGetMetadata<MRect>(pMeta_InHal, MTK_P1NODE_SENSOR_CROP_REGION, cropRect))
        {
            cropStart = cropRect.p;
            cropSize = cropRect.s;
        }
        else
            MY_LOGE("Failed to query crop region!");
    }

    // filler buffers and the raw size crop info
    rQFiller.insertTuningBuf(iFrameNum, tuningRes.tuningBuffer).  // insert tuning data
        insertInputBuf(iFrameNum, PORT_IMGI, frameBuf_RSRAW2). // input: Main2 RSRAW
        setCrop(iFrameNum, eCROP_WROT, cropStart, cropSize,
                    mpSizeMgr->getP2A(eSTEREO_SCENARIO_RECORD).mFEB_INPUT_SIZE_MAIN2).
        insertOutputBuf(iFrameNum, mapToPortID(BID_P2A_FE2B_INPUT), pImgBuf);// output: FE2B input(WROT)
    return MTRUE;
}


MBOOL
BayerMonoQTemplateProvider::
buildQParam_frame1_capture(
    DepthMapRequestPtr& rEffReqPtr,
    const Stereo3ATuningRes& tuningResult,
    QParamTemplateFiller& rQFiller
)
{
    MUINT iFrameNum = 1;
    DepthMapPipeNodeID nodeID = eDPETHMAP_PIPE_NODEID_P2AFM;
    sp<BaseBufferHandler> pBufferHandler = rEffReqPtr->getBufferHandler();
    // tuning result
    AAATuningResult tuningRes = tuningResult.tuningRes_main1;
    // UT does not test 3A
    #ifndef GTEST
    // LSC
    IImageBuffer* pLSC2Src;
    if(tuningRes.tuningResult.pLsc2Buf != NULL)
    {
        // input: LSC2 buffer (for tuning)
        pLSC2Src = static_cast<IImageBuffer*>(tuningRes.tuningResult.pLsc2Buf);
        rQFiller.insertInputBuf(iFrameNum, PORT_DEPI, pLSC2Src);
    }
    else
    {
        MY_LOGE("LSC2 buffer from 3A is NULL!!");
        return MFALSE;
    }
    #endif

    // input : FSRAW
    IImageBuffer* pBuf_FSRAW1 = pBufferHandler->requestBuffer(nodeID, BID_P2A_IN_FSRAW1);
    // output: FE1B_input
    IImageBuffer* pFE1BInBuf = pBufferHandler->requestBuffer(nodeID, BID_P2A_FE1B_INPUT);

    // FOV crop: crop settings already configured at preparation stage
    rQFiller.insertInputBuf(iFrameNum, PORT_IMGI, pBuf_FSRAW1). //FSRAW
            setCropResize(iFrameNum, eCROP_WROT, pFE1BInBuf->getImgSize()).
            insertOutputBuf(iFrameNum, mapToPortID(BID_P2A_FE1B_INPUT), pFE1BInBuf). //FE1B_input
            insertTuningBuf(iFrameNum, tuningRes.tuningBuffer);

    return MTRUE;
}

MBOOL
BayerMonoQTemplateProvider::
buildQParam_frame1(
    DepthMapRequestPtr& rEffReqPtr,
    const Stereo3ATuningRes& tuningResult,
    QParamTemplateFiller& rQFiller
)
{
    // FOV crop : already applied in P1 RRZO
    MUINT iFrameNum = 1;
    DepthMapPipeNodeID nodeID = eDPETHMAP_PIPE_NODEID_P2AFM;
    sp<BaseBufferHandler> pBufferHandler = rEffReqPtr->getBufferHandler();
    // input : RSRAW
    IImageBuffer* frameBuf_RSRAW1 = pBufferHandler->requestBuffer(nodeID, BID_P2A_IN_RSRAW1);
    // tuning result
    AAATuningResult tuningRes = tuningResult.tuningRes_main1;

    // fill input buffer/tuning buffer
    rQFiller.insertTuningBuf(iFrameNum, tuningRes.tuningBuffer). // insert tuning data
            insertInputBuf(iFrameNum, PORT_IMGI, frameBuf_RSRAW1); // input: Main1 RSRAW

    // ouput: FE1B_input
    IImageBuffer* pImgBuf = pBufferHandler->requestBuffer(nodeID, BID_P2A_FE1B_INPUT);
    rQFiller.insertOutputBuf(iFrameNum, mapToPortID(BID_P2A_FE1B_INPUT), pImgBuf). //FE1B_input
            setCrop(iFrameNum, eCROP_WROT, MPoint(0,0), frameBuf_RSRAW1->getImgSize(), pImgBuf->getImgSize());

    return MTRUE;
}

MBOOL
BayerMonoQTemplateProvider::
buildQParam_frame3_denoise_capture(
    DepthMapRequestPtr& rEffReqPtr,
    const Stereo3ATuningRes& tuningResult,
    QParamTemplateFiller& rQFiller
)
{
    // DENOISE Capture only - no MYS generation
    MUINT iFrameNum = 3;
    DepthMapPipeNodeID nodeID = eDPETHMAP_PIPE_NODEID_P2AFM;
    sp<BaseBufferHandler> pBufferHandler = rEffReqPtr->getBufferHandler();
    // input: FE1BBuf_in
    IImageBuffer* pFE1BImgBuf;
    MBOOL bRet=pBufferHandler->getEnqueBuffer(nodeID, BID_P2A_FE1B_INPUT, pFE1BImgBuf);
    // output: FE1C_input
    IImageBuffer* pFE1CBuf_in =pBufferHandler->requestBuffer(nodeID, BID_P2A_FE1C_INPUT);
    // output: Rect_in1, use JPSMain1(get from remap) to avoid buffer copy.
    DepthMapBufferID bid_JPSMain1 = mpFlowOption->reMapBufferID(rEffReqPtr->getRequestAttr(), BID_P2A_OUT_RECT_IN1);
    IImageBuffer* pRectIn1Buf = pBufferHandler->requestBuffer(nodeID, bid_JPSMain1);
    // Rect_in1 : set active area
    pRectIn1Buf->setExtParam(mpSizeMgr->getP2A(eSTEREO_SCENARIO_CAPTURE).mRECT_IN_CONTENT_SIZE);
    // output: FE1BO
    IImageBuffer* pFE1BOBuf = pBufferHandler->requestBuffer(nodeID, BID_P2A_OUT_FE1BO);

    if(!bRet | !pFE1CBuf_in | !pRectIn1Buf | !pFE1BOBuf)
        return MFALSE;

    // Rect_in1 need setCrop. bcz its size is not constant between scenario
    rQFiller.insertInputBuf(iFrameNum, PORT_IMGI, pFE1BImgBuf). // FE1BBuf_in
            insertOutputBuf(iFrameNum, mapToPortID(BID_P2A_FE1C_INPUT), pFE1CBuf_in). // FE1C_input
            setCrop(iFrameNum, eCROP_WDMA, MPoint(0,0), pFE1BImgBuf->getImgSize(), pRectIn1Buf->getImgSize()).
            insertOutputBuf(iFrameNum, mapToPortID(BID_P2A_OUT_RECT_IN1), pRectIn1Buf). // Rect_in1
            insertOutputBuf(iFrameNum, mapToPortID(BID_P2A_OUT_FE1BO), pFE1BOBuf). // FE1BO
            insertTuningBuf(iFrameNum, nullptr);

    #ifdef GTEST
    IImageBuffer* pFEInputBuf = pBufferHandler->requestBuffer(nodeID, BID_FE2_HWIN_MAIN1);
    rQFiller.insertOutputBuf(iFrameNum, mapToPortID(BID_FE2_HWIN_MAIN1), pFEInputBuf);
    #endif
    return MTRUE;
}

INPUT_RAW_TYPE
BayerMonoQTemplateProvider::
getInputRawType(
    sp<DepthMapEffectRequest> pReq,
    StereoP2Path path
)
{
    INPUT_RAW_TYPE rawType;
    if(pReq->getRequestAttr().opState == eSTATE_CAPTURE)
    {
        rawType = (path == eP2APATH_MAIN1) ? eFULLSIZE_RAW : eRESIZE_RAW;
    }
    else
    {
        rawType = eRESIZE_RAW;
    }
    return rawType;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  BayerMonoQTemplateProvider Private Member
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

MBOOL
BayerMonoQTemplateProvider::
prepareTemplateParams_Bayer()
{
    VSDOF_LOGD("+");
    // prepare QParams template
    ENUM_ROTATION eRot = StereoSettingProvider::getModuleRotation();
    MINT32 iModuleTrans = -1;

    switch(eRot)
    {
        case eRotate_0:
            iModuleTrans = 0;
            break;
        case eRotate_90:
            iModuleTrans = eTransform_ROT_90;
            break;
        case eRotate_180:
            iModuleTrans = eTransform_ROT_180;
            break;
        case eRotate_270:
            iModuleTrans = eTransform_ROT_270;
            break;
        default:
            MY_LOGE("Not support module rotation =%d", eRot);
            return MFALSE;
    }

    MBOOL bRet = prepareQParamsTemplate_BAYER_NORMAL(iModuleTrans);
    bRet &= prepareQParamsTemplate_BAYER_CAPTURE(iModuleTrans);
    if(!bRet)
    {
        MY_LOGE("prepareQParamsTemplate failed!!!");
        return MFALSE;
    }
    VSDOF_LOGD("-");
    return MTRUE;
}

MBOOL
BayerMonoQTemplateProvider::
prepareQParamsTemplate_BAYER_NORMAL(MINT32 iModuleTrans)
{
    VSDOF_LOGD("+");
    const P2ABufferSize& rP2AFMSize = mpSizeMgr->getP2A(eSTEREO_SCENARIO_RECORD);
    MUINT iFrameNum = 0;

    // FOV crop already applied in RRZO
    MPoint ptOrigin(0, 0);
    MSize szNotSet(0, 0);
    MBOOL bSuccess =
        QParamTemplateGenerator(iFrameNum, miSensorIdx_Main1, ENormalStreamTag_Normal).
            addInput(PORT_IMGI).
            // IMG2O : FD
            addCrop(eCROP_CRZ, ptOrigin, szNotSet, rP2AFMSize.mFD_IMG_SIZE).
            addOutput(mapToPortID(BID_P2A_OUT_FDIMG)).
            // WDMA : MV_F
            addCrop(eCROP_WDMA, ptOrigin, szNotSet, rP2AFMSize.mMAIN_IMAGE_SIZE).
            addOutput(mapToPortID(BID_P2A_OUT_MV_F)).
            // WROT: Bayer out
            addCrop(eCROP_WROT, ptOrigin, szNotSet, rP2AFMSize.mBAYER_OUT_SIZE).
            addOutput(PORT_WROTO, iModuleTrans).                // do module rotation
            generate(mQParam_BAYER_NORMAL);

    VSDOF_LOGD("- : res=%d", bSuccess);
    return bSuccess;
}

MBOOL
BayerMonoQTemplateProvider::
prepareQParamsTemplate_BAYER_CAPTURE(MINT32 iModuleTrans)
{
    VSDOF_LOGD("+");
    MUINT iFrameNum = 0;
    const P2ABufferSize& rP2AFMSize = mpSizeMgr->getP2A(eSTEREO_SCENARIO_CAPTURE);

    // need FOV crop
    MPoint ptFD_CROP = rP2AFMSize.mFD_IMG_CROP.startPt;
    MSize szFD_CROP = rP2AFMSize.mFD_IMG_CROP.contentSize();
    MPoint ptMVF_CROP = rP2AFMSize.mMAIN_IMAGE_CROP.startPt;
    MSize szMVF_CROP = rP2AFMSize.mMAIN_IMAGE_CROP.contentSize();
    MPoint ptBAYER_CROP = rP2AFMSize.mBAYER_OUT_CROP.startPt;
    MSize szBAYER_CROP = rP2AFMSize.mBAYER_OUT_CROP.contentSize();

    //
    MBOOL bSuccess =
        QParamTemplateGenerator(iFrameNum, miSensorIdx_Main1, ENormalStreamTag_Normal).
            addInput(PORT_IMGI).
            #ifndef GTEST
            // Need extra input port for LSC buffer
            addInput(PORT_DEPI).
            #endif
            // IMG2O : FD
            addCrop(eCROP_CRZ, ptFD_CROP, szFD_CROP, rP2AFMSize.mFD_IMG_SIZE).
            addOutput(mapToPortID(BID_P2A_OUT_FDIMG)).
            // WDMA : MV_F
            addCrop(eCROP_WDMA, ptMVF_CROP, szMVF_CROP, rP2AFMSize.mMAIN_IMAGE_SIZE).
            addOutput(mapToPortID(BID_P2A_OUT_MV_F)).
            // WROT: Bayer out
            addCrop(eCROP_WROT, ptBAYER_CROP, szBAYER_CROP, rP2AFMSize.mBAYER_OUT_SIZE).
            addOutput(PORT_WROTO, iModuleTrans).                // do module rotation
            generate(mQParam_BAYER_CAPTURE);

    VSDOF_LOGD("- : res=%d", bSuccess);
    return bSuccess;
}

}; // NSFeaturePipe_DepthMap
}; // NSCamFeature
}; // NSCam

