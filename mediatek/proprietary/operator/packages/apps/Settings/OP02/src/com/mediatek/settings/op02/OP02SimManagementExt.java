/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op02;

import android.content.Context;
import android.os.SystemProperties;
import android.support.v7.preference.Preference;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.mediatek.internal.telephony.MtkDefaultSmsSimSettings;
import com.mediatek.settings.ext.DefaultSimManagementExt;

import java.util.List;

/**
 * CU sim switch.
 */
public class OP02SimManagementExt extends DefaultSimManagementExt {

    private static final String TAG = "OP02SimManagementExt";
    private Context mContext;
    private static final String KEY_CELLULAR_DATA = "sim_cellular_data";
    private static final String KEY_CALLS = "sim_calls";
    private static final String KEY_SMS = "sim_sms";
    public static final int DATA_PICK = 0;
    public static final int CALLS_PICK = 1;
    public static final int SMS_PICK = 2;
    /**
    * Constructor method.
    * @param context is Settings's context.
    */
    public OP02SimManagementExt(Context context) {
        super();
        mContext = context;
    }

    private void print(String msg, int[] lists) {
        if (lists != null) {
            for (int i : lists) {
                Log.d(TAG, msg + i);
            }
        } else {
            Log.d(TAG, msg + "is null");
        }
    }

    /**
     * Called when SIM dialog is about to show for SIM info changed
     * @return false if plug-in do not need SIM dialog
     */
    @Override
    public boolean isSimDialogNeeded() {
        return false;
    }

    /**
     * M:spec CU feature: when select data from sim dialog.
     * default data and data enable must be in same card.
     * @param subid Sub id.
     */
    @Override
    public void setDataState(int subid) {
        TelephonyManager mTelephonyManager;
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(mContext);
        mTelephonyManager = TelephonyManager.from(mContext);
        boolean enableBefore = mTelephonyManager.getDataEnabled();
        int mResetSubId = subscriptionManager.getDefaultDataSubscriptionId();
        Log.d(TAG, "setDataState subid = " + subid
                + " enable_before = " + enableBefore
                + " mResetSubId = " + mResetSubId);

        if (subscriptionManager.isValidSubscriptionId(subid)
                && subid != mResetSubId) {
            subscriptionManager.setDefaultDataSubId(subid);
            if (enableBefore) {
                mTelephonyManager.setDataEnabled(subid, true);
                mTelephonyManager.setDataEnabled(mResetSubId, false);
            }
        }
    }

    private boolean isTwoSimInserted() {
        String sim1 = SystemProperties.get("ril.iccid.sim1", "");
        String sim2 = SystemProperties.get("ril.iccid.sim2", "");
        String noSim = "N/A";
        if (noSim.equals(sim1) || noSim.equals(sim2)) {
            Log.d(TAG, "isTwoSimInserted: " + sim1 + " " + sim2);
            return false;
        }
        return true;
    }

    /**
     * For CTA case fail,CU dismiss the google default dialog,
     * when use insert only one cu card, google default dialog will not show,
     * But CTA auto case will choose sim when dialog show,(CU will not show)
     * and google default simsettings will not enable when simcount <= 1.
     * So need add this method let user can choose sim in simsetting
     * @param simPref simPref
     * @param type type
     * @param size size
     */
    public void configSimPreferenceScreen(Preference simPref, String type, int size) {
        Log.d(TAG, "configSimPreferenceScreen type: " + type + ", size: " + size);
        int count = TelephonyManager.getDefault().getPhoneCount();
        boolean hasActiveSubscription
            = SubscriptionManager.from(mContext).getActiveSubscriptionInfoCount() >= 1;
        Log.i(TAG, "isGeminiSupport phone count = " + count
                + " hasActiveSubscription = " + hasActiveSubscription);
        if (count <= 1) {
            return;
        }
        switch (type) {
            case KEY_CALLS:
            case KEY_SMS:
                simPref.setEnabled(size >= 1 && hasActiveSubscription);
                break;
            default:
                break;
        }
    }

    /**
     * For CTA case fail,CU dismiss the google default dialog,
     * when use insert only one cu card, google default dialog will not show,
     * But CTA auto case will choose sim when dialog show,(CU will not show)
     * and google default simsettings will not enable when simcount <= 1.
     * So changed the value "always ask" to simcard1
     * @param value "always ask","sim1","sim2" means 0,1,2
     * @return value when ActiveSubscriptionInfoCount is 1,
     * and value is 0 --"always adk", changed value to 1 --sim1
     */
//    public int customizeValue(int value) {
//        if ((value == 0) && (SubscriptionManager.from(mContext)
//                .getActiveSubscriptionInfoCount() == 1) &&
//                TelephonyManager.getDefault().getPhoneCount() > 1) {
//            value = 1;
//            Log.d(TAG, "customizeValue, value = " + value);
//        }
//        return value;
//    }
    
    /**
     * simDialogOnClick.
     * @param id type of sim prefrence
     * @param value value of position selected
     * @param context context
     * @return handled by plugin or not
     */
    @Override
    public boolean simDialogOnClick(int id, int value, Context context) {
        boolean isNeedId = false;
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        final List<SubscriptionInfo> subInfoList =
            subscriptionManager.getActiveSubscriptionInfoList();
        switch (id) {
            case DATA_PICK:
                break;
            case CALLS_PICK:
                if ((value == 0) &&
                        (SubscriptionManager.from(mContext).getActiveSubscriptionInfoCount() == 1)
                            && TelephonyManager.getDefault().getPhoneCount() > 1) {
                    /// For single card, click item.
                    value = 1;
                    Log.d(TAG, "customizeValue, value = " + value);
                }
                final TelecomManager telecomManager =
                        TelecomManager.from(context);
                final List<PhoneAccountHandle> phoneAccountsList =
                        telecomManager.getCallCapablePhoneAccounts();
                Log.d(TAG, "phoneAccountsList = " + phoneAccountsList.toString());
    
                /// M: for ALPS02320816 @{
                // phone account may changed in background
                if (value > phoneAccountsList.size()) {
                    Log.w(TAG, "phone account changed, do noting! value = " +
                            value + ", phone account size = " +
                            phoneAccountsList.size());
                    break;
                }
                /// @}
                Log.d(TAG, "value = " + value);
                setUserSelectedOutgoingPhoneAccount(value < 1 ?
                        null : phoneAccountsList.get(value - 1));
                isNeedId = true;
                break;
            case SMS_PICK:
                if ((value == 0) &&
                        (SubscriptionManager.from(mContext).getActiveSubscriptionInfoCount() == 1)
                            && TelephonyManager.getDefault().getPhoneCount() > 1) {
                    /// For single card, click item.
                    value = 1;
                    Log.d(TAG, "customizeValue, value = " + value);
                }
                int subId = getPickSmsDefaultSub(subInfoList, value);
                setDefaultSmsSubId(context, subId);
                isNeedId = true;
                break;
            default:
                throw new IllegalArgumentException("Invalid dialog type "
                        + id + " in SIM dialog.");
        }
        return isNeedId;
    }

    private void setUserSelectedOutgoingPhoneAccount(PhoneAccountHandle phoneAccount) {
        Log.d(TAG, "setUserSelectedOutgoingPhoneAccount phoneAccount = " + phoneAccount);
        final TelecomManager telecomManager = TelecomManager.from(mContext);
        telecomManager.setUserSelectedOutgoingPhoneAccount(phoneAccount);
    }

    private int getPickSmsDefaultSub(final List<SubscriptionInfo> subInfoList,
            int value) {
        int subId = SubscriptionManager.INVALID_SUBSCRIPTION_ID;

        if (value < 1) {
            int length = subInfoList == null ? 0 : subInfoList.size();
            if (length == 1) {
                subId = subInfoList.get(value).getSubscriptionId();
            } else {
                ///just for fix build error , on O migration.
//                subId = MtkDefaultSmsSimSettings.ASK_USER_SUB_ID;
            }
        } else if (value >= 1 && value < subInfoList.size() + 1) {
            subId = subInfoList.get(value - 1).getSubscriptionId();
        }
        Log.d(TAG, "getPickSmsDefaultSub, value: " + value + ", subId: " + subId);
        return subId;
    }

    private static void setDefaultSmsSubId(final Context context, final int subId) {
        Log.d(TAG, "setDefaultSmsSubId, sub = " + subId);
        final SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
        subscriptionManager.setDefaultSmsSubId(subId);
    }

}
