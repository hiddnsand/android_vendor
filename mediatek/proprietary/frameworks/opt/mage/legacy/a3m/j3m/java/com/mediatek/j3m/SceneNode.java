/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M SceneNode interface.
 */

package com.mediatek.j3m;

/* Extend JNI Overview */ /*! \addtogroup a3mJusr

<h2>A3M Scene Construction</h2>

As described in the \ref a3mPrimer "Primer" the scene is described mainly by a
hierachical tree of objects (the SceneGraph) that includes cameras and lights,
and the active camera settings including projection type.

\e Every object (including lights, cameras, ...) is described as a SceneNode.

A SceneNode has the following properties:

- position in the hierarchy, defined via setParent()
- children
- position, scaling and rotation <i>relative to its parent</i>
- name, a simple text string
- render flags

<h3>%SceneNode Names</h3>

A SceneNode's name is typically defined by the DCC tool that created it.  The
name can be read back and the SceneGraph can be searched for a node of a
particular name.  This is useful when the application needs to control the
movement of a part of a predefined scene programatically.  Names to not have to
be unique within the scene graph, although it may be more useful to the user if
this is the case.

<h3>%SceneNode Render Flags</h3>

Each SceneNode has a set of flag which can be used to define whether that node
is to be rendered or not in a particular %RenderBlock pass.  These flags can be
read or set for each %Scenenode using a %FlagMask.  If a flag is specified in a
%RenderBlock, then the corresponding flag must be set in a %SceneNode for that
node to be rendered.  See %RenderBlock and %FlagMask for more information on
how to use flags.

*/

/*!
 * \ingroup a3mJusrScenenodes
 * @{
 */
/**
 * Base SceneNode class provides the generic attributes such as position,
 * scale, rotation, visibility, etc and methods to manage the scene node structure
 * (the scene graph).
 */
public interface SceneNode {
    /** Identifier for this node type */
    char NODE_TYPE = 'N';

    /**
     * Returns the type of this scene node.
     * This value can be compared against the NODE_TYPE field of the various
     * SceneNode interfaces to determine whether this node is a SceneNode,
     * Solid, Camera or Light.
     *
     * @return Node type ID
     */
    char getNodeType();

    /**
     * Sets the name of the scene node.
     *
     * @param name Scene node name
     */
    void setName(String name);

    /**
     * Returns the name of the scene node.
     *
     * @return Scene node name
     */
    String getName();

    /**
     * Sets the parent scene node of the scene node.
     * If the parent is given as null, this node will have no parent.
     *
     * @param node the new parent node
     */
    void setParent(SceneNode node);

    /**
     * Sets the parent of the scene node while optionally preserving the world
     * transform.
     * Normally when setting a new parent node, the local tranformation of the
     * node will be preserved, but using this function, the world
     * transformation can instead be maintained.
     *
     * @param node the new parent node
     * @param preserveWorldTransform true to preserve world transform
     */
    void setParent(SceneNode node, boolean preserveWorldTransform);

    /**
     * Returns the parent of the scene node.
     * Null will be returned if the scene node has not parent.
     *
     * @return the parent node
     */
    SceneNode getParent();

    /**
     * Returns number of children attached to current scene node
     *
     * @return Returns scene node children count
     */
    int getChildCount();

    /**
     * Gets requested child scene node.
     *
     * @param i index in SceneNode vector containing children
     * @return returns requested child scene node
     */
    SceneNode getChild(int i);

    /**
     * Find a scene node by its name
     *
     * @param name the name to search for
     * @return the scene node or null if not found
     */
    SceneNode find(String name);

    /**
     * Points the scene node at a point in 3D space, while rotating the node to
     * align with a specified global up-vector.
     * Forwards in this context is the node's positive local z-axis.  The node
     * will be rotated around the local z-axis such that its local y-axis is
     * aligned with the given global up-vector.
     *
     * @param worldForwardX X-component of world-space vector to point at
     * @param worldForwardY Y-component of world-space vector to point at
     * @param worldForwardZ Z-component of world-space vector to point at
     */
    void point(float worldForwardX, float worldForwardY, float worldForwardZ);

    /**
     * Points the scene node at a point in 3D space, while rotating the node to
     * align with a specified global up-vector.
     * Forwards in this context is the node's positive local z-axis.  The node
     * will be rotated around the local z-axis such that its local y-axis is
     * aligned with the given global up-vector.
     *
     * @param worldForwardX X-component of world-space vector to point at
     * @param worldForwardY Y-component of world-space vector to point at
     * @param worldForwardZ Z-component of world-space vector to point at
     * @param worldUpX X-component of world-space up-vector to align with
     * @param worldUpY Y-component of world-space vector to align with
     * @param worldUpZ Z-component of world-space vector to align with
     */
    void point(
        float worldForwardX, float worldForwardY, float worldForwardZ,
        float worldUpX, float worldUpY, float worldUpZ);

    /**
     * Points the scene node at a point in 3D space, while rotating the node to
     * align with a specified global up-vector.
     * Forwards in this context is the node's positive local z-axis.  The node
     * will be rotated around the local z-axis such that its local y-axis is
     * aligned with the given global up-vector.
     *
     * @param worldForwardX X-component of world-space vector to point at
     * @param worldForwardY Y-component of world-space vector to point at
     * @param worldForwardZ Z-component of world-space vector to point at
     * @param worldUpX X-component of world-space up-vector to align with
     * @param worldUpY Y-component of world-space vector to align with
     * @param worldUpZ Z-component of world-space vector to align with
     * @param localForwardX X-component of local forward-vector
     * @param localForwardY Y-component of local forward-vector
     * @param localForwardZ Z-component of local forward-vector
     */
    void point(
        float worldForwardX, float worldForwardY, float worldForwardZ,
        float worldUpX, float worldUpY, float worldUpZ,
        float localForwardX, float localForwardY, float localForwardZ);

    /**
     * Points the scene node at a point in 3D space, while rotating the node to
     * align with a specified global up-vector.
     * Forwards in this context is the node's positive local z-axis.  The node
     * will be rotated around the local z-axis such that its local y-axis is
     * aligned with the given global up-vector.
     *
     * @param worldForwardX X-component of world-space vector to point at
     * @param worldForwardY Y-component of world-space vector to point at
     * @param worldForwardZ Z-component of world-space vector to point at
     * @param worldUpX X-component of world-space up-vector to align with
     * @param worldUpY Y-component of world-space vector to align with
     * @param worldUpZ Z-component of world-space vector to align with
     * @param localForwardX X-component of local forward-vector
     * @param localForwardY Y-component of local forward-vector
     * @param localForwardZ Z-component of local forward-vector
     * @param localUpX X-component of local up-vector
     * @param localUpY Y-component of local up-vector
     * @param localUpZ Z-component of local up-vector
     */
    void point(
        float worldForwardX, float worldForwardY, float worldForwardZ,
        float worldUpX, float worldUpY, float worldUpZ,
        float localForwardX, float localForwardY, float localForwardZ,
        float localUpX, float localUpY, float localUpZ);

    /**
     * Sets the position relative to the node's parent.
     *
     * @param x X component of the new position
     * @param y Y component of the new position
     * @param z Z component of the new position
     */
    void setPosition(float x, float y, float z);

    /**
     * Returns the X component of the position relative to the node's parent.
     *
     * @return Position X component
     */
    float getPositionX();

    /**
     * Returns the Y component of the position relative to the node's parent.
     *
     * @return Position Y component
     */
    float getPositionY();

    /**
     * Returns the Z component of the position relative to the node's parent.
     *
     * @return Position Z component
     */
    float getPositionZ();

    /**
     * Sets the rotation relative to the node's parent.
     * The rotation is specified as a quaternion q where q = a + ib + jc + kd
     *
     * @param a A component of the new rotation
     * @param b B component of the new rotation
     * @param c C component of the new rotation
     * @param d D component of the new rotation
     */
    void setRotation(float a, float b, float c, float d);

    /**
     * Returns the A component of the rotation relative to the node's parent.
     *
     * @return Rotation A component
     */
    float getRotationA();

    /**
     * Returns the B component of the rotation relative to the node's parent.
     *
     * @return Rotation B component
     */
    float getRotationB();

    /**
     * Returns the C component of the rotation relative to the node's parent.
     *
     * @return Rotation C component
     */
    float getRotationC();

    /**
     * Returns the D component of the rotation relative to the node's parent.
     *
     * @return Rotation D component
     */
    float getRotationD();

    /**
     * Sets the scale relative to the node's parent.
     *
     * @param x X component of the new scale
     * @param y Y component of the new scale
     * @param z Z component of the new scale
     */
    void setScale(float x, float y, float z);

    /**
     * Returns the X component of the scale relative to the node's parent.
     *
     * @return Scale X component
     */
    float getScaleX();

    /**
     * Returns the Y component of the scale relative to the node's parent.
     *
     * @return Scale Y component
     */
    float getScaleY();

    /**
     * Returns the Z component of the scale relative to the node's parent.
     *
     * @return Scale Z component
     */
    float getScaleZ();

    /**
     * Returns whether this node is locally mirrored.
     * This indicates whether the node would appear as a mirror image of its
     * untransformed self, when not taking into account parent transformations.
     *
     * @return True if the node is locally mirrored
     */
    boolean getLocalMirrored();

    /**
     * Returns whether this node is globally mirrored.
     * This indicates whether the node would appear as a mirror image of its
     * untransformed self, when taking into account parent transformations.
     *
     * @return True if the node is globally mirrored
     */
    boolean getWorldMirrored();

    /**
     * Sets all flags specified by the mask to the specified state.
     *
     * @param mask mask containing flags whose state to set
     * @param state boolean flag state to set
     */
    void setFlags(FlagMask mask, boolean state);

    /**
     * Returns whether all flags specified by the mask are in the specified
     * state.
     *
     * @param mask mask containing flags whose state to check
     * @return true if all specified flags are inthe specified state
     */
    boolean getFlags(FlagMask mask);

    /**
     * Checks the derived state of one or more flags for this node.
     * Derived flags take into account the state of the flags of parents of the
     * scene node (not just the direct parent, but also their parents, and so
     * on).  A flag state is only inherited from a parent when it is set to the
     * non-default state (e.g. a flag called VISIBLE whose default state is true
     * will only be inherited if it is set to false; conversely, a flag called
     * HIDDEN whose default state is false will only be inherited when it is set
     * to true).
     *
     * @param mask mask containing flags to check
     * @return true if all the specified derived flags are set
     */
    boolean getDerivedFlags(FlagMask mask);
}

/*! @} */
