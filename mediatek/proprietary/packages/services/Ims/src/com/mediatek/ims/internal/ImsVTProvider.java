/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.ims.internal;

import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.telecom.VideoProfile.CameraCapabilities;
import android.telecom.Connection;
import android.telecom.VideoProfile;
import android.view.Surface;
import android.util.Log;

// for Data usage
import android.telephony.TelephonyManager;
import android.content.Context;
import android.net.IConnectivityManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.NetworkCapabilities;
import android.net.NetworkStats;
//import android.net.NetworkStats.UID_ALL;
import android.os.INetworkManagementService;
import android.os.IBinder;
import android.os.ServiceManager;
import java.util.Objects;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.Set;
import java.lang.Integer;
import java.lang.Thread;
import java.lang.Integer;
import java.lang.Thread;

// for async the action to anthor thread
import com.android.internal.os.SomeArgs;

import com.android.ims.internal.ImsVideoCallProvider;

import com.mediatek.ims.internal.ImsVTProviderUtil.Size;
import com.mediatek.ims.internal.VTSource;
import com.mediatek.internal.telephony.IMtkTelephonyEx;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;
import android.hardware.camera2.CameraCharacteristics;
import mediatek.telecom.MtkVideoProfile;

public class ImsVTProvider extends ImsVideoCallProvider implements
        VTSource.EventCallback {

    static {
        if (!SystemProperties.get("ro.mtk_ims_video_call_support", "none").equals("none")) {
            System.loadLibrary("mtk_vt_wrapper");
        }
    }

    /**
     * The interface used to notify video call related state/event
     */
    public interface VideoProviderStateListener {
        void onReceivePauseState(boolean isPause);
    }

    public static final int VT_PROVIDER_INVALIDE_ID                             = -10000;

    // ===================================================================================
    // operation constant
    //
    // AOSP original refrence from ImsVideoCallProvider class
    private static final int MSG_SET_CALLBACK                                      = 1;
    private static final int MSG_SET_CAMERA                                        = 2;
    private static final int MSG_SET_PREVIEW_SURFACE                               = 3;
    private static final int MSG_SET_DISPLAY_SURFACE                               = 4;
    private static final int MSG_SET_DEVICE_ORIENTATION                            = 5;
    private static final int MSG_SET_ZOOM                                          = 6;
    private static final int MSG_SEND_SESSION_MODIFY_REQUEST                       = 7;
    private static final int MSG_SEND_SESSION_MODIFY_RESPONSE                      = 8;
    private static final int MSG_REQUEST_CAMERA_CAPABILITIES                       = 9;
    private static final int MSG_REQUEST_CALL_DATA_USAGE                           = 10;
    private static final int MSG_SET_PAUSE_IMAGE                                   = 11;
    //
    // MTK extend
    //
    private static final int MSG_SET_UI_MODE                                       = 701;
    private static final int MSG_SWITCH_FEATURE                                    = 702;
    // ===================================================================================

    // ===================================================================================
    // callback event constant
    public static final int SESSION_EVENT_RECEIVE_FIRSTFRAME                       = 1001;
    public static final int SESSION_EVENT_SNAPSHOT_DONE                            = 1002;
    public static final int SESSION_EVENT_RECORDER_EVENT_INFO_UNKNOWN              = 1003;
    public static final int SESSION_EVENT_RECORDER_EVENT_INFO_REACH_MAX_DURATION   = 1004;
    public static final int SESSION_EVENT_RECORDER_EVENT_INFO_REACH_MAX_FILESIZE   = 1005;
    public static final int SESSION_EVENT_RECORDER_EVENT_INFO_NO_I_FRAME           = 1006;
    public static final int SESSION_EVENT_RECORDER_EVENT_INFO_COMPLETE             = 1007;
    public static final int SESSION_EVENT_CALL_END                                 = 1008;
    public static final int SESSION_EVENT_CALL_ABNORMAL_END                        = 1009;
    public static final int SESSION_EVENT_START_COUNTER                            = 1010;
    public static final int SESSION_EVENT_PEER_CAMERA_OPEN                         = 1011;
    public static final int SESSION_EVENT_PEER_CAMERA_CLOSE                        = 1012;
    public static final int SESSION_EVENT_LOCAL_BW_READY_IND                       = 1013;

    public static final int SESSION_EVENT_RECV_SESSION_CONFIG_REQ                  = 4001;
    public static final int SESSION_EVENT_RECV_SESSION_CONFIG_RSP                  = 4002;
    public static final int SESSION_EVENT_HANDLE_CALL_SESSION_EVT                  = 4003;
    public static final int SESSION_EVENT_PEER_SIZE_CHANGED                        = 4004;
    public static final int SESSION_EVENT_LOCAL_SIZE_CHANGED                       = 4005;

    // it should not be used, after N MR1, we do this on java layer
    public static final int SESSION_EVENT_DATA_USAGE_CHANGED                       = 4006;
    public static final int SESSION_EVENT_CAM_CAP_CHANGED                          = 4007; // useless after HAL3

    public static final int SESSION_EVENT_BAD_DATA_BITRATE                         = 4008;
    public static final int SESSION_EVENT_DATA_BITRATE_RECOVER                     = 4009;
    public static final int SESSION_EVENT_RECV_CANCEL_SESSION_IND                  = 4010;
    public static final int SESSION_EVENT_DATA_PATH_PAUSE                          = 4011;
    public static final int SESSION_EVENT_DATA_PATH_RESUME                         = 4012;
    public static final int SESSION_EVENT_DEFAULT_LOCAL_SIZE                       = 4013;
    public static final int SESSION_EVENT_GET_CAP                                  = 4014;
    public static final int SESSION_EVENT_LOCAL_BUFFER                             = 4015;
    public static final int SESSION_EVENT_UPLINK_STATE_CHANGE                      = 4016;

    public static final int SESSION_EVENT_ERROR_SERVICE                            = 8001;
    public static final int SESSION_EVENT_ERROR_SERVER_DIED                        = 8002;
    public static final int SESSION_EVENT_ERROR_CAMERA_CRASHED                     = 8003;
    public static final int SESSION_EVENT_ERROR_CODEC                              = 8004;
    public static final int SESSION_EVENT_ERROR_REC                                = 8005;
    public static final int SESSION_EVENT_ERROR_CAMERA_SET_IGNORED                 = 8006;

    public static final int SESSION_EVENT_WARNING_SERVICE_NOT_READY                = 9001;
    // ===================================================================================

    // ===================================================================================
    // session modify result constant
    //
    // For UA session modify result
    public static final int SESSION_MODIFY_OK                                      = 0;
    // Wrong video direction cause up/downgrade failed
    public static final int SESSION_MODIFY_WRONGVIDEODIR                           = 1;
    // Acct or session object broken
    public static final int SESSION_MODIFY_INTERNALERROR                           = 2;
    // Bandwidth modify require failed
    public static final int SESSION_MODIFY_RESULT_BW_MODIFYFAILED                  = 3;
    // Call session not in active state
    public static final int SESSION_MODIFY_NOACTIVESTATE                           = 4;
    // Local release:Ex.SRVCC,Hungup,call refresh timer timeout,no rpt packets
    public static final int SESSION_MODIFY_LOCALREL                                = 5;
    // The call at hold state
    public static final int SESSION_MODIFY_ISHOLD                                  = 6;
    // Acorrding the video action state, not need up/downgrade
    public static final int SESSION_MODIFY_NONEED                                  = 7;
    // Input parameter invalid
    public static final int SESSION_MODIFY_INVALIDPARA                             = 8;
    // SIPTX error,transaction timeout
    public static final int SESSION_MODIFY_REQTIMEOUT                              = 9;
    // Reject by remote
    public static final int SESSION_MODIFY_REJECTBYREMOTE                          = 10;
    // ===================================================================================

    // ===================================================================================
    // session cancel result constant
    //
    public static final int SESSION_MODIFY_CANCEL_OK                               = 0;
    public static final int SESSION_MODIFY_CANCEL_FAILED_DOWNGRADE                 = 1;
    public static final int SESSION_MODIFY_CANCEL_FAILED_NORMAL                    = 2;
    public static final int SESSION_MODIFY_CANCEL_FAILED_DISABLE                   = 3;
    public static final int SESSION_MODIFY_CANCEL_FAILED_REMOTE                    = 4;
    public static final int SESSION_MODIFY_CANCEL_FAILED_INTERNAL                  = 5;
    public static final int SESSION_MODIFY_CANCEL_FAILED_LOCAL                     = 6;
    public static final int SESSION_MODIFY_CANCEL_FAILED_BW                        = 7;
    public static final int SESSION_MODIFY_CANCEL_FAILED_TIMEOUT                   = 8;
    // ===================================================================================

    public static final Uri REPLACE_PICTURE_PATH = Uri.parse("content://PATH");
    public static final String MTK_VILTE_ROTATE_DELAY = "persist.mtk_vilte_rotate_delay";

    public static final int UPLINK_STATE_STOP_RECORDING                            = 0;
    public static final int UPLINK_STATE_START_RECORDING                           = 1;
    public static final int UPLINK_STATE_PAUSE_RECORDING                           = 2;
    public static final int UPLINK_STATE_RESUME_RECORDING                          = 3;

    public static final int MODE_PAUSE_BY_HOLD                                     = 1;
    public static final int MODE_PAUSE_BY_TURNOFFCAM                               = 2;

    public static final int POSTEVENT_IGNORE_ID                                    = -10;

    static final String                         TAG = "ImsVT";

    private int                                 mId = 1;
    public int                                  mMode = ImsVTProviderUtil.UI_MODE_FG;
    public long                                 mInitialUsage;
    private static int                          mDefaultId = VT_PROVIDER_INVALIDE_ID;
    public boolean                              mInitComplete = false;
    private boolean                             mNeedReportDataUsage = true;
    private boolean                             mDuringSessionRequestOperation = false;
    private boolean                             mDuringSessionRemoteRequestOperation = false;
    private boolean                             mIsDuringResetMode = false;
    private Object                              mSessionOperationFlagLock = new Object();
    private VideoProfile                        mLastRequestVideoProfile;

    private boolean                             mIsMaCrashed = false;

    public VTSource                             mSource;
    private String                              mCameraId = null;
    public boolean                              mHasRequestCamCap = false;

    public  int                                 mPauseCount = 0;
    private int                                 mUplinkState = UPLINK_STATE_STOP_RECORDING;
    private Size                                mPreviewSize = new Size(320, 240);

    protected HandlerThread                     mProviderHandlerThread;
    private final Handler                       mProviderHandler;

    private final Set<VideoProviderStateListener> mListeners = Collections.newSetFromMap(
            new ConcurrentHashMap<VideoProviderStateListener, Boolean>(8, 0.9f, 1));

    /*start*/
    private class ConnectionEx {
        public class VideoProvider {
            private static final int SESSION_MODIFY_MTK_BASE = 200;
            public static final int SESSION_MODIFY_CANCEL_UPGRADE_FAIL = SESSION_MODIFY_MTK_BASE;
            public static final int SESSION_MODIFY_CANCEL_UPGRADE_FAIL_AUTO_DOWNGRADE =
                    SESSION_MODIFY_MTK_BASE+1;
            public static final int SESSION_MODIFY_CANCEL_UPGRADE_FAIL_REMOTE_REJECT_UPGRADE =
                    SESSION_MODIFY_MTK_BASE+2;
        }
    }
    /*end*/

    private int                                 mOrientation = 0;
    private Runnable                            mOrientationRunnable = null;

    public ImsVTProvider() {
        super();
        Log.d(TAG, "New ImsVTProvider without id");
        mId = VT_PROVIDER_INVALIDE_ID;
        mInitComplete = false;

        mProviderHandlerThread = new HandlerThread("ProviderHandlerThread");
        mProviderHandlerThread.start();

        mProviderHandler = new Handler(mProviderHandlerThread.getLooper()) {

            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MSG_SET_CAMERA:
                        setCameraInternal((String) msg.obj);
                        break;
                    case MSG_SET_PREVIEW_SURFACE:
                        setPreviewSurfaceInternal((Surface) msg.obj);
                        break;
                    case MSG_SET_DISPLAY_SURFACE:
                        setDisplaySurfaceInternal((Surface) msg.obj);
                        break;
                    case MSG_SET_DEVICE_ORIENTATION:
                        setDeviceOrientationInternal(msg.arg1);
                        break;
                    case MSG_SET_ZOOM:
                        setZoomInternal((Float) msg.obj);
                        break;
                    case MSG_SEND_SESSION_MODIFY_REQUEST: {
                        SomeArgs args = (SomeArgs) msg.obj;
                        try {
                            VideoProfile fromProfile = (VideoProfile) args.arg1;
                            VideoProfile toProfile = (VideoProfile) args.arg2;

                            sendSessionModifyRequestInternal(fromProfile, toProfile);
                        } finally {
                            args.recycle();
                        }
                        break;
                    }
                    case MSG_SEND_SESSION_MODIFY_RESPONSE:
                        sendSessionModifyResponseInternal((VideoProfile) msg.obj);
                        break;
                    case MSG_REQUEST_CAMERA_CAPABILITIES:
                        requestCameraCapabilitiesInternal();
                        break;
                    case MSG_REQUEST_CALL_DATA_USAGE:
                        requestCallDataUsageInternal();
                        break;
                    case MSG_SET_PAUSE_IMAGE:
                        setPauseImageInternal((Uri) msg.obj);
                        break;
                    case MSG_SET_UI_MODE:
                        setUIModeInternal((int) msg.obj);
                        break;
                    case MSG_SWITCH_FEATURE:
                        SomeArgs args = (SomeArgs) msg.obj;
                        try {
                            int feature = (int) args.arg1;
                            boolean on = (boolean) args.arg2;

                            switchFeatureInternal(feature, on);
                        } finally {
                            args.recycle();
                        }

                        break;
                    default:
                        break;
                }
            }
        };

        mOrientationRunnable = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "setDeviceOrientation, apply orientation:" + mOrientation);
                mSource.setDeviceOrientation(mOrientation);
                nSetDeviceOrientation(mId, mOrientation);
            }
        };
    }

    public void setId(int id) {
        Log.d(TAG, "setId id = " + id);
        Log.d(TAG, "setId mId = " + mId);

        mId = id;

        if (mDefaultId == VT_PROVIDER_INVALIDE_ID) {
            mDefaultId = mId;
        }
    }

    public int getId() {
        return mId;
    }

    public void setDuringSessionRequest(boolean b) {

        synchronized (mSessionOperationFlagLock) {
            mDuringSessionRequestOperation = b;
            Log.w(TAG, "setDuringSessionRequest : " + mDuringSessionRequestOperation);
        }
    }

    public boolean getDuringSessionRequest() {
        return mDuringSessionRequestOperation;
    }

    public void setDuringSessionRemoteRequest(boolean b) {

        synchronized (mSessionOperationFlagLock) {
            mDuringSessionRemoteRequestOperation = b;
            Log.w(TAG, "setDuringSessionRemoteRequest : " + mDuringSessionRemoteRequestOperation);
        }
    }

    public boolean getDuringSessionRemoteRequest() {
        return mDuringSessionRemoteRequestOperation;
    }

    public void setMaCrashed(boolean b) {
        mIsMaCrashed = b;
        Log.w(TAG, "setMaCrashed : " + mIsMaCrashed);
    }

    public boolean getMaCrashed() {
        return mIsMaCrashed;
    }

    public void handleMaErrorProcess() {

        Log.w(TAG, "[ID=" + mId + "] handleMaErrorProcess()");

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        if (getMaCrashed()) {
            Log.w(TAG, "handleMaErrorProcess() : already during MA crash, skip");
            return;
        }

        // actualy, we don't need the original state for reqest
        VideoProfile currentProfile = new VideoProfile(VideoProfile.STATE_BIDIRECTIONAL, VideoProfile.QUALITY_MEDIUM);
        VideoProfile reqestProfile = new VideoProfile(VideoProfile.STATE_AUDIO_ONLY, VideoProfile.QUALITY_MEDIUM);

        waitSessionOperationComplete();

        setDuringSessionRequest(true);
        mLastRequestVideoProfile = reqestProfile;
        nRequestPeerConfig(mId, ImsVTProviderUtil.packFromVdoProfile(reqestProfile));

        setMaCrashed(true);
    }

    public void waitSessionOperationComplete() {
        while (true == mDuringSessionRequestOperation || true == mDuringSessionRemoteRequestOperation) {
            try {
                Log.w(TAG, "Wait for Session operation complete!");
                Thread.sleep(500);
            } catch(InterruptedException ex) {
            }
        }
    }

    public void quitThread() {
        mProviderHandlerThread.quitSafely();
    }

    public VTSource getSource() {
        return mSource;
    }

    public void waitInitComplete() {
        while (false == mInitComplete && mMode != ImsVTProviderUtil.UI_MODE_DESTROY) {
            try {
                Log.w(TAG, "Wait for initialization complete!");
                Thread.sleep(500);
            } catch(InterruptedException ex) {
            }
        }
    }

    private static void updateDefaultId() {

        if (!ImsVTProviderUtil.recordContain(mDefaultId)) {
            if (ImsVTProviderUtil.recordSize() != 0) {
                mDefaultId = ImsVTProviderUtil.recordPopId();
                return;
            }
            mDefaultId = VT_PROVIDER_INVALIDE_ID;
        }
        return;
    }

    public static native int nInitRefVTP();
    public static native int nInitialization(int id);
    public static native int nFinalization(int id);
    public static native int nSetCamera(int id, int cam);
    public static native int nSetPreviewSurface(int id, Surface surface);
    public static native int nSetDisplaySurface(int id, Surface surface);
    public static native int nSetCameraParameters(int id, int major_sim_id, VTSource.Resolution[] cams_resolution);
    public static native int nSetDeviceOrientation(int id, int rotation);
    public static native int nSetUIMode(int id, int mode);
    public static native int nRequestPeerConfig(int id, String config);
    public static native int nResponseLocalConfig(int id, String config);
    public static native int nSnapshot(int id, int type, String uri);
    public static native int nStartRecording(int id, int type, String url, long maxSize);
    public static native int nStopRecording(int id);
    public static native int nSwitchFeature(int id, int feature, int on);

    public void onSetCamera(String cameraId) {
        mProviderHandler.obtainMessage(MSG_SET_CAMERA, cameraId).sendToTarget();
    }

    public void onSetPreviewSurface(Surface surface) {
        mProviderHandler.obtainMessage(MSG_SET_PREVIEW_SURFACE, surface).sendToTarget();
    }

    public void onSetDisplaySurface(Surface surface) {
        mProviderHandler.obtainMessage(MSG_SET_DISPLAY_SURFACE, surface).sendToTarget();
    }

    public void onSetDeviceOrientation(int rotation) {
        mProviderHandler.obtainMessage(MSG_SET_DEVICE_ORIENTATION, rotation, 0).sendToTarget();
    }

    public void onSetZoom(float value) {
        mProviderHandler.obtainMessage(MSG_SET_ZOOM, value).sendToTarget();
    }

    public void onSendSessionModifyRequest(VideoProfile fromProfile, VideoProfile toProfile) {
        SomeArgs args = SomeArgs.obtain();
        args.arg1 = fromProfile;
        args.arg2 = toProfile;
        mProviderHandler.obtainMessage(MSG_SEND_SESSION_MODIFY_REQUEST, args).sendToTarget();
    }

    public void onSendSessionModifyResponse(VideoProfile responseProfile) {
        mProviderHandler.obtainMessage(
                MSG_SEND_SESSION_MODIFY_RESPONSE, responseProfile).sendToTarget();
    }

    public void onRequestCameraCapabilities() {
        mProviderHandler.obtainMessage(MSG_REQUEST_CAMERA_CAPABILITIES).sendToTarget();
    }

    public void onRequestCallDataUsage() {
        mProviderHandler.obtainMessage(MSG_REQUEST_CALL_DATA_USAGE).sendToTarget();
    }

    public void onSetPauseImage(Uri uri) {
        mProviderHandler.obtainMessage(MSG_SET_PAUSE_IMAGE, uri).sendToTarget();
    }

    public void onSetUIMode(int mode) {
        mProviderHandler.obtainMessage(MSG_SET_UI_MODE, mode).sendToTarget();
    }

    public void onSwitchFeature(int feature, boolean on) {
        SomeArgs args = SomeArgs.obtain();
        args.arg1 = feature;
        args.arg2 = on;
        mProviderHandler.obtainMessage(MSG_SWITCH_FEATURE, args).sendToTarget();
    }

    public void setCameraInternal(String cameraId) {

        Log.d(TAG, "[ID=" + mId + "] [onSetCamera] id : " + cameraId);

        waitInitComplete();

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        int count = 0;

        if (cameraId == null) {
            mHasRequestCamCap = false;
        }

        if (mMode == ImsVTProviderUtil.UI_MODE_BG) {
            Log.w(TAG, "[ID=" + mId + "] [onSetCamera] onSetCamera, not set camera when in BG, only save id=" + cameraId);
        } else {

            if (cameraId != null) {

                while (true) {

                    if (mUplinkState == UPLINK_STATE_PAUSE_RECORDING) {

                        if (count < 10) {
                            Log.w(TAG, "[ID=" + mId + "] [onSetCamera] onSetCamera, Call hold or held, wait and retry");
                        } else {
                            // Set camera when call Hold or Held may cause error in future
                            // because it re-trigger MA connect camera and MA will fall in a wrong state.
                            // So we just skip when call hold & held
                            Log.w(TAG, "[ID=" + mId + "] [onSetCamera] onSetCamera, Call hold or held, ignore setCamera");
                            handleCallSessionEvent(SESSION_EVENT_ERROR_CAMERA_SET_IGNORED);
                            return;
                        }

                    } else if (mUplinkState == UPLINK_STATE_STOP_RECORDING && count > 0) {
                        Log.w(TAG, "[ID=" + mId + "] [onSetCamera] onSetCamera, recording stopped");
                        return;
                    } else {
                        //Release other call's camera to prevent surface is used by other camera
                        ImsVTProviderUtil.updateCameraUsage(mId);

                        //VTSource will auto restart preview and recording
                        mSource.open(cameraId);
                        mSource.showMe();
                        nSetCamera(mId, Integer.valueOf(cameraId));
                        break;
                    }

                    try {
                        Thread.sleep(200);
                    } catch(InterruptedException ex) {
                    }
                    count++;
                }

            } else {
                mSource.hideMe();
                mSource.close();
            }
        }
        mCameraId = cameraId;

        Log.d(TAG, "[ID=" + mId + "] [onSetCamera] Finish");

    }

    public void setPreviewSurfaceInternal(Surface surface) {

        Log.d(TAG, "[ID=" + mId + "] [onSetPreviewSurface] Start");
        Log.d(TAG, "[ID=" + mId + "] [onSetPreviewSurface] surface: " + surface);

        waitInitComplete();

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        mSource.setPreviewSurface(surface);

        nSetPreviewSurface(mId, surface);

        if(surface == null) {
            ImsVTProviderUtil.surfaceSet(mId, true, false);
        } else {
            ImsVTProviderUtil.surfaceSet(mId, true, true);
        }

        if (ImsVTProviderUtil.surfaceGet(mId) == 0) {
            ImsVTProvider vp = ImsVTProviderUtil.recordGet(mId);
            if (vp != null) {
                vp.handleCallSessionEvent(SESSION_EVENT_CALL_END);
            }
        }

        Log.d(TAG, "[ID=" + mId + "] [onSetPreviewSurface] Finish");

    }

    public void setDisplaySurfaceInternal(Surface surface) {

        Log.d(TAG, "[ID=" + mId + "] [onSetDisplaySurface] Start");
        Log.d(TAG, "[ID=" + mId + "] [onSetDisplaySurface] surface: " + surface);

        waitInitComplete();

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        nSetDisplaySurface(mId, surface);

        if(surface == null) {
            ImsVTProviderUtil.surfaceSet(mId, false, false);
        } else {
            ImsVTProviderUtil.surfaceSet(mId, false, true);
        }

        if (ImsVTProviderUtil.surfaceGet(mId) == 0) {
            ImsVTProvider vp = ImsVTProviderUtil.recordGet(mId);
            if (vp != null) {
                vp.handleCallSessionEvent(SESSION_EVENT_CALL_END);
            }
        }

        Log.d(TAG, "[ID=" + mId + "] [onSetDisplaySurface] Finish");

    }

    public void setDeviceOrientationInternal(int rotation) {

        Log.d(TAG, "[ID=" + mId + "] [onSetDeviceOrientation] Start");
        Log.d(TAG, "[ID=" + mId + "] [onSetDeviceOrientation] rotation: " + rotation);

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        waitInitComplete();

        if (mOrientation != rotation) {

            Log.d(TAG, "[ID=" + mId + "] [onSetDeviceOrientation] device orientation change from "
                    + mOrientation + " to " + rotation);
            mOrientation = rotation;

            mProviderHandler.removeCallbacks(mOrientationRunnable);
            // Wait 500 ms to prevent rotation frequently
            mProviderHandler.postDelayed(mOrientationRunnable,
                SystemProperties.getInt(MTK_VILTE_ROTATE_DELAY, 500));
        }

        Log.d(TAG, "[ID=" + mId + "] [onSetDeviceOrientation] Finish");

    }

    public void setZoomInternal(float value) {

        Log.d(TAG, "[ID=" + mId + "] [onSetZoom] Start");
        Log.d(TAG, "[ID=" + mId + "] [onSetZoom] value: " + value);

        waitInitComplete();

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        mSource.setZoom(value);

        Log.d(TAG, "[ID=" + mId + "] [onSetZoom] Finish");

    }

    public void sendSessionModifyRequestInternal(VideoProfile fromProfile, VideoProfile toProfile) {

        Log.d(TAG, "[ID=" + mId + "] [onSendSessionModifyRequest] Start");
        Log.d(TAG, "[ID=" + mId + "] [onSendSessionModifyResponse] fromProfile:" + fromProfile.toString());
        Log.d(TAG, "[ID=" + mId + "] [onSendSessionModifyResponse] toProfile:" + toProfile.toString());

        waitInitComplete();

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        // For pause case, it mean App switch FG & BG
        // we dont go the normal session midify flow
        int modifyFlag = (toProfile.getVideoState() ^ fromProfile.getVideoState());
        int targerPause = (VideoProfile.STATE_PAUSED & toProfile.getVideoState());

        if (VideoProfile.STATE_PAUSED == modifyFlag && VideoProfile.STATE_PAUSED  == targerPause) {

            onSetUIMode(ImsVTProviderUtil.UI_MODE_BG);
            return;

        } else if (VideoProfile.STATE_PAUSED == modifyFlag && 0 == targerPause) {

            onSetUIMode(ImsVTProviderUtil.UI_MODE_FG);
            return;
        }

        // we reject the request when MA is crashed
        if (getMaCrashed()) {

            Log.e(TAG, "onSendSessionModifyRequest : MA is Chreash, so reject upgrade directly!");

            if (VideoProfile.isTransmissionEnabled(toProfile.getVideoState()) ||
                VideoProfile.isReceptionEnabled(toProfile.getVideoState())) {

                receiveSessionModifyResponse(
                        Connection.VideoProvider.SESSION_MODIFY_REQUEST_FAIL,
                        fromProfile,
                        toProfile);

                return;
            }
        }

        boolean isCancelUpgrade = false;
        if (toProfile.getVideoState() == MtkVideoProfile.STATE_CANCEL_UPGRADE) {
            isCancelUpgrade = true;
        }

        // we reject the second request when during another request
        // but for cancel request, it is Ok to overlapping
        if (getDuringSessionRequest() && !isCancelUpgrade) {

            Log.e(TAG, "onSendSessionModifyRequest : Another operation is still not finish, so reject upgrade directly!");

            receiveSessionModifyResponse(
                    Connection.VideoProvider.SESSION_MODIFY_REQUEST_FAIL,
                    fromProfile,
                    toProfile);

            return;
        }

        // when VilTE switch is turn off, we should not send request or the rsp won't back
        if (ImsVTProviderUtil.isVideoCallOn() || VideoProfile.isAudioOnly(toProfile.getVideoState())) {

            setDuringSessionRequest(true);

            mLastRequestVideoProfile = toProfile;

            nRequestPeerConfig(mId, ImsVTProviderUtil.packFromVdoProfile(toProfile));

        } else {
            receiveSessionModifyResponse(
                    Connection.VideoProvider.SESSION_MODIFY_REQUEST_FAIL,
                    fromProfile,
                    toProfile);
        }

        Log.d(TAG, "[ID=" + mId + "] [onSendSessionModifyRequest] Finish");

    }

    public void sendSessionModifyResponseInternal(VideoProfile responseProfile) {

        Log.d(TAG, "[ID=" + mId + "] [onSendSessionModifyResponse] Start");
        Log.d(TAG, "[ID=" + mId + "] [onSendSessionModifyResponse] responseProfile:" + responseProfile.toString());

        waitInitComplete();

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        if (!ImsVTProviderUtil.isVideoCallOn()) {
            // Always response audio_only when ViLTE off
            VideoProfile audioResponseProfile = new VideoProfile(VideoProfile.STATE_AUDIO_ONLY, responseProfile.getQuality());
            nResponseLocalConfig(mId, ImsVTProviderUtil.packFromVdoProfile(audioResponseProfile));
        } else {
            nResponseLocalConfig(mId, ImsVTProviderUtil.packFromVdoProfile(responseProfile));
        }
        setDuringSessionRemoteRequest(false);

        Log.d(TAG, "[ID=" + mId + "] [onSendSessionModifyResponse] Finish");

    }

    public void requestCameraCapabilitiesInternal() {

        Log.d(TAG, "[ID=" + mId + "] [onRequestCameraCapabilities] Start");

        waitInitComplete();

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        CameraCharacteristics camera_cs = mSource.getCameraCharacteristics();
        if (null == camera_cs) {
            Log.e(TAG, "onRequestCameraCapabilities: camera_cs null!");
            return;
        }

        float zoom_max = camera_cs.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM);
        boolean zoom_support = (zoom_max > 1)? true:false;

        CameraCapabilities camCap =
            new CameraCapabilities(mPreviewSize.width, mPreviewSize.height, zoom_support, zoom_max);

        changeCameraCapabilities(camCap);

        mHasRequestCamCap = true;

        Log.d(TAG, "[ID=" + mId + "] [onRequestCameraCapabilities] Finish");

    }

    private boolean canRequestDataUsage() {
        Log.d(TAG, "[canRequestDataUsage]");

        boolean forceRequest = SystemProperties.get("persist.ims.data.simulate").equals("1");
        if (forceRequest) {
            return true;
        }

        return mNeedReportDataUsage;
    }

    public void requestCallDataUsageInternal() {

        Log.d(TAG, "[ID=" + mId + "] [onRequestCallDataUsage] Start");

        waitInitComplete();

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        if (!canRequestDataUsage()) {
            return;
        }

        // ====================================================================
        // get IMS/WIFI Interface and data from CONNECTIVITY SERVICE
        //
        String mActiveImsIface = "";
        String mActiveWifiIface = "";

        ConnectivityManager sConnMgr = (ConnectivityManager) ImsVTProviderUtil.mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        Network [] nets = sConnMgr.getAllNetworks();
        NetworkInfo nwInfo;

        if (null != nets) {

            for (Network net : nets) {

                nwInfo = sConnMgr.getNetworkInfo(net);

                if (nwInfo != null && nwInfo.isConnected() ) {

                    Log.d(TAG, "[onRequestCallDataUsage] nwInfo:" + nwInfo.toString());

                    NetworkCapabilities netCap = sConnMgr.getNetworkCapabilities(net);

                    if (null == netCap) {
                        Log.d(TAG, "[onRequestCallDataUsage] netCap = null");
                        return;
                    }

                    Log.d(TAG, "[onRequestCallDataUsage] checking net=" + net + " cap=" + netCap);

                    if (null != sConnMgr.getLinkProperties(net)) {
                        if (netCap.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                            mActiveWifiIface = sConnMgr.getLinkProperties(net).getInterfaceName();
                            Log.d(TAG, "[onRequestCallDataUsage] mActiveWifiIface=" + mActiveWifiIface);

                        } else if (netCap.hasCapability(NetworkCapabilities.NET_CAPABILITY_IMS)) {
                            mActiveImsIface = sConnMgr.getLinkProperties(net).getInterfaceName();
                            Log.d(TAG, "[onRequestCallDataUsage] mActiveImsIface=" + mActiveImsIface);

                        } else {
                            Log.d(TAG, "[onRequestCallDataUsage] netCap neither contain WIF nor LTE.");
                        }
                    } else {
                        Log.e(TAG, "[onRequestCallDataUsage] sConnMgr.getLinkProperties(net) = NULL");
                    }
                }
            }

            if ("" == mActiveImsIface) {
                Log.e(TAG, "[onRequestCallDataUsage] mActiveImsIface is empty");
                return;
            }

        } else {
            Log.d(TAG, "[onRequestCallDataUsage] getAllNetworks returns null.");
            return;
        }


        // ====================================================================


        // ====================================================================\
        // Calculate the usage of ViLTE IMS
        //
        IBinder b = ServiceManager.getService(Context.NETWORKMANAGEMENT_SERVICE);
        INetworkManagementService mNetworkManager = INetworkManagementService.Stub.asInterface(b);

        try {
            // should use UID_ALL = -1
            NetworkStats uidSnapshot = mNetworkManager.getNetworkStatsUidDetail(-1);

            int VILTE_UID = 1000;
            long usage_ImsTaginImsInterface = getTaggedSnapshot(uidSnapshot, VILTE_UID, mActiveImsIface, ImsVTProviderUtil.TAG_VILTE_MOBILE + mId);
            //long usage_WifiTaginImsInterface = getTaggedSnapshot(uidSnapshot, VILTE_UID, mActiveImsIface, ImsVTProviderUtil.TAG_VILTE_WIFI + mId);
            //long usage_WifiTaginWifiInterface = getTaggedSnapshot(uidSnapshot, VILTE_UID, mActiveWifiIface, ImsVTProviderUtil.TAG_VILTE_WIFI + mId);

            ImsVTProviderUtil.usageSet(mId, usage_ImsTaginImsInterface);

            changeCallDataUsage(usage_ImsTaginImsInterface - mInitialUsage);

        } catch (RemoteException e) {
            Log.d(TAG, "Exception:" + e);
            return;
        }
        // ====================================================================
        Log.d(TAG, "[ID=" + mId + "] [onRequestCallDataUsage] Finish");

    }

    // Example of network info file:
    //
    //     idx              2
    //     iface            ccmni1
    //     acct_tag_hex     0x0
    //     uid_tag_int      0
    //     cnt_set          0           // foreground or background
    //     rx_bytes         3085
    //     rx_packets       15
    //     tx_bytes         827
    //     tx_packets       15
    //     rx_tcp_bytes     366
    //     rx_tcp_packets   6
    //     rx_udp_bytes     2719
    //     rx_udp_packets   9
    //     rx_other_bytes   0
    //     rx_other_packets 0
    //     tx_tcp_bytes     252
    //     tx_tcp_packets   6
    //     tx_udp_bytes     575
    //     tx_udp_packets   9
    //     tx_other_bytes   0
    //     tx_other_packets 0

    private long getTaggedSnapshot(NetworkStats uidSnapshot, int match_uid, String iface, int tag) {

        Log.i(TAG, "getTaggedSnapshot match_uid:" + match_uid + ", iface:" + iface + ", tag:" + NetworkStats.tagToString(tag));

        long TotalBytes = 0;
        NetworkStats.Entry entry = null;

        for (int j = 0; j < uidSnapshot.size(); j++) {

            entry = uidSnapshot.getValues(j, entry);

            if (Objects.equals(entry.iface, iface) && entry.uid == match_uid && entry.tag == tag) {

                Log.i(TAG, "getTaggedSnapshot entry:" + entry.toString());

                TotalBytes += entry.rxBytes;
                TotalBytes += entry.txBytes;
                //Log.i(TAG, "getTaggedSnapshot entry.rxBytes:" + Long.toString(entry.rxBytes));
                //Log.i(TAG, "getTaggedSnapshot entry.txBytes:" + Long.toString(entry.txBytes));
            }
        }
        Log.i(TAG, "TotalBytes:" + Long.toString(TotalBytes));
        return TotalBytes;
    }
    //

    public void setPauseImageInternal(Uri uri) {
        Log.d(TAG, "[ID=" + mId + "] [onSetPauseImage] Start");
        Log.d(TAG, "[ID=" + mId + "] [onSetPauseImage] uri: " + uri);

        waitInitComplete();

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        mSource.setReplacePicture(uri);

        Log.d(TAG, "[ID=" + mId + "] [onSetPauseImage] Finish");

    }

    // MTK proprietary interface
    public void setUIModeInternal(int mode) {

        Log.d(TAG, "[ID=" + mId + "] [onSetUIMode] Start");
        Log.d(TAG, "[ID=" + mId + "] [onSetUIMode] mode: " + mode);

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        switch (mode) {
            case ImsVTProviderUtil.UI_MODE_FG:
                if (mCameraId != null) {
                     //Release other call's camera to prevent surface is used by other camera
                     ImsVTProviderUtil.updateCameraUsage(mId);

                     //VTSource will auto restart preview and recording
                     mSource.open(mCameraId);
                     mSource.showMe();
                     nSetCamera(mId, Integer.valueOf(mCameraId));
                } else {
                    mSource.hideMe();
                    mSource.close();
                }

                mMode = mode;
                nSetUIMode(mId, mode);

                notifyVideoPauseStateChange();
                break;

            case ImsVTProviderUtil.UI_MODE_BG:

                if (mIsDuringResetMode) {
                    Log.d(TAG, "[ID=" + mId + "] [onSetUIMode] reset mode (voice call) should not recv BG, skip");
                    break;
                }

                //Release camera when in BG
                mSource.hideMe();
                mSource.close();

                mMode = mode;
                nSetUIMode(mId, mode);

                notifyVideoPauseStateChange();
                break;

            // Full/normal screen just for performance, don't affect control flow
            case ImsVTProviderUtil.UI_MODE_FULL_SCREEN:
            case ImsVTProviderUtil.UI_MODE_NORMAL_SCREEN:
                nSetUIMode(mId, mode);
                break;

            case ImsVTProviderUtil.UI_MODE_RESET:

                if (mMode != ImsVTProviderUtil.UI_MODE_DESTROY) {
                    mMode = ImsVTProviderUtil.UI_MODE_FG;
                }

                mIsDuringResetMode = true;
                Thread resetModeRecoverThread = new Thread(new Runnable() {
                    public synchronized void run() {
                        Log.d(TAG, "[ID=" + mId + "] [onSetUIMode] resetModeRecoverThread start");

                        //Add delay to prevent IMS FWK reset mode earlier than InCallUI go to BG.
                        try {
                            Thread.sleep(600);
                            mIsDuringResetMode = false;
                        } catch(InterruptedException ex) {
                        }
                        Log.d(TAG, "[ID=" + mId + "] [onSetUIMode] resetModeRecoverThread finish");
                    }
                });
                resetModeRecoverThread.start();

                notifyVideoPauseStateChange();
                break;

            case ImsVTProviderUtil.UI_MODE_DESTROY:
                if (mMode != ImsVTProviderUtil.UI_MODE_DESTROY) {
                    mMode = mode;
                    nFinalization(mId);
                }
                break;

            default:
                break;
        }

        Log.d(TAG, "[ID=" + mId + "] [onSetUIMode] Finish");

    }

    public void switchFeatureInternal(int feature, boolean on) {

        Log.d(TAG, "[ID=" + mId + "] [switchFeature] Start");
        Log.d(TAG, "[ID=" + mId + "] [switchFeature] feature: " + feature + ", on: " + on);

        waitInitComplete();

        if (mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
            return;
        }

        if (on) {
            nSwitchFeature(mId, feature, 1);

        } else {

            // when ViLTE/ViWiFi is off
            // currnet un-finish request won't get response, so we send fail rsp to App insteaad
            if (mDuringSessionRequestOperation) {

                receiveSessionModifyResponse(
                        Connection.VideoProvider.SESSION_MODIFY_REQUEST_FAIL,
                        mLastRequestVideoProfile,
                        mLastRequestVideoProfile);
            }

            nSwitchFeature(mId, feature, 0);
        }

        Log.d(TAG, "[ID=" + mId + "] [switchFeature] Finish");

    }

    public void onError() {

        Log.d(TAG, "[ID=" + mId + "] [onError] Start");

        handleMaErrorProcess();
        handleCallSessionEvent(SESSION_EVENT_ERROR_CAMERA_CRASHED);

        Log.d(TAG, "[ID=" + mId + "] [onError] Finish");

    }

    public void addVideoProviderStateListener(VideoProviderStateListener listener) {
        mListeners.add(listener);
    }

    public void removeVideoProviderStateListener(VideoProviderStateListener listener) {
        mListeners.remove(listener);
    }

    public void notifyVideoPauseStateChange() {

        Log.d(TAG, "[ID=" + mId + "] [notifyVideoPauseStateChange] Start");

        boolean isVideoStatePause = false;

        if (ImsVTProviderUtil.UI_MODE_FG == mMode) {
            isVideoStatePause = false;
        } else if (ImsVTProviderUtil.UI_MODE_BG == mMode){
            isVideoStatePause = true;
        }

        for (VideoProviderStateListener listener : mListeners) {
            if (listener != null) {
                listener.onReceivePauseState(isVideoStatePause);
                Log.d(TAG, "[ID=" + mId + "] [notifyVideoPauseStateChange] isVideoStatePause: " + isVideoStatePause);
            }
        }

        Log.d(TAG, "[ID=" + mId + "] [notifyVideoPauseStateChange] Finish");

    }

    public static void postEventFromNative(
            int msg,
            int id,
            int arg1,
            int arg2,
            int arg3,
            Object obj1,
            Object obj2,
            Object obj3) {

        ImsVTProvider vp = ImsVTProviderUtil.recordGet(id);

        if (null == vp &&
            msg != SESSION_EVENT_ERROR_SERVER_DIED &&
            msg != SESSION_EVENT_GET_CAP &&
            id != POSTEVENT_IGNORE_ID) {
            Log.e(TAG, "Error: post event to Call is already release or has happen error before!");
            return;
        }

        Log.i(TAG, "[ID=" + id + "] [postEventFromNative]: " + msg);
        switch (msg) {
            case SESSION_EVENT_RECEIVE_FIRSTFRAME:
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_RECEIVE_FIRSTFRAME");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_SNAPSHOT_DONE:
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_SNAPSHOT_DONE");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_RECORDER_EVENT_INFO_UNKNOWN:
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_RECORDER_EVENT_INFO_UNKNOWN");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_RECORDER_EVENT_INFO_REACH_MAX_DURATION:
                Log.d(TAG, "postEventFromNative : msg = " +
                     "SESSION_EVENT_RECORDER_EVENT_INFO_REACH_MAX_DURATION");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_RECORDER_EVENT_INFO_REACH_MAX_FILESIZE:
                Log.d(TAG, "postEventFromNative : msg = " +
                     "SESSION_EVENT_RECORDER_EVENT_INFO_REACH_MAX_FILESIZE");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_RECORDER_EVENT_INFO_NO_I_FRAME:
                Log.d(TAG, "postEventFromNative : msg = " +
                    "SESSION_EVENT_RECORDER_EVENT_INFO_NO_I_FRAME");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_RECORDER_EVENT_INFO_COMPLETE:
                Log.d(TAG, "postEventFromNative : msg = " +
                    "SESSION_EVENT_RECORDER_EVENT_INFO_COMPLETE");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_CALL_END:
            case SESSION_EVENT_CALL_ABNORMAL_END:
                Log.d(TAG, "postEventFromNative : msg = " +
                      "SESSION_EVENT_CALL_END / SESSION_EVENT_CALL_ABNORMAL_END");

                // Call release to leave VTSource handler thread
                vp.getSource().release();

                ImsVTProviderUtil.recordRemove(id);
                updateDefaultId();

                vp.handleCallSessionEvent(msg);

                vp.mProviderHandlerThread.quitSafely();

                vp.mMode = ImsVTProviderUtil.UI_MODE_DESTROY;
                break;

            case SESSION_EVENT_START_COUNTER:
                Log.d(TAG, "postEventFromNative : msg = MSG_START_COUNTER");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_PEER_CAMERA_OPEN:
                Log.d(TAG, "postEventFromNative : msg = MSG_PEER_CAMERA_OPEN");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_PEER_CAMERA_CLOSE:
                Log.d(TAG, "postEventFromNative : msg = MSG_PEER_CAMERA_CLOSE");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_LOCAL_BW_READY_IND:
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_LOCAL_BW_READY_IND");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_RECV_SESSION_CONFIG_REQ: {
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_RECV_SESSION_CONFIG_REQ");

                VideoProfile requestProfile = ImsVTProviderUtil.unPackToVdoProfile((String) obj1);

                vp.setDuringSessionRemoteRequest(true);

                vp.receiveSessionModifyRequest(requestProfile);

                // for the not upgrade case, we rsp directly
                if (!VideoProfile.isBidirectional(requestProfile.getVideoState())) {

                    Log.d(TAG, "Do onSendSessionModifyResponse directly for not upgrade case");

                    vp.onSendSessionModifyResponse(requestProfile);
                }

                break;
            }

            case SESSION_EVENT_RECV_CANCEL_SESSION_IND:
                Log.d(TAG, "postEventFromNative : msg = " +
                    "SESSION_EVENT_RECV_SESSION_CONFIG_REQ / SESSION_EVENT_RECV_CANCEL_SESSION_IND");

                VideoProfile IndicationProfile = ImsVTProviderUtil.unPackToVdoProfile((String) obj1);

                vp.receiveSessionModifyRequest(IndicationProfile);

                break;

            case SESSION_EVENT_RECV_SESSION_CONFIG_RSP: {
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_RECV_SESSION_CONFIG_RSP");

                if (vp.mMode == ImsVTProviderUtil.UI_MODE_DESTROY) {
                    Log.d(TAG, "call end, ignore response");
                    return;
                }

                //Need retry downgrade when MA crash
                if (vp.getMaCrashed() && SESSION_MODIFY_REJECTBYREMOTE == arg1) {
                    try {
                        Thread.sleep(500);
                    } catch(InterruptedException ex) {
                    }

                    VideoProfile vp_req = ImsVTProviderUtil.unPackToVdoProfile((String) obj1);
                    if (vp_req.getVideoState() == VideoProfile.STATE_AUDIO_ONLY) {
                        vp.mLastRequestVideoProfile = vp_req;
                        nRequestPeerConfig(id, ImsVTProviderUtil.packFromVdoProfile(vp_req));
                        return;
                    }
                }

                // Need resend session modify when call is not active until call become active
                if (SESSION_MODIFY_NOACTIVESTATE == arg1) {
                    try {
                        Thread.sleep(500);
                    } catch(InterruptedException ex) {
                    }
                    VideoProfile retryProfile = ImsVTProviderUtil.unPackToVdoProfile((String) obj1);
                    vp.mLastRequestVideoProfile = retryProfile;
                    nRequestPeerConfig(id, ImsVTProviderUtil.packFromVdoProfile(retryProfile));

                } else {

                    VideoProfile respounseProfile = ImsVTProviderUtil.unPackToVdoProfile((String) obj2);
                    int state = Connection.VideoProvider.SESSION_MODIFY_REQUEST_FAIL;

                    if (respounseProfile.getVideoState() == MtkVideoProfile.STATE_CANCEL_UPGRADE) {
                        state = ConnectionEx.VideoProvider.SESSION_MODIFY_CANCEL_UPGRADE_FAIL;
                        switch(arg1) {
                            case SESSION_MODIFY_CANCEL_OK:
                                state = Connection.VideoProvider.SESSION_MODIFY_REQUEST_SUCCESS;
                                break;
                            case SESSION_MODIFY_CANCEL_FAILED_DOWNGRADE:
                                state = ConnectionEx.VideoProvider.
                                        SESSION_MODIFY_CANCEL_UPGRADE_FAIL_AUTO_DOWNGRADE;
                                break;
                            case SESSION_MODIFY_CANCEL_FAILED_REMOTE:
                                state = ConnectionEx.VideoProvider.
                                        SESSION_MODIFY_CANCEL_UPGRADE_FAIL_REMOTE_REJECT_UPGRADE;
                                break;
                            default:
                                state = ConnectionEx.VideoProvider.
                                        SESSION_MODIFY_CANCEL_UPGRADE_FAIL;
                                break;
                        }
                    } else {
                        switch (arg1) {
                            case SESSION_MODIFY_OK:
                                state = Connection.VideoProvider.SESSION_MODIFY_REQUEST_SUCCESS;
                                break;
                            case SESSION_MODIFY_NONEED: {

                                VideoProfile requestProfile = ImsVTProviderUtil.unPackToVdoProfile((String) obj1);

                                // Retry to downgrade when ViLTE is off and downgrade fail
                                // In some case, upgrade is not finish will cause NONEED fail.
                                if (!ImsVTProviderUtil.isVideoCallOn() && VideoProfile.isAudioOnly(requestProfile.getVideoState())) {
                                    vp.mLastRequestVideoProfile = requestProfile;
                                    nRequestPeerConfig(id, ImsVTProviderUtil.packFromVdoProfile(requestProfile));
                                    return;
                                } else {
                                    state = Connection.VideoProvider.SESSION_MODIFY_REQUEST_INVALID;
                                }

                                break;
                            }
                            case SESSION_MODIFY_NOACTIVESTATE:
                            case SESSION_MODIFY_INVALIDPARA:
                                state = Connection.VideoProvider.SESSION_MODIFY_REQUEST_INVALID;
                                break;
                            case SESSION_MODIFY_REQTIMEOUT:
                                state = Connection.VideoProvider.SESSION_MODIFY_REQUEST_TIMED_OUT;
                                break;
                            case SESSION_MODIFY_REJECTBYREMOTE:
                                state = Connection.VideoProvider.SESSION_MODIFY_REQUEST_REJECTED_BY_REMOTE;
                                break;
                            default:
                                state = Connection.VideoProvider.SESSION_MODIFY_REQUEST_FAIL;
                                break;
                        }
                    }

                    vp.receiveSessionModifyResponse(
                            state,
                            ImsVTProviderUtil.unPackToVdoProfile((String) obj1),
                            ImsVTProviderUtil.unPackToVdoProfile((String) obj2));

                    vp.setDuringSessionRequest(false);
                }

                break;
            }

            case SESSION_EVENT_HANDLE_CALL_SESSION_EVT:
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_HANDLE_CALL_SESSION_EVT");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_PEER_SIZE_CHANGED:
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_PEER_SIZE_CHANGED");
                vp.changePeerDimensions(arg1, arg2);
                break;

            case SESSION_EVENT_LOCAL_SIZE_CHANGED: {
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_LOCAL_SIZE_CHANGED");

                if(vp.mPreviewSize.width == arg1 && vp.mPreviewSize.height == arg2) {
                    Log.d(TAG, "local size no change => Do not notify!");
                    break;
                }

                vp.mPreviewSize.width = arg1;
                vp.mPreviewSize.height = arg2;
                Log.d(TAG, "Update preview size, w=" + vp.mPreviewSize.width + ", h=" + vp.mPreviewSize.height);

                // Need update to App after first request
                if(true == vp.mHasRequestCamCap) {
                    //Recreate recording session for size changed
                    vp.getSource().stopRecording();
                    vp.getSource().startRecording();

                    vp.onRequestCameraCapabilities();
                } else {
                    Log.d(TAG, "Not request yet, just only update default w/h");
                }
                break;
            }

            case SESSION_EVENT_DATA_USAGE_CHANGED:
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_DATA_USAGE_CHANGED");

                // native module will post value when RTP Info is ON
                // the value mean the data loss rate and will multiply by -1 (i.e. arg1 < 0 )
                // we pass to App only when the switch is turn on, or just skip
                boolean RTP_DBG_ON = SystemProperties.get("persist.radio.vilte_RTPInfo").equals("1") ? true : false;
                if (RTP_DBG_ON) {
                    vp.changeCallDataUsage(arg1);
                }
                break;

            case SESSION_EVENT_BAD_DATA_BITRATE:
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_BAD_DATA_BITRATE");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_GET_CAP:
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_GET_CAP");

                VTSource.Resolution [] cams_info = VTSource.getAllCameraResolutions();
                if(null == cams_info) {
                    Log.e(TAG, "Error: sensor resolution = NULL");
                }

                int major_sim_id = 0;
                // Not MIMS case, set major sim id to get correct codec capability
                if (SystemProperties.getInt(ImsVTProviderUtil.MULTI_IMS_SUPPORT, 1) <= 1) {
                    IMtkTelephonyEx iTelEx = IMtkTelephonyEx.Stub.asInterface(
                        ServiceManager.getService("phoneEx"));
                    if (iTelEx != null) {
                        try {
                            while (iTelEx.isCapabilitySwitching()) {
                                try {
                                    Thread.sleep(200);
                                } catch(InterruptedException ex) {
                                }
                            }
                        } catch (RemoteException e) {
                            Log.d(TAG, "Exception:" + e);
                        }

                        major_sim_id = RadioCapabilitySwitchUtil.getMainCapabilityPhoneId();
                    } else {
                        Log.e(TAG, "iTelEx = NULL, cannot get phoneEx");
                    }
                } else {
                    major_sim_id = id;
                }
                nSetCameraParameters(id, major_sim_id, cams_info);

                break;

            case SESSION_EVENT_LOCAL_BUFFER:
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_LOCAL_BUFFER");

                vp.getSource().stopRecording();
                vp.getSource().setRecordSurface((Surface)obj3);
                vp.getSource().startRecording();
                vp.mPauseCount = 0;
                break;

            case SESSION_EVENT_UPLINK_STATE_CHANGE:
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_UPLINK_STATE_CHANGE");
                vp.mUplinkState = arg1;

                switch (arg1) {
                    case UPLINK_STATE_STOP_RECORDING:
                        vp.getSource().stopRecording();
                        vp.getSource().setRecordSurface(null);
                        break;

                    case UPLINK_STATE_START_RECORDING:
                        break;

                    case UPLINK_STATE_PAUSE_RECORDING:
                        if (arg2 == MODE_PAUSE_BY_HOLD) {
                            if (vp.mPauseCount == 0) {
                                vp.getSource().stopRecording();
                            }

                            vp.mPauseCount++;
                        }
                        break;

                    case UPLINK_STATE_RESUME_RECORDING:
                        if (arg2 == MODE_PAUSE_BY_HOLD) {
                            vp.mPauseCount--;

                            //Start recording after all hold/held resume
                            if (vp.mPauseCount == 0) {
                                vp.getSource().startRecording();
                            }
                        }
                        break;

                    default:
                        break;

                }
                break;

            case SESSION_EVENT_DEFAULT_LOCAL_SIZE:
                Log.d(TAG, "postEventFromNative : msg = SESSION_EVENT_DEFAULT_LOCAL_SIZE, W=" + arg1 + ", H=" + arg2);
                vp.mPreviewSize.width = arg1;
                vp.mPreviewSize.height = arg2;
                break;

            case SESSION_EVENT_ERROR_SERVICE:
                Log.d(TAG, "postEventFromNative : msg = MSG_ERROR_SERVICE");

                // Call release to leave VTSource handler thread
                vp.getSource().release();

                ImsVTProviderUtil.recordRemove(id);
                updateDefaultId();

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_ERROR_SERVER_DIED:
                Log.d(TAG, "postEventFromNative : msg = MSG_ERROR_SERVER_DIED");
                ImsVTProviderUtil.quitAllThread();
                ImsVTProviderUtil.recordRemoveAll();
                updateDefaultId();

                // because the event may happen when no call exist
                // need to check firstly
                if (vp != null) {
                    // Call release to leave VTSource handler thread
                    vp.getSource().release();

                    vp.handleCallSessionEvent(msg);
                    vp.mProviderHandlerThread.quitSafely();
                }
                break;

            case SESSION_EVENT_ERROR_CAMERA_CRASHED:
                Log.d(TAG, "postEventFromNative : msg = MSG_ERROR_CAMERA_CRASHED");

                vp.handleMaErrorProcess();
                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_ERROR_CODEC:
                Log.d(TAG, "postEventFromNative : msg = MSG_ERROR_CODEC");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_ERROR_REC:
                Log.d(TAG, "postEventFromNative : msg = MSG_ERROR_REC");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_ERROR_CAMERA_SET_IGNORED:
                Log.d(TAG, "postEventFromNative : msg = MSG_ERROR_CAMERA_SET_IGNORED");

                vp.handleCallSessionEvent(msg);
                break;

            case SESSION_EVENT_DATA_PATH_PAUSE:
            case SESSION_EVENT_DATA_PATH_RESUME:
                Log.d(TAG, "postEventFromNative : msg = EVENT_DATA_PATH_CHANGED");

                vp.handleCallSessionEvent(msg);
                break;

            default:
                Log.d(TAG, "postEventFromNative : msg = UNKNOWB");
                break;
        }
    }
}
