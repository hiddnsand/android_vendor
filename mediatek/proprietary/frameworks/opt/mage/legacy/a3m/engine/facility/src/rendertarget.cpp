/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * RenderTarget Implementaion
 *
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/

#include <a3m/rendertarget.h>  /* This class's API                           */
#include <GLES2/gl2.h>          /* for glGenFramebuffers etc.                 */
#include <error.h>         /* for CHECK_GL_ERROR                         */
#include <a3m/log.h>            /* A3M log API                                */

namespace
{
}

namespace a3m
{
  /*
   * RenderTarget constructor
   */
  RenderTarget::RenderTarget( Texture2D::Ptr const& colourTexture,
                              Texture2D::Ptr const& depthTexture,
                              A3M_BOOL depthBuffer,
                              A3M_BOOL stencilBuffer )
    : m_framebuffer( 0 ),
      m_colourTexture( colourTexture ),
      m_depthTexture( depthTexture ),
      m_depthRenderbuffer( 0 ),
      m_stencilRenderbuffer( 0 ),
      m_valid( A3M_FALSE )
  {
    glGenFramebuffers( 1, &m_framebuffer );

    if( m_framebuffer == 0 )
    {
      A3M_LOG_ERROR( "Failed to create framebuffer", 0 );
      return;
    }

    glBindFramebuffer( GL_FRAMEBUFFER, m_framebuffer );
    CHECK_GL_ERROR;

    GLsizei width = 0, height = 0;

    // Bind the colour texture to the framebuffer.
    if( colourTexture && colourTexture->m_resource->getId() )
    {
      glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                              GL_TEXTURE_2D,
                              colourTexture->m_resource->getId(), 0 );
      width  = static_cast<GLsizei>(colourTexture->m_width);
      height = static_cast<GLsizei>(colourTexture->m_height);
      CHECK_GL_ERROR;
    }

    // Bind the depth texture to the framebuffer.
    if( depthTexture && depthTexture->m_resource->getId() )
    {
      glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                              GL_TEXTURE_2D,
                              depthTexture->m_resource->getId(), 0 );
      CHECK_GL_ERROR;
      if( width == 0 && height == 0 )
      {
        width  = static_cast<GLsizei>(depthTexture->m_width);
        height = static_cast<GLsizei>(depthTexture->m_height);
      }
      else if( ( width  != static_cast<GLsizei>( depthTexture->m_width ) ) ||
               ( height != static_cast<GLsizei>( depthTexture->m_height ) ) )
      {
        A3M_LOG_ERROR ("Colour and depth textures different sizes", 0 );
      }
    }
    else if( depthBuffer ) //Create a depth buffer
    {
      glGenRenderbuffers( 1, &m_depthRenderbuffer );
      if( m_depthRenderbuffer == 0 )
      {
        A3M_LOG_ERROR ("Failed to create depth renderbuffer", 0 );
      }
      else
      {
        glBindRenderbuffer( GL_RENDERBUFFER, m_depthRenderbuffer );
        CHECK_GL_ERROR;

        glRenderbufferStorage( GL_RENDERBUFFER, GL_DEPTH_COMPONENT16,
                               width, height );
        CHECK_GL_ERROR;

        glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                                   GL_RENDERBUFFER, m_depthRenderbuffer );
        CHECK_GL_ERROR;
      }
    }

    // Create a stencil buffer if specified.
    if( stencilBuffer )
    {
      glGenRenderbuffers( 1, &m_stencilRenderbuffer );
      if( m_stencilRenderbuffer == 0 )
      {
        A3M_LOG_ERROR ("Failed to create stencil renderbuffer", 0 );
      }
      else
      {
        glBindRenderbuffer( GL_RENDERBUFFER, m_stencilRenderbuffer );
        CHECK_GL_ERROR;

        glRenderbufferStorage( GL_RENDERBUFFER, GL_STENCIL_INDEX8,
                               width, height );
        CHECK_GL_ERROR;

        glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT,
                                   GL_RENDERBUFFER, m_stencilRenderbuffer );
        CHECK_GL_ERROR;
      }
    }

    // Check the status of the framebuffer and log any errors
    GLenum status = glCheckFramebufferStatus( GL_FRAMEBUFFER );
    switch( status )
    {
    case GL_FRAMEBUFFER_COMPLETE:
      m_valid = A3M_TRUE;
      break;
    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
      A3M_LOG_ERROR ("Framebuffer attachment points not complete.", 0 );
      break;
    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
      A3M_LOG_ERROR ("No valid attachments in framebuffer.", 0 );
      break;
    case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
      A3M_LOG_ERROR ("Framebuffer attachments do not have same "
                     "width and height.", 0 );
      break;
    case GL_FRAMEBUFFER_UNSUPPORTED:
      A3M_LOG_ERROR ("Combination of internal formats used by framebuffer"
                     " attachments is not renderable.", 0 );
      break;
    default:
      A3M_LOG_ERROR ("Error: Unknown status code: %d", status );
      break;
    }

    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
  }

  /*
   * Destructor
   */
  RenderTarget::~RenderTarget()
  {
    if( m_stencilRenderbuffer )
    {
      glDeleteRenderbuffers( 1, &m_stencilRenderbuffer );
      CHECK_GL_ERROR;
    }
    if( m_depthRenderbuffer )
    {
      glDeleteRenderbuffers( 1, &m_depthRenderbuffer );
      CHECK_GL_ERROR;
    }
    if( m_framebuffer )
    {
      glDeleteFramebuffers( 1, &m_framebuffer );
      CHECK_GL_ERROR;
    }
  }

  /*
   * Set new colour texture
   */
  void RenderTarget::setColourTexture( Texture2D::Ptr const& texture )
  {
    m_colourTexture = texture;

    glBindFramebuffer( GL_FRAMEBUFFER, m_framebuffer );
    CHECK_GL_ERROR;

    // Bind the colour texture to the framebuffer.
    if( texture )
    {
      glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                              GL_TEXTURE_2D,
                              texture->m_resource->getId(), 0 );
      CHECK_GL_ERROR;
    }
  }

  /*
   * Set new depth texture
   */
  void RenderTarget::setDepthTexture( Texture2D::Ptr const& texture )
  {
    m_depthTexture = texture;

    glBindFramebuffer( GL_FRAMEBUFFER, m_framebuffer );
    CHECK_GL_ERROR;

    // Bind the depth texture to the framebuffer.
    if( texture )
    {
      glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                              GL_TEXTURE_2D,
                              texture->m_resource->getId(), 0 );
      CHECK_GL_ERROR;
    }

    // Delete the depth render buffer if there is one (it is no longer being
    // used).
    if( m_depthRenderbuffer )
    {
      glDeleteRenderbuffers( 1, &m_depthRenderbuffer );
      CHECK_GL_ERROR;
      m_depthRenderbuffer = 0;
    }
  }

  /*
   * Enable this render target
   */
  void RenderTarget::enable()
  {
    glBindFramebuffer( GL_FRAMEBUFFER, m_framebuffer );
    CHECK_GL_ERROR;
  }

  /*
   * Disable this render target
   */
  void RenderTarget::disable() const
  {
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    CHECK_GL_ERROR;
  }

} /* end of namespace */
