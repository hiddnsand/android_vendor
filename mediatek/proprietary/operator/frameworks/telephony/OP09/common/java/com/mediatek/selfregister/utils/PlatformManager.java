package com.mediatek.selfregister.utils;

import android.app.AlarmManager;
import android.app.AlarmManager.OnAlarmListener;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UserManager;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.mediatek.custom.CustomProperties;
import com.mediatek.selfregister.Const;
import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PlatformManager {

    private static final String TAG = Const.TAG_PREFIX + "PlatformManager";

    private static final String VALUE_DEFAULT_MANUFACTURER = "MTK";
    private static final String VALUE_DEFALUT_SOFTWARE_VERSION = "N0.P1";

    private TelephonyWrapper mTelephonyWrapper;
    private ConnectivityWrapper mConnectivityWrapper;

    public PlatformManager(Context context) {
        mTelephonyWrapper = new TelephonyWrapper(context);
        mConnectivityWrapper = new ConnectivityWrapper(context);
    }

    public static boolean isFeatureSupported() {
        int property = SystemProperties.getInt("ro.mtk_ct4greg_app", -1);
        if (property != 1) {
            Log.i(TAG, "[isFeatureSupported] not support");
            return false;
        }
        return true;
    }

    public static String encryptMessage(int value) {
        return encryptMessage("" + value);
    }

    public static String encryptMessage(String text) {
        String result = text;
        if (Build.TYPE.equals(Build.USER) && text != null && text.length() > 0) {
            try {
                MessageDigest digest = MessageDigest.getInstance("MD5");
                digest.update(text.getBytes());
                result = Utils.bytesToHexString(digest.digest());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private boolean isInfoValid(String info, int length) {
        return (info != null && info.length() == length);
    }

    public static int getDefaultDataSubId() {
        return SubscriptionManager.getDefaultDataSubscriptionId();
    }

    public static int getDefaultSim() {
        int subId = getDefaultDataSubId();
        int slotId = SubscriptionManager.getSlotIndex(subId);
        Log.i(TAG, "getDefaultSim " + slotId);
        return slotId;
    }

    /*
     * Methods related to subId
     */
    public static int[] getSubId(int slotId) {
        return SubscriptionManager.getSubId(slotId);
    }

    public static boolean isSubIdsValid(int[] subId) {
        if (subId == null || subId[0] < 0) {
            if (subId == null) {
                Log.e(TAG, "subId is " + subId);
            } else {
                Log.e(TAG, "subId not valid, subId[0] is " + subId[0]);
            }
            return false;
        }
        return true;
    }

    /*
     * Get complex IMSI (For master,slave slot, get CDMA/LTE IMSI;)
     */
    public String[] getComplexImsi(int slotId) {
        String imsiArray[] = {Const.VALUE_EMPTY, Const.VALUE_EMPTY};

        int[] subId = getSubId(slotId);
        if (isSubIdsValid(subId)) {
            if (isCdma4GCard(slotId)) {
                // 4G CT, set both
                imsiArray[0] = getCdmaImsiForCT(slotId);
                imsiArray[1] = getSubscriberId(subId[0]);
            } else if (isCdma3GCard(slotId)) {
                // 3G CT, set CDMA IMSI
                imsiArray[0] = getCdmaImsiForCT(slotId);
            } else {
                // set LTE IMSI
                imsiArray[1] = getSubscriberId(subId[0]);
            }
        }

        Log.i(TAG, "[getComplexImsi] Slot " + slotId + " CDMA/LTE IMSI"
                + encryptMessage(imsiArray[0]) + "/" + encryptMessage(imsiArray[1]));
        return imsiArray;
    }

    //------------------------------------------------------
    // Wrapper for Connectivity
    //------------------------------------------------------
    public boolean hasNetworkConnection(int type) {
        return mConnectivityWrapper.hasNetworkConnection(type);
    }

    //------------------------------------------------------
    // Wrapper for Telephony
    //------------------------------------------------------
    public boolean areSlotsInfoReady(int[] slotArray) {
        return mTelephonyWrapper.areSlotsInfoReady(slotArray);
    }

    public CellLocation getCellLocation(int slotId) {
        return mTelephonyWrapper.getCellLocation(slotId);
    }

    public String getDeviceMeid(int[] slotIdArray) {
        return mTelephonyWrapper.getDeviceMeid(slotIdArray);
    }

    public String getImei(int slotId) {
        return mTelephonyWrapper.getImei(slotId);
    }

    public int getSimSwitchSlot() {
        return mTelephonyWrapper.getSimSwitchSlot();
    }

    public boolean hasIccCard(int slotId) {
        return mTelephonyWrapper.hasIccCard(slotId);
    }

    public boolean isNetworkRoaming(int slotId) {
        return mTelephonyWrapper.isNetworkRoaming(slotId);
    }

    public boolean isSingleLoad() {
        return mTelephonyWrapper.isSingleLoad();
    }

    public boolean isValidUim(int slotId) {
        return mTelephonyWrapper.isValidUim(slotId);
    }

    public boolean supportCTForAllSlots() {
        return mTelephonyWrapper.supportCTForAllSlots();
    }

    public void registerPhoneListener(PhoneStateListener listener) {
        if (listener != null) {
            mTelephonyWrapper.registerPhoneListener(listener);
        } else {
            Log.i(TAG, "[register] listener is null, do nothing");
        }
    }

    public void unRegisterPhoneListener(PhoneStateListener listener) {
        if (listener != null) {
            mTelephonyWrapper.unRegisterPhoneListener(listener);
        } else {
            Log.i(TAG, "[unRegister] listener is null, do nothing");
        }
    }

    //------------------------------------------------------
    // Wrapper for Telephony IMSI
    //------------------------------------------------------

    public String getCdmaImsiForCT(int slotId) {
        return mTelephonyWrapper.getCDMAImsi(slotId);
    }

    private String getImsiInfo(int slotId) {
        return mTelephonyWrapper.getImsi(slotId);
    }

    public String getSubscriberId(int subId) {
        return mTelephonyWrapper.getSubscriberId(subId);
    }

    public String getUimSubscriberId(int subId) {
        return mTelephonyWrapper.getUimSubscriberId(subId);
    }

    //------------------------------------------------------
    // Logical process for IMSI
    //------------------------------------------------------

    private boolean isCdma4GCard(int slotId) {
        return mTelephonyWrapper.isCdma4GCard(slotId);
    }

    private boolean isCdma3GCard(int slotId) {
        return mTelephonyWrapper.isCdma3GCard(slotId);
    }

    private String[] getSupportCardType(int slotId) {
        return mTelephonyWrapper.getSupportCardType(slotId);
    }

    /*
     * If it's for IccCard, return 1; for UiccCard, return 2
     */
    public int getSimType(int slotId) {
        String[] cardType = getSupportCardType(slotId);
        if (Arrays.asList(cardType).contains("USIM")) {
            Log.i(TAG, "Slot " + slotId + " is Uicc");
            return 2;
        } else {
            Log.i(TAG, "Slot " + slotId + " not Uicc");
            return 1;
        }
    }

    public String getProcessedIccid(String iccid) {
        if (!isInfoValid(iccid, TelephonyWrapper.LENGTH_ICCID)) {
            iccid = TelephonyWrapper.VALUE_DEFAULT_ICCID;
        }
        return iccid;
    }

    public String getIccId(int slotId) {
        return mTelephonyWrapper.getIccId(slotId);
    }

    //------------------------------------------------------
    // Function of Alarm and device related
    //------------------------------------------------------

    public static void setRtcAlarm(Context context, OnAlarmListener listener, int second) {
        setAlarm(context, listener, second, AlarmManager.RTC_WAKEUP);
    }

    public static void setElapsedAlarm(Context context, OnAlarmListener listener, int second) {
        setAlarm(context, listener, second, AlarmManager.ELAPSED_REALTIME_WAKEUP);
    }

    public static void setAlarm(Context context, OnAlarmListener listener, int second,
            int alarmType) {
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        long triggerAtMillis = System.currentTimeMillis() + second * 1000;
        if (alarmType != AlarmManager.RTC_WAKEUP) {
            triggerAtMillis = SystemClock.elapsedRealtime() + second * 1000;
        }
        alarm.setExact(alarmType, triggerAtMillis, TAG, listener, null);
    }

    public static void cancelAlarm(Context context, OnAlarmListener listener) {
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(listener);
    }

    public static String getHardwareVersion() {
        long systemSize = getDirectorySize(Environment.getRootDirectory());
        long dataSize = getDirectorySize(Environment.getDataDirectory());

        Log.d(TAG, "systemSize:" + systemSize + "\n" + "dataSize:" + dataSize);

        long totalSize = analyseTotalStorage(systemSize + dataSize);

        return totalSize + "G";
    }

    /**
     * If the size locate in the section of 8-16, returns 16;
     * If the size locate in the section of 16-32, returns 32;
     * And so on.
     * @param size
     * @return Integer like "16", "32", "64", Unit: G.
     */
    private static int analyseTotalStorage(long size) {
        double total = ((double) size) / (1024 * 1024 * 1024);
        Log.d(TAG, "analyseTotalStorage(), total: " + total);

        int storageSize = 1;
        while (total > storageSize) {
            storageSize = storageSize << 1;
        }

        return storageSize;
    }

    @SuppressWarnings("deprecation")
    private static long getDirectorySize(File path) {
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return blockSize * totalBlocks;
    }

    //------------------------------------------------------
    // Function Related to shared preferences
    //------------------------------------------------------
    public static void showVolteState(Context context) {
        String state = "";
        String[] path = {"volte_vt_enabled", "volte_vt_enabled_sim2"};

        for (String item: Const.VOLTE_PATH) {
            state += Settings.Global.getInt(context.getContentResolver(), item, 0);
        }
        Log.i(TAG, "Volte state is " + state);
    }

    // ------------------------------------------------------
    //     CustomProperties
    // ------------------------------------------------------

    public static String getManufacturer() {
        String manufacturer = CustomProperties.getString(CustomProperties.MODULE_DM,
                CustomProperties.MANUFACTURER, VALUE_DEFAULT_MANUFACTURER);
        return manufacturer;
    }

    public static String getSoftwareVersion() {
        String version = CustomProperties.getString(CustomProperties.MODULE_DM,
                "SoftwareVersion", VALUE_DEFALUT_SOFTWARE_VERSION);
        return version;
    }

    private class ConnectivityWrapper {

        private static final String TAG = Const.TAG_PREFIX + "ConnectivityWrapper";

        private ConnectivityManager mConnectivityManager;
        private Context mContext;

        public ConnectivityWrapper(Context context) {
            mConnectivityManager = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        }

        public boolean hasNetworkConnection(int type) {
            if (mConnectivityManager == null) {
                mConnectivityManager = (ConnectivityManager) mContext.getSystemService(
                    Context.CONNECTIVITY_SERVICE);
                Log.e(TAG, "mConnectivityManager is null, reinit");
            }

            Network[] networks = mConnectivityManager.getAllNetworks();
            for (Network network: networks) {
                NetworkInfo networkInfo = mConnectivityManager.getNetworkInfo(network);

                if (networkInfo.getType() == type && networkInfo.isConnected()) {
                    Log.i(TAG, "[hasNetworkConnection] " + type + " is connected");
                    return true;
                }
            }
            return false;
        }
    }

    private class TelephonyWrapper {

        private static final String TAG = Const.TAG_PREFIX + "TelephonyWrapper";

        private static final String VALUE_DEFAULT_MEID = "A0000100001000";
        private static final String VALUE_DEFAULT_IMEI = "860001000010000";
        public static final String VALUE_DEFAULT_IMSI = "000000000000000";
        public static final String VALUE_DEFAULT_ICCID = "00000000000000000000";

        private static final int LENGTH_MEID = 14;
        private static final int LENGTH_IMEI = 15;
        public static final int LENGTH_ICCID = 20;
        public static final int LENGTH_IMSI = 15;
        private static final int LENGTH_OPERATOR = 5;

        private static final String OPERATOR_CT_4G = "46011";
        private static final String OPERATOR_CT_MAC = "45502";
        private static final String OPERATOR_CT = "46003";

        private TelephonyManager mTelephonyManager;
        private MtkTelephonyManagerEx mMtkTelephonyManagerEx;

        private String[] PROPERTY_ICCID_SIM = {
            "ril.iccid.sim1",
            "ril.iccid.sim2",
            "ril.iccid.sim3",
            "ril.iccid.sim4",
            };

        public TelephonyWrapper(Context context) {
            mTelephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
            mMtkTelephonyManagerEx = new MtkTelephonyManagerEx(context);
            if (mTelephonyManager == null) {
                throw new Error("telephony manager is null");
            }
        }


        // ------------------------------------------------------
        //    Wrapper for SystemProperties
        // ------------------------------------------------------

        /*
         * if OP09_SPEC0212_SEGDEFAULT, CT card could only in slot0;
         * CT card could in two slots.
         */
        public boolean supportCTForAllSlots() {
            String optr_spec_seg = SystemProperties.get("persist.operator.optr", "") + "_"
                    + SystemProperties.get("persist.operator.spec", "") + "_"
                    + SystemProperties.get("persist.operator.seg", "");


            if (optr_spec_seg.equals("OP09_SPEC0212_SEGDEFAULT")) {
                // A lab (not support)
                return false;
            } else {
                return true;
            }
        }

        /*
         * Get the slot which has 3/4G capability and map to (0, 1)
         */
        public int getSimSwitchSlot() {
            // Get the slot which has 3/4G capability (1 or 2)
            int value = SystemProperties.getInt("persist.radio.simswitch", -1) - 1;
            Log.i(TAG, "3/4G is on slot " + value);
            if (value == Const.SLOT_ID_0 || value == Const.SLOT_ID_1) {
                return value;
            } else {
                return Const.SLOT_ID_INVALID;
            }
        }

        public String getIccId(int slotId) {
            String value = SystemProperties.get(PROPERTY_ICCID_SIM[slotId], "");

            if (isInfoValid(value, LENGTH_ICCID)) {
                return value;
            }
            return VALUE_DEFAULT_ICCID;
        }

        // ------------------------------------------------------
        //    Wrapper for TelephonyManager
        // ------------------------------------------------------

        public String getDeviceMeid(int[] slots) {
            for (int slotId: slots) {
                String result = mTelephonyManager.getMeid(slotId);
                if (isInfoValid(result, LENGTH_MEID)) {
                    return result;
                }
            }
            return VALUE_DEFAULT_MEID;
        }

        public String getImei(int slotId) {
            String result = mTelephonyManager.getImei(slotId);
            if (isInfoValid(result, LENGTH_IMEI)) {
                return result;
            }
            return VALUE_DEFAULT_IMEI;
        }

        public String getNetworkOperator(int subId) {
            return mTelephonyManager.getNetworkOperator(subId);
        }

        public String getSimOperator(int subId) {
            return mTelephonyManager.getSimOperator(subId);
        }

        public String getSubscriberId(int subId) {
            return mTelephonyManager.getSubscriberId(subId);
        }

        public boolean hasIccCard(int slotId) {
            return mTelephonyManager.hasIccCard(slotId);
        }

        public boolean isSingleLoad() {
            return (mTelephonyManager.getSimCount() == 1);
        }

        public boolean isNetworkRoaming(int slotId) {
            int[] subId = getSubId(slotId);

            if (isSubIdsValid(subId)) {
                return mTelephonyManager.isNetworkRoaming(subId[0]);
            }
            return false;
        }

        public void registerPhoneListener(PhoneStateListener listener) {
            mTelephonyManager.listen(listener, PhoneStateListener.LISTEN_NONE);
            mTelephonyManager.listen(listener, PhoneStateListener.LISTEN_SERVICE_STATE);
        }

        public void unRegisterPhoneListener(PhoneStateListener listener) {
            mTelephonyManager.listen(listener, PhoneStateListener.LISTEN_NONE);
        }

        // ------------------------------------------------------
        //    Wrapper for MtkTelephonyManagerEx
        // ------------------------------------------------------

        public int getIccAppFamily(int slotId) {
            int iccType = mMtkTelephonyManagerEx.getIccAppFamily(slotId);
            Log.i(TAG, "Slot " + slotId + " iccType is : " + iccType);
            return iccType;
        }

        public CellLocation getCellLocation(int slotId) {
            return mMtkTelephonyManagerEx.getCellLocation(slotId);
        }

        public String[] getSupportCardType(int slotId) {
            return mMtkTelephonyManagerEx.getSupportCardType(slotId);
        }

        public String getUimSubscriberId(int subId) {
            return mMtkTelephonyManagerEx.getUimSubscriberId(subId);
        }

        /**
         * if 3G CT, CDMA IMSI; else, IMSI
         */
        public String getImsi(int slotId) {
            String result = VALUE_DEFAULT_IMSI;
            int[] subId = getSubId(slotId);

            if (isSubIdsValid(subId)) {
                if (isCdma3GCard(slotId)) {
                    result = getUimSubscriberId(subId[0]);
                } else {
                    result = getSubscriberId(subId[0]);
                }
            }

            if (isInfoValid(result, LENGTH_IMSI)) {
                return result;
            }

            return VALUE_DEFAULT_IMSI;
        }

        public boolean isCdma4GCard(int slotId) {
            return getIccAppFamily(slotId) == (MtkTelephonyManagerEx.APP_FAM_3GPP |
                MtkTelephonyManagerEx.APP_FAM_3GPP2);
        }

        public boolean isCdma3GCard(int slotId) {
            return getIccAppFamily(slotId) == MtkTelephonyManagerEx.APP_FAM_3GPP2;
        }

        public String getCDMAImsi(int slotId) {
            String result = VALUE_DEFAULT_IMSI;;

            int[] subId = getSubId(slotId);
            if (isSubIdsValid(subId)) {
                result = getUimSubscriberId(subId[0]);
            }

           if (isInfoValid(result, LENGTH_IMSI)) {
                return result;
            }

            return VALUE_DEFAULT_IMSI;
        }

        // ------------------------------------------------------
        //    check SIM state
        // ------------------------------------------------------
        public boolean areSlotsInfoReady(int[] slotId) {
            for (int i = 0; i < slotId.length; ++i) {
                if (!isSlotInfoReady(slotId[i])) {
                    Log.i(TAG, "isSlotInfoReady " + slotId[i] + " not ready.");
                    return false;
                }
            }
            return true;
        }

        private boolean isSlotInfoReady(int slotId) {
            if (hasIccCard(slotId)) {
                int[] subId = getSubId(slotId);

                if (isSubIdsValid(subId)) {
                    String networkOperator = getNetworkOperator(subId[0]);
                    String simOperator = getSimOperator(subId[0]);

                    if (isInfoValid(networkOperator, LENGTH_OPERATOR)
                            && isInfoValid(simOperator, LENGTH_OPERATOR)) {
                        return true;
                    } else {
                        Log.i(TAG, "networkOptr/simOptr not all ready");
                    }
                }
            } else {
                // No SIM for this slot
                return true;
            }
            return false;
        }

        /**
         * Whether uim's network operator, UIM operator and phone type is correct
         *
         * @param slotId
         * @return true or false
         */
        public boolean isValidUim(int slotId) {
            int[] subId = getSubId(slotId);
            if (!isSubIdsValid(subId)) {
                return false;
            }

            // After register to VoLTE, not check phone type as it may not be CDMA
            String networkOperator = getNetworkOperator(subId[0]);
            Log.i(TAG, "[isValidUim] slot " + slotId + " network operator " + networkOperator);

            if (OPERATOR_CT.equals(networkOperator)
                    || OPERATOR_CT_MAC.equals(networkOperator)
                    || OPERATOR_CT_4G.equals(networkOperator)) {

                String simOperator = getSimOperator(subId[0]);
                Log.i(TAG, "[isValidUim] slot " + slotId + " sim operator " + simOperator);

                if (simOperator.equals("46003") || simOperator.equals("46011")
                        || simOperator.equals("20404") || simOperator.equals("45403")
                        || simOperator.equals("45431")) {
                    return true;
                }
            }
            return false;
        }

    }
}
