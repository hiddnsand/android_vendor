package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.SwitchPreference;
import android.widget.EditText;

import com.mediatek.sensorhub.settings.Utils;

import java.util.Arrays;

public class CustomerSensorBaseActivity extends BaseActivity implements OnPreferenceChangeListener {

    protected SwitchPreference mSensorSwitch;
    private EditText mReportRate;
    private EditText mReportLatency;
    protected int mReceiveDataTimes = 0;
    private String mSensorStatusKey;
    private int mSensorType;

    public CustomerSensorBaseActivity(String key, int sensorType) {
        super(key);
        mSensorStatusKey = key;
        mSensorType = sensorType;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.customer_sensor_pref);
        initializeAllPreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePreferenceStatus();
    }

    @Override
    protected void onPause() {
        Utils.setReprotTime(mSensorType, Integer.valueOf(mReportRate.getText()
                .toString()), Integer.valueOf(mReportLatency.getText().toString()),
                mReceiveDataTimes);
        super.onPause();
    }

    private void initializeAllPreferences() {
        mSensorSwitch = (SwitchPreference) Utils.createPreference(Utils.TYPE_SWITCH, mSensorKeyMap
                .get(mSensorType).getName(), String
                .valueOf(mSensorType), getPreferenceScreen(), this);
        mSensorSwitch.setOrder(-1);
        CustomerSensorLayout customerLayout =
            (CustomerSensorLayout) findPreference("sensor_customer_view");
        mReportRate = (EditText) customerLayout.findViewById(R.id.report_rate);
        mReportLatency = (EditText) customerLayout.findViewById(R.id.report_latency);
    }

    private void updatePreferenceStatus() {
        boolean status = Utils.getSensorStatus(mSensorStatusKey);
        mSensorSwitch.setChecked(status);
        mReportRate.setEnabled(!status);
        mReportLatency.setEnabled(!status);
        mReportRate.setText(String.valueOf(Utils.getReprotRate(mSensorType)));
        mReportLatency.setText(String.valueOf(Utils.getReprotLatency(mSensorType)));
        mReceiveDataTimes = Utils.getReceiveDataTimes(mSensorType);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean bNewValue = (Boolean) newValue;
        if (preference == mSensorSwitch) {
            Utils.setSensorStatus(mSensorStatusKey, bNewValue);
            if (mBound) {
                mSensorService.registerSensorWithCustomeTime(preference.getKey(), bNewValue,
                        Integer.valueOf(mReportRate.getText().toString()), Integer
                                .valueOf(mReportLatency.getText().toString()));
            }
            mReportRate.setEnabled(!bNewValue);
            mReportLatency.setEnabled(!bNewValue);
            mReceiveDataTimes = 0;
        }
        return true;
    }

    @Override
    public void onSensorChanged(float[] value) {
        mReceiveDataTimes++;
        mSensorSwitch.setSummary(Arrays.toString(value) + getString(R.string.receive_string)
                + mReceiveDataTimes);
    }
}
