/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.mediatek.cellbroadcastreceiver;



import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;

import com.mediatek.cmas.ext.ICmasMainSettingsExt;
import com.mediatek.cellbroadcastreceiver.CheckBoxAndSettingsPreference.OnSettingChangedListener;

import java.io.IOException;
import java.util.List;
/**
 * Settings activity for the cell broadcast receiver.
 */
public class CellBroadcastMainSettings extends PreferenceActivity
                               implements OnSettingChangedListener {
    private static final String TAG = "[CMAS]CellBroadcastMainSettings";

    public static int sSlotId;
    public static int sReadySlotId = -1;

    private CheckBoxAndSettingsPreference mEnableCBCheckBox;
    private CheckBoxAndSettingsPreference mImminentCheckBox;
    private CheckBoxAndSettingsPreference mAmberCheckBox;
    private CheckBoxAndSettingsPreference mSpeechCheckBox;
    private CheckBoxAndSettingsPreference mEnableAllCheckBox;
    public static final String KEY_ENABLE_ALERT_VIBRATE = "enable_key_alert_vibrate";
    public static final String KEY_ALERT_SOUND_VOLUME = "enable_key_sound_volume";

    private MediaPlayer mMediaPlayer;
    private float mAlertVolume = 1.0f;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences_main);

        mEnableCBCheckBox = (CheckBoxAndSettingsPreference)
            findPreference(CheckBoxAndSettingsPreference.KEY_ENABLE_CELLBROADCAST);
        mEnableCBCheckBox.setOnSettingChangedListener(this);
        mImminentCheckBox = (CheckBoxAndSettingsPreference)
            findPreference(CheckBoxAndSettingsPreference.KEY_ENABLE_CMAS_IMMINENT_ALERTS);
        mAmberCheckBox = (CheckBoxAndSettingsPreference)
            findPreference(CheckBoxAndSettingsPreference.KEY_ENABLE_CMAS_AMBER_ALERTS);
        mSpeechCheckBox = (CheckBoxAndSettingsPreference)
            findPreference(CheckBoxAndSettingsPreference.KEY_ENABLE_ALERT_SPEECH);
        mEnableAllCheckBox = (CheckBoxAndSettingsPreference)
        findPreference(CheckBoxAndSettingsPreference.KEY_ENABLE_ALL_ALERT);
        mEnableAllCheckBox.setOnSettingChangedListener(this);

        refreshEnableCheckBox();
        IntentFilter intentFilter =
                new IntentFilter(CellBroadcastConfigService.ACTION_SIM_STATE_CHANGED);
        registerReceiver(mSimStateChangeReceiver, intentFilter); 

        Log.d(TAG, "calling Main setting Plugin");
        ICmasMainSettingsExt optAddingAlertVolumeVibration = (ICmasMainSettingsExt) 
                CellBroadcastPluginManager.getCellBroadcastPluginObject(
                CellBroadcastPluginManager.CELLBROADCAST_PLUGIN_TYPE_MAIN_SETTINGS);
        if (optAddingAlertVolumeVibration != null) {
            if (optAddingAlertVolumeVibration.needToaddAlertSoundVolumeAndVibration()) {
                addAlertSoundVolumeAndVibration();
            }

            optAddingAlertVolumeVibration.activateSpanishAlertOption(
            CellBroadcastMainSettings.this);
            Log.d(TAG, "Called Main setting Plugin");
        }

        if (CellBroadcastReceiverApp.isCellAreaInTw()) {
            PreferenceScreen prefSet = getPreferenceScreen();
            CheckBoxPreference alertMessage = new CheckBoxPreference(this);
            alertMessage.setKey(CheckBoxAndSettingsPreference.KEY_ENABLE_ALERT_MESSAGE);
            alertMessage.setChecked(true);
            alertMessage.setDefaultValue(true);
            alertMessage.setTitle(R.string.enable_cmas_alerts_messages_title);
            alertMessage.setSummary(R.string.enable_cmas_alerts_messages_summary);
            prefSet.addPreference(alertMessage);

            alertMessage.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {

                    boolean value = (Boolean) newValue;
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(
                            getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();

                    editor.putBoolean(CheckBoxAndSettingsPreference.KEY_ENABLE_ALERT_MESSAGE,
                    value);
                    editor.commit();
                    CellBroadcastReceiver.startConfigService(getApplicationContext());
                    return true;
                }
            });

            CheckBoxPreference alertRMTMessage = new CheckBoxPreference(this);
            alertRMTMessage.setKey(CellBroadcastConfigService.ENABLE_CMAS_RMT_SUPPORT);
            alertRMTMessage.setDefaultValue(false);
            alertRMTMessage.setChecked(false);
            alertRMTMessage.setTitle(R.string.enable_cmas_rmt_support_title);
            alertRMTMessage.setSummary(R.string.enable_cmas_rmt_support_summary);
            prefSet.addPreference(alertRMTMessage);

            alertRMTMessage.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {

                    boolean value = (Boolean) newValue;
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(
                            getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();

                    editor.putBoolean(CellBroadcastConfigService.ENABLE_CMAS_RMT_SUPPORT, value);
                    editor.commit();
                    CellBroadcastReceiver.startConfigService(getApplicationContext());
                    return true;
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
        // TODO Auto-generated method stub
        stopMediaPlayer();

        AlertTonePreviewDialog alertPreviewDialog = (AlertTonePreviewDialog)
                findPreference("cmas_preview_alert_tone");
        alertPreviewDialog.onDialogClosed(false);
    }


    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mSimStateChangeReceiver);
    }

    void refreshEnableCheckBox() {
        this.runOnUiThread(new Runnable() {
            public void run() {
                boolean canReceiveMessage = false;
                SubscriptionManager subscriptionManager =
                        new SubscriptionManager(
                        CellBroadcastMainSettings.this.getApplicationContext());
                List<SubscriptionInfo> subscriptionInfoList =
                        subscriptionManager.getActiveSubscriptionInfoList();
                if (subscriptionInfoList == null ||
                        (subscriptionInfoList != null && subscriptionInfoList.isEmpty())) {
                    Log.d(TAG, "there is no sim card");
                    canReceiveMessage = false;
                } else {
                for (SubscriptionInfo subInfo : subscriptionInfoList) {
                    // check whether has a SIM that can receive CMAS
                    if (TelephonyManager.getDefault().getSimState(subInfo.getSimSlotIndex()) ==
                            TelephonyManager.SIM_STATE_READY) {
                        canReceiveMessage = true;
                    }
                 }
                }
                Log.d(TAG, "refreshEnableCheckBox canReceiveMessage " + canReceiveMessage);
                if (canReceiveMessage) {
                    mEnableCBCheckBox.setEnabled(true);
                    setCheckBoxPreferenceEnable(mEnableCBCheckBox.isChecked());
                } else {
                    mEnableCBCheckBox.setEnabled(false);
                    setCheckBoxPreferenceEnable(false);
                }
            }
        });

    }

    public void setCheckBoxPreferenceEnable(boolean enabled) {
        mImminentCheckBox.setEnabled(enabled && (!mEnableAllCheckBox.isChecked()));
        mAmberCheckBox.setEnabled(enabled && (!mEnableAllCheckBox.isChecked()));
        mSpeechCheckBox.setEnabled(enabled);
        mEnableAllCheckBox.setEnabled(enabled);
    }

    @Override
    public void onEnableCBChanged() {
        Log.d(TAG, "onEnableCBChanged ");
        if (mEnableCBCheckBox.isChecked()) {
            Log.d(TAG, "onEnableCBChanged true ");
            setCheckBoxPreferenceEnable(true);
        } else {
            Log.d(TAG, "onEnableCBChanged false");
            setCheckBoxPreferenceEnable(false);
        }
    }

    @Override
    public void onEnableAllChanged() {
        Log.d(TAG, "onEnableAllChanged ");
        if (mEnableAllCheckBox.isChecked()) {
            Log.d(TAG, "onEnableAllChanged true ");
            setAlertPreferenceEnable(true);
        } else {
            Log.d(TAG, "onEnableAllChanged false");
            setAlertPreferenceChecked(true);
            setAlertPreferenceEnable(false);
        }
    }

    public void setAlertPreferenceEnable(boolean enabled) {
        mImminentCheckBox.setEnabled(enabled);
        mAmberCheckBox.setEnabled(enabled);
    }

    private void setAlertPreferenceChecked(boolean checked) {
        mImminentCheckBox.setChecked(checked);
        mAmberCheckBox.setChecked(checked);
    }

    private BroadcastReceiver mSimStateChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshEnableCheckBox();
        }
    };

    /**
     * Stop mediaplayer when press Home key.
     */
     private void stopMediaPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private void addAlertSoundVolumeAndVibration() {
        Log.d(TAG, "[addAlertSoundVolume]");
        CheckBoxPreference vibrate = new CheckBoxPreference(CellBroadcastMainSettings.this);
        PreferenceScreen prefSet = getPreferenceScreen();
        vibrate.setKey(KEY_ENABLE_ALERT_VIBRATE);
        vibrate.setChecked(true);
        vibrate.setDefaultValue(true);
        vibrate.setTitle(getString(R.string.enable_alert_vibrate_title));
        vibrate.setSummary(getString(R.string.enable_alert_vibrate_summary));
        prefSet.addPreference(vibrate);

        vibrate.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {

                boolean value = (Boolean) newValue;
                SharedPreferences prefs =
                        PreferenceManager.getDefaultSharedPreferences(
                        CellBroadcastMainSettings.this);
                SharedPreferences.Editor editor = prefs.edit();

                editor.putBoolean(KEY_ENABLE_ALERT_VIBRATE, value);
                editor.commit();
                ICmasMainSettingsExt optAddingAlertVolumeVibration = (ICmasMainSettingsExt)
                        CellBroadcastPluginManager.getCellBroadcastPluginObject(
                        CellBroadcastPluginManager.CELLBROADCAST_PLUGIN_TYPE_MAIN_SETTINGS);
                if (optAddingAlertVolumeVibration != null) {
                    optAddingAlertVolumeVibration.updateVibrateValue(value);
                }
                return true;
            }
        });

        Preference volume = new Preference(CellBroadcastMainSettings.this);

        volume.setKey(KEY_ALERT_SOUND_VOLUME);
        volume.setDefaultValue(1.0f);
        volume.setTitle(getString(R.string.alert_sound_volume));
        volume.setSummary(getString(R.string.alert_volume_summary));

        prefSet.addPreference(volume);
        Log.d(TAG, "addAlertSoundVolume   Preference is added");

        OnPreferenceClickListener prefClickListener = new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Log.i(TAG, "OnPreferenceClickListener onclicked ");
                final AlertDialog.Builder dialog =
                        new AlertDialog.Builder(CellBroadcastMainSettings.this);
                LayoutInflater flater = getLayoutInflater();
                View v = flater.inflate(R.layout.alert_dialog_view, null);

                SeekBar sb = (SeekBar) v.findViewById(R.id.seekbar);
                // set bar's progress
                SharedPreferences prefs =
                        PreferenceManager.getDefaultSharedPreferences(
                        CellBroadcastMainSettings.this);
                float pro = 1.0f;
                pro = prefs.getFloat(KEY_ALERT_SOUND_VOLUME, 1.0f);
                int progress = (int) (pro * 100);
                if (progress < 0) {
                    progress = 0;
                } else if (progress > 100) {
                    progress = 100;
                }
                Log.d(TAG, "open volume setting,progress:" + progress + ",pro:" + pro);
                sb.setProgress(progress);
                sb.setOnSeekBarChangeListener(getSeekBarListener());
                dialog.setTitle(getString(R.string.alert_sound_volume))
                .setView(v)
                .setPositiveButton(getString(R.string.button_dismiss), new OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        // TODO Auto-generated method stub
                        SharedPreferences prefs =
                                PreferenceManager.getDefaultSharedPreferences(
                                CellBroadcastMainSettings.this);
                        SharedPreferences.Editor editor = prefs.edit();

                        editor.putFloat(KEY_ALERT_SOUND_VOLUME, mAlertVolume);
                        editor.commit();
                        ICmasMainSettingsExt optAddingAlertVolumeVibration = (ICmasMainSettingsExt)
                                CellBroadcastPluginManager.getCellBroadcastPluginObject(
                                CellBroadcastPluginManager.CELLBROADCAST_PLUGIN_TYPE_MAIN_SETTINGS);
                        if (optAddingAlertVolumeVibration != null) {
                            optAddingAlertVolumeVibration.updateVolumeValue(mAlertVolume);
                        }
                        Log.d(TAG, "Volume saved:" + mAlertVolume);
                        if (mMediaPlayer != null) {
                            mMediaPlayer.release();
                            mMediaPlayer = null;
                        }
                    }
                })
                .setNegativeButton(getString(R.string.button_cancel), new OnClickListener() {

                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                        if (mMediaPlayer != null) {
                            mMediaPlayer.release();
                            mMediaPlayer = null;
                        }
                    }
                })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            dialog.cancel();

                            if (mMediaPlayer != null) {
                                mMediaPlayer.release();
                                mMediaPlayer = null;
                            }
                        }
                        return false;
                    }
                })
                .show();
                return true;
            }
        };
        volume.setOnPreferenceClickListener(prefClickListener);
    }


    private SeekBar.OnSeekBarChangeListener getSeekBarListener() {
        return new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mAlertVolume = progress / 100.0f;
                    Log.d(TAG, "volume:" + mAlertVolume);
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.d(TAG, "onStartTrackingTouch");

                if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                    mMediaPlayer.stop();
                } else {
                    mMediaPlayer = new MediaPlayer();
                    mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                    try {
                        AssetFileDescriptor afd =
                                ((CellBroadcastMainSettings.this)).getResources().openRawResourceFd(
                                        R.raw.attention_signal);
                        if (afd != null) {
                            mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd
                                    .getStartOffset(), afd.getLength());
                            afd.close();
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "IOException onStartTrackingTouch: " + e);
                    } catch (IllegalStateException e) {
                        Log.e(TAG, "IllegalStateException onStartTrackingTouch: " + e);
                    }
                }
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // make some sample sound
                Log.d(TAG, "onStopTrackingTouch");
                try {
                    mMediaPlayer.setVolume(mAlertVolume, mAlertVolume);
                    mMediaPlayer.prepare();
                    mMediaPlayer.seekTo(0);
                    mMediaPlayer.start();
                } catch (IOException e) {
                    Log.e(TAG, "exception onStopTrackingTouch: " + e);
                } catch (IllegalStateException e) {
                    Log.e(TAG, "IllegalStateException onStartTrackingTouch: " + e);
                }
            }
        };
    }

}
