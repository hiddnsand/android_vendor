package com.orangelabs.rcs.core.ims.rcsua;

import android.content.Context;

import com.orangelabs.rcs.provider.settings.RcsSettings;
import com.orangelabs.rcs.utils.logger.Logger;
import com.orangelabs.rcs.core.ims.ImsModule;
import com.orangelabs.rcs.core.ims.network.ImsNetworkInterface;
import com.orangelabs.rcs.core.ims.rcsua.RcsUaAdapter.RcsUaEvent;

/**
 * The Class RcsProxyRegistrationHandler.
 */
public class RcsProxySessionHandler implements RcsUaEventDispatcher.RCSEventDispatcher {

    private Logger logger = Logger.getLogger(this.getClass().getName());
    
    /**
     * IMS network interface
     */
    private ImsNetworkInterface networkInterface;
    
    private static final int REGISTRATION_TIMER = 1000;
    private static final int MAX_REGISTRATION_TIMER = 20000;

    private static final String TAG = "RcsProxySessionHandler";
    private static RcsUaAdapter mRcsuaAdapt = null;
    private static boolean mIsRegistrationSuccessful = false;
    private static boolean mIsDeRegistrationRequestSent = false;
    private static RcsProxySessionHandler mProxySessionHandler = null;
    
    private static Object mLock = new Object();
    private static int mInstanceCount  = -1;
    private static Object mWaitRegisterResponse = new Object();
    private static Object mWaitDeRegisterResponse = new Object();
    private static Object mWaitReRegisterResponse = new Object();

    /**
     * Instantiates a new rcs proxy session handler.
     *
     * @param context the context
     */
    private RcsProxySessionHandler(Context context) {
        logger.debug(TAG+" constructor");         
        if (mRcsuaAdapt == null) {
            mRcsuaAdapt = RcsUaAdapter.getInstance();
        }
        
        initInstanceCount();
    }

    public ImsNetworkInterface RcsProxyRegistrationNetworkInterface() {
        return networkInterface;
    }
    
    public static synchronized RcsProxySessionHandler getProxySessionHandler(Context context){
        if(mProxySessionHandler == null){
                mProxySessionHandler = new RcsProxySessionHandler(context);
            }
        return mProxySessionHandler;
    }
    
    public synchronized void initInstanceCount(){
        logger.debug(TAG+" initInstanceCount send init");            
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsuaAdapt.RCS_PROXY_EVENT_RCS_INIT);
        mRcsuaAdapt.writeEvent(event);
        
        mInstanceCount = 0;
    }
    
    public synchronized void incrementInstanceCount(){
        
        logger.debug(TAG+" incrementInstanceCount send active");            
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsuaAdapt.RCS_PROXY_EVENT_RCS_ACTIVE);
        mRcsuaAdapt.writeEvent(event);
      
        mInstanceCount++;
    }

    public synchronized void decrementInstanceCount(){
    	
        logger.debug(TAG+" decrementInstanceCount send deinit");   
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsuaAdapt.RCS_PROXY_EVENT_RCS_INACTIVE);
        mRcsuaAdapt.writeEvent(event);
        
        mInstanceCount--;
    }
    
    
    public synchronized void deInitInstanceCount(){
        logger.debug(TAG+" deInitInstanceCount send deInit");            
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsuaAdapt.RCS_PROXY_EVENT_RCS_DEINIT);
        mRcsuaAdapt.writeEvent(event);

        mInstanceCount = -1;
    }

    /**
     * Event callback. 
     *
     * @param event the event
     * runs in RCSUA event dispatcher 
     */
    public void EventCallback(RcsUaEvent event) {

        try {
            int requestId = event.getRequestID();;
            String state = event.getString(event.getDataLen());

            switch (requestId) {
            case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_REGISTERING:
                logger.debug(TAG+"registering state : " +state);                 
                 if(state.equals("1")){
                    logger.debug("RCS_UA registering currently. wait for REG Success update");
                 }else{
                    logger.debug("RCS_UA couldn't add capab successfully. stop registering process");
					synchronized (mWaitRegisterResponse) {
						//failure set registration as failed.
						mIsRegistrationSuccessful = false;
						logger.debug(TAG+"setRegistrationStatus : "+ mIsRegistrationSuccessful);
						mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
						mWaitRegisterResponse.notify();
					}
                 }
                break;
                
            case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_REGISTER:
                logger.debug(TAG+"RCS_PROXY_EVENT_RSP_RCS_REGISTER , state: "+state);                                 
                //NOTIFY LISTENING MODULE read the content and based on that set the value                     
                synchronized (mWaitRegisterResponse) {
                    if(state.equals("1")){
    					mIsRegistrationSuccessful = true;
    				}
                    else{
                        mIsRegistrationSuccessful = false;
                    }
                    //set the registration status and notify
                    mRcsuaAdapt.setRegisteringState(false);
                    logger.debug(TAG+"setRegistrationStatus : "+ mIsRegistrationSuccessful);
                    mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
                    mWaitRegisterResponse.notify();
                }
                break;

            case RcsUaAdapter.RCS_PROXY_EVENT_RSP_RCS_DEREGISTER:
                logger.debug( TAG+"Deregistered successfully");
                //read the content and based on that set the value                     
                synchronized (mWaitDeRegisterResponse) {
                	 //set the registration status and notify
	                mIsRegistrationSuccessful = false;
                    logger.debug(TAG+"setRegistrationStatus : "+ mIsRegistrationSuccessful);
	                mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
	                mWaitDeRegisterResponse.notify();
                }

                //set the registration status 
                mRcsuaAdapt.setRegistrationStatus(mIsRegistrationSuccessful);
                break;
            case RcsUaAdapter.RCS_PROXY_EVENT_RES_REG_INFO:
                if(mRcsuaAdapt.isRegisteredbyVolte()) {
                    synchronized (mWaitReRegisterResponse) {
                        
                        mWaitReRegisterResponse.notify();
                    }
                }
                break;   
            default:
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Enable request.
     */
    public void enableRequest() {
    }

    /**
     * Disable request.
     */
    public void disableRequest() {
    }

}
