# build hwcomposer static library

LC_MTK_PLATFORM := $(shell echo $(MTK_PLATFORM) | tr A-Z a-z )

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

ifeq ($(MTK_HWC_SUPPORT), yes)

ifeq ($(MTK_HWC_VERSION), 1.4)

LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/Android.mk
LOCAL_C_INCLUDES += \
	frameworks/native/services/surfaceflinger \
	$(TOP)/$(MTK_ROOT)/hardware/include \
	$(TOP)/$(MTK_ROOT)/hardware/hwcomposer \
	$(TOP)/$(MTK_ROOT)/hardware/hwcomposer/include \
	$(TOP)/$(MTK_ROOT)/hardware/gralloc_extra/include \
	$(TOP)/$(MTK_ROOT)/hardware/dpframework/include \
	$(TOP)/$(MTK_ROOT)/hardware/bwc/inc \
	$(TOP)/$(MTK_ROOT)/hardware/m4u/$(LC_MTK_PLATFORM) \
	$(TOP)/$(MTK_ROOT)/platform/$(LC_MTK_PLATFORM)/kernel/drivers/dispsys \
	$(TOP)/vendor/mediatek/proprietary/external/libion_mtk/include \
	$(TOP)/system/core/libion/include \
	$(TOP)/system/core/libsync/include \
	$(TOP)/system/core/libsync

MTK_HWC_PLATFORM_SRC := $(MTK_ROOT)/hardware/libhwcomposer/$(LC_MTK_PLATFORM)

LOCAL_SRC_FILES := \
	../$(LC_MTK_PLATFORM)/platform.cpp \
	utils/transform.cpp \
	hwc.cpp \
	dispatcher.cpp \
	worker.cpp \
	display.cpp \
	hwdev.cpp \
	event.cpp \
	overlay.cpp \
	queue.cpp \
	sync.cpp \
	composer.cpp \
	bliter.cpp \
	service.cpp

LOCAL_CFLAGS:= \
	-DLOG_TAG=\"hwcomposer\"

ifneq ($(strip $(TARGET_BUILD_VARIANT)), eng)
LOCAL_CFLAGS += -DMTK_USER_BUILD
endif

LOCAL_CFLAGS += -DUSE_NATIVE_FENCE_SYNC

LOCAL_CFLAGS += -DUSE_SYSTRACE

LOCAL_CFLAGS += -DMTK_HWC_VER_1_4

#LOCAL_CFLAGS += -DMTK_HWC_PROFILING

LOCAL_MODULE := hwcomposer.$(TARGET_BOARD_PLATFORM).1.4
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

include $(MTK_STATIC_LIBRARY)

endif # MTK_HWC_VERSION

endif # MTK_HWC_SUPPORT
