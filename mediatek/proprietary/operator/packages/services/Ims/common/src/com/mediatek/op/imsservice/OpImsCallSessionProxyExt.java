/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op.imsservice;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Message;
import android.os.RemoteException;
import android.telephony.Rlog;

import com.mediatek.ims.internal.IMtkImsCallSessionListener;
import com.mediatek.ims.internal.op.ImsCallSessionProxyExt;
import com.mediatek.ims.MtkImsCallSessionProxy;

public class OpImsCallSessionProxyExt extends ImsCallSessionProxyExt {
    private static final String TAG = "OpImsCallSessionProxyExt";

    public OpImsCallSessionProxyExt() {
    }

    private static final int EVENT_GTT_CAPABILITY_INDICATION     = 110;
    public void handleMessage(Message msg, MtkImsCallSessionProxy proxy, String callId) {
        Rlog.d(TAG, "received message: " + msg);
        switch(msg.what) {
            case EVENT_GTT_CAPABILITY_INDICATION:
                handleGttCapabilityIndication((AsyncResult) msg.obj, proxy, callId);
                break;
            default:
                break;
        }
    }

    private void handleGttCapabilityIndication(AsyncResult ar, MtkImsCallSessionProxy proxy, String callId) {
        // +EIMSTCAP:<call id>,<local text capability>,<remote text capability>
        // <local text status>,<real remote text capability>
        // 0  Off
        // 1  On
        Rlog.d(TAG, "handleGttCapabilityIndication callId" + callId);
        if (ar == null) {
            Rlog.d(TAG, "handleGttCapabilityIndication ar is null");
            return;
        }

        int[] result = (int[]) ar.result;
        if (proxy == null || callId == null || result[0] != Integer.parseInt(callId)) {
            return;
        }
        int localCapability = result[1];
        int remoteCapability = result[2];
        int localTextStatus = result[3];
        int realRemoteTextCapability = result[4];
        Rlog.d(TAG, "handleGttCapabilityIndication local cap= " + localCapability +
                " remo status= " + remoteCapability + " local status= "+ localTextStatus +
                " remo cap= " + realRemoteTextCapability);

        proxy.notifyTextCapabilityChanged(localCapability, remoteCapability,
            localTextStatus, realRemoteTextCapability);
    }

    public void notifyTextCapabilityChanged(IMtkImsCallSessionListener listener,
            MtkImsCallSessionProxy mtkImsCallSessionProxy,
            int localCapability, int remoteCapability,
            int localTextStatus, int realRemoteTextCapability) {
        if (listener == null) {
            Rlog.d(TAG, "notifyTextCapabilityChanged() listener is null");
            return;
        }
        try {
            listener.callSessionTextCapabilityChanged(mtkImsCallSessionProxy,
                    localCapability, remoteCapability,
                    localTextStatus, realRemoteTextCapability);
        } catch (RemoteException e) {
            Rlog.e(TAG, "RemoteException callSessionTextCapabilityChanged()");
        }
    }
}
