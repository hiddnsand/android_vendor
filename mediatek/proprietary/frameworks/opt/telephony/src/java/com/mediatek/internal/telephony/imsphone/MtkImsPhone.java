/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.internal.telephony.imsphone;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.PowerManager;
import android.os.ResultReceiver;
import android.os.SystemProperties;

import android.telephony.CarrierConfigManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.Rlog;
import android.telephony.TelephonyManager;

import com.android.ims.ImsCallForwardInfo;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.ims.ImsReasonInfo;
import com.android.ims.ImsSsInfo;
import com.android.ims.ImsUtInterface;
import com.android.internal.telephony.CallForwardInfo;
import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandsInterface;

import static com.android.internal.telephony.CommandsInterface.CB_FACILITY_BAOC;
import static com.android.internal.telephony.CommandsInterface.CB_FACILITY_BAOIC;
import static com.android.internal.telephony.CommandsInterface.CB_FACILITY_BAOICxH;
import static com.android.internal.telephony.CommandsInterface.CB_FACILITY_BAIC;
import static com.android.internal.telephony.CommandsInterface.CB_FACILITY_BAICr;
import static com.android.internal.telephony.CommandsInterface.CB_FACILITY_BA_ALL;
import static com.android.internal.telephony.CommandsInterface.CB_FACILITY_BA_MO;
import static com.android.internal.telephony.CommandsInterface.CB_FACILITY_BA_MT;

import static com.android.internal.telephony.CommandsInterface.CF_ACTION_DISABLE;
import static com.android.internal.telephony.CommandsInterface.CF_ACTION_ENABLE;
import static com.android.internal.telephony.CommandsInterface.CF_ACTION_ERASURE;
import static com.android.internal.telephony.CommandsInterface.CF_ACTION_REGISTRATION;
import static com.android.internal.telephony.CommandsInterface.CF_REASON_ALL;
import static com.android.internal.telephony.CommandsInterface.CF_REASON_ALL_CONDITIONAL;
import static com.android.internal.telephony.CommandsInterface.CF_REASON_NO_REPLY;
import static com.android.internal.telephony.CommandsInterface.CF_REASON_NOT_REACHABLE;
import static com.android.internal.telephony.CommandsInterface.CF_REASON_BUSY;
import static com.android.internal.telephony.CommandsInterface.CF_REASON_UNCONDITIONAL;

import com.android.internal.annotations.VisibleForTesting;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.Connection;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneNotifier;
import com.android.internal.telephony.TelephonyComponentFactory;
import com.android.internal.telephony.TelephonyProperties;
import com.android.internal.telephony.imsphone.ImsPhone;
import com.android.internal.telephony.imsphone.ImsPhoneMmiCode;
import com.android.internal.telephony.uicc.IccRecords;


import com.mediatek.ims.MtkImsCallForwardInfo;
import com.mediatek.ims.MtkImsReasonInfo;
import com.mediatek.ims.MtkImsUt;
import com.mediatek.internal.telephony.MtkCallForwardInfo;
import com.mediatek.internal.telephony.MtkGsmCdmaPhone;

import static com.mediatek.internal.telephony.MtkGsmCdmaPhone.EVENT_IMS_UT_CSFB;
import static com.mediatek.internal.telephony.MtkGsmCdmaPhone.EVENT_USSI_CSFB;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.PROPERTY_UT_CFU_NOTIFICATION_MODE;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.UT_CFU_NOTIFICATION_MODE_DISABLED;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.UT_CFU_NOTIFICATION_MODE_ON;
import static com.mediatek.internal.telephony.MtkTelephonyProperties.UT_CFU_NOTIFICATION_MODE_OFF;

import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.MtkRIL;
import com.mediatek.internal.telephony.MtkSuppSrvRequest;

import java.lang.Boolean;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import mediatek.telephony.MtkServiceState;

/**
 * {@hide}
 */
public class MtkImsPhone extends ImsPhone {
    private static final String LOG_TAG = "MtkImsPhone";

    public enum FeatureType {
        VOLTE_ENHANCED_CONFERENCE,
        VOLTE_CONF_REMOVE_MEMBER,
        VIDEO_RESTRICTION,
        VOLTE_ECT,
    }

    private String mDialString;

    public boolean mUssiCSFB = false;

    /**
     * Used as the message in CallStateException.
     * We don't support dialing a USSD number while there is an existing IMS call.
     */
    public static final String USSD_DURING_IMS_INCALL = "ussd_during_ims_incall";

    public static final String UT_BUNDLE_KEY_CLIR = "queryClir";

    private static final String IMS_CALL_BARRING_PASSWORD = "persist.radio.ss.imscbpwd";

    private static final String CFU_TIME_SLOT = "persist.radio.cfu.timeslot.";

    public static final int EVENT_GET_CALL_FORWARD_TIME_SLOT_DONE = 109;
    public static final int EVENT_SET_CALL_FORWARD_TIME_SLOT_DONE = 110;

    {
        mSS = new MtkServiceState();
    }

    // Constructors
    public MtkImsPhone(Context context, PhoneNotifier notifier, Phone defaultPhone) {
        this(context, notifier, defaultPhone, false);
    }

    @VisibleForTesting
    public MtkImsPhone(Context context, PhoneNotifier notifier, Phone defaultPhone,
                    boolean unitTestMode) {
        super("MtkImsPhone", context, notifier, unitTestMode);

        mDefaultPhone = defaultPhone;
        /// M: ALPS02759855. ImsPhoneCallTracker may change service state earlier. @{
        // mCT = TelephonyComponentFactory.getInstance().makeImsPhoneCallTracker(this);
        // mSS.setStateOff();
        mSS.setStateOff();
        /// @}
        // The ImsExternalCallTracker needs to be defined before the ImsPhoneCallTracker, as the
        // ImsPhoneCallTracker uses a thread to spool up the ImsManager.  Part of this involves
        // setting the multiendpoint listener on the external call tracker.  So we need to ensure
        // the external call tracker is available first to avoid potential timing issues.

        mExternalCallTracker =
                TelephonyComponentFactory.getInstance().makeImsExternalCallTracker(this);
        mCT = TelephonyComponentFactory.getInstance().makeImsPhoneCallTracker(this);
        mCT.registerPhoneStateListener(mExternalCallTracker);
        mExternalCallTracker.setCallPuller(mCT);

        mPhoneId = mDefaultPhone.getPhoneId();

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, LOG_TAG);
        mWakeLock.setReferenceCounted(false);

        if (mDefaultPhone.getServiceStateTracker() != null) {
            mDefaultPhone.getServiceStateTracker()
                    .registerForDataRegStateOrRatChanged(this,
                            EVENT_DEFAULT_PHONE_DATA_STATE_CHANGED, null);
        }
        updateDataServiceState();

        mDefaultPhone.registerForServiceStateChanged(this, EVENT_SERVICE_STATE_CHANGED, null);
        // Force initial roaming state update later, on EVENT_CARRIER_CONFIG_CHANGED.
        // Settings provider or CarrierConfig may not be loaded now.
    }

    @Override
    protected Connection dialInternal(String dialString, int videoState,
                                    Bundle intentExtras, ResultReceiver wrappedCallback)
            throws CallStateException {
        // Need to make sure dialString gets parsed properly
        /// M: Ignore stripping for VoLTE SIP uri. @{
        // String newDialString = PhoneNumberUtils.stripSeparators(dialString);
        String newDialString = dialString;
        if (!PhoneNumberUtils.isUriNumber(dialString)) {
            newDialString = PhoneNumberUtils.stripSeparators(dialString);
        }
        /// @}

        // handle in-call MMI first if applicable
        if (handleInCallMmiCommands(newDialString)) {
            return null;
        }

        if (mDefaultPhone.getPhoneType() == PhoneConstants.PHONE_TYPE_CDMA) {
            return mCT.dial(dialString, videoState, intentExtras);
        }

        // Only look at the Network portion for mmi
        /// M: Ignore extracting for VoLTE SIP uri. @{
        // String networkPortion = PhoneNumberUtils.extractNetworkPortionAlt(newDialString);
        String networkPortion = dialString;
        if (!PhoneNumberUtils.isUriNumber(dialString)) {
            networkPortion = PhoneNumberUtils.extractNetworkPortionAlt(newDialString);
        }
        /// @}
        MtkImsPhoneMmiCode mmi =
                MtkImsPhoneMmiCode.newFromDialString(networkPortion, this);
        if (DBG) Rlog.d(LOG_TAG,
                "dialInternal: dialing w/ mmi '" + mmi + "'...");

        /// M: If there is an existing call, CS_FB will cause call end. So just ignore it. @{
        if (isUssdDuringInCall(mmi)) {
            if (DBG) {
                Rlog.d(LOG_TAG, "USSD during in-call, ignore this operation!");
            }
            throw new CallStateException(USSD_DURING_IMS_INCALL);
        }
        /// @}

        /// M:  CS Fall back to GsmCdmaPhone for MMI code. @{
        mDialString = dialString;
        /// @}
        if (mmi == null) {
            return mCT.dial(dialString, videoState, intentExtras);
        } else if (mmi.isTemporaryModeCLIR()) {
            return mCT.dial(mmi.getDialingNumber(), mmi.getCLIRMode(), videoState, intentExtras);
        } else if (!mmi.isSupportedOverImsPhone()) {
            // If the mmi is not supported by IMS service,
            // try to initiate dialing with default phone
            // Note: This code is never reached; there is a bug in isSupportedOverImsPhone which
            // causes it to return true even though the "processCode" method ultimately throws the
            // exception.
            Rlog.i(LOG_TAG, "dialInternal: USSD not supported by IMS; fallback to CS.");
            /// M:  ALPS03012236,Go to CS domain in the processCode of GsmCdmaPhone. @{
            ((MtkGsmCdmaPhone)mDefaultPhone).setCsFallbackStatus(MtkPhoneConstants.UT_CSFB_ONCE);
            /// @}
            throw new CallStateException(CS_FALLBACK);
        } else {
            if (mUssiCSFB) {
                Rlog.d(LOG_TAG, "USSI CSFB");
                mUssiCSFB = false;
                throw new CallStateException(CS_FALLBACK);
            }

            mPendingMMIs.add(mmi);
            mMmiRegistrants.notifyRegistrants(new AsyncResult(null, mmi, null));

            try {
                mmi.processCode();
            } catch (CallStateException cse) {
                if (CS_FALLBACK.equals(cse.getMessage())) {
                    Rlog.i(LOG_TAG, "dialInternal: fallback to GSM required.");
                    // Make sure we remove from the list of pending MMIs since it will handover to
                    // GSM.
                    mPendingMMIs.remove(mmi);
                    throw cse;
                }
            }

            return null;
        }
    }

    public void explicitCallTransfer(String number, int type) {
        ((MtkImsPhoneCallTracker)mCT).unattendedCallTransfer(number, type);
    }

    /// M: for USSD over IMS workaround. @{
    private boolean isUssdDuringInCall(MtkImsPhoneMmiCode mmi) {

        if (mmi == null || !mmi.isUssdNumber()) {
            return false;
        }

        return isInCall();
    }
    /// @}

    @Override
    public void setImsRegistered(boolean value) {
        mImsRegistered = value;
        /// M: ALPS02494504. Remove notification when registering on IMS. @{
        if (mImsRegistered) {
            final String notificationTag = "wifi_calling";
            final int notificationId = 1;

            NotificationManager notificationManager =
                    (NotificationManager) mContext.getSystemService(
                            Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationTag, notificationId);
        }
        /// @}
    }

    /// M: @{
    /* package */
    void onIncomingUSSD(int ussdMode, String ussdMessage) {
        if (DBG) Rlog.d(LOG_TAG, "onIncomingUSSD ussdMode=" + ussdMode);

        boolean isUssdError;
        boolean isUssdRequest;

        isUssdRequest
            = (ussdMode == CommandsInterface.USSD_MODE_REQUEST);

        isUssdError
            = (ussdMode != CommandsInterface.USSD_MODE_NOTIFY
                && ussdMode != CommandsInterface.USSD_MODE_REQUEST);

        MtkImsPhoneMmiCode found = null;
        for (int i = 0, s = mPendingMMIs.size() ; i < s; i++) {
            if(((MtkImsPhoneMmiCode)mPendingMMIs.get(i)).isPendingUSSD()) {
                found = (MtkImsPhoneMmiCode)mPendingMMIs.get(i);
                break;
            }
        }

        if (found != null) {
            // Complete pending USSD
            if (isUssdError) {
                /// M: Set USSI as CSFB @{
                mUssiCSFB = true;
                /// @}
                found.onUssdFinishedError();
            } else {
                found.onUssdFinished(ussdMessage, isUssdRequest);
            }
        } else { // pending USSD not found
            // The network may initiate its own USSD request

            // ignore everything that isnt a Notify or a Request
            // also, discard if there is no message to present
            if (!isUssdError && ussdMessage != null) {
                MtkImsPhoneMmiCode mmi;
                mmi = MtkImsPhoneMmiCode.newNetworkInitiatedUssd(ussdMessage,
                        isUssdRequest,
                        this);
                onNetworkInitiatedUssd(mmi);
            }
        }
    }

    // For VoLTE enhanced conference call.
    public Connection dial(List<String> numbers, int videoState)
            throws CallStateException {
        return ((MtkImsPhoneCallTracker)mCT).dial(numbers, videoState);
    }

    public void hangupAll() throws CallStateException {
        if (DBG) {
            Rlog.d(LOG_TAG, "hangupAll");
        }
        ((MtkImsPhoneCallTracker)mCT).hangupAll();
    }
    /// @}

    protected boolean isMTKSubClass() {
        return true;
    }

    /// M: MTK SS start @{
    @Override
    public void onMMIDone(ImsPhoneMmiCode mmi) {
        /// M: @{
        Rlog.d(LOG_TAG, "onMMIDone: " + mmi + ", mUssiCSFB=" + mUssiCSFB);
        dumpPendingMmi();
        /// @}
        /// M: Set USSI as CSFB @{
        if (mUssiCSFB) {
            String dialString = ((MtkImsPhoneMmiCode)mmi).getUssdDialString();
            Message msgCSFB = mDefaultPhone.obtainMessage(EVENT_USSI_CSFB,
                    dialString);
            mDefaultPhone.sendMessage(msgCSFB);
            mPendingMMIs.remove(mmi);
            return;
        }
        super.onMMIDone(mmi);
    }

    public void removeMmi(ImsPhoneMmiCode mmi) {
        Rlog.d(LOG_TAG, "removeMmi: " + mmi);
        dumpPendingMmi();
        mPendingMMIs.remove(mmi);
    }

    public void dumpPendingMmi() {
        int size = mPendingMMIs.size();
        if (size == 0) {
            Rlog.d(LOG_TAG, "dumpPendingMmi: none");
            return;
        }
        for (int i=0; i<size; i++) {
            Rlog.d(LOG_TAG, "dumpPendingMmi: " + mPendingMMIs.get(i));
        }
    }

    /**
     * CS Fall back to GsmCdmaPhone for MMI code.
     *
     *@param reason the reason for CS fallback
     */
    public void handleMmiCodeCsfb(int reason, MtkImsPhoneMmiCode mmi) {
        if (DBG) {
            Rlog.d(LOG_TAG, "handleMmiCodeCsfb: reason = " + reason + ", mDialString = "
                    + mDialString + ", mmi=" + mmi);
        }
        removeMmi(mmi);

        if (reason == MtkImsReasonInfo.CODE_UT_XCAP_403_FORBIDDEN) {
            ((MtkGsmCdmaPhone)mDefaultPhone).setCsFallbackStatus(
                    MtkPhoneConstants.UT_CSFB_UNTIL_NEXT_BOOT);
        } else if (reason == MtkImsReasonInfo.CODE_UT_UNKNOWN_HOST) {
            ((MtkGsmCdmaPhone)mDefaultPhone).setCsFallbackStatus(
                    MtkPhoneConstants.UT_CSFB_ONCE);
        }
        MtkSuppSrvRequest ss = MtkSuppSrvRequest.obtain(
                MtkSuppSrvRequest.SUPP_SRV_REQ_MMI_CODE, null);
        ss.mParcel.writeString(mDialString);
        Message msgCSFB = mDefaultPhone.obtainMessage(EVENT_IMS_UT_CSFB, ss);

        mDefaultPhone.sendMessage(msgCSFB);
    }

    @Override
    protected boolean isValidCommandInterfaceCFReason (int commandInterfaceCFReason) {
        switch (commandInterfaceCFReason) {
        case CF_REASON_UNCONDITIONAL:
        case CF_REASON_BUSY:
        case CF_REASON_NO_REPLY:
        case CF_REASON_NOT_REACHABLE:
        case CF_REASON_ALL:
        case CF_REASON_ALL_CONDITIONAL:
        case MtkRIL.CF_REASON_NOT_REGISTERED:
            return true;
        default:
            return false;
        }
    }

    @Override
    protected int getConditionFromCFReason(int reason) {
        switch(reason) {
            case CF_REASON_UNCONDITIONAL: return ImsUtInterface.CDIV_CF_UNCONDITIONAL;
            case CF_REASON_BUSY: return ImsUtInterface.CDIV_CF_BUSY;
            case CF_REASON_NO_REPLY: return ImsUtInterface.CDIV_CF_NO_REPLY;
            case CF_REASON_NOT_REACHABLE: return ImsUtInterface.CDIV_CF_NOT_REACHABLE;
            case CF_REASON_ALL: return ImsUtInterface.CDIV_CF_ALL;
            case CF_REASON_ALL_CONDITIONAL: return ImsUtInterface.CDIV_CF_ALL_CONDITIONAL;
            case MtkRIL.CF_REASON_NOT_REGISTERED: return ImsUtInterface.CDIV_CF_NOT_LOGGED_IN;
            default:
                break;
        }

        return ImsUtInterface.INVALID;
    }

    @Override
    protected int getCFReasonFromCondition(int condition) {
        switch(condition) {
            case ImsUtInterface.CDIV_CF_UNCONDITIONAL: return CF_REASON_UNCONDITIONAL;
            case ImsUtInterface.CDIV_CF_BUSY: return CF_REASON_BUSY;
            case ImsUtInterface.CDIV_CF_NO_REPLY: return CF_REASON_NO_REPLY;
            case ImsUtInterface.CDIV_CF_NOT_REACHABLE: return CF_REASON_NOT_REACHABLE;
            case ImsUtInterface.CDIV_CF_ALL: return CF_REASON_ALL;
            case ImsUtInterface.CDIV_CF_ALL_CONDITIONAL: return CF_REASON_ALL_CONDITIONAL;
            case ImsUtInterface.CDIV_CF_NOT_LOGGED_IN: return MtkRIL.CF_REASON_NOT_REGISTERED;
            default:
                break;
        }

        return CF_REASON_NOT_REACHABLE;
    }

    @Override
    public void getCallForwardingOption(int commandInterfaceCFReason,
            Message onComplete) {
        if (DBG) Rlog.d(LOG_TAG, "getCallForwardingOption reason=" + commandInterfaceCFReason);
        if (isValidCommandInterfaceCFReason(commandInterfaceCFReason)) {
            if (DBG) Rlog.d(LOG_TAG, "requesting call forwarding query.");
            if (commandInterfaceCFReason == CF_REASON_UNCONDITIONAL) {
                TelephonyManager.setTelephonyProperty(mDefaultPhone.getPhoneId(),
                        PROPERTY_UT_CFU_NOTIFICATION_MODE,
                        UT_CFU_NOTIFICATION_MODE_DISABLED);
            }

            Message resp;
            resp = obtainMessage(EVENT_GET_CALL_FORWARD_DONE, onComplete);

            try {
                ImsUtInterface ut = mCT.getUtInterface();
                ut.queryCallForward(getConditionFromCFReason(
                        commandInterfaceCFReason), null, resp);
            } catch (ImsException e) {
                sendErrorResponse(onComplete, e);
            }
        } else if (onComplete != null) {
            sendErrorResponse(onComplete);
        }
    }

    @Override
    public void setCallForwardingOption(int commandInterfaceCFAction,
            int commandInterfaceCFReason,
            String dialingNumber,
            int serviceClass,
            int timerSeconds,
            Message onComplete) {
        if (DBG) Rlog.d(LOG_TAG, "setCallForwardingOption action=" + commandInterfaceCFAction
                + ", reason=" + commandInterfaceCFReason + " serviceClass=" + serviceClass);
        if ((isValidCommandInterfaceCFAction(commandInterfaceCFAction)) &&
                (isValidCommandInterfaceCFReason(commandInterfaceCFReason))) {
            Message resp;

            // + [ALPS02301009]
            if (dialingNumber == null || dialingNumber.isEmpty()) {
                if (mDefaultPhone != null &&
                    mDefaultPhone.getPhoneType() == PhoneConstants.PHONE_TYPE_GSM) {
                    if ((mDefaultPhone instanceof MtkGsmCdmaPhone)
                        && ((MtkGsmCdmaPhone) mDefaultPhone).isSupportSaveCFNumber()) {
                        if (isCfEnable(commandInterfaceCFAction)) {
                            String getNumber =
                                ((MtkGsmCdmaPhone) mDefaultPhone).getCFPreviousDialNumber(
                                    commandInterfaceCFReason);

                            if (getNumber != null && !getNumber.isEmpty()) {
                                dialingNumber = getNumber;
                            }
                        }
                    }
                }
            }
            // - [ALPS02301009]
            ImsPhone.Cf cf = new ImsPhone.Cf(dialingNumber,
                    (commandInterfaceCFReason == CF_REASON_UNCONDITIONAL ? true : false),
                    onComplete);
            resp = obtainMessage(EVENT_SET_CALL_FORWARD_DONE,
                    isCfEnable(commandInterfaceCFAction) ? 1 : 0, 0, cf);

            try {
                ImsUtInterface ut = mCT.getUtInterface();
                ut.updateCallForward(getActionFromCFAction(commandInterfaceCFAction),
                        getConditionFromCFReason(commandInterfaceCFReason),
                        dialingNumber,
                        serviceClass,
                        timerSeconds,
                        // M:
                        resp);
                        /// @}
            } catch (ImsException e) {
                sendErrorResponse(onComplete, e);
            }
        } else if (onComplete != null) {
            sendErrorResponse(onComplete);
        }
    }

    private int getCBTypeFromFacility(String facility) {
        if (CB_FACILITY_BAOC.equals(facility)) {
            return ImsUtInterface.CB_BAOC;
        } else if (CB_FACILITY_BAOIC.equals(facility)) {
            return ImsUtInterface.CB_BOIC;
        } else if (CB_FACILITY_BAOICxH.equals(facility)) {
            return ImsUtInterface.CB_BOIC_EXHC;
        } else if (CB_FACILITY_BAIC.equals(facility)) {
            return ImsUtInterface.CB_BAIC;
        } else if (CB_FACILITY_BAICr.equals(facility)) {
            return ImsUtInterface.CB_BIC_WR;
        } else if (CB_FACILITY_BA_ALL.equals(facility)) {
            return ImsUtInterface.CB_BA_ALL;
        } else if (CB_FACILITY_BA_MO.equals(facility)) {
            return ImsUtInterface.CB_BA_MO;
        } else if (CB_FACILITY_BA_MT.equals(facility)) {
            return ImsUtInterface.CB_BA_MT;
        }

        return 0;
    }

    @Override
    public void setCallBarring(String facility, boolean lockState, String password, Message
            onComplete) {
        if (DBG) Rlog.d(LOG_TAG, "setCallBarring facility=" + facility
                + ", lockState=" + lockState);
        Message resp;
        resp = obtainMessage(EVENT_SET_CALL_BARRING_DONE, onComplete);

        int action;
        if (lockState) {
            action = CommandsInterface.CF_ACTION_ENABLE;
        }
        else {
            action = CommandsInterface.CF_ACTION_DISABLE;
        }

        try {
            ImsUtInterface ut = mCT.getUtInterface();

            SystemProperties.set(IMS_CALL_BARRING_PASSWORD, password);
            // password is not required with Ut interface
            ut.updateCallBarring(getCBTypeFromFacility(facility), action, resp, null);
        } catch (ImsException e) {
            sendErrorResponse(onComplete, e);
        }
    }

    private static class CfEx {
        final String mSetCfNumber;
        final long[] mSetTimeSlot;
        final Message mOnComplete;
        final boolean mIsCfu;

        CfEx(String cfNumber, long[] cfTimeSlot, boolean isCfu, Message onComplete) {
            mSetCfNumber = cfNumber;
            mSetTimeSlot = cfTimeSlot;
            mIsCfu = isCfu;
            mOnComplete = onComplete;
        }
    }

    public void saveTimeSlot(long[] timeSlot) {
        String timeSlotKey = CFU_TIME_SLOT + mPhoneId;
        String timeSlotString = "";
        if (timeSlot != null && timeSlot.length == 2) {
            timeSlotString = Long.toString(timeSlot[0]) + "," + Long.toString(timeSlot[1]);
        }
        SystemProperties.set(timeSlotKey, timeSlotString);
        Rlog.d(LOG_TAG, "timeSlotString = " + timeSlotString);
    }

    public long[] getTimeSlot() {
        String timeSlotKey = CFU_TIME_SLOT + mPhoneId;
        String timeSlotString = SystemProperties.get(timeSlotKey, "");
        long[] timeSlot = null;
        if (timeSlotString != null && !timeSlotString.equals("")) {
            String[] timeArray = timeSlotString.split(",");
            if (timeArray.length == 2) {
                timeSlot = new long[2];
                for (int i = 0; i < 2; i++) {
                    timeSlot[i] = Long.parseLong(timeArray[i]);
                    Calendar calenar = Calendar.getInstance(TimeZone.getDefault());
                    calenar.setTimeInMillis(timeSlot[i]);
                    int hour = calenar.get(Calendar.HOUR_OF_DAY);
                    int min = calenar.get(Calendar.MINUTE);
                    Calendar calenar2 = Calendar.getInstance(TimeZone.getDefault());
                    calenar2.set(Calendar.HOUR_OF_DAY, hour);
                    calenar2.set(Calendar.MINUTE, min);
                    timeSlot[i] = calenar2.getTimeInMillis();
                }
            }
        }
        Rlog.d(LOG_TAG, "timeSlot = " + Arrays.toString(timeSlot));
        return timeSlot;
    }

    public void getCallForwardInTimeSlot(int commandInterfaceCFReason,
            Message onComplete) {
        if (DBG) {
            Rlog.d(LOG_TAG, "getCallForwardInTimeSlot reason = " + commandInterfaceCFReason);
        }
        if (commandInterfaceCFReason == CF_REASON_UNCONDITIONAL) {
            if (DBG) {
                Rlog.d(LOG_TAG, "requesting call forwarding in a time slot query.");
            }

            TelephonyManager.setTelephonyProperty(mDefaultPhone.getPhoneId(),
                    PROPERTY_UT_CFU_NOTIFICATION_MODE,
                    UT_CFU_NOTIFICATION_MODE_DISABLED);

            Message resp;
            resp = obtainMessage(EVENT_GET_CALL_FORWARD_TIME_SLOT_DONE, onComplete);

            try {
                ImsUtInterface ut = mCT.getUtInterface();
                ((MtkImsUt)ut).queryCallForwardInTimeSlot(
                        getConditionFromCFReason(commandInterfaceCFReason),
                        resp);
            } catch (ImsException e) {
                sendErrorResponse(onComplete, e);
            }
        } else if (onComplete != null) {
            sendErrorResponse(onComplete);
        }
    }

    public void setCallForwardInTimeSlot(int commandInterfaceCFAction,
            int commandInterfaceCFReason,
            String dialingNumber,
            int timerSeconds,
            long[] timeSlot,
            Message onComplete) {
        if (DBG) {
            Rlog.d(LOG_TAG, "setCallForwardInTimeSlot action = " + commandInterfaceCFAction
                    + ", reason = " + commandInterfaceCFReason);
        }
        if ((isValidCommandInterfaceCFAction(commandInterfaceCFAction)) &&
                (commandInterfaceCFReason == CF_REASON_UNCONDITIONAL)) {
            Message resp;
            CfEx cfEx = new CfEx(dialingNumber, timeSlot, true, onComplete);
            resp = obtainMessage(EVENT_SET_CALL_FORWARD_TIME_SLOT_DONE,
                    commandInterfaceCFAction, 0, cfEx);

            try {
                ImsUtInterface ut = mCT.getUtInterface();
                ((MtkImsUt)ut).updateCallForwardInTimeSlot(
                        getActionFromCFAction(commandInterfaceCFAction),
                        getConditionFromCFReason(commandInterfaceCFReason),
                        dialingNumber,
                        timerSeconds,
                        timeSlot,
                        resp);
             } catch (ImsException e) {
                sendErrorResponse(onComplete, e);
             }
        } else if (onComplete != null) {
            sendErrorResponse(onComplete);
        }
    }

    private MtkCallForwardInfo[] handleCfInTimeSlotQueryResult(MtkImsCallForwardInfo[] infos) {
        MtkCallForwardInfo[] cfInfos = null;

        if (supportMdAutoSetupIms()) {
            if (infos != null && infos.length != 0) {
                cfInfos = new MtkCallForwardInfo[infos.length];
            }
        } else {
            // ALPS03047471: Fix JE when query CF
            if (infos != null) {
                cfInfos = new MtkCallForwardInfo[infos.length];
            }
        }

        IccRecords r = mDefaultPhone.getIccRecords();
        if (infos == null || infos.length == 0) {
            if (r != null) {
                // Assume the default is not active
                // Set unconditional CFF in SIM to false
                setVoiceCallForwardingFlag(r, 1, false, null);
            }
        } else {
            for (int i = 0, s = infos.length; i < s; i++) {
                if (infos[i].mCondition == ImsUtInterface.CDIV_CF_UNCONDITIONAL &&
                        (infos[i].mServiceClass & CommandsInterface.SERVICE_CLASS_VOICE) != 0) {
                    if (r != null) {
                        setVoiceCallForwardingFlag(r, 1, (infos[i].mStatus == 1),
                            infos[i].mNumber);
                        saveTimeSlot(infos[i].mTimeSlot);
                    }
                }
                cfInfos[i] = getMtkCallForwardInfo(infos[i]);
            }
        }

        return cfInfos;
    }

    private MtkCallForwardInfo getMtkCallForwardInfo(MtkImsCallForwardInfo info) {
        MtkCallForwardInfo cfInfo = new MtkCallForwardInfo();
        cfInfo.status = info.mStatus;
        cfInfo.reason = getCFReasonFromCondition(info.mCondition);
        cfInfo.serviceClass = info.mServiceClass;
        cfInfo.toa = info.mToA;
        cfInfo.number = info.mNumber;
        cfInfo.timeSeconds = info.mTimeSeconds;
        cfInfo.timeSlot = info.mTimeSlot;
        return cfInfo;
    }


    @Override
    public void sendUssdResponse(String ussdMessge) {
        Rlog.d(LOG_TAG, "sendUssdResponse");
        MtkImsPhoneMmiCode mmi = MtkImsPhoneMmiCode.newFromUssdUserInput(ussdMessge, this);
        mPendingMMIs.add(mmi);
        /// M: @{
        Rlog.d(LOG_TAG, "sendUssdResponse: " + ussdMessge + ", mmi=" + mmi);
        dumpPendingMmi();
        /// @}
        mMmiRegistrants.notifyRegistrants(new AsyncResult(null, mmi, null));
        mmi.sendUssd(ussdMessge);
    }

    // Assign return message for terminating USSI.
    public void cancelUSSD(Message response) {
        ((MtkImsPhoneCallTracker)mCT).cancelUSSD(response);
    }

    @Override
    protected CommandException getCommandException(int code, String errorString) {
        Rlog.d(LOG_TAG, "getCommandException code= " + code
                + ", errorString= " + errorString);
        CommandException.Error error = CommandException.Error.GENERIC_FAILURE;

        switch(code) {
            case ImsReasonInfo.CODE_UT_NOT_SUPPORTED:
                error = CommandException.Error.REQUEST_NOT_SUPPORTED;
                break;
            case ImsReasonInfo.CODE_UT_CB_PASSWORD_MISMATCH:
                error = CommandException.Error.PASSWORD_INCORRECT;
                break;
            case ImsReasonInfo.CODE_UT_SERVICE_UNAVAILABLE:
                error = CommandException.Error.RADIO_NOT_AVAILABLE;
                break;
            case MtkImsReasonInfo.CODE_UT_XCAP_403_FORBIDDEN:
                error = CommandException.Error.OPERATION_NOT_ALLOWED;
                break;
            case MtkImsReasonInfo.CODE_UT_UNKNOWN_HOST:
                error = CommandException.Error.NO_NETWORK_FOUND;
                break;
            case MtkImsReasonInfo.CODE_UT_XCAP_409_CONFLICT:
                // 409 CONFLICT
                error = CommandException.Error.OEM_ERROR_1;
                break;
          case MtkImsReasonInfo.CODE_UT_XCAP_832_TERMINAL_BASE_SOLUTION:
                error = CommandException.Error.OEM_ERROR_7;
                break;
            default:
                break;
        }

        return new CommandException(error, errorString);
    }

    @Override
    protected CallForwardInfo getCallForwardInfo(ImsCallForwardInfo info) {
        CallForwardInfo cfInfo = new CallForwardInfo();
        cfInfo.status = info.mStatus;
        cfInfo.reason = getCFReasonFromCondition(info.mCondition);
        cfInfo.serviceClass = info.mServiceClass;
        cfInfo.toa = info.mToA;
        cfInfo.number = info.mNumber;
        cfInfo.timeSeconds = info.mTimeSeconds;
        return cfInfo;
    }

    @Override
    protected CallForwardInfo[] handleCfQueryResult(ImsCallForwardInfo[] infos) {
        CallForwardInfo[] cfInfos = null;

        if (supportMdAutoSetupIms()) {
            // AOSP logic for 93
            if (infos != null && infos.length != 0) {
                cfInfos = new CallForwardInfo[infos.length];
            }
        } else {
            // ALPS03047471: Fix JE when query CF
            if (infos != null) {
                cfInfos = new CallForwardInfo[infos.length];
            }
        }

        IccRecords r = mDefaultPhone.getIccRecords();
        if (infos == null || infos.length == 0) {
            if (r != null) {
                // Assume the default is not active
                // Set unconditional CFF in SIM to false
                setVoiceCallForwardingFlag(r, 1, false, null);
            }
        } else {
            for (int i = 0, s = infos.length; i < s; i++) {
                if (infos[i].mCondition == ImsUtInterface.CDIV_CF_UNCONDITIONAL &&
                        (infos[i].mServiceClass & CommandsInterface.SERVICE_CLASS_VOICE) != 0) {
                    if (r != null) {
                        setVoiceCallForwardingFlag(r, 1, (infos[i].mStatus == 1),
                            infos[i].mNumber);
                    }
                }
                cfInfos[i] = getCallForwardInfo(infos[i]);
            }
        }

        return cfInfos;
    }

    @Override
    protected int[] handleCbQueryResult(ImsSsInfo[] infos) {
        int[] cbInfos = new int[1];
        // cbInfos[0] = SERVICE_CLASS_NONE;

        // if (infos[0].mStatus == 1) {
        //     cbInfos[0] = SERVICE_CLASS_VOICE;
        // }

        // M:
        cbInfos[0] = infos[0].mStatus;
        // @}
        return cbInfos;
    }

    @Override
    public void handleMessage(Message msg) {
        AsyncResult ar = (AsyncResult) msg.obj;

        if (DBG) Rlog.d(LOG_TAG, "handleMessage what=" + msg.what);

        switch (msg.what) {
            case EVENT_SET_CALL_FORWARD_DONE:
                {
                    IccRecords r = mDefaultPhone.getIccRecords();
                    ImsPhone.Cf cf = (Cf) ar.userObj;
                    int cfAction = msg.arg1;
                    int cfReason = msg.arg2;

                    int cfEnable = isCfEnable(cfAction) ? 1 : 0;

                    if (cf.mIsCfu && ar.exception == null && r != null) {
                        if (((MtkGsmCdmaPhone) mDefaultPhone).queryCFUAgainAfterSet()
                            && cfReason == CF_REASON_UNCONDITIONAL) {
                            if (ar.result == null) {
                                Rlog.i(LOG_TAG, "arResult is null.");
                            } else {
                                Rlog.d(LOG_TAG, "[EVENT_SET_CALL_FORWARD_DONE check cfinfo.");
                            }
                        } else {
                            setVoiceCallForwardingFlag(r, 1, cfEnable == 1, cf.mSetCfNumber);
                        }
                    }

                    // + [ALPS02301009]
                    if (mDefaultPhone.getPhoneType() == PhoneConstants.PHONE_TYPE_GSM) {
                        if ((mDefaultPhone instanceof MtkGsmCdmaPhone)
                            && ((MtkGsmCdmaPhone) mDefaultPhone).isSupportSaveCFNumber()) {
                            if (ar.exception == null) {
                                if (cfEnable == 1) {
                                    boolean ret =
                                        ((MtkGsmCdmaPhone) mDefaultPhone).applyCFSharePreference(
                                            cfReason, cf.mSetCfNumber);
                                    if (!ret) {
                                        Rlog.d(LOG_TAG, "applySharePreference false.");
                                    }
                                }

                                if (cfAction == CF_ACTION_ERASURE) {
                                    ((MtkGsmCdmaPhone) mDefaultPhone).clearCFSharePreference(
                                            cfReason);
                                }
                            }
                        }
                    }
                    // - [ALPS02301009]
                    sendResponse(cf.mOnComplete, null, ar.exception);
                }
                break;

            case EVENT_GET_CALL_FORWARD_TIME_SLOT_DONE:
                MtkCallForwardInfo[] mtkCfInfos = null;
                if (ar.exception == null) {
                    mtkCfInfos = handleCfInTimeSlotQueryResult(
                           (MtkImsCallForwardInfo[]) ar.result);
                }

                if ((ar.exception != null) && (ar.exception instanceof ImsException)) {
                    ImsException imsException = (ImsException) ar.exception;
                    if ((imsException != null) && (imsException.getCode() ==
                           MtkImsReasonInfo.CODE_UT_XCAP_403_FORBIDDEN)) {
                        ((MtkGsmCdmaPhone)mDefaultPhone).setCsFallbackStatus(
                                MtkPhoneConstants.UT_CSFB_UNTIL_NEXT_BOOT);
                        Message resp = (Message) ar.userObj;
                        if (resp != null) {
                           AsyncResult.forMessage(resp, mtkCfInfos, new CommandException(
                                   CommandException.Error.REQUEST_NOT_SUPPORTED));
                           resp.sendToTarget();
                           return;
                       }
                    }
                }
                sendResponse((Message) ar.userObj, mtkCfInfos, ar.exception);
                break;

            case EVENT_SET_CALL_FORWARD_TIME_SLOT_DONE: {
                IccRecords records = mDefaultPhone.getIccRecords();
                CfEx cfEx = (CfEx) ar.userObj;
                if (cfEx.mIsCfu && ar.exception == null && records != null) {
                    int cfAction = msg.arg1;
                    int cfEnable = isCfEnable(cfAction) ? 1 : 0;

                    setVoiceCallForwardingFlag(records, 1, cfEnable == 1, cfEx.mSetCfNumber);
                    saveTimeSlot(cfEx.mSetTimeSlot);
                }

                if ((ar.exception != null) && (ar.exception instanceof ImsException)) {
                    ImsException imsException = (ImsException) ar.exception;
                    if ((imsException != null) && (imsException.getCode() ==
                           MtkImsReasonInfo.CODE_UT_XCAP_403_FORBIDDEN)) {
                        ((MtkGsmCdmaPhone)mDefaultPhone).setCsFallbackStatus(
                                MtkPhoneConstants.UT_CSFB_UNTIL_NEXT_BOOT);
                        Message resp = cfEx.mOnComplete;
                        if (resp != null) {
                           AsyncResult.forMessage(resp, null, new CommandException(
                                   CommandException.Error.REQUEST_NOT_SUPPORTED));
                           resp.sendToTarget();
                           return;
                       }
                    }
                }
                sendResponse(cfEx.mOnComplete, null, ar.exception);
                break;
            }

            case EVENT_GET_CALL_BARRING_DONE:
                {
                    // Do not do in 93 IMS SS logic
                    if (supportMdAutoSetupIms() == false) {
                        if (((MtkGsmCdmaPhone) mDefaultPhone).isOpTransferXcap404() &&
                            (ar.exception != null) && (ar.exception instanceof ImsException)) {
                             ImsException imsException = (ImsException) ar.exception;
                             if ((imsException != null) && (imsException.getCode() ==
                                    MtkImsReasonInfo.CODE_UT_XCAP_404_NOT_FOUND)) {
                                 Message resp = (Message) ar.userObj;
                                 if (resp != null) {
                                    AsyncResult.forMessage(resp, null, new CommandException(
                                            CommandException.Error.NO_SUCH_ELEMENT));
                                    resp.sendToTarget();
                                    return;
                                }
                            }
                        }
                    }
                    int[] ssInfos = null;
                    if (ar.exception == null) {
                        ssInfos = handleCbQueryResult((ImsSsInfo[])ar.result);
                    }
                    sendResponse((Message) ar.userObj, ssInfos, ar.exception);
                }
                break;

            case EVENT_GET_CLIR_DONE:
                Bundle ssInfo = (Bundle) ar.result;
                int[] clirInfo = null;
                if (ssInfo != null) {
                    clirInfo = ssInfo.getIntArray(ImsPhoneMmiCode.UT_BUNDLE_KEY_CLIR);
                    int[] clirSetting = ((MtkGsmCdmaPhone) mDefaultPhone).getSavedClirSetting();
                    if (clirSetting[0] == CommandsInterface.CLIR_DEFAULT) {
                        Rlog.d(LOG_TAG, "Set clirInfo[0] to default");
                        clirInfo[0] = clirSetting[0];
                    }
                }
                sendResponse((Message) ar.userObj, clirInfo, ar.exception);
                break;

            case EVENT_SET_CLIR_DONE:
                {
                    if (ar.exception == null) {
                        /// M: Save Clir setting @{
                        if (mDefaultPhone.getPhoneType() == PhoneConstants.PHONE_TYPE_GSM
                                && mDefaultPhone instanceof MtkGsmCdmaPhone) {
                            ((MtkGsmCdmaPhone) mDefaultPhone).saveClirSetting(msg.arg1);
                        }
                        /// @}
                    }
                    sendResponse((Message) ar.userObj, null, ar.exception);
                }
                break;

            case EVENT_SET_CALL_BARRING_DONE:
                {
                    // Do not do in 93 IMS SS logic
                    if (supportMdAutoSetupIms() == false) {
                        if (((MtkGsmCdmaPhone) mDefaultPhone).isOpTransferXcap404()
                            && (ar.exception != null)
                            && (ar.exception instanceof ImsException)) {
                             ImsException imsException = (ImsException) ar.exception;
                             if ((imsException != null) && (imsException.getCode() ==
                                    MtkImsReasonInfo.CODE_UT_XCAP_404_NOT_FOUND)) {
                                 Message resp = (Message) ar.userObj;
                                 if (resp != null) {
                                    AsyncResult.forMessage(resp, null, new CommandException(
                                            CommandException.Error.NO_SUCH_ELEMENT));
                                    resp.sendToTarget();
                                    return;
                                }
                             }
                        }
                    }
                    sendResponse((Message) ar.userObj, null, ar.exception);
                }
                break;

            default:
                super.handleMessage(msg);
                break;
        }
    }
    /// @} // MTK SS end

    private boolean supportMdAutoSetupIms() {
        boolean r = false;
        if (SystemProperties.get("ro.md_auto_setup_ims").equals("1")) {
            r = true;
        }
        return r;
    }

    public boolean isFeatureSupported(FeatureType feature) {
        if (feature == FeatureType.VOLTE_ENHANCED_CONFERENCE
                || feature == FeatureType.VIDEO_RESTRICTION || feature == FeatureType.VOLTE_ECT) {
            final List<String> voLteEnhancedConfMccMncList = Arrays.asList(
                    // 1. CMCC:
                    "46000", "46002", "46007", "46008");
            final List<String> voLteECTSupportList = Arrays.asList(
                    // 1. Vodafone:
                    "20205", "20404", "21401", "21406", "21670", "22210", "22601", "23099",
                    "23003", "23415", "23591", "24099", "26204", "26202", "26209", "26801",
                    "27201", "27402", "27403", "27801", "28602", "90128");

            IccRecords iccRecords = mDefaultPhone.getIccRecords();
            if (iccRecords == null) {
                Rlog.d(LOG_TAG, "isFeatureSupported(" + feature + ") no iccRecords");
                return false;
            }

            String mccMnc = iccRecords.getOperatorNumeric();
            if (feature == FeatureType.VOLTE_ECT) {
                boolean retECT = voLteECTSupportList.contains(mccMnc);
                Rlog.d(LOG_TAG, "vodafone isFeatureSupported(" + feature + "): retECT = " + retECT
                        + " current mccMnc = " + mccMnc);
                return retECT;
            } else {
                boolean ret = voLteEnhancedConfMccMncList.contains(mccMnc);
                Rlog.d(LOG_TAG, "isFeatureSupported(" + feature + "): ret = " + ret
                        + " current mccMnc = " + mccMnc);
                return ret;
            }
        } else if (feature == FeatureType.VOLTE_CONF_REMOVE_MEMBER) {
            // Support remove member by default.
            return true;
        }
        return false;
    }
}
