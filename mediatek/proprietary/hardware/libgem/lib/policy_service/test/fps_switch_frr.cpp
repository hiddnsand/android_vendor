#include <inttypes.h>

#include <cutils/memory.h>

#include <utils/Log.h>

#include <binder/IPCThreadState.h>
#include <binder/ProcessState.h>
#include <binder/IServiceManager.h>

#include <dfps/FpsPolicy.h>
#include <dfps/FpsInfo.h>

#define __STDC_FORMAT_MACROS 1

using namespace android;

#define FRR_MIN_FPS 20
#define FRR_MAX_FPS 60

int main(int /*argc*/, char** /*argv*/)
{
    sp<ProcessState> ps(ProcessState::self());
    ps->startThreadPool();

    String8 string("");
    string.appendFormat("arr-switch-test");
    sp<FpsPolicy> policy = new FpsPolicy(FpsPolicy::API_THERMAL, string);
    int number;
    FpsRange range;
    policy->getPanelInfoNumber(&number);
    printf("total fps: %d\n", number);
    for (int i = 0; i < number; i++) {
        policy->getFpsRange(i, &range);
        printf("+ panel fps: [%d] %d~%d\n", i, range.min, range.max);
    }
    srand(0);

    int fps_mode = 0;
    printf("[FRR] fps switch mode:\n");
    printf("0: random fps\n");
    printf("1: constant fps(%d,%d)\n", FRR_MIN_FPS, FRR_MAX_FPS);
    scanf("%d", &fps_mode);

    int sleep_time = 0;
    printf("interval between switch fps?(ms)\n");
    scanf("%d", &sleep_time);

    while (true) {
        switch (fps_mode) {
            case 0:
            {
                int diff = FRR_MAX_FPS - FRR_MIN_FPS + 1;
                int fps = (rand() % diff) + FRR_MIN_FPS;
                policy->setFps(fps, FpsPolicy::MODE_FRR);
                break;
            }

            case 1:
            {
                static int count = 0;
                if (count % 2) {
                    policy->setFps(FRR_MAX_FPS, FpsPolicy::MODE_FRR);
                } else {
                    policy->setFps(FRR_MIN_FPS, FpsPolicy::MODE_FRR);
                }
                count++;
                break;
            }

            default:
                break;
        }
        for (int i = 0; i < sleep_time; i++) {
            usleep(1000);
        }
    }

    int i = 0;
    printf("press any key to release client\n");
    scanf("%d", &i);

    policy->cancelFpsRequestListener();
    policy = NULL;

    printf("press any key to exit\n");
    scanf("%d", &i);

    return 0;
}
