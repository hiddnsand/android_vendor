package com.mediatek.op18.telephony;

import android.content.Context;
import android.content.Intent;

import android.os.Build;
import android.os.SystemProperties;
import android.provider.Settings;

import android.telephony.Rlog;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.SubscriptionController;

import com.mediatek.internal.telephony.datasub.IDataSubSelectorOPExt;
import com.mediatek.internal.telephony.datasub.DataSubSelector;
import com.mediatek.internal.telephony.datasub.DataSubSelectorUtil;
import com.mediatek.internal.telephony.datasub.DataSubSelectorOpExt;
import com.mediatek.internal.telephony.datasub.SimSwitchForDSSExt;
import com.mediatek.internal.telephony.datasub.DataSubConstants;
import com.mediatek.internal.telephony.datasub.ISimSwitchForDSSExt;
import com.mediatek.internal.telephony.datasub.CapabilitySwitch;
import com.mediatek.internal.telephony.MtkSubscriptionManager;

public class Op18DataSubSelectorOpExt implements IDataSubSelectorOPExt {
    private static final boolean USER_BUILD = TextUtils.equals(Build.TYPE, "user");
    private static boolean DBG = true;

    private static String LOG_TAG = "Op18DSSExt";

    private DataSubSelectorOpExt mDataSubSelectorOpExt = null;

    private SimSwitchForDSSExt mSimSwitchForDSSExt = null;

    private static Context mContext = null;

    private static DataSubSelector mDataSubSelector = null;

    private static ISimSwitchForDSSExt mSimSwitchForDSS = null;

    private static CapabilitySwitch mCapabilitySwitch = null;

    private Intent mIntent = null;

    private static final String PRIMARY_SIM = "primary_sim";

    public Op18DataSubSelectorOpExt(Context context) {
        mContext = context;
    }

    public void init(DataSubSelector dataSubSelector, ISimSwitchForDSSExt simSwitchForDSS) {
        mDataSubSelector = dataSubSelector;
        mCapabilitySwitch = CapabilitySwitch.getInstance(mContext, dataSubSelector);
        mSimSwitchForDSS = simSwitchForDSS;
        mDataSubSelectorOpExt = new DataSubSelectorOpExt(mContext);
        mSimSwitchForDSSExt = new SimSwitchForDSSExt(mContext);
        mSimSwitchForDSSExt.init(mDataSubSelector);
        mDataSubSelectorOpExt.init(mDataSubSelector, mSimSwitchForDSSExt);
    }

    @Override
    public void handleSimStateChanged(Intent intent) {
        String simStatus = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
        int slotId = intent.getIntExtra(PhoneConstants.SLOT_KEY, PhoneConstants.SIM_ID_1);
        if (simStatus.equals(IccCardConstants.INTENT_VALUE_ICC_IMSI)) {
            mCapabilitySwitch.handleSimImsiStatus(intent);
            handleNeedWaitImsi();
            handleNeedWaitUnlock(intent);
        } else if (simStatus.equals(IccCardConstants.INTENT_VALUE_ICC_ABSENT)) {
            mCapabilitySwitch.handleSimImsiStatus(intent);
        } else if (simStatus.equals(IccCardConstants.INTENT_VALUE_ICC_NOT_READY)) {
            mCapabilitySwitch.handleSimImsiStatus(intent);
        }
    }

    @Override
    public void handleSubinfoRecordUpdated(Intent intent) {
        int detectedType = intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                    MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE);
        if (detectedType != MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE) {
            subSelector(intent);
        }
    }

    private void handleNeedWaitImsi() {
        if (CapabilitySwitch.isNeedWaitImsi()) {
            CapabilitySwitch.setNeedWaitImsi(Boolean.toString(false));
            log("get imsi and need to check op18 again");
            if (mSimSwitchForDSS.checkCapSwitch(-1) == false) {
                CapabilitySwitch.setNeedWaitImsi(Boolean.toString(true));
            }
        }
        if (CapabilitySwitch.isNeedWaitImsiRoaming() == true) {
            CapabilitySwitch.setNeedWaitImsiRoaming(Boolean.toString(false));
        }
    }

    private void handleNeedWaitUnlock(Intent intent) {
        if (CapabilitySwitch.isNeedWaitUnlock()) {
            CapabilitySwitch.setNeedWaitUnlock("false");
            subSelector(intent);
        }
        if (CapabilitySwitch.isNeedWaitUnlockRoaming()) {
            CapabilitySwitch.setNeedWaitUnlockRoaming("false");
        }
    }

    @Override
    public void subSelector(Intent intent) {
        // op18 has multi policy, decide here
        switch(mCapabilitySwitch.getCapabilitySwitchPolicy()) {
            case DataSubConstants.POLICY_NO_AUTO:
                log("subSelectorForOp18: no auto policy, skip");
                return;
            case DataSubConstants.POLICY_DEFAULT:
                // default policy of op18 is to follow OM.
                mDataSubSelectorOpExt.subSelector(intent);
                return;
            case DataSubConstants.POLICY_POLICY1:
                // keep run op18 policy
                break;
            default:
                log("subSelectorForOp18: Unknow policy, skip");
                return;
        }
        int phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        int insertedSimCount = 0;
        int insertedStatus = 0;
        int detectedType = (intent == null) ? mCapabilitySwitch.getSimStatus() :
                intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS, 0);
        String[] currIccId = new String[mDataSubSelector.getPhoneNum()];

        log("DataSubSelector for op18");

        for (int i = 0; i < mDataSubSelector.getPhoneNum(); i++) {
            currIccId[i] = DataSubSelectorUtil.getIccidFromProp(i);
            if (currIccId[i] == null || "".equals(currIccId[i])) {
                log("error: iccid not found, wait for next sub ready");
                return;
            }
            if (!USER_BUILD) {
                log("currIccId[" + i + "] : " + currIccId[i]);
            }
            if (!DataSubConstants.NO_SIM_VALUE.equals(currIccId[i])) {
                ++insertedSimCount;
                insertedStatus = insertedStatus | (1 << i);
            } else {
                log("clear mcc.mnc:" + i);
                String propStr;
                if (i == 0) {
                    propStr = "gsm.sim.ril.mcc.mnc";
                } else {
                    propStr = "gsm.sim.ril.mcc.mnc." + (i + 1);
                }
                SystemProperties.set(propStr, "");
            }
        }
        // check pin lock
        if (mCapabilitySwitch.isSimUnLocked() == false) {
            log("DataSubSelector for OP18: do not switch because of sim locking");
            CapabilitySwitch.setNeedWaitUnlock("true");
            mIntent = intent;
            return;
        } else {
            log("DataSubSelector for OP18: no pin lock");
            CapabilitySwitch.setNeedWaitUnlock("false");
        }

        log("Inserted SIM count: " + insertedSimCount + ", insertedStatus: " + insertedStatus);

        //Get previous default data
        // TODO: Need to review this part.
        String defaultIccid = SystemProperties.get(DataSubConstants.PROPERTY_DEFAULT_DATA_ICCID);
        if (!USER_BUILD) {
            log("Default data Iccid = " + defaultIccid);
        }
        if (insertedSimCount == 0) {
            // No SIM inserted
            // 1. Default Data: unset
            // 2. Data Enable: OFF
            // 3. 34G: No change
            log("C0: No SIM inserted, set data unset");
            setDefaultData(SubscriptionManager.INVALID_PHONE_INDEX);
            mDataSubSelector.syncDefaultDataToMd(SubscriptionManager.INVALID_PHONE_INDEX);
        } else if (insertedSimCount == 1) {
            for (int i = 0; i < mDataSubSelector.getPhoneNum(); i++) {
                if ((insertedStatus & (1 << i)) != 0) {
                    phoneId = i;
                    break;
                }
            }

            if (detectedType == MtkSubscriptionManager.EXTRA_VALUE_NEW_SIM) {
                // Case 1: Single SIM + New SIM:
                // 1. Default Data: this sub
                // 2. Data Enable: OFF
                // 3. 34G: this sub
                log("C1: Single SIM + New SIM: Set Default data to phone:" + phoneId);
                if (mCapabilitySwitch.setCapability(phoneId)) {
                    setDefaultData(phoneId);
                    mDataSubSelector.syncDefaultDataToMd(phoneId);
                }
                mDataSubSelector.setDataEnabled(phoneId, true);
            } else {
                if (defaultIccid == null || "".equals(defaultIccid)) {
                    //It happened from two SIMs without default SIM -> remove one SIM.
                    // Case 3: Single SIM + Non Data SIM:
                    // 1. Default Data: this sub
                    // 2. Data Enable: OFF
                    // 3. 34G: this sub
                    log("C3: Single SIM + Non Data SIM: Set Default data to phone:" + phoneId);
                    if (mCapabilitySwitch.setCapability(phoneId)) {
                        setDefaultData(phoneId);
                        mDataSubSelector.syncDefaultDataToMd(phoneId);
                    }
                    mDataSubSelector.setDataEnabled(phoneId, true);
                } else {
                    if (defaultIccid.equals(currIccId[phoneId])) {
                        // Case 2: Single SIM + Defult Data SIM:
                        // 1. Default Data: this sub
                        // 2. Data Enable: No Change
                        // 3. 34G: this sub
                        log("C2: Single SIM + Data SIM: Set Default data to phone:" + phoneId);
                        if (mCapabilitySwitch.setCapability(phoneId)) {
                            setDefaultData(phoneId);
                            mDataSubSelector.syncDefaultDataToMd(phoneId);
                        }
                    } else {
                        // Case 3: Single SIM + Non Data SIM:
                        // 1. Default Data: this sub
                        // 2. Data Enable: OFF
                        // 3. 34G: this sub
                        log("C3: Single SIM + Non Data SIM: Set Default data to phone:" + phoneId);
                        if (mCapabilitySwitch.setCapability(phoneId)) {
                            setDefaultData(phoneId);
                            mDataSubSelector.syncDefaultDataToMd(phoneId);
                        }
                        mDataSubSelector.setDataEnabled(phoneId, true);
                    }
                }
            }
        } else if (insertedSimCount >= 2) {
            // data switching
            if (mSimSwitchForDSS.checkCapSwitch(-1) == false) {
                // need wait imsi ready
                CapabilitySwitch.setNeedWaitImsi(Boolean.toString(true));
                mIntent = intent;
                return;
            }
        }
    }

    @Override
    public void handleAirPlaneModeOff(Intent intent) {
        subSelector(intent);
    }

    public void handlePlmnChanged(Intent intent) {}

    public void handleDataEnable(int status) {}

    public void handleDefaultDataChanged(Intent intent) {}

    private void log(String txt) {
        if (DBG) {
            Rlog.d(LOG_TAG, txt);
        }
    }

    private void loge(String txt) {
        if (DBG) {
            Rlog.e(LOG_TAG, txt);
        }
    }

    private void setDefaultData(int phoneId) {
        SubscriptionController subController = SubscriptionController.getInstance();
        int sub = MtkSubscriptionManager.getSubIdUsingPhoneId(phoneId);
        int currSub = SubscriptionManager.getDefaultDataSubscriptionId();

        log("setDefaultData: " + sub + ", current default sub:" + currSub);
        if (sub != currSub && sub >= SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
            // M: DUAL IMS {
            updateImsSim(mContext, sub);
            // @}

        }
        mDataSubSelector.setDefaultData(phoneId);
    }

    /**
     * Update global setting for IMS SIM.
     * @param context Context
     * @param subId ims sim subid
     */
    private void updateImsSim(Context context, int subId) {
        if (!SystemProperties.get("ro.md_auto_setup_ims").equals("1") &&
                SystemProperties.getInt("ro.mtk_multiple_ims_support", 1) != 1) {
            log("updateImsSim, subId = " + subId);
            Settings.Global.putInt(context.getContentResolver(), PRIMARY_SIM,
                   subId);
            mDataSubSelector.updateNetworkMode(context, subId);
        }
    }
}