/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.gallery.plugin.common;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Context;
import android.content.Intent;

import android.util.Log;

import com.mediatek.gallery.plugin.common.R;
import com.mediatek.gallery3d.video.IMovieItem;
import com.mediatek.gallery3d.video.IMovieList;
import com.mediatek.gallery3d.video.IMovieListLoader;
import com.mediatek.gallery3d.video.IMovieListLoader.LoaderListener;
import com.mediatek.gallery3d.video.DefaultMovieListLoader;


public class MovieListHooker extends PluginBaseHooker implements LoaderListener {
    private static final String TAG = "MovieListHooker";
    private static final boolean LOG = true;

    private static final int MENU_NEXT = 1;
    private static final int MENU_PREVIOUS = 2;

    private static final String EXTRA_ENABLE_VIDEO_LIST =
            "mediatek.intent.extra.ENABLE_VIDEO_LIST"; // Gallery will enable this feature

    private MenuItem mMenuNext;
    private MenuItem mMenuPrevious;

    private IMovieListLoader mMovieLoader;
    private IMovieList mMovieList;

    public MovieListHooker(Context context) {
        super(context);
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMovieLoader = new DefaultMovieListLoader();
        mMovieLoader.fillVideoList(getContext(), getIntent(), this, getMovieItem());
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mMovieLoader.cancelList();
    }
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (mMovieList != null) { //list should be filled
            if (mMovieLoader != null && isEnabledVideoList(getIntent())) {
                mMenuPrevious = menu.add(MENU_HOOKER_GROUP_ID,
                                         getMenuActivityId(MENU_PREVIOUS),
                                         0,
                                         mPluginContext.getString(R.string.previous));
                mMenuNext = menu.add(MENU_HOOKER_GROUP_ID,
                                     getMenuActivityId(MENU_NEXT),
                                     0,
                                     mPluginContext.getString(R.string.next));
            }
        }
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        super.onPrepareOptionsMenu(menu);
        updatePrevNext();
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        super.onOptionsItemSelected(item);
        switch(getMenuOriginalId(item.getItemId())) {
        case MENU_PREVIOUS:
            if (mMovieList == null) {
                return false;
            }
            getPlayer().startNextVideo(mMovieList.getPrevious(getMovieItem()));
            return true;
        case MENU_NEXT:
            if (mMovieList == null) {
                return false;
            }
            getPlayer().startNextVideo(mMovieList.getNext(getMovieItem()));
            return true;
        default:
            return false;
        }
    }

    @Override
    public void onMovieItemChanged(final IMovieItem item) {
        super.onMovieItemChanged(item);
        updatePrevNext();
    }

    private void updatePrevNext() {
        if (LOG) {
            Log.v(TAG, "updatePrevNext()");
        }
        if (mMovieList != null && mMenuPrevious != null && mMenuNext != null) {
            if (isFirst(getMovieItem()) && isLast(getMovieItem())) { //only one movie
                mMenuNext.setVisible(false);
                mMenuPrevious.setVisible(false);
            } else {
                mMenuNext.setVisible(true);
                mMenuPrevious.setVisible(true);
            }
            if (isFirst(getMovieItem())) {
                mMenuPrevious.setEnabled(false);
            } else {
                mMenuPrevious.setEnabled(true);
            }
            if (isLast(getMovieItem())) {
                mMenuNext.setEnabled(false);
            } else {
                mMenuNext.setEnabled(true);
            }
        }
    }

    @Override
    public void onListLoaded(final IMovieList movieList) {
        mMovieList = movieList;
        getContext().invalidateOptionsMenu();
        if (LOG) {
            Log.v(TAG, "onListLoaded() " + (mMovieList != null ? mMovieList.size() : "null"));
        }
    }


    private boolean isFirst(IMovieItem item) {
        return mMovieList.getPrevious(item) == null;
    }


    private boolean isLast(IMovieItem item) {
        return mMovieList.getNext(item) == null;
    }

    private boolean isEnabledVideoList(Intent intent) {
        boolean enable = true;
        if (intent != null && intent.hasExtra(EXTRA_ENABLE_VIDEO_LIST)) {
            enable = intent.getBooleanExtra(EXTRA_ENABLE_VIDEO_LIST, true);
        }
        if (LOG) {
            Log.v(TAG, "isEnabledVideoList() return " + enable);
        }
        return enable;
    }
}
