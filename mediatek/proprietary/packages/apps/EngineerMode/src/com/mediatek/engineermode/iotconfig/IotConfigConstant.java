package com.mediatek.engineermode.iotconfig;

public class IotConfigConstant {
    private static final String TAG = "EM_IotConfigConstant";
    public static final String FK_SS_BOOLCONFIG = "persist.ss.boolconfig";
    public static final String FK_SS_BOOLVALUE = "persist.ss.boolvalue";
    public static final String FK_SS_CONTENTTYPE = "persist.ss.contenttype";
    public static final String FK_SS_AUID = "persist.ss.auid";
    public static final String FK_SS_XCAPROOT = "persist.ss.xcaproot";
    public static final String FK_SS_RELEID_CFU = "persist.ss.ruleid.cfu";
    public static final String FK_SS_RELEID_CFB = "persist.ss.ruleid.cfb";
    public static final String FK_SS_RELEID_CFNRY = "persist.ss.ruleid.cfnry";
    public static final String FK_SS_RELEID_CFNRC = "persist.ss.ruleid.cfnrc";
    public static final String FK_SS_RELEID_CFNL = "persist.ss.ruleid.cfnl";
    public static final String FK_SS_DIGEST_ID = "persist.ss.digest.id";
    public static final String FK_SS_DIGEST_PWD = "persist.ss.digest.pwd";
    public static final String FK_SS_XCAPPORT = "persist.ss.xcapport";
    public static final String FK_SS_MEDIATYPE = "persist.ss.mediatype";
    public static final String FK_SS_ALIVETIMER = "persist.ss.alivetimer";
    public static final String FK_SS_REQTIMER = "persist.ss.reqtimer";
    public static final String FK_SS_CDTIMER = "persist.ss.cdtimer";
    public static final String FK_SS_GBA_BSFSERVERURL = "persist.gba.bsfserverurl";
    public static final String FK_SS_GBA_ENABLEBATRUSTALL = "persist.gba.enablegbatrustall";
    public static final String FK_SS_GBA_ENABLEBAFORCERUN = "persist.gba.enablegbaforcerun";
    public static final String FK_SS_GBA_GBATYPE = "persist.gba.gbatype";
    public static final String BOOLEANTYPE = "boolean";
    public static final String STRINGTYPE = "String";
    public static final String INTEGERTYPE = "int";

    public static boolean isNumeric(String s) {
        if (s != null && !"".equals(s.trim()))
            return s.matches("^[0-9]*$");
        else
            return false;
    }

}
