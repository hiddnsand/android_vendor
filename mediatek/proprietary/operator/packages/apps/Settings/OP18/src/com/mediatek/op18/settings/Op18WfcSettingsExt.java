package com.mediatek.op18.settings;

import android.content.Context;
import android.support.v7.preference.PreferenceScreen;

import com.android.ims.ImsManager;
//import com.android.settings.widget.SwitchBar;

import com.mediatek.settings.ext.DefaultWfcSettingsExt;

/**
 * Plugin implementation for WFC Settings.
 */
public class Op18WfcSettingsExt extends DefaultWfcSettingsExt {

    private static final String TAG = "Op18WfcSettingsExt";

    private Context mContext;
    private Context mAppContext;
    private Op18WfcSettings  mWfcSettings = null;
    ImsManager mImsManager;
//    private SwitchBar mHotspotSwitchBar;

    /** Constructor.
     * @param context context
     */
    public Op18WfcSettingsExt(Context context) {
        super();
        mContext = context;
        mWfcSettings = Op18WfcSettings.getInstance(context);
        mWfcSettings.register();
    }

    /** Customize WFC pref as per operator requirement
         * @param context context
         * @param preferenceScreen preferenceScreen
         * @return
         */
    @Override
    public void customizedWfcPreference(Context context, PreferenceScreen preferenceScreen) {
        mAppContext = context;
        //mWfcSettings = Op18WfcSettings.getInstance(context);
        mWfcSettings.customizedWfcPreference(context, preferenceScreen);
    }

    /** Called on events like onResume/onPause etc from WirelessSettings.
    * @param event resume/puase etc.
    * @return
    */
    /*
    @Override
    public void onWirelessSettingsEvent(int event) {
        Log.d(TAG, "Wireless setting event:" + event);
        //mWfcSettings = Op18WfcSettings.getInstance(mContext);
        switch (event) {
            case DefaultWfcSettingsExt.RESUME:
                mWfcSettings.register();
                break;

            case DefaultWfcSettingsExt.PAUSE:
                mWfcSettings.unRegister();
                break;

            case DefaultWfcSettingsExt.CONFIG_CHANGE:
                if (!ImsManager.isWfcEnabledByPlatform(mContext)) {
                    mWfcSettings.removeWfcPreference();
                }
                break;
            default:
                break;
        }
    }*/
}
