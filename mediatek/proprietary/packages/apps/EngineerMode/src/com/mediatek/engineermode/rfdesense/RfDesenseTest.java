/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.rfdesense;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.DialogInterface;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.ModemCategory;
import com.mediatek.engineermode.R;

import java.util.ArrayList;

public class RfDesenseTest extends Activity implements OnItemClickListener {
    public static final String TAG = "RfDesenseTest";

    private static final int DIALOG_CONFIRM = 0;
    private static final int FLIGHT_MODE = 0;
    private static final int FLIGHT_MODE_CDMA = 1;
    private static final int FLIGHT_MODE_EPOF = 2;

    private Phone phone = null;
    private Phone mPhone = null;
    private Phone mCdmaPhone = null;

    private ProgressDialog mIndicator = null;

        private final Handler mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == FLIGHT_MODE) {
                    AsyncResult ar = (AsyncResult) msg.obj;
                    String text = "";
                    if (ar.exception == null) {
                        if (FeatureSupport.isSupported(FeatureSupport.FK_MTK_C2K_SUPPORT) &&
                                !FeatureSupport.is93Modem()) {
                            String[] cmd = new String[3];
                            cmd[0] = "AT+CPOF";
                            cmd[1] = "";
                            cmd[2] = "DESTRILD:C2K";
                            String[] cmd_s = ModemCategory.getCdmaCmdArr(cmd);
                            Elog.d(TAG, "send:" + cmd_s[0] + ",cmd_s.length = " + cmd_s.length);
                            if (mCdmaPhone != null) {
                                mCdmaPhone.invokeOemRilRequestStrings(cmd_s,
                                          mHandler.obtainMessage(FLIGHT_MODE_CDMA));
                            }
                        } else {
                            if (mIndicator != null) {
                                mIndicator.dismiss();
                            }
                            text = "Enter flight mode.";
                            Intent intent = new Intent();
                            intent.setClass(RfDesenseTest.this, RfDesenseTxTest.class);
                            RfDesenseTest.this.startActivity(intent);
                            Toast.makeText(RfDesenseTest.this, text, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (mIndicator != null) {
                            mIndicator.dismiss();
                        }
                        text = "Failed to enter flight mode.";
                        Toast.makeText(RfDesenseTest.this, text, Toast.LENGTH_SHORT).show();
                    }
                } else if (msg.what == FLIGHT_MODE_EPOF) {
                    AsyncResult ar = (AsyncResult) msg.obj;
                    String text = "";
                    if (ar.exception == null) {
                        if (mIndicator != null) {
                            mIndicator.dismiss();
                        }
                        text = "Enter flight mode.";
                        Intent intent = new Intent();
                        intent.setClass(RfDesenseTest.this, RfDesenseTxTest.class);
                        RfDesenseTest.this.startActivity(intent);
                        Toast.makeText(RfDesenseTest.this, text, Toast.LENGTH_SHORT).show();
                    } else {
                        String[] cmd = new String[3];
                        if (FeatureSupport.is93Modem()) {
                            cmd[0] = "AT+EFUN=0";
                        }
                        else{
                            cmd[0] = "AT+CPOF";
                        }
                        cmd[1] = "";
                        cmd[2] = "DESTRILD:C2K";
                        String[] cmd_s = ModemCategory.getCdmaCmdArr(cmd);
                        Elog.d(TAG, "send: " + cmd_s[0] + ",cmd_s.length = " + cmd_s.length);
                        if (mCdmaPhone != null) {
                            mCdmaPhone.invokeOemRilRequestStrings(cmd_s,
                                       mHandler.obtainMessage(FLIGHT_MODE_CDMA));
                        }
                    }
                } else if (msg.what == FLIGHT_MODE_CDMA) {
                    if (mIndicator != null) {
                        mIndicator.dismiss();
                    }
                    AsyncResult ar = (AsyncResult) msg.obj;
                    String text;
                    if (ar.exception == null) {
                        text = "Entry flight mode.";
                        Intent intent = new Intent();
                        intent.setClass(RfDesenseTest.this, RfDesenseTxTest.class);
                        RfDesenseTest.this.startActivity(intent);
                    } else {
                        text = "Failed to enter flight mode.";
                    }
                    Toast.makeText(RfDesenseTest.this, text, Toast.LENGTH_SHORT).show();
                }
            }
        };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rf_desense_test);

        ListView simTypeListView = (ListView) findViewById(R.id.list);

        ArrayList<String> items = new ArrayList<String>();
        items.add(getString(R.string.rf_desense_tx_test));
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        simTypeListView.setAdapter(adapter);
        simTypeListView.setOnItemClickListener(this);
        int phoneId = ModemCategory.getCapabilitySim();
        mPhone = PhoneFactory.getPhone(phoneId);
        if (FeatureSupport.isSupported(FeatureSupport.FK_MTK_C2K_SUPPORT)) {
            mCdmaPhone = ModemCategory.getCdmaPhone();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent();
        if (position == 0) {
            showDialog(DIALOG_CONFIRM);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DIALOG_CONFIRM:
            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (whichButton == DialogInterface.BUTTON_POSITIVE) {
                        enterFlightMode();
                    } else {
                        dialog.dismiss();
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            return builder.setTitle("TX Test")
                          .setMessage("Force entering flight mode!")
                          .setPositiveButton("OK", listener)
                          .setNegativeButton("Cancel", listener)
                          .create();

        default:
            break;
        }
        return null;
    }

    private void enterFlightMode() {
        String[] cmd = new String[2];
        if (FeatureSupport.is93Modem()) {
            cmd[0] = "AT+EFUN=0";
        }
        else{
            cmd[0] = "AT+CFUN=4";
        }

        cmd[1] = "";
        Elog.i(TAG, "send: " + cmd[0]);

        if (mPhone != null) {
            mPhone.invokeOemRilRequestStrings(cmd, mHandler.obtainMessage(FLIGHT_MODE));
        }
        mIndicator = new ProgressDialog(this);
        if (mIndicator != null) {
            mIndicator.setMessage("Enter flight mode");
            mIndicator.show();
        }
    }
}
