#ifndef __FPSPOLICYSERVICE_H__
#define __FPSPOLICYSERVICE_H__

#include <vector>

#include <semaphore.h>

#include <utils/Thread.h>

#include <linux/dfrc_drv.h>

#include "dfps/FpsPolicy.h"
#include "dfps/FpsInfo.h"
#include "dfps/FpsPolicyTracker.h"

namespace android
{
class String8;
class FpsPolicyInfo;
class SimpleInputWindowInfo;

class FpsPolicyService : public Thread
{
public:

    FpsPolicyService();
    ~FpsPolicyService();

    static char const* getServiceName() { return "FpsPolicyService"; }

    virtual status_t regPolicy(const sp<FpsPolicy>& policy, const String8& name,
                               const FpsPolicyInfo& info, sp<DupBitTube>& channel);

    virtual status_t unregPolicy(uint64_t sequence);

    virtual status_t setFps(const FpsPolicyInfo& info);

    virtual status_t setSingleLayer(int32_t single);

    virtual status_t setNumberDisplay(int32_t number);

    virtual status_t setHwcInfo(const HwcInfo &info);

    virtual status_t setInputWindows(const String8& name, const SimpleInputWindowInfo& info);

    virtual status_t regInfo(const sp<FpsInfo>& info, uint64_t id);

    virtual status_t unregInfo(uint64_t id);

    virtual status_t getPanelInfo(PanelInfo& info);

    virtual status_t setForegroundInfo(int32_t pid, String8& packageName);

    virtual status_t getFpsRange(FpsRange& range);

    virtual status_t setWindowFlag(int32_t flag, int32_t mask);

    virtual status_t enableTracker(int32_t enable);

    virtual status_t forbidAdjustingVsync(int32_t forbid);

    virtual status_t dump(int fd, const Vector<String16>& args);

    virtual status_t readyToRun();

private:
    class FpsPolicyInfoPack : public RefBase {
    public:
        FpsPolicyInfoPack(const String8& name, const class FpsPolicyInfo& info)
            : mName(name)
            , mInfo(info)
        {
            mChannel = new DupBitTube();
        }
        virtual ~FpsPolicyInfoPack()
        {
            mChannel = NULL;
        }
        String8 mName;
        class FpsPolicyInfo mInfo;
        sp<DupBitTube> mChannel;
    };

    class InputWindowPack : public RefBase {
    public:
        InputWindowPack(const String8& name, const class SimpleInputWindowInfo& info)
            : mName(name)
            , mInfo(info)
        {
        }
        virtual ~InputWindowPack() {};
        String8 mName;
        SimpleInputWindowInfo mInfo;
    };

    class FpsInfoPack : public RefBase {
    public:
        FpsInfoPack(const sp<FpsInfo>& info, const uint64_t id)
            : mInfo(info)
            , mId(id)
        {
        }
        sp<FpsInfo> mInfo;
        uint64_t mId;
    };

    status_t unregPolicy(uint64_t sequence, bool binderDied);

    void tryGetRequestLocked();

    void printListPolicyInfoLocked(String8& result);

    void printListInfoLocked(String8& result);

    void printListFpsRangeLocked(String8& result);

    void printSystemInfoLocked(String8& result);

    virtual bool threadLoop();

    void packDfrcPolicy(sp<FpsPolicyInfoPack> policy, DFRC_DRV_POLICY *dfrcPolicy);

    void packFpsPolicyInfo(const int num, FpsPolicyInfo *info,
                           const DFRC_DRV_VSYNC_REQUEST *request,
                           const DFRC_DRC_REQUEST_SET *requestSet,
                           const sp<FpsPolicyInfoPack> &pack);

    void packFpsRequest(const int num, FpsRequest *report, const DFRC_DRV_VSYNC_REQUEST *request,
                        const DFRC_DRC_REQUEST_SET *requestSet, const sp<FpsPolicyInfoPack> &pack);

    void dumpVsyncRequestLocked(const DFRC_DRV_VSYNC_REQUEST& request,
                                const DFRC_DRC_REQUEST_SET& requestSet);

    mutable Mutex mInfoLock;
    mutable KeyedVector<uint64_t, sp<FpsPolicyInfoPack> > mList;
    mutable KeyedVector<uint64_t, sp<FpsInfoPack> > mCbList;

    Condition mCondition;
    bool mStop;
    uint64_t mCount;

    uint64_t mProcessedCount;
    int32_t mFps;
    int32_t mSwMode;

    int32_t mForceFps;
    int32_t mForceApi;

    int32_t mSingleLayer;
    int32_t mNumDisplay;

    String8 mForceInputWindowName;
    SimpleInputWindowInfo mForceInputWindowInfo;

    int32_t mDfrcFd;

    DFRC_DRV_VSYNC_REQUEST mRequest;
    DFRC_DRV_PANEL_INFO mPanelInfo;
    std::vector<DFRC_DRV_REFRESH_RANGE> mRange;

    int32_t mFgPid;
    String8 mFgPackageName;

    int32_t mWindowFlag;

    bool mNeedInitTracker;
    sp<FpsPolicyTracker> mTracker;

    int32_t mForbidVsync;
};

};

#endif
