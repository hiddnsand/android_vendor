#ifndef HWC_DISPLAY_H_
#define HWC_DISPLAY_H_

#include <utils/Singleton.h>
#include <hardware/hwcomposer.h>
#include "hwc_priv.h"
#include "gralloc_mtk_defs.h"

#define FAKE_DISPLAY -30

using namespace android;

struct dump_buff;

// ---------------------------------------------------------------------------

struct DisplayData
{
    uint32_t width;
    uint32_t height;
    uint32_t format;
    float xdpi;
    float ydpi;
    nsecs_t refresh;
    bool has_vsync;
    bool connected;
    bool secure;
    int subtype;
};

class DisplayManager : public Singleton<DisplayManager>
{
public:
    DisplayManager();
    ~DisplayManager();

    enum
    {
        MAX_DISPLAYS = HWC_NUM_DISPLAY_TYPES,
    };

    enum DISP_QUERY_TYPE
    {
        DISP_CURRENT_NUM  = 0x00000001,
    };

    // query() is used for client to get capability
    int query(int what, int* value);

    // dump() for debug prupose
    void dump(struct dump_buff* log);

    struct EventListener : public virtual RefBase
    {
        // onVSync() is called to notify vsync signal
        virtual void onVSync(int dpy, nsecs_t timestamp, bool enabled) = 0;

        // onPlugIn() is called to notify a display is plugged
        virtual void onPlugIn(int dpy) = 0;

        // onPlugOut() is called to notify a display is unplugged
        virtual void onPlugOut(int dpy) = 0;

        // onHotPlug() is called to notify external display hotplug event
        virtual void onHotPlugExt(int dpy, int connected) = 0;
    };

    // setListener() is used for client to register listener to get event
    void setListener(const sp<EventListener>& listener);

    // requestVSync() is used for client to request vsync signal
    void requestVSync(int dpy, bool enabled);

    // requestNextVSync() is used by HWCDispatcher to request next vsync
    void requestNextVSync();

    // vsync() is callback by vsync thread
    void vsync(int dpy, nsecs_t timestamp, bool enabled);

    // hotplugExt() is called to insert or remove extrenal display
    void hotplugExt(int dpy, bool connected, bool fake = false);

    // hotplugVir() is called to insert or remove virtual display
    void hotplugVir(int dpy, hwc_display_contents_1_t* list);

    // blank() notifies which dispaly is blank or unblank
    void blank(int dpy, int blank);

    // getFakeDispNum() gets amount of fake external displays
    int getFakeDispNum() { return m_fake_disp_num; }

    // m_data is details information for each display device
    DisplayData* m_data;

    // m_profile_level is used for profiling purpose
    static int m_profile_level;

private:
    enum
    {
        DISP_PLUG_NONE       = 0,
        DISP_PLUG_CONNECT    = 1,
        DISP_PLUG_DISCONNECT = 2,
    };

    // hotplugPost() used to do post jobs after insert/remove display
    void hotplugPost(int dpy, bool connected, int state);

    // printDisplayInfo() used to print out display data
    void printDisplayInfo(int dpy);

    // m_curr_disp_num is current amount of displays
    unsigned int m_curr_disp_num;

    // m_fake_disp_num is amount of fake external displays
    unsigned int m_fake_disp_num;

    // m_listener is used for listen vsync event
    sp<EventListener> m_listener;
};

#endif // HWC_DISPLAY_H_
