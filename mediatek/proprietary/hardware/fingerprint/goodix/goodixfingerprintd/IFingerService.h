/*
 * Copyright (C) 2013-2016, Shenzhen Huiding Technology Co., Ltd.
 * All Rights Reserved.
 */

#ifndef I_FINGER_SERVICE_H_
#define I_FINGER_SERVICE_H_
#include <binder/IInterface.h>
namespace android {

#define FINGER_SERVICE  "fingerprint"

class IFingerService : public android::IInterface {
public:
        DECLARE_META_INTERFACE(FingerService)

        virtual int32_t getEnrolledFingerprints(uint32_t group_id, uint32_t** list) = 0;
        virtual int32_t remove(uint32_t group_id, uint32_t finger_id) = 0;
};

} // namespace android

#endif // FINGER_SERVICE_H_
