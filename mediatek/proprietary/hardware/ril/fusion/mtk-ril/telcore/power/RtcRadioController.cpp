/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 * Include
 *****************************************************************************/
#include "RtcRadioController.h"
#include "RfxMainThread.h"
#include <log/log.h>
#include "utils/RefBase.h"
#include "utils/Errors.h"
#include <cutils/properties.h>
#include <string.h>
#include "RfxIntsData.h"

/*****************************************************************************
 * Class RfxController
 *****************************************************************************/

#define RFX_LOG_TAG "RpRadioCont"
#define ARRAY_LENGTH(array) (sizeof(array)/sizeof(array[0]))

static const int INVALID_VALUE = -1;
static const int RETRY_TIME_MS = 500;

extern "C" void setRadioState(RIL_RadioState newState, RIL_SOCKET_ID rid);

RFX_IMPLEMENT_CLASS("RtcRadioController", RtcRadioController, RfxController);

RtcRadioController::RtcRadioController() {
}

RtcRadioController::~RtcRadioController() {
}

void RtcRadioController::onInit() {
    RfxController::onInit();
    logD(RFX_LOG_TAG, "init()");

    static const int request[] = {
        RFX_MSG_REQUEST_RADIO_POWER,
    };

    getStatusManager()->setIntValue(RFX_STATUS_KEY_RADIO_STATE, RADIO_STATE_UNAVAILABLE);
    registerToHandleRequest(request, ARRAY_LENGTH(request));
    registerForStatusChange();

    logD(RFX_LOG_TAG, "init():Done");
}

void RtcRadioController::registerForStatusChange() {
    logD(RFX_LOG_TAG, "registerForStatusChange");
    getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_RADIO_STATE,
        RfxStatusChangeCallback(this, &RtcRadioController::onRadioStateChanged));
}

void RtcRadioController::onRadioStateChanged(RfxStatusKeyEnum key,
        RfxVariant old_value, RfxVariant value) {
    RFX_UNUSED(key);
    //sync with rild's state
    int newValue = value.asInt();
    int oldValue = old_value.asInt();
    bool requestPower = getStatusManager()->getBoolValue(RFX_STATUS_KEY_REQUEST_RADIO_POWER, false);
    if ((requestPower == false) && ((RIL_RadioState)newValue == RADIO_STATE_ON) &&
            ((RIL_RadioState)oldValue != RADIO_STATE_UNAVAILABLE)) {
        logD(RFX_LOG_TAG, "radio State: %d -> %d, STATUS_KEY_REQUEST_RADIO_POWER = false not update to RILJ",
                oldValue, newValue);
    } else {
        if (newValue != oldValue) {
            logD(RFX_LOG_TAG, "radio State: %d -> %d, using google native API for urc", oldValue, newValue);
            setRadioState((RIL_RadioState)newValue, (RIL_SOCKET_ID)m_slot_id);
        } else {
            logD(RFX_LOG_TAG, "radio state not change (%d), do not update to RILJ", newValue);
        }
    }
}

bool RtcRadioController::onPreviewMessage(const sp<RfxMessage>& message) {
    if (message->getType() == REQUEST) {
        if (canHandleRequest(message)) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

bool RtcRadioController::onCheckIfResumeMessage(const sp<RfxMessage>& message) {
   if (message->getType() == REQUEST) {
          if (canHandleRequest(message)) {
              return true;
          } else {
              return false;
          }
      } else {
          return true;
      }
}

bool RtcRadioController::canHandleRequest(const sp<RfxMessage>& message) {
    RFX_UNUSED(message);
    RadioPowerLock radioLock = (RadioPowerLock) getStatusManager()
            ->getIntValue(RFX_STATUS_KEY_RADIO_LOCK, RADIO_LOCK_IDLE);
    if (radioLock) {
        return false;
    }
    return true;
}

bool RtcRadioController::onHandleRequest(const sp<RfxMessage>& message) {
    int id = message->getId();
    logD(RFX_LOG_TAG, "onHandleRequest: %s(%d)", idToString(id), id);
    switch (id) {
    case RFX_MSG_REQUEST_RADIO_POWER:
        requestRadioPower(message);
        break;
    default:
        break;
    }
    return true;
}

void RtcRadioController::requestRadioPower(const sp<RfxMessage>& message) {
    // update radio lock
    getStatusManager()->setIntValue(RFX_STATUS_KEY_RADIO_LOCK, RADIO_LOCK_BY_RADIO);

    int power = ((int *)message->getData()->getData())[0];
    logD(RFX_LOG_TAG, "requestRadioPower, slotId:%d, onOff: %d", m_slot_id, power);

    bool modemPowerOff = getStatusManager()->getBoolValue(RFX_STATUS_KEY_MODEM_POWER_OFF, false);
    if (modemPowerOff) {
        RLOGD("modemPowerOff, just response to RILJ");
        sp<RfxMessage> response = RfxMessage::obtainResponse(RIL_E_RADIO_NOT_AVAILABLE, message);
        responseToRilj(response);
        return;
    }

    if (power) {
        getStatusManager()->setBoolValue(RFX_STATUS_KEY_REQUEST_RADIO_POWER, true);
    } else {
        getStatusManager()->setBoolValue(RFX_STATUS_KEY_REQUEST_RADIO_POWER, false);
    }
    mPendingRequest.insert({message->getPToken(), message});
    requestToMcl(message, true);
}

bool RtcRadioController::onHandleUrc(const sp<RfxMessage>& message) {
    RFX_UNUSED(message);
    return true;
}

bool RtcRadioController::onHandleResponse(const sp<RfxMessage>& message) {
    int id = message->getId();
    logD(RFX_LOG_TAG, "onHandleResponse: %s(%d)", idToString(id), id);
    switch (id) {
    case RFX_MSG_REQUEST_RADIO_POWER:
        handleRadioPowerResponse(message);
        break;
    default:
        break;
    }
    return true;
}

bool RtcRadioController::handleRadioPowerResponse(const sp<RfxMessage>& message) {
    // update radio lock
    getStatusManager()->setIntValue(RFX_STATUS_KEY_RADIO_LOCK, RADIO_LOCK_IDLE);
    RIL_Errno rilErrno = message->getError();

    sp<RfxMessage> retryMsg = findPendingRequest(mPendingRequest, message);
    sp<RfxAction> action = findAction(mActionMap, message);
    if (RIL_E_OEM_ERROR_1 == rilErrno) {
        // EFUN may error when ERAT processing, need retry
        getStatusManager()->setIntValue(RFX_STATUS_KEY_RADIO_LOCK, RADIO_LOCK_BY_RADIO);
        requestToMcl(retryMsg, true, ms2ns(RETRY_TIME_MS));
        return false;
    } else if ((sp<RfxAction>) NULL != action) {
        action->act();
        return true;
    }
    responseToRilj(message);
    return true;
}

bool RtcRadioController::onCheckIfRejectMessage(const sp<RfxMessage>& message,
        bool isModemPowerOff,int radioState) {
    logD(RFX_LOG_TAG, "onCheckIfRejectMessage, id = %d, isModemPowerOff = %d, rdioState = %d",
            message->getId(), isModemPowerOff, radioState);
    int id = message->getId();
    if (RFX_MSG_REQUEST_RADIO_POWER == id) {
        return false;
    }
    return true;
}

sp<RfxMessage> RtcRadioController::findPendingRequest(std::unordered_map<int, sp<RfxMessage>>
        pendingRequest, const sp<RfxMessage>& msg) {
    std::unordered_map<int, sp<RfxMessage>>::const_iterator result =
            pendingRequest.find(msg->getPToken());

    if (result == pendingRequest.end()) {
        logD(RFX_LOG_TAG, "findPendingRequest: do not find corresponding request");
        RFX_ASSERT("Should not be here");
    }
    sp<RfxMessage> requestMsg = result->second;
    pendingRequest.erase(msg->getPToken());
    logD(RFX_LOG_TAG, "findPendingRequest: find request, PToken = %d", requestMsg->getPToken());
    return requestMsg;
}

sp<RfxAction> RtcRadioController::findAction(std::unordered_map<int, sp<RfxAction>> actionMap,
        const sp<RfxMessage>& msg) {
    std::unordered_map<int, sp<RfxAction>>::const_iterator result =
            actionMap.find(msg->getToken());

    if (result == actionMap.end()) {
        return NULL;
    }
    sp<RfxAction> action = result->second;
    actionMap.erase(msg->getToken());
    logD(RFX_LOG_TAG, "findAction: find request, PToken = %d", msg->getPToken());
    return action;
}

void RtcRadioController::moduleRequestRadioPower(bool power, const sp<RfxAction>& action) {
    RLOGD("moduleRequestRadioPower: slot%d, requestPower:%d",
            m_slot_id, power);
    int desirePower[1] = {power ? 1 : 0};
    sp<RfxMessage> radioRequest = RfxMessage::obtainRequest(m_slot_id, RFX_MSG_REQUEST_RADIO_POWER,
            RfxIntsData(desirePower, 1));
    if ((sp<RfxAction>) NULL != action) {
        mActionMap.insert({radioRequest->getToken(), action});
    }

    // To avoid conflict with request from framework, we should execute request directly
    // update radio lock
    getStatusManager()->setIntValue(RFX_STATUS_KEY_RADIO_LOCK, RADIO_LOCK_BY_RADIO);
    bool modemPowerOff = getStatusManager()->getBoolValue(RFX_STATUS_KEY_MODEM_POWER_OFF, false);
    if (modemPowerOff) {
        RLOGD("modemPowerOff, do not execute moduleRequestRadioPower");
        return;
    }
    mPendingRequest.insert({radioRequest->getPToken(), radioRequest});
    requestToMcl(radioRequest, true);
}
