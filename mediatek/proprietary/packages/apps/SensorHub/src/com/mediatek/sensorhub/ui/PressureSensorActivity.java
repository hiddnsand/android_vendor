package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class PressureSensorActivity extends CustomerSensorBaseActivity{

    public PressureSensorActivity() {
        super(Utils.KEY_PRESSURE_STATUS, Sensor.TYPE_PRESSURE);
    }
}
