LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/hardware/libomx/audio))
include $(CLEAR_VARS)
LOCAL_MODULE = libMtkOmxAlacDec
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_OWNER = mtk
LOCAL_MODULE_SUFFIX = .so
LOCAL_PROPRIETARY_MODULE = true
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libutils libcutils libdl liblog
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libMtkOmxAlacDec.so
include $(BUILD_PREBUILT)
endif
