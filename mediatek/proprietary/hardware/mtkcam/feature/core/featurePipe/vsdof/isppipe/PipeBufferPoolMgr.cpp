/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**
 * @file PipeBufferPoolMgr.cpp
 * @brief BufferPoolMgr for ThirdParty pipe
*/

// Standard C header file

// Android system/core header file
#include <ui/gralloc_extra.h>
// mtkcam custom header file

// mtkcam global header file
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>
#include <mtkcam/feature/stereo/pipe/IIspPipe.h>
#include <mtkcam/def/ImageFormat.h>
// Module header file
// Local header file
#include "PipeBufferPoolMgr.h"
#include "PipeBufferHandler.h"

// Logging header file
#undef PIPE_CLASS_TAG
#define PIPE_CLASS_TAG "PipeBufferPoolMgr"
#include <featurePipe/core/include/PipeLog.h>

/*******************************************************************************
* Namespace start.
********************************************************************************/
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace DualCamThirdParty {

using namespace NSIoPipe::NSPostProc;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Instantiation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

PipeBufferPoolMgr::
PipeBufferPoolMgr(sp<IspPipeSetting> pPipeSetting)
{
    MBOOL bRet = this->initializeBufferPool(pPipeSetting);

    if(!bRet)
    {
        MY_LOGE("Failed to initialize buffer pool set! Cannot continue..");
        uninit();
        return;
    }
}

PipeBufferPoolMgr::
~PipeBufferPoolMgr()
{
    uninit();
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  PipeBufferPoolMgr Private Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

MBOOL
PipeBufferPoolMgr::
initializeBufferPool(sp<IspPipeSetting> pPipeSetting)
{
    // create
    CREATE_IMGBUF_POOL(
                mpPVYUVBufferPool,
                "mpPVYUVBufferPool",
                pPipeSetting->mszPreviewYUV,
                eImgFmt_NV21,
                PipeImgBufferPool::USAGE_HW
                );

    CREATE_IMGBUF_POOL(
                mpRRZOBufferPool_Main1,
                "mpRRZOBufferPool_Main1",
                pPipeSetting->mszRRZO_Main1,
                eImgFmt_NV21,
                PipeImgBufferPool::USAGE_HW
                );

    CREATE_IMGBUF_POOL(
                mpRRZOBufferPool_Main2,
                "mpRRZOBufferPool_Main2",
                pPipeSetting->mszRRZO_Main2,
                eImgFmt_NV21,
                PipeImgBufferPool::USAGE_HW
                );

    // TuningBufferPool creation
    mpTuningBufferPool = TuningBufferPool::create("VSDOF_TUNING_P2A", INormalStream::getRegTableSize());

    // allocate
    ALLOCATE_BUFFER_POOL(mpPVYUVBufferPool, ISP_PIPE_FRAMEBUFFER_SIZE);
    ALLOCATE_BUFFER_POOL(mpRRZOBufferPool_Main1, ISP_PIPE_FRAMEBUFFER_SIZE);
    ALLOCATE_BUFFER_POOL(mpRRZOBufferPool_Main2, ISP_PIPE_FRAMEBUFFER_SIZE);
    ALLOCATE_BUFFER_POOL(mpTuningBufferPool, MAX_P2_FRAME);

    // build buffer map
    this->buildImageBufferPoolMap();

    return MTRUE;
}

MBOOL
PipeBufferPoolMgr::
uninit()
{
    PipeImgBufferPool::destroy(mpPVYUVBufferPool);
    PipeImgBufferPool::destroy(mpRRZOBufferPool_Main1);
    PipeImgBufferPool::destroy(mpRRZOBufferPool_Main2);
    TuningBufferPool::destroy(mpTuningBufferPool);
    return MTRUE;
}

MBOOL
PipeBufferPoolMgr::
buildImageBufferPoolMap()
{
    #define ADD_IMAGEBUFFER_MAP(bufferID, bufferPool)\
        mBIDtoImgBufPoolMap.add(bufferID, bufferPool);\
        mBIDtoBufferTyepMap.add(bufferID, eBUFFER_IMAGE);

    ADD_IMAGEBUFFER_MAP(BID_P2A_OUT_PV_YUV, mpPVYUVBufferPool);
    ADD_IMAGEBUFFER_MAP(BID_P2A_OUT_YUV_MAIN1, mpRRZOBufferPool_Main1);
    ADD_IMAGEBUFFER_MAP(BID_P2A_OUT_YUV_MAIN2, mpRRZOBufferPool_Main2);
    return MTRUE;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  PipeBufferPoolMgr Public Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
SmartPipeImgBuffer
PipeBufferPoolMgr::
request(IspPipeBufferID id)
{
    ssize_t index;
    if((index=mBIDtoImgBufPoolMap.indexOfKey(id)) >= 0)
    {
        sp<PipeImgBufferPool> pBufferPool = mBIDtoImgBufPoolMap.valueAt(index);
        return pBufferPool->request();
    }
    else
    {
        return nullptr;
    }
}

SmartGraphicBuffer
PipeBufferPoolMgr::
requestGB(IspPipeBufferID id)
{
    return nullptr;
}

SmartTuningBuffer
PipeBufferPoolMgr::
requestTB(IspPipeBufferID id)
{
    if(id == BID_P2A_TUNING)
    {
        return mpTuningBufferPool->request();
    }
    else
    {
        MY_LOGE("Cannot find the TuningBufferPool with scenario:%d of buffer id:%d!!", id);
            return NULL;
    }
}

sp<PipeBufferHandler>
PipeBufferPoolMgr::
createBufferHandler()
{
    return new PipeBufferHandler(this);
}

IspBufferType
PipeBufferPoolMgr::
queryBufferType(
    IspPipeBufferID bid
)
{
    ssize_t index;
    if((index = mBIDtoBufferTyepMap.indexOfKey(bid)) >= 0)
    {
        return mBIDtoBufferTyepMap.valueAt(index);
    }
    return eBUFFER_INVALID;
}

}; // DualCamThirdParty
}; // NSFeaturePipe
}; // NSCamFeature
}; // NSCam

