/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/HwPipeline"
//
#include "PipelineDefaultImp.h"
#include "PipelineUtility.h"
//
#include <mtkcam/drv/IHalSensor.h>
//
#include <mtkcam/pipeline/hwnode/P1Node.h>
#include <mtkcam/pipeline/hwnode/P2Node.h>
#include <mtkcam/pipeline/hwnode/FDNode.h>
#include <mtkcam/pipeline/hwnode/JpegNode.h>
#include <mtkcam/pipeline/hwnode/RAW16Node.h>
#include <mtkcam/pipeline/extension/MFNR.h>
//
#include <mtkcam/utils/metastore/IMetadataProvider.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
//
#include <mtkcam/utils/hw/IScenarioControl.h>
#include <mtkcam/utils/hw/HwTransform.h>
//
#include <cutils/compiler.h>
//
#include <camera_custom_eis.h>

using namespace android;
using namespace NSCam;
using namespace NSCam::EIS;
using namespace NSCam::v3;
using namespace NSCam::Utils;
using namespace NSCam::v3::Utils;
using namespace NSCam::v3::NSPipelineContext;


/******************************************************************************
 *
 ******************************************************************************/
PipelineModel_Default*
PipelineModel_Default::
create(MINT32 const openId, wp<IPipelineModelMgr::IAppCallback> pAppCallback)
{
    String8 const name = String8::format("%s:%d", magicName(), openId);
    PipelineModel_Default* pPipelineModel = new PipelineDefaultImp(openId, name, pAppCallback);
    if  ( ! pPipelineModel ) {
        MY_LOGE("fail to new an instance");
        return NULL;
    }
    //
    return pPipelineModel;
}

/******************************************************************************
 *
 ******************************************************************************/
PipelineDefaultImp::MyProcessedParams::
MyProcessedParams(MINT32 const openId/*, NSCamHW::HwInfoHelper& helper*/)
    : mOpenId(openId)
    //
    , mConfigParams()
    , mSkipJpeg(MFALSE)
    //
    , mReprocParams()
    , mCHSvrParams()
    , mPipelineConfigParams()
    , mHwParams()
    , mStreamingPipeParams()
    //
    , mHwInfoHelper(openId)
{
#if MTKCAM_HAVE_ADV_SETTING
    mStreamingPipeParams.mForceEnableIMGO = property_get_int32("debug.feature.forceEnableIMGO", 0);
#endif
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::MyProcessedParams::
querySensorStatics()
{
    if ( ! mHwInfoHelper.updateInfos() )
    {
        MY_LOGE("cannot properly update infos");
        return DEAD_OBJECT;
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
PipelineDefaultImp::MyProcessedParams::
isSupportAdvP2() const
{
#if MTKCAM_HAVE_ADV_SETTING
#ifdef MTKCAM_SUPPORT_COMMON_P2NODE
    if( mStreamingPipeParams.mpAdvSetting != NULL && mStreamingPipeParams.mpAdvSetting->useAdvP2)
    {
        return MTRUE;
    }
#endif // MTKCAM_SUPPORT_COMMON_P2NODE
#endif
    return MFALSE;
}

/******************************************************************************
 *
 ******************************************************************************/
PipelineDefaultImp::MyRecordParams
PipelineDefaultImp::MyProcessedParams::
genRecordParam(PipeConfigParams const& configParams)
{
    PipelineDefaultImp::MyRecordParams recordParam{MFALSE, MFALSE, MSize()};
#define hasUsage(flag, usage) ((flag & usage) == usage)
    for (size_t i = 0; i < configParams.vImage_Yuv_NonStall.size(); i++) {
        if  ( hasUsage(
                    configParams.vImage_Yuv_NonStall[i]->getUsageForConsumer(),
                    GRALLOC_USAGE_HW_VIDEO_ENCODER
                    ) )
        {
            recordParam.hasVRConsumer = MTRUE;
            recordParam.videoSize = configParams.vImage_Yuv_NonStall[i]->getImgSize();
            recordParam.has4KVR = ( recordParam.videoSize.w*recordParam.videoSize.h > 8000000 )? MTRUE : MFALSE;
            break;
        }
    }
#undef hasUsage
    return recordParam;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::MyProcessedParams::
preprocess()
{
    PipelineDefaultImp::MyRecordParams recordParam = genRecordParam(mConfigParams);
    //
    MSize maxStreamSize;
    {
        struct Log
        {
            static String8
                skippedStream(IImageStreamInfo* pStreamInfo)
                {
                    return String8::format(
                            "skipped stream - format:0x%x type:%x size:%dx%d",
                            pStreamInfo->getImgFormat(), pStreamInfo->getStreamType(),
                            pStreamInfo->getImgSize().w, pStreamInfo->getImgSize().h
                            );
                }

            static String8
                candidateStream(IImageStreamInfo* pStreamInfo)
                {
                    return String8::format(
                            "candidate stream - format:0x%x type:%x size:%dx%d",
                            pStreamInfo->getImgFormat(), pStreamInfo->getStreamType(),
                            pStreamInfo->getImgSize().w, pStreamInfo->getImgSize().h
                            );
                }
        };
        //
        if  ( IImageStreamInfo* pStreamInfo = mConfigParams.pImage_Raw.get() ) {
            if  ( pStreamInfo->getStreamType() == eSTREAMTYPE_IMAGE_IN ) {
                MY_LOGD("%s", Log::skippedStream(pStreamInfo).string());
            }
            else {
                MY_LOGD("%s", Log::candidateStream(pStreamInfo).string());
                maxStreamSize = pStreamInfo->getImgSize();
            }
        }
        //
        if  ( IImageStreamInfo* pStreamInfo = mConfigParams.pImage_Jpeg_Stall.get() ) {
            MY_LOGD("%s", Log::candidateStream(pStreamInfo).string());
            if  ( maxStreamSize.size() <= pStreamInfo->getImgSize().size() ) {
                maxStreamSize = pStreamInfo->getImgSize();
            }
        }
        //
        for (size_t i = 0; i < mConfigParams.vImage_Yuv_NonStall.size(); i++) {
            if  ( IImageStreamInfo* pStreamInfo = mConfigParams.vImage_Yuv_NonStall[i].get()) {
                MY_LOGD("%s", Log::candidateStream(pStreamInfo).string());
                if  ( maxStreamSize.size() <= pStreamInfo->getImgSize().size()) {
                    maxStreamSize = pStreamInfo->getImgSize();
                }
            }
        }
    }
    //
    // update processed params
    mPipelineConfigParams.mbHasRecording = recordParam.hasVRConsumer;
    mPipelineConfigParams.mVideoSize     = (mPipelineConfigParams.mbHasRecording)? recordParam.videoSize : MSize();
    mPipelineConfigParams.mb4KRecording  = recordParam.has4KVR;
    mPipelineConfigParams.mbHasRaw       = mConfigParams.pImage_Raw.get() ? MTRUE : MFALSE;
    mReprocParams.mbOpaqueReproc         = ( mConfigParams.pImage_Opaque_Out.get() ||
                       mConfigParams.pImage_Opaque_In.get() ) ? MTRUE : MFALSE;
    mReprocParams.mbYuvReproc            = mConfigParams.pImage_Yuv_In.get() ? MTRUE : MFALSE;
    mPipelineConfigParams.mbHasJpeg      = mConfigParams.pImage_Jpeg_Stall.get() ? MTRUE : MFALSE;
    mPipelineConfigParams.mbHasLcso      = isNeedLcso();
    mPipelineConfigParams.mMaxStreamSize = maxStreamSize;
    // constrained high speed video
    mPipelineConfigParams.mOperation_mode= mConfigParams.mOperation_mode;
    if ( mPipelineConfigParams.mOperation_mode )
    {
        android::sp<IMetadataProvider const>
            pMetadataProvider = NSMetadataProviderManager::valueFor(mOpenId);
        IMetadata::IEntry const& entry = pMetadataProvider->getMtkStaticCharacteristics()
                                         .entryFor(MTK_CONTROL_AVAILABLE_HIGH_SPEED_VIDEO_CONFIGURATIONS);
        if  ( entry.isEmpty() ) {
            MY_LOGW("no static MTK_CONTROL_AVAILABLE_HIGH_SPEED_VIDEO_CONFIGURATIONS");
            mCHSvrParams.mDefaultBusrstNum = 1;
        }
        else {
            // [width, height, fps_min, fps_max, batch_size]
            MBOOL hit = MFALSE;
            for ( size_t i=0; i<entry.count(); i+=5 )
            {
                MY_LOGI_IF( 1, "[width(%d), height(%d), fps_min(%d), fps_max(%d), batch_size(%d)]",
                            entry.itemAt(i  , Type2Type<MINT32>()), entry.itemAt(i+1, Type2Type<MINT32>()),
                            entry.itemAt(i+2, Type2Type<MINT32>()), entry.itemAt(i+3, Type2Type<MINT32>()),
                            entry.itemAt(i+4, Type2Type<MINT32>()) );
                if ( recordParam.videoSize.w == entry.itemAt(i, Type2Type<MINT32>()) &&
                     recordParam.videoSize.h == entry.itemAt(i+1, Type2Type<MINT32>()) )
                {
                    if ( mCHSvrParams.mResetAeTargetFps_Req )
                    {
                        mCHSvrParams.mAeTargetFpsMin   = mCHSvrParams.mAeTargetFpsMin_Req;
                        mCHSvrParams.mResetAeTargetFps_Req = MFALSE;
                    }
                    else
                        mCHSvrParams.mAeTargetFpsMin   = entry.itemAt(i+2, Type2Type<MINT32>());
                    mCHSvrParams.mAeTargetFpsMax   = entry.itemAt(i+3, Type2Type<MINT32>());
                    mCHSvrParams.mDefaultBusrstNum = entry.itemAt(i+4, Type2Type<MINT32>());
                    hit = MTRUE;
                    break;
                }
            }
            if ( !hit )
                MY_LOGW("no matching high speed profile(%dx%d)", recordParam.videoSize.w, recordParam.videoSize.h);
        }
    }
    MY_LOGD("max stream(%d, %d), raw(%d), jpeg(%d), hasRecording(%d), operation_mode(%d):burst(%d)",
             mPipelineConfigParams.mMaxStreamSize.w, mPipelineConfigParams.mMaxStreamSize.h,
             mPipelineConfigParams.mbHasRaw, mPipelineConfigParams.mbHasJpeg,
             mPipelineConfigParams.mbHasRecording, mPipelineConfigParams.mOperation_mode,
             mCHSvrParams.mDefaultBusrstNum );
    //
    mPipelineConfigParams.mbUseP1Node    = MTRUE;
    mPipelineConfigParams.mbUseP2Node    = MTRUE;
    // mbUseP2TSNode  = (hasVRConsumer && mbHasJpeg) || mbOpaqueReproc || mbYuvReproc;
    mPipelineConfigParams.mbUseP2TSNode  = mPipelineConfigParams.mbHasJpeg;
    mPipelineConfigParams.mbUseFDNode    = (!mPipelineConfigParams.mOperation_mode)? MTRUE : MFALSE;
    mPipelineConfigParams.mbUseJpegNode  = mPipelineConfigParams.mbHasJpeg;
    mPipelineConfigParams.mbUseRaw16Node = mPipelineConfigParams.mbHasRaw && (eImgFmt_RAW16 == mConfigParams.pImage_Raw->getImgFormat());
    //
    return OK;
};


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::MyProcessedParams::
decideSensor()
{
    struct sensormodeHelper
    {
        // use enum to select sensor mode if have preferred sensor mode.
        enum
        {
            eNORMAL_PREVIEW = 0,
            eNORMAL_VIDEO,
            eNORMAL_CAPTURE,
            eSLIM_VIDEO1,
            eSLIM_VIDEO2,
            eNUM_SENSOR_MODE,
        };
        //
                    sensormodeHelper( MyProcessedParams* rpParams )
                        : sensorFps{0}
                        , sensorMode{0}
                        , selectIdx(-1)
                        , supportPrevMode(true)
                        , pParams(rpParams)
                    {
                    #if MTKCAM_HAVE_ADV_SETTING
                        if(pParams->mStreamingPipeParams.mpAdvSetting != NULL)
                        {
                            pParams->mStreamingPipeParams.mVhdrMode = pParams->mStreamingPipeParams.mpAdvSetting->vhdrMode;
                            if ( pParams->mStreamingPipeParams.mpAdvSetting->vhdrMode != SENSOR_VHDR_MODE_NONE )
                            {
                                // Currently  only need to check sensor scenario preview
                                // If preview not support, directly use video
                                MUINT32 supportHDRMode = 0;
                                if (! rpParams->mHwInfoHelper.querySupportVHDRMode(SENSOR_SCENARIO_ID_NORMAL_PREVIEW, supportHDRMode)){
                                    MY_LOGE("helper.querySupportVHDRMode Failed!");
                                    return;
                                }
                                if(supportHDRMode != pParams->mStreamingPipeParams.mpAdvSetting->vhdrMode){
                                    MY_LOGD("sensor preview mode not support vhdr, change to use video mode if select preview mode");
                                    supportPrevMode = false;
                                }
                            }
                        }
                    #endif
                    #define addMode(idx, _scenarioId_, _key_)                                                   \
                        do {                                                                                    \
                            MBOOL bResult = MTRUE;                                                              \
                            bResult = rpParams->mHwInfoHelper.getSensorSize(_scenarioId_, sensorSize[idx]);     \
                            if (CC_UNLIKELY(bResult == MFALSE)) {                                               \
                                MY_LOGD("getSensorSize failed"); break;                                         \
                            }                                                                                   \
                            bResult = rpParams->mHwInfoHelper.getSensorFps (_scenarioId_, sensorFps[idx] );     \
                            if (CC_UNLIKELY(bResult == MFALSE)) {                                               \
                                MY_LOGD("getSensorFps failed"); break;                                          \
                            }                                                                                   \
                            sensorMode[idx] = _scenarioId_;                                             \
                            MY_LOGD("candidate mode %d, size(%d, %d)@%d", idx, sensorSize[idx].w,       \
                                    sensorSize[idx].h, sensorFps[idx]);                                 \
                        } while(0)
                        addMode(eNORMAL_PREVIEW, SENSOR_SCENARIO_ID_NORMAL_PREVIEW, preview);
                        addMode(eNORMAL_VIDEO  , SENSOR_SCENARIO_ID_NORMAL_VIDEO  , video);
                        addMode(eNORMAL_CAPTURE, SENSOR_SCENARIO_ID_NORMAL_CAPTURE, capture);
                        addMode(eSLIM_VIDEO1,    SENSOR_SCENARIO_ID_SLIM_VIDEO1, video1);
                        addMode(eSLIM_VIDEO2,    SENSOR_SCENARIO_ID_SLIM_VIDEO2, video2);
                    #undef addMode
                    //
                    #if 1 // preview/video mode, accept by FOV verification
                        NSCamHW::HwTransHelper helper(rpParams->mOpenId);
                        MBOOL acceptPrv = verifyFov(helper, eNORMAL_PREVIEW);
                        // force video mode for 4K recording
                        MBOOL acceptVdo = verifyFov(helper, eNORMAL_VIDEO) |
                                          rpParams->mPipelineConfigParams.mb4KRecording | rpParams->mPipelineConfigParams.mOperation_mode;
                        // MBOOL acceptVdo = verifyFov(helper, eNORMAL_VIDEO);
                    #define altMode(src, dst)                               \
                        do {                                                \
                            sensorSize[src] = sensorSize[dst];              \
                            sensorFps [src] = sensorFps [dst];              \
                            sensorMode[src] = sensorMode[dst];              \
                            MY_LOGD("alt candidate mode %d=>%d", src, dst); \
                        } while(0)
                        if (!acceptPrv) {
                            if (!acceptVdo) {
                                altMode(eNORMAL_VIDEO  , eNORMAL_CAPTURE);
                                altMode(eNORMAL_PREVIEW, eNORMAL_CAPTURE);
                            } else {
                                altMode(eNORMAL_PREVIEW, eNORMAL_VIDEO  );
                            }
                        } else if (!acceptVdo) {
                            altMode(eNORMAL_VIDEO,
                                (sensorFps[eNORMAL_CAPTURE] >= 30 ?
                                eNORMAL_CAPTURE : (supportPrevMode) ? eNORMAL_PREVIEW : eNORMAL_VIDEO));
                        }
                        // force skip video mode if no GRALLOC_USAGE_HW_VIDEO_ENCODER
                        if (!pParams->mPipelineConfigParams.mbHasRecording)
                        {
                            altMode(eNORMAL_VIDEO  , eNORMAL_CAPTURE);
                        }
                    #undef altMode
                    #endif
                    };
                    ~sensormodeHelper()
                    {
                        if( selectIdx != -1 ) {
                            pParams->mHwParams.mSensorMode = sensorMode[selectIdx];
                            pParams->mHwParams.mSensorSize = sensorSize[selectIdx];
                            pParams->mHwParams.mSensorFps  = sensorFps [selectIdx];
                            //
                            refineFps_MultiOpen(pParams->mHwParams.mSensorMode, pParams->mHwParams.mSensorFps);
                            //
                            MY_LOGD("select mode %d, size(%d, %d)@%d vhdr mode(%d)",
                                    pParams->mHwParams.mSensorMode,
                                    pParams->mHwParams.mSensorSize.w, pParams->mHwParams.mSensorSize.h,
                                    pParams->mHwParams.mSensorFps, pParams->mStreamingPipeParams.mVhdrMode
                                   );
                        } else {
                            MY_LOGW("sensor mode is not selected!");
                            for( int i = 0; i < eNUM_SENSOR_MODE; i++ ) {
                                MY_LOGD("mode %d, size(%d, %d)@%d",
                                        sensorMode[i],
                                        sensorSize[i].w, sensorSize[i].h,
                                        sensorFps[i]
                                       );
                            }
                        }
                    }
        MBOOL       verifyFov(NSCamHW::HwTransHelper helper, MUINT const mode)
                    {
                        #define FOV_DIFF_TOLERANCE (0.002)
                        float dX = 0.0f;
                        float dY = 0.0f;
                        return (helper.calculateFovDifference(
                            sensorMode[mode], &dX, &dY) &&
                            dX < FOV_DIFF_TOLERANCE && dY < FOV_DIFF_TOLERANCE)
                            ? MTRUE : MFALSE;
                    };
        MVOID       refineFps_MultiOpen(MUINT const /*mode*/, MUINT& /*fps*/)
                    {}
                    //
        MSize                       sensorSize[eNUM_SENSOR_MODE];
        MINT32                      sensorFps [eNUM_SENSOR_MODE];
        MUINT                       sensorMode[eNUM_SENSOR_MODE];
        //
        int                         selectIdx;
        bool                        supportPrevMode;
        MyProcessedParams* const    pParams;
    } aHelper(this);
    //
    // 1. Raw stream configured: find sensor mode with raw size.
    if  ( IImageStreamInfo* pStreamInfo = mConfigParams.pImage_Raw.get() ) {
        bool hit = false;
        for (int i = 0; i < sensormodeHelper::eNUM_SENSOR_MODE; i++) {
            if  (pStreamInfo->getImgSize() == aHelper.sensorSize[i]) {
                aHelper.selectIdx = i;
                hit = true;
                break;
            }
        }
        if( !hit ) {
            MY_LOGE("Can't find sesnor size that equals to raw size");
            return UNKNOWN_ERROR;
        }
    } else
    // 2. if has VR consumer: sensor video mode is preferred
    // if ( 1 ) {
    MY_LOGI_IF( mPipelineConfigParams.mOperation_mode, "mAeTargetFpsMin(%d) sensorMode(%d).fps(%d)",
                mCHSvrParams.mAeTargetFpsMin, sensormodeHelper::eSLIM_VIDEO1, aHelper.sensorFps[sensormodeHelper::eSLIM_VIDEO1]);
    if ( mPipelineConfigParams.mOperation_mode==IPipelineModelMgr::OperationMode::C_HIGH_SPEED_VIDEO_MODE &&
        mPipelineConfigParams.mbHasRecording && mCHSvrParams.mAeTargetFpsMin==aHelper.sensorFps[sensormodeHelper::eSLIM_VIDEO1] ) {
        aHelper.selectIdx = sensormodeHelper::eSLIM_VIDEO1;
    }
    else if ( mPipelineConfigParams.mbHasRecording ) {
        aHelper.selectIdx = sensormodeHelper::eNORMAL_VIDEO;
    }
    else {
        //policy:
        //    find the smallest size that is "larger" than max of stream size
        //    (not the smallest difference)
        bool hit = false;
        for (int i = 0; i < sensormodeHelper::eNUM_SENSOR_MODE; i++) {
            if  ( mPipelineConfigParams.mMaxStreamSize.w <= aHelper.sensorSize[i].w &&
                  mPipelineConfigParams.mMaxStreamSize.h <= aHelper.sensorSize[i].h )
            {
                aHelper.selectIdx = i;
                hit = true;
                break;
            }
        }
        if( !hit ) {
            // pick largest one
            MY_LOGW("select capture mode");
            aHelper.selectIdx = sensormodeHelper::eNORMAL_CAPTURE;
        }
    }
    //
    // Change sensor mode if preview mode is selected but not support
    if(aHelper.selectIdx == sensormodeHelper::eNORMAL_PREVIEW && ! aHelper.supportPrevMode)
        aHelper.selectIdx = sensormodeHelper::eNORMAL_VIDEO;

    return OK;
};

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::MyProcessedParams::
decideP1()
{
    struct refine
    {
        static
            MVOID       not_larger_than(MSize& size, MSize const& limit) {
                if( size.w > limit.w ) size.w = limit.w;
                if( size.h > limit.h ) size.h = limit.h;
            }
        static
            MVOID       not_smaller_than(MSize& size, MSize const& limit) {
                if( size.w < limit.w ) size.w = limit.w;
                if( size.h < limit.h ) size.h = limit.h;
            }
        static
            MSize       align_2(MSize const& size) {
#define align2(x)  ((x+1) & (~1))
                return MSize(align2(size.w), align2(size.h));
#undef align2
            }
        static
            MSize       scale_roundup(MSize const& size, int mul, int div) {
                return MSize((size.w * mul + div - 1) / div, (size.h * mul + div - 1) / div);
            }
    };
    //
    if ( ! mHwInfoHelper.queryPixelMode( mHwParams.mSensorMode, mHwParams.mSensorFps, mHwParams.mPixelMode ) )
        return UNKNOWN_ERROR;
    //
    MSize const sensorSize = mHwParams.mSensorSize;
    //
#if 1
#define MAX_PREVIEW_W           (2560)
#else
#define MAX_PREVIEW_W           (1920)
#endif
#define MIN_RRZO_RATIO_100X     (25)

#define CHECK_TARGET_SIZE(_msg_, _size_) \
        MY_LOGD_IF(0, "%s: target size(%dx%d)", _msg_, _size_.w, _size_.h);

    // estimate preview yuv max size
    MSize const max_preview_size = refine::align_2(
            MSize(MAX_PREVIEW_W, MAX_PREVIEW_W * sensorSize.h / sensorSize.w));
    //
    MSize maxYuvStreamSize;
    MSize largeYuvStreamSize;
    for (size_t i = 0; i < mConfigParams.vImage_Yuv_NonStall.size(); i++ )
    {
        MSize const streamSize = mConfigParams.vImage_Yuv_NonStall[i]->getImgSize();
        // if stream's size is suitable to use rrzo
        if( streamSize.w <= max_preview_size.w && streamSize.h <= max_preview_size.h )
            refine::not_smaller_than(maxYuvStreamSize, streamSize);
        else
            refine::not_smaller_than(largeYuvStreamSize, streamSize);
    }
    MY_LOGD_IF( !!maxYuvStreamSize, "max yuv stream size(%dx%d)",
                maxYuvStreamSize.w, maxYuvStreamSize.h);
    MY_LOGD_IF( !!largeYuvStreamSize, "large yuv stream size(%dx%d), burst capture",
                largeYuvStreamSize.w, largeYuvStreamSize.h );
    //
    mStreamingPipeParams.mbIsEIS     = ( mPipelineConfigParams.mbHasRecording && !mPipelineConfigParams.mOperation_mode &&
                   CamManager::getInstance()->getFirstUsingId()==mOpenId )?
                   MTRUE : MFALSE;
    MBOOL bDual =  CamManager::getInstance()->isMultiDevice();

    mStreamingPipeParams.mEisInfo.mode   = mStreamingPipeParams.mbIsEIS ? getEisMode() : EIS_MODE_OFF;
    mStreamingPipeParams.mEisInfo.factor = mStreamingPipeParams.mbIsEIS ? getEisRatio100X() : 100;
    mStreamingPipeParams.mbHasRsso = mStreamingPipeParams.mbIsEIS ? isNeedRsso(mStreamingPipeParams.mEisInfo.mode) : MFALSE;
    mStreamingPipeParams.mEisInfo.videoConfig = mPipelineConfigParams.mb4KRecording ? EISCustom::VIDEO_CFG_4K2K : EISCustom::VIDEO_CFG_FHD;
    if(EIS_MODE_IS_EIS_QUEUE_ENABLED(mStreamingPipeParams.mEisInfo.mode))
    {
        mStreamingPipeParams.mEisInfo.queueSize= EISCustom::getForwardFrames(mStreamingPipeParams.mEisInfo.videoConfig);
        mStreamingPipeParams.mEisInfo.startFrame = EISCustom::getForwardStartFrame();
    }

    // use resized raw if
    // 1. raw sensor
    // 2. some streams need this
    if( mHwInfoHelper.isRaw() )
    {
        //
        // currently, should always enable resized raw due to some reasons...
        //
        // initial value
        MSize target_rrzo_size = ( !bDual && mPipelineConfigParams.mb4KRecording )?
                                 largeYuvStreamSize : maxYuvStreamSize;
        CHECK_TARGET_SIZE("max yuv stream", target_rrzo_size);
        // apply limitations
        //  1. lower bounds
        {
            // get eis ownership and apply eis hw limitation
            if ( mStreamingPipeParams.mbIsEIS ) {
                MUINT32 minRrzoEisW = getMinRrzoEisW();
                MSize const min_rrzo_eis_size = refine::align_2(
                        MSize(minRrzoEisW, minRrzoEisW * sensorSize.h / sensorSize.w));
                refine::not_smaller_than(target_rrzo_size, min_rrzo_eis_size);
                target_rrzo_size = refine::align_2(
                        refine::scale_roundup(target_rrzo_size, mStreamingPipeParams.mEisInfo.factor, 100)
                        );
               CHECK_TARGET_SIZE("eis lower bound limitation", target_rrzo_size);
            }
            MSize const min_rrzo_hw_size = refine::align_2(
                    MSize(sensorSize.w*MIN_RRZO_RATIO_100X/100, sensorSize.h*MIN_RRZO_RATIO_100X/100) );
            refine::not_smaller_than(target_rrzo_size, min_rrzo_hw_size);
            CHECK_TARGET_SIZE("rrz hw lower bound limitation", target_rrzo_size);
        }
        //  2. upper bounds
        {
            if ( !mPipelineConfigParams.mb4KRecording )
            {
                refine::not_larger_than(target_rrzo_size, max_preview_size);
                CHECK_TARGET_SIZE("preview upper bound limitation", target_rrzo_size);
            }
            refine::not_larger_than(target_rrzo_size, sensorSize);
            CHECK_TARGET_SIZE("sensor size upper bound limitation", target_rrzo_size);
        }
        // 3. dual mode prevent 2-pixel mode.
        if ( mHwParams.mPixelMode!=ONE_PIXEL_MODE && bDual )
        {
            MY_LOGD_IF( 1, "pixel mode(%d) sensor(%dx%d)",
                        mHwParams.mPixelMode, sensorSize.w, sensorSize.h);
#define GET_MAX_RRZO_W(_x)      ( (_x)/2 - 32 )
            MSize const max_2pixel_bin_rrzo_size = refine::align_2(
                    MSize( GET_MAX_RRZO_W(sensorSize.w),
                           GET_MAX_RRZO_W(sensorSize.w)*sensorSize.h/sensorSize.w )
                    );
#undef GET_MAX_RRZO_W
            refine::not_larger_than(target_rrzo_size, max_2pixel_bin_rrzo_size);
            CHECK_TARGET_SIZE("2-pixel bin upper bound limitation", target_rrzo_size);
        }
        MY_LOGD_IF(1, "rrzo size(%dx%d)", target_rrzo_size.w, target_rrzo_size.h);
        //
        mHwParams.mResizedrawSize = target_rrzo_size;
        // check hw limitation with pixel mode & stride
        if( ! mHwInfoHelper.getRrzoFmt(10, mHwParams.mResizedrawFormat) ||
            ! mHwInfoHelper.alignRrzoHwLimitation(target_rrzo_size, mHwParams.mSensorSize, mHwParams.mResizedrawSize) ||
            ! mHwInfoHelper.alignPass1HwLimitation( mHwParams.mPixelMode, mHwParams.mResizedrawFormat,
                                                    MFALSE, mHwParams.mResizedrawSize, mHwParams.mResizedrawStride ) )
        {
            MY_LOGE("wrong params about rrzo");
            return BAD_VALUE;
        }
        MY_LOGI_IF( 1, "rrzo size(%dx%d) stride %zu",
                    mHwParams.mResizedrawSize.w, mHwParams.mResizedrawSize.h, mHwParams.mResizedrawStride);
    }
#undef CHECK_TARGET_SIZE
    //
    // use full raw, if
    // 1. jpeg stream (&& not met BW limit)
    // 2. raw stream
    // 3. opaque stream
    // 4. or stream's size is beyond rrzo's limit
    MBOOL useImgo =
        (mPipelineConfigParams.mbHasJpeg && ! mPipelineConfigParams.mbHasRecording) ||
        mPipelineConfigParams.mbHasRaw ||
        mReprocParams.mbOpaqueReproc ||
        !!largeYuvStreamSize;
#if MTKCAM_HAVE_ADV_SETTING
    // use full raw, if need to dump pure raw for BitTrue check
    useImgo = useImgo || mStreamingPipeParams.mForceEnableIMGO;
#endif
    if( useImgo )
    {
        mHwParams.mFullrawSize = sensorSize;
        // check hw limitation with pixel mode & stride
        if( ! mHwInfoHelper.getImgoFmt(10, mHwParams.mFullrawFormat) ||
            ! mHwInfoHelper.alignPass1HwLimitation( mHwParams.mPixelMode, mHwParams.mFullrawFormat, MTRUE,
                                                    mHwParams.mFullrawSize, mHwParams.mFullrawStride ) )
        {
            MY_LOGE("wrong params about imgo");
            return BAD_VALUE;
        }
        MY_LOGI_IF( 1, "imgo size(%dx%d) stride %zu",
                    mHwParams.mFullrawSize.w, mHwParams.mFullrawSize.h, mHwParams.mFullrawStride);
    }
    else
    {
        mHwParams.mFullrawSize = MSize(0,0);
    }
    return OK;
};

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
PipelineDefaultImp::MyProcessedParams::
isNeedLcso()
{

    MBOOL normalLTM = (MTKCAM_LTM_SUPPORT) != 0 ? MTRUE : MFALSE;
#if MTKCAM_HAVE_ADV_SETTING
    MBOOL izHDR = (mStreamingPipeParams.mpAdvSetting != NULL && (mStreamingPipeParams.mpAdvSetting->vhdrMode == SENSOR_VHDR_MODE_IVHDR
            || mStreamingPipeParams.mpAdvSetting->vhdrMode == SENSOR_VHDR_MODE_ZVHDR));
    MBOOL mHDR = (mStreamingPipeParams.mpAdvSetting != NULL && (mStreamingPipeParams.mpAdvSetting->vhdrMode == SENSOR_VHDR_MODE_MVHDR));

    return (izHDR || normalLTM) && (!mHDR);
#else
    return normalLTM;
#endif
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
PipelineDefaultImp::MyProcessedParams::
isNeedRsso(MUINT32 eisMode) const
{
    MBOOL ret = MFALSE;
    if( EIS_MODE_IS_EIS_30_ENABLED(eisMode) &&
        EIS_MODE_IS_EIS_IMAGE_ENABLED(eisMode) )
    {
        ret = MTRUE;
    }
    return ret;
}


/******************************************************************************
 *
 ******************************************************************************/
#define MIN_RRZO_EIS_W          (1280)
MUINT32
PipelineDefaultImp::MyProcessedParams::
getMinRrzoEisW()
{
    MUINT32 width = MIN_RRZO_EIS_W;
#if MTKCAM_HAVE_ADV_SETTING
    if( mStreamingPipeParams.mpAdvSetting != NULL )
    {
    }
#endif // MTKCAM_HAVE_ADV_SETTING
    return width;
}


/******************************************************************************
 *
 ******************************************************************************/
#define EIS_RATIO_100X          (120)  // src/dst = 1.2
MUINT32
PipelineDefaultImp::MyProcessedParams::
getEisRatio100X() const
{
    MUINT32 factor = EISCustom::getEIS12Factor(), adbFactor = 0;
#if MTKCAM_HAVE_ADV_SETTING
    if( mStreamingPipeParams.mpAdvSetting != NULL && isSupportAdvP2() )
    {
        factor = mStreamingPipeParams.mpAdvSetting->eisFactor;
    }
#endif // MTKCAM_HAVE_ADV_SETTING
    adbFactor = property_get_int32("debug.mtkcam.eis.factor", 0);
    MY_LOGI("EIS factor=%d, debug.mtkcam.eis.factor=%d", factor, adbFactor);
    if( adbFactor >= 100 )
    {
        factor = adbFactor;
    }
    return factor;
}


/******************************************************************************
 *
 ******************************************************************************/
MUINT32
PipelineDefaultImp::MyProcessedParams::
getEisMode() const
{
    MUINT32 eisMode = EIS_MODE_OFF;
#if MTKCAM_HAVE_ADV_SETTING
    if( isSupportAdvP2() )
    {
        eisMode = mStreamingPipeParams.mpAdvSetting->eisMode;
    }
#endif // MTKCAM_HAVE_ADV_SETTING
    if( property_get_int32("debug.mtkcam.eis.adv", 0) > 0 )
    {
        eisMode = (1<<EIS_MODE_EIS_30) | (1<<EIS_MODE_GYRO) | (1<<EIS_MODE_EIS_DEJELLO) | (1<<EIS_MODE_IMAGE) | (1<<EIS_MODE_EIS_QUEUE);
    }

    return eisMode;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::parsedAppRequest::
parse()
{
    struct categorize_img_stream
    {
        typedef KeyedVector< StreamId_T, sp<IImageStreamBuffer> >       IImageSBMapT;
        typedef KeyedVector< StreamId_T, sp<IImageStreamInfo> >         IImageInfoMapT;
        MERROR      operator()(
                IImageSBMapT& map,
                IImageInfoMapT* pMapRaw,
                IImageInfoMapT* pMapOpaque,
                IImageInfoMapT* pMapYuv,
                IImageInfoMapT* pMapJpeg
                )
        {
            for(size_t i = 0; i < map.size(); i++ )
            {
                sp<IImageStreamBuffer> buf = map.valueAt(i);
                if ( IImageStreamInfo const* pStreamInfo = buf->getStreamInfo() )
                {
                    IImageInfoMapT* pTargetMap = NULL;
                    switch( pStreamInfo->getImgFormat() )
                    {
                        //case eImgFmt_BAYER10: //TODO: not supported yet
                        //case eImgFmt_BAYER12:
                        //case eImgFmt_BAYER14:
                        case eImgFmt_RAW16:
                            pTargetMap = pMapRaw;
                            break;
                        case eImgFmt_CAMERA_OPAQUE:
                            pTargetMap = pMapOpaque;
                            break;
                            //
                        case eImgFmt_BLOB:
                            pTargetMap = pMapJpeg;
                            break;
                            //
                        case eImgFmt_YV12:
                        case eImgFmt_NV21:
                        case eImgFmt_YUY2:
                        case eImgFmt_Y8:
                        case eImgFmt_Y16:
                            pTargetMap = pMapYuv;
                            break;
                            //
                        default:
                            MY_LOGE("Unsupported format:0x%x", pStreamInfo->getImgFormat());
                            break;
                    }
                    if( pTargetMap == NULL ) {
                        MY_LOGE("cannot get target map");
                        return UNKNOWN_ERROR;
                    }
                    //
                    pTargetMap->add(
                            pStreamInfo->getStreamId(),
                            const_cast<IImageStreamInfo*>(pStreamInfo)
                            );
                }
            }
            return OK;
        }
    };
    //
    CHECK_ERROR(
            categorize_img_stream() (
                pRequest->vIImageBuffers,
                &vIImageInfos_Raw, &vIImageInfos_Opaque, &vIImageInfos_Yuv, NULL
                )
            );
    CHECK_ERROR(
            categorize_img_stream() (
                pRequest->vOImageBuffers,
                &vOImageInfos_Raw, &vOImageInfos_Opaque, &vOImageInfos_Yuv, &vOImageInfos_Jpeg
                )
            );
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
PipelineDefaultImp::
PipelineDefaultImp(
    MINT32 const openId,
    android::String8 const& name,
    wp<IPipelineModelMgr::IAppCallback> pAppCallback
)
    : mCommonInfo(std::make_shared<CommonInfo>(openId, name))
    , mParams(std::make_shared<MyProcessedParams>(openId))
{
    char value[PROPERTY_VALUE_MAX] = {'\0'};
    property_get("jpeg.rotation.enable", value, "1");
    int32_t enable = atoi(value);
    mCommonInfo->mJpegRotationEnable = (enable & 0x1)? MTRUE : MFALSE;
    MY_LOGD("Jpeg Rotation enable: %d", mCommonInfo->mJpegRotationEnable);
    //
#if MTKCAM_HAVE_ADV_SETTING
    mCommonInfo->mpAdvSettingMgr = AdvCamSettingMgr::getInstance(openId);
    if (mCommonInfo->mpAdvSettingMgr != NULL)
    {
        mCommonInfo->mpAdvSettingMgr->init();
    }
#endif
    mCommonInfo->mLogLevel = ::property_get_int32("debug.camera.log", 0);
    if ( mCommonInfo->mLogLevel == 0 ) {
        mCommonInfo->mLogLevel = ::property_get_int32("debug.camera.log.hwpipeline", 0);
    }

    mVendorHandler = new VendorHandler(mCommonInfo, mParams);
    if  ( mVendorHandler == nullptr ) {
        MY_LOGE("Bad mVendorHandler");
    }

    mConfigHandler  = new ConfigHandler(mCommonInfo, mParams, mVendorHandler);
    if  ( mConfigHandler == nullptr ) {
        MY_LOGE("Bad mConfigHandler");
    }

    mRequestHandler = new RequestHandler(mCommonInfo, mParams, pAppCallback, mConfigHandler, mVendorHandler);
    if  ( mRequestHandler == nullptr ) {
        MY_LOGE("Bad mRequestHandler");
    }
}


/******************************************************************************
 *
 ******************************************************************************/
PipelineDefaultImp::
~PipelineDefaultImp()
{
    FUNC_START;
    //
#if MTKCAM_HAVE_IVENDOR_SUPPORT
    plugin::NSVendorManager::remove(getOpenId());
#endif
    FUNC_END;
}

/******************************************************************************
 *
 ******************************************************************************/
void
PipelineDefaultImp::
onLastStrongRef(const void* /*id*/)
{
    FUNC_START;
    FUNC_END;
}


/******************************************************************************
 *
 ******************************************************************************/
/**
 * Given:
 *      App input meta streams
 *      App in/out image streams
 *
 * Action:
 *      Determine CONFIG stream set
 *      Determine I/O streams of each node
 *      Prepare Hal stream pools
 *      Configure each node (with their streams)
 *
 */
MERROR
PipelineDefaultImp::
configure(
    PipeConfigParams const& rConfigParams,
    android::sp<IPipelineModel> pOldPipeline
)
{
    RWLock::AutoRLock _l(mRWLock);
    if( mConfigHandler==nullptr )
        return DEAD_OBJECT;

    return mConfigHandler->configureLocked(rConfigParams, pOldPipeline);
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::
submitRequest(AppRequest& request)
{
    RWLock::AutoRLock _l(mRWLock);
    if( mRequestHandler==nullptr )
        return DEAD_OBJECT;

    return mRequestHandler->submitRequestLocked(request);
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
PipelineDefaultImp::
beginFlush()
{
    FUNC_START;
    //
    if( mCommonInfo->mpPipelineContext.get() )
        mCommonInfo->mpPipelineContext->flush();
    else
        MY_LOGW("no context");
    //
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
PipelineDefaultImp::
endFlush()
{
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
PipelineDefaultImp::
waitDrained()
{
    FUNC_START;
    if( mCommonInfo->mpPipelineContext.get() )
        mCommonInfo->mpPipelineContext->waitUntilDrained();
    FUNC_END;
}

/******************************************************************************
 *
 ******************************************************************************/
sp<PipelineContext>
PipelineDefaultImp::
getContext()
{
    FUNC_START;
    if( mCommonInfo->mpPipelineContext.get() )
        return mCommonInfo->mpPipelineContext;
    FUNC_END;
    return NULL;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
PipelineDefaultImp::
skipStream(
    MBOOL skipJpeg,
    IImageStreamInfo* pStreamInfo
) const
{
    if  (
            skipJpeg
        &&  pStreamInfo->getImgFormat() == HAL_PIXEL_FORMAT_BLOB
        &&  pStreamInfo->getImgSize().size() >= 1920*1080
        )
    {
 //&& limited mode
        return MTRUE;
    }

    return MFALSE;
}
