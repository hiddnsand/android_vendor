/**
 * {@hide}
 */
package com.mediatek.wfo;

interface IWifiOffloadListener {
    void onHandover(in int simIdx, in int stage, in int ratType);
    void onRoveOut(in int simIdx, in boolean roveOut, in int rssi);
    void onRequestImsSwitch(in int simIdx, in boolean isImsOn);
}
