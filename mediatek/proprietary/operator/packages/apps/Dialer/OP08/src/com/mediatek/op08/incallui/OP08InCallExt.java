package com.mediatek.op08.incallui;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.telecom.DisconnectCause;
import android.util.Log;

import com.android.ims.ImsManager;
import com.android.incallui.call.CallList;
import com.android.incallui.call.DialerCall;
import com.mediatek.incallui.ext.DefaultInCallExt;

/**
 * Plugin implementation for InCallExt.
 */
public class OP08InCallExt extends DefaultInCallExt {
    private static final String TAG = "OP08InCallExt";
    private Context mContext;

    /** Constructor.
     * @param context context
     */
    public OP08InCallExt(Context context) {
        mContext = context;
    }

    @Override
    public boolean showCongratsPopup(DisconnectCause disconnectCause) {
        Log.d(TAG, "showCongratsPopup disconnectCause = " + disconnectCause.getCode());
        DialerCall call = CallList.getInstance().getDisconnectedCall();
        if (ImsManager.isWfcEnabledByUser(mContext)
                && !(disconnectCause.getCode() == DisconnectCause.OTHER)
                && !(disconnectCause.getCode() == DisconnectCause.REJECTED)
                && !(disconnectCause.getCode() == DisconnectCause.MISSED)
                && !(disconnectCause.getCode() == DisconnectCause.ERROR)
                && !(disconnectCause.getCode() == DisconnectCause.BUSY)
                && call != null && call.hasProperty(android.telecom.Call.Details.PROPERTY_WIFI)) {
            showCongratsPopup();
            return true;
        }
        return false;
    }

    /**
     * show congrats popup.
     */
    private void showCongratsPopup() {
        int timer = 500;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "CongratsPopup shown");
                Intent intent = new Intent(mContext, AlertDialogActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        };
        Handler handler  = new Handler();
        handler.postDelayed(runnable, timer);
    }
}