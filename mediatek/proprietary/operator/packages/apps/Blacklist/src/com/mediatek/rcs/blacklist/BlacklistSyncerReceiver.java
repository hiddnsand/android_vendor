/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.rcs.blacklist;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import java.util.ArrayList;

/**
 * BlacklistSyncerReceiver.
 */
public class BlacklistSyncerReceiver extends BroadcastReceiver {
    private static final String TAG = "Blacklist_block";

    private ContentObserver mContentObserver;
    private Context mContext;
    private Handler mSyncHandler;

    private static final int EVENT_BLOCKED_NUMBER_CHANGED = 1;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        mContentObserver = new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                Log.d(TAG, TAG, "Blocked number changed...");
                mSyncHandler.sendEmptyMessage(EVENT_BLOCKED_NUMBER_CHANGED);
                super.onChange(selfChange);
            }
        };

        makeSyncHandler();
        Log.i(TAG, this, "Register observer");

        // It will always watch blocked number
        mContext.getContentResolver().registerContentObserver(
                BlacklistSyncBlocked.BLOCKED_URI, true, mContentObserver);
    }

    private void makeSyncHandler() {
        HandlerThread thread = new HandlerThread("sync-blocked-receiver");
        thread.start();

        mSyncHandler = new Handler(thread.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Log.d(TAG, TAG, "sync-receiver handleMessage " + msg.what);
                switch (msg.what) {
                    case EVENT_BLOCKED_NUMBER_CHANGED:
                        boolean deleting = BlacklistSyncBlocked.getInstance().getDeleting();
                        Log.i(TAG, TAG, "deleting:" + deleting);
                        if (!deleting) {
                            handleBlockedNumberChanged();
                        }
                        break;

                    default:
                        break;
                }
            }
        };
    }

    /**
     * handleBlockedNumberChanged.
     * query blocked number
     * query blacklist number
     * sync blocked nubmer to blacklist
     */
    private void handleBlockedNumberChanged() {
        ArrayList<String> blockList = new ArrayList<String>();
        ArrayList<String> blackList = new ArrayList<String>();
        ArrayList<String> addList = new ArrayList<String>();
        ArrayList<String> removeList = new ArrayList<String>();

        Log.i(TAG, this, "[handleBlockedNumberChanged]");
        mSyncHandler.removeMessages(EVENT_BLOCKED_NUMBER_CHANGED);

        Cursor blockedCursor = mContext.getContentResolver().query(
                BlacklistSyncBlocked.BLOCKED_URI,
                BlacklistSyncBlocked.BLOCKED_PROJECTION, null, null, null);
        try {
            while ((blockedCursor != null) && blockedCursor.moveToNext()) {
                String blockedNumber = blockedCursor.getString(0);
                Log.d(TAG, this, "newlist :" + Log.formatNumber(blockedNumber));
                blockList.add(blockedNumber);
            }

            Cursor blackCursor = mContext.getContentResolver().query(
                    BlacklistData.AUTHORITY_URI,
                    new String[] {BlacklistData.BlacklistTable.PHONE_NUMBER},
                    null, null, null);
            try {
                while ((blackCursor != null) && blackCursor.moveToNext()) {
                    String number = blackCursor.getString(0);
                    blackList.add(number);
                }
            } finally {
                if (blackCursor != null) {
                    blackCursor.close();
                }
            }

            if (blockList.size() == 0 && blackList.size() != 0) {
                removeNumberList(blackList);
            }

            for (String aNum : blockList) {
                if (!blackList.contains(aNum)) {
                    addList.add(aNum);
                    Log.d(TAG, this, "addList :" + Log.formatNumber(aNum));
                }
            }
            addNumberList(addList);

            for (String rNum : blackList) {
                if (!blockList.contains(rNum)) {
                    removeList.add(rNum);
                    Log.d(TAG, this, "removeList :" + Log.formatNumber(rNum));
                }
            }
            removeNumberList(removeList);

        } finally {
            if (blockedCursor != null) {
                blockedCursor.close();
            }
        }
    }

    private void removeNumberList(ArrayList<String> list) {
        Log.i(TAG, this, "[removeNumberList] start, " + list.size());
        for (String toRemvoeNumber : list) {
            Log.d(this, "delete number :" + Log.formatNumber(toRemvoeNumber));
            mContext.getContentResolver().delete(
                Uri.withAppendedPath(BlacklistData.AUTHORITY_URI, Uri.encode(toRemvoeNumber)),
                null, null);
        }
        Log.i(TAG, "this, [removeNumberList] end");
    }

    private void addNumberList(ArrayList<String> list) {
        Log.i(TAG, this, "[addNumberList] start, " + list.size());
        for (String number : list) {
            Log.d(this, "add number :" + Log.formatNumber(number));
            ContentValues blackVal = new ContentValues();
            blackVal.put(BlacklistData.BlacklistTable.PHONE_NUMBER, number);
            mContext.getContentResolver().insert(BlacklistData.AUTHORITY_URI, blackVal);
        }
        Log.i(TAG, this, "[addNumberList] end");
    }
}
