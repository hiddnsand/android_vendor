#ifndef _CAM_NOTIFY_H_
#define _CAM_NOTIFY_H_

#include <mtkcam/def/common.h>

/**
    callback path , callback at a user-inidicated timing.
*/
class P1_TUNING_NOTIFY
{
    public:
    virtual ~P1_TUNING_NOTIFY(){}
    virtual const char* TuningName(void) = 0;
    virtual void p1TuningNotify(MVOID *pInput,MVOID *pOutput)= 0;

    MVOID*  m_pClassObj;
};

typedef struct _LMV_INPUT_INFO{
    NSCam::MSize    sRMXOut;
    NSCam::MSize    sHBINOut;   //HDS
    NSCam::MSize    sTGOut;
    MBOOL    bYUVFmt;

    MUINT32  pixMode;

    struct{
        MUINT32 start_x;
        MUINT32 start_y;
        MUINT32 crop_size_w;
        MUINT32 crop_size_h;

        MUINT32 in_size_w;
        MUINT32 in_size_h;
    }RRZ_IN_CROP;
}LMV_INPUT_INFO;

typedef struct _LMV_SGG_CFG{
    MUINT32 PGN;
    MUINT32 GMRC_1;
    MUINT32 GMRC_2;
    MBOOL   bSGG2_Bypass;
    MBOOL   bSGG2_EN;
}LMV_SGG_CFG;

typedef struct _LMV_CFG
{
    MUINT32 cfg_lmv_prep_me_ctrl1;  // CAM_LMV_PREP_ME_CTRL1
    MUINT32 cfg_lmv_prep_me_ctrl2;  // CAM_LMV_PREP_ME_CTRL2
    MUINT32 cfg_lmv_lmv_th;         // CAM_LMV_LMV_TH
    MUINT32 cfg_lmv_fl_offset;      // CAM_LMV_FL_OFFSET
    MUINT32 cfg_lmv_mb_offset;      // CAM_LMV_MB_OFFSET
    MUINT32 cfg_lmv_mb_interval;    // CAM_LMV_MB_INTERVAL
    MUINT32 cfg_lmv_gmv;            // CAM_LMV_GMV, not use
    MUINT32 cfg_lmv_err_ctrl;       // CAM_LMV_ERR_CTRL, not use
    MUINT32 cfg_lmv_image_ctrl;     // CAM_LMV_IMAGE_CTRL
    MUINT32 enLMV;
    MUINT32 bypassLMV;
public :
    _LMV_CFG(MUINT32 a_lmvPrepMeCtrl1 = 0,
                 MUINT32 a_lmvPrepMeCtrl2 = 0,
                 MUINT32 a_lmvLmvTh = 0,
                 MUINT32 a_lmvFlOffset = 0,
                 MUINT32 a_lmvMbOffset = 0,
                 MUINT32 a_lmvMbInterval = 0,
                 MUINT32 a_lmvGmv = 0,
                 MUINT32 a_lmvErrCtrl = 0,
                 MUINT32 a_lmvImageCtrl = 0)
                          : cfg_lmv_prep_me_ctrl1(a_lmvPrepMeCtrl1),
                            cfg_lmv_prep_me_ctrl2(a_lmvPrepMeCtrl2),
                            cfg_lmv_lmv_th(a_lmvLmvTh),
                            cfg_lmv_fl_offset(a_lmvFlOffset),
                            cfg_lmv_mb_offset(a_lmvMbOffset),
                            cfg_lmv_mb_interval(a_lmvMbInterval),
                            cfg_lmv_gmv(a_lmvGmv),
                            cfg_lmv_err_ctrl(a_lmvErrCtrl),
                            cfg_lmv_image_ctrl(a_lmvImageCtrl)

    {
        enLMV = 0;
        bypassLMV = 0;
    }
}LMV_CFG;

typedef struct _RSS_CROP_SIZE{
    MFLOAT   w_start;
    MFLOAT   h_start;
    MUINT32 w_size;
    MUINT32 h_size;
}RSS_CROP_SIZE;

#endif

