#ifndef _DAEMON_H
#define _DAEMON_H

extern uint64_t STORAGE_FULL_EVENT;
extern uint64_t FOLDER_REMOVED_EVENT;
extern uint64_t SIG_STOPLOG_EVENT;
extern int event_fd;

int make_full_path(void);
void handle_client_msg(void *cli_msg);
void maybe_config_msg(char *msg);
void wait_client(void *socket_id);
int setup_socket(void);
int set_redirect_flag(void);

#endif
