/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Win32 Window implementation.
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <windows.h>                  /* Win32 API functions                 */
#include <windowsx.h>                 /* For GET_X_L_PARAM() etc.            */
#include <egl/egl.h>                  /* OpenGL EGL                          */
#include <a3m/fpmaths.h>              /* for clamp()                         */
#include <a3m/log.h>                  /* A3M log functions                   */
#include <a3m/window.h>               /* This module's header                */
#include <eglcontext.h>               /* for EglContext                      */

namespace
{

  using namespace a3m;

  A3M_CHAR8 const* WINDOW_CLASS_NAME = "A3MWindow";

  /*
   * Converts a Win32 mouse key to a mouse button value
   */
  MouseButton convertMouseKey(A3M_UINT32 key)
  {
    // Win32 mouse key code can encode multiple mouse buttons at once.  We only
    // pay attention to one at a time, with priority: LEFT > RIGHT > MIDDLE
    if (key & MK_LBUTTON)
    {
      return MOUSE_BUTTON_LEFT;
    }
    else if (key & MK_RBUTTON)
    {
      return MOUSE_BUTTON_RIGHT;
    }
    else if (key & MK_MBUTTON)
    {
      return MOUSE_BUTTON_MIDDLE;
    }
    else
    {
      A3M_LOG_WARN("Mouse button %d (0x%X) is not supported.", key, key);
      return MOUSE_BUTTON_UNKNOWN;
    }
  }

  /*
   * Converts a Win32 virtual key to a Keycode
   */
  KeyCode convertVirtualKey(A3M_UINT32 key)
  {
    switch (key)
    {
    case '0':          return KEY_0;
    case '1':          return KEY_1;
    case '2':          return KEY_2;
    case '3':          return KEY_3;
    case '4':          return KEY_4;
    case '5':          return KEY_5;
    case '6':          return KEY_6;
    case '7':          return KEY_7;
    case '8':          return KEY_8;
    case '9':          return KEY_9;
    case 'A':          return KEY_A;
    case 'B':          return KEY_B;
    case 'C':          return KEY_C;
    case 'D':          return KEY_D;
    case 'E':          return KEY_E;
    case 'F':          return KEY_F;
    case 'G':          return KEY_G;
    case 'H':          return KEY_H;
    case 'I':          return KEY_I;
    case 'J':          return KEY_J;
    case 'K':          return KEY_K;
    case 'L':          return KEY_L;
    case 'M':          return KEY_M;
    case 'N':          return KEY_N;
    case 'O':          return KEY_O;
    case 'P':          return KEY_P;
    case 'Q':          return KEY_Q;
    case 'R':          return KEY_R;
    case 'S':          return KEY_S;
    case 'T':          return KEY_T;
    case 'U':          return KEY_U;
    case 'V':          return KEY_V;
    case 'W':          return KEY_W;
    case 'X':          return KEY_X;
    case 'Y':          return KEY_Y;
    case 'Z':          return KEY_Z;

    case VK_OEM_8:      return KEY_GRAVE;
    case VK_OEM_MINUS:  return KEY_MINUS;
    case VK_OEM_PLUS:   return KEY_EQUALS;
    case VK_OEM_4:      return KEY_OPEN_BRACKET;
    case VK_OEM_6:      return KEY_CLOSE_BRACKET;
    case VK_OEM_1:      return KEY_SEMICOLON;
    case VK_OEM_3:      return KEY_APOSTROPHE;
    case VK_OEM_7:      return KEY_HASH;
    case VK_OEM_5:      return KEY_BACKSLASH;
    case VK_OEM_COMMA:  return KEY_COMMA;
    case VK_OEM_PERIOD: return KEY_PERIOD;
    case VK_OEM_2:      return KEY_SLASH;

    case VK_ESCAPE:    return KEY_ESCAPE;
    case VK_TAB:       return KEY_TAB;
    case VK_CAPITAL:   return KEY_CAPS_LOCK;

    case VK_SHIFT:
    case VK_LSHIFT:
    case VK_RSHIFT:
      return KEY_SHIFT;

    case VK_CONTROL:
    case VK_LCONTROL:
    case VK_RCONTROL:
      return KEY_CONTROL;

    case VK_MENU:
    case VK_LMENU:
    case VK_RMENU:
      return KEY_ALT;

    case VK_LWIN:
    case VK_RWIN:
      return KEY_SUPER;

    case VK_APPS: return KEY_MENU;

    case VK_SPACE:     return KEY_SPACE;
    case VK_BACK:      return KEY_BACKSPACE;
    case VK_RETURN:    return KEY_RETURN;

    case VK_SNAPSHOT:  return KEY_PRINT_SCREEN;
    case VK_SCROLL:    return KEY_SCROLL_LOCK;
    case VK_PAUSE:     return KEY_PAUSE;

    case VK_INSERT:    return KEY_INSERT;
    case VK_DELETE:    return KEY_DELETE;
    case VK_HOME:      return KEY_HOME;
    case VK_END:       return KEY_END;
    case VK_PRIOR:     return KEY_PAGE_UP;
    case VK_NEXT:      return KEY_PAGE_DOWN;

    case VK_UP:        return KEY_UP;
    case VK_DOWN:      return KEY_DOWN;
    case VK_LEFT:      return KEY_LEFT;
    case VK_RIGHT:     return KEY_RIGHT;

    case VK_NUMLOCK:   return KEY_NUM_LOCK;
    case VK_NUMPAD0:   return KEY_NUMPAD_0;
    case VK_NUMPAD1:   return KEY_NUMPAD_1;
    case VK_NUMPAD2:   return KEY_NUMPAD_2;
    case VK_NUMPAD3:   return KEY_NUMPAD_3;
    case VK_NUMPAD4:   return KEY_NUMPAD_4;
    case VK_NUMPAD5:   return KEY_NUMPAD_5;
    case VK_NUMPAD6:   return KEY_NUMPAD_6;
    case VK_NUMPAD7:   return KEY_NUMPAD_7;
    case VK_NUMPAD8:   return KEY_NUMPAD_8;
    case VK_NUMPAD9:   return KEY_NUMPAD_9;
    case VK_ADD:       return KEY_NUMPAD_ADD;
    case VK_SUBTRACT:  return KEY_NUMPAD_SUBTRACT;
    case VK_MULTIPLY:  return KEY_NUMPAD_MULTIPLY;
    case VK_DIVIDE:    return KEY_NUMPAD_DIVIDE;
    case VK_DECIMAL:   return KEY_NUMPAD_DECIMAL;
    case VK_SEPARATOR: return KEY_NUMPAD_SEPARATOR;

    case VK_F1:        return KEY_F1;
    case VK_F2:        return KEY_F2;
    case VK_F3:        return KEY_F3;
    case VK_F4:        return KEY_F4;
    case VK_F5:        return KEY_F5;
    case VK_F6:        return KEY_F6;
    case VK_F7:        return KEY_F7;
    case VK_F8:        return KEY_F8;
    case VK_F9:        return KEY_F9;
    case VK_F10:       return KEY_F10;
    case VK_F11:       return KEY_F11;
    case VK_F12:       return KEY_F12;
    case VK_F13:       return KEY_F13;
    case VK_F14:       return KEY_F14;
    case VK_F15:       return KEY_F15;
    case VK_F16:       return KEY_F16;
    case VK_F17:       return KEY_F17;
    case VK_F18:       return KEY_F18;
    case VK_F19:       return KEY_F19;
    case VK_F20:       return KEY_F20;
    case VK_F21:       return KEY_F21;
    case VK_F22:       return KEY_F22;
    case VK_F23:       return KEY_F23;
    case VK_F24:       return KEY_F24;

    case VK_CANCEL:    return KEY_CANCEL;
    case VK_PLAY:      return KEY_PLAY;
    case VK_ZOOM:      return KEY_ZOOM;

    case VK_CLEAR:     return KEY_CLEAR;
    case VK_EXECUTE:   return KEY_EXEC;
    case VK_HELP:      return KEY_HELP;
    case VK_PRINT:     return KEY_PRINT;
    case VK_SELECT:    return KEY_SELECT;

    default:
      A3M_LOG_WARN("Key %d (0x%X) is not supported.", key, key);
      return KEY_UNKNOWN;
    }
  }

  /*
   * Implementation of Window interface for Microsoft Windows.
   */
  class WindowWin32 : public Window
  {
  public:
    /*
     * Constructor
     */
    WindowWin32(
      const A3M_CHAR8* title,
      A3M_INT32 width,
      A3M_INT32 height,
      A3M_INT32 bpp,
      A3M_BOOL setClientArea);

    /*
     * Destructor
     */
    ~WindowWin32();

    // Override
    void refresh();
    // Override
    void setListener(WindowListener* listener);
    // Override
    void setSize(A3M_INT32 width, A3M_INT32 height);
    // Override
    A3M_INT32 getLeft() const;
    // Override
    A3M_INT32 getTop() const;
    // Override
    A3M_INT32 getWidth() const;
    // Override
    A3M_INT32 getHeight() const;
    // Override
    A3M_UINT32 getId() const;

    /*
     * Processes window events.
     */
    void processMessage(
      HWND hWnd,
      UINT message,
      WPARAM wParam,
      LPARAM lParam);

  private:
    A3M_BOOL m_mouseIsCaptured;   /* Whether mouse is captured in window    */
    HWND m_hWnd;                  /* Window handle                          */
    HINSTANCE m_hInstance;        /* Module handle                          */
    A3M_INT32 m_left;             /* Window left edge position              */
    A3M_INT32 m_top;              /* Window top edge position               */
    A3M_INT32 m_width;            /* Window width                           */
    A3M_INT32 m_height;           /* Window height                          */
    EglContext::Ptr m_context;    /* EGL Context                            */
    WindowListener* m_listener;   /* Event listener                         */
  };

  /*
   * Windows message callback
   */
  LRESULT CALLBACK onMessage(
    HWND hWnd,
    UINT message,
    WPARAM wParam,
    LPARAM lParam)
  {
    if (hWnd)
    {
      WindowWin32* window = reinterpret_cast<WindowWin32*>(
                              GetWindowLongPtr(hWnd, GWLP_USERDATA));

      if (window)
      {
        window->processMessage(hWnd, message, wParam, lParam);
      }
    }

    if (message == WM_CLOSE)
    {
      return 0;
    }

    return DefWindowProc(hWnd, message, wParam, lParam);
  }

  WindowWin32::WindowWin32(
    const A3M_CHAR8* title,
    A3M_INT32 width,
    A3M_INT32 height,
    A3M_INT32 bpp,
    A3M_BOOL setClientArea) :
    m_mouseIsCaptured(A3M_FALSE),
    m_hWnd(0),
    m_hInstance(0),
    m_left(0),
    m_top(0),
    m_width(width),
    m_height(height),
    m_listener(0)
  {
    WNDCLASS  wc;
    DWORD     windowStyle;
    A3M_INT32 windowX, windowY;

    m_hInstance = GetModuleHandle(0);

    if (!m_hInstance)
    {
      A3M_LOG_ERROR("Failed to get module handle.");
      return;
    }

    /* register class */
    wc.style         = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc   = (WNDPROC)onMessage;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = m_hInstance;
    wc.hIcon         = LoadIcon(0, IDI_WINLOGO);
    wc.hCursor       = LoadCursor(0, IDC_ARROW);
    wc.hbrBackground = 0;
    wc.lpszMenuName  = 0;
    wc.lpszClassName = WINDOW_CLASS_NAME;

    if (!RegisterClass(&wc))
    {
      A3M_LOG_ERROR("Failed to register window class.");
      return;
    }

    /* init instance */
    windowStyle  = WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_VISIBLE;
    windowStyle |= WS_OVERLAPPEDWINDOW;

    // Disable window resizing.
    windowStyle &= ~WS_SIZEBOX;
    windowStyle &= ~WS_MAXIMIZEBOX;

    windowX      = CW_USEDEFAULT;
    windowY      = 0;

    RECT rect = {0, 0, width, height};
    if (setClientArea && AdjustWindowRectEx(&rect, windowStyle, FALSE, WS_EX_APPWINDOW))
    {
      width = rect.right - rect.left;
      height = rect.bottom - rect.top;
    }

    m_hWnd = CreateWindow(WINDOW_CLASS_NAME, title, windowStyle,
                          windowX, windowY,
                          width, height,
                          0, 0, m_hInstance, 0);

    if (!m_hWnd)
    {
      A3M_LOG_ERROR("Failed to create window.");
      return;
    }

    // Store a pointer to the Window class in the Win32 window, so that we can
    // redirect event messages to the correct class.
    SetWindowLongPtr(m_hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));

    if (ShowWindow(m_hWnd, SW_SHOW))
    {
      A3M_LOG_INFO("Window was previously shown.");
    }

    if (!UpdateWindow(m_hWnd))
    {
      A3M_LOG_ERROR("Failed to update Window.");
    }

    m_context.reset(new EglContext(GetDC(m_hWnd), m_hWnd));

    if (!m_context->isValid())
    {
      A3M_LOG_ERROR("Failed to create a rendering context.");
      return;
    }
  }

  WindowWin32::~WindowWin32()
  {
    if (m_hWnd)
    {
      /* Destroy the rendering context */
      m_context.reset();

      /* Close the window */
      if (!DestroyWindow(m_hWnd))
      {
        A3M_LOG_ERROR("Failed to destroy window.");
      }

      if (!UnregisterClass(WINDOW_CLASS_NAME, m_hInstance))
      {
        A3M_LOG_ERROR("Failed to unregister window class.");
      }
    }
  }

  void WindowWin32::refresh()
  {
    /* Swap buffers before dispatching messages in case dispatching messages
     * causes a picking operation which might corrupt the back buffer */
    m_context->swapBuffers();

    MSG msg;

    while (PeekMessage(&msg, 0, 0U, 0U, PM_REMOVE))
    {
      if (TranslateMessage(&msg))
      {
        /* A virtual-key message has successfully been translated to a WM_CHAR
         * message.  Nothing to do. */
      }

      DispatchMessage(&msg);
    }
  }

  void WindowWin32::setListener(WindowListener* listener)
  {
    m_listener = listener;
  }

  // Switch window orientation reference the window's top/left corner.
  void WindowWin32::setSize(A3M_INT32 width, A3M_INT32 height)
  {
    SetWindowPos (m_hWnd, HWND_TOPMOST, 0, 0,
                  width + GetSystemMetrics(SM_CXFIXEDFRAME) * 2,
                  height + GetSystemMetrics(SM_CYFIXEDFRAME) * 2 +
                  GetSystemMetrics(SM_CYCAPTION), SWP_NOMOVE );
  }

  A3M_INT32 WindowWin32::getLeft() const
  {
    return m_left;
  }

  A3M_INT32 WindowWin32::getTop() const
  {
    return m_top;
  }

  A3M_INT32 WindowWin32::getWidth() const
  {
    return m_width;
  }

  A3M_INT32 WindowWin32::getHeight() const
  {
    return m_height;
  }

  A3M_UINT32 WindowWin32::getId() const
  {
    return reinterpret_cast<A3M_UINT32>(m_hWnd);
  }

  void WindowWin32::processMessage(
    HWND hWnd,
    UINT message,
    WPARAM wParam,
    LPARAM lParam)
  {
    A3M_ASSERT(m_hWnd == hWnd);

    if (m_listener)
    {
      switch (message)
      {
      case WM_CLOSE:
        m_listener->onClose(this);
        break;

      case WM_SIZE:
        m_width = LOWORD(lParam);
        m_height = HIWORD(lParam);
        m_listener->onResize(this, m_width, m_height);
        break;

      case WM_MOVE:
        m_left = GET_X_LPARAM(lParam);
        m_top = GET_Y_LPARAM(lParam);
        m_listener->onMove(this, m_left, m_top);
        break;

      case WM_KEYUP:
        m_listener->onKeyUp(this, convertVirtualKey(wParam));
        break;

      case WM_KEYDOWN:
        m_listener->onKeyDown(this, convertVirtualKey(wParam));
        break;

      case WM_MOUSEMOVE:
      {
        A3M_INT32 x = GET_X_LPARAM(lParam);
        A3M_INT32 y = GET_Y_LPARAM(lParam);
        m_listener->onMouseMove(this, x, y);
        break;
      }

      case WM_LBUTTONDOWN:
      case WM_RBUTTONDOWN:
      case WM_MBUTTONDOWN:
      {
        MouseButton button;

        switch (message)
        {
        case WM_LBUTTONDOWN: button = MOUSE_BUTTON_LEFT; break;
        case WM_RBUTTONDOWN: button = MOUSE_BUTTON_RIGHT; break;
        case WM_MBUTTONDOWN: button = MOUSE_BUTTON_MIDDLE; break;
        }

        A3M_INT32 x = GET_X_LPARAM(lParam);
        A3M_INT32 y = GET_Y_LPARAM(lParam);
        m_listener->onMouseButtonDown(this, button, x, y);
        break;
      }

      case WM_LBUTTONUP:
      case WM_RBUTTONUP:
      case WM_MBUTTONUP:
      {
        ReleaseCapture();
        m_mouseIsCaptured = A3M_FALSE;

        MouseButton button;

        switch (message)
        {
        case WM_LBUTTONUP: button = MOUSE_BUTTON_LEFT; break;
        case WM_RBUTTONUP: button = MOUSE_BUTTON_RIGHT; break;
        case WM_MBUTTONUP: button = MOUSE_BUTTON_MIDDLE; break;
        }

        A3M_INT32 x = GET_X_LPARAM(lParam);
        A3M_INT32 y = GET_Y_LPARAM(lParam);
        m_listener->onMouseButtonUp(this, button, x, y);
        break;
      }

      case WM_MOUSEWHEEL:
      {
        A3M_INT32 delta = GET_WHEEL_DELTA_WPARAM(wParam);
        m_listener->onMouseWheelScroll(this, delta);
        break;
      }

      default:
        break;
      }
    }

    // Special processing to capture mouse if a mouse button is clicked inside
    // of the window area, so that the specific position of the mouse when it
    // leaves the window can be obtained.
    if (message == WM_MOUSEMOVE)
    {
      A3M_INT32 x = GET_X_LPARAM(lParam);
      A3M_INT32 y = GET_Y_LPARAM(lParam);

      RECT clientRect;
      GetClientRect(m_hWnd, &clientRect);

      A3M_BOOL const mouseInClientRect =
        clientRect.left <= x && x < clientRect.right &&
        clientRect.top <= y && y < clientRect.bottom;

      if (m_mouseIsCaptured)
      {
        if (!mouseInClientRect)
        {
          ReleaseCapture();
          m_mouseIsCaptured = A3M_FALSE;

          if (m_listener)
          {
            A3M_INT32 leaveX = clamp<A3M_INT32>(
                                 x, clientRect.left, clientRect.right - 1);
            A3M_INT32 leaveY = clamp<A3M_INT32>(
                                 y, clientRect.top, clientRect.bottom - 1);

            m_listener->onMouseLeave(this, leaveX, leaveY);
          }
        }
      }
      else if (mouseInClientRect &&
               (wParam & (MK_LBUTTON | MK_RBUTTON | MK_RBUTTON)))
      {
        /* Track the mouse when it moves outside the window.
           Using SetCapture rather than TrackMouseEvent / WM_MOUSELEAVE
           so that we obtain the latest mouse position. */
        m_mouseIsCaptured = A3M_TRUE;
        SetCapture(m_hWnd);
      }
    }
  }

} // namespace

namespace a3m
{

  /*
   * Factory function.
   */
  Window::Ptr createWindow(
    const A3M_CHAR8* title,
    A3M_INT32 width,
    A3M_INT32 height,
    A3M_INT32 bpp,
    A3M_BOOL setClientArea)
  {
    return Window::Ptr(
             new WindowWin32(title, width, height, bpp, setClientArea));
  }

} // namespace a3m
