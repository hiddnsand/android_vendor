#ifndef HWC_DISPATCHER_H_
#define HWC_DISPATCHER_H_

#include <utils/threads.h>
#include <utils/KeyedVector.h>
#include <utils/BitSet.h>

#include <hardware/hwcomposer.h>
#include "ui/gralloc_extra.h"

#include "utils/tools.h"

#include "display.h"
#include "worker.h"
#include "composer.h"
#include "hwc_priv.h"

using namespace android;

struct dump_buff;
class SyncFence;

// ---------------------------------------------------------------------------

enum {
    HWC_LAYER_TYPE_INVALID = 0,
    HWC_LAYER_TYPE_FBT     = 1,
    HWC_LAYER_TYPE_UI      = 2,
    HWC_LAYER_TYPE_MM      = 3,
};

enum HWC_POST_STATE
{
    HWC_POST_INVALID        = 0x0000,
    HWC_POST_OUTBUF_DISABLE = 0x0010,
    HWC_POST_OUTBUF_ENABLE  = 0x0011,
    HWC_POST_INPUT_NOTDIRTY = 0x0100,
    HWC_POST_INPUT_DIRTY    = 0x0101,

    HWC_POST_CONTINUE_MASK  = 0x0001,
};

enum {
    HWC_SEQUENCE_INVALID = 0,
};

// HWLayer is used to store information of layers selected
// to be processed by hardware composer
struct HWLayer
{
    // used to determine if this layer is enabled
    bool enable;

    // hwc_layer index in hwc_display_contents_1
    int index;

    // identify if layer should be handled by UI or MM path
    int type;

    // identify if layer has dirty pixels
    bool dirty;

    union
    {
        // information needed for UI layer
        struct
        {
            // mva is the mapping va for layer buffer
            int mva;

            // ion_fd is the ion fd for layer buffer
            int ion_fd;

#ifdef MTK_HWC_PROFILING
            // fbt_input_layers is amount of layers handled by gles
            int fbt_input_layers;

            // fbt_input_bytes is bytes of layers handled by gles
            int fbt_input_bytes;
#endif

            // fake_ovl_id is used by fbt of fake display
            int fake_ovl_id;
        };

        // information needed for MM layer
        struct
        {
            // disp_fence is used to queue back
            // to display buffer queue at MM thread processing
            int disp_fence;
        };
    };

    // release fence index from display driver
    unsigned int fence_index;

    // hwc_layer_1 from SurfaceFlinger
    hwc_layer_1 layer;

    // buffer informations
    hwc_buffer_info_t buffer_info;

    // for dirty information
    gralloc_extra_ion_sf_info_t ext_info;
};

// HWOutbuf is used to store information for
// outbuf of virtual display
struct HWOutbuf
{
    HWOutbuf()
        : ion_fd(-1)
        , handle(NULL)
    { }

    // ion_fd is the ion fd for output buffer
    int ion_fd;

    // out_acq_fence_fd is used for hwc to know if outbuf could be read
    int out_acquire_fence_fd;

    // out_retire_fence_fd is used to notify producer
    // if output buffer could be wrote
    int out_retire_fence_fd;

    // out_retire_fence_idx is retire fence index from display driver
    unsigned int out_retire_fence_idx;

    // outbuf native handle
    buffer_handle_t handle;

    // buffer informations
    hwc_buffer_info_t buffer_info;

    // private handle information
    PrivateHandle priv_handle;
};

// DispatcherJob is used as a job unit for Dispatcher Thread
struct DispatcherJob
{
    DispatcherJob()
        : enable(false)
        , display_id(0)
        , num_layers(0)
        , fbt_exist(false)
        , need_sync(false)
        , num_ui_layers(0)
        , num_mm_layers(0)
        , post_state(HWC_POST_INVALID)
        , sequence(HWC_SEQUENCE_INVALID)
        , timestamp(0)
        , hasCameraLayer(false)
    { }

    // check if job should be processed
    bool enable;

    // display id
    int display_id;

    // amount of hwcLayers
    int num_layers;

    // check if fbt is used
    bool fbt_exist;

    // check if UI and MM should be synced
    bool need_sync;

    // amount of layers for ui thread
    int num_ui_layers;

    // amount of layers for mm thread
    int num_mm_layers;

    // used to determine if need to trigger ui/mm thread
    int post_state;

    // used as a sequence number for profiling latency purpose
    unsigned int sequence;

    // used for video frame
    unsigned int timestamp;

    // input layers for composing
    HWLayer* hw_layers;

    // output buffer for virtual display
    HWOutbuf hw_outbuf;

    // workaround to solve MDP blit time too long
    bool hasCameraLayer;
};

// HWCDispatcher is used to dispatch job to
// UILayerComposer and MMLayerComposer
class HWCDispatcher : public HWCThread
{
public:
    HWCDispatcher();

    virtual ~HWCDispatcher();

    // onPlugIn() is used to notify if a new display is added
    void onPlugIn(int dpy);

    // onPlugOut() is used to notify if a new display is removed
    void onPlugOut(int dpy);

    // onBlank() is used to wait display thread idle when blanking display
    void onBlank(int dpy, int blank);

    // onVSync() is used to receive vsync signal
    void onVSync(int dpy);

    // getJob() is used for HWCMediator to get a new job for filling in
    DispatcherJob* getJob(int dpy);

    // setJob() is used to update jobs in hwc::set()
    void setJob(int dpy, struct hwc_display_contents_1* list);

    // prepare() is used to for HWCMediator for preparing a new job group
    void prepare();

    // set() is used to queue job group and it would be handled in threadLoop()
    void set();

    // dump() is used for debugging purpose
    void dump(struct dump_buff* log, int dump_level);

    int getJobDepth();

    // saveFbt() is used to save present FBT handle
    // return true, if present FBT is the different with previous
    bool saveFbtHandle(int dpy, buffer_handle_t handle);

private:
    virtual void onFirstRef();
    virtual bool threadLoop();

    void releaseResource(int dpy);

    void waitNextVSync();

    mutable Mutex m_vsync_lock;
    Condition m_vsync_cond;
    bool m_vsync_signaled;

    mutable Mutex m_plug_lock_loop;
    mutable Mutex m_plug_lock_main;

    BitSet32 m_alloc_disp_ids;

    // DispatcherJob is a job unit for single display.
    // DispatcherJobGroup is used for grouping jobs for different display
    // which need to be handled at same composition period.
    struct DispatcherJobGroup
    {
        DispatcherJob* jobs[DisplayManager::MAX_DISPLAYS];
    };
    // m_curr_group points the job group used between prepare() and set()
    DispatcherJobGroup* m_curr_group;

    // m_group_queue is job group list
    // which new job group would be queued in set()
    typedef Vector<DispatcherJobGroup*> Fifo;
    Fifo m_group_queue;

    class PostHandler : public LightRefBase<PostHandler>
    {
    public:
        PostHandler(int dpy, const sp<OverlayEngine>& ovl_engine);
        virtual ~PostHandler();

        // set() is used to get retired fence from display driver
        virtual void set(struct hwc_display_contents_1* list, DispatcherJob* job) = 0;

        // process() is used to wait outbuf is ready to use
        // and sets output buffer to display driver
        virtual void process(DispatcherJob* job) = 0;

    protected:
        // m_disp_id is used to identify which display
        int m_disp_id;

        // m_ovl_engine is used for config overlay engine
        sp<OverlayEngine> m_ovl_engine;

        // m_sync_fence is used to create or wait fence
        sp<SyncFence> m_sync_fence;
    };

    class PhyPostHandler : public PostHandler
    {
    public:
        PhyPostHandler(int dpy, const sp<OverlayEngine>& ovl_engine)
            : PostHandler(dpy, ovl_engine)
        { }

        virtual void set(struct hwc_display_contents_1* list, DispatcherJob* job);

        virtual void process(DispatcherJob* job);
    };

    class VirPostHandler : public PostHandler
    {
    public:
        VirPostHandler(int dpy, const sp<OverlayEngine>& ovl_engine)
            : PostHandler(dpy, ovl_engine)
        { }

        virtual void set(struct hwc_display_contents_1* list, DispatcherJob* job);

        virtual void process(DispatcherJob* job);

    private:
        void setError(DispatcherJob* job);
    };

    // WorkerCluster is used for processing composition of single display.
    // One WokerCluster would creates two threads to handle UI or MM layers.
    // Different display would use different WorkCluster to handle composition.
    struct WorkerCluster
    {
        WorkerCluster()
            : enable(false)
            , ovl_engine(NULL)
            , ui_thread(NULL)
            , mm_thread(NULL)
            , post_handler(NULL)
            , sync_ctrl(NULL)
        { }

        bool enable;

        sp<OverlayEngine> ovl_engine;
        sp<UILayerComposer> ui_thread;
        sp<MMLayerComposer> mm_thread;
        sp<PostHandler> post_handler;
        sp<SyncControl> sync_ctrl;
    };
    // m_workers is the WorkerCluster array used by different display composition.
    WorkerCluster m_workers[DisplayManager::MAX_DISPLAYS];

    // m_sequence is used as a sequence number for profiling latency purpose
    // initialized to 1, (0 is reserved to be an error code)
    unsigned int m_sequence;
    int jobDepth;

    // used to save the handle of previous fbt. When need to skip redundant
    // composition, compare the present fbt.
    buffer_handle_t m_prev_fbt[DisplayManager::MAX_DISPLAYS];

public:
    // enable/disable skip redundant composition
    bool m_disable_skip_redundant;
};

#endif // HWC_DISPATCHER_H_
