LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/external/yv12_util))
include $(CLEAR_VARS)
LOCAL_MODULE = libyv12util
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX = .so
LOCAL_MODULE_TAGS = optional
LOCAL_SHARED_LIBRARIES = libnativehelper libcutils libdl liblog libskia libutils libui libandroid libgui libEGL libGLESv2 libandroid_runtime
LOCAL_MULTILIB = 32
LOCAL_SRC_FILES_32 = arm/libyv12util.so
include $(BUILD_PREBUILT)
endif
