#define DEBUG_LOG_TAG "HWC"

#include <cutils/properties.h>

#include "utils/debug.h"
#include "utils/tools.h"

#include "hwc.h"
#include "hwdev.h"
#include "platform.h"
#include "display.h"
#include "overlay.h"
#include "dispatcher.h"
#include "worker.h"
#include "composer.h"
#include "bliter.h"
#include "sync.h"
#include "service.h"

#include "Transform.h"
#include "ui/gralloc_extra.h"

// ---------------------------------------------------------------------------

// init static vars
DevicePlatform::PlatformConfig DevicePlatform::m_config;

// init global vars
sp<HWCDispatcher> g_dispatch_thread = NULL;

#ifdef MTK_HWC_PROFILING
bool g_handle_all_layers = false;
bool g_prepare_all_layers = false;
#endif

// ---------------------------------------------------------------------------

ANDROID_SINGLETON_STATIC_INSTANCE(HWCMediator);

HWCMediator::HWCMediator()
{
    // check features setting
    initFeatures();

    MMUDevice& mmu_dev(MMUDevice::getInstance());

    // create HWCDispatcher thread
    g_dispatch_thread = new HWCDispatcher();
    if (g_dispatch_thread == NULL) HWC_LOGE("Failed to create HWCDispatcher!!");

    char value[PROPERTY_VALUE_MAX];
    sprintf(value, "%d", DevicePlatform::m_config.compose_level);
    property_set("debug.hwc.compose_level", value);

    // check if virtual display could be composed by hwc
    status_t err = DispDevice::getInstance().createOverlaySession(HWC_DISPLAY_VIRTUAL);
    m_features.virtuals = (err == NO_ERROR);
    DispDevice::getInstance().destroyOverlaySession(HWC_DISPLAY_VIRTUAL);
}

HWCMediator::~HWCMediator()
{
    g_dispatch_thread = NULL;
}

void HWCMediator::open(hwc_private_device_t* device)
{
    // create listener
    struct DisplayListener : public DisplayManager::EventListener
    {
        DisplayListener(hwc_private_device_t* dev) : device(dev) { }
    private:
        hwc_private_device_t* device;
        virtual void onVSync(int dpy, nsecs_t timestamp, bool enabled)
        {
            if (enabled && device && device->procs && device->procs->vsync)
                device->procs->vsync(device->procs, dpy, timestamp);

            g_dispatch_thread->onVSync(dpy);
        }
        virtual void onPlugIn(int dpy)
        {
            g_dispatch_thread->onPlugIn(dpy);
        }
        virtual void onPlugOut(int dpy)
        {
            g_dispatch_thread->onPlugOut(dpy);
        }
        virtual void onHotPlugExt(int dpy, int connected)
        {
            if (device && device->procs && device->procs->hotplug)
                device->procs->hotplug(device->procs, dpy, connected);
        }
    };
    DisplayManager::getInstance().setListener(new DisplayListener(device));

    // initialize ServiceManager
    ServiceManager::getInstance().init();
}

void HWCMediator::close(hwc_private_device_t* device)
{
    DisplayManager::getInstance().setListener(NULL);
}

void HWCMediator::dump(char* buff, int buff_len)
{
    memset(buff, 0, buff_len);

    struct dump_buff log = {
        msg:      buff,
        msg_len:  buff_len,
        len:      0,
    };

    dump_printf(&log, "\n[HWC Features Support]\n"
            "  ext=%d vir=%d enhance=%d\n",
            m_features.externals, m_features.virtuals, m_features.enhance);

    DisplayManager::getInstance().dump(&log);

    char value[PROPERTY_VALUE_MAX];

    // check compose level
    property_get("debug.hwc.compose_level", value, "0");
    DevicePlatform::m_config.compose_level = atoi(value);

    dump_printf(&log, "\n[HWC Platform Config]\n"
            "  plat=%x comp=%d client=%d\n",
            DevicePlatform::m_config.platform,
            DevicePlatform::m_config.compose_level,
            DevicePlatform::m_config.client_id);

    // check profile level
    property_get("debug.hwc.profile_level", value, "0");
    DisplayManager::m_profile_level = atoi(value);

    // check dump level
    property_get("debug.hwc.dump_level", value, "0");
    int dump_level = atoi(value);

    g_dispatch_thread->dump(&log, (dump_level & (DUMP_MM | DUMP_SYNC)));

    // reset dump level
    property_set("debug.hwc.dump_level", "0");

#ifdef MTK_HWC_PROFILING
    property_get("debug.hwc.profile_handle_all", value, "0");
    g_handle_all_layers = (atoi(value) != 0);
#endif
}

int HWCMediator::query(int what, int* value)
{
    switch (what)
    {
        case HWC_BACKGROUND_LAYER_SUPPORTED:
            // not support background layer
            value[0] = 0;
            break;

        case HWC_VSYNC_PERIOD:
            value[0] = DisplayManager::getInstance().m_data[0].refresh;
            break;

        case HWC_FEATURES_STATE:
            memcpy(value, &m_features, sizeof(hwc_feature_t));
            break;

        case HWC_NUM_EXTRA_BUFFER:
            value[0] = 0;
            break;

        default:
            return -EINVAL;
    }

    return 0;
}

int HWCMediator::eventControl(int dpy, int event, int enabled)
{
    switch (event)
    {
        case HWC_EVENT_VSYNC:
            DisplayManager::getInstance().requestVSync(dpy, enabled);
            break;

        default:
            return -EINVAL;
    }

    return 0;
}

// for hwc 1.3/1.4 api compatible
// just call setPowerMode to do blank function
int HWCMediator::blank(int dpy, int blank)
{
    int mode = blank ? HWC_POWER_MODE_OFF : HWC_POWER_MODE_NORMAL;
    return setPowerMode(dpy, mode);
}

int HWCMediator::setPowerMode(int dpy, int mode)
{
    // screen blanking based on early_suspend in the kernel
    HWC_LOGI("SetPowerMode(%d) Display(%d)", mode, dpy);

     g_dispatch_thread->setPowerMode(dpy, mode);

    DisplayManager::getInstance().setPowerMode(dpy, mode);

    return 0;
}

int HWCMediator::getConfigs(int dpy, uint32_t* configs, size_t* numConfigs)
{
    HWC_LOGI("getConfigs Display(%d)", dpy);

    if (dpy < 0 || dpy >= DisplayManager::MAX_DISPLAYS)
    {
        HWC_LOGE("Failed to get display configs (dpy=%d)", dpy);
        return -EINVAL;
    }

    if (configs) configs[0] = 0;
    if (numConfigs) *numConfigs = 1;

    return 0;
}

int HWCMediator::getAttributes(
    int dpy, uint32_t config,
    const uint32_t* attributes, int32_t* values)
{
    HWC_LOGI("getAttributes Display(%d)", dpy);

    if (dpy < 0 || dpy >= DisplayManager::MAX_DISPLAYS || config != 0)
    {
        HWC_LOGE("Failed to get display attributes (dpy=%d, config=%d)", dpy, config);
        return -EINVAL;
    }

    const DisplayData* disp_data = &DisplayManager::getInstance().m_data[dpy];

    if (disp_data->connected == false)
    {
        HWC_LOGE("Failed to get display attributes (dpy=%d is not connected)", dpy);
        return -EINVAL;
    }

    int i = 0;
    while (attributes[i] != HWC_DISPLAY_NO_ATTRIBUTE)
    {
        switch (attributes[i])
        {
            case HWC_DISPLAY_VSYNC_PERIOD:
                values[i] = disp_data->refresh;
                break;
            case HWC_DISPLAY_WIDTH:
                values[i] = disp_data->width;
                break;
            case HWC_DISPLAY_HEIGHT:
                values[i] = disp_data->height;
                break;
            case HWC_DISPLAY_DPI_X:
                values[i] = disp_data->xdpi * 1000;
                break;
            case HWC_DISPLAY_DPI_Y:
                values[i] = disp_data->ydpi * 1000;
                break;
            case HWC_DISPLAY_SUBTYPE:
                values[i] = disp_data->subtype;
                break;
            default:
                HWC_LOGE("unknown display attribute[%d] %#x", i, attributes[i]);
                return -EINVAL;
        }

        i++;
    }

    return 0;
}

inline bool layerDirty(hwc_layer_1_t* layer, const bool& dirty,
    gralloc_extra_ion_sf_info_t* ext_info)
{

    bool dirty_buff = (ext_info->status & GRALLOC_EXTRA_MASK_DIRTY);
    bool dirty_param = false;

    if (layer->compositionType == HWC_FRAMEBUFFER_TARGET) {
        return true;
    }

    if (dirty)
    {
        dirty_param = ext_info->status & GRALLOC_EXTRA_MASK_DIRTY_PARAM;
    }
    else
    {
        const int src_crop_x = getSrcLeft(layer);
        const int src_crop_y = getSrcTop(layer);
        const int src_crop_w = getSrcWidth(layer);
        const int src_crop_h = getSrcHeight(layer);
        const int dst_crop_x = layer->displayFrame.left;
        const int dst_crop_y = layer->displayFrame.top;
        const int dst_crop_w = WIDTH(layer->displayFrame);
        const int dst_crop_h = HEIGHT(layer->displayFrame);
        if (ext_info->src_crop.x != src_crop_x ||
            ext_info->src_crop.y != src_crop_y ||
            ext_info->src_crop.w != src_crop_w ||
            ext_info->src_crop.h != src_crop_h ||
            ext_info->dst_crop.x != dst_crop_x ||
            ext_info->dst_crop.y != dst_crop_y ||
            ext_info->dst_crop.w != dst_crop_w ||
            ext_info->dst_crop.h != dst_crop_h)
        {
            ext_info->src_crop.x = src_crop_x;
            ext_info->src_crop.y = src_crop_y;
            ext_info->src_crop.w = src_crop_w;
            ext_info->src_crop.h = src_crop_h;

            ext_info->dst_crop.x = dst_crop_x;
            ext_info->dst_crop.y = dst_crop_y;
            ext_info->dst_crop.w = dst_crop_w;
            ext_info->dst_crop.h = dst_crop_h;

            dirty_param = true;
        }

        int prev_orient = (ext_info->status & GRALLOC_EXTRA_MASK_ORIENT);
        int prev_alpha  = (ext_info->status & GRALLOC_EXTRA_MASK_ALPHA);
        int prev_blend  = (ext_info->status & GRALLOC_EXTRA_MASK_BLEND);
        int curr_orient = layer->transform << 12;
        int curr_alpha  = layer->planeAlpha << 16;
        int curr_blend  = ((layer->blending && 0x100) +
                (layer->blending && 0x004) +
                (layer->blending && 0x001)) << 24;

        dirty_param |= (prev_orient != curr_orient);
        dirty_param |= (prev_alpha != curr_alpha);
        dirty_param |= (prev_blend != curr_blend);

        gralloc_extra_sf_set_status(
                ext_info, GRALLOC_EXTRA_MASK_ORIENT, curr_orient);

        gralloc_extra_sf_set_status(
                ext_info, GRALLOC_EXTRA_MASK_ALPHA, curr_alpha);

        gralloc_extra_sf_set_status(
                ext_info, GRALLOC_EXTRA_MASK_BLEND, curr_blend);

        gralloc_extra_sf_set_status(
                ext_info, GRALLOC_EXTRA_MASK_DIRTY_PARAM, ((int)dirty_param << 26));

        grallocExtraPerform(
                layer->handle, GRALLOC_EXTRA_SET_IOCTL_ION_SF_INFO, ext_info);
    }

    return (dirty_buff | (dirty_param << 1));
}

int HWCMediator::getActiveConfig(int dpy)
{
    // TODO
    if (dpy < 0 || dpy >= DisplayManager::MAX_DISPLAYS)
    {
        HWC_LOGE("Failed to get active configs (dpy=%d)", dpy);
        return -EINVAL;
    }

    return 0;
}

int HWCMediator::setActiveConfig(int dpy, int index)
{
    // TODO
    if (dpy < 0 || dpy >= DisplayManager::MAX_DISPLAYS || index != 0)
    {
        HWC_LOGE("Failed to set active configs (dpy=%d, idx=%d)", dpy, index);
        return -EINVAL;
    }

    return 0;
}

int HWCMediator::setCursorPosition(int /*dpy*/, int /*x*/, int /*y*/)
{
    // TODO
    return 0;
}

inline int layerType(
    int dpy, hwc_layer_1_t* layer, int curr_num, int max_num,
    bool fbt_layer_exist, int num_fbt, bool& is_dirty,
    gralloc_extra_ion_sf_info_t* ext_info)
{
    hwc_buffer_info_t info;

    if (!layer->handle) {
        return HWC_LAYER_TYPE_INVALID;
    }
    else
    {
        getBufferInfo(layer->handle, &info);
        grallocExtraQuery(layer->handle, GRALLOC_EXTRA_GET_IOCTL_ION_SF_INFO, ext_info);
    }

    if (curr_num >= max_num)
        return HWC_LAYER_TYPE_INVALID;

    if (layer->flags & HWC_SKIP_LAYER)
        return HWC_LAYER_TYPE_INVALID;

    int compose_level = DevicePlatform::m_config.compose_level;

    bool secure = DisplayManager::getInstance().m_data[dpy].secure;
    if (((info.usage & GRALLOC_USAGE_PROTECTED) ||
         (info.usage & GRALLOC_USAGE_SECURE)) && !secure)
        return HWC_LAYER_TYPE_INVALID;
    is_dirty = layerDirty(layer, is_dirty, ext_info);

    int buffer_type = (info.status & GRALLOC_EXTRA_MASK_TYPE);
    if (buffer_type == GRALLOC_EXTRA_BIT_TYPE_VIDEO ||
        buffer_type == GRALLOC_EXTRA_BIT_TYPE_CAMERA ||
        info.format == HAL_PIXEL_FORMAT_YV12)
    {
        if (num_fbt != 0 || compose_level & COMPOSE_DISABLE_MM)
            return HWC_LAYER_TYPE_INVALID;

        // check platform capability
        if (DevicePlatform::getInstance().isMMLayerValid(layer, &info))
        {
            // always treat third party app queued yuv buffer as dirty
            if (buffer_type != GRALLOC_EXTRA_BIT_TYPE_VIDEO &&
                buffer_type != GRALLOC_EXTRA_BIT_TYPE_CAMERA)
                is_dirty = true;

#ifdef MTK_SVP_SUPPORT
            if (info.usage & GRALLOC_USAGE_SECURE)
                layer->hints |= HWC_HINT_SECURE_COMP;
#endif
            return HWC_LAYER_TYPE_MM;
        }
        else
        {
            return HWC_LAYER_TYPE_INVALID;
        }
    }

    // any layers above layer handled by fbt should also be handled by fbt
    if (fbt_layer_exist || (compose_level & COMPOSE_DISABLE_UI))
        return HWC_LAYER_TYPE_INVALID;

    // check platform capability
    if (DevicePlatform::getInstance().isUILayerValid(layer, &info))
    {
        // always treat CPU layer as dirty
        if (buffer_type != GRALLOC_EXTRA_BIT_TYPE_GPU)
            is_dirty = true;

        return HWC_LAYER_TYPE_UI;
    }
    else
    {
        return HWC_LAYER_TYPE_INVALID;
    }
}

#ifdef MTK_HWC_PROFILING
inline int layerBytes(hwc_layer_1_t* layer, hwc_frect_t& frect)
{
    if (!layer->handle)
        return 0;

    int format;
    grallocExtraQuery(layer->handle, GRALLOC_EXTRA_GET_FORMAT, &format);
    int bpp = getBitsPerPixel(format);
    float w = frect.right - frect.left;
    float h = frect.bottom - frect.top;
    float size = w * h;
    return (int)(size * bpp / 8);
}
#endif

static void handleDisplayList(
    int dpy, hwc_display_contents_1_t* list, DispatcherJob* job)
{
    uint32_t max_ovl_num = job->num_layers;

    if (max_ovl_num <= 0)
    {
        HWC_LOGW("No available overlay resource (input=%d)", max_ovl_num);
    }

    // TODO: only handle primary and virtual displays
    bool bypass_all = (dpy == HWC_DISPLAY_EXTERNAL);
    bool fbt_layer_exist = bypass_all;

    int top_type = HWC_LAYER_TYPE_INVALID;
    int top_idx = -1;

    int num_ui = 0;
    int num_mm = 0;
    int num_fbt = 0;

#ifdef MTK_HWC_PROFILING
    int total_fbt_size = 0;
#endif

    bool need_check_type = true;
    if (HWC_DISPLAY_VIRTUAL == dpy) {
        need_check_type = false;
        for (uint32_t i = 0; i < list->numHwLayers; ++i)
        {
            int usage;
            hwc_layer_1_t* layer = &list->hwLayers[i];
            grallocExtraQuery(layer->handle, GRALLOC_EXTRA_GET_USAGE, &usage);
            if (usage & GRALLOC_USAGE_SECURE) {
                need_check_type = true;
                break;
            }
        }
    }

    // for HWC 1.2, lastest layer is used by opengl
    for (uint32_t i = 0; i < list->numHwLayers - 1; i++)
    {
        hwc_layer_1_t* layer = &list->hwLayers[i];
        gralloc_extra_ion_sf_info_t ext_info;

        layer->compositionType = HWC_FRAMEBUFFER;
        layer->hints = 0;

        if (bypass_all)
        {
#ifdef MTK_HWC_PROFILING
            total_fbt_size += layerBytes(layer, layer->sourceCropf);
#endif
            num_fbt++;
            continue;
        }

        uint32_t curr_ovl_num = num_ui + num_mm;

        bool is_dirty = false;
        int type = HWC_LAYER_TYPE_INVALID;
        if (need_check_type) {
            type = layerType(dpy, layer, curr_ovl_num, max_ovl_num,
                    fbt_layer_exist, num_fbt, is_dirty, &ext_info);
        }

        // check if need to set video timestamp
        int buffer_type = (ext_info.status & GRALLOC_EXTRA_MASK_TYPE);
        if (buffer_type == GRALLOC_EXTRA_BIT_TYPE_VIDEO)
        {
            job->timestamp = ext_info.timestamp;
        }

        if (HWC_LAYER_TYPE_INVALID == type)
        {
#ifdef MTK_HWC_PROFILING
            total_fbt_size += layerBytes(layer, layer->sourceCropf);
#endif
            fbt_layer_exist = true;
            num_fbt++;
            continue;
        }

        if (HWC_LAYER_TYPE_UI == type)
            num_ui++;
        else
            num_mm++;

        if (curr_ovl_num == max_ovl_num - 1)
        {
            top_type = type;
            top_idx = i;
        }

        // overlay input id's z-order is from bottom to top
        // (started from zero)

        HWLayer* hw_layer = &job->hw_layers[curr_ovl_num];
        hw_layer->enable  = true;
        hw_layer->index   = i;
        hw_layer->type    = type;
        hw_layer->dirty   = is_dirty;
        hw_layer->ext_info = ext_info;

        // label layer that is used by hwc
        layer->compositionType = HWC_OVERLAY;
        layer->hints |= HWC_HINT_CLEAR_FB;
    }

    if (list->numHwLayers == 1)
        fbt_layer_exist = true;
    else if (num_fbt == 0)
        fbt_layer_exist = false;

    if (fbt_layer_exist)
    {
        // set fbt layer
        hwc_layer_1_t* fbt_layer = &list->hwLayers[list->numHwLayers - 1];

        if (fbt_layer->compositionType == HWC_FRAMEBUFFER_TARGET)
        {
            // need to reserved one input for opengl
            if (HWC_LAYER_TYPE_INVALID != top_type && top_idx != -1)
            {
                if (HWC_LAYER_TYPE_UI == top_type)
                    num_ui--;
                else
                    num_mm--;

                list->hwLayers[top_idx].compositionType = HWC_FRAMEBUFFER;
                list->hwLayers[top_idx].hints = 0;
            }

            HWLayer* fbt_hw_layer = &job->hw_layers[num_ui + num_mm];
            fbt_hw_layer->enable  = true;
            fbt_hw_layer->index   = list->numHwLayers - 1;
            fbt_hw_layer->type    = HWC_LAYER_TYPE_FBT;

#ifdef MTK_HWC_PROFILING
            fbt_hw_layer->fbt_input_layers = num_fbt;
            fbt_hw_layer->fbt_input_bytes  = total_fbt_size;
#endif
        }
        else
        {
            HWC_LOGE("FRAMEBUFFER_TARGET is not last layer!! dpy(%d)", dpy);
            fbt_layer_exist = false;
        }
    }

    // prepare job in job group
    job->fbt_exist = fbt_layer_exist;
    job->num_ui_layers = num_ui;
    job->num_mm_layers = num_mm;
}

int HWCMediator::prepare(size_t num_display, hwc_display_contents_1_t** displays)
{
    if (!num_display || NULL == displays)
    {
        HWC_LOGW("Prepare, no displays, num=%d", num_display);
        return 0;
    }

    hwc_display_contents_1_t* list;

#ifdef MTK_HWC_PROFILING
    if (g_handle_all_layers)
    {
        g_prepare_all_layers = true;

        for (uint32_t i = 0; i < num_display; i++)
        {
            list = displays[i];
            if (list == NULL || list->numHwLayers <= 0) continue;

            for (uint32_t j = 0; j < list->numHwLayers - 1; j++)
            {
                hwc_layer_1_t* layer = &list->hwLayers[j];
                layer->compositionType = HWC_OVERLAY;
                layer->hints |= HWC_HINT_CLEAR_FB;
            }
        }

        return 0;
    }
#endif

    // prepare job group
    g_dispatch_thread->prepare();

    for (uint32_t i = 0; i < num_display; i++)
    {
        if (DisplayManager::MAX_DISPLAYS <= i) break;

        list = displays[i];

        // check if need to plug in/out virutal display
        if (HWC_DISPLAY_VIRTUAL == i)
        {
            if (!m_features.virtuals)
            {
                HWC_LOGD("PRE/bypass/dpy=%d/novir", i);
                continue;
            }

            DisplayManager::getInstance().hotplugVir(i, list);
        }

        if (list == NULL)
            continue;

        if (list->numHwLayers <= 0)
        {
            // do nothing if list is invalid
            HWC_LOGD("PRE/bypass/dpy=%d/list=%p", i, list);
            continue;
        }

        if (list->numHwLayers == 1)
        {
            if (HWC_DISPLAY_VIRTUAL == i)
            {
                if (!m_features.copyvds)
                {
                    // do nothing when virtual display with "fbt only"
                    HWC_LOGD("PRE/bypass/dpy=%d/num=0", i);
                    continue;
                }
            }
        }

        if (list->flags & HWC_SKIP_DISPLAY)
        {
            HWC_LOGD("PRE/bypass/dpy=%d/skip", i);
            continue;
        }

        DispatcherJob* job = g_dispatch_thread->getJob(i);
        if (NULL != job)
        {
            handleDisplayList(i, list, job);

            HWC_LOGD("PRE/dpy=%d/num=%d/fbt=%d/ui=%d/mm=%d",
                i, list->numHwLayers - 1, job->fbt_exist,
                job->num_ui_layers, job->num_mm_layers);
        }
        else
        {
            HWC_LOGE("Failed to get job for prepare !!");
        }
    }

    return 0;
}

int HWCMediator::set(size_t num_display, hwc_display_contents_1_t** displays)
{
    if (!num_display || NULL == displays)
    {
        HWC_LOGW("Set, no displays, num=%d", num_display);
        return 0;
    }

    hwc_display_contents_1_t* list;

#ifdef MTK_HWC_PROFILING
    if (g_handle_all_layers || g_prepare_all_layers)
    {
        g_dispatch_thread->onBlank(HWC_DISPLAY_PRIMARY, true);

        for (uint32_t i = 0; i < num_display; i++)
        {
            list = displays[i];
            if (list == NULL) continue;

            clearListAll(list);
        }

        g_prepare_all_layers = false;
        return 0;
    }
#endif

    for (uint32_t i = 0; i < num_display; i++)
    {
        if (DisplayManager::MAX_DISPLAYS <= i) break;

        list = displays[i];
        if (list == NULL)
            continue;

        if (list->numHwLayers <= 0)
        {
            HWC_LOGD("SET/bypass/dpy=%d/list=%p", i, list);
            continue;
        }

        if (HWC_DISPLAY_VIRTUAL == i)
        {
            if (list->numHwLayers == 1)
            {
                if (!m_features.copyvds)
                {
                    HWC_LOGD("SET/bypass/dpy=%d/num=0", i);
                    clearListFbt(list);
                    continue;
                }
            }
            else if (!m_features.virtuals)
            {
                HWC_LOGD("SET/bypass/dpy=%d/novir", i);
                clearListFbt(list);
                continue;
            }
        }

        if (list->flags & HWC_SKIP_DISPLAY)
        {
            HWC_LOGD("SET/bypass/dpy=%d/skip", i);
            continue;
        }

        g_dispatch_thread->setJob(i, list);
    }

    g_dispatch_thread->set();

    return 0;
}
