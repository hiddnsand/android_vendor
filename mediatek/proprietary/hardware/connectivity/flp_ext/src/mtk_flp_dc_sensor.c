/*****************************************************************************
 * Include
 *****************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <math.h>
#include "mtk_flp_lowpower.h"
#ifndef K2_PLATFORM
#include "mtk_flp_sys.h"
#include "mtk_flp.h"
#include "mtk_flp_dc.h"
#include <sys/socket.h>
#include <sys/un.h>
#include <private/android_filesystem_config.h>
#else
#include "mtk_flp_connsys_sys_type.h"
#include "mtk_flp_connsys_sys.h"
#include "mtk_gps_bora_flp.h"
#endif

#ifdef DEBUG_LOG

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "FLP_DC_SENS"

#ifndef K2_PLATFORM
#define FLPE(...)   mtk_flp_sys_dbg(MTK_FLP_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#define FLPD(...)   mtk_flp_sys_dbg(MTK_FLP_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define FLPW(...)   mtk_flp_sys_dbg(MTK_FLP_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define FLP_TRC(...)   mtk_flp_sys_dbg(MTK_FLP_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#else
#define FLPD(...)
#define FLPW(...)
#define FLPE(...)
#endif

#else
#define FLPD(...)
#define FLPW(...)
#define FLPE(...)
#endif


/*****************************************************************************
 * Define
 *****************************************************************************/
#ifndef M_PI
#define M_PI       3.14159265358979323846
#endif

#define NELEMS(x)    (int)(sizeof(x) / sizeof(x[0]))
#define MAX_UINT32 0xfffffffe //max value of ttick, for ttick rollover calculation
#define MAX_FIRST_TTFF_TIME  60 //after 60sec, will trigger pdr loc regardless of lack of first fix

typedef struct PDR_GPS
{
    /* gps report location in lla under () frame*/
    double location[3];

    /* velocity values are in meter per second (m/s) under (N,E,D) frame*/
    float velocity[3];

    /* velocity one sigma error in meter per second under (N,E,D) frame*/
    float  velocitySigma[3];

    /*  horizontal dilution of precision value in unitless */
    float HDOP;

    /*  horizontal accuracy of location in meter */
    float HACC;

} PDR_GPS, *LPPDR_GPS;

/*****************************************************************************
 * Function Prototype
 *****************************************************************************/
int mtk_flp_dc_sensor_process(MTK_FLP_MSG_T *prmsg);

#ifndef K2_PLATFORM
void mtk_flp_dc_sensor_main(void);
extern UINT32 mtk_flp_sys_get_time_tick();
#else
void mtk_flp_dc_sensor_main(MTK_FLP_MSG_T *prmsg);
#endif

//*****************************************
// Report thread function call backs
//*****************************************
#ifndef K2_PLATFORM
#define TURN_ON_DC_SENSOR    1

#if (TURN_ON_DC_SENSOR == 1 )
DcReportThread    Report_thrd_SENSOR = (DcReportThread)mtk_flp_dc_sensor_main;
DcProcessFunction    Process_Func_SENSOR = (DcProcessFunction)mtk_flp_dc_sensor_process;
#else
DcReportThread    Report_thrd_SENSOR = NULL;
DcProcessFunction    Process_Func_SENSOR = NULL;
#endif
#endif


/*****************************************************************************
 * Global Variables
 *****************************************************************************/
int smd_status = 0;
int smd_start = 0;

#ifndef K2_PLATFORM
#define MTKFLP_COMM_SENSOR_SOCK_CLI_PATH   "/data/mpe_flp/mpe2flpsensor"
#define MTKFLP_COMM_SENSOR_SOCK_SVR_PATH   "/data/mpe_flp/flpsensor2mpe"

int flp_sensor_server_fd = -1;
int flp_sensor_client_fd = -1;
static struct sockaddr_un sensor_client_sockaddr;
static struct sockaddr_un sensor_server_sockaddr;

#define SOCK_BUF_LEN    65535
char sockbuf[SOCK_BUF_LEN];
#endif


#ifndef K2_PLATFORM //AP version
static int mtk_flp_dc_sensor_connect_socket(void) {
    int res;
    /* Create socket with MPED */
    if((flp_sensor_client_fd = socket(AF_LOCAL, SOCK_DGRAM, 0)) == -1) {
        FLPD("open client sock failed\r\n");
        return -1;
    }

    unlink(MTKFLP_COMM_SENSOR_SOCK_CLI_PATH);
    memset(&sensor_client_sockaddr, 0, sizeof(sensor_client_sockaddr));
    sensor_client_sockaddr.sun_family = AF_LOCAL;
    strcpy(sensor_client_sockaddr.sun_path, MTKFLP_COMM_SENSOR_SOCK_CLI_PATH);

    if (bind(flp_sensor_client_fd, (struct sockaddr *)&sensor_client_sockaddr, sizeof(sensor_client_sockaddr)) < 0) {
        FLPD("Bind client error: %s\n", strerror(errno));
        return -1;
    }

    res = chmod(MTKFLP_COMM_SENSOR_SOCK_CLI_PATH, 0660);
    chown(MTKFLP_COMM_SENSOR_SOCK_CLI_PATH, -1, AID_INET);
    FLPD("chmod res = %d\n", res);  //770<--mode

    if((flp_sensor_server_fd = socket(AF_LOCAL, SOCK_DGRAM, 0)) < 0) {
        FLPD("open server sock failed\r\n");
        return -1;
    }
    memset(&sensor_server_sockaddr, 0, sizeof(sensor_server_sockaddr));
    sensor_server_sockaddr.sun_family = AF_LOCAL;
    strcpy(sensor_server_sockaddr.sun_path, MTKFLP_COMM_SENSOR_SOCK_SVR_PATH);
    return 0;
}

static int mtk_flp_dc_sensor_send_msg(MTK_FLP_MSG_T *msg, int len) {
    FLPD("Sending %d to MPED\n", len);
    if(flp_sensor_server_fd <=0) {
        FLPD("DC Send to MPED: UDP socket not connected\n");
        return -1;
    } if(msg == NULL) {
        FLPD("socket msg NULL\n");
        return -1;
    }

    if(sendto(flp_sensor_server_fd, msg, len, 0, (struct sockaddr*)&sensor_server_sockaddr, sizeof(sensor_server_sockaddr)) < 0) {
        FLPD("DC send to MPED fail:%s\n", strerror(errno));
    }
    return 0;
}

static int mtk_flp_dc_sensor_recv_msg(MTK_FLP_MSG_T **msg) {
    int recvlen = 0;
    socklen_t addrlen = sizeof(sensor_client_sockaddr);

    *msg = NULL;
    FLPD("Receiving from MPED\n");

    recvlen = recvfrom(flp_sensor_client_fd, &sockbuf[0], SOCK_BUF_LEN, 0, (struct sockaddr*)&sensor_client_sockaddr, &addrlen);

    if(recvlen >= (int)sizeof(MTK_FLP_MSG_T)) {
        FLPD("%d bytes received\n", recvlen);
        *msg = (MTK_FLP_MSG_T *)sockbuf;
        return recvlen;
    } else {
        FLPD("Error recveiving MPED UDP data: %s\n", strerror(errno));
        return -1;
    }
    return -1;
}

void mtk_flp_dc_sensor_main(void) {
    MTK_FLP_MSG_T *flp_msg = NULL, *flp_msg1 = NULL, *msg = NULL;
    MTK_FLP_LOCATION_T  loc_in;
    char localbuf[512];
    int flp_loc_available = -1, retlen = -1;

    memset(&loc_in, 0, sizeof(MTK_FLP_LOCATION_T));

    if(mtk_flp_dc_sensor_connect_socket() == 0) {
        FLPD("mtk_flp_dc_sensor_main init OK\n");
    } else {
        FLPD("mtk_flp_dc_sensor_main init failed\n");
        return;
    }

    while(1) {
        retlen = mtk_flp_dc_sensor_recv_msg((MTK_FLP_MSG_T **)&msg);
        if(retlen <= 0) {
            continue;
        }
        switch(msg->type){
            case CMD_FLP_START_SENSOR_RES:
                FLPD("SENSOR start ok");
                break;
            case CMD_FLP_STOP_SENSOR_RES:
                FLPD("SENSOR stop ok");
                break;
            case CMD_SCP_SEND_ADR:
                if(!(mtk_flp_lowpower_query_source() & FLP_TECH_MASK_SENSORS)) {
                    FLPD("sensor not yet started, send stop command\n");
                    flp_msg = mtk_flp_sys_msg_alloc( sizeof(MTK_FLP_MSG_T));
                    flp_msg->type = CMD_FLP_STOP_SENSOR;
                    flp_msg->length = 0;
                    mtk_flp_dc_sensor_send_msg(flp_msg, (flp_msg->length)+sizeof(MTK_FLP_MSG_T));
                    mtk_flp_sys_msg_free(flp_msg);
                    return;
                }
                if(msg->length >= sizeof(MTK_FLP_LOCATION_T)) {
                    memcpy(&loc_in, ((INT8*)msg)+ sizeof(MTK_FLP_MSG_T), sizeof(MTK_FLP_LOCATION_T));
                    flp_msg = mtk_flp_sys_msg_alloc(sizeof(MTK_FLP_MSG_T) +sizeof(MTK_FLP_LOCATION_T));
                    flp_msg->type = MTK_FLP_MSG_DC_REPORT_LOC_NTF;
                    flp_msg->length = sizeof(MTK_FLP_LOCATION_T);
                    memcpy(((INT8*)flp_msg)+sizeof(MTK_FLP_MSG_T), &loc_in, sizeof(MTK_FLP_LOCATION_T));
                    mtk_flp_sys_msg_send(MTK_FLP_TASKID_DC, flp_msg);
                    FLPD("ADR_NTF=LNG::%f LAT::%f ALT::%f SIZE::%d SRC::%d ACC:%f \n",
                        loc_in.longitude, loc_in.latitude, loc_in.altitude, loc_in.size,loc_in.sources_used,loc_in.accuracy);

                    if((mtk_flp_lowpower_query_mode() < FLP_BATCH_GEOFENCE_ONLY) && (mtk_flp_lowpower_query_mode() != FLP_BATCH_NONE)) {
                        //send DIAG location
                        sprintf(localbuf,"SENS_NTF=LNG:%f LAT:%f ALT:%f", loc_in.longitude, loc_in.latitude, loc_in.altitude);
                        flp_msg1 = mtk_flp_sys_msg_alloc( sizeof(MTK_FLP_MSG_T) +strlen(localbuf));
                        flp_msg1->type = MTK_FLP_MSG_DC_DIAG_REPORT_DATA_NTF;
                        flp_msg1->length = strlen(localbuf);
                        memcpy(((INT8*)flp_msg1)+sizeof(MTK_FLP_MSG_T), localbuf, strlen(localbuf));
                        mtk_flp_sys_msg_send(MTK_FLP_TASKID_DC, flp_msg1);
                     }
                } else {
                    FLPD("mtk_flp_dc_sensor_main, adr not matching: msg_len:%d expect_len:%d\n", msg->length, sizeof(MTK_FLP_LOCATION_T));
                }
                break;
            case CMD_SCP_SEND_REBOOT:
                if(mtk_flp_lowpower_query_source() & FLP_TECH_MASK_SENSORS) {
                    FLPD("START PDR");
                    flp_msg = mtk_flp_sys_msg_alloc( sizeof(MTK_FLP_MSG_T));
                    flp_msg->type = CMD_FLP_START_SENSOR;
                    flp_msg->length = 0;
                    mtk_flp_dc_sensor_send_msg(flp_msg, (flp_msg->length)+sizeof(MTK_FLP_MSG_T));
                    mtk_flp_sys_msg_free(flp_msg);
                }
                break;
            default:
                FLPD("Unknown wifi message type: %x", msg->type);
                break;
        }
    }
}

int mtk_flp_dc_sensor_process(MTK_FLP_MSG_T *prmsg) {
    MTK_FLP_BATCH_OPTION_T *option;
    MTK_FLP_MSG_T *ptr;

    option = (MTK_FLP_BATCH_OPTION_T *)((UINT8*)prmsg+sizeof(MTK_FLP_MSG_T));
    FLPD("Sensor Proc msg(%x) type:0x%02x len:%d", (unsigned int)prmsg, prmsg->type,prmsg->length);

    switch( prmsg->type )
    {
    case MTK_FLP_MSG_DC_START_CMD:
        if( option->sources_to_use & FLP_TECH_MASK_SENSORS) {
            FLPD("START PDR");
            ptr = mtk_flp_sys_msg_alloc( sizeof(MTK_FLP_MSG_T));
            ptr->type = CMD_FLP_START_SENSOR;
            ptr->length = 0;
            mtk_flp_dc_sensor_send_msg(ptr, (ptr->length)+sizeof(MTK_FLP_MSG_T));
            mtk_flp_sys_msg_free(ptr);
        }
        break;
    case MTK_FLP_MSG_DC_STOP_CMD:
        {
            FLPD("Stop sensor pdr");
            ptr = mtk_flp_sys_msg_alloc( sizeof(MTK_FLP_MSG_T));
            ptr->type = CMD_FLP_STOP_SENSOR;
            ptr->length = 0;
            mtk_flp_dc_sensor_send_msg(ptr, (ptr->length)+sizeof(MTK_FLP_MSG_T));
            mtk_flp_sys_msg_free(ptr);
        }
        break;
    default:
        FLPD("Unknown pdr message to send");
        break;
    }
    return 0;
}

#else //offload
void mtk_flp_dc_sensor_main(MTK_FLP_MSG_T *prmsg) {
    MTK_FLP_MSG_T   *flp_msg=NULL, *flp_msg1 =NULL;
    MTK_FLP_LOCATION_T  loc_in;
    char localbuf[512];
    int flp_loc_available = -1;

    memset(&loc_in, 0, sizeof(MTK_FLP_LOCATION_T));
    if((prmsg == NULL) || (prmsg->length < 0)) {
        FLP_TRC("mtk_flp_dc_sensor_main, recv prmsg is null pointer\r\n");
        return;
    }

    FLP_TRC("sensor main msg(%x) type:0x%02x len:%d", (unsigned int)prmsg, prmsg->type,prmsg->length);

    switch( prmsg->type ) {
        case CMD_FLP_START_SENSOR_RES:
            FLP_TRC("SENSOR start ok");
            break;
        case CMD_FLP_STOP_SENSOR_RES:
            FLP_TRC("SENSOR stop ok");
            break;
        case CMD_SCP_SEND_ADR:
            if(!(mtk_flp_lowpower_query_source() & FLP_TECH_MASK_SENSORS)) {
                FLP_TRC("sensor not yet started, send stop command\n");
                flp_msg = mtk_flp_mcu_mem_alloc( sizeof(MTK_FLP_MSG_T));
                flp_msg->type = CMD_FLP_STOP_SENSOR;
                flp_msg->length = 0;
                mtk_flp_mcu_sys_msg_send(MTK_FLP_TASKID_SCP, flp_msg);
                return;
            }
            if(prmsg->length >= sizeof(MTK_FLP_LOCATION_T)) {
                memcpy(&loc_in, ((INT8*)prmsg)+ sizeof(MTK_FLP_MSG_T), sizeof(MTK_FLP_LOCATION_T));
                flp_msg = mtk_flp_mcu_mem_alloc(sizeof(MTK_FLP_MSG_T) +sizeof(MTK_FLP_LOCATION_T));
                flp_msg->type = MTK_FLP_MSG_DC_REPORT_LOC_NTF;
                flp_msg->length = sizeof(MTK_FLP_LOCATION_T);
                memcpy(((INT8*)flp_msg)+sizeof(MTK_FLP_MSG_T), &loc_in, sizeof(MTK_FLP_LOCATION_T));
                mtk_flp_mcu_sys_msg_send(MTK_FLP_TASKID_DC, flp_msg);
                FLP_TRC("ADR_NTF=LNG::%f LAT::%f ALT::%f SIZE::%d SRC::%d ACC:%f \n",
                    loc_in.longitude, loc_in.latitude, loc_in.altitude, loc_in.size,loc_in.sources_used,loc_in.accuracy);

                if((mtk_flp_lowpower_query_mode() < FLP_BATCH_GEOFENCE_ONLY) && (mtk_flp_lowpower_query_mode() != FLP_BATCH_NONE)) {
                    //send DIAG location
                    sprintf(localbuf,"SENS_NTF=LNG:%f LAT:%f ALT:%f", loc_in.longitude, loc_in.latitude, loc_in.altitude);
                    flp_msg1 = mtk_flp_mcu_mem_alloc( sizeof(MTK_FLP_MSG_T) +strlen(localbuf));
                    flp_msg1->type = MTK_FLP_MSG_DC_DIAG_REPORT_DATA_NTF;
                    flp_msg1->length = strlen(localbuf);
                    memcpy(((INT8*)flp_msg1)+sizeof(MTK_FLP_MSG_T), localbuf, strlen(localbuf));
                    mtk_flp_mcu_sys_msg_send(MTK_FLP_TASKID_HAL, flp_msg1);
                 }
            } else {
                FLP_TRC("mtk_flp_dc_sensor_main, adr not matching: msg_len:%d expect_len:%d\n", prmsg->length, sizeof(MTK_FLP_LOCATION_T));
            }
            break;
        #ifdef MTK_SMART_SMD_GEOFENCE
        case CMD_SCP_SEND_SMD:
            if(prmsg->length >= sizeof(int)) {
                memcpy(&smd_status, ((INT8*)prmsg)+ sizeof(MTK_FLP_MSG_T), sizeof(int));
                FLP_TRC("smd_status = %d",smd_status);
            }
            break;
        #endif
        default:
            FLP_TRC("Unknown wifi message type: %x", prmsg->type);
            break;

    }
}

int mtk_flp_dc_sensor_process(MTK_FLP_MSG_T *prmsg) {
    MTK_FLP_BATCH_OPTION_T *option;
    MTK_FLP_MSG_T *ptr;
    int scp_logging_config = -1;

    option = (MTK_FLP_BATCH_OPTION_T *)((UINT8*)prmsg+sizeof(MTK_FLP_MSG_T));
    FLP_TRC("Sensor Proc msg(%x) type:0x%02x len:%d", (unsigned int)prmsg, prmsg->type,prmsg->length);

    switch(prmsg->type) {
    case MTK_FLP_MSG_DC_START_CMD:
        if(option->sources_to_use & FLP_TECH_MASK_SENSORS) {
            FLP_TRC("START PDR");
            ptr = mtk_flp_mcu_mem_alloc( sizeof(MTK_FLP_MSG_T));
            ptr->type = CMD_FLP_START_SENSOR;
            ptr->length = 0;
            mtk_flp_mcu_sys_msg_send(MTK_FLP_TASKID_SCP,ptr);
        }
        break;
    case MTK_FLP_MSG_DC_STOP_CMD:
        {
            FLP_TRC("Stop sensor pdr");
            ptr = mtk_flp_mcu_mem_alloc( sizeof(MTK_FLP_MSG_T));
            ptr->type = CMD_FLP_STOP_SENSOR;
            ptr->length = 0;
            mtk_flp_mcu_sys_msg_send(MTK_FLP_TASKID_SCP,ptr);
        }
        break;
    case MTK_FLP_MSG_SYS_SCP_CONFIG_CMD:
        if(prmsg->length >= sizeof(int)) {
            memcpy(&scp_logging_config,((INT8*)prmsg)+ sizeof(MTK_FLP_MSG_T), sizeof(int));
            FLP_TRC("SCP log enable =%d \n",scp_logging_config);
            ptr = mtk_flp_mcu_mem_alloc(sizeof(MTK_FLP_MSG_T) + sizeof(int));
            ptr->type = CMD_FLP_SCP_LOGGING_CTRL;
            ptr->length = sizeof(int);
            memcpy( ((INT8*)ptr)+sizeof(MTK_FLP_MSG_T), &scp_logging_config, sizeof(int));
            mtk_flp_mcu_sys_msg_send(MTK_FLP_TASKID_SCP,ptr);
        }
        break;
    default:
        FLP_TRC("Unknown pdr message to send");
        break;
    }
    return 0;
}

#ifdef MTK_SMART_SMD_GEOFENCE
int mtk_flp_dc_sensor_smd_start() {
    MTK_FLP_MSG_T *ptr;
    FLP_TRC("Smart goefence start smd sensor");
    if(smd_start == 0) {
        ptr = mtk_flp_mcu_mem_alloc(sizeof(MTK_FLP_MSG_T));
        ptr->type = CMD_FLP_START_SMD_SENSOR;
        ptr->length = 0;
        mtk_flp_mcu_sys_msg_send(MTK_FLP_TASKID_SCP, ptr);
        smd_start = 1;
    } else {
        FLP_TRC("Smart goefence already start smd sensor");
    }
    return 0;
}

int mtk_flp_dc_sensor_smd_stop() {
    MTK_FLP_MSG_T *ptr;
    FLP_TRC("Smart goefence stop smd sensor");
    if(smd_start == 1) {
        ptr = mtk_flp_mcu_mem_alloc(sizeof(MTK_FLP_MSG_T));
        ptr->type = CMD_FLP_STOP_SMD_SENSOR;
        ptr->length = 0;
        mtk_flp_mcu_sys_msg_send(MTK_FLP_TASKID_SCP, ptr);
        smd_status = 0;
        smd_start = 0;
    } else {
        FLP_TRC("Smart goefence already stop smd sensor");
    }
    return 0;
}

int mtk_flp_dc_sensor_get_smd_status() {
    return smd_status;
}
#endif
#endif //endof offload

