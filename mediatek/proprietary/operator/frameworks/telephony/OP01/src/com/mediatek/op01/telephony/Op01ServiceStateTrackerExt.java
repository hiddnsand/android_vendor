/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op01.telephony;

import android.content.Context;
import android.content.res.Resources;
import android.os.SystemProperties;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.TelephonyProperties;

import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.MtkTelephonyProperties;
import com.mediatek.internal.telephony.ratconfiguration.RatConfiguration;
import com.mediatek.internal.telephony.ServiceStateTrackerExt;

import mediatek.telephony.MtkServiceState;

/**
 * Implement methods to support China Mobile requirements.
 *
 * @return
 */
public class Op01ServiceStateTrackerExt extends ServiceStateTrackerExt {
    private Context mContext;

    //[ALPS01646248] To support auto switch rat mode to 2G only for 3M TDD csfb project when inserting non-CMCC China operator Card
    private String[] CU_CT_GSM_ONLY_PLMN_SIM = {"46001", "46006", "46009", "45407", "46005", "45502","46003","46011"};
    private String[] CMCC_PLMN_SIM = {"46000", "46002", "46004", "46007", "46008"};

    public Op01ServiceStateTrackerExt() {
    }

    public Op01ServiceStateTrackerExt(Context context) {
        mContext = context;
    }

    @Override
    public String onUpdateSpnDisplay(String plmn, MtkServiceState ss, int phoneId) {
        //[ALPS01663902]-Start
        if (plmn == null) {
            return plmn;
        }
        //[ALPS01663902]-End
        int radioTechnology;
        boolean isRoming;

        radioTechnology = ss.getRilVoiceRadioTechnology();
        isRoming = ss.getRoaming();
        log("onUpdateSpnDisplay: radioTechnology = " + radioTechnology
                + ", phoneId = " + phoneId + ",isRoming = " + isRoming);

        // for LTE
        if (radioTechnology == MtkServiceState.RIL_RADIO_TECHNOLOGY_LTE
                && plmn != Resources.getSystem().getText(
                        com.android.internal.R.string.lockscreen_carrier_default).toString()) {
            plmn = plmn + " 4G";
        } else if (radioTechnology > MtkServiceState.RIL_RADIO_TECHNOLOGY_EDGE
                && radioTechnology != MtkServiceState.RIL_RADIO_TECHNOLOGY_GSM
                && plmn != Resources.getSystem().getText(
                        com.android.internal.R.string.lockscreen_carrier_default).toString()) {
            plmn = plmn + " 3G";
        }
        if (isRoming) {
            String prop1 = TelephonyManager.getTelephonyProperty(
                    phoneId, TelephonyProperties.PROPERTY_ICC_OPERATOR_ALPHA, "");
            log("getSimOperatorName simId = " + phoneId + " prop1 = " + prop1);
            if (prop1.equals("")) {
                String prop2 = TelephonyManager.getTelephonyProperty(
                        phoneId, MtkTelephonyProperties.PROPERTY_ICC_OPERATOR_DEFAULT_NAME, "");
                log("getMTKdefinedSimOperatorName simId = " + phoneId + " prop2 = " + prop2);
                if (!prop2.equals("")) {
                    plmn = plmn + "(" + prop2 + ")";
                }
            } else {
                plmn = plmn + "(" + prop1 + ")";
            }
        }
        log("Current PLMN: " + plmn);
        return plmn;
    }

    @Override
    public boolean needSpnRuleShowPlmnOnly() {
        return true;
    }

    /*[ALPS01577029]-START:To support auto switch rat mode:
       (1) 2G only feature:
         case 1. CT/CU sim card when 3M project

       Returns -1 when not need to be switched, or return real rat mode
     */
    @Override
    public int needAutoSwitchRatMode(int phoneId,String nwPlmn){
        boolean isTdd = false;
        String basebandCapability;
        String property_name = "gsm.baseband.capability";
        String testSimProperty = "gsm.sim.ril.testsim";
        int modemType;
        int testMode = SystemProperties.getInt("gsm.gcf.testmode", 0);
        int currentNetworkMode = -1;
        boolean isTestIccCard = false;

        //[ALPS01646248]
        String simOperator = null;
        int userType = UNKNOWN_USER;

        //op01 need support LTE
        if (!isLTESupport()) {
            log("needSwitchRatMode()= -1 cause not LTE support");
            return currentNetworkMode;
        }

        //get sim switch status
        int switchStatus = Integer.valueOf(
                SystemProperties.get(MtkPhoneConstants.PROPERTY_CAPABILITY_SWITCH, "1"));

        log("needSwitchRatMode: phoneId=" + phoneId + ", switchStatus=" + switchStatus +
            ",SubscriptionManager.isValidPhoneId(phoneId)=" +
            SubscriptionManager.isValidPhoneId(phoneId));

        if (SubscriptionManager.isValidPhoneId(phoneId)){
            if (phoneId == (switchStatus - 1)) { // main sim
                userType = getUserType(phoneId);
                if (userType == UNKNOWN_USER) {
                    log("needSwitchRatMode()= -1 cause userType is unknown");
                    return currentNetworkMode;
                }

                if((phoneId >= 0 && phoneId <= 3) &&
                    SystemProperties.getInt(PROPERTY_RIL_TEST_SIM[phoneId], 0) == 1){
                    isTestIccCard = true;
                }

                basebandCapability = SystemProperties.get(property_name);
                if ((basebandCapability != null) && (!(basebandCapability.equals("")))){
                    modemType = Integer.valueOf(basebandCapability);
                    log("needSwitchRatMode: modemType="+modemType);
                    if ((modemType & TDSCDMA) == TDSCDMA){
                       isTdd = true;
                    }
                }

                log("needSwitchRatMode: isTdd:"+isTdd+",isWorldPhone:"+isWorldPhoneSupport()+
                    ",isLte:"+isLTESupport()+",nwPlmn:"+nwPlmn +",testMode:"+testMode +
                    ",isTestIccCard:"+isTestIccCard+",userType:"+userType);

                if ((testMode != 0) || (isTestIccCard == true)) {
                    log("needSwitchRatMode()= -1 cause testMode or testIccCard");
                    return currentNetworkMode;
                }

                if ((userType == TYPE3_USER) && !isWorldPhoneSupport() && (isTdd == true)){//type3
                    log("needSwitchRatMode:set Rat to 2G when inserting CU/CT Card for 3M project");
                    return Phone.NT_MODE_GSM_ONLY;
                }

            } else { // vice sim
                int mainSimUserType = getUserType(switchStatus - 1); // main sim
                userType = getUserType(phoneId); // vice sim
                if (mainSimUserType == TYPE1_USER && userType != TYPE1_USER) {
                    log("needSwitchRatMode:set no type1 user Rat to 2G when type1+type2/3");
                    return Phone.NT_MODE_WCDMA_PREF;
                }
            }
        }
        log("needSwitchRatMode: currentNetworkMode = " + currentNetworkMode);
        return currentNetworkMode;
    }

    private int getUserType(int phoneId) {

        final String imsi = PhoneFactory.getPhone(phoneId).getSubscriberId();
        log("needSwitchRatMode(phoneId=" + phoneId + ")imsi=" +imsi);

        if (imsi != null && imsi.length() >= 5) {
            for (String plmn : CMCC_PLMN_SIM) {
                if (imsi.startsWith(plmn)) {
                    return TYPE1_USER;
                }
            }
            for (String plmn : CU_CT_GSM_ONLY_PLMN_SIM) {
                if (imsi.startsWith(plmn)) {
                    return TYPE3_USER;
                }
            }
            return TYPE2_USER;
        }

        return UNKNOWN_USER;
    }

    @Override
    public boolean allowSpnDisplayed() {
        return false;
    }

    private static final String PROPERTY_RIL_TEST_SIM[] = {
        "gsm.sim.ril.testsim",
        "gsm.sim.ril.testsim.2",
        "gsm.sim.ril.testsim.3",
        "gsm.sim.ril.testsim.4",
    };

    private static final int TDSCDMA = 0x08;
    private static final int UNKNOWN_USER = 0;
    private static final int TYPE1_USER = 1; //CMCC
    private static final int TYPE2_USER = 2; //
    private static final int TYPE3_USER = 3; //CU/CT

    private boolean isLTESupport() {
        if (SystemProperties.getInt("ro.boot.opt_lte_support", 0) == 1) {
            return true;
        }
        return false;
    }

    private boolean isWorldPhoneSupport() {
        return (RatConfiguration.isWcdmaSupported() && RatConfiguration.isTdscdmaSupported());
    }
}

