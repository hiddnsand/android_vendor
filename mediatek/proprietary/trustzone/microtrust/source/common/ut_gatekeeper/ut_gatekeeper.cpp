/*
 * Copyright (C) 2015-2016 MICROTRUST Incorporated
 * All rights reserved
 *
 * This file and software is confidential and proprietary to MICROTRUST Inc.
 * Unauthorized copying of this file and software is strictly prohibited.
 * You MUST NOT disclose this file and software unless you get a license
 * agreement from MICROTRUST Incorporated.
 */

#define IMSG_TAG "microgatekeeper"
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string.h>

#include <sys/cdefs.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#include "ut_gatekeeper.h"
#include <hardware/gatekeeper.h>
#include <hardware/hardware.h>

#include <imsg_log.h>

//#define GK_DEBUG

#define CMD_ID_ENROLL 0x01
#define CMD_ID_VARIFY 0x02
#define CMD_ID_DELETE_USER 0x03
#define GATEKEEPER_BUFF_SIZE (4096)
#define DEVICE_NODE "/dev/teei_fp"
#define TEEI_IOC_MAGIC 'T'
#define CMD_FP_GATEKEEPER_CMD _IO(TEEI_IOC_MAGIC, 0x3)

gatekeeper_device_t* dev = NULL;

static int ut_gk_transfer_buff(void* buff) {
    int fd = 0;
    if (NULL == buff) {
        IMSG_ERROR("[%s] buff is NULL!", __func__);
        return -EINVAL;
    }

    fd = open(DEVICE_NODE, O_RDWR);
    if (fd < 0) {
        IMSG_ERROR("[%s] can't open DEVICE_NODE!", __func__);
        return -EFAULT;
    }
    if (ioctl(fd, CMD_FP_GATEKEEPER_CMD, buff)) {
        IMSG_ERROR("[%s] can't ioctl DEVICE_NODE!", __func__);
        close(fd);
        return -EFAULT;
    }
    close(fd);
    return 0;
}

static int ut_gk_enroll(const struct gatekeeper_device* dev, uint32_t uid,
                        const uint8_t* current_password_handle,
                        uint32_t current_password_handle_length, const uint8_t* current_password,
                        uint32_t current_password_length, const uint8_t* desired_password,
                        uint32_t desired_password_length, uint8_t** enrolled_password_handle,
                        uint32_t* enrolled_password_handle_length) {
    int ret = 0;
    uint32_t retry_timeout = 0;
    uint8_t* position = NULL;
    uint8_t* gatekeeper_transfer_buffer = NULL;
    ut_gk_enroll_out_t* ut_gk_enroll_out = NULL;
    ut_gk_struct_t* ut_gk_struct = NULL;
    (void)dev;
    IMSG_ENTER();

    if (desired_password_length >= 100) {
        IMSG_ERROR("[%s] desired_password_length >= 100", __func__);
        ret = -EINVAL;
        goto out;
    }

    if (enrolled_password_handle == NULL || enrolled_password_handle_length == NULL ||
        desired_password == NULL || desired_password_length == 0) {
        IMSG_ERROR(
            "[%s] Input param error, handle %#x || handle_length %#x || desired_password %#x || "
            "desired_password_length %d",
            __func__, enrolled_password_handle, enrolled_password_handle_length, desired_password,
            desired_password_length);
        ret = -EINVAL;
        goto out;
    }

    if (current_password_handle == NULL || current_password_handle_length == 0 ||
        current_password == NULL || current_password_length == 0) {
        current_password_handle = NULL;
        current_password_handle_length = 0;
        current_password = NULL;
        current_password_length = 0;
    }

#ifdef GK_DEBUG
    IMSG_DEBUG("====[%s] [ree] enroll variable [in] begin====", __func__);
    IMSG_DEBUG("====[%s] uid is %d====", __func__, uid);
    IMSG_DEBUG("====[%s] current_password_handle_length is %d====", __func__,
               current_password_handle_length);
    IMSG_DEBUG("====[%s] current_password_length is %d====", __func__, current_password_length);
    IMSG_DEBUG("====[%s] desired_password_length is %d====", __func__, desired_password_length);
    IMSG_DEBUG("====[%s] [ree] enroll variable [in] end====", __func__);
#endif
    gatekeeper_transfer_buffer = (uint8_t*)malloc(GATEKEEPER_BUFF_SIZE);
    if (NULL == gatekeeper_transfer_buffer) {
        IMSG_ERROR("[%s] unable to malloc gatekeeper_transfer_buffer", __func__);
        ret = -EINVAL;
        goto out;
    }
    memset(gatekeeper_transfer_buffer, 0, GATEKEEPER_BUFF_SIZE);

    ut_gk_enroll_out = (ut_gk_enroll_out_t*)malloc(sizeof(ut_gk_enroll_out_t));
    if (NULL == ut_gk_enroll_out) {
        IMSG_ERROR("[%s] unable to malloc ut_gk_enroll_out", __func__);
        ret = -EINVAL;
        goto out;
    }
    memset(ut_gk_enroll_out, 0, sizeof(ut_gk_enroll_out_t));

    ut_gk_struct = (ut_gk_struct_t*)malloc(sizeof(ut_gk_struct_t));
    if (NULL == ut_gk_struct) {
        IMSG_ERROR("[%s] unable to malloc ut_gk_struct", __func__);
        ret = -EINVAL;
        goto out;
    }
    memset(ut_gk_struct, 0, sizeof(ut_gk_struct_t));

    // set ut_gk_struct for  const length parts
    ut_gk_struct->cmd = CMD_ID_ENROLL;
    ut_gk_struct->uid = uid;
    ut_gk_struct->ut_gk_enroll.current_password_handle_length = current_password_handle_length;
    ut_gk_struct->ut_gk_enroll.current_password_length = current_password_length;
    ut_gk_struct->ut_gk_enroll.desired_password_length = desired_password_length;

    // copy ut_gk_struct to gatekeeper_transfer_buffer
    memcpy((void*)gatekeeper_transfer_buffer, (void*)ut_gk_struct, sizeof(ut_gk_struct_t));
    position = gatekeeper_transfer_buffer + sizeof(ut_gk_struct_t);

    // copy varify length parts to gatekeeper_transfer_buffer
    memcpy(position, current_password_handle, current_password_handle_length);
    position += current_password_handle_length;
    memcpy(position, current_password, current_password_length);
    position += current_password_length;
    memcpy(position, desired_password, desired_password_length);

    // transfer buff to tee
    if (ut_gk_transfer_buff(gatekeeper_transfer_buffer)) {
        IMSG_ERROR("[%s] can't transfer buff to tee", __func__);
        ret = -EINVAL;
        goto out;
    }

    // copy buffer to struct
    memcpy(ut_gk_enroll_out, gatekeeper_transfer_buffer, sizeof(ut_gk_enroll_out_t));

    // if there is an error?
    if (ut_gk_enroll_out->error == ERROR_RETRY) {
#ifdef GK_DEBUG
        IMSG_DEBUG("====[%s] ut_gk_enroll_out->error == ERROR_RETRY ====", __func__);
        IMSG_DEBUG("====[%s] ut_gk_enroll_out->retry_timeout is %lu====", __func__,
                   ut_gk_enroll_out->retry_timeout);
#endif
        retry_timeout = ut_gk_enroll_out->retry_timeout;
        IMSG_INFO("====  From ree enroll, ERROR_RETRY!! ====");
        ret = retry_timeout;
        goto out;
    } else if (ut_gk_enroll_out->error != ERROR_NONE) {
        IMSG_INFO("====  From ree enroll, ERROR!! ====");
        ret = -EINVAL;
        goto out;
    }

    // copy gatekeeper_transfer_buffer to return variable
    *enrolled_password_handle_length = ut_gk_enroll_out->enrolled_password_handle_length;
    *enrolled_password_handle = (uint8_t*)malloc(*enrolled_password_handle_length);
    memcpy(*enrolled_password_handle, gatekeeper_transfer_buffer + sizeof(ut_gk_enroll_out_t),
           *enrolled_password_handle_length);

#ifdef GK_DEBUG
    IMSG_DEBUG("====[%s] ut_gk_enroll_out->error no error ====", __func__);
    IMSG_DEBUG("====[%s] [ree] enroll variable [out] begin====", __func__);
    IMSG_DEBUG("====[%s] sizeof(struct password_handle_t)=%d====", __func__,
               sizeof(struct password_handle_t));
    IMSG_DEBUG("====[%s] version is %u====", __func__,
               ((struct password_handle_t*)(*enrolled_password_handle))->version);
    IMSG_DEBUG("====[%s] user_id is %llu====", __func__,
               ((struct password_handle_t*)(*enrolled_password_handle))->user_id);
    IMSG_DEBUG("====[%s] flags is %llu====", __func__,
               ((struct password_handle_t*)(*enrolled_password_handle))->flags);
    IMSG_DEBUG("====[%s] salt is %llu====", __func__,
               ((struct password_handle_t*)(*enrolled_password_handle))->salt);
    IMSG_DEBUG("====[%s] password_length is %lu====", __func__, *enrolled_password_handle_length);
    IMSG_DEBUG("====[%s] hardware_backed is %u====", __func__,
               ((struct password_handle_t*)(*enrolled_password_handle))->hardware_backed);
    IMSG_DEBUG("====[%s] [ree] enroll variable [out] end====", __func__);
#endif
    IMSG_INFO("====  From ree enroll, SUCCESS!! ====");

out:
    if (ut_gk_enroll_out != NULL) {
        free(ut_gk_enroll_out);
    }
    if (ut_gk_struct != NULL) {
        free(ut_gk_struct);
    }
    if (gatekeeper_transfer_buffer != NULL) {
        free(gatekeeper_transfer_buffer);
    }
    return ret;
}

static int ut_gk_verify(const struct gatekeeper_device* dev, uint32_t uid, uint64_t challenge,
                        const uint8_t* enrolled_password_handle,
                        uint32_t enrolled_password_handle_length, const uint8_t* provided_password,
                        uint32_t provided_password_length, uint8_t** auth_token,
                        uint32_t* auth_token_length, bool* request_reenroll) {
    int ret = 0;
    uint8_t* position = NULL;
    uint8_t* gatekeeper_transfer_buffer = NULL;
    ut_gk_verify_out_t* ut_gk_verify_out = NULL;
    ut_gk_struct_t* ut_gk_struct = NULL;
    (void)dev;
    IMSG_ENTER();
    if (enrolled_password_handle == NULL || enrolled_password_handle_length == 0 ||
        provided_password == NULL || provided_password_length == 0 || auth_token == NULL ||
        auth_token_length == NULL || request_reenroll == NULL) {
        IMSG_ERROR(
            "[%s] Input param error, handle %#x || handle_length %d || provided_password %#x || "
            "provided_password_length %d || auth_token %#x || auth_token_length %#x || "
            "request_reenroll %#x",
            __func__, enrolled_password_handle, enrolled_password_handle_length, provided_password,
            provided_password_length, auth_token, auth_token_length, request_reenroll);
        ret = -EINVAL;
        goto out;
    }
    if (provided_password_length >= 100) {
        IMSG_ERROR("[%s] provided_password_length >= 100", __func__);
        provided_password_length = 17;
        // return -EINVAL;
    }

#ifdef GK_DEBUG
    IMSG_DEBUG("====[%s] [ree] verify variable [in] begin====", __func__);
    IMSG_DEBUG("====[%s] uid is %lu====", __func__, uid);
    IMSG_DEBUG("====[%s] challenge is %llu====", __func__, challenge);
    IMSG_DEBUG("====[%s] enrolled_password_handle_length is %lu====", __func__,
               enrolled_password_handle_length);
    IMSG_DEBUG("====[%s] provided_password_length is %lu====", __func__, provided_password_length);
    IMSG_DEBUG("====[%s] version is %u====", __func__,
               ((struct password_handle_t*)(enrolled_password_handle))->version);
    IMSG_DEBUG("====[%s] user_id is %llu====", __func__,
               ((struct password_handle_t*)(enrolled_password_handle))->user_id);
    IMSG_DEBUG("====[%s] flags is %llu====", __func__,
               ((struct password_handle_t*)(enrolled_password_handle))->flags);
    IMSG_DEBUG("====[%s] salt is %llu====", __func__,
               ((struct password_handle_t*)(enrolled_password_handle))->salt);
    IMSG_DEBUG("====[%s] hardware_backed is %u====", __func__,
               ((struct password_handle_t*)(enrolled_password_handle))->hardware_backed);
    IMSG_DEBUG("====[%s] [ree] verify variable [in] end ====", __func__);
#endif
    gatekeeper_transfer_buffer = (uint8_t*)malloc(GATEKEEPER_BUFF_SIZE);
    if (NULL == gatekeeper_transfer_buffer) {
        IMSG_ERROR("[%s] unable to malloc gatekeeper_transfer_buffer", __func__);
        ret = -EINVAL;
        goto out;
    }
    memset(gatekeeper_transfer_buffer, 0, GATEKEEPER_BUFF_SIZE);

    ut_gk_verify_out = (ut_gk_verify_out_t*)malloc(sizeof(ut_gk_verify_out_t));
    if (NULL == ut_gk_verify_out) {
        IMSG_ERROR("[%s] unable to malloc ut_gk_verify_out", __func__);
        ret = -EINVAL;
        goto out;
    }
    memset(ut_gk_verify_out, 0, sizeof(ut_gk_verify_out_t));

    ut_gk_struct = (ut_gk_struct_t*)malloc(sizeof(ut_gk_struct_t));
    if (NULL == ut_gk_struct) {
        IMSG_ERROR("[%s] unable to malloc ut_gk_struct", __func__);
        ret = -EINVAL;
        goto out;
    }
    memset(ut_gk_struct, 0, sizeof(ut_gk_struct_t));

    // set ut_gk_struct for  const length parts
    ut_gk_struct->cmd = CMD_ID_VARIFY;
    ut_gk_struct->uid = uid;
    ut_gk_struct->ut_gk_verify.challenge = challenge;
    ut_gk_struct->ut_gk_verify.enrolled_password_handle_length = enrolled_password_handle_length;
    ut_gk_struct->ut_gk_verify.provided_password_length = provided_password_length;

    // copy ut_gk_struct to gatekeeper_transfer_buffer
    memcpy((void*)gatekeeper_transfer_buffer, (void*)ut_gk_struct, sizeof(ut_gk_struct_t));
    position = (uint8_t*)gatekeeper_transfer_buffer + sizeof(ut_gk_struct_t);

    // copy varify length parts to gatekeeper_transfer_buffer
    memcpy(position, enrolled_password_handle, enrolled_password_handle_length);
    position += enrolled_password_handle_length;
    memcpy(position, provided_password, provided_password_length);

    // transfer buff to tee
    if (ut_gk_transfer_buff(gatekeeper_transfer_buffer)) {
        IMSG_ERROR("[%s] can't transfer buff to tee", __func__);
        ret = -EINVAL;
        goto out;
    }

    // copy buffer to struct
    memcpy(ut_gk_verify_out, gatekeeper_transfer_buffer, sizeof(ut_gk_verify_out_t));

    // if there is an error?
    if (ut_gk_verify_out->error == ERROR_RETRY) {
#ifdef GK_DEBUG
        IMSG_DEBUG("====[%s] ut_gk_verify_out->error == ERROR_RETRY ====", __func__);
        IMSG_DEBUG("====[%s] ut_gk_verify_out->retry_timeout is %lu====", __func__,
                   ut_gk_verify_out->retry_timeout);
#endif

        uint32_t retry_timeout;
        retry_timeout = ut_gk_verify_out->retry_timeout;
        IMSG_INFO("====  From ree verify, ERROR_RETRY!! ====");
        ret = retry_timeout;
        goto out;
    } else if (ut_gk_verify_out->error != ERROR_NONE) {
        IMSG_INFO("====  From ree verify, ERROR!! ====");
        ret = -EINVAL;
        goto out;
    }

    // copy gatekeeper_transfer_buffer to return variable
    *request_reenroll = ut_gk_verify_out->request_reenroll;
    *auth_token_length = ut_gk_verify_out->auth_token_length;
    *auth_token = (uint8_t*)malloc(*auth_token_length);
    memcpy(*auth_token, gatekeeper_transfer_buffer + sizeof(ut_gk_verify_out_t),
           *auth_token_length);

#ifdef GK_DEBUG
    IMSG_DEBUG("====[%s] [ree] verify variable [out] begin====", __func__);
    IMSG_DEBUG("====[%s] ut_gk_verify_out->error no error ====", __func__);
    IMSG_DEBUG("====[%s] request_reenroll is %u====", __func__, *request_reenroll);
    IMSG_DEBUG("====[%s] auth_token_length is %lu====", __func__, *auth_token_length);
    IMSG_DEBUG("====[%s] version is %u====", __func__, ((hw_auth_token_t*)(*auth_token))->version);
    IMSG_DEBUG("====[%s] challenge is %llu====", __func__,
               ((hw_auth_token_t*)(*auth_token))->challenge);
    IMSG_DEBUG("====[%s] user_id is %llu====", __func__,
               ((hw_auth_token_t*)(*auth_token))->user_id);
    IMSG_DEBUG("====[%s] authenticator_id is %llu====", __func__,
               ((hw_auth_token_t*)(*auth_token))->authenticator_id);
    IMSG_DEBUG("====[%s] authenticator_type is %lu====", __func__,
               ((hw_auth_token_t*)(*auth_token))->authenticator_type);
    IMSG_DEBUG("====[%s] timestamp is %llu====", __func__,
               ((hw_auth_token_t*)(*auth_token))->timestamp);
    IMSG_DEBUG("====[%s] [ree] verify variable [out] end====", __func__);
#endif
    IMSG_INFO("====  From ree verify, SUCCESS!! ====");

out:
    if (ut_gk_verify_out != NULL) {
        free(ut_gk_verify_out);
    }
    if (ut_gk_struct != NULL) {
        free(ut_gk_struct);
    }
    if (gatekeeper_transfer_buffer != NULL) {
        free(gatekeeper_transfer_buffer);
    }
    return ret;
}

int ut_gk_delete_user(const struct gatekeeper_device* dev, uint32_t uid) {
    int ret = 0;
    ut_gk_struct_t* ut_gk_struct = (ut_gk_struct_t*)malloc(sizeof(ut_gk_struct_t));
    uint8_t* gatekeeper_transfer_buffer = (uint8_t*)malloc(GATEKEEPER_BUFF_SIZE);
    ut_gk_del_user_out_t ut_gk_del_user_out;
    (void)dev;
    IMSG_ENTER();

    if (NULL == gatekeeper_transfer_buffer) {
        IMSG_ERROR("[%s] unable to malloc gatekeeper_transfer_buffer", __func__);
        ret = -EINVAL;
        goto out;
    }
    memset(gatekeeper_transfer_buffer, 0, GATEKEEPER_BUFF_SIZE);

    if (NULL == ut_gk_struct) {
        IMSG_ERROR("[%s] unable to malloc ut_gk_struct", __func__);
        ret = -EINVAL;
        goto out;
    }
    memset(ut_gk_struct, 0, sizeof(ut_gk_struct_t));

    // set ut_gk_struct for  const length parts
    ut_gk_struct->cmd = CMD_ID_DELETE_USER;
    ut_gk_struct->uid = uid;

    // copy ut_gk_struct to gatekeeper_transfer_buffer
    memcpy((void*)gatekeeper_transfer_buffer, (void*)ut_gk_struct, sizeof(ut_gk_struct_t));

    // transfer buff to tee
    if (ut_gk_transfer_buff(gatekeeper_transfer_buffer)) {
        IMSG_ERROR("[%s] can't transfer buff to tee", __func__);
        ret = -EINVAL;
        goto out;
    }

    memcpy(&ut_gk_del_user_out, gatekeeper_transfer_buffer, sizeof(ut_gk_del_user_out_t));
    if (ut_gk_del_user_out.error != ERROR_NONE) {
        IMSG_INFO("====  From ree delete_user, FAILED!! %d ====", ut_gk_del_user_out.error);
        ret = -EINVAL;
        goto out;
    }
    IMSG_INFO("====  From ree delete_user, SUCCESS!! ====");

out:
    if (ut_gk_struct != NULL) {
        free(ut_gk_struct);
    }
    if (gatekeeper_transfer_buffer != NULL) {
        free(gatekeeper_transfer_buffer);
    }
    return ret;
}

static int ut_gk_close(hw_device_t* dev) {
    IMSG_ENTER();
    free(dev);
    return 0;
}

static int ut_gk_open(const hw_module_t* module, const char* name, hw_device_t** device) {
    IMSG_ENTER();

    if (strcmp(name, HARDWARE_GATEKEEPER) != 0) {
        IMSG_ERROR("[%s] strcmp(name, HARDWARE_GATEKEEPER) != 0", __func__);
        return -EINVAL;
    }

    dev = (gatekeeper_device_t*)malloc(sizeof(gatekeeper_device_t));
    if (NULL == dev) {
        IMSG_ERROR("[%s] unable to malloc dev", __func__);
        return -EFAULT;
    }
    memset(dev, 0, sizeof(gatekeeper_device_t));

    dev->common.tag = HARDWARE_DEVICE_TAG;
    dev->common.version = 1;
    dev->common.module = (struct hw_module_t*)module;
    dev->common.close = ut_gk_close;
    dev->enroll = ut_gk_enroll;
    dev->verify = ut_gk_verify;
    dev->delete_user = ut_gk_delete_user;
    dev->delete_all_users = NULL;

    *device = &dev->common;
    return 0;
}

static struct hw_module_methods_t gatekeeper_module_methods = {
    .open = ut_gk_open,
};

struct gatekeeper_module HAL_MODULE_INFO_SYM __attribute__((visibility("default"))) = {
    .common =
        {
            .tag = HARDWARE_MODULE_TAG,
            .module_api_version = GATEKEEPER_MODULE_API_VERSION_0_1,
            .hal_api_version = HARDWARE_HAL_API_VERSION,
            .id = GATEKEEPER_HARDWARE_MODULE_ID,
            .name = "Gatekeeper UT HAL",
            .author = "The Microtrust Gatekeeper Source Project",
            .methods = &gatekeeper_module_methods,
            .dso = 0,
            .reserved = {},
        },
};
