/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.services.telephony;

import android.os.AsyncResult;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.telecom.Connection;
import android.telecom.PhoneAccountHandle;
import android.telephony.DisconnectCause;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;

import com.android.internal.telephony.Call;
import com.android.internal.telephony.CallStateException;
import com.android.internal.telephony.Phone;
import com.android.services.telephony.DisconnectCauseUtil;
import com.android.services.telephony.Log;
import com.android.services.telephony.TelephonyConnection;

import com.mediatek.internal.telephony.MtkGsmCdmaPhone;

import java.util.ArrayList;
import java.util.List;

import mediatek.telecom.MtkConnection;
import mediatek.telecom.MtkTelecomManager;


/// M: CC: to check whether the device has on-going ECC
import com.mediatek.internal.telephony.RadioManager;

/// M: CC: GSA HD Voice for 2/3G network support @{
import com.mediatek.services.telephony.MtkSpeechCodecType;
/// @}

/**
 * MTK TelephonyConnection abstract class.
 * Inherit from AOSP TelephonyConnection and adding required interfaces/methods for use.
 */
public abstract class MtkTelephonyConnection extends TelephonyConnection {

    private static final String TAG = "TelephonyConn";

    // Sensitive log task
    private static final String PROP_FORCE_DEBUG_KEY = "persist.log.tag.tel_dbg";
    private static final boolean SENLOG = TextUtils.equals(Build.TYPE, "user");
    private static final boolean SDBG = TextUtils.equals(Build.TYPE, "user") ? false : true;
    private static final boolean TELDBG = (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1);

    /// M: CC: ECC retry
    private boolean mIsLocallyDisconnecting = false;

    protected MtkTelephonyConnection(com.android.internal.telephony.Connection originalConnection,
            String callId, boolean isOutgoingCall) {
        super(originalConnection, callId, isOutgoingCall);
        /// M: CC: GSA HD Voice for 2/3G network support @{
        if (mSpeechType == null) {
            mSpeechType = MtkSpeechCodecType.fromInt(0); // do init if null
        }
        /// @}
        if (originalConnection != null) {
            registerForMtkEventHandler();
        }
    }

    private static final int MTK_EVENT_BASE = 1000;
    /// M: CC: Modem reset related handling
    private static final int EVENT_RADIO_OFF_OR_NOT_AVAILABLE       = MTK_EVENT_BASE;
    /// M: CC: For 3G VT only
    private static final int EVENT_VT_STATUS_INFO                   = MTK_EVENT_BASE + 1;
    /// M: CC: GSA HD Voice for 2/3G network support
    private static final int EVENT_SPEECH_CODEC_INFO                = MTK_EVENT_BASE + 2;
    /// M: CC: CDMA call accepted
    private static final int EVENT_CDMA_CALL_ACCEPTED               = MTK_EVENT_BASE + 3;

    /// M: CC: GSA HD Voice for 2/3G network support @{
    private static final String PROPERTY_HD_VOICE_STATUS = "af.ril.hd.voice.status";
    private MtkSpeechCodecType mSpeechType = null;
    /// @}

    /// M: CC: DTMF request special handling @{
    // Stop DTMF when TelephonyConnection is disconnected
    protected boolean mDtmfRequestIsStarted = false;
    /// @}

    private final Handler mMtkHandler = new Handler(mHandler.getLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_DISCONNECT:
                    int cause = mOriginalConnection == null
                            ? android.telephony.DisconnectCause.NOT_DISCONNECTED
                            : mOriginalConnection.getDisconnectCause();
                    Log.i(this, "Receives MSG_DISCONNECT, cause=" + cause);

                    /// M: CC: ECC retry @{
                    if (!mIsLocallyDisconnecting
                            && cause != android.telephony.DisconnectCause.NOT_DISCONNECTED
                            && cause != android.telephony.DisconnectCause.NORMAL
                            && cause != android.telephony.DisconnectCause.LOCAL) {
                        Log.i(this, "ECC retry: check whether need to retry, "
                                + "connectionState=" + mConnectionState);
                        // Assume only one ECC exists
                        if (mTreatAsEmergencyCall
                                && MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()
                                && mConnectionState.isDialing()
                                && !MtkTelephonyConnectionServiceUtil.getInstance()
                                        .eccRetryTimeout()) {
                            Log.i(this, "ECC retry: meet retry condition");
                            close(); // To removeConnection
                            MtkTelephonyConnectionServiceUtil.getInstance().performEccRetry();
                            break;
                        }
                    }
                    /// @}
                    updateState();
                    break;

                /// M: CC: Modem reset related handling @{
                case EVENT_RADIO_OFF_OR_NOT_AVAILABLE:
                    notifyConnectionLost();
                    onLocalDisconnected();
                    break;
                /// @}

                /// M: CC: For 3G VT only @{
                case EVENT_VT_STATUS_INFO:
                    int status = ((int[]) ((AsyncResult) msg.obj).result)[0];
                    notifyVtStatusInfo(status);
                    break;
                /// @}

                /// M: CC: GSA HD Voice for 2/3G network support @{
                case EVENT_SPEECH_CODEC_INFO:
                    int value = (int) ((AsyncResult) msg.obj).result;
                    log("EVENT_SPEECH_CODEC_INFO : " + value);
                    if (isHighDefAudio(value)) {
                        setAudioQuality(
                            com.android.internal.telephony.Connection.AUDIO_QUALITY_HIGH_DEFINITION);
                    } else {
                        setAudioQuality(
                            com.android.internal.telephony.Connection.AUDIO_QUALITY_STANDARD);
                    }
                    break;
                /// @}

                /// M: CC: CDMA call accepted @{
                case EVENT_CDMA_CALL_ACCEPTED:
                    Log.i(this, "Receives EVENT_CDMA_CALL_ACCEPTED");
                    updateConnectionCapabilities();
                    fireOnCdmaCallAccepted();
                    break;
                /// @}
            }
        }
    };

    /**
     * Creates a string representation of this {@link TelephonyConnection}.  Primarily intended for
     * use in log statements.
     *
     * @return String representation of the connection.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[TelephonyConnection objId:");
        sb.append(System.identityHashCode(this));
        sb.append(" telecomCallID:");
        sb.append(getTelecomCallId());
        sb.append(" type:");
        if (isImsConnection()) {
            sb.append("ims");
        } else if (this instanceof MtkGsmCdmaConnection) {
            sb.append("gsmcdma");
        }
        sb.append(" state:");
        sb.append(Connection.stateToString(getState()));
        sb.append(" capabilities:");
        sb.append(capabilitiesToString(getConnectionCapabilities()));
        sb.append(" properties:");
        sb.append(propertiesToString(getConnectionProperties()));
        sb.append(" address:");
        sb.append(Log.pii(getAddress()));
        sb.append(" originalConnection:");
        sb.append(mOriginalConnection);
        sb.append(" partOfConf:");
        if (getConference() == null) {
            sb.append("N");
        } else {
            sb.append("Y");
        }
        sb.append(" confSupported:");
        sb.append(mIsConferenceSupported ? "Y" : "N");
        sb.append("]");
        return sb.toString();
    }

    /// M: CC: Set PhoneAccountHandle for ECC @{
    private PhoneAccountHandle mAccountHandle;
    //[ALPS01794357]
    /**
     * @return The PhoneAccountHandle this connection really used.
     */
    public PhoneAccountHandle getAccountHandle() { return mAccountHandle; }

    /**
     *  set the PhoneAccountHandle this connection really used.
     */
    public void setAccountHandle(PhoneAccountHandle handle) {
        mAccountHandle = handle;
        sendConnectionEvent(MtkConnection.EVENT_PHONE_ACCOUNT_CHANGED,
                MtkConnection.ConnectionEventHelper.buildParamsForPhoneAccountChanged(handle));
    }
    /// @}

    /// M: CC: Modem reset related handling @{
    /**
     * Notify to telecomm if the connection is lost.
     * @hide
     */
    public final void notifyConnectionLost() {
        sendConnectionEvent(MtkConnection.EVENT_CONNECTION_LOST, null);
    }
    /// @}

    /// M: CC: Force updateState for Connection once its ConnectionService is set @{
    /**
     * Base class for forcing call state update after ConnectionService is set,
     * to keep phoneCapabilities up-to-date.
     * see {@link ConnectionService#addConnection}
     * To be overrided by children classes.
     * @hide
     */
    protected void fireOnCallState() {
        updateState();
    }
    /// @}

    /// M: CC: HangupAll for FTA 31.4.4.2 @{
    public void onHangupAll() {
        log("onHangupAll");
        if (mOriginalConnection != null) {
            try {
                Phone phone = getPhone();
                if (phone != null) {
                    ((MtkGsmCdmaPhone)phone).hangupAll();
                } else {
                    Log.w(TAG, "Attempting to hangupAll a connection without backing phone.");
                }
            } catch (CallStateException e) {
                Log.e(TAG, e, "Call to phone.hangupAll() failed with exception");
            }
        }
    }
    /// @}

    /// M: CC: TelephonyConnection destroy itself upon user hanging up [ALPS01850287] @{
    void onLocalDisconnected() {
        log("mOriginalConnection is null, local disconnect the call");
        /// M: CC: ECC retry @{
        if (mTreatAsEmergencyCall) {
            if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                Log.i(this, "ECC retry: clear ECC param");
                MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
            }
            /// M: CC: to check whether the device has on-going ECC
            RadioManager.getInstance().forceSetEccState(false);
        }
        /// @}
        setDisconnected(DisconnectCauseUtil.toTelecomDisconnectCause(
                android.telephony.DisconnectCause.LOCAL));
        close();
        updateConnectionCapabilities();
    }
    /// @}

    protected void hangup(int telephonyDisconnectCode) {
        /// M: CC: ECC retry @{
        mIsLocallyDisconnecting = true;
        /// @}
        super.hangup(telephonyDisconnectCode);
    }

    @Override
    public void setOriginalConnection(com.android.internal.telephony.Connection originalConnection) {
        Log.v(this, "new TelephonyConnection, originalConnection: " + originalConnection);
        /// M: CC: GSA HD Voice for 2/3G network support @{
        if (mSpeechType == null) {
            mSpeechType = MtkSpeechCodecType.fromInt(0); // do init if null
        }
        /// @}

        clearOriginalConnection();
        mOriginalConnectionExtras.clear();
        mOriginalConnection = originalConnection;
        mOriginalConnection.setTelecomCallId(getTelecomCallId());
        getPhone().registerForPreciseCallStateChanged(
                mHandler, MSG_PRECISE_CALL_STATE_CHANGED, null);
        getPhone().registerForHandoverStateChanged(
                mHandler, MSG_HANDOVER_STATE_CHANGED, null);
        getPhone().registerForRingbackTone(mHandler, MSG_RINGBACK_TONE, null);
        getPhone().registerForSuppServiceNotification(mHandler, MSG_SUPP_SERVICE_NOTIFY, null);
        getPhone().registerForOnHoldTone(mHandler, MSG_ON_HOLD_TONE, null);
        getPhone().registerForInCallVoicePrivacyOn(mHandler, MSG_CDMA_VOICE_PRIVACY_ON, null);
        getPhone().registerForInCallVoicePrivacyOff(mHandler, MSG_CDMA_VOICE_PRIVACY_OFF, null);
        registerForMtkEventHandler();
        mOriginalConnection.addPostDialListener(mPostDialListener);
        mOriginalConnection.addListener(mOriginalConnectionListener);

        // Set video state and capabilities
        setVideoState(mOriginalConnection.getVideoState());
        setOriginalConnectionCapabilities(mOriginalConnection.getConnectionCapabilities());
        setWifi(mOriginalConnection.isWifi());
        setAudioModeIsVoip(mOriginalConnection.getAudioModeIsVoip());
        setVideoProvider(mOriginalConnection.getVideoProvider());
        setAudioQuality(mOriginalConnection.getAudioQuality());
        setTechnologyTypeExtra();

        // Post update of extras to the handler; extras are updated via the handler to ensure thread
        // safety. The Extras Bundle is cloned in case the original extras are modified while they
        // are being added to mOriginalConnectionExtras in updateExtras.
        Bundle connExtras = mOriginalConnection.getConnectionExtras();
            mHandler.obtainMessage(MSG_CONNECTION_EXTRAS_CHANGED, connExtras == null ? null :
                    new Bundle(connExtras)).sendToTarget();

        if (PhoneNumberUtils.isEmergencyNumber(mOriginalConnection.getAddress())) {
            mTreatAsEmergencyCall = true;
        }

        if (isImsConnection()) {
            mWasImsConnection = true;
        }
        mIsMultiParty = mOriginalConnection.isMultiparty();

        Bundle extrasToPut = new Bundle();
        List<String> extrasToRemove = new ArrayList<>();
        if (mOriginalConnection.isActiveCallDisconnectedOnAnswer()) {
            extrasToPut.putBoolean(Connection.EXTRA_ANSWERING_DROPS_FG_CALL, true);
        } else {
            extrasToRemove.add(Connection.EXTRA_ANSWERING_DROPS_FG_CALL);
        }

        if (shouldSetDisableAddCallExtra()) {
            extrasToPut.putBoolean(Connection.EXTRA_DISABLE_ADD_CALL, true);
        } else {
            extrasToRemove.add(Connection.EXTRA_DISABLE_ADD_CALL);
        }
        putExtras(extrasToPut);
        removeExtras(extrasToRemove);

        // updateState can set mOriginalConnection to null if its state is DISCONNECTED, so this
        // should be executed *after* the above setters have run.
        updateState();
        if (mOriginalConnection == null) {
            Log.w(this, "original Connection was nulled out as part of setOriginalConnection. " +
                    originalConnection);
        }

        fireOnOriginalConnectionConfigured();
    }

    /**
     * Un-sets the underlying radio connection.
     */
    @Override
    protected void clearOriginalConnection() {
        if (mOriginalConnection != null) {
            if (getPhone() != null) {
                /// M: CC: DTMF request special handling @{
                // Stop DTMF when TelephonyConnection is disconnected
                if (mDtmfRequestIsStarted) {
                    onStopDtmfTone();
                    mDtmfRequestIsStarted = false;
                }
                /// @}
                getPhone().unregisterForPreciseCallStateChanged(mHandler);
                getPhone().unregisterForRingbackTone(mHandler);
                getPhone().unregisterForHandoverStateChanged(mHandler);
                getPhone().unregisterForDisconnect(mHandler);
                getPhone().unregisterForSuppServiceNotification(mHandler);
                getPhone().unregisterForOnHoldTone(mHandler);
                getPhone().unregisterForInCallVoicePrivacyOn(mHandler);
                getPhone().unregisterForInCallVoicePrivacyOff(mHandler);
                /// M: CC: Modem reset related handling
                getPhone().unregisterForRadioOffOrNotAvailable(mMtkHandler);
                /// M: CC: GSA HD Voice for 2/3G network support @{
                if (getPhone() instanceof MtkGsmCdmaPhone) {
                    ((MtkGsmCdmaPhone) getPhone()).unregisterForSpeechCodecInfo(mMtkHandler);
                }
                /// @}
                /// M: CC: For 3G VT only
                if (getPhone() instanceof MtkGsmCdmaPhone) {
                    ((MtkGsmCdmaPhone) getPhone()).unregisterForVtStatusInfo(mMtkHandler);
                }
                /// M: CC: CDMA call accepted @{
                if (getPhone() instanceof MtkGsmCdmaPhone) {
                    ((MtkGsmCdmaPhone) getPhone()).unregisterForCdmaCallAccepted(mMtkHandler);
                }
                /// @}
            }
            mOriginalConnection.removePostDialListener(mPostDialListener);
            mOriginalConnection.removeListener(mOriginalConnectionListener);
            mOriginalConnection = null;
        }
    }
    private void log(String s) {
        Log.d(TAG, s);
    }

    protected void updateStateInternal() {
        if (mOriginalConnection == null) {
            return;
        }
        Call.State newState;
        // If the state is overridden and the state of the original connection hasn't changed since,
        // then we continue in the overridden state, else we go to the original connection's state.
        if (mIsStateOverridden && mOriginalConnectionState == mOriginalConnection.getState()) {
            newState = mConnectionOverriddenState;
        } else {
            newState = mOriginalConnection.getState();
        }

        /// M: CC: ECC retry @{
        if (mTreatAsEmergencyCall
                && !mIsLocallyDisconnecting
                && mOriginalConnection.getState() == Call.State.DISCONNECTED
                && (mConnectionState.isDialing()
                    || mConnectionState == Call.State.IDLE)) {
            int cause = mOriginalConnection.getDisconnectCause();
            Log.i(this, "ECC retry: remote DISCONNECTED, state=" + mConnectionState + ", cause=" + cause);

            // Assume only one ECC exists
            if (cause != android.telephony.DisconnectCause.NORMAL
                    && cause != android.telephony.DisconnectCause.LOCAL
                    && cause != android.telephony.DisconnectCause.NOT_VALID
                    && MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()
                    && !MtkTelephonyConnectionServiceUtil.getInstance().eccRetryTimeout()) {
                newState = mConnectionState;
                Log.i(this, "ECC retry: meet retry condition, keep state=" + newState);
            }
        }
        /// @}

        Log.v(this, "Update state from %s to %s for %s", mConnectionState, newState, this);

        if (mConnectionState != newState) {
            mConnectionState = newState;
            /// M: CC: GSA HD Voice for 2/3G network support @{
            // Force update when call state is changed to DIALING/ACTIVE/INCOMING(Foreground)
            switch (newState) {
                case ACTIVE:
                case DIALING:
                case ALERTING:
                case INCOMING:
                case WAITING:
                    if (isHighDefAudio(0)) {
                        setAudioQuality(com.android.internal.telephony.Connection.
                                AUDIO_QUALITY_HIGH_DEFINITION);
                    } else {
                        setAudioQuality(com.android.internal.telephony.Connection.
                                AUDIO_QUALITY_STANDARD);
                    }
                    break;
                default:
                    break;
            }
            /// @}

            switch (newState) {
                case IDLE:
                    break;
                case ACTIVE:
                    /// M: CC: ECC retry @{
                    // Assume only one ECC exists
                    if (mTreatAsEmergencyCall
                            && MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                        Log.i(this, "ECC retry: clear ECC param");
                        MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
                    }
                    /// @}
                    setActiveInternal();
                    break;
                case HOLDING:
                    setOnHold();
                    break;
                case DIALING:
                case ALERTING:
                    if (mOriginalConnection != null && mOriginalConnection.isPulledCall()) {
                        setPulling();
                    } else {
                        setDialing();
                    }
                    break;
                case INCOMING:
                case WAITING:
                    setRinging();
                    break;
                case DISCONNECTED:
                    /// M: CC: ECC retry @{
                    // Assume only one ECC exists
                    if (mTreatAsEmergencyCall) {
                        if (MtkTelephonyConnectionServiceUtil.getInstance().isEccRetryOn()) {
                            Log.i(this, "ECC retry: clear ECC param");
                            MtkTelephonyConnectionServiceUtil.getInstance().clearEccRetryParams();
                        }
                        /// M: CC: to check whether the device has on-going ECC
                        RadioManager.getInstance().forceSetEccState(false);
                    }
                    /// @}

                    // We can get into a situation where the radio wants us to redial the same
                    // emergency call on the other available slot. This will not set the state to
                    // disconnected and will instead tell the TelephonyConnectionService to create
                    // a new originalConnection using the new Slot.
                    if (mOriginalConnection.getDisconnectCause() ==
                            DisconnectCause.DIALED_ON_WRONG_SLOT) {
                        fireOnOriginalConnectionRetryDial();
                    } else {
                        setDisconnected(DisconnectCauseUtil.toTelecomDisconnectCause(
                                mOriginalConnection.getDisconnectCause(),
                                mOriginalConnection.getVendorDisconnectCause()));
                        close();
                    }
                    break;
                case DISCONNECTING:
                    /// M: CC: ECC retry @{
                    mIsLocallyDisconnecting = true;
                    /// @}
                    break;
            }
        }
    }

    protected static String capabilitiesToStringInternal(int capabilities, boolean isLong) {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        if (isLong) {
            builder.append("Capabilities:");
        }

        if (can(capabilities, CAPABILITY_HOLD)) {
            builder.append(isLong ? " CAPABILITY_HOLD" : " hld");
        }
        if (can(capabilities, CAPABILITY_SUPPORT_HOLD)) {
            builder.append(isLong ? " CAPABILITY_SUPPORT_HOLD" : " sup_hld");
        }
        if (can(capabilities, CAPABILITY_MERGE_CONFERENCE)) {
            builder.append(isLong ? " CAPABILITY_MERGE_CONFERENCE" : " mrg_cnf");
        }
        if (can(capabilities, CAPABILITY_SWAP_CONFERENCE)) {
            builder.append(isLong ? " CAPABILITY_SWAP_CONFERENCE" : " swp_cnf");
        }
        if (can(capabilities, CAPABILITY_RESPOND_VIA_TEXT)) {
            builder.append(isLong ? " CAPABILITY_RESPOND_VIA_TEXT" : " txt");
        }
        if (can(capabilities, CAPABILITY_MUTE)) {
            builder.append(isLong ? " CAPABILITY_MUTE" : " mut");
        }
        if (can(capabilities, CAPABILITY_MANAGE_CONFERENCE)) {
            builder.append(isLong ? " CAPABILITY_MANAGE_CONFERENCE" : " mng_cnf");
        }
        if (can(capabilities, CAPABILITY_SUPPORTS_VT_LOCAL_RX)) {
            builder.append(isLong ? " CAPABILITY_SUPPORTS_VT_LOCAL_RX" : " VTlrx");
        }
        if (can(capabilities, CAPABILITY_SUPPORTS_VT_LOCAL_TX)) {
            builder.append(isLong ? " CAPABILITY_SUPPORTS_VT_LOCAL_TX" : " VTltx");
        }
        if (can(capabilities, CAPABILITY_SUPPORTS_VT_LOCAL_BIDIRECTIONAL)) {
            builder.append(isLong ? " CAPABILITY_SUPPORTS_VT_LOCAL_BIDIRECTIONAL" : " VTlbi");
        }
        if (can(capabilities, CAPABILITY_SUPPORTS_VT_REMOTE_RX)) {
            builder.append(isLong ? " CAPABILITY_SUPPORTS_VT_REMOTE_RX" : " VTrrx");
        }
        if (can(capabilities, CAPABILITY_SUPPORTS_VT_REMOTE_TX)) {
            builder.append(isLong ? " CAPABILITY_SUPPORTS_VT_REMOTE_TX" : " VTrtx");
        }
        if (can(capabilities, CAPABILITY_SUPPORTS_VT_REMOTE_BIDIRECTIONAL)) {
            builder.append(isLong ? " CAPABILITY_SUPPORTS_VT_REMOTE_BIDIRECTIONAL" : " VTrbi");
        }
        if (can(capabilities, CAPABILITY_CANNOT_DOWNGRADE_VIDEO_TO_AUDIO)) {
            builder.append(isLong ? " CAPABILITY_CANNOT_DOWNGRADE_VIDEO_TO_AUDIO" : " !v2a");
        }
        if (can(capabilities, CAPABILITY_SPEED_UP_MT_AUDIO)) {
            builder.append(isLong ? " CAPABILITY_SPEED_UP_MT_AUDIO" : " spd_aud");
        }
        if (can(capabilities, CAPABILITY_CAN_UPGRADE_TO_VIDEO)) {
            builder.append(isLong ? " CAPABILITY_CAN_UPGRADE_TO_VIDEO" : " a2v");
        }
        if (can(capabilities, CAPABILITY_CAN_PAUSE_VIDEO)) {
            builder.append(isLong ? " CAPABILITY_CAN_PAUSE_VIDEO" : " paus_VT");
        }
        if (can(capabilities, CAPABILITY_CONFERENCE_HAS_NO_CHILDREN)) {
            builder.append(isLong ? " CAPABILITY_SINGLE_PARTY_CONFERENCE" : " 1p_cnf");
        }
        if (can(capabilities, CAPABILITY_CAN_SEND_RESPONSE_VIA_CONNECTION)) {
            builder.append(isLong ? " CAPABILITY_CAN_SEND_RESPONSE_VIA_CONNECTION" : " rsp_by_con");
        }
        if (can(capabilities, CAPABILITY_CAN_PULL_CALL)) {
            builder.append(isLong ? " CAPABILITY_CAN_PULL_CALL" : " pull");
        }
        /// M: CC: Interface for ECT @{
        if ((capabilities & MtkConnection.CAPABILITY_CONSULTATIVE_ECT) != 0) {
            builder.append(" CAPABILITY_CONSULTATIVE_ECT");
        }
        /// @}

        builder.append("]");
        return builder.toString();
    }

    /// M: CC: Proprietary CRSS handling @{
    /** @hide */
    public final void notifyActionFailed(int action) {
        Log.i(this, "notifyActionFailed action = " + action);
        sendConnectionEvent(MtkConnection.EVENT_OPERATION_FAILED,
                MtkConnection.ConnectionEventHelper.buildParamsForOperationFailed(action));
    }

    /** @hide */
    public void notifySSNotificationToast(
            int notiType, int type, int code, String number, int index) {
        Log.i(this, "notifySSNotificationToast notiType = " + notiType + " type = " + type
                + " code = " + code + " number = " + number + " index = " + index);
        sendConnectionEvent(MtkConnection.EVENT_SS_NOTIFICATION,
                MtkConnection.ConnectionEventHelper.buildParamsForSsNotification(notiType, type,
                        code, number, index));
    }

    /** @hide */
    public void notifyNumberUpdate(String number) {
        Log.i(this, "notifyNumberUpdate number = " + number);
        if (!TextUtils.isEmpty(number)) {
            sendConnectionEvent(MtkConnection.EVENT_NUMBER_UPDATED,
                    MtkConnection.ConnectionEventHelper.buildParamsForNumberUpdated(number));
        }
    }

    /** @hide */
    public void notifyIncomingInfoUpdate(int type, String alphaid, int cliValidity) {
        Log.i(this, "notifyIncomingInfoUpdate type = "
                + type + " alphaid = " + alphaid + " cliValidity = " + cliValidity);
        sendConnectionEvent(MtkConnection.EVENT_INCOMING_INFO_UPDATED,
                MtkConnection.ConnectionEventHelper.buildParamsForIncomingInfoUpdated(type,
                        alphaid, cliValidity));
    }
    /// @}

    /// M: CC: Interface for ECT @{
    /**
     * Handle explicit call transfer.
     * @hide
     */
    public void onExplicitCallTransfer() {
        log("onExplicitCallTransfer");
        Phone phone = mOriginalConnection.getCall().getPhone();
        try {
            phone.explicitCallTransfer();
        } catch (CallStateException e) {
            Log.e(TAG, e, "Exception occurred while trying to do ECT.");
        }
    }

    /// M: CC: For 3G VT only @{
    /**
     * Notify to telecomm of the VT call status info.
     * @param status The VT status
     * @hide
     */
    public final void notifyVtStatusInfo(int status) {
        Log.d(this, "notifyVtStatusInfo %s" + status);
        sendConnectionEvent(MtkConnection.EVENT_3G_VT_STATUS_CHANGED,
                MtkConnection.ConnectionEventHelper.buildParamsFor3GVtStatusChanged(status));
    }
    /// @}

    /// M: CC: GSA HD Voice for 2/3G network support @{
    /**
     * This function used to check whether the input value is of HD type.
     * @param value The speech codec type value
     * @return {@code true} if the codec type is of HD type and {@code false} otherwise.
     */
    private boolean isHighDefAudio(int value) {
        String op = null;
        String hdStat = null;
        op = android.os.SystemProperties.get("persist.operator.optr", "OM");
        log("isHighDefAudio, optr:" + op);
        op = op.concat("=");
        hdStat = android.os.SystemProperties.get(PROPERTY_HD_VOICE_STATUS, "");
        if (hdStat != null && !hdStat.equals("")) {
            log("HD voice status: " + hdStat);
            boolean findOp = hdStat.indexOf(op) != -1;
            boolean findOm = hdStat.indexOf("OM=") != -1;
            int start = 0;
            if (findOp && !op.equals("OM=")) {
                start = hdStat.indexOf(op) + op.length(); //OPXX=Y
            } else if (findOm) {
                start = hdStat.indexOf("OM=") + 3; //OM=Y
            }
            // Ex: ril.hd.voice.status="OM=Y;OP07=N;OP12=Y;"
            String isHd = hdStat.length() > (start + 1) ? hdStat.substring(start, start + 1) : "";
            if (isHd.equals("Y")) {
                return true;
            } else {
                return false;
            }
        } else {
            if (value != 0) {
                mSpeechType = MtkSpeechCodecType.fromInt(value);
            }
            boolean isHd = mSpeechType.isHighDefAudio();
            log("isHighDefAudio = " + isHd);
            return isHd;
        }
    }
    /// @}

    /// M: CC: CDMA call accepted @{
    private void fireOnCdmaCallAccepted() {
        Log.i(this, "fireOnCdmaCallAccepted: %s", stateToString(getState()));
        sendConnectionEvent(MtkConnection.EVENT_CDMA_CALL_ACCEPTED, null);
    }
    /// @}

    /// M: CC: ECC retry @{
    protected void resetTreatAsEmergencyCall() {
        mTreatAsEmergencyCall = false;
    }

    protected void setEmergencyCall(boolean isEmergency) {
        mTreatAsEmergencyCall = isEmergency;
        if (isEmergency) {
            Log.i(this, "ECC retry: set call as emergency call");
        }
    }
    /// @}

    private void registerForMtkEventHandler() {
        if (mMtkHandler == null) {
            Log.d(TAG, "registerForMtkEventHandler() failed, invalid handler");
            return;
        }
        Log.v(TAG, "registerForMtkEventHandler()");
        /// M: CC: ECC retry @{
        getPhone().registerForDisconnect(mMtkHandler, MSG_DISCONNECT, null);
        /// @}
        /// M: CC: Modem reset related handling
        getPhone().registerForRadioOffOrNotAvailable(
                mMtkHandler, EVENT_RADIO_OFF_OR_NOT_AVAILABLE, null);
        /// M: CC: GSA HD Voice for 2/3G network support @{
        if (getPhone() instanceof MtkGsmCdmaPhone) {
            ((MtkGsmCdmaPhone) getPhone()).registerForSpeechCodecInfo(mMtkHandler,
                    EVENT_SPEECH_CODEC_INFO, null);
        }
        /// @}
        /// M: CC: For 3G VT only
        if (getPhone() instanceof MtkGsmCdmaPhone) {
            ((MtkGsmCdmaPhone) getPhone()).registerForVtStatusInfo(
                    mMtkHandler, EVENT_VT_STATUS_INFO, null);
        }
        /// M: CC: CDMA call accepted @{
        if (getPhone() instanceof MtkGsmCdmaPhone) {
            ((MtkGsmCdmaPhone) getPhone()).registerForCdmaCallAccepted(
                    mMtkHandler, EVENT_CDMA_CALL_ACCEPTED, null);
        }
        /// @}
    }
}
