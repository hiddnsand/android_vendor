package com.mediatek.providers.settings.ext;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;

import com.mediatek.common.util.OperatorCustomizationFactoryLoader;
import com.mediatek.common.util.OperatorCustomizationFactoryLoader.OperatorFactoryInfo;

public class OpSettingsProviderCustomizationFactoryBase {

    public IDatabaseHelperExt makeDatabaseHelp(Context context) {
        return new DefaultDatabaseHelperExt(context);
    }

    private static final List<OperatorFactoryInfo> sOpFactoryInfoList
                                                = new ArrayList<OperatorFactoryInfo>();
    static {
        sOpFactoryInfoList.add(
                new OperatorFactoryInfo("OP01SettingsProvider.apk",
                    "com.mediatek.providers.settings.op01.Op01SettingsProviderCustomizationFactory",
                    "com.mediatek.providers.settings.op01",
                    "OP01"
                 ));

        sOpFactoryInfoList.add(
                new OperatorFactoryInfo("OP03SettingsProvider.apk",
                    "com.mediatek.op03.settingsProvider.OP03SettingsProviderCustomizationFactory",
                    "com.mediatek.op03.settingsProvider",
                    "OP03",
                    "SEGDEFAULT"
                 ));

        sOpFactoryInfoList.add(
                new OperatorFactoryInfo("OP05SettingsProvider.jar",
                    "com.mediatek.op05.settingsProvider.OP05SettingsProviderCustomizationFactory",
                    null,
                    "OP05",
                    "SEGDEFAULT"
                 ));

        sOpFactoryInfoList.add(
                new OperatorFactoryInfo("OP06SettingsProvider.jar",
                    "com.mediatek.op06.settingsProvider.OP06SettingsProviderCustomizationFactory",
                    null,
                    "OP06",
                    "SEGDEFAULT"
                 ));

        sOpFactoryInfoList.add(
                new OperatorFactoryInfo("OP08SettingsProvider.apk",
                    "com.mediatek.op08.settingsProvider.OP08SettingsProviderCustomizationFactory",
                    "com.mediatek.op08.settingsProvider",
                    "OP08",
                    "SEGDEFAULT"
                 ));

        sOpFactoryInfoList.add(
                new OperatorFactoryInfo("OP16SettingsProvider.jar",
                    "com.mediatek.op16.settingsProvider.OP16SettingsProviderCustomizationFactory",
                    null,
                    "OP16",
                    "SEGDEFAULT"
                 ));

        sOpFactoryInfoList.add(
                new OperatorFactoryInfo("OP18SettingsProvider.jar",
                    "com.mediatek.op18.settingsProvider.OP18SettingsProviderCustomizationFactory",
                    null,
                    "OP18",
                    "SEGDEFAULT"
                 ));
    }

    static OpSettingsProviderCustomizationFactoryBase sFactory = null;
    public static synchronized OpSettingsProviderCustomizationFactoryBase getOpFactory(
            Context context) {
      if (sFactory == null) {
            sFactory =(OpSettingsProviderCustomizationFactoryBase)
                      OperatorCustomizationFactoryLoader.loadFactory(context, sOpFactoryInfoList);
            if (sFactory == null) {
                sFactory = new OpSettingsProviderCustomizationFactoryBase();
            }
        }
        return sFactory;
    }
}
