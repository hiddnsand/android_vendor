/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*
 *
 */

package com.mediatek.xcap.client;

import android.content.Context;
import android.net.Network;
import android.os.Build;
import android.os.IBinder;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.android.okhttp.Headers;

import com.mediatek.gba.GbaManager;
import com.mediatek.gba.NafSessionKey;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.xcap.auth.AkaDigestAuth;
import com.mediatek.xcap.header.WwwAuthHeader;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
/**
 * XcapClient class.
 */
public class XcapClient {
    private static final String TAG = "XcapClient";

    // Sensitive log task
    private static final String PROP_FORCE_DEBUG_KEY = "persist.log.tag.tel_dbg";
    private static final boolean SENLOG = TextUtils.equals(Build.TYPE, "user");
    private static final boolean TELDBG = (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1);

    private HttpURLConnection mConnection = null;

    private GbaManager mGbaManager;
    private static Map<Integer, String> mNafFqdnCache = new HashMap<Integer, String>();
    private String mUserAgent;

    // Default connection and socket timeout of 60 seconds. Tweak to taste.
    private static final int SOCKET_OPERATION_TIMEOUT = 30 * 1000;
    private static final int MAX_SOCKET_CONNECTION = 30;
    private static final String AUTH_HDR = "WWW-Authenticate";

    private XcapDebugParam mDebugParam = XcapDebugParam.getInstance();
    private Network mNetwork;
    private Context mContext;
    private int     mPhoneId;
    private static int mRequestCount = 0;
    private TrustManager[] mTrustAllCerts = new TrustManager[] {new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
        }

        public void checkClientTrusted(java.security.cert.X509Certificate[] certs,
                String authType) {
        }

        public void checkServerTrusted(java.security.cert.X509Certificate[] certs,
                String authType) {
        }
    } };

    public static final String METHOD_PUT = "PUT";
    public static final String METHOD_GET = "GET";
    public static final String METHOD_DELETE = "DELETE";

    final protected static char[] hexArray = "0123456789abcdef".toCharArray();

    private static final String[] PROPERTY_RIL_TEST_SIM = {
        "gsm.sim.ril.testsim",
        "gsm.sim.ril.testsim.2",
        "gsm.sim.ril.testsim.3",
        "gsm.sim.ril.testsim.4",
    };


    /**
     * Constructor.
     *
     */
    public XcapClient(Context context, int phoneId) {
        mContext = context;
        mPhoneId = phoneId;
        composeUserAgent();
        Log.i(TAG , "XcapClient context: " + context + " phoneId:" + phoneId);
        initialize();
    }

    /**
     * Constructor.
     *
     * @param userAgent XCAP client User Agent
     * @param phoneId   to indicate the command from which SIM
     */
    public XcapClient(Context context, String userAgent, int phoneId) {
        mContext = context;
        mPhoneId = phoneId;
        mUserAgent = userAgent;
        Log.i(TAG , "XcapClient context: " + context + " phoneId:" + phoneId);
        initialize();
    }

    /**
     * Constructor.
     *
     * @param network dedicated network
     * @param phoneId to indicate the command from which SIm
     */
    public XcapClient(Context context, Network network, int phoneId) {
        mContext = context;
        mPhoneId = phoneId;
        composeUserAgent();
        Log.i(TAG , "XcapClient context: " + context + " phoneId:" + phoneId);
        if (network != null) {
            mNetwork = network;
        }
        initialize();
    }

    /**
     * Constructor.
     *
     * @param userAgent XCAP client User Agent
     * @param network dedicated network
     * @param phoneId to indicate the command from which SIM
     */
    public XcapClient(Context context, String userAgent, Network network, int phoneId) {
        mContext = context;
        mPhoneId = phoneId;
        mUserAgent = userAgent;
        Log.i(TAG , "XcapClient context: " + context + " phoneId:" + phoneId);

        if (network != null) {
            mNetwork = network;
        }
        initialize();
    }

    private void composeUserAgent() {
        boolean isGbaEnabled = false;
        IBinder b = ServiceManager.getService("GbaService");
        if (b != null) {
            Log.i(TAG , "GbaService Enabled");
            isGbaEnabled = true;
        }

        if (mDebugParam.getXcapUserAgent() != null && !mDebugParam.getXcapUserAgent().isEmpty()) {
            mUserAgent = mDebugParam.getXcapUserAgent();
        } else {
            mUserAgent = "XCAP Client" + (isGbaEnabled ? " 3gpp-gba" : "");
        }
    }

    private void initialize() {
        mGbaManager = GbaManager.getDefaultGbaManager(mContext);
    }

    /**
     * Shutdown connection.
     */
    public void shutdown() {
        if (mConnection != null) {
            mConnection.disconnect();
        }
    }

    private void addExtraHeaders(HttpURLConnection connection, Headers rawHeaders) {
        if (rawHeaders == null) {
            return;
        }
        Set<String> names = rawHeaders.names();
        for (String name : names) {
            List<String> values = rawHeaders.values(name);
            for (String value : values) {
                if (!name.isEmpty() && !value.isEmpty()) {
                    // Add the header if the param is valid
                    connection.setRequestProperty(name, value);
                    break;
                }
            }
        }
    }

    private void logRequestHeaders(HttpURLConnection connection) {
        Map<String, List<String>> headerFields = connection.getRequestProperties();

        Log.d(TAG, "Request Headers:");

        for (Map.Entry<String, List<String>> entry : headerFields.entrySet()) {
            final String key = entry.getKey();
            final List<String> values = entry.getValue();
            if (values != null) {
                for (String value : values) {
                    Log.d(TAG, key + ": " + value);
                }
            }
        }
    }

    private void logResponseHeaders(HttpURLConnection connection) {
        Map<String, List<String>> headerFields = connection.getHeaderFields();

        Log.d(TAG, "Response Headers:");

        for (Map.Entry<String, List<String>> entry : headerFields.entrySet()) {
            final String key = entry.getKey();
            final List<String> values = entry.getValue();
            if (values != null) {
                for (String value : values) {
                    Log.d(TAG, key + ": " + value);
                }
            }
        }
    }

    private byte[] getNafSecureProtocolId(HttpURLConnection connection) {
        boolean isTlsEnabled = false;
        String protocol = connection.getURL().getProtocol();
        if (protocol.equals("https")) {
            isTlsEnabled = true;
        }
        Log.d(TAG, "getNafSecureProtocolId: protocol=" + protocol
                + ", isTlsEnabled=" + isTlsEnabled);
        return mGbaManager.getNafSecureProtocolId(isTlsEnabled);
    }

    private void handle401Exception(HttpURLConnection connection) throws IOException {
        WwwAuthHeader wwwAuthHeader = null;
        String auth_header = connection.getHeaderField(AUTH_HDR);

        mRequestCount = 1;

        if (auth_header != null) {
            wwwAuthHeader = WwwAuthHeader.parse(auth_header);
            Log.d(TAG, "handle401Exception: wwwAuthHeader=" + wwwAuthHeader);
        } else {
            Log.e(TAG, "handle401Exception: authentication header has something wrong");
            return;
        }
        byte[] uaId = getNafSecureProtocolId(connection);
        // need set network to GBA before run GBA
        if (mNetwork != null) {
            mGbaManager.setNetwork(mNetwork);
        }

        String nafFqdn = null;
        String realm = wwwAuthHeader.getRealm();
        if (realm.length() > 0) {
            String[] segments = realm.split(";");
            nafFqdn = segments[0].substring(segments[0].indexOf("@") + 1);
            Log.d(TAG, "handle401Exception: nafFqdn=" + nafFqdn + ", mPhoneId=" + mPhoneId);
            mNafFqdnCache.put(mPhoneId, nafFqdn);
        } else {
            Log.e(TAG, "handle401Exception: realm is empty string !!!");
            return;
        }
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(mPhoneId);
        NafSessionKey nafSessionKey = mGbaManager.runGbaAuthentication(nafFqdn, uaId,
                true, subId);
        if (nafSessionKey == null || nafSessionKey.getKey() == null) {
            Log.e(TAG, "handle401Exception: nafSessionKey Error!");
            if (nafSessionKey != null && (nafSessionKey.getException() != null) &&
                        (nafSessionKey.getException() instanceof IllegalStateException)) {
                String msg = ((IllegalStateException) nafSessionKey.getException())
                        .getMessage();

                if ("HTTP 403 Forbidden".equals(msg)) {
                    Log.i(TAG, "GBA hit 403");
                    throw new IOException("HTTP 403 Forbidden");
                } else if ("HTTP 400 Bad Request".equals(msg)) {
                    Log.i(TAG, "GBA hit 400");
                    throw new IOException("HTTP 400 Bad Request");
                }
            }
            return;
        } else {
            nafSessionKey.setAuthHeader(auth_header);
            mGbaManager.updateCachedKey(nafFqdn, uaId, subId, nafSessionKey);
            Log.d(TAG, "handle401Exception: nafSessionKey=" + nafSessionKey);
        }
    }

    /**
     * Utility function to convert byte array to hex string.
     *
     * @param bytes the byte array value.
     * @return the hex string value.
     *
     */
    private String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];

        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }

        return new String(hexChars);
    }

    private AkaDigestAuth getAkaDigestAuth(HttpURLConnection connection, String method,
            String content) {
        NafSessionKey nafSessionKey = null;
        WwwAuthHeader wwwAuthHeader = null;
        String nafFqdn = mNafFqdnCache.get(mPhoneId);
        byte[] uaId = getNafSecureProtocolId(connection);
        int subId = MtkSubscriptionManager.getSubIdUsingPhoneId(mPhoneId);

        //check NVIOT or PCT test
        if (("1".equals(SystemProperties.get(PROPERTY_RIL_TEST_SIM[mPhoneId], "0"))) &&
                    mRequestCount > 1) {
            Log.d(TAG, "getAkaDigestAuth: force to run gba");
            return null;
        }

        if (nafFqdn != null && !mGbaManager.isGbaKeyExpired(nafFqdn, uaId, subId)) {
            nafSessionKey = mGbaManager.getCachedKey(nafFqdn, uaId, subId);
            Log.d(TAG, "getAkaDigestAuth: nafFqdn=" + nafFqdn + ", mPhoneId=" + mPhoneId
                    + "nafSessionKey=" + nafSessionKey);
            if (nafSessionKey != null && nafSessionKey.getAuthHeader() != null) {
                wwwAuthHeader = WwwAuthHeader.parse(nafSessionKey.getAuthHeader());
                Log.d(TAG, "getAkaDigestAuth: wwwAuthHeader=" + wwwAuthHeader);
            }
        }

        if (nafSessionKey == null || wwwAuthHeader == null) {
            return null;
        }

        String password = bytesToHex(
                Base64.encode(nafSessionKey.getKey(), Base64.NO_WRAP));
        String nc = String.format("%08x", mRequestCount);
        Log.d(TAG, "getAkaDigestAuth: password=" + password + ", nc=" + nc
                + ", url=" + connection.getURL().getPath());
        return new AkaDigestAuth(wwwAuthHeader, nafSessionKey.getBtid(), null, password,
            connection.getURL().getPath(), nc, method, content);
    }

    private HttpURLConnection execute(URL url, String method, byte[] xml,
            Headers additionalRequestHeaders) throws IOException {
        int tryCount = 3;
        boolean success = false;
        //OkHttp usage
        mConnection = null;
        // Set property to hint okhttp to remove Accept-Encoding header
        System.setProperty("xcap.req", "true");
        boolean isTrustAll = mDebugParam.getEnableXcapTrustAll();

        if (isTrustAll) {
            // Install the all-trusting trust manager
            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, mTrustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (GeneralSecurityException se) {
                se.printStackTrace();
            }

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        }
        mRequestCount += 1;

        while (tryCount > 0 && !success) {
            try {
                if (!SENLOG || TELDBG) {
                    Log.d(TAG, method + " :" + url.toString());
                } else {
                    Log.d(TAG, method + " :" + "[hidden]");
                }

                if (mNetwork != null) {
                    mConnection = (HttpURLConnection) mNetwork.openConnection(url);
                } else {
                    mConnection = (HttpURLConnection) url.openConnection();
                }
                mConnection.setDoInput(true);
                mConnection.setConnectTimeout(SOCKET_OPERATION_TIMEOUT);
                mConnection.setReadTimeout(SOCKET_OPERATION_TIMEOUT);
                //mConnection.setWriteTimeout(SOCKET_OPERATION_TIMEOUT);
                // Header: User-Agent
                mConnection.setRequestProperty("User-Agent", mUserAgent);
                addExtraHeaders(mConnection, additionalRequestHeaders);
                // Add Authorization header
                String content;
                if (xml == null) {
                    content = "";
                } else {
                    content = new String(xml);
                }
                AkaDigestAuth akaDigestAuth = getAkaDigestAuth(mConnection, method, content);
                if (akaDigestAuth != null) {
                    akaDigestAuth.calculateRequestDigest();
                    mConnection.setRequestProperty("Authorization",
                        akaDigestAuth.createAuthorHeaderValue());
                }
                // Different stuff for GET and POST
                if (METHOD_PUT.equals(method)) {
                    mConnection.setDoOutput(true);
                    mConnection.setRequestMethod(METHOD_PUT);
                    if (Log.isLoggable(TAG, Log.DEBUG)) {
                        logRequestHeaders(mConnection);
                    }
                    // Sending request body
                    final OutputStream out =
                            new BufferedOutputStream(mConnection.getOutputStream());
                    out.write(xml);
                    out.flush();
                    out.close();
                } else if (METHOD_GET.equals(method)) {
                    mConnection.setRequestMethod(METHOD_GET);
                    if (Log.isLoggable(TAG, Log.DEBUG)) {
                        logRequestHeaders(mConnection);
                    }
                }

                // Get response
                final int responseCode = mConnection.getResponseCode();
                final String responseMessage = mConnection.getResponseMessage();
                Log.d(TAG, "HTTP: " + responseCode + " " + responseMessage);
                if (Log.isLoggable(TAG, Log.DEBUG)) {
                    logResponseHeaders(mConnection);
                }

                if (responseCode == 200 || responseCode == 403  || responseCode == 304 ||
                        responseCode == 412 || responseCode == 201) {
                    success = true;
                    break;
                } else if (responseCode == 409) {
                    success = true;
                    break;
                } else if (responseCode == 401) {
                    System.setProperty("gba.auth", "401");
                    Log.d(TAG, "HTTP status code is 401. Force to run GBA");
                    handle401Exception(mConnection);
                } else {
                    Log.d(TAG, "HTTP status code is not 200 or 403 or 409");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
                throw e;
            } catch (ProtocolException e) {
                e.printStackTrace();
                throw e;
            } catch (IOException e) {
                e.printStackTrace();

                if (e instanceof SocketTimeoutException) {
                    if (tryCount-1 > 0) {
                        Log.d(TAG, "SocketTimeoutException: wait for retry.");
                    } else {
                        throw e;
                    }
                } else if ("HTTP 403 Forbidden".equals(e.getMessage())) {
                    success = true;
                    throw new IOException("GBA hit HTTP 403 Forbidden");
                } else if ("HTTP 400 Bad Request".equals(e.getMessage())) {
                    success = true;
                    throw new IOException("GBA hit HTTP 400 Bad Request");
                } else {
                    throw e;
                }
            } finally {
                if (!success) {
                    try {
                        tryCount--;
                        if (tryCount > 0) {
                            Thread.sleep(5 * 1000);
                            Log.d(TAG, "retry once");
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return mConnection;
        //OkHttp usage end
    }

    /**
     * HTTP GET.
     *
     * @param  uri document URI
     * @param  additionalRequestHeaders HTTP headers
     * @return HTTP response
     * @throws IOException if I/O error
     */
    public HttpURLConnection get(URI uri, Headers additionalRequestHeaders) throws IOException {
        return execute(uri.toURL(), METHOD_GET, null, additionalRequestHeaders);
    }

    /**
     * HTTP PUT.
     *
     * @param  uri document URI
     * @param  mimetype MIME TYPE
     * @param  content content to upload
     * @return HTTP response
     * @throws IOException if I/O error
     */
    public HttpURLConnection put(URI uri, String mimetype, String content) throws IOException {
        Log.d(TAG, "PUT: " + content);
        return put(uri, mimetype, content.getBytes("UTF-8"), null, null, null);

    }

    /**
     * HTTP PUT.
     *
     * @param  uri document URI
     * @param  mimetype MIME TYPE
     * @param  content content to upload
     * @param  additionalRequestHeaders HTTP headers
     * @return HTTP response
     * @throws IOException if I/O error
     */
    public HttpURLConnection put(URI uri, String mimetype, String content,
            Headers additionalRequestHeaders) throws IOException {
        Log.d(TAG, "PUT: " + content);
        return put(uri, mimetype, content.getBytes("UTF-8"), additionalRequestHeaders, null, null);

    }

    /**
     * HTTP PUT.
     *
     * @param  uri document URI
     * @param  mimetype MIME TYPE
     * @param  content content to upload in string format
     * @param  additionalRequestHeaders HTTP headers
     * @param  eTag E-TAG
     * @param  condition use with E-TAG
     * @return HTTP response
     * @throws IOException if I/O error
     */
    public HttpURLConnection put(URI uri, String mimetype, String content,
            Headers additionalRequestHeaders, String eTag, String condition) throws IOException {
        Log.d(TAG, "PUT: " + content);
        return put(uri, mimetype, content.getBytes("UTF-8"), additionalRequestHeaders, eTag,
                condition);

    }

    /**
     * HTTP PUT.
     *
     * @param  uri document URI
     * @param  mimetype MIME TYPE
     * @param  content content to upload in byte array format
     * @param  additionalRequestHeaders HTTP headers
     * @param  eTag E-TAG
     * @param  condition use with E-TAG
     * @return HTTP response
     * @throws IOException if I/O error
     */
    public HttpURLConnection put(URI uri, String mimetype, byte[] content,
            Headers additionalRequestHeaders, String eTag, String condition) throws IOException {
        Headers headers = null;
        if (additionalRequestHeaders != null) {
            Headers.Builder headersBuilder = additionalRequestHeaders.newBuilder();
            headersBuilder.add(XcapConstants.HDR_KEY_CONTENT_TYPE, mimetype);
            headers = headersBuilder.build();
        }
        return execute(uri.toURL(), METHOD_PUT, content, headers);
    }

    /**
     * HTTP DELETE.
     *
     * @param  uri document URI
     * @return HTTP response
     * @throws IOException if I/O error
     */
    public HttpURLConnection delete(URI uri) throws IOException {
        return delete(uri, null, null, null);
    }

    /**
     * HTTP DELETE.
     *
     * @param  uri document URI
     * @param  additionalRequestHeaders HTTP headers
     * @return HTTP response
     * @throws IOException if I/O error
     */
    public HttpURLConnection delete(URI uri, Headers additionalRequestHeaders) throws IOException {
        return delete(uri, additionalRequestHeaders, null, null);
    }

    /**
     * HTTP DELETE.
     *
     * @param  uri document URI
     * @param  additionalRequestHeaders HTTP headers
     * @param  eTag E-TAG
     * @param  condition use with E-TAG
     * @return HTTP response
     * @throws IOException if I/O error
     */
    public HttpURLConnection delete(URI uri, Headers additionalRequestHeaders, String eTag,
            String condition) throws IOException {
        return execute(uri.toURL(), METHOD_DELETE, null, additionalRequestHeaders);
    }
}
