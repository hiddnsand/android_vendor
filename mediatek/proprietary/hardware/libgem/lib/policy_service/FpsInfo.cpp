#include <inttypes.h>

#include <cutils/atomic.h>
#include <cutils/log.h>
#include <cutils/properties.h>

#include "dfps/FpsInfo.h"

#if !defined(__STDC_FORMAT_MACROS)
#define __STDC_FORMAT_MACROS 1
#endif

#define UNUSED(param) do { (void)(param); } while(0)

namespace android {

inline uint64_t getUniqueInfoId()
{
    static volatile int32_t nextId = 0;
    uint64_t id = static_cast<uint64_t>(getpid()) << 32;
    id |= static_cast<uint32_t>(android_atomic_inc(&nextId));
    return id;
}

HwcInfo::HwcInfo()
    : singleLayer(0)
    , numDisplay(0)
{
}

FpsInfo::FpsInfo()
    : mState(NO_ERROR)
    , mCb(NULL)
    , mSingleLayer(0)
    , mNumDisplay(0)
    , mForceWindowPid(0)
    , mEnableTracker(false)
    , mForbidVSync(false)
{
    mId = getUniqueInfoId();
    memset(&mHwcInfo, 0, sizeof(mHwcInfo));
    UNUSED(mState);
    UNUSED(mSingleLayer);
    UNUSED(mNumDisplay);
    UNUSED(mForceWindowPid);
    UNUSED(mEnableTracker);
    UNUSED(mForbidVSync);
}

FpsInfo::~FpsInfo()
{
}

status_t FpsInfo::connectServiceLocked()
{
    return NO_ERROR;
}

void FpsInfo::updateState(int32_t flag)
{
    UNUSED(flag);
}

status_t FpsInfo::setSingleLayer(const int32_t single)
{
    status_t res = NO_ERROR;
    UNUSED(single);

    return res;
}

status_t FpsInfo::setNumberDisplay(const int32_t number)
{
    status_t res = NO_ERROR;
    UNUSED(number);

    return res;
}

status_t FpsInfo::setHwcInfo(const HwcInfo& info)
{
    status_t res = NO_ERROR;
    UNUSED(info);

    return res;
}

status_t FpsInfo::setInputWindows(const String8& name, const SimpleInputWindowInfo& info)
{
    status_t res = NO_ERROR;
    UNUSED(name);
    UNUSED(info);

    return res;
}

status_t FpsInfo::regFpsInfoListener(sp<FpsInfoListener> &cb)
{
    status_t res = NO_ERROR;
    UNUSED(cb);

    return res;
}

status_t FpsInfo::cbGetFps(int32_t fps)
{
    UNUSED(fps);
    return NO_ERROR;
}

status_t FpsInfo::setForegroundInfo(int32_t pid, String8& packageName)
{
    status_t res = NO_ERROR;
    UNUSED(pid);
    UNUSED(packageName);

    return res;
}

status_t FpsInfo::setWindowFlag(const int32_t flag, const int32_t mask)
{
    status_t res = NO_ERROR;
    UNUSED(flag);
    UNUSED(mask);

    return res;
}

void FpsInfo::enableTracker(const bool enable)
{
    UNUSED(enable);
}

status_t FpsInfo::forbidAdjustingVsync(bool forbid)
{
    status_t res = NO_ERROR;
    UNUSED(forbid);

    return res;
}

};
