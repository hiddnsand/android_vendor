package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;
import android.util.Log;

import com.mediatek.sensorhub.settings.Utils;

public class OrientationSensorActivity extends CustomerSensorBaseActivity {

    public OrientationSensorActivity() {
        super(Utils.KEY_ORIENTATION_STATUS, Sensor.TYPE_ORIENTATION);
    }

    @Override
    public void onAccuracyChanged(int accuracy) {
        Log.d("SensorEventListenerService", "accuracy : " + accuracy);
        mSensorSwitch.setSummary(mSensorSwitch.getSummary() + getString(R.string.accuracy_string)
                + accuracy);
    }
}
