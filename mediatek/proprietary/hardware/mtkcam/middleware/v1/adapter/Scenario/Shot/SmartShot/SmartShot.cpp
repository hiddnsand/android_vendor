/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/SmartShot"
//
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/def/common.h>
#include <mtkcam/utils/std/Trace.h>
//
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/aaa/IIspMgr.h>
//
#include <mtkcam/middleware/v1/IShot.h>
//
#include "ImpShot.h"
#include "SmartShot.h"
//
#include <mtkcam/utils/hw/CamManager.h>
using namespace NSCam::Utils;
//
#include <mtkcam/middleware/v1/LegacyPipeline/StreamId.h>
#include <mtkcam/middleware/v1/camshot/_params.h>
//
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
//
#include <mtkcam/middleware/v1/LegacyPipeline/ILegacyPipeline.h>
#include <mtkcam/utils/hw/HwInfoHelper.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <utils/Vector.h>
#include <sys/stat.h>
//
#include <mtkcam/aaa/IDngInfo.h>
//
#include <mtkcam/middleware/v1/LegacyPipeline/LegacyPipelineUtils.h>
#include <mtkcam/middleware/v1/LegacyPipeline/LegacyPipelineBuilder.h>
#include <mtkcam/middleware/v1/LegacyPipeline/buffer/StreamBufferProviderFactory.h>

#include <mtkcam/aaa/IHal3A.h>
#include <mtkcam/pipeline/extension/MFNR.h>

#include <errno.h>

#include <limits>

using namespace android;
using namespace NSShot::NSSmartShot;
using namespace NSCam;
using namespace NSCam::v1;
using namespace NSCam::v1::NSLegacyPipeline;
using namespace NSCamHW;
using namespace NS3Av3;
using namespace NSCam::plugin;
using namespace NSCam::Utils;

#define CHECK_OBJECT(x)  do{                                        \
    if (x == nullptr) { MY_LOGE("Null %s Object", #x); return MFALSE;} \
} while(0)

#include <cutils/properties.h>
#define DUMP_KEY  "debug.smartshot.dump"
#define DUMP_PATH "/sdcard/smartshot"
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("(%d)(%d)(%s)[%s] " fmt, ::gettid(), getOpenId(), getShotName(), __FUNCTION__, ##arg)

#define MYU_LOGE(fmt, arg...)       CAM_LOGE("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)

//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)
#define FUNC_START                  MY_LOGD("+")
#define FUNC_END                    MY_LOGD("-")

#define DEFAULT_MAX_FRAME_NUM       6

/******************************************************************************
 *
 ******************************************************************************/
extern
sp<IShot>
createInstance_SmartShot(
    char const*const    pszShotName,
    uint32_t const      u4ShotMode,
    int32_t const       i4OpenId
)
{
    sp<IShot>      pShot        = NULL;
    sp<SmartShot>  pImpShot     = NULL;
    //
    //  (1.1) new Implementator.
    pImpShot = new SmartShot(pszShotName, u4ShotMode, i4OpenId, false);
    if  ( pImpShot == 0 ) {
        CAM_LOGE("[%s] new SmartShot", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (1.2) initialize Implementator if needed.
    if  ( ! pImpShot->onCreate() ) {
        CAM_LOGE("[%s] onCreate()", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (2)   new Interface.
    pShot = new IShot(pImpShot);
    if  ( pShot == 0 ) {
        CAM_LOGE("[%s] new IShot", __FUNCTION__);
        goto lbExit;
    }
    //
lbExit:
    //
    //  Free all resources if this function fails.
    if  ( pShot == 0 && pImpShot != 0 ) {
        pImpShot->onDestroy();
        pImpShot = NULL;
    }
    //
    return  pShot;
}

/******************************************************************************
 *
 ******************************************************************************/
extern
sp<IShot>
createInstance_ZsdSmartShot(
    char const*const    pszShotName,
    uint32_t const      u4ShotMode,
    int32_t const       i4OpenId
)
{
    sp<IShot>  pShot            = NULL;
    sp<SmartShot>  pImpShot = NULL;
    //
    //  (1.1) new Implementator.
    pImpShot = new SmartShot(pszShotName, u4ShotMode, i4OpenId, true);
    if  ( pImpShot == 0 ) {
        CAM_LOGE("[%s] new SmartShot", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (1.2) initialize Implementator if needed.
    if  ( ! pImpShot->onCreate() ) {
        CAM_LOGE("[%s] onCreate()", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (2)   new Interface.
    pShot = new IShot(pImpShot);
    if  ( pShot == 0 ) {
        CAM_LOGE("[%s] new IShot", __FUNCTION__);
        goto lbExit;
    }
    //
lbExit:
    //
    //  Free all resources if this function fails.
    if  ( pShot == 0 && pImpShot != 0 ) {
        pImpShot->onDestroy();
        pImpShot = NULL;
    }
    //
    return  pShot;
}
/******************************************************************************
 *
 ******************************************************************************/
template <typename T>
inline MBOOL
tryGetMetadata(
    IMetadata const* const pMetadata,
    MUINT32 const tag,
    T & rVal
)
{
    if( pMetadata == NULL ) {
        //MY_LOGE("pMetadata == NULL");
        return MFALSE;
    }

    IMetadata::IEntry entry = pMetadata->entryFor(tag);
    if( !entry.isEmpty() ) {
        rVal = entry.itemAt(0, Type2Type<T>());
        return MTRUE;
    }
    return MFALSE;
}


/******************************************************************************
 *  This function is invoked when this object is firstly created.
 *  All resources can be allocated here.
 ******************************************************************************/
bool
SmartShot::
onCreate()
{
    bool ret = true;
    return ret;
}


/******************************************************************************
 *  This function is invoked when this object is ready to destryoed in the
 *  destructor. All resources must be released before this returns.
 ******************************************************************************/
void
SmartShot::
onDestroy()
{

}

/******************************************************************************
 *
 ******************************************************************************/
SmartShot::
SmartShot(
    char const*const pszShotName,
    uint32_t const   u4ShotMode,
    int32_t const    i4OpenId,
    bool const       isZsd
)
    : ImpShot(pszShotName, u4ShotMode, i4OpenId)
    , mSensorFps(0)
    , mPixelMode(0)
    , mu4Scenario(SENSOR_SCENARIO_ID_UNNAMED_START)
    , mu4Bitdepth(0)
    , mbZsdFlow(isZsd)
    //
    , mpManager(NULL)
    , mpPipeline(NULL)
    , mpImageCallback(NULL)
    , mpCallbackHandler(NULL)
    , mpMetadataListener(NULL)
    , mpMetadataListenerFull(NULL)
    //
    , mpJpegPool()
    , mCapReqNo(0)
    , mLastRotation(0)
    , mLastPicFmt(0)
    , mNeedReconstruct(MFALSE)
{
    MY_LOGD("mbZsdFlow %d %d", mbZsdFlow, isZsd);
    mDumpFlag = ::property_get_int32(DUMP_KEY, 0);
    if( mDumpFlag ) {
        MY_LOGD("enable dump flag 0x%x", mDumpFlag);
        MINT32 err = mkdir(DUMP_PATH, S_IRWXU | S_IRWXG | S_IRWXO);
        if(err != 0)
        {
            MY_LOGE("mkdir failed");
        }
    }
    // set default maximum frame number
    setMaxCaptureFrameNum(DEFAULT_MAX_FRAME_NUM);

    ::memset(&mSemCaptureDone, 0x00, sizeof( sem_t ));
    // init semaphore
    sem_init(&mSemCaptureDone, 0, 0);
}


/******************************************************************************
 *
 ******************************************************************************/
SmartShot::
~SmartShot()
{
    MY_LOGD("~SmartShot() +");

    auto pPipeline = mpPipeline;

    if( pPipeline.get() )
    {
        pPipeline->waitUntilDrained();
        pPipeline->flush();
        mpPipeline = nullptr;
    }

    if( mResultMetadataSetMap.size() > 0 )
    {
        int n = mResultMetadataSetMap.size();
        for(int i=0; i<n; i++)
        {
            MY_LOGW("requestNo(%d) doesn't clear before SmartShot destroyed",mResultMetadataSetMap.keyAt(i));
            mResultMetadataSetMap.editValueAt(i).selectorGetBufs.clear();
        }
    }
    mResultMetadataSetMap.clear();

    // destroy semaphore
    sem_destroy(&mSemCaptureDone);
    MY_LOGD("~SmartShot() -");
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SmartShot::
sendCommand(
    uint32_t const  cmd,
    MUINTPTR const  arg1,
    uint32_t const  arg2,
    uint32_t const  arg3
)
{
    bool ret = true;
    //
    switch  (cmd)
    {
    //  This command is to reset this class. After captures and then reset,
    //  performing a new capture should work well, no matter whether previous
    //  captures failed or not.
    //
    //  Arguments:
    //          N/A
    case eCmd_reset:
        ret = onCmd_reset();
        break;

    //  This command is to perform capture.
    //
    //  Arguments:
    //          N/A
    case eCmd_capture:
        ret = onCmd_capture();
        // update last info
        updateLastInfo();
        break;

    //  This command is to perform cancel capture.
    //
    //  Arguments:
    //          N/A
    case eCmd_cancel:
        onCmd_cancel();
        break;

    case eCmd_getIsNeedNewOne:
        {
            MBOOL* pResult = reinterpret_cast<MBOOL*>(arg1);
            *pResult = isNeedToReconstruct();
        }
        break;
    //
    default:
        ret = ImpShot::sendCommand(cmd, arg1, arg2, arg3);
    }
    //
    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
SmartShot::
onMsgReceived(
    MINT32 cmd, /*NOTIFY_MSG*/
    MINT32 /*arg1*/, MINT32 /*arg2*/,
    void*  /*arg3*/
)
{
    switch  (cmd)
    {
        case plugin::MSG_ON_NEXT_CAPTURE_READY:
            if(mbZsdFlow) mpShotCallback->onCB_P2done();
        break;
        case plugin::MSG_ON_SHUTTER_CALLBACK:
            mpShotCallback->onCB_Shutter(true,0);
        break;
    }

    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
bool
SmartShot::
onCmd_reset()
{
    bool ret = true;
    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
SmartShot::
createNewPipeline()
{
    MBOOL ret = MFALSE;
    auto pPipeline = mpPipeline;
    //
    if ( !pPipeline.get() ) ret = MTRUE;
    if ( !mpManager.get() ) ret = MTRUE;
    //
    /*if( mLastPipelineMode != getLegacyPipelineMode() )
    {
        MY_LOGD("LegacyPipelineMode changed (%d)->(%d), need to create pipeline",mLastPipelineMode,getLegacyPipelineMode());
        return MTRUE;
    }
    //
#warning "Need to use run-time change Rotation method to avoid re-create pipeline"
    if( mLastRotation != getRotation() )
    {
        MY_LOGD("Rotation changed (%d)->(%d), need to create pipeline",mLastRotation,getRotation());
        return MTRUE;
    }
    //
#warning "Need to use run-time change JpegSize method to avoid re-create pipeline"
    if( mLastJpegsize != mJpegsize )
    {
        MY_LOGD("JpegSize changed (%dx%d)->(%dx%d), need to create pipeline",
            mLastJpegsize.w,mLastJpegsize.h,
            mJpegsize.w,mJpegsize.h);
        return MTRUE;
    }*/

    MY_LOGD("createNewPipeline %d", ret);

    return ret;
}

MBOOL
SmartShot::
isNeedToReconstruct()
{
    sp<IParamsManager> pParamsMgr = mShotParam.mpParamsMgr.promote();
    if (CC_UNLIKELY( pParamsMgr.get() == nullptr )) {
        MY_LOGE("cannot get params manager, assume need to reconstruct");
        return MTRUE;
    }

    // query picture rotation
    MUINT32 transform = pParamsMgr->getInt(CameraParameters::KEY_ROTATION);
    switch (transform) {
    case 0:     transform = 0;                   break;
    case 90:    transform = eTransform_ROT_90;   break;
    case 180:   transform = eTransform_ROT_180;  break;
    case 270:   transform = eTransform_ROT_270;  break;
    default:
        break;
    }
    // query picture size
    int iPictureWidth = 0, iPictureHeight = 0;
    pParamsMgr->getPictureSize(&iPictureWidth, &iPictureHeight);
    MY_LOGD("lastTransform=%u, transform=%u",mLastRotation, transform);
    MY_LOGD("lastSize=%dx%d, size=%dx%d",
            mLastJpegsize.w, mLastJpegsize.h,
            iPictureWidth, iPictureHeight);

    MSize newSize = (transform == 0 || transform == eTransform_ROT_180)
        ? MSize(iPictureWidth, iPictureHeight)
        : MSize(iPictureHeight, iPictureWidth);
    // query picture format
    int newPicFmt = eImgFmt_JPEG;
    newPicFmt = MtkCameraParameters::queryImageFormat(pParamsMgr->getStr(CameraParameters::KEY_PICTURE_FORMAT));
    MY_LOGD("lastPicFmt=0x%X, newPicFmt=0x%X",mLastPicFmt, newPicFmt);

    if ( mLastRotation != transform )             return MTRUE;
    if ( mLastJpegsize.size() != newSize.size() ) return MTRUE;
    if ( mLastPicFmt   != newPicFmt )             return MTRUE;
    if ( mNeedReconstruct )                            return MTRUE;

    return MFALSE;
}

/******************************************************************************
 *
 ******************************************************************************/
bool
SmartShot::
onCmd_capture()
{
    CAM_TRACE_NAME("SmartShot onCmd_capture");

    mu4Scenario = mShotParam.muSensorMode;

    HwInfoHelper helper(getOpenId());
    if (mu4Scenario == SENSOR_SCENARIO_ID_UNNAMED_START) {
        if (helper.get4CellSensorSupported()) {
            mu4Scenario = SENSOR_SCENARIO_ID_NORMAL_PREVIEW;
        }
        else {
            mu4Scenario = SENSOR_SCENARIO_ID_NORMAL_CAPTURE;
        }
    }
    MY_LOGD("scenario(%d), sensorMode(%d)", mu4Scenario, mShotParam.muSensorMode);

    /* monitor this scope, if exited, mark as capture done */
    ScopeWorker ___scope_worker([this](void* /*arg*/) {
        mStateMgr.updateState(SHOTSTATE_DONE);
        sem_post(&mSemCaptureDone);
        MY_LOGD("ShotState: SHOTSTATE_DONE");
    });

    /* check cpature state */
    bool bCancel = mStateMgr.doWork<bool>(
            [this](ShotState& state, void* /*arg*/) -> bool {
                if (state == SHOTSTATE_CANCEL) {
                    MY_LOGD("ShotState: SHOTSTATE_CANCEL");
                    return true;
                }
                else {
                    state = SHOTSTATE_CAPTURE;
                    MY_LOGD("ShotState: SHOTSTATE_CAPTURE");
                    return false;
                }
            });

    if (bCancel) {
        MY_LOGD("canceled capture");
        return true;
    }

    // pause preview
    std::shared_ptr<void> _preview_controller = pausePreview();

    if ( createNewPipeline()) {
        MINT32 runtimeAllocateCount = 0;
        beginCapture( runtimeAllocateCount );
        //
        mpManager = NSVendorManager::get(getOpenId());
        CHECK_OBJECT(mpManager);
        //
        if (!constructCapturePipeline()) {
            MY_LOGW("construct capture pipeline fail");
            return false;
        }
    }

    //
    plugin::InputInfo inputInfo;
    MINT64 mode = [ this ](MINT32 const type) -> MINT64 {
        switch(type)
        {
            case eShotMode_EngShot:
            case eShotMode_MfllShot:
            case eShotMode_ZsdMfllShot: {
                return MTK_PLUGIN_MODE_MFNR;
            }
            case eShotMode_ZsdHdrShot:
            case eShotMode_HdrShot: {
                return MTK_PLUGIN_MODE_HDR;
            }
        }
        MY_LOGW("no mapped vendor for type %d", type);
        return MTK_PLUGIN_MODE_COMBINATION;
    }(getShotMode());

    // set Input
    if(!makeVendorCombination(inputInfo, mode)){
        MY_LOGE("make vendor combination failed!");
        return false;
    }

    // update post nr settings
    for (size_t i = 0; i < inputInfo.combination.size(); ++i) {
        MY_LOGD("in.combination[%zu]:0x%" PRIx64 ", MTK_PLUGIN_MODE_NR(0x%X)",
                 i, inputInfo.combination[i], MTK_PLUGIN_MODE_NR);
        if (MTK_PLUGIN_MODE_NR == inputInfo.combination[i]) {
            updatePostNRSetting(MTK_NR_MODE_AUTO, (mode == MTK_PLUGIN_MODE_MFNR), &mShotParam.mHalSetting);
        }
    }

    inputInfo.appCtrl      = mShotParam.mAppSetting;
    inputInfo.halCtrl      = mShotParam.mHalSetting;
    //
    inputInfo.fullRaw      = mpInfo_FullRaw;
    inputInfo.resizedRaw   = mpInfo_ResizedRaw;
    inputInfo.lcsoRaw      = mpInfo_LcsoRaw;
    inputInfo.jpegYuv      = mpInfo_Yuv;
    inputInfo.thumbnailYuv = mpInfo_YuvThumbnail;
    inputInfo.postview     = mpInfo_YuvPostview;
    inputInfo.jpeg         = mpInfo_Jpeg;
    //
    inputInfo.isZsdMode    = mbZsdFlow;
    inputInfo.sensorMode   = mu4Scenario;

    MINT32 shotCount;
    plugin::OutputInfo outputInfo;
    plugin::InputSetting inputSetting;
    plugin::VendorInfo vInfo;
    {
        Mutex::Autolock _l(MFNRVendor::sCtrlerAccessLock);
        mpManager->get(plugin::CALLER_SHOT_SMART, inputInfo, outputInfo);
        // User defined
        vInfo.vendorMode = mode;
        if (mode == MTK_PLUGIN_MODE_MFNR)
        {
            if (inputInfo.postview.get()) {
                vInfo.appOut.push_back( inputInfo.postview->getStreamId() );
            }
        }
        //
        inputSetting.pMsgCb = this;
        for ( int i = 0; i < outputInfo.frameCount; ++i ) {
            plugin::FrameInfo info;
            info.frameNo = mCapReqNo + i;
            info.curAppControl = mShotParam.mAppSetting;
            info.curHalControl = mShotParam.mHalSetting;
            info.curAppControl += outputInfo.settings[i].setting.appMeta;
            info.curHalControl += outputInfo.settings[i].setting.halMeta;
            info.vVendorInfo.add(mode, vInfo);
            inputSetting.vFrame.push_back(info);
        }
        // update p2 control
        updateSetting(outputInfo, inputSetting);
        //
        mpManager->set(plugin::CALLER_SHOT_SMART, inputSetting);
    }
    //
    shotCount = inputSetting.vFrame.size();
    MY_LOGD("shotCount:%d", shotCount);
    if ( mbZsdFlow ) {
        // submit to zsd preview pipeline
        Vector<SettingSet> vSettings;
        for ( MINT32 i = 0; i < shotCount; ++i ) {
            SettingSet s;
            s.appSetting = inputSetting.vFrame[i].curAppControl;
            s.halSetting = inputSetting.vFrame[i].curHalControl;
            vSettings.push_back(s);
        }
        applyRawBufferSettings(vSettings, inputSetting.vFrame.size());
    }

    // resume preview after all request has been sent
    _preview_controller = nullptr;

    // get multiple raw buffer and send to capture pipeline
    for ( MINT32 i = 0; i < shotCount; ++i ) {
        IMetadata halSetting = mShotParam.mHalSetting;
        if ( mbZsdFlow ) {
            // get Selector Data (buffer & metadata)
            status_t status = OK;
            android::sp<IImageBuffer> pBuf = NULL; // full raw buffer
            IMetadata selectorAppMetadata; // app setting for this raw buffer. Ex.3A infomation
            status = getSelectorData(
                        selectorAppMetadata,
                        halSetting,
                        pBuf
                    );
            if( status != OK ) {
                MY_LOGE("GetSelectorData Fail!");
                mNeedReconstruct = MTRUE;
                goto lbExit;
            }
            //
            {
                Mutex::Autolock _l(mResultMetadataSetLock);
                mResultMetadataSetMap.editValueFor(mCapReqNo).selectorAppMetadata = selectorAppMetadata;
            }
        } else {
            halSetting += inputSetting.vFrame[i].curHalControl;
            IMetadata::IEntry entry(MTK_HAL_REQUEST_SENSOR_SIZE);
            entry.push_back(mSensorSize, Type2Type< MSize >());
            halSetting.update(entry.tag(), entry);
            {
                Mutex::Autolock _l(mResultMetadataSetLock);
                IMetadata resultAppMetadata;
                IMetadata resultHalMetadata;
                IMetadata selectorAppMetadata;
                Vector<ISelector::BufferItemSet> bufferSet;
                mResultMetadataSetMap.add(
                        static_cast<MUINT32>(mCapReqNo),
                        ResultSet_T{static_cast<MUINT32>(mCapReqNo), resultAppMetadata, resultHalMetadata, selectorAppMetadata, bufferSet, NONE_CB_DONE}
                        );
            }
        }
        // submit setting to capture pipeline
        if (OK != submitCaptureSetting(
                    i == 0,
                    inputSetting.vFrame[i].curAppControl,
                    halSetting) ) {
            MY_LOGE("Submit capture setting fail.");
            mNeedReconstruct = MTRUE;
            goto lbExit;
        }
        mCapReqNo++;
    }

lbExit:
    // 1. wait pipeline done
    // 2. set selector back to default zsd selector
    // 3. set full raw buffer count back to normal
    endCapture();
    //
    return MTRUE;
}


/******************************************************************************
 *
 ******************************************************************************/
void
SmartShot::
onCmd_cancel()
{
    FUNC_START;
    auto pPipeline = mpPipeline;

    /* checking current state and update state to cancel */
    ShotState currentState = mStateMgr.doWork<ShotState>(
            [this](ShotState& state, void* /*arg*/) {
                ShotState currState = state;
                state = SHOTSTATE_CANCEL;
                MY_LOGD("ShotState: SHOTSTATE_CANCEL");
                /* wait capture done semaphore signaled */
                return currState;
            });

    /* wait catpure done */
    if (currentState == SHOTSTATE_CAPTURE) {
        MY_LOGW("%s: wait capture semaphore signal", __FUNCTION__);
        sem_wait(&mSemCaptureDone);
        MY_LOGW("%s: capture semaphore ready", __FUNCTION__);
    }

    if( pPipeline.get() )
    {
        pPipeline->waitUntilDrained();
        pPipeline->flush();
        mpPipeline = nullptr;
    }

    /* Reconstuct shot instance after capture cancel */
    mNeedReconstruct = MTRUE;

    FUNC_END;
}

/******************************************************************************
*
*******************************************************************************/
MBOOL
SmartShot::
handlePostViewData(IImageBuffer *pImgBuffer)
{
    CAM_TRACE_CALL();

    MY_LOGD("+ (pImgBuffer) = (%p)", pImgBuffer);
    class scopedVar
    {
    public:
                    scopedVar(IImageBuffer* pBuf)
                    : mBuffer(pBuf) {
                        if( mBuffer )
                            mBuffer->lockBuf(LOG_TAG, eBUFFER_USAGE_SW_READ_MASK);
                    }
                    ~scopedVar() {
                        if( mBuffer )
                            mBuffer->unlockBuf(LOG_TAG);
                    }
    private:
        IImageBuffer* mBuffer;
    } _local(pImgBuffer);

    mpShotCallback->onCB_PostviewClient(0,
                                        pImgBuffer
                                        );

    MY_LOGD("-");

    return  MTRUE;
}

/******************************************************************************
*
*******************************************************************************/
MBOOL
SmartShot::
handleRaw16CB(IImageBuffer const *pImgBuffer)
{
    CAM_TRACE_CALL();
    MY_LOGD("+ (pImgBuffer) = (%p)", pImgBuffer);
    mpShotCallback->onCB_Raw16Image(reinterpret_cast<IImageBuffer const*>(pImgBuffer));
    return  MTRUE;
}

/******************************************************************************
*
*******************************************************************************/
MBOOL
SmartShot::
handleDNGMetaCB(MUINT32 const requestNo)
{
    MY_LOGD("handleDNGMetaCB(%d)",requestNo);
    CAM_TRACE_CALL();
    //
    int idx = -1;
    {
        Mutex::Autolock _l(mResultMetadataSetLock);
        idx = mResultMetadataSetMap.indexOfKey(requestNo);
    }
    //
    if(idx < 0 )
    {
        MY_LOGE("mResultMetadataSetMap can't find requestNo(%d)",requestNo);
        return MFALSE;
    }
    else
    {
        IMetadata rDngMeta;
        IMetadata rHalMeta;
        IMetadata rAppMeta;
        {
            Mutex::Autolock _l(mResultMetadataSetLock);
            rHalMeta = mResultMetadataSetMap.editValueFor(requestNo).halResultMetadata;
            rAppMeta = mResultMetadataSetMap.editValueFor(requestNo).appResultMetadata;
            rDngMeta = MAKE_DngInfo(LOG_TAG, getOpenId())->getShadingMapFromHal(rHalMeta, rAppMeta);
        }
        //
        MY_LOGD("rDngMeta(%d) size(%d)",requestNo,rDngMeta.count());
        MY_LOGD("before append: mResultMetadataSetMap(%d) size(%d)",requestNo,rAppMeta.count());
        rAppMeta+=rDngMeta;
        MY_LOGD("after append: mResultMetadataSetMap(%d) size(%d)",requestNo,rAppMeta.count());
        //
        if( rAppMeta.count() > 0 )
        {
            MY_LOGD("handleDNGMetaCB onCB_DNGMetaData(%d) size(%d)",requestNo,rAppMeta.count());
            mpShotCallback->onCB_DNGMetaData((MUINTPTR)&rAppMeta);
        }
    }
    return  MTRUE;
}

/******************************************************************************
*
*******************************************************************************/
MBOOL
SmartShot::
handleJpegData(MUINT32 const requestNo __attribute__((unused)), IImageBuffer* pJpeg)
{
    CAM_TRACE_CALL();
    //
    class scopedVar
    {
    public:
                    scopedVar(IImageBuffer* pBuf)
                    : mBuffer(pBuf) {
                        if( mBuffer )
                            mBuffer->lockBuf(LOG_TAG, eBUFFER_USAGE_SW_READ_MASK);
                    }
                    ~scopedVar() {
                        if( mBuffer )
                            mBuffer->unlockBuf(LOG_TAG);
                    }
    private:
        IImageBuffer* mBuffer;
    } _local(pJpeg);
    //
    uint8_t const* puJpegBuf = (uint8_t const*)pJpeg->getBufVA(0);
    MUINT32 u4JpegSize = pJpeg->getBitstreamSize();

    MY_LOGD("+ (puJpgBuf, jpgSize) = (%p, %d)",
            puJpegBuf, u4JpegSize);

    // dummy raw callback
    mpShotCallback->onCB_RawImage(0, 0, NULL);

    // Jpeg callback
    mpShotCallback->onCB_CompressedImage_packed(0,
                                         u4JpegSize,
                                         puJpegBuf,
                                         0,                       //callback index
                                         true                     //final image
                                         );
    MY_LOGD("-");

    return MTRUE;
}

/******************************************************************************
*
*******************************************************************************/
MERROR
SmartShot::
updateSetting(
    plugin::OutputInfo& param,
    /*out*/
    plugin::InputSetting& setting
)
{

    {
        MINT64 p2_setting = MTK_P2_ISP_PROCESSOR|MTK_P2_MDP_PROCESSOR|MTK_P2_CAPTURE_REQUEST;
        if (param.inCategory == plugin::FORMAT_RAW && param.outCategory == plugin::FORMAT_YUV)
            p2_setting = MTK_P2_RAW_PROCESSOR|MTK_P2_CAPTURE_REQUEST;
        else if (param.inCategory == plugin::FORMAT_YUV && param.outCategory == plugin::FORMAT_YUV)
            p2_setting = MTK_P2_ISP_PROCESSOR|MTK_P2_YUV_PROCESSOR|MTK_P2_MDP_PROCESSOR|MTK_P2_CAPTURE_REQUEST;
        else if (param.inCategory == plugin::FORMAT_RAW && param.outCategory == plugin::FORMAT_RAW)
            p2_setting = MTK_P2_RAW_PROCESSOR|MTK_P2_ISP_PROCESSOR|MTK_P2_MDP_PROCESSOR|MTK_P2_CAPTURE_REQUEST;;

        for( size_t i = 0; i < setting.vFrame.size(); ++i ) {
            IMetadata::IEntry entry(MTK_PLUGIN_P2_COMBINATION);
            entry.push_back(p2_setting, Type2Type< MINT64 >());
            setting.vFrame.editItemAt(i).curHalControl.update(entry.tag(), entry);
        }
        MY_LOGD("cathy %" PRId64 , p2_setting);
    }

    return OK;
    // check flash needed
    // yes --> apply raw
    // no --> normal
}


void
SmartShot::
setMaxCaptureFrameNum(MINT32 num)
{
    mMaxCaptureFrameNum.store(num);
}


MINT32
SmartShot::
getMaxCaptureFrameNum()
{
    return mMaxCaptureFrameNum.load();
}


/******************************************************************************
*
*******************************************************************************/
static
MVOID
PREPARE_STREAM(BufferList& vDstStreams, StreamId id, MBOOL criticalBuffer)
{
    vDstStreams.push_back(
        BufferSet{
            .streamId       = id,
            .criticalBuffer = criticalBuffer,
        }
    );
}

MBOOL
SmartShot::
applyRawBufferSettings(
    Vector< SettingSet > vSettings,
    MINT32 shotCount
)
{
    MY_LOGD("Apply user's setting.");
    //
    sp<StreamBufferProvider> pProvider = mpConsumer.promote();
    if(pProvider == NULL) {
        MY_LOGE("pProvider is NULL!");
        return MFALSE;
    }
    //
    sp<IFeatureFlowControl> pFeatureFlowControl = IResourceContainer::getInstance(getOpenId())->queryFeatureFlowControl();
    if(pFeatureFlowControl == NULL) {
        MY_LOGE("IFeatureFlowControl is NULL!");
        return MFALSE;
    }
    //
    HwInfoHelper helper(getOpenId());
    MSize sensorSize;
    if( ! helper.updateInfos() ) {
        MY_LOGE("cannot properly update infos!");
        return MFALSE;
    }
    //
    if( ! helper.getSensorSize( mu4Scenario, sensorSize) ) {
        MY_LOGE("cannot get params about sensor!");
        return MFALSE;
    }
    MY_LOGD("sensorMode(%d), sensorSize(%d,%d)", mu4Scenario, sensorSize.w, sensorSize.h);
    //
    for ( size_t i = 0; i < vSettings.size(); ++i ) {
        IMetadata::IEntry entry(MTK_HAL_REQUEST_SENSOR_SIZE);
        entry.push_back(sensorSize, Type2Type< MSize >());
        vSettings.editItemAt(i).halSetting.update(entry.tag(), entry);
    }
    //
    BufferList           vDstStreams;
    Vector< MINT32 >     vRequestNo;

    Vector<sp<IImageStreamInfo>> rawInputInfos;
    pProvider->querySelector()->queryCollectImageStreamInfo( rawInputInfos );
    BufferList vDstStreams_disp;
    MBOOL bRrzo = MFALSE;
    for( size_t i = 0; i < rawInputInfos.size() ; i++) {
        PREPARE_STREAM(vDstStreams, rawInputInfos[i]->getStreamId(),  true);
        if (rawInputInfos[i]->getStreamId() == eSTREAMID_IMAGE_PIPE_RAW_RESIZER) bRrzo = MTRUE;
    }
    // prepare RRZO, however we dont need it but LMV does.
    if (!bRrzo)
        PREPARE_STREAM(vDstStreams, eSTREAMID_IMAGE_PIPE_RAW_RESIZER, false);

    // PREPARE_STREAM(vDstStreams, eSTREAMID_IMAGE_PIPE_RAW_OPAQUE,  true);
    // PREPARE_STREAM(vDstStreams, eSTREAMID_IMAGE_PIPE_YUV_00,      false);

    if( OK != pFeatureFlowControl->submitRequest(
                vSettings,
                vDstStreams,
                vRequestNo
                )
      )
    {
        MY_LOGE("submitRequest failed");
        return MFALSE;
    }
    //
    Vector< MINT32 >     vActualRequestNo;
    if ( static_cast<MUINT32>(shotCount) < vRequestNo.size() ) {
        for ( MINT32 i = 0; i < shotCount; ++i ) vActualRequestNo.push_back(vRequestNo[i]);
    } else {
        vActualRequestNo = vRequestNo;
    }
    //
    status_t status;
    {
        mspOriSelector = pProvider->querySelector();
        sp<ZsdRequestSelector> pSelector = new ZsdRequestSelector();
        pSelector->setWaitRequestNo(vActualRequestNo);
        status = pProvider->switchSelector(pSelector);
        MY_LOGD("ZsdRequestSelector");
    }
    // TODO add LCSO consumer set the same Selector
    if(status != OK)
    {
        MY_LOGE("change selector Fail!");
        return MFALSE;
    }
    MY_LOGD("change selector Success");
    //
    return MTRUE;
}
/******************************************************************************
*
*******************************************************************************/

MERROR
SmartShot::
submitCaptureSetting(
    MBOOL mainFrame,
    IMetadata appSetting,
    IMetadata halSetting
)
{
    ILegacyPipeline::ResultSet resultSet;
    if ( mbZsdFlow ){
        Mutex::Autolock _l(mResultMetadataSetLock);
        resultSet.add(eSTREAMID_META_APP_DYNAMIC_P1, mResultMetadataSetMap.editValueFor(mCapReqNo).selectorAppMetadata);
    }
    auto pPipeline = mpPipeline;
    if( !pPipeline.get() )
    {
        MY_LOGW("get pPipeline fail");
        return UNKNOWN_ERROR;
    }
    //
    {
        MBOOL expResult, isoResult;
        MINT64 expVal;
        MINT32 isoVal;

        isoResult = IMetadata::getEntry<MINT32>(const_cast<IMetadata*>(&appSetting), MTK_SENSOR_SENSITIVITY, isoVal);
        expResult = IMetadata::getEntry<MINT64>(const_cast<IMetadata*>(&appSetting), MTK_SENSOR_EXPOSURE_TIME, expVal); // ms->us

        MY_LOGD("%s:=========================", __FUNCTION__);
        MY_LOGD("%s: App metadata", __FUNCTION__);
        MY_LOGD("%s: Get Iso -> %d, iso->: %d", __FUNCTION__, isoResult, isoVal);
        MY_LOGD("%s: Get Exp -> %d, exp->: %" PRId64 "", __FUNCTION__, expResult, expVal);
        MY_LOGD("%s:=========================", __FUNCTION__);
    }
    //
    if ( mainFrame ) {
        if( OK != pPipeline->submitSetting(
                    mCapReqNo,
                    appSetting,
                    halSetting,
                    &resultSet
                    )
          )
        {
            MY_LOGE("submitRequest failed");
            return UNKNOWN_ERROR;
        }
    } else {
        BufferList vDstStreams;
        if ( !mbZsdFlow ) {
            PREPARE_STREAM(vDstStreams, eSTREAMID_IMAGE_PIPE_RAW_OPAQUE, false);
            if ( mShotParam.mbEnableLtm )
                PREPARE_STREAM(vDstStreams, eSTREAMID_IMAGE_PIPE_RAW_LCSO, false);
        } else {
            Vector<sp<IImageStreamInfo>> rawInputInfos;
            sp<StreamBufferProvider> pConsumer = mpConsumer.promote();
            if( !pConsumer.get() ) return UNKNOWN_ERROR;
            pConsumer->querySelector()->queryCollectImageStreamInfo( rawInputInfos );
            for( size_t i = 0; i < rawInputInfos.size() ; i++) {
                PREPARE_STREAM(vDstStreams, rawInputInfos[i]->getStreamId(),  false);
            }
        }
        if( OK != pPipeline->submitRequest(
                    mCapReqNo,
                    appSetting,
                    halSetting,
                    vDstStreams,
                    &resultSet
                    )
          )
        {
            MY_LOGE("submitRequest failed");
            return UNKNOWN_ERROR;
        }
    }


    return OK;
}

/******************************************************************************
*
*******************************************************************************/
std::shared_ptr<void>
SmartShot::
pausePreview()
{
    // non-zsd, no need pause preview
    CAM_TRACE_NAME("pause_preview");
    if (CC_UNLIKELY( !mbZsdFlow )) {
        MY_LOGD("no need to pause preview due to non-zsd mode");
        return nullptr;
    }

    sp<IFeatureFlowControl> _pf = IResourceContainer::getInstance(getOpenId())->queryFeatureFlowControl();
    if (CC_UNLIKELY( _pf == nullptr )) {
        MY_LOGW("IFeatureFlowControl is nullptr, cannot pause preview, ignored");
        return nullptr;
    }

    auto _result = _pf->pausePreview(true);
    if (CC_UNLIKELY( _result )) {
        MY_LOGW("pause preview failed");
        return nullptr;
    }
    //
    MY_LOGD("paused preview");
    //
    return std::shared_ptr<void>(nullptr, [_pf](void*) mutable {
        CAM_TRACE_NAME("resume_preview");
        auto _result = _pf->resumePreview();
        if (CC_UNLIKELY( _result )) {
            CAM_LOGW("resume preview failed");
        }
        else {
            CAM_LOGD("resumed preview");
        }
        _pf = nullptr;
    });
}

/******************************************************************************
*
*******************************************************************************/
MVOID
SmartShot::
updateLastInfo()
{
    mLastRotation = mShotParam.mu4Transform;
    mLastPicFmt   = mShotParam.miPictureFormat;
    mLastJpegsize = mJpegsize;
}

/******************************************************************************
*
*******************************************************************************/
MVOID
SmartShot::
endCapture()
{
    if ( mbZsdFlow ) {
        // change to ZSD selector
        sp<StreamBufferProvider> pConsumer = mpConsumer.promote();
        if( pConsumer != NULL) {
            if(mspOriSelector != nullptr) {
                MY_LOGD("restore original selector");
                pConsumer->setSelector(mspOriSelector);
                mspOriSelector = nullptr;
            }else{
                MY_LOGD("create new selector");
            sp<ZsdSelector> pSelector = new ZsdSelector();
            pConsumer->switchSelector(pSelector);
        }
    }
    }
    else {
        // wait pipeline done
        auto pPipeline = mpPipeline;
        if( pPipeline.get() ) {
            pPipeline->waitUntilDrainedAndFlush();
            mpPipeline = nullptr;
        }
    }
}

/******************************************************************************
*
*******************************************************************************/
MBOOL
SmartShot::
createStreams()
{
    CAM_TRACE_CALL();
    //
    mu4Bitdepth = getShotRawBitDepth(); // ImpShot
    //
    MUINT32 const openId        = getOpenId();
    MUINT32 const sensorMode    = mu4Scenario;
    MUINT32 const bitDepth      = mu4Bitdepth;
    //
    MBOOL const bEnablePostview = (mShotParam.miPostviewClientFormat != eImgFmt_UNKNOWN) ? MTRUE : MFALSE;
    MSize const postviewSize    = MSize(mShotParam.mi4PostviewWidth, mShotParam.mi4PostviewHeight);
    MINT const  postviewFmt     = static_cast<EImageFormat>(mShotParam.miPostviewClientFormat);
    MINT const yuvfmt_main      = eImgFmt_YUY2;
    MINT const yuvfmt_thumbnail = eImgFmt_YUY2;
    //
    MSize const thumbnailsize = MSize(mJpegParam.mi4JpegThumbWidth, mJpegParam.mi4JpegThumbHeight);
    //
    //

    if(mbZsdFlow) {
        mpInfo_ResizedRaw = nullptr;
        Vector<sp<IImageStreamInfo>> rawInputInfos;
        sp<StreamBufferProvider> pConsumer = mpConsumer.promote();
        if( !pConsumer.get() ) return UNKNOWN_ERROR;
        pConsumer->querySelector()->queryCollectImageStreamInfo( rawInputInfos );
        for(size_t i = 0; i < rawInputInfos.size() ; i++)
        {
            if(rawInputInfos[i]->getStreamId() == eSTREAMID_IMAGE_PIPE_RAW_LCSO)
                mpInfo_LcsoRaw = rawInputInfos[i];
            else if(rawInputInfos[i]->getStreamId() == eSTREAMID_IMAGE_PIPE_RAW_OPAQUE)
                mpInfo_FullRaw = rawInputInfos[i];
        }
    }
    else {
        HwInfoHelper helper(openId);
        // Fullsize Raw
        {
            if( ! helper.updateInfos() ) {
                MY_LOGE("cannot properly update infos");
                return MFALSE;
            }
            //
            if( ! helper.getSensorSize( sensorMode, mSensorSize) ||
                    ! helper.getSensorFps( sensorMode, mSensorFps) ||
                    ! helper.queryPixelMode( sensorMode, mSensorFps, mPixelMode)
              ) {
                MY_LOGE("cannot get params about sensor");
                return MFALSE;
            }
            //
            MSize size = mSensorSize;
            MINT format;
            size_t stride;
            MUINT const usage = 0; //not necessary here
            if( ! helper.getImgoFmt(bitDepth, format) ||
                    ! helper.alignPass1HwLimitation(mPixelMode, format, true, size, stride) )
            {
                MY_LOGE("wrong params about imgo");
                return MFALSE;
            }
            //
            sp<IImageStreamInfo>
                pStreamInfo = createRawImageStreamInfo(
                        "SmartShot:Fullraw",
                        eSTREAMID_IMAGE_PIPE_RAW_OPAQUE,
                        eSTREAMTYPE_IMAGE_INOUT,
                        getMaxCaptureFrameNum(), 1,
                        usage, format, size, stride
                        );
            if( pStreamInfo == nullptr ) {
                return MFALSE;
            }
            mpInfo_FullRaw = pStreamInfo;
        }
        // Resized raw (for disable frontal binning use-case)
        if (helper.isRaw() && !!postviewSize)
        {
            MSize size;
            MINT format;
            size_t stride;
            MUINT const usage =
                eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE;

            if(!helper.getRrzoFmt(bitDepth, format) ||
               !helper.alignRrzoHwLimitation(postviewSize, mSensorSize, size) ||
               !helper.alignPass1HwLimitation(mPixelMode, format, false, size, stride))
            {
                MY_LOGE("wrong parameter for RRZO");
                return MFALSE;
            }

            sp<IImageStreamInfo>
                pStreamInfo = createRawImageStreamInfo(
                    "SmartShot:ResizedRaw",
                    eSTREAMID_IMAGE_PIPE_RAW_RESIZER,
                    eSTREAMTYPE_IMAGE_INOUT,
                    getMaxCaptureFrameNum(), 1,
                    usage, format, size, stride);

            if( pStreamInfo == nullptr ) {
                MY_LOGE("create ImageStreamInfo ResizedRaw failed");
                return MFALSE;
            }

            MY_LOGD("create ImageStreamInfo ResizedRaw done");
            mpInfo_ResizedRaw = pStreamInfo;
        }
        // Lcso Raw
        if( mShotParam.mbEnableLtm )
        {
            NS3Av3::LCSO_Param lcsoParam;
            if ( auto pIspMgr = MAKE_IspMgr() ) {
                pIspMgr->queryLCSOParams(lcsoParam);
            }

            MUINT const usage = eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE; //not necessary here
            //
            sp<IImageStreamInfo>
                pStreamInfo = createRawImageStreamInfo(
                        "SingleShot:LCSraw",
                        eSTREAMID_IMAGE_PIPE_RAW_LCSO,
                        eSTREAMTYPE_IMAGE_INOUT,
                        getMaxCaptureFrameNum(), 1,
                        usage, lcsoParam.format, lcsoParam.size, lcsoParam.stride
                        );
            if( pStreamInfo == nullptr ) {
                return MFALSE;
            }
            mpInfo_LcsoRaw = pStreamInfo;
        }
    }
    //
    // postview YUV
    if (bEnablePostview)
    {
        MSize size        = postviewSize;
        MINT format       = postviewFmt;
        MUINT const usage = 0; //not necessary here
        MUINT32 transform = 0;
        sp<IImageStreamInfo>
            pStreamInfo = createImageStreamInfo(
                    "SingleShot:Postview",
                    eSTREAMID_IMAGE_PIPE_YUV_00,
                    eSTREAMTYPE_IMAGE_INOUT,
                    2, 1,
                    usage, format, size, transform
                    );
        if( pStreamInfo == nullptr ) {
            return BAD_VALUE;
        }
        //
        mpInfo_YuvPostview = pStreamInfo;
    }
    //
    // Yuv
    {
        MSize size        = mJpegsize;
        MINT format       = yuvfmt_main;
        MUINT const usage = 0; //not necessary here
        MUINT32 transform = mShotParam.mu4Transform;
        sp<IImageStreamInfo>
            pStreamInfo = createImageStreamInfo(
                    "SmartShot:MainYuv",
                    eSTREAMID_IMAGE_PIPE_YUV_JPEG,
                    eSTREAMTYPE_IMAGE_INOUT,
                    2, 1,
                    usage, format, size, transform
                    );
        if( pStreamInfo == nullptr ) {
            return BAD_VALUE;
        }
        //
        mpInfo_Yuv = pStreamInfo;
    }
    //
    // Thumbnail Yuv
    {
        MSize size        = thumbnailsize;
        MINT format       = yuvfmt_thumbnail;
        MUINT const usage = 0; //not necessary here
        MUINT32 transform = 0;
        sp<IImageStreamInfo>
            pStreamInfo = createImageStreamInfo(
                    "SmartShot:ThumbnailYuv",
                    eSTREAMID_IMAGE_PIPE_YUV_THUMBNAIL,
                    eSTREAMTYPE_IMAGE_INOUT,
                    2, 1,
                    usage, format, size, transform
                    );
        if( pStreamInfo == nullptr ) {
            return BAD_VALUE;
        }
        //
        mpInfo_YuvThumbnail = pStreamInfo;
    }
    //
    // Jpeg
    {
        MSize size        = mJpegsize;
        MINT format       = eImgFmt_BLOB;
        MUINT const usage = 0; //not necessary here
        MUINT32 transform = 0;
        sp<IImageStreamInfo>
            pStreamInfo = createImageStreamInfo(
                    "SmartShot:Jpeg",
                    eSTREAMID_IMAGE_JPEG,
                    eSTREAMTYPE_IMAGE_INOUT,
                    1, 1,
                    usage, format, size, transform
                    );
        if( pStreamInfo == nullptr ) {
            return BAD_VALUE;
        }
        //
        mpInfo_Jpeg = pStreamInfo;
    }
    //
    return MTRUE;

}

/******************************************************************************
*
*******************************************************************************/
MBOOL
SmartShot::
createPipeline()
{
    CAM_TRACE_CALL();
    FUNC_START;
    LegacyPipelineBuilder::ConfigParams LPBConfigParams;
    LPBConfigParams.mode = getLegacyPipelineMode();
    LPBConfigParams.enableEIS = MFALSE;
    LPBConfigParams.enableLCS = mShotParam.mbEnableLtm;
    LPBConfigParams.pluginUser = plugin::CALLER_SHOT_SMART;
    if ( mbZsdFlow ) {
        Vector<sp<IImageStreamInfo>> rawInputInfos;
        sp<StreamBufferProvider> pConsumer = mpConsumer.promote();
        if( !pConsumer.get() ) return UNKNOWN_ERROR;
        pConsumer->querySelector()->queryCollectImageStreamInfo( rawInputInfos );
        for(size_t i = 0; i < rawInputInfos.size() ; i++)
        {
            if(rawInputInfos[i]->getStreamId() == eSTREAMID_IMAGE_PIPE_RAW_LCSO)
                LPBConfigParams.enableLCS = MTRUE;
        }
    }
    //
    HwInfoHelper helper(getOpenId());
    if( ! helper.updateInfos() ) {
        MY_LOGE("cannot properly update infos");
        return BAD_VALUE;
    }
    //
    if (helper.getDualPDAFSupported(mu4Scenario))
    {
        LPBConfigParams.enableDualPD = MTRUE;
    }
    //
    MUINT32 pipeBit;
    MINT ImgFmt = eImgFmt_BAYER12;
    if( helper.getLpModeSupportBitDepthFormat(ImgFmt, pipeBit) )
    {
        using namespace NSCam::NSIoPipe::NSCamIOPipe;
        if(pipeBit & CAM_Pipeline_14BITS)
        {
            LPBConfigParams.pipeBit = CAM_Pipeline_14BITS;
        }
        else
        {
            LPBConfigParams.pipeBit = CAM_Pipeline_12BITS;
        }
    }
    //
    sp<LegacyPipelineBuilder> pBuilder = LegacyPipelineBuilder::createInstance(
                                            getOpenId(),
                                            "SmartShot",
                                            LPBConfigParams);
    CHECK_OBJECT(pBuilder);

    mpImageCallback = new SImageCallback(this, 0);
    CHECK_OBJECT(mpImageCallback);

    sp<BufferCallbackHandler> pCallbackHandler = new BufferCallbackHandler(getOpenId());
    CHECK_OBJECT(pCallbackHandler);
    pCallbackHandler->setImageCallback(mpImageCallback);
    mpCallbackHandler = pCallbackHandler;

    sp<StreamBufferProviderFactory> pFactory =
                StreamBufferProviderFactory::createInstance();
    //
    Vector<PipelineImageParam> vImageParam;
    //
    {
        MY_LOGD("createPipeline for smart shot");
        if ( mbZsdFlow ) {
            Vector<sp<IImageStreamInfo>> rawInputInfos;
            sp<StreamBufferProvider> pConsumer = mpConsumer.promote();
            if( !pConsumer.get() ) return UNKNOWN_ERROR;
            pConsumer->querySelector()->queryCollectImageStreamInfo( rawInputInfos );
            //
            Vector<PipelineImageParam> vImgSrcParams;
            sp<IImageStreamInfo> pStreamInfo = NULL;
            for(size_t i = 0; i < rawInputInfos.size() ; i++) {
                pStreamInfo = rawInputInfos[i];
                sp<CallbackBufferPool> pPool = new CallbackBufferPool(pStreamInfo);
                //pPool->addBuffer(pBuf);
                mpCallbackHandler->setBufferPool(pPool);
                //
                pFactory->setImageStreamInfo(pStreamInfo);
                pFactory->setUsersPool(
                        mpCallbackHandler->queryBufferPool(pStreamInfo->getStreamId())
                    );
                PipelineImageParam imgParam = {
                    .pInfo     = pStreamInfo,
                    .pProvider = pFactory->create(false),
                    .usage     = 0
                };
                vImgSrcParams.push_back(imgParam);
            }
            //
            if( OK != pBuilder->setSrc(vImgSrcParams) ) {
                MY_LOGE("setSrc failed");
                return MFALSE;
            //
            }
        } else {

            // query VHDR sensor mode
            MUINT32 vhdrMode = SENSOR_VHDR_MODE_NONE;
            sp<IParamsManager> pParamsMgr = mShotParam.mpParamsMgr.promote();
            if (CC_UNLIKELY( pParamsMgr.get() == nullptr )) {
                MY_LOGE("cannot promote IParamsManager, use SENSOR_VHDR_MODE_NONE as default");
            }
            else {
                // query from feature table to decide whether
                // single-frame HDR or multi-frame HDR capture is supported
                bool isSingleFrameCaptureHDR = pParamsMgr->getSingleFrameCaptureHDR();
                // query current VHDR mode
                uint32_t __vhdrMode = pParamsMgr->getVHdr();
                if (isSingleFrameCaptureHDR) {
                    switch (__vhdrMode) {
                    case SENSOR_VHDR_MODE_ZVHDR:
                        vhdrMode = SENSOR_VHDR_MODE_ZVHDR;
                        break;
                    case SENSOR_VHDR_MODE_IVHDR:
                        vhdrMode = SENSOR_VHDR_MODE_IVHDR;
                        break;
                    default:;
                    }
                }
            }

            MY_LOGD("vhdrMode(0x%X)", vhdrMode);

            PipelineSensorParam sensorParam(
            /*.mode      = */mu4Scenario,
            /*.rawType   = */0x0000,
            /*.size      = */mSensorSize,
            /*.fps       = */(MUINT)mSensorFps,
            /*.pixelMode = */mPixelMode,
            /*.vhdrMode  = */vhdrMode
            );
            //
            if( OK != pBuilder->setSrc(sensorParam) ) {
                MY_LOGE("setSrc failed");
                return MFALSE;
            }
            //
            sp<IScenarioControl> pScenarioCtrl = IScenarioControl::create(getOpenId());
            if( pScenarioCtrl.get() == NULL )
            {
                MY_LOGE("get Scenario Control fail");
                return MFALSE;
            }
            IScenarioControl::ControlParam param;
            param.scenario   = IScenarioControl::Scenario_Capture;
            param.sensorSize = mSensorSize;
            param.sensorFps  = mSensorFps;
            if(LPBConfigParams.enableDualPD)
                FEATURE_CFG_ENABLE_MASK(param.featureFlag,IScenarioControl::FEATURE_DUAL_PD);

            pScenarioCtrl->enterScenario(param);
            pBuilder->setScenarioControl(pScenarioCtrl);
            //
            // fullraw
            if( mpInfo_FullRaw.get() )
            {
                sp<IImageStreamInfo> pStreamInfo = mpInfo_FullRaw;
                //
                sp<CallbackBufferPool> pPool = new CallbackBufferPool(pStreamInfo);
                pPool->allocateBuffer(
                                  pStreamInfo->getStreamName(),
                                  pStreamInfo->getMaxBufNum(),
                                  pStreamInfo->getMinInitBufNum()
                                  );
                mpCallbackHandler->setBufferPool(pPool);
                //
                pFactory->setImageStreamInfo(pStreamInfo);
                pFactory->setUsersPool(
                            mpCallbackHandler->queryBufferPool(pStreamInfo->getStreamId())
                        );
                //
                PipelineImageParam imgParam = {
                    .pInfo     = pStreamInfo,
                    .pProvider = pFactory->create(false),
                    .usage     = 0
                };
                vImageParam.push_back(imgParam);
            }
            // resized raw
            if( mpInfo_ResizedRaw.get() )
            {
                sp<IImageStreamInfo> pStreamInfo = mpInfo_ResizedRaw;
                //
                PipelineImageParam imgParam = {
                    .pInfo     = pStreamInfo,
                    .pProvider = nullptr,
                    .usage     = 0
                };
                vImageParam.push_back(imgParam);
            }
            // resized raw
            if( mpInfo_LcsoRaw.get() )
            {
                sp<IImageStreamInfo> pStreamInfo = mpInfo_LcsoRaw;
                //
                PipelineImageParam imgParam = {
                    .pInfo     = pStreamInfo,
                    .pProvider = nullptr,
                    .usage     = 0
                };
                vImageParam.push_back(imgParam);
            }
        }

    }
    //
    {
        if( mpInfo_Yuv.get() )
        {
            CAM_TRACE_NAME("set mpInfo_Yuv");
            sp<IImageStreamInfo> pStreamInfo = mpInfo_Yuv;
            //
            sp<CallbackBufferPool> pPool = new CallbackBufferPool(pStreamInfo);
            {
                CAM_TRACE_NAME("alloc_buf");
                pPool->allocateBuffer(
                                  pStreamInfo->getStreamName(),
                                  pStreamInfo->getMaxBufNum(),
                                  pStreamInfo->getMinInitBufNum()
                                  );
                mpCallbackHandler->setBufferPool(pPool);
            }
            //
            pFactory->setImageStreamInfo(pStreamInfo);
            pFactory->setUsersPool(
                        mpCallbackHandler->queryBufferPool(pStreamInfo->getStreamId())
                    );
            //
            PipelineImageParam imgParam = {
                .pInfo     = pStreamInfo,
                .pProvider = pFactory->create(false),
                .usage     = 0
            };
            vImageParam.push_back(imgParam);
        }
        //
        if( mpInfo_YuvPostview.get() )
        {
            CAM_TRACE_NAME("set mpInfo_YuvPostview");
            sp<IImageStreamInfo> pStreamInfo = mpInfo_YuvPostview;
            //
            sp<CallbackBufferPool> pPool = new CallbackBufferPool(pStreamInfo);
            {
                CAM_TRACE_NAME("alloc_buf");
                pPool->allocateBuffer(
                                  pStreamInfo->getStreamName(),
                                  pStreamInfo->getMaxBufNum(),
                                  pStreamInfo->getMinInitBufNum()
                                  );
            }
            mpCallbackHandler->setBufferPool(pPool);
            //
            pFactory->setImageStreamInfo(pStreamInfo);
            pFactory->setUsersPool(
                        mpCallbackHandler->queryBufferPool(pStreamInfo->getStreamId())
                    );
            //
            PipelineImageParam imgParam = {
                .pInfo     = pStreamInfo,
                .pProvider = pFactory->create(false),
                .usage     = 0
            };
            vImageParam.push_back(imgParam);
        }
        //
        if( mpInfo_YuvThumbnail.get() )
        {
            CAM_TRACE_NAME("set mpInfo_YuvThumbnail");
            sp<IImageStreamInfo> pStreamInfo = mpInfo_YuvThumbnail;
            //
            sp<CallbackBufferPool> pPool = new CallbackBufferPool(pStreamInfo);
            {
                CAM_TRACE_NAME("alloc_buf");
                pPool->allocateBuffer(
                                  pStreamInfo->getStreamName(),
                                  pStreamInfo->getMaxBufNum(),
                                  pStreamInfo->getMinInitBufNum()
                                  );
            }
            mpCallbackHandler->setBufferPool(pPool);
            //
            pFactory->setImageStreamInfo(pStreamInfo);
            pFactory->setUsersPool(
                        mpCallbackHandler->queryBufferPool(pStreamInfo->getStreamId())
                    );
            //
            PipelineImageParam imgParam = {
                .pInfo     = pStreamInfo,
                .pProvider = pFactory->create(false),
                .usage     = 0
            };
            vImageParam.push_back(imgParam);
        }
        //
        if( mpInfo_Jpeg.get() )
        {
            CAM_TRACE_NAME("set mpInfo_Jpeg");
            sp<IImageStreamInfo> pStreamInfo = mpInfo_Jpeg;
            //
            sp<CallbackBufferPool> pPool = new CallbackBufferPool(pStreamInfo);
            {
                CAM_TRACE_NAME("allocateBuffer");
                pPool->allocateBuffer(
                                  pStreamInfo->getStreamName(),
                                  pStreamInfo->getMaxBufNum(),
                                  pStreamInfo->getMinInitBufNum()
                                  );
            }
            mpCallbackHandler->setBufferPool(pPool);
            //
            pFactory->setImageStreamInfo(pStreamInfo);
            pFactory->setUsersPool(
                        mpCallbackHandler->queryBufferPool(pStreamInfo->getStreamId())
                    );
            //
            PipelineImageParam imgParam = {
                .pInfo     = pStreamInfo,
                .pProvider = pFactory->create(false),
                .usage     = 0
            };
            vImageParam.push_back(imgParam);
        }
        //
        if( OK != pBuilder->setDst(vImageParam) ) {
            MY_LOGE("setDst failed");
            return MFALSE;
        }
    }
    //
    mpPipeline = pBuilder->create();
    //
    FUNC_END;
    return MTRUE;

}

/******************************************************************************
*
*******************************************************************************/
MINT
SmartShot::
getLegacyPipelineMode(void)
{
    int shotMode = getShotMode();
    EPipelineMode pipelineMode = getPipelineMode();
    int legacyPipeLineMode = LegacyPipelineMode_T::PipelineMode_Capture;
    switch(shotMode)
    {
        default:
            legacyPipeLineMode = (pipelineMode == ePipelineMode_Feature) ?
                LegacyPipelineMode_T::PipelineMode_Feature_Capture :
                LegacyPipelineMode_T::PipelineMode_Capture;
            break;
    }
    return legacyPipeLineMode;
}

/******************************************************************************
*
*******************************************************************************/
status_t
SmartShot::
getSelectorData(
    IMetadata& rAppSetting,
    IMetadata& rHalSetting,
    android::sp<IImageBuffer>& pBuffer
)
{
    CAM_TRACE_CALL();
    //
    sp<StreamBufferProvider> pConsumer = mpConsumer.promote();
    if(pConsumer == NULL) {
        MY_LOGE("mpConsumer is NULL!");
        return UNKNOWN_ERROR;
    }
    //
    sp< ISelector > pSelector = pConsumer->querySelector();
    if(pSelector == NULL) {
        MY_LOGE("can't find Selector in Consumer");
        return UNKNOWN_ERROR;
    }
    //
    status_t status = OK;
    MINT32 rRequestNo;
    Vector<ISelector::MetaItemSet> rvResultMeta;
    Vector<ISelector::BufferItemSet> rvBufferSet;
    status = pSelector->getResult(rRequestNo, rvResultMeta, rvBufferSet);

    // check
    MBOOL found = MFALSE;
    for ( size_t i = 0; i < rvBufferSet.size(); ++i ) {
        if (rvBufferSet[i].id == eSTREAMID_IMAGE_PIPE_RAW_OPAQUE) {
            pBuffer = rvBufferSet[i].heap->createImageBuffer();
            found = MTRUE;
            break;
        }
    }
    if ( !found || rvBufferSet.isEmpty() ) {
        MY_LOGE("Selector get input raw buffer failed! bufferSetSize(%zu). found(%d)", rvBufferSet.size(), found);
        return UNKNOWN_ERROR;
    }

    if(status != OK) {
        MY_LOGE("Selector getResult Fail!");
        return UNKNOWN_ERROR;
    }

    if(rvResultMeta.size() < 2) {
        MY_LOGE("ZsdSelect getResult rvResultMeta(%zu)", rvResultMeta.size());
        return UNKNOWN_ERROR;
    }

    // get app & hal metadata
    if(    rvResultMeta.editItemAt(0).id == eSTREAMID_META_APP_DYNAMIC_P1
        && rvResultMeta.editItemAt(1).id == eSTREAMID_META_HAL_DYNAMIC_P1 )
    {
        rAppSetting  = rvResultMeta.editItemAt(0).meta;
        rHalSetting += rvResultMeta.editItemAt(1).meta;
    }
    else
    if(    rvResultMeta.editItemAt(1).id == eSTREAMID_META_APP_DYNAMIC_P1
        && rvResultMeta.editItemAt(0).id == eSTREAMID_META_HAL_DYNAMIC_P1 )
    {
        rAppSetting  = rvResultMeta.editItemAt(1).meta;
        rHalSetting += rvResultMeta.editItemAt(0).meta;
    }
    else {
        MY_LOGE("Something wrong for selector metadata.");
        return UNKNOWN_ERROR;
    }

    for ( size_t i = 0; i < rvBufferSet.size(); ++i ) {
        pBuffer = rvBufferSet[i].heap->createImageBuffer();
        //
        if(pBuffer == NULL) {
            MY_LOGE("get buffer is NULL!");
            return UNKNOWN_ERROR;
        }

       //update p1 buffer to pipeline pool
        sp<CallbackBufferPool> pPool = mpCallbackHandler->queryBufferPool( rvBufferSet[i].id );
        if( pPool == NULL) {
            MY_LOGE("query Pool Fail!");
            return UNKNOWN_ERROR;
        }
        pPool->addBuffer(pBuffer);
    }
    //
    {
        Mutex::Autolock _l(mResultMetadataSetLock);

        IMetadata resultAppMetadata;
        IMetadata resultHalMetadata;
        mResultMetadataSetMap.add(
                static_cast<MUINT32>(mCapReqNo),
                ResultSet_T{static_cast<MUINT32>(mCapReqNo), resultAppMetadata, resultHalMetadata, rAppSetting, rvBufferSet, NONE_CB_DONE }
                );
    }
    return OK;
}

/******************************************************************************
*
*******************************************************************************/
MBOOL
SmartShot::
makeVendorCombination(
    plugin::InputInfo&  inputInfo,
    MINT64              const mode
)
{
    MBOOL ret = MTRUE;

    switch(mode){
        case MTK_PLUGIN_MODE_MFNR:
        case MTK_PLUGIN_MODE_HDR:
            inputInfo.combination.push_back(mode);
            inputInfo.combination.push_back(MTK_PLUGIN_MODE_NR);
            inputInfo.combination.push_back(MTK_PLUGIN_MODE_COPY);
            break;
        case MTK_PLUGIN_MODE_MFHR:
        case MTK_PLUGIN_MODE_BMDN:
        case MTK_PLUGIN_MODE_FUSION_3rdParty:
            inputInfo.combination.push_back(mode);
            break;
        default:
            MY_LOGE("can't support this vendor mode! %" PRId64 "", mode);
            ret = MFALSE;
    }
    return ret;
}
/*******************************************************************************
*
********************************************************************************/
MVOID
SmartShot::
onMetaReceived(
    MUINT32         const requestNo,
    StreamId_T      const streamId,
    MBOOL           const errorResult,
    IMetadata       const result
)
{
    CAM_TRACE_FMT_BEGIN("onMetaReceived No%d,StreamID %#" PRIx64, requestNo,streamId);
    MY_LOGD("requestNo %d, stream %#" PRIx64", errResult:%d", requestNo, streamId, errorResult);
    //
    {
        Mutex::Autolock _l(mResultMetadataSetLock);
        int idx = mResultMetadataSetMap.indexOfKey(requestNo);
        if(idx < 0 )
        {
            MY_LOGE("mResultMetadataSetMap can't find requestNo(%d)",requestNo);
            for ( size_t i=0; i<mResultMetadataSetMap.size(); i++) {
                MY_LOGD( "mResultMetadataSetMap(%zu/%zu)  requestNo(%d) buf(%p)",
                         i, mResultMetadataSetMap.size(), mResultMetadataSetMap[i].requestNo,
                         mResultMetadataSetMap[i].selectorGetBufs[0].heap.get() );
            }
            return;
        }
    }
    //
    MINT32 callbackState = NONE_CB_DONE;
//#warning "Need to modify if pipeline change"
    switch (streamId)
    {
        case eSTREAMID_META_APP_FULL:
            {
                Mutex::Autolock _l(mResultMetadataSetLock);
                mResultMetadataSetMap.editValueFor(requestNo).appResultMetadata = result;
                mResultMetadataSetMap.editValueFor(requestNo).appResultMetadata += mResultMetadataSetMap.editValueFor(requestNo).selectorAppMetadata;
                mResultMetadataSetMap.editValueFor(requestNo).callbackState |= FULL_APP_META_CB_DONE;
                callbackState = mResultMetadataSetMap.editValueFor(requestNo).callbackState;
            }

            if( (callbackState & FULL_ALL_META_CB_DONE) == FULL_ALL_META_CB_DONE )
            {
                if(isDataMsgEnabled(EIShot_DATA_MSG_RAW))
                {
                    handleDNGMetaCB(requestNo);//dng metadata callback
                }
            }
            //
            if( (mResultMetadataSetMap.editValueFor(requestNo).callbackState & DNG_CB_DONE) == DNG_CB_DONE )
            {
                //clear result metadata
                Mutex::Autolock _l(mResultMetadataSetLock);
                mResultMetadataSetMap.removeItem(requestNo);
                MY_LOGD("clear mResultMetadataSetMap(%d)",requestNo);
            }
            break;
        case eSTREAMID_META_HAL_FULL:
            {
                Mutex::Autolock _l(mResultMetadataSetLock);
                mResultMetadataSetMap.editValueFor(requestNo).halResultMetadata = result;
                mResultMetadataSetMap.editValueFor(requestNo).callbackState |= FULL_HAL_META_CB_DONE;
                callbackState = mResultMetadataSetMap.editValueFor(requestNo).callbackState;
            }
            //
            if( (callbackState & FULL_ALL_META_CB_DONE) == FULL_ALL_META_CB_DONE )
            {
                if(isDataMsgEnabled(EIShot_DATA_MSG_RAW))
                {
                    handleDNGMetaCB(requestNo);//dng metadata callback
                }
            }
            //
            if( (callbackState & DNG_CB_DONE) == DNG_CB_DONE )
            {
                //clear result metadata
                Mutex::Autolock _l(mResultMetadataSetLock);
                mResultMetadataSetMap.removeItem(requestNo);
                MY_LOGD("clear mResultMetadataSetMap(%d)",requestNo);
            }
            break;
        case eSTREAMID_META_HAL_DYNAMIC_P2:
            {
                Mutex::Autolock _l(mResultMetadataSetLock);
                mResultMetadataSetMap.editValueFor(requestNo).halResultMetadata = result;
            }
            break;
        case eSTREAMID_META_APP_DYNAMIC_P2:
            {
                Mutex::Autolock _l(mResultMetadataSetLock);
                mResultMetadataSetMap.editValueFor(requestNo).appResultMetadata += result;
            }
            break;
        default:
            MY_LOGD("unsupported streamID(%#" PRIx64")", streamId);
    }
    //
    CAM_TRACE_FMT_END();
}

/*******************************************************************************
*
********************************************************************************/
MVOID
SmartShot::
onDataReceived(
    MUINT32 const               requestNo,
    StreamId_T const            streamId,
    MBOOL   const               errorBuffer,
    android::sp<IImageBuffer>&  pBuffer
)
{
    CAM_TRACE_FMT_BEGIN("onDataReceived No%d,streamId %#" PRIx64"",requestNo,streamId);
    MY_LOGD("requestNo %d, streamId %#" PRIx64", buffer %p, error %d", requestNo, streamId, pBuffer.get(), errorBuffer);
    //
    if( pBuffer != nullptr )
    {
        MINT32 data = NSCamShot::ECamShot_DATA_MSG_NONE;
        //
        switch (streamId)
        {
            case eSTREAMID_IMAGE_PIPE_RAW_OPAQUE:
                data = NSCamShot::ECamShot_DATA_MSG_RAW;
                if(isDataMsgEnabled(EIShot_DATA_MSG_RAW))
                {
                    pBuffer->lockBuf(LOG_TAG, eBUFFER_USAGE_SW_READ_MASK);
                    handleRaw16CB(pBuffer.get());
                    pBuffer->unlockBuf(LOG_TAG);
                }
                //
                if ( mbZsdFlow ) {
                    sp<StreamBufferProvider> pProvider = IResourceContainer::getInstance(getOpenId())->queryConsumer( eSTREAMID_IMAGE_PIPE_RAW_OPAQUE );
                    MY_LOGD("Query Consumer OpenID(%d) StreamID(%d)", getOpenId(), eSTREAMID_IMAGE_PIPE_RAW_OPAQUE);
                    if( pProvider == NULL) {
                        MY_LOGE("can't find StreamBufferProvider in ConsumerContainer");
                    }
                    else {
                        Vector<ISelector::BufferItemSet> bufferSet;
                        {
                            Mutex::Autolock _l(mResultMetadataSetLock);
                            bufferSet = mResultMetadataSetMap.editValueFor(requestNo).selectorGetBufs;
                        };

                        sp< ISelector > pSelector = pProvider->querySelector();
                        if(pSelector == NULL) {
                            MY_LOGE("can't find Selector in Consumer when p2done");
                        } else {
                            for(size_t i = 0; i < bufferSet.size() ; i++)
                                pSelector->returnBuffer(bufferSet.editItemAt(i));
                        }
                    }
                    //
                    {
                        Mutex::Autolock _l(mResultMetadataSetLock);
                        for (auto& itr : mResultMetadataSetMap.editValueFor(requestNo).selectorGetBufs) {
                            itr = ISelector::BufferItemSet();
                        }
                    }
                }
                //
                {
                    Mutex::Autolock _l(mResultMetadataSetLock);
                    mResultMetadataSetMap.editValueFor(requestNo).callbackState |= RAW_CB_DONE;
                    if( (mResultMetadataSetMap.editValueFor(requestNo).callbackState & DNG_CB_DONE) == DNG_CB_DONE )
                    {
                        //clear result metadata
                        mResultMetadataSetMap.removeItem(requestNo);
                        MY_LOGD("clear mResultMetadataSetMap(%d)",requestNo);
                    }
                }
                break;
            case eSTREAMID_IMAGE_PIPE_YUV_JPEG:
                data = NSCamShot::ECamShot_DATA_MSG_YUV;
                break;
            case eSTREAMID_IMAGE_PIPE_YUV_00:
                data = NSCamShot::ECamShot_DATA_MSG_POSTVIEW;
                handlePostViewData(pBuffer.get());
                break;
            case eSTREAMID_IMAGE_JPEG:
                data = NSCamShot::ECamShot_DATA_MSG_JPEG;
                handleJpegData(requestNo, pBuffer.get());
                break;
            default:
                data = NSCamShot::ECamShot_DATA_MSG_NONE;
                break;
        }
        //
        if( mDumpFlag & data )
        {
            String8 filename = String8::format("%s/SmartShot_%dx%d",
                    DUMP_PATH, pBuffer->getImgSize().w, pBuffer->getImgSize().h);
            switch( data )
            {
                case NSCamShot::ECamShot_DATA_MSG_RAW:
                    filename += String8::format("_%zd.raw", pBuffer->getBufStridesInBytes(0));
                    break;
                case NSCamShot::ECamShot_DATA_MSG_YUV:
                case NSCamShot::ECamShot_DATA_MSG_POSTVIEW:
                    filename += String8(".yuv");
                    break;
                case NSCamShot::ECamShot_DATA_MSG_JPEG:
                    filename += String8(".jpeg");
                    break;
                default:
                    break;
            }
            pBuffer->lockBuf(LOG_TAG, eBUFFER_USAGE_SW_READ_MASK);
            pBuffer->saveToFile(filename);
            pBuffer->unlockBuf(LOG_TAG);
            //
            MY_LOGD("dump buffer in %s", filename.string());
        }
    }
    CAM_TRACE_FMT_END();
}

/*******************************************************************************
*
********************************************************************************/
MERROR
SmartShot::
checkStreamAndEncodeLocked( MUINT32 const /*requestNo*/)
{
    return OK;
}

/*******************************************************************************
*
********************************************************************************/
MVOID
SmartShot::
beginCapture(
    MINT32 rAllocateCount
)
{
    CAM_TRACE_CALL();
    //
     HwInfoHelper helper(getOpenId());
     if( ! helper.updateInfos() ) {
         MY_LOGE("cannot properly update infos");
     }
     if( ! helper.getSensorSize( mu4Scenario, mSensorSize) ) {
         MY_LOGE("cannot get params about sensor");
     }
    //
    // TODO add other consumer
    if ( mbZsdFlow ) {
        mpConsumer = IResourceContainer::getInstance(getOpenId())->queryConsumer( eSTREAMID_IMAGE_PIPE_RAW_OPAQUE );
        MY_LOGD("Query Consumer OpenID(%d) StreamID(%d)", getOpenId(), eSTREAMID_IMAGE_PIPE_RAW_OPAQUE);
        sp<StreamBufferProvider> pProvider = mpConsumer.promote();
        if( pProvider == NULL) {
            MY_LOGE("can't find StreamBufferProvider in ConsumerContainer");
            return;
        }
        sp<IImageStreamInfo> pInfo = pProvider->queryImageStreamInfo();
        pProvider->updateBufferCount("SmartShot", pInfo->getMaxBufNum() + rAllocateCount);
    }
    mJpegsize      = (mShotParam.mu4Transform & eTransform_ROT_90) ?
         MSize(mShotParam.mi4PictureHeight, mShotParam.mi4PictureWidth):
         MSize(mShotParam.mi4PictureWidth, mShotParam.mi4PictureHeight);
    //
    createStreams();
}

/*******************************************************************************
*
********************************************************************************/
MBOOL
SmartShot::
constructCapturePipeline()
{
    CAM_TRACE_CALL();

    //
    MY_LOGD("zsd raw stream %#" PRIx64 ", (%s) size(%dx%d), fmt 0x%x",
            mpInfo_FullRaw->getStreamId(),
            mpInfo_FullRaw->getStreamName(),
            mpInfo_FullRaw->getImgSize().w,
            mpInfo_FullRaw->getImgSize().h,
            mpInfo_FullRaw->getImgFormat()
           );
    // create new pipeline
    if ( ! createPipeline() ) {
        MY_LOGE("createPipeline failed");
        return MFALSE;
    }
    //
    auto pPipeline = mpPipeline;
    CHECK_OBJECT(pPipeline);
    //
    if( !pPipeline.get() )
    {
        MY_LOGW("get pPipeline fail");
        return MFALSE;
    }
    sp<ResultProcessor> pResultProcessor = pPipeline->getResultProcessor().promote();
    CHECK_OBJECT(pResultProcessor);

    // metadata
    mpMetadataListener = new SMetadataListener(this);
    pResultProcessor->registerListener( std::numeric_limits<MUINT32>::min(), std::numeric_limits<MUINT32>::max(), true, mpMetadataListener);

    mpMetadataListenerFull = new SMetadataListener(this);
    pResultProcessor->registerListener( std::numeric_limits<MUINT32>::min(), std::numeric_limits<MUINT32>::max(), false, mpMetadataListenerFull);

    return MTRUE;
}

/*******************************************************************************
*
********************************************************************************/
SmartShotUtil*
SmartShotUtil::
getInstance()
{
    static SmartShotUtil ssUtil;
    return &ssUtil;
}
/*******************************************************************************
*
********************************************************************************/
SmartShotUtil::
SmartShotUtil()
{
  mpSem = new Semaphore(0);
  if (mpSem == NULL)
  {
      MYU_LOGE("alloc Semaphore fail!!");
  }
}
/*******************************************************************************
*
********************************************************************************/
SmartShotUtil::
~SmartShotUtil()
{
    if (mpSem)
        delete mpSem;
}
/*******************************************************************************
*
********************************************************************************/
void
SmartShotUtil::
SyncDualCam(
    int openId
)
{
    MBOOL bSync;
    {
        Mutex::Autolock _l(mOpenIdMutex);
        mvOpenIds.push_back(openId);
        if ((bSync = (mvOpenIds.size() >= 2)))
            mvOpenIds.clear();
    }

    if (bSync)
    {
        mpSem->post();
    }
    else
    {
        bool bStatus = mpSem->timedWait(10000);  // 10 secs
        if (bStatus == false)
        {
            MYU_LOGE("sem_timedwait(), %s, errno = %d", strerror(errno), errno);
        }
    }
}
