/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings.op01;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.NetworkInfo.DetailedState;
import android.net.NetworkUtils;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings.System;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceChangeListener;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.settingslib.core.AbstractPreferenceController;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.wifi.AccessPoint;

import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.mediatek.provider.MtkSettingsExt;

public class OP01WifiPreferenceController extends AbstractPreferenceController implements LifecycleObserver, OnResume, OnPause{

    private Context mContext;
    private Context mOP01Context;

    private ListPreference mConnectTypePref;
    private ListPreference mConnectReminderPref;
    private SwitchPreference mPriorityTypePref;
    private Preference mPrioritySettingPref;

    private Preference mGatewayPref;
    private Preference mNetmaskPref;

    private static final int WIFI_CONNECT_REMINDER_ALWAYS = 0;
    private static final String KEY_CONNECT_TYPE = "connect_type";
    private static final String KEY_CONNECT_REMINDER = "connect_reminder";
    private static final String KEY_PRIORITY_TYPE = "priority_type";
    private static final String KEY_PRIORITY_SETTINGS = "priority_settings";
    private static final String KEY_SELECT_SSID_TYPE = "select_ssid_type";

    private String TAG = "OP01WifiPreferenceController";
    private final IntentFilter mFilter;
    private WifiManager mWifiManager;
    private ContentResolver mContentResolver;

    public OP01WifiPreferenceController(Context context, Context op01Context, Lifecycle lifecycle) {
        super(context);
        mContext = context;
        mOP01Context = op01Context;
        mFilter = new IntentFilter();
        mFilter.addAction(WifiManager.LINK_CONFIGURATION_CHANGED_ACTION);
        mFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        mWifiManager = ((WifiManager) mContext.getSystemService(Context.WIFI_SERVICE));
        mContentResolver = context.getContentResolver();
        lifecycle.addObserver(this);
    }

    /**
     * Displays preference in this controller.
     */
    public void displayPreference(PreferenceScreen screen) {
        Log.i(TAG, "displayPreference");
        initConnectView(screen);
    }

    @Override
    public String getPreferenceKey() {
        return null;
    }

    @Override
    public boolean isAvailable() {
        return false;
    }

    public void initConnectView(PreferenceScreen screen) {
        Log.i(TAG, "initConnectView");
        mConnectTypePref = new ListPreference(screen.getPreferenceManager()
                .getContext());
        mConnectTypePref.setTitle(mOP01Context
                .getString(R.string.wifi_connect_type_title));
        mConnectTypePref.setDialogTitle(mOP01Context
                .getString(R.string.wifi_connect_type_title));
        mConnectTypePref.setEntries(mOP01Context.getResources().getTextArray(
                R.array.wifi_connect_type_entries));
        mConnectTypePref.setEntryValues(mOP01Context.getResources()
                .getTextArray(R.array.wifi_connect_type_values));
        mConnectTypePref.setKey(KEY_CONNECT_TYPE);
        mConnectTypePref
                .setOnPreferenceChangeListener(mPreferenceChangeListener);
        screen.addPreference(mConnectTypePref);

        mConnectReminderPref = new ListPreference(screen.getPreferenceManager()
                .getContext());
        mConnectReminderPref.setTitle(mOP01Context
                .getString(R.string.wifi_reminder_frequency_title));
        mConnectReminderPref.setDialogTitle(mOP01Context
                .getString(R.string.wifi_reminder_frequency_title));
        mConnectReminderPref.setEntries(mOP01Context.getResources()
                .getTextArray(R.array.wifi_reminder_entries));
        mConnectReminderPref.setEntryValues(mOP01Context.getResources()
                .getTextArray(R.array.wifi_reminder_values));
        mConnectReminderPref.setKey(KEY_CONNECT_REMINDER);
        mConnectReminderPref
                .setOnPreferenceChangeListener(mPreferenceChangeListener);
        screen.addPreference(mConnectReminderPref);

        mPriorityTypePref = new SwitchPreference(screen.getPreferenceManager()
                .getContext());
        mPriorityTypePref.setTitle(mOP01Context.getResources().getString(
                R.string.wifi_priority_type_title));
        mPriorityTypePref.setSummary(mOP01Context.getResources().getString(
                R.string.wifi_priority_type_summary));
        mPriorityTypePref.setKey(KEY_PRIORITY_TYPE);
        mPriorityTypePref
                .setOnPreferenceChangeListener(mPreferenceChangeListener);
        screen.addPreference(mPriorityTypePref);

        mPrioritySettingPref = new Preference(screen.getPreferenceManager()
                .getContext());
        mPrioritySettingPref.setTitle(mOP01Context.getResources().getString(
                R.string.wifi_priority_settings_title));
        mPrioritySettingPref.setSummary(mOP01Context.getResources().getString(
                R.string.wifi_priority_settings_summary));
        mPrioritySettingPref.setKey(KEY_PRIORITY_SETTINGS);
        mPrioritySettingPref
                .setOnPreferenceClickListener(mPreferenceclickListener);
        screen.addPreference(mPrioritySettingPref);
        mPrioritySettingPref.setDependency(KEY_PRIORITY_TYPE);

        mGatewayPref = new Preference(screen.getPreferenceManager()
                .getContext());
        mGatewayPref.setTitle(mOP01Context.getString(R.string.wifi_gateway));
        screen.addPreference(mGatewayPref);

        mNetmaskPref = new Preference(screen.getPreferenceManager()
                .getContext());
        mNetmaskPref.setTitle(mOP01Context
                .getString(R.string.wifi_network_netmask));
        screen.addPreference(mNetmaskPref);
    }

    private Preference.OnPreferenceClickListener mPreferenceclickListener = new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            Intent intent = new Intent("com.mediatek.OP01.PRIORITY_SETTINGS");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            try {
                mOP01Context.startActivity(intent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(mOP01Context, R.string.wifi_priority_missing_app,
                        Toast.LENGTH_SHORT).show();
            }
            return true;
        }
    };
    private OnPreferenceChangeListener mPreferenceChangeListener = new OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            String key = preference.getKey();
            Log.d("@M_" + TAG, "key=" + key);

            if (KEY_CONNECT_TYPE.equals(key)) {
                Log.d("@M_" + TAG, "Wifi connect type is " + newValue);
                try {
                    System.putInt(mOP01Context.getContentResolver(),
                            MtkSettingsExt.System.WIFI_CONNECT_TYPE, Integer
                                    .parseInt(((String) newValue)));
                    if (mConnectTypePref != null) {
                        CharSequence[] array = mOP01Context
                                .getResources()
                                .getTextArray(R.array.wifi_connect_type_entries);
                        mConnectTypePref.setSummary((String) array[Integer
                                .parseInt(((String) newValue))]);
                    }
                } catch (NumberFormatException e) {
                    Log.d("@M_" + TAG, "set Wifi connect type error ");
                    return false;
                }
                try {
                    System.putInt(mOP01Context.getContentResolver(),
                            MtkSettingsExt.System.WIFI_SELECT_SSID_TYPE, Integer
                                    .parseInt(((String) newValue)));
                } catch (NumberFormatException e) {
                    Log.d("@M_" + TAG, "set Wifi SSID reselect type error ");
                    return false;
                }
            } else if (KEY_PRIORITY_TYPE.equals(key)) {
                boolean checked = ((Boolean) newValue).booleanValue();
                System.putInt(mOP01Context.getContentResolver(),
                        MtkSettingsExt.System.WIFI_PRIORITY_TYPE,
                        checked ? MtkSettingsExt.System.WIFI_PRIORITY_TYPE_MAMUAL
                                : MtkSettingsExt.System.WIFI_PRIORITY_TYPE_DEFAULT);
            } else if (KEY_CONNECT_REMINDER.equals(key)) {
                try {
                    System.putInt(mOP01Context.getContentResolver(),
                            MtkSettingsExt.System.WIFI_CONNECT_REMINDER, Integer
                                    .parseInt(((String) newValue)));
                    if (mConnectReminderPref != null) {
                        CharSequence[] array = mOP01Context.getResources()
                                .getTextArray(R.array.wifi_reminder_entries);
                        mConnectReminderPref.setSummary((String) array[Integer
                                .parseInt(((String) newValue))]);
                    }
                } catch (NumberFormatException e) {
                    Log.d("@M_" + TAG, "set Wifi connect type error ");
                    return false;
                }
            }
            return true;
        }
    };
    
    @Override
    public void onResume() {
        Log.i(TAG, "onResume");
        mContext.registerReceiver(mReceiver, mFilter);
        updateWifiInfo();
        updateConnectViewValue();
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause");
        mContext.unregisterReceiver(mReceiver);
    }
    
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(WifiManager.LINK_CONFIGURATION_CHANGED_ACTION) ||
                    action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                updateWifiInfo();
            }
        }
    };
    
    private void updateWifiInfo() {
        WifiInfo wifiInfo = mWifiManager.getConnectionInfo();

        String gateway = null;
        String netmask = null;
        DhcpInfo dhcpInfo = mWifiManager.getDhcpInfo();
        Log.d("@M_" + TAG, "refreshNetworkInfoView() dhcpInfo = " + dhcpInfo);
        Log.d("@M_" + TAG, "refreshNetworkInfoView() wifiInfo = " + wifiInfo);
        if (wifiInfo != null) {
            if (dhcpInfo != null) {
                int netmaskInt = 0;
                gateway = ipTransfer(dhcpInfo.gateway);
                Log.d("@M_" + TAG, "refreshNetworkInfoView() dhcpInfo.netmask = "
                    + dhcpInfo.netmask);
                ConnectivityManager cm =
                    (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                LinkProperties prop = cm.getLinkProperties(ConnectivityManager.TYPE_WIFI);
                if (prop != null) {
                    Log.d("@M_" + TAG, "prop not null");
                    Collection<LinkAddress> linkAddresses = prop.getLinkAddresses();
                    for (LinkAddress addr : linkAddresses) {
                        if (addr.getAddress() instanceof Inet4Address) {
                            netmaskInt = NetworkUtils.prefixLengthToNetmaskInt(
                                addr.getNetworkPrefixLength());
                        }
                    }
                    netmask = ipTransfer(netmaskInt);
                }
            }
        }
        String defaultText = mOP01Context.getString(R.string.status_unavailable);
        mGatewayPref.setSummary(gateway == null ? defaultText : gateway);
        mNetmaskPref.setSummary(netmask == null ? defaultText : netmask);
    }

    private String ipTransfer(int value) {
        String result = null;
        if (value != 0) {
            if (value < 0) {
                value += 0x100000000L;
            }
            result = String.format("%d.%d.%d.%d",
                    value & 0xFF, (value >> 8) & 0xFF, (value >> 16) & 0xFF, (value >> 24) & 0xFF);
        }
        return result;
    }
    
    private void updateConnectViewValue(){
        Log.i(TAG, "updateConnectViewValue");
        if (mConnectTypePref != null) {
            int value = System.getInt(mContentResolver,
                    MtkSettingsExt.System.WIFI_CONNECT_TYPE,
                MtkSettingsExt.System.WIFI_CONNECT_TYPE_AUTO);
            mConnectTypePref.setValue(String.valueOf(value));
            CharSequence[] array =
                mOP01Context.getResources().getTextArray(R.array.wifi_connect_type_entries);
            mConnectTypePref.setSummary((String) array[value]);

            int value1 = System.getInt(mContentResolver,
                    MtkSettingsExt.System.WIFI_SELECT_SSID_TYPE,
                MtkSettingsExt.System.WIFI_SELECT_SSID_AUTO);
            if (value != value1) {
                System.putInt(mContentResolver, MtkSettingsExt.System.WIFI_SELECT_SSID_TYPE, value);
            }
        }
        if (mPriorityTypePref != null) {
            mPriorityTypePref.setChecked(System.getInt(mContentResolver,
                    MtkSettingsExt.System.WIFI_PRIORITY_TYPE,
                    MtkSettingsExt.System.WIFI_PRIORITY_TYPE_DEFAULT) == MtkSettingsExt.System.WIFI_PRIORITY_TYPE_MAMUAL);
        }

        if (mConnectReminderPref != null) {
            int value = System.getInt(mContentResolver,
                    MtkSettingsExt.System.WIFI_CONNECT_REMINDER,
                WIFI_CONNECT_REMINDER_ALWAYS);
            mConnectReminderPref.setValue(String.valueOf(value));
            CharSequence[] array =
                mOP01Context.getResources().getTextArray(R.array.wifi_reminder_entries);
            mConnectReminderPref.setSummary((String) array[value]);
        }
    }

}
