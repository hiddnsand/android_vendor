/*
 * Copyright (C) 2016 MediaTek Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See http://www.gnu.org/licenses/gpl-2.0.html for more details.
 */

#ifndef __TLC_TPLAY_LOG_H__
#define __TLC_TPLAY_LOG_H__

#include <android/log.h>

#define LOG_TAG "[Crypto CA] "

/* Macro definitions */
#define LOG_D(...) __android_log_print(ANDROID_LOG_DEBUG , LOG_TAG, __VA_ARGS__)
#define LOG_I(...) __android_log_print(ANDROID_LOG_INFO  , LOG_TAG, __VA_ARGS__)
#define LOG_W(...) __android_log_print(ANDROID_LOG_WARN  , LOG_TAG, __VA_ARGS__)
#define LOG_E(...) __android_log_print(ANDROID_LOG_ERROR , LOG_TAG, __VA_ARGS__)


/* Log print */
#define MULTI_LOG_I(fmt, args...) \
do { \
	LOG_I(fmt, ##args); \
	fprintf(stderr, "%s" fmt, LOG_TAG, ##args); \
} while(0)

#define MULTI_LOG_E(fmt, args...) \
do { \
	LOG_E(fmt, ##args); \
	fprintf(stderr, "%s" fmt, LOG_TAG, ##args); \
} while(0)

#endif // __TLC_TPLAY_LOG_H__
