/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.deviceregister.utils;

import android.app.AlarmManager;
import android.app.AlarmManager.OnAlarmListener;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.telephony.PhoneStateListener;
import android.telephony.SmsMessage;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.mediatek.custom.CustomProperties;
import com.mediatek.deviceregister.Const;
import com.mediatek.deviceregister.RegisterMessage;
import com.mediatek.internal.telephony.devreg.DeviceRegisterController;
import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PlatformManager {

    private static final String TAG = Const.TAG_PREFIX + "PlatformManager";

    private static final String VALUE_DEFAULT_MANUFACTURER = "MTK";
    private static final String VALUE_DEFALUT_SOFTWARE_VERSION = "N0.P1";

    private static final String SERVER_ADDRESS = "10659401";
    private static final short PORT = 0;

    private TelephonyWrapper mTelephonyWrapper;

    public PlatformManager(Context context) {
        mTelephonyWrapper = new TelephonyWrapper(context);
    }

    public static boolean isFeatureSupported() {
        int property = SystemProperties.getInt("ro.mtk_devreg_app", -1);
        if (property != 1) {
            Log.i(TAG, "[isFeatureSupported] not support");
            return false;
        }
        return true;
    }

    public static String encryptMessage(String text) {
        String result = text;

         if (Build.TYPE.equals(Build.USER) && text != null && text.length() > 0) {
            try {
                MessageDigest digest = MessageDigest.getInstance("MD5");
                digest.update(text.getBytes());
                result = Utils.bytesToHexString(digest.digest());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static void setExactAlarm(Context context, OnAlarmListener listener, int second) {
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        long triggerAtMillis = SystemClock.elapsedRealtime() + second * 1000;
        alarm.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerAtMillis, TAG, listener, null);
    }

    public String getProcessedImsi(String imsi) {
        if (!isInfoValid(imsi, TelephonyWrapper.LENGTH_IMSI)) {
            imsi = TelephonyWrapper.VALUE_DEFAULT_IMSI;
        }
        return imsi;
    }

    /*
     * Methods related to subId
     */
    public static int[] getSubId(int slotId) {
        return SubscriptionManager.getSubId(slotId);
    }

    public static boolean isSubIdsValid(int[] subId) {
        if (subId == null || subId[0] < 0) {
            if (subId == null) {
                Log.e(TAG, "subId is " + subId);
            } else {
                Log.e(TAG, "subId not valid, subId[0] is " + subId[0]);
            }
            return false;
        }
        return true;
    }

    /*
     * Methods related to info check, compare
     */
    public static boolean isInfoValid(String info, int length) {
        return (info != null && info.length() == length);
    }

    public int getDefaultSim() {
        int subId = SubscriptionManager.getDefaultDataSubscriptionId();
        int slotId = SubscriptionManager.getSlotIndex(subId);
        Log.i(TAG, "getDefaultSim " + slotId);
        return slotId;
    }

    //------------------------------------------------------
    // Wrapper for Telephony
    //------------------------------------------------------
    public boolean areSlotsInfoReady(int[] slots) {
        return mTelephonyWrapper.areSlotsInfoReady(slots);
    }

    public String getCdmaImsiForCT(int slotId) {
        return mTelephonyWrapper.getCDMAImsi(slotId);
    }

    public String getDeviceMeid(int[] slots) {
        return mTelephonyWrapper.getDeviceMeid(slots);
    }

    public String getImei(int slotId) {
        return mTelephonyWrapper.getImei(slotId);
    }

    public String getImsiInfo(int slotId) {
        return mTelephonyWrapper.getImsi(slotId);
    }

    public int getSimSwitchSlot() {
        return mTelephonyWrapper.getSimSwitchSlot();
    }

    public boolean isNetworkRoaming(int slotId) {
        int[] subId = getSubId(slotId);

        if (isSubIdsValid(subId)) {
            return mTelephonyWrapper.isNetworkRoaming(subId[0]);
        }
        return false;
    }

    public boolean isSingleLoad() {
        return mTelephonyWrapper.isSingleLoad();
    }

    public boolean isValidUim(int subId) {
        return mTelephonyWrapper.isValidUim(subId);
    }

    public boolean supportCTForAllSlots() {
        return mTelephonyWrapper.supportCTForAllSlots();
    }

    public void registerPhoneListener(PhoneStateListener listener) {
        if (listener != null) {
            mTelephonyWrapper.registerPhoneListener(listener);
        } else {
            Log.i(TAG, "[register] listener is null, do nothing");
        }
    }

    public void unRegisterPhoneListener(PhoneStateListener listener) {
        if (listener != null) {
            mTelephonyWrapper.unRegisterPhoneListener(listener);
        } else {
            Log.i(TAG, "[unRegister] listener is null, do nothing");
        }
    }

    //------------------------------------------------------
    // Methods related to CustomProperties
    //------------------------------------------------------
    public static String getManufacturer() {
        String manufacturer = CustomProperties.getString(CustomProperties.MODULE_DM,
                CustomProperties.MANUFACTURER, VALUE_DEFAULT_MANUFACTURER);
        return manufacturer;
    }

    public static String getSoftwareVersion() {
        String version = CustomProperties.getString(CustomProperties.MODULE_DM,
                "SoftwareVersion", VALUE_DEFALUT_SOFTWARE_VERSION);
        return version;
    }

    /*
     * Wrapper class to send message and check register result
     */
    public static class SmsWrapper {

        private static final byte COMMAND_TYPE_RECEIVED = RegisterMessage.COMMAND_TYPE_RECEIVED;

        public static void sendRegisterMessage(DeviceRegisterController constroller,
                byte[] message, PendingIntent intent, int slotId) {
            Log.i(TAG, "sendRegisterMessage length " + message.length + " from slot " + slotId);

            int[] subId = getSubId(slotId);
            if (isSubIdsValid(subId)) {
                // Don't use interface of SmsManager which adds a header, as operator won't send
                // the feedback if receiving a message with header.
                constroller.sendDataSms(subId[0],
                        SERVER_ADDRESS, null, PORT, PORT, message, intent, null);
            }
        }

        /*
         * get data from intent, and analyze it to check if register is successful.
         */
        public static boolean checkRegisterResult(byte[] pduByte) {
            if (pduByte != null && pduByte.length > 0) {
                SmsMessage message = SmsMessage.createFromPdu(pduByte, SmsMessage.FORMAT_3GPP2);
                String originatingAddress = message.getOriginatingAddress();

                if (originatingAddress.equals(SERVER_ADDRESS)) {
                    byte[] data = message.getUserData();
                    Log.i(TAG, "message user data:" + Utils.bytesToHexString(data));

                    if (data != null && data.length > 1) {
                        byte confirmByte = data[1];
                        if (confirmByte == COMMAND_TYPE_RECEIVED) {
                            Log.i(TAG, "Register success!");
                            return true;
                        }

                    } else {
                        Log.i(TAG, "[checkRegisterResult] Invalid data");
                    }

                } else {
                    Log.i(TAG, "[checkRegisterResult] Invalid address " + originatingAddress);
                }

            } else {
                Log.i(TAG, "[checkRegisterResult] Pdu is valid!");
            }
            return false;
        }
    }

    private class TelephonyWrapper {

        private static final String TAG = Const.TAG_PREFIX + "TelephonyWrapper";

        private static final String VALUE_DEFAULT_MEID = "A0000100002000";
        private static final String VALUE_DEFAULT_IMEI = "860001000020000";
        public static final String VALUE_DEFAULT_IMSI = "000001000030000";

        private static final int LENGTH_MEID = Const.LENGTH_MEID;
        private static final int LENGTH_IMEI = 15;
        public static final int LENGTH_IMSI = 15;
        private static final int LENGTH_OPERATOR = 5;

        private static final String OPERATOR_CT_4G = "46011";
        private static final String OPERATOR_CT_MAC = "45502";
        private static final String OPERATOR_CT = "46003";

        private TelephonyManager mTelephonyManager;
        private MtkTelephonyManagerEx mMtkTelephonyManagerEx;

        public TelephonyWrapper(Context context) {
            mTelephonyManager = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            mMtkTelephonyManagerEx = new MtkTelephonyManagerEx(context);
            if (mTelephonyManager == null) {
                throw new Error("telephony manager is null");
            }
        }

        // ------------------------------------------------------
        //    Wrapper for SystemProperties
        // ------------------------------------------------------

        /*
         * if OP09_SPEC0212_SEGDEFAULT, CT card could only in slot0; else, support all slots.
         */
        public boolean supportCTForAllSlots() {
            String optr_spec_seg = SystemProperties.get("persist.operator.optr", "") + "_"
                    + SystemProperties.get("persist.operator.spec", "") + "_"
                    + SystemProperties.get("persist.operator.seg", "");

            if (optr_spec_seg.equals("OP09_SPEC0212_SEGDEFAULT")) {
                // A lab (not support)
                return false;
            } else {
                return true;
            }
        }

        /*
         * Get the slot which has 3/4G capability and map to (0, 1)
         */
        public int getSimSwitchSlot() {
            // Get the slot which has 3/4G capability (1 or 2)
            int value = SystemProperties.getInt("persist.radio.simswitch", -1) - 1;
            Log.i(TAG, "3/4G is on slot " + value);
            if (value == Const.SLOT_ID_0 || value == Const.SLOT_ID_1) {
                return value;
            } else {
                return Const.SLOT_ID_INVALID;
            }
        }

        // ------------------------------------------------------
        //    Wrapper for TelephonyManager
        // ------------------------------------------------------
        public boolean isSingleLoad() {
            return (mTelephonyManager.getSimCount() == 1);
        }

        public boolean isNetworkRoaming(int subId) {
            return mTelephonyManager.isNetworkRoaming(subId);
        }

        public String getDeviceMeid(int[] slots) {
            for (int slotId: slots) {
                String result = mTelephonyManager.getMeid(slotId);
                if (isInfoValid(result, LENGTH_MEID)) {
                    return result;
                }
            }
            return VALUE_DEFAULT_MEID;
        }

        public String getNetworkOperator(int subId) {
            return mTelephonyManager.getNetworkOperator(subId);
        }

        public int getCurrentPhoneType(int subId) {
            return mTelephonyManager.getCurrentPhoneType(subId);
        }

        public String getImei(int slotId) {
            String result = mTelephonyManager.getImei(slotId);
            if (isInfoValid(result, LENGTH_IMEI)) {
                return result;
            }
            return VALUE_DEFAULT_IMEI;
        }

        public String getSimOperator(int subId) {
            return mTelephonyManager.getSimOperator(subId);
        }

        public String getSubscriberId(int subId) {
            return mTelephonyManager.getSubscriberId(subId);
        }

        public void registerPhoneListener(PhoneStateListener listener) {
            mTelephonyManager.listen(listener, PhoneStateListener.LISTEN_NONE);
            mTelephonyManager.listen(listener, PhoneStateListener.LISTEN_SERVICE_STATE);
        }

        public void unRegisterPhoneListener(PhoneStateListener listener) {
            mTelephonyManager.listen(listener, PhoneStateListener.LISTEN_NONE);
        }

        // ------------------------------------------------------
        //    Wrapper for MtkTelephonyManagerEx
        // ------------------------------------------------------

        public int getIccAppFamily(int slotId) {
            int iccType = mMtkTelephonyManagerEx.getIccAppFamily(slotId);
            Log.i(TAG, "Slot " + slotId + " iccType is : " + iccType);
            return iccType;
        }

        public String getUimSubscriberId(int subId) {
            return mMtkTelephonyManagerEx.getUimSubscriberId(subId);
        }

        // ------------------------------------------------------
        //    IMSI related
        // ------------------------------------------------------

        /**
         * if 3G CT, CDMA IMSI; else, IMSI
         */
        public String getImsi(int slotId) {
            String result = VALUE_DEFAULT_IMSI;
            int[] subId = getSubId(slotId);

            if (isSubIdsValid(subId)) {
                if (isCdma3GCard(slotId)) {
                    result = getUimSubscriberId(subId[0]);
                } else {
                    result = getSubscriberId(subId[0]);
                }
            }

            if (isInfoValid(result, LENGTH_IMSI)) {
                return result;
            }
            return VALUE_DEFAULT_IMSI;
        }

        public boolean isCdma3GCard(int slotId) {
            return getIccAppFamily(slotId) == MtkTelephonyManagerEx.APP_FAM_3GPP2;
        }

        public String getCDMAImsi(int slotId) {
            String result = VALUE_DEFAULT_IMSI;;
            int[] subId = getSubId(slotId);

            if (isSubIdsValid(subId)) {
                result = getUimSubscriberId(subId[0]);
            }

            if (isInfoValid(result, LENGTH_IMSI)) {
                return result;
            }
            return VALUE_DEFAULT_IMSI;
        }

        // ------------------------------------------------------
        //    check SIM state
        // ------------------------------------------------------
        public boolean areSlotsInfoReady(int[] slots) {
            for (int i = 0; i < slots.length; ++i) {
                if (!isSlotInfoReady(slots[i])) {
                    return false;
                }
            }
            return true;
        }

        private boolean isSlotInfoReady(int slotId) {
            int[] subId = getSubId(slotId);

            if (isSubIdsValid(subId)) {
                String networkOperator = getNetworkOperator(subId[0]);
                String simOperator = getSimOperator(subId[0]);

                if (isInfoValid(networkOperator, LENGTH_OPERATOR)
                        && isInfoValid(simOperator, LENGTH_OPERATOR)) {
                    return true;
                } else {
                    Log.i(TAG, "networkOptr/simOptr not all ready");
                }
            } else {
                // no valid sub, return true directly
                return true;
            }
            return false;
        }

        /**
         * Whether uim's network operator, UIM operator and phone type is correct
         *
         * @param slotId
         * @return true or false
         */
        public boolean isValidUim(int slotId) {

            int[] subId = getSubId(slotId);
            if (!isSubIdsValid(subId)) {
                return false;
            }

            int phoneType = getCurrentPhoneType(subId[0]);
            Log.i(TAG, "[isValidUim] slot " + slotId + " phone type " + phoneType);

            if (TelephonyManager.PHONE_TYPE_CDMA == phoneType) {
                String networkOperator = getNetworkOperator(subId[0]);
                Log.i(TAG, "[isValidUim] slot " + slotId + " network operator " + networkOperator);

                if (OPERATOR_CT.equals(networkOperator)
                        || OPERATOR_CT_MAC.equals(networkOperator)
                        || OPERATOR_CT_4G.equals(networkOperator)) {

                    String simOperator = getSimOperator(subId[0]);
                    Log.i(TAG, "[isValidUim] slot " + slotId + " sim operator " + simOperator);

                    if (simOperator.equals("46003") || simOperator.equals("46011")
                            || simOperator.equals("20404") || simOperator.equals("45403")
                            || simOperator.equals("45431")) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /*
     * Reserved interface for debug
     */
    public static class DebugWrapper {

        /*
         * Convert MEID to pESN and write to UIM, then read
         */
        public static void testUimRW(int uimSlot) {
            String esnOrMeid = "A0000038A6AA69";
            EsnWrapper.setEsnToUim(uimSlot, esnOrMeid);
            Log.i(TAG, "Write " + esnOrMeid + " to slot " + uimSlot);

            Log.i(TAG, "MEID from uim is " + EsnWrapper.getEsnOrMeidFromUim(uimSlot));
        }
    }
}
