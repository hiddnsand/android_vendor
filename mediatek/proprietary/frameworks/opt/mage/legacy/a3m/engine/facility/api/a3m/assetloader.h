/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * AssetLoader class
 *
 */
#pragma once
#ifndef A3M_ASSETLOADER_H
#define A3M_ASSETLOADER_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/pointer.h>          /* SharedPtr etc.   */
#include <a3m/noncopyable.h>      /* NonCopyable      */

namespace a3m
{
  /** \defgroup a3mAssetloader AssetLoader
   * \ingroup  a3mRefAssets
   *
   * The asset loader is a base class for the loading of assets
   * from the file system.  Typically, a loader object gets registered
   * with an associated AssetCache for loading of new assets.
   *
   * A number of default asset loaders exist within A3M (e.g. regular
   * Texture2D, PVR Texture2D, TextureCube, ShaderProgram).  These are
   * registered automatically with their associated AssetCache objects by
   * AssetCachePool upon construction.  Therefore it is not necessary for the
   * client to explicitly register loaders when using AssetCachePool for loading
   * assets.
   *
   * The default asset loaders are not part of the public API of A3M.
   *
   * \code
   * using namespace a3m;
   *
   * // Instantiate the AssetCachePool.  This automatically registers default
   * // loaders for each asset and there's no need for the client to register
   * // loaders (provided the default loaders are sufficient), as well as
   * // creating a ResourceCache for tracking of allocated resources.
   * AssetCachePool::Ptr pool(new AssetCachePool());
   *
   * // Set the search path where assets should be read from.  This call will
   * // register the path with all caches owned by the pool.
   * registerSource(*pool, ".");
   *
   * // Load an asset using the appropriate cache
   * Texture2D::Ptr texture = pool->texture2DCache()->get("elephant.png");
   *
   * // The client can still register a custom loader...
   * CustomTextureLoader::Ptr customLoader(new CustomTextureLoader());
   * pool->texture2DCache()->registerLoader(customLoader);
   *
   * Texture2D::Ptr texture2 = pool->texture2DCache()->get("giraffe.custom");
   * // If the default loader can't load "giraffe.custom", the cache will
   * // try to load it with customLoader
   *
   * \endcode
   *
   * @{
   */

  /**
   * AssetLoader class.
   * Reads binary data from the filesystem and returns a new asset.
   */
  template <typename T>
  class AssetLoader : public Shared, NonCopyable
  {
  public:
    A3M_NAME_SHARED_CLASS( AssetLoader )

    /** Smart pointer type for this class */
    typedef SharedPtr< AssetLoader<T> > Ptr;

    /** Destructor
     */
    virtual ~AssetLoader() {};

    /** Loads a new asset by name, and stores it in a cache.
     * If the asset cannot be found, a null pointer will be returned.
     *
     * \return Smart pointer to a new asset, or null
     */
    virtual SharedPtr<T> load(
      typename T::CacheType& cache, /**< Cache to use to create the asset */
      A3M_CHAR8 const* name /**< Name of the asset to load */) = 0;

    /** Checks whether the given asset name indicates that it is supported.
     * This function will look at an asset's name, and check for known patterns
     * such as file extensions, to see whether the loader is likely to be able
     * to load the asset.
     *
     * \return TRUE if the loader thinks it can load the asset
     */
    virtual A3M_BOOL isKnown(
      A3M_CHAR8 const* name /**< Name of the asset */) = 0;
  };

  /** @} */

} /* namespace a3m */

#endif /* A3M_ASSETLOADER_H */
