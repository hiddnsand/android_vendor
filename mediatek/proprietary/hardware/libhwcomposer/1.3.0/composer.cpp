#define DEBUG_LOG_TAG "COMP"

#include <hardware/hwcomposer.h>
#include "gralloc_mtk_defs.h"
#include <hardware/gralloc.h>

#include "utils/debug.h"
#include "utils/tools.h"

#include "composer.h"
#include "display.h"
#include "overlay.h"
#include "dispatcher.h"
#include "worker.h"
#include "sync.h"
#include "hwc.h"
#include "hwdev.h"
#include "platform.h"
#include "hwc_priv.h"

#include <sync/sync.h>

#include <ui/GraphicBuffer.h>

#define CLOGD(i, x, ...) HWC_LOGD("(%d:%d) " x, m_disp_id, i, ##__VA_ARGS__)
#define CLOGI(i, x, ...) HWC_LOGI("(%d:%d) " x, m_disp_id, i, ##__VA_ARGS__)
#define CLOGW(i, x, ...) HWC_LOGW("(%d:%d) " x, m_disp_id, i, ##__VA_ARGS__)
#define CLOGE(i, x, ...) HWC_LOGE("(%d:%d) " x, m_disp_id, i, ##__VA_ARGS__)

// ---------------------------------------------------------------------------

inline void computeBufferCrop(
    Rect& src_crop, Rect& dst_crop,
    int disp_width, int disp_height)
{
    if (dst_crop.right > disp_width)
    {
        int diff = dst_crop.right - disp_width;
        dst_crop.right = disp_width;
        src_crop.right -= diff;
    }

    if (dst_crop.bottom > disp_height)
    {
        int diff = dst_crop.bottom - disp_height;
        dst_crop.bottom = disp_height;
        src_crop.bottom -= diff;
    }

    if (dst_crop.left >= 0 && dst_crop.top >=0)
        return;

    if (dst_crop.left < 0)
    {
        src_crop.left -= dst_crop.left;
        dst_crop.left = 0;
    }

    if (dst_crop.top < 0)
    {
        src_crop.top -= dst_crop.top;
        dst_crop.top = 0;
    }
}

// ---------------------------------------------------------------------------

LayerHandler::LayerHandler(int dpy, sp<OverlayEngine> ovl_engine)
    : m_disp_id(dpy)
    , m_ovl_engine(ovl_engine)
    , m_sync_fence(new SyncFence(dpy))
{
    m_disp_data = &DisplayManager::getInstance().m_data[dpy];
}

LayerHandler::~LayerHandler()
{
    m_ovl_engine = NULL;
    m_sync_fence = NULL;
}

// ---------------------------------------------------------------------------

ComposerHandler::ComposerHandler(int dpy, sp<OverlayEngine> ovl_engine)
    : LayerHandler(dpy, ovl_engine)
{ }

void ComposerHandler::set(
    struct hwc_display_contents_1* list,
    DispatcherJob* job)
{
    uint32_t total_num = job->num_layers;

    for (uint32_t i = 0; i < total_num; i++)
    {
        HWLayer* hw_layer = &job->hw_layers[i];

        // this layer is not enable
        if (!hw_layer->enable) continue;

        // skip mm layers
        if (HWC_LAYER_TYPE_MM == hw_layer->type) continue;

        hwc_layer_1_t* layer = &list->hwLayers[hw_layer->index];

        // check if buffer could be get from native handle
        status_t err = NO_ERROR;
        int fb_mva = 0;
        int ion_fd = DISP_NO_ION_FD;
        bool is_use_ion = true;

        if (HWC_LAYER_TYPE_FBT == hw_layer->type)
        {
            err = grallocExtraQuery(layer->handle, GRALLOC_EXTRA_GET_FB_MVA, &fb_mva);
            if (err != NO_ERROR)
            {
                CLOGW(i, "FBT might use ION (handle=%p)", layer->handle);
                fb_mva = 0;
            }
            else
            {
                is_use_ion = false;
            }
        }

        if (is_use_ion)
        {
            int fd = getIonFd(layer->handle);
            if (fd < 0)
            {
                CLOGE(i, "Failed to get ION fd !! (handle=%p) !!", layer->handle);

                // not compose this layer
                hw_layer->enable = false;
                continue;
            }

            ion_fd = fd;
        }

        // remember buffer info
        hw_layer->mva = fb_mva;
        hw_layer->ion_fd = ion_fd;
        getBufferInfo(layer->handle, &hw_layer->buffer_info);
        int is_need_flush =
            ((hw_layer->buffer_info.ext_info.status & GRALLOC_EXTRA_MASK_TYPE) != GRALLOC_EXTRA_BIT_TYPE_GPU);

        // get release fence from display driver
        {
            OverlayPrepareParam prepare_param;
            prepare_param.id            = i;
            prepare_param.ion_fd        = ion_fd;
            prepare_param.is_need_flush = is_need_flush;

            status_t err = m_ovl_engine->prepareInput(prepare_param);
            if (NO_ERROR != err)
            {
                prepare_param.fence_index = 0;
                prepare_param.fence_fd = -1;
            }
            hw_layer->fence_index = prepare_param.fence_index;

            if (prepare_param.fence_fd <= 0)
            {
                CLOGE(i, "Failed to get releaseFence !!");
            }
            layer->releaseFenceFd = prepare_param.fence_fd;
        }
        memcpy(&hw_layer->layer, layer, sizeof(hwc_layer_1_t));

        CLOGD(i, "releaseFence (fd=%d, idx=%d) acquireFence (fd=%d)"
            "(handle=%p, ion=%d) flush(%d)",
            layer->releaseFenceFd, hw_layer->fence_index,
            layer->acquireFenceFd, layer->handle, ion_fd, is_need_flush);

        layer->acquireFenceFd = -1;
    }
}

void ComposerHandler::process(DispatcherJob* job)
{
    uint32_t total_num = job->num_layers;
    uint32_t i = 0;

    OverlayPortParam* const* ovl_params = m_ovl_engine->getInputParams();

    // wait until each layer is ready, then set to overlay
    for (i = 0; i < total_num; i++)
    {
        HWLayer* hw_layer = &job->hw_layers[i];

        // this layer is not enable
        if (!hw_layer->enable) continue;

        // skip mm layers
        if (HWC_LAYER_TYPE_MM == hw_layer->type) continue;

        hwc_layer_1_t* layer = &hw_layer->layer;
        m_sync_fence->wait(layer->acquireFenceFd, 1000, DEBUG_LOG_TAG);

        m_ovl_engine->setInputDirect(i);
    }

    // fill overlay engine setting
    int param_count = 0;
    for (i = 0; i < total_num; i++)
    {
        HWLayer* hw_layer = &job->hw_layers[i];

        // this layer is not enable
        if (!hw_layer->enable)
        {
            ovl_params[i]->state    = OVL_IN_PARAM_DISABLE;
            ovl_params[i]->sequence = HWC_SEQUENCE_INVALID;
            continue;
        }

        // skip mm layers
        if (HWC_LAYER_TYPE_MM == hw_layer->type) continue;

        hwc_layer_1_t* layer = &hw_layer->layer;
        hwc_buffer_info_t* buffer_info = &hw_layer->buffer_info;

        // [NOTE]
        // Since OVL does not support float crop, adjust coordinate to interger
        // as what SurfaceFlinger did with hwc before version 1.2
        hwc_frect_t* src_cropf = &layer->sourceCropf;
        int l = (int)(ceilf(src_cropf->left));
        int t = (int)(ceilf(src_cropf->top));
        int r = (int)(floorf(src_cropf->right));
        int b = (int)(floorf(src_cropf->bottom));
        Rect src_crop(l, t, r, b);
        Rect dst_crop(*(Rect *)&(layer->displayFrame));
        computeBufferCrop(src_crop, dst_crop, m_disp_data->width, m_disp_data->height);

        OverlayPortParam* param = ovl_params[i];

        if (HWC_LAYER_TYPE_FBT == hw_layer->type && false == hw_layer->enable)
            param->state = OVL_IN_PARAM_IGNORE;
        else
            param->state = OVL_IN_PARAM_ENABLE;

        param->mva          = (void*)hw_layer->mva;
        param->pitch        = buffer_info->y_stride;
        param->format       = buffer_info->format;
        param->src_crop     = src_crop;
        param->dst_crop     = dst_crop;
        param->is_sharpen   = false;
        param->fence_index  = hw_layer->fence_index;
        // use hw layer type as identity
        param->identity     = hw_layer->type;
        param->protect      = (buffer_info->usage & GRALLOC_USAGE_PROTECTED);
        param->secure       = (1 == HWCMediator::getInstance().m_features.svp) &&
                              (buffer_info->usage & GRALLOC_USAGE_SECURE);
        param->alpha_enable = 1;
        param->alpha        = layer->planeAlpha;
        param->sequence     = job->sequence;
#ifdef MTK_HWC_PROFILING
        if (HWC_LAYER_TYPE_FBT == hw_layer->type)
        {
            param->fbt_input_layers = hw_layer->fbt_input_layers;
            param->fbt_input_bytes  = hw_layer->fbt_input_bytes;
        }
#endif
        param->ion_fd      = hw_layer->ion_fd;
    }
}

// ---------------------------------------------------------------------------

ExtComposerHandler::ExtComposerHandler(sp<OverlayEngine> ovl_engine)
    : LayerHandler(HWC_DISPLAY_EXTERNAL, ovl_engine)
{ }

void ExtComposerHandler::set(
    struct hwc_display_contents_1* list,
    DispatcherJob* job)
{
    int fbt_idx = job->num_ui_layers + job->num_mm_layers;
    if (!job->fbt_exist)
    {
        CLOGE(fbt_idx, "No FBT for external display");
        return;
    }

    HWLayer* hw_layer = &job->hw_layers[fbt_idx];
    hwc_layer_1_t* layer = &list->hwLayers[list->numHwLayers - 1];
    // NOTE: only primary display could use mva for FBT
    int fd = getIonFd(layer->handle);
    if (fd < 0)
    {
        CLOGE(fbt_idx, "Failed to get ION fd !! (handle=%p) !!", layer->handle);

        hw_layer->enable = false;
        return;
    }

    // remember buffer info
    status_t err = NO_ERROR;
    hw_layer->mva = 0;
    hw_layer->ion_fd = fd;
    getBufferInfo(layer->handle, &hw_layer->buffer_info);

    // get release fence from display driver
    {
        OverlayPrepareParam prepare_param;
        prepare_param.ion_fd        = hw_layer->ion_fd;
        prepare_param.is_need_flush = 0;

        if (FAKE_DISPLAY != m_disp_data->subtype)
        {
            err = ExternalStream::getInstance().prepareInput(prepare_param);
        }
        else
        {
            // TODO: should assign ovl id accoring number of fake display
            hw_layer->fake_ovl_id = m_ovl_engine->getAvailableInputNum();
            prepare_param.id = hw_layer->fake_ovl_id;

            CLOGI(fbt_idx, "Use overlay input(%d) for fake display", hw_layer->fake_ovl_id);

            err = m_ovl_engine->prepareInput(prepare_param);
        }

        if (NO_ERROR != err)
        {
            prepare_param.fence_index = 0;
            prepare_param.fence_fd = -1;
        }
        hw_layer->fence_index = prepare_param.fence_index;

        if (prepare_param.fence_fd <= 0)
        {
            CLOGE(fbt_idx, "Failed to get releaseFence !!");
        }
        layer->releaseFenceFd = prepare_param.fence_fd;
    }
    memcpy(&hw_layer->layer, layer, sizeof(hwc_layer_1_t));

    CLOGD(fbt_idx, "releaseFence (fd=%d, idx=%d) acquireFence (fd=%d) (handle=%p, ion=%d)",
        layer->releaseFenceFd, hw_layer->fence_index,
        layer->acquireFenceFd, layer->handle, hw_layer->ion_fd);

    layer->acquireFenceFd = -1;
}

void ExtComposerHandler::process(DispatcherJob* job)
{
    if (!job->fbt_exist)
        return;

    int fbt_idx = job->num_ui_layers + job->num_mm_layers;
    HWLayer* hw_layer = &job->hw_layers[fbt_idx];
    if (!hw_layer->enable)
        return;

    // fill setting for external display
    hwc_layer_1_t* layer = &hw_layer->layer;
    OverlayPortParam param;
    {
        hwc_buffer_info_t* buffer_info = &hw_layer->buffer_info;

        // [NOTE]
        // Since OVL does not support float crop, adjust coordinate to interger
        // as what SurfaceFlinger did with hwc before version 1.2
        hwc_frect_t* src_cropf = &layer->sourceCropf;
        int l = (int)(ceilf(src_cropf->left));
        int t = (int)(ceilf(src_cropf->top));
        int r = (int)(floorf(src_cropf->right));
        int b = (int)(floorf(src_cropf->bottom));
        Rect src_crop(l, t, r, b);
        Rect dst_crop(*(Rect *)&(layer->displayFrame));
        computeBufferCrop(src_crop, dst_crop, m_disp_data->width, m_disp_data->height);

        param.mva         = NULL;
        param.pitch       = buffer_info->y_stride;
        param.format      = buffer_info->format;
        param.src_crop    = src_crop;
        param.dst_crop    = dst_crop;
        param.is_sharpen  = false;
        param.fence_index = hw_layer->fence_index;
        param.identity    = hw_layer->type;
        param.protect     = (hw_layer->buffer_info.usage & GRALLOC_USAGE_PROTECTED);
        param.secure      = (hw_layer->buffer_info.usage & GRALLOC_USAGE_SECURE);
#ifdef MTK_HWC_PROFILING
        if (HWC_LAYER_TYPE_FBT == hw_layer->type)
        {
            param.fbt_input_layers = hw_layer->fbt_input_layers;
            param.fbt_input_bytes  = hw_layer->fbt_input_bytes;
        }
#endif
        param.ion_fd      = hw_layer->ion_fd;
    }

    // wait until fbt is ready, then set to external display
    {
        m_sync_fence->wait(layer->acquireFenceFd, 1000, DEBUG_LOG_TAG);

        if (FAKE_DISPLAY != m_disp_data->subtype)
        {
            ExternalStream::getInstance().setInputDirect(param);
        }
        else
        {
            CLOGE(fbt_idx, "Set overlay input(%d) for fake display", hw_layer->fake_ovl_id);
            m_ovl_engine->setInputDirect(hw_layer->fake_ovl_id, &param);
        }
    }
}
