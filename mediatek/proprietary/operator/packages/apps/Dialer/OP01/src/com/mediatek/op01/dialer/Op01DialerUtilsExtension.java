package com.mediatek.op01.dialer;

import com.mediatek.dialer.ext.DefaultDialerUtilsExtension;

public class Op01DialerUtilsExtension extends DefaultDialerUtilsExtension {
    private static final String TAG = "Op01DialerUtilsExtension";
    /**
     * for op01.
     * plug-in we do not use google default blocked number features
     * @return false in op01
     */
    @Override
    public boolean shouldUseBlockedNumberFeature() {
        return false;
    }
}