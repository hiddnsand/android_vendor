package com.mediatek.mms.plugin;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.google.android.mms.util.SqliteWrapper;
import com.mediatek.mms.ext.DefaultOpWappushMessagingNotificationExt;

import mediatek.telephony.MtkTelephony.WapPush;

/**
 * Op01WappushMessagingNotificationExt.
 *
 */
public class Op18WappushMessagingNotificationExt extends
        DefaultOpWappushMessagingNotificationExt {

    private static final String TAG = "Op18WappushMessagingNotificationExt";
    public static final String NOTIFICATION_RINGTONE_PREFERENCE_KEY = "pref_key_ringtone";
    public static final String DEFAULT_RINGTONE = "content://settings/system/notification_sound";

    /**
     * Construction.
     * @param context Context
     */
    public Op18WappushMessagingNotificationExt(Context context) {
        super(context);
    }

    /**
     * Retrun sim specific ringtone uri.
     * @param context Context
     * @param ringToneStr default ringtoneUri
     * @param threadId recent wappush threadid
     * @return ringTone
     */
    public String getWapPushSimRingtone(Context context, String ringToneStr, long threadId) {
        String[] projection = new String[] {
                                             WapPush.THREAD_ID,
                                             WapPush.SUBSCRIPTION_ID,
                                             WapPush.TEXT
                                            };
        String NEW_INCOMING_WAPPUSH_CONSTRAINT = "(" + WapPush.THREAD_ID + " = " + threadId + ")";

        int slotId = 0;
        int []subIdArray = null;
        int subId = 0;
        Cursor cursor = null;

        Log.d(TAG, "getWapPushSimRingtone for thread_id: " + threadId);
        ContentResolver resolver = context.getContentResolver();
        cursor = SqliteWrapper.query(context, resolver, WapPush.CONTENT_URI, projection,
                NEW_INCOMING_WAPPUSH_CONSTRAINT, null, WapPush.DATE + " desc");
        if (cursor == null || cursor.getCount() == 0) {
            Log.d(TAG, "getWapPushSimRingtone: cursor == null or count == 0");
            subIdArray = SubscriptionManager.getSubId(slotId);
            subId = subIdArray[0];
        } else {
            subId = getSubIdForIncomingPush(cursor);
        }
        Log.d(TAG, "subId: " + subId);
        String ringTone = getWapPushRingtone(context, subId);
        Log.d(TAG, "ringtone : " + ringTone);
        return ringTone;
    }

    private int getSubIdForIncomingPush(Cursor cursor) {
        long threadId;
        long subId = 0;
        String text;

        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    threadId = cursor.getLong(0);
                    subId = cursor.getLong(1);
                    text = cursor.getString(2);
                    Log.d(TAG, "threadId = " + threadId + ", subId = " + subId +
                            ",text = " + text);
                }
            } finally {
                cursor.close();
            }
        }
        return (int) subId;
    }

    private String getWapPushRingtone(Context context, int subId) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String ringtoneKey = Long.toString(subId) + "_" + NOTIFICATION_RINGTONE_PREFERENCE_KEY;
        String appRing = prefs.getString(ringtoneKey, DEFAULT_RINGTONE);
        Log.d(TAG, "getWapPushRingtone : " + appRing + " subId : " + subId);
        return appRing;
    }
}
