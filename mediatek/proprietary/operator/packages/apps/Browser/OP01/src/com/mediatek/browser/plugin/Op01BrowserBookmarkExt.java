package com.mediatek.browser.plugin;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.browser.provider.Browser;
import com.android.browser.provider.BrowserContract;
import com.android.browser.provider.BrowserContract.Bookmarks;
import com.android.browser.provider.BrowserContract.Images;
import com.mediatek.browser.ext.DefaultBrowserBookmarkExt;

import com.mediatek.browser.plugin.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Op01BrowserBookmarkExt extends DefaultBrowserBookmarkExt {

    private static final String TAG = "Op01BrowserBookmarkExt";

    private static final String TABLE_IMAGES = "images";
    private static final String EXTRA_EDIT_BOOKMARK = "bookmark";
    private static final String EXTRA_IS_FOLDER = "is_folder";
    private static final String EXTRA_CREATE_FOLDER = "create_folder";
    private static final long FIXED_ID_ROOT = 1;

    private Context mContext;

    public Op01BrowserBookmarkExt(Context context) {
        super();
        mContext = context;
    }

    public int addDefaultBookmarksForCustomer(SQLiteDatabase db) {
        TypedArray preloads = null;
        Log.i("@M_" + TAG, "Enter: " + "addDefaultBookmarksForCustomer" + " --OP01 implement" + db);
        Resources res = mContext.getResources();
        final CharSequence[] bookmarks = res.getTextArray(
                R.array.bookmarks_for_op01);
        preloads = mContext.getResources().obtainTypedArray(R.array.bookmark_preloads_for_op01);
        return addDefaultBookmarks(db, bookmarks, preloads);
    }

    public void createBookmarksPageOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.i("@M_" + TAG, "Enter: " + "createBookmarksPageOptionsMenu" + " --OP01 implement");
        menu.add(mContext.getResources().getString(R.string.add_new_bookmark));
        menu.add(mContext.getResources().getString(R.string.new_folder));
    }

    public boolean bookmarksPageOptionsMenuItemSelected(MenuItem item, Activity activity, long folderId) {
        Log.i("@M_" + TAG, "Enter: " + "bookmarksPageOptionsMenuItemSelected" + " --OP01 implement");
        if (item.getTitle().equals(mContext.getResources().getString(R.string.add_new_bookmark))) {
            String url = "http://";
            String title = "";
            Intent intent = new Intent(Intent.ACTION_INSERT, Browser.BOOKMARKS_URI);
            intent.putExtra(BrowserContract.Bookmarks.TITLE, title);
            intent.putExtra(BrowserContract.Bookmarks.PARENT, folderId);
            intent.putExtra(BrowserContract.Bookmarks.URL, url);
            Log.i("@M_" + TAG, "Add bookmark, CurrentFolderId: " + folderId);
            activity.startActivity(intent);
            return true;
        } else if (item.getTitle().equals(mContext.getResources().getString(R.string.new_folder))) {
            Intent intent = new Intent(Intent.ACTION_INSERT, Browser.BOOKMARKS_URI);
            Bundle bundle = new Bundle();
            bundle.putString(BrowserContract.Bookmarks.TITLE,
                    mContext.getResources().getString(R.string.new_folder));
            bundle.putLong(BrowserContract.Bookmarks._ID, -1);
            bundle.putLong(BrowserContract.Bookmarks.PARENT, folderId);
            bundle.putBoolean(EXTRA_CREATE_FOLDER, true);
            intent.putExtra(EXTRA_EDIT_BOOKMARK, bundle);
            intent.putExtra(EXTRA_IS_FOLDER, true);
            Log.i("@M_" + TAG, "Add bookmark folder, CurrentFolderId: " + folderId);
            activity.startActivity(intent);
            return true;
        } else {
            return false;
        }
    }

    public boolean customizeEditExistingFolderState(Bundle bundle, boolean state) {
        Log.i(TAG, "Enter: " + "customizeEditExistingFolderState" + " --OP01 implement");
        if (bundle != null
            && bundle.getBoolean(EXTRA_CREATE_FOLDER, false)) {
            return false;
        } else {
            return state;
        }
    }

    public String getCustomizedEditFolderFakeTitleString(Bundle bundle, String title) {
        Log.i(TAG, "Enter: " + "getCustomizedEditFolderFakeTitleString" + " --OP01 implement");
        if (bundle != null
            && bundle.getBoolean(EXTRA_CREATE_FOLDER, false)) {
            return mContext.getResources().getString(R.string.new_folder);
        } else {
            return title;
        }
    }

    public void showCustomizedEditFolderNewFolderView(
            View newFolder, View divider, Bundle bundle) {
        Log.i(TAG, "Enter: " + "showCustomizedEditFolderNewFolderView" + " --OP01 implement");
        if (bundle != null
            && bundle.getBoolean(EXTRA_CREATE_FOLDER, false)) {
            if (newFolder != null) {
                newFolder.setVisibility(View.GONE);
            }
            if (divider != null) {
                divider.setVisibility(View.GONE);
            }
        } else {
            if (newFolder != null) {
                newFolder.setVisibility(View.VISIBLE);
            }
            if (divider != null) {
                divider.setVisibility(View.VISIBLE);
            }
        }
    }

    public boolean shouldSetCustomizedEditFolderSelection(Bundle bundle, boolean setValue) {
        Log.i(TAG, "Enter: " + "shouldSetCustomizedEditFolderSelection" + " --OP01 implement");
        if (bundle != null
            && bundle.getBoolean(EXTRA_CREATE_FOLDER, false)) {
            return true;
        } else {
            return setValue;
        }
    }

    public Boolean saveCustomizedEditFolder(Context context, String title,
            long currentFolder, Bundle bundle, String errorMessage) {
        Log.i(TAG, "Enter: " + "saveCustomizedEditFolder" + " --OP01 implement");
        if (bundle != null
            && bundle.getBoolean(EXTRA_CREATE_FOLDER, false)) {
            // Add the folder into the database
            ContentValues values = new ContentValues();
            values.put(BrowserContract.Bookmarks.TITLE, title);
            values.put(BrowserContract.Bookmarks.IS_FOLDER, 1);
            if (currentFolder == -1) {
                Log.i(TAG, "saveCustomizedEditFolder: invalid folder=" + currentFolder
                        + ", revised to " + FIXED_ID_ROOT);
                currentFolder = FIXED_ID_ROOT;
            }
            values.put(BrowserContract.Bookmarks.PARENT, currentFolder);
            Uri uri = context.getContentResolver().insert(
                    BrowserContract.Bookmarks.CONTENT_URI, values);
            Log.i(TAG, "saveCustomizedEditFolder: parent=" + currentFolder
                    + ", uri=" + uri);
            if (uri != null) {
                return new Boolean(true);
            } else {
                Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
                return new Boolean(false);
            }
        } else {
            return null;
        }
    }

    private int addDefaultBookmarks(SQLiteDatabase db, CharSequence[] bookmarks, TypedArray preloads) {
        Resources res = mContext.getResources();
        int size = bookmarks.length;
        Log.i("@M_" + TAG, "bookmarks length = " + size);
        try {
            String parent = Long.toString(FIXED_ID_ROOT);
            String now = Long.toString(System.currentTimeMillis());
            for (int i = 0; i < size; i = i + 2) {
                CharSequence bookmarkDestination = bookmarks[i + 1];
                db.execSQL("INSERT INTO bookmarks (" +
                    Bookmarks.TITLE + ", " +
                    Bookmarks.URL + ", " +
                    Bookmarks.IS_FOLDER + "," +
                    Bookmarks.PARENT + "," +
                    Bookmarks.POSITION + "," +
                    Bookmarks.DATE_CREATED +
                    ") VALUES (" +
                    "'" + bookmarks[i] + "', " +
                    "'" + bookmarkDestination + "', " +
                    "0," +
                    "1," +
                    (Integer.toString(i)) + "," +
                    now +
                    ");");

                int faviconId = preloads.getResourceId(i, 0);
                int thumbId = preloads.getResourceId(i + 1, 0);
                byte[] thumb = null, favicon = null;
                try {
                    thumb = readRaw(res, thumbId);
                } catch (IOException e) {
                    Log.i("@M_" + TAG, "read thumbnail failed!");
                }
                try {
                    favicon = readRaw(res, faviconId);
                } catch (IOException e) {
                    Log.i("@M_" + TAG, "read favicon failed!");
                }
                if (thumb != null || favicon != null) {
                    ContentValues imageValues = new ContentValues();
                    imageValues.put(Images.URL, bookmarkDestination.toString());
                    if (favicon != null) {
                        imageValues.put(Images.FAVICON, favicon);
                    }
                    if (thumb != null) {
                        imageValues.put(Images.THUMBNAIL, thumb);
                    }
                    db.insert(TABLE_IMAGES, Images.FAVICON, imageValues);
                }
            }
        } finally {
            preloads.recycle();
        }
        return size;
    }

    private byte[] readRaw(Resources res, int id) throws IOException {
        if (id == 0) {
            return null;
        }
        InputStream is = res.openRawResource(id);
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[4096];
            int read;
            while ((read = is.read(buf)) > 0) {
                bos.write(buf, 0, read);
            }
            bos.flush();
            return bos.toByteArray();
        } finally {
            is.close();
        }
    }

}
