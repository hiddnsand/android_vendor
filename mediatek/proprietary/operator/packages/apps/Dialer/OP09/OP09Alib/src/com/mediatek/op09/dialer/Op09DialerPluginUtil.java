package com.mediatek.op09.dialer;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;

import java.util.List;

public class Op09DialerPluginUtil {

    private static final String TAG = "OP09DialerPluginUtil";

    private static Context mPluginContext;
    private Context mHostContext;
    private static final String TEL_SERVICE = "TelephonyConnectionService";

    public Op09DialerPluginUtil(Context context) {
        mHostContext = context.getApplicationContext();
        try {
            mPluginContext = context.createPackageContext("com.mediatek.op09.dialer", Context.CONTEXT_INCLUDE_CODE | Context.CONTEXT_IGNORE_SECURITY);
        } catch (NameNotFoundException e) {
            Log.d(TAG, "no com.mediatek.op09.dialer packages");
        }
    }

    public Context getPluginContext() {
        return mPluginContext;
    }

    public Context getHostContext() {
        return mHostContext;
    }

    public static boolean isSimInsert(final int slot) {
        final ITelephony iTel = ITelephony.Stub.asInterface(ServiceManager
                .getService(Context.TELEPHONY_SERVICE));
        boolean isSimInsert = false;
        log("[isSimInserted] slot :" + slot);
        if (iTel != null && SubscriptionManager.isValidSlotIndex(slot)) {
            isSimInsert = TelephonyManager.from(mPluginContext).hasIccCard(slot);
            log("[isSimInserted] isSimInsert :" + isSimInsert);
        }
        return isSimInsert;
    }

    /**
     * @return subId for a slotId
     */
    public static int getSubIdUsingSlotId(int slotId) {
        int[] sub = SubscriptionManager.getSubId(slotId);
        int subId = SubscriptionManager.INVALID_SIM_SLOT_INDEX;
        if (sub != null) {
            subId = sub[0];
        }
        return subId;
    }

    public static PhoneAccountHandle getPstnPhoneAccountHandleBySlotId(
                Context context, int slotId) {
        PhoneAccountHandle accountHandle = null;
        TelecomManager telecomManager = TelecomManager.from(context);
        TelephonyManager telephonyManager = TelephonyManager.from(context);

        final List<PhoneAccountHandle> accountHandles = telecomManager
                .getAllPhoneAccountHandles();
        for (PhoneAccountHandle handle : accountHandles) {
            if (handle.getComponentName().getShortClassName().endsWith(TEL_SERVICE)) {
                if (handle.getId() != null) {
                    PhoneAccount account = telecomManager.getPhoneAccount(handle);
                    int subId = telephonyManager.getSubIdForPhoneAccount(account);
                    log("subId: " + subId + " slot " + SubscriptionManager.getSlotIndex(subId));
                    if (slotId == SubscriptionManager.getSlotIndex(subId)) {
                        return handle;
                    }
                }
            }
        }
        return accountHandle;
    }

    /**
     * A variant of {@link #getCallIntent(android.net.Uri)} but also accept a call
     * origin and {@code Account} and {@code VideoCallProfile} state.
     * For more information about call origin, see comments in Phone package (PhoneApp).
     */
    public static Intent getCallIntent(String number, PhoneAccountHandle accountHandle) {
        final Intent intent = new Intent(Intent.ACTION_CALL, getCallUri(number));
        if (accountHandle != null) {
            intent.putExtra(TelecomManager.EXTRA_PHONE_ACCOUNT_HANDLE, accountHandle);
        }

        return intent;
    }
    /**
     * Return Uri with an appropriate scheme, accepting both SIP and usual phone call
     * numbers.
     */
    public static Uri getCallUri(String number) {
        if (isUriNumber(number)) {
            return Uri.fromParts(PhoneAccount.SCHEME_SIP, number, null);
        }
        return Uri.fromParts(PhoneAccount.SCHEME_TEL, number, null);
    }

    public static boolean isUriNumber(String number) {
        // Note we allow either "@" or "%40" to indicate a URI, in case
        // the passed-in string is URI-escaped. (Neither "@" nor "%40"
        // will ever be found in a legal PSTN number.)
        return number != null && (number.contains("@") || number.contains("%40"));
    }

    private static void log(String msg) {
        Log.d(TAG, msg);
    }
}
