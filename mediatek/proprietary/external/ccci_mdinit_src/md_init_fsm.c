/*
* Copyright (C) 2011-2017 MediaTek Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <cutils/properties.h>
#include <android/log.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <stdlib.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/mman.h>
#include <time.h>
#include <sys/sysinfo.h>
#include "libnvram.h"
#include "CFG_MDTYPE_File.h"
#include "CFG_file_lid.h"
#include "hardware/ccci_intf.h"
#include "CFG_MD_SBP_File.h"
#include "ccci_mdinit_cfg.h"
#include "ccci_common.h"

// for FSD wake_lock not released
#include <hardware_legacy/power.h>
#define FSD_WAKE_UNLOCK() release_wake_lock("ccci_fsd")

// NVRAM issue make timeout value as 22s,
#define MAX_OPEN_PORT_RETRY_NUM            (600)

//--------------structure define-----------------//
typedef struct
{
    unsigned int data[2];
    unsigned int channel;
    unsigned int reserved;
} CCCI_BUFF_T;

/* MD Message, this is for user space deamon use */
enum {
    CCCI_MD_MSG_FORCE_STOP_REQUEST = 0xFAF50001,
    CCCI_MD_MSG_FLIGHT_STOP_REQUEST,
    CCCI_MD_MSG_FORCE_START_REQUEST,
    CCCI_MD_MSG_FLIGHT_START_REQUEST,
    CCCI_MD_MSG_RESET_REQUEST,

    CCCI_MD_MSG_EXCEPTION,
    CCCI_MD_MSG_SEND_BATTERY_INFO,
    CCCI_MD_MSG_STORE_NVRAM_MD_TYPE,
    CCCI_MD_MSG_CFG_UPDATE,
    CCCI_MD_MSG_RANDOM_PATTERN,
};

/* MD Status, this is for user space deamon use */
enum {
    CCCI_MD_STA_INIT = -1,
    CCCI_MD_STA_BOOT_READY = 0,
    CCCI_MD_STA_BOOT_UP,
    CCCI_MD_STA_RESET,
    CCCI_MD_STA_STOP,
    CCCI_MD_STA_FLIGHT_MODE,
    CCCI_MD_STA_EXCEPTION,
};

enum {
    MD_DEBUG_REL_INFO_NOT_READY = 0,
    MD_IS_DEBUG_VERSION,
    MD_IS_RELEASE_VERSION
};

//----------------maro define-----------------//
// For rild common
#define RILD_PROXY_NAME            "ril-proxy"
#define RILD_PROXY_SERVICE_STATUS    "init.svc.ril-proxy"
// For MD1
#define MONITOR_DEV_FOR_MD1        "/dev/ccci_monitor"
#define MUXD_FOR_MD1_NAME        "gsm0710muxd"
#define RILD_FOR_MD1_NAME        "ril-daemon-mtk"
#define MD_LOG_FOR_MD1_NAME        "mdlogger"
//#define MD_LOG_FOR_MD1_NAME_E    "ecccimdlogger"
#define MD_LOG_FOR_MD1_NAME_E        "emdlogger1"
#define FSD_FOR_MD1_NAME        "ccci_fsd"
#define MD1_INIT_CMD            "0"
#define MD1_TAG                "ccci_mdinit(1)"

// For MD2
#define MONITOR_DEV_FOR_MD2        "/dev/ccci2_monitor"
#define MUXD_FOR_MD2_NAME        "gsm0710muxdmd2"
#define RILD_FOR_MD2_NAME        "ril-daemon-mtkmd2"
#define MD_LOG_FOR_MD2_NAME        "dualmdlogger"
#define MD_LOG_FOR_MD2_NAME_E        "emdlogger2"
#define FSD_FOR_MD2_NAME        "ccci2_fsd"
#define MD2_INIT_CMD            "1"
#define MD2_TAG                "ccci_mdinit(2)"

// For MD3
#define MONITOR_DEV_FOR_MD3        "/dev/ccci3_monitor"
#define MUXD_FOR_MD3_NAME        "invalid"
#define RILD_FOR_MD3_NAME        "viarild"
#define MD_LOG_FOR_MD3_NAME        "dualmdlogger"
#define MD_LOG_FOR_MD3_NAME_E        "emdlogger3"
#define FSD_FOR_MD3_NAME        "ccci3_fsd"
#define MD3_INIT_CMD            "2"
#define MD3_TAG                "ccci_mdinit(3)"

// Common
#define MD_INIT_OLD_FILE        "/sys/class/BOOT/BOOT/boot/md"
#define MD_INIT_NEW_FILE        "/sys/kernel/ccci/boot"
#define BOOT_MODE_FILE            "/sys/class/BOOT/BOOT/boot/boot_mode" // controlled by mtxxxx_boot.c
#define META_MODE            '1'
#define FACTORY_MODE        '4'
#define CCCI_MONITOR_CH            (0xf0000000)
#define MD_COMM_TAG            "ccci_mdinit(0)"
#define MD_RESET_WAIT_TIME        "mediatek.debug.md.reset.wait"
#define MDLOGGER_CAN_RESET        "debug.md.reset"

// Phase out
#define MUX_DAEMON_SINGLE    "gsm0710muxd-s"
#define RIL_DAEMON_SINGLE    "ril-daemon-s"

static md_type_struct *nvram_md_type = NULL;

//----------------variable define-----------------//
static int  md_ctl_fsm_curr_state = CCCI_MD_STA_INIT; /* Modem Control Finity State Machine global control variable */
static int  need_silent_reboot = 0; /* This varialbe will set to 1 when modem exception at boot ready state */
static int  ignore_time_out_msg = 0;
static int  gotten_md_info = MD_DEBUG_REL_INFO_NOT_READY;
static int  system_ch_handle = 0;
static int  mdlogger_cnt = 0;
static int  md_id = -1;
static char sys_boot_mode = 0;
static char muxd_name[PROPERTY_KEY_MAX];
static char rild_name[PROPERTY_KEY_MAX];
static char mdlogger_name[PROPERTY_KEY_MAX];
static char fsd_name[PROPERTY_KEY_MAX];
static char md_boot_name[32];
//  service property name init.svc.service
static const char *pre_service_status = "init.svc.";
static char muxd_service_status[PROPERTY_KEY_MAX];
static char rild_service_status[PROPERTY_KEY_MAX];
static char mdlogger_service_status[PROPERTY_KEY_MAX];
static char fsd_service_status[PROPERTY_KEY_MAX];

// service start/stop wait time
#define MAX_WAIT_FOR_PROPERTY  6000  // wait 6s for service status changed

typedef struct _wait_prop_t
{
    char        *prop_name;
    char        *disr_value;
} wait_prop_t;

static int start_service_verified(const char *service_name, const char*service_status_name, int waitmsec);
static int stop_service_verified(const char *service_name, const char*service_status_name, int waitmsec);
static int wait_property_ready(wait_prop_t *wp, const int count, const int waitmsec);
static void update_service_name(void);
static int rild_generation(void);
static int set_md_boot_env_data(int md_id, int fd);

/* External functin list by env_setting.c */
extern int compute_random_pattern(unsigned int * p_val);
extern int get_stored_modem_type_val(int md_id);
extern int store_modem_type_val(int md_id, int new_val);
extern unsigned int parse_sys_env_rat_setting(void);


static int check_rild_ee_indication(void)
{
    char buf[PROPERTY_VALUE_MAX];
    int val = 0;

    property_get("persist.ril.ee.delay", buf, "none");
    if (0 != strcmp(buf, "none")) {
        val = atoi(buf);
        CCCI_LOGD("Wait rild %ds to aware MD EE\n",val);
        if(0< val && val < 10)
            return val;
    }
    CCCI_LOGD("Do not notify rild about MD EE\n");
    return 0;
}

static int check_curret_md_status(int desired)
{
    if (md_ctl_fsm_curr_state == desired) {
        CCCI_LOGI("status already is %d\n", desired);
        return 1;
    }
    return 0;
}

static void set_current_md_status(int status)
{
    char buf[PROPERTY_VALUE_MAX];
    char name[50];
    time_t cur_time;
    int len;

    md_ctl_fsm_curr_state = status;
    switch(status){
    case CCCI_MD_STA_INIT:
        len = snprintf(buf, sizeof(buf), "init");
        break;
    case CCCI_MD_STA_BOOT_READY:
        len = snprintf(buf, sizeof(buf), "ready");
        break;
    case CCCI_MD_STA_BOOT_UP:
        len = snprintf(buf, sizeof(buf), "bootup");
        break;
    case CCCI_MD_STA_RESET:
        len = snprintf(buf, sizeof(buf), "reset");
        break;
    case CCCI_MD_STA_STOP:
        len = snprintf(buf, sizeof(buf), "stop");
        break;
    case CCCI_MD_STA_FLIGHT_MODE:
        len = snprintf(buf, sizeof(buf), "flightmode");
        break;
    case CCCI_MD_STA_EXCEPTION:
        len = snprintf(buf, sizeof(buf), "exception");
        break;
    default:
        len = snprintf(buf, sizeof(buf), "undefined");
        break;
    }

    time(&cur_time);

    snprintf(name, sizeof(name), "mtk.md%d.status",md_id+1);
    CCCI_LOGD("set md status:%s=%s \n",name,buf);
    RLOGI("MD%d set status: %s=%s \n", md_id+1, name, buf);
    property_set(name,buf);
    if (md_id == MD_SYS3) {
        snprintf(name, sizeof(name), "net.cdma.mdmstat");
        CCCI_LOGD("set md status:%s=%s \n", name, buf);
        property_set(name, buf);
    }
}

static char get_sys_boot_mode(void)
{
    int fd, ret;
    size_t s;
    volatile char data[20];

    if (!sys_boot_mode) {
        fd = open(BOOT_MODE_FILE, O_RDONLY);
        if (fd < 0) {
            CCCI_LOGE("fail to open %s: err_no=%d", BOOT_MODE_FILE, errno);
            return 0;
        }

        s = read(fd, (void *)data, sizeof(char) * 3);
        if (s <= 0) {
            CCCI_LOGE("fail to read %s err_no=%d", BOOT_MODE_FILE, errno);
            sys_boot_mode = '0';
        } else {
            sys_boot_mode = data[0];
        }

        close(fd);
    }
    CCCI_LOGD("system boot Mode: %d\n", sys_boot_mode);
    return sys_boot_mode;
}

static int is_factory_mode(void)
{
    if (get_sys_boot_mode() == FACTORY_MODE)
        return 1;
    else
        return 0;
}

static int is_meta_mode(void)
{
    int fd, ret;
    size_t s;
    volatile char data[20];

    fd = open(BOOT_MODE_FILE, O_RDONLY);
    if (fd < 0) {
        CCCI_LOGE("fail to open %s: err_no=%d", BOOT_MODE_FILE, errno);
        return 0;
    }

    s = read(fd, (void *)data, sizeof(char) * 3);
    if (s <= 0) {
        CCCI_LOGE("fail to read %s err_no=%d", BOOT_MODE_FILE, errno);
        ret = 0;
    } else {
        if (data[0] == META_MODE)
            ret = 1;
        else
            ret = 0;
    }

    close(fd);
    return ret;
}

static void check_to_restart_md(void)
{
    char this_md_status_key[32];
    char the_other_md_status_key[32];
    if(md_id == MD_SYS1) {
        snprintf(this_md_status_key, 32, "%s", "mtk.md1.status");
        snprintf(the_other_md_status_key, 32, "%s", "mtk.md3.status");
    }else if (md_id == MD_SYS3) {
        snprintf(this_md_status_key, 32, "%s", "mtk.md3.status");
        snprintf(the_other_md_status_key, 32, "%s", "mtk.md1.status");
    }
    char buf[PROPERTY_VALUE_MAX];
    int ret;
    int cnt=0;
    int ccif_on = 1;

    do{
        ret = property_get(the_other_md_status_key, buf, NULL);
        if (ret == 0) {
            if (md_id == MD_SYS1 && !is_factory_mode()) {
                if (rild_generation() == 0) { /* c2k in one case */
                    stop_service_verified(RILD_PROXY_NAME, RILD_PROXY_SERVICE_STATUS, MAX_WAIT_FOR_PROPERTY);
                    CCCI_LOGD("check_to_restart_md kill ril-proxy, md1 only\n");
                } else
                    CCCI_LOGD("ignore kill ril-proxy, fusion\n");
            }
            CCCI_LOGI("check_to_restart_md:%s not exist\n", the_other_md_status_key);
            return;
        }
        if (strcmp(buf, "reset") == 0) {
            if (md_id == MD_SYS3) {
                if (0 != ioctl(system_ch_handle, CCCI_IOC_RESET_MD1_MD3_PCCIF, NULL)){
                    CCCI_LOGE("reset pccif fail \n");
                }
                CCCI_LOGD("reset pccif/ccirq done\n");
            }
            property_set(the_other_md_status_key, "reset_start");
            wait_for_property(the_other_md_status_key, "reset_start", MAX_WAIT_FOR_PROPERTY);
            CCCI_LOGI("check_to_restart_md:set %s reset_start done\n", the_other_md_status_key);
            break;
        }
        cnt++;
        if ((cnt%100) == 0)
            CCCI_LOGI("check_to_restart_md:waiting %s=%s...\n", the_other_md_status_key, buf);
        usleep(30*1000);
    }while(1);
    CCCI_LOGI("md%d check_to_restart_md:waiting %s done\n", md_id+1, the_other_md_status_key);

    do{
        ret = property_get(this_md_status_key, buf, NULL);
        if (ret == 0) {
            CCCI_LOGI("check_to_restart_md:%s not exist\n", this_md_status_key);
            return;
        }
        if (strcmp(buf,"reset_start") == 0) {
            CCCI_LOGI("check_to_restart_md: %s reset_start\n", this_md_status_key);
            break;
        }
        cnt++;
        if ((cnt%100) == 0)
            CCCI_LOGI("check_to_restart_md:waiting %s =%s...\n", this_md_status_key, buf);
        usleep(30*1000);
    }while(1);

    if (md_id == MD_SYS1 && !is_factory_mode()) {
        if (rild_generation() == 0) { /* c2k in one case */
            stop_service_verified(RILD_PROXY_NAME, RILD_PROXY_SERVICE_STATUS, MAX_WAIT_FOR_PROPERTY);
            CCCI_LOGD("check_to_restart_md kill ril-proxy\n");
        } else
            CCCI_LOGD("ignore kill ril-proxy, fusion\n");
    }
}

/****************************************************************************/
/* modem control message handle function                                                                  */
/*                                                                                                                           */
/****************************************************************************/

static void delay_to_reset_md(void)
{
    char buf[PROPERTY_VALUE_MAX];
    int val;
    property_get(MD_RESET_WAIT_TIME, buf, "none");
    if (0 != strcmp(buf, "none")) {
        val = atoi(buf);
    CCCI_LOGD("Wait modem %ds to reset md\n",val);
    if(0< val && val < 10)
        sleep(val);
    else
        CCCI_LOGD("Wait modem time invalid:%s\n", buf);
    }
}

static void stop_all_ccci_up_layer_services(void)
{
    int retry= 0;
    char buf[PROPERTY_VALUE_MAX];
    wait_prop_t wp[3];
    int wpcount = 0;
    int stopped = 0;

    CCCI_LOGD("stop all up layer service\n");

    stop_service_verified(fsd_name, fsd_service_status, MAX_WAIT_FOR_PROPERTY);
    FSD_WAKE_UNLOCK();
    if (!is_factory_mode()) {
        stop_service_verified(rild_name, rild_service_status, MAX_WAIT_FOR_PROPERTY);

        if(md_id != MD_SYS3){
            property_set("ctl.stop", muxd_name);
            wp[wpcount].prop_name = muxd_service_status;
            wp[wpcount].disr_value ="stopped";
            wpcount++;
        }
    }
    if(mdlogger_cnt) {
        while(retry < 6){
            retry++;
            property_get(MDLOGGER_CAN_RESET, buf, "0");
            if (0 != strcmp(buf, "0"))
                break;
            usleep(50*1000);
        }
        if (retry >= 6)
            CCCI_LOGD("MDlogger not set %s\n",MDLOGGER_CAN_RESET);
        property_set("ctl.stop", mdlogger_name);
        wp[wpcount].prop_name = mdlogger_service_status;
        wp[wpcount].disr_value ="stopped";
        wpcount++;

        mdlogger_cnt = 0;
    }

    stopped = wait_property_ready(wp, wpcount, MAX_WAIT_FOR_PROPERTY);
    if (stopped > 0)
        CCCI_LOGD("stop all up layer service succeeded!\n");
    else
        CCCI_LOGE("stop all up layer service failed\n");
}

static void start_all_ccci_up_layer_services(void)
{
    CCCI_LOGD("start all ccci up layer services\n");

    if (need_silent_reboot) {
        CCCI_LOGD("set ril.mux.report.case 2\n");
        property_set("ril.mux.report.case", "2"); /* set mux flag here, should before muxd */
    }

    if(mdlogger_cnt == 0) {
        // CCCI_LOGD("start mdlogger\n");
        start_service_verified(mdlogger_name, mdlogger_service_status, 0);
        mdlogger_cnt = 1;
    }

    update_service_name();
    if (!is_factory_mode()) {
        if(md_id != MD_SYS3) {
            start_service_verified(muxd_name, muxd_service_status, MAX_WAIT_FOR_PROPERTY);
        } else {
            // CCCI_LOGD("start c2k rild\n");
            start_service_verified(rild_name, rild_service_status, MAX_WAIT_FOR_PROPERTY);
        }
    }
}

static int get_nvram_ef_data(int fid, int recsize, void* pdata)
{
    F_ID nvram_ef_fid = {0,0,0};
    int rec_size = 0;
    int rec_num = 0;
    bool isread = false;
    int ret = 0;

    if (pdata == NULL) {
        CCCI_LOGE("get_nvram_ef_data: pdata=NULL, fid:%d, recsize:%d\n", fid, recsize);
        return -1;
    }

    nvram_ef_fid = NVM_GetFileDesc(fid, &rec_size, &rec_num, isread);
    if (nvram_ef_fid.iFileDesc < 0) {
        CCCI_LOGE("get_nvram_ef_data: Fail to get nvram file descriptor!! fid:%d, errno:0x%x\n", fid, errno);
        return -1;
    }

    if (rec_size != read(nvram_ef_fid.iFileDesc, pdata, rec_size)) {
        CCCI_LOGE("get_nvram_ef_data: Fail to read nvram file!! fid:%d, errno:0x%x\n", fid, errno);
        return -1;
    }

    if (!NVM_CloseFileDesc(nvram_ef_fid)) {
        CCCI_LOGE("get_nvram_ef_data: Fail to close nvram file!! fid:%d, errno:0x%x\n", fid, errno);
    }

    return ret;
}

static int store_nvram_ef_data(int fid, int recsize, void* pdata)
{
    F_ID nvram_ef_fid = {0,0,0};
    int rec_size = 0;
    int rec_num = 0;
    bool isread = false;
    void *old_buf = NULL;
    unsigned int *pold_data = NULL;
    int ret = 0;

    CCCI_LOGD("store_nvram_ef_data fid:%d, size:%d, pdata:0x%x\n",
        fid, recsize, ((pdata == NULL) ? 0 : ((unsigned int) pdata)));

    // back up the old data
    old_buf = (void *)malloc(recsize);
    if (!old_buf) {
        CCCI_LOGE("store_nvram_ef_data fid: allocate data memory err:0x%x\n", errno);
    } else {
        ret = get_nvram_ef_data(fid, recsize, old_buf);
        pold_data = (unsigned int *)old_buf;
    }

    if (pold_data != NULL)
        CCCI_LOGD("store_nvram_ef_data ret:%d, data1:%d, data2:0x%x\n",
                fid, (!ret) ? (*pold_data) : 0, (!ret) ? (*(pold_data + 1)) : 0);
    else
        CCCI_LOGE("pold_data is NULL.\n");

    nvram_ef_fid = NVM_GetFileDesc(fid, &rec_size, &rec_num, isread);
    if (nvram_ef_fid.iFileDesc < 0) {
        CCCI_LOGE("store_nvram_ef_data: Fail to get nvram file descriptor!! fid:%d, errno:0x%x\n", fid, errno);
        if (old_buf)
            free(old_buf);
        return -1;
    }

    if (rec_size != write(nvram_ef_fid.iFileDesc, pdata, rec_size)) {
        CCCI_LOGE("store_nvram_ef_data: Fail to write nvram file!! fid:%d, errno:0x%x, rec_size:%d, recsize:%d\n"
            , fid, errno, rec_size, recsize);
        ret = -1;
        // Try to recovery old data
        if (pold_data) {
            rec_size = write(nvram_ef_fid.iFileDesc, pold_data, recsize);
            CCCI_LOGE("store_nvram_ef_data: recovery data!! fid:%d, errno:0x%x, rec_size:%d, recsize:%d\n"
                , fid, errno, rec_size, recsize);
        }
    }

    if (!NVM_CloseFileDesc(nvram_ef_fid)) {
        CCCI_LOGE("store_nvram_ef_data: Fail to close nvram file!! fid:%d, errno:0x%x\n", fid, errno);
    }

    if (old_buf)
        free(old_buf);
    return ret;
}


static int start_service_verified(const char *service_name, const char*service_status_name, int waitmsec)
{
    int succeeded = -1;
    char value[PROPERTY_VALUE_MAX] = {'\0'};

    succeeded = property_get(service_status_name, value, NULL);
    if (succeeded > 0)
        CCCI_LOGI("start_service %s, current state:%s, returned:%d\n", service_status_name, value, succeeded);
    else if (succeeded == 0)
        CCCI_LOGI("start_service %s, but returned 0, maybe has no this property\n", service_status_name);
    else
        CCCI_LOGI("start_service %s, returned:%d\n", service_status_name, succeeded);
    property_set("ctl.start", service_name);
    succeeded = wait_for_property(service_status_name, "running", waitmsec);
    return succeeded;
}

static int stop_service_verified(const char *service_name, const char*service_status_name, int waitmsec)
{
    int succeeded = -1;

    property_set("ctl.stop", service_name);
    succeeded = wait_for_property(service_status_name, "stopped", waitmsec);
    return succeeded;
}

// return 1: ready,  0, not ready
static int wait_property_ready(wait_prop_t *wp, const int count, const int waitmsec)
{
    int i;
    int ready = 0;
    int watiflag = 0;
    int bitflag = 0;
    int maxtimes = waitmsec /PROPERTY_WAIT_TIME;
    int needtry = 1;
    wait_prop_t *curwt = NULL;
    char value[PROPERTY_VALUE_MAX] = {'\0'};
    int needgetprop = 0;

    if (count > (int)(sizeof(watiflag) * 8)) {
        CCCI_LOGI("%s: count > %d, return 0\n", __func__, (8 * sizeof(int)));
        return ready;
    }

    if (maxtimes < 1)
        maxtimes = 1;

    CCCI_LOGI("%s: count:%d, waitmsec:%d, loop:%d\n", __func__, count, waitmsec, maxtimes);
    while ((maxtimes-- > 0) && needtry) {
        curwt = wp;
        needtry = 0;

        //CCCI_LOGI("%s watiflag:0x%x\n", __func__, watiflag);
        for (i=0; i<count; i++) {
            bitflag = ( 1<< i);

            needgetprop = (!(watiflag & bitflag));
            if (needgetprop) {
                if (wait_for_property(curwt->prop_name, curwt->disr_value, 0) >= 0)
                    watiflag |= bitflag;
                else
                    if (needtry == 0) needtry = 1;
            }

            if (needgetprop && (maxtimes % 10 == 0)) {
                CCCI_LOGI("%s: retry name:%s, disrvalue:%s, loop:%d\n",
                    __func__, curwt->prop_name, curwt->disr_value, maxtimes);
            }
            curwt++;
        }

        // if need to retry, sleep about 100ms
        if (needtry)
            usleep(PROPERTY_WAIT_TIME * 1000);
    }

    if (!needtry){
        CCCI_LOGI("%s:Succeeded! count:%d, waitmsec:%d, loop:%d\n", __func__, count, waitmsec, maxtimes);
        ready = 1;
    } else {
        CCCI_LOGI("%s:Failed! count:%d, waitmsec:%d, loop:%d\n", __func__, count, waitmsec, maxtimes);
    }

    return ready;
}

static int common_msg_handler(int msg, int resv)
{
    int ret = 1;
    int data = 0;
    char str[32];
    int rild_pid, rild_delay;
    char rild_pid_key[PROPERTY_KEY_MAX] = {0};
    char rild_pid_value[PROPERTY_VALUE_MAX] = {0};
    char property_name[50];

    switch (msg) {
    case CCCI_MD_MSG_SEND_BATTERY_INFO:
        if(ioctl(system_ch_handle, CCCI_IOC_SEND_BATTERY_INFO, &data))
            CCCI_LOGE("send md battery info fail: %d", errno);
        else
            CCCI_LOGD("send md battery info OK");
        break;
    case CCCI_MD_MSG_CFG_UPDATE:
        CCCI_LOGD("CFG UPDATE: 0x%x(dummy)\n", resv);
        break;
    case CCCI_MD_MSG_RANDOM_PATTERN:
        CCCI_LOGD("CCCI_MD_MSG_RANDOM_PATTERN\n");
        compute_random_pattern((unsigned int*) &data);
        if (0 != ioctl(system_ch_handle, CCCI_IOC_RESET_AP, &data))
            CCCI_LOGE("reset_ap_ioctl failed.\n");
        break;
    case CCCI_MD_MSG_STORE_NVRAM_MD_TYPE:
        CCCI_LOGD("CCCI_MD_MSG_STORE_SIM_MODE\n");
        ioctl(system_ch_handle, CCCI_IOC_GET_MD_TYPE_SAVING, &data);
        CCCI_LOGD("md%d type in kernel(%d)\n", md_id+1, data);
        if (data == get_stored_modem_type_val(md_id))
            CCCI_LOGD("No need to store md type(%d)\n", data);
        else
            store_modem_type_val(md_id, data);
        break;
    case CCCI_MD_MSG_EXCEPTION:
        if (check_curret_md_status(CCCI_MD_STA_EXCEPTION))
            break;
        CCCI_LOGD(" CCCI_MD_MSG_EXCEPTION\n");
        set_current_md_status(CCCI_MD_STA_EXCEPTION);
        if(mdlogger_cnt == 0) {
            CCCI_LOGD("start mdlogger when MD exception happens early\n");
            start_service_verified(mdlogger_name, mdlogger_service_status, MAX_WAIT_FOR_PROPERTY);
            mdlogger_cnt = 1;
        }
        need_silent_reboot = 1;
        if(md_id == MD_SYS3) {
            /*c2krild will use this "exception" state to decide how to triger NE*/
            snprintf(property_name, sizeof(property_name), "net.cdma.mdmstat");
            CCCI_LOGD("set md3 status:%s=exception \n",property_name);
            property_set(property_name,"exception");
        }
        rild_delay = check_rild_ee_indication();
        if(rild_delay) {
            snprintf(rild_pid_key, PROPERTY_KEY_MAX, "ril.pid.%d", md_id+1);
            if (property_get(rild_pid_key, rild_pid_value, NULL) > 0) {
                rild_pid = atoi(rild_pid_value);
                CCCI_LOGD("get ril pid=%d\n", rild_pid);
                if(rild_pid > 0) {
                    int val = (SIGUSR2<<16)|(rild_pid&0xFFFF);
                    ret = ioctl(system_ch_handle, CCCI_IOC_SEND_SIGNAL_TO_USER, &val);
                    CCCI_LOGD("send SIGUSR2 to ril %d/%d\n", ret, errno);
                    sleep(rild_delay);
                }
            } else {
                CCCI_LOGE("get rild_pid property failed. rild_pid_key=%s\n", rild_pid_key);
            }
        }
        break;
    default:
        ret = 0;
        break;
    }
    return ret;
}

/****************************************************************************/
/* initial and main thread                                                                                           */
/*                                                                                                                           */
/****************************************************************************/
static int trigger_modem_to_run(unsigned int monitor_fd, int flight_mode, int first_boot)
{
    int fd, ret, curr_md_type;
    int current_boot_mode = MD_BOOT_MODE_INVALID;
    char data[20];
    char md_id_str[16];

    CCCI_LOGD("trigger modem to run! %d %d\n", flight_mode, first_boot);
    set_current_md_status(CCCI_MD_STA_RESET);
    if (flight_mode) {
        wait_for_property("ril.getccci.response", "1", 500);
        if (!is_meta_mode()) {
            stop_all_ccci_up_layer_services();
        }
    }
    ret = ioctl(monitor_fd, CCCI_IOC_GET_MD_TYPE, &curr_md_type);
    if(0 == ret){
        snprintf(md_id_str, 16, "%d", curr_md_type);
        property_set("ril.active.md", md_id_str);
    } else {
        CCCI_LOGD("[Active MD]get md type fail: %d(%d)\n", errno, ret);
    }

    if (!first_boot)
        check_to_restart_md();
    check_decrypt_ready();
    set_md_boot_env_data(md_id, monitor_fd);
    start_service_verified(fsd_name, fsd_service_status, MAX_WAIT_FOR_PROPERTY);

    set_current_md_status(CCCI_MD_STA_BOOT_UP);
    if (first_boot)
        ccci_ccb_init_users(md_id);
    ret = ioctl(monitor_fd, CCCI_IOC_DO_START_MD, NULL);
    if (ret) {
        CCCI_LOGE("Fail to trigger md run, ioctl %d %d\n", ret, errno);
        perror("");
        return -1;
    }

start_service:
    ret = ioctl(monitor_fd, CCCI_IOC_GET_MD_STATE, &current_boot_mode);
    if (ret < 0) {
        CCCI_LOGE("fail to get modem state(%d) %d %d\n", current_boot_mode, ret, errno);
        return -1;
    }
    if (current_boot_mode == MD_STATE_READY) {
        set_current_md_status(CCCI_MD_STA_BOOT_READY);
        ccci_ccb_check_users(md_id); // must before we start MDlogger
        if (!is_meta_mode()) {
            if (ioctl(monitor_fd, CCCI_IOC_GET_MD_BOOT_MODE, &current_boot_mode) == 0) {
                if (current_boot_mode != MD_BOOT_MODE_META)
                    start_all_ccci_up_layer_services();
                else
                    CCCI_LOGD("boot MD into META mode when system is in normal mode\n");
            } else {
                start_all_ccci_up_layer_services();
            }
        }
        need_silent_reboot = 0;
    } else {
        CCCI_LOGI("modem state %d\n", current_boot_mode);
        return -1;
    }
    CCCI_LOGD("modem boot ready and deamon begin to run!\n");
    return 0;
}

static void update_service_name(void)
{
    if(md_id != 0)
        return;

    snprintf(muxd_name, 32, MUXD_FOR_MD1_NAME);
    snprintf(muxd_service_status, PROPERTY_KEY_MAX, "%s%s", pre_service_status, muxd_name);
    snprintf(rild_name, 32, "%s", RILD_FOR_MD1_NAME);
    snprintf(rild_service_status, PROPERTY_KEY_MAX, "%s%s", pre_service_status, rild_name);

    CCCI_LOGD("Current muxd: %s, rild:%s\n", muxd_name, rild_name);
}

static int get_usp_sbp_setting(void)
{
#define MTK_USP_SBP_NAME "persist.mtk_usp_md_sbp_code"
    char buf[PROPERTY_VALUE_MAX] ={ 0 };
    int ret;
    int sbp_code = 0;

    ret = property_get(MTK_USP_SBP_NAME, buf, NULL);
    if (ret == 0) {
        CCCI_LOGI("USP_SBP:%s not exist\n", MTK_USP_SBP_NAME);
        return -1;
    }
    sbp_code = atoi(buf);
    if (sbp_code < 0) {
        CCCI_LOGE("USP_SBP:%s=%s,usp_sbp=%d is a invalide value\n", MTK_USP_SBP_NAME, buf, sbp_code);
        sbp_code = 0;
    } else
        CCCI_LOGI("USP_SBP:%s=%s,usp_sbp=%d\n", MTK_USP_SBP_NAME, buf, sbp_code);

    return sbp_code;
}

static int get_sbp_subid_setting(void)
{
#define MTK_OP_SBP_SUB_ID_NAME "persist.operator.subid"
    char buf[PROPERTY_VALUE_MAX] ={ 0 };
    int ret;
    int sub_id_code = 0;

    ret = property_get(MTK_OP_SBP_SUB_ID_NAME, buf, NULL);
    if (ret == 0) {
        CCCI_LOGI("SBP_SUB_ID:%s not exist\n", MTK_OP_SBP_SUB_ID_NAME);
        return 0;
    }
    sub_id_code = atoi(buf);
    if (sub_id_code < 0) {
        CCCI_LOGE("SBP_SUB_ID:%s=%s,sub_id=%d is a invalide value\n", MTK_OP_SBP_SUB_ID_NAME, buf, sub_id_code);
        sub_id_code = 0;
    } else
        CCCI_LOGI("SBP_SUB_ID:%s=%s,usp_sbp=%d\n", MTK_OP_SBP_SUB_ID_NAME, buf, sub_id_code);

    return sub_id_code;
}


// return: 0-fail,  others-setting value
static unsigned int get_cip_sbp_setting(int md_id)
{
#define CIP_SBP_FILE "CIP_MD_SBP"
#define CIP_MD2_SBP_FILE "CIP_MD2_SBP"
    char md_cip_sbp_file[100];
    int fd, read_len;
    struct stat sbp_stat;
    char cip_sbp_value[12];
    long retl = 0;
    unsigned int ret = 0;
    char *endptr = NULL;

    memset(md_cip_sbp_file, 0x0, sizeof(md_cip_sbp_file));
    if (md_id == MD_SYS1) {
        snprintf(md_cip_sbp_file, sizeof(md_cip_sbp_file), "%s%s", md_img_cip_folder, CIP_SBP_FILE);
    } else if (md_id == MD_SYS2) {
        snprintf(md_cip_sbp_file, sizeof(md_cip_sbp_file), "%s%s", md_img_cip_folder, CIP_MD2_SBP_FILE);
    } else {
        CCCI_LOGD("get_cip_sbp_setting, md_id:%d is error!\n", md_id);
        return 0;
    }

    umask(0007);
    if (stat(md_cip_sbp_file, &sbp_stat) < 0) {
        CCCI_LOGD("get_cip_sbp_setting, file %s NOT exists!\n", md_cip_sbp_file);
        return 0;
    }

    fd = open(md_cip_sbp_file, O_RDONLY, 0660);
    if(fd < 0) {
        CCCI_LOGD("get_cip_sbp_setting, open file %s Fail! err:%d\n", md_cip_sbp_file, errno);
        return 0;
    }

    memset(cip_sbp_value, 0x0, sizeof(cip_sbp_value));
    read_len = (int)read(fd, cip_sbp_value, sizeof(cip_sbp_value));
    if(read_len < 0) {
        CCCI_LOGD("get_cip_sbp_setting, read file %s Fail! err:%d\n", md_cip_sbp_file, errno);
        close(fd);
        return 0;
    }
    close(fd);

    retl = strtol(cip_sbp_value, &endptr, 0);
    if (endptr != NULL) {
        CCCI_LOGD("get_cip_sbp_setting, Warning!! endptr is not NULL! value:%x\n", *((unsigned int *)endptr));
    }
    if (retl > 0) {
        ret = (unsigned int)(retl & 0xFFFFFFFF);
        CCCI_LOGD("get_cip_sbp_setting, get sbp setting:0x%x\n", ret);
    } else {
        CCCI_LOGD("get_cip_sbp_setting, Error!! sbp setting is 0!\n");
    }

    return ret;
}
static int get_nvram_sbp_code(int md_id)
{
#define MD_SBP_PATH_FILE "/vendor/nvdata/APCFG/APRDCL/MD_SBP"
    int getsbpcode = 0;
    int store_sbp_code = 0;
    MD_SBP_Struct *nvram_sbp_info = NULL;
    int md_sbp_lid = -1;

    md_sbp_lid = NVM_GetLIDByName(MD_SBP_PATH_FILE);
    if (md_sbp_lid < 0) {
        CCCI_LOGE("Error!! get sbp lid fail!!!%d\n", md_sbp_lid);
        goto EXIT_FUN;
    }
    nvram_sbp_info = (MD_SBP_Struct *)malloc(sizeof(MD_SBP_Struct));
    if (nvram_sbp_info == NULL) {
        CCCI_LOGD("Error!! malloc md sbp code fail! errno:%d\n", errno);
        goto EXIT_FUN;
    }
    memset((void *)nvram_sbp_info, 0, sizeof(MD_SBP_Struct));
    getsbpcode = get_nvram_ef_data(md_sbp_lid,    sizeof(MD_SBP_Struct), nvram_sbp_info);
    if (getsbpcode != 0) {
        CCCI_LOGD("Error!! get_nvram_ef_data fail lid=%d,ret:%d\n", md_sbp_lid, getsbpcode);
        goto EXIT_FUN;

    }
    if (md_id == MD_SYS2)
        store_sbp_code = nvram_sbp_info->md2_sbp_code;
    else
        store_sbp_code = nvram_sbp_info->md_sbp_code;
EXIT_FUN:
    if (nvram_sbp_info) {
        free(nvram_sbp_info);
        nvram_sbp_info = NULL;
    }

    return store_sbp_code;
}

static int get_project_sbp_code(int md_id)
{
    int sbp_code = 0;
    char tmp_buf[10] = {0};

    if (md_id == MD_SYS2) {
        // get md2 SBP code from ProjectConfig.mk from ccci_lib
        if (0 == query_prj_cfg_setting("MTK_MD2_SBP_CUSTOM_VALUE", tmp_buf, sizeof(tmp_buf)))
            sbp_code = atoi(tmp_buf);
    } else {
        // get md1 SBP code from ProjectConfig.mk from ccci_lib
        if (0 == query_prj_cfg_setting("MTK_MD_SBP_CUSTOM_VALUE", tmp_buf, sizeof(tmp_buf)))
            sbp_code = atoi(tmp_buf);
    }
    return sbp_code;
}

/*
 *  for MD SBP feature, diferent operators use the same md image
 *  MTK_MD_SBP_CUSTOM_VALUE_ must be definied on ProjectConfig.mk
 *    0: INVALID value, the project need SBP feature, but value 0 needn't transform to modem
 *  related files:
 *    MD_SBP: under /vendor/nvdata/APCFG/APRDCL/, see about CFG_MD_SBP_File.h
 *      the md_sbp_value of MD_SBP could be assigned by MTK_MD_SBP_CUSTOM_VALUE_ in ProjectConfig.mk
 *      if MTK_MD_SBP_CUSTOM_VALUE_=0, it means need SBP process flow, but not care ProjectConfig value.
 *    CIP_MD_SBP: under /custom/etc/firmware/, It is Hexadecimai number string, ex: 0x3
 *  Rules:
 *    wwop project use CIP_MD_SBP file, the number transform from CIP_MD_SBP MUST NOT be 0
 *        MTK_MD_SBP_CUSTOM_VALUE_ SHOULD be defined as 0x0(or 0) in ProjectConfig.mk of wwop project
 *    in non-wwop project:
 *      MTK_MD_SBP_CUSTOM_VALUE_ Defined: the MTK_MD_SBP_CUSTOM_VALUE_ should be transfer to md
 *      MTK_MD_SBP_CUSTOM_VALUE_ Undefined: the value in MD_SBP should be transfer to md
 *    The sb_code needn't transfer to md from the second boot up time, if the sb_code is not changed from the first boot.
 */
static int get_md_sbp_code(int md_id)
{
    int sbp_default = 0;
    int cip_sbp_value = 0;
    int stored_sbp_code = 0;
    static int sbpc = 0;
    int usp_sbp_value;

    /* NOTES:
    * priority: USP > CIP > ProjectConfig > meta tool
    * Assume:
    *    0. uniservice pack property for global device
    *    1. wwop(CIP) project could not be modified by meta tool
    *    2. ProjectConfiged project could not be modified by meta tool
    *    3. meta tool could modified project MUST NOT define MTK_MD_SBP_CUSTOM
    */
    usp_sbp_value =  get_usp_sbp_setting();
    if (usp_sbp_value >= 0) { /* Carrier 2.0 */
        sbpc = usp_sbp_value;
        CCCI_LOGI("Get: usp_sbp=%d\n", usp_sbp_value);
        return sbpc;
    }

    cip_sbp_value = get_cip_sbp_setting(md_id);
    sbp_default = get_project_sbp_code(md_id);
    stored_sbp_code = get_nvram_sbp_code(md_id);

    if (cip_sbp_value > 0)
        sbpc = cip_sbp_value;
    else if (sbp_default > 0)
        sbpc = sbp_default;
    else if (stored_sbp_code > 0)
        sbpc = stored_sbp_code;

    CCCI_LOGD("Get: usp_sbp=%d, cip_sbp=%d, project_sbp=%d, nvram_sbp=%d, set sbp=%d\n",
        usp_sbp_value, cip_sbp_value, sbp_default, stored_sbp_code, sbpc);

    return sbpc;
}

extern void* monitor_time_update_thread(void* arg);
extern int time_srv_init(void);

typedef enum {
    MODE_UNKNOWN = -1,      /* -1 */
    MODE_IDLE,              /* 0 */
    MODE_USB,               /* 1 */
    MODE_SD,                /* 2 */
    MODE_POLLING,           /* 3 */
    MODE_WAITSD,            /* 4 */
} LOGGING_MODE;
/* MD logger configure file */
#define MD1_LOGGER_FILE_PATH "/data/mdlog/mdlog1_config"
#define MD2_LOGGER_FILE_PATH "/data/mdlog/mdlog2_config"
#define MD3_LOGGER_FILE_PATH "/data/mdlog/mdlog3_config"
#define MDLOGGER_FILE_PATH   "/data/mdl/mdl_config"

static int get_mdlog_boot_mode(int md_id)
{
    int fd, ret;
    unsigned int mdl_mode = 0;
    char pBuildType[PROPERTY_VALUE_MAX] = {0};

    switch(md_id) {
    case 0:
        fd = open(MD1_LOGGER_FILE_PATH, O_RDONLY, 0660);
        if(fd < 0)
            fd = open(MDLOGGER_FILE_PATH, O_RDONLY, 0660);
        break;
    case 1:
        fd = open(MD2_LOGGER_FILE_PATH, O_RDONLY, 0660);
        break;
    case 3:
        fd = open(MD3_LOGGER_FILE_PATH, O_RDONLY, 0660);
        break;
    default:
        CCCI_LOGE("Open md_id=%d error!\n", md_id);
        fd = -1;
        break;
    }
    if (fd < 0) {
        CCCI_LOGE("Open md_log_config file failed, errno=%d!\n", errno);
        property_get("ro.build.type", pBuildType, "eng");
        if (0 == strcmp(pBuildType, "eng"))
                mdl_mode = MODE_SD;
        else
                mdl_mode = MODE_IDLE;
        return mdl_mode;
    }
    ret = read(fd, &mdl_mode, sizeof(unsigned int));
    if (ret != sizeof(unsigned int)) {
        CCCI_LOGE("read failed ret=%d, errno=%d!\n", ret, errno);
        property_get("ro.build.type", pBuildType, "eng");
        if (0 == strcmp(pBuildType, "eng"))
                mdl_mode = MODE_SD;
        else
                mdl_mode = MODE_IDLE;
    }
    close(fd);
    return mdl_mode;
}

static int get_md_dbg_dump_flag(int md_id)
{
    char buf[PROPERTY_VALUE_MAX] = { '\0' };
    int ret = -1; /* equal to 0xFFFFFFFF as Uint32 in kernel */

    if (md_id == MD_SYS1)
        property_get("persist.ccci.md1.dbg_dump", buf, "none");
    else if (md_id == MD_SYS3)
        property_get("persist.ccci.md3.dbg_dump", buf, "none");

    if (0 != strcmp(buf, "none"))
        ret = strtoul(buf, NULL, 16);
    return ret;
}

int set_md_boot_env_data(int md_id, int fd)
{
    unsigned int data[16] = { 0 };

    data[0] = get_mdlog_boot_mode(md_id);
    data[1] = get_md_sbp_code(md_id);
    data[2] = get_md_dbg_dump_flag(md_id);
    data[3] = get_sbp_subid_setting();
    CCCI_LOGI("set md boot data:mdl=%d sbp=%d dbg_dump=%d sbp_sub=%d\n", data[0], data[1], data[2], data[3]);
    ioctl(fd, CCCI_IOC_SET_BOOT_DATA, &data);
    return 0;
}

/* Util function */
static int get_ft_inf_key_val_helper(char key[], int *pval, char str[], int length)
{
    int i;
    char curr_key[64];
    char curr_val[64];
    int step = 0;
    int key_size = 0;
    int val_size = 0;

    for (i = 0; i < length; i++){
        if (step == 0) {
            /* Get key step */
            if (((str[i] >= 'a') && (str[i] <= 'z')) ||
                ((str[i] >= 'A') && (str[i] <= 'Z')) ||
                ((str[i] >= '0') && (str[i] <= '9')) || (str[i]=='_')) {
                curr_key[key_size] = str[i];
                key_size++;
            } else if (str[i]==':') {
                step = 1;
            } else {
                /* Invalid raw string */
                return -1;
            }
        } else if (step == 1) {
            /* Get key step */
            if ((str[i] >= '0') && (str[i] <= '9')) {
                curr_val[val_size] = str[i];
                val_size++;
            } else if ((str[i]==',') || (str[i]=='\0')){
                /* find a key:val pattern? */
                if (val_size == 0) {
                    /* val size abnormal */
                    return -1;
                }
                curr_key[key_size] = 0;
                curr_val[val_size] = 0;
                if (strcmp(key, curr_key) != 0) {
                    key_size = 0;
                    val_size = 0;
                    step = 0;
                } else {
                    *pval = atoi(curr_val);
                    return 1;
                }
            } else {
                /* Invalid raw string */
                return -1;
            }
        } /* end of if (step...) */
    }/* end of for (... */

    curr_key[key_size] = 0;
    curr_val[val_size] = 0;
    if (strcmp(key, curr_key) != 0)
        return 0;

    if (val_size == 0)
        return -1;

    *pval = atoi(curr_val);
    return 1;
}

static int get_ft_inf_key_val(char key[], int *p_val)
{
    int fd;
    int ft_raw_size;
    char *p_ft_raw;
    int ret;
    fd = open("/sys/kernel/ccci/ft_info", O_RDONLY);
    if (fd < 0)
        return -11;

    p_ft_raw = malloc(4096);
    if (p_ft_raw == NULL) {
        close(fd);
        return -12;
    }

    ft_raw_size = read(fd, p_ft_raw, 4096);
    if (ft_raw_size <= 0) {
        free(p_ft_raw);
        p_ft_raw = NULL;
        close(fd);
        return -13;
    }

    ret = get_ft_inf_key_val_helper(key, p_val, p_ft_raw, ft_raw_size);
    free(p_ft_raw);
    p_ft_raw = NULL;
    close(fd);

    return ret;
}

static int get_int_property_val(const char *name, const char *desired_value)
{
    char value[PROPERTY_VALUE_MAX] = {'\0'};
    int retpropget = 0;
    int val;

    retpropget = property_get(name, value, desired_value);
    if (retpropget > 0)
        val = atoi(value);
    else
        val = atoi(desired_value);

    CCCI_LOGI("get_int_property_val: %s [%d]", name, val);
    return val;
}

int rild_generation(void)
{
    char value[PROPERTY_VALUE_MAX] = {'\0'};
    int retpropget = 0;
    int ril_gen = 0;

    retpropget = property_get("ro.mtk_ril_mode", value, "legacy chip");
    if ((retpropget > 0) && (strcmp(value, "c6m_1rild") == 0))
        ril_gen = 1;

    CCCI_LOGI("rild_generation value: %s [%d]", value, ril_gen);
    return ril_gen;
}

static void wait_for_rst_lock_free(int fd)
{
    int rst_cnt;
    int retry_cnt;

    if (fd < 0)
        CCCI_LOGD("Open bc port fail, ignore rst lock cnt check\n");
    else {
        retry_cnt = 10;
        while (retry_cnt-- > 0) {
            if (ioctl(fd, CCCI_IOC_GET_HOLD_RST_CNT, &rst_cnt) < 0) {
                CCCI_LOGD("get rst lock cnt fail\n");
                break;
            }
            if (rst_cnt == 0)
                break;
            sleep(1);
            if (ioctl(fd, CCCI_IOC_SHOW_LOCK_USER, &rst_cnt) < 0)
                CCCI_LOGD("show rst lock user fail\n");
        }
    }
}

int main_v2(int input_md_id, int sub_ver)
{
    int ret, fd, tmp=0;
    ssize_t s;
    CCCI_BUFF_T buff;
    char nvram_init_val[PROPERTY_VALUE_MAX] = {0};
    char dev_port[32];
    pthread_t tid;
    unsigned int monitor_ch;
    int port_open_retry = MAX_OPEN_PORT_RETRY_NUM;
    int md_type = 0;
    char tmp_buf[4];
    char *lk_info_buf;
    int bc_fd;

    CCCI_LOGI("md_init ver:2.20, sub:%d", sub_ver);

    //Check if input parameter is valid
    md_id = input_md_id - 1;
    monitor_ch = CCCI_MONITOR_CH;

    set_current_md_status(CCCI_MD_STA_INIT);

    //Configure service and dev port name
    switch(md_id) {
    case 0:
        snprintf(dev_port, 32, "%s", ccci_get_node_name(USR_CCCI_CTRL, MD_SYS1));
        snprintf(muxd_name, 32, "%s", MUXD_FOR_MD1_NAME);
        snprintf(rild_name, 32, "%s", RILD_FOR_MD1_NAME);
        snprintf(mdlogger_name, 32, "%s", MD_LOG_FOR_MD1_NAME_E);
        snprintf(fsd_name, 32, "%s", FSD_FOR_MD1_NAME);
        break;
    case 1:
        snprintf(dev_port, 32, "%s", ccci_get_node_name(USR_CCCI_CTRL, MD_SYS2));
        snprintf(muxd_name, 32, "%s", MUXD_FOR_MD2_NAME);
        snprintf(rild_name, 32, "%s", RILD_FOR_MD2_NAME);
        snprintf(mdlogger_name, 32, "%s", MD_LOG_FOR_MD2_NAME_E);
        snprintf(fsd_name, 32, "%s", FSD_FOR_MD2_NAME);
        break;
    case 2:
        snprintf(dev_port, 32, "%s", ccci_get_node_name(USR_CCCI_CTRL, MD_SYS3));
        snprintf(muxd_name, 32, "%s", MUXD_FOR_MD3_NAME);
        snprintf(rild_name, 32, "%s", RILD_FOR_MD3_NAME);
        snprintf(mdlogger_name, 32, "%s", MD_LOG_FOR_MD3_NAME_E);
        snprintf(fsd_name, 32, "%s", FSD_FOR_MD3_NAME);
        break;
    default:
        CCCI_LOGE("[Error]Invalid md sys id: %d\n", md_id+1);
        return -1;
    }

    snprintf(muxd_service_status, PROPERTY_KEY_MAX, "%s%s", pre_service_status, muxd_name);
    snprintf(rild_service_status, PROPERTY_KEY_MAX, "%s%s", pre_service_status, rild_name);
    snprintf(mdlogger_service_status, PROPERTY_KEY_MAX, "%s%s", pre_service_status, mdlogger_name);
    snprintf(fsd_service_status, PROPERTY_KEY_MAX, "%s%s", pre_service_status, fsd_name);

    CCCI_LOGI("[%s][%s][%s][%s]\n",
        muxd_service_status,
        rild_service_status,
        mdlogger_service_status,
        fsd_service_status);

    snprintf(md_boot_name, 32, MD_INIT_NEW_FILE); // <== Using new md boot file

    check_decrypt_ready();

    // Retry to open if dev node attr not ready
    while(1) {
        fd = open(dev_port, O_RDWR);
        if (fd < 0) {
            port_open_retry--;
            if(port_open_retry>0) {
                usleep(10*1000);
                continue;
            } else {
                CCCI_LOGE("open %s fail: %d", dev_port, errno);
                return -1;
            }
        } else {
            CCCI_LOGD("%s is opened(%d).", dev_port, (MAX_OPEN_PORT_RETRY_NUM-port_open_retry));
            break;
        }
    }

    md_image_exist_check(fd, curr_md_id);

    // Get current MD type and update mux daemon name
    update_service_name();

    if(time_srv_init()==0){
        pthread_create(&tid, NULL, monitor_time_update_thread, NULL);
    }

    tmp = parse_sys_env_rat_setting();
    if(ioctl(fd, CCCI_IOC_RELOAD_MD_TYPE, &tmp) != 0)
        CCCI_LOGD("update modem type to kernel fail: err=%d", errno);

    ret = trigger_modem_to_run(fd, 0, 1);
    if (ret < 0)
        CCCI_LOGE("boot modem fail!\n");

    if( gotten_md_info == MD_DEBUG_REL_INFO_NOT_READY ){
        if(0 == ioctl(fd, CCCI_IOC_GET_MD_INFO, &tmp)){
            gotten_md_info = tmp;
            if(gotten_md_info == MD_IS_DEBUG_VERSION)
                CCCI_LOGD("MD is debug version");
            else if(gotten_md_info == MD_IS_RELEASE_VERSION)
                CCCI_LOGD("MD is release version");
            else{
                CCCI_LOGD("MD version is not ready now");
                gotten_md_info = MD_DEBUG_REL_INFO_NOT_READY;
            }
        }else{
            CCCI_LOGD("MD version is unknow: err=%d", errno);
        }
    }

    system_ch_handle = fd;
    bc_fd = open("/dev/ccci_mdx_sta", O_RDWR);
    /* block on reading CCCI_MONITOR device until a modem reset message is gotten */
    do {
        s = read(fd, (void *)&buff, sizeof(buff));
        if (s<=0) {
            if(s != -1) {
                CCCI_LOGE("read fail ret=%d\n", errno);
            }
            continue;
        } else if (s!= sizeof(buff)) {
            CCCI_LOGE("read CCCI data with unexpected size: %d\n", (int)s);
            continue;
            //return -1;
        }

        CCCI_LOGD("buff.data[0] = 0x%08X, data[1] = 0x%08X, channel = %08X, reserved = 0x%08X\n",
            buff.data[0], buff.data[1], buff.channel, buff.reserved);

        if ( (buff.data[0] == 0xFFFFFFFF) && (buff.channel == monitor_ch) ) { /* Monitor channel message */
            /* Check common message first */
            if (common_msg_handler(buff.data[1], buff.reserved)) {
                continue;
            }

            switch (buff.data[1]) {
            case CCCI_MD_MSG_FORCE_STOP_REQUEST:
                if (check_curret_md_status(CCCI_MD_STA_STOP))
                    break;
                if (check_curret_md_status(CCCI_MD_STA_FLIGHT_MODE))
                    break;
                delay_to_reset_md();
                tmp = 0;
                ret = ioctl(fd, CCCI_IOC_DO_STOP_MD, &tmp);
                wait_for_rst_lock_free(bc_fd);
                if (!is_meta_mode()) {
                    stop_all_ccci_up_layer_services();
                }
                set_current_md_status(CCCI_MD_STA_STOP);
                break;
            case CCCI_MD_MSG_FORCE_START_REQUEST:
                if (check_curret_md_status(CCCI_MD_STA_BOOT_READY))
                    break;

                trigger_modem_to_run(fd, 0, 0);
                break;

            case CCCI_MD_MSG_FLIGHT_STOP_REQUEST:
                if (check_curret_md_status(CCCI_MD_STA_STOP))
                    break;
                if (check_curret_md_status(CCCI_MD_STA_FLIGHT_MODE))
                    break;

                RLOGI("MD%d enter flight mode\n", md_id+1);
                tmp = 1;
                ret = ioctl(fd, CCCI_IOC_DO_STOP_MD, &tmp);
                set_current_md_status(CCCI_MD_STA_FLIGHT_MODE);
                break;
            case CCCI_MD_MSG_FLIGHT_START_REQUEST:
                if (check_curret_md_status(CCCI_MD_STA_BOOT_READY))
                    break;

                RLOGI("MD%d leave flight mode\n", md_id+1);
                trigger_modem_to_run(fd, 1, 0);
                break;

            case CCCI_MD_MSG_RESET_REQUEST:
                delay_to_reset_md();
                tmp = 0;
                ret = ioctl(fd, CCCI_IOC_DO_STOP_MD, &tmp);
                wait_for_rst_lock_free(bc_fd);
                if (!is_meta_mode())
                    stop_all_ccci_up_layer_services();
                set_current_md_status(CCCI_MD_STA_STOP);

                trigger_modem_to_run(fd, 0, 0);
                break;

            default:
                CCCI_LOGE("Invalid message, should not enter here!!!\n");
                break;
            }
        } else { /* pattern not invalid */
            CCCI_LOGE("[Error]Invalid pattern, data[0]:%08x channel:%08x\n", buff.data[0], buff.channel);
            continue;
        }
    } while (1);

    system_ch_handle = 0;

    return 0;
}

