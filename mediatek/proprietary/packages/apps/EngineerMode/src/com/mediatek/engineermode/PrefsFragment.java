/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.provider.Settings;
import android.widget.Toast;


import com.mediatek.engineermode.cip.CipUtil;
import com.mediatek.engineermode.cxp.CarrierExpressUtil;
import com.mediatek.internal.telephony.worldphone.WorldPhoneUtil;
import java.io.File;

public class PrefsFragment extends PreferenceFragment {
    private static final String TAG = "EM/PrefsFragment";
    private static int[] FRAGMENT_RES = {R.xml.telephony,
            R.xml.connectivity, R.xml.hardware_testing, R.xml.location,
            R.xml.log_and_debugging, R.xml.others};

    private static int[] FRAGMENT_RES_WIFIONLY = {
            R.xml.connectivity, R.xml.hardware_testing, R.xml.location,
            R.xml.log_and_debugging, R.xml.others};
    private static final String INNER_LOAD_INDICATOR_FILE =
            "/vendor/etc/system_update/address.xml";
    private static final int MTK_NFC_CHIP_TYPE_6605 = 0x02;
    private static final String[] KEY_REMOVE_ARRAY = {"de_sense", "display", "battery_log",
        "sensor", "io", "touchscreen", "memory"};
    private int mXmlResId;
    private boolean mIsInit = false;

    /**
     * Default empty constructor
     */
    public PrefsFragment() {

    }

    /**
     * Set this fragment resource
     *
     * @param resIndex
     *            Resource ID
     */
    public void setResource(int resIndex) {
        mXmlResId = resIndex;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(isWifiOnly()){
            FRAGMENT_RES = FRAGMENT_RES_WIFIONLY;
        }
        else if (UserHandle.MU_ENABLED && UserManager.supportsMultipleUsers()
               && UserManager.get(getActivity()).getUserHandle() != UserHandle.USER_OWNER) {
            FRAGMENT_RES = FRAGMENT_RES_WIFIONLY;
        }
    }
    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen screen, Preference pref) {
        if (FeatureSupport.isSupportedEmSrv()) {
            return super.onPreferenceTreeClick(screen, pref);
        }
        String id = pref.getKey();
        for (String c : KEY_REMOVE_ARRAY) {
            if (id.equals(c)) {
                Toast.makeText(getActivity(), R.string.notice_wo_emsvr,
                    Toast.LENGTH_LONG).show();
                return true;
            }
        }
        return super.onPreferenceTreeClick(screen, pref);
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    private void removePreference(PreferenceScreen prefScreen, String prefId) {
        Preference pref = (Preference) findPreference(prefId);
        if (pref != null) {
            prefScreen.removePreference(pref);
        }
    }

    private void removeUnsupportedItems() {
        PreferenceScreen screen = getPreferenceScreen();
        // em info only for 91 modem and later
        if (FeatureSupport.is90Modem() || FeatureSupport.is3GOnlyModem()) {
            removePreference(screen, "em_info");
            removePreference(screen, "mdm_em_info");
            removePreference(screen, "cdma_network_info");
            // LTE CA on/off only for 91 92 93 and later modem
            removePreference(screen, "ltecaconfig");
        }

        if(FeatureSupport.is93Modem()){
            removePreference(screen, "c2k_ir_settings");
            removePreference(screen, "cdma_network_select");
            removePreference(screen, "u3_phy");
            removePreference(screen, "em_info");
            removePreference(screen, "lte_network_info");
            removePreference(screen, "network_info");
            removePreference(screen, "cdma_network_info");
            removePreference(screen, "test_sim_switch");
        }
        else{
            removePreference(screen, "SIBCaptureActivity");
        }

        if(FeatureSupport.is91Modem() || FeatureSupport.is92Modem() ){
            removePreference(screen, "em_info");
            removePreference(screen, "lte_network_info");
            removePreference(screen, "network_info");
        }

        // anttunerdebug only for 92 93 and later modem
        if (FeatureSupport.is90Modem() || FeatureSupport.is91Modem()
                                       || FeatureSupport.is3GOnlyModem()) {
            removePreference(screen, "anttunerdebug");
        }
        if (  !( isTestSim()
           || FeatureSupport.isEngLoad() || FeatureSupport.isUserDebugLoad()
             || ChipSupport.isFeatureSupported(ChipSupport.MTK_USERLOAD_SUPPORT) ) ) {
            removePreference(screen, "auto_answer");
            Log.d("@M_" + TAG, "it is not an test sim card!");
        }else{
            Log.d("@M_" + TAG, "it is an test sim card or debug load");
        }

        if ( !( ChipSupport.isFeatureSupported(ChipSupport.MTK_USERLOAD_SUPPORT)
                || FeatureSupport.isEngLoad() || FeatureSupport.isUserDebugLoad() ) ) {
                Log.d("@M_" + TAG,"it is customer user load!");
                removePreference(screen, "atci");
                removePreference(screen, "bip");
                removePreference(screen, "voice_settings");
                removePreference(screen, "md_log_filter");
        }
        if ( FeatureSupport.isUserLoad()) {
            //telephony
            removePreference(screen, "amr_wb");
            removePreference(screen, "te_dev_tool");
            removePreference(screen, "cmas");
            removePreference(screen, "test_card");
            removePreference(screen, "lte_tool");
            removePreference(screen, "sbp");
            removePreference(screen, "simme_lock1");
            removePreference(screen, "simme_lock2");
            removePreference(screen, "u3_phy");
            removePreference(screen, "sim_switch");
            removePreference(screen, "md_em_filter");
            removePreference(screen, "mbim");
            removePreference(screen, "rat_config");
            removePreference(screen, "iatype");
            removePreference(screen, "conference");
            removePreference(screen, "channel_lock");

            //connectivity
            removePreference(screen, "cds_information");

            //hardware testing
            removePreference(screen, "camera89");
            removePreference(screen, "camerasolo");
            removePreference(screen, "camera_desence");
            removePreference(screen, "Device Manager");
            removePreference(screen, "de_sense");
            removePreference(screen, "io");
            removePreference(screen, "memory");
            removePreference(screen, "power");
            removePreference(screen, "touchscreen");
            removePreference(screen, "usb");
            removePreference(screen, "usbmode_switch");
            removePreference(screen, "smart_motion");

            //Location
            removePreference(screen, "fused_location_provider");
            removePreference(screen, "auto_dialer");
            removePreference(screen, "cw_test");
            removePreference(screen, "desense_at");
            removePreference(screen, "clk_quality_at");

            //log and debuging
            removePreference(screen, "battery_log");
            removePreference(screen, "modem_warning");
            removePreference(screen, "modem_debug");
            if (!FeatureSupport.isSupported(FeatureSupport.FK_MTK_TEL_LOG_SUPPORT)) {
                removePreference(screen, "telephony_log_setting");
            }

            //others
            removePreference(screen, "swla");
            removePreference(screen, "cip");
            removePreference(screen, "mdml_sample");
            removePreference(screen, "system_update");
            removePreference(screen, "usbacm");
            removePreference(screen, "saber");
            removePreference(screen, "usb_checker_enabler");
            removePreference(screen, "spc");
            removePreference(screen, "carrier_express");
        }

        int versionCode = Settings.Global.getInt(getActivity().
                getContentResolver(), "nfc_controller_code", -1);
        Log.i(TAG, "versionCode " + versionCode);
        if (versionCode != MTK_NFC_CHIP_TYPE_6605) {
            removePreference(screen, "hqanfc");
            removePreference(screen, "nfc_dta");
        }
        if (!ChipSupport.isFeatureSupported(ChipSupport.ST_NFC_SUPPORT)) {
            removePreference(screen, "nfc_st");
        }

        if (FeatureSupport.isSupported(FeatureSupport.FK_EVDO_DT_SUPPORT)
                || !"dsda".equals(FeatureSupport.getProperty(FeatureSupport.FK_MTK_DSDA_SUPPORT))
                || !FeatureSupport.isSupported(FeatureSupport.FK_MTK_C2K_SUPPORT)) {
            removePreference(screen, "ext_md_logger");
        }


        if (ChipSupport.isFeatureSupported(ChipSupport.MTK_FM_SUPPORT)) {
            if (!ChipSupport.isFeatureSupported(ChipSupport.MTK_FM_TX_SUPPORT)) {
                removePreference(screen, "fm_transmitter");
            }
        } else {
            removePreference(screen, "fm_receiver");
            removePreference(screen, "fm_transmitter");
        }

        // AGPS is not ready if MTK_AGPS_APP isn't defined
        if (!ChipSupport.isFeatureSupported(ChipSupport.MTK_AGPS_APP)
                || !ChipSupport.isFeatureSupported(ChipSupport.MTK_GPS_SUPPORT)
                || !isVoiceCapable() || isWifiOnly()) {
            removePreference(screen, "auto_dialer");
        }

        if (!ChipSupport.isFeatureSupported(ChipSupport.MTK_GPS_SUPPORT)) {
            removePreference(screen, "ygps");
            removePreference(screen, "cw_test");
        }

        // BT is not ready if MTK_BT_SUPPORT isn't defined
        if (!ChipSupport.isFeatureSupported(ChipSupport.MTK_BT_SUPPORT)) {
            removePreference(screen, "bluetooth");
        }

        // wifi is not ready if MTK_WLAN_SUPPORT isn't defined
        if (!ChipSupport.isFeatureSupported(ChipSupport.MTK_WLAN_SUPPORT)) {
            removePreference(screen, "wifi");
        }

        if (!FeatureSupport.isSupported(FeatureSupport.FK_APC_SUPPORT)) {
            removePreference(screen, "apc");
        }

        if (!isVoiceCapable() || isWifiOnly()) {
            removePreference(screen, "auto_answer");
        }

        // if it single sim, then the flow is the same as before
        if (TelephonyManager.getDefault().getSimCount() > 1) {
            /**
             * if it is Gemini, then the flow is : it start a TabActivity, then
             * the TabActivity will start sim1 or sim2 simLock activity Intent
             * to launch SIM lock TabActivity
             */
            // intent.setComponent(new
            // ComponentName("com.android.simmelock","com.android.simmelock.TabLockList"));
            removePreference(screen, "simme_lock1");
        } else {
            // Intent to launch SIM lock settings
            // intent.setComponent(new
            // ComponentName("com.android.simmelock","com.android.simmelock.LockList"));
            removePreference(screen, "simme_lock2");
        }
        Log.i("@M_" + TAG, "ChipSupport.getChip(): " + ChipSupport.getChip());
        Intent intent = new Intent();
        intent.setClassName("com.mediatek.emcamera", "com.mediatek.emcamera.AutoCalibration");
        if (isActivityAvailable(intent)) {
            removePreference(screen, "camera89");
        } else {
            Log.i("@M_" + TAG, "emcamera is not available, show engineermode->camera");
            removePreference(screen, "camerasolo");
        }

        if (ChipSupport.MTK_6797_SUPPORT <= ChipSupport.getChip()) {
            removePreference(screen, "camera_desence");
        }

        if (!FeatureSupport.isSupported(FeatureSupport.FK_FD_SUPPORT)) {
            removePreference(screen, "fast_dormancy");
        }

        File innerLoadIndicator = new File(INNER_LOAD_INDICATOR_FILE);
        if (!innerLoadIndicator.exists()) {
            removePreference(screen, "system_update");
        }

        if (!FeatureSupport.isPackageExisted(this.getActivity(), FeatureSupport.PK_CDS_EM)) {
            removePreference(screen, "cds_information");
        }

        Preference pref = (Preference) findPreference("cmas");
        if (pref != null && !isActivityAvailable(pref.getIntent())) {
            removePreference(screen, "cmas");
        }

        if (!FeatureSupport.isSupported(FeatureSupport.FK_EVDO_DT_SUPPORT)) {
            removePreference(screen, "saber");
        }

        String mOptr = SystemProperties.get("persist.operator.optr");
        String mOpSeg = SystemProperties.get("persist.operator.seg");
        if (!"OP01".equals(mOptr)) {
            removePreference(screen, "ConfigureCheck2_Send_Test");
            removePreference(screen, "ConfigureCheck2_Self_Test");
        }

        if (!"OP12".equals(mOptr)) {
            removePreference(screen, "spc");
        }

        if (!("OP09".equals(mOptr) &&
             ("SEGDEFAULT".equals(mOpSeg) || "SEGC".equals(mOpSeg)))) {
            removePreference(screen, "CT_ConfigureCheck");
        }
        if (!(("OP01".equals(mOptr)) &&
            (FeatureSupport.isSupported(FeatureSupport.FK_MTK_C2K_SUPPORT)))) {
            removePreference(screen, "c2k_ap_ir");
        }

        String usbCheckerState = SystemProperties.get("ro.mtk_usb_cba_support", "0");
        Log.i("@M_" + TAG, "ro.mtk_usb_cba_support is " + usbCheckerState);

        if (!usbCheckerState.equals("1")) {
            removePreference(screen, "usb_checker_enabler");
        }

        if (!SystemProperties.get("ro.mtk_cmcc_ft_precheck_support").equals("1")) {
            removePreference(screen, "cmcc_ftprecheck");
        }

        if (!SystemProperties.get("ro.mtk_modem_monitor_support").equals("1")) {
            removePreference(screen, "mdml_sample");
            removePreference(screen, "mdm_em_info");
            removePreference(screen, "mdm_config");
        }

        if (FeatureSupport.isSupported(FeatureSupport.FK_MTK_C2K_SUPPORT)) { // For C2K
            ;
        } else if (("OP09".equals(mOptr)) && ("SEGDEFAULT".equals(mOpSeg))) { // For CT
            removePreference(screen, "network_select");
        } else {
            removePreference(screen, "cdma_network_select");
        }

        if (!(("OP09".equals(mOptr)) && ("SEGDEFAULT".equals(mOpSeg)))) {
            removePreference(screen, "test_card");
        }

        if (!FeatureSupport.isSupported(FeatureSupport.FK_DEVREG_APP)) {
            removePreference(screen, "device_register");
            Log.i("@M_" + TAG, "Not show entry for DEVREG.");
        }

        if (!FeatureSupport.isSupported(FeatureSupport.FK_WFD_SUPPORT)) {
            removePreference(screen, "wfd_settings");
        }

        if (!FeatureSupport.isSupported(FeatureSupport.FK_LTE_SUPPORT)) {
            removePreference(screen, "lte_network_info");
        }

        if (!CipUtil.isCipSupported()) {
            removePreference(screen, "cip");
        }

        if (!CarrierExpressUtil.isCarrierExpressSupported()) {
            removePreference(screen, "carrier_express");
        }

        if (!FeatureSupport.isSupported(FeatureSupport.FK_LTE_SUPPORT)
                && ModemCategory.getModemType() == ModemCategory.MODEM_TD) {
            removePreference(screen, "antenna");
        }

        if (!FeatureSupport.isSupported(FeatureSupport.FK_LTE_SUPPORT)
                && ChipSupport.getChip() <= ChipSupport.MTK_6735_SUPPORT) {
            removePreference(screen, "antenna");
        }


        if (!FeatureSupport.isSupported(FeatureSupport.FK_VOLTE_SUPPORT)) {
            removePreference(screen, "ims");
            removePreference(screen, "conference");
        }

        if (! ( SystemProperties.get("ro.sim_me_lock_mode", "0").equals("0") ||
                SystemProperties.get("ro.sim_me_lock_mode", "0").equals("2"))
                ) {
           removePreference(screen, "simme_lock1");
           removePreference(screen, "simme_lock2");
        }

        if (!FeatureSupport.isSupported(FeatureSupport.FK_AAL_SUPPORT)) {
            removePreference(screen, "aal");
        }
        if (!FeatureSupport.isSupported(FeatureSupport.FK_MTK_EPDG_CONFIG)) {
            removePreference(screen, "epdg_config");
        }

        if (!FeatureSupport.isSupported(FeatureSupport.FK_TC1_FEATURE)) {
            removePreference(screen, "security_status");
            removePreference(screen, "antenna_diversity");
        } else {
            removePreference(screen, "antenna");
        }
        if (SystemProperties.get("ro.mtk_sensorhub_support").equals("1") == false) {
        //    removePreference(screen, "sensor_hub_test");
            removePreference(screen, "smart_motion");
        }

        if (ChipSupport.getChip() < ChipSupport.MTK_6735_SUPPORT) {
            removePreference(screen, "amr_wb");
        }
        if (!FeatureSupport.isSupported(FeatureSupport.FK_MTK_C2K_SUPPORT)) {
            removePreference(screen, "c2k_ir_settings");
        }

        if (!FeatureSupport.isSupported(FeatureSupport.FK_VILTE_SUPPORT)) {
             removePreference(screen, "vilte");
        }
        if (FeatureSupport.isSupported(FeatureSupport.FK_MTK_WEARABLE_PLATFORM)) {
            removePreference(screen, "camera89");
            removePreference(screen, "camerasolo");
            removePreference(screen, "camera_desence");
            removePreference(screen, "usb");
            removePreference(screen, "fm_receiver");
            removePreference(screen, "debug_utils");
            removePreference(screen, "modem_debug");
        }
        if (!FeatureSupport.isSupported(FeatureSupport.FK_MTK_C2K_SUPPORT)) {
            removePreference(screen, "bypass");
        }
        if (!FeatureSupport.isSupported(FeatureSupport.FK_MD_WM_SUPPORT)) {
            Log.d("@M_" + TAG, "FK_MD_WM_SUPPORT unsupport");
            removePreference(screen, "rat_config");
        }

        if (WorldPhoneUtil.isWorldPhoneSupport()) {
            if (WorldPhoneUtil.isWorldModeSupport()) {
                removePreference(screen, "modem_switch");
            } else {
                removePreference(screen, "world_mode");
            }
        } else {
            removePreference(screen, "world_mode");
            removePreference(screen, "modem_switch");
        }

        if (FeatureSupport.isSupported(FeatureSupport.FK_BSP_PACKAGE)) {
            removePreference(screen, "voice_settings");
            removePreference(screen, "wfd_settings");
            removePreference(screen, "spc");
            //telephony
            removePreference(screen, "pref_ecc_num_key");
        }

         //o0 bsp branch didn't support the feature
        if( (!FeatureSupport.isO1Branch()) &&
            FeatureSupport.isSupported(FeatureSupport.FK_BSP_PACKAGE) ){
            removePreference(screen, "auto_answer");
            removePreference(screen, "CT_ConfigureCheck");
        }

        //o0 bsp and tk branch didn't  support the feature
        if(!FeatureSupport.isO1Branch()){
            removePreference(screen, "iot_config");
        }

    }

    private boolean isVoiceCapable() {
        TelephonyManager telephony = (TelephonyManager) getActivity()
                .getSystemService(Context.TELEPHONY_SERVICE);
        boolean bVoiceCapable = telephony != null && telephony.isVoiceCapable();
        Log.i("@M_" + TAG, "sIsVoiceCapable : " + bVoiceCapable);
        return bVoiceCapable;
    }

    private boolean isWifiOnly() {
        ConnectivityManager connManager = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean bWifiOnly = false;
        if (null != connManager) {
            bWifiOnly = !connManager
                    .isNetworkSupported(ConnectivityManager.TYPE_MOBILE);
            Log.i("@M_" + TAG, "bWifiOnly: " + bWifiOnly);
        }
        return bWifiOnly;
    }
   private boolean isTestSim() {
        boolean isTestSim = SystemProperties.get("gsm.sim.ril.testsim").equals("1") ||
                            SystemProperties.get("gsm.sim.ril.testsim.2").equals("1") ||
                            SystemProperties.get("gsm.sim.ril.testsim.3").equals("1") ||
                            SystemProperties.get("gsm.sim.ril.testsim.4").equals("1");
        return isTestSim;
    }
    private boolean isActivityAvailable(Intent intent) {
        return null != getActivity().getPackageManager().resolveActivity(intent, 0);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        // TODO Auto-generated method stub
        super.setUserVisibleHint(isVisibleToUser);
        Log.i("@M_" + TAG, "setUserVisibleHint : " + isVisibleToUser + " index of " + mXmlResId);
        if (isVisibleToUser) {
            if (!mIsInit) {
                // Load preferences from xml.
                addPreferencesFromResource(FRAGMENT_RES[mXmlResId]);
                removeUnsupportedItems();
                mIsInit = true;
            }
            PreferenceScreen screen = getPreferenceScreen();

            int count = screen.getPreferenceCount();
            for (int i = 0; i < count; i++) {
                Preference pre = screen.getPreference(i);
                if (null != pre) {
                    Intent intent = pre.getIntent();
                    pre.setEnabled(isActivityAvailable(intent));
                }
            }
        }
    }


}
