package com.mediatek.engineermode.desenseat;

import java.util.HashMap;


/**
 * Band item information.
 *
 */
/**
 * @author mtk80357
 *
 */
public class BandItem {
    private String mBandName;
    private int mPower = -1;
    private int[] mChannleList = {-1 , -1 , -1};
    private int mBandValue = -1;
    private static HashMap<String, Integer[]> sDefaultData;
    private boolean mSelected = false;
    private boolean mIsULFreq = false;
    private BandType mBandType = BandType.BAND_NULL;

    /**
     * Telephony band type.
     *
     */
    enum BandType {
        BAND_NULL,
        BAND_GSM,
        BAND_WCDMA,
        BAND_TD,
        BAND_LTE_FDD,
        BAND_LTE_TDD,
        BAND_CDMA
    };

    /**
     * Constructor function.
     * @param bandName name for user to understand.
     * @param bandvalue value for real use with modem.
     * @param bandtype telephony band type.
     */
    public BandItem(String bandName, int bandvalue, BandType bandtype) {
        // TODO Auto-generated constructor stub
        mBandName = bandName;
        mBandValue = bandvalue;
        mBandType = bandtype;
//        Elog.d(TAG, "add " + mBandName + "  " + mBandName);
        addDefaultValue(mBandName);

    }

    /**
     * Constructor function.
     * @param bandName name for user to understand
     * @param bandvalue value for real use with modem
     * @param bandtype telephony band type
     * @param isULFreq special param for Lte
     */
    public BandItem(String bandName, int bandvalue, BandType bandtype, boolean isULFreq) {
        // TODO Auto-generated constructor stub
        this(bandName, bandvalue, bandtype);
        mIsULFreq = isULFreq;
    }

    public boolean isUlFreq() {
        return mIsULFreq;
    }
    public BandType getType() {
        return mBandType;
    }
    public String getBandName() {
        return mBandName;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }

    public int getPower() {
        return mPower;
    }

    public void setPower(int power) {
        mPower = power;
    }

    public int getBandValue() {
        return mBandValue;
    }

    public void setBandValue(int bandValue) {
        mBandValue = bandValue;
    }

    /**
     * @param ch1 channel 1
     * @param ch2 channel 2
     * @param ch3 channel 3
     */
    public void setChannel(int ch1, int ch2, int ch3) {
        mChannleList[0] = ch1;
        mChannleList[1] = ch2;
        mChannleList[2] = ch3;
    }

    public int[] getChannel() {
        return mChannleList;
    }

    /**
     * @return get information summary to show
     */
    public String getSummary() {
        StringBuilder summary = new StringBuilder(mIsULFreq ? "ULFreq: " : "Channel: ");
        boolean notSet = true;
        for (int k = 0 ; k < 3 ; k++) {
            if (mChannleList[k] != -1) {
                summary.append(mChannleList[k]).append("   ");
                notSet = false;
            }
        }
        if (notSet) {
            summary.append("NA    ");
        }

        summary.append((mPower == -1) ? "PWR:NA" : ("PWR: " + mPower));

        return summary.toString();
    }

    public boolean isSelected() {
        return mSelected;
    }

    static void initDefaultData() {
        sDefaultData = new HashMap<String, Integer[]>();
        sDefaultData.put("GSM 850", new Integer[]{192, -1, -1, 33, 128});
        sDefaultData.put("DCS 1800", new Integer[]{512, -1, -1, 33, 8});
        sDefaultData.put("WCDMA Band 2", new Integer[]{9262, 9400, 9538, 24, 2});
        sDefaultData.put("WCDMA Band 5", new Integer[]{4132, 4175, 4233, 24, 5});
        sDefaultData.put("Band F", new Integer[]{9404, -1, -1, 24, 6});
        sDefaultData.put("LTE(FDD) Band 3", new Integer[]{17100, -1, -1, 24, 2});
        sDefaultData.put("LTE(FDD) Band 5", new Integer[]{8240, 8365, 8489, 24, 4});
        sDefaultData.put("LTE(FDD) Band 13", new Integer[]{7770, 7869, -1, 24, 12});
        sDefaultData.put("LTE(FDD) Band 14", new Integer[]{7880, 7930, 7979, 24, 13});
        sDefaultData.put("LTE(FDD) Band 17", new Integer[]{7040, 7100, 7159, 24, 16});
        sDefaultData.put("LTE(FDD) Band 20", new Integer[]{8320, 8469, 8619, 24, 19});
        sDefaultData.put("LTE(TDD) Band 44", new Integer[]{46437, -1, -1, 24, 11});

    }

    private void addDefaultValue(String bandName) {
        Integer[] data = sDefaultData.get(bandName);
        if (data != null) {
            mChannleList[0] = data[0];
            mChannleList[1] = data[1];
            mChannleList[2] = data[2];
            mPower = data[3];
            mBandValue = data[4];
            mSelected = true;
        }
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "mBandName:" + mBandName + " Summary: " + getSummary();
    }

}
