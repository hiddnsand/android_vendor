/*
* Copyright (c) 2014 - 2016 MediaTek Inc.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files
* (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>

#include "tlctplay.h"
#include "tlctplay_log.h"
#include "MobiCoreDriverApi.h"

/* Error code */
#define NO_ERROR (0)

/*  Test status */
static unsigned int _tests_total  = 0; /* Number of conditions checked */
static unsigned int _tests_failed = 0; /* Number of conditions failed  */

/*
 *   Begin and end test macro
 */
#define TEST_BEGIN(name)                                        \
	bool _all_ok = true;                                    \
	const char *_test = name;                               \
	MULTI_LOG_I("%s:\n\n", _test);


#define TEST_END                                                \
{                                                               \
	if (_all_ok)                                            \
		MULTI_LOG_I("%s: PASSED\n\n", _test);                   \
	else                                                    \
		MULTI_LOG_I("%s: FAILED\n\n", _test);                   \
}

/*
 * EXPECT_* macros to check test results.
 */
#define EXPECT_EQ(expected, actual, msg)                        \
{                                                               \
	typeof(actual) _e = expected;                           \
	typeof(actual) _a = actual;                             \
	_tests_total++;                                         \
	if (_e != _a) {                                         \
		MULTI_LOG_E("%s: expected " #expected " (%d), "      \
		    "actual " #actual " (%d)\n",               \
		    msg, (int)_e, (int)_a);                     \
		_tests_failed++;                                \
		_all_ok = false;                                \
	}                                                       \
}

#define EXPECT_GT(expected, actual, msg)                        \
{                                                               \
	typeof(actual) _e = expected;                           \
	typeof(actual) _a = actual;                             \
	_tests_total++;                                         \
	if (_e <= _a) {                                         \
		MULTI_LOG_E("%s: expected " #expected " (%d), "      \
		    "actual " #actual " (%d)\n",                \
		    msg, (int)_e, (int)_a);                     \
		_tests_failed++;                                \
		_all_ok = false;                                \
	}                                                       \
}

#define EXPECT_GE_ZERO(actual, msg)                             \
{                                                               \
	typeof(actual) _a = actual;                             \
	_tests_total++;                                         \
	if (_a < 0) {                                           \
		MULTI_LOG_E("%s: expected >= 0 "                     \
		    "actual " #actual " (%d)\n", msg, (int)_a); \
		_tests_failed++;                                \
		_all_ok = false;                                \
	}                                                       \
}


#define EXPECT_GT_ZERO(actual, msg)                             \
{                                                               \
	typeof(actual) _a = actual;                             \
	_tests_total++;                                         \
	if (_a <= 0) {                                          \
		MULTI_LOG_E("%s: expected > 0 "                      \
		    "actual " #actual " (%d)\n", msg, (int)_a); \
		_tests_failed++;                                \
		_all_ok = false;                                \
	}                                                       \
}

static size_t read_file_content(
    const char* pPath,
    uint8_t** ppContent)
{
    FILE*   pStream;
    long    filesize;
    uint8_t* content = NULL;

   /*
    * The stat function is not used (not available in WinCE).
    */

   /* Open the file */
   pStream = fopen(pPath, "rb");
   if (pStream == NULL)
   {
      fprintf(stderr, "Error: Cannot open file: %s.\n", pPath);
      return 0;
   }

   if (fseek(pStream, 0L, SEEK_END) != 0)
   {
      fprintf(stderr, "Error: Cannot read file: %s.\n", pPath);
      goto error;
   }

   filesize = ftell(pStream);
   if (filesize < 0)
   {
      fprintf(stderr, "Error: Cannot get the file size: %s.\n", pPath);
      goto error;
   }

   if (filesize == 0)
   {
      fprintf(stderr, "Error: Empty file: %s.\n", pPath);
      goto error;
   }

   /* Set the file pointer at the beginning of the file */
   if (fseek(pStream, 0L, SEEK_SET) != 0)
   {
      fprintf(stderr, "Error: Cannot read file: %s.\n", pPath);
      goto error;
   }

   /* Allocate a buffer for the content */
   content = (uint8_t*)malloc(filesize);
   if (content == NULL)
   {
      fprintf(stderr, "Error: Cannot read file: Out of memory.\n");
      goto error;
   }

   /* Read data from the file into the buffer */
   if (fread(content, (size_t)filesize, 1, pStream) != 1)
   {
      fprintf(stderr, "Error: Cannot read file: %s.\n", pPath);
      goto error;
   }

   /* Close the file */
   fclose(pStream);
   *ppContent = content;

   /* Return number of bytes read */
   return (size_t)filesize;

error:
   if (content  != NULL)
   {
       free(content);
   }
   fclose(pStream);
   return 0;
}

static const uint32_t DEVICE_ID = MC_DEVICE_ID_DEFAULT;
typedef struct {
    struct {
        uint32_t	command;
        int32_t		response;
    };
} tciMessage_t, *tciMessage_ptr;

tciMessage_t *tci;
mcSessionHandle_t sessionHandle;

mcResult_t tlc_open(mcSpid_t spid, uint8_t* pTAData,  uint32_t nTASize)
{
    mcResult_t mcRet;
    mcVersionInfo_t versionInfo;

    LOG_I("Opening <t-base device");
    mcRet = mcOpenDevice(DEVICE_ID);
    if (MC_DRV_OK != mcRet) {
        LOG_E("Error opening device: %d", mcRet);
        return mcRet;
    }

    mcRet = mcGetMobiCoreVersion(MC_DEVICE_ID_DEFAULT, &versionInfo);
    if (MC_DRV_OK != mcRet) {
        LOG_E("mcGetMobiCoreVersion failed %d", mcRet);
        mcCloseDevice(DEVICE_ID);
        return mcRet;
    }
    LOG_I("productId        = %s", versionInfo.productId);
    LOG_I("versionMci       = 0x%08X", versionInfo.versionMci);
    LOG_I("versionSo        = 0x%08X", versionInfo.versionSo);
    LOG_I("versionMclf      = 0x%08X", versionInfo.versionMclf);
    LOG_I("versionContainer = 0x%08X", versionInfo.versionContainer);
    LOG_I("versionMcConfig  = 0x%08X", versionInfo.versionMcConfig);
    LOG_I("versionTlApi     = 0x%08X", versionInfo.versionTlApi);
    LOG_I("versionDrApi     = 0x%08X", versionInfo.versionDrApi);
    LOG_I("versionCmp       = 0x%08X", versionInfo.versionCmp);

	#ifdef WIN32
	mcRet = mcMallocWsm(DEVICE_ID, 0, sizeof(tciMessage_t), (uint8_t **)&tci, 0);
	if ((mcRet != MC_DRV_OK) || (tci == NULL)){
        LOG_E("Allocation of TCI failed");
        mcCloseDevice(DEVICE_ID);
        return MC_DRV_ERR_NO_FREE_MEMORY;
	}
	#else
    tci = (tciMessage_t*)malloc(sizeof(tciMessage_t));
    if (tci == NULL) {
        LOG_E("Allocation of TCI failed");
        mcCloseDevice(DEVICE_ID);
        return MC_DRV_ERR_NO_FREE_MEMORY;
    }
	#endif

    memset(tci, 0, sizeof(tciMessage_t));

    LOG_I("Opening the session");
    memset(&sessionHandle, 0, sizeof(sessionHandle));
    sessionHandle.deviceId = DEVICE_ID; // The device ID (default device is used)
    mcRet = mcOpenTrustlet(
            &sessionHandle,
            spid,
            pTAData,
            nTASize,
            (uint8_t *) tci,
            sizeof(tciMessage_t));
    if (MC_DRV_OK != mcRet) {
        LOG_E("Open session failed: %d", mcRet);
        #ifdef WIN32
        mcFreeWsm(DEVICE_ID, (uint8_t *)tci);
        #else
        free(tci);
        #endif
        tci = NULL;
        mcCloseDevice(DEVICE_ID);
    }
    else {
        LOG_I("open() succeeded");
    }

    return mcRet;
}

void tlc_close(void)
{
    mcResult_t ret;

    LOG_I("Closing the session.");
    ret = mcCloseSession(&sessionHandle);
    if (MC_DRV_OK != ret) {
        LOG_E("Closing session failed: %d.", ret);
        // Continue even in case of error.
    }

    LOG_I("Closing <t-base device.");
    ret = mcCloseDevice(DEVICE_ID);
    if (MC_DRV_OK != ret) {
        LOG_E("Closing <t-base device failed: %d.", ret);
        // Continue even in case of error.
    }
    free(tci);
    tci = NULL;
}

static int load_secure_services()
{
    uint8_t* pTAData = NULL;
    uint32_t nTASize;
    mcResult_t ret;

	/* read driver content - UUID : 05200000000000000000000000000000*/
    nTASize = read_file_content(
                        "/system/app/mcRegistry/05200000000000000000000000000000.tabin", // Not .tlbin
                        &pTAData);
    if (nTASize == 0) {
        LOG_E("Trusted Application not found");
        return MC_DRV_ERR_TRUSTLET_NOT_FOUND;
    }

	/* load driver */
    ret = tlc_open(MC_SPID_TRUSTONIC_OTA, pTAData, nTASize);
    free(pTAData);

	if (MC_DRV_OK != ret) {
		LOG_E("load secure driver failed: %d", ret);
		return ret;
	}

	/* load TA */
	ret = ta_open();
	if (MC_DRV_OK != ret)
		LOG_E("load TA failed: %d", ret);

	return ret;
}

static void close_secure_services()
{
	/* unload TA */
    ta_close();

	/* unload driver */
	tlc_close();
}

#define TOTAL_TEST_CASES 5
static void run_test_cases()
{
	int rc, ts_num = 0;
	char msg[32] = {0};

	TEST_BEGIN("CRYPTO CA TEST");

	/* reset test state */
	_tests_total  = 0;
	_tests_failed = 0;
	//MULTI_LOG_I("Run all tests\n");

	/* run test cases */
	for(ts_num = 1; ts_num <= TOTAL_TEST_CASES; ts_num++) {
		rc = ta_cmd_send(ts_num);
		sprintf(msg, "test case %d", ts_num);
		EXPECT_EQ(NO_ERROR, rc, msg);
	}

	/* summary check results */
	if (_tests_failed == 0)
		MULTI_LOG_I("All tests : PASS\n");
	else
		MULTI_LOG_I("Some tests : FAIL\n");

	TEST_END
}

int main(int argc, char *args[])
{
	int rc;

	(int)argc;
	(char*)args;

	rc = load_secure_services();
    if (MC_DRV_OK != rc) {
        MULTI_LOG_E("load secure service failed: %d", rc);
        return rc;
    }

	run_test_cases();
    close_secure_services();

    return rc;
}
