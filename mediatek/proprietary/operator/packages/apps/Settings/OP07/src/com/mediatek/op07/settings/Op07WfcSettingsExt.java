package com.mediatek.op07.settings;

import android.content.Context;
import android.os.SystemProperties;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;

import com.mediatek.settings.ext.DefaultWfcSettingsExt;

/**
 * Plugin implementation for WFC Settings plugin
 */

public class Op07WfcSettingsExt extends DefaultWfcSettingsExt {

    private static final String TAG = "Op07WfcSettingsExt";
    private static final String AOSP_KEY_WFC_SETTINGS = "wifi_calling_settings";
    private Context mContext;

    /** Constructor.
     * @param context context
     */
    public Op07WfcSettingsExt(Context context) {
        super();
        mContext = context;
    }

    /** Customize WFC pref as per operator requirement, on basis of MCC+MNC
        * Called from WirelessSettings
         * @param context context
         * @param preferenceScreen preferenceScreen
         * @return
         */
    @Override
    public void customizedWfcPreference(Context context, PreferenceScreen preferenceScreen) {

        if (isEntitlementEnabled()) {
            Preference wfcSettingsPreference = (Preference) preferenceScreen
                .findPreference(AOSP_KEY_WFC_SETTINGS);
            Log.d(TAG, "remove wfc preference:" + wfcSettingsPreference);
            if (preferenceScreen != null && wfcSettingsPreference != null) {
                preferenceScreen.removePreference(wfcSettingsPreference);
            }
        }
    }

    public static boolean isEntitlementEnabled() {
        boolean isEntitlementEnabled = (1 == SystemProperties.getInt
                ("persist.entitlement_enabled", 1) ? true : false);
        Log.d(TAG, "isEntitlementEnabled:" + isEntitlementEnabled);

        return isEntitlementEnabled;

    }
}
