/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M Camera "Java" implementation.
 */
#pragma once
#ifndef JNI_A3M_CAMERA_H
#define JNI_A3M_CAMERA_H

#include <a3m/camera.h>
#include <a3m_scenenode.h>

class A3mCamera : public A3mSceneNode
{
private:
  A3mCamera(a3m::Camera::Ptr const& native) :
    A3mSceneNode(native)
  {
  }

  // Avoid the minor inefficiency in getNativeCamera() of creating a SharedPtr.
  a3m::Camera* getNativeRaw() const
  {
    return static_cast<a3m::Camera*>(getNative().get());
  }

public:
  // The following functions are ignored by SWIG (see a3m.i)
  static A3mCamera* toWrapper(a3m::Camera::Ptr const& native)
  {
    return native ? new A3mCamera(native) : 0;
  }

  static a3m::Camera::Ptr toNative(A3mCamera* wrapper)
  {
    return wrapper ? wrapper->getNativeCamera() : a3m::Camera::Ptr();
  }

  a3m::Camera::Ptr getNativeCamera() const
  {
    return a3m::static_ptr_cast<a3m::Camera>(getNative());
  }

public:
    void setProjectionType(int type)
    {
      getNativeRaw()->setProjectionType(
          static_cast<a3m::Camera::ProjectionType>(type));
    }

    int getProjectionType() const
    {
      return static_cast<int>(getNativeRaw()->getProjectionType());
    }

    void setFov(int units, float fov)
    {
      getNativeRaw()->setFov(
          a3m::Anglef(static_cast<a3m::Anglef::Units>(units), fov));
    }

    float getFov(int units) const
    {
      return getNativeRaw()->getFov().get(
          static_cast<a3m::Anglef::Units>(units));
    }

    void setWidth(float width)
    {
      getNativeRaw()->setWidth(width);
    }

    float getWidth() const
    {
      return getNativeRaw()->getWidth();
    }

    void setNear(float near_)
    {
      getNativeRaw()->setNear(near_);
    }

    float getNear() const
    {
      return getNativeRaw()->getNear();
    }

    void setFar(float far_)
    {
      getNativeRaw()->setFar(far_);
    }

    float getFar() const
    {
      return getNativeRaw()->getFar();
    }

    void setStereo(float focalDistance, float eyeSeparation)
    {
      getNativeRaw()->setStereo(focalDistance, eyeSeparation);
    }

    bool isStereo() const

    {
      return getNativeRaw()->isStereo();
    }
};

#endif // JNI_A3M_CAMERA_H
