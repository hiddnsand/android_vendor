package com.mediatek.ims.ext;

import android.content.Context;

/** Factory class to make default plugin objects.
  */
public class OpImsServiceCustomizationFactoryBase {
    /**
    * An API to make ImsService default impl object.
    * @param context context
    * @return IImsServiceExt
    */
    public IImsServiceExt makeImsServiceExt(Context context) {
         return new ImsServiceExt(context);
    }
}
