 /*
 * Copyright (c) 2013 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#define LOG_TAG "secmem_tlc"

/* LOG_TAG need to be defined before include log.h */
#include <android/log.h>

#include <utils/CallStack.h>
#include <utils/String8.h>

#define SKIP_DEBUG_ALIAS_MACRO
#include "secmem_tlc.h"    /* secure memory tlc api */
#include "secmem_ioctl.h"  /* secure memory kernel ioctl */

using namespace android;

#ifdef MEMCHECK_DEBUG
#define SEC_MEMCHECK_TAG "SEC_MEMCHECK"
#define SEC_MEMCHECK(handle) \
do { \
	LOG_I("SEC_MEMCHECK_BEGIN"); \
	String8 logtag = String8::format("%s[0x%x]", SEC_MEMCHECK_TAG, handle); \
	CallStack stack(String8::format("%s[0x%x]", \
		SEC_MEMCHECK_TAG, handle).string()); \
	LOG_I("SEC_MEMCHECK_END"); \
} while(0)
#else
#define SEC_MEMCHECK(handle)    do {} while(0)
#endif

#define MAX_NAME_SZ         (32)

#define LOG_D(...) __android_log_print(ANDROID_LOG_DEBUG , LOG_TAG, __VA_ARGS__)
#define LOG_I(...) __android_log_print(ANDROID_LOG_INFO  , LOG_TAG, __VA_ARGS__)
#define LOG_W(...) __android_log_print(ANDROID_LOG_WARN  , LOG_TAG, __VA_ARGS__)
#define LOG_E(...) __android_log_print(ANDROID_LOG_ERROR , LOG_TAG, __VA_ARGS__)

static unsigned int open_device_count = 0;
static int secmem_fd = -1;

pthread_mutex_t mem_mutex = PTHREAD_MUTEX_INITIALIZER;

typedef struct {
	uint32_t    alignment;  /* IN */
	uint32_t    size;       /* IN */
	uint32_t    refcount;   /* INOUT */
	uint32_t    sec_handle; /* OUT */
	uint32_t    id;
	uint8_t    *owner;
} UREE_SECMEM_PARAM;

static uint32_t tlcMemOpenInternal(void)
{
	uint32_t ret = UREE_SUCCESS;

	/* check if fd is already opened */
	open_device_count++;
	if (open_device_count > 1) {
		LOG_I("tlcMemOpenInternal: ignore this time, open_device_count: %d, fd: %d", open_device_count, secmem_fd);
		goto exit;
	}

	/* open the secure memory device */
	secmem_fd = open(SECMEM_DEV, O_RDWR, 0);
	if (secmem_fd < 0) {
		LOG_E("tlcMemOpenInternal: open device file failed for R/W: %d (%s)", errno, strerror(errno));

		secmem_fd = open(SECMEM_DEV, O_RDONLY, 0);
		if (secmem_fd < 0) {
			ret = UREE_ERR_OPEN_DEV_FAIL;
			open_device_count--;
			goto exit;
		}
		LOG_E("tlcMemOpenInternal: open device file for read only");
	}

	LOG_I("tlcMemOpenInternal: open secure memory driver success, open_device_count: %d, fd: %d", open_device_count, secmem_fd);

exit:

	return ret;
}

uint32_t tlcMemOpen(void)
{
	uint32_t ret;

	pthread_mutex_lock(&mem_mutex);

	LOG_I("tlcMemOpen: opening secure memory driver");

	ret = tlcMemOpenInternal();

	pthread_mutex_unlock(&mem_mutex);

	return ret;
}

uint32_t tlcMemClose(void)
{
	uint32_t ret = UREE_SUCCESS;

	pthread_mutex_lock(&mem_mutex);

	/* close file description */
	LOG_I("tlcMemClose: closing secure memory driver");

	if (open_device_count == 0) {
		LOG_I("tlcMemClose: device is still not opened, open_device_count: %d", open_device_count);
	} else if(open_device_count > 1) {
		open_device_count--;
		LOG_I("tlcMemClose: can't really close the device, remained open_device_count: %d", open_device_count);
	} else {
		open_device_count--;
		LOG_I("tlcMemClose: close device, open_device_count: %d, fd: %d", open_device_count, secmem_fd);
		ret = close(secmem_fd);
		secmem_fd = -1;
	}

	pthread_mutex_unlock(&mem_mutex);

	return ret;
}

static uint32_t executeCmd(UREE_SECMEM_PARAM *sec_mem_param, unsigned int ioctl_cmd)
{
	uint32_t ret = UREE_SUCCESS;
	struct secmem_param ioctl_param;

	memset(&ioctl_param, 0, sizeof(struct secmem_param));

	/* execute IOCTL commands */
	if (secmem_fd < 0) {
		LOG_E("executeCmd: device is still not opened: %d (try open)", secmem_fd);
		ret = UREE_ERR_INVALID_OPERATION;
		/* Need? to be refined */
		if ((ret = tlcMemOpenInternal()) != UREE_SUCCESS) {
			LOG_E("executeCmd: try open failed, open_device_count: %d, fd: %d", open_device_count, secmem_fd);
			goto exit;
		}
	}

	ioctl_param.alignment = sec_mem_param->alignment;
	ioctl_param.refcount = sec_mem_param->refcount;
	ioctl_param.sec_handle = sec_mem_param->sec_handle;
	ioctl_param.size = sec_mem_param->size;
	ioctl_param.id = sec_mem_param->id;
	if (sec_mem_param->owner) {
		char *owner = strrchr((char*)sec_mem_param->owner, '/');
		int len;
		owner = (owner == NULL)? (char*)sec_mem_param->owner : owner+1;
		len = strlen(owner) + 1;
		len = len > MAX_NAME_SZ ? MAX_NAME_SZ : len;
		ioctl_param.owner_len = len;
		memcpy(ioctl_param.owner, owner, len);
		ioctl_param.owner[MAX_NAME_SZ - 1] = '\0';
	} else {
		strcpy((char*)ioctl_param.owner,"Unknown");
		ioctl_param.owner_len = strlen((char*)ioctl_param.owner)+1;
	}

	if ((ret = ioctl(secmem_fd, ioctl_cmd, &ioctl_param)) != 0) {
		LOG_E("executeCmd: ioctl for command %d failed: %d (%s)", _IOC_NR(ioctl_cmd), ret, strerror(errno));
		goto exit;
	}

	/* get responses */
	sec_mem_param->alignment = ioctl_param.alignment;
	sec_mem_param->refcount = ioctl_param.refcount;
	sec_mem_param->sec_handle = ioctl_param.sec_handle;
	sec_mem_param->size = ioctl_param.size;

exit:

	return ret;
}

//------------------------------------
uint32_t UREE_AllocSecuremem(UREE_SECUREMEM_HANDLE *mem_handle, uint32_t alignment, uint32_t size, uint8_t *owner, uint32_t id)
{
	uint32_t ret;
	UREE_SECMEM_PARAM sec_mem_param;

	LOG_I("SEC mem perform allocation");
	pthread_mutex_lock(&mem_mutex);

	sec_mem_param.alignment = alignment;
	sec_mem_param.size = size;
	sec_mem_param.refcount = 0;
	sec_mem_param.sec_handle = 0;
	sec_mem_param.id = id;
	sec_mem_param.owner = owner;

	ret = executeCmd(&sec_mem_param, SECMEM_MEM_ALLOC);

	*mem_handle = sec_mem_param.sec_handle;

	if (ret != UREE_SUCCESS) {
		LOG_E("Unable to execute CMD_SEC_MEM_ALLOC command: %d", ret);
		goto exit;
	}

	LOG_I("The handle in secure world is (0x%x)", *mem_handle);

	SEC_MEMCHECK(*mem_handle);

exit:
	pthread_mutex_unlock(&mem_mutex);
	return ret;
}

uint32_t UREE_AllocSecurememZero(UREE_SECUREMEM_HANDLE *mem_handle, uint32_t alignment, uint32_t size, uint8_t *owner, uint32_t id)
{
	uint32_t ret;
	UREE_SECMEM_PARAM sec_mem_param;

	LOG_I("SEC mem perform allocation (Zero)");
	pthread_mutex_lock(&mem_mutex);

	sec_mem_param.alignment = alignment | 0x1;
	sec_mem_param.size = size;
	sec_mem_param.refcount = 0;
	sec_mem_param.sec_handle = 0;
	sec_mem_param.id = id;
	sec_mem_param.owner = owner;

	ret = executeCmd(&sec_mem_param, SECMEM_MEM_ALLOC);

	*mem_handle = sec_mem_param.sec_handle;

	if (ret != UREE_SUCCESS) {
		LOG_E("Unable to execute CMD_SEC_MEM_ALLOC command: %d", ret);
		goto exit;
	}

	LOG_I("The handle in secure world is (0x%x)", *mem_handle);

	SEC_MEMCHECK(*mem_handle);

exit:
	pthread_mutex_unlock(&mem_mutex);
	return ret;
}

uint32_t UREE_ReferenceSecuremem(UREE_SECUREMEM_HANDLE mem_handle, uint32_t *refcount, uint8_t *owner, uint32_t id)
{
	/* Need to implement whole Picture */
	uint32_t ret;
	UREE_SECMEM_PARAM sec_mem_param;

	LOG_I("SEC mem perform Reference, handle(0x%x)", mem_handle);
	pthread_mutex_lock(&mem_mutex);

	sec_mem_param.sec_handle = mem_handle;
	sec_mem_param.id = id;
	sec_mem_param.owner = owner;

	ret = executeCmd(&sec_mem_param, SECMEM_MEM_REF);

	*refcount = sec_mem_param.refcount;

	if (ret != UREE_SUCCESS) {
		LOG_E("Unable to execute CMD_SEC_MEM_REF command: %d", ret);
		goto exit;
	}

	LOG_I("The refcount is (0x%x)", *refcount);

exit:
	pthread_mutex_unlock(&mem_mutex);
	return ret;
}

uint32_t UREE_UnreferenceSecuremem(UREE_SECUREMEM_HANDLE mem_handle, uint32_t *refcount, uint8_t *owner, uint32_t id)
{
	/* Need to implement whole Picture */
	uint32_t ret;
	UREE_SECMEM_PARAM sec_mem_param;

	LOG_I("SEC mem perform Unreference, handle(0x%x)", mem_handle);
	pthread_mutex_lock(&mem_mutex);

	//sec_mem_param.refcount = 0;
	sec_mem_param.sec_handle = mem_handle;
	sec_mem_param.id = id;
	sec_mem_param.owner = owner;

	ret = executeCmd(&sec_mem_param, SECMEM_MEM_UNREF);

	*refcount = sec_mem_param.refcount;

	if (ret != UREE_SUCCESS) {
		LOG_E("Unable to execute CMD_SEC_MEM_UNREF command: %d", ret);
		goto exit;
	}

	LOG_I("The refcount is (0x%x)", *refcount);

exit:
	pthread_mutex_unlock(&mem_mutex);
	return ret;
}

uint32_t UREE_AllocSecurememTBL(UREE_SECUREMEM_HANDLE *mem_handle, uint32_t alignment, uint32_t size, uint8_t *owner, uint32_t id)
{
	uint32_t ret;
	UREE_SECMEM_PARAM sec_mem_param;

	LOG_I("SEC mem perform allocation with TBL");
	pthread_mutex_lock(&mem_mutex);

	sec_mem_param.alignment = alignment;
	sec_mem_param.size = size;
	sec_mem_param.refcount = 0;
	sec_mem_param.sec_handle = 0;
	sec_mem_param.id = id;
	sec_mem_param.owner = owner;

	ret = executeCmd(&sec_mem_param, SECMEM_MEM_ALLOC_TBL);

	*mem_handle = sec_mem_param.sec_handle;

	if (ret != UREE_SUCCESS) {
		LOG_E("Unable to execute CMD_SEC_MEM_ALLOC_TBL command: %d", ret);
		goto exit;
	}

	LOG_I("The handle in secure world is (0x%x)", *mem_handle);

	SEC_MEMCHECK(*mem_handle);

exit:
	pthread_mutex_unlock(&mem_mutex);
	return ret;
}

uint32_t UREE_AllocSecurememTBLZero(UREE_SECUREMEM_HANDLE *mem_handle, uint32_t alignment, uint32_t size, uint8_t *owner, uint32_t id)
{
	uint32_t ret;
	UREE_SECMEM_PARAM sec_mem_param;

	LOG_I("SEC mem perform allocation with TBL (Zero)");
	pthread_mutex_lock(&mem_mutex);

	sec_mem_param.alignment = alignment | 0x1;
	sec_mem_param.size = size;
	sec_mem_param.refcount = 0;
	sec_mem_param.sec_handle = 0;
	sec_mem_param.id = id;
	sec_mem_param.owner = owner;

	ret = executeCmd(&sec_mem_param, SECMEM_MEM_ALLOC_TBL);

	*mem_handle = sec_mem_param.sec_handle;

	if (ret != UREE_SUCCESS) {
		LOG_E("Unable to execute CMD_SEC_MEM_ALLOC_TBL command: %d", ret);
		goto exit;
	}

	LOG_I("The handle in secure world is (0x%x)", *mem_handle);

	SEC_MEMCHECK(*mem_handle);

exit:
	pthread_mutex_unlock(&mem_mutex);
	return ret;
}

uint32_t UREE_UnreferenceSecurememTBL(UREE_SECUREMEM_HANDLE mem_handle, uint32_t *refcount, uint8_t *owner, uint32_t id)
{
	/* Need to implement whole Picture */
	uint32_t ret;
	UREE_SECMEM_PARAM sec_mem_param;

	LOG_I("SEC mem perform Unreference with TBL, handle(0x%x)", mem_handle);
	pthread_mutex_lock(&mem_mutex);

	sec_mem_param.sec_handle = mem_handle;
	sec_mem_param.id = id;
	sec_mem_param.owner = owner;

	ret = executeCmd(&sec_mem_param, SECMEM_MEM_UNREF_TBL);

	*refcount = sec_mem_param.refcount;

	if (ret != UREE_SUCCESS) {
		LOG_E("Unable to execute CMD_SEC_MEM_UNREF_TBL command: %d", ret);
		goto exit;
	}

	LOG_I("The refcount is (0x%x)", *refcount);

exit:
	pthread_mutex_unlock(&mem_mutex);
	return ret;
}


uint32_t UREE_DumpSecurememInfo(void)
{
	uint32_t ret;
	UREE_SECMEM_PARAM sec_mem_param;

	memset(&sec_mem_param, 0, sizeof(UREE_SECMEM_PARAM));

	pthread_mutex_lock(&mem_mutex);

	ret = executeCmd(&sec_mem_param, SECMEM_MEM_DUMP_INFO);

	pthread_mutex_unlock(&mem_mutex);
	return ret;
}

