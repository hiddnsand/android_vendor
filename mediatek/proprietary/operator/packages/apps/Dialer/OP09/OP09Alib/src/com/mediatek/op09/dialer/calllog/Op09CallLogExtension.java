package com.mediatek.op09.dialer.calllog;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mediatek.dialer.ext.DefaultCallLogExtension;
import com.mediatek.op09.dialer.Op09DialerPluginUtil;
import com.mediatek.op09.dialer.R;

public class Op09CallLogExtension extends DefaultCallLogExtension {
    private static final String TAG = "Op09CallLogExtension";
    private static final String ITEM_TYPE = "call_type";
    private static final String ACCOUNT_LABEL = "call_account_label";
    private static final String ID = "id";
    public static final int ICON_ID = 100;

    /**
     *for OP09
     * set account for call log list
     *
     * @param Context context
     * @param View view
     * @param PhoneAccountHandle phoneAccountHandle
     */
    public void setCallAccountForCallLogList(Context context, View view,
                                             PhoneAccountHandle phoneAccountHandle) {
        log("setCallAccountForCallLogList");
        TelephonyManager telephonyManager = (TelephonyManager) context.
                getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.getDefault().getPhoneCount() < 2) {
            return;
        }
        Op09DialerPluginUtil dialerPlugin = new Op09DialerPluginUtil(context);
        Context pluginContext = dialerPlugin.getPluginContext();
        Resources resource = context.getResources();
        String packageName = context.getPackageName();
        TelecomManager telecomManager = (TelecomManager) context.getSystemService(
                Context.TELECOM_SERVICE);
        PhoneAccount account = telecomManager.getPhoneAccount(phoneAccountHandle);

        View callType =
                view.findViewById(
                        resource.getIdentifier(ITEM_TYPE, ID, packageName));

        ViewGroup callLogItem = null;
        if (callType != null) {
            callLogItem = (ViewGroup) callType.getParent();
        }

        View accountLable =
                view.findViewById(
                        resource.getIdentifier(ACCOUNT_LABEL, ID, packageName));
        ViewGroup viewChild = (ViewGroup) callLogItem.findViewById(ICON_ID);

        /// no account info.
        if (phoneAccountHandle == null || account == null) {
            if (viewChild != null) {
                viewChild.setVisibility(View.GONE);
            }
            if (accountLable != null) {
                accountLable.setVisibility(View.GONE);
            }
            return;
        }

        if (null == view || null == context) {
            return;
        }

        if (accountLable != null) {
            callLogItem.removeView(accountLable);
            accountLable.setVisibility(View.VISIBLE);
        } else {
            log("Call log items, but no account lable.");
            return;
        }

        Bitmap iconBitmap = account.getIcon().getBitmap();

        if (null == viewChild && null != iconBitmap && null != callLogItem) {
            LayoutInflater mInflater;
            mInflater = LayoutInflater.from(pluginContext);
            ViewGroup iconLayout = (ViewGroup) mInflater.inflate(R.layout.ct_sim_indicator, null);
            iconLayout.setId(ICON_ID);
            ImageView simIndicator = (ImageView)iconLayout.findViewById(R.id.simIndicator);
            simIndicator.setVisibility(View.VISIBLE);
            simIndicator.setImageDrawable(new BitmapDrawable(iconBitmap));
            iconLayout.addView(accountLable, 1);
            callLogItem.addView(iconLayout,3);
        } else if (null != iconBitmap && null != callLogItem) {
            viewChild.setVisibility(View.VISIBLE);
            View simIndicator = viewChild.getChildAt(0);
            if (null != simIndicator && (simIndicator instanceof ImageView)) {
                simIndicator.setVisibility(View.VISIBLE);
                ((ImageView)simIndicator).setImageDrawable(new BitmapDrawable(iconBitmap));
            }
        }
    }

    private void log(String msg) {
        Log.d(TAG, msg);
    }

    /**
     * for op09.
     * plug-in whether show sim label account or not
     * @return Account null or not
     */
    public boolean shouldReturnAccountNull() {
        return false;
    }
}

