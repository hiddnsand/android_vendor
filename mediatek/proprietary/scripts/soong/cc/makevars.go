package mtkCc

import (
	"os"
	"path/filepath"
	"strings"

	"android/soong/android"
	"android/soong/android/mediatek"
)

var (
	pctx = android.NewPackageContext("android/soong/android/mediatek")
)

func init() {
	android.RegisterMakeVarsProvider(pctx, mtkMakeVarsProvider)
	pctx.Import("android/soong/cc/config")
}

func mtkGlobalDefine(quote bool) ([]string, []string) {
	var cflags, includes []string
	mtkBasicPackage := mediatek.GetFeature("MTK_BASIC_PACKAGE")
	mtkEmulatorSupport := mediatek.GetFeature("MTK_EMULATOR_SUPPORT")
	if (mtkBasicPackage != "yes") && (mtkEmulatorSupport != "yes") {
		cflags = append(cflags, "-DMTK_AOSP_ENHANCEMENT")
	}
	if v := mediatek.GetFeature("AUTO_ADD_GLOBAL_DEFINE_BY_NAME"); v != "" {
		for _, name := range strings.Fields(v) {
			value := mediatek.GetFeature(name)
			if (value != "") &&
				(value != "no") &&
				(value != "NO") &&
				(value != "none") &&
				(value != "NONE") &&
				(value != "false") &&
				(value != "FALSE") {
				cflags = append(cflags, "-D"+name)
			}
		}
	}
	if v := mediatek.GetFeature("AUTO_ADD_GLOBAL_DEFINE_BY_VALUE"); v != "" {
		for _, name := range strings.Fields(v) {
			value := mediatek.GetFeature(name)
			for _, def := range strings.Fields(value) {
				if (value != "") &&
					(value != "no") &&
					(value != "NO") &&
					(value != "none") &&
					(value != "NONE") &&
					(value != "false") &&
					(value != "FALSE") {
					cflags = append(cflags, "-D"+strings.ToUpper(def))
				}
			}
		}
	}
	if v := mediatek.GetFeature("AUTO_ADD_GLOBAL_DEFINE_BY_NAME_VALUE"); v != "" {
		for _, name := range strings.Fields(v) {
			value := mediatek.GetFeature(name)
			if (value != "") &&
				(value != "no") &&
				(value != "NO") &&
				(value != "none") &&
				(value != "NONE") &&
				(value != "false") &&
				(value != "FALSE") {
				if quote {
					cflags = append(cflags, "-D"+name+"=\\\""+value+"\\\"")
				} else {
					cflags = append(cflags, "-D"+name+"=\""+value+"\"")
				}
			}
		}
	}
	includes = append(includes, "vendor/mediatek/proprietary/hardware/audio/common/include")
	for _, base := range []string{mediatek.MtkPathCustom, mediatek.MtkPathCustomPlatform, mediatek.MtkPathCommon} {
		if base != "" {
			for _, sub := range []string{"cgen/cfgdefault", "cgen/cfgfileinc", "cgen/inc", "cgen"} {
				includes = append(includes, filepath.Join(base, sub))
			}
		}
	}
	return cflags, includes
}

func mtkGlobalDefaults(ctx android.LoadHookContext) {
	type props struct {
		Target struct {
			Android struct {
				Cflags       []string
				Include_dirs []string
			}
		}
	}
	p := &props{}
	cflags, gIncludes := mtkGlobalDefine(false)
	var includeDirs []string
	for _, dir := range gIncludes {
		if _, err := os.Stat(dir); err == nil {
			includeDirs = append(includeDirs, dir)
		}
	}
	p.Target.Android.Cflags = cflags
	p.Target.Android.Include_dirs = includeDirs
	ctx.AppendProperties(p)
}

func mtkMakeVarsProvider(ctx android.MakeVarsContext) {
	mtkGlobalCflags, mtkGlobalIncludes := mtkGlobalDefine(true)
	ctx.Strict("MTK_GLOBAL_CFLAGS", strings.Join(mtkGlobalCflags, " "))
	ctx.Strict("MTK_GLOBAL_C_INCLUDES", strings.Join(mtkGlobalIncludes, " "))
}
